# -*- coding: utf-8 -*-
from django.db import models
from djutil.models import TimeStampedModel

from dbcommon.models.default_permissions import DefaultPermissions
from .hotel import Hotel


DEFAULT_EXTERNAL_ROOM_OBJECT_ID = 1


class ExternalProviderRoomDetails(TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200, default='')
    rate_plan_name = models.CharField(max_length=50, default='', null=True)
    rate_plan_code = models.CharField(max_length=50, default='', null=True)
    room_code = models.CharField(max_length=50, default='')

    def __unicode__(self):  # __unicode__ on Python 2
        return '{0}'.format(self.name)

    def __str__(self):
        return'{0}'.format(self.name)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_external_provider_room_details'


class Room(TimeStampedModel, DefaultPermissions):
    hotel = models.ForeignKey(
        Hotel,
        related_name='rooms',
        on_delete=models.DO_NOTHING)
    room_type = models.CharField(max_length=200)
    room_type_code = models.CharField(
        max_length=200, null=True, blank=True, default='')
    price = models.DecimalField(max_digits=8, decimal_places=2)
    quantity = models.PositiveIntegerField()
    available = models.PositiveIntegerField()
    max_guest_allowed = models.PositiveIntegerField()
    description = models.TextField(max_length=2000, null=True, blank=True)
    base_rate = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    max_adult_with_children = models.PositiveIntegerField(default=1)
    max_children = models.PositiveIntegerField(default=1)
    room_area = models.PositiveIntegerField(default=200)
    cs_id = models.CharField(max_length=200, null=True, blank=True)
    external_room_details = models.ForeignKey(ExternalProviderRoomDetails, default=DEFAULT_EXTERNAL_ROOM_OBJECT_ID,
                                              on_delete=models.DO_NOTHING)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_room'
        ordering = ['hotel__name']

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s [%s]' % (self.hotel.name, self.room_type)

    def __str__(self):  # __unicode__ on Python 2
        return '%s [%s]' % (self.hotel.name, self.room_type)

    def get_occupancy_string(self):
        occupancy_message = "%(guest_count)s Guests (Max %(adult_count)s %(adult_string)s)"
        occupancy_dict = {
            'guest_count': self.max_adult_with_children,
            'adult_count': self.max_guest_allowed,
            'adult_string': ''}
        if self.max_guest_allowed > 1:
            occupancy_dict['adult_string'] = "Adults"
        else:
            occupancy_dict['adult_string'] = "Adult"
        return occupancy_message % occupancy_dict

    def get_mm_hotel_code(self):
        from apps.bookingstash.models import HotelRoomMapper
        mapper = HotelRoomMapper.objects.filter(room_id=self.id)
        if mapper:
            mapper = mapper.first()
            return mapper.mm_hotel_code
        else:
            return None

    get_mm_hotel_code.short_description = 'MM Hotel Code'

    def get_mm_room_code(self):
        from apps.bookingstash.models import HotelRoomMapper
        mapper = HotelRoomMapper.objects.filter(room_id=self.id)
        if mapper:
            mapper = mapper.first()
            return mapper.mm_room_code
        else:
            return None

    get_mm_room_code.short_description = 'MM Room Code'




