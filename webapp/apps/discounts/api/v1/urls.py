# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.discounts.api.v1.available_promotions import AvailablePromotionAPI

app_name = 'discount-promotion_v1'
urlpatterns = [url(r"^featured/$",
                   AvailablePromotionAPI.as_view(),
                   name='available-promotions'),
               ]
