# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0022_auto_20160608_1040'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('fot', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FotReservationsRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('check_in_date', models.DateField()),
                ('status', models.CharField(max_length=100)),
                ('hotel', models.ForeignKey(to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_fotreservationsrequest',
            },
        ),
    ]
