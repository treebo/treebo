# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discounts', '0019_hotelcouponmapforautoapply_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discountcoupon',
            name='code',
            field=models.CharField(default=b'', unique=True, max_length=200, db_index=True),
        ),
    ]
