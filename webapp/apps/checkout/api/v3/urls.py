from django.conf.urls import url

from apps.checkout.api.v3.booking_confirmation import ConfirmBooking
from apps.checkout.api.v3.itinerary_page_api import ItineraryPageAPI
from apps.checkout.api.v3.pay_now import PayNow
from apps.checkout.api.v3.remove_discount import RemoveDiscount
from apps.checkout.api.v3.get_discount import ApplyDiscount

app_name = 'checkout_v3'

urlpatterns = [
    url(r'^coupon/$', ApplyDiscount.as_view(), name='apply-coupon-v3'),
    url(r'^itinerary/$', ItineraryPageAPI.as_view(), name='itinerary-page-v3'),
    url(r'^coupon/remove/$', RemoveDiscount.as_view(), name='remove-coupon-v3'),
    url(r'^paynow/$', PayNow.as_view(), name='paynow-v3'),
    url(r'^confirmbooking/$', ConfirmBooking.as_view(), name='confirmbooking-v3'),

]
