from rest_framework import serializers

from dbcommon.models.location import City

__author__ = 'Rajdeep'


class CitySerializer(serializers.ModelSerializer):
    image_url = serializers.CharField(source='cover_image')

    class Meta:
        model = City
        fields = (
            'id',
            'name',
            'description',
            'tagline',
            'subscript',
            'status',
            'portrait_image',
            'landscape_image',
            'image_url',
            'city_latitude',
            'city_longitude',
            'slug')
