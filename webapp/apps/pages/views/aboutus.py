import logging

from base.views.template import TreeboTemplateView
from services.restclient.contentservicerestclient import ContentServiceClient
from data_services.respositories_factory import RepositoriesFactory

__author__ = 'devang'

logger = logging.getLogger(__name__)

city_data_service = RepositoriesFactory.get_city_repository()
class Aboutus(TreeboTemplateView):
    template_name = "aboutus/index.html"
    redirect_mobile_to_desktop_view = True

    def getData(self, request, args, kwargs):
        template_name = "aboutus/index.html"
        seo = ContentServiceClient.getValueForKey(request, 'seo_pages', 1)

        return {'scripts': [], 'styles': ['desktop/css/aboutus.css'],
                'scripts': ['desktop/js/aboutus.js'],
                'seo': seo['aboutus'], 'footerCopyrightDisabled': True,
                'cities': city_data_service.filter_cities(status=1)}

    def getMobileData(self, request, args, kwargs):
        return {'styles': ['css/terms.css']}
