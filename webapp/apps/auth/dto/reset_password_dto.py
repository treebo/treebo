from rest_framework import serializers


class ResetPasswordDTO(serializers.Serializer):
    verification_code = serializers.CharField(max_length=50)
    password = serializers.CharField(max_length=100)
    confirm = serializers.CharField(max_length=100)
