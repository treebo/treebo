import logging

from django.db.models.query_utils import Q
from django.db import IntegrityError

from apps.profiles.tasks import send_registration_email
from apps.referral.service.service import ReferralService
from common.exceptions.treebo_exception import TreeboValidationException
from dbcommon.models.profile import User
from apps.profiles import utils as profile_utils
from services.common_services.otp_service import OtpService

logger = logging.getLogger(__name__)


class UserRegistrationService:
    @staticmethod
    def register(
            firstName,
            lastName,
            phone,
            email,
            password,
            click_id="",
            referral_code="",
            send_sms=False,
            send_email=True):
        new_user = False

        user_object = UserRegistrationService.get_existing_user(email, phone)
        if user_object is None:
            logger.info("User with email %s doesn't exist", email)
            user_object = User()
            new_user = True
            user_object.email = email

        if user_object.is_guest:
            logger.info('user with email %s is guest', email)
            new_user = False
            user_object.is_guest = False
        else:
            if not new_user:
                raise TreeboValidationException("User already registered")

        try:
            user_object.email = email
            user_object.phone_number = phone
            user_object.first_name = firstName
            user_object.last_name = lastName
            user_object.set_password(password)
            user_object.is_otp_verified = False
            user_object.is_active = True
            user_object.save()
        except IntegrityError as exc:
            logger.exception(
                "Guest user %s is already registered to web", email)
            return user_object

        try:
            if send_sms and phone and phone.isdigit():
                OtpService.send_otp(phone, firstName)
        except BaseException:
            logger.error("Non string value passed as mobile number")

        logger.info(
            'Generating Referral Code for newly registered user: %s and referral code: {referral_code}',
            email,
            extra={
                "referral_code": referral_code})

        ReferralService.generate_referral_code(
            user_object,
            referrer_code=referral_code,
            share_link_identifier=click_id)
        if send_email and new_user:
            send_registration_email.delay(user_object.id)

        return user_object

    @staticmethod
    def strip_gmail_special_chars(email):
        import re
        if "gmail" in email:
            stripped_email = email.replace("@gmail.com", "")
            stripped_email = re.sub(
                r"\+.*", "", stripped_email, flags=re.IGNORECASE)
            return stripped_email.replace(".", "") + "@gmail.com"
        return email

    @staticmethod
    def get_existing_user(email, phone=None):
        sanitized_email = UserRegistrationService.strip_gmail_special_chars(
            email)
        user_object = None
        if phone:
            user_object = User.objects.filter(phone_number=phone).first()

        if not user_object and email:
            user_object = User.objects.filter(
                Q(email=sanitized_email) | Q(email=email)).first()

        return user_object

    def register_guest_user(self, first_name, last_name, email, phone):
        self.register(first_name, last_name, phone, email,
                      profile_utils.generateRandomPassword())
