# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0021_auto_20160609_0408'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='is_audit',
            field=models.BooleanField(default=False),
        ),
    ]
