# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0010_bookingrequest_session_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='add_to_cart_hotel_id',
            field=models.CharField(max_length=10, null=True, blank=True),
        ),
    ]
