# -*- coding: utf-8 -*-
from apps.common.data_classes.container import Container
from apps.pricing.data_classes import DatePrice


# pylint: disable=invalid-name
class Charge(DatePrice):
    CREDIT = 'credit'
    NON_CREDIT = 'non-credit'

    CHARGE_TYPES = [CREDIT, NON_CREDIT]

    def __init__(self, uid, name, charge_type, applicable_date, pre_tax, tax, status):
        self.uid = uid
        self.name = name

        self.charge_type = charge_type
        self.status = status

        super(Charge, self).__init__(applicable_date, pre_tax, tax)

    def is_active(self):
        return self.status in ['created']

    def __eq__(self, other):
        if isinstance(other, Charge):
            return hash(self) == hash(other)
        return NotImplemented

    def __hash__(self):
        return hash(tuple([self.uid,
                           self.name,
                           self.charge_type,
                           self.date.isoformat(),
                           int(self.pre_tax),
                           int(self.tax)],
                          ))

    def __repr__(self):
        return "{u}.{kls}: {n} - {t} ({d}, pretax:{p}, tax:{tx})".format(kls=self.__class__.__name__,
                                                                         t=self.charge_type,
                                                                         u=self.uid,
                                                                         n=self.name,
                                                                         d=self.date,
                                                                         p=self.pre_tax,
                                                                         tx=self.tax,
                                                                         )

    def __str__(self):
        return self.__repr__()


class Charges(Container):

    def active(self):
        return [item for item in self.items if item.is_active()]
