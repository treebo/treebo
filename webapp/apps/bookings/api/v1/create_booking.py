# -*- coding:utf-8 -*-
"""
    CreateBooking  API , internal api called through other apps internally
"""

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.bookings.serializers import BookingSerializer
from apps.bookings.services.booking import BookingService
from apps.bookings import constants
from base import log_args
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger
from data_services.respositories_factory import RepositoriesFactory
from data_services.exceptions import HotelDoesNotExist
from apps.common.slack_alert import SlackAlertService as slack_alert
from rest_framework.status import HTTP_409_CONFLICT, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST, HTTP_200_OK

__author__ = 'rohitjain'

logger = get_booking_logger(__name__)

hotel_repository = RepositoriesFactory.get_hotel_repository()


class CreateBooking(TreeboAPIView):
    """
        base internal api to create booking
    """
    __booking_service = BookingService()
    authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        return super(CreateBooking, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        booking_data = request.data
        hotel_id = booking_data.get('hotel_id')
        room_type = booking_data.get("room_type")
        hotels = hotel_repository.filter_hotels(id=int(hotel_id))
        hotel = hotels[0] if hotels else None
        if not hotel:
            raise HotelDoesNotExist
        #hotel = Hotel.objects.prefetch_related('rooms').get(pk=hotel_id)
        #room = hotel.rooms.filter(room_type_code=room_type).first()
        rooms = hotel_repository.get_rooms_for_hotel(hotel)
        room = next(
            (room for room in rooms if room.room_type_code == room_type),
            None)
        #room = [room for room in hotel.rooms if room.room_type_code == room_type]
        booking_data["hotel"] = hotel_id
        booking_data["room"] = room.id if room else None
        try:
            status, booking = BookingService.create_booking(booking_data)

            if status[0] == constants.BOOKING_ALREADY_PROCESSED[0]:
                return JsonResponse(
                    {"error": constants.BOOKING_ALREADY_PROCESSED[1]}, status=HTTP_409_CONFLICT)

            if status[0] == constants.BOOKING_PROCESS_FAILED[0]:
                return JsonResponse(
                    {"error": constants.BOOKING_PROCESS_FAILED[1]}, status=HTTP_500_INTERNAL_SERVER_ERROR)

            if status[0] == constants.INVALID_BOOKING_AMOUNT[0]:
                logger.error(
                    "invalid booking amount for booking %s ",
                    booking_data)
                return JsonResponse(
                    {"error": constants.INVALID_BOOKING_AMOUNT[1]}, status=HTTP_400_BAD_REQUEST)

            if not booking:
                logger.error(
                    "booking creation failed for data %s ",
                    booking_data)
                return JsonResponse({"error": error_message(
                    codes.BOOKING_CREATION_ERROR)}, status=HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                output_serializer = BookingSerializer(instance=booking)
                return JsonResponse(output_serializer.data, status=HTTP_200_OK)
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=HTTP_500_INTERNAL_SERVER_ERROR, request_param=request.POST,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception(
                "booking creation failed with internal error for data %s ",
                booking_data)
            return JsonResponse(
                {
                    "error": error_message(
                        codes.BOOKING_CREATION_ERROR),
                    "developer_message": str(e)},
                status=HTTP_500_INTERNAL_SERVER_ERROR)
