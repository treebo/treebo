# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('fot', '0005_auto_20160629_1437'),
    ]

    operations = [
        migrations.RenameField(
            model_name='fot',
            old_name='notification_status',
            new_name='show_notification',
        ),
        migrations.RenameField(
            model_name='fot',
            old_name='socail_web_profile',
            new_name='social_web_profile',
        ),
    ]
