import logging
from rest_framework.exceptions import MethodNotAllowed
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.services.cookie_service import CookieService
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import UserUnauthroized
from apps.profiles.service.user_service import UserService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.token_helper import TokenHelper
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes

logger = logging.getLogger(__name__)


class LogoutApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    user_service = UserService()
    token_helper = TokenHelper()
    cookie_service = CookieService()

    def post(self, request, format=None):
        raise MethodNotAllowed("POST")

    @log_args(logger)
    def get(self, request, format=None):
        """Register user
        """
        try:
            if not (hasattr(request, 'user')
                    and request.user and request.user.is_authenticated()):
                logger.error("email logout invalid request %s ", request.data)
                response = api_response.treebo_exception_error_response(
                    UserUnauthroized())
            else:
                user = request.user
                token = self.token_helper.get_token(request)
                self.user_service.logout_by_token(request, token)
                response = api_response.prep_success_response(
                    'User successfully logged out')
                self.cookie_service.set_user_info_in_cookie(response, user)
        except TreeboException as e:
            logger.exception("treebo logout exception %s ", request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception(
                "internal error logout exception %s ",
                request.data)
            response = api_response.internal_error_response(
                error_codes.get(error_codes.LOGIN_FAILED)['msg'], LogoutApi.__name__)
        return response
