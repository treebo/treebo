# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0029_errorcodes'),
        ('dbcommon', '0035_merge'),
    ]

    operations = [
    ]
