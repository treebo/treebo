import logging

from django.core.cache import cache

from apps.bookingstash.service.availability_service import get_availability_service
from base import log_args

logger = logging.getLogger(__name__)


class SearchCacheService:
    @log_args(logger)
    def get_min_hotels_price_from_cache(self, hotel_ids, checkin_date, checkout_date, room_config):
        hotel_price_list = {}
        available_hotel_ids = []
        
        available_hotels_dict = get_availability_service().get_available_rooms_for_hotels(hotel_ids, checkin_date,
                                                                                          checkout_date, room_config)
        for availability_tupple in available_hotels_dict:
            available_hotel_ids.append(availability_tupple[0])
        
        try:
            for hotel_id in hotel_ids:
                
                min_sell_price_cache_index_key = str(hotel_id) + '_min_sell_price'
                min_sell_price = cache.get(min_sell_price_cache_index_key)
                
                if hotel_id not in available_hotel_ids:
                    hotel_price_list[hotel_id] = {"net_payable_amount": 0}
                else:
                    if min_sell_price:
                        hotel_price_list[hotel_id] = {"net_payable_amount": min_sell_price}
                    else:
                        hotel_price_list[hotel_id] = {"net_payable_amount": 1800}
        
        except Exception as err:
            logger.error("Minimum price from cache failed", str(err))
            raise err
        return hotel_price_list
