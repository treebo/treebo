import logging
from decimal import Decimal

from django.db.models import Q
from django.utils.encoding import smart_str

from apps.reviews.models import TAHotelIDMapping, TAReviewRatingCountMapping

logger = logging.getLogger(__name__)


class HotelReviewCountService(object):
    def hotel_review_count_mapping(self, location_serializer, ta_loc_id):
        try:
            ta_hotel_map = TAHotelIDMapping.objects.filter(
                ta_location_id=ta_loc_id).first()
            if ta_hotel_map:
                logger.info(
                    "Mapping between hotel %s and ta %s exists in HotelReviewCountService",
                    ta_hotel_map.hotel.id,
                    ta_hotel_map.ta_location_id)

                location = location_serializer.data
                self.create_or_update_review_count(location, ta_hotel_map)
            else:
                logger.info(
                    "No mapping for the given tid exists %s for review count",
                    ta_loc_id)

        except Exception:
            logger.exception(
                "Exception in creating new entry in review count mapping table for %s",
                ta_loc_id)

    def create_or_update_review_count(self, location, ta_hotel_map):
        try:
            total = 0
            logger.info("count %s", location['review_rating_count'])
            if location['review_rating_count']:
                for key in list(location['review_rating_count'].keys()):
                    val = int(location['review_rating_count'][key])
                    total += val

                for key in list(location['review_rating_count'].keys()):
                    val = location['review_rating_count'][key]
                    try:
                        count_value, create = TAReviewRatingCountMapping.objects.get_or_create(
                            hotel_ta_mapping=ta_hotel_map, review_rating=str(key))

                        if create:
                            logger.info(
                                "create %s in review_rating_count Entry  .. create/update for hotel %s and ta_loc_id %s",
                                create,
                                ta_hotel_map.hotel.id,
                                ta_hotel_map.ta_location_id)

                    except TAReviewRatingCountMapping.MultipleObjectsReturned:
                        count_value = TAReviewRatingCountMapping.objects.filter(
                            hotel_ta_mapping=ta_hotel_map, review_rating=str(key)).first()
                    self.update_review_count_table(count_value, total, val)
        except Exception:
            logger.exception(
                "Exception in saving review count mapping object for %s",
                smart_str(
                    location['location_id']))

    def update_review_count_table(self, count_value, total, val):
        count_value.count_of_customers = smart_str(val)
        count_value.percentage_of_customers = Decimal(
            (Decimal(val) / Decimal(total)) * 100)
        count_value.save()

    def fetch_review_count(self, ta_loc_id=None, hotel_id=None):
        try:
            if ta_loc_id or hotel_id:
                reviews_count = TAReviewRatingCountMapping.objects.filter(
                    Q(hotel_ta_mapping__ta_location_id=ta_loc_id) | Q(hotel_ta_mapping__hotel__id=hotel_id))
                if reviews_count:
                    return reviews_count
                else:
                    logger.error(
                        "No reviews_count mapping exist for ta_loc_id %s and hotel_id %s",
                        ta_loc_id,
                        hotel_id)
                    return None
            else:
                logger.debug(
                    "both ta_loc_id and hotel_id are None in fetching reviews_count mappings")
        except Exception:
            logger.exception(
                "Exception in fetching the reviews_count mapping for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)
            return None

    def delete_reviews_count(self, ta_loc_id=None, hotel_id=None):
        try:
            reviews_count = self.fetch_review_count(
                ta_loc_id=ta_loc_id, hotel_id=hotel_id)
            for rev_count in reviews_count:
                rev_count.delete()
                logger.info(
                    "Deleted reviews_count successfully for ta_loc_id %s and hotel-id %s",
                    ta_loc_id,
                    hotel_id)
            else:
                logger.error(
                    "No reviews_count mapping exist for ta_loc_id %s and hotel_id %s",
                    ta_loc_id,
                    hotel_id)
        except Exception:
            logger.exception(
                "Exception in deleting reviews_count mapping for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)
