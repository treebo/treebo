import json
import logging

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework import status

from apps.featuregate.feature_manager_service import FeatureManagerService
from apps.featuregate.serializer.feature_manager_dto import FeatureManagerDTO, FeatureManagerSerializer
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class FeatureManagerAPI(TreeboAPIView):
    feature_manager_service = FeatureManagerService()

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FeatureManagerAPI, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request):
        try:
            try:
                feature_manager_serializer = FeatureManagerSerializer(
                    data=request.GET)
            except Exception as exc:
                logger.exception(exc)
                context = {
                    "status": "error",
                    "msg": "Please send all key variables"}
                return Response(context, status=status.HTTP_400_BAD_REQUEST)

            if not feature_manager_serializer.is_valid():
                return Response(
                    feature_manager_serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST)

            feature_manager_dto = feature_manager_serializer.save()
            feature_manager_data = self.feature_manager_service.get_overridden_values(
                feature_manager_dto)

            if feature_manager_data:
                return Response(feature_manager_data)
            else:
                context = {
                    "status": "error",
                    "msg": "No overridden values found"}
                return Response(context)

        except Exception as exc:
            logger.exception(str(exc))
            context = {
                "status": "error",
                "msg": "Unbale to fetch feature information"}
            return Response(context, status=500)

    @log_args(logger)
    def post(self, request, format=None):
        raise Exception('Method not supported')
