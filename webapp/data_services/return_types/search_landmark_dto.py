from data_services.return_types.dto import DTO


class SearchLandmarkDTO(DTO):

    def __init__(self, properties_dict):
        super(SearchLandmarkDTO, self).__init__(properties_dict)

    def get_sitemap_url(self):
        return self.sitemap_url
