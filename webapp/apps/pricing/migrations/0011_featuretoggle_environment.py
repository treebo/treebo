# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pricing', '0010_featuretoggle_roompricev2'),
    ]

    operations = [
        migrations.AddField(
            model_name='featuretoggle',
            name='environment',
            field=models.CharField(default=b'preprod', max_length=200),
        ),
    ]
