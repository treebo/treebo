import logging

from apps.bookingstash.service.availability_service import get_availability_service
from apps.discounts import CouponValidationParams
from apps.discounts.services.discount_coupon_services import DiscountCouponServices
from apps.discounts.services.hotel_coupon_auto_apply_service import HotelCouponAutoApplyService
from apps.pricing.api.v4 import RoomPrice
from apps.pricing.services.price_request_dto import PriceRequestDTO_V3
from apps.pricing.services.pricing_service_v3 import PricingServiceV3
from apps.reward.service.reward import RewardService
from apps.pricing.transformers.search_api_transformer import SearchAPITransformer

logger = logging.getLogger(__name__)


class ExternalAPITransformer(object):
    reward_service = RewardService()
    hotel_autoapply_service = HotelCouponAutoApplyService()
    discount_coupon_service = DiscountCouponServices()
    pricing_service = PricingServiceV3()
    srp_pricing_service = SearchAPITransformer()

    def fetch_hotel_room_prices(self, hotel_ids, checkin_date, checkout_date, room_config, utm_source):
        logged_in_user = False
        room_config_list = room_config.split(",")  # StashUtils.room_config_string_to_list(room_config)
        room_availability_per_hotel = get_availability_service().get_available_rooms_for_hotels(hotel_ids,
                                                                                checkin_date,
                                                                                checkout_date,
                                                                                room_config)
        hotel_discount_map = self.srp_pricing_service.construct_hotel_discount_map(room_availability_per_hotel,
                                                                                   hotel_ids)
        price_request_dto = PriceRequestDTO_V3(checkin_date, checkout_date, hotel_discount_map, room_config_list,
                                               logged_in_user=logged_in_user, utm_source=utm_source)
        hotel_wise_cumulative_prices = self.pricing_service.get_search_page_price(price_request_dto)
        hotel_price_details = self.transform_pricing_for_display(hotel_wise_cumulative_prices, hotel_ids,
                                                                 room_config, checkin_date, checkout_date,
                                                                 user=None, apply_wallet=False,
                                                                 utm_source=utm_source,
                                                                 logged_in_user=False)
        return hotel_price_details

    def transform_pricing_for_display(self, hotel_wise_cumulative_prices, hotel_id_list,
                                      room_config, checkin_date, checkout_date, user, apply_wallet,
                                      utm_source=None, logged_in_user=False):
        """
        Transforms the prices returned from pricing service to what the display requires
        :param hotel_wise_cumulative_prices: Pricing Service response
        :param hotel_id_list: List of hotel ids
        :param room_config: room config
        :param checkin_date: check in
        :param checkout_date: check out
        :param utm_source : utm source
        :return:
        """
        room_availability_per_hotel = get_availability_service().get_available_rooms_for_hotels(
            hotel_id_list, checkin_date, checkout_date, room_config)
        logger.info(
            "Room Availability per hotel: %s",
            room_availability_per_hotel)
        hotel_ids = [int(hotel_id) for hotel_id in hotel_id_list]
        hotel_price_details = self.get_hotel_room_rateplan_map(hotel_ids, hotel_wise_cumulative_prices,
                                                               room_availability_per_hotel)
        hotel_price_details_from_auto_apply, hotel_coupon_map = self.apply_auto_coupon(hotel_price_details, hotel_ids,
                                                                                       utm_source, checkin_date,
                                                                                       checkout_date, room_config,
                                                                                       user, logged_in_user,
                                                                                       room_availability_per_hotel)

        logger.info("hotel_price_details_from_auto_apply %s ", hotel_price_details_from_auto_apply)
        logger.info("hotel_price_details %s ", hotel_price_details)
        final_data = dict()
        for key, room_price in hotel_price_details_from_auto_apply.items():
            hotel_id, room_type, rate_plan = key
            hotel_id = int(hotel_id)
            if not final_data.get(hotel_id):
                final_data[hotel_id] = {}
            final_data[int(hotel_id)][room_type] = room_price
        return final_data

    def apply_auto_coupon(self, hotel_price_details, hotel_ids, utm_source, checkin_date, checkout_date, room_config, user, logged_in_user,
                          room_availability_per_hotel):
        if not utm_source:
            return hotel_price_details, {}
        hotel_coupon_map = self.hotel_autoapply_service.get_hotel_coupons_basis_hotel_ids_and_utm_source(hotel_ids, utm_source, checkin_date,
                                                                                                         checkout_date)
        hotel_percentage_map = self.get_discount_percetage_for_auto_apply(hotel_price_details, hotel_coupon_map, checkin_date, checkout_date,
                                                                          room_config, utm_source, user)
        logger.info("hotel_coupon_map %s  ", hotel_coupon_map)
        logger.info("hotel_percentage_map %s  ", hotel_percentage_map)
        if hotel_percentage_map:
            room_config_list = room_config.split(",")
            price_request_dto = PriceRequestDTO_V3(checkin_date, checkout_date, hotel_percentage_map, room_config_list,
                                                   logged_in_user=logged_in_user, utm_source=utm_source)
            hotel_wise_cumulative_prices = self.pricing_service.get_search_page_price(price_request_dto)
            hotel_price_details = self.get_hotel_room_rateplan_map(hotel_ids, hotel_wise_cumulative_prices, room_availability_per_hotel)
            logger.info("hotel_percentage_map new for auto apply %s  ", hotel_price_details)
        return hotel_price_details, hotel_coupon_map

    def get_discount_percetage_for_auto_apply(self, hotel_price_details, hotel_coupon_map, checkin_date, checkout_date, room_config, utm_source,
                                              user):
        discount_percentage_map = dict()
        if not hotel_coupon_map:
            return discount_percentage_map
        for key, minimal_available_room_price in hotel_price_details.items():
            hotel_id, room_type, rate_plan = key
            aa_coupon_code = hotel_coupon_map.get(int(hotel_id), None)
            if not hotel_coupon_map.get(int(hotel_id), None):
                discount_percentage_map[key] = 0
                continue
            if not minimal_available_room_price:
                discount_percentage_map[key] = 0
                continue
            city_id = None
            price = minimal_available_room_price.price
            pretax_price = price.pretax_price
            member_discount = price.member_discount
            total_amount = price.sell_price
            rack_rate = price.rack_rate
            room_type = None
            booking_channel = None
            paytype = None
            param = CouponValidationParams(checkin_date, checkout_date, room_config, aa_coupon_code, hotel_id, city_id, user, user,
                                           pretax_price, member_discount, total_amount, rack_rate, room_type, booking_channel, paytype, utm_source)
            coupon, discount_amount, discount_error = DiscountCouponServices.validate_and_apply_coupon_code(param)
            discount_percent = DiscountCouponServices.get_discount_percentage(discount_amount, pretax_price, member_discount, total_amount)
            discount_percentage_map[key] = discount_percent
        return discount_percentage_map

    def get_hotel_room_rateplan_map(self, hotel_ids, hotel_wise_cumulative_prices, room_availability_per_hotel):
        final_data = dict()
        for hotel_id in hotel_ids:
            room_wise_price = hotel_wise_cumulative_prices.get(hotel_id, dict())
            if not room_wise_price:
                logger.warning("room_wise_price is not available for hotel %s ", hotel_id)
                continue
            for room_type, rate_plan_price in room_wise_price.items():
                minimal_available_room_price = None
                minimal_available_room_type = 'oak'
                minimal_available_rate_plan = None
                for rate_plan, rate_plan_prices in rate_plan_price.items():
                    room_price = RoomPrice(room_type, rate_plan, rate_plan_prices.get('price'),
                                           rate_plan_prices.get('meta'))
                    room_type = room_price.room_type
                    available_rooms = room_availability_per_hotel.get((hotel_id, room_type), 0)
                    if available_rooms > 0 and room_price.price.base_price > 0 and (not minimal_available_room_price or
                                                                                    minimal_available_room_price.price.sell_price >
                                                                                    room_price.price.sell_price):
                        minimal_available_room_price = room_price
                        minimal_available_rate_plan = rate_plan
                        minimal_available_room_type = room_type
                key = hotel_id, minimal_available_room_type, minimal_available_rate_plan
                final_data[key] = minimal_available_room_price
        return final_data