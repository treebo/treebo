from django.conf.urls import url

from apps.true_caller.api.v1.fetch_user_detail import FetchUserDetail
from apps.true_caller.api.v1.get_request_nonce import GetRequestNonce
from apps.true_caller.api.v1.submit_response_webhook import SubmitResponseWebhook

app_name = 'true_caller_v1'
urlpatterns = [
    url(r"^request-nonce/$", GetRequestNonce.as_view(), name='get-request-nonce'),
    url(r"^user-detail/$", FetchUserDetail.as_view(), name='fetch-user-detail'),
    url(r"^webhook/submit-response$", SubmitResponseWebhook.as_view(), name='submit-response-webhook')
]
