class UrlConverter:

    def __init__(self):
        pass

    @staticmethod
    def replace_url_string(url):
        """
        replace http with https in the url string
        :param url:
        :return new_url:
        """
        new_url = url.replace('http://', 'https://')
        return new_url
