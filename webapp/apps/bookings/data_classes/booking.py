# -*- coding: utf-8 -*-
import datetime
import logging

from django.conf import settings

from apps.bookings.data_classes.payments import Payments
from apps.common.data_classes.base_data_class import BaseDataClass
from apps.common.data_classes.user import User
from apps.common.date_time_utils import is_naive_datetime, blindly_replace_with_application_timezone
from .charges import Charges
from .guest import Guests
from .room_stay import RoomStays
from .status import Status, InitiatedStatus, Temporary, ConfirmedStatus
from apps.hotels.templatetags.url_tag import get_image_uncode_url

logger = logging.getLogger(__name__)


# pylint: disable=too-many-instance-attributes
class Booking(BaseDataClass):

    def __init__(self,
                 uid,
                 hotel_id,
                 gst_details,
                 source,
                 comments='',
                 additional_emails_for_voucher=None
                 ):

        # default initial values
        self.crs_reference_id = None
        self.crs_version = None
        self.crs_bill_id = None
        self.crs_bill_version = None

        self.pricing_scheme = None
        self.payment_type = None
        self.created_timestamp = None
        self.cancelled_timestamp = None

        self.payable_amount = None
        self.pre_tax_amount = None
        self.tax_amount = None
        self.paid_amount = None
        self.total_amount = None

        self._expiry_time_stamp = None
        self._status = InitiatedStatus
        self._booking_owner = None
        self._stay_source = None

        self._room_stays = RoomStays([])
        self._charges = Charges([])
        self._payments = Payments([])
        # set initial values
        self.uid = uid
        self.hotel_id = hotel_id
        self.gst_details = gst_details
        self.source = source
        self.comments = comments
        self.is_user_logged_in = False
        self.additional_emails_for_voucher = additional_emails_for_voucher or []

    @property
    def booking_id(self):
        return self.uid

    @property
    def checkin(self):
        checkins = [li.checkin for li in self.room_stays if li.is_active]
        if not checkins:
            # in case of cancelled bookings
            checkins = [li.checkin for li in self.room_stays]
        return min(checkins)

    @property
    def checkout(self):
        checkouts = [li.checkout for li in self.room_stays if li.is_active]
        if not checkouts:
            # in case of cancelled bookings
            checkouts = [li.checkout for li in self.room_stays]
        return max(checkouts)

    @property
    def room_stays(self):
        return self._room_stays

    @property
    def charges(self):
        return self._charges

    @property
    def payments(self):
        return self._payments

    @property
    def status(self):
        return self._status

    @property
    def expiry_time_stamp(self):
        if self.status > Temporary:
            return None
        return self._expiry_time_stamp

    @property
    def guests(self):
        all_guests = [guest for room_stay in self.room_stays for guest in room_stay.guests]
        return Guests(all_guests)

    @property
    def booking_owner(self):
        return self._booking_owner

    @property
    def is_active(self):
        return any([rs.is_active for rs in self.room_stays])

    @property
    def stay_source(self):
        return self._stay_source

    @property
    def no_of_nights(self):
        """ With c/i and c/o at room level this gets complicated. To keep it simple for now"""
        return abs((self.checkout.date() - self.checkin.date()).days)

    @booking_owner.setter
    def booking_owner(self, booking_owner):
        if not isinstance(booking_owner, User):
            raise Exception
        self._booking_owner = booking_owner

    @room_stays.setter
    def room_stays(self, room_stays):
        if isinstance(room_stays, RoomStays):
            self._room_stays = room_stays
        else:
            self._room_stays = RoomStays(room_stays)

    @charges.setter
    def charges(self, charges):
        if isinstance(charges, Charges):
            self._charges = charges
        else:
            self._charges = Charges(charges)

    @payments.setter
    def payments(self, payments):
        if isinstance(payments, Payments):
            self._payments = payments
        else:
            self._payments = Payments(payments)

    @stay_source.setter
    def stay_source(self, stay_source):
        self._stay_source = stay_source

    @classmethod
    def stay_source_from_db_model(cls, db_model):
        if db_model.payment_mode.lower() == "paid":
            return "Web (Prepaid)"
        if db_model.payment_mode.lower() == "part paid":
            return "Web (Part-paid)"

        return "Web (Pay@hotel)"

    @classmethod
    def expiry_time_stamp_from_db_model(cls, db_model):
        if db_model.booking_status == db_model.INITIATED and db_model.payment_mode in [db_model.PAID,
                                                                                       db_model.PART_PAID]:
            expiry_timestamp = blindly_replace_with_application_timezone(
                datetime.datetime.now()) + datetime.timedelta(
                minutes=settings.HOLD_TIME_IN_MINS_TO_CONFIRM_BOOKING)
            return expiry_timestamp
        return None

    @expiry_time_stamp.setter
    def expiry_time_stamp(self, time_stamp):
        if self.status.name == ConfirmedStatus.name:
            time_stamp = None

        if time_stamp is not None:
            assert isinstance(time_stamp, datetime.datetime)
            if is_naive_datetime(time_stamp):
                raise RuntimeError('Need time zone aware expiry time stamp')
        self._expiry_time_stamp = time_stamp

    @status.setter
    def status(self, value):
        assert isinstance(value, Status)
        self._status = value

    def __repr__(self):
        return "<{kls}:{u}>".format(kls=self.__class__.__name__,
                                    u=self.uid,
                                    )

    def __str__(self):
        return "{kls}:{u}".format(kls=self.__class__.__name__,
                                  u=self.uid,
                                  )

    @classmethod
    def get_hotel_details_from_db_model(cls, db_model):
        hotel_detail = {
            "state": str(db_model.hotel.state.name) if db_model.hotel and db_model.hotel.state else "",
            "address": str(db_model.hotel.address()) if db_model.hotel and db_model.hotel.address() else "",
            "city": str(db_model.hotel.get_city_name()) if db_model.hotel and db_model.hotel.get_city_name() else "",
            "locality": str(db_model.hotel.locality.name) if db_model.hotel and db_model.hotel.locality else "",
            'hotel_code': str(db_model.hotel.hotelogix_id) if db_model.hotel and db_model.hotel.hotelogix_id else "",
            'hotel_cs_id': str(db_model.hotel.cs_id) if db_model.hotel and db_model.hotel.cs_id else "",
            'hotel_id': db_model.hotel.id if db_model.hotel and db_model.hotel.id else "",
            'contact_number': db_model.hotel.phone_number if db_model.hotel and db_model.hotel.phone_number else "",
            'hotel_name': str(db_model.hotel.name) if db_model.hotel and db_model.hotel.name else "",
            'longitude': str(db_model.hotel.longitude) if db_model.hotel and db_model.hotel.longitude else "",
            'latitude': str(db_model.hotel.latitude) if db_model.hotel and db_model.hotel.latitude else "",
            'images': [str(get_image_uncode_url(image.url.url)) for image in db_model.hotel.image_set.all()[:5]],
            'showcased_image_url': str(get_image_uncode_url(db_model.hotel.get_showcased_image_url().url))
            if db_model.hotel and db_model.hotel.get_showcased_image_url() and db_model.hotel.get_showcased_image_url().url else "",
            'property': {
                'provider': db_model.hotel.provider_name,
                'type': db_model.hotel.property_type
            }
        }
        return hotel_detail

    @classmethod
    def get_room_config_from_db_model(cls, db_model):
        roomType = db_model.room.room_type_code if db_model.room else ""
        roomCount = len(db_model.room_config.split(",")) if db_model.room_config else 0

        room_config = {
            "room_count": roomCount, "no_of_adults": db_model.adult_count,
            "no_of_childs": db_model.child_count, "room_type": roomType,
        }
        return room_config

    @classmethod
    def get_audit_from_db_model(cls, db_model):
        return db_model.is_audit

    @classmethod
    def get_room_config_string_from_db(cls, db_model):
        roomconfig = db_model.room_config if db_model.room_config else ''
        return roomconfig

    @classmethod
    def get_wallet_applied_status_from_db_model(cls, db_model):
        return db_model.wallet_applied

    @classmethod
    def get_wallet_deducted_amount_from_db_model(cls, db_model):
        return db_model.wallet_deduction

    @classmethod
    def get_discount_amount_from_db_model(cls, db_model):
        return db_model.discount

    @classmethod
    def get_voucher_amount_from_db_model(cls, db_model):
        return db_model.voucher_amount

    @classmethod
    def get_member_discount_from_db_model(cls, db_model):
        return db_model.member_discount

    @classmethod
    def get_coupon_code_from_db_model(cls, db_model):
        return db_model.coupon_code

    @classmethod
    def get_rate_plan_from_db_model(cls, db_model):
        return db_model.rate_plan

    @classmethod
    def get_booking_table_id_from_db_model(cls, db_model):
        return db_model.id

    @classmethod
    def get_checkin_checkout_time_from_db_model(cls,db_model):
        try:
            time_tuple = db_model.comments.split(',')
            checkin_time = str(time_tuple[0].split(':')[1])
            checkout_time = str(time_tuple[1].split(':')[1])
            checkout_time = checkout_time[0:4]
            return checkin_time, checkout_time
        except Exception as exc:
            return "", ""
