import logging

from django.conf import settings
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from apps.bookingstash.service.availability_service import get_availability_service
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.pricing.exceptions import RequestError, PriceNotAvailableForHotel
from apps.pricing.services.price_request_dto import PriceRequestDto
from apps.pricing.services.pricing_service import SoaError
from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.pricing.transformers.hd_page_api_transformer import HDPageAPITransformer
from base.views import validators
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class HotelRoomPricesSerializer(serializers.Serializer):
    checkin = serializers.DateField()
    checkout = serializers.DateField()
    roomconfig = serializers.CharField(
        max_length=100, validators=[
            validators.validate_roomconfig])
    apply_wallet = serializers.BooleanField(
        default=settings.APPLY_WALLET, required=False)


class HotelRoomPrices(TreeboAPIView):
    pricing_service = PricingServiceV2()
    validationSerializer = HotelRoomPricesSerializer
    price_response_transformer = HDPageAPITransformer()

    def get(self, request, *args, **kwargs):
        """
        API to fetch room prices for all rooms for the given hotel.
        checkin -- Checkin Date (Use format: yyyy-MM-dd)
        checkout -- Checkout Date (Use format: yyyy-MM-dd)
        roomconfig -- Room Configuration in format: 1-0,2-0,2-1 (For a-c, 'a' is adult count, and 'c' is children count)
        """
        try:
            if hasattr(
                    request,
                    'user') and request.user and request.user.is_authenticated():
                logger.info("request.user %s is logged in ", request.user)
                logged_in_user = True
            else:
                logged_in_user = False
            validated_data = self.serializerObject.validated_data
            apply_wallet = bool(validated_data['apply_wallet'])
            user = request.user
            price_info = self.process_validated_data(
                validated_data, kwargs, apply_wallet, user, logged_in_user=logged_in_user)
            return Response(price_info)
        except PriceNotAvailableForHotel as price_error:
            logger.exception("PriceNotAvailable Error")
            return Response({"error": {"msg": price_error.message,
                                       "code": price_error.error_code}},
                            status=HTTP_500_INTERNAL_SERVER_ERROR)
        except RequestError as e:
            logger.exception("RequestError received")
            return Response({"error": error_message(
                codes.INVALID_REQUEST)}, status=HTTP_400_BAD_REQUEST)
        except SoaError as e:
            logger.exception(
                "SoaError Exception occurred while applying coupon code using SOA API")
            return Response({"error": error_message(
                codes.INVALID_REQUEST)}, status=e.status)

        except Exception as e:
            logger.exception(
                "Unknown Exception occurred while applying coupon code using SOA API")
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        dev_msg="WEB Pricing V4", message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            return Response({"error": error_message(
                codes.REQUEST_FAILED)}, status=HTTP_500_INTERNAL_SERVER_ERROR)

    def process_validated_data(
            self,
            validated_data,
            kwargs,
            apply_wallet=False,
            user=None,
            logged_in_user=False):
        checkin_date, checkout_date, room_config = validated_data[
            'checkin'], validated_data['checkout'], validated_data['roomconfig']
        utm_source = None
        # StashUtils.room_config_string_to_list(room_config)
        room_config_list = room_config.split(",")
        hotel_id = int(kwargs["hotel_id"])
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotel = hotel_repository.get_hotel_by_id_from_web(hotel_id=hotel_id)
        if not hotel:
            raise RequestError(
                status=HTTP_400_BAD_REQUEST,
                message=codes.INVALID_HOTEL_ID)
        price_request_dto = PriceRequestDto(
            checkin_date,
            checkout_date,
            [hotel_id],
            room_config_list,
            logged_in_user=logged_in_user)

        room_wise_cumulative_prices = self.pricing_service.get_hd_page_price(
            price_request_dto)
        if not room_wise_cumulative_prices:
            raise PriceNotAvailableForHotel(hotel.name)

        room_wise_prices, wallet_info = self.price_response_transformer.transform_pricing_for_display(
            room_wise_cumulative_prices, hotel, apply_wallet, user, checkin_date, checkout_date, hotel_id, utm_source, room_config, logged_in_user)

        self._update_availability(
            hotel_id,
            checkin_date,
            checkout_date,
            room_config,
            room_wise_prices)
        final_price = {
            "total_price": room_wise_prices,
            "wallet_info": wallet_info
        }
        return final_price

    def _update_availability(
            self,
            hotel_id,
            checkin_date,
            checkout_date,
            room_config,
            room_wise_prices):
        available_rooms = get_availability_service().get_available_rooms(
            int(hotel_id), checkin_date, checkout_date, room_config)

        logger.info("Available Rooms: %s", available_rooms)

        for room in list(available_rooms.keys()):
            room_type_code = room.room_type_code.lower()
            room_price = room_wise_prices[room_type_code]
            if room_price.get('rate_plans'):
                room_price['availability'] = True
                room_price['available'] = available_rooms[room]
