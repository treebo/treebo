from rest_framework import serializers

from apps.common import date_time_utils as dt

from apps.common.serializer_fields import CommaSeparatedIntegerField


class AvailabilityRequestDTO(serializers.Serializer):
    """
    Dto to store the request for fetching availability information for a list of hotels
    """
    checkin = serializers.DateField(required=True, format=dt.DATE_FORMAT)
    checkout = serializers.DateField(required=True, format=dt.DATE_FORMAT)
    hotels = CommaSeparatedIntegerField(required=True)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass
