import logging
import json
from django.conf import settings

from services.common_services.api_retry_service import APIRetryService

logger = logging.getLogger(__name__)


class TreeboNotificationService(object):
    SEND_EMAIL_URL = '/v1/notification/email/'
    SEND_SMS_URL = '/v1/notification/sms/'
    api_service = APIRetryService()

    def send_email(
            self,
            emails,
            content,
            subject,
            attachments=[],
            consumer='web'):
        api_request = self._prepare_request_for_email(
            subject, content, emails, attachments, consumer)
        email_notif_api_utl = str(
            settings.NOTIFICATION_API_HOST) + self.SEND_EMAIL_URL
        json_string = json.dumps(api_request, default=lambda o: o.__dict__)
        headers = {'content-type': 'application/json'}
        logger.info(" Send Email api request %s ", json_string)
        json_response = self.api_service.post_api_response_with_retry(
            email_notif_api_utl, json_string, headers, 3)
        logger.info(
            "Calling Notification API to send email to %s subject %s with content %s and response %s ",
            emails,
            subject,
            content,
            json_response)
        return json_response

    def send_sms(self, phones, sms_text):
        api_request = self._prepare_request_for_sms(phones, sms_text)
        sms_notif_api_utl = str(
            settings.NOTIFICATION_API_HOST) + self.SEND_SMS_URL
        json_string = json.dumps(api_request, default=lambda o: o.__dict__)
        headers = {'content-type': 'application/json'}
        logger.info(" Send SMS api request %s ", json_string)
        json_response = self.api_service.post_api_response_with_retry(
            sms_notif_api_utl, json_string, headers, 3)
        logger.info(
            "Sending sms via notification service to %s and response %s",
            phones,
            json_response)
        data = json_response['data']['data']
        return data['notification_id']

    def _prepare_request_for_sms(self, phonenumbers, text):
        return {
            "data": {
                "message_type": "Whitelisted",
                "body": text,
                "sender": "",
                "receivers": phonenumbers,
                "consumer": "otp_service"
            }
        }

    def _prepare_request_for_email(
            self,
            subject,
            content,
            emails,
            attachments,
            consumer='web'):
        sender_name = str(settings.SERVER_EMAIL.split('<')[0]).strip()
        sender = str(settings.SERVER_EMAIL.split('<')[1]).split('>')[0]
        return {
            "data": {
                "subject": subject,
                "body_text": "",
                "body_html": content,
                "sender": sender,
                "sender_name": sender_name,
                "receivers": {
                    "to": emails,
                    "cc": [],
                    "bcc": []
                },
                "consumer": consumer,
                "attachments": attachments
            }
        }
