import logging
import io
import csv

import datetime
from django.conf import settings
from django.core.cache import cache

from apps.pricing.services.pricing_service import PricingService
from apps.reviews.models import HotelTAOverallRatings
from apps.third_party.service.criteo_service import GenerateCriteoCSV
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.encoding import smart_str

from data_services.respositories_factory import RepositoriesFactory

__author__ = 'ansilkareem'

from apps.third_party.service.trip_advisor import constants
from django.core.mail import EmailMessage

logger = logging.getLogger(__name__)


class CriteoAPI(APIView):
    help = "create csv for criteo"

    hotel_repository = RepositoriesFactory.get_hotel_repository()

    def get(self, request):
        try:
            criteo_file = GenerateCriteoCSV()
            self.send_csv_file(criteo_file.create_and_save_csv())
            return Response({"success": True}, status=200)
        except BaseException:
            logger.exception('Exception occured while generating criteo csv')
            return Response({"success": False}, status=400)

    def create_and_push_csv(self):
        logger.info("updating csv")
        csv_writer, csv_file = self.initilaize_csv()
        logger.info("Done")
        hotels = self.hotel_repository.get_active_hotel_list()
        for hotel in hotels[:10]:
            self.append_to_csv(csv_writer, hotel)

    def initilaize_csv(self, ):
        logger.info(" Initializing CSV for hotel inventory of Treebo")
        csv_file = io.StringIO()
        field_names = [
            'Hotel ID',
            'Name',
            'Base Price',
            'Sale Price',
            'URL',
            'Category id 1',
            'image url',
            'star rating',
            'badges',
            'category id 2',
            'description']
        csv_writer = csv.DictWriter(
            csv_file, delimiter=',', fieldnames=field_names)
        csv_writer.writeheader()

        return csv_writer, csv_file

    def send_csv_file(self, csv_file):
        try:
            logger.info("Sending Updated Criteo CSV")
            mail = EmailMessage(
                constants.CRITEO_CSV_EMAIL_SUBJECT,
                constants.CRITEO_CSV_EMAIL_BODY,
                settings.SERVER_EMAIL,
                settings.CRITEO_CSV_EMAIL_ID)
            mail.attach(
                constants.CRITEO_CSV_FILE_NAME,
                csv_file.getvalue(),
                'text/csv')
            mail.send()
            logger.info("mail sent")
        except Exception:
            logger.exception("Error occured in sending criteo csv email")

    def append_to_csv(self, csv_writer, hotel):
        logger.info("Appending to CSV for the Hotel ID %s", hotel.id)
        checkin = str(
            (datetime.datetime.now() +
             datetime.timedelta(
                days=constants.CHECKIN_DATE_CRITEO_DIFF)).date())
        checkout = str(
            (datetime.datetime.now() +
             datetime.timedelta(
                days=constants.CHECKOUT_DATE_CRITEO_DIFF)).date())
        sitemap_url = self.hotel_repository.get_sitemap_url(hotel)
        base_price, sale_price = self.get_prices_for_criteo_feed(
            hotel.id, checkin, checkout, constants.CRITEO_ROOM_CONFIG)
        url = constants.CRITEO_HD_PAGE_URL % (
            constants.BASE_URL, sitemap_url, checkin, checkout, constants.CRITEO_ROOM_CONFIG)
        item = {
            "Hotel ID": smart_str(hotel.id),
            "Name": smart_str(hotel.name),
            "Base Price": smart_str(base_price),
            "Sale Price": smart_str(sale_price),
            "URL": smart_str(url),
            "Category id 1": smart_str(hotel.city.name),
            "badges": "AC Room,TV,Free WiFi,Breakfast,Best Toiletries",
            "category id 2": smart_str(hotel.locality),
            "description": smart_str(hotel.description)
        }
        image_data_services = RepositoriesFactory.get_image_repository()
        images = image_data_services.get_images_for_hotels(hotels=[hotel.id])
        if images:
            image = images[0]
        else:
            image = None
        #image = Image.objects.filter(hotel=hotel).first()
        rating_obj = HotelTAOverallRatings.objects.filter(
            hotel_ta_mapping__hotel__id=hotel.id).values('overall_rating')
        if rating_obj:
            item.update({
                "star rating": smart_str(rating_obj[0]['overall_rating'])
            })
        if image:
            item.update({
                "image url": constants.IMAGE_URL + str(image.url)
            })

        csv_writer.writerow(item)

    def get_prices_for_criteo_feed(
            self,
            hotel_id,
            checkin,
            checkout,
            room_config):
        base_price = 0
        sale_price = 0
        try:
            pricing_response = PricingService.get_price_from_soa(
                checkin=checkin,
                checkout=checkout,
                hotel_ids=[hotel_id],
                room_config=room_config,
                include_price_breakup=True,
                get_from_cache=True)
            if pricing_response:
                if pricing_response["data"]["hotels"]:
                    for room in pricing_response["data"]["hotels"][0]["rooms"]:
                        if not base_price or base_price > room['price']['base_price']:
                            base_price = room['price']['base_price']
                        if not sale_price or sale_price > room['price']['sell_price']:
                            sale_price = room['price']['sell_price']
            else:
                logger.error(
                    "No response from pricing service for hotel id %s",
                    hotel_id)
                raise Exception

        except Exception:
            logger.exception(
                "exception in calling Pricing service for criteo for hotel id %s",
                hotel_id)
        logger.info(
            "Returned sale price %s base price %s for hotel %s",
            sale_price,
            base_price,
            hotel_id)
        return base_price, sale_price
