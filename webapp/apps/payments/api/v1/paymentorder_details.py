# -*- coding:utf-8 -*-
"""
    paymentorder_details_dtoPaymentInfo  API
"""

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from apps.bookings.models import Booking
from apps.bookings.services.booking import BookingService
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.payments.dto.paymentorder_details_dto import PaymentOrderDetailsDTO
from apps.payments.serializer import PaymentOrderSerializer
from apps.payments.service.payment_service import PaymentService
from base.logging_decorator import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger
from common.exceptions.treebo_exception import TreeboException
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = get_booking_logger(__name__)


class PaymentOrderDetails(TreeboAPIView):
    __booking_service = BookingService()
    payment_service = PaymentService()
    authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PaymentOrderDetails, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        try:
            paymentorder_details_dto = PaymentOrderDetailsDTO(
                data=request.data)
            if not paymentorder_details_dto.is_valid():
                logger.error(
                    " invalid request for paymentorder_details_dto payment %s ",
                    request.data)
                return api_response.invalid_request_error_response(
                    paymentorder_details_dto.errors, PaymentOrderDetails.__name__)
            logger.info("paymentorder_details_dto payment %s ", request.data)
            order_id = paymentorder_details_dto.data.get('order_id', None)
            hotel_code = paymentorder_details_dto.data.get('hotel_code', None)
            group_code = paymentorder_details_dto.data.get('group_code', None)
            paymentorders, payment_detail, payment_detail_breakup, booking = self.payment_service.get_paymentorder_details(
                hotel_code, group_code, order_id)

            if paymentorders:
                logger.info(
                    "got payment order for payment %s is %s  ",
                    request.data,
                    paymentorders)
                payment_data_temp = PaymentOrderSerializer(
                    instance=paymentorders.first(), many=False).data
                payment_data = {
                    "order_id": payment_data_temp['ps_order_id'],
                    "payment_id": payment_data_temp['order_id'],
                    "status": payment_data_temp['status'],
                    "amount": payment_data_temp['amount'],
                    "gateway_type": payment_data_temp['gateway_type'],
                    "pg_order_id": payment_data_temp['pg_order_id'],
                    "currency": 'INR'
                }
                data = {
                    "per_room_paid_amounts": payment_detail,
                    "payment_details": payment_data,
                    "rate_plan": booking.rate_plan if booking.rate_plan else 'EP',
                    "rate_plan_meta": booking.rate_plan_meta if booking.rate_plan_meta else {},
                }
                return api_response.prep_success_response(data)
            else:
                logger.error(
                    "paymentorder not found paymentorder_details_dto %s  ",
                    paymentorder_details_dto.data)
                return api_response.error_response_from_error_code(
                    error_codes.PAYMENT_ORDER_NOT_FOUND, api_name=PaymentOrderDetails.__name__)
        except TreeboException as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.POST,
                                                        message=e.__str__(), dev_msg="TreeboException",
                                                        class_name=self.__class__.__name__)
            logger.exception(
                "treebo paymentorder_details_dto payment v1 %s ",
                request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.POST,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception(
                "internal paymentorder_details_dto payment v1 internal error %s ",
                request.data)
            response = api_response.internal_error_response(
                error_codes.get(
                    error_codes.UNABLE_TO_FETCH_PAYMENT)['msg'],
                PaymentOrderDetails.__name__)
        return response
