from rest_framework import serializers
from webapp.apps.common.validators import CustomValidator as custom_validator


class PmsUserCreationDTO(serializers.Serializer):
    first_name = serializers.CharField(max_length=200)
    last_name = serializers.CharField(max_length=200)
    email = serializers.EmailField(max_length=200)
    phone_number = serializers.CharField(max_length=200)
    hotel_id = serializers.CharField(max_length=7, allow_null=True)
    role = serializers.CharField()
    aom_email = serializers.EmailField(max_length=200, allow_null=True)

    def validate_phone_number(self, phone_number):
        return custom_validator.normalise_phone_number(phone_number)
