from rest_framework import serializers


class POISerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    distance = serializers.DecimalField(max_digits=12, decimal_places=1)


class POICategorySerializer(serializers.Serializer):
    title = serializers.CharField(max_length=200)
    items = POISerializer(allow_null=True, required=False, many=True)


class NearbyPOISerializer(serializers.Serializer):
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    content = POICategorySerializer(allow_null=True, required=False, many=True)
