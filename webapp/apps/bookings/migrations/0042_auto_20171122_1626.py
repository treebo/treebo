# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0041_auto_20170928_0832'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='wallet_applied',
            field=models.NullBooleanField(
                default=False),
        ),
        migrations.AddField(
            model_name='booking',
            name='wallet_deduction',
            field=models.DecimalField(
                default=0,
                null=True,
                max_digits=9,
                decimal_places=4),
        ),
        migrations.AlterField(
            model_name='booking',
            name='booking_status',
            field=models.CharField(
                default='Tentative',
                max_length=50,
                null=True,
                blank=True,
                choices=[
                    ('Tentative',
                     'Tentative'),
                    ('Awaiting HX Confirmation',
                     'Awaiting HX Confirmation'),
                    ('Confirm',
                     'Confirm'),
                    ('Cancel',
                     'Cancel'),
                    ('Cancellation Pending',
                     'Cancellation Pending'),
                    ('Failure',
                     'Failure'),
                    ('Temp Booking Created',
                     'Temp Booking Created'),
                    ('Part Success',
                     'Part Success')]),
        ),
        migrations.AlterField(
            model_name='booking',
            name='paymentorder',
            field=models.ForeignKey(
                default=None,
                blank=True,
                to='bookings.PaymentOrder',
                null=True,
                on_delete=models.DO_NOTHING),
        ),
    ]
