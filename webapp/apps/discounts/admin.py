# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf.urls import url
# Register your models here.
# from apps.discounts.api.v1.update_constraints import CouponConstraintFormView
from apps.discounts.models import DiscountCouponConstraints, ConstraintExpression, DiscountCoupon, \
    ConstraintKeywords, ConstraintOperators, FlashDiscount, FlatPricingRange, FlatPricingConfig
from base.views.template import TreeboTemplateView
from django.contrib import admin

from dbcommon.admin import ReadOnlyAdminMixin

import csv
import logging
from django import forms
from django.core.urlresolvers import reverse
from django.views.generic.edit import FormView
from rest_framework.response import Response

from apps.discounts.coupon_service import CouponService
from base.views.api import TreeboAPIView
from common.services.feature_toggle_api import FeatureToggleAPI


class UpdateConstraintForm(forms.Form):
    constraints = forms.FileField()


class CouponConstraintFormView(FormView):
    template_name = 'admin/discounts/update_constraints.html'
    form_class = UpdateConstraintForm

    def get_success_url(self):
        return reverse('admin:discounts_discountcoupon_changelist')

    def form_valid(self, form):
        csv_file = csv.reader(form.cleaned_data.get('constraints'))
        constraints = []
        for row in csv_file:
            constraints.append(row)
        CouponService.update_constraints(constraints)
        return super(CouponConstraintFormView, self).form_valid(form)


class CreateCouponsView(FormView):
    template_name = 'admin/discounts/create_coupons.html'
    form_class = UpdateConstraintForm

    def get_success_url(self):
        return reverse('admin:discounts_discountcoupon_changelist')


@admin.register(ConstraintKeywords)
class ConstraintKeywordsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'keyword', 'type')
    readonly_fields = ('keyword', 'type', 'description')

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields
        return ()


@admin.register(ConstraintOperators)
class ConstraintOperatorsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'operator')
    readonly_fields = ('operator', 'description')

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields
        return ()


class DiscountCouponConstraintInline(admin.TabularInline):
    list_display = (
        'id',
        'coupon',
        'keyword',
        'operation',
        'value_one',
        'value_two')
    readonly_fields = ('id',)
    model = DiscountCouponConstraints
    extra = 0


class ConstraintExpressionInline(admin.TabularInline):
    list_display = ('id', 'coupon', 'expression')
    model = ConstraintExpression
    extra = 0


@admin.register(DiscountCouponConstraints)
class DiscountCouponConstraintAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'coupon',
        'keyword',
        'operation',
        'value_one',
        'value_two')
    list_filter = ('coupon', 'keyword')


@admin.register(DiscountCoupon)
class DiscountCouponAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'code',
        'discount_operation',
        'discount_value',
        'discount_application',
        'coupon_type',
        'start_date',
        'expiry',
        'is_active',
        'is_internal',
        'is_visible',
        'is_referral',
        'is_prepaid')
    list_filter = ('discount_operation',)
    inlines = [DiscountCouponConstraintInline, ConstraintExpressionInline]
    search_fields = ('code',)

    def get_formsets_with_inlines(self, request, obj=None):
        for inline in self.get_inline_instances(request, obj):
            if isinstance(
                    inline,
                    DiscountCouponConstraintInline) and obj is None:
                continue
            elif isinstance(inline, ConstraintExpressionInline) and obj is None:
                continue
            yield inline.get_formset(request, obj), inline

    def get_urls(self):
        urls = super(DiscountCouponAdmin, self).get_urls()
        my_urls = [url(r'^update_constraints/$',
                       CouponConstraintFormView.as_view(),
                       name='update-constraints'),
                   url(r'^create_coupons/$',
                       CreateCouponsView.as_view(),
                       name='create-coupons'),
                   ]
        return my_urls + urls

    def formfield_for_choice_field(self, db_field, request, **kwargs):
        SALE_PRICE, RACK_RATE = DiscountCoupon.SALE_PRICE, DiscountCoupon.RACK_RATE
        if db_field.name == 'price_component_applicable':
            kwargs['choices'] = ((SALE_PRICE, SALE_PRICE),)
            if FeatureToggleAPI.is_enabled("coupon", "rack_rate", False):
                kwargs['choices'] += ((RACK_RATE, RACK_RATE),)
        return super(
            DiscountCouponAdmin,
            self).formfield_for_choice_field(
            db_field,
            request,
            **kwargs)


@admin.register(ConstraintExpression)
class ConstraintExpressionAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'coupon', 'expression')


@admin.register(FlatPricingConfig)
class FlatPricingConfigAdmin(admin.ModelAdmin):
    list_display = ('start_date', 'end_date', 'accepted_booking_channel', 'accepted_utm_source', 'enabled_property_providers')


@admin.register(FlatPricingRange)
class FlatPricingRangeAdmin(admin.ModelAdmin):
    list_display = ('range_start', 'range_end', 'pre_tax', 'tax', 'tax_slab')


@admin.register(FlashDiscount)
class FlashDiscountAdmin(admin.ModelAdmin):
    list_display = ('start_date', 'end_date', 'discount_percentage', 'reward_percentage', 'reward_cap',
                    'enabled_property_providers', 'priority_level')
