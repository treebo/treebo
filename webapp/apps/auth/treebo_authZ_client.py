import logging
import requests

from django.conf import settings

logger = logging.getLogger(__name__)


class TreeboAuthZClient:
    @staticmethod
    def assign_role(auth_id, resource_id, role):
        authz_payload = {
            "role": role,
            "resource_id": resource_id,
            "resource_type": 'hotel',
        }
        headers = {"X-Application-Id": "treebo-pms"}
        url = settings.TREEBO_AUTHZ_SERVER + "api/v1/users/{0}/roles/".format(auth_id)

        response = requests.post(url, json=authz_payload, headers=headers)
        if response.status_code != 201:
            logger.error("Authz update failed")
            return None
        return True
