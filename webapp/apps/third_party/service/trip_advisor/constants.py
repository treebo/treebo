LANGUAGE = "en_US"
DATE_FORMAT = "%Y-%m-%d"
CURRENCY = "INR"
BASE_URL = "www.treebo.com"
TA_HOTEL_URL = "https://%s%s?checkin=%s&checkout=%s&roomconfig=%s&roomtype=%s&utm_source=%s&utm_medium=%s&utm_campaign=%s"

TA_UTM_SOURCE = "TA"
TA_UTM_MEDIUM = "Meta"
TA_UTM_CAMPAIGN = "cpc"
ERROR_CODE = {
    1: "Unknown Error",
    2: "Request Cannot Be Parsed",
    3: "Invalid Hotel ID",
    4: "Request for Timeout",
    5: "Recoverable Error"}
TA_ROOM_CODE = {
    "Acacia": "SINGLE",
    "Oak": "OAK",
    "Maple": "MAPLE",
    "Mahogany": "MAHOGANY"}
DEFAULT_AMENITIES = set(
    ["BATHROOMS", "FREE_BREAKFAST", "FREE_INTERNET", "FREE_WIFI"])
MAPPED_AMENITIES = {
    "bar": "BAR_LOUNGE",
    "business event hosting": "BUSINESS_SERVICES",
    "guest laundry": "DRY_CLEANING",
    "gym": "FITNESS_CENTER",
    "kitchenett": "KITCHENETTE",
    "safety locker": "LOCKERS_STORAGE",
    "parking": "PARKING_AVAILABLE",
    "restaurant on premises": "RESTAURANT",
    "room service": "ROOM_SERVICE",
    "swimming pool": "SWIMMING_POOL"}

COUNTRY = "INDIA"
HOTEL_HD_PAGE_URL = "https://%s%s?utm_source=%s&utm_medium=%s&utm_campaign=%s"
CRITEO_HD_PAGE_URL = "https://%s%s?checkin=%s&checkout=%s&roomconfig=%s"
CRITEO_ROOM_CONFIG = "1-0"
CHECKIN_DATE_CRITEO_DIFF = 0
CHECKOUT_DATE_CRITEO_DIFF = 1

TA_CSV_EMAIL_SUBJECT = "Trip Advisor Inventory Sheet from Treebo Hotels"
TA_CSV_EMAIL_BODY = "PFA find the Inventory sheet of hotels from Treebo"
TA_CSV_FILE_NAME = "hotel_inventory_treebo.csv"

CRITEO_CSV_EMAIL_SUBJECT = "Criteo CSV File from Treebo Hotels"
CRITEO_CSV_EMAIL_BODY = "PFA find the CSV of hotels from Treebo"
CRITEO_CSV_FILE_NAME = "criteo_feed.csv"
IMAGE_URL = "https://images.treebohotels.com/files/"
