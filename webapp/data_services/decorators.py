from dbcommon.models.hotel import Hotel


class SingletonDecorator:

    def __init__(self, klass):
        self.klass = klass
        self.instance = None

    def __call__(self, *args, **kwargs):
        if self.instance is None:
            self.instance = self.klass(*args, **kwargs)
        return self.instance


def convert_hotel_web_id_to_cs_id(func):
    def func_wrapper(property_id):
        cs_id = None
        hotels = Hotel.objects.filter(id=property_id).values('cs_id')
        if hotels:
            cs_id = hotels[0]['cs_id']
        if cs_id:
            return func(property_id=cs_id)
        return False, None, None
    return func_wrapper
