# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0030_auto_20160927_0548'),
    ]

    state_operations = [
        migrations.DeleteModel('bookingrequest')
    ]
    operations = [
        migrations.SeparateDatabaseAndState(state_operations=state_operations)
    ]
