import datetime
import logging

from django.conf import settings
from django.core.cache import cache
from builtins import str

from apps.pricing.api.v5.hotel_prices import HotelRoomPricesSerializer
from apps.content.meta_cache_service import MetaCacheService
from apps.pricing.services.price_request_dto import PriceRequestDto
from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.pricing.transformers.search_api_transformer import SearchAPITransformer
from common.constants import common_constants as const
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room

logger = logging.getLogger(__name__)

MAX_VALUE = 1000000
MIN_VALUE = 0


class UpdateMinPriceService:

    def create_keys(self, origin_string):
        """
        creates keys for min price, sell_price, rack_rate and discount for origin(hotel, city, locality)
        :param origin_string: hotel_id or city_name or locality_name
        :return:
        """
        key_dict = dict()
        key_dict.update({'min_price_key': origin_string + '_min_price'})
        key_dict.update({'min_sell_price_key': origin_string + '_min_sell_price'})
        key_dict.update({'min_rack_rate_key': origin_string + '_min_rack_rate'})
        key_dict.update({'max_discount_key': origin_string + '_max_discount'})
        return key_dict

    def initialize_price_dict_for_city(self):
        """
        creates a dictionary of prices for city
        :return:
        """
        price_dict = dict()
        price_dict.update({'city_wise_price': dict()})
        price_dict.update({'city_wise_sell_price': dict()})
        price_dict.update({'city_wise_rack_rate': dict()})
        price_dict.update({'city_wise_discount': dict()})
        return price_dict

    def initialize_price_dict_for_locality(self):
        """
        creates a dictionary of prices for locality
        :return:
        """
        price_dict = dict()
        price_dict.update({'locality_wise_price': dict()})
        price_dict.update({'locality_wise_sell_price': dict()})
        price_dict.update({'locality_wise_rack_rate': dict()})
        price_dict.update({'locality_wise_discount': dict()})
        return price_dict

    def update_min_price(self, key, price_dict_key, price_value, price_dict, price_keys):
        """
        updates min price,sell price and rack rate for city/locality
        :param key: key to use in price_keys
        :param price_dict_key: key to use in price_dict
        :param price_value: price to be compared and set
        :param price_dict: dictionary that holds all the prices and discounts
        :param price_keys: dict of all the keys
        :return:
        """
        if price_value != MAX_VALUE:
            key = price_keys.get(key)
            min_price_dict = price_dict.get(price_dict_key)
            if key in min_price_dict:
                if price_value < min_price_dict[key]:
                    min_price_dict[key] = price_value
            else:
                min_price_dict[key] = price_value

    def update_max_discount(self, key, price_dict_key, discount_value, price_dict, price_keys):
        """
        updates discount for city/locality
        :param key: key to use in price_keys
        :param price_dict_key: key to use in price_dict
        :param discount_value: value to be compared and set
        :param price_dict: dictionary that holds all the prices and discounts
        :param price_keys: dict of all keys
        :return:
        """
        if discount_value != MIN_VALUE:
            key = price_keys.get(key)
            max_discount_dict = price_dict.get(price_dict_key)
            if key in max_discount_dict:
                if discount_value > max_discount_dict[key]:
                    max_discount_dict[key] = discount_value
            else:
                max_discount_dict[key] = discount_value

    def __set_prices_in_cache(self, price_dict, key):
        """
        sets all the price and discount values for city/locality in cache
        :param price_dict: dictionary of all the prices and discounts
        :param key: key to use in price_dict
        :return:
        """
        final_dict = price_dict.get(key)
        for key, value in final_dict.items():
            logger.info('Cache set.Setting value %s for key %s', value, key)
            cache.set(key, value)
            cache.persist(key)

    def __set_cache_by_price_property(self, key, price_value, price_keys_for_hotel):
        """
        Sets the cache entry for the pricing property e.g. rack_rate, sell_price, max_discount etc
        :param key: the key to use for finding the cached property
        :param price_value: the value to be set
        :param price_keys_for_hotel: the dict that holds the value
        """
        key = price_keys_for_hotel.get(key)
        logger.info('Cache set.Setting value %s for key %s', price_value, key)
        cache.set(key, price_value)
        cache.persist(key)

    def __clear_meta_cache(self):
        """
        clears the meta content cache
        :return:
        """
        cache_service = MetaCacheService("", "")
        cache_service.delete_cache()

    def __find_max_price_across_room_types(self, hotel_id, hotel_prices):
        room_wise_price = hotel_prices.get(hotel_id, dict())
        max_sell_price = MIN_VALUE
        if room_wise_price:
            for room_type, rate_plan_price in room_wise_price.items():
                for rate_plan, rate_plan_prices in rate_plan_price.items():
                    if rate_plan_prices.get('price').base_price > 0 and (not max_sell_price
                                                                         or rate_plan_prices.get('price').sell_price
                                                                         > max_sell_price):
                        max_sell_price = rate_plan_prices.get('price').sell_price
        return max_sell_price

    def __calculate_max_price(self, hotel_id):
        """
        caculates max price for max room config for a three day window
        :param hotel_id:
        :param pricing_service:
        :param first_day:
        :return max_price:
        """
        pricing_service = PricingServiceV2()
        max_price = MIN_VALUE
        room = Room.objects.filter(hotel_id=hotel_id).order_by('-max_adult_with_children')[0]
        logger.info('Fetched max config room for hotel id %s', hotel_id)
        max_adults = room.max_adult_with_children - room.max_children
        max_config_str = str(max_adults) + '-' + str(room.max_children)
        room_config_list = [str(max_config_str)]
        hotel_id_list = [(str(hotel_id))]
        days_count = 0
        first_day = datetime.datetime.today()
        try:
            while days_count < 3:
                last_day = first_day + datetime.timedelta(days=1)
                price_request_dto = PriceRequestDto(first_day, last_day, hotel_id_list, room_config_list)
                hotel_prices = pricing_service.get_search_page_price(price_request_dto)
                logger.info('Fetched pricing for room config %s for hotel id %s', max_config_str, hotel_id)
                logger.info('Fetched available rooms for room config %s for hotel id %s', max_config_str, hotel_id)
                max_sell_price = int(
                    round(self.__find_max_price_across_room_types(hotel_id, hotel_prices)))
                if max_sell_price > max_price:
                    max_price = max_sell_price
                days_count += 1
        except Exception as e:
            logger.info("Could not find pricing for hotel id %d", hotel_id)
            logger.exception(e)
        return max_price

    def generate_validated_data(self, checkin_date, checkout_date, room_config, utm_source, channel):
        kwargs = {'checkin': checkin_date,
                  'checkout': checkout_date,
                  'roomconfig': room_config,
                  'utm_source': utm_source,
                  'channel': channel}
        serializer_obj = HotelRoomPricesSerializer(data=kwargs)
        serializer_obj.is_valid(raise_exception=True)
        return serializer_obj.validated_data

    def update_min_price_in_cache(self):
        hotels = Hotel.objects.filter(status=const.ENABLED)
        srp_pricing_service = SearchAPITransformer()
        price_dict_for_city = self.initialize_price_dict_for_city()
        price_dict_for_locality = self.initialize_price_dict_for_locality()
        min_across_all_hotels = MAX_VALUE
        max_across_all_hotels = MIN_VALUE

        for hotel in hotels:
            first_day = datetime.datetime.now().date()
            min_price = MAX_VALUE
            max_discount = MIN_VALUE
            min_rack_rate = MAX_VALUE
            min_sell_price = MAX_VALUE
            days_count = 0
            try:
                # calculating minimum price values for a three day window
                while days_count < 3:
                    last_day = first_day + datetime.timedelta(days=1)
                    config_str = '1' + '-' + '0'
                    hotel_id_list = [str(hotel.id)]
                    validated_data = {'checkin': first_day,
                                      'checkout': last_day,
                                      'roomconfig': config_str,
                                      'utm_source': 'direct',
                                      'channel': 'website'}
                    cheapest_hotel_price = srp_pricing_service.fetch_cheapest_room_prices_available(
                        validated_data, hotel_id_list)
                    pretax_price = int(round((cheapest_hotel_price[hotel.id]['pretax_price'])))
                    sell_price = int(round(cheapest_hotel_price[hotel.id]['sell_price']))
                    rack_rate = int(round(cheapest_hotel_price[hotel.id]['rack_rate']))
                    total_discount = int(round(cheapest_hotel_price[hotel.id]['total_discount_percent']))
                    if pretax_price != 0 and pretax_price < min_price:
                        min_price = pretax_price
                    if sell_price != 0 and sell_price < min_sell_price:
                        min_sell_price = sell_price
                    if rack_rate != 0 and rack_rate < min_rack_rate:
                        min_rack_rate = rack_rate
                    if total_discount > max_discount:
                        max_discount = total_discount
                    first_day = last_day
                    days_count += 1
                max_price = self.__calculate_max_price(hotel.id)
            except Exception as e:
                logger.info("Could not find pricing for hotel %s" % hotel.id)
                logger.exception(e)
                continue

            # updating hotel wise min price, rack rate, sell price and discount
            price_keys_for_hotel = self.create_keys(str(hotel.id))
            if min_price != MAX_VALUE:
                self.__set_cache_by_price_property('min_price_key', min_price, price_keys_for_hotel)
            if min_sell_price != MAX_VALUE:
                self.__set_cache_by_price_property('min_sell_price_key', min_sell_price, price_keys_for_hotel)
            if min_rack_rate != MAX_VALUE:
                self.__set_cache_by_price_property('min_rack_rate_key', min_rack_rate, price_keys_for_hotel)
            if max_discount != MIN_VALUE:
                self.__set_cache_by_price_property('max_discount_key', max_discount, price_keys_for_hotel)
            if max_price != MIN_VALUE:
                key = 'hotel_id_' + str(hotel.id) + '_max_price'
                logger.info('Cache set.Setting max price %s for key %s', max_price, key)
                cache.set(key, max_price)
                cache.persist(key)

            # updating city wise min price, rack rate, sell price and discount
            price_keys_for_city = self.create_keys(str(hotel.city.name))
            self.update_min_price('min_price_key', 'city_wise_price', min_price, price_dict_for_city,
                                  price_keys_for_city)
            self.update_min_price('min_sell_price_key', 'city_wise_sell_price', min_sell_price,
                                  price_dict_for_city, price_keys_for_city)
            self.update_min_price('min_rack_rate_key', 'city_wise_rack_rate', min_rack_rate,
                                  price_dict_for_city, price_keys_for_city)
            self.update_max_discount('max_discount_key', 'city_wise_discount', max_discount,
                                     price_dict_for_city, price_keys_for_city)

            # updating locality wise min price, rack rate, sell price and discount
            price_keys_for_locality = self.create_keys(str(hotel.locality.name))
            self.update_min_price('min_price_key', 'locality_wise_price', min_price,
                                  price_dict_for_locality, price_keys_for_locality)
            self.update_min_price('min_sell_price_key', 'locality_wise_sell_price',
                                  min_sell_price, price_dict_for_locality, price_keys_for_locality)
            self.update_min_price('min_rack_rate_key', 'locality_wise_rack_rate',
                                  min_rack_rate, price_dict_for_locality, price_keys_for_locality)
            self.update_max_discount('max_discount_key', 'locality_wise_discount',
                                     max_discount, price_dict_for_locality, price_keys_for_locality)

            # updating overall min_price and max_price across all hotels
            if min_price < min_across_all_hotels:
                min_across_all_hotels = min_price
            if max_price > max_across_all_hotels:
                max_across_all_hotels = max_price

        # setting city wise min price , rack_rate, sell_price, discount in cache
        self.__set_prices_in_cache(price_dict_for_city, 'city_wise_price')
        self.__set_prices_in_cache(price_dict_for_city, 'city_wise_sell_price')
        self.__set_prices_in_cache(price_dict_for_city, 'city_wise_rack_rate')
        self.__set_prices_in_cache(price_dict_for_city, 'city_wise_discount')

        # setting locality wise min price , rack_rate, sell_price, discount in cache
        self.__set_prices_in_cache(price_dict_for_locality, 'locality_wise_price')
        self.__set_prices_in_cache(price_dict_for_locality, 'locality_wise_sell_price')
        self.__set_prices_in_cache(price_dict_for_locality, 'locality_wise_rack_rate')
        self.__set_prices_in_cache(price_dict_for_locality, 'locality_wise_discount')

        logger.info('Cache set.Setting min price %s for key %s', min_across_all_hotels, settings.MINPRICE_KEY)
        cache.set(settings.MINPRICE_KEY, min_across_all_hotels)
        cache.persist(settings.MINPRICE_KEY)
        logger.info('Cache set.Setting max price %s for key %s', max_across_all_hotels, settings.MAXPRICE_KEY)
        cache.set(settings.MAXPRICE_KEY, max_across_all_hotels)
        cache.persist(settings.MAXPRICE_KEY)
