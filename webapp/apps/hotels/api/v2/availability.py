import logging

from rest_framework import serializers
from rest_framework.response import Response
from web_sku.availability.services.availability_service import AvailabilityService

from apps.search.services.sku_search_cache_service import SkuSearchCacheService
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboValidationException
from common.services.feature_toggle_api import FeatureToggleAPI
from dbcommon.services.web_cs_transformer import WebCsTransformer
from django.conf import settings

logger = logging.getLogger(__name__)
from apps.common import error_codes as codes
from apps.common.error_codes import get as get_error_msg
from collections import defaultdict
from webapp.apps.bookingstash.service.availability_service import get_availability_service


class HotelAvailabilitySerializer(serializers.Serializer):
    checkin = serializers.DateField(required=True)
    checkout = serializers.DateField(required=True)
    roomconfig = serializers.CharField(max_length=100, default='1-0', required=False)

    def validate(self, data):
        if data['checkout'] <= data['checkin']:
            raise serializers.ValidationError("Checkout date must be after checkin date.")
        return data


class HotelAvailability(TreeboAPIView):
    validationSerializer = HotelAvailabilitySerializer

    def __init__(self):
        self.web_cs_transformer = WebCsTransformer()
        self.availibility_service = AvailabilityService()
        self.cache_search = SkuSearchCacheService()

    def get(self, request, *args, **kwargs):

        """
        API to fetch availability for all rooms for the given hotel.
        checkin -- Checkin Date (Use format: yyyy-MM-dd)
        checkout -- Checkout Date (Use format: yyyy-MM-dd)
        roomconfig -- Room Configuration in format: 1-0,2-0,2-1 (For a-c, 'a' is adult count, and 'c' is children count)
        hotels -- hotel ids in format: 1,3,5,6
        """

        try:
            validated_data = self.serializerObject.validated_data
            check_in = validated_data['checkin']
            check_out = validated_data['checkout']
            room_config = validated_data['roomconfig']
            logger.info("get hotel Availability with query params:{}".format(str(request.GET.dict())))
            hotel_ids = request.GET.dict()['hotels'].split(',')
        except ValueError:
            return Response({'error': get_error_msg(
                codes.INVALID_DATES)}, status=400)
        except TreeboValidationException as e:
            return Response({'error': get_error_msg(
                codes[e.message])}, status=400)
        try:
            if FeatureToggleAPI.is_enabled(settings.DOMAIN, "pricing_revamp", False):
                available_rooms = self.get_sku_room_availability(hotel_ids, check_in, check_out,
                                                                 room_config)
            else:
                available_rooms = get_availability_service().get_available_rooms_for_hotels(
                    hotel_ids, check_in, check_out, room_config)

            rooms_availability = defaultdict(lambda: defaultdict(dict))
            for hotel_room, availability_count in list(
                available_rooms.items()):
                rooms_availability[hotel_room[0]
                ][hotel_room[1].title()] = availability_count

            return Response(rooms_availability)
        except BaseException:
            return Response({'error': get_error_msg(
                codes.INVALID_ROOM_CONFIG)}, status=400)

    def get_sku_room_availability(self, hotel_ids, checkin_date, checkout_date, room_config):
        cs_id_map = self.web_cs_transformer.get_hotel_cs_ids_from_web_ids(hotel_ids).get('live')
        cs_ids = []
        room_availability_per_hotel = {}
        available_hotels_dict = {}
        try:
            for hotel_id in cs_id_map:
                cs_ids.append(cs_id_map.get(hotel_id))
            if cs_ids:
                available_hotels_dict = self.cache_search.get_availability_data(cs_ids, checkin_date, checkout_date,
                                                                                room_config)
            if available_hotels_dict:
                for hotel_id in hotel_ids:
                    cs_id = cs_id_map.get(int(hotel_id))
                    sku_room_availibilty = available_hotels_dict.get(cs_id)
                    if sku_room_availibilty:
                        for sku_room_type in sku_room_availibilty:
                            room_count = sku_room_availibilty.get(sku_room_type)
                            room_availability_per_hotel.update({(hotel_id, str(sku_room_type)): room_count})
        except Exception as err:
            print(err)

        return room_availability_per_hotel
