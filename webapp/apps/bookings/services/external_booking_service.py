# -*- coding:utf-8 -*-
"""
    Interface For Booking Client
"""
import abc


class ExternalBookingService(object):
    """
        Interface Base Class for Implementing External Booking Service
    """

    @abc.abstractmethod
    def save_booking(
            self,
            booking_id,
            user=None,
            request_id=None,
            is_secure=True):
        """
        :param booking_id:
        :param user:
        :param request_id:
        :param is_secure:
        :return:
        """
        pass

    @abc.abstractmethod
    def confirm_booking(self, booking_id, user_id=None, request_id=None):
        """
        :param booking_id:
        :param user_id:
        :param request_id:
        :return:
        """
        pass

    @abc.abstractmethod
    def cancel_booking(self, order_id, user_id=None, request_id=None):
        """
        :param order_id:
        :param user_id:
        :param request_id:
        :return:
        """

    @abc.abstractmethod
    def create_booking(
            self,
            booking_id,
            user_id=None,
            request_id=None,
            is_secure=True):
        """
        :param booking_id:
        :param user_id:
        :param request_id:
        :param is_secure:
        :return:
        """
        pass

    @abc.abstractmethod
    def update_gst(self, booking_id):
        """
        Updates the gst detail associated with a booking
        :param booking_id:
        :return:
        """
        pass
