from random import randint


class RandomNumberGenerator:

    def __init__(self):
        pass

    @staticmethod
    def generate_number(lower_limit, higher_limit):
        return randint(lower_limit, higher_limit)
