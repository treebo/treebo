import json
import mock
from mock import patch

from datetime import date, datetime, time
from django.test import SimpleTestCase

from apps.pricing import utils
from apps.pricing.dateutils import date_range, ymd_str_to_date
from apps.pricing.models import RoomPrice, PromoConditions
from apps.pricing.utils import PriceUtils
from dbcommon.models.room import Room
from dbcommon.models.hotel import Hotel


class PriceUtilsGetRoomPriceForDateRangeTest(SimpleTestCase):
    def setUp(self):
        self.mock_state_tax_manager = mock.patch.object(
            utils.StateTax, 'objects').start()
        self.mock___getRoomPriceForDate = mock.patch.object(
            PriceUtils, '_PriceUtils__getRoomPriceForDate').start()
        self.start_date = datetime(2016, 8, 20).date()
        self.end_date = datetime(2016, 8, 22).date()

    def tearDown(self):
        mock.patch.stopall()

    def yield_state_tax(self):
        for _ in range(50):
            yield {'tax_value__sum': 9.4}

    def yield_room_price_no_promo(self):
        for _ in range(50):
            yield {'pretax': 4, 'tax': 1, 'sell': 6, 'rack_rate': 8, 'pretax_rackrate': 7, 'discount': 0}, False

    def yield_room_price_with_promo(self):
        for _ in range(50):
            yield {'pretax': 4, 'tax': 1, 'sell': 6, 'rack_rate': 8, 'pretax_rackrate': 7, 'discount': 0}, True

    def test_getRoomPriceForDateRange_should_return_no_promo(self):
        self.mock___getRoomPriceForDate.side_effect = self.yield_room_price_no_promo()
        self.mock_state_tax_manager.filter.return_value.aggregate.side_effect = self.yield_state_tax()
        _, _, promo_applied = PriceUtils.getRoomPriceForDateRange(
            self.start_date, self.end_date, mock.Mock(), mock.Mock())
        self.assertFalse(promo_applied)
        self.assertEqual(self.mock___getRoomPriceForDate.call_count, 2)

    def test_getRoomPriceForDateRange_should_return_promo(self):
        self.mock___getRoomPriceForDate.side_effect = self.yield_room_price_with_promo()
        self.mock_state_tax_manager.filter.return_value.aggregate.side_effect = self.yield_state_tax()
        _, _, promo_applied = PriceUtils.getRoomPriceForDateRange(
            self.start_date, self.end_date, mock.Mock(), mock.Mock())
        self.assertTrue(promo_applied)
        self.assertEqual(self.mock___getRoomPriceForDate.call_count, 2)

    def test_getRoomPriceForDateRange_should_have_price_breakup_for_all_dates(
            self):
        self.mock___getRoomPriceForDate.side_effect = self.yield_room_price_with_promo()
        self.mock_state_tax_manager.filter.return_value.aggregate.side_effect = self.yield_state_tax()
        _, date_wise_prices, _ = PriceUtils.getRoomPriceForDateRange(
            self.start_date, self.end_date, mock.Mock(), mock.Mock())
        for d in date_range(self.start_date, self.end_date):
            self.assertTrue(d in date_wise_prices)

    def test_getRoomPriceForDateRange_should_have_all_price_components(self):
        self.mock___getRoomPriceForDate.side_effect = self.yield_room_price_with_promo()
        self.mock_state_tax_manager.filter.return_value.aggregate.side_effect = self.yield_state_tax()
        total_prices, _, _ = PriceUtils.getRoomPriceForDateRange(
            self.start_date, self.end_date, mock.Mock(), mock.Mock())
        for component in (
            'pretax',
            'tax',
            'sell',
            'rack_rate',
            'pretax_rackrate',
                'discount'):
            self.assertTrue(component in total_prices)


class PriceUtilsCreateNewRoomPriceTest(SimpleTestCase):
    def setUp(self):
        self.mock_cmrp_manager = mock.patch.object(
            utils.ChannelManagerRatePlan, 'objects').start()
        self.mock_preferred_promo_manager = mock.patch.object(
            utils.PreferredPromo, 'objects').start()
        self.mock_luxury_tax_manager = mock.patch.object(
            utils.LuxuryTax, 'objects').start()
        self.mock_getPriceAfterAutoPromo = mock.patch.object(
            PriceUtils, '_PriceUtils__getPriceAfterAutoPromo').start()
        self.save_room_price_mock = mock.patch.object(
            utils.RoomPrice, 'save').start()

    def tearDown(self):
        mock.patch.stopall()

    def test_should_take_base_price_from_channelManagerRatePlan(self):
        mock_cmrp = mock.Mock()
        # Use PropertyMock to assert attribute access
        base_price = mock.PropertyMock(return_value=100)
        price_increment = mock.PropertyMock(return_value=50)
        type(mock_cmrp).base_price = base_price
        type(mock_cmrp).price_increment = price_increment
        self.mock_cmrp_manager.filter.return_value.order_by.return_value.first.return_value = mock_cmrp

        self.mock_preferred_promo_manager.filter.return_value.prefetch_related.return_value = mock.Mock()
        self.mock_luxury_tax_manager.filter.return_value = [mock.Mock()]

        mock_room = Room(id=1, hotel=Hotel(id=5))
        room_price_property = mock.PropertyMock(return_value=500)
        type(mock_room).price = room_price_property

        d = datetime(2016, 8, 20).date()
        hotel_price_dict = {d: {'sell_price': 10, 'tax': 3, 'rack_rate': 13}}
        self.mock_getPriceAfterAutoPromo.return_value = (
            hotel_price_dict, False)
        occupancy = 2
        PriceUtils._PriceUtils__create_new_room_price(
            d, mock_room, mock.Mock(), occupancy)
        base_price.assert_called_once_with()
        price_increment.assert_called_once_with()
        room_price_property.assert_not_called()

    def test_should_take_base_price_from_room(self):
        self.mock_cmrp_manager.filter.return_value.order_by.return_value.first.return_value = None

        self.mock_preferred_promo_manager.filter.return_value.prefetch_related.return_value = mock.Mock()
        self.mock_luxury_tax_manager.filter.return_value = [mock.Mock()]

        mock_room = Room(id=1, hotel=Hotel(id=5, price_increment=50))
        room_price_property = mock.PropertyMock(return_value=500)
        type(mock_room).price = room_price_property

        d = datetime(2016, 8, 20).date()
        hotel_price_dict = {d: {'sell_price': 10, 'tax': 3, 'rack_rate': 13}}
        self.mock_getPriceAfterAutoPromo.return_value = (
            hotel_price_dict, False)
        occupancy = 2
        PriceUtils._PriceUtils__create_new_room_price(
            d, mock_room, mock.Mock(), occupancy)
        room_price_property.assert_called_once_with()


class PriceUtils__getRoomPriceForDateTest(SimpleTestCase):
    def setUp(self):
        self.mock_roomprice_manager = patch.object(
            utils.RoomPrice, 'objects').start()
        self.mock_create_new_room_price = patch.object(
            PriceUtils, '_PriceUtils__create_new_room_price').start()
        self.mock_get_pretax_price = patch.object(
            PriceUtils, '_PriceUtils__getPretaxPrice').start()

    def tearDown(self):
        mock.patch.stopall()

    def test___getRoomPriceForDate_shouldCreateNewRoomPrice(self):
        self.mock_roomprice_manager.filter.return_value\
                                   .select_related.return_value\
                                   .order_by.return_value\
                                   .first.return_value = None
        mock_room_price = mock.create_autospec(RoomPrice,
                                               **{'id': 1,
                                                  'total_amount': 100,
                                                  'tax_amount': 10,
                                                  'rack_rate': 120,
                                                  'promo_applied': True})
        self.mock_create_new_room_price.return_value = mock_room_price
        self.mock_get_pretax_price.return_value = 110
        prices, promo_applied = PriceUtils._PriceUtils__getRoomPriceForDate(
            datetime.now().date(), mock.Mock(), 1, mock.Mock(), None, None)
        self.mock_create_new_room_price.assert_called()

    def test___getRoomPriceForDate_shouldGetExistingRoomPrice(self):
        mock_room_price = mock.create_autospec(RoomPrice,
                                               **{'id': 1,
                                                  'total_amount': 100,
                                                  'tax_amount': 10,
                                                  'rack_rate': 120,
                                                  'promo_applied': True})
        self.mock_roomprice_manager.filter.return_value\
                                   .select_related.return_value\
                                   .order_by.return_value\
                                   .first.return_value = mock_room_price

        self.mock_get_pretax_price.return_value = 110
        prices, promo_applied = PriceUtils._PriceUtils__getRoomPriceForDate(
            datetime.now().date(), mock.Mock(), 1, mock.Mock(), None, None)
        self.mock_create_new_room_price.assert_not_called()

    @patch.object(PriceUtils, 'getTaxOnPrice')
    def test___getRoomPriceForDate_ShouldRecalculateTaxOnCoupon(
            self, mock_get_tax_on_price):
        mock_get_tax_on_price.return_value = 9
        mock_room_price = mock.create_autospec(RoomPrice,
                                               **{'id': 1,
                                                  'total_amount': 100,
                                                  'tax_amount': 10,
                                                  'rack_rate': 120,
                                                  'promo_applied': True})
        self.mock_roomprice_manager.filter.return_value\
                                   .select_related.return_value\
                                   .order_by.return_value\
                                   .first.return_value = mock_room_price

        self.mock_get_pretax_price.return_value = 110
        coupon_percent = 10
        from apps.discounts.models import DiscountCoupon
        coupon = mock.Mock(
            **{'code': 'TEST', 'price_component_applicable': DiscountCoupon.SALE_PRICE})
        prices, promo_applied = PriceUtils._PriceUtils__getRoomPriceForDate(
            datetime.now().date(), mock.Mock(), 1, mock.Mock(), coupon_percent, coupon)
        mock_get_tax_on_price.assert_called()


class PriceUtils__getApplicationPromoFromAllPromosTest(SimpleTestCase):
    def setUp(self):
        mock_conditions = [mock.Mock(spec=PromoConditions,
                                     **{'condition_name': 'RoomTypeList',
                                         'condition_params': "[\"Oak\"]"})]
        self.mock_promo = mock.Mock(
            **{'type': 'Basic', 'conditions.all.return_value': mock_conditions})

    def test__getApplicationPromo_shouldReturnEmptyPromo(self):
        result = PriceUtils._PriceUtils__getApplicationPromoFromAllPromos(
            datetime.now(), mock.Mock(), mock.Mock(), mock.Mock(), None)
        self.assertEqual(result, [])

    def test__getApplicationPromo_PromoTypeBasic(self):
        applicable_promo_on_date = [self.mock_promo]
        result = PriceUtils._PriceUtils__getApplicationPromoFromAllPromos(datetime.now(), mock.Mock(
        ), mock.Mock(), mock.Mock(**{'room_type_code': 'Oak'}), applicable_promo_on_date)
        self.assertEqual(result, [self.mock_promo])

    def test__getApplicationPromo_PromoTypeMinStay_shouldReturnPromo(self):
        self.mock_promo.configure_mock(**{'type': 'MinStay', 'min_nights': 2})
        applicable_promo_on_date = [self.mock_promo]
        result = PriceUtils._PriceUtils__getApplicationPromoFromAllPromos(datetime.now(), date(
            2016, 8, 22), date(2016, 8, 25), mock.Mock(**{'room_type_code': 'Oak'}), applicable_promo_on_date)
        self.assertEqual(result, [self.mock_promo])

    def test__getApplicationPromo_PromoTypeMinStay_shouldNotReturnPromo(self):
        self.mock_promo.configure_mock(**{'type': 'MinStay', 'min_nights': -1})
        applicable_promo_on_date = [self.mock_promo]
        result = PriceUtils._PriceUtils__getApplicationPromoFromAllPromos(datetime.now(), date(
            2016, 8, 22), date(2016, 8, 25), mock.Mock(**{'room_type_code': 'Oak'}), applicable_promo_on_date)
        self.assertEqual(result, [])

    def test__getApplicationPromo_PromoTypeEarlyBird_shouldReturnPromo(self):
        self.mock_promo.configure_mock(
            **{'type': 'EarlyBird', 'min_advance_days': 2})
        applicable_promo_on_date = [self.mock_promo]
        result = PriceUtils._PriceUtils__getApplicationPromoFromAllPromos(datetime(2016, 8, 18), date(
            2016, 8, 22), date(2016, 8, 25), mock.Mock(**{'room_type_code': 'Oak'}), applicable_promo_on_date)
        self.assertEqual(result, [self.mock_promo])

    def test__getApplicationPromo_PromoTypeEarlyBird_shouldNotReturnPromo(
            self):
        self.mock_promo.configure_mock(
            **{'type': 'EarlyBird', 'min_advance_days': -1})
        applicable_promo_on_date = [self.mock_promo]
        result = PriceUtils._PriceUtils__getApplicationPromoFromAllPromos(datetime(2016, 8, 18), date(
            2016, 8, 22), date(2016, 8, 25), mock.Mock(**{'room_type_code': 'Oak'}), applicable_promo_on_date)
        self.assertEqual(result, [])

    def test__getApplicationPromo_PromoTypeLastMinute_shouldReturnPromo(self):
        self.mock_promo.configure_mock(
            **{'type': 'LastMinute', 'max_advance_days': 2})
        applicable_promo_on_date = [self.mock_promo]
        result = PriceUtils._PriceUtils__getApplicationPromoFromAllPromos(datetime(2016, 8, 23), date(
            2016, 8, 22), date(2016, 8, 25), mock.Mock(**{'room_type_code': 'Oak'}), applicable_promo_on_date)
        self.assertEqual(result, [self.mock_promo])

    def test__getApplicationPromo_PromoTypeLastMinute_shouldNotReturnPromo(
            self):
        self.mock_promo.configure_mock(
            **{'type': 'LastMinute', 'max_advance_days': -1})
        applicable_promo_on_date = [self.mock_promo]
        result = PriceUtils._PriceUtils__getApplicationPromoFromAllPromos(datetime(2016, 8, 25), date(
            2016, 8, 22), date(2016, 8, 25), mock.Mock(**{'room_type_code': 'Oak'}), applicable_promo_on_date)
        self.assertEqual(result, [])


class PriceUtilsSingleFlowMethodTest(SimpleTestCase):
    @mock.patch.object(PriceUtils, '_PriceUtils__create_new_room_price')
    def test_sync_roomprice_from_channelmanagerrateplan_shouldCallCreateNewRoomPriceForAllDates(
            self, mock_create_new_room_price):
        start_date = datetime(2016, 8, 20).date()
        end_date = datetime(2016, 8, 22).date()
        total_days = (end_date - start_date).days
        PriceUtils.sync_roomprice_from_channelmanagerrateplan(
            start_date, end_date, mock.Mock(), mock.Mock(), mock.Mock())
        self.assertEqual(mock_create_new_room_price.call_count, total_days)
