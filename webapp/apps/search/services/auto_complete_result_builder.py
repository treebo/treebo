from django.conf import settings
from apps.hotels.utils import generate_hash
from django.template.defaultfilters import slugify
from apps.search.constants import TESTING_TEXT

SEARCH_RESULT_TYPE = {
    'HOTEL': 'hotel',
    'LOCALITY': 'locality',
    'LANDMARK': 'landmark',
    'STATE': 'state',
    'CITY': 'city',
    'GOOGLE_API': 'google_api',
    'CATEGORY': 'category',
    'HOTEL_TYPE': 'hotel_type',
    'AMENITY': 'amenity'
}


class AutoCompleteResultBuilder(object):
    @staticmethod
    def build_hotel_result(hotels):
        return [{
            'type': SEARCH_RESULT_TYPE['HOTEL'],
            'label': '{0}, {1}, {2}'.format(hotel.name, hotel.locality.name, hotel.city.name),
            'hotel_id': hotel.id,
            'hashed_hotel_id': generate_hash(hotel.id),
            'area': {
                'hotel': hotel.name,
                'locality': hotel.locality.name,
                'city': hotel.city.name,
                'city_slug': hotel.city.slug,
                'state': hotel.state.name,
                'country': 'India'
            },
            'coordinates': {
                'lat': float(hotel.latitude),
                'lng': float(hotel.longitude)
            }
        } for hotel in hotels]

    @staticmethod
    def build_locality_result(localities):
        return [{
            'type': SEARCH_RESULT_TYPE['LOCALITY'],
            'label': '{0}, {1}'.format(locality.name, locality.city.name),
            'area': {
                'locality_slug': locality.get_locality_slug(),
                'locality': locality.name,
                'city': locality.city.name if locality.city else None,
                'city_slug': locality.city.slug if locality.city else None,
                'state': locality.city.state.name if locality.city.state else None,
                'country': 'India'
            },
            'coordinates': {
                'lat': float(locality.latitude),
                'lng': float(locality.longitude)
            }
        } for locality in localities if locality.name.lower() not in TESTING_TEXT]

    @staticmethod
    def build_landmark_result(landmarks):
        return [{
            'type': SEARCH_RESULT_TYPE['LANDMARK'],
            'label': '{0}, {1}'.format(
                landmark.search_label if landmark.search_label else landmark.seo_url_name,
                landmark.city.name),
            'area': {
                'landmark_slug': landmark.get_landmark_slug(),
                'landmark': landmark.seo_url_name,
                'city': landmark.city.name if landmark.city else None,
                'city_slug': slugify(landmark.city.slug) if landmark.city else None,
                'state': landmark.city.state.name if landmark.city.state else None,
                'country': 'India'
            },
            'coordinates': {
                'lat': float(landmark.latitude) if landmark.latitude else '12.9667',
                'lng': float(landmark.longitude) if landmark.longitude else '77.5667'
            }
        } for landmark in landmarks if landmark.seo_url_name.lower() not in TESTING_TEXT]

    @staticmethod
    def build_city_result(cities, hotel_count_dict=dict()):
        return [{
            'type': SEARCH_RESULT_TYPE['CITY'],
            'hotel_count': None if not hotel_count_dict else hotel_count_dict.get(city.id, 0),
            'label': '{0}, {1}'.format(city.name, city.state.name) if city.state else city.name,
            'area': {
                'locality': None,
                'city': city.name,
                'city_slug': city.slug,
                'state': city.state.name if city.state else None,
                'country': 'India',
            }
        } for city in cities if city.status != 0 and city.name.lower() not in TESTING_TEXT]

    @staticmethod
    def build_state_result(states):
        return [{
            'type': SEARCH_RESULT_TYPE['STATE'],
            'label': '{0}'.format(state.name),
            'area': {
                'locality': None,
                'city': None,
                'state': state.name,
                'country': 'India',
            }
        } for state in states if state.name.lower() not in TESTING_TEXT]

    @staticmethod
    def build_google_api_result(predictions):
        return [{
            'type': SEARCH_RESULT_TYPE['GOOGLE_API'],
            'label': prediction.description,
            'place_id': prediction.place_id,
        } for prediction in predictions]

    @staticmethod
    def build_category_result(categories, city):

        return [{
            'type': SEARCH_RESULT_TYPE['CATEGORY'],
            'label': '{0}'.format(category.name),
            'category': category.name,
            'area': {
                'city': city.name,
                'city_slug': city.slug,
            },
        } for category in categories]

    @staticmethod
    def build_popular_amenity_result(amenities, city):
        return [{
            'type': SEARCH_RESULT_TYPE['AMENITY'],
            'label': '{0}'.format(amenity.name),
            'amenity': amenity.name,
            'area': {
                'city': city.name,
                'city_slug': city.slug,
            },
        } for amenity in amenities]

    @staticmethod
    def build_hotel_types_near_landmark_result(all_categories, landmark):
        return [{
            'type': SEARCH_RESULT_TYPE['HOTEL_TYPE'],
            'label': '{0}'.format(category["name"]),
            'category': category["name"],
            'area': {
                'landmark_slug': landmark.get_landmark_slug(),
                'landmark': landmark.seo_url_name,
                'city': landmark.city.name,
                'city_slug': landmark.city.slug,
            },
        } for category in all_categories]

    @staticmethod
    def build_hotel_types_in_locality_result(all_categories, locality):
        return [{
            'type': SEARCH_RESULT_TYPE['HOTEL_TYPE'],
            'label': '{0}'.format(category["name"]),
            'category': category["name"],
            'area': {
                'locality': locality.name,
                'locality_slug': locality.get_locality_slug(),
                'city': locality.city.name,
                'city_slug': locality.city.slug,
            },
        } for category in all_categories]
