import math
import logging
from web_sku.availability.services.availability_service import AvailabilityService

from apps.content.models import ContentStore
from apps.hotels.service.hotel_service import HotelService
from apps.hotels.service.hotel_sub_brand_service import HotelSubBrandService
from apps.search.constants import INDEPENDENT_HOTEL_BRAND_CODE, \
    INDEPENDENT_HOTELS_SEARCH_DISTANCE_CAP, \
    INDEPENDENT_HOTELS_DEFAULT_DISTANCE_CAP, GDC, DIRECT
from apps.search.services.sku_search_cache_service import SkuSearchCacheService
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.promotions.services.promotion_service import PromotionsService
from common.utilities.date_time_utils import get_day_difference
from dbcommon.models.hotel import HotelMetaData
from dbcommon.services.web_cs_transformer import WebCsTransformer
from webapp.apps.bookingstash.service.availability_service import get_availability_service
from webapp.apps.bookingstash.service.availability_service import get_max_adults_with_children
from django.conf import settings

logger = logging.getLogger(__name__)


class PricingSortingService():
    counter = 0
    hotel_size = 0

    cnt_sold_out = 0
    cnt_override = 0
    cnt_pricing = 0
    cnt_occupancy = 0

    hotels_sold_out = []
    hotels_override = []
    hotels_pricing = []
    hotels_occupancy = []
    excluded_ids_list = []
    hotels_unbranded = []
    promoted_hotel = None
    meta_hotel_id = None
    hotel_web_cs_id_dict = {}
    availability_service = get_availability_service()
    promotions_service = PromotionsService()

    def __init__(self):
        self.web_cs_transformer = WebCsTransformer()
        self.availibility_service = AvailabilityService()
        self.cache_search = SkuSearchCacheService()

    def set_sort_defaults(self):
        self.counter = 0
        self.hotel_size = 0
        self.cnt_sold_out = 0
        self.cnt_override = 0
        self.cnt_pricing = 0
        self.cnt_occupancy = 0
        self.promoted_hotel = None
        self.meta_hotel_id = None
        self.hotels_sold_out = []
        self.hotels_override = []
        self.hotels_pricing = []
        self.hotels_occupancy = []
        self.excluded_ids_list = []
        self.hotels_unbranded = []
        self.hotel_web_cs_id_dict = {}

    def sort_recommended_hotels(
        self,
        hotel_map,
        hotel_ids,
        checkin_date,
        checkout_date,
        room_config,
        search_channel,
        room_availability_per_hotel=dict(),
        hotel_web_cs_id_dict={},
        meta_hotel_id=None
        ):

        self.set_sort_defaults()
        self.hotel_web_cs_id_dict = hotel_web_cs_id_dict
        self.meta_hotel_id = meta_hotel_id
        self.hotel_size = len(hotel_ids)
        # the sold out ones
        self.set_sorted_hotels_sold_out(hotel_map)
        # removing the sold out hotels from the consideration set
        available_hotel_ids = [
            hotel_id for hotel_id in hotel_ids if hotel_id not in self.excluded_ids_list]
        # the overriden ones
        self.set_sorted_hotels_overriden(hotel_ids, available_hotel_ids, search_channel)

        if FeatureToggleAPI.is_enabled("search", "unbranded_hotels", False):
            # sorting unbranded hotels
            self.recommended_sort_unbranded_hotels(available_hotel_ids)

        # the pricing ones
        self.set_sorted_hotels_pricing(hotel_map)
        # the occupancy ones
        self.set_sorted_hotels_occupancy(
            hotel_map,
            available_hotel_ids,
            checkin_date,
            checkout_date,
            room_config,
            room_availability_per_hotel)
        # the assembly...
        self.assemble_sort_results(hotel_map)
        return hotel_map

    def assemble_sort_results(self, hotel_map):

        # Search ordering changes
        if self.promoted_hotel and hotel_map and self.promoted_hotel in hotel_map.keys():
            hotel_map[self.promoted_hotel]['promoted'] = True

        # the sort bucket sequence
        sort_buckets = [
            self.hotels_override,
            self.hotels_occupancy,
            self.hotels_pricing,
            self.hotels_unbranded,
            self.hotels_sold_out]
        sort_index = 0
        for sort_bucket in sort_buckets:
            for hotel_id in sort_bucket:
                hotel_map[hotel_id]['sort_index'] = sort_index
                sort_index += 1

    def set_sorted_hotels_overriden(self, hotel_ids, available_hotel_ids, search_channel):
        self.set_hotel_override_cap()
        self.hotels_override = self.get_override_hotel_ids(hotel_ids, available_hotel_ids, search_channel)[
                               :self.override_cap]
        self.excluded_ids_list += self.hotels_override

    def set_sorted_hotels_sold_out(self, hotel_map):
        filtered_sold_out_map = list(
            filter(
                (lambda x_y: x_y[1]['net_payable_amount'] <= 0),
                iter(
                    list(
                        hotel_map.items()))))
        sold_out_counter = self.hotel_size + 1
        sold_out_list = []
        for id, hotel_data in filtered_sold_out_map:
            self.hotels_sold_out.append(id)
            # sold_out_counter -= 1
            # hotel_data['sort_index'] = sold_out_counter

        self.hotels_sold_out.sort()
        self.cnt_sold_out = len(self.hotels_sold_out)
        self.excluded_ids_list += self.hotels_sold_out

    def set_sorted_hotels_pricing(self, hotel_map):
        # sorting basis pricing
        sorted_hotel_map = sorted(
            iter(
                list(
                    hotel_map.items())),
            key=lambda x_y1: (x_y1[1]['net_payable_amount'], x_y1[0]))

        # filtered set:
        consideration_subset_map = list(
            filter(
                (lambda x_y2: x_y2[0] not in self.excluded_ids_list),
                sorted_hotel_map))
        # setting the pricing count
        self.set_gochi_count(self.excluded_ids_list)
        self.cnt_pricing = self.gochi_count
        # skimming the pricing sorted hotels now
        for id, hotel_data in consideration_subset_map:
            self.hotels_pricing.append(id)
        self.hotels_pricing = self.hotels_pricing[(-1 * self.cnt_pricing):]
        # updating the excluded ids list
        self.excluded_ids_list += self.hotels_pricing

    def set_sorted_hotels_occupancy(
        self,
        hotel_map,
        available_hotel_ids,
        checkin_date,
        checkout_date,
        room_config,
        rooms_availability_per_hotel={}):

        # excluding the hotel id which are already assigned sort index .
        # fetching the cheapest room_type's room count for all hotels
        room_wise_availability_per_hotel = {}

        for hotel_id in available_hotel_ids:
            cs_id = self.hotel_web_cs_id_dict.get(hotel_id)
            rooms = rooms_availability_per_hotel.get(cs_id)
            for sku_room_type in rooms:
                room_count = rooms.get(sku_room_type)
                room_wise_availability_per_hotel.update({(hotel_id, str(sku_room_type)): room_count})

        max_adults, max_adults_with_children = get_max_adults_with_children(room_config)
        stay_period_day_count = get_day_difference(
            str(checkin_date), str(checkout_date))
        hotel_service = HotelService()

        sorted_exclude_map = list(
            filter(
                (lambda x_y3: x_y3[0] not in self.excluded_ids_list),
                iter(
                    list(
                        hotel_map.items()))))
        hotel_ids_occupancy = [
            hotel_id for hotel_id,
                         data in sorted_exclude_map]
        hotels_room_count_map = hotel_service.get_total_rooms_by_hotel_ids(
            hotel_ids_occupancy, max_adults, max_adults_with_children)
        hotel_occupancy_map = {}

        for hotel_id in hotel_ids_occupancy:
            hotel_occupancy_map[hotel_id] = {
                'rooms_total': 0,
                'rooms_available': 0,
                'occupancy_index': 0.0}

        # filling in the availability
        if list(room_wise_availability_per_hotel.keys()):
            for hotel_room_key in list(room_wise_availability_per_hotel.keys()):
                if hotel_occupancy_map.get(hotel_room_key[0]):
                    hotel_occupancy_map[hotel_room_key[0]
                    ]['rooms_available'] += room_wise_availability_per_hotel[hotel_room_key]

        # filling in the total room count and the occupancy index
        if hotels_room_count_map:
            for hotel_id in hotels_room_count_map:
                total_room_count = hotels_room_count_map[hotel_id] * \
                                   stay_period_day_count
                hotel_occupancy_map[hotel_id]['rooms_total'] = total_room_count
                rooms_available = hotel_occupancy_map[hotel_id]['rooms_available']
                rooms_occupied = total_room_count - rooms_available
                # now that we have all the ready to eat occupancy data, let's add the availability index to the sorted exclude map
                # the lower the index, the poorer the occupancy
                if total_room_count:
                    occupancy_index = float(
                        rooms_occupied) / float(total_room_count)
                    hotel_occupancy_map[hotel_id]['occupancy_index'] = occupancy_index

        cnt = 0  # the counter
        for hotel_id, data in sorted_exclude_map:
            data['occupancy_index'] = hotel_occupancy_map[hotel_id]['occupancy_index']
            sorted_exclude_map[cnt] = (hotel_id, data)
            cnt += 1

        # sorting basis occupancy
        consideration_subset_map = sorted(
            sorted_exclude_map,
            key=lambda x_y4: (x_y4[1]['occupancy_index'], x_y4[0]))
        # setting the occupancy count
        self.cnt_occupancy = self.hotel_size - \
                             self.cnt_override - self.cnt_pricing - self.cnt_sold_out
        for id, data in consideration_subset_map:
            self.hotels_occupancy.append(id)
        self.excluded_ids_list += self.hotels_occupancy

    def set_gochi_count(self, excluded_ids_list):
        gochi_ratio = ContentStore.objects.filter(
            name='gochi_ratio', version=1).first()
        total_pricing_count = self.hotel_size - len(excluded_ids_list)
        self.gochi_count = int(math.floor(
            (total_pricing_count * int(gochi_ratio.value) / 100)))

    def set_hotel_override_cap(self):
        config_override_cap = ContentStore.objects.filter(
            name='recommended_sort_override_cap', version=1).first()
        self.override_cap = 3
        if config_override_cap:
            try:
                self.override_cap = int(config_override_cap.value)
            except Exception as e:
                pass

    def get_override_hotel_ids(self, hotel_ids, available_hotel_ids, search_channel):
        hotel_override_list = []
        hotel_cs_web_id_dict = {}
        hotel_meta_data_list = []
        try:
            hotel_cs_ids = []
            for web_id in available_hotel_ids:
                cs_id = self.hotel_web_cs_id_dict[web_id]
                hotel_cs_ids.append(cs_id)
                hotel_cs_web_id_dict[cs_id] = web_id

            if FeatureToggleAPI.is_enabled("search", "meta_order", False) and self.meta_hotel_id \
                    and (int(self.meta_hotel_id) in available_hotel_ids):
                hotel_override_list.append(int(self.meta_hotel_id))

            if FeatureToggleAPI.is_enabled("search", "promotions_order", False):
                hotel_cs_id = self.promotions_service.get_promoted_hotel(hotel_cs_ids)
                self.promoted_hotel = hotel_cs_web_id_dict[hotel_cs_id] if hotel_cs_id else None
                if self.promoted_hotel:
                    hotel_override_list.append(self.promoted_hotel)

            hotel_meta_data_list = HotelMetaData.getHotelMetaDataInfo(
                available_hotel_ids, search_channel)

            if not hotel_meta_data_list and search_channel == GDC:
                search_channel = DIRECT
                hotel_meta_data_list = HotelMetaData.getHotelMetaDataInfo(
                    available_hotel_ids, search_channel)



        except Exception as e:
            print(e)
        for hotel_meta_data in hotel_meta_data_list:
            if hotel_meta_data.hotel_id not in hotel_override_list:
                hotel_override_list.append(hotel_meta_data.hotel_id)

        return hotel_override_list

    def sort_by_distance_hotels(self, hotel_map, room_wise_availability_per_hotel=None,
                                cs_id_map={}, hotel_searched=None):
        available_hotels_list = []
        available_hotel_ids = []
        sold_out_hotels_list = []
        sort_index_dict = {}
        hotel_ids = [hotel.id for hotel in hotel_map]

        for hotel_id in hotel_ids:
            cs_id = cs_id_map.get(hotel_id)
            sort_index_dict[hotel_id] = {
                "available_room_types": []
            }
            availability_dict = room_wise_availability_per_hotel.get(cs_id, None)
            sort_index_dict[hotel_id]["available_room_types"] = list(availability_dict) if availability_dict else []
            cs_availability_status = self.cache_search.get_rooms_avalability_per_hotel(
                availability_dict)
            if cs_availability_status:
                available_hotel_ids.append(hotel_id)

        for hotel in hotel_map:
            if hotel.id not in available_hotel_ids:
                sold_out_hotels_list.append(hotel)
            else:
                available_hotels_list.append(hotel)

        if FeatureToggleAPI.is_enabled("search", "unbranded_hotels", False) and not \
            FeatureToggleAPI.is_enabled(settings.DOMAIN, "search_repo_pattern", False):
            # sorting unbranded + treebo based on new logic
            available_hotels_list = self.distance_sort_unbranded_hotels(available_hotels_list)

        distance_sorted_hotels = available_hotels_list + sold_out_hotels_list

        if hotel_searched:
            distance_sorted_hotels = [hotel_searched] + distance_sorted_hotels

        sort_index_counter = 0
        for hotel in distance_sorted_hotels:
            sort_index_dict[hotel.id]['sort_index'] = sort_index_counter
            sort_index_counter += 1

        return sort_index_dict

    @staticmethod
    def distance_sort_unbranded_hotels(hotels_list):
        """
        Sort hotel_list based on new logic, will show treebo properties till some distance cap
        :param hotels_list: assuming hotel_list is already distance sorted
        :return:
        """
        sorted_hotels_list, unbranded_hotels, already_sorted_hotels = [], [], []

        unbranded_hotel_ids = HotelSubBrandService.get_hotels_ids_for_brand(
            brand_code=INDEPENDENT_HOTEL_BRAND_CODE)

        unbranded_search_distance = ContentStore.objects.filter(
            name=INDEPENDENT_HOTELS_SEARCH_DISTANCE_CAP, version=1).first()

        distance_cap = unbranded_search_distance.value['distance'] if unbranded_search_distance \
            else INDEPENDENT_HOTELS_DEFAULT_DISTANCE_CAP

        for count, hotel_obj in enumerate(hotels_list):
            if hotel_obj.id not in unbranded_hotel_ids and hotel_obj.distance <= distance_cap:
                sorted_hotels_list.append(hotel_obj)
            elif hotel_obj.id in unbranded_hotel_ids and hotel_obj.distance <= distance_cap:
                unbranded_hotels.append(hotel_obj)
            else:
                already_sorted_hotels = hotels_list[count:]
                break

        sorted_hotels_list = sorted_hotels_list + unbranded_hotels + already_sorted_hotels

        return sorted_hotels_list

    def recommended_sort_unbranded_hotels(self, hotel_ids):
        """
        all treebo properties will show above unbranded hotels
        :param:
        :return:
        """
        if hotel_ids:
            unbranded_hotel_ids = HotelSubBrandService.get_hotels_ids_for_brand(
                brand_code=INDEPENDENT_HOTEL_BRAND_CODE)

            for hotel_id in unbranded_hotel_ids:
                if hotel_id in hotel_ids and hotel_id not in self.hotels_override:
                    self.hotels_unbranded.append(hotel_id)

            self.excluded_ids_list += self.hotels_unbranded
