HD_COMPONENT_TITLE = "seo_hd_component_titles"
FACILITIES_ORDER = "facilities_order"
DEFAULT_FACILITIES_ORDER_DICT = {"General": 2,
                                 "Business Facilities": 5,
                                 "Eating & Dining": 4,
                                 "Personal Services": 3,
                                 "In room facilities": 1,
                                 "Recreation": 6}
DEFAULT_HD_FACILITY_TITLE = "facilities"
DEFAULT_HD_FEEDBACK_TITLE = "positive_feedback"
DEFAULT_POSITIVES_ORDER = "default_positives_order"
DEFAULT_POSITIVES_DICT = {
    "Restaurant / Kitchen - Breakfast": 1,
    "Restaurant - Breakfast Food Quality": 2,
    "Staff - Behavior / Politeness": 3,
    "Location": 4
}
MAX_BLOG_TO_DISPLAY = 25
