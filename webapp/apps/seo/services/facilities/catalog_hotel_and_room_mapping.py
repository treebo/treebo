import logging

from django.conf import settings

from apps.seo.models.web_catalog_hotel_mapping import WebCatalogHotelMapping, CatalogRoomMapping
from apps.seo.services.common_services.hit_url import URLBaseClass
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.room import Room
from data_services.exceptions import HotelDoesNotExist
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class WebCatalogMapping(object):
    def check_web_and_catalog_hotels(self):
        try:
            url = settings.CATALOG_HOTEL_URL
            url_response = URLBaseClass()
            response_json = url_response.get_response(url=url)
            for data in response_json:
                hx_id = data.get('hx_id')
                catalog_hotel_id = data.get('id')
                self.update_web_and_catalog_hotels(
                    hotelogix_id=hx_id, catalog_hotel_id=catalog_hotel_id)
        except Exception:
            logger.exception(
                "Exception in mapping web hotels and cataloguing service")

    def update_web_and_catalog_hotels(self, hotelogix_id, catalog_hotel_id):
        try:
            if WebCatalogHotelMapping.objects.filter(
                    hotel__hotelogix_id=hotelogix_id,
                    catalog_hotel_id=catalog_hotel_id).exists():
                logger.info(
                    "Already exists for hotel %s and catalog id %s",
                    hotelogix_id,
                    catalog_hotel_id)
            else:
                logger.info(
                    "Creating catalog mapping for hotel %s and catalog id %s",
                    hotelogix_id,
                    catalog_hotel_id)
                hotel = Hotel.objects.get(hotelogix_id=hotelogix_id)
                catalog_hotel_id = catalog_hotel_id
                WebCatalogHotelMapping.objects.create(
                    hotel=hotel, catalog_hotel_id=catalog_hotel_id).save()
                logger.info(
                    "saved mapping %s %s",
                    hotelogix_id,
                    catalog_hotel_id)
                self.map_web_and_catalog_rooms(hotelogix_id=hotelogix_id)
        except Hotel.DoesNotExist as e:
            logger.exception("No such hotel exists %s", hotelogix_id)
        except Exception as e:
            logger.exception(
                "Exception in mapping web hotels and cataloguing service")
            raise e

    def map_web_and_catalog_rooms(self, hotelogix_id):
        try:
            logger.info("Now save rooms id for the hotel %s", hotelogix_id)
            catalog_hotel = WebCatalogHotelMapping.objects.get(
                hotel__hotelogix_id=hotelogix_id)
            url = settings.CATALOG_ROOMS_URL.format(
                catalog_hotel.catalog_hotel_id)
            url_response = URLBaseClass()
            response_json = url_response.get_response(url=url)
            for data in response_json:
                if CatalogRoomMapping.objects.filter(
                        hotel_map=catalog_hotel,
                        catalog_room_id=data.get('id')).exists():
                    logger.info(
                        "Room mapping - already exists for hotel id %s catalog hotel id %s and catalog room id %s",
                        catalog_hotel.hotel.id,
                        catalog_hotel.catalog_hotel_id,
                        data.get('id'))
                    continue
                else:
                    logger.info(
                        "Room mapping - created for hotel id %s catalog hotel id %s and catalog room id %s",
                        catalog_hotel.hotel.id,
                        catalog_hotel.catalog_hotel_id,
                        data.get('id'))
                    catalog_room_id = data.get('id')
                    room_type_code = data.get('room_type').get('type')
                    if catalog_room_id and room_type_code:
                        room_obj = Room.objects.filter(
                            hotel__hotelogix_id=hotelogix_id,
                            room_type_code__iexact=room_type_code).first()
                        if room_obj:
                            CatalogRoomMapping.objects.create(
                                hotel_map=catalog_hotel, room=room_obj, catalog_room_id=catalog_room_id).save()
                            logger.info(
                                "saved mapping %s %s %s",
                                catalog_hotel.catalog_hotel_id,
                                data.get('id'),
                                room_type_code)
                        else:
                            logger.error(
                                "No object in room table for %s %s exists",
                                room_type_code,
                                hotelogix_id)
                    else:
                        logger.error(
                            "Error in getting the room id %s for hotel %s room %s",
                            catalog_room_id,
                            catalog_hotel.hotelogix_id,
                            room_type_code)

        except WebCatalogHotelMapping.DoesNotExist as e:
            logger.exception(
                "Hotel with %s does not exist in catalog mapping",
                hotelogix_id)
            raise e

        except Exception as e:
            logger.exception(
                "Exception in mapping web rooms and cataloguing service")
            raise e
