from rest_framework.status import HTTP_409_CONFLICT, HTTP_404_NOT_FOUND, HTTP_401_UNAUTHORIZED, HTTP_400_BAD_REQUEST, \
    HTTP_503_SERVICE_UNAVAILABLE, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_403_FORBIDDEN, HTTP_429_TOO_MANY_REQUESTS

from apps.common import error_codes
from common.exceptions.treebo_exception import TreeboException, ExternalServiceException


class UserAlreadyPresentException(TreeboException):
    def __init__(self, *args, **kwargs):
        super(UserAlreadyPresentException, self).__init__()
        self.message = 'User already present.'
        self.status_code = HTTP_409_CONFLICT
        self.error_code = self.app_type + "2001"


class UserWithPhoneAlreadyExist(TreeboException):
    def __init__(self, *args, **kwargs):
        super(UserWithPhoneAlreadyExist, self).__init__()
        self.message = 'User phone already present.'
        self.status_code = HTTP_409_CONFLICT
        self.error_code = self.app_type + "2002"


class UserWithEmailAlreadyExist(TreeboException):
    def __init__(self, *args, **kwargs):
        super(UserWithEmailAlreadyExist, self).__init__()
        self.message = 'User email already present.'
        self.status_code = HTTP_409_CONFLICT
        self.error_code = self.app_type + "2003"


class UserWithAuthIDAlreadyExist(TreeboException):
    def __init__(self, *args, **kwargs):
        super(UserWithAuthIDAlreadyExist, self).__init__()
        self.message = 'User auth id already present.'
        self.status_code = HTTP_409_CONFLICT
        self.error_code = self.app_type + "2004"


class PhoneNumberDoesnotMatch(TreeboException):
    def __init__(self, *args, **kwargs):
        super(PhoneNumberDoesnotMatch, self).__init__()
        self.message = 'Given phone number doesnt match with the AuthN records'
        self.status_code = HTTP_409_CONFLICT


class AuthNClientException(TreeboException):
    def __init__(self, message, *args, **kwargs):
        super(AuthNClientException, self).__init__()
        self.message = message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR


class UPSClientException(ExternalServiceException):
    def __init__(self, message, *args, **kwargs):
        super(UPSClientException, self).__init__()
        self.message = message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR


class AuthZClientException(TreeboException):
    def __init__(self, message, *args, **kwargs):
        super(AuthZClientException, self).__init__()
        self.message = message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR


class AuthException(TreeboException):
    def __init__(self, message, status_code):
        super(AuthException, self).__init__()
        self.message = message
        self.status_code = status_code
        self.error_code = self.app_type + "3000"


class OTPSendException(AuthException):
    def __init__(self, message, status_code, otp_request_attempts=None, max_otp_request_attempts=None):
        super(OTPSendException, self).__init__(message, status_code)
        self.otp_request_attempts = otp_request_attempts
        self.max_otp_request_attempts = max_otp_request_attempts


class OTPVerifyException(AuthException):
    def __init__(self, message, status_code, failed_validation_attempts=None, max_failed_validation_attempts=None):
        super(OTPVerifyException, self).__init__(message, status_code)
        self.failed_validation_attempts = failed_validation_attempts
        self.max_failed_validation_attempts = max_failed_validation_attempts


class TokenNotFoundException(TreeboException):
    def __init__(self, message):
        super(TokenNotFoundException, self).__init__()
        self.message = 'Token not found.'
        self.status_code = HTTP_404_NOT_FOUND
        self.error_code = self.app_type + "3001"


class InvalidTokenException(TreeboException):
    def __init__(self, token):
        super(InvalidTokenException, self).__init__()
        self.message = 'Invalid token.'
        self.status_code = HTTP_404_NOT_FOUND
        self.error_code = self.app_type + "3001"


class UserEmailNotFoundException(TreeboException):
    def __init__(self, email):
        super(UserEmailNotFoundException, self).__init__()
        self.message = 'User email not found.'
        self.status_code = HTTP_404_NOT_FOUND
        self.error_code = self.app_type + "3002"


class UserAppCredentialsNotFound(TreeboException):
    def __init__(self, grant_type):
        super(UserAppCredentialsNotFound, self).__init__(grant_type)
        self.message = 'Credentials not found.'
        self.status_code = HTTP_404_NOT_FOUND
        self.error_code = self.app_type + "3003"


class UserUnauthroized(TreeboException):
    def __init__(self):
        super(UserUnauthroized, self).__init__()
        self.message = 'Unauthorized.'
        self.status_code = HTTP_401_UNAUTHORIZED
        self.error_code = self.app_type + "3004"


class PaymentOrderException(TreeboException):
    def __init__(
            self,
            message,
            developer_message,
            status_code,
            *args,
            **kwargs):
        super(PaymentOrderException, self).__init__()
        self.message = message
        self.developer_message = developer_message
        self.status_code = status_code
        self.error_code = self.app_type + "4000"


class PaymentValidationException(TreeboException):
    def __init__(
            self,
            developer_message,
            status_code=HTTP_400_BAD_REQUEST,
            *args,
            **kwargs):
        super(PaymentValidationException, self).__init__()
        self.message = 'Payment validaton failed.'
        self.developer_message = developer_message
        self.status_code = status_code
        self.error_code = self.app_type + "4001"


class HotelNotFoundException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(HotelNotFoundException, self).__init__()
        self.message = 'Unable to find hotel.'
        self.developer_message = developer_message
        self.status_code = HTTP_404_NOT_FOUND
        self.error_code = self.app_type + "4003"


class RefundStatusException(TreeboException):
    def __init__(self, developer_message, status_code, *args, **kwargs):
        super(RefundStatusException, self).__init__()
        self.message = 'Unable to fetch refund status.'
        self.developer_message = str(developer_message)
        self.status_code = status_code
        self.error_code = self.app_type + "4004"


class RefundQuoteException(TreeboException):
    def __init__(self, developer_message, status_code, *args, **kwargs):
        super(RefundQuoteException, self).__init__()
        self.message = 'Unable to fetch refund amount.'
        self.developer_message = str(developer_message)
        self.status_code = status_code
        self.error_code = self.app_type + "4005"


class PaymentGenerationException(TreeboException):
    def __init__(
            self,
            message,
            developer_message,
            status_code,
            *args,
            **kwargs):
        super(PaymentGenerationException, self).__init__()
        self.message = message
        self.developer_message = developer_message
        self.status_code = status_code
        self.error_code = self.app_type + "4007"


class PaymentUpdateException(TreeboException):
    def __init__(self, developer_message, status_code, *args, **kwargs):
        super(PaymentUpdateException, self).__init__()
        self.message = 'Unable to update payment.'
        self.developer_message = developer_message
        self.status_code = status_code
        self.error_code = self.app_type + "4008"


class PaymentVerifyException(TreeboException):
    def __init__(self, message, developer_message, *args, **kwargs):
        super(PaymentVerifyException, self).__init__()
        error = error_codes.get(error_codes.UNABLE_TO_VERIFY_PAYMENT)
        self.message = message
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])

class StatusPendingException(TreeboException):
    def __init__(self, message, developer_message, *args, **kwargs):
        super(StatusPendingException, self).__init__()
        error = error_codes.get(error_codes.UNABLE_TO_UPDATE_STATUS)
        self.message = message
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])

class PaymentConfirmationPullException(TreeboException):
    def __init__(self, message, developer_message, *args, **kwargs):
        super(PaymentConfirmationPullException, self).__init__()
        error = error_codes.get(error_codes.UNABLE_TO_CONFIRM_PAYMENT_ON_PULL)
        self.message = message
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class PaymentClientRefundException(TreeboException):
    def __init__(self, message, status_code, developer_message="", *args, **kwargs):
        super(PaymentClientRefundException, self).__init__()
        self.message = message
        self.developer_message = developer_message
        self.status_code = status_code


class UserAssigmentException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(UserAssigmentException, self).__init__()
        self.message = 'Unable assign user.'
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST


class GoogleLocationException(TreeboException):
    def __init__(self, developer_message, status_code, *args, **kwargs):
        super(GoogleLocationException, self).__init__()
        self.message = 'Unable to find location by longitude and latitude.'
        self.developer_message = developer_message
        self.status_code = status_code
        self.error_code = self.app_type + "4009"


class StateNotFoundException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(StateNotFoundException, self).__init__()
        self.message = 'Unable to find state by longitude and latitude.'
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + "4010"


class WifiConfigNotFound(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(WifiConfigNotFound, self).__init__()
        self.message = 'Unable to find wifi common config.'
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + "4010"


class WalletBalanceException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(WalletBalanceException, self).__init__()
        error = error_codes.get(error_codes.WALLET_BALANCE_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error_codes.WALLET_BALANCE_ERROR)


class LoyaltyPolicyException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(LoyaltyPolicyException, self).__init__()
        error = error_codes.get(error_codes.LOYALTY_POLICY_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error_codes.LOYALTY_POLICY_ERROR)


class UserServiceException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(UserServiceException, self).__init__()
        error = error_codes.get(error_codes.GET_USERID_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class PricingException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(PricingException, self).__init__()
        error = error_codes.get(error_codes.PRICING_SERVICE_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class CouponException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(CouponException, self).__init__()
        error = error_codes.get(error_codes.INVALID_COUPON)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class RemoveCouponException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(RemoveCouponException, self).__init__()
        error = error_codes.get(error_codes.COUPON_REMOVE_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class ApplyCouponException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(ApplyCouponException, self).__init__()
        error = error_codes.get(error_codes.INVALID_COUPON_CODE)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class RoomNotAvailableException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(RoomNotAvailableException, self).__init__()
        error = error_codes.get(error_codes.ROOM_NOT_FOUND)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class InvalidRequestException(TreeboException):
    def __init__(self, errors, *args, **kwargs):
        super(InvalidRequestException, self).__init__()
        error = error_codes.get(error_codes.INVALID_REQUEST)
        self.errors = errors
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class InvalidDiscountCouponException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(InvalidDiscountCouponException, self).__init__()
        error = error_codes.get(error_codes.INVALID_COUPON_CODE)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class WalletPaymentException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(WalletPaymentException, self).__init__()
        error = error_codes.get(error_codes.INVALID_COUPON_CODE)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class PaymentOrderAlreadyPresent(TreeboException):
    def __init__(self, *args, **kwargs):
        super(PaymentOrderAlreadyPresent, self).__init__()
        error = error_codes.get(error_codes.PAYMENT_ALREADY_PRESENT)
        self.message = error['msg']
        self.developer_message = ''
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class PaymentOrderNotFound(TreeboException):
    def __init__(self, *args, **kwargs):
        super(PaymentOrderNotFound, self).__init__()
        error = error_codes.get(error_codes.PAYMENT_ORDER_NOT_FOUND)
        self.message = error['msg']
        self.developer_message = ''
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class InvalidPaymentOrder(TreeboException):
    def __init__(self, *args, **kwargs):
        super(InvalidPaymentOrder, self).__init__()
        error = error_codes.get(error_codes.PAYMENT_ORDER_NOT_FOUND)
        self.message = error['msg']
        self.developer_message = ''
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class EmptyPayloadInCRSMigrationEventException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(EmptyPayloadInCRSMigrationEventException, self).__init__()
        error = error_codes.get(error_codes.EMPTY_PAYLOAD_IN_CRS_MIGRATION_EVENT)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + error['code']


class BookingUpdateFailedInCRSIntegrationException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(BookingUpdateFailedInCRSIntegrationException, self).__init__()
        error = error_codes.get(error_codes.BOOKING_UPDATE_FAILED_IN_CRS_MIGRATION)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = error['status_code']
        self.error_code = self.app_type + error['code']


class BadITSAPIRequestException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(BadITSAPIRequestException, self).__init__()
        error = error_codes.get(error_codes.BAD_ITS_API_REQUEST)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + error['code']


class InactiveHotelITSRequestException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(InactiveHotelITSRequestException, self).__init__()
        error = error_codes.get(error_codes.NO_HOTELS_FOUND)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + error['code']

class ITSServiceUnavailableException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(ITSServiceUnavailableException, self).__init__()
        error = error_codes.get(error_codes.ITS_SERVICE_UNAVAILABLE)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_503_SERVICE_UNAVAILABLE
        self.error_code = self.app_type + error['code']


class NoHotelsFoundException(TreeboException):
    def __init__(self, *args, **kwargs):
        super(NoHotelsFoundException, self).__init__()
        error = error_codes.get(error_codes.NO_HOTELS_FOUND)
        self.message = error['msg']
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + error['code']


class InvalidBookingDates(TreeboException):
    def __init__(self, *args, **kwargs):
        super(InvalidBookingDates, self).__init__()
        error = error_codes.get(error_codes.INVALID_DATE_FORMAT)
        self.message = error['msg']
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + error['code']


class InitiateBookingValidationError(TreeboException):
    def __init__(self, message, *args, **kwargs):
        super(InitiateBookingValidationError, self).__init__()
        self.message = message
        self.status_code = HTTP_400_BAD_REQUEST


class AxisRoomBookingValidationError(TreeboException):
    def __init__(self, message, *args, **kwargs):
        super(AxisRoomBookingValidationError, self).__init__()
        self.message = message
        self.status_code = HTTP_400_BAD_REQUEST

class AxisRoomBookingConfirmationError(TreeboException):
    def __init__(self, message, *args, **kwargs):
        super(AxisRoomBookingConfirmationError, self).__init__()
        self.message = message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR


class CreateBookingValidationError(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(CreateBookingValidationError, self).__init__()
        error = error_codes.get(error_codes.CREATE_BOOKING_VALIDATION_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class ExternalServiceUnavailableException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(ExternalServiceUnavailableException, self).__init__()
        error = error_codes.get(error_codes.EXTERNAL_SERVICE_UNAVAILABLE)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_503_SERVICE_UNAVAILABLE
        self.error_code = self.app_type + error['code']


class InvalidPageException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(InvalidPageException, self).__init__()
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST


class InvalidPartpayPolicyInContentStore(TreeboException):
    def __init__(self, message, *args, **kwargs):
        super(InvalidPartpayPolicyInContentStore, self).__init__()
        self.message = message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR


class KeyNotFoundInContentStore(TreeboException):
    def __init__(self, message, *args, **kwargs):
        super(KeyNotFoundInContentStore, self).__init__()
        self.message = message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR


class PartpayNotApplicable(TreeboException):
    def __init__(self, message):
        super(PartpayNotApplicable, self).__init__()
        self.message = message
        self.status_code = HTTP_400_BAD_REQUEST


class PublishEventToGrowthError(TreeboException):
    def __init__(self, message):
        super(PublishEventToGrowthError, self).__init__()
        self.message = message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR


class TrueCallerAPIError(TreeboException):
    def __init__(self, developer_message, status_code, *args, **kwargs):
        super(TrueCallerAPIError, self).__init__()
        error = error_codes.get(error_codes.TRUE_CALLER_API_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = status_code
        self.error_code = self.app_type + error['code']


class TrueCallerServiceUnavailableException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(TrueCallerServiceUnavailableException, self).__init__()
        error = error_codes.get(error_codes.TRUE_CALLER_SERVICE_UNAVAILABLE)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_503_SERVICE_UNAVAILABLE
        self.error_code = self.app_type + error['code']


class TrueCallerFetchProfileUserRejectedException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(TrueCallerFetchProfileUserRejectedException, self).__init__()
        error = error_codes.get(error_codes.TRUE_CALLER_FETCH_PROFILE_ERROR_USER_REJECTED)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_401_UNAUTHORIZED
        self.error_code = self.app_type + error['code']


class IncompleteGSTDetailInRequestException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(IncompleteGSTDetailInRequestException, self).__init__()
        error = error_codes.get(error_codes.INCOMPLETE_GST_DETAIL)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + error['code']


class InvalidRequestParamInAPIException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(InvalidRequestParamInAPIException, self).__init__()
        error = error_codes.get(error_codes.INVALID_REQUEST_PARAM_IN_API)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + error['code']


class ReCaptchaServiceUnavailableException(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(ReCaptchaServiceUnavailableException, self).__init__()
        error = error_codes.get(error_codes.RECAPTCHA_SERVICE_UNAVAILABLE)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_503_SERVICE_UNAVAILABLE
        self.error_code = self.app_type + error['code']


class ReCaptchaVerifyError(TreeboException):
    def __init__(self, developer_message, error_messages, *args, **kwargs):
        super(ReCaptchaVerifyError, self).__init__()
        error = error_codes.get(error_codes.RECAPTCHA_VERIFY_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + error['code']
        self.error_messages = error_messages


class ReCaptchaScoreBelowThresholdError(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(ReCaptchaScoreBelowThresholdError, self).__init__()
        error = error_codes.get(error_codes.RECAPTCHA_SCORE_BELOW_THRESHOLD_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_403_FORBIDDEN
        self.error_code = self.app_type + error['code']


class UnverifiedPAHBookingLimitExceededException(TreeboException):
    def __init__(self, *args, **kwargs):
        super(UnverifiedPAHBookingLimitExceededException, self).__init__()
        error = error_codes.get(error_codes.UNVERIFIED_PAH_BOOKING_LIMIT_EXCEEDED_EXCEPTION)
        self.message = error['msg']
        self.status_code = HTTP_429_TOO_MANY_REQUESTS
        self.error_code = self.app_type + error['code']
