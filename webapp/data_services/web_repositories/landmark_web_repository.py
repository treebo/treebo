from base import log_args
from django.db.models import Q
from dbcommon.models.landmark import SearchLandmark


class LandmarkWebRepository:

    @log_args()
    def seo_landmarks_with_city_seo_enabled(self):
        return SearchLandmark.objects.filter(
            enable_for_seo=True, city__enable_for_seo=True).all()

    @log_args()
    def get_landmark_objects_with_filter(self, **kwargs):
        return SearchLandmark.objects.filter(**kwargs).all()

    @log_args()
    def get_all_active_landmarks_for_city(self, city_id):
        return SearchLandmark.objects.filter(status=1, city__id=city_id).all()

    @log_args()
    def get_landmarks_by_freetext_url(self, query_string):
        return SearchLandmark.objects.filter(
            free_text_url__iexact=query_string, status=1).all()

    @log_args()
    def get_landmark_for_seo_url_and_city_name(
            self,
            landmark_name,
            city_name,
            free_text_url_check_for_landmark=False):
        if free_text_url_check_for_landmark:
            return SearchLandmark.objects.filter(
                Q(Q(city__name__iexact=city_name) | Q(city__slug__iexact=city_name)) & Q(
                    Q(free_text_url__iexact=landmark_name) | Q(seo_url_name__iexact=landmark_name))).first()
        return SearchLandmark.objects.filter(Q(seo_url_name__iexact=landmark_name) & Q(status=1) & Q(
            Q(city__name__iexact=city_name) | Q(city__slug__iexact=city_name))).first()

    # TODO: Give it a better name
    @log_args()
    def get_landmarks_for_city_with_exclusion(
            self,
            city_name,
            enable_for_seo,
            city_enabled_for_seo,
            excluded_ids_list):
        return SearchLandmark.objects.filter(
            city__name__iexact=city_name,
            enable_for_seo=enable_for_seo,
            city__enable_for_seo=city_enabled_for_seo,
            status=1).exclude(
            id__in=excluded_ids_list)
