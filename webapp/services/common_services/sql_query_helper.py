from django.db import connection



class SqlQueryHelper:

    def __init__(self):
        pass

    @staticmethod
    def execute_query_and_map_to_dictionary_with_params(query_string, params):
        with connection.cursor() as cursor:
            cursor.execute(query_string, params)
            col_names = [desc[0] for desc in cursor.description]
            while True:
                row = cursor.fetchone()
                if row is None:
                    break
                row_dict = dict(list(zip(col_names, row)))
                yield row_dict
            return
