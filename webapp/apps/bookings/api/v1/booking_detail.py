import logging

from apps.common.exceptions.booking_exceptions import BookingNotFoundException
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.bookings.services.booking_detail_service import BookingDetailService
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import UserUnauthroized
from apps.common import error_codes
from base.views.api import TreeboAPIView
from base.renderers import TreeboCustomJSONRenderer

logger = logging.getLogger(__name__)


class BookingDetail(TreeboAPIView):
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    renderer_classes = [TreeboCustomJSONRenderer, ]

    def get(self, request, **kwargs):

        booking_id = kwargs.get('booking_id', '')
        if not (hasattr(request, 'user')
                and request.user and request.user.is_authenticated()):
            response = api_response.treebo_exception_error_response(UserUnauthroized())
        else:
            try:
                user = request.user
                booking_detail_service = BookingDetailService(booking_id=booking_id)
                booking_details = booking_detail_service.get_booking_detail()
                response = api_response.prep_success_response(booking_details)
            except BookingNotFoundException as e:
                logger.exception("booking not found for the booking_id {booking_id}".format(booking_id=booking_id))
                response = api_response.bad_request_error_message(error_message=e.developer_message,
                                                                  error_type="booking_not_found",
                                                                  error_code='WEB_4002')
            except Exception as e:
                logger.exception("Exception {e} is coming for the user {user}.".format(user=user,e=e))
                response = api_response.internal_error_response(error_codes.get(
                    error_codes.UNABLE_TO_FETCH_BOOKING), BookingDetail.__name__)

        return response
