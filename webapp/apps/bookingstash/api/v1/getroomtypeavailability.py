import logging
from datetime import datetime, timedelta

from rest_framework.response import Response

from apps.bookingstash.service.availability_service import get_availability_service
from apps.common.utils import Utils
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes

logger = logging.getLogger(__name__)


class RoomTypeAvailability(TreeboAPIView):
    @log_args(logger)
    def get(self, request):
        checkInDate, checkOutDate, hashedHotelId, roomType = self.__parseRequest(
            request)

        if not hashedHotelId:
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.NO_HOTEL_ROOM_ID_SUPPLIED))

        cheapestRoom = get_availability_service().get_room_type_min_availability(
            hashedHotelId, checkInDate, checkOutDate, roomType)

        return Response({'minavailability': cheapestRoom})

    def __parseRequest(self, request):
        d = request.GET

        try:
            checkInDate = d.get('checkin') if 'checkin' in d and d.get(
                'checkin') else datetime.now().strftime("%Y-%m-%d")
        except ValueError:
            logger.info(
                "Invalid date format received for checkIn :" +
                d.get(
                    'checkin',
                    None))
            checkInDate = datetime.now().strftime("%Y-%m-%d")
        try:
            checkOutDate = d.get('checkout') if 'checkout' in d and d.get(
                'checkout') else (
                datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
        except ValueError:
            logger.info(
                "Invalid date format received for checkOut :" +
                d.get(
                    'checkout',
                    None))
            checkOutDate = (
                datetime.now() +
                timedelta(
                    days=1)).strftime("%Y-%m-%d")

        hashedHotelId = d.get('hotelid', None)

        roomType = d.get('roomtype') if 'roomtype' in d else 'Oak'
        return checkInDate, checkOutDate, hashedHotelId, roomType


class RoomTypeAvailabilityAll(TreeboAPIView):
    @log_args(logger)
    def get(self, request):
        checkInDate, checkOutDate = self.__parseRequest(request)

        cheapestRoom = get_availability_service().get_room_type_min_availability_for_all_hotels(
            checkInDate, checkOutDate)

        return Response({'minavailability': cheapestRoom})

    def __parseRequest(self, request):
        d = request.GET

        try:
            checkInDate = d.get('checkin') if 'checkin' in d and d.get(
                'checkin') else datetime.now().strftime("%Y-%m-%d")
        except ValueError:
            logger.info(
                "Invalid date format received for checkIn :" +
                d.get(
                    'checkin',
                    None))
            checkInDate = datetime.now().strftime("%Y-%m-%d")
        try:
            checkOutDate = d.get('checkout') if 'checkout' in d and d.get(
                'checkout') else (
                datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
        except ValueError:
            logger.info(
                "Invalid date format received for checkOut :" +
                d.get(
                    'checkout',
                    None))
            checkOutDate = (
                datetime.now() +
                timedelta(
                    days=1)).strftime("%Y-%m-%d")

        return checkInDate, checkOutDate
