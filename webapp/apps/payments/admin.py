# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin.options import flatten_fieldsets
from django.core.urlresolvers import reverse
# Register your models here.
from apps.bookings.models import Booking, Reservation, RoomBooking, AsyncJobStatus, PaymentOrder
from dbcommon.admin import ReadOnlyAdminMixin
from apps.payments.models import PaymentMethod, PaymentOffer


@admin.register(PaymentMethod)
class PaymentMethodAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'name',
        'category',
        'image_url',
        'gateway_code',
        'ranking',
        'utm_source')


@admin.register(PaymentOffer)
class PaymentMethodAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        "payment_method",
        "offer_percentage",
        "credit_mode",
        "minimum_transaction",
        "ranking",
        "description",
        "terms_conditions",
        "gateway_offer_id",
        "image_url",
        "coupon_code",
        "utm_source",
        "auto_apply",

    )
