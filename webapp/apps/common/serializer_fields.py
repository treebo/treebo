from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers


class CommaSeparatedIntegerField(serializers.Field):
    """
    Helps in serialization and deserialization of input coming in comma separated integers
    """

    default_error_messages = {
        'invalid': _('Only comma separated integer fields are allowed'),
    }

    def __init__(self, *args, **kwargs):
        super(CommaSeparatedIntegerField, self).__init__(*args, **kwargs)

    def to_internal_value(self, data):
        try:
            ids = [int(val) for val in data.split(",")]
        except Exception as e:
            self.fail('invalid')
        return ids

    def to_representation(self, value):
        if value is None:
            return []

        assert isinstance(value, list), "Value should be a list"
        return ",".join(value)
