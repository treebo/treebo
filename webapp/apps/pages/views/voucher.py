import logging

from base.views.template import TreeboTemplateView

__author__ = 'devang'

logger = logging.getLogger(__name__)


class Voucher(TreeboTemplateView):
    template_name = "voucher/index.html"
    redirect_mobile_to_desktop_view = True

    def getData(self, request, args, kwargs):
        template_name = "voucher/index.html"
        return {'scripts': [], 'styles': ['desktop/css/voucher.css']}

    def getMobileData(self, request, args, kwargs):
        return {'styles': ['css/terms.css']}
