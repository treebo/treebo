import csv
import logging

from django.core.management import BaseCommand
from django.utils.encoding import smart_str

from apps.reviews.cache.cache_service import CacheService
from apps.reviews.cache.page_config import PageConfig
from apps.reviews.models import TAHotelIDMapping
from apps.third_party.service.trip_advisor.fetch_reviews_service import TAReviewAPIService
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from common.constants import common_constants as const
from data_services.exceptions import HotelDoesNotExist

import collections

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    cache_service = CacheService()
    external_service = TAReviewAPIService()

    def handle(self, *args, **options):
        try:
            self.update_ta_hotel_mapping_table()
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            hotels = hotel_repository.get_active_hotel_list()
            config = PageConfig()
            pages = [
                attr for attr in dir(config) if not isinstance(
                    getattr(
                        config,
                        attr),
                    collections.Callable) and not attr.startswith("__")]
            for hotel in hotels:
                try:
                    self.external_service.fetch_review_and_update_audit(
                        hotel.id)
                    for page in pages:
                        self.cache_service.get_or_update_cache(
                            hotel.id, getattr(config, page))
                except BaseException:
                    logger.exception(
                        'exception occured for hotel id: %s hotel_logix_id: %s',
                        hotel.id,
                        hotel.hotelogix_id)
        except BaseException:
            logger.exception('exception occured in handler')

    def update_ta_hotel_mapping_table(self, ):
        with open('treebo_india_mapped.csv') as csvfile:
            file = csv.reader(csvfile, delimiter=',')
            for row in file:
                if row[14] and row[0] != "id":
                    hotel_id = None
                    try:
                        hotel_id = int(row[0])
                        hotel = Hotel.objects.get(id=hotel_id)
                        hotel_ta_map, create = TAHotelIDMapping.objects.get_or_create(
                            hotel=hotel, ta_location_id=str(row[14]), write_review_url=smart_str(row[17]))
                    except HotelDoesNotExist as e:
                        logger.info("Hotel does not exist for id ", hotel_id)
                    except BaseException:
                        logger.exception(
                            'Exception while updating tahotelidmapping from csv for entry %s', row)
