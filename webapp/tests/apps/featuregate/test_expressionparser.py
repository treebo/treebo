from django.test import SimpleTestCase
from apps.featuregate import constants
from apps.featuregate.expression_parser_new import ExpressionParser
from apps.featuregate import constants as featureConstants


class MockConstraintValidator():
    def __init__(self, checkin, checkout, hotel_id, totalcost, locality, city):
        self.__checkin = checkin
        self.__checkout = checkout
        self.__hotel_id = hotel_id
        self.__totalcost = totalcost
        self.__locality = locality
        self.__city = city
        self.message = constants.SUCCESS

    def validatorFactory(self, parsedTerm):
        result = bool(int(parsedTerm))
        print(("mock validatorFactory  " + str(parsedTerm), " ret ", result))
        if result:
            self.message = constants.SUCCESS
        else:
            self.message = constants.FAILURE
        return result


class ExpressionVaildator(SimpleTestCase):
    def setUp(self):
        self.constraintValidator = MockConstraintValidator(
            '2017-01-10', '2017-01-15', 15, 234, 'jaipur', 3)
        pass

    def test_and_1(self):
        self.__run_succes("1 AND 1")

    def test_and_2(self):
        self.__run_fail("1 AND 0")

    def test_and_3(self):
        self.__run_fail("1 AND 0 AND 1")

    def test_and_4(self):
        self.__run_succes("1 AND 1 AND 1")

    def test_or_1(self):
        self.__run_fail("0 OR 0")

    def test_or_2(self):
        self.__run_succes("1 OR 0")

    def test_or_3(self):
        self.__run_succes("1 OR 0 OR 1")

    def test_or_4(self):
        self.__run_succes("1 OR 1 OR 1")

    def test_andor_1(self):
        self.__run_succes("0 AND 1 OR 1")

    def test_andor_2(self):
        self.__run_fail("0 AND 1 OR 0")

    def test_andor_3(self):
        self.__run_succes("1 AND ( 1 OR 0 )")

    def test_andor_4(self):
        self.__run_succes("( 1 AND 1 ) OR 0")

    def test_andor_5(self):
        self.__run_succes("( 1 OR 1 OR ( 1 AND 0 ) ) OR 0")

    def test_andor_6(self):
        self.__run_succes("( 1 AND 1 ) OR ( 1 AND 0 )")

    def test_andor_7(self):
        self.__run_fail("( 1 AND 1 ) AND ( 1 AND 0 )")

    def test_andor_8(self):
        self.__run_fail(
            "( ( 1 AND 0 ) AND ( 1 OR 1 ) ) OR ( 1 OR 0 ) AND ( 1 AND 0 )")

    def test_andor_9(self):
        self.__run_succes(
            "( ( 1 AND 0 ) OR ( 1 OR 1 ) ) OR ( 1 OR 0 ) AND ( 1 AND 0 )")

    def __run_succes(self, exp):
        tokens = exp.split(' ')
        print((" ------ tesing exp for success ", exp))
        ExpressionParser(tokens, self.constraintValidator).parse_expression()
        print((" success ", exp, "staus ", self.constraintValidator.message))
        self.assertEqual(
            self.constraintValidator.message,
            featureConstants.SUCCESS)

    def __run_fail(self, exp):
        tokens = exp.split(' ')
        print(("------ tesing exp for fail ", exp))
        ExpressionParser(tokens, self.constraintValidator).parse_expression()
        print((" failed ", exp, "staus ", self.constraintValidator.message))
        self.assertEqual(
            self.constraintValidator.message,
            featureConstants.FAILURE)
