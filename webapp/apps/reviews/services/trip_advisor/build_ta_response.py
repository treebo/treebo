import logging

from apps.reviews.services.trip_advisor.hotel_ta_mapping_service import HotelTAMappingService
from apps.reviews.services.trip_advisor.review_config_service import ReviewAppConfigService

from apps.reviews.serializers.review_serializers import HDPageReviewDTO, SearchPageReviewDTO
from apps.reviews.services.review_response_data.hd_review_data import HDReviewData
from apps.reviews.services.review_response_data.search_review_data import SearchReviewData

logger = logging.getLogger(__name__)


class BuildTAReviewResponse(object):
    review_app_config = ReviewAppConfigService()

    def __init__(self, hotel_id):
        self.hotel_id = int(hotel_id)
        self.ta_loc_id, self.write_url = self.fetch_ta_location_id()

    def build_response_hd(self, ):
        try:
            response = dict()
            ta_review = dict()
            if self.ta_loc_id:
                hd_review = HDReviewData(self.hotel_id, self.ta_loc_id)
                hd_review.build()
                hd_page_serializer = HDPageReviewDTO(hd_review)
                ta_review = hd_page_serializer.data
                ta_review["ta_enabled"] = True
            else:
                logger.error(
                    "Hotel Enabled.. but no mapping in TA Hotel ID Mapping %s", str(
                        self.hotel_id))
                ta_review["ta_enabled"] = False
            response["ta_reviews"] = ta_review
            return response
        except Exception as e:
            logger.exception(
                "Exception in building hd page response for %s", str(
                    self.hotel_id))
            raise e

    def build_response_search(self, ):
        try:
            ta_review = dict()
            response = dict()
            config = self.review_app_config.fetch_app_config()
            if self.ta_loc_id and config:
                search_review = SearchReviewData(self.hotel_id, self.ta_loc_id)
                search_review.build()
                search_page_serializer = SearchPageReviewDTO(search_review)
                ta_review = search_page_serializer.data
                ta_review["ta_enabled"] = True
            else:
                ta_review["ta_enabled"] = False
            response["ta_reviews"] = ta_review
            return response
        except Exception as e:
            logger.exception(
                "Exception in building hd page response for %s", str(
                    self.hotel_id))
            raise e

    def fetch_ta_location_id(self, ):
        hotel_ta_map = HotelTAMappingService()
        mapped_obj = hotel_ta_map.fetch_mapping(hotel_id=self.hotel_id)
        if mapped_obj:
            return mapped_obj.ta_location_id, mapped_obj.write_review_url
        else:
            logger.error(
                "Mapping of hotel id %s and corresponding ta loc id not found", str(
                    self.hotel_id))
            return None, None
