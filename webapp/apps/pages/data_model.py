import logging
from datetime import datetime

from django.conf import settings
from django.core.urlresolvers import reverse

from apps.checkout.models import BookingRequest
from apps.hotels import utils as hotel_utils
from apps.common import date_time_utils

logger = logging.getLogger(__name__)


class QuickbookData(object):
    def __init__(self, *args, **kwargs):
        self.hotel_id = kwargs.get('hotel_id', None)
        self.hotelid = kwargs.get('hotelid', None)
        self.channel = kwargs.get('channel', None)
        redirect = False
        if self.hotelid:
            if not self.hotelid.isdigit():
                self.hashed_hotel_id = self.hotelid
                self.hotel_id = hotel_utils.decode_hash(self.hotelid)
            else:
                self.hotel_id = self.hotelid
                self.hashed_hotel_id = hotel_utils.generate_hash(
                    int(self.hotelid))
            redirect = True
        else:
            if not self.hotel_id.isdigit():
                self.hashed_hotel_id = self.hotel_id
                self.hotel_id = hotel_utils.decode_hash(self.hotel_id)
                redirect = True
            else:
                self.hashed_hotel_id = hotel_utils.generate_hash(
                    int(self.hotel_id))
        self.room_config = kwargs.get("roomconfig")
        self.booking_channel = kwargs.get("channel", BookingRequest.WEBSITE)
        self.room_type = kwargs.get("roomtype").title()
        self.checkin_date_string = kwargs.get("checkin")
        self.checkout_date_string = kwargs.get("checkout")
        self.coupon_code = kwargs.get("couponcode", None)
        self.utm_source = kwargs.get("utm_source", "")
        self.utm_tracking_id = kwargs.get("utm_tracking_id", None)
        if self.utm_source:
            self.utm_source = str(self.utm_source).strip().lower()
        if self.checkin_date_string:
            # TODO: DateUtils common method
            check_in = datetime.strptime(
                self.checkin_date_string,
                date_time_utils.DATE_FORMAT)
            self.checkin_date = check_in.date()
        else:
            self.checkin_date = None
            self.checkout_date_string = kwargs.get("checkout")
        if self.checkout_date_string:
            check_out = datetime.strptime(self.checkout_date_string,
                                          date_time_utils.DATE_FORMAT)
            self.checkout_date = check_out.date()
        else:
            self.checkout_date = None
        if redirect:
            redirect_url = reverse(
                'pages:itinerary') + "?hotel_id={0}&roomconfig={1}&roomtype={2}&checkin={3}&checkout={4}&couponcode={5}".format(
                self.hotel_id, self.room_config, self.room_type, self.checkin_date_string, self.checkout_date_string,
                self.coupon_code)
            bid = kwargs.get('bid', None)
            if bid:
                redirect_url += "bid=" + bid
            self.redirect_url = redirect_url
        else:
            self.redirect_url = None

        self.rate_plan = kwargs.get('rateplan')
        apply_wallet = str(
            kwargs.get(
                'apply_wallet',
                settings.APPLY_WALLET)).lower()
        self.apply_wallet = False
        if apply_wallet == 'true':
            self.apply_wallet = True


class ItineraryPageData(QuickbookData):
    def __init__(self, *args, **kwargs):
        super(ItineraryPageData, self).__init__(*args, **kwargs)
        self.booking_request_id = kwargs.get('bid')
