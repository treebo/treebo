import datetime
from decimal import Decimal
from common.custom_logger.booking.log import get_booking_logger
from apps.bookings.models import Booking
from apps.bookings.serializers import BookingCreationSerializer
from apps.bookings.services.booking import BookingService
from apps.common.exceptions.custom_exception import InitiateBookingValidationError, \
    AxisRoomBookingValidationError, AxisRoomBookingConfirmationError
from apps.bookings.repositories.booking_repository import BookingServiceRepository
from apps.bookings.models import PaymentOrder
from axis_rooms.axis_rooms_booking import AxisRoomBooking
from django.conf import settings
from apps.bookings import constants

logger = get_booking_logger(__name__)


class AxisRoomsBookingService:
    """
        Service layer to create booking in axis room
    """

    def __init__(self):
        pass

    @staticmethod
    def get_amount_collected_and_payment_status(booking, payment_mode, async=True):

        amount_collected = 0

        # need to add the logic for checking the payment from paymentorder table and add here
        if payment_mode == constants.PAYMENT_STATUS_PAY_AT_HOTEL:
            try:
                if booking.wallet_applied:
                    amount_collected += float(booking.wallet_deduction)
                    payment_mode = constants.PAYMENT_STATUS_PARTIAL_PAYMENT
                if booking.payment_mode == Booking.PARTIAL_RESERVE:
                    payment_orders = BookingServiceRepository.get_paymentorders_by_booking_order_id_except_wallet(
                        booking.order_id)
                    amount_collected += sum(float(payment_order.amount) for payment_order in payment_orders if payment_order.is_verified())
                    payment_mode = constants.PAYMENT_STATUS_PARTIAL_PAYMENT
            except PaymentOrder.DoesNotExist:
                message = "Payment order record not found for order id:{order_id} and " \
                          "payment mode:{payment_mode}".format(order_id=booking.order_id,
                                                               payment_mode=payment_mode)
                logger.exception(message)
                AxisRoomBookingConfirmationError(message)
            except Exception as e:
                message = "Exception while calculating the amount collected value for " \
                          "order id:{order_id} payment mode:{payment_mode} for a home stay " \
                          "booking".format(order_id=booking.order_id, payment_mode=payment_mode)
                logger.exception(message)
                raise AxisRoomBookingConfirmationError("{message} due to:{exception}".format(message=message,
                                                                                             exception=str(e)))
        return payment_mode, amount_collected

    @staticmethod
    def call_axis_room_wrapper(**kwargs):

        for key, value in kwargs.items():
            if not value and int(value) != 0:
                raise AxisRoomBookingValidationError("No value present for key %s", str(key))

        date_today = datetime.datetime.now().strftime("%Y-%m-%d")

        if kwargs['checkin'] > kwargs['checkout'] or kwargs['checkin'] < date_today or kwargs['checkout'] < date_today:
            raise AxisRoomBookingValidationError("Invalid checkin or checkout date")

        axis_room_wrapper = AxisRoomBooking(env=settings.ENVIRONMENT)

        hotel_cs_id = kwargs['hotel'].cs_id
        room_config_list = (kwargs['room_config'].strip(',')).split(',')
        total_rooms = len(room_config_list)

        commission = round(float(kwargs['total_amount'] - kwargs['taxes']) * 0.15, 2)
        supplier_amount = round(float(kwargs['total_amount']) - commission, 2)

        external_room_details = kwargs['room'].external_room_details
        rate_id, rate_name = external_room_details.rate_plan_code, external_room_details.rate_plan_name
        room_id = external_room_details.room_code

        response = None
        if kwargs['api'] == 'create_booking':
            response = axis_room_wrapper.create_booking(
                    booking_id=kwargs['booking_id'], hotel_id=hotel_cs_id, total_amount=kwargs['total_amount'],
                    taxes=kwargs['taxes'], supplier_amount=supplier_amount, commission=commission,
                    checkout_date=kwargs['checkout'], checkin_date=kwargs['checkin'], total_adults=kwargs['adults'],
                    total_children=kwargs['children'], guest_name=kwargs['name'], guest_email=kwargs['email'],
                    guest_phone=kwargs['phone'], payment_status=kwargs['payment_status'], rate_id=rate_id,
                    rate_name=rate_name, room_id=room_id, total_rooms=total_rooms, room_config_list=room_config_list,
                    amount_collected=kwargs['amount_collected'])

        elif kwargs['api'] == 'cancel_booking':
            response = axis_room_wrapper.cancel_booking(
                    booking_id=kwargs['booking_id'], hotel_id=hotel_cs_id, total_amount=kwargs['total_amount'],
                    taxes=kwargs['taxes'], supplier_amount=supplier_amount, commission=commission,
                    checkout_date=kwargs['checkout'], checkin_date=kwargs['checkin'], total_adults=kwargs['adults'],
                    total_children=kwargs['children'], guest_name=kwargs['name'], guest_email=kwargs['email'],
                    guest_phone=kwargs['phone'], payment_status=kwargs['payment_status'], rate_id=rate_id,
                    rate_name=rate_name, room_id=room_id, total_rooms=total_rooms, room_config_list=room_config_list,
                    amount_collected=kwargs['amount_collected'])

        return response

    @staticmethod
    def initiate_booking(booking_data, booking_request=None):
        """
        Creates
        :param booking_data:
        :param booking_request:
        :return:
        """
        booking = None
        if Decimal(booking_data.get('total_amount')) == 0:
            return None

        if booking_data.get('order_id'):
            booking = Booking.objects.filter(
                order_id=booking_data.get('order_id')).first()

        # if already a booking object is present
        if booking:
            serializer = BookingCreationSerializer(data=booking_data)
            if serializer.is_valid():
                booking = serializer.update(booking, serializer.validated_data)
                booking_request.order_id = booking.order_id
                booking_request.booking_id = booking.id
                booking_request.save()
                logger.info(
                    "Axis rooms Booking initiated with [id, order_id]=[%s, %s]",
                    booking.id,
                    booking.order_id)
                return booking
            else:
                logger.error(
                    "Error in Axis Rooms Service while updating Booking in initiate_booking: %s",
                    serializer.errors)
                raise InitiateBookingValidationError(message=serializer.errors)
        else:
            # when creating a new booking object
            booking_data["order_id"] = BookingService.generate_order_id()
            serializer = BookingCreationSerializer(data=booking_data)
            if serializer.is_valid():
                booking = serializer.save()
                booking_request.order_id = booking.order_id
                booking_request.booking_id = booking.id
                booking_request.save()
                logger.info(
                    "Axis Rooms Booking initiated with [id, order_id]=[%s, %s]",
                    booking.id,
                    booking.order_id)
                return booking
            else:
                logger.error(
                    "Error in Axis Room api while creating Booking in initiate_booking: %s",
                    serializer.errors)
                raise InitiateBookingValidationError(message=serializer.errors)

    @staticmethod
    def confirm_booking(booking):
        """
        confirm booking (calling axis rooms lib)
        :param booking:
        :return:
        """
        logger.info("confirm_booking method called with order_id: %s", booking.order_id)
        payment_mode = constants.PAYMENT_STATUS_PAY_AT_HOTEL if \
            booking.payment_mode == booking.PARTIAL_RESERVE else constants.PAYMENT_STATUS_PREPAID
        payment_status, amount_collected = AxisRoomsBookingService.get_amount_collected_and_payment_status(
                                                        booking=booking, payment_mode=payment_mode)

        response = AxisRoomsBookingService.call_axis_room_wrapper(amount_collected=amount_collected,
                        api="create_booking", total_amount=booking.total_amount, taxes=booking.tax_amount,
                        name=booking.guest_name, email=booking.guest_email, phone=booking.guest_mobile,
                        checkin=str(booking.checkin_date), checkout=str(booking.checkout_date), adults=booking.adult_count,
                        children=booking.child_count, room_config=booking.room_config, payment_status=payment_status,
                        hotel=booking.hotel, room=booking.room, booking_id=booking.order_id)

        logger.info("Axis room booking response message %s", response.message)

        booking.booking_status = booking.CONFIRM
        booking.save()

        return booking

    @staticmethod
    def cancel_booking(booking):

        if booking.payment_mode == booking.NOT_PAID or booking.payment_mode == booking.PARTIAL_RESERVE:
            payment_status, amount_collected = AxisRoomsBookingService.get_amount_collected_and_payment_status(
                                                    booking=booking, payment_mode=constants.PAYMENT_STATUS_PAY_AT_HOTEL)
        else:
            payment_status, amount_collected = AxisRoomsBookingService.get_amount_collected_and_payment_status(
                                                    booking=booking, payment_mode=constants.PAYMENT_STATUS_PREPAID)

        AxisRoomsBookingService.call_axis_room_wrapper(amount_collected=amount_collected,
                            api="cancel_booking", total_amount=booking.total_amount, taxes=booking.tax_amount,
                            name=booking.guest_name, email=booking.guest_email, phone=booking.guest_mobile,
                            checkin=str(booking.checkin_date), checkout=str(booking.checkout_date),
                            adults=booking.adult_count, children=booking.child_count, room_config=booking.room_config,
                            payment_status=payment_status, hotel=booking.hotel, room=booking.room,
                            booking_id=booking.order_id)

        booking.booking_status = booking.CANCEL
        booking.save()

        return True

    @staticmethod
    def create_booking(booking_data, booking_request=None):
        """
        Creates a booking in axis room
        :rtype: Booking
        """
        booking = None

        if Decimal(booking_data.get('total_amount')) == 0:
            return constants.INVALID_BOOKING_AMOUNT, None

        if booking_data.get('order_id'):
            booking = Booking.objects.filter(
                order_id=booking_data.get('order_id')).first()

        # if booking object already present
        if booking:
            return constants.BOOKING_ALREADY_PROCESSED, None

        # When creating new booking object
        else:
            booking_data["order_id"] = BookingService.generate_order_id()
            booking_data['booking_status'] = Booking.TEMP_BOOKING_CREATED
            logger.info(
                "Creating homestay new Booking with order id %s ",
                booking_data["order_id"])
            serializer = BookingCreationSerializer(data=booking_data)
            if serializer.is_valid():
                booking = serializer.save()
                booking_request.order_id = booking.order_id
                booking_request.booking_id = booking.id
                booking_request.save()

                payment_status, amount_collected = AxisRoomsBookingService.get_amount_collected_and_payment_status(
                                                booking=booking, payment_mode=constants.PAYMENT_STATUS_PAY_AT_HOTEL)

                response = AxisRoomsBookingService.call_axis_room_wrapper(amount_collected=amount_collected,
                            api="create_booking", total_amount=booking.total_amount, taxes=booking.tax_amount,
                            name=booking.guest_name, email=booking.guest_email, phone=booking.guest_mobile,
                            checkin=str(booking.checkin_date), checkout=str(booking.checkout_date),
                            adults=booking.adult_count, children=booking.child_count, room_config=booking.room_config,
                            payment_status=payment_status, hotel=booking.hotel, room=booking.room, booking_id=booking.order_id)

                logger.info("Axis room booking response message %s", response.message)
                booking.booking_status = booking.CONFIRM
                booking.save()

                return booking
            logger.error(
                "Error in Axis Room api while creating Booking in create_booking: %s",
                serializer.errors)
