import json
import logging

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.response import Response
from apps.content.models import ContentStore
from apps.featuregate.feature_manager_service import FeatureManagerService
from apps.featuregate.serializer.feature_manager_dto import FeatureManagerDTO, FeatureManagerSerializer
from base import log_args
from base.views.api import TreeboAPIView
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class ContentAPI(TreeboAPIView):
    feature_manager_service = FeatureManagerService()

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ContentAPI, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request):
        context = {}
        try:
            feature_manager_serializer = FeatureManagerSerializer(
                data=request.GET)
        except Exception as exc:
            logger.exception(exc)
            context = {
                "status": "error",
                "msg": "Please send all key variables"}
            return Response(context, status=status.HTTP_400_BAD_REQUEST)
        if not feature_manager_serializer.is_valid():
            return Response(
                feature_manager_serializer.errors,
                status=status.HTTP_400_BAD_REQUEST)

        feature_manager_dto = feature_manager_serializer.save()
        feature_manager_data = self.feature_manager_service.get_overridden_values(
            feature_manager_dto)

        keys = str(request.GET['keys']).split(',')

        try:
            content_keys = []
            for key in keys:
                key = key.strip()
                if feature_manager_data and key in feature_manager_data:
                    content_keys.append(str(feature_manager_data[key]))
                else:
                    logger.debug("No overridden values found for key %s", key)
                    content_keys.append(key)

            contents = ContentStore.objects.filter(
                name__in=content_keys, version=1)

            for content in contents:
                context.update({
                    content.name: json.loads(content.value)
                })

            return Response(context)
        except Exception as exc:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        message=exc.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception(exc.__str__)
            context = {
                "status": "error",
                "msg": "Unable to fetch info from content store"}
            return Response(context, status=500)

    @log_args(logger)
    def post(self, request, format=None):
        raise Exception('Method not supported')
