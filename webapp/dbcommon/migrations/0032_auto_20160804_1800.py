# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0031_auto_20160804_1618'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='amenity',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Amenity', 'verbose_name_plural': 'Amenities'},
        ),
        migrations.AlterModelOptions(
            name='amenitymembership',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Amenity Membership', 'verbose_name_plural': 'Amenity Memberships'},
        ),
        migrations.AlterModelOptions(
            name='apkdetails',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='city',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'City', 'verbose_name_plural': 'Cities'},
        ),
        migrations.AlterModelOptions(
            name='cityalias',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'City Alias', 'verbose_name_plural': 'City Aliases'},
        ),
        migrations.AlterModelOptions(
            name='cityblog',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'City Blog', 'verbose_name_plural': 'City Blogs'},
        ),
        migrations.AlterModelOptions(
            name='cityseodetails',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'City SEO Details', 'verbose_name_plural': 'City SEO Details'},
        ),
        migrations.AlterModelOptions(
            name='corporate',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='coupon',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Coupon', 'verbose_name_plural': 'Coupons'},
        ),
        migrations.AlterModelOptions(
            name='coupon_constraint',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Coupon Constraint', 'verbose_name_plural': 'Coupon Constraints'},
        ),
        migrations.AlterModelOptions(
            name='coupon_date_constraint',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Coupon Date Constraint', 'verbose_name_plural': 'Coupon Date Constraints'},
        ),
        migrations.AlterModelOptions(
            name='directions',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Direction', 'verbose_name_plural': 'Directions'},
        ),
        migrations.AlterModelOptions(
            name='emaillinkhashvalue',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Email Link HashValue', 'verbose_name_plural': 'Email Link HashValues'},
        ),
        migrations.AlterModelOptions(
            name='facility',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Facility', 'verbose_name_plural': 'Facilities'},
        ),
        migrations.AlterModelOptions(
            name='feedback',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Feedback', 'verbose_name_plural': 'Feedbacks'},
        ),
        migrations.AlterModelOptions(
            name='highlight',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Highlight', 'verbose_name_plural': 'Highlights'},
        ),
        migrations.AlterModelOptions(
            name='hotel',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Hotel', 'verbose_name_plural': 'Hotels'},
        ),
        migrations.AlterModelOptions(
            name='hotelimage',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='image',
            options={'ordering': ['-is_showcased', 'position', 'hotel__name', 'room__room_type'], 'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name_plural': 'Images'},
        ),
        migrations.AlterModelOptions(
            name='landmark',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Landmark', 'verbose_name_plural': 'Landmarks'},
        ),
        migrations.AlterModelOptions(
            name='landmarktype',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Landmark Type', 'verbose_name_plural': 'Landmark Types'},
        ),
        migrations.AlterModelOptions(
            name='locality',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Locality', 'verbose_name_plural': 'Localities'},
        ),
        migrations.AlterModelOptions(
            name='notificationarchive',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Notification Archive', 'verbose_name_plural': 'Notification Archives'},
        ),
        migrations.AlterModelOptions(
            name='notifynewlocation',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'Notify New Location', 'verbose_name_plural': 'Notify New Location'},
        ),
        migrations.AlterModelOptions(
            name='otp',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='partner',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='partnerinfo',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='room',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='seodetails',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'SEO Details', 'verbose_name_plural': 'SEO Details'},
        ),
        migrations.AlterModelOptions(
            name='state',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'State', 'verbose_name_plural': 'States'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'user', 'verbose_name_plural': 'users'},
        ),
        migrations.AlterModelOptions(
            name='userdiscount',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'User Discount', 'verbose_name_plural': 'User Discounts'},
        ),
        migrations.AlterModelOptions(
            name='useremail',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='usermetadata',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'User Metadata', 'verbose_name_plural': 'User Metadata'},
        ),
        migrations.AlterModelOptions(
            name='userotp',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'User OTP', 'verbose_name_plural': 'User OTPs'},
        ),
        migrations.AlterModelOptions(
            name='userreferral',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='userreferralreward',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
    ]
