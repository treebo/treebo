# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.discounts.api.coupon import CreateCouponAPI
from . import views

urlpatterns = [
    url(r"^$", views.CreateDiscount.as_view(), name='create-discount'),
    url(r"^constraint/$", views.CreateConstraint.as_view(), name='create-constraint'),
    url(r"^update/(?P<pk>[0-9]+)/$", views.UpdateDiscount.as_view(), name='update-discount'),
    url(r"^bulkdiscounts/$", views.CreateBulkDiscounts.as_view(), name='create-bulk-discounts'),
    url(r"^featured/$", views.AvailableCoupon.as_view(), name='available-discounts'),
    url(r"^applicablediscounts/$", views.ApplicableDiscounts.as_view(),
        name='applicable-discounts'),
    url(r"^prioritydiscount/$", views.PriorityDiscount.as_view(), name='priority-discount'),
    url(r"^checkapplicability/$", views.CheckApplicability.as_view(), name='check-applicability'),
    url(r"^applydiscount/$", views.ApplyDiscount.as_view(), name='apply-discount'),
    url(r"^autopromo/$", views.AutoPromo.as_view(), name='apply-discount'),
    url(r"^createcoupon/$", CreateCouponAPI.as_view(), name='create-coupon'),
]
