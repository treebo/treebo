# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('dbcommon', '0029_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(
                related_query_name='user',
                related_name='user_set',
                to='auth.Group',
                blank=True,
                help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.',
                verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='is_superuser',
            field=models.BooleanField(
                default=False,
                help_text='Designates that this user has all permissions without explicitly assigning them.',
                verbose_name='superuser status'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(
                related_query_name='user',
                related_name='user_set',
                to='auth.Permission',
                blank=True,
                help_text='Specific permissions for this user.',
                verbose_name='user permissions'),
        ),
        migrations.AlterField(
            model_name='user',
            name='city',
            field=models.CharField(
                default='',
                max_length=200,
                null=True,
                blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='dob',
            field=models.DateField(
                null=True,
                verbose_name='dob',
                blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(
                max_length=254,
                verbose_name='First Name'),
        ),
        migrations.AlterField(
            model_name='user',
            name='last_name',
            field=models.CharField(
                default='',
                max_length=254,
                verbose_name='Last Name',
                blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='middle_name',
            field=models.CharField(
                default='',
                max_length=20,
                verbose_name='Middle Name',
                blank=True),
        ),
    ]
