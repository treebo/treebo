#!/bin/bash
set -x
export ENV=$1
export VERSION=$2
export APP=$3
export CONFIG_BRANCH=$4
export BUILD_ITEM=$5
export ENV_CONFIG_FOR_BUILD_ITEM=$6

if [ "$ENV" == "dev" ]; then
    ENV_CONFIG_FOR_BUILD_ITEM=$1
fi

source /opt/$APP/docker/docker_env/base.env
source /opt/$APP/docker/docker_env/$ENV_CONFIG_FOR_BUILD_ITEM.env

echo "config loaded from file $CONFIG_MOUNT_PATH"

if [ ! -d "$HOST_LOG_ROOT" ]; then
    echo "Creating HOST_LOG_ROOT directory at: $HOST_LOG_ROOT"
    mkdir -p $HOST_LOG_ROOT
fi

if [ ! -d "$HOST_STATIC_PIPELINE" ]; then
    echo "Creating HOST_STATIC_PIPELINE directory at: $HOST_STATIC_PIPELINE"
    mkdir -p $HOST_STATIC_PIPELINE
fi

if [ ! -d "$HOST_STATIC_ROOT" ]; then
    echo "Creating HOST_STATIC_ROOT directory at: $HOST_STATIC_ROOT"
    mkdir -p $HOST_STATIC_ROOT
fi

if [ "$ENV" == "dev" ]; then
    sudo chown -R conman:conman /opt/$APP
    sudo chmod -R 755 /opt/$APP

    rm -rf /opt/$APP/direct_config
    rm -rf /opt/$APP/dbdump

    mkdir /opt/$APP/direct_config
    mkdir /opt/$APP/dbdump
    sudo s3cmd get s3://treebo-db-dump/web-full-dump/devdb_masking.sql /opt/$APP/dbdump --region=ap-south-1 --skip-existing

    git clone git@bitbucket.org:treebo/direct_config.git --branch $CONFIG_BRANCH /opt/$APP/direct_config

    sudo s3cmd get s3://treebo-db-dump/web-full-dump/treebo.dump /opt/$APP/dbdump --region=ap-south-1 --skip-existing
    sudo pg_restore --no-owner --no-acl -f /opt/$APP/dbdump/backup.sql /opt/$APP/dbdump/treebo.dump

    export DB_SETUP=/opt/$APP/dbdump/
    export HOST_NAME="$(hostname).treebo.cc"
    echo "HOST_NAME: $HOST_NAME"

    lsof -i :80 | awk '{print $2}' | grep -v 'PID' | xargs -I {} kill -9 {}

    docker-compose --verbose -f /opt/$APP/docker/compose/docker-dev.yml up --build -d

elif [ "$ENV_CONFIG_FOR_BUILD_ITEM" == "production"  ] || [ "$ENV_CONFIG_FOR_BUILD_ITEM" == "gdc"  ]; then
     file_path=/opt/$APP/docker/compose/docker-production-${BUILD_ITEM:-backend}.yml
     docker rm -f nginx-gdc-production_new
     docker-compose --verbose -f ${file_path} up --build -d
     sleep 5
#     docker cp app_direct_production_treebobackend:/usr/src/app/webapp/scripts/mobile /tmp/
#     docker cp app_direct_production_treebobackend:/usr/src/app/webapp/scripts/desktop /tmp/
#     docker cp /tmp/mobile  app_direct_production_treebobackend:/usr/src/app/webapp/dist
#     docker cp /tmp/desktop  app_direct_production_treebobackend:/usr/src/app/webapp/dist
#     rm -rf /tmp/mobile
#     rm -rf /tmp/desktop
     docker restart app_direct_production_treebobackend

elif [ "$ENV_CONFIG_FOR_BUILD_ITEM" == "tools" ]; then
     file_path=/opt/$APP/docker/compose/docker-production-${BUILD_ITEM:-backend}.yml
     docker rm -f nginx-tools-production-new
     docker-compose --verbose -f ${file_path} up --build -d

elif [ "$ENV_CONFIG_FOR_BUILD_ITEM" == "tools-staging" ]; then
     file_path=/opt/$APP/docker/compose/docker-staging-${BUILD_ITEM:-backend}.yml
     docker rm -f nginx-tools-staging-new
     docker-compose --verbose -f ${file_path} up --build -d

else
     lsof -i :80 | awk '{print $2}' | grep -v 'PID' | xargs -I {} kill -9 {}
     docker-compose --verbose -f /opt/$APP/docker/compose/docker-staging.yml -p treebo_web_backend up --build -d
     sleep 5
#     docker cp app_direct_staging_treebo:/usr/src/app/webapp/scripts/mobile /tmp/
#     docker cp app_direct_staging_treebo:/usr/src/app/webapp/scripts/desktop /tmp/
#     docker cp /tmp/mobile  app_direct_staging_treebo:/usr/src/app/webapp/dist
#     docker cp /tmp/desktop  app_direct_staging_treebo:/usr/src/app/webapp/dist
#     rm -rf /tmp/mobile
#     rm -rf /tmp/desktop
     docker restart app_direct_staging_treebo
fi

