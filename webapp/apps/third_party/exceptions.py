
from common.exceptions.treebo_exception import TreeboException


class HotelFeedDataError(TreeboException):
    def __init__(self, *args):
        super(HotelFeedDataError, self).__init__(*args)


class RoomDataParseError(TreeboException):
    def __init__(self, *args):
        super(RoomDataParseError, self).__init__(*args)

class TrivagoCsvDataGenerationError(Exception):
    def __init__(self, message):
        super(TrivagoCsvDataGenerationError, self).__init__(message)


class IxigoCsvDataGenerationError(Exception):
    def __init__(self, message):
        super(IxigoCsvDataGenerationError, self).__init__(message)
