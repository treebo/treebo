import json
import logging

from django.conf import settings
from ipware.ip import get_ip
from requests.exceptions import HTTPError
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.bookingstash.service.availability_service import get_availability_service
from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from apps.checkout.service import checkout_service
from apps.checkout.service.checkout_service import save_booking_state, sanitise_data
from apps.common import error_codes
from apps.common.slack_alert import SlackAlertService
from apps.discounts.exceptions import InvalidCouponException
from apps.payments import payment_gateway
from common.services.feature_toggle_api import FeatureToggleAPI
from base.logging_decorator import log_args
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from services.restclient.bookingrestclient import BookingClient
from services.restclient.paymentclient import PaymentClient

logger = logging.getLogger(__name__)


class PayNow(TreeboAPIView):
    payment_client = PaymentClient()
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)

    def get(self, request, *args, **kwargs):

        booking_data = request.GET
        bid = booking_data.get('bid')

        try:
            is_available = self.__check_availability(bid)
        except BookingRequest.DoesNotExist:
            context = {'available': False, 'error': {
                'msg': 'Booking Request Does not exist',
                'code': "5005"
            }}
            return Response(context, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            context = {'available': False, 'error': {
                'msg': str(e),
                'code': "5001"
            }}
            SlackAlertService.web_alert("Error in availability : " + str(bid))
            return Response(context, status=HTTP_500_INTERNAL_SERVER_ERROR)

        if not is_available:
            context = {'available': is_available, 'error': {
                'msg': 'Room is sold out',
                'code': "5001"
            }}
            SlackAlertService.web_alert(
                "Room is sold out for bid : " + str(bid))
            return Response(context, status=status.HTTP_400_BAD_REQUEST)
        logger.debug("PayNow: Post request made")
        try:
            if FeatureToggleAPI.is_enabled("booking", "instant_booking", True):
                booking_request = self.__freeze_booking_request(
                    booking_data, request, True)
            else:
                booking_request = self.__freeze_booking_request(
                    booking_data, request, False)
        except InvalidCouponException as exc:
            logger.exception(exc)
            context = {'coupon_applicable': False, 'error': {
                'msg': str(exc),
                'code': "5004"
            }}
            return Response(context, status=status.HTTP_400_BAD_REQUEST)
        amount_to_send = int(booking_request.total_amount * 100)
        try:
            payment_mode = settings.DEFAULT_PAYMENT_GATEWAY
            payment_details = self.payment_client.generate_payment_details(
                amount_to_send / 100.00, bid, booking_request.hotel_id, payment_mode)
        except HTTPError as e:
            context = {'error': {
                'msg': 'Unable to generate payment details.',
                'code': "5004"
            }}
            return Response(context, status=status.HTTP_400_BAD_REQUEST)

        try:
            if booking_request:
                order_payment_details = {
                    'amount_to_store': round(booking_request.total_amount),
                    'razorpay_key': settings.RAZORPAY_KEYS,
                    'amount_to_send': amount_to_send,
                    "pg_order_id": payment_details['pg_order_id'],
                    "ps_order_id": payment_details['ps_order_id']
                }
                return Response(order_payment_details)
            else:
                logger.error("Unable to initiate booking")
                return Response({
                    "error": {
                        "msg": "Unable to initiate booking",
                        "code": "5002"
                    }
                }, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            logger.exception("Unable to initiate booking")
            return Response({
                "error": {
                    "msg": "Unable to initiate booking",
                    "code": "5002"
                }
            }, status=status.HTTP_400_BAD_REQUEST)

    def __freeze_booking_request(self, booking_data, request, instant_booking):

        bid = booking_data.get('bid')
        logger.info(
            "__freeze_booking_request for pay@hotel %s ",
            json.dumps(booking_data))

        booking_payload = {
            "bid": bid,
            # Hardcoding value 0, TODO: make a map for paid and unpaid nad set
            # the value for it
            "pay_at_hotel": '0',
            "name": booking_data.get('name', ''),
            "email": booking_data.get('email', ''),
            "phone": booking_data.get('mobile', ''),
            "organization_name": booking_data.get('organization_name', ''),
            "organization_address": booking_data.get('organization_address', ''),
            "organization_taxcode": booking_data.get('organization_taxcode', ''),
            "user_signup_channel": request.COOKIES.get("user_signup_channel"),
            "message": booking_data.get('message', ""),
            "check_in_time": booking_data.get('check_in_time'),
            "check_out_time": booking_data.get('check_out_time'),
            "user": request.user,
            "instant_booking": instant_booking,
            'http_host': request.META['HTTP_HOST'],
            "payment_mode": "Paid",
            "utm_source": str(request.COOKIES.get('utm_source')) if request.COOKIES.get('utm_source') else "",
            "utm_medium": str(request.COOKIES.get('utm_medium')) if request.COOKIES.get('utm_medium') else "",
            "referral_click_id": str(request.COOKIES.get('avclk')) if request.COOKIES.get('avclk') else "",
            "utm_campaign": str(request.COOKIES.get('utm_campaign')) if request.COOKIES.get('utm_campaign') else "",
            "host_ip_address": str(get_ip(request)) if get_ip(request) else ""
        }

        logger.info(
            "Booking Payload for freezing booking request: %s",
            booking_payload)
        booking_request = save_booking_state(booking_payload)
        return booking_request

    def __get_payment_gateway_payload(
            self,
            booking_request,
            is_secure,
            payment_gateway_name):
        pg_object = payment_gateway.pg_factory.get(payment_gateway_name)
        if pg_object:
            payment_gateway_payload = pg_object.build_pg_payload(
                booking_request, is_secure=is_secure)
            return payment_gateway_payload
        raise TreeboException()

    def __booking_handler(self, is_secure, booking_request, payment_gateway):
        '''

        :param is_secure: For https connections
        :param booking_request: booking request object
        :param payment_gateway: Payment gateway name
        :return: Map of payment gateway details and order id
        '''
        booking_request.comments = checkout_service.build_special_preference(
            booking_request)
        booking_request.save()
        booking = BookingClient.initiate_booking(booking_request)
        booking_request.status = BookingRequest.PAYMENT_IN_PROGRESS
        booking_request.order_id = booking["order_id"]
        booking_request.booking_id = booking["id"]
        booking_request.save()
        payment_booking_details = {}
        payment_booking_details.update({
            "order_id": booking_request.order_id,
            'email': booking_request.email,
            'name': booking_request.name,
            'contact': booking_request.phone,
            'bid': booking_request.id
        })
        return payment_booking_details

    def __check_availability(self, bid):
        booking_request = BookingRequest.objects.get(id=bid)
        available_rooms = get_availability_service().get_available_rooms(
            booking_request.hotel_id,
            booking_request.checkin_date,
            booking_request.checkout_date,
            booking_request.room_config)
        for room, availability in list(available_rooms.items()):
            if booking_request.room_type.lower(
            ) == room.room_type_code.lower() and availability > 0:
                return True
        return False

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        booking_data = request.data
        booking_data = sanitise_data(booking_data)
        bid = booking_data.get('bid')
        channel = booking_data.get('channel', 'desktop')
        # FIXME: Move to DB
        payment_gateway = settings.DEFAULT_PAYMENT_GATEWAY

        is_secure = request.is_secure()
        if_available = self.__check_availability(bid)

        if not if_available:
            context = {'available': if_available, 'error': {
                'msg': 'Room is sold out',
                'code': "5001"
            }}
            SlackAlertService.web_alert(
                "Room is sold out for bid: " + str(bid))
            return Response(context, status=status.HTTP_400_BAD_REQUEST)
        logger.debug("PayNow: Post request made")
        organization_taxcode = request.data.get('organization_taxcode', None)
        if not BookingHelper.validate_gstin_for_booking(
                organization_taxcode, bid=bid, api_call='paynow post v3'):
            return Response({"error": {
                "msg": error_codes.get(error_codes.INVALID_GSTIN_NUMBER)['msg'],
                "code": error_codes.get(error_codes.INVALID_GSTIN_NUMBER)['code']
            }
            }, status=HTTP_400_BAD_REQUEST)
        try:
            if FeatureToggleAPI.is_enabled("booking", "instant_booking", True):
                booking_request = self.__freeze_booking_request(
                    booking_data, request, True)
            else:
                booking_request = self.__freeze_booking_request(
                    booking_data, request, False)
        except InvalidCouponException as exc:
            context = {'coupon_applicable': False, 'error': {
                'msg': str(exc),
                'code': "5004"
            }}
            SlackAlertService.web_alert(str(exc) + " for bid : " + str(bid))
            return Response(context, status=status.HTTP_400_BAD_REQUEST)
        try:
            order_payment_details = self.__booking_handler(
                is_secure, booking_request, payment_gateway)

            return Response({
                "order_id": order_payment_details.get('order_id'),
                "contact": order_payment_details.get('contact'),
                "email": order_payment_details.get('email'),
            })
        except TreeboException as te:
            logger.exception("Unsupported payment gateway")
            return Response({
                "error": {
                    "msg": "Unsupported payment gateway",
                    "code": "5003"
                }
            }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.exception("Unable to initiate booking")
            return Response({
                "error": {
                    "msg": "Unable to initiate booking",
                    "code": "5002"
                }
            }, status=status.HTTP_400_BAD_REQUEST)
