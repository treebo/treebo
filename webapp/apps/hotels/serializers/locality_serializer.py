from rest_framework import serializers

from dbcommon.models.location import Locality


class LocalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Locality
        fields = '__all__'


class LocalityAPISerializer(serializers.ModelSerializer):
    coordinates = serializers.SerializerMethodField()

    def get_coordinates(self, locality):
        return {
            'lat': locality.latitude,
            'lng': locality.longitude
        }

    class Meta:
        model = Locality
        fields = ('name', 'city', 'coordinates')
