import _ from 'underscore';
var $ = require('jquery');
import clientAnalytics from './clientanalytics';

let BaseView = {
    setup : function(options){
        this.eventConfig = options.eventConfig || {};
        this.analyticsConfigfunc = options.analyticsConfigfunc;
        this.name = options.name || '';
        this.widgets = options.widgets;
        this.selecter = options.selecter;
        this.init();
        this.loaded();
    },
    init(){
        var self = this;
        _.each(this.widgets,function(w){
            _.extend(self, w.methods);
        });
    },
    loaded(){
        _.each(this.widgets,w => {
            this[w.name+'Loaded']();
        });
        this.pageLoaded && this.pageLoaded();
        var self = this;
        $(window).load(function(){
            var analyticsObj = clientAnalytics();
            var analyticsConfig = self.analyticsConfigfunc ? self.analyticsConfigfunc() : {};
            var eventName = self.name ? `${self.name} Viewed` : '';
            if (eventName) {
              analyticsObj.sendAnalyticsEvents(eventName, _.extend(analyticsConfig, {nonInteraction: 1}));
            }
        });

    },
    toString(){
        console.log("View Name -: ",this.name);
    }
}

module.exports = BaseView;
