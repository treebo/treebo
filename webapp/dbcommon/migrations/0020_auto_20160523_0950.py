# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0019_auto_20160518_1305'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='amenity',
            options={'verbose_name': 'Amenity', 'verbose_name_plural': 'Amenities'},
        ),
        migrations.AlterModelOptions(
            name='amenitymembership',
            options={'verbose_name': 'Amenity Membership',
                     'verbose_name_plural': 'Amenity Memberships'},
        ),
        migrations.AlterModelOptions(
            name='city',
            options={'verbose_name': 'City', 'verbose_name_plural': 'Cities'},
        ),
        migrations.AlterModelOptions(
            name='cityalias',
            options={'verbose_name': 'City Alias', 'verbose_name_plural': 'City Aliases'},
        ),
        migrations.AlterModelOptions(
            name='cityblog',
            options={'verbose_name': 'City Blog', 'verbose_name_plural': 'City Blogs'},
        ),
        migrations.AlterModelOptions(
            name='cityseodetails',
            options={'verbose_name': 'City SEO Details', 'verbose_name_plural': 'City SEO Details'},
        ),
        migrations.AlterModelOptions(
            name='coupon',
            options={'verbose_name': 'Coupon', 'verbose_name_plural': 'Coupons'},
        ),
        migrations.AlterModelOptions(
            name='coupon_constraint',
            options={'verbose_name': 'Coupon Constraint',
                     'verbose_name_plural': 'Coupon Constraints'},
        ),
        migrations.AlterModelOptions(
            name='coupon_date_constraint',
            options={'verbose_name': 'Coupon Date Constraint',
                     'verbose_name_plural': 'Coupon Date Constraints'},
        ),
        migrations.AlterModelOptions(
            name='directions',
            options={'verbose_name': 'Direction', 'verbose_name_plural': 'Directions'},
        ),
        migrations.AlterModelOptions(
            name='emaillinkhashvalue',
            options={'verbose_name': 'Email Link HashValue',
                     'verbose_name_plural': 'Email Link HashValues'},
        ),
        migrations.AlterModelOptions(
            name='facility',
            options={'verbose_name': 'Facility', 'verbose_name_plural': 'Facilities'},
        ),
        migrations.AlterModelOptions(
            name='feedback',
            options={'verbose_name': 'Feedback', 'verbose_name_plural': 'Feedbacks'},
        ),
        migrations.AlterModelOptions(
            name='highlight',
            options={'verbose_name': 'Highlight', 'verbose_name_plural': 'Highlights'},
        ),
        migrations.AlterModelOptions(
            name='hotel',
            options={'verbose_name': 'Hotel', 'verbose_name_plural': 'Hotels'},
        ),
        migrations.AlterModelOptions(
            name='landmark',
            options={'verbose_name': 'Landmark', 'verbose_name_plural': 'Landmarks'},
        ),
        migrations.AlterModelOptions(
            name='landmarktype',
            options={'verbose_name': 'Landmark Type', 'verbose_name_plural': 'Landmark Types'},
        ),
        migrations.AlterModelOptions(
            name='locality',
            options={'verbose_name': 'Locality', 'verbose_name_plural': 'Localities'},
        ),
        migrations.AlterModelOptions(
            name='notificationarchive',
            options={'verbose_name': 'Notification Archive',
                     'verbose_name_plural': 'Notification Archives'},
        ),
        migrations.AlterModelOptions(
            name='notifynewlocation',
            options={'verbose_name': 'Notify New Location',
                     'verbose_name_plural': 'Notify New Location'},
        ),
        migrations.AlterModelOptions(
            name='seodetails',
            options={'verbose_name': 'SEO Details', 'verbose_name_plural': 'SEO Details'},
        ),
        migrations.AlterModelOptions(
            name='state',
            options={'verbose_name': 'State', 'verbose_name_plural': 'States'},
        ),
        migrations.AlterModelOptions(
            name='userdiscount',
            options={'verbose_name': 'User Discount', 'verbose_name_plural': 'User Discounts'},
        ),
        migrations.AlterModelOptions(
            name='usermetadata',
            options={'verbose_name': 'User Metadata', 'verbose_name_plural': 'User Metadata'},
        ),
        migrations.AlterModelOptions(
            name='userotp',
            options={'verbose_name': 'User OTP', 'verbose_name_plural': 'User OTPs'},
        ),
    ]
