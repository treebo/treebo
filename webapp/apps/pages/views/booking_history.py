import logging
import requests
from django.http import HttpResponseRedirect
from apps.bookings.services.booking_history_service import BookingHistoryService
from apps.common.json_encoder import DateTimeEncoder
from base.views.template import TreeboTemplateView
import json

__author__ = 'pramod'

logger = logging.getLogger(__name__)


class BookingHistory(TreeboTemplateView):
    template_name = "account/index.html"
    booking_history_service = BookingHistoryService()

    def getData(self, request, args, kwargs):
        user = request.user
        if user is None or user.id is None:
            return HttpResponseRedirect('/')
        booking_dict, profile, cities, google_maps = self.booking_history_service.get_booking_history(
            user)
        context = {
            'context': json.dumps(booking_dict, cls=DateTimeEncoder),
            'profile': json.dumps(profile),
            'cities': cities,
            'googleMaps': google_maps,
            'scripts': ['desktop/js/account.js'],
            'styles': ['desktop/css/account.css']

        }
        return context
