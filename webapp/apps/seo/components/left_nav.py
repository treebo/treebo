from apps.seo.components.base_component import BaseComponent
from apps.seo.components.landmarks import Landmarks
from apps.seo.components.localities import Localities
from apps.seo.components.near_by_locality import NearbyLocalities
from apps.seo.components.nearby_landmarks import NearbyLandmarks
import random

__author__ = 'ansilkareem'


class LeftNav(BaseComponent):

    def __init__(self, current_city, footer_data, exclude_list):
        super(LeftNav, self).__init__(current_city, footer_data, exclude_list)

    def build(self):
        localities = NearbyLocalities(self.current_city, self.footer_data, self.current_city.city_latitude,
                                      self.current_city.city_longitude)
        localities = localities.build()
        landmarks = NearbyLandmarks(self.current_city, self.footer_data, self.current_city.city_latitude,
                                    self.current_city.city_longitude)
        landmarks = landmarks.build()
        left_nav_items = localities['content'] + landmarks['content']
        random.shuffle(left_nav_items)
        return {
            'left_nav': {
                'title': '%s HOTELS BY LOCATION' % self.current_city.slug.upper() if
                self.current_city.slug else self.current_city.name.upper(),
                'content': left_nav_items
            }
        } if left_nav_items else{
            'left_nav': {
            }
        }
