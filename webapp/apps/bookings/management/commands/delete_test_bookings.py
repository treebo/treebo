import datetime
import hmac
import logging
import operator
import sys
import traceback
from hashlib import sha1

import requests
from bs4 import BeautifulSoup
from django.conf import settings
from django.core.mail import EmailMessage
from django.core.management import BaseCommand
from django.db.models import Q
from django.template import loader, Context

from apps.bookings.models import Booking, RoomBooking
from functools import reduce

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Delete test bookings in Treebo and Hotelogix'
    consumer_key = settings.CONSUMER_KEY
    consumer_secret = settings.CONSUMER_SECRET
    hotelogix_url = settings.HOTELOGIX_URL
    bookingdetails_url = settings.BOOKING_DETAILS_URL
    sender_email = 'kulizadev@gmail.com'
    recipients = [
        'vinay.misra@treebohotels.com',
        'amith.soman@treebohotels.com']
    username_list = ['webtesting', 'web', 'testing']
    email_list = ['@kuliza.com']
    username = 'kulizadev@gmail.com'
    password = 'kuliza123'

    def handle(self, *args, **options):
        try:
            self.delete_bookings()
        except Exception as exc:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            self.send_exception_email(
                "Exception raised in delete test bookings", repr(
                    traceback.format_exception(
                        exc_type, exc_obj, exc_tb)))
            logger.exception(
                "delete_bookings %s" %
                repr(
                    traceback.format_exception(
                        exc_type,
                        exc_obj,
                        exc_tb)))

    def delete_bookings(self):
        bookings = ""
        try:
            date_min = datetime.datetime.combine(
                datetime.date.today(), datetime.time.min)
            date_max = datetime.datetime.combine(
                datetime.date.today() +
                datetime.timedelta(
                    days=9),
                datetime.time.max)
            bookings = Booking.objects.filter(
                Q(checkin_date__range=(date_min, date_max)), Q(status=Booking.CONFIRM),
                reduce(operator.or_,
                       (Q(guest_name__icontains=x) for x in Command.username_list)) | Q(
                    guest_email__in=Command.email_list)
            )
        except Booking.DoesNotExist:
            logger.exception("There are no bookings to cancel for today")
            exit()
        except Exception as exc:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            self.build_error("get bookings from DB",
                             repr(traceback.format_exception(exc_type, exc_obj,
                                                             exc_tb)))

        logger.debug("bookings %d" % bookings.__len__())

        booking_cancel_success = []
        booking_cancel_failure = []
        for booking in bookings:
            logger.debug("Booking ID = %s" % booking.order_id)
            room_list = self.cancel_booking(booking)
            if len(room_list) > 0:
                # Success
                booking_cancel_success.append(booking)
                booking.status = Booking.CANCEL
                booking.save()
            else:
                # following room bookings failed
                booking_cancel_failure.append(booking)
            logger.debug(
                "==========================================================")
        logger.debug("Success count %d, Failure count %d" % (
            len(booking_cancel_success), len(booking_cancel_failure)))
        self.send_summary_email(booking_cancel_success, booking_cancel_failure)

    def cancel_booking(self, booking):
        order_id = booking.order_id
        room_list = []
        # get booking ids from table
        room_bookings = ""
        try:
            room_bookings = RoomBooking.objects.filter(booking_id=booking.id)
        except RoomBooking.DoesNotExist:
            logger.exception(
                "No rooms found for booking with id %s" %
                booking.order_id)
            return room_list
        except Exception as exc:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            self.build_error("get room bookings from DB",
                             repr(traceback.format_exception(exc_type, exc_obj,
                                                             exc_tb)))
        logger.debug("room_bookings %d" % room_bookings.__len__())
        # cancel in HX
        for room in room_bookings:
            try:
                reservation_id = room.reservation_id
                logger.debug("Reservation ID =  %s" % room.reservation_id)
                accesskey, accesssecret = self.getAuthDetails()
                cancel_xml = self.cancel_xml(
                    accesskey, order_id, reservation_id)
                cancel_signature = self.create_signature(
                    accesssecret, cancel_xml)
                cancel_xml_response = self.make_hx_call(
                    "cancel", cancel_xml, cancel_signature, accesskey)
                if cancel_xml_response is not None:
                    status = cancel_xml_response.find('status')
                    logger.debug("response status = %s" % status)
                    if status is not None:
                        booking.status = Booking.CANCEL
                        booking.save()
                        room_list.append(room)
            except Exception as exc:
                logger.exception("Exception on Cancelling %s" % exc.message)

        # delete from RoomBooking
        # room_bookings.delete()
        return room_list

    def send_exception_email(self, subject, content):
        receiver = Command.recipients
        sender = Command.sender_email
        msg = EmailMessage(subject, content, sender, receiver)
        msg.send()

    #
    def build_error(self, method, exception_text):
        logger.debug(exception_text)
        self.send_exception_email(method, exception_text)

    def getAuthDetails(self):
        auth_xml = self.ws_auth_xml()
        auth_signature = self.ws_auth_signature(auth_xml)
        # set what your server acceptsprint( headers
        headers = {
            'Content-Type': 'text/xml',
            'X-HAPI-Signature': auth_signature}

        response = requests.post(
            self.hotelogix_url + "wsauth",
            data=auth_xml,
            headers=headers)
        xml = BeautifulSoup(response.text, "xml")
        logger.debug("Auth XML = %s" % xml)
        a_key = xml.hotelogix.response.accesskey["value"]
        a_secret = xml.hotelogix.response.accesssecret["value"]
        return a_key, a_secret

    def ws_auth_xml(self):
        time = datetime.datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + \
            "'><request method='wsauth' key='" + Command.consumer_key + "'></request></hotelogix>"
        return xml

    def ws_auth_signature(self, xml):
        signature = hmac.new(Command.consumer_secret, xml, sha1)
        ws_auth_signature = signature.hexdigest()
        return ws_auth_signature

    def cancel_xml(self, accesskey, order_id, reservation_id):
        time = datetime.datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + "'><request method='cancel' key='" + accesskey + "'><orderId value='" + order_id + \
            "'/><reservationId value='" + reservation_id + "'/><cancelCharge amount='0'/><cancelDescription>This is a test cancel from webservice</cancelDescription></request></hotelogix>"
        return xml

    def create_signature(self, accesssecret, xml):
        signature = hmac.new(
            str(accesssecret),
            str(xml),
            digestmod=sha1).hexdigest()
        return signature

    def make_hx_call(self, api, xml, signature, key):
        logger.debug("HX Api call %s" % api)
        # set what your server accepts
        headers = {'Content-Type': 'text/xml', 'X-HAPI-Signature': signature}
        logger.debug(api + " request xml = ")
        logger.debug(xml)
        response = requests.post(
            Command.bookingdetails_url + api,
            data=xml,
            headers=headers)
        xml_response = BeautifulSoup(response.text, "xml")
        logger.debug(api + " response xml = ")
        logger.debug(xml_response)
        if xml_response is not None and xml_response.find(
                "status")["message"] == "SUCCESS":
            logger.debug(api + " SUCCESS!!")
            xml_response = BeautifulSoup(response.text, "xml")
        else:
            xml_response = None
            logger.debug(api + "HX call failed!!")

        return xml_response

    def send_summary_email(self, success_list, failure_list):
        mail_template = loader.get_template('hotels/mail_delete_bookings.html')
        data = Context({"success_list": success_list,
                        "failure_list": failure_list})
        content = mail_template.render(data)
        msg = EmailMessage(
            "Delete Test Booking Summary",
            content,
            settings.EMAIL_HOST_USER,
            Command.recipients)
        msg.content_subtype = "html"

        msg.send()
