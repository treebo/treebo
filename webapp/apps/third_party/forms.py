from django import forms


class TrivagoFileUploadForm(forms.Form):
    trivagoFile = forms.FileField(label='Select File')
