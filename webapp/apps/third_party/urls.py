from django.conf.urls import url

from apps.third_party.api.v1.dynamic_ads_cache_api import DynamicAdsCacheSetApi
from apps.third_party.api.v1.dynamic_ads_feed_api import DynamicAdsHotelFeedCreate, DynamicAdsHotelFeedEmail, \
    DynamicAdsFeedDownload, DynamicAdsFeedForAppDownload
from apps.third_party.api.v1.generate_criteo_feed import CriteoAPI
from apps.third_party.api.v1.generate_mapping import UpdateMapping
from apps.third_party.api.v1.google_hotel_ads import UpdatePOS, UpdateHotelList, CreateQueryMessage, Transaction, \
    GenerateGHACSV
from apps.third_party.api.v1.third_party_hotel_availability import ThirdPartyHotelAvailability
from apps.third_party.api.v1.trip_advisor import HotelAvailability, GenerateTACSV
from apps.third_party.api.v1.trivago_hotel_data import TrivagoHotelData, TrivagoCsvHotelData, \
    TrivagoCSVDownloader, TrivagoUpdateCSVFile, TrivagoUpdateCSVFileSync, TrivagoBidFileDownloader
from apps.third_party.api.v1.trivago_hotel_upload_api import TrivagoAutomaticHotelUploader, TrivagoFileUploader

app_name = 'thirdparty'

urlpatterns = [
    url(r"^(?P<source>[\w-]+)/hotelavailability/", ThirdPartyHotelAvailability.as_view(), name="third-party-hotelavailability"),
    url(r"^trivago/hoteldata/", TrivagoHotelData.as_view(), name="trivago-hoteldata"),
    url(r"^(?P<source>[\w-]+)/hotelcsvdata/", TrivagoCsvHotelData.as_view(), name="trivago-hotelcsvdata"),
    url(r"^(?P<source>[\w-]+)/hotel/details/$", TrivagoCSVDownloader.as_view(),
        name="trivago-hotelgetcsvdata"),
    url(r"^trivago/hotel/update_details/$", TrivagoUpdateCSVFile.as_view(),
        name="trivago-hotelupdatecsvdata"),
    url(r"^trivago/hotel/update_details_sync/$", TrivagoUpdateCSVFileSync.as_view(),
        name="trivago-hotelupdatecsvdatasync"),
    url(r"^trivago/bids/$", TrivagoBidFileDownloader.as_view(),
        name="trivago-hotelbiddata"),
    url(r"^trivago/upload/", TrivagoFileUploader.as_view(), name="trivago-upload"),
    url(r"^trivago/hotel_automatic_upload/", TrivagoAutomaticHotelUploader.as_view(), name="trivago-hotel-automatic-upload"),

    url(r"^dynamicads/hotelfeed/(?P<feed_type>[\w-]+)$", DynamicAdsHotelFeedCreate.as_view(), name="dynamicads-hotelfeed-create"),
    url(r"^dynamicads/cache-set/(?P<feed_component>[\w-]+)$", DynamicAdsCacheSetApi.as_view(),
        name = "dynamicads-cache"),
    url(r"^dynamicads/hotelfeed/download/(?P<feed_type>[\w-]+)$", DynamicAdsFeedDownload.as_view(), name="dynamicads-hotelfeed-download"),
    url(r"^dynamicads/hotelfeed/app/download/(?P<feed_type>[\w-]+)$", DynamicAdsFeedForAppDownload.as_view(),
        name="dynamicads-hotelfeed-forapp-download"),
    url(r"^dynamicads/hotelfeed/email/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/(?P<feed_type>[\w-]+)$", DynamicAdsHotelFeedEmail.as_view(), name="dynamicads-hotelfeed-download"),
    url(r'^hotelads/updateposfile.xml$',UpdatePOS.as_view(), name ='update_pos'),
    url(r'^hotelads/updatehotellist.xml$',UpdateHotelList.as_view(), name ='update_hotel_list'),
    url(r'^hotelads/querymessage.xml$',CreateQueryMessage.as_view(), name ='create_query_message'),
    url(r'^hotelads/transactionmessage.xml$',Transaction.as_view(), name ='transaction'),
    url(r'^tripadvisor/generatemapping$',UpdateMapping.as_view(), name ='update-mapping'),
    url(r'^hotelads/generateghacsv$',GenerateGHACSV.as_view(), name ='generate-gha-csv'),
    url(r'^tripadvisor/hotel_availability$',HotelAvailability.as_view(), name ='hotel-availability'),
    url(r'^tripadvisor/generatetacsv$',GenerateTACSV.as_view(), name ='generate-tripadvisor-csv'),
    url(r'^criteo/generatecsv$', CriteoAPI.as_view(), name='generate-criteo-csv'),


]
