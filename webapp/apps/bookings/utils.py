# coding=utf-8
"""
    Utils
"""

import functools
import os
import sys
import traceback
import threading
from apps.common.thread_local_utils import thread_locals

async_local = thread_locals

from django.db import DatabaseError, InterfaceError, close_old_connections

from apps.common import utils as tracker_utils
from common.custom_logger.booking.log import get_booking_logger

logger = get_booking_logger(__name__)


def __get_order_details_from_response(booking_id, xml):
    order_amount = xml('orderamount').attr("amount")
    order_id = str(booking_id)
    guest = xml('guest')
    billing_name = guest('fname').text()
    billing_email = guest('email').text()
    billing_tel = guest('mobile').text()
    return billing_email, billing_name, billing_tel, order_amount, order_id


def unableToSaveAnalytics(user_id, analytics_data, channel):
    try:
        tracker_utils.trackAnalyticsServerEvent(
            user_id, 'Unable to Save', analytics_data, channel=channel)
    except Exception as err:
        logger.debug(
            "__unableToSaveAnalytics Error %s " %
            traceback.format_exc())


def async_job_logging_init():
    """
    :return:
    """

    def decorator(base_function):
        """
        :param base_function:
        :return:
        """
        @functools.wraps(base_function)
        def wrapper(*args, **kwargs):
            """
            :param args:
            :param kwargs:
            :return:
            """
            # payload is 2 argument
            # TODO see if there is a better way to get args
            payload = args[1]
            set_logging_attributes(payload)
            logger.info(
                'calling {0} with , data {1}'.format(
                    base_function.__name__, args))
            response = base_function(*args, **kwargs)
            clear_logging_attributes()
            return response
        return wrapper
    return decorator


def set_logging_attributes(payload):
    """
    :param payload:
    :return:
    """
    data = payload["data"]
    async_local.request_id = data.get('request_id', None)
    async_local.job_id = payload.get('job_id', None)
    logger.info('logging attributes set as {0} {1}'.format(
        async_local.__dict__, threading.current_thread()))


def clear_logging_attributes():
    """
    :return:
    """
    logger.info(
        'logging attributes cleared as {0}'.format(
            async_local.__dict__))
    if hasattr(async_local, 'request_id'):
        del async_local.request_id
    if hasattr(async_local, 'job_id'):
        del async_local.job_id


def get_value(value):
    """
    Get a utf-8 version of the input string
    :param (unicode) value:
    """
    return value.encode('utf-8').decode() if value and value.lower() != 'none' else ''
