from django.db.models import Q

from apps.seo.components.base_component import BaseComponent
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.serializers.serializer import LocalitySerializer
from dbcommon.models.location import Locality


__author__ = 'ansilkareem'


class Localities(BaseComponent):

    def __init__(self, current_city, footer_data, exclude_list):
        super(
            Localities,
            self).__init__(
            current_city,
            footer_data,
            exclude_list)

    def build(self):
        data = {
            'key': 'locality',
            'title': 'LOCALITIES',
            'content': AutoCompleteResultBuilder.build_locality_result(
                Locality.objects.filter(
                    city__name__iexact=self.current_city.name,
                    enable_for_seo=True,
                    city__enable_for_seo=True).exclude(
                    id__in=self.exclude_list))} if self.footer_data and self.current_city else {
            'key': 'locality',
            'title': 'LOCALITIES',
            'content': []}
        serialized_obj = LocalitySerializer(data=data)
        if serialized_obj.is_valid():
            return serialized_obj.validated_data
        else:
            return {'key': 'locality', 'title': 'LOCALITIES',
                    'content': []
                    }
