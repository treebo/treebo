# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0017_bookingrequest_booking_channel'),
    ]

    operations = [
        migrations.RenameField(
            model_name='roombooking',
            old_name='Booking',
            new_name='booking',
        ),
        migrations.AddField(
            model_name='booking',
            name='channel',
            field=models.CharField(
                default='',
                max_length=50,
                blank=True,
                choices=[
                    ('msite',
                     'msite'),
                    ('website',
                     'website')]),
        ),
    ]
