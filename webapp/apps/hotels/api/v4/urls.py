from django.conf.urls import url

from apps.hotels.api.v4.hotel_detail import HotelDetail

app_name = 'hotels_v4'

urlpatterns = [
    url(r"^(?P<cs_hotel_id>[0-9]+)/details/$", HotelDetail.as_view(), name='hotel-detail-v4'),

]
