import logging
from apps.common.exceptions.booking_exceptions import BookingNotFoundException

from apps.payments.offers.repositories.payment_offer import PaymentOfferRespository

logger = logging.getLogger(__name__)


class PaymentOfferService:

    def apply_payment_offer(self, request, bid, payment_method_id):
        from apps.checkout.models import BookingRequest
        from apps.checkout.service.itinerary_handler_v2 import ItineraryHandlerV2

        try:
            booking_request = BookingRequest.objects.get(pk=bid)
        except BookingRequest.DoesNotExist:
            raise BookingNotFoundException('bid %d not present ' % bid)

        checkin = booking_request.checkin_date
        checkout = booking_request.checkout_date
        # utm_source = booking_request.utm_source
        current_user = getattr(request, 'user', None)

        coupon_code = booking_request.coupon_code
        if payment_method_id:
            payment_offers = PaymentOfferRespository().auto_applicable_offers_for_payment_method(
                payment_method_id, utm_source=booking_request.utm_source)
            if payment_offers:
                coupon_code = payment_offers[0].coupon_code

        itinerary_handler = ItineraryHandlerV2()
        data = itinerary_handler.get_pricing_data_from_booking_request_and_coupon(
            booking_request, coupon_code)

        price = itinerary_handler.get_price(request, data['page_data'], data['availability_data'],
                                            data['pricing_data'], True, checkin,
                                            checkout, booking_request=booking_request)

        payment_offer_coupon_code = None
        if price["selected_rate_plan"].get("payment_offer_price"):
            payment_offer_coupon_code = price["selected_rate_plan"]["payment_offer_price"][
                "coupon_code"]

        itinerary_handler.update_booking_request_with_prices(
            booking_request,
            price["selected_rate_plan"],
            current_user,
            coupon_code=price["selected_rate_plan"]["price"]["coupon_code"],
            rate_plan_detail=price["selected_rate_plan"]["rate_plan"],
            wallet_info=price["wallet_info"]
        )

        itinerary_handler.add_committed_price_for_booking_request(
            booking_request,
            price["selected_rate_plan"]["price"]
        )
        if payment_offer_coupon_code:
            price["coupon_code"] = payment_offer_coupon_code

        return price

    def get_payment_offer_for_coupon_code(self, coupon_code, utm_source, channel):
        return PaymentOfferRespository().get_payment_offer_for_coupon_code(coupon_code,
                                                                           utm_source,
                                                                           channel)

    def get_gateway_payment_coupon_codes_for_utm_source(self, utm_source):
        payment_offers = PaymentOfferRespository().get_offers_for_utm(
            utm_source)
        return [payment_offer.gateway_offer_id for payment_offer in payment_offers]

    def get_applicable_payment_offer(self, utm_source, channel):
        payment_offers = PaymentOfferRespository().get_auto_applicable_payment_offers_for_utm(
            utm_source, channel)
        if not payment_offers:
            return None
        return payment_offers[0]

    def get_offers_for_utm(self, utm_source):
        return PaymentOfferRespository().get_offers_for_utm(
            utm_source)

    def get_offer_api_response(self, utm_source):
        payment_offers = PaymentOfferRespository().get_offers_for_utm(
            utm_source)
        if not payment_offers:
            return []
        return [{
            "id": payment_offer.uid,
            "type": payment_offer.payment_method.category,
            "name": payment_offer.title,
            "payment_method_id": payment_offer.payment_method.uid,
            "offer_percentage": payment_offer.offer_percentage,
            "credit_mode": payment_offer.credit_mode,
            "minimum_transaction": payment_offer.minimum_transaction,
            "terms_and_conditions": payment_offer.terms_conditions,
            "description": payment_offer.description,
            "ranking": payment_offer.ranking,
            "gateway_offer_id": payment_offer.gateway_offer_id,
            "image_url": payment_offer.image_url,
        } for payment_offer in payment_offers]
