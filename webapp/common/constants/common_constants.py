from django.conf import settings

__author__ = 'varunachar'

from ths_common.constants.base_enum import BaseEnum

EMAIL_FORMAT = "html"

# Coupon Constants
COUPON_NOT_FOUND = "Coupon not applicable. Please try another coupon code"
COUPON_NOT_ACTIVE = "Coupon no longer active. Please try another coupon code"
COUPON_EXPIRED = "Coupon expired"
COUPON_MIN_CART = "Coupon not applicable for the selected rooms"
COUPON_EXHAUSTED = "Coupon usage already exhausted. Please try another coupon code"
DISCOUNT_APPLIED = "Discount successfully applied"
INVALID_DATA = "Invalid data for applying discount"
HOTEL_CONSTRAINT = "Coupon not applicable on selected hotel. Please try another coupon code"
USER_CONSTRAINT = "Coupon not applicable on your account. Please try another coupon code"
DATE_CONSTRAINT = "Coupon not applicable for the selected dates. Please try another coupon code"
DURATION_ERROR = "Coupon not applicable for the selected duration of stay. Please try another coupon code"

# Profile Constants
OLD_PASSWORD = 'Current Password'
NEW_PASSWORD = 'New Password'
NEW_PASSWORD_CONFIRMATION = 'New Password Confirmation'
EMAIL = 'Email'
EMAIL_VERIFY_SUBJECT = 'Verify email'
EMAIL_FORGOT_PASSWORD_SUBJECT = 'Treebo Hotels: Reset Password Link'
REGISTRATION_SUBJECT = "Welcome to Treebo Hotels"
EMAIL_ALREADY_REGISTERED = "That email address is already in use. Please log in."

FORGOT_PASSWORD_TEXT = """
Dear $user,

We've received a request to change your password on www.treebo.com.

Click the link below to reset your password:

$link

if you don't want to change your password, please ignore this email.

Thanks,
Team Treebo.
                            """
SIGNUP_COUPON_CREATED = 0
SIGNUP_COUPON_PENDING = 1
SIGNUP_COUPON_SENT = 2
SIGNUP_COUPON_STATUS = (
    (SIGNUP_COUPON_CREATED, 'Created'),
    (SIGNUP_COUPON_PENDING, 'Pending'),
    (SIGNUP_COUPON_SENT, "Sent")
)
SIGNUP_COUPON_DISCOUNT = 20
SIGNUP_COUPON_MAXUSAGE = 1
SIGNUP_COUPON_MAXDISCOUNT = 1000
SIGNUP_COUPON_MINVALUE = 800
SIGNUP_COUPON_VALIDDAYS = 30
SIGNUP_COUPON_TERMS = "20% discount on sign up and first booking. \nMaximum discount value upto INR 1000.\nOffer subject to availability of rooms.\nOffer applicable only on bookings done through Treebo website and call center.\nThis coupon cannot be used in conjunction with any other discounts or offers from Treebo or any of its partners."
FEEDBACK_SUBJECT = "Treebo Feedback"

# Hotel Constants
SUCCESS = 'SUCCESS'
MERCHANT_ID = "66971"
CURRENCY = "INR"
LANGUAGE = "EN"
EMPTY_XML = "<?xml version='1.0'?>"
WEB_PACKAGE = "Treebo Standard"
PROMOTIONAL_WEB_PACKAGE = "Treebo Promotional"
CUSTOMER_CARE_NUMBER = "9322-800-100"
BILLING_ADDRESS = "Treebo"
BILLING_CITY = "Bangalore"
BILLING_STATE = "Karnataka"
BILLING_COUNTRY = "India"
BILLING_ZIP = "560066"
BILLING_TEL = "9863091723"
GUESTS_PER_ROOM = 2
ERROR_MESSAGE = "Your request has timed out. Please try another booking."
FACEBOOK_APP_ID = "501360290015135"

OTP_MESSAGE = "You shall receive an OTP on this number"
OTP_SMS_MESSAGE = '''Dear %(first_name)s,
Your One Time Password(OTP) for verifying your mobile number on www.treebo.com is %(otp)s.
Thank you.'''
BOOKING_SMS_MESSAGE = '''Dear %(first_name)s, Your booking for %(rooms)s rooms at %(hotel_name)s is confirmed.Booking Id: %(booking_ids)s Check-in: %(checkin_date)s Check-out: %(checkout_date)s Address: %(address)s Here is the google maps link for directions %(google_link)s Have a pleasant stay! Please feel free to call us at +919322800100 for any assistance.
'''
NEXT_DAY_RESERVATION_SMS_MESSAGE = '''Dear %s, Here are detailed directions to %s: %s. Here is the google maps link for directions %s. Have a pleasent stay!'''
OTP_ERROR_MESSAGE = "Please Enter the correct OTP sent to your mobile number"
OTP_RESENT_MESSAGE = "OTP has been resent to your mobile number"
OTP_SMS_ERROR = "Error occured while sending the OTP. Please try again"
SEND_SMS = True
GOOGLE_MAPS_LINK = "https://maps.google.com/?q="
MIN_ROOMS_AVAILABLE = 2
PDF_PATH = "webapp/static/files/"
PARTNER_EMAIL = 'abhay.kothari@treebohotels.com'
PARTNER_EMAIL_SUBJECT = 'Partner Info added'

BANGALORE_LAT = ''
BANGALORE_LONG = ''
DEFAULT_LOCALITY = ''
LAST_BOOKING_TIME = 3
DEFAULT_MIN_DISTANCE = 50

NOTIFY_LOCATION_EMAIL = 'alerts.webbooking@treebohotels.com'
NOTIFY_LOCATION_EMAIL_SUBJECT = 'New Hotel request in city %s, locality %s'
NOTIFY_LOCATION_EMAIL_CONTENT = ' Hi ,\n\n New request for hotel has been added.\n\n Details:\n\n ' \
                                'Locality: %s\n\n City: %s\n\n Email: %s'
DEFAULT_GUEST = "1"
DEFAULT_ROOM = "1"
RESERVED = "Reserved"
CROP_TYPE = "bottom"
QUALITY = "1"
FIT_TYPE = "crop"
IMAGE_FM = "jpg"
IMAGE_QUALITY = "35"
GMAP_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin_latlng&destinations=$destination_latlng"
GMAP_DIRECTIONS_URL = "https://maps.googleapis.com/maps/api/directions/json?origin=$origin_latlng&destination=$destination_latlng&key=$key&mode=$mode"
GMAP_STATIC_URL = "https://maps.googleapis.com/maps/api/staticmap?size=$size&markers=icon:$icon_src%7C$origin_latlng&markers=icon:$icon_dest%7Clabel:$dest_label%7C$destination_latlng&path=weight:6%7Ccolor:blue%7Cenc:$polyline&key=$key"
GMAP_API_KEY = "AIzaSyDmeDs1kE1AUpI7mbFQmMMyQzwP8qsSboA"
DIRECTION_MODE_WALKING = "walking"
DIRECTION_MODE_DRIVING = "driving"
GMAP_STATIC_SIZE = "600x300"
GMAP_MODE_DISTANCE = 500
GMAP_STATIC_HOTEL_ICON_URL = "//s3-ap-southeast-1.amazonaws.com/treebo/static/images/Icons/treebo_hotel_location_pin-new.png"
GMAP_STATIC_DESTINATION_ICON_URL = "//s3-ap-southeast-1.amazonaws.com/treebo/static/images/Icons/Location_pin_black.png"
DISABLED = 0
ENABLED = 1
STATUS_CHOICES = (
    (DISABLED, 'Disabled'),
    (ENABLED, 'Enabled')
)

HIDE = 0
DISPLAY = 1
DISPLAY_CHOICES = (
    (DISPLAY, 'Display'),
    (HIDE, 'Hide')
)
CANCEL_BOOKING_MESSAGE = "Your request cannot be processed"
BOOKING_CONFIRM = 1
BOOKING_CANCEL = 0
CANCELLATION_PENDING = -1
BOOKING_STATUS = (
    (BOOKING_CONFIRM, 'Confirm'),
    (BOOKING_CANCEL, 'Cancel'),
    (CANCELLATION_PENDING, 'Cancellation Pending')
)
CANCEL_BOOKING_START_DATE = 'Sep 7 2015  1:33PM'
HASHING_SALT = "bqoksIlEnWtsL9lmaiZD"

VERIFIED = "V"
NOT_VERIFIED = "N"
DELETED = "D"
PHONE_CHOICE = (
    (VERIFIED, 'Verfied'),
    (NOT_VERIFIED, 'Not Verified'),
    (DELETED, 'Deleted')
)
COUNTRY_CODE = "IN"
MAX_GUESTS = 30
PREPAID_COUPON = "TBO50"
PREPAID_COUPON_ERROR = "Sorry! This offer is available for pre-paid bookings only.Consider paying now itself? Don't worry, it's still cancellable free of charge"
HOTELOGIX_RETRY_COUNT = 3
ACCESS_KEY = "accesskey"
ACCESS_SECRET = "accesssecret"
DO_NOT_SEND_CITY_PHONE = 'DONOT_SEND_CITY_PHONE'
DO_NOT_SEND_CITY_EMAIL = 'DONOT_SEND_CITY_EMAIL'
DO_NOT_SEND_NOTIFICATION_PHONE = 'DONOT_SEND_NOTIFICATION_PHONE'
DO_NOT_SEND_NOTIFICATION_EMAIL = 'DONOT_SEND_NOTIFICATION_EMAIL'

# For Trivago
TRIVAGO_API_VERSION = 4
TRIVAGO_API_LANGUAGE_DEFAULT = 'en_IN'
TRIVAGO_API_CURRENCY_DEFAULT = 'INR'

# For SEO
FREQUENTLY_BOOKED_KEY = 'FREQUENTLY_BOOKED_'

SUPERSAVER = "Supersaver"

class BaseTemplate(BaseEnum):
    @property
    def identifier(self):
        return self.values[0]

    @property
    def subject(self):
        return self.values[1]


class EmailTemplates(BaseTemplate):
    PAYMENT_DROPOFF = "payment_drop_off_notification", \
                           "Complete Your Booking in {city} before prices go up!"

class MessageTemplates(BaseTemplate):
    PAYMENT_DROPOFF = "soft_block_payment_link"
