from rest_framework import serializers
import re


class PhoneNumberSerializer(serializers.Serializer):
    phone_number = serializers.CharField(required=True, max_length=15)

    def validate(self, data):
        number = data['phone_number']
        is_valid_phone_number_sequence = bool(re.match('^[\+]*[0-9]{1,15}$', number))
        if not is_valid_phone_number_sequence:
            raise serializers.ValidationError("Invalid Phone number")
        return data
