import $ from 'jquery';
import BaseView from './treebolibs/baseview';
import searchWidget from './widgets/search';
import React from 'react';
import {render} from 'react-dom';
import {Router, Route, Link, browserHistory} from 'react-router';
import { createStore, combineReducers } from 'redux';
import classNames from 'classnames';
import moment from 'moment';
import * as toastr from 'toastr';
import treejax from './treebolibs/treejax';
import Clipboard from 'clipboard';
import 'parsleyjs';
import cookies from 'js-cookie';
import queryString from 'query-string';
import Share from './widgets/share';
import '../css/account.less';


const $loader = $("#commonLoader");
const $csrfMiddlewareToken = $('input[name="csrfmiddlewaretoken"]')
const BookingHistoryView = Object.create(BaseView);
const bookingData = JSON.parse($('#bookingDetailData').val());
const referralData = JSON.parse($('#referralData').val());

BookingHistoryView.pageLoaded = () => {
    methods.initApp();
    methods.suppressAccountLinks();
};

const Bookings = React.createClass({
    render: ()=>{
        const previousStay = classNames({
            hide: false,
            bookings__item:true
        });
        return (
            <div className="bookings">
                <div className="account-page__header pos-rel">BOOKING HISTORY</div>
                <div id="upcomingStay" className="bookings__item">
                    <div className="bookings__item__header">Upcoming Stays</div>
                    <div className="bookings__item__body">
                        {bookingData.upcoming.map((v,i)=>{
                            return (<Booking bookingData = {v} type="upcoming" key={i}/>)
                        })}
                    </div>
                </div>
                {(() => {
                    if(bookingData.previous.length){
                        return (
                            <div id="previousStay" className={previousStay}>
                                <div className="bookings__item__header">Previous Stays</div>
                                <div className="bookings__item__body">
                                    {bookingData.previous.map((v,i)=>{
                                        return (<Booking bookingData = {v} type="previous" key={i}/>)
                                    })}
                                </div>
                            </div>
                        )
                    }
                })()}
            </div>
        )
    }
});

const BookingDetails = React.createClass({
    getInitialState() {
        return {
            cancelPromptShown : false
        }
    },
    cancelBooking() {
        const [type, bookingId] = this.props.params.bookingId.split("||");
        const [bookingDetails] = bookingData[type].filter((v,i)=>{
            return v.order_id === bookingId;
        });
        const {email} = bookingDetails.user_detail;
        const order_id = bookingDetails.order_id;
        const self = this;
        $loader.removeClass('hide');
        self.toggleCancelConfirm();
        treejax({
                url: '/api/v1/checkout/booking/cancel/',
                type: 'POST',
                data: {
                    order_id,
                    email,
				}
            })
		.then(response => {
            toastr.success('Your booking is cancelled successfully.', 'Thank You!', {timeOut: 5000, closeButton: true, positionClass: "toast-top-right"});
			bookingDetails.status = "CANCELLED";
			self.props.history.goBack();
		}, error => {
            toastr.error(error, {timeOut: 5000, closeButton: true, positionClass: "toast-top-right"});
		}).finally(() => {
            $loader.addClass('hide');
        });


    },
    toggleCancelConfirm() {
        let cancelState = this.state.cancelPromptShown;
        this.setState({cancelPromptShown: !cancelState })
    },
    render() {
        const [type, orderId] = this.props.params.bookingId.split("||");
        const [bookingDetails] = bookingData[type].filter((v,i)=>{
            return v.order_id === orderId;
        });

        const {status, group_code, booking_date, hotel_detail, checkin_time, order_id,
            user_detail, checkout_time, room_config, payment_detail, cancel_date,
            paid_at_hotel, checkin_date, flag, checkout_date, hotel_name } = bookingDetails;

        const {no_of_adults, room_count, room_type, no_of_childs} = room_config;

        const {discount, room_price, tax, total} = payment_detail;

        const {name, email, contact_number} = user_detail;
        const {locality, city} = hotel_detail;

        const roomString  = `${room_count} Room${room_count > 1 ? 's ' : ' '} (${room_type})`;
        const kidsString = no_of_childs>0 ? `${no_of_childs} Kid${no_of_childs > 1 ? 's ' : ' '}` : '';
        const guestString  = `${no_of_adults} Adult${no_of_adults > 1 ? 's ' : ' '} ${kidsString}`;

        const cancelPopUp = this.state.cancelPromptShown ? <CancelPopUp onCancel={this.toggleCancelConfirm} onConfirm={this.cancelBooking} /> : '';

        const payStr = paid_at_hotel ? 'Payable' : 'Paid';

        const cancelBtn = (status === "CANCELLED" || type === 'previous' ) ? '' : <div className="btn pos-fix booking-cancel" onClick={this.toggleCancelConfirm}>Cancel</div>;
        const cancelledMsg = status === "CANCELLED" ? <div className="booking-info__cancelled-msg alert alert--warning ">Booking successfully cancelled on {moment(cancel_date ? cancel_date: undefined).format('Do MMM\'YY')}</div> : ''
        return (
            <div className="booking-details">
                <div className="account-page__header pos-rel">BOOKING DETAILS
                    <Link to="/account/bookings" className="pos-abs booking-info__back-link"><i className="icon-back"></i></Link>
                </div>
                {cancelledMsg}
                <div className="account-page__body">
                    <div className="bookings__item">
                        <div className="bookings__item__body booking-details__item">
                            <div className="booking-details__item__hotel-name">{hotel_name}</div>
                            <div className="booking-details__item__hotel-address">{locality}, {city}</div>
                            <div className="booking-details__item__booking-id">{order_id}</div>
                            <div className="booking-details__item__room-guest-info">
                                <span className="rooms">{roomString}</span>
                                <span className="guests">{guestString}</span>
                            </div>
							<div className="booking-details__item__dates">{moment(checkin_date).format('ddd\, Do MMM\'YY')} - {moment(checkout_date).format('ddd\, Do MMM\'YY')}</div>

                        </div>
                    </div>
                    <div className="bookings__item">
                        <div className="bookings__item__header">Primary Traveller Information</div>
                        <div className="bookings__item__body booking-details__item">
                            <div className="booking-details__item__traveller-name">{name}</div>
                            <div className="booking-details__item__traveller-email">{email}</div>
                            <div className="booking-details__item__traveller-number">{contact_number}</div>
                        </div>
                    </div>
                    {/*<div className="bookings__item">
                        <div className="bookings__item__header">Special Instruction</div>
                        <div className="bookings__item__body requests">
                            <div className="requests__item"></div>
                        </div>
                    </div>*/}
                    <div className="bookings__item">
                        <div className="bookings__item__header">Payment Information</div>
                        <div className="bookings__item__body payment-info">
                            <div className="payment-info__item payment-info__base-price row">
                                <div className="col col-1">Room Price</div>
                                <div className="col col-2"><i className="icon-inr"></i>{room_price}</div>
                            </div>
                            <div className="payment-info__item payment-info__tax row">
                                <div className="col col-1">Tax</div>
                                <div className="col col-2"><i className="icon-inr"></i>{tax}</div>
                            </div>
                            <div className="payment-info__item payment-info__discount row">
                                <div className="col col-1">Discount</div>
                                <div className="col col-2">&minus; <i className="icon-inr"></i>{discount}</div>
                            </div>
                            <div className="payment-info__item payment-info__total row">
                                <div className="col col-1">Total {payStr}</div>
                                <div className="col col-2"><i className="icon-inr"></i>{total}</div>
                            </div>

                        </div>
                    </div>
                    {cancelBtn}
                    {cancelPopUp}
                </div>
            </div>
        )
    }
})

const CancelPopUp = React.createClass({
    render() {
        return (
            <div id="cancelConfirmModal" className="prompt">
                <div className="prompt__body">
                    <div className="prompt__confirm-text">Are you sure you want to cancel booking?</div>
                    <div className="row prompt__action">
                        <div className="col prompt__action__item prompt__action__item--cancel" onClick={this.props.onCancel}>Cancel</div>
                        <div className="col prompt__action__item prompt__action__item--ok" onClick={this.props.onConfirm}>Ok</div>
                    </div>
                </div>
            </div>
        )
    }
})

const Booking = (props)=>{
    const {bookingData, type} = props;
    const {status, group_code, booking_date, hotel_detail, checkin_time, order_id,
        user_detail, checkout_time, room_config, payment_detail,
        cancel_date, paid_at_hotel, checkin_date, flag, checkout_date, hotel_name} = bookingData;
    const {no_of_adults, room_count, room_type, no_of_childs} = room_config;
    const {locality, city} = hotel_detail;
    const roomString  = `${room_count} Room${room_count > 1 ? 's ' : ' '}`;
    const kidsString = no_of_childs>0 ? `${no_of_childs} Kid${no_of_childs > 1 ? 's ' : ' '}` : '';
    const guestString  = `${no_of_adults} Adult${no_of_adults > 1 ? 's ' : ' '} ${kidsString}`;
    const bookingStatusClass = `booking-info__status booking-info__status--${status}`;
    const detailsLink = `/account/details/${type}||${order_id}`;
    return (
        <Link to={detailsLink}>
            <div className="bookings__item__booking booking-info hotel row pos-rel">
                <div className="col col-1">
                    <div className="booking-info__hotel-name">{hotel_name}</div>
                    <div className="booking-info__hotel-address">{locality}, {city}</div>
                    <div className="booking-info__room-guest-info">
                        <span className="rooms">{roomString}</span>
                        <span className="guests">{guestString}</span>
                    </div>
                    <div className={bookingStatusClass}>{status ==='RESERVED' ? 'Confirmed' : 'Cancelled'}</div>
                </div>
                <div className="col col-2 pos-abs">
                    {moment(checkin_date).format('Do MMM\'YY')}
                    <div className="sep-dash text-center"><i className="icon-returnarrows"></i></div>
                    {moment(checkout_date).format('Do MMM\'YY')}
                </div>
            </div>
        </Link>
    )
};


const ReferralRewardItem = (reward, i) => {
  return (
    <div className="referrals__status__list__item" key={i}>
      <div className="referrals__status__list__item__email">{reward.email}</div>
      <div className="referrals__status__list__item__status">{reward.event_name}</div>
    </div>
  );
}

class Referrals extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			copyText: 'Copy',
		}
	}

	componentDidMount() {
		const clipboard = new Clipboard('#ref-copy');
		clipboard.on('success', () => {
			this.setState({
				copyText: 'Copied!',
			});
			setTimeout(() => {
				this.setState({
					copyText: 'Copy',
				});
			}, 2000);
		});
	}

  render() {
    const { referral_code, share, shareurl, total_signup, total_reward_earned, all_referral_rewards} = referralData;
    return (
      <div className="referrals">
        <div className="referrals__main">
          <div className="referrals__main__title">You can earn Paytm cash by referring your friends to Treebo!</div>
          <div className="referrals__main__banner">
              <img className="referrals__main__banner-img"
                   src="http://images.treebohotels.com/images/checkout_email_img.jpg" alt="" width="100" />
          </div>
          <div className="referrals__main__sub-title">
             Invite all your friends to experience an awesome stay at Treebo & get Rs. 500 Paytm cash on their first stay.
            Your friends also get Rs. 500 off on their first stay at Treebo. <a href="https://treeboreferral.squarespace.com/" target="_blank">Terms & Conditions</a>
          </div>
          <div className="referrals__main__url-wrapper">
            <input className="referrals__main__url-wrapper__url" type="text" disabled value={shareurl} />
						<button id="ref-copy" className="referrals__main__url-wrapper__copy-btn btn" onClick={this.onCopyShareUrl} data-clipboard-text={shareurl}>{this.state.copyText}</button>
          </div>
					<Share shareSocial={referralData.share} shareUrl={shareurl} />
        </div>
        <div className="referrals__status__sec-title">REFERRAL STATUS</div>
        <div className="referrals__status">
          <div className="referrals__status__block row">
            {/*<div className="referrals__status__block__box text-center col">X<br /><div className="box__sub-text">Referred</div></div>*/}
            {/*<div className="referrals__status__block__box text-center">{total_signup}<br /><div className="box__sub-text">Signed Up</div></div>*/}
            <div className="referrals__status__block__box text-center"> Total successful referrals : {total_reward_earned} </div>
          </div>
          <div className="referrals__status__list">
            { all_referral_rewards.map(ReferralRewardItem) }
          </div>
        </div>
      </div>
    );
  }
}

const Account = React.createClass({
  render() {
    return this.props.children;
  }
})

const App = () => {
  return (
    <Router history={ browserHistory }>
      <Route path="/account" component={Account}>
        <Route path="bookings" component={Bookings} />
        <Route path="details/:bookingId" component={BookingDetails} />
        <Route path="referrals" component={Referrals} />
      </Route>
    </Router>
  )
};

let methods = {
  initApp() {
    render(<App/>, document.getElementById('accountPage'))
  },
  suppressAccountLinks() {
    $('.js-accounts-link').click(function (e) {
      e.preventDefault();
      browserHistory.push($(this).attr('href'));
      $('body').removeClass('modal--open');
      $('#pageContainer').removeClass('page--open');
    });
  }
}

const bookingHistoryConfig = { name: 'Account Page', widgets: [searchWidget] };
const bookingHistoryView = BookingHistoryView.setup(bookingHistoryConfig);
