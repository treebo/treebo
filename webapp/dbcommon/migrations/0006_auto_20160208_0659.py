# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0005_auto_20151103_1609'),
    ]

    operations = [
        migrations.CreateModel(
            name='citySeoDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('citySeoTitleContent',
                 models.CharField(max_length=500, db_column='city_seo_title')),
                ('citySeoDescriptionContent',
                 models.CharField(max_length=500, db_column='city_seo_description')),
                ('citySeoKeywordContent',
                 models.CharField(max_length=500, db_column='city_seo_keyword')),
                ('city', models.ForeignKey(to='dbcommon.City', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_cityseodetails',
            },
        ),
        migrations.CreateModel(
            name='SeoDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('titleDescription',
                 models.CharField(max_length=50, db_column='title_description')),
                ('metaContent', models.CharField(max_length=500, db_column='meta_content')),
                ('hotel', models.ForeignKey(to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_seodetails',
            },
        ),
    ]
