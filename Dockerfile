# FROM directive instructing base image to build upon
# FROM docker-hub-m.treebo.com:5000/treebo-python3.5:v1

FROM python:3.5

ARG req_file=requirements.txt
RUN mkdir -p /usr/src/app/

COPY $req_file /usr/src/app/requirements.txt
COPY requirements/base.txt /usr/src/app/base.txt
RUN pip install -r /usr/src/app/requirements.txt

RUN mkdir -p /usr/src/scripts/ \
    && mkdir -p /var/log/website/ \
    && mkdir -p /usr/src/app/webapp/dist/ \
    && mkdir -p /usr/src/app/webapp/static_pipeline/ \
    && mkdir -p /usr/src/app/webapp/static/files/ \
    && mkdir -p /var/image/image_upload/ \
    && mkdir -p /var/data/dynamicads/ \
    && mkdir -p /var/data/trivago/ \
    && mkdir -p /usr/src/app/webapp/static/ \
    && mkdir -p /usr/src/app/webapp/dist/desktop/ \
    && mkdir -p /usr/src/app/webapp/dist/mobile/ \
    && touch /usr/src/gunicorn.pid \
    && touch /usr/src/gunicorn.sock \
    && apt-get update && apt-get install -y \
            curl \
            xvfb \
            fontconfig \
            dbus \
            software-properties-common \
            npm

RUN apt-get install wkhtmltopdf xvfb xauth xfonts-base xfonts-75dpi fontconfig -y && \
    echo 'xvfb-run --server-args="-screen 0, 1024x768x24" /usr/bin/wkhtmltopdf $*' > /usr/bin/wkhtmltopdf.sh && \
       chmod a+rx /usr/bin/wkhtmltopdf.sh && \
       ln -s /usr/bin/wkhtmltopdf.sh /usr/local/sbin/wkhtmltopdf



RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
RUN node -v
RUN npm -v

COPY . /usr/src/app
COPY scripts /usr/src/scripts

WORKDIR /usr/src/app/
RUN npm run setup

VOLUME /var/run/docker.sock
ENV PYTHONPATH $PYTHONPATH:/usr/src/app/

EXPOSE 8000