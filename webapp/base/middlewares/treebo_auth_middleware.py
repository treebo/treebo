import logging

from django.contrib.auth.models import AnonymousUser
from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.common.exceptions.custom_exception import InvalidTokenException, AuthException
from apps.profiles.service.user_registration import UserRegistration
from base.token_helper import TokenHelper
from dbcommon.models.profile import User

logger = logging.getLogger(__name__)


class TreeboAuthMiddleWare(object):
    auth_service = TreeboAuthClient()
    user_service = UserRegistration()
    token_helper = TokenHelper()

    def process_request(self, request):

        if not (hasattr(request, 'user')
                and request.user and request.user.is_authenticated()):
            access_token = self.token_helper.get_token(request)
            user = None
            if access_token:
                user, access_token = self.fetch_user_from_token(access_token)
            if not user:
                user = AnonymousUser()
            user.token = access_token
            request.user = request._cached_user = user
            self.token_helper.set_token(request, access_token)
        else:
            logger.info(
                " treebo auth middleware , user is already set %s ", str(
                    request.user))

    def process_response(self, request, response):
        token = ''
        if hasattr(
                request,
                'user') and request.user and request.user.is_authenticated():
            logger.info(
                " treebo auth middleware ret user is authentiated %s ",
                request.user.id)
            token = request.user.token
            if token and not str(token).startswith('Bearer'):
                token = "Bearer " + str(token)
        response.set_cookie('token', token)
        response['X-Auth-Token'] = token
        return response

    def fetch_user_from_token(self, access_token):
        user = None
        try:
            try:
                user_data = self.auth_service.validate_token(access_token)
            except InvalidTokenException as e:
                old_token = access_token
                access_token = self.token_helper.refresh_and_update_token(
                    old_token)
                logger.info(
                    " token %s got expired and new token is %s ",
                    old_token,
                    access_token)
                if access_token:
                    user_data = self.auth_service.validate_token(access_token)
                else:
                    user_data = {}
            if user_data:
                user = UserRegistration().register(
                    user_data['id'],
                    user_data['email'],
                    user_data['phone_number'],
                    user_data['first_name'],
                    user_data['last_name'])
        except AuthException as e:
            logger.error("auth exception in middleware %s ", access_token)
        except Exception as e:
            logger.exception(
                "error in user middleware token invalid %s ",
                access_token)
        return user, access_token
