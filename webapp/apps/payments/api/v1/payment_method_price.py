# -*- coding:utf-8 -*-
"""
    payment_detailsPaymentInfo  API
"""
from base.logging_decorator import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger

logger = get_booking_logger(__name__)


class PaymentMethodPrice(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = []

    @log_args(logger)
    def get(self, request):
        try:
            payment_method_id = request.GET['payment_method_id']

        except IndexError as exp:
            msg = "Wrong request recieved", exp
            logger.exception(msg)
            return {"message": msg}
        except Exception as e:
            pass
