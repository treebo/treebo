import logging
from http import HTTPStatus
from base.views.api import TreeboAPIView
from rest_framework import serializers
from base.decorator.request_validator import validate_request
from apps.promotions.services.promotion_service import PromotionsService
from apps.common.api_response import APIResponse as api_response
from apps.common.slack_alert import SlackAlertService as slack_alert
logger = logging.getLogger(__name__)


class MetaRedirectionSerializer(serializers.Serializer):
    hotel_cs_id = serializers.CharField(required=True)


class MetaRedirectionType(TreeboAPIView):
    promoted_service = PromotionsService()

    @validate_request(MetaRedirectionSerializer)
    def get(self, request, *args, **kwargs):
        try:
            validated_data = kwargs['validated_data']
            hotel_cs_id = validated_data['hotel_cs_id']
            redirection_type = self.promoted_service.get_hotel_redirection_type(hotel_cs_id)
            response = api_response.prep_success_response({"redirection_type": redirection_type})
        except Exception as exception:
            logger.exception('Exception occurred in Meta Redirection type with error {}',format(exception.__str__))
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param=request.GET,
                                                        dev_msg="Meta Redirection Type error",
                                                        message=exception.__str__(),
                                                        class_name=self.__class__.__name__)
            response = api_response.prep_simple_error_response(msg="Meta Redirection Type error",
                                                               status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                               code=HTTPStatus.INTERNAL_SERVER_ERRO)

        return response
