import logging

from apps.reviews.models import TAAPIAuditTable, TAHotelIDMapping, ReviewAppConfiguration
from apps.reviews.services.trip_advisor.review_config_service import ReviewAppConfigService

logger = logging.getLogger(__name__)


class APIAuditService(object):
    def create_new_audit(self, location, user_reviews, hotel_loc_id):
        try:
            config = ReviewAppConfiguration.objects.all().first()
            review_config = ReviewAppConfigService()
            if review_config.is_audit_enabled(config):
                ta_audit = TAAPIAuditTable()
                hotel_ta_loc_mapping = TAHotelIDMapping.objects.filter(
                    ta_location_id=hotel_loc_id).first()
                if hotel_ta_loc_mapping:
                    logger.info("Audit Enable and writing for ta_loc_id  %s and hotel id  %s", hotel_loc_id,
                                hotel_ta_loc_mapping.hotel.id)

                    ta_audit.hotel_ta_mapping = hotel_ta_loc_mapping
                    ta_audit.location_api_url = location.url
                    ta_audit.location_api_url_response_json = location.response
                    if location.status == 200:
                        ta_audit.location_api_url_response_status = TAAPIAuditTable.SUCCESS
                    else:
                        ta_audit.location_api_url_response_status = TAAPIAuditTable.FAILURE

                    ta_audit.user_location_reviews_api_url = user_reviews.url
                    ta_audit.user_location_reviews_api_url_response_json = user_reviews.response
                    if user_reviews.status == 200:
                        ta_audit.user_location_reviews_api_url_response_status = TAAPIAuditTable.SUCCESS
                    else:
                        ta_audit.user_location_reviews_api_url_response_status = TAAPIAuditTable.FAILURE
                    ta_audit.save()
                else:
                    logger.error(
                        "no mapping exists for such location id %s",
                        hotel_loc_id)
        except Exception:
            logger.exception(
                "Exception in creating new entry in audit table for %s",
                hotel_loc_id)
