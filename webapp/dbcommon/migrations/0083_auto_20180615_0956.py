# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0082_auto_20180425_1225'),
    ]

    operations = [
        migrations.AddField(
            model_name='locality',
            name='is_hotel_present',
            field=models.NullBooleanField(default=True),
        ),
        migrations.AddField(
            model_name='searchlandmark',
            name='is_hotel_present',
            field=models.NullBooleanField(default=True),
        ),
    ]
