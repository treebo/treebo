import logging

from django.core.cache import cache
from rest_framework.response import Response

from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class HotelStatus(TreeboAPIView):
    hotel_repository = RepositoriesFactory.get_hotel_repository()

    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'cs_apicache_hotels_status'

    def get_cache_key(self):
        return HotelStatus.CACHE_KEY

    @staticmethod
    def invalidate_cache():
        cache.delete(HotelStatus.CACHE_KEY)

    def get(self, request, *args, **kwargs):
        """
        Gives active status of all hotels
        """
        try:
            result = cache.get(self.get_cache_key())
        except Exception as e:
            result = None

        if result:
            return Response(result)

        # WARNING: Explicitly using base_manager here to return data for
        # inactive hotels also.
        hotels_list = self.hotel_repository.get_all_hotels()
        hotels = {hotel.id: True if hotel.status ==
                  1 else False for hotel in hotels_list}
        cache.set(self.get_cache_key(), hotels, HotelStatus.CACHE_TIMEOUT)
        return Response(hotels)
