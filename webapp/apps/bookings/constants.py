# coding=utf-8
"""
    Booking Flow constants
"""

BOOKING_ALREADY_PROCESSED = (
    "BOOKING_ALREADY_PROCESSED",
    "Booking for the given id already processed/in-process.")
BOOKING_PROCESS_FAILED = (
    "BOOKING_PROCESS_FAILED",
    "Booking processing failed.")
BOOKING_NOT_FOUND = ("BOOKING_NOT_FOUND", "Booking for given id not found.")
BOOKING_PROCESS_SUCCESS = (
    "BOOKING_PROCESS_SUCCESS",
    "Booking process added successfully.")
INVALID_BOOKING_AMOUNT = (
    "INVALID_BOOKING_AMOUNT",
    "Booking amount is invalid.")

WALLET_GATEWAY_TYPE = "treebo_wallet"
RAZORPAY_GATEWAY_TYPE = 'razorpay'
AMAZON_GATEWAY_TYPE = 'amazonpay'
PHONEPE_APP_GATEWAY_TYPE = 'phonepe_app'
PHONEPE_GATEWAY_TYPE = 'phonepe'
WALLET_SUCCESS_STATUS = "CAPTURED"
HOMESTAY_PROPERTY_TYPE = "homestay"
HOMESTAY_PROVIDER_WT = "wandertrails"

CATALOGUING_SERVICE_APPLICATION_URL = "api/channels/direct/applications/"
CATALOGUING_SERVICE_SUB_CHANNELS_URL = "api/channels/direct/subchannels/"
CATALOGUING_SERVICE_ROOM_TYPES_URL = "api/roomtypes/"
CS_OTHER_SUB_CHANNEL = 'OTHERS_DIRECT'
CS_OTHER_APPLICATION = 'Others'

# 0 -> Prepaid, 1 -> Pay At Hotel, 2 -> Partial Payment  => Axis Room API
PAYMENT_STATUS_PREPAID = 0
PAYMENT_STATUS_PAY_AT_HOTEL = 1
PAYMENT_STATUS_PARTIAL_PAYMENT = 2
PAYMENT_OFFER_GATEWAY = 'payment_offer'
THSC_CONTEXT_USER = 'Web Backend'
THSC_CONTEXT_DEFAULT_APPLICATION = 'Default Application'

PAY_AT_HOTEL = 'Web (Pay@hotel)'
PAY_NOW = 'Web (Prepaid)'


class AsyncJobStates(object):
    """
        easyjoblite_status
    """
    CREATED = 'CREATED'
    OPEN = 'OPEN'
    SUCCESS = 'SUCCESS'
    FAILED = 'FAILED'
    FAILURE_HANDLE_INIT = 'FAILURE_HANDLE_INIT'
    FAILURE_HANDLE_INIT_MAILER = 'FAILURE_HANDLE_INIT_MAILER'
    FAILURE_HANDLED = 'FAILURE_HANDLED'


INITIATE_BOOKING_TAG = ["initiate_booking"]
BOOKING_TAGS_LIST = ["confirm_booking", "create_booking"]
PAYMENT_TAGS_LIST = ["update_payment"]
GST_TAGS_LIST = ["update_gst"]
AGGREGATE_FAILURE_EMAIL_SUBJECT = {"URGENT": "URGENT: ", "CREATE_BOOKING": "Create Booking, ",
                                   "PAYMENT": "Payment Update, ", "GST": "GST Update, ",
                                   "CRS": "Failed For CRS booking order ID: ",
                                   "HX": "Failed For Hx booking order ID: "}
STOP_EMAILS = "stop_emails"
STOP_FAILURE_EMAILS = "stop_failure_emails"
