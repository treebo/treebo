import datetime

from ths_common.constants.scheduled_job_constants import JobStatus

from common.job.job_entity import JobEntity
from common.job.models import JobModel
from django.db import transaction


class JobAdapter:
    @staticmethod
    def to_db_model(job_entity, update=False):
        job = JobModel.objects.filter(job_id=job_entity.job_id).first() if update else JobModel()
        job.crs_booking_id = job_entity.crs_booking_id
        job.serialized_job = job_entity.data
        job.job_name = job_entity.job_name
        job.eta = job_entity.eta
        job.generated_at = job_entity.generated_at
        job.job_id = job_entity.job_id
        job.status = job_entity.status.value
        job.picked_at = job_entity.picked_at
        job.failure_message = job_entity.failure_message
        job.total_tries = job_entity.total_tries

        return job

    @staticmethod
    def to_entity(job_model):
        if not job_model:
            return None
        return JobEntity(job_model.job_name, job_model.crs_booking_id,
                         job_model.serialized_job,
                         job_model.eta, generated_at=job_model.generated_at, job_id=job_model.job_id,
                         status=JobStatus(job_model.status),
                         picked_at=job_model.picked_at, failure_message=job_model.failure_message,
                         total_tries=job_model.total_tries)


class JobRepository:

    @staticmethod
    @transaction.atomic
    def get_oldest_schedulable_job():
        query = JobModel.objects.select_for_update().filter(status=JobStatus.CREATED.value, eta__lte=datetime.datetime.utcnow())
        return JobAdapter.to_entity(job_model=query.first())

    @staticmethod
    def get_job(job_id):
        job_model = JobModel.objects.filter(job_id=job_id).first()
        return JobAdapter.to_entity(job_model)

    @staticmethod
    def save(job_entity):
        job_model = JobAdapter.to_db_model(job_entity)
        job_model.save()
        return JobAdapter.to_entity(job_model)

    @staticmethod
    def update(job_entity):
        job_model = JobAdapter.to_db_model(job_entity, update=True)
        job_model.save()
        return JobAdapter.to_entity(job_model)

    @staticmethod
    def get_all_jobs(job_name=None):
        job_models = JobModel.objects.filter(job_name=job_name).all()
        return [JobAdapter.to_entity(job_model) for job_model in job_models]

    @staticmethod
    def update_status(job_entity, success, failure_message=None):
        status = JobStatus.FINISHED if success else JobStatus.FAILED
        job_entity.status = status
        job_entity.failure_message = failure_message
        job_entity.total_tries += 1
        return job_entity

    @staticmethod
    def is_booking_present(crs_booking_id):
        job_model = JobModel.objects.filter(crs_booking_id=crs_booking_id)
        if job_model.all():
            return True
        else:
            return False
