

from django.conf.urls import url

from apps.bookings.api.v3.failure_mail_aggregate_cron import FailureMailAggregateCron
from apps.bookings.api.v3.retry_failed_bookings import RetryFailedBookings
from apps.bookings.api.v3.update_gst_detail import UpdateGSTDetail

app_name = 'bookings_v3'
urlpatterns = [
    url(r"^retry-failed-bookings/$", RetryFailedBookings.as_view(), name='retry-failed-bookings'),
    url(r"^update-gst-detail/$", UpdateGSTDetail.as_view(), name='update-gst-detail'),
    url(r"^aggregate-failure-email/$", FailureMailAggregateCron.as_view(),
        name="aggregate-failure-email")
]
