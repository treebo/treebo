insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'check_in_time','Standard Check-In Time','12:00 PM',TRUE ,TRUE ,TRUE );

insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'check_out_time','Standard Check-Out Time','11:00 AM',TRUE ,TRUE ,TRUE );

insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'early_check_in','Early Check-In Policy','Early check-in between 10 am to 12 pm is possible (free of charge) but can be confirmed, subject to availability, only upon arrival at the hotel and not in advance. For check-in prior to 10 AM, an extra night''s tariff shall be applicable.',TRUE ,TRUE ,TRUE );

insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'late_check_out_policy','Late Check-Out Policy','Late check-out between 12 pm and 1 pm is possible (free of charge) but can be confirmed, subject to availability, only at the time of check-out and not in advance. For check-out after 1 PM, an extra night''s tariff shall be applicable.',TRUE ,TRUE ,TRUE );

insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'child_reservation_policy','Child Reservation Policy','A maximum of 1 child upto an age of 8 years and an extra infant upto an age of 2 years is allowed free of charge, subject to room occupancy capacity. Breakfast will be included in stay but no extra bed or mattress will be provided. Extra person charges are applicable for anyone above 8 years of age. Extra person charges are also applicable for extra kids during check-in.',TRUE ,TRUE ,TRUE );

insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'identification_card','Identification Card','All adults must carry one of these photo ID cards at the time of check-in: Driving License, Voters Card, Passport, Ration Card or Aadhar Card. PAN Cards are not accepted.',TRUE ,TRUE ,TRUE );

insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'cancellation_policy','Cancellation Policy','No cancellation fee is charged if the booking is cancelled 24 hours prior to the standard check-in time.',TRUE ,TRUE ,TRUE );

insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'others_policy','Other Policies','Our hotels reserve the right of admission to ensure safety and comfort of guests. This may include cases such as local residents, unmarried and unrelated couples among others.',TRUE ,TRUE ,TRUE );

insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'is_local_id_allowed','Local ID''s Policy','Guests can check in using any local or outstation ID proof (PAN card not accepted).',TRUE ,TRUE ,FALSE );

insert into hotels_policy (created_at,modified_at,policy_type,title,description,display_in_need_to_know,display_in_policy,is_global)
values (Now(),now(),'is_couple_friendly','Couple Friendly','welcomes Couples.',TRUE ,TRUE ,FALSE );

insert into hotels_hotel_policy_map(created_at,modified_at,hotel_id_id,policy_id_id) VALUES (Now(),now(),84,9);
insert into hotels_hotel_policy_map(created_at,modified_at,hotel_id_id,policy_id_id) VALUES (Now(),now(),84,10);

