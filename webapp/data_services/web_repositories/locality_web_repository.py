from base import log_args
from dbcommon.models.location import Locality


class LocalityWebRepository(object):

    @log_args()
    def get_all_localities(self):
        return Locality.objects.all()

    @log_args()
    def filter_localities(self, *args, **kwargs):
        return Locality.objects.filter(**kwargs)

    @log_args()
    def get_single_locality(self, *args, **kwargs):
        return Locality.objects.get(**kwargs)
