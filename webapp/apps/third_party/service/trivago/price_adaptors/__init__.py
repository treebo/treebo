import abc
import logging

logger = logging.getLogger(__name__)


class TrivagoPriceAdaptor(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def hit_and_fetch_prices_for_room(self, hotel_data_params, hotel):
        return

    @abc.abstractmethod
    def calculate_prices_as_per_trivago(self, room_details, hotel_data_params):
        return
