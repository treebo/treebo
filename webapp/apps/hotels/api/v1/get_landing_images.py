__author__ = 'Rajdeep'

from base.views.api import TreeboAPIView


class BannerInformation(TreeboAPIView):
    @staticmethod
    def getBannerInformation():
        result = []
        singleBannerItem = {}
        singleBannerItem[
            'target_url'] = "//www.treebo.com/search/?query=delhi,%20jaipur,%20chennai&lat=&long=&locality="
        singleBannerItem[
            'image_url'] = "//s3-ap-southeast-1.amazonaws.com/treebo/static/images/26janhp2.jpg"
        result.append(singleBannerItem)

        return result
