from django.core.management.base import BaseCommand

from dbcommon.models.miscellanous import Image


class Command(BaseCommand):
    help = 'Update category to room if image belongs to room'

    def handle(self, *args, **options):
        self.update_category()

    @staticmethod
    def update_category():
        # Images having hotel_id and room_id and which are currently having category=others
        # are being updated to category=room, We are updating category of images belonging to others
        # category only because some images having room_id and hotel_id could belong to washroom
        # category and those should not be changed.
        Image.objects.filter(hotel_id__isnull=False, room_id__isnull=False,
                             url__isnull=False, category="Others").update(category='Room')
