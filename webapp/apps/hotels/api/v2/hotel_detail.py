import logging

from django.core.cache import cache
from rest_framework.response import Response
from apps.hotels.hotel_detail_utilities import get_categorised_images, get_hotel_directions, \
    get_categorised_guest_supported_info, get_user_reviews
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from services.restclient.contentservicerestclient import ContentServiceClient
from data_services.exceptions import HotelDoesNotExist

logger = logging.getLogger(__name__)

hotel_repository = RepositoriesFactory.get_hotel_repository()
facility_repository = RepositoriesFactory.get_facility_repository()
image_repository = RepositoriesFactory.get_image_repository()

class HotelDetail(TreeboAPIView):
    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'cs_apicache_hotel_detail_{0}'

    def get_cache_key(self, hotel_id):
        return HotelDetail.CACHE_KEY.format(hotel_id)

    @staticmethod
    def invalidate_cache(hotel_id):
        if hotel_id:
            cache.delete(HotelDetail.CACHE_KEY.format(hotel_id))
        else:
            cache.delete_pattern(HotelDetail.CACHE_KEY.format('*'))

    def get(self, request, *args, **kwargs):
        """
        Model Dependencies: Hotel, Facility, Image, Room, ContentStore.
        Gives Hotel details.
        """
        hotel_id = int(kwargs.get('hotel_id'))
        try:
            result = cache.get(self.get_cache_key(hotel_id))
        except Exception as e:
            logger.error(e)
            result = None
        if result:
            return Response(result)

        try:
            hotel = hotel_repository.get_hotel_details_with_all_related_objects(
                hotel_id)
        except HotelDoesNotExist:
            hotel_essential_information = {
                "is_active": False,
            }
            return Response(hotel_essential_information)
        hotel_images = hotel_repository.get_image_set_for_hotel(hotel)
        rooms_set = hotel_repository.get_rooms_for_hotel(hotel)
        directions_set = hotel_repository.get_directions_set_for_hotel(hotel)
        facility_set = hotel_repository.get_facilities_for_hotel(hotel)
        images = get_categorised_images(hotel_images)
        rooms = get_categorised_guest_supported_info(rooms_set)
        directions_css_dist_list = get_hotel_directions(directions_set)
        promises = ContentServiceClient.getValueForKey(
            request, 'promises_list', 1)
        policies = ContentServiceClient.getValueForKey(
            request, 'policies_list', 1)
        reviews = get_user_reviews(hotel)
        hotel_facilities_list = [
            {"name": facility.name, "css_class": facility.css_class}
            for facility in facility_set
        ]

        hotel_essential_information = {
            "id": hotel_id,
            "name": hotel.name,
            "phone": hotel.phone_number,
            "is_active": True if hotel.status == 1 else False,
            "address": {
                "city": hotel.city.name,
                "city_id": hotel.city.id,
                "street": hotel.street,
                "locality": hotel.locality.name,
                "landmark": hotel.landmark,
                "state": hotel.state.name,
                "pincode": hotel.locality.pincode},
            "coordinates": {
                "lat": hotel.latitude,
                "lng": hotel.longitude},
            "description": hotel.description,
            "images": images,
            "promises": promises,
            "rooms": rooms,
            "policies": policies,
            "facilities": hotel_facilities_list,
            "trilights": [
                highlight.description for highlight in hotel_repository.get_highlights_for_hotel(hotel)],
            "accessibilities": directions_css_dist_list,
            "reviews": reviews,
        }

        cache.set(self.get_cache_key(hotel_id), hotel_essential_information,
                  HotelDetail.CACHE_TIMEOUT)
        return Response(hotel_essential_information)