import logging
from datetime import datetime

import requests
from django.conf import settings
from django.core.validators import RegexValidator
from django.http import JsonResponse
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

from apps.bookingstash.utils import StashUtils
from apps.common import date_time_utils
from apps.common.utils import round_to_nearest_integer
from apps.pricing.dateutils import date_to_ymd_str, ymd_str_to_date
from apps.pricing.api.v4 import RoomPrice
from apps.pricing.exceptions import SoaError, RequestError, CheckinDateError
from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.pricing_api_impl import PriceUtilsV2
from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.pricing.transformers.search_api_transformer import SearchAPITransformer
from apps.pricing.utils import PriceUtils
from base.views import validators
from base.views.api import TreeboAPIView
from dbcommon.models.hotel import Hotel
from apps.common.slack_alert import SlackAlertService as slack_alert

from apps.pricing.services.price_request_dto import PriceRequestDto

logger = logging.getLogger(__name__)

def validate_checkin_checkout(checkin, checkout):
    if checkin >= checkout:
         raise CheckinDateError("Checkin Date is Greater than or qual to Checkout date", HTTP_400_BAD_REQUEST)


class PricingSerializer(serializers.Serializer):
    hotel_id = serializers.CharField(
        max_length=1000,
        validators=[
            RegexValidator(
                regex='^(?:\d+,?)+$',
                message='Invalid value for hotel_id parameter. '
                'Please provide a comma-separated list of hotel_ids',
                code='invalid_hotel_ids')])
    checkin = serializers.DateField()
    checkout = serializers.DateField()
    roomconfig = serializers.CharField(
        max_length=100, validators=[
            validators.validate_roomconfig])
    apply_wallet = serializers.BooleanField(
        default=settings.APPLY_WALLET, required=False)


class CheapestPriceAPIView(TreeboAPIView):
    pricing_service = PricingServiceV2()
    validationSerializer = PricingSerializer
    price_response_transformer = SearchAPITransformer()

    def get(self, request, *args, **kwargs):
        try:
            hotel_ids = request.GET.get('hotel_id').split(",")
            check_in = request.GET.get('checkin')
            check_out = request.GET.get('checkout')
            validate_checkin_checkout(check_in, check_out)
            room_config = request.GET.get('roomconfig')
            validated_data = self.serializerObject.validated_data
            user = request.user
            apply_wallet = bool(validated_data['apply_wallet'])
            checkin_date, checkout_date, room_config = validated_data[
                'checkin'], validated_data['checkout'], validated_data['roomconfig']

            # StashUtils.room_config_string_to_list(room_config)
            room_config_list = room_config.split(",")
            checkin_date = ymd_str_to_date(check_in)
            checkout_date = ymd_str_to_date(check_out)
        except Exception as e:
            return JsonResponse(
                dict(
                    error=dict(
                        code=HTTP_400_BAD_REQUEST,
                        msg=e.message)),
                status=HTTP_400_BAD_REQUEST)

        if not len(hotel_ids):
            return JsonResponse(
                dict(
                    error=dict(
                        code=HTTP_400_BAD_REQUEST,
                        msg="hotels should not none")),
                status=HTTP_400_BAD_REQUEST)

        logged_in_user = hasattr(
            request, 'user') and request.user and request.user.is_authenticated()
        try:
            price_request_dto = PriceRequestDto(
                checkin_date,
                checkout_date,
                hotel_ids,
                room_config_list,
                logged_in_user=logged_in_user)

            hotel_wise_cumulative_prices = self.pricing_service.get_search_page_price(
                price_request_dto)

            transformed_price = self.price_response_transformer.transform_pricing_for_display(
                hotel_wise_cumulative_prices,
                hotel_ids,
                room_config,
                checkin_date,
                checkout_date,
                user,
                apply_wallet)

            return Response(transformed_price)
        except SoaError as e:
            logger.exception("Error fetching price for search page")
            return JsonResponse(
                {"error": {"code": e.status, "msg": e.msg}}, status=HTTP_400_BAD_REQUEST)
        except RequestError as e:
            logger.exception("Error fetching price for search page")
            return JsonResponse(
                {"error": {"code": e.status, "msg": str(e)}}, status=HTTP_400_BAD_REQUEST)

        except Exception as e:
            logger.exception("Error fetching price for search page")
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        dev_msg="WEB Pricing V4", message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            return JsonResponse(
                {
                    "error": {
                        "code": HTTP_500_INTERNAL_SERVER_ERROR,
                        "msg": str(e)}},
                status=HTTP_500_INTERNAL_SERVER_ERROR)
