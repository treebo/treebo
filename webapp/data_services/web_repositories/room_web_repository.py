from base import log_args
from dbcommon.models.room import Room
from django.core.exceptions import ObjectDoesNotExist
import collections


class RoomWebRepository(object):

    @log_args()
    def get_all_rooms(self):
        return Room.objects.all()

    @log_args()
    def get_single_Room(self, *args, **kwargs):
        try:
            return Room.objects.get(**kwargs)
        except Exception as e:
            raise RoomDoesNotExist

    def get_rooms_for_hotel(self, hotel_id):
        rooms = Room.objects.filter(hotel_id=hotel_id)
        return rooms

    def get_room_types_for_hotel(self, hotel_id):
        return Room.objects.filter(hotel_id=hotel_id).values_list('room_type_code', flat=True)


    @log_args()
    def filter_rooms(self, *args, **kwargs):
        return Room.objects.filter(**kwargs)

    @log_args()
    def filter_room_by_string_matching(self, fields_list, filter_params):
        from django.db.models import Q
        query = Q()
        for field in fields_list:
            field_name = field['name']
            field_value = field['value']
            case_sensitive = field['case_sensitive']
            partial = field['partial']
            search_string_suffix = get_query_string(
                case_sensitive=case_sensitive, partial=partial)
            if search_string_suffix:
                field_name = field_name + '__' + search_string_suffix
            query = query | Q(**{field_name: field_value})
        if filter_params:
            query = query & Q(**filter_params)
        return Room.objects.filter(query)

    @log_args()
    def get_room_with_related_entities(
            self,
            select_list,
            prefetch_list,
            filter_params):
        query = Room.objects
        if select_list:
            query = query.select_related(*select_list)
        if prefetch_list:
            query = query.prefetch_related(*prefetch_list)
        if filter_params:
            return query.filter(**filter_params)
        return query.all()

    def get_roomtypes_in_hotel(self, hotel_ids):
        rm_types = Room.objects.filter(hotel__id__in=hotel_ids)
        hotel_room_type = collections.defaultdict(dict)
        for rm_type in rm_types:
            ht_rec = hotel_room_type[rm_type.hotel.id]
            ht_rec[rm_type.room_type_code.lower()] = rm_type.id
            hotel_room_type[rm_type.hotel.id] = ht_rec
        return hotel_room_type

    @staticmethod
    def get_external_room_details(hotel_id, room_type_code):
        try:
            room_object = Room.objects.filter(hotel__id=hotel_id, room_type_code=room_type_code)\
                .values('external_room_details__name', 'description').first() or {}
        except ObjectDoesNotExist as e:
            raise e
        return room_object


# TODO: Move to common plae
def get_query_string(case_sensitive, partial):
    if case_sensitive and partial:
        return 'contains'
    if not case_sensitive and partial:
        return 'icontains'
    if case_sensitive and not partial:
        return 'exact'
    if not case_sensitive and not partial:
        return 'iexact'
    return None


class RoomDoesNotExist(Exception):
    pass
