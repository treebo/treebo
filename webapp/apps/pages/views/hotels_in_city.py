import json
import logging
from datetime import datetime, timedelta

from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect, Http404
from django.template.defaultfilters import slugify
from django.utils import dateformat

import common.constants.common_constants as const
from apps.common import date_time_utils
from apps.featuregate.manager import FeatureManager
from apps.featuregate.models import Page
from apps.hotels import utils as hotel_utils
from apps.hotels.helpers import CityHotelHelper
from apps.hotels.service.hotel_service import HotelEnquiryValidator
from apps.pages.views.search import Search
from base.views.template import TreeboTemplateView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboValidationException
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.facilities import Facility
from dbcommon.models.hotel import CitySeoDetails
from dbcommon.models.location import City, CityAlias
from services import maps
from services.restclient.contentservicerestclient import ContentServiceClient
from services.restclient.searchclient import SearchClient

logger = logging.getLogger(__name__)


class HotelsInCity(TreeboTemplateView):
    template_name = "searchresults/index.html"

    hotel_details_suffix = ''
    default_search_url = ''
    room_config_str = ''
    hotel_page = 'searchhotel'

    def render_to_response(self, context, **response_kwargs):
        response = super(
            HotelsInCity,
            self).render_to_response(
            context,
            **response_kwargs)
        if 'queryParams' in context and 'checkIn' in context['queryParams']:
            response.set_cookie(
                'checkin', context['queryParams']['checkInCookie'])
        if 'queryParams' in context and 'checkOut' in context['queryParams']:
            response.set_cookie('checkout',
                                context['queryParams']['checkOutCookie'])
        return response

    def __init__(self, **kwargs):
        super(HotelsInCity, self).__init__(**kwargs)
        self.__myMap = maps.Maps()

    def _get_common_data(self, request, args, kwargs):
        city_data_service = RepositoriesFactory.get_city_repository()
        city_string = kwargs.get('cityString', '').replace('-', ' ')
        city_search_args = []
        city_search_args.append({'name': 'name',
                                 'value': city_string,
                                 'case_sensitive': False,
                                 'partial': True})
        current_cities = city_data_service.filter_cities_by_string_matching(
            city_search_args)
        if current_cities:
            city = current_cities[0]
        else:
            city = None
        #city = City.objects.filter(name__icontains=city_string)

        if len(city) is 0:
            city_alias = CityAlias.objects.filter(
                alias__icontains=city_string).prefetch_related('city').first()
            if city_alias:
                hotels_in_city_url = reverse(
                    'pages:hotels_in_city', kwargs={
                        'cityString': city_alias.city.lower()})
                return HttpResponseRedirect(hotels_in_city_url)
            else:
                raise Http404
        nearby_result = []
        search_string, check_in_date, check_out_date, rooms_config, latitude, longitude, locality = self.__parse_request(
            request, kwargs)
        room_config_string = request.GET.get('roomconfig', '1-0')
        search_string = search_string.replace('-', ' ')
        logger.debug('Search string: %s', search_string)
        rooms_required = len(rooms_config)
        guest = 0
        for room_config in rooms_config:
            guest = guest + room_config[0]

        try:
            HotelEnquiryValidator(
                check_in_date,
                check_out_date,
                rooms_config).validate()
        except TreeboValidationException as e:
            logger.warn("Invalid search request received")
            if e.code == error_codes.INVALID_DATES:
                return HttpResponseRedirect(self.default_search_url)
            raise e

        search_results = SearchClient.get_search_results(
            request,
            search_string,
            check_in_date,
            check_out_date,
            room_config_string,
            latitude,
            longitude,
            locality)
        hotels = search_results['hotels']
        page = Page.objects.filter(page_name=self.hotel_page).first()

        for hotel in hotels:
            kwargs = hotel_utils.get_hotel_url_kwargs_from_dict(hotel)
            hotel['hotelUrl'] = reverse(
                'pages:hotel-details-new',
                kwargs=kwargs) + self.hotel_details_suffix + '&roomtype=' + hotel['cheapest_room_type']
            if page:
                feature_manager = FeatureManager(
                    page, check_in_date, check_out_date, hotel.id, '', '', None)
                overridden_values = feature_manager.get_overriding_values()
            else:
                overridden_values = {}
            for overriddenKey in list(overridden_values.keys()):
                hotel[overriddenKey] = overridden_values[overriddenKey]

        facilities = list(Facility.objects.values_list('name', flat=True))
        is_nearby = False

        context = self.__generateContext(
            check_in_date,
            check_out_date,
            guest,
            latitude,
            locality,
            longitude,
            hotels,
            nearby_result,
            rooms_required,
            search_string,
            search_results['allLocalityList'],
            facilities,
            is_nearby,
            search_results['parentQuery'],
            search_results['isLocalitySearch'],
            search_results['cityOrStateOrHotelNotSearchedExplicitly'],
            search_results['localityName'])

        context['googleMaps'] = True
        context['jsvars'] = str(json.dumps(
            {
                'locality': search_results['localityName'],
                'city': search_results['cityName'],
                'isHotelInCityPage': True
            }))

        city_name = search_results['cityName']
        city_slug = slugify(city_name)
        seoKey = "search_" + city_slug + "_" + "desc"
        try:
            seo_data = ContentServiceClient.getValueForKey(request, seoKey, 1)
            seo_data = seo_data['text']
        except Exception as exc:
            seo_data = ""

        context['cityData'] = seo_data
        context['cityName'] = city_name
        seo_city_title = ""
        seo_city_description = ""
        seo_city_keyword = ""
        try:
            seo_city = CitySeoDetails.objects.get(
                city__name__icontains=city_name)
            seo_city_title = seo_city.citySeoTitleContent
            seo_city_description = seo_city.citySeoDescriptionContent
            seo_city_keyword = seo_city.citySeoKeywordContent
        except Exception as e:
            logger.debug(e)

        context['seo'] = {
            "canonical": request.build_absolute_uri(
                "/hotels-in-" + city_slug + "/"),
            "title": seo_city_title,
            "description": seo_city_description,
            "keywords": seo_city_keyword}
        return context

    def getData(self, request, args, kwargs):
        context = self._get_common_data(request, args, kwargs)
        context['styles'] = ['desktop/css/searchresults.css']
        context['scripts'] = ['desktop/js/searchresults.js']
        return context

    def getMobileData(self, request, args, kwargs):
        context = self._get_common_data(request, args, kwargs)
        context['styles'] = ['mobile/css/searchresults.css']
        context['scripts'] = ['mobile/js/searchresults.js']
        return context

    def __generateContext(
            self,
            check_in_date,
            check_out_date,
            guest,
            latitude,
            locality,
            longitude,
            result,
            nearbyResult,
            rooms_required,
            search_string,
            localities,
            facilities,
            is_nearby,
            parent_query,
            is_locality_search,
            is_not_city_state_hotel_search,
            locality_name):
        sorted_city_details = CityHotelHelper.getCityHotels()
        sort_array = ['price', 'distance', 'recommend']
        sort_options = {
            'price': {'value': 'Price', 'selected': False},
            'recommend': {'value': 'Recommendation', 'selected': False},
            'distance': {'value': 'Distance', 'selected': True}
        }
        if is_not_city_state_hotel_search is False and is_locality_search is False:
            sort_array = ['price', 'recommend']
            sort_options = {
                'price': {'value': 'Price', 'selected': True},
                'recommend': {'value': 'Recommendation', 'selected': False}
            }

        filter_data = {
            'sortddVisibility': False,
            'filterddVisibility': False,
            'sortArr': sort_array,
            'sortOptions': sort_options,
            'filterArr': ['budget', 'locality', 'amenity', 'category'],
            'filters': {
                'budget': [{'selected': False, 'key': [0, 1500], 'value': '0-1500'},
                           {'selected': False, 'key': [1501, 3000], 'value': '1500-3000'},
                           {'selected': False, 'key': [3001, 300000], 'value': 'above 3000'}],
                'locality': localities,
                'amenity': facilities,
                'category': []
            },
            'showOnlyAvailable': False
        }

        bread_crumb_list = [{'title': 'home', 'url': '/'}]
        if bool(parent_query):
            for single_query in parent_query:
                lat = str(single_query['lat'])
                long = str(single_query['long'])
                name = str(single_query['name'])

                bread_crumb_list.append(
                    {
                        'title': single_query['name'],
                        'url': "/search/?locality={0}&lat={1}&long={2}&query={3}&checkin={4}&checkout={5}&roomconfig={6}".format(
                            '',
                            lat,
                            int,
                            name,
                            datetime.now().strftime(
                                date_time_utils.DATE_FORMAT),
                            (datetime.now() +
                             timedelta(
                                days=1)).strftime(
                                date_time_utils.DATE_FORMAT),
                            self.room_config_str)})

        marketing_banner = ContentServiceClient.getValueForKey(
            self.request, 'search_marketing_flat_banner', 1)
        if marketing_banner is not None:
            marketing_banner['position'] = (len(result) / 2 + 1)

        no_of_nights = (check_out_date - check_in_date).days
        search_string = search_string.title()
        context = {
            'cities': sorted_city_details,
            'isLocalitySearched': is_locality_search,
            'nearbyResults': nearbyResult,
            'isNearby': is_nearby,
            'results': result,
            'breadCrumbs': bread_crumb_list,
            'googleMaps': True,
            'marketBanner': marketing_banner,
            'cityDetails': sorted_city_details,
            'filterData': json.dumps(filter_data),
            'queryParams': {
                'checkIn': dateformat.format(
                    check_in_date,
                    "jS M'y"),
                'checkOut': dateformat.format(
                    check_out_date,
                    "jS M'y"),
                'checkInCookie': check_in_date.strftime(
                    date_time_utils.DATE_FORMAT),
                'checkOutCookie': check_out_date.strftime(
                    date_time_utils.DATE_FORMAT),
                'no_of_nights': no_of_nights,
                'guests': guest,
                'rooms': rooms_required,
                'search': search_string,
                'latitude': latitude,
                'longitude': longitude,
                'locality': locality}}

        if is_locality_search:
            context['localityName'] = locality_name
        return context

    def __parse_request(self, request, kwargs):
        """
        Extract the query information from the request
        :rtype : tuple
        :param request:
        :return:
        """
        d = request.GET
        query = kwargs.get('cityString', '')
        cookie = request.COOKIES
        if cookie.get('checkin'):
            check_in_date = cookie.get('checkin')
            if '00:00:00' not in check_in_date:
                check_in_date += ' 00:00:00'
            check_in_date = datetime.strptime(
                check_in_date, "%Y-%m-%d %H:%M:%S")
            check_in_date = check_in_date.replace(hour=0, minute=0, second=0)
            if check_in_date < datetime.today():
                check_in_date = datetime.now().replace(
                    hour=0, minute=0, second=0, microsecond=0)
        else:
            check_in_date = datetime.now()
            check_in_date = check_in_date.replace(
                hour=0, minute=0, second=0, microsecond=0)

        if cookie.get('checkout'):
            check_out_date = cookie.get('checkout')
            if '00:00:00' not in check_out_date:
                check_out_date += ' 00:00:00'
            check_out_date = datetime.strptime(
                check_out_date, "%Y-%m-%d %H:%M:%S")
            check_out_date = check_out_date.replace(hour=0, minute=0, second=0)
            if check_out_date <= check_in_date:
                check_out_date = check_in_date + timedelta(days=1)
        else:
            check_out_date = datetime.now() + timedelta(days=1)
            check_out_date = check_out_date.replace(
                hour=0, minute=0, second=0, microsecond=0)

        room_config_string = '1-0'
        room_config = [(1, 0)]
        latitude = const.BANGALORE_LAT
        longitude = const.BANGALORE_LAT
        locality = const.DEFAULT_LOCALITY

        self.hotel_details_suffix = "?checkin={0}&checkout={1}&roomconfig={2}".format(
            str(check_in_date.strftime(date_time_utils.DATE_FORMAT)),
            str(check_out_date.strftime(date_time_utils.DATE_FORMAT)), str(room_config_string))
        self.default_search_url = "/search/?locality={0}&lat={1}&long={2}&query={3}&checkin={4}&checkout={5}&roomconfig={6}".format(
            locality,
            latitude,
            longitude,
            query,
            datetime.now().strftime(
                date_time_utils.DATE_FORMAT),
            (datetime.now() +
             timedelta(
                days=1)).strftime(
                date_time_utils.DATE_FORMAT),
            room_config_string)
        self.room_config_str = room_config_string

        return query, check_in_date, check_out_date, room_config, latitude, longitude, locality
