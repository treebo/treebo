# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0036_merge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='errorcodes',
            old_name='msg',
            new_name='message',
        ),
        migrations.AddField(
            model_name='city',
            name='state',
            field=models.ForeignKey(
                to='dbcommon.State',
                null=True,
                on_delete=models.DO_NOTHING),
        ),
    ]
