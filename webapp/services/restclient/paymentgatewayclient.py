import logging

import requests
from django.core.urlresolvers import reverse

from base.middlewares.request import get_domain

logger = logging.getLogger(__name__)


class PaymentGatewayClient:

    def __init__(self):
        pass

    @staticmethod
    def __get_payment_gateway_api(page_name):
        # FIXME: Move this to payment gateway directory, shouldn't be in
        # checkout
        return get_domain() + reverse('checkout_v2:{0}'.format(page_name))

    @staticmethod
    def __make_call(page_name, data):
        return requests.post(PaymentGatewayClient.__get_payment_gateway_api(
            page_name), data=data, headers={'referer': "/"}, allow_redirects=True)

    @staticmethod
    def __get_payment_details(
            order_id,
            status,
            razorpay_payment_id,
            amount_paid):
        return {
            'order_id': order_id,
            'status': status,
            'razorpay_payment_id': razorpay_payment_id,
            'amount_paid': amount_paid
        }

    @staticmethod
    def razor_pay_handler(order_id, status, razorpay_payment_id, amount_paid):
        """
        Creates a new booking in Hx
        :param booking_request:
        :return:
        """

        payment_gateway_details = PaymentGatewayClient.__get_payment_details(
            order_id, status, razorpay_payment_id, amount_paid)
        response = PaymentGatewayClient.__make_call(
            "razorpay", payment_gateway_details)
        if response.status_code != requests.codes.ok:
            response.raise_for_status()
        payment_gateway_response = response.json()
        return payment_gateway_response
