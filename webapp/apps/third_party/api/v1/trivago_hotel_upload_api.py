import logging

from django.conf import settings
from django.http import HttpResponse
from rest_framework.response import Response

from apps.third_party.forms import TrivagoFileUploadForm
from apps.third_party.service.trivago_upload_service import TrivagoFileUploaderService
from apps.third_party.tasks import trivago_upload_hotel_data_automatic
from base.views.api import TreeboAPIView
from base.views.template import TreeboTemplateView

logger = logging.getLogger(__name__)


class TrivagoFileUploader(TreeboTemplateView):
    template_name = "trivago/trivago_file_upload.html"
    form_class = TrivagoFileUploadForm

    def getData(self, request, args, kwargs):
        return {'form': self.form_class}

    def getMobileData(self, request, args, kwargs):
        return {'form': self.form_class}

    def post(self, request, *args, **kwargs):
        form = TrivagoFileUploadForm(request.POST, request.FILES)
        try:
            if form.is_valid():
                TrivagoFileUploaderService.save_file_and_initiate_upload(
                    request.FILES['trivagoFile'])
                return HttpResponse('<h2>File upload success</h2>')
            else:
                return HttpResponse('<h2>Form not valid</h2>')
        except Exception as e:
            logger.exception(e)
            return HttpResponse('<h2>Some error occurred</h2>')


class TrivagoAutomaticHotelUploader(TreeboAPIView):

    def get(self, request):
        try:
            url_prefix = request.build_absolute_uri('/').rstrip('/')
            trivago_upload_hotel_data_automatic.delay(url_prefix)
            return HttpResponse(
                '<h2>Hotel inventory upload has been triggerred</h2>')
        except BaseException:
            logger.exception('Failed')
            return HttpResponse('<h2>Some error occurred</h2>')


class TrivagoBidFileUploadView(TreeboTemplateView):
    template_name = "trivago/trivago_bid_file_upload.html"
    form_class = TrivagoFileUploadForm

    def getData(self, request, args, kwargs):
        return {'form': self.form_class}

    def getMobileData(self, request, args, kwargs):
        return {'form': self.form_class}

    def post(self, request, *args, **kwargs):
        form = TrivagoFileUploadForm(request.POST, request.FILES)
        try:
            if form.is_valid():
                file_obj = request.FILES['trivagoFile']
                file_dir_path = settings.TRIVAGO_BID_FILE_PATH
                file_name = settings.TRIVAGO_BID_FILE_NAME
                TrivagoFileUploaderService.store_file(
                    file_dir_path, file_name, file_obj)
                return HttpResponse('<h2>File upload success</h2>')
            else:
                return HttpResponse('<h2>Form not valid</h2>')
        except Exception as e:
            logger.exception(e)
            return HttpResponse('<h2>Some error occurred</h2>')
