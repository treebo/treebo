# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0069_auto_20171031_0845'),
    ]

    operations = [
        migrations.CreateModel(
            name='HotelMetaData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('key', models.CharField(max_length=100)),
                ('value', models.CharField(max_length=200)),
                ('hotel', models.ForeignKey(related_name='meta_data', to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
                'verbose_name_plural': 'Hotel Meta data',
                'default_permissions': ('add', 'change', 'delete', 'read'),
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AlterUniqueTogether(
            name='hotelmetadata',
            unique_together=set([('hotel', 'key')]),
        ),
    ]
