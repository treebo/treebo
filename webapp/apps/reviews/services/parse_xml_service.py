import traceback

import xmltodict
import json
import logging
import ast

from django.conf import settings

from apps.reviews.services.trip_advisor.hotel_reviews_data import HotelReviewsDataService
from apps.reviews.services.trip_advisor.hotel_ta_mapping_service import HotelTAMappingService
from apps.third_party.service.trip_advisor.serializers.ta_location_reviews_serializer.location_reviews_api_serializer import \
    LocationReviewsAPISerializer
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class XMLParser(object):
    def update_reviews_table(self, ta_id, name, data):
        try:
            hotel_ta_map = HotelTAMappingService()
            map_obj = hotel_ta_map.fetch_mapping(ta_loc_id=ta_id)
            if map_obj:
                ta_loc_id = map_obj.ta_location_id
                serializer_user_rev = LocationReviewsAPISerializer(data=data)
                logger.info(
                    "TA XML FEED - for ta_loc_id %s serializer.is_valid() %s serializer.errors %s",
                    ta_id,
                    serializer_user_rev.is_valid(),
                    serializer_user_rev.errors)
                logger.info("serializer_user_rev %s", serializer_user_rev)
                hotel_review_data = HotelReviewsDataService()
                hotel_review_data.hotel_reviews_data(
                    serializer_user_rev, ta_loc_id, user_generated=True)
            else:
                logger.error(
                    "No such mapping exists for hotel %s %s",
                    str(ta_id),
                    str(name))

        except Exception as ex:
            logger.exception("Exception in calling TA APIs")
            slack_alert.review_alert(
                "Exception occured while fetching XML FEED reviews and updating audit for hotel_id : %s %s "
                "\n%s", ta_id, name, traceback.format_exc())
            raise ex

    def parse_xml_feed(self):
        try:
            with open('review_feed.xml') as fd:
                xml_data = xmltodict.parse(fd.read())
                xml_data = json.dumps(xml_data)
                xml_data = ast.literal_eval(xml_data)
                # logger.info("xml_data %s", xml_data)
                if xml_data:
                    self.create_response_dict(xml_data)
                else:
                    logger.error("Error in dumping xml data to json")
        except Exception:
            logger.exception("Exception in reading TA feed xml")

    def create_response_dict(self, xml_data):
        try:
            property_dict = xml_data.get('PropertyList')
            property_data_list = property_dict.get('Property')
            logger.info("property_data_list count %s", len(property_data_list))
            for property in property_data_list:
                data = []
                hotel_review_dict = dict()
                property_ta_id = property.get('@id')
                property_city_id = property.get('@cityid')
                property_country_id = property.get('@countryid')
                property_name = property.get('Name')
                reviews_data_dict = property.get('Reviews')
                reviews_data_list = reviews_data_dict.get('Review')
                logger.info(
                    "reviews_data_list count %s",
                    len(reviews_data_list))
                for review in reviews_data_list:
                    review_dict = self.create_review_dict(review)
                    if review_dict:
                        data.append(review_dict)
                if data:
                    hotel_review_dict['data'] = data
                    logger.info("\n hotel_review_dict %s", hotel_review_dict)
                    self.update_reviews_table(
                        property_ta_id, property_name, hotel_review_dict)
        except Exception as e:
            logger.exception(
                "Exception in generating the data dict for reviews serializer")
            raise e

    def create_review_dict(self, review_data):
        review_dict = dict()
        try:
            # TODO: user rating text url - ??
            if review_data.get('ModerationStatus').lower() == 'published':
                review_dict['id'] = review_data.get('@id')
                review_dict['lang'] = review_data.get('Language')
                review_dict['location_id'] = review_data.get('@id')
                review_dict['published_date'] = review_data.get(
                    'DatePublished')
                review_dict['rating'] = review_data.get('Rating')
                review_dict['rating_image_url'] = settings.TRIPADVISOR_BASE_URL + \
                    review_data.get('ReviewURL')
                review_dict['title'] = review_data.get('Title')
                review_dict['text'] = review_data.get('Text')
                review_dict['trip_type'] = review_data.get('TripType')
                review_dict['user'] = self.create_author_dict(
                    review_data.get('Author'))
                if review_data.get('ManagementResponse') and review_data['ManagementResponse'][
                        'ModerationStatus'].lower() == "published":
                    logger.info("calling owner data")
                    review_dict['owner_response'] = self.create_owner_response_dict(
                        review_data.get('ManagementResponse'))
                if review_data.get('ReviewSubratings'):
                    logger.info(
                        "calling subratings %s",
                        review_data.get('ReviewSubratings'))
                    review_dict['subratings'] = self.create_subratings_list(
                        review_data.get('ReviewSubratings'))
                if review_data.get('ReviewDetails'):
                    logger.info("review details")
                    travel_date = self.get_review_details(
                        review_data.get('ReviewDetails'))
                    review_dict['travel_date'] = travel_date
        except Exception:
            logger.exception(
                "Exception in generating review data %s",
                review_data)
        # logger.info("\n \n review dict returned %s", review_dict)
        return review_dict

    def create_owner_response_dict(self, owner_data):
        owner_response = dict()
        logger.info("creating owner response dict")
        try:
            owner_response['id'] = owner_data.get('@id')
            owner_response['lang'] = owner_data.get('Language')
            owner_response['published_date'] = owner_data.get('DatePublished')
            owner_response['author'] = owner_data.get(
                'Author').get('AuthorName')
            owner_response['text'] = owner_data.get('Text')
        except Exception:
            logger.exception(
                "Exception in generating owner response data %s",
                owner_data)
        logger.info("\n \n owner_response dict returned %s", owner_response)
        return owner_response

    def create_author_dict(self, author_data):
        user = dict()
        try:
            user['username'] = author_data.get('AuthorName')
            if author_data.get('SmallAvatarURL'):
                user['image'] = author_data['SmallAvatarURL'].get('#text')
            user['user_location'] = dict()
            user['user_location']['name'] = author_data.get('Location')
            user['user_location']['id'] = None

        except Exception:
            logger.exception(
                "Exception in generating author/user data %s",
                author_data)
        # logger.info("\n \n user dict returned %s", user)
        return user

    def create_subratings_list(self, subratings_data_dict):
        subratings = []
        try:
            subrating_list = subratings_data_dict.get('ReviewSubrating')
            logger.info("subrating_list %s", subrating_list)
            if subrating_list:
                logger.info("subrating list is present")
                for subrating in subrating_list:
                    subrating_dict = self.create_subratings_dict(subrating)
                    subratings.append(subrating_dict)
        except Exception:
            logger.exception(
                "Exception in generating subratings list %s",
                subratings_data_dict)
        logger.info("\n \n subratings list returned %s", subratings)
        return subratings

    def create_subratings_dict(self, subrating):
        subrating_dict = dict()
        try:
            subrating_dict['name'] = subrating.get('@name')
            subrating_dict['value'] = subrating.get('Value')
        except Exception:
            logger.exception(
                "Exception in generating subrating dict %s",
                subrating)
        logger.info("\n \n subratings dict returned %s", subrating_dict)
        return subrating_dict

    def get_review_details(self, review_details_dict):
        travel_date = None
        try:
            review_detail_list = review_details_dict.get('ReviewDetail')
            if review_detail_list:
                logger.info("review_detail_list is present")
                for review_detail in review_detail_list:
                    if review_detail.get('@name') == "DateOfVisit":
                        travel_date = review_detail.get("Value")
        except Exception:
            logger.exception(
                "Exception in checking review details %s",
                review_details_dict)
        logger.info(
            "\n \n returned review details -- travel_date %s",
            travel_date)
        return travel_date
