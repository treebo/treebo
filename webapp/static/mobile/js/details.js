//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//
//
//     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//     A Buddha statue to bless your code to be bug free.
//

import $ from 'jquery';
import { _ } from 'underscore';
import moment from 'moment';
import searchWidget from './widgets/search';
import BaseView from './treebolibs/baseview';
import 'slick-carousel';
import queryString from 'query-string';
import 'whatwg-fetch';
import '../css/details.less';
import cookies from 'js-cookie';

const $loader = $('#commonLoader');
let loaded = false;
let ww = window.innerWidth;
let wh = window.innerHeight;

const parsed = queryString.parse(location.search);
if (parsed.modify) {
  $('.modify__widget__details').trigger('click');
}

const methods = {
    initResults() {
        this.updateGalleryImages();
        $(window).on("orientationchange",() => {
          methods.updateGalleryImages();
        });

        $('.js-room-carousel').slick({
            // lazyLoad: 'ondemand',
            initialSlide: 3,
			slidesToShow: 1,
        }).on('afterChange', (e, { $slides }, curr) => {
            const nextElm = $slides.eq(curr + 1).find('.slick-loading');
            const nextSrc = nextElm.attr('data-lazy');
            nextElm.attr('src', nextSrc);
			nextElm.removeAttr('data-lazy');
        }).on('click', '.slick-arrow', (e) => {
            e.stopPropagation();
        }).on('click', this.initFullScreen);

        $('#commonModal .modal__body')
            .html('<div class="carousel carousel-full"></div>');


        $('.similar__carousel').slick({
            lazyLoad: 'ondemand',
			prevArrow: false,
        	nextArrow: false,
			initialSlide: 3,
			slidesToShow: 2,
            // dots: true,
            //customPaging: (slider, i) => `${i + 1} of ${slider.slideCount}`,
			variableWidth: true
        })




        $('#searchSubmitBtn').remove();
        $('.search-box__header').remove();
        $('#searchModal .modal__footer')
            .append('<button class="btn btn--block modify-search__done">Done</button>');

        $('.modify-search__done').on('click', () => {
            this.paramChangeHandler();
        });

        $('.js-book').on('click', this.makeBooking);

        $('#roomType').val(this.getSelectedRoom());
    },
    updateGalleryImages(){
        let isSrc = false;
        ww = window.innerWidth;
        wh = window.innerHeight;
        $('img.js-resize-fit').each(function (i) {
            isSrc = false;
            const $this = $(this);
            let src = $this.attr('data-lazy');
            if(!src){
                isSrc = true;
                src = $this.attr('src');
            }
            src = src.replace(/w=(\d+)/g, `w=${ww}`);

            if (i === 0) {
                $this.attr('src', src);
                $this.removeAttr('data-lazy');
            } else {
                $this.attr(`${isSrc? 'src': 'data-lazy'}`, src);
            }
        });
    },
    initFullScreen(e) {
        e.preventDefault();

        $('#commonModal').removeClass('hide');
        const carousel = $('#commonModal .carousel-full');

        if (!carousel.hasClass('slick-initialized')) {
            const gallery = $('.hidden-gallery');
            const bgUrl = gallery.data('bg-url');
            const images = $('.hidden-gallery').val().split('||')
                .slice(0, -1).map((v) =>
                    `<div style="background-image:url('${bgUrl}')" class="carousel__img">
                        <img data-lazy="${v}&w=${ww}&h=${wh - 50}"></div>`
                );

            carousel.html(images).slick({
                lazyLoad: 'ondemand',
                asNavFor: '.js-room-carousel',
            });
        }
        const index = $(this).slick('slickCurrentSlide');
        carousel.slick('slickGoTo', index, true);
    },
    makeBooking() {
        window.location = `${rt.quickbook_base_url}${location.search}&couponcode=`;
    },
    closeModify() {
        $('body').trigger('modal:close');
    },
    getSelectedRoom() {
        const roomSelect = $('.room-details');
        return roomSelect.find('.room-details__rooms--selected').data('room') ||
            roomSelect.find('.room-details__rooms:not(.room-details__rooms--unavailable)')
                .first().data('room');
    },
    makeParams() {
        const roomType = $('#roomType').val();

        return {
            // couponcode:hdmodel.currentcoupon,
            roomconfig: searchWidget.methods.getRoomConfigUrl(),
            roomtype: roomType,
            checkin: searchWidget.methods.getCheckInDate(),
            checkout: searchWidget.methods.getCheckOutDate(),
            hotelid: rt.hotelId,
            modify: parsed.modify,
        };
    },
    pluralize(value, text = 's') {
        return +value > 1 ? text : '';
    },
    updateRoomDetails(rooms) {
        let room;
        const roomSelect = $('#roomType');

        $('.room-details__rooms--unavailable').removeClass('room-details__rooms--unavailable');
        for (room of rooms) {
			const jRoom = $(`.room-details__rooms[data-room=${room.roomtype}]`)
            if (!room.available) {
                    jRoom.addClass('room-details__rooms--unavailable')
                // roomSelect.find(`[value="${room.roomtype}"]`).prop('disabled', true);
            }
			jRoom.find('.room-price__price').text(Math.floor(room.rackrate));
        }

        $('.room-details__rooms--selected').removeClass('room-details__rooms--selected');
        $(`.room-details__rooms[data-room=${roomSelect.val()}]`)
            .addClass('room-details__rooms--selected');
    },
    updateDetails(data) {
        const checkin = moment(searchWidget.methods.getCheckInDate()).format('Do MMM\'YY');
        const checkout = moment(searchWidget.methods.getCheckOutDate()).format('Do MMM\'YY');
        const { rooms, adults } = searchWidget.methods.getRoomConfig();

        $('#modify-widget-price').html(data.final_price);
        $('#modify-widget-checkin').html(checkin);
        $('#modify-widget-checkout').html(checkout);
        // $('#modify-widget-nights')
        //     .html(`${data.nights.length} Night${this.pluralize(data.nights.length)}`);
        $('#modify-widget-rooms').html(`${rooms} Room${this.pluralize(rooms)} <span class="middot">&middot;</span>
            ${adults} Guest${this.pluralize(adults)}`);

        this.updateRoomDetails(data.roomdetails);

        // Reset error messages
        $('.room__error-text').html('');
        $('.room__error__container').addClass('hide');
        $('.js-book').prop('disabled', false).removeClass('btn--disabled');
        $('.modify__widget__error-content').html('')
            .parent('.modify__widget__error').addClass('hide');
    },
    changeUrl(newParams) {
        const query = $.param(newParams);
        const newUrl = `${location.pathname}?${query}${location.hash}`;
        history.replaceState(null, null, decodeURIComponent(newUrl));

        cookies.set('checkin', searchWidget.methods.getCheckInDate());
+       cookies.set('checkout', searchWidget.methods.getCheckOutDate());
    },
    showError(error) {
        $('.room__error-text').html(error);
        $('.room__error__container').removeClass('hide');

        if (!loaded) {
            $('.js-book').prop('disabled', true).addClass('btn--disabled');
            $('.modify__widget__error-content').html(error)
                .parent('.modify__widget__error').removeClass('hide');
        }
    },
    fetchData(params, initialLoad) {
        const query = $.param(params);

        if (!params.roomtype) {
            return this.showError('Sorry, all rooms are sold out');
        }

        $loader.removeClass('hide');
        return fetch(`/api/v1/pricing/availability?${query}`, { method: 'get' })
            .then((resp) => resp.json())
            .then(({ data }) => {
                if (data.status === 'error') {
                    this.showError(data.msg);
                } else if (data.priceError) {
                    this.showError(data.priceError);
                } else {
                    this.updateDetails(data);
                    if(!initialLoad){
                        this.closeModify();
                    }

                    _.defer((p) => {
                        this.changeUrl(p);
                    }, params);
                }
            }).catch(({ message }) => {
                this.showError(message);
            }).then(() => {
                $loader.addClass('hide');
                loaded = true;
            });
    },
    paramChangeHandler(initialLoad) {
        _.defer(() => {
            const params = this.makeParams();
            this.fetchData(params, initialLoad);
        });
    },
};

let DetailsView = Object.create(BaseView);

methods.initResults();
DetailsView.pageLoaded = () => {
    searchWidget.methods.searchWidgetLoaded();
    methods.paramChangeHandler(true);

    $('.hotel__name__link').on('click', function () {
        const ele = $(this);
        const url = ele.attr('href').split('?');
        const params = queryString.parse(url[1]);
        const newParams = queryString.stringify(
            _.extend(params, _.pick(methods.makeParams(), ['roomconfig', 'checkin', 'checkout']))
        );
        ele.attr('href', `${url[0]}?${newParams}`);
    });
};

let analyticsConfigfunc = function(){
    return  {
        hotel_id:rt.hotelId,
        hotel_name:rt.hotelName,
        tags:rt.tags,
        locality: rt.locality,
        city:rt.cityName,
        hrental_id: rt.hotelId,
        hrental_pagetype: 'offerdetail',
        hrental_startdate: rt.getCheckin(),
        hrental_enddate: rt.getCheckout(),
        hrental_totalvalue: $('.analytics-totalprice').text(),
    }
}

DetailsView.setup({ name: 'Hotel Detail Page', widgets: [] });
