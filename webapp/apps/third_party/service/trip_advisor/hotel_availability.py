from __future__ import absolute_import
from __future__ import print_function

import ast
import json
import logging
import datetime
from datetime import timedelta

from django.conf import settings
from django.db.models import Prefetch
from django.http import HttpResponse
from django.utils.encoding import smart_str

from apps.pricing.dateutils import ymd_str_to_date
from apps.third_party.service.trip_advisor import constants
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room

from apps.third_party.service.trip_advisor.price_adaptors.non_rate_plan_prices import NonRatePlanPriceAdaptor
from apps.third_party.service.trip_advisor.price_adaptors.rate_plan_prices import RatePlanPriceAdaptor
from apps.pricing.exceptions import SoaError

logger = logging.getLogger(__name__)


class HotelAvailabilityService(object):
    price_adaptor = None

    def read_request_data(self, data):
        try:
            if settings.NRP_ENABLED:
                self.price_adaptor = RatePlanPriceAdaptor()
            else:
                self.price_adaptor = NonRatePlanPriceAdaptor()

            data = self.extract_from_request(data)
            hotel_availability_json = self.create_availability_json(data)
            return HttpResponse(json.dumps(hotel_availability_json), content_type="application/json")
        except Exception as e:
            logger.error("Exception in hotel availability service due to %s", str(e))
            data["errors"] = self.generate_error_tag(code=1, hotel_id_list=None)
            hotel_availability_json = self.create_availability_json(data)
            return HttpResponse(json.dumps(hotel_availability_json), content_type="application/json")

    def extract_from_request(self, data):
        try:
            logger.info("reading the request")
            data = {smart_str(k.strip().encode('utf-8')): smart_str(v.strip().encode('utf-8')) for k, v in data.items()}
        except Exception as e:
            logger.exception("Exception in parsing request and creating errors response key")
            logger.error("Error in parsing due to %s", str(e))
            data["errors"] = self.generate_error_tag(code=2, hotel_id_list=None)
        return data

    def validate_checkin_checkout_dates(self, checkin, checkout):
        check_in_date = datetime.datetime.strptime(checkin, "%Y-%m-%d").date()
        current_date = datetime.datetime.now().date()
        valid_checkin = current_date if check_in_date < current_date else check_in_date
        check_out_date = datetime.datetime.strptime(checkout, "%Y-%m-%d").date()
        valid_checkout = valid_checkin + timedelta(days=1) if check_out_date <= valid_checkin else check_out_date
        return valid_checkin, valid_checkout

    def create_availability_json(self, data):
        try:
            response = dict()
            partner_ta_mapping = dict()
            response["api_version"] = int(data.get("api_version")) if data.get("api_version") else 0
            check_in = data.get("start_date")
            check_out = data.get("end_date")
            valid_checkin, valid_checkout = self.validate_checkin_checkout_dates(check_in, check_out)
            response["start_date"] = str(valid_checkin)
            response["end_date"] = str(valid_checkout)
            response["currency"] = data.get("currency")
            response["user_country"] = data.get("user_country")
            response["device_type"] = data.get("device_type")
            response["query_key"] = data.get("query_key")
            response["lang"] = constants.LANGUAGE
            response["hotel_ids"] = []
            hotel_ids_list = []
            data_hotels = ast.literal_eval(data.get("hotels"))

            for h in data_hotels:
                response["hotel_ids"].append(int(h.get("ta_id")))
                hotel_ids_list.append(h.get("partner_id"))
                partner_ta_mapping[h.get("partner_id")] = int(h.get("ta_id"))

            logger.info("partner_ta_mapping %s", partner_ta_mapping)
            if data.get("errors"):
                response["errors"] = data.get("errors")
                return response

            party = ast.literal_eval(data.get("party"))
            if party:
                response_party, room_config, num_room_requested, ta_url_room_config = self.get_adult_child_config(party)
                response["party"] = response_party
                checkin_date = response["start_date"]
                checkout_date = response["end_date"]
                hotels, errors = self.generate_hotels_key_data(hotel_ids_list, checkin_date, checkout_date,
                                                               room_config, num_room_requested, ta_url_room_config,
                                                               partner_ta_mapping)

                len_hotels = len(hotels)
                response['hotels'] = hotels
                response["num_hotels"] = len_hotels

                len_errors = len(errors)
                if len_errors > 0:
                    response["errors"] = errors
            return response
        except Exception as e:
            logger.error("Exception in creating response due to %s", str(e))

    def generate_hotels_key_data(self, hotel_ids_list, checkin_date, checkout_date, room_config, num_room_requested,
                                 ta_url_room_config, partner_ta_mapping):
        hotels = []
        errors = []
        try:
            error_hotel_id_list = []
            for hotel_id in hotel_ids_list:
                if not Hotel.objects.filter(id=hotel_id, status=1).exists():
                    error_hotel_id_list.append(hotel_id)
                    hotel_ids_list.remove(hotel_id)
                    continue
            if len(error_hotel_id_list) == 0:
                error_hotel_id_list = None
            if len(hotel_ids_list) == 0:
                hotel_ids_list = None

            if hotel_ids_list:
                checkin = ymd_str_to_date(checkin_date)
                checkout = ymd_str_to_date(checkout_date)
                try:
                    hotel_prices, room_availability_per_hotel = self.price_adaptor.get_price_and_availability(
                        hotel_ids_list, checkin, checkout, room_config)
                except SoaError as err:
                    logger.error("Exception in fetching prices for hotel", str(err))
                    errors = self.generate_error_tag(1, hotel_id_list=None, error_cause=str(err))
                    hotel_prices = {}
                    room_availability_per_hotel = {}

                hotel_data = Hotel.objects.select_related('city').prefetch_related(
                    Prefetch('rooms', queryset=Room.objects.filter(hotel_id__in=hotel_ids_list))
                ).filter(id__in=hotel_ids_list)

                for hotel in hotel_data:
                    room_types = dict()
                    hotel_availability_data = dict()
                    hotel_price = hotel_prices.get(hotel.id) if hotel_prices else None
                    hotel_availability_data["hotel_id"] = int(partner_ta_mapping[str(hotel.id)])
                    hotel_availability_data["room_types"] = self.price_adaptor.create_room_types_data(
                        room_types, hotel, hotel_price, checkin_date, checkout_date, room_availability_per_hotel,
                        num_room_requested, ta_url_room_config)

                    if hotel_availability_data["room_types"]:
                        hotel_availability_data["response_type"] = "available"
                    else:
                        hotel_availability_data["response_type"] = "unavailable"
                    hotels.append(hotel_availability_data)

            if error_hotel_id_list:
                errors = self.generate_error_tag(3, error_hotel_id_list)

        except Exception as e:
            logger.exception("Exception in generating hotels key data")
            logger.error("Error cause is %s", str(e))
            errors = self.generate_error_tag(code=1, hotel_id_list=None)

        return hotels, errors

    def generate_error_tag(self, code, hotel_id_list, error_cause=None):
        error_list = []
        errors = dict()
        errors["error_code"] = code
        message = constants.ERROR_CODE[code]
        errors["message"] = message
        if error_cause:
            errors["error_cause"] = error_cause
        if hotel_id_list is not None:
            errors["hotel_ids"] = hotel_id_list
        error_list.append(errors)
        return error_list

    def get_adult_child_config(self, party):
        max_adults = 0
        room_config = None
        num_room_requested = 0
        response_party = []
        num_adult_list = []
        num_children_list = []
        for room in party:
            response_party.append(room)
            if max_adults < room["adults"]:
                max_adults = room["adults"]
            if room.get("children"):
                children = len(room["children"])
            else:
                children = 0
            adult_child = str(room["adults"]) + "-" + str(children)
            if not room_config:
                room_config = adult_child
            else:
                room_config = room_config + "," + adult_child
            num_adult_list.append(str(room["adults"]))
            num_children_list.append(str(children))
            num_room_requested += 1
        ta_url_adults = ",".join(num_adult_list)
        ta_url_children = ",".join(num_children_list)
        ta_url_room_config = ta_url_adults + "-" + ta_url_children
        return response_party, room_config, num_room_requested, ta_url_room_config
