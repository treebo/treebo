import logging
from apps.content.models import ContentStore
from django.core.cache import cache
from base.decorator.cache_processor import cache_process
from apps.common.exceptions.custom_exception import KeyNotFoundInContentStore

logger = logging.getLogger(__name__)


class ContentStoreService(object):

    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'content_store_web_direct_{key}'

    @staticmethod
    def invalidate_cache(key=None):
        if key:
            cache.delete(ContentStoreService.CACHE_KEY.format(key=key))
        else:
            cache.delete_pattern(ContentStoreService.CACHE_KEY.format(key='*'))

    @staticmethod
    def get_contents(keys, version=1):
        """
        Take the list of keys and fetches the content store entry for the those keys
        :param keys:
        :type keys:
        :param version:
        :type version:
        :return [ContentStore]:
        :rtype:
        """
        contents = []
        for key in keys:
            try:
                contents.append(ContentStoreService.get_content(key, version))
            except KeyNotFoundInContentStore:
                continue
        return contents

    @staticmethod
    @cache_process(CACHE_KEY, CACHE_TIMEOUT, ["key"])
    def get_content(key='', version=1):
        """
        Take the key and fetches the content store entry for that key
        :param key: ContentStore name
        :param version: ContentStore version
        :return [ContentStore]: ContentStore object
        :rtype:
        """
        try:
            value = ContentStore.objects.filter(name=key, version=version).first()
            if not value:
                raise KeyNotFoundInContentStore("key:{key} not found in content store".format(key=key))
        except ContentStore.DoesNotExist:
            logger.error("Key not found in the content store please make an entry for key:{key} "
                         "in content store".format(key=key))
            raise KeyNotFoundInContentStore("key:{key} not found in content store".format(key=key))
        return value
