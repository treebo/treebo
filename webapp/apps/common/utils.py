import datetime
import decimal
import json
import logging
import re
import sys
import urllib.request
import urllib.error
import urllib.parse
from decimal import Decimal, ROUND_HALF_UP

import django.core.handlers.wsgi
import rest_framework.request
from django.conf import settings
from ipware.ip import get_ip
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from apps.checkout.models import BookingRequest
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.content.models import ContentStore
from base import log_args
from common.constants import common_constants
from common.constants import error_codes
from common.utilities import number_utils

logger = logging.getLogger(__name__)
# analytics.write_key = settings.SEGMENT_EVENTS_KEY
# analytics.debug = settings.SEGMENT_DEBUG

TWO_DECIMAL = Decimal('.01')
ZERO_DECIMAL = Decimal('1.')


def round_to_two_decimal(price):
    """
    :param price:
    :return:
    """
    return Decimal(price).quantize(TWO_DECIMAL, rounding=ROUND_HALF_UP)


def round_to_nearest_integer(price):
    """
    :param price:
    :return:
    """
    return Decimal(price).quantize(ZERO_DECIMAL, rounding=ROUND_HALF_UP)


def get_number_of_days(checkin_date, checkout_date):
    """
    :param checkin_date:
    :param checkout_date:
    :return:
    """
    delta = checkin_date - checkout_date
    return delta.days


def get_intermediate_dates(date1, date2):
    """
    Get dates between two dates
    Args:
        date1: Start Date string in 'dd-mm-yyyy' format
        date2: End Date string in 'dd-mm-yyyy' format

    Returns: List of string representation of dates between date1 and date2

    """
    date1 = datetime.datetime.strptime(date1, '%Y-%m-%d')
    date2 = datetime.datetime.strptime(date2, '%Y-%m-%d')

    delta = date2 - date1

    return [date1 + datetime.timedelta(days=i) for i in range(delta.days)]


def validate_gstin(gstin):
    gstin = str(gstin).upper()
    gst_pattern = r'[\d]{2}[A-Z]{5}[\d]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$'
    if re.match(gst_pattern, gstin):
        return gstin
    return False


class Utils(object):
    """
        Utils
    """

    @staticmethod
    def get_image_size(flavour, template_name):
        """
        :param flavour:
        :param template_name:
        :return:
        """
        if flavour == "full":
            if template_name == "search":
                return 310, 184
        if template_name == "search-rooms":
            return 302, 250
        if template_name == "details":
            return 417, 370

        if flavour == "mobile":
            if template_name == "index-city":
                return 270, 205
            if template_name == "search":
                return 480, 235

        if template_name == "details":
            return 301, 187, 1286, 600
        if template_name == "search":
            return 420, 320
        if template_name == "search-rooms":
            return 302, 250
        if template_name == "confirmation" or template_name == "traveller" or template_name == "order":
            return 242, 190
        if template_name == "share":
            return 294, 330
        if template_name == "home":
            return 480, 205
        if template_name == "city":
            return 334, 350
        if template_name == "city_hotel":
            return 495, 350
        if template_name == "city_portrait_image":
            return 390, 430
        if template_name == "city_landscape_image":
            return 564, 205

        return "", ""

    @staticmethod
    def build_image_params(width, height, fit, crop, quality):
        """
        :param width:
        :param height:
        :param fit:
        :param crop:
        :param quality:
        :return:
        """
        if Utils.is_valid_number(width) and Utils.is_valid_number(height):
            return "?fm=pjpg&w=" + str(width) + \
                "&h=" + str(height) + "&fit=" + fit
        else:
            return ""

    @staticmethod
    def chunks(l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]

    @staticmethod
    def is_valid_number(number):
        """
        :param number:
        :return:
        """
        try:
            float(number)
            return True
        except BaseException:
            return False

    @staticmethod
    def buildError(exception_message, line_number):
        """
        :param exception_message:
        :param line_number:
        :return:
        """
        message = "Exception " + exception_message + \
            " line number = " + str(line_number)
        logger.error(message)

    __MOBILE_TEMPLATES = {
        "search": (480, 235),
        "detail-rooms": (480, 205),
        "detail-banner": (480, 400),
        "detail-nearBy": (480, 205),
        "detail-gallery": (480, 400),
        "home": (480, 205)
    }

    @staticmethod
    def getMobileImageSize(template_name):
        """
        :param template_name:
        :return:
        """
        if template_name in Utils.__MOBILE_TEMPLATES:
            return Utils.__MOBILE_TEMPLATES.get(template_name)
        else:
            return 480, 235

    @staticmethod
    def get_image_size(flavour, template_name):
        """
        :param flavour:
        :param template_name:
        :return:
        """
        if flavour == "full":
            if template_name == "search":
                return 310, 184
            if template_name == "search-rooms":
                return 302, 250
            if template_name == "details":
                return 417, 370
        if flavour == "mobile":
            if template_name == "index-city":
                return 270, 205
            if template_name == "search":
                return 480, 235
        if template_name == "details":
            return 301, 187, 1286, 600
        if template_name == "search":
            return 420, 320
        if template_name == "search-rooms":
            return 302, 250
        if template_name == "confirmation" or template_name == "traveller" or template_name == "order":
            return 242, 190
        if template_name == "share":
            return 294, 330
        if template_name == "home":
            return 480, 205
        if template_name == "city":
            return 334, 350
        if template_name == "city_hotel":
            return 495, 350
        if template_name == "city_portrait_image":
            return 390, 430
        if template_name == "city_landscape_image":
            return 564, 205

        return "", ""

    @staticmethod
    def build_image_params(width, height, fit, crop, quality):
        """
        :param width:
        :param height:
        :param fit:
        :param crop:
        :param quality:
        :return:
        """
        if number_utils.is_valid_number(
                width) and number_utils.is_valid_number(height):
            return "?w=" + str(width) + "&h=" + str(height) + "&fit=" + fit + "&fm=" + \
                common_constants.IMAGE_FM + "&q=" + common_constants.IMAGE_QUALITY
        else:
            return ""

    @staticmethod
    def chunks(l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]

    # @staticmethod
    # def is_valid_number(number):
    #     try:
    #         float(number)
    #         return True
    #     except:
    #         return False

    @staticmethod
    def buildError(exception_message, line_number):
        """
        :param exception_message:
        :param line_number:
        :return:
        """
        message = "Exception " + exception_message + \
            " line number = " + str(line_number)
        logger.error(message)

    @staticmethod
    def buildErrorResponseContext(error):
        """
        :param error:
        :return:
        """
        return {
            'status': error_codes.ERROR_STATUS,
            'code': error.get('code'),
            'msg': error.get('msg'),
            'redirect': False
        }

    @staticmethod
    def buildErrorResponseContext(code):
        """
        :param code:
        :return:
        """
        if error_codes.get(code) is not None:
            error = error_codes.get(code)
            return {
                'status': error_codes.ERROR_STATUS,
                'code': error.get('code'),
                'msg': error.get('msg')
            }
        else:
            return {
                'status': error_codes.ERROR_STATUS,
                'msg': 'Invalid data provided'
            }


class AnalyticsEventTracker(object):
    """
        AnalyticsEventTracker
    """
    availableEventName = settings.SERVER_ANALYTICS_EVENT_NAMES
    treeboIdentifier = None
    serverEventName = None
    eventType = None
    eventArgs = {}
    segmentIntegrations = {}
    context = {}

    MSITE = 'msite'
    WEBSITE = 'website'
    APP = 'app'
    ANDROID = 'android'
    IOS = 'ios'

    @log_args(logger)
    def __init__(
            self,
            event_type,
            user_identifier,
            send_event=0,
            event_name="",
            event_args=dict(),
            treebo_integrations=dict(),
            context=dict(),
            channel=None,
            anonymous_id=None):
        # Instantiate all the class variables
        self.eventType = event_type
        self.treeboIdentifier = get_user_identifier(user_identifier)
        self.serverEventName = self.getServerEventName(event_name)
        self.eventArgs = event_args
        self.analyticsIntegrations(treebo_integrations)
        self.context = context
        self.channel = channel
        self.anonymous_id = anonymous_id
        self.segement_key = settings.SEGMENT_EVENTS_KEYS[self.channel]
        if not self.is_ignore_analytics(user_identifier) and not self.is_blacklisted(
                user_identifier) and send_event != 0 and self.treeboIdentifier and self.serverEventName:
            self.pushEventToSegment(self.segement_key)

    def is_blacklisted(self, request):
        try:
            ip = get_ip(request)
        except Exception as e:
            ip = 'NONE'
        content = ContentStore.objects.filter(name="blacklisted_ips")
        if content is not None:
            ips = json.loads(content[0].value)["ip"]
            if ips is not None and len(ips) > 0:
                for blacklisted_ip in ips:
                    if str(blacklisted_ip) == ip:
                        return True
        return False

    def is_ignore_analytics(self, request):
        """
        :param django.core.handlers.wsgi.WSGIRequest request:
        """
        if isinstance(
                request,
                rest_framework.request.Request) or isinstance(
                request,
                django.core.handlers.wsgi.WSGIRequest):
            return request.COOKIES.get('ignore_analytics', 'false') == 'true'
        return False

    @log_args(logger)
    def analyticsIntegrations(self, treebo_integrations):
        self.segmentIntegrations = {'All': True}

    @log_args(logger)
    def event_user_identifier(self, user_identifier):
        """
        :param user_identifier:
        :return:
        """
        if type(user_identifier) in [int, str]:
            return user_identifier
        elif isinstance(user_identifier, rest_framework.request.Request) \
                or isinstance(user_identifier, django.core.handlers.wsgi.WSGIRequest) \
                or isinstance(user_identifier, BookingRequest):
            try:
                if user_identifier.user.is_authenticated():
                    return user_identifier.user.id
                else:
                    return re.sub(
                        '[^0-9a-zA-Z\-]',
                        '',
                        urllib.parse.unquote(
                            user_identifier.COOKIES.get(
                                'ajs_user_id',
                                'xxxxxxxxx')))
            except BaseException:
                try:
                    return re.sub(
                        '[^0-9a-zA-Z\-]',
                        '',
                        urllib.parse.unquote(
                            user_identifier.COOKIES.get(
                                'ajs_user_id',
                                'xxxxxxxxx')))
                except BaseException:
                    return user_identifier.data.get('anonymousId', None)
        else:
            logger.warn(
                "##### wrong variable type passed for user_identifier: %s",
                type(user_identifier))
            return None

    @log_args(logger)
    def getServerEventName(self, event_name):
        try:
            return event_name
        except KeyError:
            logger.info(
                "#### No server side event name found for event_name :")
            logger.info(event_name)
            return None

    @log_args(logger)
    def pushEventToSegment(self, write_key):
        """
        :param write_key:
        :return:
        """
        try:
            method = 'track'
            self.call_segment_event(write_key,
                                    method,
                                    str(self.treeboIdentifier),
                                    self.serverEventName,
                                    self.eventArgs,
                                    integrations=self.segmentIntegrations,
                                    context=self.context,
                                    anonymous_id=self.anonymous_id)
        except Exception as e:
            logger.error(e)

    def call_segment_event(self, write_key, method, *args, **kwargs):
        try:
            # analytics.track(str(self.treeboIdentifier), self.serverEventName, self.eventArgs,
            # integrations=self.segmentIntegrations, context=self.context)
            from analytics.client import Client
            on_error = None
            debug = settings.SEGMENT_DEBUG
            send = True
            default_client = Client(write_key, debug=debug, on_error=on_error,
                                    send=send)
            fn = getattr(default_client, method)
            fn(*args, **kwargs)
        except Exception as e:
            slack_alert.web_alert("segment event error " + str(e))
            logger.error(str(e))


@log_args(logger)
def trackAnalyticsServerEvent(
        request,
        event_name,
        event_args={},
        channel=None):
    """
    :param request:
    :param event_name:
    :param event_args:
    :param channel:
    :return:
    """
    event_args['channel'] = channel
    try:
        AnalyticsEventTracker(
            'Event',
            request,
            1,
            event_name,
            event_args,
            channel=channel)
    except Exception as exc:
        slack_alert.web_alert(
            "segment track analytics server event error " + str(exc))


@log_args(logger)
def trackConfirmOrder(
        request,
        event_name,
        event_args,
        channel,
        anonymous_id=None):
    try:
        localContext = {}
        event_args['channel'] = channel
        if request.COOKIES.get('utm_params', None):
            localContext["campaign"] = request.COOKIES.get('utm_params')
        gaClientId = re.sub(
            'GA[0-9].[0-9].',
            '',
            request.COOKIES.get(
                "_ga",
                ""))
        if gaClientId != "":
            localContext["Google Analytics"] = {"clientId": gaClientId}
        ip = get_ip(request)
        if ip is not None:
            localContext["client_ip"] = ip
        localContext['is_mobile'] = request.is_mobile
        localContext['timestamp'] = datetime.datetime.now().strftime(
            '%Y-%m-%d %H:%M %p')
        track_event = AnalyticsEventTracker(
            'Event',
            request,
            1,
            event_name,
            event_args,
            context=localContext,
            channel=channel,
            anonymous_id=anonymous_id)
    except Exception as exc:
        slack_alert.web_alert(
            "segment track confirm order error " +
            str(exc))
        exc_type, exc_obj, exc_tb = sys.exc_info()
        Utils.buildError(str(exc), exc_tb.tb_lineno)


def is_status_4xx(status_code):
    if HTTP_400_BAD_REQUEST <= status_code < HTTP_500_INTERNAL_SERVER_ERROR:
        return True
    return False


def get_utm_params_from_request(request):
    utm_source = request.COOKIES.get('utm_source', '')
    utm_medium = request.COOKIES.get('utm_medium', '')
    utm_campaign = request.COOKIES.get('utm_campaign', '')
    utm_term = request.COOKIES.get('utm_term', '')
    utm_content = request.COOKIES.get('utm_content', '')
    utm_params = None
    if any([utm_source, utm_medium, utm_campaign, utm_term, utm_content]):
        utm_params = {
            "source": utm_source[0] if isinstance(
                utm_source, list) else utm_source, "medium": utm_medium[0] if isinstance(
                utm_medium, list) else utm_medium, "name": utm_campaign[0] if isinstance(
                utm_campaign, list) else utm_campaign, "term": utm_term[0] if isinstance(
                    utm_term, list) else utm_term, "content": utm_content[0] if isinstance(
                        utm_content, list) else utm_content}

    return utm_params


def get_user_identifier(user_identifier):
    """
    :param user_identifier:
    :return:
    """
    if type(user_identifier) in [int, str]:
        return user_identifier
    elif isinstance(user_identifier, rest_framework.request.Request) or isinstance(user_identifier,
                                                                                   django.core.handlers.wsgi.WSGIRequest) or isinstance(
            user_identifier, BookingRequest):
        try:
            if user_identifier.user.is_authenticated():
                return user_identifier.user.id
            else:
                return re.sub(
                    '[^0-9a-zA-Z\-]',
                    '',
                    urllib.request.unquote(
                        user_identifier.COOKIES.get(
                            'ajs_user_id',
                            'xxxxxxxxx')))
        except BaseException:
            try:
                return re.sub(
                    '[^0-9a-zA-Z\-]',
                    '',
                    urllib.request.unquote(
                        user_identifier.COOKIES.get(
                            'ajs_user_id',
                            'xxxxxxxxx')))
            except BaseException:
                logger.exception("exception while get user identifier")
                return user_identifier.COOKIES.get(
                    'anonymousId', None) or user_identifier.data.get(
                    'user_id', None)

    else:
        logger.warn(
            "##### wrong variable type passed for user_identifier: %s",
            type(user_identifier))
        return None


def quantize_and_round_off(amount, decimal_places_to_round_off=2, round_off_strategy=decimal.ROUND_HALF_UP):
    """
    The Standard for quantizing decimals/ floats / ints.
    This converts input to decimals and applies quantize.
    This is to provide consistency where we round off through out the code base
    """

    if not isinstance(amount, decimal.Decimal):
        amount = decimal.Decimal(amount)

    _round_off_string = '0.' + '0' * decimal_places_to_round_off
    exponent_round_off = decimal.Decimal(_round_off_string)

    return amount.quantize(exponent_round_off, rounding=round_off_strategy)


def mangle_emails(email_addresses):
    return ['{e}.mangled.com'.format(e=email) for email in email_addresses]
