# pylint: disable=no-member
import logging

from thsc.crs.entities.booking import Booking as THSCBooking

from apps.bookings.providers.thsc_utils import ThscUtils
from apps.bookings.providers.interfaces.cancel_booking import CancelBookingInterface
from apps.bookings.providers.get_booking.treebo import TreeboCRSGetBooking

logger = logging.getLogger(__name__)


class TreeboCRSCancelBooking(CancelBookingInterface):
    def __init__(self, booking, **kwargs):
        ThscUtils.set_thsc_context(booking.source.application)
        super(TreeboCRSCancelBooking, self).__init__(booking, **kwargs)

    def _cancel(self):
        thsc_booking = THSCBooking.create_instance_for_update(self.booking.crs_reference_id, self.booking.crs_version)
        thsc_booking.cancel(self.kwargs.get('cancellation_message') or 'N/A')
        cancelled_booking = TreeboCRSGetBooking.get_from_thsc_booking(thsc_booking)
        return cancelled_booking
