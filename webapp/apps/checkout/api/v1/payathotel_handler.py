import logging

from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.views import APIView

from base import log_args
from services.restclient.bookingrestclient import BookingClient

logger = logging.getLogger(__name__)


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class PayAtHotelHandler(APIView):
    authentication_classes = (
        CsrfExemptSessionAuthentication,
        BasicAuthentication)

    @log_args(logger)
    def post(self, request):
        order_id = request.POST.get("order_id")
        booking = BookingClient.confirm_booking(order_id)
        return redirect(
            reverse(
                "pages:order", kwargs={
                    "order_id": booking["order_id"]}))
