from rest_framework import serializers


class CityCoordinateSerializer(serializers.Serializer):
    lat = serializers.DecimalField(max_digits=14, decimal_places=10)
    lng = serializers.DecimalField(max_digits=14, decimal_places=10)
