from django.contrib import admin

from apps.seo.models.nearby_poi import POI, POICategory


@admin.register(POICategory)
class POICategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'category_name', 'is_city_mapped')
    search_fields = ('category_name',)
    list_filter = ('category_name',)


@admin.register(POI)
class POIAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'poi_name',
        'poi_location',
        'poi_city',
        'get_category_name',
        'poi_lat',
        'poi_lng')
    search_fields = ('poi_name',)
    list_filter = ('category__category_name', 'poi_city')
    actions = ['delete_selected']

    def get_category_name(self, obj):
        if obj and obj.category:
            return str(obj.category.category_name)
        else:
            return None

    get_category_name.short_description = 'Category'
