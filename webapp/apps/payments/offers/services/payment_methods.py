from collections import defaultdict

from apps.payments.offers.repositories.payment_mothod import PaymentMethodRepository
from apps.payments.constants import CREDIT_CARD, DEBIT_CARD


class PaymentMethodService:

    def get_payment_methods_for_utm(self, utm_source):
        payment_methods = PaymentMethodRepository().get_payment_methods_for_utm(utm_source)
        if not payment_methods:
            return []
        payment_method_response = defaultdict(list)
        for payment_method in payment_methods:
            if payment_method.category in (CREDIT_CARD, DEBIT_CARD):
                payment_method_response['card'] = True
                continue
            payment_method_response[payment_method.category].append(dict(
                id=payment_method.uid,
                name=payment_method.name,
                image_url=payment_method.image_url,
                gateway_code=payment_method.gateway_code,
                ranking=payment_method.ranking))
        return payment_method_response
