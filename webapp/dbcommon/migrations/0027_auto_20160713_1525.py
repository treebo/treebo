# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0026_auto_20160713_1231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='is_otp_verified',
            field=models.NullBooleanField(default=False),
        ),
    ]
