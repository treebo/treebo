import logging

from base.views.template import TreeboTemplateView
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'devang'

logger = logging.getLogger(__name__)


class Join(TreeboTemplateView):
    template_name = "join/index.html"
    redirect_mobile_to_desktop_view = True

    def getData(self, request, args, kwargs):
        seo = ContentServiceClient.getValueForKey(request, 'seo_pages', 1)
        return {'scripts': ['desktop/js/joinus.js'],
                'styles': ['desktop/css/joinus.css'],
                'seo': seo['join'], 'footerCopyrightDisabled': True}
