import json
import logging

from django.conf import settings

from apps.seo.components.nearby_landmarks import NearbyLandmarks
from apps.third_party.service.constants import DEFAULT_PHONE, MAX_RATING
from apps.third_party.service.dynamic_ads_feeds_service import DynamicAdsService
from common.exceptions.treebo_exception import TreeboException
from services.restclient.contentservicerestclient import ContentServiceClient

logger = logging.getLogger(__name__)
__author__ = "Niket Gaurav"


class HotelProxyForGoogleFeed(object):
    email_subject = 'Reg Hotel data for Google adwords'
    email_body = 'Please find the attached hotel data csv'
    file_path = settings.DYNAMIC_ADS_FILE_PATH
    file_name = 'Google_adwords_hotel_feed.csv'
    file_path_name = file_path + file_name

    @staticmethod
    def get_headers():
        headers = ['Property ID', 'Property name', 'Final URL', 'Image URL', 'Destination name', 'Description', 'Price',
                   'Sale price', 'Star rating', 'Category', 'Contextual keywords', 'Address', 'Tracking template',
                   'Custom parameter', 'Final mobile URL', 'Priority']
        return headers

    @staticmethod
    def as_list(hotel_obj, for_app=False):
        try:
            prices_dict = DynamicAdsService.get_room_prices(hotel_obj.id, for_app)
            Hotel_Final_url = DynamicAdsService.get_new_hotel_url(hotel_obj)
            hotel_address = str(hotel_obj.street + ", " + hotel_obj.city.name + ", " + hotel_obj.state.name)
            priority_flag = DynamicAdsService.is_hotel_on_priority(hotel_obj.id)
            list_values = [hotel_obj.id, hotel_obj.name, Hotel_Final_url,
                           DynamicAdsService.get_image_url(hotel_obj), hotel_obj.locality.name,
                               "Flat 50% off on all hotels!",
                           prices_dict['rack_rate'], prices_dict['sell_rate'],
                               "", "", "", hotel_address, "", "",
                           Hotel_Final_url, priority_flag]
            list_value = [str(x) if type(x) not in [str, list] else x for x in
                          list_values]
            logger.debug("Complete Parameters Are %s", list_values)
            return list_value
        except:
            raise Exception

    def get_app_file_name(self):
        return None

    def get_app_file_path(self):
        return None


class HotelProxyForFacebookFeed(object):
    email_subject = 'Reg Hotel data for Facebook ads'
    email_body = 'Please find the attached hotel data csv.'
    file_path = settings.DYNAMIC_ADS_FILE_PATH
    file_name = 'Facebook_ads_hotel_feed.csv'
    file_path_name = file_path + file_name

    @staticmethod
    def get_headers():
        headers = ['hotel_id', 'name', 'description', 'brand', 'address.addr1', 'address.city', 'address.city_id',
                   'address.region', 'address.country', 'address.postal_code', 'latitude', 'longitude',
                   'neighborhood[0]',
                   'margin_level', 'base_price', 'phone', 'star_rating', 'guest_rating[0].rating_system',
                   'guest_rating[0].score', 'guest_rating[0].number_of_reviewers', 'guest_rating['
                                                                                   '0].max_score',
                   'image[0].url', 'image[0].tag',
                   'image[1].url','image[1].tag','image[2].url','image[2].tag','image[3].url','image[3].tag',
                   'image[4].url', 'image[4].tag','loyalty_program', 'url', 'applink', "sale_price", 'Priority']
        return headers

    @staticmethod
    def as_list(hotel_obj, for_app=False):
        try:
            logger.info("Got Hotel_obj for %s in as_list", hotel_obj.name)
            prices_dict = DynamicAdsService.get_room_prices(hotel_obj.id, for_app)
            landmark = HotelProxyForFacebookFeed.get_nearby_landmark(hotel_obj)
            guest_rating = HotelProxyForFacebookFeed.get_guest_rating(hotel_obj)
            hotel_images = HotelProxyForFacebookFeed.get_image_set(hotel_obj)
            phone_number = HotelProxyForFacebookFeed.get_phone_number(hotel_obj)
            if phone_number == "":
                phone_number = DEFAULT_PHONE
            if not landmark or landmark == "":
                landmark = str(hotel_obj.street)
            priority_flag = DynamicAdsService.is_hotel_on_priority(hotel_obj.id)
            list_value = [hotel_obj.id, hotel_obj.name, "Flat 50% off on all hotels!", "Treebo",
                          hotel_obj.street,
                          hotel_obj.city.name, hotel_obj.city_id, hotel_obj.state.name,
                          'INDIA', hotel_obj.locality.pincode, hotel_obj.latitude, hotel_obj.longitude, landmark,
                          "",
                          prices_dict['rack_rate'], phone_number, "",
                          guest_rating["rating_system"], guest_rating["score"], guest_rating["number_of_reviewers"],
                          MAX_RATING, hotel_images[0]["url"], [hotel_images[0]["tag"]],
                          hotel_images[1]["url"],
                          [hotel_images[1]["tag"]], hotel_images[2]["url"], [hotel_images[2]["tag"]],
                          hotel_images[3]["url"], [hotel_images[3]["tag"]], hotel_images[4]["url"],
                          [hotel_images[4]["tag"]],
                          "Treebo Rewards", DynamicAdsService.get_new_hotel_url(hotel_obj), "",
                          prices_dict['sell_rate'], priority_flag]
            list_value = [str(x) if type(x) not in [str, list] else x for x in
                          list_value]
            logger.info("Hotel list %s",list_value)
            return list_value
        except:
            logger.error("Couldn't write for values %s",hotel_obj.name)
            raise Exception

    @staticmethod
    def get_address(hotel):
        """
        Need to give facebook address in a particular format
        :param hotel:
        :return:
        """
        fb_address = {'addr1': str(hotel.street),
                      'city': str(hotel.city.name),
                      'city_id': str(hotel.city_id),
                      'region': str(hotel.state.name),
                      'country': 'INDIA',
                      'postal_code': str(hotel.locality.pincode)}
        return fb_address

    @staticmethod
    def get_image_set(hotel):
        """
        For Image set
        We are using only 2 but have provisions for more
        :param hotel:
        :return:
        """
        fb_image_set = []
        for image in hotel.image_set.all():
            fb_image = {
                "url": image.url.name + str("?w=300&h=300"),
                "tag": "room types"
            }

            fb_image_set.append(fb_image)
        logger.info("Got Images As %s",fb_image_set)
        return fb_image_set

    @staticmethod
    def get_nearby_landmark(hotel):
        """
        For nearby Landmark
        :param hotel:
        :return:
        """
        footer_data = ContentServiceClient.getValueForKey(None, 'footer_data', "1")
        try:
            nearby_landmarks = (NearbyLandmarks(hotel.city, footer_data,hotel.latitude,hotel.longitude,footer_data['no_of_hotels_cap'])).build()
            return nearby_landmarks['content'][0]['label']
        except:
            logger.error("Error in Fetching Ladmark For %s",hotel.name)
            return str(hotel.street)

    @staticmethod
    def get_guest_rating(hotel):
        """
        For getting guest rating from Tripadvisor
        :param hotel:
        :return:
        """
        logger.info("Getting Guest Rating For Hotel %s",hotel.name)
        hotel_rating_response=DynamicAdsService.get_hotel_overall_star_rating(hotel.id)
        logger.info("Got Response From Hotel_Rating as %s",hotel_rating_response)
        hotel_rating = {
            "score": hotel_rating_response[0],
            "number_of_reviewers": hotel_rating_response[1],
            "rating_system":"TripAdvisor"
        }
        return hotel_rating

    @staticmethod
    def get_phone_number(hotel):
        """
        Getting 10 digits phone number concateneted with +91
        :param hotel:
        :return:
        """
        unmodified_phone = str((hotel.phone_number).replace("-", ""))
        unmodified_phone = unmodified_phone.replace(" ", "")
        unmodified_phone = unmodified_phone.lstrip('0')
        if len(unmodified_phone) < 10 or len(unmodified_phone) > 10:
            logger.error("phone number %s", unmodified_phone)
            return ""
        complete_phone = "+91"+" " + unmodified_phone
        return complete_phone

    def get_app_file_name(self):
        return None

    def get_app_file_path(self):
        return None


class HotelProxyForCriteo(object):
    email_subject = 'Reg Hotel data for Crieto ads'
    email_body = 'Please find the attached hotel data csv.'
    file_path = settings.DYNAMIC_ADS_FILE_PATH
    file_name = 'Criteo_ads_hotel_feed.csv'
    file_path_name = file_path + file_name

    @staticmethod
    def get_headers():
        headers = ['Property ID', 'Property name', 'Final URL', 'Image URL', 'Destination name', 'Description', 'Price',
                   'Sale price', 'Star rating', 'Category', 'Contextual keywords', 'Address', 'City',
                   'Tracking template', 'Amenities', 'Custom parameter', 'Final mobile URL', 'Priority']
        return headers

    @staticmethod
    def as_list(hotel_obj, for_app=False):
        try:
            prices_dict = DynamicAdsService.get_room_prices(hotel_obj.id, for_app)
            hotel_final_url = DynamicAdsService.get_new_hotel_url(hotel_obj)
            hotel_image_url = DynamicAdsService.get_image_url(hotel_obj)
            hotel_address = str(hotel_obj.street + hotel_obj.city.name + hotel_obj.state.name)
            priority_flag = DynamicAdsService.is_hotel_on_priority(hotel_obj.id)
            list_values = [hotel_obj.id, hotel_obj.name, hotel_final_url,
                           hotel_image_url, hotel_obj.locality.name,
                           "Flat 50% off on all hotels!",
                           prices_dict['rack_rate'], prices_dict['sell_rate'],
                           "", "", "", hotel_address, hotel_obj.city.name, "",
                           "", "", hotel_final_url, priority_flag]
            list_values = [str(x) if type(x) not in [str, list] else x for x in
                           list_values]
            logger.info("Created A list for criteo feed %s ", json.dumps(list_values))
            return list_values
        except Exception as e:
            logger.error("Failed For Writing Data %s %s", hotel_obj.name, e)
            raise Exception

    def get_app_file_name(self):
        return 'Criteo_ads_hotel_feed_for_app.csv'

    def get_app_file_path(self):
        file_name = self.get_app_file_name()
        return self.file_path + file_name


class DynamicAdsFactory(object):

    @staticmethod
    def get_proxy(feed_type):
        """
        To get proxy object for either google or facebook
        :param feed_type:
        :return:
        """
        if 'google' in feed_type:
            proxy_object = HotelProxyForGoogleFeed()
        elif 'facebook' in feed_type:
            proxy_object = HotelProxyForFacebookFeed()
        elif 'criteo' in feed_type:
            proxy_object = HotelProxyForCriteo()
        else:
            raise TreeboException('Feed type not provided')
        return proxy_object
