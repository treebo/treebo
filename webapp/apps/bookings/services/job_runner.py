from django.conf import settings
from easyjoblite import orchestrator, response, constants, job
from apps.bookings.models import AsyncJobStatus
from apps.bookings.constants import AsyncJobStates
from common.custom_logger.booking.log import get_booking_logger
from base.db_decorator import db_retry_decorator
import json
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = get_booking_logger(__name__)

job_runner = orchestrator.Orchestrator(
    rabbitmq_url=settings.BROKER_URL,
    config_file=settings.EASY_JOB_LITE_SETTINGS_PATH)
print(("The rabbitmq url is %s", settings.BROKER_URL))
job_runner.setup_entities()



logger.info('Initialize easy joblite config %s',settings.EASY_JOB_LITE_SETTINGS_PATH)
logger.info('init easy joblite config BROKER_URL %s',settings.BROKER_URL)


def enqueue_local_job(api, tag, data, notification_handler):
    """
    :param api:
    :param tag:
    :param data:
    :param notification_handler:
    :return:
    """
    booking_id = None
    if "booking_id" in data:
        booking_id = data["booking_id"]
    data_json = json.dumps(data)
    logger.info("The rabbitmq url being used for enque is %s", settings.BROKER_URL)
    job_id, retry_count = None, 0

    while True:
        retry_count += 1
        if retry_count > 3:
            slack_alert.send_slack_alert_for_exceptions(
                status=500,
                request_param=data.get('booking_id', None),
                message="Exception occured while booking enqueue",
                class_name="function enqueue_local_job")

            update_job_status(
                job.EasyJob.generate_id(),
                400,
                tag,
                data,
                message="Exception occurred while booking enqueue",
                status=AsyncJobStates.FAILURE_HANDLE_INIT)
            break
        try:
            job_id = job_runner.enqueue_job(
                api,
                constants.API_LOCAL,
                tag=tag,
                data=data,
                notification_handler=notification_handler)
            break
        except Exception as e:
            logger.exception("Error in enqueue job")
            continue
    try:
        async_job_status, _ = AsyncJobStatus.objects.get_or_create(
            job_id=job_id, defaults=dict(code=100, status=AsyncJobStates.CREATED))
        async_job_status.data = data_json
        async_job_status.tag = tag
        async_job_status.booking_id = booking_id
        async_job_status.save()
    except Exception:
        logger.exception(
            'AsyncJobStatus creation failed for job_id {0}'.format(job_id))
    return job_id


def create_job_response(status_code, message=None, data=None):
    """
    :param status_code:
    :param message:
    :param data:
    :return:
    """

    return response.EasyResponse(status_code, message, data)


@db_retry_decorator()
def update_job_status(
        job_id,
        code,
        tag=None,
        data=None,
        message=None,
        status=AsyncJobStates.FAILED):
    """
    :param job_id:
    :param code:
    :param tag:
    :param data:
    :param message:
    :param status:
    :return:
    """
    try:
        async_job_status, _ = AsyncJobStatus.objects.get_or_create(
            job_id=job_id)
        async_job_status.code = code
        try:
            if message:
                async_job_status.message = message[0:250]
        except Exception as e:
            logger.error("Error in trying to get message from job response")

        if code == 200:
            async_job_status.status = AsyncJobStates.SUCCESS
        else:
            async_job_status.status = status
        if tag:
            async_job_status.tag = tag
        if data:
            if "booking_id" in data:
                async_job_status.booking_id = data["booking_id"]
            try:
                async_job_status.data = json.dumps(data)
            except Exception as e:
                logger.info("Exception in serializing data")
                logger.exception(e)
        async_job_status.save()
        logger.info(
            'AsyncJob Status updated {0}'.format(
                async_job_status.__dict__))
    except AsyncJobStatus.DoesNotExist:
        logger.exception(
            'AsyncJob Status update failed for job_id {0} , to status {1}'.format(
                job_id, status))
