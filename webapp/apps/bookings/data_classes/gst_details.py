# -*- coding: utf-8 -*-
from apps.common.data_classes.address import Address
from apps.common.data_classes.base_data_class import BaseDataClass


class GstDetails(BaseDataClass):
    def __init__(self, legal_name, gstin_number, address):
        self.legal_name = legal_name or ''

        self.legal_name = self.legal_name.title()

        self.gstin_number = gstin_number or ''
        assert isinstance(address, Address)
        self.address = address

        self.crs_id = None

    def __eq__(self, other):
        if isinstance(other, GstDetails):
            return (self.legal_name == other.legal_name
                    and self.gstin_number == other.gstin_number
                    and self.address == other.address
                    )
        return NotImplemented

    def __hash__(self):
        return hash(tuple([self.legal_name, self.gstin_number, self.address]))

    def __repr__(self):
        return "<{kls}:({u} {g} {a})>".format(kls=self.__class__.__name__,
                                              u=self.legal_name,
                                              g=self.gstin_number,
                                              a=self.address,
                                              )

    def __str__(self):
        return "{u}, {g}, {a}".format(a=self.address,
                                      u=self.legal_name,
                                      g=self.gstin_number,
                                      )
