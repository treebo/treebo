# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0077_auto_20180424_1158'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='locality',
            name='free_text_url',
        ),
    ]
