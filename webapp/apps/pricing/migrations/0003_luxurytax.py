# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('pricing', '0002_roomrateplan'),
    ]

    operations = [
        migrations.CreateModel(
            name='LuxuryTax',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('start_range', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                (
                    'end_range',
                    models.DecimalField(default=100000, max_digits=20, decimal_places=10)),
                ('tax_value', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('state', models.ForeignKey(to='dbcommon.State', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_luxurytax',
            },
        ),
    ]
