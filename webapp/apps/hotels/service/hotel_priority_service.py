import requests
from django.conf import settings
import logging

logger = logging.getLogger(__name__)


class HotelPriorityService:

    @staticmethod
    def get_priority_hotels(hotel_ids):

        prf_hotels_list = []

        url = settings.PSYC_TRIGGER_GET_PRIORITY + "priority/"
        params = {
            "hotel_ids": str(",".join(str(hotel_id) for hotel_id in hotel_ids))
        }
        response = HotelPriorityService.call_psc_trigger(url=url, params=params)

        if response:
            response_data = response.get('data')
            for hotel_id, is_priority in response_data.items():
                if is_priority:
                    prf_hotels_list.append(hotel_id)

        return prf_hotels_list

    @staticmethod
    def check_priority(hotel_id):
        priority_hotel_list = HotelPriorityService.get_priority_hotels([hotel_id])
        if hotel_id in priority_hotel_list:
            return True
        return False

    @classmethod
    def call_psc_trigger(cls, url, params):
        response = requests.get(url=url, params=params)

        if response.status_code == 200:
            return response.json()
        else:
            logger.error("Exception occurred while calling psyc trigger get priority api url = %s, "
                         "status_code = %s", str(url), str(response.status_code))

        return None
