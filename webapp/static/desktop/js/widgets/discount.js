import _ from 'underscore';
import React from 'react';
import classNames from 'classnames';
var treejax = require('../treebolibs/treejax');
import parsley from 'parsleyjs';
import $ from 'jquery';


// TODO - Publish discount
// Total Discount

/* hover card plugin
 * @author a3itjain
 * */

(function($) {
    $.fn.hovercard = function(options) {

        //Set defauls for the control
        var defaults = {
            openOnLeft: false,
            openOnTop: false,
            delay: 0,
            autoAdjust: false,
            onHoverIn: function() {},
            onHoverOut: function() {}
        };
        //Update unset options with defaults if needed
        var options = $.extend(defaults, options);

        //Executing functionality on all selected elements
        return this.each(function() {
            var obj = $(this);
            //toggle hover card details on hover
            obj.closest(".hc-preview").hover(function() {
				var $this = $(this);
                adjustToViewPort($this);
                var curHCDetails = $this.find(".hc-details").eq(0);
                curHCDetails.stop(true, true).delay(options.delay).fadeIn();


                //Default functionality on hoverin, and also allows callback
                if (typeof options.onHoverIn == 'function') {
                    //Callback function
                    options.onHoverIn.call(this);
                }

            }, function() {
                var $this = $(this);
                $this.find(".hc-details").eq(0).stop(true, true).fadeOut(300, function() {
                    $this.css("zIndex", "0");
                    $(".carousel-inner").css("overflow", "hidden");
                    if (typeof options.onHoverOut == 'function') {
                        options.onHoverOut.call(this);
                    }
                });
            });


            obj.closest(".hc-preview").click(function() {
                var $this = $(this);
                adjustToViewPort($this);
                var curHCDetails = $this.find(".hc-details").eq(0);
                curHCDetails.stop(true, true).delay(options.delay).fadeIn();

                $this.css("zIndex", "200");
                $(".carousel-inner").css("overflow", "visible");
                //Default functionality on hoverin, and also allows callback
                if (typeof options.onHoverIn == 'function') {
                    //Callback function
                    options.onHoverIn.call(this);
                }

            })

            //Opening Directions adjustment
            function adjustToViewPort(hcPreview) {

                var hcDetails = hcPreview.find('.hc-details').eq(0),
                    hcPreviewRect = hcPreview[0].getBoundingClientRect(),
                    hcdTop = hcPreviewRect.top - 20, //Subtracting 20px of padding;
                    hcdRight = hcPreviewRect.left + 35 + hcDetails.width(), //Adding 35px of padding;
                    hcdBottom = hcPreviewRect.top + 35 + hcDetails.height(), //Adding 35px of padding;
                    hcdLeft = hcPreviewRect.top - 10; //Subtracting 10px of padding;

                //Check for forced open directions, or if need to be autoadjusted
                if (options.openOnLeft || (options.autoAdjust && (hcdRight > window.innerWidth))) {
                    hcDetails.addClass("hc-details-open-left");
                } else {
                    hcDetails.removeClass("hc-details-open-left");
                }
                if (options.openOnTop || (options.autoAdjust && (hcdBottom > window.innerHeight))) {
                    hcDetails.addClass("hc-details-open-top");
                } else {
                    hcDetails.removeClass("hc-details-open-top");
                }
            }


        });
    };
})($);


let ApplyScreen = React.createClass({
	getInitialState(){
		return {
			"currentcoupon":'',
			"couponerror": this.props.couponerror || ""
		}
	},

	componentDidMount(){
		var discountParsley = $('.discount__form').parsley();
		var self = this;
        discountParsley.on('form:submit',function(){
        	self.props.applyCoupon(self.state.currentcoupon);
            return false;
        });
	},

	componentWillReceiveProps(nextProps){
		this.setState({
			currentcoupon:nextProps.couponcode,
			couponerror:nextProps.couponerror
		});
	},

	handleCouponChange(evt){
		this.setState({currentcoupon: evt.target.value});
	},

	clickHandler(){
		this.setState({ couponerror:'' });
	},

	render(){
		var rootClass = classNames({
	      'apply-screen': true,
	      'hide': !this.props.visible
	    });

		return (

			<div className={rootClass}>
				<div className="flex-column js-applycouponwidget">
					<div className="flex-row">
						<form className="discount__form flex-row">
							<input className="analytics-voucher discount__voucher" required="true" value={this.state.currentcoupon} onChange={this.handleCouponChange}
                            data-parsley-required="true"

                            data-parsley-required-message="Sorry! This coupon code is not valid"
                            data-parsley-errors-container="#discountError" type="text" placeholder="Enter Promo Code"/><input onClick={this.clickHandler} type="submit" value="Apply" className="analytics-apply flex-row flex--justify-center flex--align-center  btn discount__applybtn js-couponbtn"/>
		                </form>
	                </div>
	                <div id="discountError" className="error text-left">
	                	{this.state.couponerror}
	                </div>
				</div>
			</div>
		);
	}
});


let CouponHeader = React.createClass({
	render(){

		var rootClass = classNames({
		  'mb10':true,
	      'hide': !this.props.visible,
	      'coupon-header':true,
        'apply-coupon' :true
	    });
		return (
			<div className={rootClass}>
					<span>Have a coupon ?</span>
					&nbsp;<a onClick={this.props.showApplyCouponWidget} className="js-applycoupon link" href="javascript:void(0);">Apply Coupon Code</a>
			</div>
		)
	}
});


var AppliedScreen = React.createClass({

	discountRemoveHandler(){
		this.props.remove();
	},
	 componentDidMount() {
		 $('#couponDesc').hovercard();
	 },

	 componentDidUpdate() {
		 $('#couponDesc').hovercard();
	 },

	render(){
		var rootClass = classNames({
	      'discount__applied-screen': true,
	      'flex-column':true,
	      'hide': !this.props.visible,
	      'applied-coupon':true,
	    });
    var discount_show;
    if(this.props.voucherAmount) {
      discount_show = (<div className='discount__discount-price'> - <i className="icon-rupee mr5 "></i><span className="analytics-discountvalue">{this.props.voucherAmount}</span> </div>);
    } else {
      discount_show = (<div className='discount__discount-price'> - <i className="icon-rupee mr5 "></i><span className="analytics-discountvalue">{this.props.discount}</span> </div>);
    }
		var removeLink = this.props.autopromo == false ? <a className="ml5 link" onClick={this.discountRemoveHandler} href="javascript:void(0)">Remove</a> : "";

		var offerUrl = "/offers/#"+this.props.couponcode;
        var desc = this.props.couponDesc && this.props.couponDesc.trim() ? (<div className="analytics-coupon discount__applied-block__desc hc-preview" id="couponDesc">
                      <label id="demo-basic" className="hc-name ellipsis">{this.props.couponDesc}</label>
                      <div className="hc-details box-shadow"><p>{this.props.couponDesc}</p></div>
                    </div> ) : '';
		return (
			<div className={rootClass}>
				<div className="flex-row details">
					<div className="text-right coupon-value flex-column discount__applied-block itinerary-discount-change">
						<div className="grayTxt discount__applied-block__tagline">Coupon Code Applied</div>
                        {desc}
					</div>
					<div className="text-right flex-column">
            {discount_show}
						<div>{removeLink}</div>
					</div>
				</div>
			</div>
		);
	}
});

var Loader = React.createClass({
	render(){

		var rootClass = classNames({
			'loader-container' : true,
			'loader-container--inline': true,
	      	'hide': !this.props.visible
	    });

		return (
			<div  className={rootClass}>
				<div className="loader-box v-center">
					<div className="sk-fading-circle small">
						<div className="sk-circle1 sk-circle"></div>
						<div className="sk-circle2 sk-circle"></div>
						<div className="sk-circle3 sk-circle"></div>
						<div className="sk-circle4 sk-circle"></div>
						<div className="sk-circle5 sk-circle"></div>
						<div className="sk-circle6 sk-circle"></div>
						<div className="sk-circle7 sk-circle"></div>
						<div className="sk-circle8 sk-circle"></div>
						<div className="sk-circle9 sk-circle"></div>
						<div className="sk-circle10 sk-circle"></div>
						<div className="sk-circle11 sk-circle"></div>
						<div className="sk-circle12 sk-circle"></div>
					</div>
				</div>
			</div>
		)
	}
});


var Discount = React.createClass({

	getInitialState(){
		return {
			autopromo:this.props.data.autopromo===true ? true : false,
			applyscreenvisible:false,
			appliedscreenvisible:this.props.data.couponcode==='' ? false : true,
			couponheadervisible:this.props.data.couponcode==='' ? true : false,
			couponcode:this.props.data.couponcode,
			couponerror:'',
			discount:this.props.data.discount ? this.props.data.discount : 0,
      voucherAmount:this.props.data.voucherAmount ? this.props.data.voucherAmount : 0,
			couponDesc:this.props.data.couponDesc,
			loadervisible:false
		}
	},

	componentWillReceiveProps(nextProps){
		var newState = {
			applyscreenvisible:false,
			appliedscreenvisible:nextProps.data.couponcode==='' ? false : true,
			couponheadervisible:nextProps.data.couponcode==='' ? true : false,
			couponcode:nextProps.data.couponcode,
			couponerror:nextProps.data.discountError||'',
			discount:nextProps.data.discount ? nextProps.data.discount : 0,
      voucherAmount:nextProps.data.voucherAmount ? nextProps.data.voucherAmount : 0,
			couponDesc:nextProps.data.couponDesc
		}

		this.setState(_.extend({},this.state,newState));
	},

	showApplyCouponWidget(){
		this.setState({
			couponheadervisible:false,
			applyscreenvisible:true
		});
	},


	onApplySuccess(data){
		this.setState({
			appliedscreenvisible:true,
			applyscreenvisible:false,
			couponcode:data.couponcode,
			discount:data.discount,
      voucherAmount:data.voucherAmount,
			couponDesc:data.couponDesc,
			autopromo:data.autopromo
		});
	},

	onCouponRemove(){

		var self = this;
		this.setState({
			appliedscreenvisible:true,
			applyscreenvisible:false,
			couponheadervisible:false,
			couponcode:""
		});
		if(this.state.autopromo==false){
			this.applyCoupon("","remove");

		}
	},

	applyCoupon(couponcode,mode){
		var data = this.props.data.query_param;
    couponcode = couponcode.trim();
		data['couponcode'] = couponcode;

		var self = this;
		self.setState({loadervisible:true})
		var queryparams = $.param(data);
		treejax({
            url: self.props.data.url + queryparams,
            type: 'GET',
        }).then(function (data, status, xhr) {
			if(data.discountError){
				self.setState({ couponerror:"Sorry! This coupon code is not valid" });
			}else if(data.couponcode && data.couponcode.trim()!=""){
				self.onApplySuccess(data);
				$('body').trigger('discount:applied',{code:data.couponcode,value:data.discount, voucher_value:data.voucherAmount, priceobj:data});
			}

			if(mode && mode=="remove"){
				self.setState({
					appliedscreenvisible:false,
					applyscreenvisible:false,
					couponheadervisible:true,
					couponcode:""
				});
				$('body').trigger('discount:removed',{value:data.discount, voucher_value:data.voucherAmount, priceobj:data});
			}

        },
        function (treeboError) {
            console.log(treeboError);
            self.setState({ couponerror:"Sorry! This coupon code is not valid" });
        }).finally(function(){
			self.setState({loadervisible:false})

		});
	},

	render(){
		return (
			<div className="discount pos-rel">
				<Loader visible={this.state.loadervisible}/>
				<CouponHeader visible={this.state.couponheadervisible} showApplyCouponWidget={this.showApplyCouponWidget}/>

				<ApplyScreen visible={this.state.applyscreenvisible} couponcode={this.state.couponcode} applyCoupon={this.applyCoupon} couponerror={this.state.couponerror}/>

				<AppliedScreen autopromo={this.state.autopromo} visible={this.state.appliedscreenvisible} couponcode={this.state.couponcode} remove={this.onCouponRemove} discount={this.state.discount} voucherAmount = {this.state.voucherAmount} couponDesc={this.state.couponDesc}/>
			</div>

		);
	}
});


module.exports = Discount;
