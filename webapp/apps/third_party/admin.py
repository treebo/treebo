from django.contrib import admin

from apps.third_party.models import TrivagoFileUploadModel


@admin.register(TrivagoFileUploadModel)
class TrivagoFileUploadModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'modified_at', 'trivago_file',)
