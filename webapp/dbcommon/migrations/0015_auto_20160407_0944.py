# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0014_facility_rooms'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='anniversary_date',
            field=models.DateField(
                default='9999-01-01',
                verbose_name='anniversary',
                blank=True),
        )]
