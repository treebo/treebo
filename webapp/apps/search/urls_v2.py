from django.conf.urls import url

from apps.search.api.v2.hotel_search import HotelSearchApi
from apps.search.api.v2.search_api import SearchAPI

urlpatterns = [
    url(r'hotels$', HotelSearchApi.as_view(), name='hotel_search_api'),
    url(r'', SearchAPI.as_view(), name='search_api'),
]
