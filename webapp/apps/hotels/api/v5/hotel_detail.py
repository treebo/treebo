import logging
from random import randint
from django.core.cache import cache
from rest_framework import status
from rest_framework.response import Response

from apps.hotels.service.hotel_policy_service import HotelPolicyService
from apps.hotels.hotel_detail_utilities import get_categorised_images, get_hotel_directions, \
    get_categorised_guest_supported_info, get_user_reviews, get_categorised_images_v5
from apps.hotels.service.hotel_service import HotelService
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from services.restclient.contentservicerestclient import ContentServiceClient
from data_services.exceptions import HotelDoesNotExist
from apps.hotels.service.hotel_sub_brand_service import HotelSubBrandService

logger = logging.getLogger(__name__)

hotel_repository = RepositoriesFactory.get_hotel_repository()
facility_repository = RepositoriesFactory.get_facility_repository()
image_repository = RepositoriesFactory.get_image_repository()
hotel_brands = HotelSubBrandService


class HotelDetail(TreeboAPIView):
    CACHE_TIMEOUT = 60 * 60 * 24 + randint(0, 20)
    CACHE_KEY = 'cs_apicache_hotel_detail_v5_{0}'

    def get_cache_key(self, hotel_id):
        return HotelDetail.CACHE_KEY.format(hotel_id)

    @staticmethod
    def invalidate_cache(hotel_id=None):
        logger.info("Invalidating cache")
        if hotel_id:
            cache.delete(HotelDetail.CACHE_KEY.format(hotel_id))
        else:
            cache.delete_pattern(HotelDetail.CACHE_KEY.format('*'))

    def get(self, request, *args, **kwargs):
        """
        Model Dependencies: Hotel, Facility, Image, Room, ContentStore.
        Gives Hotel details.
        """
        hotel_id = int(kwargs.get('hotel_id'))
        try:
            result = cache.get(self.get_cache_key(hotel_id))
        except Exception as e:
            result = None
        if result:
            return Response(result)

        try:
            hotel = hotel_repository.get_hotel_details_with_all_related_objects(
                hotel_id, direct=True)
        except HotelDoesNotExist:
            return Response({'Bad Request': "Hotel Does not Exist"},
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            rooms_set = hotel_repository.get_rooms_for_hotel(hotel)
            directions_set = hotel_repository.get_directions_set_for_hotel(hotel)
            facility_set = hotel_repository.get_facilities_for_hotel(hotel)
            images = hotel.get_categorised_images()
            images = get_categorised_images_v5(images)
            rooms = get_categorised_guest_supported_info(rooms_set)
            directions_css_dist_list = get_hotel_directions(directions_set)
            promises = ContentServiceClient.getValueForKey(
                request, 'promises_list', 1)
            policies = HotelPolicyService.get_hotel_policies(hotel_id=hotel_id)
            reviews = get_user_reviews(hotel)
            hotel_facilities_list = [
                {"name": facility.name, "css_class": facility.css_class}
                for facility in facility_set
            ]

            brands = hotel_brands.get_brands_from_db(hotel)

            hotel_essential_information = {
                "id": hotel_id,
                "name": HotelService().get_display_name_from_hotel_name(hotel.name),
                "phone": hotel.phone_number,
                "is_active": bool(hotel.status),
                "cs_id": hotel.cs_id,
                "address": {
                    "city": hotel.city.name,
                    "city_id": hotel.city.id,
                    "street": hotel.street,
                    "locality": hotel.locality.name,
                    "landmark": hotel.landmark,
                    "state": hotel.state.name if hotel.state else None,
                    "pincode": hotel.locality.pincode if hotel.locality else None},
                "coordinates": {
                    "lat": hotel.latitude,
                    "lng": hotel.longitude},
                "description": hotel.description,
                "images": images,
                "promises": promises,
                "rooms": rooms,
                "hotel_policies": policies,
                "facilities": hotel_facilities_list,
                "trilights": [
                    highlight.description for highlight in
                    hotel_repository.get_highlights_for_hotel(hotel)],
                "accessibilities": directions_css_dist_list,
                "reviews": reviews,
                "property": {
                    "provider": hotel.provider_name,
                    "type": hotel.property_type},
                "brands": brands,
                "hygiene_shield_name": hotel.hygiene_shield_name
            }
            cache.set(self.get_cache_key(hotel_id), hotel_essential_information,
                      HotelDetail.CACHE_TIMEOUT)
        except Exception as e:
            logger.exception(
                "While retrieving Hotel Details, getting error {error}".format(error=str(e)))
        return Response(hotel_essential_information)
