import os
import zipfile

from fabric.api import cd, sudo
from fabric.api import task
from ilogue.fexpect import expect, expecting

"""fab deploy_test -i ../../hotel-treebo.pem -H ubuntu@test.treebo.com"""


@task
def deploy_test():
    prompts = []
    # prompts += expect("Password for \'https://kulizadevserver@bitbucket.org\':", "kuliza123")
    prompts += expect("Password for \'https://ashu0992sharma@bitbucket.org\':",
                      "ashutosh@123")
    code_dir = '/home/ubuntu/treebo/treebo'
    # code_dir = '/home/kuliza200/Desktop/treebov5/treebo'
    workon = 'source ../bin/activate && '
    with cd(code_dir):
        # sudo("git branch")
        # sudo("git status")
        # sudo("git stash")
        with expecting(prompts):
            # run("sudo git pull treebo treebo-release-6")
            # sudo("git stash pop")
            sudo("pip install -r requirements.txt")
            sudo("python manage.py migrate")
            sudo("python manage.py collectstatic")
            sudo(
                "scp -r webapp/static_pipeline/css/home_min.css webapp/static/css/home_min.css")
            sudo(
                "scp -r webapp/static_pipeline/css/searchv4_min.css webapp/static/css/searchv4_min.css")
            sudo(
                "scp -r webapp/static_pipeline/css/detail_min.css webapp/static/css/detail_min.css")
            sudo(
                "scp -r webapp/static_pipeline/css/confirmation_min.css webapp/static/css/confirmation_min.css")
            sudo(
                "scp -r webapp/static_pipeline/css/whyus_min.css webapp/static/css/whyus_min.css")
            sudo(
                "scp -r webapp/static_pipeline/css/join_our_network_min.css webapp/static/css/join_our_network_min.css")
            sudo(
                "scp -r webapp/static_pipeline/css/city_min.css webapp/static/css/city_min.css")

            sudo("scp -r webapp/static_pipeline/js/min_js.js webapp/static/js/min_js.js")
            sudo(
                "scp -r webapp/static_pipeline/js/confirmation_min.js webapp/static/js/confirmation_min.js")
            sudo(
                "scp -r webapp/static_pipeline/js/detail_min.js webapp/static/js/search/detail_min.js")
            sudo(
                "scp -r webapp/static_pipeline/js/searchv4_min.js webapp/static/js/search/searchv4_min.js")

        # sudo("git stash pop")
        # run("sudo git pull treebo treebo-release-6")
        # run("sudo git pull origin treebo")
        # sudo("fuser -n tcp -k 8001")
        # sudo("fuser -n tcp -k 8000")
        # sudo(workon + "dtach -n `mktemp -u /tmp/treebo.XXXX` nohup python manage.py runserver 0.0.0.0:8000", pty=False)

        # sudo(workon + "export DJANGO_CONFIGURATION=Development")
        # sudo("dtach -n `mktemp -u /tmp/treebo.XXXX` python manage.py runserver 0.0.0.0:80", pty=False)
        # sudo( "export DJANGO_CONFIGURATION=Development")
        # sudo("echo $DJANGO_CONFIGURATION")

        # sudo (workon+ "supervisorctl restart treebo")
        print("Started server")


@task
def zip_move():
    zip_file = '../../treebotest.zip'
    zipf = zipfile.ZipFile(zip_file, 'w')
    path = './'
    #	code_dir = '/home/ubuntu/treebo/treebo_test'
    with cd(path):
        # sudo("git branch")
        sudo("ls")
        for root, dirs, files in os.walk(path):
            print(root)
            print(dirs)
            print(path)
            for file in files:
                zipf.write(os.path.join(root, file))
        zipf.close()
        print('Done')


@task
def extract():
    code_dir = '/home/ubuntu/treebo/treebo'
    zip_file_path = '../../Python.zip'
    extract_path = "./"
    with cd(code_dir):
        with zipfile.ZipFile(zip_file_path, "r") as z:
            z.extractall(extract_path)


@task
def remove_git():
    code_dir = '/home/ubuntu/treebo_test/treebo'
    with cd(code_dir):
        sudo("find . | grep .git | xargs rm -rf")
