# TODO: Add logging
from log_request_id import local

thread_locals = local


def get_current_user():
    """
    :return:
    """
    return getattr(thread_locals, 'user_id', None)


def get_current_request_id():
    """
    :return:
    """
    return getattr(thread_locals, 'request_id', None)


def set_thread_variable(key, val):
    setattr(thread_locals, key, val)


def get_thread_variable(key, default=None):
    return getattr(thread_locals, key, default)


def delete_thread_variable(key):
    if hasattr(thread_locals, key):
        delattr(thread_locals, key)
