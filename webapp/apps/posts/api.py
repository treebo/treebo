__author__ = 'ansilkareem'

from http import HTTPStatus
import logging

from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.posts.tasks import submit_question, get_faqs
from apps.common.slack_alert import SlackAlertService as slack_alert
from rest_framework.response import Response
from base.views.api import TreeboAPIView

from webapp.apps.posts.serializers import FaqsRequestSerializer

logger = logging.getLogger(__name__)


class PostQuestionAPI(TreeboAPIView):
    authentication_classes = [CsrfExemptSessionAuthentication, ]

    def post(self, request, *args, **kwargs):
        try:
            logger.info('New Question Add request', request.data)
            if request.user.is_authenticated():
                question = request.data.get('question')
                page = request.data.get('page')
                user = request.user
                username = ' '.join(
                    [user.first_name, user.middle_name, user.last_name])
                submit_question.delay(question, page, username)
                return Response({
                    'success': True
                })
            else:
                return Response({
                    'success': False,
                    'error_msg': 'User is not logged in'
                })
        except Exception as ex:
            return Response({
                'success': False,
                'error_msg': str(ex)
            })


class FaqsApi(TreeboAPIView):
    validationSerializer = FaqsRequestSerializer

    def get(self, request, *args, **kwargs):

        try:
            faq_request = self.serializerObject.validated_data
            faq_response = get_faqs(faq_request)
            return Response({
                'success': True,
                'data': faq_response,
                'status': HTTPStatus.OK
            })
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(
                status=HTTPStatus.INTERNAL_SERVER_ERROR, request_param=request.GET,
                message=e.__str__(),
                class_name=self.__class__.__name__)
            return Response(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                            data={
                                'msg': e.__str__()
                            })
