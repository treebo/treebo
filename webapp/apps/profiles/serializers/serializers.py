from rest_framework import serializers

from dbcommon.models.profile import User, UserOtp, UserEmail


class UserProfileSerializer(serializers.ModelSerializer):
    work_email = serializers.SerializerMethodField()
    anniversary = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            'work_email',
            'first_name',
            'last_name',
            'gender',
            'dob',
            'anniversary',
            'email',
            'phone',
            'city')

    def get_work_email(self, obj):
        try:
            work_email = UserEmail.objects.get(user=obj, email_type="W").email
        except UserEmail.DoesNotExist:
            work_email = obj.email
        return work_email

    def get_anniversary(self, obj):
        if obj.anniversary_date:
            return obj.anniversary_date.strftime('%Y-%m-%d')
        else:
            return obj.anniversary_date

    def get_phone(self, obj):
        return obj.phone_number


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('middle_name', 'is_staff', 'is_active', 'password')


class UserRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = (
            'middle_name',
            'is_staff',
            'is_active',
            'uuid',
            'gender',
            'dob',
            'is_guest',
            'city',
            'id',
            'last_login',
            'created_at',
            'modified_at',
            'user_permissions',
            'groups')


class UserActivationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'is_active')


class PhoneValidationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserOtp
        fields = '__all__'
