import logging

from django.core.management import BaseCommand
from django.conf import settings
from kombu import Connection, Queue, Exchange

from apps.hx_to_crs_hotel_migration.services.event_consumer import IntegrationEventConsumer

logger = logging.getLogger('queue_listeners')


class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            rabbit_url = settings.CRS_INTEGRATION_EVENT_RABBITMQ_SETTING['url']
            queue_name = settings.CRS_INTEGRATION_EVENT_QUEUE_NAME
            routing_key = settings.CRS_INTEGRATION_EVENT_RABBITMQ_SETTING['routing_key']

            exchange = Exchange(settings.CRS_INTEGRATION_EVENT_RABBITMQ_SETTING['exchange'], type="topic")
            queues = Queue(
                name=queue_name,
                exchange=exchange,
                routing_key=routing_key)

            with Connection(rabbit_url, heartbeat=4) as conn:
                worker = IntegrationEventConsumer(conn, queues)
                worker.run()
        except Exception as e:
            logger.exception(e)
