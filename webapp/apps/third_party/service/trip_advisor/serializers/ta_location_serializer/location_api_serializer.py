from rest_framework import serializers


class AddressSerializer(serializers.Serializer):
    street1 = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    street2 = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    city = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    state = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    country = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    postalcode = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    address_string = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)


class TripTypeSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    value = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    localized_name = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)


class UserLocationSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    id = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    user_location = UserLocationSerializer(required=False)


class ReviewSerializer(serializers.Serializer):
    id = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    lang = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    location_id = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    published_date = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    rating = serializers.IntegerField(required=False, initial=0)
    helpful_votes = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    rating_image_url = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
    url = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
    trip_type = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    travel_date = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    text = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    title = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    user = UserSerializer(required=False, allow_null=True)


class AncestorsSerializer(serializers.Serializer):
    abbrv = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    level = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    name = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    location_id = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)


class SubratingSerializer(serializers.Serializer):
    rating_image_url = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
    name = serializers.CharField(
        required=True,
        allow_blank=True,
        allow_null=True)
    value = serializers.CharField(
        required=True,
        allow_blank=True,
        allow_null=True)
    localized_name = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)


class RankingDataSerializer(serializers.Serializer):
    ranking_string = serializers.CharField(
        required=True, allow_blank=True, allow_null=True)
    ranking_out_of = serializers.CharField(
        required=True, allow_blank=True, allow_null=True)
    geo_location_id = serializers.CharField(
        required=True, allow_blank=True, allow_null=True)
    ranking = serializers.CharField(
        required=True,
        allow_blank=True,
        allow_null=True)
    geo_location_name = serializers.CharField(
        required=True, allow_blank=True, allow_null=True)
    ranking_string_detail = serializers.CharField(
        required=True, allow_blank=True, allow_null=True)


class CategorySerializer(serializers.Serializer):
    name = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    localized_name = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)


class SubCategorySerializer(serializers.Serializer):
    name = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    localized_name = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)


class AwardSerializer(serializers.Serializer):
    award_type = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    year = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    images = serializers.DictField(
        child=serializers.CharField(
            required=False,
            allow_blank=True,
            allow_null=True),
        required=False)
    categories = serializers.ListField(
        child=serializers.CharField(
            required=False,
            allow_null=True,
            allow_blank=True),
        required=False)
    display_name = serializers.CharField(
        required=True, allow_blank=True, allow_null=True)


class LocationAPISerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    address_obj = AddressSerializer(required=False, allow_null=True)
    latitude = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    longitude = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    rating = serializers.CharField(required=True)
    location_id = serializers.CharField(required=True)
    trip_types = TripTypeSerializer(many=True, required=False, allow_null=True)
    reviews = ReviewSerializer(many=True, allow_null=True)
    write_review = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
    ancestors = AncestorsSerializer(many=True, required=False, allow_null=True)
    hours = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    percent_recommended = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    partner_location_id = serializers.ListField(
        child=serializers.CharField(
            required=False,
            allow_null=True,
            allow_blank=True))
    review_rating_count = serializers.DictField(
        child=serializers.CharField(), required=True)
    subratings = SubratingSerializer(required=True, many=True)
    ranking_data = RankingDataSerializer(required=True, allow_null=True)
    photo_count = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True,
        default=0)
    location_string = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    web_url = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
    price_level = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    rating_image_url = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
    awards = AwardSerializer(required=True, many=True, allow_null=True)
    num_reviews = serializers.CharField(
        required=True, allow_blank=True, allow_null=True)
    category = CategorySerializer(required=False, allow_null=True)
    subcategory = SubCategorySerializer(
        required=False, many=True, allow_null=True)
    see_all_photos = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
