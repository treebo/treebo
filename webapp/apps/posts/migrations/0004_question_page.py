# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0003_auto_20171013_0713'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='page',
            field=models.CharField(max_length=5000, null=True, blank=True),
        ),
    ]
