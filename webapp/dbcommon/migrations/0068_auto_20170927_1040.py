# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0067_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='seo_image',
            field=models.TextField(default='', null=True, blank=True),
        ),
    ]
