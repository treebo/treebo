import logging

import requests
from django.core.urlresolvers import reverse

from services.restclient.request_wrapper import wrap_internal_api_call

logger = logging.getLogger(__name__)


class DiscountClient:
    def __init__(self):
        pass

    @staticmethod
    def getDiscountPrefixCount(request, prefix):
        count = 0
        url = request.build_absolute_uri(
            reverse(
                'discount:prefix-discount-count',
                kwargs={
                    'prefix': prefix}))
        response = wrap_internal_api_call(request, requests.get, url)
        # response = requests.get(url)
        if response.status_code == 200 or response.status_code == 304:
            count = response.json()['count']
        return int(count)

    @staticmethod
    def createCouponForUser(
            request,
            userId,
            couponCode,
            discountValue,
            maxUsage,
            maxDiscount,
            minValue,
            validDays,
            terms):
        url = request.build_absolute_uri(
            reverse('discount:create-user-discount'))
        payload = {
            'user_id': userId,
            'code': couponCode,
            'discount_operation': 'PERCENTAGE',
            'discount_value': discountValue,
            'max_available_usages': maxUsage,
            'max_available_discount': maxDiscount,
            'expiry': validDays,
            'terms': terms,
            'min_value': minValue}
        response = wrap_internal_api_call(
            request, requests.post, url, data=payload)
        # response = requests.put(url, data=payload)
        if response.status_code == 200 or response.status_code == 304:
            return response.json()['code']

    @staticmethod
    def getExternalDiscounts(request):
        url = request.build_absolute_uri(
            reverse('discount:applicable-discounts'))
        response = wrap_internal_api_call(request, requests.get, url)
        # response = requests.get(url)
        if response.status_code == 200 or response.status_code == 304:
            return response.json()
