# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0038_booking_member_discount'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='channel',
            field=models.CharField(
                default='',
                max_length=50,
                blank=True,
                choices=[
                    ('msite',
                     'msite'),
                    ('website',
                     'website'),
                    ('app',
                     'app'),
                    ('gdc',
                     'gdc'),
                    ('android',
                     'android'),
                    ('ios',
                     'ios')]),
        ),
    ]
