import time
import logging

LOG = logging.getLogger(__name__)


def timeit(logger=None):
    def timed(method):
        def wrapper(*args, **kw):
            ts = time.time()
            result = method(*args, **kw)
            te = time.time()
            log = logger if logger else logging.getLogger(__name__)
            log.info('method name: %r, execution time: %2.2f sec' % (method.__name__, te - ts))
            return result

        return wrapper

    return timed
