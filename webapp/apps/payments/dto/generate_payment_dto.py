from rest_framework import serializers


class GeneratePaymentDTO(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=9, decimal_places=2)
    bid = serializers.CharField(max_length=40)
    hotel_id = serializers.CharField(max_length=20)
    payment_mode = serializers.CharField(max_length=40)
