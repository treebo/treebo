# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.apiv1.views.contactus import SaveContactUs
from apps.apiv1.views.feedback import Feedback
from apps.apiv1.views.joinus import JoinSave
from apps.apiv1.views.notify_location import NotifyLocation
from apps.auth.api.v1.otp_api import OtpAPI, OtpVerifyAPI, OtpIsVerifiedAPI
from apps.hotels.api.v1.getallhotels import GetAllHotels

app_name = 'restapi'

urlpatterns = [
    url(r"^notify-location", NotifyLocation.as_view(), name='notify-location'),

    url(r"^joinussave/", JoinSave.as_view(), name='join-save'),
    url(r"^contactus/", SaveContactUs.as_view(), name='contactus-save'),
    url(r"^feedback/", Feedback.as_view(), name='feebback-save'),
    # FIXME DEPRECATED
    url(r"^getallhotels", GetAllHotels.as_view(), name="get-all-hotels"),
    url(r"^otp/$", OtpAPI.as_view(), name="send-otp"),
    url(r"^otp/isverified/$",
        OtpIsVerifiedAPI.as_view(),
        name="is_verified-otp"),
    url(r"^otp/verify/$", OtpVerifyAPI.as_view(), name="verify-otp"),
]
