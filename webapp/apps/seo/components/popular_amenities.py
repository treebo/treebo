import logging

from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.components.base_component import BaseComponent
from apps.seo.serializers.serializer import PopularAmenitiesSerializer
from dbcommon.models.facilities import CityAmenityMapforAutoPageCreation, Facility
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class PopularAmenities(BaseComponent):
    MAX_AMENITIES_TO_DISPLAY = 5

    def __init__(self, current_city, footer_data, exclude_list):
        super(PopularAmenities, self).__init__(current_city, footer_data, exclude_list)

    def build(self):
        city_amenity_objects = CityAmenityMapforAutoPageCreation.objects.filter(city=self.current_city, is_enabled=True).order_by('-modified_at')
        popular_amenity_id_list = []
        for obj in city_amenity_objects:
            hotels = Hotel.objects.filter(city=obj.city, facility__name=obj.amenity.name)
            if hotels:
                logger.info("Hotels exist with %s in %s", obj.amenity.name, obj.city.name)
                popular_amenity_id_list.append(obj.amenity.id)
            else:
                logger.info("No hotels with %s in %s", obj.amenity.name, obj.city.name)
        popular_amenity_id_list = popular_amenity_id_list[:PopularAmenities.MAX_AMENITIES_TO_DISPLAY]
        data = {'key': 'popular_amenities',
                'title': 'POPULAR AMENITIES IN %s' % self.current_city.name.upper(),
                'content': AutoCompleteResultBuilder.build_popular_amenity_result(
                    Facility.objects.filter(id__in=popular_amenity_id_list),
                    self.current_city
                )}
        serialized_obj = PopularAmenitiesSerializer(data=data)
        if serialized_obj.is_valid():
            return serialized_obj.validated_data
        else:
            return {
                'key': 'popular_amenities',
                'title': 'POPULAR AMENITIES IN %s' % self.current_city.name.upper(),
                'content': []
            }
