import logging
import requests
from django.conf import settings
from apps.common.slack_alert import SlackAlertService as slack_alert
import json

logger = logging.getLogger(__name__)


class GrowthServiceProvider:
    CANCELLATION_HASH_API = "/growth/cancellation/cancel-hash/"
    CANCELLATION_BOOKING_DETAILS_API = "/growth/cancellation/booking_details/{}"
    CANCEL_BOOKING_API = "/growth/cancellation/cancel-booking/"

    def cancellation_hash_api_call(self, data):
        response = self.__make_call(method="POST", page_name=self.CANCELLATION_HASH_API, data=data)
        if response.get("data"):
            return response
        logger.error("Unexpected response from realization client: {}".format(response))
        return

    def cancellation_booking_details_api_call(self, cancellation_hash):
        response = self.__make_call(method="GET", data=None,
                                    page_name=self.CANCELLATION_BOOKING_DETAILS_API.format(cancellation_hash))
        if response.get("data"):
            return response
        logger.error("Unexpected response from realization client: {}".format(response))
        return

    def cancel_booking_api_call(self, data):
        response = self.__make_call(method="POST", page_name=self.CANCEL_BOOKING_API, data=data)
        if response and response.get("data"):
            return response
        logger.error("Unexpected response from realization client: {}".format(response))
        return

    @staticmethod
    def __make_call(method, page_name, data):
        url = settings.GROWTH_REALIZATION_API_HOST + page_name
        logger.info('Calling growth api Url: {url} and Payload: {data}'.format(url=url, data=data))
        response = requests.request(method=method, url=url, data=json.dumps(data),
                                    headers={'Content-Type': 'application/json'},
                                    allow_redirects=True)

        if not response or not response.ok:
            slack_alert.send_slack_alert_for_third_party(status=response.status_code,
                                                         class_name=GrowthServiceProvider.__class__.__name__,
                                                         url=url,
                                                         reason=response.text,
                                                         data=data)
            return
        return response.json()
