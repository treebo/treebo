# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0017_hotel_price_increment'),
        ('pricing', '0004_statetax'),
    ]

    operations = [
        migrations.CreateModel(
            name='RoomPrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('date', models.DateField(db_index=True)),
                ('guest_count', models.IntegerField(default=1)),
                ('total_amount', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('tax_amount', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('promo_applied', models.BooleanField(default=False)),
                ('room', models.ForeignKey(to='dbcommon.Room', on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
