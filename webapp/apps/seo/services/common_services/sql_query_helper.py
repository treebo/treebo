from django.db import connection



class SqlQueryHelper:

    def __init__(self):
        pass

    @staticmethod
    def get_distance_query_str(latitude, longitude):
        # earth_radius = 3960.0  # for miles
        earth_radius = 6371.0  # for kms

        return str(earth_radius)+" * 2 * ASIN(SQRT(POWER(SIN(" \
               "(" + str(latitude) + " - latitude) * 0.0174532925 / 2), 2) + COS(" + \
               str(latitude) + " * 0.0174532925) * COS(latitude * 0.0174532925) * " \
                               " POWER(SIN((" + str(longitude) + " - longitude) * 0.0174532925 / 2), 2) ))"

    @staticmethod
    def get_distance_query_for_param_str():
        # earth_radius = 3960.0  # for miles
        earth_radius = 6371.0  # for kms

        return str(earth_radius) + " * 2 * ASIN(SQRT(POWER(SIN((%s - latitude) * 0.0174532925 / 2), 2) + " \
                                   "COS(%s * 0.0174532925) * COS(latitude * 0.0174532925) *  " \
                                   "POWER(SIN((%s - longitude) * 0.0174532925 / 2), 2) ))"

    @staticmethod
    def execute_query_and_map_to_dictionary(query_string):
        with connection.cursor() as cursor:
            cursor.execute(query_string)
            col_names = [desc[0] for desc in cursor.description]
            while True:
                row = cursor.fetchone()
                if row is None:
                    break
                row_dict = dict(list(zip(col_names, row)))
                yield row_dict
            return

    @staticmethod
    def execute_query_and_return_dict_with_distance(query_string):
        with connection.cursor() as cursor:
            cursor.execute(query_string)
            query_result = cursor.fetchall()

            row_dict = {}
            for row in query_result:
                row_dict[row[0]] = row[1]

            sorted_row_dict = sorted(row_dict.items(), key=lambda x: x[1])
            return sorted_row_dict
