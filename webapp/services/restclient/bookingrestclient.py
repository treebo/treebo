import json
import logging

import requests
from django.conf import settings
from django.core.urlresolvers import reverse
from apps.checkout.serializers import BookingRequestSerializer
from apps.common.exceptions.booking_exceptions import BookingInitiateException, DuplicateBookingCreationException,\
    BookingCreationException, BookingConfirmationException
from apps.common.utils import is_status_4xx
from django.conf import settings
from services.restclient.request_wrapper import wrap_internal_api_call
from services.restclient.supportmodels.bookingaux import Booking
from rest_framework.status import HTTP_409_CONFLICT

logger = logging.getLogger(__name__)


class BookingClient:
    @staticmethod
    def __get_booking_api(page_name, **kwargs):
        name = 'bookings_v1'
        return settings.TREEBO_WEB_URL + reverse(name + ':{0}'.format(page_name), kwargs=kwargs)

    @staticmethod
    def __make_call(page_name, data, method='post', **kwargs):
        url = BookingClient.__get_booking_api(page_name, **kwargs)
        logger.info('hitting confirm booking url %s , data %s',url,data)
        return requests.request(method, url, json=data, headers={'referer': "/"}, allow_redirects=True)

    def __init__(self):
        pass

    @staticmethod
    def __get_booking_data(booking_request_data):
        booking_data = {
            "checkin_date": booking_request_data["checkin_date"],
            "checkout_date": booking_request_data["checkout_date"],
            "adult_count": booking_request_data["adults"],
            "child_count": booking_request_data["children"],
            "room_config": booking_request_data["room_config"],
            "room_type": booking_request_data["room_type"],
            "hotel_id": booking_request_data["hotel_id"],
            "rack_rate": booking_request_data["rack_rate"],
            "mm_promo": booking_request_data["mm_promo"],
            "pretax_amount": booking_request_data["pretax_amount"],
            "tax_amount": booking_request_data["tax_amount"],
            "total_amount": booking_request_data["total_amount"],
            "guest_name": booking_request_data["name"],
            "guest_email": booking_request_data["email"],
            "guest_mobile": booking_request_data["phone"],
            "comments": booking_request_data["comments"],
            "coupon_apply": booking_request_data["coupon_apply"],
            "coupon_code": booking_request_data["coupon_code"],
            "payment_mode": booking_request_data["payment_mode"],
            "discount": booking_request_data["discount_value"],
            "channel": booking_request_data["booking_channel"],
            "user_id": booking_request_data["user"],
            "is_audit": booking_request_data["is_audit"],
            "organization_name": booking_request_data.get("organization_name", ''),
            "organization_taxcode": booking_request_data.get("organization_taxcode", ''),
            "organization_address": booking_request_data.get("organization_address", ''),
            "order_id": booking_request_data["order_id"],
            "payment_amount": booking_request_data["voucher_amount"],
            "voucher_amount": booking_request_data["voucher_amount"],
            "rate_plan": booking_request_data.get("rate_plan"),
            "rate_plan_meta": booking_request_data.get("rate_plan_meta"),
            "member_discount": booking_request_data["member_discount"],
            "wallet_applied": bool(booking_request_data.get("wallet_applied", False)),
            "wallet_deduction": booking_request_data.get("wallet_deduction", 0.0),
        }
        return booking_data

    @staticmethod
    def get_booking_by_orderid(order_id):
        response = BookingClient.__make_call(
            "booking_by_order_id", {}, method='get', orderId=order_id)
        if response.status_code != requests.codes.created:
            response.raise_for_status()
        booking = response.json()
        return booking

    @staticmethod
    def create_booking(booking_request, is_pay_at_hotel=False):
        """
        Creates a new booking in Hx
        :param booking_request:
        :return:
        """
        payload = BookingRequestSerializer(instance=booking_request).data
        payload['is_pay_at_hotel'] = is_pay_at_hotel
        booking_data = BookingClient.__get_booking_data(payload)
        logger.info("Payload for create_booking for bid: %s => %s",
                    booking_request.id, json.dumps(booking_data))
        response = BookingClient.__make_call("create-booking", booking_data)
        if not response.ok:
            if response.status_code == HTTP_409_CONFLICT:
                logger.error(
                    "Booking already processed: %s with status %s ",
                    response.content,
                    response.status_code)
                raise DuplicateBookingCreationException(
                    developer_message=json.dumps(response.json()))
            if is_status_4xx(response.status_code):
                logger.error(
                    "Error response in create_booking 4xx: %s with status %s ",
                    response.content,
                    response.status_code)
                raise BookingCreationException(
                    developer_message=json.dumps(response.json()))
            logger.error(
                "Error response in create_booking 5xx: %s with status %s",
                response.content,
                response.status_code)
            response.raise_for_status()
        booking = response.json()
        return booking

    @staticmethod
    def initiate_booking(booking_request):
        """
        Creates a temporary booking in Hx
        :param booking_request:
        :return:
        """
        payload = BookingRequestSerializer(instance=booking_request).data
        booking_data = BookingClient.__get_booking_data(payload)
        logger.info("Payload for init_booking for bid: %s => %s",
                    booking_request.id, json.dumps(booking_data))
        response = BookingClient.__make_call("init-booking", booking_data)
        if not response.ok:
            if is_status_4xx(response.status_code):
                logger.error(
                    "Error response in init_booking 4xx: %s with status code %s ",
                    response.content,
                    response.status_code)
                raise BookingInitiateException(
                    developer_message=json.dumps(response.json()))
            logger.error(
                "Error response in init_booking 5xx: %s with status code %s ",
                response.content,
                response.status_code)
            response.raise_for_status()
        booking = response.json()
        logger.info(
            "initiate_booking success with data %s with status code %s",
            json.dumps(booking),
            response.status_code)
        return booking

    @staticmethod
    def getBookingByOrderId(request, orderId):
        url = request.build_absolute_uri(
            reverse(
                'bookings_v1:booking_by_order_id',
                kwargs={
                    'orderId': str(orderId)}))
        logger.debug('Booking By Order ID URL: %s', url)
        response = wrap_internal_api_call(request, requests.get, url)
        # response = requests.get(url)
        logger.debug('Response status code: %s', response.status_code)
        if response.status_code == 200 or response.status_code == 304:
            bookingJson = response.json()
            return Booking(bookingJson)

    @staticmethod
    def getBookingsByUserEmail(request, email, userId):
        url = request.build_absolute_uri(
            reverse(
                'bookings_v1:get_bookings_by_users',
                kwargs={
                    'email': email,
                    'userId': userId}))
        response = wrap_internal_api_call(request, requests.get, url)
        # response = requests.get(url)
        bookingsJson = None
        if response.status_code == 200 or response.status_code == 304:
            bookingsJson = response.json()
        return bookingsJson

    @staticmethod
    def getRoomBookingByBookingId(request, bookingId):
        roomCount = roomId = None
        url = request.build_absolute_uri(
            reverse(
                'bookings_v1:get-room-bookings',
                kwargs={
                    'bookingId': bookingId}))
        response = wrap_internal_api_call(request, requests.get, url)
        # responsez = requests.get(urlx)
        if response.status_code == 200 or response.status_code == 304:
            jsonR = response.json()
            roomCount = jsonR['data']['roomBookingCount']
            roomId = jsonR['data']['roomBooking']['room_id']
        return roomCount, roomId

    @staticmethod
    def confirm_booking(order_id):
        response = BookingClient.__make_call(
            "confirm-booking", {"order_id": order_id})
        if not response.ok:
            if is_status_4xx(response.status_code):
                logger.error(
                    "Error response in confirm booking 4xx: %s with status code %s ",
                    response.content,
                    response.status_code)
                raise BookingConfirmationException(
                    developer_message=json.dumps(response.json()))
            logger.error(
                "Error response in confirm booking 5xx: %s with status code %s ",
                response.content,
                response.status_code)
            response.raise_for_status()
        booking = response.json()
        return booking

    @staticmethod
    def update_payment_details(
            payment_gateway_id,
            order_id,
            amount,
            payment_mode):
        response = BookingClient.__make_call("update-payment", {
            "order_id": order_id,
            "amount": amount,
            "payment_mode": payment_mode,
            "payment_gateway_id": payment_gateway_id
        })
        if response.status_code != requests.codes.ok:
            response.raise_for_status()
        return response.json()

    @staticmethod
    def cancel_booking(order_id, email):
        response = BookingClient.__make_call("cancel-booking", {
            "order_id": order_id,
            "email": email
        })
        if response.status_code != requests.codes.ok:
            response.raise_for_status()
        return response.json()

    @staticmethod
    def is_booking_confirmed(order_id):
        response = BookingClient.__make_call("check-status", {
            "order_id": order_id
        })
        if response.status_code != requests.codes.ok:
            response.raise_for_status()
        data = response.json()["data"]
        if "status" not in data:
            raise requests.HTTPError("No response received")
        return data["booking_status"]

    @staticmethod
    def getBookingById(request, bookingId):
        # get-bookings
        url = request.build_absolute_uri(
            reverse(
                'bookings_v1:get-bookings',
                kwargs={
                    'bookingId': str(bookingId)}))
        response = wrap_internal_api_call(request, requests.get, url)
        # response = requests.get(url)
        if response.status_code == 200 or response.status_code == 304:
            bJson = response.json()
            return Booking(bJson)

    @staticmethod
    def getLastBookedTime(request, hotelId, hours):
        # get-last-booking_time
        lastBookingTime = None
        url = request.build_absolute_uri(
            reverse(
                'bookings_v1:get-last-booking-time',
                kwargs={
                    'hotelId': str(hotelId),
                    'delta': str(hours)}))
        response = wrap_internal_api_call(request, requests.get, url)
        # response = requests.get(url)
        if response.status_code == 200 or response.status_code == 304:
            lastBookingTime = response.json()['data']['lastBookingTime']
        return lastBookingTime

    @staticmethod
    def getHotelReservations(
            request,
            checkInDateCopy,
            checkOutDateCopy,
            status,
            sumByStr,
            groupByStr):
        # get_reservations
        url = request.build_absolute_uri(
            reverse('bookings_v1:get-reservations'))
        payload = {
            'groupBy': groupByStr,
            'sumBy': sumByStr,
            'status': status,
            'checkInDate': checkInDateCopy,
            'checkOutDate': checkOutDateCopy}
        response = wrap_internal_api_call(request, requests.get, url)
        # responsez = requests.get(urlx, params=payload)
        jsonR = None
        if response.status_code == 200 or response.status_code == 304:
            jsonR = response.json()
        return jsonR

    # @staticmethod
    # def update_booking_source(order_id, source):
    #     response = BookingClient.__make_call("update-booking-source", {
    #         "order_id": order_id,
    #         "source": source,
    #     })
    #     if response.status_code != requests.codes.ok:
    #         response.raise_for_status()
    #     return response.json()