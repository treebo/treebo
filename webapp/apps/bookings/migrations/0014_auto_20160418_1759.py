# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0013_bookingrequest_booking_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='confirmbooking_job_id',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='savebooking_job_id',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
