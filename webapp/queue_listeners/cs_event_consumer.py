import logging
from kombu.mixins import ConsumerMixin
from webapp.base.benchmark_decorator import timeit

from psycopg2 import InterfaceError
from base.db_decorator import db_retry_decorator
from dbcommon.data_services import models_updater
from apps.common.slack_alert import SlackAlertService

logger = logging.getLogger(__name__)


# TODO(For Veena): Add UTs
class CSEventConsumer(ConsumerMixin):

    def __init__(self, connection, queues, message_processing_fn):
        self.connection = connection
        self.queues = queues
        self.message_processing_fn = message_processing_fn

    def get_consumers(self, Consumer, channel):
        return [Consumer(queues=self.queues,
                         callbacks=[self.on_message])]

    def on_message(self, body, message):
        try:
            logger.info("Got message from CS from exchange %s, for queue %s, routing key %s",
                        message.delivery_info['exchange'],
                        self.queues.name,
                        message.delivery_info['routing_key'])
            self.process_message_from_cs(body, message)
            message.ack()
        except Exception as e:
            logger.exception(e)

    @db_retry_decorator()
    @timeit(logger=logger)
    def process_message_from_cs(self, body, message):
        try:
            logger.info("Received message %s" % body)
            cs_msg = models_updater.store_dump_of_incoming_message(body['cs_property_id'], body,
                                                                   message.delivery_info['routing_key'],
                                                                   message.delivery_info['exchange'], self.queues.name)
            is_message_processed, result_msg = self.message_processing_fn(body['data'], body['cs_property_id'])
            self.post_process_message(cs_msg, is_message_processed, result_msg)
        except InterfaceError as e:
            logger.error("Encountered %s" % e.__str__())
            raise e
        except Exception as e:
            logger.error("Error % in processing cs message" % e.__str__())
            SlackAlertService.send_cs_error_msg_to_slack(error_msg="Error %s in parsing cs message"
                                                                   % e.__str__(),
                                                         class_name=self.__class__.__name__)

    def post_process_message(self, cs_msg, is_message_processed, result_msg):
        if cs_msg:
            cs_msg.is_consumed = is_message_processed
            cs_msg.failure_message = result_msg[0:190]
            cs_msg.save()
            if not is_message_processed:
                SlackAlertService.send_cs_error_msg_to_slack(cs_msg.cs_id, result_msg,
                                                             class_name=self.__class__.__name__)
