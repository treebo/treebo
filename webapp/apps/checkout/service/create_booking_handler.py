import json
import logging
from concurrent.futures import ThreadPoolExecutor

from apps.bookings import constants
from apps.bookings.serializers import BookingSerializer
from apps.bookings.services.booking import BookingService
from apps.checkout.serializers import BookingRequestSerializer
from apps.checkout.service.booking_request_details import BookingRequestParser
from apps.checkout.service.trivago_conversion_tracking import TrivagoConversionTracking
from apps.common.exceptions.booking_exceptions import DuplicateBookingCreationException, BookingCreationException
from apps.checkout.service.checkout_handler import CheckoutHandler
from data_services.exceptions import HotelDoesNotExist
from data_services.respositories_factory import RepositoriesFactory
from apps.payments.service.payment_service_v2 import PaymentServiceV2
from apps.reward.service.reward import RewardService

logger = logging.getLogger(__name__)
hotel_repository = RepositoriesFactory.get_hotel_repository()
from apps.bookings.services.axis_rooms_booking_service import AxisRoomsBookingService
from django.conf import settings
from apps.bookings.constants import HOMESTAY_PROVIDER_WT, HOMESTAY_PROPERTY_TYPE
from common.services.feature_toggle_api import FeatureToggleAPI
from base.middlewares.set_request_id import get_current_user, get_current_request_id


class CreateBooking:

    @staticmethod
    def get_booking_service(hotel):

        logger.info("Booking initiate api called for hotel %s and property type %s", hotel.name, hotel.property_type)
        if hotel.provider_name == HOMESTAY_PROVIDER_WT:
            return AxisRoomsBookingService()
        else:
            return BookingService()

    @staticmethod
    def internal_create_booking(booking_request, is_pay_at_hotel=False, user=None):
        payload = BookingRequestSerializer(instance=booking_request).data
        payload['is_pay_at_hotel'] = is_pay_at_hotel
        booking_data = BookingRequestParser.booking_request_handler(payload)
        logger.info("Payload for create_booking for bid: %s => %s",
                    booking_request.id, json.dumps(booking_data))

        hotel_id = booking_data.get('hotel_id')
        room_type = booking_data.get("room_type")
        hotels = hotel_repository.filter_hotels(id=int(hotel_id))
        hotel = hotels[0] if hotels else None
        if not hotel:
            raise HotelDoesNotExist
        rooms = hotel_repository.get_rooms_for_hotel(hotel)
        room = next(
            (room for room in rooms if room.room_type_code == room_type),
            None)

        booking_data["hotel"] = hotel_id
        booking_data["room"] = room.id if room else None

        booking_service = BookingService()

        logger.info("Booking request for booking request id {}: {}".format(
            str(get_current_request_id()) if get_current_request_id() else None,
            str(booking_request) if booking_request else None))
        booking = booking_service.create_booking(booking_data, booking_request)
        output_serializer = BookingSerializer(instance=booking)
        trivago_conversion_tracking = TrivagoConversionTracking(booking=booking, booking_request=booking_request)
        logger.info("Trivago Conversion Tracking required: %s for order_id %s and utm_source %s",
                    trivago_conversion_tracking.is_trivago_ack_required(),
                    str(booking.order_id), str(booking_request.utm_source))
        if trivago_conversion_tracking.is_trivago_ack_required():
            with ThreadPoolExecutor() as executor:
                executor.submit(trivago_conversion_tracking.send_booking_confirmation_ack)

        wallet_payment_success = False
        wallet_sufficient_funds = True
        if booking_request.wallet_applied:
            _, _, _, wallet_spendable_balance, _, _, debit_mode, _ = \
                RewardService().get_wallet_balance(user, booking_request.booking_channel)

            if float(round( booking_request.wallet_deduction, 2)) > wallet_spendable_balance:
                logger.error("Insufficient funds in wallet, wallet has %s, deduction is %s",
                             str(wallet_spendable_balance), str(booking_request.wallet_deduction))
                wallet_sufficient_funds = False

            if wallet_sufficient_funds:
                wallet_payment_success, payment_order, reason = PaymentServiceV2().one_step_pay(
                    user, amount=booking_request.wallet_deduction,
                    booking_order_id=booking_request.order_id,
                    particulars="Points used for booking",
                    debit_mode=debit_mode)

                logger.info(
                    "handle_wallet_payment for pay@hotel for bid %s done for user %s and  "
                    "wallet_payment_success %s",
                    booking_request.id,
                    user,
                    wallet_payment_success)

                if wallet_payment_success:
                    booking_service.confirm_booking(booking)

        booking_status = CheckoutHandler().get_booking_status(
            booking_request, user, wallet_payment_success)

        booking_request.status = booking_status
        booking_request.save()

        return output_serializer.data
