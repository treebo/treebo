import json
import logging

from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from apps.apiv1.apihelper import Helper
from apps.common import utils
from apps.common.utils import AnalyticsEventTracker
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class JoinSave(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(JoinSave, self).dispatch(*args, **kwargs)

    def get(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        try:
            join_us_json = json.dumps(request.data)
            join_us_data = json.loads(join_us_json)
            if "csrfmiddlewaretoken" in join_us_data:
                del join_us_data["csrfmiddlewaretoken"]
            email_content = Helper.getEmailContent(join_us_data)

            status = self.__email(
                settings.JOINUS_SUBJECT,
                email_content,
                settings.SERVER_EMAIL,
                settings.JOINUS_EMAIL_LIST)
            if "success" == status:
                utils.trackAnalyticsServerEvent(
                    request,
                    'Join Us Data Submitted',
                    join_us_data,
                    channel=AnalyticsEventTracker.WEBSITE)
                return Response(
                    {"status": "success", "msg": "Successfully Submitted"})
            else:
                return Response(
                    {"status": "error", "msg": "Something went wrong"}, status=404)

        except Exception as exc:
            logger.exception(exc)
            data = {"status": "error", "msg": "Something went wrong"}
            return Response(data, status=404)

    def __email(self, subject, emailContent, fromEmail, toMailList):
        try:
            msg = EmailMessage(subject, emailContent, fromEmail, toMailList)
            msg.content_subtype = "html"
            msg.send()

            logger.info("Successfully email %s" % subject)
            return "success"

        except Exception as exc:
            logger.exception(
                "Error in sending %s Email with exception %s" %
                (subject, exc))
            return "failure"
