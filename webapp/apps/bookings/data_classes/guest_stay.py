# -*- coding: utf-8 -*-

from __future__ import division

import datetime
import uuid

from django.core.exceptions import ValidationError

from apps.common.data_classes.base_data_class import BaseDataClass
from apps.common.data_classes.container import Container
from apps.common.date_time_utils import is_naive_datetime, LocalTimeZoneObject
from apps.bookings.data_classes.status import Status, status_in_active_statuses, InitiatedStatus


class GuestStay(BaseDataClass):

    def __init__(self, uid, guest, checkin, checkout):
        self.uid = str(uid)
        self._generated_uid = False
        if not self.uid:
            self.uid = uuid.uuid4()
            self._generated_uid = True

        self.guest = guest

        assert isinstance(checkin, datetime.datetime)
        assert isinstance(checkout, datetime.datetime)

        if is_naive_datetime(checkin):
            raise ValueError('Need time zone aware checkin')
        if is_naive_datetime(checkout):
            raise ValueError('Need time zone aware checkout')

        self.checkin = checkin
        self.checkout = checkout

        if self.checkin >= self.checkout:
            raise ValidationError("checkin cannot be greater than or equal to checkout for {gs}".format(gs=self))

        self._status = InitiatedStatus

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        assert isinstance(status, Status)
        self._status = status

    @property
    def is_active(self):
        return status_in_active_statuses(self.status)

    @property
    def guest_id(self):
        return self.guest.uid

    def __eq__(self, other):
        if isinstance(other, GuestStay):
            return hash(self) == hash(other)
        return NotImplemented

    def __hash__(self):
        # do not include guest here. Guest details are different from guest stay
        return hash(tuple([
            self.uid,
            self.checkin.astimezone(LocalTimeZoneObject).isoformat(),
            self.checkout.astimezone(LocalTimeZoneObject).isoformat(),
        ]))

    def __repr__(self):
        return '<{kls} ({g} {ci} -> {co})>'.format(
            kls=self.__class__.__name__,
            g=self.guest,
            ci=self.checkin,
            co=self.checkout,
        )

    def __str__(self):
        return '{g}'.format(g=self.guest)


class GuestStays(Container):
    def __sub__(self, other):
        ret = super(GuestStays, self).__sub__(other)
        if ret is not NotImplemented:
            return [item for item in ret if item.is_active]
        return NotImplemented
