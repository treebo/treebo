from django.db import models
from djutil.models import TimeStampedModel
from django.contrib.postgres.fields import ArrayField

from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.models.hotel import Hotel


class TAHotelIDMapping(TimeStampedModel):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    ta_location_id = models.CharField(max_length=200)
    write_review_url = models.TextField(null=True, blank=True)
    enable_iframe_widget = models.BooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'trip_advisor_hotel_mapping'
        verbose_name = 'TA Hotel ID Mapping'
        verbose_name_plural = 'TA Hotel ID Mapping'

    def __unicode__(self):
        return '{0}{1}{2}'.format(
            self.hotel.id,
            " ",
            self.hotel.name,
            "  ",
            self.ta_location_id)

    def __str__(self):
        return '{0}{1}{2}'.format(
            self.hotel.id,
            " ",
            self.hotel.name,
            "  ",
            self.ta_location_id)


class HotelTAOverallRatings(TimeStampedModel):
    hotel_ta_mapping = models.ForeignKey(
        TAHotelIDMapping, on_delete=models.CASCADE)
    overall_rating = models.DecimalField(
        default=0.0, decimal_places=1, max_digits=20)
    url_overall_rating_image = models.TextField()
    write_review_url = models.TextField()
    read_review_url = models.TextField()
    overall_ranking = models.PositiveIntegerField(default=0)
    overall_ranking_out_of = models.PositiveIntegerField(default=0)
    overall_ranking_string = models.TextField(null=True, default=True)
    geo_location_name = models.CharField(max_length=200, null=True, blank=True)
    num_reviews_count = models.PositiveIntegerField(default=0)

    class Meta(DefaultPermissions.Meta):
        db_table = 'trip_advisor_hotel_overall_ratings'
        verbose_name = 'TA Hotel Overall Ratings'
        verbose_name_plural = 'TA Hotel Overall Ratings'

    def __unicode__(self):
        return '{0}{1}{2}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            " ",
            self.overall_rating)

    def __str__(self):
        return '{0}{1}{2}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            " ",
            self.overall_rating)


class TASubrating(TimeStampedModel):
    subrating_name = models.TextField(null=True, blank=True, unique=True)
    subrating_localized_name = models.TextField(null=True, blank=True)
    is_enabled = models.BooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'trip_advisor_subratings'
        verbose_name = 'TA Subratings'
        verbose_name_plural = 'TA Subratings'

    def __unicode__(self):
        return '{0}{1}{2}'.format(
            self.subrating_name,
            " ",
            self.subrating_localized_name,
            " ",
            self.is_enabled)

    def __str__(self):
        return '{0}{1}{2}'.format(
            self.subrating_name,
            " ",
            self.subrating_localized_name,
            " ",
            self.is_enabled)
class TAHotelSubratingMapping(TimeStampedModel):
    hotel_ta_mapping = models.ForeignKey(
        TAHotelIDMapping, on_delete=models.CASCADE)
    subrating = models.ForeignKey(TASubrating, on_delete=models.CASCADE)
    subrating_value = models.DecimalField(
        default=0.0, max_digits=20, decimal_places=1)
    url_subrating = models.TextField()

    class Meta(DefaultPermissions.Meta):
        unique_together = (('hotel_ta_mapping', 'subrating'),)
        db_table = 'trip_advisor_hotel_subrating_mapping'
        verbose_name = 'TA Hotel Sub Rating Mapping'
        verbose_name_plural = 'TA Hotel Sub Rating Mapping'

    def __unicode__(self):
        return '{0}{1}{2}{3}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            " ",
            self.subrating.subrating_name,
            " ",
            self.subrating_value)

    def __str__(self):
        return '{0}{1}{2}{3}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            " ",
            self.subrating.subrating_name,
            " ",
            self.subrating_value)

class TAReviewRatingCountMapping(TimeStampedModel):
    ONE = '1'
    TWO = '2'
    THREE = '3'
    FOUR = '4'
    FIVE = '5'

    review_rating_choices = (
        (ONE, '1'),
        (TWO, '2'),
        (THREE, '3'),
        (FOUR, '4'),
        (FIVE, '5'),
    )

    hotel_ta_mapping = models.ForeignKey(
        TAHotelIDMapping, on_delete=models.CASCADE)
    review_rating = models.CharField(
        max_length=100,
        default=FIVE,
        choices=review_rating_choices)
    count_of_customers = models.PositiveIntegerField(default=0)
    percentage_of_customers = models.DecimalField(
        default=0.0, max_digits=20, decimal_places=0)

    class Meta(DefaultPermissions.Meta):
        db_table = 'trip_advisor_review_rating_count'
        verbose_name = 'TA Review Rating Count'
        verbose_name_plural = 'TA Review Rating Count'

    def __unicode__(self):
        return '{0}{1}{2}{3}{4}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            " ",
            self.review_rating,
            "",
            self.count_of_customers,
            " ",
            self.percentage_of_customers)

    def __str__(self):
        return '{0}{1}{2}{3}{4}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            " ",
            self.review_rating,
            "",
            self.count_of_customers,
            " ",
            self.percentage_of_customers)

class TAAwardslist(TimeStampedModel):
    award_name = models.TextField(unique=True)
    award_category = models.TextField(null=True, blank=True)
    is_enabled = models.BooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'trip_advisor_awards'
        verbose_name = 'TA Awards'
        verbose_name_plural = 'TA Awards'

    def __unicode__(self):
        return '{0}{1}'.format(self.award_name, self.is_enabled)

    def __str__(self):
        return '{0}{1}'.format(self.award_name, self.is_enabled)

class TAAwardsHotelMapping(TimeStampedModel):
    award = models.ForeignKey(TAAwardslist, on_delete=models.CASCADE)
    hotel_ta_mapping = models.ForeignKey(
        TAHotelIDMapping, on_delete=models.CASCADE)
    award_image_url = models.TextField(null=True, blank=True)
    award_year = models.CharField(null=True, blank=True, max_length=20)

    class Meta(DefaultPermissions.Meta):
        db_table = 'trip_advisor_awards_hotel_mapping'
        verbose_name = 'TA Awards Hotel Mapping'
        verbose_name_plural = 'TA Awards Hotel Mapping'

    def __unicode__(self):
        return '{0}{1}{2}{3}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            " ",
            self.award.award_name,
            " ",
            self.award_year)

    def __str__(self):
        return '{0}{1}{2}{3}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            " ",
            self.award.award_name,
            " ",
            self.award_year)


class TAHotelReviews(TimeStampedModel):
    hotel_ta_mapping = models.ForeignKey(
        TAHotelIDMapping, on_delete=models.CASCADE)
    ta_review_id = models.CharField(max_length=20)
    review_text = models.TextField(null=True, blank=True)
    review_title = models.TextField(null=True, blank=True)
    review_published_date = models.DateTimeField(null=True, blank=True)
    helpful_votes = models.CharField(max_length=20, null=True, blank=True)
    user_rating = models.DecimalField(
        default=0.0, decimal_places=1, max_digits=20)
    user_rating_image_url = models.TextField(null=True, blank=True)
    user_rating_text_url = models.TextField(null=True, blank=True)
    user_name = models.CharField(max_length=200, null=True, blank=True)
    user_image_url = models.TextField(null=True, blank=True)
    owner_rating_reply_text = models.TextField(null=True, blank=True)
    owner_description = models.TextField(null=True, blank=True)
    owner_reply_published_date = models.DateTimeField(null=True, blank=True)
    review_is_active = models.BooleanField(default=True)
    review_is_user_generated = models.BooleanField(default=False)
    review_is_crawlable = models.BooleanField(default=False)

    class Meta(DefaultPermissions.Meta):
        db_table = 'trip_advisor_top_reviews'
        verbose_name = 'TA Top Reviews'
        verbose_name_plural = 'TA Top Reviews'

    def __unicode__(self):
        return '{0}{1}{2}{3}{4}{5}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            ":",
            self.review_title,
            " ",
            self.user_name,
            " ",
            self.user_rating,
            " ",
            self.review_is_active)

    def __str__(self):
        return '{0}{1}{2}{3}{4}{5}'.format(
            self.hotel_ta_mapping.hotel.name,
            " ",
            self.hotel_ta_mapping.ta_location_id,
            ":",
            self.review_title,
            " ",
            self.user_name,
            " ",
            self.user_rating,
            " ",
            self.review_is_active)


class ReviewAppConfiguration(TimeStampedModel):
    enable_review_app = models.BooleanField(default=False)
    enable_for_all_hotels = models.BooleanField(default=False)
    disable_for_hotel_list = models.TextField(
        null=True, blank=True, default='')
    enable_for_hotel_list = models.TextField(null=True, blank=True, default='')
    is_audit_enable = models.BooleanField(default=False)
    percent_for_ranking_visibility = models.PositiveIntegerField(default=15)
    num_subratings_to_show = models.PositiveIntegerField(default=4)
    num_non_user_reviews_to_show = models.PositiveIntegerField(default=10)
    num_user_reviews_to_show = models.PositiveIntegerField(default=10)
    award_description = models.TextField(
        default="TripAdvisor Certificate of Excellence - Awarded to properties that consistently earn great reviews")
    disable_for_category_list = ArrayField(models.CharField(max_length=50, blank=True), size=20, default=[], blank=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'trip_advisor_app_toggle'
        verbose_name = 'TA App Toggle'
        verbose_name_plural = 'TA App Toggle'


class TAAPIAuditTable(TimeStampedModel):
    SUCCESS = 'SUCCESS'
    FAILURE = 'FAILURE'
    api_response_choices = ((SUCCESS, 'SUCCESS'),
                            (FAILURE, 'FAILURE'),
                            )
    hotel_ta_mapping = models.ForeignKey(
        TAHotelIDMapping, on_delete=models.CASCADE)
    location_api_url = models.TextField(null=True, blank=True, default='')
    location_api_url_response_json = models.TextField(
        null=True, blank=True, default='')
    location_api_url_response_status = models.CharField(
        max_length=100, choices=api_response_choices, default=SUCCESS)
    location_reviews_api_url = models.TextField(
        null=True, blank=True, default='')
    location_reviews_api_url_response_json = models.TextField(
        null=True, blank=True, default='')
    location_reviews_api_url_response_status = models.CharField(
        max_length=100, choices=api_response_choices, default=SUCCESS)
    user_location_reviews_api_url = models.TextField(
        null=True, blank=True, default='')
    user_location_reviews_api_url_response_json = models.TextField(
        null=True, blank=True, default='')
    user_location_reviews_api_url_response_status = models.CharField(
        max_length=100, choices=api_response_choices, default=SUCCESS)

    class Meta(DefaultPermissions.Meta):
        db_table = 'trip_advisor_api_audit'
        verbose_name = 'TA API Audit'
        verbose_name_plural = 'TA API Audit'
