import logging
import json

from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.authentication import BasicAuthentication
from apps.common.exceptions.booking_exceptions import BookingNotFoundException
from apps.common.exceptions.custom_exception import PaymentOrderNotFound

from apps.auth.csrf_exempt_session_authentication import \
    CsrfExemptSessionAuthentication
from apps.bookings.models import PaymentOrder, Booking
from apps.checkout.service.itinerary_handler import get_itinerary_data_from_booking_id

from apps.checkout.service.constant import get_payment_status, FinalBookingStatus
from base.views.api import TreeboAPIView
from apps.checkout.serializers import json_dateserialiser
from services.restclient.payment_client_v2 import PaymentClientV2
from django.conf import settings

logger = logging.getLogger(__name__)


class PaymentStatusAPI(TreeboAPIView):
    authentication_classes = (
        BasicAuthentication, CsrfExemptSessionAuthentication)
    payment_client = PaymentClientV2()

    def get(self, request, *args, **kwargs):
        order_id = kwargs["order_id"]
        count_of_status_call_from_ui = int(request.query_params["count"])

        ''' UI declares the request as timeout if the number of /status api calls is greater than 10
            so we are using this variable keep track when we need to pull the payment status from
            gateway instead of waiting for the gateway to send the server to server callback 
        '''
        logger.info('Recieved get payment status/booking status request for '
                    'order id:{}'.format(kwargs["order_id"]))
        if not order_id:
            return Response('Order Id is missing', status=HTTP_400_BAD_REQUEST)
        return self.get_confirmation_status(order_id, count_of_status_call_from_ui)

    def post(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    def get_confirmation_status(self, order_id, count_of_status_call_from_ui):
        try:
            logger.info("Entered function to generate the confirmation status from the "
                        "payment status for order id:{}".format(order_id))
            payment_orders = PaymentOrder.objects.filter(booking_order_id=order_id)
            if len(payment_orders) == 0:
                raise PaymentOrderNotFound("No payment orders found")
            payment_status = self.get_payment_status(payment_orders, count_of_status_call_from_ui)
            logger.info("Payment status from multiple Payment Order record for "
                        "order id:{} is:{}".format(order_id, payment_status))
            response_data = dict(status=payment_status)
            if payment_status == FinalBookingStatus.FAILURE:
                try:
                    booking = Booking.objects.get(order_id=order_id)
                except Exception as e:
                    logger.exception('Booking not found for order_id %s', order_id)
                    raise BookingNotFoundException(e.message)
                itinerary_data = get_itinerary_data_from_booking_id(booking.id)
                response_data.update(itinerary_data)
                try:
                    logger.info("Payment Failed hence sending the response with itinerary data. "
                                "the response being sent is:{}".format(json.dumps(response_data,
                                                                                  default=json_dateserialiser)))
                except Exception as e:
                    logger.exception("Exception occured while json deserializing the itinerary data for logging")
            return Response(data=response_data)

        except Exception as e:
            logger.exception('Exception occured for order_id %s', order_id)
            return Response(e.message, status=HTTP_400_BAD_REQUEST)

    def get_payment_status(self, payment_orders, count_of_status_call_from_ui):
        '''
        Current change, same order id can have multiple payments from various gateways,
        1) Hence if either of the gateways are in captured status this method should return success.
        2) In case of payments where rewards are used too, in that case we dont check the status
        for that since even if the reward fails we still confirm the booking
        3) In case of only rezorpay this api will not be called.
        4) Pull payment status from gateway will not happen in case of reazorpay since razorpay
        callbacks are not happening from backend.
        5) In case some of the payments records are in failure and a some in ISSUED we return
        issued since the customer would have retried payments after failre from one gateway.
        6) In case all the records except treebo reward and razorpay are in FAILED state we return FAILURE

        :param payment_orders: same order id can have multiple records in payment order table other than treebo rewards
        :param count_of_status_call_from_ui: this is the number of times ui has already called this api and the s2s callback is still not arrived
        :return:
        '''
        is_pending = False
        for order_object in payment_orders:
            if order_object.gateway_type != 'treebo_wallet' and order_object.gateway_type != 'razorpay':
                if order_object.status == PaymentOrder.CAPTURED:
                    return get_payment_status(PaymentOrder.CAPTURED)
                if order_object.status == PaymentOrder.ISSUED:
                    if count_of_status_call_from_ui >= settings.MAX_WAIT_COUNT_FOR_S2S_CALLBACK:
                        logger.info("Payment Transaction Exceeded timeout for gateway:{} and "
                                    "order id:{}".format(order_object.gateway_type,
                                                         order_object.booking_order_id))
                        self.payment_client.get_and_update_payment_status_from_gateway(
                            order_object.ps_order_id, order_object.ps_payment_id)
                    is_pending = True
        if is_pending:
            return get_payment_status(PaymentOrder.ISSUED)
        return get_payment_status(PaymentOrder.FAILED)
