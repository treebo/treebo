import json
import logging
from datetime import datetime, timedelta

from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.utils import dateformat

import common.constants.common_constants as const
from apps.featuregate.manager import FeatureManager
from apps.featuregate.models import Page
from apps.hotels import utils as hotel_utils
from apps.hotels.helpers import CityHotelHelper
from apps.hotels.service.hotel_service import HotelEnquiryValidator
from base.views.template import TreeboTemplateView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboValidationException
from apps.common import date_time_utils
from dbcommon.models.facilities import Facility
from services import maps
from services.restclient.contentservicerestclient import ContentServiceClient
from services.restclient.searchclient import SearchClient

logger = logging.getLogger(__name__)


class Search(TreeboTemplateView):
    template_name = "searchresults/index.html"

    hotel_detail_suffix = ''
    default_search_url = ''
    room_config_str = ''
    hotel_page = 'searchhotel'

    def __init__(self, **kwargs):
        super(Search, self).__init__(**kwargs)
        self.__myMap = maps.Maps()
        Search.__myMap = self.__myMap

    def getData(self, request, args, kwargs):
        context = self.__getSearchData(request, args, kwargs)
        context['styles'] = ['desktop/css/searchresults.css']
        context['scripts'] = ['desktop/js/searchresults.js']
        context['googleMaps'] = True
        return context

    def getMobileData(self, request, args, kwargs):
        context = self.__getSearchData(request, args, kwargs)
        context['styles'] = ['mobile/css/searchresults.css']
        context['scripts'] = ['mobile/js/searchresults.js']
        return context

    def __getSearchData(self, request, args, kwargs):
        result = []
        nearbyResult = []
        search_str, check_in_date, check_out_date, rooms_config, latitude, longitude, locality, location_type = self.__parse_request(
            request)
        room_config_string = request.GET.get('roomconfig', '1-0')
        rooms_required = len(rooms_config)
        guest = 0
        for single_config in rooms_config:
            guest = guest + single_config[0]

        try:
            HotelEnquiryValidator(
                check_in_date,
                check_out_date,
                rooms_config).validate()
        except TreeboValidationException as e:
            logger.info("Invalid search request received: %s", str(e))
            if e.code == error_codes.INVALID_DATES:
                return HttpResponseRedirect(self.default_search_url)
            raise e

        search_results = SearchClient.get_search_results(
            request,
            search_str,
            check_in_date,
            check_out_date,
            room_config_string,
            latitude,
            longitude,
            locality,
            location_type)
        hotels = search_results['hotels']
        page = Page.objects.filter(page_name=self.hotel_page).first()

        for hotel in hotels:
            kwargs = hotel_utils.get_hotel_url_kwargs_from_dict(hotel)
            hotel['hotelUrl'] = reverse(
                'pages:hotel-details-new',
                kwargs=kwargs) + self.hotel_detail_suffix + '&roomtype=' + hotel['cheapest_room_type']
            if page:
                feature_manager = FeatureManager(
                    page, check_in_date, check_out_date, hotel.id, '', '', None)
                overridden_values = feature_manager.get_overriding_values()
            else:
                overridden_values = {}
            for overridden_key in list(overridden_values.keys()):
                hotel[overridden_key] = overridden_values[overridden_key]

        facilities = list(Facility.objects.values_list('name', flat=True))
        is_nearby = False

        context = self.__generateContext(
            check_in_date,
            check_out_date,
            guest,
            latitude,
            locality,
            longitude,
            hotels,
            nearbyResult,
            rooms_required,
            search_str,
            search_results['allLocalityList'],
            facilities,
            is_nearby,
            search_results['parentQuery'],
            search_results['isLocalitySearch'],
            search_results['cityOrStateOrHotelNotSearchedExplicitly'],
            search_results['localityName'])

        context['jsvars'] = str(json.dumps(
            {
                'locality': search_results['localityName'],
                'city': search_results['cityName'],
            }))
        return context

    # TODO: Refactor this method. Too many parameters
    def __generateContext(
            self,
            check_in_date,
            check_out_date,
            guest,
            latitude,
            locality,
            longitude,
            result,
            nearbyResult,
            rooms_required,
            search_str,
            localities,
            facilities,
            is_nearby,
            parent_query,
            is_locality_search,
            is_not_city_state_hotel_search,
            locality_name):
        sorted_city_details = CityHotelHelper.getCityHotels()
        sort_array = ['price', 'distance', 'recommend']
        sort_options = {
            'price': {'value': 'Price', 'selected': False},
            'recommend': {'value': 'Recommendation', 'selected': False},
            'distance': {'value': 'Distance', 'selected': True}
        }
        if is_not_city_state_hotel_search is False and is_locality_search is False:
            sort_array = ['price', 'recommend']
            sort_options = {
                'price': {'value': 'Price', 'selected': True},
                'recommend': {'value': 'Recommendation', 'selected': False}
            }

        filter_data = {
            'sortddVisibility': False,
            'filterddVisibility': False,
            'sortArr': sort_array,
            'sortOptions': sort_options,
            'filterArr': ['budget', 'locality', 'amenity', 'category'],
            'filters': {
                'budget': [
                    {'selected': False, 'key': [0, 1500], 'value': '0-1500'},
                    {'selected': False, 'key': [1501, 3000], 'value': '1500-3000'},
                    {'selected': False, 'key': [3001, 300000], 'value': 'above 3000'}
                ],
                'locality': localities,
                'amenity': facilities,
                'category': []
            },
            'showOnlyAvailable': False
        }

        bread_crumb_list = [{'title': 'home', 'url': '/'}]
        if parent_query:
            for single_query in parent_query:
                lat = str(single_query['lat'])
                long = str(single_query['long'])
                name = str(single_query['name'])

                bread_crumb_list.append({
                    'title': single_query['name'],
                    'url': "{0}?locality={1}&lat={2}&long={3}&query={4}&checkin={5}&checkout={6}&roomconfig={7}".format(
                        reverse('pages:search-hotels'), '', lat, int, name,
                        datetime.now().strftime(date_time_utils.DATE_FORMAT),
                        (datetime.now() + timedelta(days=1)).strftime(date_time_utils.DATE_FORMAT),
                        self.room_config_str
                    )
                })

        marketing_banner = ContentServiceClient.getValueForKey(
            self.request, 'search_marketing_flat_banner', 1)
        if marketing_banner is not None:
            marketing_banner['position'] = (len(result) / 2 + 1)

        no_of_nights = (check_out_date - check_in_date).days
        context = {
            'cities': sorted_city_details,
            'isLocalitySearched': is_locality_search,
            'nearbyResults': nearbyResult,
            'isNearby': is_nearby,
            'results': result,
            'breadCrumbs': bread_crumb_list,
            'scripts': ['js/search.js'],
            'marketBanner': marketing_banner,
            'filterData': json.dumps(filter_data),
            'queryParams': {
                # Using django.utils.dateformat module, to format date as: 1st
                # May'16 (Adding st, nd is something that python
                # datetime.strftime can't do
                'checkIn': dateformat.format(check_in_date, "jS M'y"),
                'checkOut': dateformat.format(check_out_date, "jS M'y"),
                'no_of_nights': no_of_nights,
                'guests': guest,
                'rooms': rooms_required,
                'search': search_str,
                'latitude': latitude,
                'longitude': longitude,
                'locality': locality
            }
        }

        if is_locality_search:
            context['localityName'] = locality_name

        return context

    def __parse_request(self, request):
        """
        Extract the query information from the request
        :rtype : tuple
        :param request:
        :return:
        """
        d = request.GET
        query = d.get('query', '')
        location_type = d.get('locationtype')
        query = str(str(query).encode('ascii', 'ignore'))
        try:
            check_in_date = datetime.strptime(
                d.get('checkin', ''), date_time_utils.DATE_FORMAT)
        except ValueError:
            logger.info(
                "Invalid date format received for checkIn: %s",
                d.get(
                    'checkin',
                    "None"))
            check_in_date = datetime.now()
            check_in_date = check_in_date.replace(
                hour=0, minute=0, second=0, microsecond=0)
        try:
            check_out_date = datetime.strptime(
                d.get('checkout', ''), date_time_utils.DATE_FORMAT)
        except ValueError:
            logger.info(
                "Invalid date format received for checkOut: %s",
                d.get(
                    'checkout',
                    "None"))
            check_out_date = check_in_date + timedelta(days=1)
            check_out_date = check_out_date.replace(
                hour=0, minute=0, second=0, microsecond=0)

        room_config = []
        room_config_str = '1-0'
        try:
            room_config_str = d.get('roomconfig', '1-0')
            room_wise_configs = room_config_str.split(',')
            logger.debug("Room wise configs: %s", room_wise_configs)

            for room in room_wise_configs:
                room_tuple = room.split('-')
                adults = int(room_tuple[0])
                if len(room_tuple) > 1:
                    children = int(room_tuple[1])
                else:
                    children = 0
                room_config.append((adults, children))
        except ValueError:
            logger.info("Invalid roomConfig Format")
            room_config = [(1, 0)]

        latitude = d.get('lat') if 'lat' in d and d.get(
            'lat') != "" else const.BANGALORE_LAT
        longitude = d.get('long') if 'long' in d and d.get(
            'long') != "" else const.BANGALORE_LONG
        locality = d.get('locality') if 'locality' in d and d.get(
            'locality') != "" else const.DEFAULT_LOCALITY

        self.hotel_detail_suffix = "?checkin={0}&checkout={1}&roomconfig={2}".format(
            check_in_date.strftime(
                date_time_utils.DATE_FORMAT), check_out_date.strftime(
                date_time_utils.DATE_FORMAT), str(room_config_str))
        self.default_search_url = "{0}?locality={1}&lat={2}&long={3}&query={4}&checkin={5}&checkout={6}&roomconfig={7}".format(
            reverse('pages:search-hotels'),
            locality,
            latitude,
            longitude,
            query,
            datetime.now().strftime(
                date_time_utils.DATE_FORMAT),
            (datetime.now() +
             timedelta(
                days=1)).strftime(
                date_time_utils.DATE_FORMAT),
            room_config_str)
        self.room_config_str = room_config_str

        return query, check_in_date, check_out_date, room_config, latitude, longitude, locality, location_type
