import hashlib
import logging
import urllib.request
import urllib.error
import urllib.parse
import binascii
from django.conf import settings
from django.db.models import Q

from apps.reviews.models import TAHotelIDMapping

logger = logging.getLogger(__name__)


class GenerateJCRURL(object):
    def create_url(self, user_details):
        url = None
        try:
            ta_hotel_map = TAHotelIDMapping.objects.get(Q(hotel__id=int(
                user_details['hotel_id'])) | Q(hotel__hotelogix_id=int(user_details['hotel_id'])))
            if user_details['email'] is not None:
                logger.info("User is present %s", user_details)
                data = settings.JCR_URL_GUEST_DATA.format(
                    ta_hotel_map.ta_location_id,
                    user_details.get('email')) if user_details['name'] is None else settings.JCR_URL_GUEST_DATA.format(
                    ta_hotel_map.ta_location_id,
                    user_details.get('email')) + "&firstName=" + user_details['name']
                hashed_hex_data = self.encrypt_guest_data(data)
                url = settings.TRIPADVISOR_BASE_URL + settings.JCR_URL.format(
                    settings.TA_JCR_URL_PARTNER_KEY,
                    ta_hotel_map.ta_location_id) + "&data=" + hashed_hex_data
            else:
                logger.info("No user %s", user_details)
                url = settings.TRIPADVISOR_BASE_URL + \
                    settings.JCR_URL.format(settings.TA_JCR_URL_PARTNER_KEY, ta_hotel_map.ta_location_id)
            logger.info("URL %s", url)
        except TAHotelIDMapping.DoesNotExist:
            logger.exception(
                "Exception in finding hotel %s mapping for ta",
                user_details['hotel_id'])
        except Exception:
            logger.exception(
                "Exception in creating jcr url for user_details %s",
                user_details)
        return url

    def encrypt_guest_data(self, data):
        try:
            encrypted_str = settings.TA_JCR_URL_ENCRYPTION_KEY + data
            md5_string = self.compute_MD5_hash(encrypted_str)
            hashed_hex_data = self.encode_hex(data)
            encoded_guest_info = md5_string + hashed_hex_data
            logger.info(
                "encrypted_str %s md5_string %s hashed_hex_data %s encoded_guest_info %s",
                encrypted_str,
                md5_string,
                hashed_hex_data,
                encoded_guest_info)
            return encoded_guest_info
        except Exception as e:
            logger.exception(
                "Exception in encrypting jcr url guest data %s", data)
            raise e

    def compute_MD5_hash(self, data):
        try:
            url_encoded_data = urllib.parse.quote(data)
            m = hashlib.md5()
            m.update(url_encoded_data.encode('utf-8'))
            return m.hexdigest()
        except Exception as e:
            logger.exception(
                "Exception in creating md5 hash for data %s", data)
            raise e

    def encode_hex(self, md5_string):
        try:
            #change as part of porting to python3
            hex_encode = binascii.hexlify(md5_string.encode()).decode()
            #hex_encode = md5_string.encode('hex')
            hex_encode = hex_encode.upper()
            return hex_encode
        except Exception:
            logger.exception(
                "Exception in encoding the md5 string %s",
                md5_string)
