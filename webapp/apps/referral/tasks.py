from celery import shared_task
from celery.signals import task_prerun
from celery.utils.log import get_task_logger

from apps.referral.service.service import ReferralService
from base.filters import CeleryRequestIDFilter
from base.logging_decorator import log_args
from dbcommon.models.profile import User, UserReferral, UserReferralReward

logger = get_task_logger(__name__)


@task_prerun.connect()
def set_request_id(
        signal=None,
        sender=None,
        task_id=None,
        task=None,
        args=None,
        **kwargs):
    logger.addFilter(
        CeleryRequestIDFilter(
            kwargs.get('user_id'),
            kwargs.get('request_id'),
            task_id,
            task.name))
    logger.info(
        "set_request_id called with kwargs: %s, and task_id: %s",
        kwargs,
        task_id)


@shared_task
@log_args(logger)
def generate_referral_code(
        user_id,
        referrer_code="",
        share_link_identifier=""):
    user = User.objects.get(pk=user_id)
    return ReferralService.generate_referral_code(
        user, referrer_code, share_link_identifier)


@shared_task
@log_args(logger)
def send_sign_up_event(user_id):
    user = User.objects.get(pk=user_id)
    response = ReferralService.update_user(user)
    logger.info(
        "user profile updated %s",
        "successfully" if response["status"] == 200 else "unsucessfully")
    response = ReferralService.register_event(
        user, UserReferralReward.ReferralEvents.Signup)
    if response["status"] != 200:
        logger.error(
            "Failed to send signup event to appvirality for user %s",
            user.email)


@shared_task
@log_args(logger)
def send_transaction_event(friend_email, transaction_value):
    """
    This will consumer from Broadcast Queue and then if matches our queue
    :return:
    """
    logger.info("Send Transaction event get triggered")
    friend = User.objects.filter(email=friend_email).order_by('-id').first()
    friend_referral_details = UserReferral.objects.get(user=friend)
    try:
        friend_referral_code = friend_referral_details.friend_referral_code
        if friend_referral_code and friend_referral_code != '':
            referrer_referral_details = UserReferral.objects.select_related('user').filter(
                referral_code__icontains=friend_referral_code).order_by('-id').first()
            count = UserReferralReward.objects.filter(
                referrer=referrer_referral_details.user,
                referee=friend,
                type=UserReferralReward.ReferralEvents.Transaction).count()
            if count == 0:
                ReferralService.register_event(
                    friend,
                    UserReferralReward.ReferralEvents.Transaction,
                    transaction_value=transaction_value,
                    transaction_unit='Rs')
        else:
            logger.debug(
                "friend_referral_code is empty, can not send transaction coupon to %s",
                friend.email)
    except UserReferral.DoesNotExist:
        logger.error(
            "User %s does not have a referrer, cannot send transaction coupon",
            friend.email)
