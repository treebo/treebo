# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0031_auto_20160927_0551'),
        # migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    state_operations = [
        migrations.CreateModel(
            name='BookingRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('adults', models.PositiveIntegerField()),
                ('children', models.PositiveIntegerField()),
                ('room_required', models.PositiveIntegerField()),
                ('add_to_cart_hotel_id', models.CharField(max_length=200, null=True, blank=True)),
                ('name', models.CharField(max_length=200, null=True, blank=True)),
                ('email', models.EmailField(max_length=200, null=True, blank=True)),
                ('phone', models.CharField(max_length=20, null=True, blank=True)),
                ('coupon_code', models.CharField(max_length=200, null=True, blank=True)),
                ('discount_value', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('checkin_time', models.CharField(max_length=200, null=True, blank=True)),
                ('checkout_time', models.CharField(max_length=200, null=True, blank=True)),
                ('comments', models.TextField(default='')),
                ('booking_channel', models.CharField(default='', max_length=50, blank=True, choices=[(b'msite', b'msite'), (b'website', b'website')])),
                ('user_login_state', models.CharField(max_length=30, null=True, blank=True)),
                ('user_signup_channel', models.CharField(max_length=200, null=True, blank=True)),
                ('pay_at_hotel', models.CharField(max_length=10, null=True, blank=True)),
                ('booking_id', models.PositiveIntegerField(null=True)),
                ('order_id', models.CharField(max_length=200, null=True, blank=True)),
                ('session_id', models.CharField(max_length=200, null=True, blank=True)),
                ('booking_url', models.CharField(max_length=500, null=True, blank=True)),
                ('savebooking_job_id', models.CharField(max_length=200, null=True, blank=True)),
                ('confirmbooking_job_id', models.CharField(max_length=200, null=True, blank=True)),
                ('accesssecret', models.CharField(max_length=200, null=True, blank=True)),
                ('accesskey', models.CharField(max_length=200, null=True, blank=True)),
                ('meta_http_host', models.CharField(max_length=200, null=True, blank=True)),
                ('checkin_date', models.DateField(null=True)),
                ('checkout_date', models.DateField(null=True)),
                ('hotel_id', models.PositiveIntegerField(null=True)),
                ('room_config', models.CharField(max_length=50, null=True, blank=True)),
                ('room_type', models.CharField(max_length=50, null=True, blank=True)),
                ('coupon_apply', models.NullBooleanField(default=False)),
                ('is_audit', models.NullBooleanField(default=False)),
                ('instant_booking', models.NullBooleanField(default=None)),
                ('status', models.CharField(default='Initiated', max_length=50, null=True, blank=True, choices=[('Initiated', 'Initiated'), ('Payment In Progress', 'Payment In Progress'), ('Completed', 'Completed')])),
                ('rack_rate', models.DecimalField(null=True, max_digits=20, decimal_places=10)),
                ('mm_promo', models.DecimalField(null=True, max_digits=20, decimal_places=10)),
                ('pretax_amount', models.DecimalField(null=True, max_digits=20, decimal_places=10)),
                ('tax_amount', models.DecimalField(null=True, max_digits=20, decimal_places=10)),
                ('total_amount', models.DecimalField(null=True, max_digits=20, decimal_places=2)),
                ('payment_amount', models.DecimalField(default=0, null=True, max_digits=20, decimal_places=2)),
                ('payment_mode', models.CharField(default='Not Paid', max_length=200, null=True, blank=True)),
                ('voucher_amount', models.DecimalField(default=0, null=True, max_digits=20, decimal_places=2)),
                ('otp_verified_number', models.CharField(max_length=20, null=True, blank=True)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'bookings_bookingrequest',
                'verbose_name': 'Booking Request',
                'verbose_name_plural': 'Booking Requests',
            },
        ),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(state_operations=state_operations)
    ]
