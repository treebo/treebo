import logging

import requests
from django.core.urlresolvers import reverse

from apps.common import date_time_utils

logger = logging.getLogger(__name__)


class SearchClient(object):
    @staticmethod
    def get_search_results(
            request,
            search_string,
            checkin_date,
            checkout_date,
            room_config,
            latitude,
            longitude,
            locality,
            locationtype=None):
        search_rest_api = reverse(
            'search:search_view') + '?query={0}&checkin={1}&checkout={2}&roomconfig={3}&lat={4}&long={5}&locality={6}&locationtype={7}'.format(
            search_string,
            checkin_date.strftime(date_time_utils.DATE_FORMAT),
            checkout_date.strftime(date_time_utils.DATE_FORMAT),
            room_config, latitude, longitude, locality, locationtype
        )
        logger.info('Search API URL: %s', search_rest_api)
        response = requests.get(request.build_absolute_uri(search_rest_api))
        search_result = response.json()["data"]
        return search_result
