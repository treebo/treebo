import logging
from apps.featuregate import constants as featureConstants
from apps.featuregate.constants import FEATURE_MANAGER_CACHE_TIMEOUT
from apps.featuregate.constraint_validator import ConstraintValidator
from apps.featuregate.expression_parser_new import ExpressionParser
from apps.featuregate.models import Page, Constraints, ConstraintKeywords, ConstraintOperators, \
    VariableCondition
from apps.featuregate.exceptions import FeaturePageVariableNotFound, FeatureConstraintUpdateFailure, \
    KeywordNotFound, OperatorNotFound
from django.core.cache import cache

logger = logging.getLogger(__name__)


class FeatureManager:
    def __init__(
        self,
        pagename,
        checkin,
        checkout,
        hotel_id,
        totalcost,
        locality,
        city,
        utm_source='',
        utm_medium='',
        page=None):
        self.__city = city
        self.__pagename = pagename
        self.__checkin = checkin
        self.__checkout = checkout
        self.__hotel_id = hotel_id
        self.__totalcost = totalcost
        self.__locality = locality
        self.__page = page
        self.__utm_source = utm_source
        self.__utm_medium = utm_medium
        try:
            if not self.__page:
                self.__page = self.get_page()
        except BaseException:
            pass

    def get_page(self):
        key = "get_page" + self.__pagename
        if cache.get(key):
            return cache.get(key)
        page = Page.objects.prefetch_related(
            'variables', 'variables__variable_conditions').get(
            page_name=self.__pagename)
        cache.set(key, page, FEATURE_MANAGER_CACHE_TIMEOUT)
        return page

    def get_page_variables(self):
        key = "get_page_variables" + self.__pagename
        if cache.get(key):
            return cache.get(key)
        page_variables = self.__page.variables.all()
        cache.set(key, page_variables, FEATURE_MANAGER_CACHE_TIMEOUT)
        return page_variables

    def get_existing_rule(self, page_variable_name, keyword, keyword_type, operator):
        try:
            page = self.__page
            page_variables = page.variables.filter(name=page_variable_name,
                                                   variable_conditions__active=True)
            if not page_variables:
                raise FeaturePageVariableNotFound(
                    "No Feature gate page variable found for page:{page_name}"
                    " and feature page variable name:{page_variable_name}".format(
                        page_name=self.__page, page_variable_name=page_variable_name))
            page_variable_conditions = page_variables[0].variable_conditions.all()
            constraints = self.fetch_constraints_from_page_variable_condition(
                page_variable_conditions,
                keyword, keyword_type,
                operator)
            return page_variable_conditions, constraints
        except Exception as e:
            msg = "Exception while updating the constraints for page:{page} " \
                  "page_variable_name:{page_variable_name} and keyword:{keyword}".format(
                page=self.__page, page_variable_name=page_variable_name, keyword=keyword)
            logger.exception(msg)
            raise FeatureConstraintUpdateFailure(msg)

    def fetch_constraints_from_page_variable_condition(self, page_variable_conditions,
                                                       keyword_name=None,
                                                       keyword_type=None, operator=None):
        constraint_id_list = []
        for page_variable_condition in page_variable_conditions:
            expression = page_variable_condition.expression
            expression = expression.expression.split()
            constraint_id_list = ExpressionParser(expression).fetch_all_operands()
        return Constraints.objects.filter(id__in=constraint_id_list, keyword__keyword=keyword_name,
                                          keyword__type=keyword_type, operation__operator=operator)

    def fetch_all_constraints_from_page_variable_condition(self, page_variable_conditions):
        constraint_id_list = []
        for page_variable_condition in page_variable_conditions:
            expression = page_variable_condition.expression
            expression = expression.expression.split()
            constraint_id_list += ExpressionParser(expression).fetch_all_operands()
        key = "fetch_all_constraints_from_page_variable_condition" + str(constraint_id_list)
        if cache.get(key):
            return cache.get(key)
        result = Constraints.objects.select_related('keyword', 'operation').filter(
            id__in=constraint_id_list)
        cache.set(key, result, FEATURE_MANAGER_CACHE_TIMEOUT)
        return result

    def create_constraint(self, keyword, keyword_type, operator, value_one, value_two):
        try:
            keyword_object = ConstraintKeywords.objects.get(keyword=keyword, type=keyword_type)
            operator_object = ConstraintOperators.objects.get(operator=operator)
            constraint = Constraints()
            constraint.keyword = keyword_object
            constraint.operation = operator_object
            constraint.value_one = value_one
            constraint.value_two = value_two
            constraint.save()
            return constraint
        except ConstraintKeywords.DoesNotExist:
            raise KeywordNotFound(
                "No Constraint keyword found by keyword name:{name}".format(name=keyword))
        except ConstraintOperators.DoesNotExist:
            raise OperatorNotFound(
                "No Constraint Operator found by operator:{operator}".format(name=operator))

    def create_constraint_validator_for_page_variables(self, page_variables_conditions):
        constraint_for_page_variable_conditions = self.fetch_all_constraints_from_page_variable_condition(
            page_variables_conditions)
        constraints_dict = {constraint.id: constraint for constraint in
                            constraint_for_page_variable_conditions}
        constraint_validator = ConstraintValidator(
            self.__checkin,
            self.__checkout,
            self.__hotel_id,
            self.__totalcost,
            self.__locality,
            self.__city,
            constraints_dict,
            self.__utm_source,
            self.__utm_medium,
        )
        return constraint_validator

    def get_overriding_values(self):
        page = None
        overriding_variables = {}

        try:
            page = self.__page
            if not page:
                return overriding_variables

            # WEB-1916
            # Fetching all the values at once for now. Total no of rows as of now is few hundreds
            # Later it can be modified easily to accommodate the fetch based on the constraint ids in the expression,
            # subsequently increasing the DB calls by a few

            for page_var in self.get_page_variables():
                try:
                    if page_var.variable_conditions.count() > 0:
                        all_variable_conditions = page_var.variable_conditions.filter(
                            active=True)
                        constraint_validator = self.create_constraint_validator_for_page_variables(
                            all_variable_conditions)
                        for aCondition in all_variable_conditions:
                            expression = aCondition.expression
                            expression = expression.expression.split()
                            expression.append("EOF")

                            ExpressionParser(
                                expression, constraint_validator).parse_expression()

                            logger.debug(
                                'Forming Expression parser with expression: %s , result %s',
                                expression,
                                constraint_validator.message)
                            if constraint_validator.message == featureConstants.SUCCESS:
                                overriding_variables[page_var.name] = self.__to_object(
                                    aCondition.final_value, page_var.type)
                                logger.debug(
                                    'Overriding Variables result: %s',
                                    overriding_variables)
                                break
                except Exception as e:
                    logger.exception(
                        "Exception while processing the expresssion for page:{page_name}"
                        " page_variable:{page_variable_name}".format(page_name=self.__pagename,
                                                                     page_variable_name=page_var.name))
                    pass

        except Exception as e:
            overriding_variables = {}
            logger.exception("Exception: %s", e)
        return overriding_variables

    def __to_object(self, value, type):
        if type == 'Boolean':
            return value.lower() in ("yes", "true", "t", "1")
        else:
            return value

    @classmethod
    def feature_manager_factory(cls, feature_manager_dto):
        return cls(
            feature_manager_dto.pagename,
            feature_manager_dto.checkin,
            feature_manager_dto.checkout,
            feature_manager_dto.hotel_id,
            feature_manager_dto.totalcost,
            feature_manager_dto.locality,
            feature_manager_dto.city)
