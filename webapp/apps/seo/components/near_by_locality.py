import geopy
import geopy.distance

from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.components.base_component import BaseComponent
from apps.seo.serializers.serializer import LocalitySerializer
from dbcommon.models.location import Locality

__author__ = 'ansilkareem'


class NearbyLocalities(BaseComponent):

    def __init__(self, current_city, footer_data, origin_latitude, origin_longitude, max_results=None):
        self.origin_latitude = origin_latitude
        self.origin_longitude = origin_longitude
        self.near_by_localities = dict()
        self.sorted_nearby_localities = []
        self.max_results = max_results
        super(NearbyLocalities, self).__init__(current_city, footer_data, [])

    def build(self):
        result = dict(key='near_by_localities',
                      title='Nearby localities')
        if self.current_city and self.current_city.enable_for_seo:
            all_localities = Locality.objects.filter(enable_for_seo=True, city__enable_for_seo=True,
                                                     is_hotel_present=True,
                                                     city=self.current_city.id)
            current_location = geopy.Point(self.origin_latitude, self.origin_longitude)
            for locality in all_localities:
                locality_point = geopy.Point(locality.latitude, locality.longitude)
                distance = geopy.distance.distance(current_location, locality_point).km
                if distance <= self.distance_cap:
                    # adding to near_by cities only if the distance cap matches
                    self.near_by_localities[str(locality.name)] = (
                        distance, locality)
            # Now sorting the dict of near_by_cities
            self.sorted_nearby_localities = [seq[1][1] for seq in
                                             sorted(list(self.near_by_localities.items()), key=lambda x: x[1][0])]
            result['content'] = AutoCompleteResultBuilder.build_locality_result(self.sorted_nearby_localities[:self.max_results]) \
                if self.sorted_nearby_localities else []
            serialized_obj = LocalitySerializer(data=result)
            if serialized_obj.is_valid():
                return serialized_obj.validated_data
        return result
