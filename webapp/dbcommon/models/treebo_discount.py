# from django.db import models
# from djutil.models import TimeStampedModel
#
# __author__ = 'amithsoman'
#
#
# class ConstraintKeywords(TimeStampedModel):
# 	keyword = models.CharField(max_length=200, default="", null=True)
# 	type = models.CharField(max_length=20, default="", null=True)
# 	description = models.CharField(max_length=1000, default="", null=True)
#
#
# class ConstraintOperators(TimeStampedModel):
# 	operator = models.CharField(max_length=10, default="", null=True)
# 	description = models.CharField(max_length=100, default="", null=True)
#
#
# class DiscountCoupon(TimeStampedModel):
# 	code = models.CharField(max_length=200, default="", unique=True)
# 	FIXED = 'FIXED'
# 	PERCENTAGE = 'PERCENTAGE'
# 	NIGHT = 'NIGHT'
# 	FLATPERNIGHT = 'FLATPERNIGHT'
# 	DISCOUNT_OPERATION_CHOICES = (
# 		(FIXED, 'Fixed'),
# 		(PERCENTAGE, 'Percentage'),
# 		(NIGHT, 'Night'),
# 		(FLATPERNIGHT, 'Flatpernight')
# 	)
# 	discount_operation = models.CharField(max_length=20, choices=DISCOUNT_OPERATION_CHOICES, default=PERCENTAGE)
# 	discount_value = models.DecimalField(max_digits=5, decimal_places=1, default=0)
# 	max_available_usages = models.IntegerField(default=0)
# 	max_available_discount = models.PositiveIntegerField(default=0)
# 	expiry = models.DateField(blank=True, null=True)
# 	is_active = models.BooleanField(default=False)
# 	tagline = models.CharField(max_length=200, default="", null=True)
# 	terms = models.CharField(max_length=5000, default="", null=True)
# 	PRETAX = 0
# 	POSTTAX = 1
# 	STATUS_CHOICES = (
# 		(PRETAX, 'Pretax'),
# 		(POSTTAX, 'Posttax'))
# 	discount_application = models.IntegerField(default=POSTTAX, choices=STATUS_CHOICES)
# 	DISCOUNT = 'DISCOUNT'
# 	CASHBACK = 'CASHBACK'
# 	ROOMNIGHTS = 'ROOMNIGHTS'
# 	VOUCHER = 'VOUCHER'
# 	COUPON_TYPE_CHOICES = (
# 		(DISCOUNT, 'Discount'),
# 		(CASHBACK, 'CashBack'),
# 		(ROOMNIGHTS, 'RoomNights'),
# 		(VOUCHER, 'Voucher'),
# 	)
# 	coupon_type = models.CharField(max_length=200, choices=COUPON_TYPE_CHOICES, default=DISCOUNT)
# 	coupon_message = models.CharField(default="Discount value", max_length=500)
#
# 	def __str__(self):  # __unicode__ on Python 2
# 		return self.code
#
#
# class ConstraintExpression(TimeStampedModel):
# 	coupon = models.ForeignKey(DiscountCoupon, null=True, blank=True)
# 	expression = models.CharField(max_length=2000, default="", null=True)
#
#
# class DiscountCouponConstraints(TimeStampedModel):
# 	coupon = models.ForeignKey(DiscountCoupon, null=True, default="")
# 	keyword = models.ForeignKey(ConstraintKeywords, null=True, default="")
# 	operation = models.ForeignKey(ConstraintOperators, null=True, default="")
# 	type = models.CharField(max_length=200, null=True, blank=True)
# 	value_one = models.CharField(max_length=200, null=True, blank=True)
# 	value_two = models.CharField(max_length=200, null=True, blank=True)
