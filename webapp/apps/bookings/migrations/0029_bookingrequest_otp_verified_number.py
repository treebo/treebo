# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0028_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='otp_verified_number',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
    ]
