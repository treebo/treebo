import logging

from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.bookings.services.booking_history_service import BookingHistoryService
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import UserUnauthroized
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class BookingHistoryAPI(TreeboAPIView):
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    renderer_classes = [TreeboCustomJSONRenderer, ]
    booking_history_service = BookingHistoryService()

    def get(self, request, format=None):
        """
        Gets the booking history for a user
        """
        logger.info("booking history request")
        if not (hasattr(request, 'user')
                and request.user and request.user.is_authenticated()):
            response = api_response.treebo_exception_error_response(
                UserUnauthroized())
            return response

        user = request.user
        try:
            bookings, _, _, _ = self.booking_history_service.get_booking_history(
                user)
            response = api_response.prep_success_response(bookings)
        except TreeboException as e:
            logger.exception("treebo booking history exception %s ", user)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception("internal error booking history v2 %s", user)
            response = api_response.internal_error_response(error_codes.get(
                error_codes.UNABLE_TO_FETCH_BOOKING), BookingHistoryAPI.__name__)
        return response
