# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('pricing', '0005_roomprice'),
    ]

    operations = [
        migrations.AddField(
            model_name='roomprice',
            name='rack_rate',
            field=models.DecimalField(
                default=0,
                max_digits=20,
                decimal_places=10),
        ),
    ]
