
from django.urls import resolve
from django.test import TestCase

from .apps.apiv1.views.contactus import SaveContactUs
from .apps.apiv1.views.feedback import Feedback
from .apps.apiv1.views.joinus import JoinSave
from .apps.apiv1.views.notify_location import NotifyLocation
# from apps.apiv1.views.users.change_password import ChangePasswordAPI
# from apps.apiv1.views.users.forgot_password import ForgotPasswordAPI
from .apps.auth.api.v1.login import LoginAPI, FacebookLoginAPI, GooglePlusLoginAPI
# from apps.apiv1.views.users.notification_settings import NotificationSettingsAPI
from .apps.auth.api.v1.register import RegisterAPI
# from apps.apiv1.views.users.reset_password import ResetPasswordAPI
# from apps.bookings.v1.views.addtocart import AddToCart
# from apps.bookings.v1.views.bookingstub import GetLastBookingsByHotel, GetBookingsByUser, \
#    GetRoomBookingByRoom, \
#    BookingsByOrderId, BookingDetails, GetReservationForHotel, ReservationsForToday
#from apps.bookings.v1.views.cancelbooking import CancelBookingAPI
#from apps.bookings.v1.views.discountcoupon import ReapplyDiscount
from .apps.checkout.api.v1.payuhandler import PayU
# from apps.bookings.v1.views.printpdf import PrintPdf, DisplayPDF
from .apps.checkout.api.v1.resendemail import Remail
from .apps.bookingstash.v1.views.get_cheapest_room import CheapestRoom
from .apps.bookingstash.v1.views.getroomtypeavailability import RoomTypeAvailability, \
    RoomTypeAvailabilityAll
from .apps.bookingstash.v1.views.updateavailability import UpdateAvailability
from .apps.content.api.views import GetContent
from .apps.discounts import views
from .apps.discounts.discountstub import GetAllPrefixesCount, CreateUserDiscounts
from .apps.hotels import views as hotel_views
from .apps.hotels.api.v1.get_all_cities import CityInformation
from .apps.hotels.api.v1.get_hotel_details import HotelDetails
from .apps.hotels.api.v1.get_hotel_rooms import HotelRooms
from .apps.pages.views import landing, hotel_detail, faq, aboutus, voucher, terms, policy, \
    iesupport, offers
from .apps.pages.views.bookings import Bookings
from .apps.pages.views.city import CityDetails
from .apps.pages.views.itinerary import ItineraryPage
from .apps.pages.views.contactus import Contactus
from .apps.pages.views.feedback import Feedback
from .apps.pages.views.forgot_password import ForgotPassword
from .apps.pages.views.join import Join
from .apps.pages.views.login import Login
from .apps.pages.views.logout import Logout
from .apps.pages.views.order import Order
from .apps.pages.views.print_pdf import PdfData
from .apps.pages.views.register import Registration
from .apps.pages.views.reset_password import ResetPassword
from .apps.pages.views.settings import Settings
from .apps.pages.views.terms import Terms
from .apps.pages.views.unconfirmed import Unconfirmed
from .apps.pricing.v1.views.get_price import Pricing
from .apps.search.api.v1.search_contents import SearchWidgets
from .apps.search.api.v1 import search_api


class UrlConfigTest(TestCase):
    def setUp(self):
        self.urls = {
            '/checkoutaction/': PayU,
            '/allCities/': CityInformation,
            "/room/hashed_hotel_id/": HotelRooms,
            '/register/': Registration,
            '/login/': Login,
            '/user/forgot-password/': ForgotPassword,
            "/user/reset-password/slug-slug": ResetPassword,
            '/logout/': Logout,
            "/terms/": Terms,
            "/feedback/": Feedback,
            '/user/booking-history/': Bookings,
            "/user/settings/": Settings,
            '/rest/v1/pricing/availability': Pricing,
            "/voucher/": voucher.Voucher,
            "/aboutus/": aboutus.Aboutus,
            "/faq/": faq.Faq,
            "/terms/": terms.Terms,
            "/policy/": policy.Policy,
            "/iesupport/": iesupport.Iesupport,
            "/offers/": offers.Offers,
            "/joinus/": Join,
            "/contactus/": Contactus,
            '/': landing.Index,
            "/search/": search_api.SearchView,
            "/hotels-in-jaipur/treebo-crystal-bangalore-hashed_hotel_id/": hotel_detail.HotelDetail,
            "/booking/TRB-123234/confirmation/": Order,
            "/city/city_string-chashed_city_id/": CityDetails,

            # Import from apps.apiv1.urls
            '/rest/v1/login': LoginAPI,
            '/rest/v1/fbLogin': FacebookLoginAPI,
            '/rest/v1/googleLogin': GooglePlusLoginAPI,
            '/rest/v1/register': RegisterAPI,
            # '/rest/v1/user/forgot-password': ForgotPasswordAPI,
            # '/rest/v1/user/reset-password': ResetPasswordAPI,
            # '/rest/v1/user/change-password': ChangePasswordAPI,
            # '/rest/v1/user/notification-settings': NotificationSettingsAPI,
            # '/rest/v1/forgot-password': ForgotPasswordAPI,
            "/rest/v1/notify-location": NotifyLocation,
            # '/rest/v1/pricingAvailability/': BookingAvailability,
            '/rest/v1/contentService/': SearchWidgets,
            "/rest/v1/joinussave/": JoinSave,
            "/rest/v1/contactus/": SaveContactUs,
            "/rest/v1/feedback/": Feedback,

            # Import from apps.bookings.urls
            # "/rest/v1/bookings/addtocart": AddToCart,
            # "/rest/v1/bookings/printpdf": PrintPdf,
            # "/rest/v1/bookings/display": DisplayPDF,
            # "/rest/v1/bookings/cancelbooking": CancelBookingAPI,
            # '/rest/v1/bookings/hotels/09239/delta/009099/': GetLastBookingsByHotel,
            # '/rest/v1/bookings/email/email_id_of_user/users/03239/': GetBookingsByUser,
            # '/rest/v1/bookings/roombookings/0339|/': GetRoomBookingByRoom,
            # '/rest/v1/bookings/0390|9/': BookingDetails,
            # '/rest/v1/bookings/orders/order_id/': BookingsByOrderId,
            # '/rest/v1/bookings/reservations/': GetReservationForHotel,
            # "/rest/v1/bookings/getallreservation": ReservationsForToday,
            # "/rest/v1/bookings/reapplydiscount": ReapplyDiscount,
            "/rest/v1/bookings/resendemail": Remail,

            # Import from apps.bookingstash.urls
            '/rest/v1/bookingstash/getcheapestroom': CheapestRoom,
            '/rest/v1/bookingstash/updateavailability/': UpdateAvailability,
            '/rest/v1/bookingstash/mincount/': RoomTypeAvailability,
            '/rest/v1/bookingstash/mincountall/': RoomTypeAvailabilityAll,

            # Import from apps.content.urls
            '/rest/v1/content/getcontent': GetContent,

            # Import from apps.pages.urls
            "/itinerary/": ItineraryPage,
            # url(r"^order$", Order,
            "/pages/pdfdata": PdfData,
            "/pages/unconfirmed/": Unconfirmed,

            # Import from apps.discount.urls
            "/discount/": views.CreateDiscount,
            "/discount/users/": CreateUserDiscounts,
            "/discount/constraint/": views.CreateConstraint,
            "/discount/update/129309/": views.UpdateDiscount,
            "/discount/bulkdiscounts/": views.CreateBulkDiscounts,
            "/discount/applicablediscounts/": views.ApplicableDiscounts,
            "/discount/prioritydiscount/": views.PriorityDiscount,
            "/discount/checkapplicability/": views.CheckApplicability,
            "/discount/applydiscount/": views.ApplyDiscount,
            "/discount/autopromo/": views.AutoPromo,
            "/discount/prefixcount/prefix_count_kwargs/": GetAllPrefixesCount,
        }

    def test_url_match_view(self):
        import importlib
        fail_count = 0
        for url, view in list(self.urls.items()):
            try:
                resolver_match = resolve(url)
                func = resolver_match.func
                module = importlib.import_module(func.__module__)
                view_name = func.__name__
                clazz = getattr(module, view_name)
                self.assertEqual(clazz.__name__, view.__name__)
            except Exception as e:
                print(("Failed url matching for url:", url, " and view:", view))
                fail_count += 1

        print(("Fail Count: ", fail_count))
        self.assertEqual(fail_count, 0)
