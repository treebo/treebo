import logging
from datetime import datetime

from django.conf import settings

from apps.common.slack_alert import SlackAlertService

logger = logging.getLogger(__name__)


class CSMessagesParser:

    def parse_state_data(self, state_json):
        try:
            state_data = {}
            state_data['cs_id'] = state_json['id']
            state_data['name'] = state_json['name']
            return state_data
        except Exception as e:
            logger.exception("Error %s in parsing state related data" % e.__str__())
            if 'id' in state_json:
                SlackAlertService.send_cs_error_msg_to_slack(cs_id=state_json['id'],
                                                             error_msg="Error %s in parsing state related data "
                                                                       % e.__str__(),
                                                             class_name=self.__class__.__name__)
            else:
                SlackAlertService.send_cs_error_msg_to_slack(
                    error_msg="cs_id not present in state_json",
                    class_name=self.__class__.__name__)
            raise e

    def parse_city_data(self, city_json):
        try:
            # Check if city exists, if not create:
            city_data = {}
            city_aliases_data = []
            city_data['cs_id'] = city_json['id']
            city_data['name'] = city_json['name']
            city_data['status'] = 1
            city_data['enable_for_seo'] = True
            try:
                city_aliases = city_json['aliases']
                for alias in city_aliases:
                    city_alias_data = {}
                    city_alias_data['alias'] = alias['name']
                    city_alias_data['city'] = city_data['name']
                    city_aliases_data.append(city_alias_data)
            except Exception as e:
                logger.exception("Error %s in parsing city alias data" % e.__str__())
                SlackAlertService.send_cs_error_msg_to_slack(cs_id=city_json['id'],
                                                             error_msg="Error %s in parsing city related data"
                                                                       % e.__str__(),
                                                             class_name=self.__class__.__name__)
            return city_data, city_aliases_data
        except Exception as e:
            logger.exception("Error %s in parsing city data" % e.__str__())
            if 'id' in city_json:
                SlackAlertService.send_cs_error_msg_to_slack(cs_id=city_json['id'],
                                                             error_msg="Error %s in parsing city related data"
                                                                       % e.__str__(),
                                                             class_name=self.__class__.__name__)
            else:
                SlackAlertService.send_cs_error_msg_to_slack(
                    error_msg="cs_id not present in city_json",
                    class_name=self.__class__.__name__)
            raise e

    def parse_locality_data(self, locality_json, pincode=1):
        try:
            # Check if locality exists, if not create:
            locality_data = {}
            locality_data['cs_id'] = locality_json['id']
            locality_data['name'] = locality_json['name']
            locality_data['pincode'] = pincode
            locality_data['longitude'] = locality_json['longitude']
            locality_data['latitude'] = locality_json['latitude']
            if not locality_data['latitude'] or not locality_data['longitude'] or not float(
                locality_data['latitude']) \
                or not float(locality_data['longitude']):
                raise Exception(
                    "Latitude and longitude cannot be null or 0.0 for locality " + locality_data[
                        'name'])
            return locality_data
        except Exception as e:
            logger.exception("Error %s in parsing locality data" % e.__str__())
            if 'id' in locality_json:
                SlackAlertService.send_cs_error_msg_to_slack(cs_id=locality_json['id'],
                                                             error_msg="Error %s in parsing locality related data"
                                                                       % e.__str__(),
                                                             class_name=self.__class__.__name__)
            else:
                SlackAlertService.send_cs_error_msg_to_slack(
                    error_msg="cs_id not present in locality_json",
                    class_name=self.__class__.__name__)
            raise e

    def parse_landmark_and_directions_data(self, landmarks_json):
        result = []
        landmark_str = ''
        try:
            for landmark in landmarks_json:
                landmark_data = {}
                landmark_data['cs_id'] = landmark['id'] if 'id' in landmark else None
                landmark_data['name'] = landmark['name']
                landmark_str = landmark_data['name']
                landmark_data['latitude'] = landmark['latitude']
                landmark_data['longitude'] = landmark['longitude']
                landmark_data['status'] = 1
                directions_data = {}
                directions_data['distance'] = landmark['hotel_distance']
                hotel_directions = landmark['hotel_direction']
                direction_is_valid = self.is_valid_direction(hotel_directions)
                directions_data['directions_for_email_website'] = (landmark['hotel_direction']
                                                                   if direction_is_valid else "")
                directions_data['directions_for_sms'] = (landmark['hotel_direction']
                                                         if direction_is_valid else "")
                landmark, directions = landmark_data, directions_data
                result.append((landmark, directions))
            return result, landmark_str
        except Exception as e:
            logger.exception("Error %s in parsing landmark and direction data" % e.__str__())
            if 'id' in landmarks_json:
                SlackAlertService.send_cs_error_msg_to_slack(cs_id=landmarks_json['id'],
                                                             error_msg="Error %s in parsing landmarks related data"
                                                                       % e.__str__(),
                                                             class_name=self.__class__.__name__)
            else:
                SlackAlertService.send_cs_error_msg_to_slack(
                    error_msg="cs_id not present in landmarks_json",
                    class_name=self.__class__.__name__)
            raise e

    def is_valid_direction(self, direction):
        if not direction:
            return False
        import re
        url_regex_string = '(http?|www)'
        pattern = re.compile(url_regex_string)
        if pattern.match(direction):
            return False
        return True

    def parse_search_landmark_data(self, landmarks_json):
        landmarks_info, _ = self.parse_landmark_and_directions_data(landmarks_json)
        for landmark, directions in landmarks_info:
            landmark['seo_url_name'] = landmark['name']
            # landmark['search_label'] = landmark['name']
            # landmark['free_text_url'] = landmark['name']
        return landmarks_info

    def extract_hotel_data(self, property_info_from_cs, cs_property_id=None):

        try:
            # Check if hotel exists, if not create:
            hotel_data = {}
            hotel_data['cs_id'] = property_info_from_cs[
                'id'] if 'id' in property_info_from_cs else cs_property_id
            hotel_data['description'] = property_info_from_cs['description']['property_description']
            if not hotel_data['description']:
                raise Exception
            checkin_time = property_info_from_cs['guest_facing_details']['checkin_time']
            checkout_time = property_info_from_cs['guest_facing_details']['checkout_time']
            hotel_data['checkin_time'] = datetime.strptime(checkin_time, "%H:%M:%S")
            hotel_data['checkout_time'] = datetime.strptime(checkout_time, "%H:%M:%S")
            hotel_data['hotelogix_id'] = property_info_from_cs['hx_id']
            hotel_data['landmark'] = ''
            hotel_data['latitude'] = property_info_from_cs['location']['latitude']
            hotel_data['longitude'] = property_info_from_cs['location']['longitude']
            hotel_data['street'] = property_info_from_cs['location']['postal_address']
            hotel_data['name'] = property_info_from_cs['name']['new_name']
            hotel_data['phone_number'] = "" if property_info_from_cs['property_details'][
                                                   'reception_mobile'] is None else \
                property_info_from_cs['property_details']['reception_mobile']
            if ',' in hotel_data['phone_number']:
                hotel_data['phone_number'] = hotel_data['phone_number'].split(',')[0]
            hotel_data['status'] = 1 if (property_info_from_cs['status'] == 'LIVE' and not any(
                property['code'] == 'independent' for property in
                property_info_from_cs.get('brands'))) else 0
            hotel_data['price_increment'] = 0
            hotel_data['tagline'] = hotel_data['name']
            try:
                hotelogix_name = hotel_data['name']
                hotelogix_name = hotelogix_name.split(' ', 1)[1]
                hotelogix_name = hotelogix_name + '(' + hotel_data['hotelogix_id'] + ')'
            except Exception as e:
                logger.exception("Error in forming hotelogix name, defaulting it to hotel name")
                SlackAlertService.send_cs_error_msg_to_slack(cs_id=hotel_data['cs_id'],
                                                             error_msg="Error in forming hotelogix name,"
                                                                       " defaulting it to hotel name => %s" % (
                                                                           str(e)))
                hotelogix_name = hotel_data['name']

            hotel_data['hotelogix_name'] = hotelogix_name
            # storing property type and provider for properties
            hotel_data['provider_name'] = settings.DEFAULT_PROVIDER_NAME
            hotel_data['property_type'] = property_info_from_cs['property_details'][
                'property_type'].lower()
            if hotel_data['property_type'] == 'homestay_cottage_villa':
                hotel_data['property_type'] = 'homestay'
            if 'external_provider' in property_info_from_cs['property_details'] and \
                property_info_from_cs['property_details']['external_provider']:
                hotel_data['provider_name'] = \
                    property_info_from_cs['property_details']['external_provider'][
                        'provider_name'].lower()
            if 'hygiene_shield_name' in property_info_from_cs['property_details']:
                hotel_data['hygiene_shield_name'] = property_info_from_cs['property_details']['hygiene_shield_name']
            logger.info("Hotel data extracted")
            return hotel_data
        except Exception as e:
            logger.exception("Error %s in parsing hotel data" % e.__str__())
            if 'id' in property_info_from_cs:
                SlackAlertService.send_cs_error_msg_to_slack(cs_id=property_info_from_cs['id'],
                                                             error_msg="Error %s in parsing hotel related data"
                                                                       % e.__str__(),
                                                             class_name=self.__class__.__name__)
            elif not property_info_from_cs['description']['property_description']:
                SlackAlertService.send_cs_error_msg_to_slack(cs_id=property_info_from_cs['id'],
                                                             error_msg="Description not present for property",
                                                             class_name=self.__class__.__name__)
            else:
                SlackAlertService.send_cs_error_msg_to_slack(
                    error_msg="cs_id not present in hotel_data",
                    class_name=self.__class__.__name__)
            raise e

    def extract_highlight_data(self, cs_property_description_json):
        try:
            # Check if highlights exist, if not create
            highlights_data = []
            highlights = {k: v for k, v in list(cs_property_description_json.items()) if
                          k.startswith('trilight')}
            for value in list(highlights.values()):
                highlight_data = {}
                highlight_data['description'] = value or ''
                highlights_data.append(highlight_data)
            return highlights_data
        except Exception as e:
            logger.exception("Failed to update Hotel Highlights %s" % str(e))
            SlackAlertService.send_cs_error_msg_to_slack(
                error_msg="Failed to update Hotel Highlights %s"
                          % e.__str__.__name__)

    def get_state_json_from_message(self, cs_message_body):
        return cs_message_body['location']['state']

    def get_city_json_from_message(self, cs_message_body):
        return cs_message_body['location']['city']

    def get_locality_json_from_message(self, cs_message_body):
        return cs_message_body['location']['locality']

    def get_landmark_json_from_message(self, cs_message_body):
        return cs_message_body['landmarks'] + cs_message_body['data']['transport_stations']

    def get_pincode_from_cs_message(self, cs_message_body):
        return cs_message_body['location']['pincode']

    def get_room_data_from_json(self, cs_property_json):
        try:
            return cs_property_json['room_type_configs']
        except Exception as e:
            logger.error(e)
            return None

    def get_amenities_data_from_json(self, cs_property_json):
        try:
            return cs_property_json['amenity_summary']
        except Exception as e:
            logger.error(e)
            return None

    def get_images_data_from_json(self, cs_property_json):
        try:
            return cs_property_json['property_images']
        except Exception as e:
            logger.error(e)
            return None

    def parse_room_data(self, room_info_from_cs):
        try:
            room_type_choices = {'OAK': 'Oak (Standard)', 'MAPLE': 'Maple (Deluxe)',
                                 'MAHOGANY': 'Mahogany (Premium)',
                                 'ACACIA': 'Acacia (Solo)'}
            room_data = {}
            room_data['room_type'] = room_type_choices[room_info_from_cs['room_type']['type']]
            room_data['room_type_code'] = room_info_from_cs['room_type']['type'].capitalize()
            min_room_size = room_info_from_cs['min_room_size']
            room_data['room_area'] = int(round(float(min_room_size))) if min_room_size else 0
            room_data['cs_id'] = room_info_from_cs['id']
            room_data['price'] = 2000
            room_data['quantity'] = room_info_from_cs['room_count']
            room_data['available'] = room_data['quantity']
            room_data['description'] = ''
            if 'description' in room_info_from_cs and room_info_from_cs['description']:
                room_data['description'] = room_info_from_cs['description']
            max_children = 0 if room_info_from_cs['children'] is None else room_info_from_cs[
                'children']
            room_data['max_adult_with_children'] = room_info_from_cs['max_total']
            room_data['max_guest_allowed'] = int(
                room_info_from_cs['adults']) if 'adults' in room_info_from_cs \
                                                and room_info_from_cs['adults'] else 0
            room_data['base_rate'] = 1500
            room_data['max_children'] = max_children
            return room_data
        except Exception as e:
            logger.exception("Error %s in parsing room data" % e.__str__())
            if 'id' in room_info_from_cs:
                SlackAlertService.send_cs_error_msg_to_slack(cs_id=room_info_from_cs['id'],
                                                             error_msg="Error %s in parsing room json" % e.__str__(),
                                                             class_name=self.__class__.__name__)
            else:
                SlackAlertService.send_cs_error_msg_to_slack(
                    error_msg="cs_id not present in room json",
                    class_name=self.__class__.__name__)
            raise e

    def parse_external_room_details(self, room_info_from_cs):
        external_room_data = dict()
        try:
            external_room_data['name'] = room_info_from_cs['display_name'] if room_info_from_cs[
                'display_name'] \
                else room_info_from_cs['room_type']['type']
            external_room_data['rate_plan_name'] = room_info_from_cs['ext_rate_plan_name']
            external_room_data['rate_plan_code'] = room_info_from_cs['ext_rate_plan_code']
            external_room_data['room_code'] = room_info_from_cs['ext_room_code'] if \
            room_info_from_cs['ext_room_code'] \
                else room_info_from_cs['room_type']['code']
            return external_room_data
        except Exception as e:
            SlackAlertService.send_cs_error_msg_to_slack(
                error_msg="Error %s in parsing external room details" % e.__str__(),
                class_name=self.__class__.__name__)
            logger.exception("Error %s in parsing external room details" % e.__str__())
            raise e
