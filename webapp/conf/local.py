import logging
import os
from kombu import Exchange, Queue
from kombu.common import Broadcast
from conf import log_conf
from webapp.conf.base import *
from common.exceptions.treebo_exception import TreeboException

MIDDLEWARE_CLASSES += ['django.contrib.sessions.middleware.SessionMiddleware']
ENVIRONMENT = 'preprod'
FEATURE_TOGGLE_ENV = 'preprod'
DEBUG = True
DEVELOPMENT = False

SITE_ID = '6'
TREEBO_BASE_HOST_URL = 'localhost:8083'
TREEBO_WEB_URL = "http://localhost:8083"
TREEBO_BASE_URL = '//' + TREEBO_BASE_HOST_URL

APP_VIRALITY_PRIVATE_KEY = 'd97a79edacd64ef8855ba61e00a03a68'
APP_VIRALITY_API_KEY = 'a63403cb67874c838480a61e00a0aba0'
CELERY_HMS_TASK_NAME = 'hms_sync_booking'

#task_default_queue = 'celery_p3'



CELERY_QUEUES = (
    Queue(CELERY_DEFAULT_QUEUE,Exchange(CELERY_DEFAULT_QUEUE),routing_key=CELERY_DEFAULT_QUEUE),
    Queue(CELERY_CREATE_COUPON_QUEUE,Exchange(CELERY_CREATE_COUPON_QUEUE),routing_key=CELERY_CREATE_COUPON_QUEUE),
    Broadcast(CELERY_BROADCAST_BOOKING_QUEUE,routing_key='booking.all',exchange=EXCHANGE),
    Queue(CELERY_THIRD_PARTY_QUEUE,THIRD_PARTY_TRIGGER_EXCHANGE,routing_key=CELERY_THIRD_PARTY_QUEUE),
    Queue(CELERY_TRIVAGO_HOTEL_DETAILS,Exchange(TRIVAGO_EXCHANGE_NAME),routing_key=CELERY_TRIVAGO_HOTEL_DETAILS),
    Queue(CELERY_CREATE_COUPON_QUEUE,Exchange(CELERY_CREATE_COUPON_QUEUE),routing_key=CELERY_CREATE_COUPON_QUEUE),
)

CELERY_ROUTES = ('base.task_router.MyRouter',)

# BASE_PRICING_URL = "http://pricing-load-testing-mum-elb-01.treebo.com/v1/price?"
####
# Preprod
CONSUMER_KEY = "D2140AEC22BFF5037C56DE27BECE87E113782ABB"
CONSUMER_SECRET = "7752C96724C2BC6437CE8885363E0E4B7A939442"
####

MAX_WAIT_COUNT_FOR_S2S_CALLBACK = 5
DYNAMIC_ADS_FILE_PATH = '/ebs1/dynamicads/'

HOTELOGIX_URL = "http://crs.hotelogix.net/ws/web/"
HOTELOGIX_ADMIN_URL = "http://admin.hotelogix.net/ws/web/"

BOOKING_DETAILS_URL = "http://hotelogix.stayezee.com/ws/webv2/"
PAYMENT_SERVICE_HOST = "http://localhost:8084"

# DB_PWD = open("/root/db.pwd", "r").read().rstrip("\n")
# if not DB_PWD:
#     raise TreeboException("No password provided")

DATABASES_PREPROD = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'web_db',
        'USER': 'web_user',
        'PASSWORD': 'qW4rTwyger685r#',
        'HOST': 'web-s-sharedapps-rds-sg.cadmhukcksta.ap-southeast-1.rds.amazonaws.com',
        'PORT': '5432',
    }
}


DATABASES_VARUN = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'varunachar',
        'USER': 'varunachar',
        'PASSWORD': 'varunachar',
        'HOST': '172.40.20.210',
        'PORT': '5432',
    }
}
DATABASES_LOCAL = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'venamp',
        'USER': 'venamp',
        'PASSWORD': '12345',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
DATABASES = DATABASES_PREPROD

BASE_PRICING_HOST = 'http://price-staging.treebo.be'
BASE_PRICING_URL_V3 = BASE_PRICING_HOST + "/v3/price?"
BASE_PRICING_URL_V2 = BASE_PRICING_HOST + "/v2/price?"
BASE_PRICING_URL_V1 = BASE_PRICING_HOST + "/v1/price?"

PRICING_URL = "https://traffic.treebo.be/api/v2/hotels/"
STATIC_URL = '//gdc.treebohotels.com/dist/'
S3_URL = '//images.treebohotels.com/'
TREEBO_BASE_REST_HOST = TREEBO_WEB_URL

# PAYU PROD KEYS
PAYU_MERCHANT_ID = 'xBeWGL'
PAYU_SALT = 'cS9H6XPi'
PAYU_HANDLER_URL = TREEBO_BASE_URL + 'checkoutaction/'
PAYU_ACTION_URL = 'https://secure.payu.in/_payment'
PROWL_HOTEL_UPLOAD = 'http://staging.treebohotels.com/prowl/rest/v1/hotels/add'
PROWL_HOTEL_DISABLE = 'http://staging.treebohotels.com/prowl/rest/v1/hotels/disable'
PROWL_FOT_STATUS_UPDATE_URL = 'http://staging.treebohotels.com/prowl/rest/v1/fotfeedback'
HMS_GET_ALL_RESERVATIONS_FOR_TODAY = 'http://api.treebohotels.com/hmssync/rest/v1/todaysreservations'

AVAILABILITY_HOOK_API = {
    'b2b_axis_room_hook_api': "http://corporates.preprod.treebo.com/ext/api/daywiseInventory/"
}

# razorpay key for test level
RAZORPAY_KEYS = "rzp_test_upYNslfmrDV2tg"
RAZORPAY_SECRET = 'CAzoPh58gaTYCED3RHCJe4Pm'

RAZORPAY_API = "https://api.razorpay.com/v1/payments/"

LOG_REQUESTS = True

FACEBOOK_APP_ID = '920141571373108'
FACEBOOK_APP_SECRET = '395c13ef589da8cc82871e0478c46c42'

# Exclude list from tracking server responses - middleware : response.py
SERVER_STATUS_CAPTURE_FILTER = [200, 301, 302]

EMAIL_USE_TLS = True
EMAIL_HOST = 'email-smtp.eu-west-1.amazonaws.com'
EMAIL_HOST_USER = 'AKIAYZ7GE5SNPHU6ZJWY'
EMAIL_HOST_PASSWORD = 'BNLkNWuJPOSn2WIDtBdiNhl1jLb2LB6kNU7iqkv++7CZ'
EMAIL_PORT = 587
SERVER_EMAIL = 'Treebo Developers <dev@treebohotels.com>'

NOTIFY_HMS_EMAIL = False

CONTACTUS_AVAILABILITY_SUBJECT = 'Hotel room availability query'
CONTACTUS_CORPORATE_SUBJECT = 'Corporate travel agent query'
CONTACTUS_BOOKING_SUBJECT = 'Booking %(category)s query'
CONTACTUS_REFUND_SUBJECT = 'Refund related query'
CONTACTUS_CHECKINOUT_SUBJECT = 'Checkin checkout query'
CONTACTUS_OTHER_SUBJECT = 'Other query'

CONTACTUS_AVAILABILITY_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_CORPORATE_EMAIL_LIST = ['corporate@treebohotels.com', 'arunabh.sinha@treebohotels.com',
                                  'niharika.singh@treebohotels.com']
CONTACTUS_BOOKING_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_REFUND_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_CHECKINOUT_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_OTHER_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']

JOINUS_SUBJECT = 'Join us enquiry'
JOINUS_EMAIL_LIST = ['partners@treebohotels.com']
AVAILABILITY_FROM_BASIC_SEARCH = False
AVAILABILITY_DAY_LIMIT = 30

BROKER_URL = 'amqp://guest:guest@172.40.10.74:5673/'
CELERY_BROKER_URL = 'amqp://guest:guest@172.40.10.74:5673/'
PRICING_BROKER_URL = 'amqp://guest:guest@172.40.20.15:5672/'
AUTH_SERVER_HOST = 'http://172.40.10.74:8051'

GUEST_PREF_SUBJECT = 'Special Request Guest: %(guestname)s, Booking Id: %(bookingid)s, Hotel %(hotelname)s'
GUEST_PREF_EMAIL_LIST = ['rohit.jain@treebohotels.com']
HOTELS_FEED_MAIL_LIST = [
    'ranavishal.singh@treebohotels.com',
    'shekhar.gupta@treebohotels.com',
    'rohit.jain@treebohotels.com']
BOOKING_REPORT_MAIL_LIST = ['ranavishal.singh@treebohotels.com',
                            'rohit.jain@treebohotels.com']

DIRECT_TEAM = [
               'bibek.padhy@treebohotels.com',
               'veena.ampabathini@treebohotels.com',
               'varun.achar@treebohotels.com',
               'neeraj.prasad@treebohotels.com',
               'amitakhya.bhuyan@treebohotels.com',
               'piyush.trivedi@treebohotels.com',
               'arun.midha@treebohotels.com',
               'gaurav.kinra@treebohotels.com',
               'ainesh.patidar@treebohotels.com'
               ]

CANCELLATION_FAILURE_EMAIL_LIST = [] + DIRECT_TEAM

RESERVATION_FAILURE_EMAIL_LIST = [] + DIRECT_TEAM

RACK_RATE_FAILURE = [] + DIRECT_TEAM

ADMINS = [
    ('Varun Achar',
     'varun.achar@treebohotels.com'),
    ('Tech QA',
     'tech-qa@treebohotels.com'),
    ('Prashant Kumar',
     'prashant.kumar@treebohotels.com'),
    ('Veena Ampabathini',
     'veena.ampabathini@treebohotels.com'),
    ('Amitakhya Bhuyan',
    'amitakhya.bhuyan@treebohotels.com')
]

LOG_PATH_VARIABLE = 'LOG_PATH'
LOG_ROOT = os.getenv(LOG_PATH_VARIABLE, '/Users/rumanikumari/Documents/Workspace/logs/')
LOGGING = log_conf.get(LOG_ROOT)


# Make celery to use the Django root logger config, instead of using its own
CELERYD_HIJACK_ROOT_LOGGER = False

from celery.signals import setup_logging


@setup_logging.connect
def configure_celery_log(sender=None, **kwargs):
    import logging.config
    logging.config.dictConfig(LOGGING)


HOTEL_UPLOAD_FILE_LOCATION = LOG_ROOT
SITE_HOST_NAME = TREEBO_WEB_URL + '/'

# Redis Configuration
REDIS_URL = 'treebo-rediscache.l4kzjq.0001.apse1.cache.amazonaws.com'
REDIS_PORT = 6379
REDIS_DB = 0
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        #'BACKEND': 'redis_cache.backends.dummy.RedisDummyCache',
        'LOCATION': REDIS_URL + ':' + str(REDIS_PORT),
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'SOCKET_CONNECT_TIMEOUT': 5,
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            },
            'PICKLE_VERSION': 2,
        }
    }
}

IMAGE_UPLOAD_PATH = '/ebs1/logs/image_upload/'
POS_URL = 'https://conversion.treebo.be/api/v1/external/hotelads/updateposfile.xml'
# Third party configs
TRIVAGO_CSV_LINK = '/Users/rumanikumari/Documents/Workspace/Treebo/treebo/trivago.csv'
TRIVAGO_BID_FILE_PATH = '/ebs1/data/trivago/'

TRANSACTION_URL = "https://conversion.treebo.be/api/v1/external/hotelads/transactionmessage.xml"
GHA_CSV_EMAIL_ID = ['websales@treebohotels.com']

# GHA Updated Files
FEATURE_POS_MAIL = True
GHA_REPORT_INTERVAL_DAYS = 14

TOOLS_TRANSACTION_URL = "http://tools.treebohotels.com/api/v1/external/hotelads/transactionmessage.xml"
TOOLS_POS_URL = 'http://tools.treebohotels.com/api/v1/external/hotelads/updateposfile.xml'
POS_EMAIL = ["websales@treebohotels.com"]
SAVE_EMAIL_API_FAILURE_EMAIL_ID = ["web@treebohotels.com"]

# trip advisor
TA_CSV_EMAIL_ID = [
    "veena.ampabathini@treebohotels.com",
    "varun.achar@treebohotels.com",
    "piyush.trivedi@treebohotels.com"]
# criteo
CRITEO_CSV_EMAIL_ID = [
    "rashmi.kumari@treebohotels.com",
    "ansil.kareem@treebohotels.com"]

# growth url
GROWTH_ADD_HOTEL_URL = "http://growth.treebohotels.com/growth/common_service/update_hotel_info/"
# GHA
FETCH_BOOKING_STATUS = "http://growth.treebohotels.com/growth/transformer/booking/status/?wrid="

##partial payment growth api url
GROWTH_PARTPAY_URL = 'http://staging.treebohotels.com/growth/partpay/web/link/'

GROWTH_REFERRAL_COMM_API_URL = "http://growth.treebohotels.com/growth/referral/communication"

#AUTH_SERVER_HOST = 'http://172.40.20.183:8051'

AUTH_SERVER_HOST = 'http://172.40.10.74:8051'

APP_CRENDENTIALS = {
    'password': {
        'client_id': 'S5zQjG2Ljd1p3hBL56WKtMrCVnXzUe1nEVEXnlXL',
        'client_secret':
            'hG13WIBlkhgv9WTWGiwQMLcjgSHSt6TV16JJk67w9xpHMQomepvq5gLDSGzZPCvw13Ph4LYuJRhfH8U6UGXS5lzcrmQiu5pYOXm9xjVh0GRxFEoFWFF3bV8Xd5EOLeJA'
    }
}
# by default we use staging
HX_CONFIG = HX_STAGING

BOOKING_CLIENT_TYPE = "HotelogixClient"

EASY_JOB_LITE_SETTINGS_PATH = os.path.join(
    EASY_JOB_LITE_SETTINGS_DIR, ENVIRONMENT + ".yaml")

# cancel hash urls
GROWTH_FETCH_BOOKING_DATA_URL = "http://staging.treebohotels.com/growth/cancellation/cancelbookingdetails/"
GROWTH_SUBMIT_BOOKING_URL = "http://staging.treebohotels.com/growth/cancellation/submitcancellationrequest/"
GROWTH_CANCEL_HASH_URL = "http://staging.treebohotels.com/growth/cancellation/cancelhash"
PROWL_HOTEL_POSITIVES_DATA_URL = "http://staging.treebohotels.com/prowl/rest/v3/hotels/positivereviews/?hotelogix_code={0}&days={1}"
GROWTH_BOOKING_EXP_URL = "http://staging.treebohotels.com/growth/booking_exp/booking_exp_url/"

# seo phase 2
GROWTH_FREQUENTLY_BOOKED_URL = 'http://staging.treebohotels.com/growth/transformer/frequently_booked/'
TREEBO_WEB_URL = "https://traffic.treebo.be"
TRIPADVISOR_BASE_URL = "https://rcp-demo.ext.tripadvisor.com/"
AB_SETTINGS = {
    'is_ab_activated': True,
    'ab_base_url': 'http://ab1.treebohotel.in',
    'application_id': 'website',
    'cookies_enabled': False
}

REWARDS_SOURCE_NAME = 'loyalty-service'

CS_QUEUE_NAME = 'cs_queue_preprod'

HOTEL_POLICIES_EMAILS = [
    'neeraj.prasad@treebohotels.com',
    'gaurav.kinra@treebohotels.com',
    'amitakhya.bhuyan@treebohotels.com']


HEALTH_CHECK_CONF = dict(rmq_host=BROKER_URL, sqs_queue_name='queue_name', region_name='eu-west-1',
                         aws_secret_access_key='mykey', aws_access_key_id='access_id',
                         use_basic_db_check=True,
                         soft_dependencies=['CACHEHealthCheck', 'RMQHealthCheck']
                         )
CATALOGUE_SERVICE_RABBITMQ_SETTINGS = {
    'name': 'catalogue_service',
    'url': 'amqp://guest:guest@172.40.10.85:5672/',
    'exchange': 'cs_exchange',
    'routing_key': 'com.cs.property',
}