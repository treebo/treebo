from django.contrib.auth.backends import ModelBackend
import logging
from django.contrib.auth import get_user_model
from django.utils import timezone
from datetime import timedelta
from apps.auth.treebo_auth_client import TreeboAuthClient
from dbcommon.models.profile import AccessToken

logger = logging.getLogger(__name__)
from apps.common.exceptions.custom_exception import AuthException


class TreeboAuthModelBackend(ModelBackend):
    """
    Authenticates against settings.AUTH_USER_MODEL.
    """
    treebo_auth_client = TreeboAuthClient()
    user_model = get_user_model()

    def authenticate(self, **credentials):
        token = credentials.get('token')
        user = None
        if token:
            user = self._verify_token(token)
        email = credentials.get('email') or credentials.get('username')
        password = credentials.get('password')
        if email and password:
            user = self._verify_credentials(email, password)
        return user

    def _verify_credentials(self, email, password):
        try:
            resp = self.treebo_auth_client.login(email, password)
        except AuthException as e:
            logger.info("treebo auth backend credential failed %s ", email)
            return None
        except Exception as e:
            logger.info(
                "treebo auth backend exception credential failed %s ",
                email)
            return None
        try:
            user = self.user_model.objects.get(email=email)
            self._save_access_token(resp, user)
            return user
        except self.user_model.DoesNotExist:
            logger.error(
                "user %s exists in treebo auth but not with the client", email)
        return None

    def _verify_token(self, token):
        try:

            resp = self.treebo_auth_client.validate_token(token)
            try:
                # user is user_id from treebo auth service
                return self.user_model.objects.get(auth_id=resp.user)
            except self.user_model.DoesNotExist:
                logger.error(
                    "user exist in treebo auth but not with the client")
            return None
        except AuthException as e:
            logger.info("treebo auth backend _verify_token failed %s ", token)
            return None
        except Exception as e:
            logger.info(
                "treebo auth backend exception _verify_token failed %s ",
                token)
            return None

    def _save_access_token(self, token_info, user):
        expires = timezone.now() + timedelta(seconds=token_info['expires_in'])
        token, refresh_token = token_info['access_token'], token_info['refresh_token']
        token_type, scope = token_info['token_type'], token_info['scope']
        user.token = token
        AccessToken.objects.get_or_create(
            auth_user=user,
            token=str(token),
            expires=expires,
            scope=str(scope),
            refresh_token=str(refresh_token),
            token_type=str(token_type))

    def get_user(self, user_id):
        try:
            return self.user_model.objects.get(pk=user_id)
        except self.user_model.DoesNotExist:
            return None

    def logout(self, user, token):
        if not user:
            return None
        msg = 'Unable to delete access token in treebo auth server'
        try:
            resp = self.treebo_auth_client.logout(token)
        except AuthException:
            logger.error(msg, exc_info=True)
        return None
