import json

from apps.content.services.schema_builder import SchemaBuilder
from data_services.respositories_factory import RepositoriesFactory
from services.restclient.contentservicerestclient import ContentServiceClient
from data_services.hotel_repository import HotelRepository

__author__ = 'ansilkareem'


class PAGES(object):
    CITY = 'city'
    CATEGORY = 'category'
    LANDMARK = 'landmark'
    LOCALITY = 'locality'
    HD = 'hd'
    COMMON = 'common'
    CITY_AMENITY = 'city-amenity'
    LOCALITY_CATEGORY = 'locality-category'
    LANDMARK_CATEGORY = 'landmark-category'

    def __init__(self, page, query, item_name):
        self.page = page
        self.query = query
        self.item_name = item_name

        city_repository = RepositoriesFactory.get_city_repository()
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        city_count = city_repository.get_enabled_cities_count()
        #city_count = City.objects.filter(status=City.ENABLED).count()
        hotel_count = hotel_repository.get_total_hotel_count(
            only_active=True)

        self.meta_content = ContentServiceClient.getValueForKey(
            None, 'meta_content', 2)
        self.seo_pages = ContentServiceClient.getValueForKey(
            None, 'seo_pages', 1)
        self.footer_data = ContentServiceClient.getValueForKey(
            None, 'footer_data', 1)
        if 'city_image_url' in self.footer_data:
            self.city_image_url = self.footer_data['city_image_url']
        data = {'city_count': city_count, 'hotel_count': hotel_count}

        self.meta_content = json.loads(json.dumps(self.meta_content) % data)
        self.seo_pages = json.loads(json.dumps(self.seo_pages) % data)
        self.breadcrumbs = []
        self._build_schema()

    def _build_schema(self):
        schema_builder = SchemaBuilder(self.page, self.query)
        schema_builder.build()
        self.meta_content.update({'schema': schema_builder.schema})


    def override_seo_content(self, title, description, keywords=None, canonical_url=None, city_name=None, is_amp=None, amp_url=None):
        page = self.meta_content["page"]
        if title:
            page["title"] = title
        if canonical_url:
            # Making a list to add more links for different rel
            self.meta_content['link'] = [{'rel': 'canonical',
                                    'href': canonical_url}]

        if is_amp and is_amp is True and amp_url:
            self.meta_content['link'].append({'rel': 'amphtml', 'href': amp_url})
        for meta in self.meta_content["meta"]:
            if meta.get("property") == "og:title":
                if title:
                    meta["content"] = title
            elif meta.get("property") == "og:description" or meta.get("name") == "description":
                if description:
                    meta["content"] = description
            elif meta.get("name") == "keywords" and keywords:
                meta["content"] = keywords

    def update_meta(self, ):
        raise NotImplementedError
