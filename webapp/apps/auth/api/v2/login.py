import logging

from apps.auth.constants import UserContactType
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.user_login_dto import UserLoginDTO
from apps.auth.serializer.user_serializer import UserSerializer
from apps.auth.services.cookie_service import CookieService
from apps.auth.services.user_profile_service import UserProfileService
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import UPSClientException
from apps.profiles.service.user_service import UserService
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class LoginApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    user_service = UserService()
    cookie_service = CookieService()
    user_profile_service = UserProfileService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            user_dto = UserLoginDTO(data=request.data)
            if not user_dto.is_valid():
                logger.error("email login invalid request  ")
                return api_response.invalid_request_error_response(
                    user_dto.errors, LoginApi.__name__)
            email = user_dto.data['email']
            password = user_dto.data['password']
            user, token, user_details = self.user_service.validate_credentials(
                email, password)
            logger.info(" user login success %s ", email)
            self.user_service.login_user(request, user)
            user_dto = UserSerializer(instance=user)
            user_data = user_dto.data
            user_data['token'] = token
            response = api_response.prep_success_response(user_data)
            self.cookie_service.set_user_info_in_cookie(response, user)
            self.user_profile_service.create_user_and_user_profile(user_details, UserContactType.EMAIL)
        except UPSClientException as e:
            logger.error("Creating User failed due to {error}".format(error=e))
            return response
        except TreeboException as e:
            logger.exception("treebo login exception")
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception("internal error login exception ")
            response = api_response.internal_error_response(
                error_codes.get(error_codes.LOGIN_FAILED)['msg'], LoginApi.__name__)
        return response
