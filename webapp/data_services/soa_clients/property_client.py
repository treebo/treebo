import logging
import requests
from django.conf import settings

from rest_framework.status import HTTP_400_BAD_REQUEST

from data_services.exceptions import HTTPException, CsIDNotFoundError
from data_services.return_types.city_dto import CityDto
from data_services.return_types.dto import DTO
from data_services.return_types.hotel_dto import HotelDto
from data_services.return_types.room_dto import RoomDto, ExternalRoomDetailDto
from data_services.soa_clients.utils import *
from queue_listeners.cs_messages_parser import CSMessagesParser
from data_services.soa_clients.website_attributes_fetcher import WebSiteAttributesFetcher
from apps.common.slack_alert import SlackAlertService as slack_alert


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


class CatalogAPIResponsesProcessor:

    cs_response_parser = CSMessagesParser()
    website_attributes_fetcher = WebSiteAttributesFetcher()

    def process_property_response(self, property_resp_json):
        city_json = property_resp_json['location']['city']
        city_dto = self.get_city(city_json)
        state_json = property_resp_json['location']['state']
        state_dto = self.get_state(state_json)
        locality_json = property_resp_json['location']['locality']
        pincode = property_resp_json['location']['pincode']
        brands=property_resp_json['brands']
        locality_dto = self.get_locality(locality_json, city_dto, pincode=pincode)
        hotel_dto = self.get_hotel_dto_from_json(property_resp_json, city_dto, locality_dto)
        hotel_dto.brands = brands
        highlights_list = self.get_highlights_dto_list_from_json(property_resp_json['description'], hotel_dto)
        directions_list = self.get_directions_dto_list_from_json(property_resp_json['landmarks'], city_dto, hotel_dto)
        hotel_dto.directions_set = directions_list
        hotel_dto.hotel_highlights = highlights_list
        hotel_dto.state = state_dto
        return hotel_dto

    def get_city(self, city_json):
        try:
            city_data_from_cs, _ = self.cs_response_parser.parse_city_data(city_json)
            city_data_from_direct = self.website_attributes_fetcher.get_direct_specific_data_for_city(
                    city_cs_id=city_data_from_cs['cs_id'])
            city_data_from_cs.update(city_data_from_direct)
            city_dto = CityDto(city_data_from_cs)
            return city_dto
        except Exception as e:
            logger.exception(e)
            raise Exception from e

    def get_state(self, state_json):
        try:
            state_data = self.cs_response_parser.parse_state_data(state_json)
            state_data_from_direct = self.website_attributes_fetcher.\
                get_direct_specific_data_for_state(state_data['cs_id'])
            state_data.update(state_data_from_direct)
            state_dto = DTO(state_data)
            return state_dto
        except Exception as e:
            raise Exception from e

    def get_locality(self, locality_json, city_dto, pincode):
        try:
            locality_data = self.cs_response_parser.parse_locality_data(locality_json, pincode=pincode)
            direct_specific_data_for_locality = self.website_attributes_fetcher.\
                get_locality_specific_data_from_direct(locality_data, city_dto)
            locality_data.update(direct_specific_data_for_locality)
            locality_dto = DTO(locality_data)
            return locality_dto
        except Exception as e:
            logger.exception(e)
            raise Exception from e

    def get_directions_dto_list_from_json(self, landmarks_json, city_dto, hotel_dto):
        directions = []
        landmarks_data_from_cs_response, _ = self.cs_response_parser.parse_landmark_and_directions_data(landmarks_json)

        for landmark, directions_for_landmark in landmarks_data_from_cs_response:
            direct_specific_data_for_landmark = self.website_attributes_fetcher.get_landmark_specific_data_from_direct(city_dto)
            landmark.update(direct_specific_data_for_landmark)
            landmark_dto = DTO(landmark)
            direct_specific_data_for_direction = self.website_attributes_fetcher.get_direction_specific_data_from_direct(landmark_dto)
            directions_for_landmark.update(direct_specific_data_for_direction)
            directions_dto = DTO(directions_for_landmark)
            directions.append(directions_dto)

        return directions

    def get_hotel_dto_from_json(self, body, city_dto, locality_dto):
        try:
            hotel_data = self.cs_response_parser.extract_hotel_data(body)
            hotel_data_updated = self.website_attributes_fetcher.\
                update_hotel_data_with_details_from_direct(hotel_data, city_dto, locality_dto)
            hotel_dto = HotelDto(hotel_data_updated)
            return hotel_dto
        except Exception as e:
            logger.exception(e)
            raise Exception from e

    def get_highlights_dto_list_from_json(self, body, hotel_dto):
        highlights_data = self.cs_response_parser.extract_highlight_data(body)
        return [DTO(highlight_data) for highlight_data in highlights_data]

    def parse_images(self, body, hotel_cs_id):
        images_list = []
        if body:
            for image in body:
                image_data = {}
                url_data = {}
                url_data['url'] = image['path']
                url_dto = DTO(url_data)
                image_data['url'] = url_dto
                image_data['room_type'] = image['room_type'].title() if 'room_type' in image and image['room_type'] else None
                image_data['tagline'] = image['tag_description']
                image_data['position'] = image['sort_order']
                image_data['is_showcased'] = True if image['sort_order'] == 1 else False
                image_dto = DTO(image_data)
                images_list.append(image_dto)
        return images_list

    def get_property_room_config(self, room_config_details):
        room_config_data = {}
        room_config_dtos = []
        price_map = {'Oak':1000, 'Maple':1500, 'Mahogany':1800, 'Acacia':2000}
        try:
            for room_config in room_config_details:
                room_config_data['cs_id'] = room_config['id']
                try:
                    room_config_data['id'] = get_room_id_from_cs_id(room_config_data['cs_id']).id
                except Exception as e:
                    logger.error("No direct id found for room with cs id %s", room_config_data['cs_id'])
                    #room_config_data['id'] = 1  # TODO: Only temp. remove
                room_config_data['max_children'] = room_config['children'] if 'children' in room_config and room_config[
                    'children'] else 0
                # adult_count = room_config['adults'] if 'adults' in room_config and room_config['adults'] else 0
                # Max adult with children
                room_config_data['max_adult_with_children'] = room_config['max_total']

                room_config_data['max_guest_allowed'] = int(room_config['adults']) if 'adults' in room_config and  room_config['adults'] else 0
                room_config_data['room_type_code'] = room_config['room_type']['type'].title()
                room_config_data['quantity'] = room_config['room_count']
                if room_config_data['room_type_code'] == 'Oak':
                    room_config_data['price'] = 2400
                if room_config_data['room_type_code'] == 'Maple':
                    room_config_data['price'] = 3000
                room_config_data['price'] = price_map[room_config_data['room_type_code']]
                room_config_data['available'] = room_config['room_count']
                room_config_data['base_rate'] = 2400
                room_config_data['room_area'] = room_config['min_room_size']
                room_config_data['cs_id'] = room_config['id']
                occupancy_message = "%(guest_count)s Guests (Max %(adult_count)s %(adult_string)s)"
                occupancy_dict = {'guest_count': room_config_data['max_adult_with_children'],
                                  'adult_count': room_config_data['max_guest_allowed'], 'adult_string': ''}
                if room_config_data['max_guest_allowed'] > 1:
                    occupancy_dict['adult_string'] = "Adults"
                else:
                    occupancy_dict['adult_string'] = "Adult"
                room_config_data['occupancy_string'] = occupancy_message % occupancy_dict
                room_config_data['mm_hotel_code'] = ''
                room_config_data['mm_room_code'] = room_config['mm_id']
                room_config_data['description'] = ''
                if 'description' in room_config and room_config['description']:
                    room_config_data['description'] = room_config['description']
                room_config_data['external_room_details'] = self.get_external_provider_room_details(room_config)
                room_config_dto = RoomDto(room_config_data)
                room_config_dtos.append(room_config_dto)
        except Exception as e:
            logger.exception(e)
            raise Exception("Exception in parsing room config") from e
        sorted_room_configs = sorted(room_config_dtos, key=lambda room_config_dto:room_config_dto.price)
        return sorted_room_configs

    def get_external_provider_room_details(self, room_config):
        external_room_data = dict()
        external_room_data['name'] = room_config['room_type']['type'].title()
        external_room_data['rate_plan_name'] = settings.DEFAULT_RATE_PLAN
        external_room_data['rate_plan_code'] = settings.DEFAULT_RATE_PLAN
        external_room_data['room_code'] = room_config['room_type']['code']
        try:
            if room_config['display_name']:
                external_room_data['name'] = room_config['display_name']
            if room_config['ext_rate_plan_name']:
                external_room_data['rate_plan_name'] = room_config['ext_rate_plan_name']
            if room_config['ext_rate_plan_code']:
                external_room_data['rate_plan_code'] = room_config['ext_rate_plan_code']
            if room_config['ext_room_code']:
                external_room_data['room_code'] = room_config['ext_room_code']
            external_room_detail_dto = ExternalRoomDetailDto(external_room_data)
            return external_room_detail_dto
        except Exception as e:
            logger.error(str(e))
            external_room_detail_dto = ExternalRoomDetailDto(external_room_data)
            return external_room_detail_dto

    def get_amenities_list_from_json(self, amenities_json):
        result = {}
        result['property_amenities'] = []
        try:
            for amenity_key, amenities in list(amenities_json.items()):
                try:
                    amenities_list = []
                    result[amenity_key] = amenities_list
                    for amenity in amenities:
                        amenity_name = amenity['amenity_key']
                        if '_' in amenity_name:
                            amenity_name = amenity_name.replace('_', ' ').title() #To clean up amenity name
                            amenity['amenity_key'] = amenity_name
                        amenity = self.website_attributes_fetcher.\
                            get_direct_specific_attributes_for_amenity(amenity)
                        amenity = {'name': amenity['amenity_key'], 'css_class': amenity['css_class'],
                                   'url': amenity['url'], 'category': amenity['category']}
                        amenity_dto = DTO(amenity)
                        amenities_list.append(amenity_dto)
                    result[amenity_key] = amenities_list
                except Exception as e:
                    logger.exception(e)
        except Exception as e:
            logger.exception(e)

        return result


class CatalogClient:
    page_map = {'get_property': "properties/{property_id}",
                'get_property_amenities': "api/v1/properties/{property_id}/amenity-summary/",
                'get_property_room_configs': "api/v1/properties/{property_id}/room-type-configs/",
                'get_all_properties': "properties/",
                'get_property_images': "api/v1/properties/{property_id}/images/",
                'get_property_room_amenities': 'properties/{property_id}/rooms/{room_id}/amenities/'}

    def __init__(self, cs_response_processor, domain=settings.CATALOGUING_SERVICE_BASE_URL):
        self.base_url = domain
        self.cs_response_processor = cs_response_processor

    def get_property(self, cs_property_id):
        logger.debug("Fetch property for id: %s", cs_property_id)
        cs_property_response_json = self.get_property_json_from_cs(cs_property_id)
        cs_property_dto = self.cs_response_processor.process_property_response(cs_property_response_json)
        return cs_property_dto

    def get_property_json_from_cs(self, cs_property_id):
        url = self.get_url_with_path("get_property", url_parameters=dict(property_id=cs_property_id))
        property_resp_json = self.make_call(url)
        return property_resp_json

    def get_all_properties(self):
        url = self.get_url_with_path("get_all_properties", url_parameters={})
        res_json = self.make_call(url)
        cs_properties_dto = []
        for cs_property_json in res_json:
            cs_properties_dto.append(self.cs_response_processor.process_property_response(cs_property_json))
        return cs_properties_dto

    def get_property_images(self, cs_property_id):
        url = self.get_url_with_path("get_property_images", url_parameters=dict(property_id=cs_property_id))
        property_images_json = self.make_call(url)
        cs_property_images_dto = self.cs_response_processor.parse_images(property_images_json, cs_property_id)
        return cs_property_images_dto

    def get_property_room_configs_from_web_hotel_id(self, web_hotel_id):
        cs_property_id = get_cs_id_from_web_hotel_id(web_hotel_id)
        if not cs_property_id:
            raise CsIDNotFoundError(HTTP_400_BAD_REQUEST, "cs property id not found for hotel id:{}".format(web_hotel_id))
        return self.get_property_room_configs(cs_property_id)

    def get_property_room_configs(self, cs_property_id):
        logger.debug("Fetch property for id: %s", cs_property_id)
        url = self.get_url_with_path("get_property_room_configs", url_parameters=dict(property_id=cs_property_id))
        res_json = self.make_call(url)
        cs_property_rooms_dto = self.cs_response_processor.get_property_room_config(res_json)
        return cs_property_rooms_dto

    def get_facilities_from_hotel_id(self, hotel_id):
        cs_id = get_cs_id_from_web_hotel_id(hotel_id)
        if not cs_id:
            raise CsIDNotFoundError(HTTP_400_BAD_REQUEST, "cs id not found for the hotel id:{}".format(hotel_id))
        return self.get_facilities_for_hotel(cs_id)

    def get_facilities_for_hotel(self, hotel_cs_id):
        logger.debug("Fetch Facilities for id: %s", hotel_cs_id)
        res_json = self.make_call(
                self.get_url_with_path("get_property_amenities", url_parameters=dict(property_id=hotel_cs_id)))
        amenities = self.cs_response_processor.get_amenities_list_from_json(res_json)
        logger.debug("The count of amenities returned for cs id %s is %d", hotel_cs_id, len(amenities))
        return amenities

    def populate_rooms_with_images(self, rooms, hotel_images, cs_property_id):
        for room in rooms:
            room.image_set = [image for image in hotel_images if image.room_type and image.room_type == room.room_type_code]

        for image in hotel_images:
            setattr(image, 'room', DTO({'room_type_code':image.room_type}) if hasattr(image, 'room_type') else None)
            setattr(image, 'hotel' , cs_property_id if not hasattr(image, 'room_type') and not image.room_type else None)

        return rooms, hotel_images


    def get_hotel_data_from_cs(self, cs_property_id):
        try:
            logger.info("In getting hotel data for property id %s", cs_property_id)
            hotel_dto = self.get_property(cs_property_id)
            logger.info("Hotel from cs for cs id %s is %s", cs_property_id, hotel_dto.name)
            hotel_rooms = self.get_property_room_configs(cs_property_id)
            logger.debug("Rooms returned for hotel %s is", cs_property_id)
            hotel_images = self.get_property_images(cs_property_id)
            rooms_with_images, hotel_images = self.populate_rooms_with_images(hotel_rooms, hotel_images, cs_property_id)
            setattr(hotel_dto, 'image_set', hotel_images)
            setattr(hotel_dto, 'rooms', rooms_with_images)
            showcased_image_url = self.get_hotel_show_cased_image(hotel_dto)
            setattr(hotel_dto, 'showcased_image_url', showcased_image_url)
            logger.debug("Images returned for hotel %s", cs_property_id)
            return hotel_dto
        except Exception as e:
            logger.exception(e)
            slack_alert.send_slack_alert_for_exceptions(status='', request='',
                                                        class_name=self.__class__.__name__,
                                                        dev_msg='Error in fetching data from CS for cs_id ',
                                                        message=str(e))
            raise Exception("Could not get CS data for cs property id " + cs_property_id) from e

    def get_hotel_show_cased_image(self, hotel_dto):
        images_of_hotel = hotel_dto.image_set
        if images_of_hotel:
            logger.info("Found an image set of hotel %d with length %d", hotel_dto.id, len(images_of_hotel))
            show_cased_image = next((image for image in images_of_hotel if image.is_showcased), None)
            if show_cased_image:
                return show_cased_image.url
            logger.info("There is no showcased image")
            image = images_of_hotel[0]
            if image:
                return image.url
        logger.error("Could not find an image for hotel %s", hotel_dto.name)
        return ""

    def get_hotel_with_amenities(self, cs_id):
        try:
            hotel_data = self.get_hotel_data_from_cs(cs_id)
            amenities = self.get_facilities_for_hotel(cs_id)
            logger.debug("The amenities returned for cs id is %s", amenities)
            hotel_data.facility_set = amenities['property_amenities']
            for room in hotel_data.rooms:
                room.facility_set = amenities.get(room.room_type_code.upper(), [])
                logger.info("the number of facilities for room type %s of hotel %s is %d", room.room_type_code, cs_id,
                            len(room.facility_set))
            logger.debug("The hotel data returned is %s", hotel_data.toJSON())
            return hotel_data
        except Exception as e:
            logger.exception("Exception in fetching CS data, returning none")
            return None

    def make_call(self, url, data=None, call_type="GET", headers=None, url_parameters=None):
        headers = headers if headers else {'referer': "/"}
        response = {}
        try:
            logger.info("Making call to CS for url %s", url)
            response = requests.request(method=call_type, url=url, params=data, json=data, headers=headers,
                                        allow_redirects=True, timeout=0.5)

            if response.status_code != requests.codes.ok:
                logger.error("Call failed for type: {} payload : {} response status: {}".format(call_type, data,
                                                                                                response.status_code))
                slack_alert.send_slack_alert_for_third_party(status=response.status_code,
                                                             class_name=self.__class__.__name__,
                                                             url=url,
                                                             reason='CS api call failed for params {0}'.format(data))
                raise HTTPException
            res_json = response.json()
            logger.debug("The response from CS api call %s, is %s", url, res_json)
            return res_json
        except requests.Timeout as e:
            slack_alert.send_slack_alert_for_third_party(status=response.status_code,
                                                         class_name=self.__class__.__name__,
                                                         url=url,
                                                         reason='Request timeout in CS api call')
            logger.error("In trying to fetch request, timedout")
            raise Exception from e
        except Exception as e:
            logger.error(e)
            slack_alert.send_slack_alert_for_third_party(status=response.status_code,
                                                         class_name=self.__class__.__name__,
                                                         url=url,
                                                         reason='Exception in getting response from CS api call')
            raise Exception("Exception in getting response from CS") from e

    def get_url_with_path(self, page_name, url_parameters):
        return self.base_url + self.page_map[page_name].format(**url_parameters)
