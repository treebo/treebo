import logging

from django.conf import settings
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.views import APIView

from apps.checkout.api.v1.PaymentHandler import PaymentHandler
from apps.checkout.tasks import capture_razorpay_payment
from base import log_args
from base.middlewares.set_request_id import get_current_user, get_current_request_id

logger = logging.getLogger(__name__)


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class RazorPay(APIView, PaymentHandler):
    authentication_classes = (
        CsrfExemptSessionAuthentication,
        BasicAuthentication)

    @log_args(logger)
    def post(self, request):
        status, order_id, razorpay_payment_id, amount, booking_id, amount_to_send = self.__parse_request(
            request)

        logger.info('RazorPay Handler called with data: %s', request.data)
        booking_type = request.data.get('type', 'RazorPay')

        self._update_payments(
            amount,
            order_id,
            razorpay_payment_id,
            "RazorPay")
        self.__store_razorpay_details(amount_to_send, razorpay_payment_id)
        return self._confirm_booking(order_id, booking_id, request)

    def __store_razorpay_details(self, amount_to_send, razorpay_payment_id):
        razorpay_id = settings.RAZORPAY_KEYS
        razorpay_secret = settings.RAZORPAY_SECRET
        capture_razorpay_payment.delay(amount_to_send,
                                       razorpay_secret,
                                       razorpay_id,
                                       razorpay_payment_id,
                                       user_id=get_current_user(),
                                       request_id=get_current_request_id())

    def __parse_request(self, request):
        status = request.data['status']
        order_id = request.data['order_id']
        gatewayPaymentId = request.data['razorpay_payment_id']
        amount = request.data['amount_to_store']
        amount_to_send = request.data['amount_to_send']
        booking_id = request.data['booking_id']
        return status, order_id, gatewayPaymentId, amount, booking_id, amount_to_send
