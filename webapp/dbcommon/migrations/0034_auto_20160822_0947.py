# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0033_auto_20160822_0920'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hotel',
            old_name='CONSUMER_KEY',
            new_name='consumer_key',
        ),
        migrations.RenameField(
            model_name='hotel',
            old_name='CONSUMER_SECRET',
            new_name='consumer_secret',
        ),
    ]
