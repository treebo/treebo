import logging

from django.db import models
from django_postgres_extensions.models.fields import ArrayField
from datetime import date
from djutil.models import TimeStampedModel
from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.models.hotel import Hotel
from dbcommon.models.profile import User

logger = logging.getLogger(__name__)


class ConstraintKeywords(TimeStampedModel, DefaultPermissions):
    keyword = models.CharField(max_length=200, default="", null=True)
    type = models.CharField(max_length=20, default="", null=True)
    description = models.CharField(max_length=1000, default="", null=True)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Constraint Keyword'
        verbose_name_plural = 'Constraint Keywords'

    def __unicode__(self):
        return self.keyword

    def __str__(self):
        return self.keyword


class ConstraintOperators(TimeStampedModel, DefaultPermissions):
    operator = models.CharField(max_length=25, default="", null=True)
    description = models.CharField(max_length=100, default="", null=True)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Constraint Operator'
        verbose_name_plural = 'Constraint Operators'

    def __unicode__(self):
        return self.operator

    def __str__(self):
        return self.operator


class DiscountCoupon(TimeStampedModel, DefaultPermissions):
    code = models.CharField(max_length=200, default="", unique=True, db_index=True)
    FIXED = 'FIXED'
    PERCENTAGE = 'PERCENTAGE'
    NIGHT = 'NIGHT'
    FLATPERNIGHT = 'FLATPERNIGHT'
    FLATCOST = 'FLATCOST'
    FOTREDEEM = 'FOTREDEEM'
    DISCOUNT_OPERATION_CHOICES = (
        (FIXED, 'Fixed'),
        (PERCENTAGE, 'Percentage'),
        (NIGHT, 'Night'),
        (FLATPERNIGHT, 'Flatpernight'),
        (FLATCOST, 'Flatcost'),
        (FOTREDEEM, 'Fotredeem')
    )
    discount_operation = models.CharField(
        max_length=20,
        choices=DISCOUNT_OPERATION_CHOICES,
        default=PERCENTAGE)
    discount_value = models.DecimalField(
        max_digits=5, decimal_places=1, default=0)
    series_value = models.CharField(
        max_length=500, blank=True, null=True, default="")
    max_available_usages = models.IntegerField(default=0)
    max_available_discount = models.PositiveIntegerField(default=0)
    start_date = models.DateField(null=True, default=date.today)
    expiry = models.DateField(blank=True, null=True)
    is_active = models.BooleanField(default=False)
    is_visible = models.NullBooleanField(null=True, default=False)
    is_referral = models.NullBooleanField(null=True, default=False)
    is_prepaid = models.NullBooleanField(null=True, default=False)
    tagline = models.CharField(
        max_length=200,
        default="",
        null=True,
        blank=True)
    terms = models.CharField(
        max_length=5000,
        default="",
        null=True,
        blank=True)
    PRETAX = 0
    POSTTAX = 1
    STATUS_CHOICES = (
        (PRETAX, 'Pretax'),
        (POSTTAX, 'Posttax'))
    discount_application = models.IntegerField(
        default=POSTTAX, choices=STATUS_CHOICES)
    DISCOUNT = 'DISCOUNT'
    CASHBACK = 'CASHBACK'
    ROOMNIGHTS = 'ROOMNIGHTS'
    VOUCHER = 'VOUCHER'
    COUPON_TYPE_CHOICES = (
        (DISCOUNT, 'Discount'),
        (CASHBACK, 'CashBack'),
        (ROOMNIGHTS, 'RoomNights'),
        (VOUCHER, 'Voucher'),
    )
    coupon_type = models.CharField(
        max_length=200,
        choices=COUPON_TYPE_CHOICES,
        default=DISCOUNT)
    coupon_message = models.CharField(default="Discount value", max_length=500)
    is_auto_applicable = models.BooleanField(default=False)
    is_internal = models.BooleanField(default=True)
    SALE_PRICE = 'Sale Price'
    RACK_RATE = 'Rack Rate'
    PRICE_COMPONENT_APPLICABLE = (
        (SALE_PRICE, SALE_PRICE),
        (RACK_RATE, RACK_RATE),
    )
    price_component_applicable = models.CharField(
        max_length=50,
        choices=PRICE_COMPONENT_APPLICABLE,
        default=SALE_PRICE,
        null=True,
        blank=True)

    def save(self, *args, **kwargs):
        self.code = self.code.upper()
        super(DiscountCoupon, self).save(*args, **kwargs)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Discount Coupon'
        verbose_name_plural = 'Discount Coupons'

    def __unicode__(self):  # __unicode__ on Python 2
        return self.code

    def __str__(self):  # __unicode__ on Python 2
        return self.code

    def mark_used_for_transaction(self):
        self.max_available_usages -= 1
        self.save()

    @classmethod
    def valid_coupons(cls, booking_date):
        return cls.objects.filter(is_active=True, expiry__gte=booking_date,
                                  start_date__lte=booking_date,
                                  max_available_usages__gt=0)

    @classmethod
    def visible_coupons(cls, booking_date):
        return cls.valid_coupons(booking_date).filter(is_visible=True)

    def discount_amount_for_booking(self, booking_pretax_amount):
        operation_amount = booking_pretax_amount
        if self.discount_operation == DiscountCoupon.PERCENTAGE:
            discount = float(self.discount_value) / \
                100 * float(operation_amount)
        elif self.discount_operation == DiscountCoupon.FIXED:
            discount = float(self.discount_value)
        else:
            discount = 0.0
        discount = min(discount, self.max_available_discount, operation_amount)
        if str(self.coupon_type) != DiscountCoupon.DISCOUNT:
            discount = 0.0
        return discount

    def discount_percentage_for_booking(
            self, booking_pretax_amount, precision=3):
        discount = self.discount_amount_for_booking(booking_pretax_amount)
        return 100.0 * float(discount) / float(booking_pretax_amount)


class ConstraintExpression(TimeStampedModel, DefaultPermissions):
    coupon = models.ForeignKey(
        DiscountCoupon,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True)
    expression = models.CharField(max_length=2000, default="", null=True)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Constraint Expression'
        verbose_name_plural = 'Constraint Expressions'

    def __unicode__(self):
        return '%s [%s]' % (self.coupon, self.expression)

    def __str__(self):
        return '%s [%s]' % (self.coupon, self.expression)


class DiscountCouponConstraints(TimeStampedModel, DefaultPermissions):
    coupon = models.ForeignKey(
        DiscountCoupon,
        null=True,
        on_delete=models.DO_NOTHING)
    keyword = models.ForeignKey(
        ConstraintKeywords,
        null=True,
        on_delete=models.DO_NOTHING)
    operation = models.ForeignKey(
        ConstraintOperators,
        null=True,
        on_delete=models.DO_NOTHING)
    type = models.CharField(max_length=200, null=True, blank=True)
    value_one = models.CharField(max_length=600, null=True, blank=True)
    value_two = models.CharField(max_length=600, null=True, blank=True)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Discount Coupon Constraint'
        verbose_name_plural = 'Discount Coupon Constraints'

    def __unicode__(self):
        if self.value_two:
            return '%s [%s] (%s, %s)' % (
                self.keyword, self.operation, self.value_one, self.value_two)
        else:
            return '%s [%s] (%s)' % (
                self.keyword, self.operation, self.value_one)

    def __str__(self):
        if self.value_two:
            return '%s [%s] (%s, %s)' % (
                self.keyword, self.operation, self.value_one, self.value_two)
        else:
            return '%s [%s] (%s)' % (
                self.keyword, self.operation, self.value_one)


class HotelCouponMapForAutoApply(TimeStampedModel):
    hotel = models.ForeignKey(
        Hotel,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        related_name="hotelcoupons")
    coupon_code = models.CharField(max_length=200, null=False)
    is_enabled = models.BooleanField(default=False)
    start_date = models.DateField(null=False, default=date.today)
    end_date = models.DateField(null=False, default=date.today)
    user = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        related_name="userhotelcoupons")

    def __unicode__(self):
        return '%s,%s' % (self.hotel, self.coupon_code)

    def __str__(self):
        return '%s,%s' % (self.hotel, self.coupon_code)

    def save(self, *args, **kwargs):
        self.coupon_code = self.coupon_code.upper()
        super(HotelCouponMapForAutoApply, self).save(*args, **kwargs)


class HotelCouponMapForAutoApplyUploadLog(TimeStampedModel):
    status = models.CharField(max_length=20, null=False)
    error_message = models.CharField(max_length=200, default=None)
    update_attempted = models.CharField(max_length=100, null=False)


class FlashDiscount(TimeStampedModel):
    start_date = models.DateField(null=False)
    end_date = models.DateField(null=False)
    discount_percentage = models.PositiveIntegerField(null=False)
    reward_percentage = models.PositiveIntegerField(null=False)
    reward_cap = models.PositiveIntegerField(null=False)
    enabled_property_providers = ArrayField(models.CharField(max_length=50, blank=True), size=8, default=[])
    priority_level = models.BooleanField(default=False)


class FlatPricingConfig(TimeStampedModel):
    start_date = models.DateField(null=False)
    end_date = models.DateField(null=False)
    accepted_booking_channel = models.CharField(max_length=200, null=False)
    accepted_utm_source = models.CharField(max_length=200, null=False)
    enabled_property_providers = ArrayField(models.CharField(max_length=50, blank=True), size=8, default=[])


class FlatPricingRange(TimeStampedModel):
    range_start = models.PositiveIntegerField(null=False)
    range_end = models.PositiveIntegerField(null=False)
    pre_tax = models.FloatField(null=False)
    tax = models.FloatField(null=False)
    tax_slab = models.PositiveIntegerField(null=False)

