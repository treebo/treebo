import requests
from .weather_model_service import WeatherModelService
import logging
import json
import time
from django.conf import settings
from apps.common.slack_alert import SlackAlertService as slack_alert

WEATHER_API_URL = settings.WEATHER_API_URL
WEATHER_API_URL_ALT = settings.WEATHER_API_URL_ALT

logger = logging.getLogger(__name__)


class GetForecast():

    def get_forecast(self, area):
        page = area['page']
        identifier = area['identifier']
        icon_link = 'https://images.treebohotels.com/images/weather/'
        logger.info(
            'Fetching %s corresponding to %s id : %s',
            page,
            page,
            identifier.id)
        area_lat = None
        area_lng = None
        weather_model_service = WeatherModelService()
        if page == 'city' or page == 'category' or page == 'city-amenity':
            city_obj = weather_model_service.fetch_city_from_city_id(identifier.id)
            area_lat = city_obj.city_latitude
            area_lng = city_obj.city_longitude
        elif page == 'locality' or page == 'locality-category':
            locality_obj = weather_model_service.fetch_locality_from_locality_id(identifier.id)
            area_lat = locality_obj.latitude
            area_lng = locality_obj.longitude
        elif page == 'landmark' or page == 'landmark-category':
            landmark_obj = weather_model_service.fetch_landmark_from_landmark_id(identifier.id)
            area_lat = landmark_obj.latitude
            area_lng = landmark_obj.longitude
        forecast_url = WEATHER_API_URL + str(area_lat) + ',' + str(area_lng)
        response = requests.get(forecast_url)
        logger.info(
            'Trying Darksky weather api response code : %s',
            response.status_code)
        if response.status_code != 200:
            slack_alert.publish_alert(
                'Darksky weather api response code %s for page %s id %s' %
                (response.status_code, page, identifier.id), channel=slack_alert.WEATHER_ALERT)
        if response.status_code == 200:
            jsondata = json.loads(response.text)
            forecast = []
            try:
                for x in range(1, 8, 1):
                    day_wise_data = jsondata['daily']['data'][x]
                    icon = self.fetch_icon_mapping_first(day_wise_data['icon'])
                    forcast_dict = {
                        'date': time.strftime('%d %a', time.localtime(day_wise_data['time'])),
                        'summary': day_wise_data['summary'],
                        'icon': icon_link+icon,
                        'avg_temp': self.farenheit_to_celcius(
                            self.average_temprature(
                                day_wise_data.get('apparentTemperatureLow', 68),
                                day_wise_data.get('apparentTemperatureLowTime', 1),
                                day_wise_data.get('apparentTemperatureHigh', 104),
                                day_wise_data.get('apparentTemperatureHighTime', 1),
                            )
                        )
                    }
                    forecast.append(forcast_dict)
            except Exception as e:
                logger.exception(e)
            return forecast
        forecast_url = WEATHER_API_URL_ALT + 'lat=' + \
            str(area_lat) + '&lon=' + str(area_lng)
        response = requests.get(forecast_url)
        logger.info(
            'Darksky failed. Trying open weather api response code : %s',
            response.status_code)
        if response.status_code != 200:
            slack_alert.publish_alert(
                'Open weather api response code %s for page %s id %s' %
                (response.status_code, page, identifier.id), channel=slack_alert.WEATHER_ALERT)
        if response.status_code == 200:
            jsondata = json.loads(response.text)
            forecast = []
            for x in range(0, 7, 1):
                day_wise_data = jsondata['list'][x]
                icon = self.fetch_icon_mapping_second(
                    day_wise_data['weather'][0]['icon'])
                forcast_dict = {
                    'date': time.strftime(
                        '%d %a',
                        time.localtime(
                            day_wise_data['dt'])),
                    'summary': day_wise_data['weather'][0]['description'],
                    'icon': icon_link + icon,
                    'avg_temp': self.farenheit_to_celcius(
                        day_wise_data['temp']['day'])}
                forecast.append(forcast_dict)
            return forecast
        logger.info('Both weather api failed. Returning {}')
        slack_alert.publish_alert(
            'Both weather api failed for page %s id %s' %
            (page, identifier.id), channel=slack_alert.WEATHER_ALERT)
        return {}

    def farenheit_to_celcius(self, temp):
        c = (5 * (temp - 32)) / 9
        return round(c, 1)

    def average_temprature(
            self,
            min_temp,
            min_temp_time,
            max_temp,
            max_temp_time):
        total_time = min_temp_time + max_temp_time
        temp = (float)(min_temp_time * min_temp + max_temp_time * max_temp)
        temp = temp / total_time
        return round(temp, 1)

    def fetch_icon_mapping_first(self, icon):
        mapping = {
            'clear-day': 'clear_sky.png',
            'clear-night': 'clear_sky.png',
            'rain': 'rain.png',
            'snow': 'snow.png',
            'sleet': 'hail.png',
            'wind': 'partly_cloudy.png',
            'fog': 'partly_cloudy.png',
            'cloudy': 'partly_cloudy.png',
            'partly-cloudy-day': 'partly_cloudy.png',
            'partly-cloudy-night': 'partly_cloudy.png',
            'hail': 'hail.png',
            'thunderstorm': 'thunderstorm.png',
            'default': 'partly_cloudy.png',
        }
        if icon not in mapping:
            slack_alert.publish_alert(
                'No icon mapping found for dark sky icon %s returning default' %
                (icon), channel=slack_alert.WEATHER_ALERT)
            icon = 'default'
        return mapping[icon]

    def fetch_icon_mapping_second(self, icon):
        mapping = {
            '01d': 'clear_sky.png',
            '02d': 'partly_cloudy.png',
            '03d': 'partly_cloudy.png',
            '04d': 'partly_cloudy.png',
            '09d': 'shower_rain.png',
            '10d': 'rain.png',
            '11d': 'thunderstorm.png',
            '13d': 'snow.png',
            '50d': 'partly_cloudy.png',
            '01n': 'clear_sky.png',
            '02n': 'partly_cloudy.png',
            '03n': 'partly_cloudy.png',
            '04n': 'partly_cloudy.png',
            '09n': 'shower_rain.png',
            '10n': 'rain.png',
            '11n': 'thunderstorm.png',
            '13n': 'snow.png',
            '50n': 'partly_cloudy.png',
            'default': 'partly_cloudy.png',
        }
        if icon not in mapping:
            slack_alert.publish_alert(
                'No icon mapping found for open weather icon %s returning default' %
                (icon), channel=slack_alert.WEATHER_ALERT)
            icon = 'default'
        return mapping[icon]

