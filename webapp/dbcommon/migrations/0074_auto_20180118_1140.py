# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0073_auto_20180118_1137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hotel',
            name='cs_id',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
