import requests
import logging
from django.conf import settings
from apps.hotels.service.hotel_sub_brand_service import HotelSubBrandService
from apps.search.constants import INDEPENDENT_HOTEL_BRAND_CODE
from datetime import datetime, timezone

logger = logging.getLogger(__name__)


class TrivagoConversionTracking:

    def __init__(self, booking, booking_request):

        self.booking = booking
        self.trivago_ref_id = None
        self.utm_source = None
        if booking_request:
            self.trivago_ref_id = str(booking_request.utm_tracking_id)
            self.utm_source = booking_request.utm_source.lower()

        self.trivago_conversion_url = None
        if hasattr(settings, "TRIVAGO_CONVERSION_API_URL"):
            self.trivago_conversion_url = settings.TRIVAGO_CONVERSION_API_URL

        self.request_payload = {
            "trv_reference": self.trivago_ref_id,
            "advertiser_id": self.get_trivago_advertiser_id(),
            "hotel": str(self.booking.hotel_id),
            "arrival": str(self.booking.checkin_date).replace("-", ""),
            "departure": str(self.booking.checkout_date).replace("-", ""),
            "volume": float(self.booking.total_amount),
            "booking_id": str(self.booking.order_id),
            "booking_date": datetime.now().replace(tzinfo=timezone.utc).timestamp(),
            "number_of_rooms": len(self.booking.room_config.split(",")),
            "locale": "IN",
            "currency": "INR",
            "date_format": "Ymd"
        }

        self.headers = {
            "X-Trv-Ana-Key": settings.TRIVAGO_CONVERSION_API_KEY,
            "Content-Type": "application/json"
        }

    def get_trivago_advertiser_id(self):

        if INDEPENDENT_HOTEL_BRAND_CODE in HotelSubBrandService.get_brand_list_for_hotel(self.booking.hotel_id):
            return settings.TRIVAGO_SUPERHERO_ADVERTISER_ID
        else:
            return settings.TRIVAGO_TREEBO_ADVERTISER_ID


    def send_booking_confirmation_ack(self):
        logger.info("Trivago booking confirmation url %s and request json %s",
                   str(self.trivago_conversion_url), str(self.request_payload))
        response = requests.post(url=self.trivago_conversion_url, headers=self.headers,
                                 json=self.request_payload)
        logger.info("Response for trivago conversion tracking api %s", str(response.json()))

    def send_booking_cancellation_ack(self):
        self.request_payload['refund_amount'] = float(self.booking.total_amount)
        requests.delete(url=self.trivago_conversion_url, headers=self.headers,
                        json=self.request_payload)

    def is_trivago_ack_required(self):
        if self.utm_source == settings.TRIVAGO_UTM_SOURCE.lower() \
                        and self.trivago_ref_id and self.trivago_conversion_url:
            return True

        return False
