from apps.search.services.blog_search_service import BlogSearchService
from base.views.api import TreeboAPIView
from django.http import JsonResponse
from rest_framework import status
from django.core.cache import cache


class BlogSearch(TreeboAPIView):
    CACHE_KEY = "blog_search_city_{}"
    CACHE_TIMEOUT = 24 * 60 * 60

    def get(self, request):

        try:
            city = request.GET.get('city')
            cache_results = cache.get(self.CACHE_KEY.format(city), None)
            if cache_results:
                return cache_results

            data = BlogSearchService().get_hotels_with_price(city=city)
            return JsonResponse(data=data, safe=False)

        except Exception as e:
            return JsonResponse(status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                                data={"errors": e.__str__()}, safe=False)

