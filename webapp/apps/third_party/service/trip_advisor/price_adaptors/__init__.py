import abc
import logging
from apps.common.utils import round_to_nearest_integer
from apps.third_party.service.trip_advisor import constants
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.facilities import Facility

logger = logging.getLogger(__name__)


class TripAdvisorPriceAdaptor(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_price_and_availability(
            self,
            hotel_ids_list,
            checkin_date,
            checkout_date,
            room_config):
        return

    @abc.abstractmethod
    def create_room_types_data(
            self,
            room_types,
            hotel,
            hotel_price,
            checkin_date,
            checkout_date,
            room_availability_per_hotel,
            num_room_requested,
            ta_url_room_config):
        return

    def generate_rooms_amenities(self, room_id):
        facilities = constants.DEFAULT_AMENITIES
        rooms_facilities = Facility.objects.filter(rooms__id=room_id)
        for facility in rooms_facilities:
            mapped_amenity = constants.MAPPED_AMENITIES.get(
                facility.name.lower())
            if mapped_amenity:
                facilities.add(mapped_amenity)
        return list(facilities)

    def generate_discounts(self, discounts, discount_amount, base_price):
        try:
            promo_discounts = dict()
            discount_percent = round_to_nearest_integer(
                discount_amount * 100 / base_price)
            promo_discounts["is_percent"] = True
            promo_discounts["amount"] = int(discount_percent)
            promo_discounts["marketing_text"] = str(discount_percent) + "% OFF"
            promo_discounts["price"] = round(float(discount_amount), 2)
            promo_discounts["taxes"] = 0
            promo_discounts["taxes_at_checkout"] = 0
            promo_discounts["fees"] = 0
            promo_discounts["fees_at_checkout"] = 0
            promo_discounts["final_price"] = round(float(discount_amount), 2)
            discounts.append(promo_discounts)
        except Exception:
            logger.exception("Exception in creating discounts")
        return discounts
