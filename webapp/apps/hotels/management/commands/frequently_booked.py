import logging

from django.core.management.base import BaseCommand

from common.constants import common_constants as const
import json
import requests
from django.core.cache import cache
from django.conf import settings
from apps.search.services.categories.categories_model_services import CategoriesModelServices
from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository

CACHE_TIMEOUT = 4 * 24 * 60 * 60
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Fetches the frequency of booking for each hotel in last n days"

    def handle(self, *args, **options):
        logger.info(
            'Running management command for frequently booked caching service')
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotelogix_ids = hotel_repository.get_all_hotelogix_ids()
        logger.info('Number of hotels fetched : %s', len(hotelogix_ids))
        categories_model_service = CategoriesModelServices()
        frequently_booked_threshold = categories_model_service.fetch_value_for_booking_threshold()
        data = json.dumps({"hotel_ids": hotelogix_ids,
                           "number_of_days": frequently_booked_threshold})
        logger.info('data being requested for : %s', data)
        headers = {
            "Content-Type": "application/json"
        }
        response = requests.post(
            url=settings.GROWTH_FREQUENTLY_BOOKED_URL,
            data=data,
            headers=headers)
        jsondata = json.loads(response.text)
        logger.info('json data from response : %s', jsondata)
        if not jsondata['success']:
            logger.error(
                'Frequently booked api in Growth threw an error: %s',
                jsondata['error_message'])
            return
        logger.info(
            'json loaded successfully with success status as %s',
            jsondata['success'])
        bookings_in_past_n_days = jsondata['hotels']['items']
        for hotelogix_id in hotelogix_ids:
            cache_key = str(const.FREQUENTLY_BOOKED_KEY) + str(hotelogix_id)
            cache.set(
                cache_key,
                self.is_frequently_booked(
                    hotelogix_id,
                    bookings_in_past_n_days,
                    frequently_booked_threshold),
                CACHE_TIMEOUT)

    def is_frequently_booked(
            self,
            hotel_id,
            bookings_in_past_n_days,
            frequently_booked_threshold):
        logger.info('calculating is frequently booked for %s', str(hotel_id))
        if hotel_id in bookings_in_past_n_days:
            logger.info(
                'hotel id %s was present in the dict %s',
                hotel_id,
                bookings_in_past_n_days)
            num_of_booking_for_hotel_id = bookings_in_past_n_days[hotel_id]
        else:
            logger.info(
                'hotel id %s was not present in the dict %s',
                hotel_id,
                bookings_in_past_n_days)
            num_of_booking_for_hotel_id = 0
        logger.info(
            'frequency of booking for hotel %s is %s and threshold is %s',
            hotel_id,
            num_of_booking_for_hotel_id,
            frequently_booked_threshold)
        if num_of_booking_for_hotel_id >= frequently_booked_threshold:
            return True
        return False
