# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discounts', '0015_discountcoupon_is_prepaid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discountcoupon',
            name='is_prepaid',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='discountcoupon',
            name='is_referral',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='discountcoupon',
            name='is_visible',
            field=models.NullBooleanField(default=False),
        ),
    ]
