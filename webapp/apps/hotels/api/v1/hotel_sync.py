import logging
from rest_framework.response import Response

from base.views.api import TreeboAPIView
from dbcommon.event_publishers import publish_hotel
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class HotelSyncTrigger(TreeboAPIView):

    def get(self, request, *args, **kwargs):
        hotel_id = kwargs.get('hotel_id')
        logger.info("Hotel Sync request received for Hotel ID: %s", hotel_id)
        try:
            hotel = Hotel.objects.get(id=hotel_id)
            publish_hotel(hotel)
            return Response({"message": "Hotel sync initiated."}, status=202)
        except BaseException:
            logger.exception("Hotel Sync Failed for hotel_id: %s", hotel_id)
            return Response(
                {"message": "Invalid Hotel ID provided."}, status=400)
