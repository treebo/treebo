from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from djutil.models import TimeStampedModel
from dbcommon.models.location import City

from dbcommon.models.profile import User
from django.db.models.signals import post_save
from django.core.cache import cache


class Question(TimeStampedModel):
    NOT_PROCESSED = 'NOT_PROCESSED'
    APPROVED = 'APPROVED'
    REJECTED = 'REJECTED'
    STATUS_CHOICES = (
        (NOT_PROCESSED, 'NOT_PROCESSED'),
        (APPROVED, 'APPROVED'),
        (REJECTED, 'REJECTED'),
    )
    question_data = models.TextField(null=True, blank=True)
    user = models.TextField(null=True, blank=True)
    state = models.CharField(
        default=NOT_PROCESSED,
        choices=STATUS_CHOICES,
        max_length=100)
    status = models.BooleanField(default=False)
    submitted_by_user = models.BooleanField(default=False)
    page = models.CharField(null=True, blank=True, max_length=5000)

    def __str__(self):
        return str(self.question_data.encode('utf-8'))

    def __str__(self):  # __unicode__ on Python 2
        return str(self.question_data)


class Answer(TimeStampedModel):
    NOT_PROCESSED = 'NOT_PROCESSED'
    APPROVED = 'APPROVED'
    REJECTED = 'REJECTED'
    STATUS_CHOICES = (
        (NOT_PROCESSED, 'NOT_PROCESSED'),
        (APPROVED, 'APPROVED'),
        (REJECTED, 'REJECTED'),
    )
    answer_data = models.TextField(null=True, blank=True)
    user = models.TextField(null=True, blank=True)
    state = models.CharField(
        default=NOT_PROCESSED,
        choices=STATUS_CHOICES,
        max_length=100)
    related_question_id = models.IntegerField()
    # this identifies wether the answering is done by treebo or not.
    verified = models.BooleanField(default=False)

    def __str__(self):
        return str(self.answer_data.encode('utf-8'))

    def __str__(self):  # __unicode__ on Python 2
        return str(self.answer_data)


class QAMapping(TimeStampedModel):
    CITY = 'CITY'
    LANDMARK = 'LANDMARK'
    CATEGORY = 'CATEGORY'
    LOCALITY = 'LOCALITY'
    HD = 'HD'
    CITY_AMENITY = 'CITY-AMENITY'
    LOCALITY_CATEGORY = 'LOCALITY-CATEGORY'
    LANDMARK_CATEGORY = 'LANDMARK-CATEGORY'
    PAGE_CHOICES = (
        ('', ''),
        (CITY, 'CITY'),
        (LANDMARK, 'LANDMARK'),
        (CATEGORY, 'CATEGORY'),
        (LOCALITY, 'LOCALITY'),
        (HD, 'HD'),
        (CITY_AMENITY, 'CITY-AMENITY'),
        (LOCALITY_CATEGORY, 'LOCALITY-CATEGORY'),
        (LANDMARK_CATEGORY, 'LANDMARK-CATEGORY')
    )
    question_id = models.IntegerField()
    status = models.BooleanField(default=False)
    priority = models.DecimalField(
        validators=[
            MinValueValidator(0),
            MaxValueValidator(1)],
        decimal_places=5,
        max_digits=6)
    page_type = models.CharField(choices=PAGE_CHOICES, max_length=100)
    city = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
        null=True,
        blank=True)
    page_type_id = models.CharField(max_length=50, null=True, blank=True)


class QAToggle(TimeStampedModel):
    enable_qa = models.BooleanField(default=True)
    enable_for_all_pages = models.BooleanField(default=False)
    count_of_questions = models.PositiveIntegerField(default=0)


def clear_cache(sender, instance, **kwargs):
    cache.delete_pattern("meta_*")


post_save.connect(clear_cache, sender=QAMapping)
