# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0004_booking_cancellation_datetime'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('coupon_code', models.CharField(max_length=200, null=True, blank=True)),
                ('pay_at_hotel', models.CharField(max_length=10, null=True, blank=True)),
                ('booking_id', models.PositiveIntegerField(null=True)),
                ('name', models.CharField(max_length=200, null=True, blank=True)),
                ('email', models.EmailField(max_length=200)),
                ('phone', models.EmailField(max_length=200)),
                ('checkin_time', models.DateField()),
                ('checkout_time', models.DateField()),
                ('comments', models.TextField(default='')),
                ('adults', models.PositiveIntegerField()),
                ('children', models.PositiveIntegerField()),
                ('room_required', models.PositiveIntegerField()),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
