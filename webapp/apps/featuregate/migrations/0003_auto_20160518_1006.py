# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('featuregate', '0002_auto_20160227_0951'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='constraintexpression',
            options={'verbose_name': 'Constraint Expression',
                     'verbose_name_plural': 'Constraint Expressions'},
        ),
        migrations.AlterModelOptions(
            name='constraintkeywords',
            options={'verbose_name': 'Constraint Keyword',
                     'verbose_name_plural': 'Constraint Keywords'},
        ),
        migrations.AlterModelOptions(
            name='constraintoperators',
            options={'verbose_name': 'Constraint Operator',
                     'verbose_name_plural': 'Constraint Operators'},
        ),
        migrations.AlterModelOptions(
            name='constraints',
            options={'verbose_name': 'Constraints', 'verbose_name_plural': 'Constraints'},
        ),
        migrations.AlterModelOptions(
            name='pagevariables',
            options={'verbose_name': 'Page Variable', 'verbose_name_plural': 'Page Variables'},
        ),
        migrations.AlterModelOptions(
            name='variablecondition',
            options={'verbose_name': 'Variable Condition',
                     'verbose_name_plural': 'Variable Conditions'},
        ),
    ]
