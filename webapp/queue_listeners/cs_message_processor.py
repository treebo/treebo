import logging

import requests
from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from psycopg2 import InterfaceError

from apps.hotels import tasks
from apps.search.constants import INDEPENDENT_HOTEL_BRAND_CODE
from apps.third_party.service.google_hotel_ads.transaction_pricing_request import \
    TransactionRequestService
from base.db_decorator import db_retry_decorator, db_failure_retry
from common.constants.common_constants import SUPERSAVER
from dbcommon.data_services import models_updater
from dbcommon.data_services.external_provider_room_service import ExternalProvideRoomService
from dbcommon.data_services.room_service import RoomService
from dbcommon.dto.room_dto import RoomDto, ExternalProviderRoomDetailsDto
from dbcommon.models.facilities import FacilityCategory, Facility
from dbcommon.models.hotel import HotelAlias, HotelAliasCounter
from dbcommon.models.hotel import CSHotelBrands, HotelBrandMapping
from dbcommon.models.hotel import Hotel, HotelMetaData
from dbcommon.models.miscellanous import Image
from dbcommon.models.room import Room
from queue_listeners.cs_messages_parser import CSMessagesParser
from apps.common.slack_alert import SlackAlertService

logger = logging.getLogger(__name__)


class CSMessagesProcessor:
    cs_messages_parser = CSMessagesParser()

    def process_property_message(self, cs_property_json, cs_property_id):
        print("messages %s ", cs_property_json)
        try:
            logger.info("Inside store_cs_property")
            state_json = self.cs_messages_parser.get_state_json_from_message(cs_property_json)
            city_json = self.cs_messages_parser.get_city_json_from_message(cs_property_json)
            locality_json = self.cs_messages_parser.get_locality_json_from_message(cs_property_json)
            pincode = self.cs_messages_parser.get_pincode_from_cs_message(cs_property_json)
            state_obj = self.process_state_json(state_json)
            city_obj = self.process_city_json(city_json, state_obj.id)
            locality_obj = self.process_locality_json(locality_json, city_obj.id, pincode)
            hotel_obj, created = self.process_hotel_json(cs_property_json, cs_property_id,
                                                         city_obj.id, locality_obj.id,
                                                         state_obj.id,
                                                         landmark_str='')  # TODO: Update this value
            hotel_obj.landmark = ''
            try:
                self.process_cs_hotel_sub_brands(cs_property_json, hotel_obj)
                brand_list = cs_property_json.get("brands", None)
                if brand_list:
                    self.add_independent_hotel_alias(hotel_obj, locality_obj, brand_list)
            except:
                pass
            self.delete_hotel_details_from_cache(hotel_obj.id)
            hotel_obj.save()
            self.store_highlights_info(cs_property_json, hotel_obj.id)
            logger.info("Successfully cached hotel data.. processing dependants if available")
            try:
                self.process_dependents_of_hotel(cs_property_id, cs_property_json)
            except:
                pass
            if created:
                self.__upload_to_external_systems(hotel_obj, city_obj)
            return True, 'Message Processed'
        except InterfaceError as e:
            logger.exception(e.__str__)
            raise e
        except Exception as e:
            logger.exception("Error %s in storing cs message" % e.__str__())
            # SlackAlertService.send_cs_error_msg_to_slack(cs_id=cs_property_id,
            #                                              error_msg="Error %s in parsing cs message"
            #                                                        % e.__str__(),
            #                                              class_name=self.__class__.__name__)
            error_msg = "Error %s in processing cs message for property %s" % (
            e.__str__(), cs_property_id)
            return False, error_msg

    @db_failure_retry(retry=3)
    def process_dependents_of_hotel(self, cs_property_id, cs_property_json):
        try:
            rooms_msg_json = self.cs_messages_parser.get_room_data_from_json(cs_property_json)
            try:
                for room_msg_json in rooms_msg_json:
                    self.process_room_message(room_msg_json, cs_property_id)
            except:
                pass
            logger.info("Room in the message has been processed")
            amenity_msg_json = self.cs_messages_parser.get_amenities_data_from_json(
                cs_property_json)
            self.process_amenities(amenity_msg_json, cs_property_id)
            logger.info("Amenities in the message processed")
            images_json = self.cs_messages_parser.get_images_data_from_json(cs_property_json)
            for image_json in images_json:
                self.process_image_json(image_json, cs_property_id)
            logger.info("Images in the message have been processed")
        except Exception as e:
            logger.exception("Error in %s in processing dependants of hotel for %s", e.__str__(),
                             cs_property_id)
            return

    @db_failure_retry(retry=3)
    def process_room_message(self, room_info_from_cs, cs_property_id):
        if not room_info_from_cs:
            logger.info("No msg for room json.. returning")
            return False, 'Room message empty'
        try:
            try:
                hotel_obj = Hotel.all_hotel_objects.get(cs_id=str(cs_property_id))
            except Hotel.DoesNotExist:
                logger.info("Room data not consumed. Hotel does not exist")
                return False, 'Hotel does not exist'
            logger.info("The hotel is %s", hotel_obj)
            external_provider_room_details = self.cs_messages_parser.parse_external_room_details(
                room_info_from_cs)
            room_data_from_cs = self.cs_messages_parser.parse_room_data(room_info_from_cs)
            external_provider_room_details_dto = ExternalProviderRoomDetailsDto(
                external_provider_room_details)
            room_dto = RoomDto(room_data_from_cs)
            external_room_info_updater = ExternalProvideRoomService(room_dto,
                                                                    external_provider_room_details_dto)
            external_room_obj = external_room_info_updater.create_or_update_external_provider_room_details()
            room_data_from_cs['hotel'] = hotel_obj
            room_data_from_cs['external_room_details'] = external_room_obj
            updated_room_dto = RoomDto(room_data_from_cs)
            room_info_updater = RoomService(updated_room_dto)
            return room_info_updater.create_or_update_room()
        except Exception as e:
            logger.exception("Error %s in processing room data" % e.__str__())
            return False, str(e)

    @db_failure_retry(retry=3)
    def process_state_json(self, state_json):
        try:
            state_data = self.cs_messages_parser.parse_state_data(state_json)
            state_obj = models_updater.update_state(params=state_data)
            logger.info("Saved state info with id %d", state_obj.id)
            return state_obj
        except Exception as e:
            logger.exception(e.__str__())
            raise e

    @db_failure_retry(retry=3)
    def process_city_json(self, city_json, db_state_id):
        try:
            logger.info("Processing city json for state %s", str(db_state_id))
            city_data, city_aliases_data = self.cs_messages_parser.parse_city_data(city_json)
            city_data['state_id'] = db_state_id
            city_obj = models_updater.update_city(city_data)
            try:
                for city_alias in city_aliases_data:
                    city_alias_obj = models_updater.update_city_alias(city_alias)
            except Exception as e:
                logger.exception(e)
            return city_obj
        except Exception as e:
            logger.exception(e)
            raise e

    @db_failure_retry(retry=3)
    def process_locality_json(self, locality_json, city_id, pincode=1):
        try:
            locality_data = self.cs_messages_parser.parse_locality_data(locality_json, pincode)
            locality_data['city_id'] = city_id
            locality_obj = models_updater.update_locality(locality_data)
            return locality_obj
        except Exception as e:
            logger.exception(e)
            raise e

    @db_failure_retry(retry=3)
    def process_landmark_json(self, hotel_landmarks_json, city_id, hotel_id):
        logger.info("In updating landmarks...")
        landmarks = []
        landmarks_and_directions, landmark_str = self.cs_messages_parser.parse_landmark_and_directions_data(
            hotel_landmarks_json)
        try:
            for landmark_data, directions_data in landmarks_and_directions:
                landmark_data['city_id'] = city_id
                landmark_obj = models_updater.update_landmark(landmark_data)
                directions_data['landmark_obj_id'] = landmark_obj.id
                directions_data['hotel_id'] = hotel_id
                directions_obj = models_updater.update_hotel_landmark_directions(directions_data)
                landmarks.append(landmark_obj)
                logger.info("Landmark updated %d", landmark_obj.id)
            return landmarks, landmark_str
        except Exception as e:
            logger.exception(e)
            raise

    @db_failure_retry(retry=3)
    def store_search_landmark_info(self, hotel_landmarks_json, city_id, hotel_id):
        search_landmarks = []
        landmark_str = ''
        try:
            hotel_search_landmarks_info = self.cs_messages_parser.parse_search_landmark_data(
                hotel_landmarks_json)
            for search_landmark_info, direction_info in hotel_search_landmarks_info:
                search_landmark_info['city_id'] = city_id
                search_landmark_obj = models_updater.update_search_landmark(search_landmark_info)
                direction_info['landmark_obj_id'] = search_landmark_obj.id
                direction_info['hotel_id'] = hotel_id
                directions_obj = models_updater.update_hotel_search_landmark_directions(
                    direction_info)
                search_landmarks.append(search_landmark_info)
        except Exception as e:
            logger.exception(e)
        return search_landmarks, landmark_str

    @db_failure_retry(retry=3)
    def process_hotel_json(self, cs_property_json, cs_property_id, city_id, locality_id, state_id,
                           landmark_str):
        try:
            # Check if hotel exists, if not create:
            hotel_data = self.cs_messages_parser.extract_hotel_data(cs_property_json,
                                                                    cs_property_id)
            logger.info("Parsed hotel data: %s" % str(hotel_data))
            hotel_data['landmark'] = landmark_str
            hotel_data['city_id'] = city_id
            hotel_data['state_id'] = state_id
            hotel_data['locality_id'] = locality_id
            hotel_obj, created = models_updater.update_hotel(hotel_data)
            logger.info("Hotel %s creation state %s", cs_property_id, str(created))
            logger.info("Hotel saved")
            logger.info(hotel_obj.name)
            return hotel_obj, created
        except Exception as e:
            logger.exception("Error %s " % e.__str__())
            raise Exception

    @db_failure_retry(retry=3)
    def store_highlights_info(self, cs_property_json, hotel_id):
        try:
            highlights_info = self.cs_messages_parser.extract_highlight_data(
                cs_property_json['description'])
            for highlight_info in highlights_info:
                highlight_info['hotel_id'] = hotel_id
                highlight = models_updater.update_highlight(highlight_info)
        except Exception as e:
            e = "Failed to update Hotel Highlights"
            logger.exception(e)

    def __upload_to_external_systems(self, hotel, city):
        # self.upload_and_email_pos(hotel)
        try:
            # self.upload_and_email_transactions(hotel)
            self.upload_to_prowl(hotel, city)
            self.upload_to_growth(hotel)
        except Exception as e:
            logger.exception(e)

    def upload_to_growth(self, hotel):
        try:
            logger.info("Uploading Hotel in Growth with id %s and hotelogix-id %s", hotel.id,
                        hotel.hotelogix_id)
            url = settings.GROWTH_ADD_HOTEL_URL + str(hotel.hotelogix_id)
            response = requests.get(url)
        except Exception:
            logger.exception(
                "Exception in adding hotel in growth for hotel id %s and hotelogix_id %s", hotel.id,
                hotel.hotelogix_id)

    def upload_and_email_transactions(self, hotel):
        try:
            logger.info(
                "sending request and response of transaction messages for GHA for hotel id %s and hotelogix_id %s",
                hotel.id, hotel.hotelogix_id)
            transaction_request = TransactionRequestService()
            data = transaction_request.generate_transaction_request(hotel.id)
            logger.info("request.text %s, type %s", data, type(data))
            url = settings.TOOLS_TRANSACTION_URL
            headers = {"Content-Type": "application/xml"}
            response = requests.post(url=url, data=open('transaction_request.xml').read(),
                                     headers=headers)
            logger.info("response.text %s", response.text)
            if settings.FEATURE_POS_MAIL:
                mail = EmailMessage(settings.TRANSACTION_SUBJECT % (hotel.name, hotel.id),
                                    settings.TRANSACTION_EMAILBODY, settings.SERVER_EMAIL,
                                    settings.POS_EMAIL)
                mail.attach_file('transaction_message.xml')
                mail.attach_file('transaction_request.xml')
                mail.send()
        except Exception:
            logger.exception(
                "Sending Transaction Message Failed for hotel id %s and hotelogix_id %s", hotel.id,
                hotel.hotelogix_id)
            mail = EmailMessage(settings.TRANSACTION_SUBJECT % (hotel.name, hotel.id),
                                "Transaction Generation has failed.. Kindly generate manually for attached request file",
                                settings.SERVER_EMAIL, settings.POS_EMAIL)
            mail.attach_file('transaction_request.xml')
            mail.send()

    def upload_and_email_pos(self, hotel):
        try:
            logger.info("Sending updated POS to Email for GHA for hotel id %s and hotelogix_id %s",
                        hotel.id,
                        hotel.hotelogix_id)
            url = settings.TOOLS_POS_URL
            response = requests.get(url)
            if settings.FEATURE_POS_MAIL:
                mail = EmailMessage(settings.POS_SUBJECT % (hotel.name, hotel.id),
                                    settings.POS_EMAILBODY,
                                    settings.SERVER_EMAIL, settings.POS_EMAIL)
                mail.attach_file('point_of_sale.xml')
                mail.send()
        except Exception:
            logger.exception(
                "Point of Sale File Uploading Failed for hotel id %s and hotelogix_id %s", hotel.id,
                hotel.hotelogix_id)

    def upload_to_prowl(self, hotel, city):
        logger.info("Uploading to prowl with hotel name %s and hotel id %s", hotel.name,
                    hotel.hotelogix_id)
        try:
            url = settings.PROWL_HOTEL_UPLOAD
            logger.info("Upload to Prowl.. URL: %s", url)
            response = requests.post(url, data={"hotel_id": hotel.hotelogix_id, "name": hotel.name,
                                                "city": city.name,
                                                "latitude": hotel.latitude,
                                                "longitude": hotel.longitude})
            logger.info("hotel uploaded on prowl with hotel name %s and hotel id %s", hotel.name,
                        hotel.hotelogix_id)
            logger.info("Response status: %s", response.status_code)
        except Exception:
            logger.exception("Upload to prowl failed for hotel id %s and hotelogix_id %s", hotel.id,
                             hotel.hotelogix_id)

    def process_amenities(self, amenities_json, cs_property_id):
        if not amenities_json:
            logger.info("No amenities json.. returning")
            return False, 'No Amenities JSON'
        try:
            hotel = Hotel.all_hotel_objects.get(cs_id=cs_property_id)
            logger.info("The hotel being updated is %s", hotel)
            amenities_rejected = ''
            if isinstance(amenities_json, str):
                import json
                amenities_json = json.loads(amenities_json)
            for key, facilities_list in list(amenities_json.items()):
                for property_amenity in facilities_list:
                    try:
                        amenity_key = property_amenity['amenity_key']
                        category = property_amenity['category']
                        if not category:
                            amenities_rejected = amenities_rejected + ' , ' + amenity_key
                            continue
                        if '_' in amenity_key:
                            amenity_key = amenity_key.replace('_', ' ').title()
                        facility_category = FacilityCategory.objects.filter(
                            name__iexact=category).first()
                        if not facility_category:
                            facility_category = FacilityCategory()
                        facility_category.name = category
                        facility_category.status = True
                        facility_category.save()
                        try:
                            facility = Facility.objects.get(name__iexact=amenity_key)
                        except Exception as e:
                            logger.info("Get call failed for amenity key %s", amenity_key)
                            logger.error(e)
                            facility = Facility()
                            facility.catalog_mapped_name = amenity_key

                        facility.name = amenity_key.title()
                        facility.url = ''
                        facility.save()
                        facility.hotels.add(hotel)
                        facility.categories.clear()
                        facility.categories.add(facility_category)
                        facility.save()
                        logger.info("Facility saved with key %s", amenity_key)
                        if key.lower() != 'property_amenities':
                            try:
                                room_type_code = key.lower().title()
                                room = Room.objects.get(hotel=hotel, room_type_code=room_type_code)
                                facility.rooms.add(room)
                                logger.info("Facility %s added to room %s", amenity_key,
                                            room_type_code)
                                facility.save()
                            except Exception as e:
                                logger.info("Error in saving amenity with key %s for room %s",
                                            amenity_key,
                                            room_type_code)
                                logger.error(e)
                        logger.info("Facility saved with key %s for all rooms", amenity_key)
                    except Exception as e:
                        logger.info("Error in saving amenity for room %s", key)
                        logger.error(e)
            if not amenities_rejected:
                return True, 'Message saved'
            return True, 'Amenities rejected - ' + amenities_rejected
        except InterfaceError as e:
            logger.exception(e)
            raise e
        except Exception as e:
            logger.exception(e)
            return False, str(e)

    def get_amenities_from_body(self, body):
        import json
        if 'data' not in body or not body['data']:
            return {}
        amenities_from_message = body['data']
        if isinstance(amenities_from_message, str):
            return json.loads(amenities_from_message)
        elif isinstance(amenities_from_message, dict):
            return amenities_from_message

    def get_image_from_body(self, body):
        import json
        image_from_message = body
        if isinstance(image_from_message, str):
            return json.loads(image_from_message)
        elif isinstance(image_from_message, dict):
            return image_from_message

    @db_retry_decorator()
    def process_image_json(self, image_json, cs_property_id):
        if not image_json:
            logger.info("No image json.. returning")
            return False, 'No Image Json'
        try:
            logger.info("In storing image")
            from data_services.respositories_factory import RepositoriesFactory
            image_data_service = RepositoriesFactory.get_image_repository()
            hotel_data_service = RepositoriesFactory.get_hotel_repository()
            logger.info("The cs property id is - %s", str(cs_property_id))
            hotels = hotel_data_service.filter_hotels(cs_id=cs_property_id)
            logger.info("The hotels are - %s", str(hotels))
            hotel = hotels[0]
            logger.info("The hotel id is %d", hotel.id)

            image_path = image_json['path']

            images = image_data_service.get_images_for_hotels(hotels=[hotel], url=image_path)

            if images:
                image = images[0]
            else:
                logger.info("Image was none, so creating one")
                image = Image()
                image.is_showcased = False
                image.position = image_json['sort_order']

            image.url = image_path
            image.tagline = hotel.tagline
            image.hotel_id = hotel.id
            # Get room now
            room_type = image_json['room_type'] if 'room_type' in image_json else None
            if room_type:
                room_type_code = room_type.lower().title()
                rooms = Room.objects.filter(hotel=hotel, room_type_code=room_type_code)
                if rooms:
                    room = rooms[0]
                    image.room_id = room.id
                    image.category = 'Room'
            image.save()
            logger.info(
                "Successfully saved image for property with id %s, at sort order %d with id %d",
                cs_property_id, image_json['sort_order'], image.id)
            return True, 'Message saved'
        except InterfaceError as e:
            logger.exception(e)
            raise e
        except Exception as e:
            logger.exception(e)
            return False, str(e)

    def process_cs_hotel_sub_brands(self, cs_json_brands, hotel_obj):
        """
        To map the sub_brands from catalog to each hotel id
        If sub brand is not present then create it else check for mapping
        For hotel brand mapping remove the unused brands and add the new brands if required else do nothing
        :param cs_json_brands:
        :param hotel_obj:
        :return:
        """
        try:
            brands_list = cs_json_brands.get("brands", None)
            brands_objects_codes = []
            if not brands_list:
                logger.error(
                    "Property doesn't have any brand types %s %s %s",
                    hotel_obj.id,
                    hotel_obj.name,
                    hotel_obj.cs_id)
                raise Exception
            brand_list_codes = [brand.get('code') for brand in brands_list if
                                brand.get('status') == 'ACTIVE']
            brands_objects = CSHotelBrands.objects.filter(
                code__in=brand_list_codes).all()
            if brands_objects:
                brands_objects_codes = []
                brands_objects_codes_values = brands_objects.values('code')
                for brands_objects_codes_value in brands_objects_codes_values:
                    brands_objects_codes.append(brands_objects_codes_value.get('code'))

            if brand_list_codes != brands_objects_codes:
                for brand_dict in brands_list:
                    if brand_dict.get('code') not in brands_objects_codes:
                        self.add_brands_in_db(brand_dict)
                    logger.info("Added new brand dict %s", str(brand_dict))
                brands_objects = CSHotelBrands.objects.filter(
                    code__in=brand_list_codes).all()

            self.process_hotel_csbrands_mapping(brands_objects, hotel_obj)
            return
        except Exception as err:
            logger.exception(
                "Couldn't process cs_hotel_sub_brands for %s  %s", str(err), hotel_obj.cs_id)

    def add_brands_in_db(self, brand):
        """
        To add brands in db if not existing
        :param brand_list:
        :return:
        """
        try:
            brand_object = CSHotelBrands(
                code=brand.get('code'),
                color=brand.get('color'),
                display_name=brand.get('display_name'),
                legal_name=brand.get('legal_name'),
                logo=brand.get('logo'),
                name=brand.get('name'),
                short_description=brand.get('short_description'),
                status=brand.get('status')
            )
            brand_object.save()
        except Exception as e:
            logger.exception("Couldn't create brand entries")

    def process_hotel_csbrands_mapping(self, brands_objects, hotel_obj):
        """
        To process hotel and brand mapping
        :param brands_objects:
        :param hotel_obj:
        :return:
        """
        brand_mapped_objects = HotelBrandMapping.objects.filter(
            hotel_id=hotel_obj.id).all()
        brand_mapped_objects_ids = []
        if brand_mapped_objects:
            for brand_mapped_object in brand_mapped_objects:
                brand_mapped_objects_ids.append(brand_mapped_object.cs_brand_id)

        brands_objects_ids_list = brands_objects.values('id')
        brands_objects_ids = []
        for brands_objects_id in brands_objects_ids_list:
            brands_objects_ids.append(brands_objects_id.get('id'))

        if brand_mapped_objects_ids == brands_objects_ids:
            return

        # To Create a new entity if not mapped
        for brand_object in brands_objects:
            try:
                if brand_object.id not in brand_mapped_objects_ids:
                    hotel_mapping_object = HotelBrandMapping(
                        hotel_id=hotel_obj,
                        cs_brand=brand_object)
                    hotel_mapping_object.save()
                    if brand_object.code == INDEPENDENT_HOTEL_BRAND_CODE:
                        self.blacklist_independent_hotels_on_b2b(hotel_obj)
            except Exception as err:
                logger.exception("Couldn't create new brand object in db %s %s",
                                 brand_object.__str__(), err.__str__())

        # To remove the existing entity mapped if not needed
        for brand_mapped_objects_id in brand_mapped_objects_ids:
            try:
                if brand_mapped_objects_id not in brands_objects_ids:
                    hotel_mapping_object = HotelBrandMapping.objects.filter(
                        hotel_id=hotel_obj, cs_brand_id=brand_mapped_objects_id).all()
                    hotel_mapping_object.delete()
            except Exception as err:
                logger.exception("Couldn't delete object from db %s %s",
                                 brand_mapped_objects_id.__str__(),
                                 err.__str__())

    def delete_hotel_details_from_cache(self, hotel_id):
        try:
            key1 = 'cs_apicache_hotel_detail_' + str(hotel_id)
            key2 = 'cs_apicache_hotel_detail_v3_' + str(hotel_id)
            value1 = cache.get(key1)
            value2 = cache.get(key2)
            if value1:
                cache.delete(key1)
            if value2:
                cache.delete(key2)
            return
        except Exception as err:
            logger.exception("Cache clear failed %s, %s", hotel_id, err.__str__())
            return

    def blacklist_independent_hotels_on_b2b(self, hotel_obj):
        try:
            hotel_meta_data_object = HotelMetaData.objects.get(
                hotel_id=hotel_obj.id)
            setattr(hotel_meta_data_object, 'key', HotelMetaData.Keys.B2B_BLACKLIST)
            setattr(hotel_meta_data_object, 'value', '1')
        except ObjectDoesNotExist:
            hotel_meta_data_object = HotelMetaData(hotel=hotel_obj,
                                                   key=HotelMetaData.Keys.B2B_BLACKLIST,
                                                   value='1')
        hotel_meta_data_object.save()

    @staticmethod
    def add_independent_hotel_alias(hotel_obj, locality_obj, brands_list):

        brand_list_codes = [brand.get('code') for brand in brands_list if
                            brand.get('status') == 'ACTIVE']
        if 'independent' in brand_list_codes:
            try:
                HotelAlias.objects.get(hotel=hotel_obj)
            except HotelAlias.DoesNotExist:
                hotel_alias = HotelAlias()
                hotel_alias.hotel = hotel_obj
                locality_name = locality_obj.name.replace(',',
                                                          '') if locality_obj and locality_obj.name else ""

                hotel_alias_counter = HotelAliasCounter.objects.filter(
                    locality=locality_name).first()
                if hotel_alias_counter:
                    hotel_alias_counter.counter = hotel_alias_counter.counter + 1
                else:
                    hotel_alias_counter = HotelAliasCounter()
                    hotel_alias_counter.locality = locality_name
                    hotel_alias_counter.counter = 0
                hotel_alias_counter.save()

                alias_name = "{0} {1}".format(SUPERSAVER, locality_name)
                if hotel_alias_counter.counter:
                    alias_name = "{0} {1}".format(alias_name, str(hotel_alias_counter.counter))

                hotel_alias.name = alias_name
                hotel_alias.save()
