# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0060_wificommonconfig_wifilocationmap'),
        ('dbcommon', '0061_facility_catalog_mapped_name'),
    ]

    operations = [
    ]
