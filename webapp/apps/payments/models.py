from django.db import models
from djutil.models import TimeStampedModel
from dbcommon.models.default_permissions import DefaultPermissions


class PaymentMethod(TimeStampedModel, DefaultPermissions):
    NETBANKING = 'netbanking'
    CREDIT_CARD = 'credit_card'
    DEBIT_CARD = 'debit_card'
    WALLET = 'wallet'
    UPI = 'upi'
    EMI = 'emi'

    CATEGORY_CHOICES = (
        (NETBANKING, NETBANKING),
        (CREDIT_CARD, CREDIT_CARD),
        (DEBIT_CARD, DEBIT_CARD),
        (WALLET, WALLET),
        (UPI, UPI),
        (EMI, EMI)
    )

    name = models.CharField(max_length=200)
    category = models.CharField(max_length=50, choices=CATEGORY_CHOICES)
    image_url = models.CharField(max_length=500)
    gateway_code = models.CharField(max_length=50)
    ranking = models.PositiveIntegerField()
    utm_source = models.CharField(max_length=50)

    def __str__(self):
        return '%s %s' % (self.name, self.category)


class PaymentOffer(TimeStampedModel, DefaultPermissions):
    CASHBACK = 'cashback'
    INSTANT_DISCOUNT = 'instant_discount'
    DISCOUNT_COUPON = 'discount_coupon'
    CREDIT_MODE_CHOICES = (
        (CASHBACK, CASHBACK),
        (INSTANT_DISCOUNT, INSTANT_DISCOUNT),
        (DISCOUNT_COUPON, DISCOUNT_COUPON),
    )
    MSITE = 'msite'
    WEBSITE = 'website'
    APP = 'app'
    GDC = 'gdc'
    ANDROID = 'android'
    IOS = 'ios'
    BOOKING_CHANNEL = (
        (MSITE, MSITE),
        (WEBSITE, WEBSITE),
        (APP, APP),
        (GDC, GDC),
        (ANDROID, ANDROID),
        (IOS, IOS)
    )

    payment_method = models.ForeignKey(PaymentMethod, related_name="paymentoffers")
    offer_title = models.CharField(max_length=500, null=True)
    offer_percentage = models.DecimalField(max_digits=5, null=True, decimal_places=2)
    credit_mode = models.CharField(max_length=50, choices=CREDIT_MODE_CHOICES)
    minimum_transaction = models.DecimalField(max_digits=10, null=True, decimal_places=2)
    ranking = models.PositiveIntegerField()
    description = models.TextField(null=True, blank=True)
    terms_conditions = models.TextField(null=True, blank=True)
    gateway_offer_id = models.CharField(max_length=100)
    image_url = models.CharField(max_length=500)
    coupon_code = models.CharField(max_length=50)
    utm_source = models.CharField(max_length=50)
    auto_apply = models.BooleanField(default=False)
    channel = models.CharField(
        max_length=50,
        choices=BOOKING_CHANNEL,
        blank=True)

    def __str__(self):
        return '%s %s' % (self.payment_method, self.offer_percentage)
