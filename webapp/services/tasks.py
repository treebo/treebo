import requests
from celery import shared_task
from celery.signals import task_prerun
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core import mail
from django.core.mail.message import EmailMessage
from django.template import loader

from base.filters import CeleryRequestIDFilter
from base.logging_decorator import log_args

logger = get_task_logger(__name__)


@task_prerun.connect()
def set_request_id(
        signal=None,
        sender=None,
        task_id=None,
        task=None,
        args=None,
        **kwargs):
    logger.addFilter(
        CeleryRequestIDFilter(
            kwargs.get('user_id'),
            kwargs.get('request_id'),
            task_id,
            task.name))
    logger.info(
        "set_request_id called with kwargs: %s, and task_id: %s",
        kwargs,
        task_id)


@shared_task
@log_args(logger)
def send_sms(numbers, text, high_priority=False):
    try:
        if high_priority:
            user_id = settings.SMS_PRIORITY_VENDOR_ID
            pwd = settings.SMS_PRIORITY_VENDOR_PASSWORD
        else:
            user_id = settings.SMS_VENDOR_ID
            pwd = settings.SMS_VENDOR_PASSWORD

        url = settings.SMS_VENDOR_URL
        user_id, password = str(user_id), str(pwd)
        querystring = {
            "v": "1.1",
            "msg_type": "Text",
            "method": "sendMessage",
            "auth_scheme": "plain",
            "format": "text"}

        querystring["msg"] = text
        querystring["send_to"] = str(numbers)
        querystring["userid"] = user_id
        querystring["password"] = password
        logger.info(
            "Calling gupshup API: %s with querystring: %s",
            url,
            querystring)

        response = requests.get(url, params=querystring)
        logger.info("sms sent")

    except Exception as e:
        logger.error("Error is sending sms to %s", str(numbers))


@shared_task
@log_args(logger)
def send_email(emails, subject, template_path, context, content_type="html"):
    template = loader.get_template(template_path)
    content = template.render(context)
    email = EmailMessage(subject, content, settings.SERVER_EMAIL, emails)
    email.content_subtype = content_type
    email.send()


@shared_task
def exception_mailer(subject, message, html_message):
    logger.info("Exception_mailer called with subject: %s", subject)
    logger.info("Exception mailer called with message: %s", html_message)
    if type(html_message) == bytes:
        html_message = html_message.decode()
    mail.mail_admins(
        subject,
        message,
        fail_silently=True,
        html_message=html_message)
