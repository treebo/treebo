# -*- coding: utf-8 -*-
from django.contrib import admin

from apps.pricing.models import PreferredPromo, PromoConditions, ChannelManagerRatePlan, \
    RoomRatePlan, LuxuryTax, StateTax, RoomPrice, RoomPriceV2, FeatureToggle

from dbcommon.admin import ReadOnlyAdminMixin


@admin.register(PreferredPromo)
class PreferredPromoAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'hotel',
        'type',
        'stay_start',
        'stay_end',
        'discountType',
        'status')
    list_filter = ('hotel__name',)
    date_hierarchy = 'stay_start'


@admin.register(PromoConditions)
class PromoConditionsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'condition_name', 'condition_params', 'promo')
    search_fields = ('condition_name', 'promo__hotel__name')


@admin.register(ChannelManagerRatePlan)
class ChannelManagerRatePlanAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'room',
        'hotel',
        'start_date',
        'end_date',
        'base_price',
        'price_increment',
        'is_active')
    list_filter = ('hotel',)
    search_fields = ('hotel__name',)
    date_hierarchy = 'start_date'


@admin.register(RoomRatePlan)
class RoomRatePlanAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'room',
        'hotel',
        'start_date',
        'end_date',
        'base_price',
        'price_increment',
        'is_active')


@admin.register(LuxuryTax)
class LuxuryTaxAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'start_range', 'end_range', 'tax_value', 'state')


@admin.register(StateTax)
class StateTaxAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'state',
        'tax_type',
        'tax_value',
        'luxury_tax_on_base',
        'effective_date',
        'expiry_date')


class StateTaxInline(admin.TabularInline):
    model = StateTax
    extra = 0


@admin.register(RoomPrice)
class RoomPriceAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'date',
        'room',
        'guest_count',
        'rack_rate',
        'total_amount',
        'tax_amount',
        'promo_applied')
    list_filter = ('room__hotel__name',)
    search_fields = ('room__hotel__name',)
    date_hierarchy = 'date'


@admin.register(RoomPriceV2)
class RoomPriceV2Admin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'date',
        'room',
        'guest_count',
        'base_price',
        'extra_adult_rate',
        'extra_kid_rate')
    list_filter = ('room__hotel__name',)
    search_fields = ('room__hotel__name',)
    date_hierarchy = 'date'


@admin.register(FeatureToggle)
class FeatureToggleAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'namespace', 'feature', 'environment', 'is_enabled')
