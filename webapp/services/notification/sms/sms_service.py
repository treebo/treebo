from services import tasks


class SmsService():
    def __init__(self):
        super(SmsService, self).__init__()

    @staticmethod
    def send_sms(numbers, text, high_priority=False):
        # type: (str[], str)
        """
        Sends an sms asynchronously to the set of numbers
        :param (str[]) numbers:
        :param (str) text:
        """

        def num_filter(number):
            return number is not None and str(number).isdigit()

        sms_numbers = ",".join(filter(num_filter, numbers))
        tasks.send_sms.delay(sms_numbers, text, high_priority=high_priority)
