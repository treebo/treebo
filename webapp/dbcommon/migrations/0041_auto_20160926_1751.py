# -*- coding: utf-8 -*-


from django.db import models, migrations
from dbcommon.models.location import City


def generate_city_slugs(apps, schema_editor):
    cities = City.objects.all()
    for city in cities:
        city.save()


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0040_auto_20160926_1745'),
    ]

    operations = [
        migrations.RunPython(generate_city_slugs),
    ]
