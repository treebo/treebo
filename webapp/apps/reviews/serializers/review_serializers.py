from rest_framework import serializers

from apps.reviews.models import HotelTAOverallRatings, TAHotelReviews


class AwardsDTO(serializers.Serializer):
    image = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    description = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)
    year = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    name = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)


class SearchPageReviewDTO(serializers.Serializer):
    count = serializers.IntegerField(required=False)
    ta_enabled = serializers.BooleanField(default=False)
    image = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    rating = serializers.DecimalField(max_digits=20, decimal_places=1)
    award = AwardsDTO(required=False, allow_null=True)


class RatingPercentageDTO(serializers.Serializer):
    count = serializers.IntegerField()
    rating = serializers.IntegerField()
    percent = serializers.IntegerField()
    review_str = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)


class OverallRatingDTO(serializers.Serializer):
    rating = serializers.DecimalField(max_digits=20, decimal_places=1)
    image = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True)
    rating_percent = RatingPercentageDTO(
        required=False, many=True, allow_null=True)


class ResponseDetailDTO(serializers.Serializer):
    text = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    response_published_date = serializers.DateTimeField(
        format='%d %b %Y', required=False)
    author = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)


class UserDTO(serializers.Serializer):
    user_name = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)
    image_url = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)


class ReviewDetailDTO(serializers.Serializer):
    user_rating = serializers.DecimalField(max_digits=20, decimal_places=1)
    text = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    image = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    text_url = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    review_date = serializers.DateTimeField(format='%d %b %y', allow_null=True)
    is_jcr = serializers.BooleanField(required=False, default=False)
    is_crawlable = serializers.BooleanField(required=False, default=False)


class TopReviewsDTO(serializers.Serializer):
    response_detail = ResponseDetailDTO(required=False, allow_null=True)
    user = UserDTO(required=False, allow_null=True)
    review_detail = ReviewDetailDTO(required=False, allow_null=True)


class SubratingDTO(serializers.Serializer):
    rating = serializers.DecimalField(max_digits=20, decimal_places=1)
    image = serializers.CharField(
        allow_blank=True,
        allow_null=True,
        required=False)
    name = serializers.CharField(
        allow_blank=True,
        allow_null=True,
        required=False)


class OverallRankingDTO(serializers.Serializer):
    rank_string = serializers.CharField(
        allow_blank=True, allow_null=True, required=False)
    rank_out_of = serializers.IntegerField(default=0)
    rank = serializers.IntegerField(default=0)


class HDPageReviewDTO(serializers.Serializer):
    write_review_url = serializers.CharField(
        allow_blank=True, allow_null=True, required=False)
    ta_location_id = serializers.IntegerField()
    ta_enabled = serializers.BooleanField()
    overall_ranking = OverallRankingDTO(required=False, allow_null=True)
    hotel_id = serializers.IntegerField()
    num_reviews = serializers.IntegerField()
    subratings = SubratingDTO(many=True, required=False, allow_null=True)
    awards = AwardsDTO(many=True, required=False, allow_null=True)
    read_reviews_url = serializers.CharField(
        allow_blank=True, allow_null=True, required=False)
    top_reviews = TopReviewsDTO(many=True, required=False, allow_null=True)
    overall_rating = OverallRatingDTO(required=False, allow_null=True)
