import datetime
import logging
import sys
import traceback

from django.core.management import BaseCommand
from django.db.models import F

from apps.pricing.models import RoomPrice

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Update Price Dates'

    def handle(self, *args, **options):
        try:
            self.updatePrices()
        except Exception as exc:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            logger.exception(
                "update prices %s" %
                repr(
                    traceback.format_exception(
                        exc_type,
                        exc_obj,
                        exc_tb)))

    def updatePrices(self):
        try:
            minDatePrice = RoomPrice.objects.filter().order_by('date')[0]
            if minDatePrice.date < datetime.date.today():
                RoomPrice.objects.all().update(date=F('date') + 1)
        except Exception as exc:
            logger.exception("unable to update Prices")
