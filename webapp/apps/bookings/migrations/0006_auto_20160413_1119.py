# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0005_bookingrequest'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='accesskey',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='accesssecret',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
