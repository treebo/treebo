import datetime
import logging

from rest_framework.response import Response
from rest_framework.views import APIView
from ths_common.constants.base_enum import BaseEnum
from thsc.crs.entities.booking import Booking as CRSBooking
from ths_common.constants.booking_constants import BookingStatus

from apps.bookings.models import Booking
from apps.bookings.repositories.booking_repository import BookingServiceRepository
from apps.payments.integrations.treebo_realization import TreeboRealizationClient
from common.constants.common_constants import EmailTemplates, MessageTemplates
from common.job.job_scheduler import JobScheduler
from django.conf import settings
from data_services.respositories_factory import RepositoriesFactory
from services.restclient.communication_client import CommunicationClient
from data_services.soa_clients.property_client import CatalogClient, CatalogAPIResponsesProcessor

logger = logging.getLogger(__name__)

ETA_INTERVAL = 15

communication_client = CommunicationClient()
hotel_repository = RepositoriesFactory.get_hotel_repository()
booking_service_repository = BookingServiceRepository()
cs_responses_processor = CatalogAPIResponsesProcessor()
catalog_client = CatalogClient(cs_response_processor=cs_responses_processor)


class RoomTypes(BaseEnum):
    ACACIA = "Solo Room"
    OAK = "Standard Room"
    MAPLE = "Deluxe Room"
    MAHOGANY = "Premium Room"


def payment_drop_offs_notification_job_executor(booking_id):
    booking = booking_service_repository.get_booking_by_crs_booking_id(booking_id)
    if booking.booking_status == Booking.TEMP_BOOKING_CREATED:
        # we’re adding this as we don’t update treebo db with cancelled status
        if CRSBooking.get(booking_id) and CRSBooking.get(
            booking_id).status == BookingStatus.CANCELLED:
            return True
        hotel = hotel_repository.get_single_hotel(id=booking.hotel_id)
        realization_client = TreeboRealizationClient()

        checkin = booking.checkin_date
        checkout = booking.checkout_date
        city_name = hotel.city.name
        roomconfig = booking.room_config
        room_type = RoomTypes[(booking.room.room_type_code).upper()].value
        room_count = len(roomconfig.split(','))
        google_link = catalog_client.get_property_json_from_cs(hotel.cs_id)["location"]["maps_link"]
        url_for_same_city_hotels = settings.TREEBO_WEB_URL + "/search/?checkin={0}&checkout={1}&" \
                                                             "city={2}&landmark=&locality=&q={2}&roomconfig={3}#".format(
            checkin, checkout, city_name, roomconfig)

        payment_url, payable_amount = realization_client.generate_payment_link(booking_id,
                                                                               hotel.cs_id,
                                                                               part_payment=False)

        message_payload = dict(guest_name=booking.guest_name,
                               city_name=city_name,
                               booking_amount=payable_amount,
                               hotel_name=hotel.name,
                               hotel_address=hotel.address,
                               checkin_date=checkin.strftime("%b %d, %Y"),
                               checkout_date=checkout.strftime("%b %d, %Y"),
                               checkin_day=checkin.strftime("%A"),
                               checkout_day=checkout.strftime("%A"),
                               checkin_time=booking.checkin_time,
                               checkout_time=booking.checkout_time,
                               payment_link=payment_url,
                               image_url=hotel.showcased_image_url.url,
                               adult_count=booking.adult_count,
                               child_count=booking.child_count,
                               room_type=room_type,
                               room_count=room_count,
                               google_link=google_link,
                               other_hotels_link=url_for_same_city_hotels,
                               )

        if booking.guest_email:
            subject = EmailTemplates.PAYMENT_DROPOFF.subject.format(city=city_name)
            communication_client.send_communication(
                identifier=EmailTemplates.PAYMENT_DROPOFF.identifier,
                context_data=message_payload,
                subject=subject, to_emails=[booking.guest_email])
        # there is no sms template defined for payment dropoff notification
        # if booking.guest_mobile:
        #     communication_client.send_communication(
        #         identifier=MessageTemplates.PAYMENT_DROPOFF.identifier,
        #         context_data=message_payload,
        #         receivers=[booking.guest_mobile])
        return True
    else:
        return True


def schedule_soft_bookings_in_db():
    threshold = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(minutes=int(15))
    soft_booking_ids = BookingServiceRepository.get_soft_booking_ids_by_created_at(threshold)
    for booking_id in soft_booking_ids:
        job_entity = JobScheduler().schedule_soft_booking_notification(booking_id,
                                                                       datetime.datetime.now(
                                                                           datetime.timezone.utc) + datetime.timedelta(
                                                                           minutes=int(
                                                                               ETA_INTERVAL))
                                                                       )


class SoftBookingJobCronAPI(APIView):

    def post(self, request):
        try:
            schedule_soft_bookings_in_db()
            success = True
        except Exception as ex:
            logger.exception('Error occurred while scheduling job {}'.format(ex))
            success = False
        return Response({"message": "Scheduled soft bookings for notifications in db"})
