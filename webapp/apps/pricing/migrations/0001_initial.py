# -*- coding: utf-8 -*-


import jsonfield.fields
from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0008_auto_20160215_0853'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChannelManagerRatePlan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('is_active', models.BooleanField(default=False)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('base_price', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('price_increment',
                 models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('hotel', models.ForeignKey(to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
                ('room', models.ForeignKey(to='dbcommon.Room', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_channelmanagerrateplan',
            },
        ),
        migrations.CreateModel(
            name='PreferredPromo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('description', models.TextField()),
                ('type', models.CharField(max_length=100)),
                ('min_advance_days', models.IntegerField(default=-1)),
                ('max_advance_days', models.IntegerField(default=-1)),
                ('min_nights', models.IntegerField(default=1)),
                ('refundable', models.BooleanField()),
                ('stay_start', models.DateField()),
                ('stay_end', models.DateField()),
                ('booking_start', models.DateField()),
                ('booking_end', models.DateField()),
                ('booking_days_bitmap', models.IntegerField(default=-1)),
                ('stay_days_bitmap', models.IntegerField(default=-1)),
                ('discountType', models.CharField(default=b'Percent', max_length=100)),
                ('status',
                 models.IntegerField(default=0, choices=[(0, b'Active'), (1, b'Expired')])),
                ('hotel', models.ForeignKey(to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_preferredpromo',
            },
        ),
        migrations.CreateModel(
            name='PromoConditions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('condition_name', models.CharField(max_length=200)),
                ('condition_params', jsonfield.fields.JSONField()),
                (
                    'promo',
                    models.ForeignKey(related_name='conditions', to='pricing.PreferredPromo', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_promoconditions',
            },
        ),
    ]
