# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0008_auto_20160414_0711'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='discount_value',
            field=models.DecimalField(
                default=0,
                max_digits=20,
                decimal_places=10),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='order_id',
            field=models.CharField(
                max_length=200,
                null=True,
                blank=True),
        ),
    ]
