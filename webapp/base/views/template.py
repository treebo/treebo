import datetime
import logging

from base import log_args
from django.conf import settings
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView


logger = logging.getLogger(__name__)


class TreeboTemplateView(TemplateView):
    def get_template_names(self):
        return super(TreeboTemplateView, self).get_template_names()

    def get(self, request, *args, **kwargs):
        st = datetime.datetime.now()
        is_mobile = False
        if hasattr(request, 'is_mobile') and request.is_mobile:
            if hasattr(
                    self,
                    'redirect_mobile_to_desktop_view') and self.redirect_mobile_to_desktop_view:
                is_mobile = False
            else:
                is_mobile = True
        if is_mobile:
            context = self.getMobileData(request, args, kwargs)
            self.template_name = 'mobile/' + self.template_name
        else:
            context = self.getData(request, args, kwargs)
            self.template_name = 'desktop/' + self.template_name

        et = datetime.datetime.now()

        if isinstance(context, HttpResponseRedirect):
            return context

        context["GTM_KEY"] = settings.GTM_KEY
        context['environment'] = settings.ENVIRONMENT
        context['MAPS_KEY'] = settings.MAPS_KEY
        if isinstance(context, dict):
            if (context.get('jsvars') is not None):
                request.jsvars = context.get('jsvars')

        logger.info(
            "Controller %s took %s ms" %
            (str(
                self.__class__.__name__), int(
                (et - st).total_seconds() * 1000)))

        logger.info(
            "Controller {controller} took {time} ms", extra={
                "controller": str(
                    self.__class__.__name__), "time": int(
                    (et - st).total_seconds() * 1000)})

        return self.render_to_response(context)

    def getData(self, request, args, kwargs):
        return None

    @log_args(logger)
    def getMobileData(self, request, args, kwargs):
        """
            Return the data need by the mobile template
        :rtype : dict
        :param request:
        :param args:
        :param kwargs:
        :return: dict of values.
        """
        return None

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        data = request.POST
        st = datetime.datetime.now()
        is_mobile = False
        if hasattr(request, 'is_mobile') and request.is_mobile:
            if hasattr(
                    self,
                    'redirect_mobile_to_desktop_view') and self.redirect_mobile_to_desktop_view:
                is_mobile = False
            else:
                is_mobile = True
        if is_mobile:
            context = self.postMobileData(request, args, kwargs)
            if self.template_name:
                self.template_name = 'mobile/' + self.template_name
        else:
            context = self.postData(request, args, kwargs)
            if self.template_name:
                self.template_name = 'desktop/' + self.template_name

        et = datetime.datetime.now()
        logger.info(
            "Controller %s took %s ms" %
            (str(
                self.__class__.__name__), int(
                (et - st).total_seconds() * 1000)))

        logger.info(
            "Controller {controller} took {time} ms", extra={
                "controller": str(
                    self.__class__.__name__), "time": int(
                    (et - st).total_seconds() * 1000)})

        if isinstance(context, HttpResponseRedirect):
            return context
        return self.render_to_response(context)

    @log_args(logger)
    def postData(self, request, args, kwargs):
        return None

    @log_args(logger)
    def postMobileData(self, request, args, kwargs):
        """
            Return the data need by the mobile template
        :rtype : dict
        :param request:
        :param args:
        :param kwargs:
        :return: dict of values.
        """
        return None
