import json
import logging

from django.http import HttpResponse

from apps.common.exceptions.custom_exception import ExternalServiceUnavailableException
from apps.growth.integrations.treebo_realization import TreeboRealizationClient
from base.views.api import TreeboAPIView
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.growth.constants import CANCELLATION_REASONS

logger = logging.getLogger(__name__)


class GetBookingData(TreeboAPIView):

    def get_response_dict(self, checkin, checkout, order_id, guest_name, address, pay_at_hotel, hotel_name, cancel_hash,
                          booking_status, message):

        response_dict = {
            "status": "success",
            "data": {
                "booking_details": {
                    "checkin_date": str(checkin),
                    "checkout_date": str(checkout),
                    "cancelled_dateTime": None,
                    "booking_id": str(order_id),
                    "guest_name": str(guest_name),
                    "hotel_address": str(address),
                    "pay_at_hotel": pay_at_hotel,
                    "hotel_name": hotel_name,
                    "booking_hash": cancel_hash,
                    "reasons": CANCELLATION_REASONS,
                },
                "booking_status": {
                    "status": booking_status,
                    "message": message,
                },
            }
        }

        return response_dict

    def post(self, request):
        raise Exception("Post method not supported")

    def get(self, request, *args, **kwargs):
        try:
            cancel_hash = request.GET.get('booking_hash', None)

            if cancel_hash:
                booking_details = TreeboRealizationClient().get_booking_details_for_cancellation(cancel_hash)
                data = self.get_response_dict(checkin=booking_details["checkin_date"],
                                              checkout=booking_details["checkout_date"],
                                              address=booking_details["hotel_address"],
                                              hotel_name=booking_details["hotel_name"],
                                              cancel_hash=booking_details["booking_hash"],
                                              booking_status=booking_details["status"],
                                              order_id=booking_details["booking_id"],
                                              guest_name=booking_details["guest_name"],
                                              pay_at_hotel=booking_details["pay_at_hotel"],
                                              message=booking_details["status_message"])

                return HttpResponse(json.dumps(data), content_type="application/json", status=200)
            else:
                return HttpResponse(
                    json.dumps(
                        {
                            "error": {
                                "msg": "Oops! Something went wrong. Please try again later."}}),
                    content_type="application/json",
                    status=400)

        except ExternalServiceUnavailableException as e:
            slack_alert.send_slack_alert_for_exceptions(status=400, request_param=request.GET,
                                                        message=e.developer_message,
                                                        class_name=self.__class__.__name__)
            logger.error(e.developer_message, exc_info=e)
            return HttpResponse(
                json.dumps(
                    {
                        "error": {
                            "msg": "Oops! Something went wrong. Please try again later."}}),
                content_type="application/json",
                status=400)

        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception("Fetching booking data based on hash failed")
            return HttpResponse(
                json.dumps(
                    {
                        "error": {
                            "msg": "Oops! Something went wrong. Please try again later."}}),
                content_type="application/json",
                status=400)
