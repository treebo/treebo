# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fot', '0006_auto_20160629_1447'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fot',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='fotconfig',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='fotcredits',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='fothotel',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='fothotelblackout',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='fotreservationsrequest',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='fotsignupanswers',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
        migrations.AlterModelOptions(
            name='fotsignupquestions',
            options={'default_permissions': ('add', 'change', 'delete', 'read')},
        ),
    ]
