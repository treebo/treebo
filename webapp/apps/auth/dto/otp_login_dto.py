from rest_framework import serializers
from apps.common.validators import CustomValidator as custom_validator

NOT_AVAILABLE = 'NA'


class OtpLoginDTO(serializers.Serializer):
    phone_number = serializers.CharField(max_length=50)
    channel = serializers.CharField(max_length=20, required=False, default=NOT_AVAILABLE)

    def validate_phone_number(self, phone_number):
        return custom_validator.normalise_phone_number(phone_number)