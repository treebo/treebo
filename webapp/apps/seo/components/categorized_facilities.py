import logging
from collections import defaultdict

from django.utils.encoding import smart_str

from apps.content.models import ContentStore
from apps.hotels.service.hotel_service import HotelService
from apps.seo import constants
from apps.seo.components.base_component import BaseComponent
from apps.seo.serializers.categorised_facilities_serializer import ItemsDTO, ContentDTO, FacilitiesDTO
from webapp.services.common_services.sql_query_helper import SqlQueryHelper
from dbcommon.models.facilities import FacilityCategory, Facility
from dbcommon.models.room import Room

__author__ = 'snehilrastogi'

logger = logging.getLogger(__name__)


class CategorizedFacilities(BaseComponent):
    def __init__(self, current_city, footer_data, hotel):
        self.hotel = hotel
        super(
            CategorizedFacilities,
            self).__init__(
            current_city,
            footer_data,
            [])

    @classmethod
    def get_facility_from_dict(cls, facility_dict):
        facility = Facility()
        facility.name = facility_dict['name']
        facility.id = facility_dict['id']
        facility.created_at = facility_dict['created_at']
        facility.modified_at = facility_dict['modified_at']
        facility.catalog_mapped_name = facility_dict['catalog_mapped_name']
        facility.url = facility_dict['url']
        facility.css_class = facility_dict['css_class']
        facility.to_be_shown = facility_dict['to_be_shown']
        return facility

    def build(self):
        facilities = dict()
        try:
            hotel_rooms_id_list = [room['id'] for room in
                                   Room.objects.values('id').filter(hotel__id=int(self.hotel.id))]

            query = '''SELECT hf.id, hf.created_at, hf.modified_at, hf.name, hf.catalog_mapped_name, hf.url,
                hf.css_class, hf.to_be_shown FROM hotels_facility hf
                where
                (
                  (hf.id in (select hfr.facility_id from hotels_facility_rooms hfr where hfr.room_id = ANY(%s)))
                  or
                  (hf.id in (select hfs.facility_id from hotels_facility_hotels hfs where hfs.hotel_id = %s))
                ) and hf.css_class is not null '''
            hotel_facilities = SqlQueryHelper().execute_query_and_map_to_dictionary_with_params(
                query, [hotel_rooms_id_list, self.hotel.id])

            id_to_hotels_facility_map = {hotel_facility['id']: CategorizedFacilities.get_facility_from_dict(
                hotel_facility) for hotel_facility in hotel_facilities}

            category_facility_query = '''select fc.name, hf.facilities from hotels_facilitycategory fc,
                (Select hfc.facilitycategory_id cat_id, string_agg(hfc.facility_id::text, ',') facilities
                    from hotels_facility_categories hfc where facility_id = ANY(%s)
                    group by hfc.facilitycategory_id) hf where fc.id = hf.cat_id'''

            category_facility_map = SqlQueryHelper().execute_query_and_map_to_dictionary_with_params(
                category_facility_query, [list(id_to_hotels_facility_map.keys())])

            categorised_facilities = dict()
            for category in category_facility_map:
                name = category['name']
                id_list = category['facilities'].split(',')
                facility_list = []
                for id in id_list:
                    if id is not None and id.strip() != "":
                        facility_list.append(id_to_hotels_facility_map[int(id.strip())])
                categorised_facilities[name] = facility_list

            logger.info("categorised_facilities %s", categorised_facilities)

            facilities = self.create_categorised_facilities_data(
                categorised_facilities, hotel_rooms_id_list)

            logger.info("facilities_response %s", facilities)

        except Exception:
            logger.exception(
                "Exception in fetching the category wise facilities data")


        return {"facilities": facilities}

    def create_categorised_facilities_data(
            self, categorised_facilities, hotel_rooms_id_list):
        facilities_dict = dict()
        try:
            title = constants.DEFAULT_HD_FACILITY_TITLE
            content_store_obj = ContentStore.objects.filter(
                name=constants.HD_COMPONENT_TITLE, version=1).first()

            if content_store_obj:
                content_store_val = content_store_obj.value
                if content_store_val.get(title):
                    title = content_store_val[title]

            facilities_order = constants.DEFAULT_FACILITIES_ORDER_DICT
            content_store_facility_order_obj = ContentStore.objects.filter(
                name=constants.FACILITIES_ORDER, version=1).first()
            if content_store_facility_order_obj:
                facilities_order = content_store_facility_order_obj.value

            facility_categories_order = sorted(
                facilities_order, key=lambda v: facilities_order[v])

            content_list = list()
            for f_category in facility_categories_order:
                if f_category in list(categorised_facilities.keys()):
                    content_dict = dict()
                    items_list = []
                    facility_sorted_list = sorted(
                        categorised_facilities[f_category],
                        key=lambda object1: object1.name)
                    logger.info(
                        "facility_sorted_list %s type %s",
                        facility_sorted_list,
                        type(facility_sorted_list))
                    for facility in facility_sorted_list:
                        item_dict = dict()
                        item_dict['name'] = facility.name
                        item_dict['show'] = True
                        item_data = ItemsDTO(data=item_dict)
                        if item_data.is_valid():
                            items_list.append(item_data.validated_data)
                        else:
                            logger.error(
                                "Item data for categorised facilities is invalid %s %s",
                                item_data.errors,
                                item_dict)
                    if items_list:
                        content_dict['title'] = f_category
                        content_dict['items'] = items_list
                        content_data = ContentDTO(data=content_dict)
                        if content_data.is_valid():
                            content_list.append(content_data.validated_data)
                        else:
                            logger.error(
                                "Content data for categorised facilities is invalid %s %s",
                                content_data.errors,
                                content_dict)
            if content_list:
                facilities_dict['content'] = content_list
                facilities_dict['title'] = smart_str(title).format(
                    HotelService().get_display_name_from_hotel_name(hotel_name=self.hotel.name).title())
                facilities_data = FacilitiesDTO(data=facilities_dict)
                if facilities_data.is_valid():
                    return facilities_data.validated_data
                else:
                    logger.error(
                        "Facilities data for categorised facilities is invalid %s %s",
                        facilities_data.errors,
                        facilities_dict)
        except Exception:
            logger.exception(
                "Exception in generating the category wise facilities for %s",
                hotel_rooms_id_list)
        return facilities_dict
