

import logging
import re

from lxml import etree as et
from lxml import objectify

from apps.third_party.service.google_hotel_ads import constants
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.facilities import Facility
from dbcommon.models.hotel import Hotel
from data_services.hotel_repository import HotelRepository

#
# hotels_business = None
# hotels_gym = None
# hotels_restaurant = None
# hotels_parking = None
# hotels_laundry = None
# hotels_kitchen = None
# hotels_roomservice = None

logger = logging.getLogger(__name__)


class HotelListFeedService(object):
    def generate_hotel_list_feed(self, ):
        try:
            root = et.Element('listings')
            language = et.SubElement(root, 'language')
            language.text = 'en'
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            hotel_data = hotel_repository.get_active_hotel_list()
            #hotel_data = Hotel.objects.select_related('city', 'locality', 'state').filter(status="1")
            hotel_list_xml = self.generate_listing(root, hotel_data)
            return hotel_list_xml
        except Exception as e:
            logger.exception("Exception in generationg Hotel List Feed ")
            raise e

    def generate_listing(self, root, hotel_data):
        """

        :param root:
        :param (Hotel[]) hotel_data:
        :return:
        """
        for hotel in hotel_data:
            city = hotel.city
            listing = et.SubElement(root, 'listing')
            id = et.SubElement(listing, 'id')
            id.text = str(hotel.id)
            name = et.SubElement(listing, 'name')
            name.text = hotel.name
            self.generate_addresstag(listing, hotel, city)
            country = et.SubElement(listing, 'country')
            country.text = "IN"
            latitude = et.SubElement(listing, 'latitude')
            latitude.text = str(hotel.latitude)
            longitude = et.SubElement(listing, 'longitude')
            longitude.text = str(hotel.longitude)
            main_phone = objectify.SubElement(listing, 'phone', type="main")
            main_phone.text = self.generate_main_phone_number(hotel)
            mobile_phone = objectify.SubElement(
                listing, 'phone', type="mobile")
            mobile_phone.text = str(constants.DEFAULT_CONTACT)
            category = et.SubElement(listing, 'category')
            ct = "Hotels in " + city.name
            category.text = ct
            date = et.SubElement(listing, 'date')
            created_at = hotel.created_at
            month = created_at.strftime("%m")
            year = created_at.strftime("%Y")
            day = created_at.strftime("%d")
            date.set('month', month)
            date.set('day', day)
            date.set('year', year)
            self.generate_contenttag(listing, hotel)
        objectify.deannotate(root, xsi_nil=True, cleanup_namespaces=True)
        # for printing on console
        # print(et.tostring(root, pretty_print=True, xml_declaration=True, encoding="UTF-8"))
        # write to file:
        tree = et.ElementTree(root)
        # tree.write('google-local.xml', pretty_print=True, xml_declaration=True, encoding="UTF-8")
        # tree = et.tostring(tree)
        return tree

    def generate_addresstag(self, listing, hotel, city):
        """

        :param listing:
        :param (Hotel) hotel:
        :param (City) city:
        """
        address = et.SubElement(listing, 'address')
        address.set('format', 'detailed')
        component = objectify.SubElement(address, 'component', name="street")
        component.text = hotel.street
        component = objectify.SubElement(address, 'component', name="locality")
        locality = hotel.locality
        component.text = locality.name
        component = objectify.SubElement(address, 'component', name="City")
        component.text = city.name
        component = objectify.SubElement(address, 'component', name="State")
        state = hotel.state
        component.text = state.name
        component = objectify.SubElement(address, 'component', name="Pincode")
        component.text = str(locality.pincode)
        if hotel.landmark and not hotel.landmark.isspace() and not hotel.landmark == "-":
            component = objectify.SubElement(
                address, 'component', name="landmark")
            component.text = hotel.landmark

    def generate_contenttag(self, listing, hotel):
        content = et.SubElement(listing, 'content')
        url_image = self.generate_imagetag(content, hotel)
        attributes = et.SubElement(content, 'attributes')
        email = et.SubElement(attributes, 'email')
        email.text = constants.EMAIL_DATA
        website = et.SubElement(attributes, 'website')
        website.text = url_image
        # amenities = et.SubElement(attributes, 'amenities')
        # self.generate_amenitiestag(amenities, hotel)

    def generate_imagetag(self, content, hotel):
        image = et.SubElement(content, 'image')
        image.set('type', 'photo')
        url_image = constants.BASE_URL + hotel.get_sitemap_url()
        image.set('url', url_image)
        image.set('width', "800")
        image.set('height', "480")
        link = et.SubElement(image, 'link')
        link.text = url_image
        title = et.SubElement(image, 'title')
        title.text = hotel.tagline
        return url_image

    def generate_amenitiestag(self, amenities, hotel):
        hotels_business = Facility.objects.get(name="Business Event Hosting").hotels.all()
        hotels_gym = Facility.objects.get(name="Gym").hotels.all()
        hotels_restaurant = Facility.objects.get(name="Restaurant On Premises").hotels.all()
        hotels_parking = Facility.objects.get(name="Parking").hotels.all()
        hotels_laundry = Facility.objects.get(name="Business Event Hosting").hotels.all()
        hotels_kitchen = Facility.objects.get(name="Kitchenett").hotels.all()
        hotels_roomservice = Facility.objects.get(name="Room Service").hotels.all()
        # amenities = et.SubElement(hotel_data, "amenities")
        accept_online_booking = et.SubElement(
            amenities, "accept_online_booking")
        accept_online_booking.text = "yes"
        accessible_rooms = et.SubElement(amenities, "accessible_rooms")
        accessible_rooms.text = "yes"
        # no extra row for ths available so goes in everyhwere by default
        air_conditioned = et.SubElement(amenities, "air_conditioned")
        air_conditioned.text = "yes"
        if hotels_business.filter(id=hotel.id).exists():
            business_center = et.SubElement(amenities, "business_center")
            business_center.text = "yes"
        family_friendly = et.SubElement(amenities, "family_friendly")
        family_friendly.text = "yes"
        if hotels_gym.filter(id=hotel.id).exists():
            fitness_facilities = et.SubElement(amenities, "fitness_facilities")
            fitness_facilities.text = "yes"
        free_breakfast = et.SubElement(amenities, "free_breakfast")
        free_breakfast.text = "yes"
        free_internet = et.SubElement(amenities, "free_internet")
        free_internet.text = "yes"
        if hotels_parking.filter(id=hotel.id).exists():
            free_parking = et.SubElement(amenities, "free_parking")
            free_parking.text = "yes"
        if hotels_kitchen.filter(id=hotel.id).exists():
            in_room_kitchen = et.SubElement(amenities, "in_room_kitchen")
            in_room_kitchen.text = "yes"
        if hotels_laundry.filter(id=hotel.id).exists():
            laundry = et.SubElement(amenities, "laundry")
            laundry.text = "yes"
        non_smoking_rooms = et.SubElement(amenities, "non_smoking_rooms")
        non_smoking_rooms.text = "yes"
        if hotels_restaurant.filter(id=hotel.id).exists():
            restaurant = et.SubElement(amenities, "restaurant")
            restaurant.text = "yes"
        if hotels_roomservice.filter(id=hotel.id).exists():
            room_service = et.SubElement(amenities, "room_service")
            room_service.text = "yes"

    def generate_main_phone_number(self, hotel):
        main_phone = constants.DEFAULT_CONTACT
        if hotel.phone_number != "0":
            phone_number = hotel.phone_number.split("/")
            temp_phone = re.sub("[^0-9()-]", "", phone_number[0])
            if temp_phone:
                main_phone = str(temp_phone)
            else:
                main_phone = str(constants.DEFAULT_CONTACT)
        return main_phone
