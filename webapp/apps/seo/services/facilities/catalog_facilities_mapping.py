import logging

from django.conf import settings
from django.db.models import Q

from apps.seo.models.web_catalog_hotel_mapping import WebCatalogHotelMapping, CatalogRoomMapping
from apps.seo.services.common_services.hit_url import URLBaseClass
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.facilities import Facility
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class CatalogFacilitiesMapping(object):
    def map_catalog_failities(
            self,
            hotelogix_id=None,
            catalog_hotel_id=None,
            facilities_choice=None):
        try:
            if hotelogix_id and catalog_hotel_id:
                catalog_hotels = WebCatalogHotelMapping.objects.filter(
                    hotel__status=1,
                    hotel__hotelogix_id=hotelogix_id,
                    catalog_hotel_id=catalog_hotel_id).first()
            else:
                catalog_hotels = WebCatalogHotelMapping.objects.filter(
                    hotel__status=1).order_by('id')
            for chotel in catalog_hotels:
                logger.info("TABLE ID %s", chotel.id)
                self.update_hotel_facilities(
                    chotel, facilities_choice=facilities_choice)
            if facilities_choice == 'hotel':
                self.update_common_facilities(hotelogix_id, catalog_hotel_id)
        except Exception:
            logger.exception(
                "Exception in updating faciities from cataloguing service to web")

    def update_hotel_facilities(self, catalog_hotel, facilities_choice=None):
        try:
            hotelogix_id = catalog_hotel.hotel.hotelogix_id
            hotel_obj = Hotel.objects.get(hotelogix_id=hotelogix_id)
            catalog_hotel_id = catalog_hotel.catalog_hotel_id
            if facilities_choice == 'hotel':
                logger.info(
                    "Updating hotel facilities for %s %s %s",
                    hotelogix_id,
                    catalog_hotel_id,
                    catalog_hotel.hotel.id)
                url = settings.CATALOG_HOTEL_FACILITIES_URL.format(
                    catalog_hotel_id)
                url_response = URLBaseClass()
                response_json = url_response.get_response(url=url)
                response_dict = dict(response_json)
                for key, value in list(response_dict.items()):
                    if response_dict[key]:
                        logger.info("Facility %s is present in hotel ", key)
                        facility_obj = self.get_facility_object(
                            key, response_dict)
                        for f_obj in facility_obj:
                            if f_obj and hotel_obj not in f_obj.hotels.all():
                                logger.info(
                                    "adding facility  %s for hotel", f_obj.name)
                                f_obj.hotels.add(hotel_obj)
                                f_obj.save()
                            else:
                                logger.info(
                                    "for hotel %s already present %s", hotelogix_id, f_obj.name)
                self.update_banquet_hall(catalog_hotel_id, hotel_obj)
                self.update_bars(catalog_hotel_id, hotel_obj)
                self.update_restaurants(catalog_hotel_id, hotel_obj)
            if facilities_choice == 'room':
                logger.info("updating rooms")
                self.update_room_facilities(catalog_hotel)

        except Hotel.DoesNotExist:
            logger.exception(
                "No such hotel %s exists",
                catalog_hotel.hotel.hotelogix_id)

        except Exception as e:
            logger.exception(
                "Exception in updating facilities for %s %s",
                catalog_hotel.hotel.hotelogix_id,
                catalog_hotel.catalog_hotel_id)
            raise e

    def update_room_facilities(self, catalog_hotel):
        try:
            hotelogix_id = catalog_hotel.hotel.hotelogix_id
            catalog_hotel_id = catalog_hotel.catalog_hotel_id
            logger.info(
                "Updating room facilities for %s %s",
                hotelogix_id,
                catalog_hotel_id)
            cat_room_map_obj = CatalogRoomMapping.objects.filter(
                hotel_map__catalog_hotel_id=catalog_hotel_id)
            logger.info(
                "count of rooms %s for hotel %s %s",
                cat_room_map_obj.count(),
                catalog_hotel_id,
                hotelogix_id)
            room_amenities = dict()
            for catalog_room in cat_room_map_obj:
                logger.info(
                    "Updating facilities for %s %s",
                    catalog_room.catalog_room_id,
                    catalog_room.room.room_type_code)
                url = settings.CATALOG_ROOM_FACILITIES_URL.format(
                    catalog_hotel_id, catalog_room.catalog_room_id)
                url_response = URLBaseClass()
                response_json = url_response.get_response(url=url)
                response_dict = dict(response_json)
                for key, value in list(response_dict.items()):
                    if response_dict[key]:
                        logger.info("Facility %s is present in hotel ", key)
                        facility_obj = self.get_facility_object(
                            key, response_dict)
                        for f_obj in facility_obj:
                            if catalog_room.room in room_amenities:
                                logger.info(
                                    "key %s  exist in room amenity dict .. append facility obj",
                                    catalog_room.room)
                                room_amenities[catalog_room.room].add(f_obj)
                            else:
                                logger.info(
                                    "key %s doesnot exist in room amenity dict .. make one and append facility obj",
                                    catalog_room.room)
                                room_amenities[catalog_room.room] = set()
                                room_amenities[catalog_room.room].add(f_obj)

            logger.info(
                "room_amenities %s \n .. now saving these facilities into respective rooms",
                room_amenities)
            for room_obj, facility_set in list(room_amenities.items()):
                logger.info(
                    "For room %s %s %s",
                    room_obj.room_type_code,
                    type(room_obj),
                    room_obj.id)
                for facility_obj in facility_set:
                    if facility_obj and room_obj not in facility_obj.rooms.all():
                        logger.info(
                            "adding facility for hotel-rooms %s %s",
                            facility_obj.name,
                            type(facility_obj))
                        facility_obj.rooms.add(room_obj)
                        facility_obj.save()
                    else:
                        logger.info(
                            "for room %s already present %s",
                            room_obj.room_type_code,
                            facility_obj.name)
        except Exception as e:
            logger.exception(
                "Exception in updating facilities for %s %s",
                catalog_hotel.hotel.hotelogix_id,
                catalog_hotel.catalog_hotel_id)

    def get_facility_object(self, key, response_dict):
        facility_obj = []
        try:
            if key == 'payment':
                if response_dict[key].get(
                        'amex_accepted') or response_dict[key].get('wallet_accepted'):
                    facility_obj = Facility.objects.filter(
                        catalog_mapped_name__iexact=key)
            elif key == 'disable_friendly':
                if response_dict[key].get('disable_friendly_room_available') or response_dict[key].get(
                        'wheelchair_count') or response_dict[key].get('disable_friendly_rooms'):
                    facility_obj = Facility.objects.filter(
                        catalog_mapped_name__iexact=key)
            else:
                facility_obj = Facility.objects.filter(
                    catalog_mapped_name__iexact=key)
        except Exception:
            logger.exception(
                "exception in finding facility objects corresponding to key %s", key)
        return facility_obj

    def update_banquet_hall(self, catalog_hotel_id, hotel_obj):
        try:
            logger.info(
                "checking and updating banquet hall for hotel %s %s",
                catalog_hotel_id,
                hotel_obj.hotelogix_id)
            url = settings.CATALOG_BANQUET_HALL_URL.format(catalog_hotel_id)
            url_response = URLBaseClass()
            response_json = url_response.get_response(url=url)
            response_dict = list(response_json)
            if len(response_dict):
                facility_obj = Facility.objects.filter(
                    catalog_mapped_name__iexact='banquet_party_hall').first()
                if facility_obj and hotel_obj not in facility_obj.hotels.all():
                    logger.info(
                        "adding banquet hall as a facility for hotel %s facility obj %s",
                        hotel_obj.hotelogix_id,
                        facility_obj)
                    facility_obj.hotels.add(hotel_obj)
                    facility_obj.save()
        except Exception:
            logger.exception(
                "Exception in updating bars for %s %s",
                catalog_hotel_id,
                hotel_obj.hotelogix_id)

    def update_bars(self, catalog_hotel_id, hotel_obj):
        try:
            logger.info(
                "checking and updating bars for hotel %s %s",
                catalog_hotel_id,
                hotel_obj.hotelogix_id)
            url = settings.CATALOG_BARS_URL.format(catalog_hotel_id)
            url_response = URLBaseClass()
            response_json = url_response.get_response(url=url)
            response_dict = list(response_json)
            if len(response_dict):
                facility_obj = Facility.objects.filter(
                    catalog_mapped_name__iexact='bar').first()
                if facility_obj and hotel_obj not in facility_obj.hotels.all():
                    logger.info(
                        "adding Bars as a facility for hotel %s facility obj %s",
                        hotel_obj.hotelogix_id,
                        facility_obj)
                    facility_obj.hotels.add(hotel_obj)
                    facility_obj.save()
        except Exception:
            logger.exception(
                "Exception in updating bars for %s %s",
                catalog_hotel_id,
                hotel_obj.hotelogix_id)

    def update_restaurants(self, catalog_hotel_id, hotel_obj):
        try:
            logger.info(
                "checking and updating restaurant for hotel %s %s",
                catalog_hotel_id,
                hotel_obj.hotelogix_id)
            url = settings.CATALOG_RESTAURANTS_URL.format(catalog_hotel_id)
            url_response = URLBaseClass()
            response_json = url_response.get_response(url=url)
            response_dict = list(response_json)
            if len(response_dict):
                facility_obj = Facility.objects.filter(
                    catalog_mapped_name__iexact='restaurant_on_premises').first()
                if facility_obj and hotel_obj not in facility_obj.hotels.all():
                    logger.info(
                        "adding Restaurant on premises as a facility for hotel %s facility obj %s",
                        hotel_obj.hotelogix_id,
                        facility_obj)
                    facility_obj.hotels.add(hotel_obj)
                    facility_obj.save()
        except Exception:
            logger.exception(
                "Exception in updating restaurant for %s %s",
                catalog_hotel_id,
                hotel_obj.hotelogix_id)

    def update_common_facilities(
            self,
            hotelogix_id=None,
            catalog_hotel_id=None):
        logger.info("updating common facilities for hotels")
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        try:
            if hotelogix_id and catalog_hotel_id:
                hotels = hotel_repository.filter_hotels(
                    hotelogix_id=hotelogix_id)
            else:
                hotels = hotel_repository.get_active_hotel_list()

            facility_obj = Facility.objects.filter(
                Q(catalog_mapped_name='24_hour_front_desk') | Q(
                    catalog_mapped_name='tea_coffee') | Q(
                    catalog_mapped_name='wifi') | Q(catalog_mapped_name='kettle') | Q(
                    catalog_mapped_name='wakeup_service') | Q(
                    catalog_mapped_name='water'))
            for hotel in hotels:
                for f_obj in facility_obj:
                    if f_obj and hotel not in f_obj.hotels.all():
                        logger.info(
                            "adding facility %s to hotel %s",
                            f_obj.name,
                            hotel.hotelogix_id)
                        f_obj.hotels.add(hotel)
                    else:
                        logger.info(
                            "facility %s for hotel %s already exists",
                            f_obj.name,
                            hotel.name)

        except Exception:
            logger.exception("exception in updating the common facilities")
