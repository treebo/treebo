# Map Actions to events and associate common actions if needed
# page_id 1-50 are reserved for mobile
# page_id 51-100 are reserved for desktop
# Uses the page name specified in the urls.py files

DEVICE_TO_CONFIG_MAP = {
    "0": "W",
    "1": "M"
}

ACTION_TO_EVENTS_MAPPER = {
    "DEFAULT": {
    },
    "M": {
        "pages:index": {"page_id": 1, "include_list": []},
        "pages:hotels_in_city": {"page_id": 2, "include_list": []},
        "pages:search-hotels": {"page_id": 2, "include_list": []},
        "pages:hotel-details-new": {"page_id": 3, "include_list": []},
        "pages:confirmation": {"page_id": 4, "include_list": []},
        "pages:order": {"page_id": 5, "include_list": []},
        "pages:transaction": {"page_id": 6, "include_list": []}
    },
    "W": {

        "pages:index": {"page_id": 51, "include_list": []},
        "pages:hotels_in_city": {"page_id": 52, "include_list": []},
        "pages:search-hotels": {"page_id": 52, "include_list": []},
        "pages:hotel-details-new": {"page_id": 53, "include_list": []},
        "pages:confirmation": {"page_id": 54, "include_list": []},
        "pages:order": {"page_id": 55, "include_list": []},
        "pages:transaction": {"page_id": 56, "include_list": []}
    }
}

# Detailed config for page events and available elements on page.
# Use currentElement where-ever $(this) is needed.
CLIENT_SIDE_EVENTS_LOAD = {
    1: {"available_elements": {
        "destination": "$('#searchInput').val()",
        "checkin": "rt.getCheckin()",
        "checkout": "rt.getCheckout()",
        "adults": "rt.getAdults()",
        "rooms": "rt.getRooms()",
        "kids": "rt.getKids()",
    },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Search Treebo",
                "event_selector": "#searchSubmitBtn",
                "event_source": "Search Treebo",
                "event_action": "click",
                "event_variables": "destination,checkin,checkout,adults,rooms,kids",
                "event_computed_variables": {},
            },
            {
                "event_name": "City clicked",
                "event_selector": ".find-treebo__btn",
                "event_source": "City",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    'city': '$(currentElement).find(".find-treebo__name").html()'
                },
            },
            {
                "event_name": "Spotlight link",
                "event_selector": ".spotlight__list-item",
                "event_source": "Spotlight",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    'link': '$(currentElement).find(".spotlight__link").attr("href")'
                },
            }

    ]
    },

    2: {
        "available_elements": {
            "destination": "$('#searchInput').val()",
            "checkin": "rt.getCheckin()",
            "checkout": "rt.getCheckout()",
            "adults": "rt.getAdults()",
            "rooms": "rt.getRooms()",
            "kids": "rt.getKids()",
        },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Quick Book Clicked",
                "event_selector": ".analytics-quickbook",
                "event_source": "Quick Book Button",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    'hotel_id': "$(currentElement).closest('.results__row').data('id')",
                    'hotel_name': "$(currentElement).closest('.results__row').find('.analytics-hotelname').attr('hotelname')",
                    'price': "$(currentElement).closest('.results__row').find('.a-price').attr('price')",
                    'discount_coupon': "$(currentElement).closest('.results__row').find('.a-promo').attr('promocode')",
                    'locality': "$(currentElement).closest('.results__row').find('.analytics-address ').attr('locality')",
                    'city': "$(currentElement).closest('.results__row').find('.analytics-address ').attr('city')",
                    'has_tags': "$(currentElement).closest('.results__row').find('.analytics-hotelname').attr('tags')",
                },
            },
            {

                "event_name": "Interstitial Clicked",
                "event_selector": ".analytics-marketingbanner",
                "event_source": "Interstitial Banner",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    'url': "$(currentElement).attr('href')",
                    'num_results': "$('.analytics-count').attr('count')",
                    'search_query': "$('#searchInput').val()",
                    'locality': "rt.locality",
                    'city': 'rt.city'
                },
            },
            {
                "event_name": "Right Widget Clicked",
                "event_selector": ".analytics-rightwidgets",
                "event_source": "Right Widgets",
                "event_action": "click",
                "event_variables": "login_text,element_name",
                "event_computed_variables": {
                    'locality': 'rt.locality',
                    'city': 'rt.city',
                    'num_results': "$('.analytics-count').attr('count')",
                    'search_query': "$('#searchInput').val()",
                },
            },
            {
                "event_category": "button_event",
                "event_name": "login",
                "event_selector": ".js-headerlogin",
                "event_source": "Clicked on Login",
                "event_action": "click",
                "event_variables": "login_text,element_name",
                "event_computed_variables": {},
            }

        ]
    },
    3: {
        "available_elements": {
            "checkin": "rt.getCheckin()",
            "checkout": "rt.getCheckout()",
            "hotel_name": "rt.hotelName",
            "hotel_id": "rt.hotelId",
            "adults": "rt.getAdults()",
            "rooms": "rt.getRooms()",
            "kids": "rt.getKids()",

        },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Book Now clicked",
                "event_selector": ".analytics-book",
                "event_source": "Clicked on Book",
                "event_action": "click",
                "event_variables": "hotel_name,hotel_id,checkin,checkout,adults,rooms,kids",
                "event_computed_variables": {
                    'total_price': '$(".analytics-totalprice").html()',
                    'room_type': "rt.hdmodel.roomType",
                    # 'discount_value':"$('.analytics-discountvalue').html()",
                    'discount_coupon_shown': 'rt.deal',
                    'discount_coupon_applied': 'rt.hdmodel.currentcoupon'
                },
            },
            {
                "event_name": "Modify Clicked",
                "event_selector": ".analytics-modify",
                "event_source": "Modify query",
                "event_action": "click",
                "event_variables": "hotel_name,hotel_id,checkin,checkout,adults,rooms,kids",
                "event_computed_variables": {},
            },
            {
                "event_name": "Similar Treebo Clicked",
                "event_selector": ".analytics-similar",
                "event_source": "Room Type Select",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    "base_hotel_name": "rt.hotelName",
                    "base_hotel_id": "rt.hotelId",
                    "other_hotel_name": "$(currentElement).find('.analytics-name').attr('hotelname')",
                    "other_hotel_id": "$(currentElement).attr('id')",
                    "price": "$(currentElement).find('.a-price').attr('price')"
                },
            }

        ]
    },
    4: {"available_elements": {
        'hotel_id': "rt.hotelId",
        'hotel_name': "rt.hotelName",

    },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Discount applied",
                "event_selector": ".analytics-apply",
                "event_source": "Discount applied",
                "event_action": "click",
                "event_variables": "hotel_id,hotel_name",
                "event_computed_variables": {
                    'discount_coupon': '$(".analytics-voucher").val()',
                    'discount_value': '$(".analytics-discountvalue").text()',
                    'discount_value': '$(".analytics-coupon-desc").text()',
                },
            },
            {
                "event_name": "Special request",
                "event_selector": ".analytics-special-req",
                "event_source": "Special reques",
                "event_action": "click",
                "event_variables": "hotel_id,hotel_name",
                "event_computed_variables": {
                    'request_description': '$("#guestRequest").val()',
                },
            },
            {
                "event_name": "Pay Clicked",
                "event_selector": ".analytics-pay",
                "event_source": "Pay clicked",
                "event_action": "click",
                "event_variables": "hotel_id, hotel_name",
                "event_computed_variables": {
                    "type": "$(currentElement).attr('paytype')",
                    "checkin": "$('.analytics-checkin').text()",
                    "checkin": "$('.analytics-checkout').text()",
                    "adults": "rt.getAdults()",
                    "kids": "rt.getKids()",
                    "rooms": "rt.getRooms()",
                    "total_price": "$('#grandTotal').html()",
                    "room_type": "rt.roomtype",
                    "discount_value": "$('#discountValue').html()",
                    "discount_coupon_applied": '$(".analytics-voucher").val()'
                },
            }
    ]
    },
    6: {
        "available_elements": {
            "source": "rt.source",
            "referenceId": "rt.referenceid",
            "type": "rt.type"
        },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Checkout Flow Async page loaded",
                "event_selector": "",
                "event_source": "",
                "event_action": "",
                "event_variables": "source,referenceId,type",
            }
        ]
    },
    51: {"available_elements": {
        "destination": "$('#searchInput').val()",
        "checkin": "rt.getCheckin()",
        "checkout": "rt.getCheckout()",
        "adults": "rt.getAdults()",
        "rooms": "rt.getRooms()",
        "kids": "rt.getKids()",
    },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Search Treebo",
                "event_selector": "#searchSubmitBtn",
                "event_source": "Search Treebo",
                "event_action": "click",
                "event_variables": "destination,checkin,checkout,adults,rooms,kids",
                "event_computed_variables": {},
            },
            {
                "event_name": "Find Treebo",
                "event_selector": ".find-treebo__btn",
                "event_source": "Find a Treebo",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {},
            },

            {
                "event_name": "City clicked",
                "event_selector": ".allcity__city",
                "event_source": "City",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    'city': '$(currentElement).find(".allcity__citylink").html()'
                },
            },
            {
                "event_name": "Spotlight link",
                "event_selector": ".a-spotlight",
                "event_source": "Spotlight",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    'link': '$(currentElement).attr("href")'
                },
            }

    ]
    },

    52: {
        "available_elements": {
            "destination": "$('#searchInput').val()",
            "checkin": "rt.getCheckin()",
            "checkout": "rt.getCheckout()",
            "adults": "rt.getAdults()",
            "rooms": "rt.getRooms()",
            "kids": "rt.getKids()",
        },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Hotel View Details Clicked",
                "event_selector": ".analytics-viewdetails",
                "event_source": "View Details Link",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    'type': "$(currentElement).attr('href')",
                    'hotel_name': "$(currentElement).closest('.results__row').find('.analytics-hotelname').attr('hotelname')",
                    'price': "$(currentElement).closest('.results__row').find('.a-price').attr('price')",
                    'discount_coupon': "$(currentElement).closest('.results__row').find('.a-promo').attr('promocode')",
                    'locality': "$(currentElement).closest('.results__row').find('.analytics-address ').attr('locality')",
                    'city': "$(currentElement).closest('.results__row').find('.analytics-address ').attr('city')",
                    'is_soldout': "$(currentElement).closest('.results__row').attr('soldout')",
                    'has_tags': "$(currentElement).closest('.results__row').find('.analytics-hotelname').attr('tags')",

                },
            },
            {
                "event_name": "Quick Book Clicked",
                "event_selector": ".analytics-quickbook",
                "event_source": "Quick Book Button",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    'hotel_id': "$(currentElement).closest('.results__row').data('id')",
                    'hotel_name': "$(currentElement).closest('.results__row').find('.analytics-hotelname').attr('hotelname')",
                    'price': "$(currentElement).closest('.results__row').find('.a-price').attr('price')",
                    'discount_coupon': "$(currentElement).closest('.results__row').find('.a-promo').attr('promocode')",
                    'locality': "$(currentElement).closest('.results__row').find('.analytics-address ').attr('locality')",
                    'city': "$(currentElement).closest('.results__row').find('.analytics-address ').attr('city')",
                    'has_tags': "$(currentElement).closest('.results__row').find('.analytics-hotelname').attr('tags')",
                },
            },
            {

                "event_name": "Interstitial Clicked",
                "event_selector": ".analytics-marketingbanner",
                "event_source": "Interstitial Banner",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    'url': "$(currentElement).attr('href')",
                    'num_results': "$('.analytics-count').attr('count')",
                    'search_query': "$('#searchInput').val()",
                    'locality': "rt.locality",
                    'city': 'rt.city'
                },
            },
            {
                "event_name": "Right Widget Clicked",
                "event_selector": ".analytics-rightwidgets",
                "event_source": "Right Widgets",
                "event_action": "click",
                "event_variables": "login_text,element_name",
                "event_computed_variables": {
                    'locality': 'rt.locality',
                    'city': 'rt.city',
                    'num_results': "$('.analytics-count').attr('count')",
                    'search_query': "$('#searchInput').val()",
                },
            },
            {
                "event_category": "button_event",
                "event_name": "login",
                "event_selector": ".js-headerlogin",
                "event_source": "Clicked on Login",
                "event_action": "click",
                "event_variables": "login_text,element_name",
                "event_computed_variables": {},
            }

        ]
    },
    53: {
        "available_elements": {
            "checkin": "rt.getCheckin()",
            "checkout": "rt.getCheckout()",
            "hotel_name": "rt.hotelName",
            "hotel_id": "rt.hotelId",
            "adults": "rt.getAdults()",
            "rooms": "rt.getRooms()",
            "kids": "rt.getKids()",

        },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Book Now clicked",
                "event_selector": ".analytics-book",
                "event_source": "Clicked on Book",
                "event_action": "click",
                "event_variables": "hotel_name,hotel_id,checkin,checkout,adults,rooms,kids",
                "event_computed_variables": {
                    'total_price': '$(".analytics-totalprice").html()',
                    'room_type': "rt.hdmodel.roomType",
                    'discount_value': "$('.analytics-discountvalue').html()",
                    'discount_coupon_shown': 'rt.deal',
                    'discount_coupon_applied': 'rt.hdmodel.currentcoupon'
                },
            },
            {
                "event_name": "Room Type Select Clicked",
                "event_selector": ".analytics-select",
                "event_source": "Room Type Select",
                "event_action": "click",
                "event_variables": "hotel_name,hotel_id",
                "event_computed_variables": {
                    'room_type': "rt.hdmodel.roomType",

                },
            },
            {
                "event_name": "Similar Treebo Clicked",
                "event_selector": ".analytics-similar",
                "event_source": "Room Type Select",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {
                    "base_hotel_name": "rt.hotelName",
                    "base_hotel_id": "rt.hotelId",
                    "other_hotel_name": "$(currentElement).find('.analytics-name').attr('name')",
                    "other_hotel_id": "$(currentElement).attr('id')",
                    "price": "$(currentElement).find('.analytics-price').attr('price')",
                    "discount_coupon_shown": "$(currentElement).find('.analytics-coupon') ? $(currentElement).find('.analytics-coupon').attr('coupon') : ''"
                },
            }

        ]
    },
    54: {"available_elements": {
        'hotel_id': "rt.hotelId",
        'hotel_name': "rt.hotelName",

    },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Continue Guest Clicked",
                "event_selector": ".analytics-guest",
                "event_source": "Guest Login",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {},
            },
            {
                "event_name": "Signup Clicked",
                "event_selector": ".analytics-signup",
                "event_source": "Signup",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {},
            },
            {
                "event_name": "Email Login Clicked",
                "event_selector": ".analytics-emaillogin",
                "event_source": "Email Login",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {},
            },
            {
                "event_name": "FB Login Clicked",
                "event_selector": ".analytics-fblogin",
                "event_source": "Facebook Login",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {},
            },
            {
                "event_name": "Google Login Clicked",
                "event_selector": ".analytics-glogin",
                "event_source": "Google Login",
                "event_action": "click",
                "event_variables": "",
                "event_computed_variables": {},
            },
            {
                "event_name": "Pay Clicked",
                "event_selector": ".analytics-pay",
                "event_source": "Pay clicked",
                "event_action": "click",
                "event_variables": "hotel_id, hotel_name",
                "event_computed_variables": {
                    "type": "$(currentElement).attr('paytype')",
                    "checkin": "$('.analytics-checkin').attr('checkin')",
                    "checkin": "$('.analytics-checkout').attr('checkout')",
                    "adults": "rt.getAdults()",
                    "kids": "rt.getKids()",
                    "rooms": "rt.getRooms()",
                    "total_price": "$('#grandTotal').html()",
                    "room_type": "rt.roomtype",
                    "discount_value": "$('#discountValue').html()",
                    "discount_coupon_applied": "$('.analytics-coupon').html()"
                },
            }
    ]
    },
    56: {
        "available_elements": {
            "source": "rt.source",
            "referenceId": "rt.referenceid",
            "type": "rt.type"
        },
        "element_override": {},
        "events_list": [
            {
                "event_name": "Checkout Flow Async page loaded",
                "event_selector": "",
                "event_source": "",
                "event_action": "",
                "event_variables": "source,referenceId,type",
            }
        ]
    }
}
