# -*- coding: utf-8 -*-


from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from apps.profiles.api.v1.change_password import ChangePasswordAPI
from apps.profiles.api.v1.create_or_update_user import CreateOrUpdateUPSUser
from apps.profiles.api.v1.forgot_password import ForgotPasswordAPI
from apps.profiles.api.v1.notification_settings import NotificationSettingsAPI
from apps.profiles.api.v1.reset_password import ResetPasswordAPI
from apps.profiles.api.v1.update_profile import SaveProfile

urlpatterns = [
    url(r"^$", SaveProfile.as_view(), name='save-profile'),
    url(r'^change-password/?$',
        login_required(
            ChangePasswordAPI.as_view(),
            redirect_field_name="referrer"),
        name="change-password-api"),
    url(r'^forgot-password/?$',
        ForgotPasswordAPI.as_view(),
        name="forgot-password-api"),
    url(r'^reset-password/?$',
        ResetPasswordAPI.as_view(),
        name="reset-password-api"),
    url(r'^create-or-update-user/?',
        CreateOrUpdateUPSUser.as_view(),
        name="create-or-update-user-api"),
    # Not used
    url(r'^notification-settings/?$',
        login_required(
            NotificationSettingsAPI.as_view(),
            redirect_field_name="referrer"),
        name="notification-settings-api"),
]
