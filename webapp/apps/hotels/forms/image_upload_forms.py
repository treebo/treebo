from django import forms
from django.utils.safestring import mark_safe


class HotelImageForm(forms.Form):
    hotel_id = forms.IntegerField(widget=forms.NumberInput, help_text=mark_safe(
        "Enter the id of the hotel correctly. You should enter id or hotelogix id"))
    images = forms.FileField(
        label='Zipped Images',
        required=True,
        widget=forms.FileInput,
        help_text=mark_safe("Please upload zip with a separate folder for each room type."),
    )


class ConfirmHotelImageUpload(forms.Form):
    confirm_upload_dummy = forms.HiddenInput()
