# -*- coding:utf-8 -*-
"""
    BookingRequestAPI
"""
from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response

from apps.bookings.services.booking_request import BookingRequestServices
from base import log_args
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger

logger = get_booking_logger(__name__)


class BookingRequestAPI(TreeboAPIView):
    """
        BookingRequestAPI
    """

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        return super(BookingRequestAPI, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request):
        """
        :param request:
        :return:
        """
        user_email = request.GET.get('email')
        user_phone = request.GET.get('mobile')
        logged_user_id = ''
        logger.debug("User details {0} {1}".format(user_email, user_phone))

        try:
            if not user_email or not user_phone:
                return Response(
                    {"error": {"msg": "email and phone is required"}}, status=400)

            if request.user and request.user.is_authenticated():
                logged_user_id = str(request.user.id)

            booking_request_service = BookingRequestServices()
            booking_request = booking_request_service.get_existing_booking_request(
                user_email, user_phone, logged_user_id)
            if not booking_request:
                logger.debug(
                    "No booking request found for {0} {1}".format(
                        user_email, user_phone))
                return Response({
                    "is_booking_requested": False,
                    "coupon": str(settings.REFERRALCODE)
                })
            else:
                logger.debug(
                    "Booking request found for {0} {1}".format(
                        booking_request.email,
                        booking_request.phone))
                return Response({
                    "is_booking_requested": True,
                    "coupon": ""
                })
        except Exception as exc:
            logger.exception(exc)
            return Response(
                {"error": {"msg": "Error occurred while fetching booking_request"}}, status=400)

    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        raise MethodNotAllowed("POST")
