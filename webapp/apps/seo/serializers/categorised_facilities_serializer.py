from rest_framework import serializers


class ItemsDTO(serializers.Serializer):
    name = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    show = serializers.BooleanField(default=False)


class ContentDTO(serializers.Serializer):
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    items = ItemsDTO(required=False, many=True, allow_null=True)


class FacilitiesDTO(serializers.Serializer):
    content = ContentDTO(required=False, many=True, allow_null=True)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
