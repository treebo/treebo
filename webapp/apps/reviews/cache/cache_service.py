import json
import logging
import traceback

from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.reviews.cache.builders.cache_builder_factory import CacheBuilderFactory
from apps.reviews.cache.cache_client import CacheClient

logger = logging.getLogger(__name__)


class CacheService(object):
    cache_client = CacheClient()
    cache_factory = CacheBuilderFactory()

    def get_reviews_from_cache(self, hotel_ids, page):
        hotel_wise_cache_builders = self.cache_factory.get_builders(hotel_ids, page)
        cache_key_to_hotel_id_mapping = {builder.key: hotel_id for hotel_id, builder in
                                         hotel_wise_cache_builders.items()}

        cached_values = self.cache_client.get_many(list(cache_key_to_hotel_id_mapping.keys()))

        hotel_wise_cached_data = {}
        hotel_wise_data_to_set_in_cache = {}

        for cache_key, cached_data in cached_values.items():
            hotel_id = cache_key_to_hotel_id_mapping.get(cache_key)
            hotel_wise_cached_data[hotel_id] = json.loads(cached_data)

        for hotel_id in hotel_ids:
            if not hotel_wise_cached_data.get(hotel_id, None):
                builder = hotel_wise_cache_builders.get(hotel_id)
                builder.build()
                hotel_wise_data_to_set_in_cache[builder.key] = builder.json
                hotel_wise_cached_data[hotel_id] = json.loads(builder.json)

        if hotel_wise_data_to_set_in_cache:
            self.cache_client.set_many(hotel_wise_data_to_set_in_cache)

        return hotel_wise_cached_data

    def get_or_update_cache(self, hotel_id, page):
        try:
            builder_obj = self.cache_factory.get_builder(hotel_id, page)
            if builder_obj:
                value = self.cache_client.get_value(builder_obj.key)
                if not value:
                    builder_obj.build()
                    self.update_cache(builder_obj)
                    value = builder_obj.json
                return json.loads(value)
            else:
                return None
        except Exception as ex:
            logger.exception('Exception occured in cache get or update')
            slack_alert.review_alert(
                "Exception occured in get or update cache for hotel id : %s and page : %s. "
                "Exception :: %s", hotel_id, page, traceback.format_exc())
            return None

    def update_cache(self, builder_obj):
        try:
            # Was throwing error
            if self.cache_client.has_key(builder_obj.key):
                item = self.cache_client.get_value(builder_obj.key)
                if json.loads(item)['created_date'] < json.loads(
                        builder_obj.json)['created_date']:
                    self.cache_client.set_value(
                        builder_obj.key, builder_obj.json)
                else:
                    logger.info('Latest entry exists in cache')
            else:
                self.cache_client.set_value(builder_obj.key, builder_obj.json)
        except Exception as ex:
            logger.exception('Exception occured in CacheService update_cache.')
            slack_alert.review_alert(
                "Exception occured while updating the cache for the key : %s."
                " Exception :: %s ", builder_obj.key, traceback.format_exc())
