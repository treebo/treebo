#from . import signals

from django.apps import AppConfig

default_app_config = 'apps.bookingstash.BookingStashAppConfig'

class BookingStashAppConfig(AppConfig):

    name = 'apps.bookingstash'

    def ready(self):
        from . import signals
