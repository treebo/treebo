import datetime
from datetime import timedelta
from random import randint

from common.exceptions.treebo_exception import TreeboValidationException
from dbcommon.models.profile import Otp
from services.notification.sms.sms_service import SmsService


class OtpService():
    OTP_TEMPLATE = "Dear {0}, Your OTP for completing registration is {1}. Thank you! Treebo Hotels"

    def __int__(self):
        super(OtpService, self).__int__(self)

    @staticmethod
    def send_otp(mobile, name="Guest", OTP_TEMPLATE=OTP_TEMPLATE):
        """
        Sends an otp to the user
        :param (str) mobile:
        :return (int) otp sent
        """
        if mobile and mobile.isdigit():
            otp = OtpService.__find_existing_otp(mobile)
            if not otp:
                generated_otp = OtpService.__generate_otp()
                otp = Otp()
                otp.otp = generated_otp
                otp.active = True
                otp.mobile = mobile
                otp.save()
            SmsService.send_sms(
                [mobile], OTP_TEMPLATE.format(
                    name, otp.otp), high_priority=True)
        else:
            raise TreeboValidationException("Mobile number is not a number")

    @staticmethod
    def __find_existing_otp(mobile):
        prev_fifteen_mins = datetime.datetime.now() - timedelta(minutes=15)
        otp = Otp.objects.filter(
            mobile=mobile,
            active=True,
            created_at__gte=prev_fifteen_mins).order_by('-id').first()
        return otp

    @staticmethod
    def __generate_otp():
        return str(randint(100000, 999999))

    @staticmethod
    def veriy_otp(mobile, otp_number):
        """
        Verifies if the otp is valid for this number
        :param (str) mobile:
        :param (str) otp_number
        :return (Boolean) if valid
        """
        is_valid = False
        if mobile.isdigit() and otp_number.isdigit():
            try:
                prev_fifteen_mins = datetime.datetime.now() - timedelta(minutes=15)
                otp = Otp.objects.filter(
                    mobile=mobile,
                    otp=otp_number,
                    active=True,
                    created_at__gte=prev_fifteen_mins).order_by('-id').first()
                if otp:
                    otp.active = False
                    otp.save()
                    is_valid = True
            except Otp.DoesNotExist:
                return is_valid
        else:
            raise TreeboValidationException(
                "Mobile number or otp is not a number")

        return is_valid
