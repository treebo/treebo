# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0080_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='CityCategoryMapforAutoPageCreation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('is_enabled', models.BooleanField(default=True)),
                ('category', models.ForeignKey(blank=True, to='dbcommon.Categories', null=True)),
                ('city', models.ForeignKey(to='dbcommon.City')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'City Category Map for AutoPage Creation',
                'verbose_name_plural': 'City Category Map for AutoPage Creation',
                'default_permissions': ('add', 'change', 'delete', 'read'),
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AlterUniqueTogether(
            name='cityamenitymapforautopagecreation',
            unique_together=set([('city', 'amenity')]),
        ),
        migrations.AlterModelTable(
            name='cityamenitymapforautopagecreation',
            table='dbcommon_cityamenitymapforautopagecreation',
        ),
        migrations.AlterUniqueTogether(
            name='citycategorymapforautopagecreation',
            unique_together=set([('city', 'category')]),
        ),
    ]
