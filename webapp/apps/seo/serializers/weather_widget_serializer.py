from rest_framework import serializers


class ForecastSerializer(serializers.Serializer):
    date = serializers.CharField(max_length=1000)
    summary = serializers.CharField(max_length=1000)
    icon = serializers.CharField(max_length=1000)
    avg_temp = serializers.DecimalField(decimal_places=1, max_digits=5)


class WeatherWidgetSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=1000)
    content = ForecastSerializer(many=True)
