import logging

from django.db.models import Q
from django.utils.encoding import smart_str

from apps.reviews import constants
from apps.reviews.models import TAHotelIDMapping, TAHotelReviews, ReviewAppConfiguration

logger = logging.getLogger(__name__)


class HotelReviewsDataService(object):
    def hotel_reviews_data(
        self,
        location_review_serializer,
        ta_loc_id,
        user_generated=False):
        try:
            ta_hotel_map = TAHotelIDMapping.objects.filter(
                ta_location_id=ta_loc_id).first()
            if ta_hotel_map:
                logger.info(
                    "Mapping between hotel %s and ta %s exists in HotelReviewsDataService",
                    ta_hotel_map.hotel.id,
                    ta_hotel_map.ta_location_id)
                location_reviews = location_review_serializer.data
                location_reviews_data = location_reviews["data"]
                self.create_or_update_review_data(
                    location_reviews_data, ta_hotel_map, user_generated)
            else:
                logger.info(
                    "No mapping for the given tid exists %s for review data",
                    ta_loc_id)

        except Exception:
            logger.exception(
                "Exception in creating new entry in review data table for %s",
                ta_loc_id)

    def create_or_update_review_data(
        self,
        location_reviews_data,
        ta_hotel_map,
        user_generated=False):
        try:
            for review in location_reviews_data:
                try:
                    review_obj, create = TAHotelReviews.objects.get_or_create(
                        hotel_ta_mapping=ta_hotel_map, ta_review_id=smart_str(review['id']))
                    if create:
                        logger.info(
                            "create %s in Review data   .. create/update for hotel %s and ta_loc_id %s",
                            create,
                            ta_hotel_map.hotel.id,
                            ta_hotel_map.ta_location_id)

                except TAHotelReviews.MultipleObjectsReturned:
                    logger.info(
                        "MultipleObjectsReturned Exception in updating review data for hotel id %s and ta_loc_id %s",
                        ta_hotel_map.hotel.id,
                        ta_hotel_map.ta_location_id)
                    review_obj = TAHotelReviews.objects.filter(
                        hotel_ta_mapping=ta_hotel_map, ta_review_id=smart_str(
                            review['id'])).first()

                self.update_review_data_table(
                    review, review_obj, user_generated)

        except Exception:
            logger.exception(
                "Exception in saving review data object for %s", smart_str(
                    location_reviews_data[0]['location_id']))

    def __get_formatted_text(self, _str):
        if _str:
            return smart_str(_str.replace('"', ''))
        else:
            return ''

    def update_review_data_table(
        self,
        review,
        review_obj,
        user_generated=False):
        review_obj.review_text = self.__get_formatted_text(review.get('text'))
        review_obj.review_title = self.__get_formatted_text(
            review.get('title'))
        review_obj.review_published_date = smart_str(
            review.get('published_date'))
        review_obj.helpful_votes = smart_str(
            review.get('helpful_votes')) if smart_str(
            review.get('helpful_votes')) else 0
        review_obj.user_rating = smart_str(review.get('rating'))
        review_obj.user_rating_image_url = smart_str(
            review.get('rating_image_url'))
        review_obj.user_rating_text_url = smart_str(review.get('url'))
        review_obj.review_is_user_generated = user_generated
        review_obj.review_is_crawlable = review.get('partner_crawlable')
        user_name = review['user']['username']
        if user_name:
            review_obj.user_name = self.__get_formatted_text(
                review['user']['username'])
        else:
            review_obj.user_name = constants.DEFAULT_GUEST_NAME

        # TODO: user image url
        # review_obj.user_image_url = smart_str(review['user']['image'])
        if review['owner_response']:
            review_obj.owner_rating_reply_text = self.__get_formatted_text(
                review['owner_response'].get('text'))
            review_obj.owner_description = self.__get_formatted_text(
                review['owner_response'].get('author'))
            review_obj.owner_reply_published_date = self.__get_formatted_text(
                review['owner_response'].get('published_date'))
        review_obj.save()

    def fetch_review_data_hotel_wise(self, ta_loc_id=None, hotel_id=None):
        try:
            if ta_loc_id or hotel_id:
                reviews_data = TAHotelReviews.objects.filter(
                    Q(hotel_ta_mapping__ta_location_id=ta_loc_id) | Q(
                        hotel_ta_mapping__hotel__id=hotel_id))
                if reviews_data:
                    return reviews_data
                else:
                    logger.error(
                        "No reviews_data exist for ta_loc_id %s and hotel_id %s",
                        ta_loc_id,
                        hotel_id)
                    return None
            else:
                logger.error(
                    "both ta_loc_id and hotel_id are None in fetching reviews_data")
        except Exception:
            logger.exception(
                "Exception in fetching the reviews_data for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)
            return None

    def delete_reviews_data_hotel_wise(self, ta_loc_id=None, hotel_id=None):
        try:
            reviews_data = self.fetch_review_data_hotel_wise(
                ta_loc_id=ta_loc_id, hotel_id=hotel_id)
            for rev_data in reviews_data:
                rev_data.delete()
                logger.info(
                    "Deleted reviews_data successfully for ta_loc_id %s and hotel-id %s",
                    ta_loc_id,
                    hotel_id)
            else:
                logger.error(
                    "No reviews_data exist for ta_loc_id %s and hotel_id %s",
                    ta_loc_id,
                    hotel_id)
        except Exception:
            logger.exception(
                "Exception in deleting reviews_data for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)

    def fetch_non_user_latest_reviews(self, ta_loc_id=None, hotel_id=None):
        try:
            if ta_loc_id or hotel_id:
                app_config = ReviewAppConfiguration.objects.filter().first()
                reviews_data = TAHotelReviews.objects.filter(
                    Q(hotel_ta_mapping__ta_location_id=ta_loc_id) | Q(
                        hotel_ta_mapping__hotel__id=hotel_id),
                    review_is_active=True, review_is_user_generated=False).order_by('-modified_at')[
                               :app_config.num_non_user_reviews_to_show]
                if reviews_data:
                    return reviews_data
                else:
                    logger.error(
                        "No reviews_data exist for ta_loc_id %s and hotel_id %s",
                        ta_loc_id,
                        hotel_id)
                    return None
            else:
                logger.error(
                    "both ta_loc_id and hotel_id are None in fetching reviews_data")
        except Exception:
            logger.exception(
                "Exception in fetching the reviews_data for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)
            return None

    def fetch_user_reviews(self, ta_loc_id=None, hotel_id=None):
        """
        Finds all the reviews for that location and hotel
        :rtype: [TAHotelReviews]
        :param ta_loc_id:
        :param hotel_id:
        :return [TAHotelReviews]:
        """
        try:
            if ta_loc_id or hotel_id:
                reviews_data = TAHotelReviews.objects.filter(
                    Q(hotel_ta_mapping__ta_location_id=ta_loc_id) | Q(
                        hotel_ta_mapping__hotel__id=hotel_id),
                    review_is_active=True, review_is_user_generated=True).order_by('-user_rating',
                                                                                   '-review_published_date')
                if reviews_data:
                    return reviews_data
                else:
                    logger.error(
                        "No user reviews_data exist for ta_loc_id %s and hotel_id %s",
                        ta_loc_id,
                        hotel_id)
                    return None
            else:
                logger.error(
                    "both ta_loc_id and hotel_id are None in fetching reviews_data")
        except Exception:
            logger.exception(
                "Exception in fetching the user reviews_data for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)
            return None
