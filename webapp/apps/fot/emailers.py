import datetime
import requests
from datetime import timedelta

from django.core import mail
from django.core.mail import BadHeaderError, EmailMultiAlternatives
from django.template import Context
from django.conf import settings
from django.utils.encoding import smart_str
import logging

logger = logging.getLogger(__name__)
from django.template.loader import get_template

from apps.fot.models import Fot, FotReservationsRequest


def compile_email(from_email, to_email, subject, context, template):
    if from_email == "":
        from_email = 'Friends of Treebo <friends@treebohotels.com>'
    text_template = 'desktop/fot/emailers/' + template + '.txt'
    html_template = 'desktop/fot/emailers/' + template + '.html'
    text_content = get_template(text_template).render(context)
    html_content = get_template(html_template).render(context)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
    msg.attach_alternative(html_content, "text/html")
    return msg


def send_email(messages):
    try:
        connection = mail.get_connection()
        connection.open()
        try:
            connection.send_messages(messages)
            connectionStatus = 'Mail sent sucessfully'
        except BadHeaderError:
            connectionStatus = 'Invalid header found.'
        connection.close()
    except BaseException:
        connectionStatus = 'Connection error in sending mails'
    return connectionStatus


def send_sms(sms_template, to_mobile, context):
    text_template = 'desktop/fot/emailers/' + sms_template + '.txt'
    text_content = get_template(text_template).render(context)
    sms_url = str(settings.SMS_VENDOR_URL) + "?method=SendMessage&msg_type=TEXT&userid=" + str(
        settings.SMS_VENDOR_ID) + "&auth_scheme=plain&password=" + str(
        settings.SMS_VENDOR_PASSWORD) + "&v=1.1&format=text&send_to=" + str(
        to_mobile) + "&msg=" + smart_str(text_content)
    logger.info(requests.get(sms_url).text)
    return 'Sms succesfully sent'


def signup_confirmation(email, confirmationKey):
    fot = Fot.objects.get(user__email=email)
    confirmationKey = confirmationKey
    to_email = email
    messages = list()
    context = Context({'first_name': fot.user.first_name,
                       'confirmation_key': confirmationKey})
    msg = compile_email(
        '',
        to_email,
        'Thank you for signing up!',
        context,
        'SignupConfirmation')
    messages.append(msg)
    status = send_email(messages)
    return status


def registration_confirmation(email, status):
    fot = Fot.objects.get(user__email=email)
    to_email = email
    context = Context({'first_name': fot.user.first_name})
    messages = list()
    if status == "PASSED":
        subject = 'Congratulations! You are now a Friend of Treebo'
        template = 'RegistrationSuccess'
    else:
        subject = 'Thank you for applying for Friends of Treebo'
        template = 'RegistrationFailed'
    msg = compile_email('', to_email, subject, context, template)
    messages.append(msg)
    status = send_email(messages)
    return status


def audit_reminder():
    today = datetime.date.today()
    tomorrow = today + timedelta(days=1)
    reservations = FotReservationsRequest.objects.filter(
        status='SCHEDULED', check_in_date=tomorrow)
    messages = list()
    for u in reservations:
        context = Context({'first_name': u.user.first_name,
                           'hotel_name': u.hotel.name,
                           'city': u.hotel.city.name,
                           'date_of_audit': tomorrow,
                           'no_of_guests': u.no_of_guests})
        to_email = u.user.email
        to_mobile = u.user.phone_number
        msg = compile_email(
            '',
            to_email,
            'Your audit is coming up!',
            context,
            'AuditReminder')
        messages.append(msg)
        send_sms('AuditReminder', to_mobile, context)
    status = send_email(messages)
    return status


def checkout_reminder():
    today = datetime.date.today()
    users = FotReservationsRequest.objects.filter(
        status__in=['CHECKEDIN', 'CHECKEDOUT'], check_out_date=today)
    messages = list()
    for u in users:
        context = Context({'first_name': u.user.first_name,
                           'hotel_name': u.hotel.name,
                           'city': u.hotel.city.name,
                           'date_of_checkout': today,
                           'no_of_guests': u.no_of_guests})
        to_email = u.user.email
        msg = compile_email(
            '',
            to_email,
            'Hope you had a great audit!',
            context,
            'CheckoutReminder')
        messages.append(msg)
    status = send_email(messages)
    return status


def audit_confirmation(bookingId):  # req: bookingId
    u = FotReservationsRequest.objects.get(booking_id=bookingId)
    messages = list()
    context = Context({'first_name': u.user.first_name,
                       'hotel_name': u.hotel.name,
                       'city': u.hotel.city.name,
                       'date_of_checkout': u.check_out_date,
                       'audit_date': u.check_in_date,
                       'no_of_guests': u.no_of_guests})
    to_email = u.user.email
    msg = compile_email(
        '',
        to_email,
        'Important: Instructions for conducting your audit',
        context,
        'AuditConfirmation')
    messages.append(msg)
    status = send_email(messages)
    return status


def audit_cancellation(bookingId):  # req: bookingId
    u = FotReservationsRequest.objects.get(booking_id=bookingId)
    messages = list()
    context = Context({'first_name': u.user.first_name,
                       'hotel_name': u.hotel.name,
                       'city': u.hotel.city.name,
                       'audit_date': u.check_in_date,
                       'no_of_guests': u.no_of_guests})
    to_email = u.user.email
    msg = compile_email(
        '',
        to_email,
        'Your audit at ' +
        u.hotel.name +
        ' is cancelled !',
        context,
        'AuditCancellation')
    messages.append(msg)
    status = send_email(messages)
    return status


def feedback_submission_confirmation(users):
    messages = list()
    for u in users:
        context = Context({'first_name': u.user.first_name,
                           'hotel_name': u.hotel.name,
                           'city': u.hotel.city.name,
                           'date_of_checkout': u.check_out_date,
                           'no_of_guests': u.no_of_guests})
        to_email = u.user.email
        msg = compile_email(
            '',
            to_email,
            'Thank you for your feedback!',
            context,
            'FeedbackSubmissionConfirmation')
        messages.append(msg)
    status = send_email(messages)
    return status


# for sending blocking mails - will be called from custom command
def account_blocking_confirmation(users, is_blocked_by_sub_routines):
    messages = list()
    for u in users:
        if is_blocked_by_sub_routines:
            context = Context({'first_name': u.user.first_name,
                               'hotel_name': u.hotel.name,
                               'city': u.hotel.city.name,
                               'date_of_checkout': u.check_out_date,
                               'no_of_guests': u.no_of_guests})
        else:
            # This block is to send mail if blocked from dashboard
            context = Context({'first_name': u.user.first_name})

        template = 'AccountBlockingConfirmation'
        to_email = u.user.email
        msg = compile_email(
            '',
            to_email,
            'Your account is blocked!',
            context,
            template)
        messages.append(msg)
    status = send_email(messages)
    return status


def account_unblocking_confirmation(userId):  # req: userId
    u = Fot.objects.get(id=userId)
    messages = list()
    context = Context({'first_name': u.user.first_name})
    to_email = u.user.email
    msg = compile_email(
        '',
        to_email,
        'Hurray! Your account is unblocked now',
        context,
        'AccountUnblockingConfirmation')
    messages.append(msg)
    status = send_email(messages)
    return status


def coupon_confirmation(userId, couponcode, credits_redeem):
    u = Fot.objects.get(user_id=userId)
    messages = list()
    context = Context({'first_name': u.user.first_name,
                       'couponcode': couponcode, 'rupees': credits_redeem})
    to_email = u.user.email
    msg = compile_email(
        '',
        to_email,
        'Hurray! Your code to free nights is here ',
        context,
        'CouponConfirmation')
    messages.append(msg)
    status = send_email(messages)
    return status
