import logging

import openpyxl
from django.core.management import BaseCommand

from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.facilities import Facility

logger = logging.getLogger(__name__)

room_data_service = RepositoriesFactory.get_room_repository()


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            self.updateAmenities()
        except Exception as exc:
            logger.debug(exc)

    def updateAmenities(self):
        allFacilities = Facility.objects.all()
        alreadyPresentFacilities = []
        newFacilities = [
            'Balcony',
            'Kitchenett',
            'Living room',
            'Coffee table',
            'Dining Table',
            'Cable/DTH',
            'Fridge',
            'Microwave',
            'Stove',
            'Bath tub',
            'Shower cabinet',
            'Safety Locker',
            'Smoking room Available']

        for facility in allFacilities:
            alreadyPresentFacilities.append(facility.id)

        self.addNewFacilities(newFacilities)

    def addNewFacilities(self, newFacilities):
        hotelId = 0
        rowNumber = 1
        roomCode = ""
        for newFacility in newFacilities:
            newObject, created = Facility.objects.get_or_create(
                name=newFacility, defaults={'url': '', 'css_class': ''})

        wb = openpyxl.load_workbook('webapp/static/files/room_details.xlsx')
        sheet = wb['Sheet1']
        requiredList = [
            'hotel',
            'city',
            'roomType',
            'roomSize',
            'maxAdults',
            'maxChildren',
            'maxAdultAndChildren',
            'Balcony',
            'Kitchenett',
            'Living room',
            'Coffee table',
            'Dining Table',
            'Cable/DTH',
            'Fridge',
            'Microwave',
            'Stove',
            'Bath tub',
            'Shower cabinet',
            'Safety Locker',
            'Smoking room Available']
        allRows = sheet.rows
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        for tuples in allRows:
            i = 0
            for column in tuples:
                value = column.value
                if value is not None:
                    try:
                        if requiredList[i] == 'hotel':
                            hotelId = hotel_repository.get_single_hotel(
                                name=value).id
                        if requiredList[i] == 'roomType':
                            roomCode = value
                        if i > 6:
                            if value.lower() == 'y' or value.lower() == 'dth':
                                facility = Facility.objects.get(
                                    name=requiredList[i])
                                facility.rooms.add(
                                    room_data_service.get_single_Room(
                                        hotelId=hotelId, room_type_code=roomCode))
                    except Exception as exc:
                        logger.debug(exc)
                i = i + 1
            rowNumber += 1
