import json
import logging

import requests
from django.core.urlresolvers import reverse

from apps.content.models import ContentStore
from services.restclient.request_wrapper import wrap_internal_api_call

logger = logging.getLogger(__name__)


class ContentServiceClient:
    @staticmethod
    def getValueForKey(request, key, version):
        # Wrap build_absolute_uri, to hit to localhost, instead of domain, for
        # internal API calls
        try:
            content = ContentStore.objects.get(name=key, version=version)
            if isinstance(content.value, dict):
                return content.value
            return json.loads(content.value)
        except ContentStore.DoesNotExist:
            logger.warning(
                "ContentStore doesn't exist for key: %s and version: %s",
                key,
                version)
            return None
        except BaseException:
            logger.exception("Error loading json for %s", key)
            return None
