class FaqRequest(object):
    def __init__(self, page, city=None, hotel=None, locality=None,
                 landmark=None, category=None, offset: int = 0, limit: int = 5):
        self.page = page
        self.city = city
        self.hotel = hotel
        self.locality = locality
        self.landmark = landmark
        self.category = category
        self.offset = offset
        self.limit = limit
        self.query = None
