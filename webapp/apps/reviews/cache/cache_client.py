from django.conf import settings

from apps.reviews.subscribers.connection import RedisCache

from django.core.cache import cache


class CacheClient(object):

    def get_many(self, keys):
        return cache.get_many(keys)

    def set_many(self, key_value_pairs_to_cache):
        cache.set_many(key_value_pairs_to_cache)

    def get_value(self, key):
        try:
            return cache.get(key)
        except Exception as e:
            return None

    def set_value(self, key, value, timeout=None):
        if not timeout:
            timeout = settings.REVIEW_CACHE_TIMEOUT
        cache.set(key, value, timeout)

    def has_key(self, key):
        return key in cache
