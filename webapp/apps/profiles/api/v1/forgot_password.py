import hashlib
import logging
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from apps.auth.dto.forgot_password_dto import ForgotPasswordDTO
from apps.auth.services.password_service import PasswordService
from apps.common import utils
from base import log_args
from rest_framework.response import Response
from base.views.api import TreeboAPIView
from common.constants import error_codes, common_constants

__author__ = 'varunachar'

logger = logging.getLogger(__name__)


class ForgotPassValidator(serializers.Serializer):
    email = serializers.EmailField(required=True, error_messages={
        'required': error_codes.INVALID_EMAIL,
        'blank': error_codes.INVALID_EMAIL,
        'invalid': error_codes.INVALID_EMAIL
    })


class ForgotPasswordAPI(TreeboAPIView):
    # validationSerializer = ForgotPassValidator
    authentication_classes = []
    forgot_password_service = PasswordService()

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ForgotPasswordAPI, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, format=None):
        return {}

    @log_args(logger)
    def post(self, request, format=None):
        email = request.data.get('email')
        utils.trackAnalyticsServerEvent(
            request, 'Forgot Password Click', {
                'email': email})
        forgot_password_dto = ForgotPasswordDTO(data={'email': email})
        forgot_password_dto.is_valid()
        self.forgot_password_service.send_forgot_password_email(
            forgot_password_dto.data)
        data = {
            "status": "success",
            "msg": "Reset password link sent successfully"}
        return Response(data)
