
from django.conf.urls import url
from django.views.decorators.cache import cache_page

from apps.hotels.api.v1.availability import HotelAvailability
from apps.hotels.api.v2.availability import HotelAvailability as HotelsAvailability
from apps.hotels.api.v2.hotel_statuses import HotelStatus
from apps.hotels.api.v2.localities import Localities
from apps.hotels.api.v2.hotel_detail import HotelDetail
from apps.hotels.api.v2.hotel_details import HotelDetails
from apps.hotels.api.v2.hotels import HotelList

app_name = 'hotels_v2'
urlpatterns = [
    url(r"^(?:(?P<hotel_id>[0-9]+)/)?$", HotelList.as_view(), name='hotel-list'),
    url(r"^(?P<hotel_id>[0-9]+)/availability/$", HotelAvailability.as_view(), name='availability'),
    url(r"^(?P<hotel_id>[0-9]+)/details/$", HotelDetail.as_view(), name='hotel-information'),
    url(r"^availability/$", HotelsAvailability.as_view(), name='hotels-availability'),
    url(r"^localities/$", Localities.as_view(), name='hotels-localities'),
    url(r"^details/$", HotelDetails.as_view(), name='hotel-details'),
    url(r"^status/$", HotelStatus.as_view(), name='hotel-statuses'),
]
