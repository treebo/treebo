# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0032_auto_20161020_0659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking', name='channel', field=models.CharField(
                default='', max_length=50, blank=True, choices=[
                    (b'msite', b'msite'), (b'website', b'website'), (b'app', b'app')]), ), ]
