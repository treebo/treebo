

from django.conf.urls import url
from apps.pricing.api.v4.cheapest_price_for_all_hotels import CheapestPriceAPIView
from apps.pricing.api.v4.hotel_prices import HotelRoomPrices

app_name = 'pricing_v4'
urlpatterns = []
