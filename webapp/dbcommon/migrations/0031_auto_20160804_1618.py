# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0030_auto_20160804_1601'),
    ]

    operations = [
        migrations.RunSQL(
            ['ALTER TABLE profiles_user ALTER COLUMN is_superuser DROP NOT NULL'],
            ['ALTER TABLE profiles_user ALTER COLUMN is_superuser SET NOT NULL'],
        )]
