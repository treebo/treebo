# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0055_auto_20170620_0922'),
    ]

    operations = [
        migrations.AddField(
            model_name='categories',
            name='enable_for_seo',
            field=models.BooleanField(default=False),
        ),
    ]
