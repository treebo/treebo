from random import randint

from base.views.api import TreeboAPIView

__author__ = 'Rajdeep'


class HotelDeals(TreeboAPIView):
    @staticmethod
    def getHotelDeals():
        response = {}
        choices = [
            "Bingo! You have bagged the best deal",
            "Rooms are selling super fast in this Treebo hotel, book now!",
            "Almost done. You are just one step away from booking a perfect stay",
            "Smile, We're still holding this room for you!",
            "Congratulations! You have discovered a Value Deal. Book fast before its gone!"]

        choiceIndex = randint(0, 4)

        result = {"title": "", "desc": choices[choiceIndex]}
        response['deals'] = result

        return response
