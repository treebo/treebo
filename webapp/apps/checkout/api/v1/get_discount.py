import datetime
import logging
from collections import defaultdict

from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.checkout.models import BookingRequest
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.common.utils import round_to_two_decimal
from apps.discounts.discount_manager import DiscountManager
from apps.discounts.models import DiscountCoupon
from apps.discounts.services.discount_coupon_services import DiscountCouponServices
from apps.pricing.dateutils import date_to_ymd_str
from apps.pricing.exceptions import RequestError
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.services.pricing_service import PricingService, SoaError
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from apps.checkout.service.discount_service import DiscountService

logger = logging.getLogger(__name__)


class ApplyDiscountSerializer(serializers.Serializer):
    couponcode = serializers.CharField(max_length=15, allow_blank=True)
    bid = serializers.IntegerField()


class ApplyDiscount(TreeboAPIView):
    validationSerializer = ApplyDiscountSerializer
    discount_service = DiscountService()
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)

    def get(self, request, *args, **kwargs):
        """
        Gets the discount for a booking id and coupon code.
        couponcode -- Coupon Code
        bid -- BookingRequest Id
        """
        validated_data = self.serializerObject.validated_data
        coupon_code, booking_request_id = validated_data['couponcode'], validated_data['bid']
        booking_request = BookingRequest.objects.filter(
            pk=booking_request_id).first()

        if not booking_request:
            return Response({"error": error_message(
                codes.INVALID_BOOKING)}, status=400)

        coupon = DiscountCoupon.objects.filter(
            code__iexact=coupon_code).first()
        if coupon:
            coupon_desc = str(coupon.terms.replace('\\n', ' '))
        else:
            return Response({"error": error_message(
                codes.INVALID_COUPON)}, status=400)

        if coupon.is_referral:
            logger.info("Applying Referral coupon code")
            try:
                referral_click_id = str(request.COOKIES.get(
                    'avclk')) if request.COOKIES.get('avclk') else None
                logged_user_id = ''
                if request.user and request.user.is_authenticated():
                    logged_user_id = str(request.user.id)
                if referral_click_id:
                    DiscountCouponServices.validate_referral_coupon(
                        coupon_code, str(
                            booking_request.email), str(
                            booking_request.phone), logged_user_id)
                else:
                    return Response({"error": error_message(
                        codes.INVALID_COUPON)}, status=400)

            except Exception as exc:
                logger.exception(exc)
                return Response({"error": error_message(
                    codes.INVALID_COUPON)}, status=400)

        if FeatureToggleAPI.is_enabled("pricing", "soa", False):
            try:
                prices = self.get_from_soa(
                    request, booking_request, coupon_code)
                if prices["discountError"]:
                    return Response({"error": {"msg": prices["discountError"],
                                               "code": codes.INVALID_COUPON}
                                     }, status=400)

            except RequestError as e:
                return Response({"error": error_message(
                    codes.INVALID_COUPON)}, status=HTTP_400_BAD_REQUEST)
            except SoaError as e:
                logger.exception(
                    " SoaError Exception occurred while applying coupon code using SOA API")
                return Response({"error": error_message(
                    codes.INVALID_COUPON)}, status=e.status)
            except Exception as e:
                logger.exception(
                    "unknown Exception occurred while applying coupon code using SOA API")
                return Response({"error": error_message(
                    codes.INVALID_COUPON)}, status=HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            try:
                prices = self.discount_service.get_from_local(
                    request, booking_request, coupon_code)
                if prices["discountError"]:
                    return Response({
                        "error": {
                            "msg": prices["discountError"],
                            "code": codes.INVALID_COUPON
                        }
                    }, status=400)
            except BaseException:
                logger.exception(
                    "Exception occurred while applying coupon code using local Pricing API")
                return Response({"error": error_message(
                    codes.INVALID_COUPON)}, status=400)

        self.__update_booking_request_price(
            booking_request, prices, coupon_code)
        discount = max(prices['discount'], prices['voucherAmount'])
        context = {
            "coupon_desc": coupon_desc,
            "pretax_price": prices['price'],
            "total_tax": prices['tax'],
            "pretax_price_after_discount": round_to_two_decimal(prices['price'] - float(discount)),
            # "actual_total_cost": Decimal(booking_request.total_amount).quantize(Decimal('.01'), rounding=ROUND_UP),
            "actual_total_cost": booking_request.total_amount,
            "discount_amount": round_to_two_decimal(discount),
            "nights_breakup": prices['nights'],
            "final_price": booking_request.total_amount,
            "autopromo": False,
            "is_prepaid": bool(coupon.is_prepaid)
        }
        logger.debug('Discount coupon context: %s', context)
        return Response(context)

    def __update_booking_request_price(
            self, booking_request, prices, coupon_code):
        booking_request.pretax_amount = prices['price']
        booking_request.tax_amount = prices['tax']
        booking_request.discount_value = prices["discount"]
        booking_request.voucher_amount = prices['voucherAmount']
        booking_request.calculate_total_amount()

        if booking_request.discount_value or booking_request.voucher_amount:
            booking_request.coupon_code = coupon_code
            booking_request.coupon_apply = True
        else:
            booking_request.coupon_code = ""
            booking_request.coupon_apply = False
        booking_request.save()

    def get_from_soa(self, request, booking_request, coupon_code):
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        checkin = date_to_ymd_str(booking_request.checkin_date)
        checkout = date_to_ymd_str(booking_request.checkout_date)
        hotel = hotel_repository.get_hotel_by_id_from_web(
            hotel_id=booking_request.hotel_id)
        room_count = len(booking_request.room_config.split(','))

        # Get Discount Percentage
        room_type = booking_request.room_type
        kwargs = {
            'code': coupon_code, 'checkin': checkin, 'room_config': booking_request.room_config,
            'checkout': checkout, 'hotel_id': hotel.id, 'user': booking_request.user,
            'no_bookings': room_count, 'booking_date': datetime.datetime.now(),
            'total_pretax_cost': booking_request.pretax_amount, 'totalcost': booking_request.total_amount,
            'rack_rate': booking_request.rack_rate, 'rooms': room_type, 'city': hotel.city,
            'booking_channel': booking_request.booking_channel,
            'current_user': getattr(request, 'user', None), 'paytype': booking_request.pay_at_hotel,
            'utm_source': booking_request.utm_source
            # 'pretax_rack_rate': pretax_rack_rate not being used currently
        }
        discountManager = DiscountManager(**kwargs)
        coupon, message = discountManager.validate()
        percentage = 0
        discount_error = ""
        if coupon:
            if coupon.terms:
                coupon_desc = str(coupon.terms.replace('\\n', ' '))
            discountApplied = discountManager.apply(coupon, room_count)
            coupon_code, couponMessage, couponType, terms = coupon.code, coupon.coupon_message, \
                coupon.coupon_type, coupon.terms
            logger.debug(
                "DiscountManager response... discount" +
                str(discountApplied) +
                "terms" +
                "coupon msg" +
                couponMessage)
            discount = discountApplied
            percentage = (float(discount) /
                          float(kwargs["total_pretax_cost"])) * 100
        else:
            discount_error = message
            return {"discountError": discount_error}

        # Send Discount Percentage to SOA
        response = PricingService.get_price_from_soa(
            checkin=checkin,
            checkout=checkout,
            hotel_ids=[
                booking_request.hotel_id],
            room_config=booking_request.room_config,
            coupon_code=coupon_code,
            coupon_value=percentage,
            coupon_type="Percentage",
            include_price_breakup=True,
            get_from_cache=False,
            channel=booking_request.booking_channel,
            utm_source=booking_request.utm_source)

        room_prices = response["data"]["hotels"][0]["rooms"]
        required_room = {}
        for room in room_prices:
            if room["room_type"].lower() == room_type.lower():
                required_room = room

        nights_breakup = {}
        voucher_amount, voucher_counter = 0.0, 0
        final_price = required_room["price"]["sell_price"]
        per_room_price_for_all_nights = []
        for room_config_price in required_room.get(
                "room_config_wise_prices", []):
            for date_wise in room_config_price["date_wise_prices"]:
                date = date_wise["date"]
                price = date_wise["price"]
                one_room_price_per_night = []
                one_room_price_per_night.append(
                    float(price["autopromo_price_without_tax"] + price["tax"]))
                one_room_price_per_night.append(float(price["sell_price"]))
                per_room_price_for_all_nights.append(one_room_price_per_night)

                breakup = nights_breakup.get(date, defaultdict(float))
                breakup["date"] = date
                breakup["pretax_price"] += price["autopromo_price_without_tax"]
                breakup["tax"] += price["tax"]
                breakup["discount"] += price["coupon_discount"]
                breakup["final_price"] += price["sell_price"]
                breakup["base_price"] += price["base_price"]
                breakup["rack_rate"] += price["rack_rate"]
                nights_breakup[date] = breakup

        try:
            if coupon and str(coupon.coupon_type) == DiscountCoupon.VOUCHER and str(
                    coupon.discount_operation) == DiscountCoupon.FOTREDEEM:
                discount_value = float(coupon.discount_value)
                if discount_value <= float(final_price):
                    final_price -= discount_value
                    voucher_amount = discount_value
                else:
                    voucher_amount = final_price
                    final_price = 0.0
            else:
                logger.debug("Coupon code is not valid")
        except Exception as e:
            logger.exception(e)
            logger.debug('VoucherAmount generation failed')

        nights_breakup_list = []
        for v in list(nights_breakup.values()):
            nights_breakup_list.append(v)

        prices = {
            "price": required_room["price"]["autopromo_price_without_tax"],
            "tax": required_room["price"]["tax"],
            "discount": required_room["price"]["coupon_discount"],
            "final_price": final_price,
            "voucherAmount": voucher_amount,
            "nights": nights_breakup_list,
            "discountError": discount_error,
        }
        return prices
