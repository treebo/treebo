import json
import logging

from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.checkout.models import BookingRequest
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.common.json_encoder import DecimalEncoder
from apps.discounts.exceptions import InvalidCouponException
from apps.discounts.services.discount_coupon_services import DiscountCouponServices
from apps.pricing.exceptions import RequestError
from apps.pricing.services.price_request_dto import PriceRequestDto
from apps.pricing.services.pricing_service import SoaError
from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.pricing.transformers.coupon_api_transformer import CouponAPITransformer
from apps.pricing.transformers.itinerary_api_transformer import ItineraryAPITransformer
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class ApplyCouponSerializer(serializers.Serializer):
    couponcode = serializers.CharField(max_length=15, allow_blank=True)
    bid = serializers.IntegerField()


class ApplyCoupon(TreeboAPIView):
    validationSerializer = ApplyCouponSerializer
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    pricing_service = PricingServiceV2()
    price_response_transformer = ItineraryAPITransformer()

    def get(self, request, *args, **kwargs):
        """
        Gets the discount for a booking id and coupon code.
        couponcode -- Coupon Code
        bid -- BookingRequest Id
        """
        validated_data = self.serializerObject.validated_data
        coupon_code, booking_request_id = validated_data['couponcode'], validated_data['bid']
        booking_request = BookingRequest.objects.filter(
            pk=booking_request_id).first()

        if not booking_request:
            return Response({"error": error_message(
                codes.INVALID_BOOKING)}, status=400)

        referral_click_id = str(request.COOKIES.get(
            'avclk')) if request.COOKIES.get('avclk') else None
        utm_source = str(request.COOKIES.get('utm_source')).lower()
        current_user = getattr(request, 'user', None)
        logged_in_user = True if current_user and current_user.is_authenticated() else False

        discount_percent, coupon, error_response = self.get_discount_percent(
            coupon_code, booking_request, current_user, referral_click_id)
        if discount_percent is None:
            return error_response

        try:
            room_configs = booking_request.room_config.split(",")
            price = self.get_price(
                booking_request,
                room_configs,
                coupon,
                coupon_code,
                discount_percent,
                logged_in_user=logged_in_user,
                utm_source=utm_source)

        except RequestError as e:
            return Response({"error": error_message(
                codes.INVALID_COUPON)}, status=HTTP_400_BAD_REQUEST)
        except SoaError as e:
            logger.exception(
                " SoaError Exception occurred while applying coupon code using SOA API")
            return Response({"error": error_message(
                codes.INVALID_COUPON)}, status=e.status)
        except Exception as e:
            logger.exception(
                "unknown Exception occurred while applying coupon code using SOA API")
            return Response({"error": error_message(
                codes.INVALID_COUPON)}, status=HTTP_500_INTERNAL_SERVER_ERROR)

        logger.info(
            "Pricing API response in apply_coupon API for bid: %s, coupon_code: %s => %s",
            booking_request.id,
            coupon_code,
            json.dumps(
                price,
                cls=DecimalEncoder))

        self._update_booking_request_with_prices(
            booking_request,
            price["selected_rate_plan"]["price"],
            coupon_code,
            logged_in_user=logged_in_user)

        logger.info(
            "Prices stored in booking_request with bid: %s => [pretax_price: %s, mm_promo: %s, "
            "coupon_discount: %s, member_discount: %s, tax_amount: %s, total_amount: %s]",
            booking_request.id,
            booking_request.pretax_amount,
            booking_request.mm_promo,
            booking_request.discount_value,
            booking_request.member_discount,
            booking_request.tax_amount,
            booking_request.total_amount)

        response = price
        coupon_desc = str(coupon.terms.replace('\\n', ' '))
        response['coupon_desc'] = coupon_desc
        response['is_prepaid'] = coupon.is_prepaid
        logger.debug('Discount coupon response: %s', response)
        return Response(response)

    def get_price(
            self,
            booking_request,
            room_configs,
            coupon,
            coupon_code,
            discount_percentage,
            logged_in_user=False,
            utm_source=None):
        price_request_dto = PriceRequestDto(
            booking_request.checkin_date, booking_request.checkout_date, [booking_request.hotel_id],
            room_configs, room_type=booking_request.room_type, rate_plan=booking_request.rate_plan,
            coupon_code=coupon_code, coupon_value=discount_percentage, coupon_type="Percentage",
            channel=booking_request.booking_channel, logged_in_user=logged_in_user, utm_source=utm_source)

        prices = self.pricing_service.get_itinerary_page_price(
            price_request_dto)
        price = self.price_response_transformer.transform_price_for_display(
            prices, booking_request.rate_plan)

        # TODO: If VOUCHER and FOTREDEEM coupons are not used anymore, then remove the code
        # if coupon and str(coupon.coupon_type) == DiscountCoupon.VOUCHER and \
        #         str(coupon.discount_operation) == DiscountCoupon.FOTREDEEM:
        #     try:
        #         rp_price = price["selected_rate_plan"]["price"]
        #         discount_value = float(coupon.discount_value)
        #         final_price = rp_price['sell_price']
        #         if discount_value <= float(final_price):
        #             rp_price['sell_price'] -= discount_value
        #             rp_price['voucher_amount'] = discount_value
        #         else:
        #             rp_price['sell_price'] = 0.0
        #             rp_price['voucher_amount'] = final_price
        #     except Exception as e:
        #         logger.exception("Voucher amount not found")
        return price

    def get_discount_percent(
            self,
            coupon_code,
            booking_request,
            current_user,
            referral_click_id=None):
        logged_user_id = str(
            current_user.id) if current_user and current_user.is_authenticated() else ""
        try:
            coupon, discount, discount_error = DiscountCouponServices.validate_coupon_and_get_discount(
                booking_request, coupon_code, referral_click_id, logged_user_id, current_user)

            if discount_error:
                return None, None, Response(
                    {"error": {"msg": discount_error, "code": codes.INVALID_COUPON}}, status=400)
        except InvalidCouponException:
            return None, None, Response({"error": error_message(
                codes.INVALID_COUPON_CODE)}, status=HTTP_400_BAD_REQUEST)

        discount_percent = (float(discount) / float(
            booking_request.pretax_amount - booking_request.member_discount)) * 100
        return discount_percent, coupon, None

    def _update_booking_request_with_prices(
            self,
            booking_request,
            prices,
            coupon_code,
            logged_in_user=False):
        """
        rate_plan_detail will be dict of this form:
                {
                    "code": selected_rate_plan,
                    "description": rate_plan_meta.get("description"),
                    "tag": rate_plan_meta.get("tag")
                },
        :param booking_request:
        :param prices:
        :param logged_in_user:
        :param rate_plan_detail:
        :return:
        """
        rp_price = prices
        booking_request.pretax_amount = rp_price['pretax_price']
        booking_request.rack_rate = rp_price["base_price"]
        booking_request.mm_promo = rp_price["promo_discount"]
        booking_request.discount_value = rp_price["coupon_discount"]
        booking_request.member_discount = rp_price['member_discount']
        booking_request.voucher_amount = rp_price.get('voucher_amount')
        booking_request.tax_amount = rp_price["tax"]
        booking_request.calculate_total_amount()

        if rp_price.get("coupon_discount") or rp_price.get("voucher_amount"):
            booking_request.coupon_code = coupon_code
            booking_request.coupon_apply = True
        else:
            booking_request.coupon_code = ""
            booking_request.coupon_apply = False
        booking_request.save()
