import json
import logging

from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from ipware.ip import get_ip
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.auth.services.otp_service import OTPService
from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.bookings.api.v1.bookingstub import BookingsByOrderId
from apps.bookingstash.service.availability_service import get_availability_service
from apps.bookingstash.utils import StashUtils
from apps.checkout import constants
from apps.checkout.api.v5.unverified_pay_at_hotel_booking import UnverifiedPAHBookingUtil
from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.constants import PAY_AT_HOTEL_BOOKING_MODE, INDIA_COUNTRY_CODE
from apps.checkout.models import BookingRequest
from apps.checkout.serializers import PayAtHotelRequestSerializer
from apps.checkout.service import checkout_service
from apps.checkout.service.analytics.order_analytics import confirm_booking_analytics, \
    pah_booking_complete_analytics
from apps.checkout.service.checkout_handler import CheckoutHandler
from apps.checkout.service.checkout_service import save_booking_state, sanitise_data
from apps.checkout.service.create_booking_handler import CreateBooking
from apps.checkout.service.recaptcha_service import ReCaptchaV3Service
from apps.checkout.tasks import send_booking_confirmation_notification
from apps.common import error_codes
from apps.common.exceptions.booking_exceptions import DuplicateBookingCreationException
from apps.common.json_encoder import CustomBytesEncoder
from apps.common.slack_alert import SlackAlertService
from apps.discounts.exceptions import InvalidCouponException
from apps.discounts.models import DiscountCoupon

from apps.payments.service.payment_service_v2 import PaymentServiceV2
from base.logging_decorator import log_args
from base.renderers import TreeboJSONRenderer
from base.views.api_v2 import TreeboAPIView2
from common.exceptions.treebo_exception import TreeboValidationException, TreeboException
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.common.exceptions.custom_exception import CreateBookingValidationError, \
    InvalidRequestParamInAPIException, \
    ReCaptchaServiceUnavailableException, ReCaptchaVerifyError, ReCaptchaScoreBelowThresholdError, \
    UnverifiedPAHBookingLimitExceededException
from axis_rooms.exceptions import AxisRoomsApiFailure
from apps.common.exceptions.custom_exception import AxisRoomBookingValidationError
from apps.bookings.services.axis_rooms_booking_service import AxisRoomsBookingService
from apps.common.api_response import APIResponse as api_response
from apps.growth.services.growth_services import GrowthServices
from apps.bookings.constants import HOMESTAY_PROPERTY_TYPE, HOMESTAY_PROVIDER_WT
from dbcommon.models.hotel import Hotel
from django.conf import settings

logger = logging.getLogger(__name__)


class PayAtHotel(TreeboAPIView2):
    renderer_classes = (TreeboJSONRenderer,)
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    checkout_handler = CheckoutHandler()
    payment_service = PaymentServiceV2()
    otp_service = OTPService()
    auth_client = TreeboAuthClient()

    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    def __freeze_booking_request(self, request, instant_booking, booking_mode=None):
        json_data = request.data
        # Doing this to solve serialization/deserialization issues with bytes
        json_data = json.loads(json.dumps(json_data, cls=CustomBytesEncoder))
        logger.info("__freeze_booking_request for pay@hotel %s ", json_data)
        bid = json_data.get('bid')
        booking_payload = {
            "bid": bid,
            # Hardcoding value 1, TODO: make a map for paid and unpaid nad set
            # the value for it
            "pay_at_hotel": '1',
            "name": json_data.get('name', ''),
            "email": json_data.get('email'),
            "country_code": '91' if json_data.get('country_code', '91') == '' else json_data.get(
                'country_code', '91'),
            "phone": json_data.get('mobile', ''),
            "organization_name": json_data.get('organization_name', ''),
            "organization_address": json_data.get('organization_address', ''),
            "organization_taxcode": json_data.get('organization_taxcode', ''),
            "otp_verified_number": json_data.get('otp_verified_number', ''),
            "user_signup_channel": request.COOKIES.get("user_signup_channel"),
            "message": json_data.get('message', ""),
            "check_in_time": json_data.get('check_in_time'),
            "check_out_time": json_data.get('check_out_time'),
            "user": request.user,
            "instant_booking": instant_booking,
            "http_host": request.META['HTTP_HOST'],
            "payment_mode": "Not Paid",
            "utm_source": str(request.COOKIES.get('utm_source')) if request.COOKIES.get(
                'utm_source') else "",
            "utm_medium": str(request.COOKIES.get('utm_medium')) if request.COOKIES.get(
                'utm_medium') else "",
            "referral_click_id": str(request.COOKIES.get('avclk')) if request.COOKIES.get(
                'avclk') else "",
            "utm_campaign": str(request.COOKIES.get('utm_campaign')) if request.COOKIES.get(
                'utm_campaign') else "",
            "host_ip_address": str(get_ip(request)) if get_ip(request) else ""
        }

        logger.info(
            "Booking Payload for freezing booking request: %s",
            booking_payload)

        booking_request = save_booking_state(booking_payload, booking_mode=booking_mode)
        return booking_request

    def __create_booking(self, booking_request, user):
        """
        :param apps.checkout.models.BookingRequest:
        :return: booking request object
        """
        booking_request.comments = checkout_service.build_special_preference(
            booking_request)
        booking_request.save()

        booking = CreateBooking.internal_create_booking(booking_request, is_pay_at_hotel=True,
                                                        user=user)
        booking_request.status = BookingRequest.COMPLETED
        booking_request.save()

        return booking_request

    def __booking_handler(self, request, booking_request):
        '''

        :param booking_request:
        :return: Booking order id
        '''
        logger.info("calling create booking for bid %s ", booking_request.id)

        booking = self.__create_booking(booking_request, request.user)

        order_id = booking.order_id
        booking_instance = BookingsByOrderId()
        response = booking_instance.get(request, orderId=order_id)
        if response.status_code == 404:
            raise TreeboValidationException(error_codes.INVALID_ORDER_ID)
        booking_dict = response.data
        logger.info(
            "create booking success for bid %s have order id %s and booking data %s  ",
            booking_request.id,
            booking_request.order_id,
            booking_dict)

        return booking_request, booking.order_id

    def __check_availability(self, bid):
        booking_request = BookingRequest.objects.get(id=bid)
        available_rooms = get_availability_service().get_available_rooms(
            booking_request.hotel_id,
            booking_request.checkin_date,
            booking_request.checkout_date,
            booking_request.room_config)
        for room, availability in list(available_rooms.items()):
            if booking_request.room_type.lower(
            ) == room.room_type_code.lower() and availability > 0:
                return True
        return False

    def __is_captcha_token_required(self, phone_number, country_code, request):
        if (hasattr(request, 'user') and request.user
            and request.user.is_authenticated() and (
                request.user.is_staff or request.user.is_otp_verified)):
            return False

        try:
            request_dto = {
                'phone_number': phone_number
            }
            user = AnonymousUser()
            is_user_verified = self.otp_service.is_verified(request_dto, user) \
                if country_code == INDIA_COUNTRY_CODE else True
            captcha_token_required = not is_user_verified
            return captcha_token_required
        except Exception as e:
            logger.exception(
                "Exception in checking if OTP is verified for request parameter phone_number: {0}"
                    .format(phone_number))

            SlackAlertService.send_slack_alert_for_exceptions(status=500,
                                                              request_param=str(phone_number),
                                                              message=str(e),
                                                              class_name=PayAtHotel.__class__.__name__,
                                                              dev_msg="Exception in checking if OTP is verified "
                                                                      "in method __is_captcha_token_required."
                                                              )
        return True

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        json_data = request.data

        # TODO: Following if condition is just here to support wrong data being sent by android
        # Added on 2019-11-18. Remove this in a week or so once this is fixed on android.
        if not isinstance(json_data.get("recaptcha_user_token"), str):
            json_data['recaptcha_user_token'] = ''

        serializer = PayAtHotelRequestSerializer(data=json_data)
        if not serializer.is_valid():
            return self.get_validation_error_response(error_codes.BOOKING_VALIDATION_ERROR)

        json_data = sanitise_data(json_data)
        bid = json_data.get('bid')

        is_booking_unverified = False
        phone_number = json_data.get('mobile')
        country_code = json_data.get('country_code', INDIA_COUNTRY_CODE)
        try:
            captcha_token_required = self.__is_captcha_token_required(phone_number, country_code,
                                                                      request)

            if captcha_token_required:
                unverified_booking_made = UnverifiedPAHBookingUtil.is_unverified_booking_made(
                    phone_number)

                if unverified_booking_made:
                    raise UnverifiedPAHBookingLimitExceededException()  # Since unverified PAH booking limit is 1
                else:
                    is_booking_unverified = True

                token = json_data.get('recaptcha_user_token')
                threshold_score = float(settings.GOOGLE_RECAPTCHA_V3_THRESHOLD_SCORE)
                action_on_page = constants.GOOGLE_RECAPTCHA_V3_PAH_BOOKING_PAGE_ACTION

                ReCaptchaV3Service.validate_captcha(recaptcha_token=token,
                                                    threshold_score=threshold_score,
                                                    action_on_page=action_on_page)

        except ReCaptchaVerifyError as e:
            logger.exception(
                'ReCaptchaVerifyError while captcha verify for phone_number: {0}'.format(
                    phone_number))

            e.message += ' '
            for error_message in e.error_messages:
                e.message += error_message + ' '

            return api_response.treebo_exception_error_response_from_input(error_code=e.error_code,
                                                                           message=e.message,
                                                                           status_code=e.status_code)
        except (InvalidRequestParamInAPIException, ReCaptchaScoreBelowThresholdError) as e:
            logger.exception(
                'InvalidRequestParamInAPIException while captcha verify for phone_number: {0}'
                    .format(phone_number))

            return api_response.treebo_exception_error_response_from_input(error_code=e.error_code,
                                                                           message=e.developer_message,
                                                                           status_code=e.status_code)
        except (
            ReCaptchaServiceUnavailableException, UnverifiedPAHBookingLimitExceededException) as e:
            logger.exception('{0} while captcha verify for phone_number: {1}'
                             .format(e.__class__.__name__, phone_number))

            return api_response.treebo_exception_error_response_from_input(error_code=e.error_code,
                                                                           message=e.message,
                                                                           status_code=e.status_code)

        except Exception as e:
            logger.exception(
                'Exception while captcha verify for phone_number: {0}'.format(phone_number))

            return api_response.treebo_exception_error_response_from_input(error_code='WEB_100',
                                                                           message='Oops! Something went wrong. '
                                                                                   'Please retry after sometime.',
                                                                           status_code=HTTP_500_INTERNAL_SERVER_ERROR)

        try:
            booking_request = BookingRequest.objects.get(id=bid)
            is_available = self.payment_service.is_available(booking_request)
        except BookingRequest.DoesNotExist:
            context = {'available': False, 'error': {
                'msg': 'Booking Request Does not exist',
                'code': "5005"
            }}
            SlackAlertService.web_alert(
                "Booking Request Does not exist : " + str(bid))
            return Response(context, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            context = {'available': False, 'error': {
                'msg': e,
                'code': "5001"
            }}
            SlackAlertService.web_alert(
                "Availability error for bid : " + str(bid))
            return Response(context, status=HTTP_500_INTERNAL_SERVER_ERROR)

        if not is_available:
            # FIXME: Error codes standardizations
            # FIXME: Metrics loggers here
            context = {'available': False, 'error': {
                'msg': 'Room is sold out',
                'code': "5001"
            }}
            SlackAlertService.web_alert(
                "Room is sold out for bid : " + str(bid))
            return Response(context, status=HTTP_400_BAD_REQUEST)
        organization_taxcode = request.data.get('organization_taxcode', None)
        if not BookingHelper.validate_gstin_for_booking(
            organization_taxcode, bid=bid, api_call='pay@hotel v2'):
            return Response({"error": {
                "msg": error_codes.get(error_codes.INVALID_GSTIN_NUMBER)['msg'],
                "code": error_codes.get(error_codes.INVALID_GSTIN_NUMBER)['code']
            }
            }, status=HTTP_400_BAD_REQUEST)

        try:
            if FeatureToggleAPI.is_enabled("booking", "instant_booking", True):
                booking_request = self.__freeze_booking_request(request, True,
                                                                booking_mode=PAY_AT_HOTEL_BOOKING_MODE)
            else:
                booking_request = self.__freeze_booking_request(request, False,
                                                                booking_mode=PAY_AT_HOTEL_BOOKING_MODE)

        except InvalidCouponException as exc:
            logger.exception(exc)
            context = {'coupon_applicable': False, 'error': {
                'msg': str(exc),
                'code': "5004"
            }}
            SlackAlertService.web_alert(str(exc) + " for bid : " + str(bid))
            return Response(context, status=HTTP_400_BAD_REQUEST)

        try:
            booking_request, order_id = self.__booking_handler(request, booking_request)

            if is_booking_unverified:
                UnverifiedPAHBookingUtil.set_unverified_booking_made(booking_order_id=order_id,
                                                                     booking_owner_phone_number=phone_number)
            # updating the inventory
            StashUtils.check_booking_status_and_update_inventory(
                booking_request, booking_request.status)

            order_event_args = confirm_booking_analytics(
                request, booking_request, send_completed_order_event=True)
            pah_event_args = pah_booking_complete_analytics(request, booking_request)
            reason = 'success'
            return Response({"order_id": order_id,
                             'reason': reason,
                             'order_event_args': order_event_args})

        except AxisRoomBookingValidationError as err:
            PaymentServiceV2().initiate_refund(booking_request.order_id)
            SlackAlertService.send_slack_alert_for_exceptions(status=400, request_param=str(
                booking_request.order_id),
                                                              message=err.__str__(),
                                                              class_name=self.__class__.__name__,
                                                              dev_msg="Error in AxisRoom Booking Validation")
            return api_response.error_response_from_error_code(
                error_codes.AXIS_ROOM_FAILURE,
                status_code=HTTP_400_BAD_REQUEST,
                api_name=PayAtHotel.__name__)

        except AxisRoomsApiFailure as err:
            PaymentServiceV2().initiate_refund(booking_request.order_id)
            SlackAlertService.send_slack_alert_for_exceptions(status=500, request_param=str(
                booking_request.order_id),
                                                              message=err.message,
                                                              class_name=self.__class__.__name__,
                                                              dev_msg="Exception in AxisRoom API")
            return api_response.error_response_from_error_code(
                error_codes.AXIS_ROOM_FAILURE,
                status_code=HTTP_400_BAD_REQUEST,
                api_name=PayAtHotel.__name__)

        except DuplicateBookingCreationException as e:
            logger.exception("Duplicate booking creation: %s ", bid)
            return self.get_simple_response_from_treebo_exception_for_booking(e)
        except CreateBookingValidationError as e:
            SlackAlertService.send_slack_alert_for_exceptions(status=500,
                                                              request_param=request.data,
                                                              dev_msg=e.developer_message if e.developer_message else
                                                              "Create booking validation exception: {0}".format(
                                                                  bid),
                                                              message=e.message,
                                                              class_name=self.__class__.__name__)
            logger.exception("Create booking validation exception: %s ", bid)
            return Response({"error": e.message}, status=e.status_code)
        except TreeboException as e:
            SlackAlertService.send_slack_alert_for_exceptions(status=500,
                                                              request_param=request.data,
                                                              message=e.message,
                                                              dev_msg=e.developer_message if e.developer_message else
                                                              "TreeboException in pay@hotel v5: {0}".format(
                                                                  bid),
                                                              class_name=self.__class__.__name__)
            logger.exception("TreeboException in pay@hotel v5: %s ", bid)
            return self.get_simple_response_from_treebo_exception_for_booking(
                e)
        except Exception as e:
            SlackAlertService.send_slack_alert_for_exceptions(status=500,
                                                              request_param=request.data,
                                                              message=e.__str__(),
                                                              class_name=self.__class__.__name__)
            logger.exception(
                "Internal error pay@hotel v5 bid %s with exceptions %s",
                booking_request.id, str(e))
            return self.get_simple_response_from_error_code_for_booking(
                error_codes.BOOKING_CONFIRMATION_ERROR)
