import logging
import time
from django.core.management import BaseCommand
from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.profiles.service.profile_service import UserProfileService
from apps.profiles.service.user_registration import UserRegistration
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    auth_client = TreeboAuthClient()
    user_profile_service = UserProfileService()
    user_registration = UserRegistration()

    def add_arguments(self, parser):
        parser.add_argument(
            '--users',
            default='0',
            type=str,
            help='users id list, to which we want to register')
        parser.add_argument(
            '--remove_users',
            default='0',
            type=str,
            help='users id list, to which we want do not want to register')
        parser.add_argument(
            '--register_all',
            default=False,
            type=bool,
            help='to register all users ')
        parser.add_argument(
            '--internal_emp',
            default=False,
            type=bool,
            help='to internal emp all users ')
        parser.add_argument(
            '--start_with_userid',
            default='0',
            type=int,
            help='to start with ')
        parser.add_argument(
            '--user_at_one_time',
            default='0',
            type=int,
            help='to user_at_one_time with ')
        parser.add_argument('--limit', default='1000', type=int, help='limit ')

    def handle(self, *args, **options):
        users_id_str = options.get('users')
        remove_users_str = options.get('remove_users')
        users_id_list = [int(userid)
                         for userid in str(users_id_str).strip().split(',')]
        remove_users_list = [int(userid1) for userid1 in str(
            remove_users_str).strip().split(',')]
        register_all = options.get('register_all')
        start_with_userid = options.get('start_with_userid')
        user_at_one_time = options.get('user_at_one_time')
        limit = options.get('limit')
        logger.info("start_with_userid %s ", start_with_userid)
        logger.info("limit %s ", limit)
        logger.info("user_at_one_time %s ", user_at_one_time)
        logger.info("users_id_list %s ", users_id_list)
        logger.info(
            "remove_users_list %s %s",
            remove_users_list,
            type(remove_users_list))
        logger.info("register_all %s ", register_all)
        internal_emp = options.get('internal_emp')

        remove_users = self.fetch_user_from_id(remove_users_list)
        if internal_emp:
            users = list(self.user_profile_service.get_all_internal_users())
        elif register_all:
            users = list(
                self.user_profile_service.get_all_users(start_with_userid))
        else:
            users = self.fetch_user_from_id(users_id_list)
        logger.info(
            " users total with removal %d %s ",
            len(users),
            type(users))
        logger.info(
            " remove_users total  %d %s ",
            len(remove_users),
            type(remove_users))
        for user in remove_users:
            if user in users:
                users.remove(user)
        logger.info(" users left %d   ", len(users))
        if len(users) < 20:
            logger.info(" users left details %s  ", users)
        self.send_comm_to_users(users, limit, user_at_one_time)

    def send_comm_to_users(self, users, limit, user_at_one_time):
        userid_failed_with_exception = []
        userid_failed_with_unknown_exception = []
        userid_sucess = []
        logger.info(" total user found %d for registration ", len(users))
        for index, user in enumerate(users):
            logger.info(" index %s ", index)
            if index == limit:
                logger.info(" limit reached ")
                break
            if (index) % user_at_one_time == 0:
                time.sleep(2)
            try:
                self.auth_client.forgot_password(user.email, 'resetpassword')
                pass
            except TreeboException as e:
                userid_failed_with_exception.append(user.id)
                logger.exception(
                    " forgot email failed with user %s (%s) with exception %s ",
                    user,
                    user.id,
                    str(e))
            except Exception as e:
                userid_failed_with_unknown_exception.append(user.id)
                logger.exception(
                    " forgot email failed with user %s (%s) with unknown exception %s ",
                    user,
                    user.id,
                    str(e))

        logger.info(
            "Registration complete user_already_present  %d\n treebo exception %d\n userid_unknown_exception %d ",
            len(userid_sucess),
            len(userid_failed_with_exception),
            len(userid_failed_with_unknown_exception))
        logger.info(
            "userid_failed_with_exception %s  ",
            userid_failed_with_exception)
        logger.info(
            "userid_failed_with_unknown_exception %s  ",
            userid_failed_with_unknown_exception)
        logger.info("userid_sucess %s  ", userid_sucess)

    def fetch_user_from_id(self, users_id_list):
        users = list()
        for user_id in users_id_list:
            user = self.user_profile_service.get_user_by_id(user_id)
            if user:
                users.append(user)
        return users
