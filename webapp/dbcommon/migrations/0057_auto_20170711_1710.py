# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0056_categories_enable_for_seo'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccessToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(unique=True, max_length=255, db_index=True)),
                ('token_type', models.CharField(max_length=255)),
                ('refresh_token', models.CharField(max_length=255, unique=True, null=True)),
                ('expires', models.DateTimeField()),
                ('scope', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='user',
            name='auth_id',
            field=models.IntegerField(unique=True, null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(max_length=254, unique=True, null=True, verbose_name='email address'),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone_number',
            field=models.CharField(max_length=20, null=True, verbose_name='phone_number'),
        ),
        migrations.AddField(
            model_name='accesstoken',
            name='auth_user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.CASCADE),
        ),
    ]
