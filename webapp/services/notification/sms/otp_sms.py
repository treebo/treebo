import logging

from common.constants import common_constants
from services.notification.base_notification_service import NotificationService

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)


class OtpSmsService(NotificationService):
    template_name = common_constants.OTP_SMS_MESSAGE

    def __init__(self, notification_type, content_subtype, to_address):
        super(
            OtpSmsService,
            self).__init__(
            notification_type,
            content_subtype,
            to_address)

    def _setData(self, data):
        NotificationService._setData(self, data)

    def sendMessage(self):
        NotificationService._sendMessage(self)

    def createMessage(self):
        return NotificationService._createMessage(self)

    def collateSmsData(self, first_name, otp):
        data = {"first_name": first_name, "otp": otp}

        self._setData(data)
