import logging

from base.views.template import TreeboTemplateView
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'devang'

logger = logging.getLogger(__name__)


class RewardsTerms(TreeboTemplateView):
    template_name = "rewards-terms/index.html"

    # redirect_mobile_to_desktop_view = True

    def getData(self, request, args, kwargs):
        seo = ContentServiceClient.getValueForKey(request, 'seo_pages', 1)
        return {
            'terms': {},
            'styles': ['desktop/css/terms.css'],
            'scripts': ['desktop/js/terms.js'],
            'seo': seo['terms'],
            'footerCopyrightDisabled': True}

    def getMobileData(self, request, args, kwargs):
        return {
            'styles': ['mobile/css/terms.css'],
            'scripts': ['mobile/js/terms.js']}
