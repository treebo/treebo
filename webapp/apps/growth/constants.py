GDC_TRAVEL_AGENT_BOOKING = 'GDC_TA'
WEB_BOOKING = 'WEB'
CONTENT_STORE_TA_EMAIL_ID_NAME = 'ta_email_ids'
CHANNEL_WEB = "WEB"
STATUS_MAPPING_TO_MESSAGE = "status_mapping_for_cancellation"
SUBMIT_CANCEL_CANCELLED = 'CANCELLED'
GDC_BOOKING = 'GDC'

CANCELLATION_REASONS = [
    {
        "value": 1,
        "label": "Want to book a different hotel",
    },
    {
        "value": 2,
        "label": "Change in plan",
    },
    {
        "value": 3,
        "label": "Found a better deal",
    },
    {
        "value": 4,
        "label": "Booking created by mistake",
    },
    {
        "value": 5,
        "label": "Other",
    },
]

BOOKING_RESERVED = "reserved"
BOOKING_CANCELLED = "cancelled"
BOOKING_CANCELLED_MESSAGE = "Your booking is successfully cancelled - Refund will be initiated shortly, if applicable"
BOOKING_RESERVED_MESSAGE = "Booking Reserved"

CANCELLATION_REASONS_MAP = {
    1: "Want to book a different hotel",
    2: "Change in plan",
    3: "Found a better deal",
    4: "Booking created by mistake"
}

PARTPAY_CHANNEL_CODE = "WEB"
