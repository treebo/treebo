# -*- coding: utf-8 -*-
import abc
import json

from apps.bookings.data_classes import Booking
from apps.bookings.models import Booking as BookingModel


class CancelBookingInterface(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, booking, **kwargs):
        self.booking = booking
        self.kwargs = kwargs

    def cancel(self):
        cancelled_booking = self._cancel()
        assert isinstance(cancelled_booking, Booking)
        self._persist_details(cancelled_booking)
        return cancelled_booking

    def _persist_details(self, cancelled_booking):
        booking_model = BookingModel.objects.get(order_id=cancelled_booking.booking_id)
        booking_model.external_crs_booking_version = cancelled_booking.crs_version
        booking_model.external_crs_booking_extra_info = json.dumps(
            {'crs_booking_bill_id': cancelled_booking.crs_bill_id})
        booking_model.booking_status = cancelled_booking.status.old_name
        booking_model.save()

    @abc.abstractmethod
    def _cancel(self):
        pass
