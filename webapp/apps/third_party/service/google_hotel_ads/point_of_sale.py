

import logging

from lxml import etree as et
from lxml import objectify

from apps.third_party.service.google_hotel_ads import constants

logger = logging.getLogger(__name__)


class POSService(object):
    def pos_service(self, ):
        try:
            root = et.Element('PointsOfSale')
            pos_xml = self.generate_pos(root)
            return pos_xml
        except Exception as e:
            logger.exception("Exception in creating Point of Sale  %s", e)
            raise e

    def generate_pos(self, root):
        point_of_sale = et.SubElement(root, 'PointOfSale')
        match = objectify.SubElement(
            point_of_sale,
            'Match',
            status="yes",
            device="desktop")
        match = objectify.SubElement(
            point_of_sale,
            'Match',
            status="yes",
            device="mobile")
        match = objectify.SubElement(
            point_of_sale,
            'Match',
            status="yes",
            sitetype="localuniversal")
        match = objectify.SubElement(
            point_of_sale,
            'Match',
            status="yes",
            sitetype="mapresults")
        match = objectify.SubElement(
            point_of_sale,
            'Match',
            status="yes",
            sitetype="placepage")
        match = objectify.SubElement(
            point_of_sale,
            'Match',
            status="yes",
            sitetype="contentads")
        url = et.SubElement(point_of_sale, "URL")
        # https://www.treebohotels.com/hotels-in-chennai/treebo-saravana-boutique-inn-royapettah-34/?checkin=2016-10-21&checkout=2016-10-22&hotel_id=34&q=Treebo+Saravana+Boutique+Inn&roomconfig=2-0
        url.text = constants.POS_URL % (constants.BASE_URL,
                                        constants.ROOM_CONFIG,
                                        constants.GHA_UTM_SOURCE,
                                        constants.GHA_UTM_MEDIUM)
        objectify.deannotate(root, xsi_nil=True, cleanup_namespaces=True)
        tree = et.ElementTree(root)
        return tree
