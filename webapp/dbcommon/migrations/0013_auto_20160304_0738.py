# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0013_auto_20160303_1402'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='coupon_message',
            field=models.CharField(default='Discount value', max_length=500),
        ),
        migrations.AddField(
            model_name='coupon',
            name='coupon_type',
            field=models.IntegerField(default=0, choices=[(0, 'Discount'), (1, 'Voucher'),
                                                          (2, 'Cashback')]),
        ),
        migrations.AddField(
            model_name='coupon',
            name='discount_application',
            field=models.IntegerField(default=1, choices=[(0, 'Pretax'), (1, 'Posttax')]),
        ),
        migrations.AddField(
            model_name='state',
            name='luxury_tax_on_base',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='state',
            name='service_tax',
            field=models.DecimalField(default=8.7, max_digits=20, decimal_places=10),
        ),
        migrations.AlterField(
            model_name='cityblog',
            name='image_url',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
