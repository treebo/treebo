class PaymentDetailsV2(object):
    def __init__(
            self,
            payment_id,
            pg_payment_id,
            status,
            pg_meta={},
            **kwargs):
        self.ps_payment_id = payment_id
        self.pg_payment_id = pg_payment_id
        self.status = status
        self.pg_meta = pg_meta


class PaymentServiceV2InitiateDTO(object):
    def __init__(self, order_id, status, created_at, pg_order_id, amount,
                 currency, receipt, notes, gateway, pg_meta, payment):
        self.ps_order_id = order_id
        self.status = status
        self.created_at = created_at
        self.pg_order_id = pg_order_id
        self.amount = amount
        self.currency = currency
        self.receipt = receipt
        self.notes = notes
        self.gateway = gateway
        self.pg_meta = pg_meta
        if payment:
            self.ps_payment = PaymentDetailsV2(**payment)
        else:
            self.ps_payment = None


class PaymentServiceV2ConfirmDTO(object):
    def __init__(self, payment_id, pg_payment_id, status, pg_meta):
        self.payment_id = payment_id
        self.pg_payment_id = pg_payment_id
        self.status = status
        self.pg_meta = pg_meta
