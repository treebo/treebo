import sys


from mock import patch, Mock, MagicMock

sys.modules['apps.bookingstash.models'] = Mock()
sys.modules['data_services.factory'] = Mock()
sys.modules['apps.hotels.utils'] = Mock()
sys.modules['apps.pricing.feature_toggle_api'] = Mock()
sys.modules['apps.pricing.pricing_api_impl'] = Mock()
sys.modules['apps.pricing.utils'] = Mock()
sys.modules['webapp.dbcommon.models.hotel'] = Mock()
from webapp.apps.bookingstash.service.availability_service import get_availability_service, unset_availability_service, \
    get_max_adults_with_children, get_formatted_checkin_checkout_date, room_config_supported, \
    room_config_string_to_list
from apps.hotels import utils as hotelUtils


@patch('webapp.apps.bookingstash.service.availability_service.use_its_apis')
def test_get_its_availability_service_if_its_enabled(test_value_of_use_its_api_true):
    test_value_of_use_its_api_true.return_value = True
    unset_availability_service()

    availability_service = get_availability_service()

    assert type(availability_service).__name__ == 'ITSAvailabilityService'


@patch('webapp.apps.bookingstash.service.availability_service.use_its_apis')
def test_get_direct_availability_service_if_its_disabled(test_value_of_use_its_api_false):
    test_value_of_use_its_api_false.return_value = False
    unset_availability_service()

    availability_service = get_availability_service()

    assert type(availability_service).__name__ == 'DirectAvailabilityService'


def test_max_adults_with_children_as_two_adult_and_three_adult_and_children():

    (max_adults, max_adults_with_children) = get_max_adults_with_children('1-0,2-1')

    assert max_adults == 2
    assert max_adults_with_children == 3


def test_get_checkin_checkout_date_from_formatted_string():
    check_in_date_str = '2018-12-18'
    check_out_date_str = '2018-12-19'

    (check_in_date, check_out_date) = get_formatted_checkin_checkout_date(check_in_date_str, check_out_date_str)

    assert check_in_date.day == 18
    assert check_in_date.month == 12
    assert check_in_date.year == 2018
    assert check_out_date.day == 19
    assert check_out_date.month == 12
    assert check_out_date.year == 2018



def test_false_if_one_room_config_not_supported():
    hotelUtils.isOccupancySupported = MagicMock()
    hotelUtils.isOccupancySupported.side_effect = [True, False]

    assert room_config_supported('room1',[(1,0),(2,1)]) == False


def test_false_if_all_room_config_supported():
    hotelUtils.isOccupancySupported = MagicMock()
    hotelUtils.isOccupancySupported.side_effect = [True, True]

    assert room_config_supported('room1',[(1,0),(2,1)]) == True


def test_room_config_string_to_list_for_two_adults_and_three_children():
    room_config = room_config_string_to_list('2-3')
    assert room_config[0] == (2,3)

def test_two_room_config_string_to_list_for_two_adults_and_three_children_and_one_adults_and_one_child():
    room_config = room_config_string_to_list('2-3,1-1')
    assert room_config[0] == (2, 3)
    assert room_config[1] == (1, 1)
