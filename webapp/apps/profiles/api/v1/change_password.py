import logging

from django.contrib.auth import authenticate
from django.contrib.auth.middleware import AuthenticationMiddleware
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from rest_framework.response import Response

from apps.auth.dto.change_password_dto import ChangePasswordTokenDTO
from apps.auth.services.password_service import PasswordService
from apps.common.utils import Utils
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboValidationException

__author__ = 'varunachar'

logger = logging.getLogger(__name__)


class ChangePasswordValidator(serializers.Serializer):
    oldPassword = serializers.CharField(required=True, error_messages={
        'required': error_codes.PASSWORD_NOT_ENTERED,
        'blank': error_codes.PASSWORD_NOT_ENTERED
    })
    newPassword = serializers.CharField(required=True, error_messages={
        'required': error_codes.PASSWORD_NOT_ENTERED,
        'blank': error_codes.PASSWORD_NOT_ENTERED
    })

    def validate(self, attr):
        if attr['oldPassword'] == attr['newPassword']:
            raise TreeboValidationException(
                error_codes.OLD_NEW_PASSWORDS_MATCH)
        return attr


class ChangePasswordAPI(TreeboAPIView):
    validationSerializer = ChangePasswordValidator
    authentication_classes = (AuthenticationMiddleware,)
    change_password_service = PasswordService()

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ChangePasswordAPI, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        return {}

    def post(self, request, format=None):
        data = request.data
        oldPassword = data.get('oldPassword')
        newPassword = data.get('newPassword')

        change_password_dto = ChangePasswordTokenDTO(data={
            'old_password': oldPassword,
            'new_password': newPassword,
        })
        try:
            change_password_dto.is_valid()
            user, token = self.change_password_service.change_password(
                request, change_password_dto.data)
            user.set_password(newPassword)
            user.save()
            return Response({
                'status': 'success',
                'msg': 'Password changed successfully'
            })
        except Exception as e:
            logger.exception("exception while changing password")
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.INCORRECT_PASSWORD))
