import logging
import requests
from django.conf import settings
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class PricingClient:

    @staticmethod
    def make_pricing_call(query_url, params):
        """
        Make pricing fetch call to pricing orchestrator url with requested params
        :param query_url:
        :param params:
        """
        # Todo: Replace with updated pricing orchestrator url
        query_url = "http://pricing/new" + query_url
        response = {}
        try:
            # response = requests.get(query_url, params)
            pass
        # Todo: Add more specific exception handling/messages
        except Exception as e:
            logger.error(str(e))
            slack_alert.send_slack_alert_for_third_party(status=response.status_code,
                                                         class_name=PricingClient.__class__.__name__,
                                                         url=query_url,
                                                         reason='Pricing Service Unavailable'
                                                         )

        logger.debug("Pricing url requested %(url)s with params %(params)s",
                     {'url': query_url, 'params': params})
        # return response.json()
        # Todo: To be removed - For integration test
        return {
                  "data": {
                    "price_per_hotel": [
                      {
                        "hotel_id": "0000164",
                        "from_date": "2018-09-19T00:00:00.111Z",
                        "to_date": "2018-09-21T00:00:00.111Z",
                        "hotel_price_per_policy": [
                          {
                            "name": "EP",
                            "hotel_price_per_quote": [
                              {
                                "quote_id": "1",
                                "quote_price_per_date": [
                                  {
                                    "date": "2018-09-19T00:00:00.111Z",
                                    "pre_tax":
                                      {
                                        "list_price": 5000,
                                        "sale_price": 4000,
                                        "discounts": [
                                          {
                                            "discount_applied": 1000,
                                            "coupon_code": "EP",
                                            "discount_per_discount_type": [
                                              {
                                                "discount_id": "313",
                                                "name": "default name",
                                                "type": "Explicit",
                                                "code": "BOOKNOW",
                                                "post_conditions": []
                                              }
                                            ]
                                          }
                                        ]
                                      }
                                    ,
                                    "tax": {
                                      "breakup": [
                                        {
                                          "value": 250,
                                          "percent": 2.5,
                                          "type": "cgst"
                                        },
                                        {
                                          "value": 100,
                                          "percent": 2.5,
                                          "type": "sgst"
                                        },
                                        {
                                          "value": 0,
                                          "percent": 0,
                                          "type": "igst"
                                        }
                                      ],
                                      "total_percent": 5,
                                      "total_value": 350
                                    },
                                    "post_tax_price": 1500.78
                                  },

                                    {
                                        "date": "2018-09-20T00:00:00.111Z",
                                        "pre_tax":
                                            {
                                                "list_price": 5000,
                                                "sale_price": 4000,
                                                "discounts": [
                                                    {
                                                        "discount_applied": 1000,
                                                        "coupon_code": "EP",
                                                        "discount_per_discount_type": [
                                                            {
                                                                "discount_id": "313",
                                                                "name": "default name",
                                                                "type": "Explicit",
                                                                "code": "BOOKNOW",
                                                                "post_conditions": []
                                                            }
                                                        ]
                                                    },

                                                    {
                                                        "discount_applied": 1000,
                                                        "coupon_code": "EP",
                                                        "discount_per_discount_type": [
                                                            {
                                                                "discount_id": "314",
                                                                "name": "default name",
                                                                "type": "Explicit",
                                                                "code": "BOOKNOW",
                                                                "post_conditions": []
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ,
                                        "tax": {
                                            "breakup": [
                                                {
                                                    "value": 250,
                                                    "percent": 2.5,
                                                    "type": "cgst"
                                                },
                                                {
                                                    "value": 100,
                                                    "percent": 2.5,
                                                    "type": "sgst"
                                                },
                                                {
                                                    "value": 0,
                                                    "percent": 0,
                                                    "type": "igst"
                                                }
                                            ],
                                            "total_percent": 5,
                                            "total_value": 350
                                        },
                                        "post_tax_price": 1600.78
                                    }


                                ]
                              },

                                {
                                    "quote_id": "2",
                                    "quote_price_per_date": [
                                        {
                                            "date": "2018-09-19T00:00:00.111Z",
                                            "pre_tax":
                                                {
                                                    "list_price": 5000,
                                                    "sale_price": 4000,
                                                    "discounts": [
                                                        {
                                                            "discount_applied": 1000,
                                                            "coupon_code": "EP",
                                                            "discount_per_discount_type": [
                                                                {
                                                                    "discount_id": "313",
                                                                    "name": "default name",
                                                                    "type": "Explicit",
                                                                    "code": "BOOKNOW",
                                                                    "post_conditions": []
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ,
                                            "tax": {
                                                "breakup": [
                                                    {
                                                        "value": 250,
                                                        "percent": 2.5,
                                                        "type": "cgst"
                                                    },
                                                    {
                                                        "value": 100,
                                                        "percent": 2.5,
                                                        "type": "sgst"
                                                    },
                                                    {
                                                        "value": 0,
                                                        "percent": 0,
                                                        "type": "igst"
                                                    }
                                                ],
                                                "total_percent": 5,
                                                "total_value": 350
                                            },
                                            "post_tax_price": 1700.78
                                        },

                                        {
                                            "date": "2018-09-20T00:00:00.111Z",
                                            "pre_tax":
                                                {
                                                    "list_price": 5000,
                                                    "sale_price": 4000,
                                                    "discounts": [
                                                        {
                                                            "discount_applied": 1000,
                                                            "coupon_code": "EP",
                                                            "discount_per_discount_type": [
                                                                {
                                                                    "discount_id": "313",
                                                                    "name": "default name",
                                                                    "type": "Explicit",
                                                                    "code": "BOOKNOW",
                                                                    "post_conditions": []
                                                                }
                                                            ]
                                                        },

                                                        {
                                                            "discount_applied": 1000,
                                                            "coupon_code": "EP",
                                                            "discount_per_discount_type": [
                                                                {
                                                                    "discount_id": "314",
                                                                    "name": "default name",
                                                                    "type": "Explicit",
                                                                    "code": "BOOKNOW",
                                                                    "post_conditions": []
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ,
                                            "tax": {
                                                "breakup": [
                                                    {
                                                        "value": 250,
                                                        "percent": 2.5,
                                                        "type": "cgst"
                                                    },
                                                    {
                                                        "value": 100,
                                                        "percent": 2.5,
                                                        "type": "sgst"
                                                    },
                                                    {
                                                        "value": 0,
                                                        "percent": 0,
                                                        "type": "igst"
                                                    }
                                                ],
                                                "total_percent": 5,
                                                "total_value": 350
                                            },
                                            "post_tax_price": 1800.78
                                        }

                                    ]
                                }


                            ]
                          },

                            {
                                "name": "NRP",
                                "hotel_price_per_quote": [
                                    {
                                        "quote_id": "1",
                                        "quote_price_per_date": [
                                            {
                                                "date": "2018-09-19T00:00:00.111Z",
                                                "pre_tax":
                                                    {
                                                        "list_price": 5000,
                                                        "sale_price": 4000,
                                                        "discounts": [
                                                            {
                                                                "discount_applied": 1000,
                                                                "coupon_code": "EP",
                                                                "discount_per_discount_type": [
                                                                    {
                                                                        "discount_id": "313",
                                                                        "name": "default name",
                                                                        "type": "Explicit",
                                                                        "code": "BOOKNOW",
                                                                        "post_conditions": []
                                                                    }
                                                                ]
                                                            }
                                                        ]
                                                    }
                                                ,
                                                "tax": {
                                                    "breakup": [
                                                        {
                                                            "value": 250,
                                                            "percent": 2.5,
                                                            "type": "cgst"
                                                        },
                                                        {
                                                            "value": 100,
                                                            "percent": 2.5,
                                                            "type": "sgst"
                                                        },
                                                        {
                                                            "value": 0,
                                                            "percent": 0,
                                                            "type": "igst"
                                                        }
                                                    ],
                                                    "total_percent": 5,
                                                    "total_value": 350
                                                },
                                                "post_tax_price": 150
                                            },

                                            {
                                                "date": "2018-09-20T00:00:00.111Z",
                                                "pre_tax":
                                                    {
                                                        "list_price": 5000,
                                                        "sale_price": 4000,
                                                        "discounts": [
                                                            {
                                                                "discount_applied": 1000,
                                                                "coupon_code": "EP",
                                                                "discount_per_discount_type": [
                                                                    {
                                                                        "discount_id": "313",
                                                                        "name": "default name",
                                                                        "type": "Explicit",
                                                                        "code": "BOOKNOW",
                                                                        "post_conditions": []
                                                                    }
                                                                ]
                                                            },

                                                            {
                                                                "discount_applied": 1000,
                                                                "coupon_code": "EP",
                                                                "discount_per_discount_type": [
                                                                    {
                                                                        "discount_id": "314",
                                                                        "name": "default name",
                                                                        "type": "Explicit",
                                                                        "code": "BOOKNOW",
                                                                        "post_conditions": []
                                                                    }
                                                                ]
                                                            }
                                                        ]
                                                    }
                                                ,
                                                "tax": {
                                                    "breakup": [
                                                        {
                                                            "value": 250,
                                                            "percent": 2.5,
                                                            "type": "cgst"
                                                        },
                                                        {
                                                            "value": 100,
                                                            "percent": 2.5,
                                                            "type": "sgst"
                                                        },
                                                        {
                                                            "value": 0,
                                                            "percent": 0,
                                                            "type": "igst"
                                                        }
                                                    ],
                                                    "total_percent": 5,
                                                    "total_value": 350
                                                },
                                                "post_tax_price": 160
                                            }

                                        ]
                                    },

                                    {
                                        "quote_id": "2",
                                        "quote_price_per_date": [
                                            {
                                                "date": "2018-09-19T00:00:00.111Z",
                                                "pre_tax":
                                                    {
                                                        "list_price": 5000,
                                                        "sale_price": 4000,
                                                        "discounts": [
                                                            {
                                                                "discount_applied": 1000,
                                                                "coupon_code": "EP",
                                                                "discount_per_discount_type": [
                                                                    {
                                                                        "discount_id": "313",
                                                                        "name": "default name",
                                                                        "type": "Explicit",
                                                                        "code": "BOOKNOW",
                                                                        "post_conditions": []
                                                                    }
                                                                ]
                                                            }
                                                        ]
                                                    }
                                                ,
                                                "tax": {
                                                    "breakup": [
                                                        {
                                                            "value": 250,
                                                            "percent": 2.5,
                                                            "type": "cgst"
                                                        },
                                                        {
                                                            "value": 100,
                                                            "percent": 2.5,
                                                            "type": "sgst"
                                                        },
                                                        {
                                                            "value": 0,
                                                            "percent": 0,
                                                            "type": "igst"
                                                        }
                                                    ],
                                                    "total_percent": 5,
                                                    "total_value": 350
                                                },
                                                "post_tax_price": 170
                                            },

                                            {
                                                "date": "2018-09-20T00:00:00.111Z",
                                                "pre_tax":
                                                    {
                                                        "list_price": 5000,
                                                        "sale_price": 4000,
                                                        "discounts": [
                                                            {
                                                                "discount_applied": 1000,
                                                                "coupon_code": "EP",
                                                                "discount_per_discount_type": [
                                                                    {
                                                                        "discount_id": "313",
                                                                        "name": "default name",
                                                                        "type": "Explicit",
                                                                        "code": "BOOKNOW",
                                                                        "post_conditions": []
                                                                    }
                                                                ]
                                                            },

                                                            {
                                                                "discount_applied": 1000,
                                                                "coupon_code": "EP",
                                                                "discount_per_discount_type": [
                                                                    {
                                                                        "discount_id": "314",
                                                                        "name": "default name",
                                                                        "type": "Explicit",
                                                                        "code": "BOOKNOW",
                                                                        "post_conditions": []
                                                                    }
                                                                ]
                                                            }
                                                        ]
                                                    }
                                                ,
                                                "tax": {
                                                    "breakup": [
                                                        {
                                                            "value": 250,
                                                            "percent": 2.5,
                                                            "type": "cgst"
                                                        },
                                                        {
                                                            "value": 100,
                                                            "percent": 2.5,
                                                            "type": "sgst"
                                                        },
                                                        {
                                                            "value": 0,
                                                            "percent": 0,
                                                            "type": "igst"
                                                        }
                                                    ],
                                                    "total_percent": 5,
                                                    "total_value": 350
                                                },
                                                "post_tax_price": 180
                                            }

                                        ]
                                    }

                                ]
                            }



                        ]
                      }
                    ]
                  }
                }
