import logging
from datetime import datetime

from django.core.validators import RegexValidator
from django.http import JsonResponse
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

from apps.common import date_time_utils
from apps.common.utils import round_to_nearest_integer
from apps.pricing.exceptions import SoaError, RequestError, CheckinDateError
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.services.pricing_service import PricingService
from base.views import validators
from base.views.api import TreeboAPIView
from webapp.apps.bookingstash.service.availability_service import get_availability_service
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)

def validate_checkin_checkout(checkin, checkout):
    if checkin >= checkout:
         raise CheckinDateError("Checkin Date is Greater than or qual to Checkout date", HTTP_400_BAD_REQUEST)

class PricingSerializer(serializers.Serializer):
    # TODO: Temp change only
    hotel_id = serializers.CharField(
        max_length=1000,
        validators=[
            RegexValidator(
                regex='^(?:\d+,?)+$',
                message='Invalid value for hotel_id parameter. '
                'Please provide a comma-separated list of hotel_ids',
                code='invalid_hotel_ids')])
    checkin = serializers.DateField()
    checkout = serializers.DateField()
    roomconfig = serializers.CharField(
        max_length=100, validators=[
            validators.validate_roomconfig])


class PricingAPIView(TreeboAPIView):
    pricing_service = PricingService()

    validationSerializer = PricingSerializer

    def get(self, request, *args, **kwargs):
        try:
            hotel_ids = request.GET.get('hotel_id').split(",")
            check_in = request.GET.get('checkin')
            check_out = request.GET.get('checkout')
            validate_checkin_checkout(check_in, check_out)
            room_config = request.GET.get('roomconfig')
        except Exception as e:
            return JsonResponse(
                dict(
                    error=dict(
                        code=HTTP_400_BAD_REQUEST,
                        msg=e.message)),
                status=HTTP_400_BAD_REQUEST)

        if not len(hotel_ids):
            return JsonResponse(
                {
                    "error": {
                        "code": HTTP_400_BAD_REQUEST,
                        "msg": "hotels should not none"}},
                status=HTTP_400_BAD_REQUEST)

        is_soa_enabled = FeatureToggleAPI.is_enabled("pricing", "soa", False)
        if is_soa_enabled:
            try:
                data = self.pricing_service.get_price_from_soa(
                    checkin=check_in,
                    checkout=check_out,
                    hotel_ids=hotel_ids,
                    room_config=room_config,
                    include_price_breakup=False,
                    get_from_cache=True)
                return Response(
                    self._transform_pricing_for_display(
                        data,
                        hotel_ids,
                        room_config,
                        check_in,
                        check_out))
            except RequestError as e:
                return JsonResponse(
                    {"error": {"code": e.status, "msg": str(e)}}, status=HTTP_400_BAD_REQUEST)
            except SoaError as e:
                return JsonResponse(
                    {"error": {"code": e.code, "msg": e.msg}}, status=e.status)

            except Exception as e:
                slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                            dev_msg="WEB Pricing V2",message=e.__str__(),
                                                            class_name=self.__class__.__name__)
                return JsonResponse(
                    {
                        "error": {
                            "code": HTTP_500_INTERNAL_SERVER_ERROR,
                            "msg": str(e)}},
                    status=HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            data = self.pricing_service.get_from_local(
                check_in, check_out, hotel_ids, room_config)
            return Response(data)

    def _transform_pricing_for_display(
            self,
            pricing_data,
            hotel_id_list,
            room_config,
            check_in,
            check_out):
        """
        Transforms the prices returned from pricing service to what the display requires
        :param pricing_data: Pricing Service response
        :param hotel_id_list: List of hotel ids
        :param room_config: room config
        :param check_in: check in
        :param check_out: check out
        :return:
        """
        hotel_prices = {hotel["hotel_id"]: hotel["rooms"]
                        for hotel in pricing_data["data"]["hotels"]}
        room_availability_per_hotel = get_availability_service().get_available_rooms_for_hotels(
            hotel_id_list, check_in, check_out, room_config)
        logger.info(
            "Room Availability per hotel: %s",
            room_availability_per_hotel)
        final_data = {}
        hotel_ids = [int(hotel_id) for hotel_id in hotel_id_list]
        for hotel_id in hotel_ids:
            hotel_price = hotel_prices.get(hotel_id)
            if not hotel_price:
                final_data[hotel_id] = {
                    "cheapest_room": {
                        "room_type": "",
                        "availability": 0
                    },
                    "tax": 0,
                    "pretax": 0,
                    "rack_rate": 0,
                    "sell": 0,
                    "discount_percent": 0,
                }
            else:
                hotel_price_group_by_room_type = {
                    price['room_type']: price for price in hotel_price}
                sorted_rooms = sorted(
                    hotel_price, key=lambda e: e["price"]["sell_price"])
                for room in sorted_rooms:
                    room_type = room["room_type"]
                    available_rooms = room_availability_per_hotel.get(
                        (hotel_id, room_type), 0)
                    if available_rooms > 0 and hotel_price_group_by_room_type[
                            room_type]["price"]["base_price"] > 0:
                        break
                number_of_available_rooms = 0
                room_type = room["room_type"]
                if room["price"]["rack_rate"]:
                    discount_percent = round_to_nearest_integer(
                        room["price"]["promo_discount"] * 100 / room["price"]["base_price"])
                else:
                    discount_percent = 0

                if not FeatureToggleAPI.is_enabled("pricing", "pretax", False):
                    final_data[hotel_id] = {
                        "cheapest_room": {
                            "room_type": room["room_type"],
                            "availability": available_rooms if available_rooms else 0
                        },
                        "tax": room["price"]["tax"],
                        "pretax": room["price"]["pretax_price"],
                        "rack_rate": room["price"]["rack_rate"],  # base_price
                        "sell": room["price"]["sell_price"],
                        "post_tax": room["price"]["sell_price"],
                        "discount_percent": discount_percent
                    }
                else:
                    final_data[hotel_id] = {
                        "cheapest_room": {
                            "room_type": room["room_type"],
                            "availability": available_rooms if available_rooms else 0
                        },
                        "tax": room["price"]["tax"],
                        "pretax": room["price"]["pretax_price"],
                        "rack_rate": room["price"]["base_price"],  # rack_rate
                        "sell": room["price"]["pretax_price"],
                        "post_tax": room["price"]["sell_price"],
                        "discount_percent": discount_percent
                    }

        return final_data
