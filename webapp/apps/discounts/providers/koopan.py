import logging

import requests
from django.conf import settings

logger = logging.getLogger(__name__)


class Koopan:

    def __init__(self, channel, application, sub_channel, cs_id):
        self.channel = channel
        self.application = application
        self.sub_channel = sub_channel
        self.cs_id = cs_id

    def get_all_promotions(self):
        promotions = []
        params = {
            "channel": self.channel,
            "application": self.application,
            "sub_channel": self.sub_channel,
            "cs_id":self.cs_id
        }
        headers = {'Content-Type': 'Application/json'}
        response = requests.get(
            url=settings.DISCOUNT_HOST+'/v1/promotions/featured',
            params=params,
            headers=headers)
        if response.status_code == 200:
            promotions = response.json()["data"]
        else:
            logger.error("Discount Promotion Fetch from Promotion Api Failed")

        return promotions