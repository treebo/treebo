import logging
from math import sin, cos, sqrt, atan2, radians

import requests

from apps.common.exceptions.custom_exception import GoogleLocationException, StateNotFoundException
from dbcommon.models.location import State

logger = logging.getLogger(__name__)


class LocaiontService():
    def get_state(self, lat, lng):
        locations_data = self.get_location_from_google(lat, lng)
        state_name = ''
        for location in locations_data:
            address_components = location['address_components']
            for address_component in address_components:
                types = address_component['types']
                if 'administrative_area_level_1' in types:
                    state_name = address_component['long_name']
        state = State.objects.filter(name__icontains=state_name).first()
        logger.info("lat,lng %s %s - %s %s ", lat, lng, state_name, state)
        if not state:
            raise StateNotFoundException(' state %s ' % state_name)
        return state

    def get_location_from_google(self, lat, lng):
        headers = {}
        base_url = "http://maps.googleapis.com/maps/api/geocode/json?latlng={lat},{lng}&sensor=false"
        url = base_url.format(**{
            'lat': lat,
            'lng': lng
        })
        response = requests.get(url, headers=headers)
        if response.ok:
            return response.json()['results']
        raise GoogleLocationException(str(response.text), response.status_code)

    def get_distance_between_two_coordinate_in_meters(
            self, lat1, lon1, lat2, lon2):

        # approximate radius of earth in m
        R = 6373000.0
        lat1 = radians(lat1)
        lon1 = radians(lon1)
        lat2 = radians(lat2)
        lon2 = radians(lon2)
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        distance = R * c
        return abs(distance)
