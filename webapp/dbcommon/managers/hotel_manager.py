from django.db import models
from common.constants import common_constants as const


class HotelManager(models.Manager):
    def get_queryset(self):
        return super(
            HotelManager,
            self). get_queryset().filter(
            status=const.ENABLED).prefetch_related(
            'image_set',
            'facility_set',
            'hotel_highlights').select_related(
                'city',
                'state',
            'locality',
            'hotel_alias')


class HotelManagerWithoutPrefetch(models.Manager):
    def get_queryset(self):
        return super(
            HotelManagerWithoutPrefetch,
            self). get_queryset().filter(
            status=const.ENABLED).select_related(
            'city',
            'state',
            'locality')
