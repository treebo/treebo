from django.db import models
from djutil.models import TimeStampedModel


class TrivagoFileUploadModel(TimeStampedModel):
    trivago_file = models.FileField(upload_to='trivago/%Y/%m/%d')
