from django.db import models

# Create your models here.
from djutil.models import TimeStampedModel

from dbcommon.models.default_permissions import DefaultPermissions


class ApkDetails(TimeStampedModel, DefaultPermissions):
    app_id = models.PositiveIntegerField(default=1)
    name = models.CharField(max_length=200)
    description = models.TextField(max_length=2000, null=True, blank=True)
    version = models.PositiveIntegerField(default=1)
    apkurl = models.URLField(
        max_length=200,
        null=False,
        blank=True,
        default='')

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_apkdetails'

    def __str__(self):  # __unicode__ on Python 2
        return self.name + " " + str(self.version)
