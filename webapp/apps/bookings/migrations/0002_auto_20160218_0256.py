# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('order_id', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=30)),
                ('amount', models.CharField(max_length=50)),
                ('gateway_type', models.CharField(max_length=50)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='booking',
            name='paymentorder',
            field=models.ForeignKey(default='', blank=True, to='bookings.PaymentOrder', null=True, on_delete=models.DO_NOTHING),
        ),
    ]
