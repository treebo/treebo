import logging
import re, json

from ipware.ip import get_ip

from apps.checkout.constants import TREEBO_SYSTEM_MAILID, PREPAID_WITH_PAYMENT_OFFER_BOOKING_MODE
from apps.checkout.models import BookingRequest, CommittedPrice
from apps.checkout.serializers import BookingRequestSerializer
from apps.checkout.tasks import send_audit_confirmation_mail
from apps.discounts.exceptions import InvalidCouponException
from apps.discounts.services.discount_coupon_services import DiscountCouponServices
from apps.fot.models import FotReservationsRequest, Fot
from apps.hotels import utils as hotel_utils
from apps.pages.helper import PageHelper
from apps.pricing.dateutils import date_to_ymd_str
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.profiles import utils as profile_utils
from apps.profiles.service.user_registration import UserRegistration
from dbcommon.models.profile import User
from services.restclient.pricingrestclient import PricingClient
from services.restclient.stashrestclient import BookingStashClient
from django.conf import settings
from web_sku.catalog.services.cs_type_mappers import CsTypeMappers

logger = logging.getLogger(__name__)


def create_booking_request(request, quickbook_data):
    cs_type_mappers = CsTypeMappers()
    utm_source = str(
        request.COOKIES.get('utm_source')).lower() if request.COOKIES.get('utm_source', None) else \
        quickbook_data.utm_source

    utm_tracking_id = None
    if utm_source.lower() == settings.TRIVAGO_UTM_SOURCE.lower() and quickbook_data.utm_tracking_id:
        utm_tracking_id = quickbook_data.utm_tracking_id

    if not quickbook_data.room_type:
        quickbook_data.room_type = BookingStashClient.getCheapestRoomType(
            request,
            quickbook_data.checkin_date_string,
            quickbook_data.checkout_date_string,
            quickbook_data.hashed_hotel_id,
            quickbook_data.room_config)

    adults, children, room_required = PageHelper.parseRoomConfig(
        quickbook_data.room_config)
    user = request.user
    booking_request_data = {
        "room_type": quickbook_data.room_type,
        "hotel_id": quickbook_data.hotel_id,
        "checkin_date": quickbook_data.checkin_date,
        "checkout_date": quickbook_data.checkout_date,
        "room_config": quickbook_data.room_config,
        "booking_channel": quickbook_data.booking_channel,
        "booking_subchannel": cs_type_mappers.get_cs_sub_channel(quickbook_data.booking_channel,
                                                                 utm_source),
        "booking_application": cs_type_mappers.get_cs_application(quickbook_data.booking_channel),
        "adults": adults,
        "children": children,
        "room_required": room_required,
        "http_host": request.META['HTTP_HOST'],
        "utm_source": utm_source,
        "utm_medium": str(
            request.COOKIES.get('utm_medium')) if request.COOKIES.get('utm_medium') else "",
        "utm_campaign": str(
            request.COOKIES.get('utm_campaign')) if request.COOKIES.get('utm_campaign') else "",
        "host_ip_address": str(
            get_ip(request)) if get_ip(request) else "",
        "rate_plan": quickbook_data.rate_plan,
        "wallet_applied": quickbook_data.apply_wallet,
        "user_login_state": 'LOGGEDIN' if user and user.is_authenticated() else 'LOGGEDOUT',
        "utm_tracking_id": utm_tracking_id
    }
    serializer = BookingRequestSerializer(data=booking_request_data)
    if not serializer.is_valid():
        logger.error("Serializer errors: %s", serializer.errors)
        raise Exception(serializer.errors)
    else:
        booking_request = serializer.save()
        logger.info("BookingRequest created with ID: %s", booking_request.id)
        return booking_request


def register_guest_user(booking_request, referral_click_id=None):
    """
    register guest user to website
    :param booking_request:
    :param referral_click_id:
    :return:
    """
    referral_code, click_id = '', ''
    first_name, last_name = profile_utils.build_name(booking_request.name)
    logger.debug(
        "firstname = " +
        first_name +
        "last_name " +
        last_name +
        "email  =" +
        booking_request.email +
        "referral_click_id =" +
        referral_click_id)

    if referral_click_id:
        logger.info(
            "Register guest user called for referral program with friend referral code %s for guest %s",
            referral_click_id,
            booking_request.email)

        referral_code = referral_click_id.split('-')[1]
        click_id = referral_click_id
    user_registration = UserRegistration()
    user_dto = {
        'email': booking_request.email if booking_request.email else None,
        "first_name": first_name,
        "last_name": last_name,
        "phone_number": booking_request.phone,
        "password": profile_utils.generateRandomPassword(),
    }
    guest_user = user_registration.register_internally(
        user_dto, click_id=click_id, referral_code=referral_code)
    logger.info("Registered guest user with id: %s", guest_user.id)
    return guest_user


def validate_coupon_code(booking_request, user):
    if not FeatureToggleAPI.is_enabled(settings.DOMAIN, "pricing_revamp", False):
        valid, message = DiscountCouponServices.post_booking_validate(
            booking_request.coupon_code)
        if not valid:
            raise InvalidCouponException(message)


def save_booking_state(booking_payload, booking_mode=None):
    booking_request_id = booking_payload["bid"]
    booking_request = BookingRequest.objects.get(pk=booking_request_id)
    user = booking_payload.get("user")
    logged_user_id = ''
    if user and user.is_authenticated():
        logged_user_id = str(user.id)
    DiscountCouponServices.validate_referral_coupon(
        booking_request.coupon_code,
        booking_payload['email'],
        booking_payload['phone'],
        logged_user_id,
        booking_payload.get(
            'otp_verified_number',
            ""))
    booking_request.instant_booking = booking_payload.get("instant_booking")
    booking_request.is_audit = booking_payload.get("is_audit", False)
    booking_request.pay_at_hotel = booking_payload['pay_at_hotel']
    booking_request.organization_name = booking_payload.get(
        'organization_name', '')
    booking_request.organization_address = booking_payload.get(
        'organization_address', '')
    booking_request.organization_taxcode = booking_payload.get(
        'organization_taxcode', '')
    booking_request.payment_mode = booking_payload.get(
        'payment_mode', "Not Paid")
    booking_request.name = booking_payload["name"]
    booking_request.name = re.sub(
        r'[^a-zA-Z ]', '', booking_request.name.strip())
    booking_request.email = booking_payload["email"]
    booking_request.country_code = booking_payload["country_code"]
    booking_request.phone = booking_payload["phone"]
    booking_request.user_signup_channel = booking_payload.get(
        "user_signup_channel")
    booking_request.user_login_state = 'LOGGEDIN' if user and user.is_authenticated() else 'LOGGEDOUT'
    guest_user, clean_email, phone = User.objects.get_existing_user(
        booking_request.email, booking_request.phone, None)
    if not guest_user:
        logger.debug("Registering user with {email} and {phone}".format(email=booking_request.email,
                                                                        phone=booking_request.phone))
        guest_user = register_guest_user(
            booking_request, booking_payload['referral_click_id'])
    booking_request.user = user if user and user.is_authenticated() else guest_user
    booking_request.comments = booking_payload.get("message", "")
    booking_request.checkin_time = booking_payload.get("check_in_time")
    booking_request.checkout_time = booking_payload.get("check_out_time")
    booking_request.meta_http_host = booking_payload.get('http_host', "")
    booking_request.utm_source = booking_payload.get('utm_source', "")
    booking_request.utm_medium = booking_payload.get('utm_medium', "")
    booking_request.utm_campaign = booking_payload.get('utm_campaign', "")
    booking_request.otp_verified_number = booking_payload.get(
        'otp_verified_number', "")
    if booking_mode:
        booking_request = add_price_to_booking_request_and_committed_price(
            booking_request=booking_request, booking_mode=booking_mode)

    booking_request.save()
    validate_coupon_code(booking_request, user)
    return booking_request


def build_special_preference(booking_request):
    special_preferences = ''
    if booking_request.checkin_time is not None or booking_request.checkout_time is not None:
        special_preferences += "[Checkin Time:{0}, Checkout Time:{1}]".format(
            booking_request.checkin_time, booking_request.checkout_time)
    special_preferences += " " + booking_request.comments
    if booking_request.children > 0:
        special_preferences += ". [Number of Children: {0}]".format(
            booking_request.children)
    # if booking_request.is_audit:
    #     special_preferences += ". Finance."
    if booking_request.voucher_amount:
        special_preferences += ". FOT Redemption"
    return special_preferences


def make_fot_reservation(booking_request):
    fot_reservation_request = FotReservationsRequest.objects.filter(
        user_id=booking_request.user_id,
        hotel_id=booking_request.hotel_id,
        status=FotReservationsRequest.PENDING,
        check_in_date=booking_request.checkin_date,
        check_out_date=booking_request.checkout_date,
        no_of_guests=booking_request.adults).first()
    fot_reservation_request.booking_id = booking_request.booking_id
    fot_reservation_request.status = FotReservationsRequest.SCHEDULED
    fot_reservation_request.save()
    logger.info(
        "Fot Reservation Request created with id: %s",
        fot_reservation_request.id)
    try:
        fotUser = Fot.objects.get(user_id=booking_request.user)
        fotUser.conduct_audit()
        logger.info('user stats saved successfully')
        send_audit_confirmation_mail.delay(booking_request.booking_id)
    except Fot.DoesNotExist as e:
        logger.exception("No fot found for user %s", booking_request.user_id)


def build_pricing_for_booking(request, booking_request):
    # FIXME Remove the request object here
    """

    :param request:
    :param apps.checkout.models.BookingRequest booking_request:
    """
    checkin_date_string, checkout_date_string = date_to_ymd_str(
        booking_request.checkin_date), date_to_ymd_str(
        booking_request.checkout_date)

    is_soa_enabled = FeatureToggleAPI.is_enabled("pricing", "soa", False)
    if is_soa_enabled:
        if booking_request.discount_value and booking_request.pretax_amount:
            discount_value = round(
                booking_request.discount_value /
                booking_request.pretax_amount *
                100,
                2)
        else:
            discount_value = 0
        logger.info("Discount Percentage calculated %s", discount_value)
        rJson = PricingClient.get_room_type_price_details(
            request,
            checkin_date_string,
            checkout_date_string,
            booking_request.room_config,
            booking_request.room_type,
            booking_request.coupon_code,
            discount_value,
            "Percentage",
            [
                booking_request.hotel_id],
        )
        import json
        logger.debug(
            'rJson from pricing client: %s ,hotel id %s , room type %s ',
            json.dumps(rJson),
            booking_request.hotel_id,
            booking_request.room_type.lower())
        # Assuming itinerary page would use one hotel and one room type
        hotel_room_price_details = rJson[str(
            booking_request.hotel_id)][booking_request.room_type.lower()]
        total_price_details = hotel_room_price_details['total_price_details']
        breakup_list = __get_night_breakup(hotel_room_price_details['breakup'])
        price = {
            "pretax_price": total_price_details["autopromo_price_without_tax"],
            "base_price": total_price_details["base_price"],
            "promo_discount": total_price_details["promo_discount"],
            "total_tax": total_price_details["tax"],
            "discount_amount": total_price_details["coupon_discount"],
            "actual_total_cost": total_price_details["sell_price"],
            "tax": total_price_details["tax"],
            "coupon_discount": total_price_details["coupon_discount"],
            "sell_price": total_price_details["sell_price"],
            "voucher_amount": booking_request.voucher_amount,
            "nights_breakup": breakup_list,
        }
    else:
        rJson = PricingClient.getDiscountPricingDetails(
            request,
            checkin_date_string,
            checkout_date_string,
            booking_request.coupon_code,
            hotel_utils.generate_hash(
                int(
                    booking_request.hotel_id)),
            booking_request.room_config,
            booking_request.room_type)
        logger.debug('rJson from pricing client: %s', rJson)
        nightsBreakup = rJson['nights']
        price = {
            "pretax_price": rJson["price"],
            "base_price": rJson["pretax_rack_rate_no_rounding"],
            "promo_discount": rJson["rackRate"] -
                              rJson["sellRate"],
            "total_tax": rJson["tax"],
            "discount_amount": rJson["discount"],
            "actual_total_cost": rJson["final_price_rounded_two_decimal"],
            "tax": rJson["tax"],
            "coupon_discount": rJson["discount"],
            "sell_price": rJson["final_price_rounded_two_decimal"],
            "voucher_amount": booking_request.voucher_amount if booking_request.voucher_amount else None,
            "nights_breakup": nightsBreakup,
        }
    update_booking_request_with_prices(booking_request, price)
    return price


def build_pricing_for_booking_with_member_prices(request, booking_request):
    # FIXME Remove the request object here
    """

    :param request:
    :param apps.checkout.models.BookingRequest booking_request:
    """
    checkin_date_string, checkout_date_string = date_to_ymd_str(
        booking_request.checkin_date), date_to_ymd_str(
        booking_request.checkout_date)

    is_soa_enabled = FeatureToggleAPI.is_enabled("pricing", "soa", False)
    if hasattr(
        request,
        'user') and request.user and request.user.is_authenticated():
        logger.info("request.user %s is logged in ", request.user)
        logged_in_user = True
    else:
        logged_in_user = False
    if is_soa_enabled:
        if booking_request.discount_value and booking_request.pretax_amount:
            discount_value = round(
                booking_request.discount_value /
                booking_request.pretax_amount *
                100,
                2)
        else:
            discount_value = 0
        logger.info("Discount Percentage calculated %s", discount_value)
        room_price_breakup = PricingClient.get_room_type_price_details(
            request,
            checkin_date_string,
            checkout_date_string,
            booking_request.room_config,
            booking_request.room_type,
            booking_request.coupon_code,
            discount_value,
            "Percentage",
            [
                booking_request.hotel_id],
        )
        import json
        logger.debug(
            'room_price_breakup from pricing client: %s ,hotel id %s , room type %s ',
            json.dumps(room_price_breakup),
            booking_request.hotel_id,
            booking_request.room_type.lower())
        # Assuming itinerary page would use one hotel and one room type
        if room_price_breakup:
            hotel_room_price_details = room_price_breakup[str(
                booking_request.hotel_id)][booking_request.room_type.lower()]
            total_price_details = hotel_room_price_details['total_price_details']
            breakup_list = __get_night_breakup(
                hotel_room_price_details['breakup'])
            if total_price_details["member_price_discount"]:
                member_discount_applied = True
                member_discount_available = True
            else:
                member_discount_applied = False
                member_discount_available = False
            if logged_in_user and member_discount_applied:
                room_price_breakup = PricingClient.get_room_type_price_details_for_member_pricing(
                    request,
                    checkin_date_string,
                    checkout_date_string,
                    booking_request.room_config,
                    booking_request.room_type,
                    booking_request.coupon_code,
                    discount_value,
                    "Percentage",
                    [
                        booking_request.hotel_id],
                )
                logger.debug(
                    'member pricing - room_price_breakup from pricing client: %s ,hotel id %s , room type %s ',
                    json.dumps(room_price_breakup),
                    booking_request.hotel_id,
                    booking_request.room_type.lower())
                if room_price_breakup:
                    hotel_room_price_details = room_price_breakup[str(
                        booking_request.hotel_id)][booking_request.room_type.lower()]
                    total_price_details = hotel_room_price_details['total_price_details']
                    breakup_list = __get_night_breakup(
                        hotel_room_price_details['breakup'])
                    price = {
                        "pretax_price": total_price_details["autopromo_price_without_tax"],
                        "base_price": total_price_details["base_price"],
                        "promo_discount": total_price_details["promo_discount"],
                        "total_tax": total_price_details["member_price_tax"],
                        "discount_amount": total_price_details["member_price_coupon_discount"],
                        "actual_total_cost": total_price_details["member_sell_price"],
                        "tax": total_price_details["member_price_tax"],
                        "coupon_discount": total_price_details["member_price_coupon_discount"],
                        "sell_price": total_price_details["member_sell_price"],
                        "voucher_amount": booking_request.voucher_amount,
                        "nights_breakup": breakup_list,
                        "member_discount": total_price_details["member_price_discount"],
                        "member_discount_applied": member_discount_applied,
                        "member_discount_available": member_discount_available}
                else:
                    logger.error(
                        "No breakup for room prices with member prices came for hotel %s",
                        booking_request.hotel_id)
            else:
                price = {
                    "pretax_price": total_price_details["autopromo_price_without_tax"],
                    "base_price": total_price_details["base_price"],
                    "promo_discount": total_price_details["promo_discount"],
                    "total_tax": total_price_details["tax"],
                    "discount_amount": total_price_details["coupon_discount"],
                    "actual_total_cost": total_price_details["sell_price"],
                    "tax": total_price_details["tax"],
                    "coupon_discount": total_price_details["coupon_discount"],
                    "sell_price": total_price_details["sell_price"],
                    "voucher_amount": booking_request.voucher_amount,
                    "nights_breakup": breakup_list,
                    "member_discount": 0,
                    "member_discount_applied": False,
                    "member_discount_available": member_discount_available}
        else:
            logger.error(
                "No breakup for room prices came for hotel %s",
                booking_request.hotel_id)
    else:
        room_price_breakup = PricingClient.getDiscountPricingDetails(
            request,
            checkin_date_string,
            checkout_date_string,
            booking_request.coupon_code,
            hotel_utils.generate_hash(
                int(
                    booking_request.hotel_id)),
            booking_request.room_config,
            booking_request.room_type)
        logger.debug(
            'room_price_breakup from pricing client: %s',
            room_price_breakup)
        nightsBreakup = room_price_breakup['nights']
        price = {
            "pretax_price": room_price_breakup["price"],
            "base_price": room_price_breakup["pretax_rack_rate_no_rounding"],
            "promo_discount": room_price_breakup["rackRate"] -
                              room_price_breakup["sellRate"],
            "total_tax": room_price_breakup["tax"],
            "discount_amount": room_price_breakup["discount"],
            "actual_total_cost": room_price_breakup["final_price_rounded_two_decimal"],
            "tax": room_price_breakup["tax"],
            "coupon_discount": room_price_breakup["discount"],
            "sell_price": room_price_breakup["final_price_rounded_two_decimal"],
            "voucher_amount": booking_request.voucher_amount if booking_request.voucher_amount else None,
            "nights_breakup": nightsBreakup,
        }
    update_booking_request_with_prices(
        booking_request, price, logged_in_user=logged_in_user)
    return price


def __get_night_breakup(breakup_resp):
    breakup_list = []
    for date, price_details in list(breakup_resp.items()):
        breakup_dict = {
            'date': date,
            "pretax_price": round(price_details["pretax_price"], 2),
            "tax": round(price_details["tax"], 2),
            "sell_rate": round(price_details["sell_price"], 2),
            "discount": round(price_details["discount"], 2),
            "rack_rate": round(price_details["rack_rate"], 2),
            "final_price": round(price_details["sell_price"], 2),
            # What's difference between sell price and final price ?
            "base_price": round(price_details["base_price"], 2)
        }
        breakup_list.append(breakup_dict)
    date_wise_sorted_list = sorted(breakup_list, key=lambda k: k['date'])
    return date_wise_sorted_list


def update_booking_request_with_prices(
    booking_request,
    prices,
    logged_in_user=False,
    rate_plan_detail=None):
    """
    rate_plan_detail will be dict of this form:
            {
                "code": selected_rate_plan,
                "description": rate_plan_meta.get("description"),
                "tag": rate_plan_meta.get("tag")
            },
    :param booking_request:
    :param prices:
    :param logged_in_user:
    :param rate_plan_detail:
    :return:
    """
    booking_request.pretax_amount = prices['pretax_price']
    booking_request.rack_rate = prices["base_price"]
    booking_request.mm_promo = prices["promo_discount"]
    booking_request.discount_value = prices["coupon_discount"]
    booking_request.tax_amount = prices["tax"]
    booking_request.member_discount = prices.get('member_discount', 0)
    if rate_plan_detail:
        booking_request.rate_plan_meta = rate_plan_detail
    booking_request.calculate_total_amount()
    booking_request.save()


def sanitise_data(request_json_data):
    '''
    Sanitising name, email, phone and message for non ascii values.
    :param request_json_data:
    :return:
    '''
    request_json_data['name'] = request_json_data.get(
        'name').encode('ascii', 'ignore').strip()

    if request_json_data.get('email') == TREEBO_SYSTEM_MAILID:
        request_json_data['email'] = "".encode('ascii', 'ignore').strip()
    else:
        request_json_data['email'] = request_json_data.get(
            'email').encode('ascii', 'ignore').strip()

    request_json_data['phone'] = request_json_data.get(
        'mobile').encode('ascii', 'ignore')
    request_json_data['message'] = request_json_data.get(
        'message').encode('ascii', 'ignore')

    return request_json_data


def add_price_to_booking_request_and_committed_price(booking_request, booking_mode=None):
    booking_request_price = booking_request.bookingrequest_price.filter(
        booking_mode=booking_mode).first()

    if booking_request_price:
        booking_request.pretax_amount = booking_request_price.pretax_amount
        booking_request.discount_id = booking_request_price.discount_id
        booking_request.discount_value = booking_request_price.discount_value
        booking_request.tax_amount = booking_request_price.tax_amount
        booking_request.wallet_deduction = booking_request_price.wallet_deduction
        booking_request.wallet_applied = booking_request_price.wallet_applied
        booking_request.rate_plan = booking_request_price.rate_plan
        booking_request.coupon_code = booking_request_price.coupon_code
        booking_request.coupon_apply = booking_request_price.coupon_apply
        booking_request.total_amount = booking_request_price.total_amount
        booking_request.part_pay_amount = booking_request_price.part_pay_amount

    if FeatureToggleAPI.is_enabled(settings.DOMAIN, "discount_prepaid_hotfix", False):
        committed_price = CommittedPrice.objects.filter(booking_request=booking_request).first()
        if committed_price:
            datewise_pricing = json.loads(booking_request_price.datewise_pricing)
            committed_price.datewise_pricing = json.dumps(datewise_pricing)
            committed_price.save()

    if booking_request.payment_offer_id:
        payment_offer_br_price = booking_request.bookingrequest_price.filter(
            booking_mode=PREPAID_WITH_PAYMENT_OFFER_BOOKING_MODE).first()
        booking_request.payment_offer_price = payment_offer_br_price.total_amount

    return booking_request
