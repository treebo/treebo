from django.conf.urls import url

from apps.hotels.api.v3.hotel_detail import HotelDetail
from apps.hotels.api.v3.hotel_wifi_info import HotelWifiInfoApi
from apps.hotels.api.v3.hotels import CSHotelDetail
from apps.hotels.api.v3.room_prices import HotelRoomPrices
from apps.hotels.api.v3.update_cache_prices import UpdateCachePrice
from apps.hotels.api.v3.sku import PropertySku

app_name = 'hotels_v3'

urlpatterns = [
    url(r"^(?:(?P<cs_id>[0-9]+)/)?$", CSHotelDetail.as_view(), name='hotel-cs-detail-v3'),
    url(r"^(?P<hotel_id>[0-9]+)/room-prices/$", HotelRoomPrices.as_view(), name='room-prices-v3'),
    url(r"^(?P<hotelogix_id>[0-9]+)/wifi/$", HotelWifiInfoApi.as_view(), name='hotelwifi-info-v3'),
    url(r"^(?P<hotel_id>[0-9]+)/details/$", HotelDetail.as_view(), name='hotel-detail-v3'),
    url(r"^update_cache_prices/", UpdateCachePrice.as_view(), name='update-cache-prices'),
    url(r"^(?P<hotel_id>[0-9]+)/sku/$", PropertySku.as_view(), name='property-sku')
]
