import ast
import datetime
import logging
from datetime import timedelta
import json

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models import Prefetch
from django.template.defaultfilters import slugify
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from apps.common import date_time_utils
from apps.hotels import utils as hotel_utils
from apps.pricing.api.v5.hotel_prices import HotelRoomPricesSerializer
from apps.pricing.transformers.search_api_transformer import SearchAPITransformer
from apps.third_party.service.trivago.price_adaptors.non_rate_plan_prices import NonRatePlanPriceAdaptor
from apps.third_party.service.trivago.price_adaptors.rate_plan_prices import RatePlanPriceAdaptor
from apps.third_party.service.third_party_hotel_service import (
    TRIVAGO_API_VERSION, TRIVAGO_API_LANGUAGE_DEFAULT, TRIVAGO_API_CURRENCY_DEFAULT)
from apps.third_party.utm_source_mapping import UTM
from base import log_args
from base.views.api import TreeboAPIView
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room

logger = logging.getLogger(__name__)

THIRD_PARTY_HOTEL_URL_PREFIX = settings.TREEBO_WEB_URL


class ThirdPartyHotelAvailability(TreeboAPIView):
    price_adaptor = None
    renderer_classes = (JSONRenderer,)
    srp_pricing_service = SearchAPITransformer()

    @log_args()
    def post(self, request, **kwargs):
        try:
            if settings.NRP_ENABLED:
                self.price_adaptor = RatePlanPriceAdaptor()
            else:
                self.price_adaptor = NonRatePlanPriceAdaptor()
            utm_meta = UTM[kwargs['source']] if 'source' in kwargs else UTM['trivago']
            top_order_params, hotels_list, hotel_data_params = ThirdPartyHotelAvailability._parse_third_party_request(request,
                                                                                                                      utm_meta)
            logger.info("parsed Third Party request hotel_list:{} hotel_data_params:{} "
                        "response_root so far:{}".format(json.dumps(hotels_list), hotel_data_params, top_order_params))
            third_party_response_hotel = self.get_cheapest_hotel_details(hotel_data_params, hotels_list)
            top_order_params['hotels'] = third_party_response_hotel
            return Response({'root': top_order_params})
        except Exception as e:
            logger.info(kwargs['source']+' response consolidation failed for ThirdPartyHotelAvailability :')
            return Response(kwargs['source']+' Validation error in received values :', status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def _parse_third_party_request(request, utm_meta):
        try:
            api_version = request.POST.get('api_version')
            if int(api_version) != TRIVAGO_API_VERSION:
                logger.error(api_version)
            hotels_list = request.POST.get('hotels')
            hotels_list = ast.literal_eval(hotels_list)
            check_in_date = request.POST.get('start_date')
            check_in_date = datetime.datetime.strptime(
                check_in_date, date_time_utils.DATE_FORMAT).date()
            current_date = datetime.datetime.now().date()
            check_in_date = current_date if check_in_date < current_date else check_in_date
            free_cancellation = 'false'

            check_out_date = request.POST.get('end_date')
            check_out_date = datetime.datetime.strptime(
                check_out_date, date_time_utils.DATE_FORMAT).date()
            check_out_date = check_in_date + \
                             timedelta(days=1) if check_out_date <= check_in_date else check_out_date

            number_of_rooms = request.POST.get('num_rooms', 1)
            # India is a gross market as per Trivago
            rate_model = request.POST.get('rate_model', 'GROSS')
            request_language = request.POST.get('lang')
            request_currency = request.POST.get('currency')

            if request_language != TRIVAGO_API_LANGUAGE_DEFAULT:
                request_language = TRIVAGO_API_LANGUAGE_DEFAULT
            if request_currency != TRIVAGO_API_CURRENCY_DEFAULT:
                request_currency = TRIVAGO_API_CURRENCY_DEFAULT
            top_order_params = {
                "api_version": api_version,
                "currency": request_currency,
                "start_date": check_in_date,
                "end_date": check_out_date,
                "lang": request_language,
                "rate_model": rate_model,
                "num_rooms": number_of_rooms,
                "hotel_ids": hotels_list}
            rooms_config = ''
            for i in range(1, (int(number_of_rooms) + 1)):
                try:
                    if i != 1:
                        rooms_config += ','
                    adults = request.POST.get('room_adults_' + str(i))
                    children = request.POST.get('room_childs_' + str(i))
                    if not adults:
                        adults = 1
                    else:
                        adults = int(ast.literal_eval(adults))

                    num_adult = adults
                    if not children:
                        children = []
                    else:
                        children = ast.literal_eval(children)
                        top_order_params['room_childs_' + str(i)] = children

                    num_child = len(children)
                    for c_age in children:
                        if int(c_age) > settings.CHILDREN_AGE:
                            num_adult += 1
                            num_child -= 1

                    rooms_config += str(num_adult) + '-' + str(num_child)
                    top_order_params['room_adults_' + str(i)] = adults
                    if children:
                        top_order_params['room_childs_' + str(i)] = children

                except BaseException:
                    logger.info(
                        'adults and children failed for ThirdPartyHotelAvailability')
                    break

            if not bool(rooms_config):
                rooms_config = '1-0'
                top_order_params['room_adults_1'] = '1'

            url_prefix = request.build_absolute_uri('/').rstrip('/')

            hotel_data_params = {
                'check_in_date': check_in_date,
                'check_out_date': check_out_date,
                'rooms_config': rooms_config,
                'rate_model': rate_model,
                'number_of_rooms': int(number_of_rooms),
                'url_prefix': url_prefix,
                'free_cancellation': free_cancellation,
                'utm_source': utm_meta['utm_source'],
                'utm_medium': utm_meta['utm_medium'],
                'utm_campaign': utm_meta['utm_campaign']
            }
            logger.info(
                "top_order_params %s hotels_list %s hotel_data_params %s",
                top_order_params,
                hotels_list,
                hotel_data_params)
            return top_order_params, hotels_list, hotel_data_params
        except BaseException:
            logger.info(
                'Parsing request failed for ' + utm_meta['utm_source'] +' due to Invalid parameters',
                exc_info=True)
            raise

    def generate_validated_data(self, hotel_data_params):
        kwargs = {'checkin': hotel_data_params['check_in_date'],
                  'checkout': hotel_data_params['check_out_date'],
                  'roomconfig': hotel_data_params['rooms_config'],
                  'utm_source': hotel_data_params['utm_source'] if settings.ENABLE_TRIVAGO_AUTO_APPLY else None,
                  }
        serializer_obj = HotelRoomPricesSerializer(data=kwargs)
        serializer_obj.is_valid(raise_exception=True)
        return serializer_obj.validated_data

    def get_cheapest_hotel_details(self, hotel_data_params, hotel_ids):
        try:
            trivago_response_hotel_list = []
            trivago_response_hotel_id_list = []

            # hotel_obj_query_set = Hotel.objects.select_related('city', 'locality', 'state').prefetch_related(
            #     Prefetch('rooms', queryset=Room.objects.prefetch_related("facility_set")),
            #     'facility_set').filter(pk__in=hotel_ids, status=Hotel.ENABLED)
            validated_data = self.generate_validated_data(hotel_data_params)
            validated_data['channel'] = 'website'
            cheapest_prices_available = self.srp_pricing_service.fetch_cheapest_room_prices_available(
                validated_data, hotel_ids, source_of_invoke=hotel_data_params['utm_source'], recommended_required=False)

            transformed_price = self.transformed_prices(cheapest_prices_available)
            for hotel_id in hotel_ids:
                group_room_types = self.get_room_level_detail(transformed_price, hotel_data_params, hotel_id)
                trivago_response_hotel = {'hotel_id': hotel_id, 'room_types': group_room_types}
                trivago_response_hotel_id_list.append(hotel_id)
                trivago_response_hotel_list.append(trivago_response_hotel)
            list_of_hotel_ids_requested_but_not_in_query_set = [item for item in hotel_ids if
                                                                item not in trivago_response_hotel_id_list]
            if list_of_hotel_ids_requested_but_not_in_query_set:
                for hotel_id in list_of_hotel_ids_requested_but_not_in_query_set:
                    trivago_response_hotel = {'hotel_id': hotel_id, 'room_types': []}
                    trivago_response_hotel_list.append(trivago_response_hotel)

            return trivago_response_hotel_list
        except Exception as e:
            logger.error()
            raise e

    def transformed_prices(self, cheapest_prices_available):
        from collections import defaultdict
        hotel_wise_prices = defaultdict(list)
        transformed_map = {}
        if not cheapest_prices_available:
            return transformed_map
        for x in cheapest_prices_available:
            for k, v in list(x.items()):
                hotel_wise_prices[k].append(v)

        for hotel_id, prices in list(hotel_wise_prices.items()):
            sorted_price = sorted(prices, key=lambda k: k['sell_price'])
            transformed_map[hotel_id] = sorted_price

        return transformed_map

    def get_room_level_detail(self, transformed_price_list, hotel_data_params, hotel_id):
        try:
            consolidated_group_room_types = []
            for room_wise_prices in transformed_price_list[hotel_id]:
                hotel = Hotel.objects.filter(id=hotel_id).first()
                if not hotel:
                    return []
                hotel_data_params['hotel_url'] = ThirdPartyHotelAvailability.build_new_hotel_url(hotel,
                                                                                                 hotel_data_params)

                room_type = room_wise_prices['cheapest_room']['room_type']
                pricing_data_params = {}
                final_price, tax_price, pretax_price = \
                    self.price_adaptor.calculate_prices_as_per_trivago(room_wise_prices, hotel_data_params)
                if not any([final_price, tax_price, pretax_price]):
                    pass

                pricing_data_params['final_price'] = final_price
                pricing_data_params['tax_price'] = tax_price
                pricing_data_params['pretax_price'] = pretax_price
                pricing_data_params['room_type'] = room_type
                pricing_data_params['rooms_available'] = room_wise_prices['cheapest_room']['availability']
                pricing_data_params['room_code'] = ThirdPartyHotelAvailability.find_room_code(hotel, room_type)
                pricing_data_params['room_amenities_list'] = ThirdPartyHotelAvailability.fetch_amenities_list(hotel,
                                                                                                              room_type)
                each_room_type = ThirdPartyHotelAvailability._format_trivago_room_type(pricing_data_params,
                                                                                       hotel_data_params)
                group_room_types = []
                for i in range(0, hotel_data_params.get('number_of_rooms', 0)):
                    group_room_types.append(each_room_type)
                consolidated_group_room_types.append(group_room_types)
        except:
            logger.info(
                'Consolidated room group failed for hotel_id %s on params %s' % (hotel_id, hotel_data_params),
                exc_info=True)
            consolidated_group_room_types = []

        return consolidated_group_room_types

    def get_hotel_level_details(self, hotels_list, hotel_data_params):
        try:
            trivago_response_hotel_list = []

            hotel_obj_query_set = Hotel.objects.select_related(
                'city',
                'locality',
                'state').prefetch_related(
                Prefetch(
                    'rooms',
                    queryset=Room.objects.prefetch_related("facility_set")),
                'facility_set').filter(
                pk__in=hotels_list,
                status=Hotel.ENABLED)

            for hotel_obj in hotel_obj_query_set:
                hotel_data_params['hotel_url'] = ThirdPartyHotelAvailability.build_new_hotel_url(
                    hotel_obj, hotel_data_params)
                group_room_types = self.get_room_level_details(
                    hotel_data_params, hotel_obj)
                trivago_response_hotel = {
                    'hotel_id': hotel_obj.id,
                    'room_types': group_room_types}
                trivago_response_hotel_list.append(trivago_response_hotel)

            list_of_hotel_ids_in_query_set = list(
                hotel_obj_query_set.values_list('id', flat=True))
            list_of_hotel_ids_requested_but_not_in_query_set = [
                item for item in hotels_list if item not in list_of_hotel_ids_in_query_set]
            if list_of_hotel_ids_requested_but_not_in_query_set:
                for hotel_id in list_of_hotel_ids_requested_but_not_in_query_set:
                    trivago_response_hotel = {
                        'hotel_id': hotel_id, 'room_types': []}
                    trivago_response_hotel_list.append(trivago_response_hotel)

            return trivago_response_hotel_list

        except Exception as e:
            logger.exception(
                'checking hotel availability failed for ThirdPartyHotelAvailability')
            raise

    @staticmethod
    def build_new_hotel_url(hotel, hotel_data_params):
        try:
            hotel_url_params = '?checkin={0}&checkout={1}&roomconfig={2}'
            hotel_slug = slugify(hotel.name)
            locality_slug = slugify(hotel.locality.name)
            city_slug = slugify(hotel.city.name)
            kwargs = {
                'city_slug': city_slug,
                'hotel_slug': hotel_slug,
                'locality_slug': locality_slug,
                'hotel_id': hotel.id}

            utm_source = hotel_data_params['utm_source']
            if utm_source == settings.TRIVAGO_EXTERNAL_UTM_SOURCE:
                utm_source = settings.TRIVAGO_UTM_SOURCE

            new_hotel_url = reverse('pages:hotel-details-new', kwargs=kwargs) + hotel_url_params.format(
                hotel_data_params['check_in_date'], hotel_data_params['check_out_date'],
                hotel_data_params['rooms_config']) + '&utm_source=' + utm_source + \
                            '&utm_medium=' + hotel_data_params['utm_medium'] + '&utm_campaign=' + hotel_data_params[
                                'utm_campaign']
            hotel_url = THIRD_PARTY_HOTEL_URL_PREFIX + new_hotel_url
        except BaseException:
            logger.exception('Building hotel url failed for third party')
            hotel_url = ''
        return hotel_url

    @staticmethod
    def generate_url_prefix(request=None):
        CUSTOM_PREFIX = settings.SITE_HOST_NAME
        if CUSTOM_PREFIX:
            url_prefix = CUSTOM_PREFIX.rstrip('/')
        elif request:
            url_prefix = request.build_absolute_uri('/').rstrip('/')
        else:
            raise Exception('either CUSTOM_PREFIX or request is compulsory')
        return url_prefix

    def get_room_level_details(self, hotel_data_params, hotel):
        try:
            consolidated_group_room_types = []
            rooms_price_dict = self.price_adaptor.hit_and_fetch_prices_for_room(
                hotel_data_params, hotel)
            for room_type in rooms_price_dict:
                room_details = rooms_price_dict[room_type]
                if room_details['availability']:
                    pricing_data_params = {}
                    final_price, tax_price, pretax_price = \
                        self.price_adaptor.calculate_prices_as_per_trivago(room_details, hotel_data_params)
                    if not any([final_price, tax_price, pretax_price]):
                        continue
                    pricing_data_params['final_price'] = final_price
                    pricing_data_params['tax_price'] = tax_price
                    pricing_data_params['pretax_price'] = pretax_price
                    pricing_data_params['room_type'] = room_type
                    pricing_data_params['rooms_available'] = room_details['available']
                    pricing_data_params['room_code'] = ThirdPartyHotelAvailability.find_room_code(
                        hotel, room_type)
                    pricing_data_params['room_amenities_list'] = ThirdPartyHotelAvailability.fetch_amenities_list(
                        hotel, room_type)
                    each_room_type = ThirdPartyHotelAvailability._format_trivago_room_type(
                        pricing_data_params, hotel_data_params)
                    group_room_types = []
                    for i in range(
                        0, hotel_data_params.get(
                            'number_of_rooms', 0)):
                        group_room_types.append(each_room_type)
                    consolidated_group_room_types.append(group_room_types)
        except BaseException:
            logger.info(
                'Consolidated room group failed for hotel_id %s on params %s' %
                (hotel.id, hotel_data_params), exc_info=True)
            consolidated_group_room_types = []

        return consolidated_group_room_types

    @staticmethod
    def find_room_code(hotel, room_type):
        try:
            max_adult = int(
                hotel.rooms.filter(
                    room_type_code__iexact=room_type).first().max_guest_allowed)
            if max_adult == 3:
                room_code = 'TRIPLE'
            elif max_adult == 2:
                room_code = 'DOUBLE'
            elif max_adult == 1:
                room_code = 'SINGLE'
            else:
                room_code = 'MULTIPLE'
        except BaseException:
            logger.info(
                'Finding room code failed for Third party availability',
                exc_info=True)
            room_code = ' '
        return room_code

    @staticmethod
    def fetch_amenities_list(hotel, room_type):
        try:
            amenities_list = list(
                hotel.rooms.filter(
                    room_type_code=room_type).values_list(
                    'facility__name', flat=True)) + list(
                hotel.facility_set.all().values_list(
                    'name', flat=True))
        except BaseException:
            logger.info('Amenities list failed', exc_info=True)
            amenities_list = []
        return amenities_list

    @staticmethod
    def _format_trivago_room_type(pricing_data_params, hotel_data_params):
        try:
            each_room_type = {}
            room_detail = {}
            room_detail['breakfast_included'] = 'true'  # using default
            room_detail['breakfast_price'] = 0  # using default
            # using default
            room_detail['currency'] = TRIVAGO_API_CURRENCY_DEFAULT
            # sending pretax_price instead of final price as per call since
            # India is a gross market they dont consider city_tax so our fiunal
            # price also must be exclusive of city_tax
            room_detail['final_rate'] = pricing_data_params.get(
                'final_price', 0)
            room_detail['free_cancellation'] = hotel_data_params['free_cancellation']
            # using default 'BB' - is Bed with Breakfast
            room_detail['meal_code'] = 'BB'
            room_detail['payment_type'] = 'postpaid'  # using default
            room_detail['room_amenities'] = pricing_data_params.get(
                'room_amenities_list', [])
            # room_detail['room_code'] = pricing_data_params['roomId'] if pricing_data_params.get('roomId') else ''
            room_detail['room_code'] = pricing_data_params.get('room_code')
            room_detail['rooms_left'] = pricing_data_params.get(
                'rooms_available', 0)
            room_detail['net_rate'] = pricing_data_params.get('final_price', 0)
            rate_model = (hotel_data_params.get('rate_model')).lower()
            if rate_model in ['gross', 'ai']:
                room_detail['vat'] = 0  # using default
                room_detail['service_charge'] = 0  # using default
                room_detail['booking_fee'] = 0  # using default
                if rate_model == 'ai':
                    room_detail['net_rate'] = pricing_data_params.get(
                        'pretax_price', 0)
                    room_detail['hotel_fee'] = 0  # using default
                    room_detail['local_tax'] = pricing_data_params.get(
                        'tax_price', 0)
                    room_detail['resort_fee'] = 0  # using default
            hotel_url = hotel_data_params.get('hotel_url', '')
            room_type = pricing_data_params['room_type']
            if hotel_url:
                hotel_url = hotel_data_params['hotel_url'] + \
                            '&roomtype=' + room_type
            room_detail['url'] = hotel_url
            each_room_type[room_type] = room_detail
        except BaseException:
            logger.exception('Format trivago failed')
            each_room_type = []
        return each_room_type


'''

TRIVAGO API RESPONSE STRUCTURE for HOTEL AVAILABILITY

root: {topOrder Parameters, hotelids[], hotel[{ hotelid, room_types [[{type1},{type2},{type3}]] }] }

type1 = {"Brilliant Doppelzimmer ":
                            {"booking_fee": 0,  # no provision
                             "breakfast_included": "true",  # no provision
                             "breakfast_price": "7.20",  # no provision
                             "currency": "EUR",  # no provision
                             "final_rate": 0,
                             "free_cancellation": "true",  # no provision
                             "hotel_fee": 0,  # no provision
                             "local_tax": 0,
                             "meal_code": "RO",  # no provision
                             "net_rate": 0,
                             "payment_type": "prepaid",  # no provision
                             "resort_fee": 0,  # no provision
                             "room_amenities": [
                                 "BREAKFAST"
                             ],
                             "room_code": "DOUBLE",  # no provision
                             "rooms_left": "",
                             "service_charge": 0,  # no provision
                             "url": "http://advertiser-site.com/hoteladlon/Executive_Brilliant__Doppelzimmer?start_date=2015-04-28&end_date=2015-04-29&num_adults=2",
                             "vat": 40  # no provision
                    }
                }


TRIVAGO API RESPONSE STRUCTURE for HOTEL DATA

{
"api_version" : 4,
"lang" : "de_DE",
"hotels" :
    [
        {
            "partner_reference" : "5568",
            "name" : "Hotel Adlon",
            "street" : "Unter den Linden 77", -hotels
            "city" : "Berlin",
            "postal_code" : "10117",  - hotels locality
            "state" : "NRW", - hotels state
            "country" : "Germany",
            "latitude" : 52.516240,
            "longitude" : 13.380437,
            "desc" : "Lorem ipsium",
            "amenities" : ["Beauty Center", "Business Center", "Cafe/ Bistro", "Dampfbad"], - hotelsfacility hotels
            "url" : "http://advertiser-site.com/hoteladlon",
            "email" : "adlon@adlon.com",
            "phone" : "030-123123",
            "fax" : "030-12341234",
            "room_types"  :
                {
                    "Executive Doppelzimmer" : -hotels_room
                        {
                            "url" : "http://advertiser-site.com/hoteladlon/Executive_Doppelzimmer",
                            "desc" : "Lorem ipsium"
                        },
                    "Superior Deluxe Doppelzimmer" :
                        {
                            "url" : "http://advertiser-site.com/hoteladlon/Superior_Deluxe_Doppelzimmer",
                            "desc" : "Lorem ipsium."
                        }
                }
        }
    ]
}

'''
