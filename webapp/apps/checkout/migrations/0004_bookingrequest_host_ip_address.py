# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0003_auto_20170127_1030'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='host_ip_address',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
