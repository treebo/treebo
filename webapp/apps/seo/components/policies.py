from apps.seo.components.base_component import BaseComponent

__author__ = 'ansilkareem'


class Policies(BaseComponent):

    def __init__(self, current_city, footer_data, hotel):
        self.hotel = hotel
        super(Policies, self).__init__(current_city, footer_data, [])

    def build(self):

        policies = self.footer_data['policies'] if 'policies' in self.footer_data else [
        ]
        if self.hotel.checkin_time:
            hour = self.hotel.checkin_time.hour
            minute = self.hotel.checkin_time.minute
            if hour >= 12:
                if hour > 12:
                    hour = hour - 12
                temp = 'PM'
            else:
                temp = 'AM'
            if minute < 10:
                minute = "{0:0=2d}".format(minute)
            checkin_time = str(hour) + ":" + str(minute) + ' ' + temp
        else:
            checkin_time = '12:00 PM'
        if self.hotel.checkout_time:
            hour = self.hotel.checkout_time.hour
            minute = self.hotel.checkout_time.minute
            if hour >= 12:
                if hour > 12:
                    hour = hour - 12
                temp = 'PM'
            else:
                temp = 'AM'
            if minute < 10:
                minute = "{0:0=2d}".format(minute)
            checkout_time = str(hour) + ":" + str(minute) + ' ' + temp
        else:
            checkout_time = '11:00 AM'
        content = [{'title': 'Check-In Time',
                    'description': 'Standard Check-In Time is ' + checkin_time},
                   {'title': 'Check-Out Time',
                    'description': 'Standard Check-Out Time is ' + checkout_time}]
        content.extend(policies)
        data = {
            'title': 'POLICIES',
            'content': content
        }
        return data
