import mock

from django.test import SimpleTestCase

from apps.auth.services import user_verification_service
from apps.auth.services.user_verification_service import UserVerificationService
from dbcommon.models.profile import User, UserReferral


class UserVerificationServiceTest(SimpleTestCase):
    def setUp(self):
        pass

    @mock.patch.object(UserVerificationService,
                       '_UserVerificationService__verify_user_otp')
    @mock.patch.object(UserVerificationService, '_post_verification_hook')
    def test_verify_user(
            self,
            mock_post_verification_hook,
            mock_verify_user_otp):
        mock_verify_user_otp.return_value = True
        mock_post_verification_hook.return_value = {"status": 200}
        verified, _ = UserVerificationService.verify_user(
            mock.Mock(), mock.Mock(), mock.Mock())
        self.assertEqual(verified, mock_verify_user_otp.return_value)
        self.assertTrue(mock_verify_user_otp.called)
        self.assertTrue(mock_post_verification_hook.called)

    @mock.patch.object(user_verification_service.OtpService, 'veriy_otp')
    @mock.patch.object(user_verification_service.User, 'objects')
    def test_verify_user_otp_not_verified(
            self, mock_user_manager, mock_otp_verify):
        mock_otp_verify.return_value = False
        phone_no = '900000000'
        mock_user = mock.Mock(
            spec=User, **{'phone_number': phone_no, 'is_otp_verified': False})
        mock_user_manager.get.return_value = mock_user
        verified = UserVerificationService._UserVerificationService__verify_user_otp(
            mock.Mock(), phone_no, mock.Mock())
        self.assertFalse(verified)
        self.assertTrue(mock_otp_verify.called)

    @mock.patch.object(user_verification_service.OtpService, 'veriy_otp')
    @mock.patch.object(user_verification_service.User, 'objects')
    def test_verify_user_otp_already_verified(
            self, mock_user_manager, mock_otp_verify):
        mock_otp_verify.return_value = False
        phone_no = '900000000'
        mock_user = mock.Mock(
            spec=User, **{'phone_number': phone_no, 'is_otp_verified': True})
        mock_user_manager.get.return_value = mock_user
        verified = UserVerificationService._UserVerificationService__verify_user_otp(
            mock.Mock(), phone_no, mock.Mock())
        self.assertTrue(verified)
        self.assertFalse(mock_otp_verify.called)
        # Assert User.save() is not called
        self.assertFalse(mock_user.save.called)

    @mock.patch.object(user_verification_service.OtpService, 'veriy_otp')
    @mock.patch.object(user_verification_service.User, 'objects')
    def test_verify_user_otp_new_verification_success(
            self, mock_user_manager, mock_otp_verify):
        mock_otp_verify.return_value = True
        phone_no = '900000000'
        mock_user = mock.Mock(
            spec=User, **{'phone_number': phone_no, 'is_otp_verified': False})
        mock_user_manager.get.return_value = mock_user
        verified = UserVerificationService._UserVerificationService__verify_user_otp(
            mock.Mock(), phone_no, mock.Mock())
        self.assertTrue(verified)
        self.assertTrue(mock_otp_verify.called)
        # Assert User is saved with new otp_verified value
        self.assertTrue(mock_user.save.called)
        self.assertTrue(mock_user.is_otp_verified)

    @mock.patch.object(user_verification_service.UserReferral, 'objects')
    @mock.patch.object(
        user_verification_service.tasks.send_sign_up_event,
        'delay')
    def test_post_verification_hook(
            self,
            mock_celery_task_call,
            mock_userreferral_manager):
        mock_user = mock.Mock(spec=User, **{'id': 1})
        mock_userreferral_manager.select_related.return_value.get.return_value = mock.Mock(
            spec=UserReferral, **{'user': mock_user})
        result = UserVerificationService._post_verification_hook(mock.Mock())
        mock_celery_task_call.assert_called_once_with(mock_user.id)
        self.assertEqual(result, {"status": 200, "data": ""})
