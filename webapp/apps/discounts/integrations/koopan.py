from apps.discounts.providers.koopan import Koopan as KoopanProvider


class Koopan:

    def __init__(self, abw, channel, application, sub_channel, cs_id):
        self.abw = abw
        self.channel = channel
        self.application = application
        self.sub_channel = sub_channel
        self.cs_id = cs_id

    def get_applicable_promotions(self):
        all_available_promotions = KoopanProvider(channel=self.channel, sub_channel=self.sub_channel,
                                                  application=self.application, cs_id=self.cs_id).get_all_promotions()
        response_data = []
        for promotion in all_available_promotions:
            if promotion['abw_start'] <= self.abw <= promotion['abw_end']:
                response_data.append(
                    dict(code=promotion['code'], name=promotion['name'], description=promotion['description'],
                         incentive_value=promotion['incentive_value'],
                         incentive_value_type=promotion['incentive_value_type'], is_prepaid=promotion['is_prepaid']))

        return response_data
