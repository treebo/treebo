from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.profiles.service.user_registration import UserRegistration
from base.token_helper import TokenHelper
import logging

logger = logging.getLogger(__name__)


class SocialLoginService(object):
    auth_client = TreeboAuthClient()
    token_helper = TokenHelper()
    user_registration = UserRegistration()

    def google_login(self, social_login_dto):
        google_token = social_login_dto['token']
        logger.info(" google login with token %s ", google_token)
        token_info = self.auth_client.convert_token(
            google_token, 'google_plus_custom')
        logger.debug(
            " google token is valid %s for token %s ",
            token_info,
            google_token)
        user, user_data = self.user_registration.register_user_with_token(
            token_info['access_token'])
        self.token_helper.save_access_token(token_info, user)
        return user, token_info['access_token'], user_data

    def external_app_login(self, social_login_dto, external_app_name='facebook'):
        external_grand_token = social_login_dto['token']
        logger.info(" fb login with token %s ", external_grand_token)
        token_info = self.auth_client.convert_token(external_grand_token, external_app_name)
        logger.debug(
            " fb token is valid %s for token %s  ",
            token_info,
            external_grand_token)
        user , user_data = self.user_registration.register_user_with_token(
            token_info['access_token'])
        self.token_helper.save_access_token(token_info, user)
        return user, token_info['access_token'] , user_data
