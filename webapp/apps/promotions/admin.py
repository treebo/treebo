# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.promotions.models import PromotedHotels
from apps.promotions.models import MetaRedirectionHotels


@admin.register(PromotedHotels)
class PromotedHotelsAdmin(admin.ModelAdmin):
    list_display = (
        'hotel_id', 'priority'
    )

    search_fields = (
        'hotel_id', 'priority'
    )


@admin.register(MetaRedirectionHotels)
class MetaRedirectionHotelsAdmin(admin.ModelAdmin):
    list_display = (
        'hotel_id', 'redirection_type'
    )

    search_fields = (
        'hotel_id', 'redirection_type'
    )
