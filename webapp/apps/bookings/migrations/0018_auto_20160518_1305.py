# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0017_auto_20160518_1006'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bookingrequest',
            options={},
        ),
        migrations.AlterModelOptions(
            name='paymentorder',
            options={},
        ),
        migrations.AlterModelOptions(
            name='roombooking',
            options={},
        )
    ]
