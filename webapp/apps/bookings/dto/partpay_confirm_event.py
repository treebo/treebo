

class PartpayStatusForGrowth(object):
    CONFIRM = 'CONFIRM'

class PartpayConfirmReasons(object):
    UPFRONT_PARTPAY = 'UPFRONT_PART_PAY'

class PartpayConfirmEvent(object):
    def __init__(
            self,
            hotel_code,
            booking_code,
            group_code,
            status,
            cancellation_reason=None,
            confirmation_reason=None
    ):
        self.hotel_code = hotel_code
        self.booking_code = booking_code
        self.group_code = group_code
        self.status = status
        self.cancellation_reason = cancellation_reason
        self.confirmation_reason = confirmation_reason

