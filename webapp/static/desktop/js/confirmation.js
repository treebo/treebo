'use strict';

import $ from 'jquery';
import {Observable} from "rx";
import {_} from 'underscore';
import "slick-carousel";
import BaseView from './treebolibs/baseview';
import searchWidget from './widgets/search';
import auth from './auth';

import React from 'react';
import {render} from 'react-dom';
import { createStore, combineReducers } from 'redux';
import Discount from './widgets/discount';
import cookies from 'js-cookie';
import '../css/confirmation.less';

window.$ = $;

let treejax = require('./treebolibs/treejax');
let ConfirmationView = Object.create(BaseView);

let $emailPdfLoader = $("#emailPdfLoader");
let store = {};

let methods = {
    init : ()=>{
        $("#sendPdf").on("click", e => {
            e.preventDefault();
            $(".email-pdf").toggleClass("dd-open");

        })
        let submitpdfEvent = Rx.Observable.fromEvent($('#pdfmailSubmit'), 'click');
        let submitpdfsubscription = submitpdfEvent.subscribe(e => {
            e.preventDefault();
            const pdfParsley = $('#sendPdfForm').parsley();
            pdfParsley.validate();
            if(pdfParsley.isValid()){
                methods.sendEmail();
            };
        });
    },

    sendEmail : ()=>{
        let data = JSON.stringify();
        let email = $("#emailpdfEmail").val();
        let orderId = $("#orderId").val();
        $emailPdfLoader.removeClass('hide');
        $('#sendpdfmsg').hide();
        const p = treejax({
                url: `/api/v1/checkout/resendemail?email=${email}&order_id=${orderId}`,
                type: 'get'
            }).then((data, status, xhr) => {
                $('#sendpdfmsg').show();
                $emailPdfLoader.addClass('hide');
            },treeboError => {
                console.log(treeboError);
            }).finally(() => {
                setTimeout(() => {
                          $(".email-pdf").toggleClass("dd-open");
                          $("#sendPdfForm")[0].reset();
                          $('#sendpdfmsg').hide();
                        }, 2000);
            });
        return p;
    }
}

ConfirmationView.pageLoaded = () => {
    const urlPathname = window.location.pathname;
    if(urlPathname.search(/confirmation.?/) || urlPathname == '/hotels/order')
        $("#price-detail-itinerary").addClass('hide');

    methods.init();
    ConfirmationView.addToAnalytics();
}

ConfirmationView.addToAnalytics = () => {
    if(!rt.clientAnalyticsObj){
        setTimeout(ConfirmationView.addToAnalytics,1000);
        return;
    }
    rt.clientAnalyticsObj.increment('num_bookings');
    rt.clientAnalyticsObj.trackCharge(rt.totalAmount);
    rt.clientAnalyticsObj.setPeopleProperty(rt.lastPurchaseDate);
    rt.clientAnalyticsObj.union('visited_cities',rt.city);
    rt.clientAnalyticsObj.union('hotels_booked',rt.hotelId);
    rt.clientAnalyticsObj.increment('LTV',rt.totalAmount);
    rt.clientAnalyticsObj.alias(rt.userId);
    rt.clientAnalyticsObj.identifySession(rt.userId);
    ConfirmationView.setOrderIdCookie();
};

ConfirmationView.setOrderIdCookie = () => {
  cookies.set('order_id', rt.confirm_order_analytics.orderId, {expires: 365});
};

let analyticsConfigfunc = function(){
   return  {
       booking_code: rt.booking_code,
       hrental_id: rt.hotelId,
       hrental_pagetype: 'conversion',
       hrental_startdate: rt.checkInDate,
       hrental_enddate: rt.checkOutDate,
       hrental_totalvalue: rt.totalAmount,
       userId: rt.userId
   }
};

let confirmationConfig = { name:'Confirmation Page', widgets:[],analyticsConfigfunc};
let confirmationView = ConfirmationView.setup(confirmationConfig);
