import logging
from http import HTTPStatus
from django.conf import settings
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.pricing.exceptions import RequestError
from base.views import validators
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from apps.pricing.response_wrappers.hd_api_wrapper import HDApiWrapper
from web_sku.availability.services.availability_service import AvailabilityService
from web_sku.pricing.services.pricing_service import PricingService
from apps.payments.offers.services.payment_offer import PaymentOfferService
from web_sku.catalog.services.sku_service import SkuService
from web_sku.catalog.services.sku_request_service import SkuRequestService
from web_sku.catalog.services.cs_type_mappers import CsTypeMappers
from web_sku.common.dateutils import get_iso_date

logger = logging.getLogger(__name__)


class HotelRoomPricesSerializer(serializers.Serializer):
    checkin = serializers.DateField(validators = [
        validators.validate_date_of_request])
    checkout = serializers.DateField()
    roomconfig = serializers.CharField(
        max_length=100, validators=[
            validators.validate_roomconfig])
    apply_wallet = serializers.BooleanField(
        default=settings.APPLY_WALLET, required=False)
    utm_source = serializers.CharField(
        max_length=100,
        default=None,
        allow_null=True,
        allow_blank=True)
    channel = serializers.CharField(max_length=100, default=None, allow_null=True, allow_blank=True)
    couponcode = serializers.CharField(max_length=100, default=None, allow_null=True,
                                       allow_blank=True)

    def validate(self, data):
        if data['checkout'] <= data['checkin']:
            raise serializers.ValidationError("Checkout date must be after checkin date.")
        return data


class HotelRoomPrices(TreeboAPIView):
    validationSerializer = HotelRoomPricesSerializer
    sku_service = SkuService()
    sku_request_service = SkuRequestService()
    availability_service = AvailabilityService()
    pricing_service = PricingService()
    hotel_repository = RepositoriesFactory.get_hotel_repository()
    cs_type_mappers = CsTypeMappers()

    def get(self, request, *args, **kwargs):
        """
        API to fetch room prices for all rooms for the given hotel.
        checkin -- Checkin Date (Use format: yyyy-MM-dd)
        checkout -- Checkout Date (Use format: yyyy-MM-dd)
        roomconfig -- Room Configuration in format: 1-0,2-0,2-1 (For a-c, 'a' is adult count, and 'c' is children count)
        """
        try:
            if hasattr(
                    request,
                    'user') and request.user and request.user.is_authenticated():
                logger.info("request.user %s is logged in ", request.user)
                logged_in_user = True
            else:
                logged_in_user = False
            validated_data = self.serializerObject.validated_data
            apply_wallet = bool(validated_data['apply_wallet'])
            user = request.user
            price_info = self.process_validated_data(
                validated_data, kwargs, apply_wallet, user, logged_in_user=logged_in_user)
            return Response(price_info)
        except RequestError as e:
            logger.exception("RequestError received")
            return Response({"error": error_message(
                codes.INVALID_REQUEST)}, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.exception("Exception occurred while fetching price for request %s", str(request.GET))
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param=request.GET,
                                                        dev_msg="Hotel Pricing V7", message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            return Response({"error": error_message(codes.REQUEST_FAILED)}, status=HTTP_500_INTERNAL_SERVER_ERROR)

    def process_validated_data(
            self,
            validated_data,
            kwargs,
            apply_wallet=False,
            user=None,
            logged_in_user=False):

        try:
            hotel_ids = [kwargs["hotel_id"]]
            cs_web_id_map = {}
            for hotel in self.hotel_repository.get_hotels_for_id_list(hotel_ids):
                if hotel.cs_id is not None:
                    cs_web_id_map[hotel.cs_id.strip()] = hotel.id

            cs_hotel_ids = []
            if cs_web_id_map:
                cs_hotel_ids = list(cs_web_id_map.keys())

            if not len(cs_hotel_ids):
                raise Exception("No hotels Found")

        except Exception as e:
            # TODO: Exception chaining
            raise RequestError(
                status=HTTP_400_BAD_REQUEST,
                message=codes.INVALID_HOTEL_ID) from e

        request_skus = validated_data['skus'] if hasattr(validated_data, 'skus') else \
            self.sku_request_service.get_web_request_skus(validated_data['roomconfig'], cs_hotel_ids)
        availability_request_data = self.get_availability_request_data(validated_data, cs_hotel_ids,
                                                                       request_skus)
        sku_availability = self.availability_service.get_sku_availability(availability_request_data)
        room_wise_availability = self.availability_service. \
            get_room_wise_availability_from_sku_availability(availability_request_data, sku_availability)
        web_transformed_availability = self.availability_service.get_web_transformed_availability(
            cs_web_id_map, room_wise_availability)

        pricing_request_data = self.get_pricing_request_data(validated_data, request_skus)

        payment_offer = None

        if pricing_request_data['apply_coupon']:
            # check for auto apply coupon code only when any coupon code is not added already
            if not pricing_request_data['coupon_code']:
                payment_offer = PaymentOfferService().get_applicable_payment_offer(
                    pricing_request_data['utm']['source'], pricing_request_data.get('channel'))
            else:
                payment_offer = PaymentOfferService().get_payment_offer_for_coupon_code(
                    pricing_request_data['coupon_code'], pricing_request_data['utm']['source'],
                    pricing_request_data.get('channel'))

        if payment_offer:
            pricing_request_data['coupon_code'] = payment_offer.coupon_code

        room_wise_price = self.get_web_price(pricing_request_data, sku_availability, cs_web_id_map)

        search_api_wrapper = HDApiWrapper(hotel_ids[0], cs_hotel_ids[0], room_wise_price,
                                          web_transformed_availability,
                                          user, apply_wallet, validated_data['checkin'],
                                          validated_data['checkout'], request_skus,
                                          validated_data['channel'])
        pricing_response = search_api_wrapper.get_pricing_response()
        return pricing_response

    def get_pricing_request_data(self, page_data, pricing_request_skus):
        pricing_request_data = {
            'skus': pricing_request_skus,
            'from_date': get_iso_date(page_data['checkin']),
            'to_date': get_iso_date(page_data['checkout']),
            'mode': 'BROWSE',
            'policy': ['rp'],
            'apply_coupon': True,
            'coupon_code': page_data['couponcode'] if page_data.get('couponcode') else None,
            'channel': 'direct',
            'subchannel': self.cs_type_mappers.get_cs_sub_channel(page_data.get('channel'),
                                                                  page_data.get('utm_source')),
            'application': self.cs_type_mappers.get_cs_application(page_data.get('channel')),
            'utm': {'source': self.cs_type_mappers.get_cs_utm_source(page_data.get('utm_source'))},
            'customer_info': {},
            'pricing_type': 'all'
        }

        return pricing_request_data

    def get_availability_request_data(self, page_data, cs_hotel_ids, availability_request_skus):
        availability_request_data = {
            'skus': availability_request_skus,
            'checkin': page_data['checkin'],
            'checkout': page_data['checkout'],
            'hotels': cs_hotel_ids,
            'occupancy_config': page_data['roomconfig'],
            'channel': 'direct',
            'subchannel': self.cs_type_mappers.get_cs_sub_channel(page_data.get('channel'),
                                                                  page_data.get('utm_source')),
        }

        return availability_request_data

    def get_web_price(self, pricing_request_data, sku_availability, cs_web_id_map):

        room_config_wise_price = self.pricing_service.get_room_wise_price_from_availability(
            pricing_request_data, sku_availability)
        web_transformed_price = self.pricing_service.get_web_transformed_price(
            cs_web_id_map, room_config_wise_price)

        return web_transformed_price