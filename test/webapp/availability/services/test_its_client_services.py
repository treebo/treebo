import sys

from mock import Mock, mock

sys.modules['data_services.factory'] = Mock()
from webapp.availability.services.its_client import ITSSearchService
from webapp.availability.services.models import TDHotelAvailabilitySearchParams


def test_parse_availability_response_success():
    availability_response_with_one_hotel_two_room_types = {
        'data': [{
            'hotel_id': '12356',
            'availability': [
                {
                    'room_type': 'OAK',
                    'availability': 23
                },
                {
                    'room_type': 'MAPLE',
                    'availability': 16
                }
            ]}]
    }
    hotel_cs_ids_map = {'12356': 12}

    availability = ITSSearchService.parse_its_response_for_availability(
        availability_response_with_one_hotel_two_room_types, hotel_cs_ids_map)

    assert len(availability) == 1
    hotel = availability[0]
    assert hotel.hotel_id == 12
    assert hotel.hotel_cs_id == '12356'
    assert len(hotel.room_type_availability) == 2
    expected_room_type_availability = {'oak':23, 'maple':16}
    assert hotel.room_type_availability[0].rooms_available == expected_room_type_availability[hotel.room_type_availability[0].room_type]
    assert hotel.room_type_availability[1].rooms_available == expected_room_type_availability[hotel.room_type_availability[1].room_type]


def test_single_hotel_multiple_room_type_availability_across_config_parsing_success():
    with mock.patch.object(ITSSearchService, "make_its_call") as make_its_call_mocked:
        make_its_call_mocked.return_value = {
            "data": [
                {
                    "availability": [
                        {
                            "availability": 13,
                            "room_type": "MAPLE"
                        },
                        {
                            "availability": 7,
                            "room_type": "MAHOGANY"
                        }
                    ],
                    "hotel_id": "0241295"
                }
            ],
            "request_data": {
                "channel": "web",
                "checkin": "2019-01-06",
                "checkout": "2019-01-07",
                "hotels": "0241295",
                "roomconfig": "1-0",
                "subchannel": "direct"
            },
            "status": 200
        }
        search_param = TDHotelAvailabilitySearchParams(123, "2019-01-06", "2019-01-07", "1-0")
        search_param.hotel_cs_ids_map = {'0241295': 123}
        available_hotels = ITSSearchService.get_availability_across_config(search_param)
        assert len(available_hotels) == 1
        hotel = available_hotels[0]
        assert hotel.hotel_id == 123
        assert hotel.hotel_cs_id == '0241295'
        assert len(hotel.room_type_availability) == 2
        room_type_result_expected = {'maple': 13, 'mahogany': 7}
        assert all(room_type_result_expected[rt.room_type] == rt.rooms_available
                   for rt in hotel.room_type_availability)
        make_its_call_mocked.assert_called_once_with(
            ITSSearchService.URL_FOR_MINIMAL_ACROSS_ALL_CONFIG, search_param)


def test_date_wise_availability_of_multiple_hotels_with_multiple_room_type_with_one_hotel_unavailable_success():
    with mock.patch.object(ITSSearchService, "make_its_call") as make_its_call_mocked:
        make_its_call_mocked.return_value = {
            "data": {
                "0048880": {
                    "2018-05-26": {}
                },
                "0241295": {
                    "2018-05-26": {
                        "MAHOGANY": 2,
                        "MAPLE": 3
                    }
                }
            },
            "status": 200
        }
        search_param = TDHotelAvailabilitySearchParams('123,456', "2018-05-26", "2018-05-25", "1-0")
        search_param.hotel_cs_ids_map = {'0048880': 123, '0241295': 456}
        day_wise_availability = ITSSearchService.date_wise_room_type_ws(search_param)
        assert len(day_wise_availability[123]["2018-05-26"]) == 0
        assert len(day_wise_availability[456]["2018-05-26"]) == 2
        availability_on_26 = day_wise_availability[456]["2018-05-26"]
        room_type_result_expected = {'MAHOGANY': 2, 'MAPLE': 3}
        assert all(room_type_result_expected[rt] == available
                   for rt, available in availability_on_26.items())
        assert availability_on_26['MAHOGANY'] == 2
        make_its_call_mocked.assert_called_once_with(
            ITSSearchService.URL_FOR_DATEWISE_AVAILABILITY_ACROSS_ALL_CONFIG, search_param)


def verify_config_availability(config_availability):
    cases = {
        '2017-10-18': {
            '1-0': {
                'acacia': 1,
                'maple': 1
            },
            '2-0': {
                'oak': 2,
                'maple': 3
            },
        },
        '2017-10-19': {
            '1-0': {
                'oak': 2,
                'acacia': 4
            },
            '2-0': {
                'acacia': 2,
                'mahogany': 3
            }
        }
    }
    for room_config in config_availability.room_config_list:
        for room_type in room_config.room_types_available:
            assert cases[config_availability.date][room_config.room_config][
                       room_type.room_type] == room_type.rooms_available

def test_hotel_wise_day_wise_config_wise_roomtype_wise_availability_across_config_for_single_hotel():
    with mock.patch.object(ITSSearchService, "make_its_call") as make_its_call_mocked:
        make_its_call_mocked.return_value = {
            "status": 200,
            "data": [
                {
                    "hotel_id": '0048880',
                    "date_wise_availability": [
                        {
                            "date": "2017-10-18",
                            "room_config_wise_availability": [
                                {
                                    "room_config": "1-0",
                                    "room_type_availability": [
                                        {
                                            "room_type": "acacia",
                                            "availability": 1
                                        },
                                        {
                                            "room_type": "maple",
                                            "availability": 1
                                        }
                                    ]
                                },
                                {
                                    "room_config": "2-0",
                                    "room_type_availability": [
                                        {
                                            "room_type": "oak",
                                            "availability": 2
                                        },
                                        {
                                            "room_type": "maple",
                                            "availability": 3
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "date": "2017-10-19",
                            "room_config_wise_availability": [
                                {
                                    "room_config": "1-0",
                                    "room_type_availability": [
                                        {
                                            "room_type": "oak",
                                            "availability": 2
                                        },
                                        {
                                            "room_type": "acacia",
                                            "availability": 4
                                        }
                                    ]
                                },
                                {
                                    "room_config": "2-0",
                                    "room_type_availability": [
                                        {
                                            "room_type": "acacia",
                                            "availability": 2
                                        },
                                        {
                                            "room_type": "mahogany",
                                            "availability": 3
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
        search_param = TDHotelAvailabilitySearchParams('2', "2017-10-18", "2017-10-19", "1-0,2-0")
        search_param.hotel_cs_ids_map = {'0048880': 2}
        day_wise_availability = ITSSearchService.get_day_wise_availability_across_config(search_param)
        assert day_wise_availability is not None
        assert len(day_wise_availability.data) == 1
        hotel_data = day_wise_availability.data[0]
        assert hotel_data.hotel_id == 2 and hotel_data.hotel_cs_id == '0048880'
        assert len(hotel_data.config_wise_availability) == 2
        verify_config_availability(hotel_data.config_wise_availability[0])
        verify_config_availability(hotel_data.config_wise_availability[1])
        make_its_call_mocked.assert_called_once_with(
            ITSSearchService.URL_FOR_DATEWISE_AVAILABILITY_ACROSS_ALL_CONFIG, search_param)
