import treejax from '../treebolibs/treejax';
import cookies from 'js-cookie';

import $ from 'jquery';
import React from 'react';
import classNames from 'classnames';

class ReferralShare extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showEmail: false,
        };
    }

    componentDidMount() {
        const { form, email } = this.refs;
        $(form).parsley().on('form:submit', (e) => {
            e.submitEvent.preventDefault();
            const emails = this.state.emails;

            treejax({
                url: '/api/v1/referral/share/',
                type: 'POST',
                data: {
                    emails,
                    csrfmiddlewaretoken: cookies.get('csrftoken'),
                },
            }).then(() => {
                this.setState({ showEmail: false });
            });
            return false;
        });
    }

    handleFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    fbShare = () => {
        FB.ui({
            method: 'share',
            mobile_iframe: true,
            href: this.props.referralLink,
            quote: this.props.referralText,
        }, () => {});
    };

    render() {
        const modalClasses = classNames('modal email-invite flex-column', {
            hide: !this.state.showEmail,
        });
        return (
            <div>
                <div className="flex-row">
                    <button className="btn js-fbshare btn--facebook" onClick={this.fbShare}>
                        <div className="btn__flex-wrapper">
                            <i className="icon-fb"></i>
                            <span className="social-pipe">|</span><span>Share on Facebook</span>
                        </div>
                    </button>
                    <button className="btn js-email-share btn--google" onClick={() => this.setState({ showEmail: true })}>
                        <div className="btn__flex-wrapper">
                            <i className="icon-mail"></i>
                            <span className="social-pipe">|</span><span>Invite through Email</span>
                        </div>
                    </button>
                </div>
                <div id="shareEmail" className={modalClasses}>
                    <div className="modal__body pos-rel">
                        <div
                            className="icon icon-close pos-abs hand flex-row flex--align-center"
                            onClick={() => this.setState({ showEmail: false })}
                        >
                            <i className="icon-cross"></i>
                        </div>
                        <h2 className="email-invite__title">Invite through Email</h2>
                        <form id="emailsForm" ref="form">
                            <div className="email-invite__container">

                                <p className="email-invite__subtitle">Please enter the email addresses separated by commas</p>
                                <div className="float-labels">
                                    <input
                                        className="float-labels__input email-invite__email" name="emails" ref="email"
                                        type="text" value={this.state.emails} placeholder="ravi@gmail.com, gita@yahoo.com"
                                        pattern="^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},?\s*)+$"
                                        data-parsley-error-message="Please enter valid email address(es)"
                                        required="true" data-parsly-trigger="change" data-parsley-required="true"
                                        data-parsley-required-message="Please enter the list of emails here"
                                        onChange={this.handleFieldChange}
                                    />
                                    <label className="float-labels__label">Email List</label>
                                </div>
                            </div>
                            <button type="submit" className="btn btn--block btn--large">Send</button>
                        </form>

                    </div>
                </div>
            </div>
        );
    }
}

export { ReferralShare };
