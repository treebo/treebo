import logging

from django.conf import settings

from apps.bookings.models import PaymentOrder, Booking
from apps.bookings.services.booking import BookingService
from apps.checkout.service.booking_confirmation_service import \
    BookingConfirmationService
from services.restclient.treebo_notificationservice import TreeboNotificationService

logger = logging.getLogger(__name__)

PAYMENT_SUCCESS = 'PAYMENT_SUCCESS'
PAYMENT_DECLINED = 'PAYMENT_DECLINED'


class PhonePePaymentConfirmationService(object):
    booking_confirmation_service = BookingConfirmationService()
    treebo_notification_service = TreeboNotificationService()

    def phonepe_confirm_payment(self, pg_meta, ps_payment_id):
        payment_order = PaymentOrder.objects.filter(ps_payment_id=ps_payment_id).first()
        booking = Booking.objects.filter(order_id=payment_order.booking_order_id).prefetch_related('user_id').first()

        try:
            # for phonepe booking is already confirmed during initiate call ,this call is to update
            # relevant tables in web ,HX and  payment service after payment confirmation
            logger.info('Confirming phonepe payment for  order_id %s', payment_order.booking_order_id)
            self.confirm_payment_in_payment_service(pg_meta, payment_order, booking)
            self.update_payment_in_hx(payment_order)

        except Exception as e:
            # In case of payment failure confirm booking throws exception , after updating payment
            # service and web , here we need to capture it and send email to gdc and customer that
            # payment has failed
            logger.error('payment failed for order id %s', payment_order.booking_order_id)
            if pg_meta['code'] != PAYMENT_SUCCESS:
                self.handle_payment_failure(payment_order, booking)

            payment_order.status = PaymentOrder.FAILED
            payment_order.save()

    def update_payment_in_hx(self, payment_order):
        try:
            logger.info('Updating payment for order_id %s ,', payment_order.booking_order_id)
            booking = Booking.objects.get(order_id=payment_order.booking_order_id)
            BookingService.update_payment(booking.id)
        except Exception as e:
            logger.error('Error while updating payments to HX for order_id %s', payment_order.booking_order_id)
            self.handle_failure_in_payment_update_to_hx(payment_order)

    def handle_failure_in_payment_update_to_hx(self, payment_order):
        message='Payment Recieved by treebo but payment update to HX failed  for booking {}'.\
            format(payment_order.booking_order_id)
        self.treebo_notification_service.send_email(emails=settings.GDC_TEAM_EMAIL,content=message,
                                              subject=message)

    def confirm_payment_in_payment_service(self, pg_meta, payment_order, booking):


        user_auth_id = self.get_authenticated_user(booking)

        logger.info('call booking confirmation service for order_id %s , ps_order_id %s',
                    payment_order.booking_order_id, payment_order.ps_order_id)

        self.booking_confirmation_service.confirm_booking(
            None, pg_meta,
            payment_order.booking_order_id, \
            payment_order.ps_order_id, \
            settings.PHONEPE_GATEWAY, user_auth_id)

    def handle_payment_failure(self, payment_order, booking):
        self.send_payment_failure_notification_to_gdc(payment_order, booking)

    def send_payment_failure_notification_to_gdc(self, payment_order, booking):

        subject='Phonepe payment failure for confirmed booking - {}'.format(payment_order.booking_order_id)
        message='Please note, \n ' \
        'Booking Id - {} \n' \
        'Phonepe payment status - "Failed" ' \
        'Please call the guest on their phone number - {} and check the following\n' \
        '1. Did they make this booking by mistake? If they made it by mistake, please ask ' \
        'reservation team to cancel the booking immediately.\n \n' \
        '2. If they intend to keep the booking, we need to tell them that their payment failed on phonepe''s end but their booking is confirmed and immediately share the payment link with the traveler; however, if the guest insists to PAH, please don''t push for partial payments and let them know their booking is confirmed.\n'.format(
                 payment_order.booking_order_id,booking.guest_mobile)

        self.treebo_notification_service.send_email(emails=settings.GDC_TEAM_EMAIL, content=message, subject=subject)


    def get_authenticated_user(self, booking):
        return booking.user_id.auth_id

