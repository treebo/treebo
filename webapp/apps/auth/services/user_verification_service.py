import logging
import requests

from apps.referral import tasks
from dbcommon.models.profile import User, UserReferral
from services.common_services.otp_service import OtpService
from apps.auth.services.post_verification_hook import PostVerificationService
from django.core.urlresolvers import reverse
from base.middlewares.request import get_domain

logger = logging.getLogger(__name__)


class UserVerificationService():
    @staticmethod
    def verify_user(user_id, mobile, otp):
        is_verified = UserVerificationService.__verify_user_otp(
            user_id, mobile, otp)
        if is_verified:
            post_verification_data = requests.post(
                get_domain() +
                reverse("referral:signup"),
                data={
                    "user_id": user_id},
                allow_redirects=True)
        else:
            post_verification_data = False

        return is_verified, post_verification_data

    @staticmethod
    def __verify_user_otp(user_id, mobile, otp):
        user = User.objects.get(pk=user_id)
        if user.phone_number == mobile and user.is_otp_verified:
            return True
        else:
            is_verified = OtpService.veriy_otp(mobile, otp)
            if is_verified:
                user.is_otp_verified = is_verified
                user.save()
                return True
        return False
