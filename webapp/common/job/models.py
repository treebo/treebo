from datetime import datetime

from django.contrib.postgres.fields import JSONField
from django.db import models
from django_extensions.db.models import TimeStampedModel

from dbcommon.models.default_permissions import DefaultPermissions


class JobModel(TimeStampedModel, DefaultPermissions):
    job_id = models.CharField(max_length=50, db_index=True, unique=True)
    crs_booking_id = models.CharField(max_length=50, db_index=True, unique=True)
    serialized_job = JSONField(null=False)
    job_name = models.CharField(max_length=100, null=False)
    status = models.CharField(null=False, db_index=True, max_length=50)
    generated_at = models.DateTimeField(default=datetime.now, null=False, db_index=True)
    picked_at = models.DateTimeField(null=True)
    failure_message = models.CharField(max_length=100, null=True)
    total_tries = models.IntegerField(default=0, null=False)
    eta = models.DateTimeField(null=False, db_index=True)

    class Meta(DefaultPermissions.Meta):
        pass
