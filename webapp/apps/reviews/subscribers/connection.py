import redis

from django.conf import settings


class RedisCache(object):

    instance = None

    def __init__(self,):

        self.pool = redis.ConnectionPool(
            host=settings.REDIS_URL,
            port=settings.REDIS_PORT,
            db=settings.REDIS_DB)
        self.connection = redis.Redis(connection_pool=self.pool)
