import logging
from django.conf import settings
from apps.seo.components.base_component import BaseComponent
from apps.seo.services.common_services.sql_query_helper import SqlQueryHelper
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class HotelLocalityUtil(BaseComponent):
    @staticmethod
    def fetch_all_hotels_in_locality(locality):
        logger.info(
            "fetching deactivated hotels locality_name : %s , city_name: %s " % (locality.name, locality.city.name))
        if locality.distance_cap is None:
            locality.distance_cap = float(settings.DEFAULT_SEO_LANDMARK_RADIUS)

        distance_query_str = SqlQueryHelper.get_distance_query_for_param_str()
        sql_query = "select * from hotels_hotel where " + distance_query_str + " < %s and status = 1"

        all_hotels = Hotel.objects.raw(sql_query,
                                       [locality.latitude,
                                        locality.latitude,
                                        locality.longitude,
                                        locality.distance_cap])
        return all_hotels

    @staticmethod
    def fetch_hotel_localities(latitude, longitude):
        distance_query_str = SqlQueryHelper.get_distance_query_for_param_str()

        sql_query = "select * from hotels_locality where " + distance_query_str \
                    + " < case when distance_cap is null then " \
                    + str(settings.DEFAULT_SEO_LANDMARK_RADIUS) + " else distance_cap end "

        all_locality = Hotel.objects.raw(sql_query, [latitude, latitude, longitude])
        return all_locality

    @staticmethod
    def fetch_all_hotels_in_landmarks(landmark):
        logger.info(
            "fetching deactivated hotels landmark_name : %s , city_name: %s " % (landmark.seo_url_name, landmark.city.name))
        if landmark.distance_cap is None:
            landmark.distance_cap = float(settings.DEFAULT_SEO_LANDMARK_RADIUS)

        distance_query_str = SqlQueryHelper.get_distance_query_for_param_str()
        sql_query = "select * from hotels_hotel where " + distance_query_str + " < %s and status = 1"

        all_hotels = Hotel.objects.raw(sql_query, [landmark.latitude, landmark.latitude, landmark.longitude,
                                                   landmark.distance_cap])
        return all_hotels

    @staticmethod
    def fetch_hotel_landmarks(latitude, longitude):
        distance_query_str = SqlQueryHelper.get_distance_query_for_param_str()

        sql_query = "select * from search_landmark where " + distance_query_str \
                    + " < case when distance_cap is null then " \
                    + str(settings.DEFAULT_SEO_LANDMARK_RADIUS) + " else distance_cap end "

        all_landmarks = Hotel.objects.raw(sql_query, [latitude, latitude, longitude])
        return all_landmarks
