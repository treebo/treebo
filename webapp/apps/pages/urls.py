

from django.conf.urls import url

from apps.pages.views import print_pdf
app_name = 'pages'
urlpatterns = [
    # Below view is already added as /booking/ URL in pages/urls_2.py
    # url(r"^confirmation/", confirmation.Confirmation.as_view(), name='confirmation'),
    # url(r"^order$", Order.as_view(), name='order'), moved to main urls.py
    #url(r"^pdfdata/?$", print_pdf.PdfData.as_view(), name='pdf-data'),
  ]
