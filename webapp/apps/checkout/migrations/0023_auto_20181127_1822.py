# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-11-27 18:22
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0022_auto_20181127_1820'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bookingrequest',
            old_name='booking_sub_channel',
            new_name='booking_subchannel',
        ),
    ]
