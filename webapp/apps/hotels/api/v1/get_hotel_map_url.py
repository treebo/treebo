import logging

from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import status

from base.views.template import TreeboTemplateView
from dbcommon.models.hotel import Hotel
logger = logging.getLogger(__name__)


class HotelMapLocationUrl(TreeboTemplateView):

    def get(self, request, *args, **kwargs):
        """
        Hotel Map Url API, to fetch location url for Hotel.
        """
        google_map_url = 'https://maps.google.com'
        hotel_id = kwargs.get('hotel_id', '')
        if hotel_id:
            hotel = get_object_or_404(Hotel, id=int(hotel_id))
            map_url = hotel.get_location_url
            return HttpResponseRedirect(map_url)
        else:
            return HttpResponseRedirect(google_map_url)
