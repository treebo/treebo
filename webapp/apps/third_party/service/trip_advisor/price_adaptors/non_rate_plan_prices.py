from decimal import Decimal

from apps.bookingstash.service.availability_service import get_availability_service
from apps.pricing import dateutils
from apps.pricing.services.pricing_service import PricingService
from apps.third_party.service.trip_advisor import constants
from apps.third_party.service.trip_advisor.price_adaptors import TripAdvisorPriceAdaptor


class NonRatePlanPriceAdaptor(TripAdvisorPriceAdaptor):

    def get_price_and_availability(
            self,
            hotel_ids_list,
            checkin_date,
            checkout_date,
            room_config):
        pricing_response = PricingService.get_price_from_soa(
            checkin=checkin_date,
            checkout=checkout_date,
            hotel_ids=hotel_ids_list,
            room_config=room_config,
            include_price_breakup=False,
            get_from_cache=True)
        hotel_prices = {hotel["hotel_id"]: hotel["rooms"]
                        for hotel in pricing_response["data"]["hotels"]}

        room_availability_per_hotel = get_availability_service().get_available_rooms_for_hotels(
            hotel_ids_list,
            dateutils.ymd_str_to_date(checkin_date),
            dateutils.ymd_str_to_date(checkout_date),
            room_config)

        return hotel_prices, room_availability_per_hotel

    def create_room_types_data(
            self,
            room_types,
            hotel,
            hotel_price,
            checkin_date,
            checkout_date,
            room_availability_per_hotel,
            num_room_requested,
            ta_url_room_config):
        for room in hotel.rooms.all():
            room_type_code = room.room_type_code.lower()
            price_group_by_room = {
                price['room_type']: price for price in hotel_price}

            if room_type_code in price_group_by_room and room_availability_per_hotel.get(
                    (int(hotel.id), room_type_code), 0) > 0:
                room_data = dict()
                room_type = room_type_code.title()
                room_types[room_type] = room_data
                room_data["url"] = constants.TA_HOTEL_URL % (
                    constants.BASE_URL, hotel.get_sitemap_url(), checkin_date, checkout_date, ta_url_room_config,
                    room_type, constants.TA_UTM_SOURCE,
                    constants.TA_UTM_MEDIUM,
                    constants.TA_UTM_CAMPAIGN)
                price = price_group_by_room[room_type_code]['price']
                room_data["price"] = round(float(price["pretax_price"]), 2)
                room_data["taxes"] = round(float(price["tax"]), 2)
                room_data["fees"] = 0
                room_data["fees_at_checkout"] = 0
                room_data["taxes_at_checkout"] = 0
                final_price = Decimal(room_data["price"] + room_data["taxes"])
                room_data["final_price"] = round(final_price, 2)
                if price["promo_discount"] > 0:
                    discounts = self.generate_discounts(price)
                    if len(discounts) > 0:
                        room_data["discounts"] = discounts
                room_data["currency"] = constants.CURRENCY
                room_data["num_rooms"] = num_room_requested
                room_data["room_amenities"] = self.generate_rooms_amenities(
                    room.id)
        return room_types
