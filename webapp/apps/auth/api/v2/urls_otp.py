from django.conf.urls import patterns, url
from apps.auth.api.v2.otp_isverify import OTPIsVerifyApi
from apps.auth.api.v2.otp_login import OTPLoginApi
from apps.auth.api.v2.otp_resend_duration import OTPResendDurationAPI
from apps.auth.api.v2.otp_verify import OTPVerifyApi

urlpatterns = [
    url(r"^/verify/$", OTPVerifyApi.as_view(), name='otp-verify-v2'),
    url(r"^/is-verified/$", OTPIsVerifyApi.as_view(), name='otp-is-verified-v2'),
    url(r"^/$", OTPLoginApi.as_view(), name='otp-send-v2'),
    url(r"^/resend-duration/$", OTPResendDurationAPI.as_view(), name='otp-resend-duration-v2'),

]
