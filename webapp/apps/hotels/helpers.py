from django.core.cache import cache
from django.core.urlresolvers import reverse
from common.constants import common_constants
from data_services.respositories_factory import RepositoriesFactory


class CityHotelHelper():
    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'cs_apicache_city_helper'

    @staticmethod
    def invalidate_cache():
        cache.delete(CityHotelHelper.CACHE_KEY)

    @staticmethod
    def getCityHotels():
        try:
            cache_value = cache.get(CityHotelHelper.CACHE_KEY)
        except Exception as e:
            cache_value = None
        if cache_value:
            return cache_value
        cityDetails = []

        city_data_service = RepositoriesFactory.get_city_repository()
        cities = city_data_service.filter_cities(
            status=common_constants.ENABLED)
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotels = hotel_repository.get_active_hotel_list()
        city_ids = [hotel.city.id for hotel in hotels]
        from collections import Counter, OrderedDict
        import operator
        hotels_count_for_city = Counter(city_ids)
        sorted_hotels_count_for_city = sorted(
            list(hotels_count_for_city.items()), key=operator.itemgetter(1))
        hotels = [{'city_id': key, 'total': value}
                  for key, value in sorted_hotels_count_for_city]
        # hotels = Hotel.objects.filter(status=common_constants.ENABLED).values('city_id').annotate(
        #     total=Count('city_id')).order_by('total')
        countDict = {}
        for singleHotel in hotels:
            countDict[singleHotel['city_id']] = singleHotel['total']

        for city in cities:
            # hotels = Hotel.objects.filter(city=city).count()
            tempCityName = city.slug
            if countDict.get(city.id, 0) > 0:
                try:
                    cityDetails.append({
                        'hotelCount': countDict.get(city.id, 0),
                        'name': city.name,
                        'slug': city.slug.lower() if city.slug else city.name.lower(),
                        'landscapeImage': city.landscape_image.url if city.landscape_image else "",
                        'latitude': city.city_latitude,
                        'longitude': city.city_longitude,
                        'url': reverse('pages:hotels_in_city', kwargs={'cityString': tempCityName.lower()})
                    })
                except Exception as e:
                    print(e)
        sortedDetails = sorted(
            cityDetails,
            key=operator.itemgetter('hotelCount'),
            reverse=True)
        cache.set(
            CityHotelHelper.CACHE_KEY,
            sortedDetails,
            CityHotelHelper.CACHE_TIMEOUT)
        return sortedDetails

    @staticmethod
    def get_hotels_with_url_prefix(url_prefix):
        try:
            cache_key = CityHotelHelper.CACHE_KEY + url_prefix
            cache_value = cache.get(cache_key)
        except Exception as e:
            cache_value = None
        if cache_value:
            return cache_value
        cityDetails = []

        city_data_service = RepositoriesFactory.get_city_repository()
        cities = city_data_service.filter_cities(
            status=common_constants.ENABLED)
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotels = hotel_repository.get_active_hotel_list()
        city_ids = [hotel.city.id for hotel in hotels]
        from collections import Counter, OrderedDict
        import operator
        hotels_count_for_city = Counter(city_ids)
        sorted_hotels_count_for_city = sorted(
            list(hotels_count_for_city.items()), key=operator.itemgetter(1))
        hotels = [{'city_id': key, 'total': value}
                  for key, value in sorted_hotels_count_for_city]
        # hotels = Hotel.objects.filter(status=common_constants.ENABLED).values('city_id').annotate(
        #     total=Count('city_id')).order_by('total')
        countDict = {}
        for singleHotel in hotels:
            countDict[singleHotel['city_id']] = singleHotel['total']

        for city in cities:
            # hotels = Hotel.objects.filter(city=city).count()
            tempCityName = city.slug
            if countDict.get(city.id, 0) > 0:
                try:
                    cityDetails.append({
                        'name': city.name,
                        'slug': city.slug.lower() if city.slug else city.name.lower(),
                        'landscapeImage': city.landscape_image.url if city.landscape_image else "",
                        'latitude': city.city_latitude,
                        'longitude': city.city_longitude,
                        'url': reverse('pages:{}'.format(url_prefix),
                                       kwargs={'cityString': tempCityName.lower()})
                    })
                except Exception as e:
                    print(e)
        sortedDetails = sorted(
            cityDetails,
            key=operator.itemgetter('name'),
            reverse=True)
        cache.set(
            cache_key,
            sortedDetails,
            CityHotelHelper.CACHE_TIMEOUT)
        return sortedDetails

