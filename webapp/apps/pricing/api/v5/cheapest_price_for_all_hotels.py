import logging
from django.core.validators import RegexValidator
from django.http import JsonResponse
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.pricing.exceptions import SoaError, CheckinDateError, RequestError
from apps.pricing.transformers.search_api_transformer import SearchAPITransformer
from base.views import validators
from base.views.api import TreeboAPIView
from apps.pricing.api.v7.cheapest_price_for_all_hotels import CheapestPriceAPIView as CheapestPriceAPIViewV7
from common.services.feature_toggle_api import FeatureToggleAPI
from django.conf import settings

logger = logging.getLogger(__name__)


def validate_checkin_checkout(checkin, checkout):
    if checkin >= checkout:
         raise CheckinDateError("Checkin Date is Greater than or qual to Checkout date", HTTP_400_BAD_REQUEST)


class PricingSerializer(serializers.Serializer):
    hotel_id = serializers.CharField(
        max_length=1000,
        validators=[
            RegexValidator(
                regex='^(?:\d+,?)+$',
                message='Invalid value for hotel_id parameter. '
                'Please provide a comma-separated list of hotel_ids',
                code='invalid_hotel_ids')])
    checkin = serializers.DateField(validators = [
        validators.validate_date_of_request])
    checkout = serializers.DateField()
    roomconfig = serializers.CharField(max_length=100, validators=[validators.validate_roomconfig])
    apply_wallet = serializers.BooleanField(default=settings.APPLY_WALLET, required=False)
    utm_source = serializers.CharField(max_length=100, default=None, allow_null=True, allow_blank=True)
    channel = serializers.CharField(max_length=100, default=None, allow_null=True, allow_blank=True)

    def validate(self, data):
        if data['checkout'] <= data['checkin']:
            raise serializers.ValidationError("Checkout date must be after checkin date.")
        return data


class CheapestPriceAPIView(TreeboAPIView):
    srp_pricing_service = SearchAPITransformer()
    validationSerializer = PricingSerializer

    def get(self, request, *args, **kwargs):

        try:
            if FeatureToggleAPI.is_enabled(settings.DOMAIN, "pricing_revamp", False):
                return CheapestPriceAPIViewV7.as_view()(request, *args, **kwargs)

            validated_data = self.serializerObject.validated_data
            validate_checkin_checkout(validated_data['checkin'], validated_data['checkout'])
            user = request.user
            hotel_ids = validated_data['hotel_id'].split(",")
            apply_wallet = bool(validated_data['apply_wallet'])
            sort_criterion = request.GET.get('sort')
        except Exception as e:
            return JsonResponse(
                dict(
                    error=dict(
                        code=HTTP_400_BAD_REQUEST,
                        msg=e.message)),
                status=HTTP_400_BAD_REQUEST)
        if not len(hotel_ids):
            return JsonResponse(
                dict(
                    error=dict(
                        code=HTTP_400_BAD_REQUEST,
                        msg="hotels should not none")),
                status=HTTP_400_BAD_REQUEST)
        try:
            hotel_prices = self.srp_pricing_service.fetch_cheapest_room_prices_available(validated_data, hotel_ids, sort_criterion=sort_criterion, user=user, apply_wallet=apply_wallet)
            return Response(hotel_prices)
        except SoaError as e:
            logger.exception("Error fetching price for search page")
            return JsonResponse(
                {"error": {"code": e.status, "msg": e.msg}}, status=HTTP_400_BAD_REQUEST)
        except RequestError as e:
            logger.exception("Error fetching price for search page")
            return JsonResponse(
                {"error": {"code": e.status, "msg": str(e)}}, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.exception("Error fetching price for search page")
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        dev_msg="WEB Pricing V5", message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            return JsonResponse(
                {
                    "error": {
                        "code": HTTP_500_INTERNAL_SERVER_ERROR,
                        "msg": str(e)}},
                status=HTTP_500_INTERNAL_SERVER_ERROR)
