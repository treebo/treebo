import logging

from django.core.exceptions import ObjectDoesNotExist
from psycopg2 import InterfaceError, DatabaseError

from base.db_decorator import db_retry_decorator
from dbcommon.models.room import Room
from apps.common.slack_alert import SlackAlertService


logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)
# console_handler = logging.StreamHandler()
# console_handler.setLevel(logging.DEBUG)
# formatter = logging.Formatter(
#     '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# console_handler.setFormatter(formatter)
# logger.addHandler(console_handler)


class RoomService:

    def __init__(self, room_dto):
        self.room_dto = room_dto

    @db_retry_decorator()
    def create_or_update_room(self):
        acceptable_keys_list = [
            field.name for field in Room._meta.get_fields()]

        logger.info("[create_or_update_room] Init for hotel {} with details {}".format(str(self.room_dto.cs_id),
                                                                                       str(self.room_dto.__dict__)))

        try:
            room_obj = Room.objects.get(
                hotel_id=self.room_dto.hotel.id,
                room_type_code=self.room_dto.room_type_code)
            logger.info("[create_or_update_room] Room object GET for hotel {}: {}".format(str(self.room_dto.cs_id),
                                                                                          str(room_obj.__dict__)))
            for attribute, value in list(vars(self.room_dto).items()):
                if attribute in acceptable_keys_list:
                    setattr(room_obj, attribute, value)

            logger.info("[create_or_update_room] Room object UPDATED for hotel {}: {}".format(str(self.room_dto.cs_id),
                                                                                              str(room_obj.__dict__)))
        except ObjectDoesNotExist:
            logger.info("Creating a room object")
            try:
                room_obj = Room.objects.create(
                    hotel=self.room_dto.hotel,
                    room_type=self.room_dto.room_type,
                    room_type_code=self.room_dto.room_type_code,
                    room_area=self.room_dto.room_area,
                    cs_id=self.room_dto.cs_id,
                    price=self.room_dto.price,
                    quantity=self.room_dto.quantity,
                    available=self.room_dto.available,
                    max_guest_allowed=self.room_dto.max_guest_allowed,
                    base_rate=self.room_dto.base_rate,
                    max_adult_with_children=self.room_dto.max_adult_with_children,
                    max_children=self.room_dto.max_children,
                    external_room_details=self.room_dto.external_room_details,
                    description=self.room_dto.description)
                logger.info("[create_or_update_room] Room object CREATED for hotel {}: {}".format(str(self.room_dto.cs_id),
                                                                                                  str(room_obj.__dict__)))
            except Exception as exception:
                logger.exception("[create_or_update_room] Error creating Room object for hotel: {}".format(str(self.room_dto.cs_id)))
                raise exception

        except (InterfaceError, DatabaseError) as e:
            logger.error("Database or Interface error occured %s for %s", str(e), self.room_dto.hotel.id)
            SlackAlertService.send_cs_error_msg_to_slack(error_msg=e.__str__(),
                                                         class_name=self.__class__.__name__,
                                                         cs_id=self.room_dto.hotel.cs_id)
            raise e
        except Exception as e:
            logger.error("Error occurred in create_or_update_room due to %s for %s", str(e), self.room_dto.hotel.id)
            SlackAlertService.send_cs_error_msg_to_slack(error_msg=e.__str__(),
                                                         class_name=self.__class__.__name__,
                                                         cs_id=self.room_dto.hotel.cs_id)
            return False, str(e)

        try:
            room_obj.save()
        except Exception as exception:
            logger.exception(
                "[create_or_update_room] Error saving Room object for hotel: {}".format(str(self.room_dto.cs_id)))
            SlackAlertService.send_cs_error_msg_to_slack(error_msg=exception.__str__(),
                                                         class_name=self.__class__.__name__,
                                                         cs_id=self.room_dto.hotel.cs_id)
            raise exception
        return True, 'Message processed'

