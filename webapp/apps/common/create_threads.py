from threading import Thread
from functools import wraps


def create_thread(func):

    @wraps(func)
    def thread_func(*args, **kwargs):

        make_thread = Thread(target=func, args=args, kwargs=kwargs)
        make_thread.start()

        return make_thread

    return thread_func
