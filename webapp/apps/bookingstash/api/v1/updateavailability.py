from apps.common.slack_alert import SlackAlertService

__author__ = 'Rajdeep'
import datetime
import logging

from django.http import HttpResponse
from pyquery import PyQuery as pQ

from apps.bookingstash.models import Availability
from apps.bookingstash.models import HotelRoomMapper
from base import log_args
from base.parsers.xmlparser import XMLParser
from base.renderers import XMLRenderer
from base.views.api import APIView
from base.message_queues import web_publishers

logger = logging.getLogger(__name__)


class UpdateAvailability(APIView):
    authentication_classes = ()
    permission_classes = ()
    parser_classes = (XMLParser,)
    renderer_classes = (XMLRenderer,)

    def get_success_xml(self, echo_token):
        return """
        <OTA_HotelInvCountNotifRS xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" Version="1.0" TimeStamp="%(timestamp)s" EchoToken=%(echotoken)s xmlns="http://www.opentravel.org/OTA/2003/05">
            <Success/>
        </OTA_HotelInvCountNotifRS>
        """ % {
            'echotoken': echo_token,
            'timestamp': datetime.datetime.now().isoformat('T')
        }

    def get_failure_xml(self, echo_token):
        return """
        <OTA_HotelInvCountNotifRS xmlns:xsi="http://www.w3.org/2001/XMLSchema- instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" Version="1.0" TimeStamp="%(timestamp)s" EchoToken=%(echotoken)s xmlns="http://www.opentravel.org/OTA/2003/05" >
            <Errors xmlns="">
                <Error Type="103" Code="103">InvalidInvType</Error>
            </Errors>
        </OTA_HotelInvCountNotifRS>
        """ % {
            'echotoken': echo_token,
            'timestamp': datetime.datetime.now().isoformat('T')
        }

    @log_args(logger)
    def post(self, request):

        hx_inventory_xml = request.data
        #logger.info("recieved inventory sync input: " + hx_inventory_xml)

        reformed_xml = ""
        try:
            hx_inventory_xml_modified = request.data.decode('utf-8')
            hx_inventory_xml_modified = hx_inventory_xml_modified.replace(
                'xmlns=', 'xmlnamespace=')

            logger.info("treated inventory sync input: " + hx_inventory_xml_modified)
            reformed_xml = pQ(hx_inventory_xml_modified, parser='xml')
        except Exception as e:
            logger.exception("error parsing inventry input")
            logger.exception(e)

        count_of_inventory = len(reformed_xml('Inventory'))
        echo_token = reformed_xml('OTA_HotelInvCountNotifRQ').attr('EchoToken')
        mm_hotel_code, mm_room_code = None, None
        try:
            mm_hotel_code = reformed_xml('Inventories').attr('HotelCode')
            for i in range(0, count_of_inventory):
                start_date = reformed_xml(
                    'StatusApplicationControl').eq(i).attr('Start')
                end_date = reformed_xml(
                    'StatusApplicationControl').eq(i).attr('End')
                mm_room_code = reformed_xml(
                    'StatusApplicationControl').eq(i).attr('InvTypeCode')
                rooms_available = reformed_xml('InvCount').eq(i).attr('Count')
                logger.debug(
                    'Updating availability for [mm_hotel_code: %s, mm_room_code: %s] to: %s',
                    mm_hotel_code,
                    mm_room_code,
                    rooms_available)
                hotel_mapper_object = HotelRoomMapper.objects.get(
                    mm_hotel_code=mm_hotel_code, mm_room_code=mm_room_code)
                room_id = hotel_mapper_object.room_id
                reformed_start_date = datetime.datetime.strptime(
                    start_date, "%Y-%m-%d").date()
                reformed_end_date = datetime.datetime.strptime(
                    end_date, "%Y-%m-%d").date()
                availabilities = Availability.objects.filter(
                    room_id=room_id, date__gte=reformed_start_date, date__lte=reformed_end_date)
                logger.debug(
                    "Total Availabilities for existing room_id:%s = %s",
                    room_id,
                    availabilities.count())
                availability_by_date = {
                    availability.date: availability for availability in availabilities}

                while reformed_start_date <= reformed_end_date:
                    availability = availability_by_date.get(
                        reformed_start_date, None)
                    if not availability:
                        availability = Availability.objects.create(
                            room_id=room_id, date=reformed_start_date, availablerooms=rooms_available)
                        logger.info(
                            "Created Availability for room_id: %s, with id: %s, with value: %s",
                            room_id,
                            availability.id,
                            rooms_available)
                    else:
                        availability.availablerooms = rooms_available
                        availability.save()
                    reformed_start_date = reformed_start_date + \
                        datetime.timedelta(days=1)
        except HotelRoomMapper.DoesNotExist:
            SlackAlertService.publish_alert(
                ("HotelRoomMapper not found for mm_hotel_code: %s, and mm_room_code: %s" %
                 (mm_hotel_code, mm_room_code)), SlackAlertService.INVENTORY_CHANNEL)
            return HttpResponse(
                self.get_failure_xml(echo_token),
                content_type='text/xml')
        except Exception as ex:
            logger.exception(ex)
            SlackAlertService.publish_alert(
                ("HotelRoomMapper not found for mm_hotel_code: %s, and mm_room_code: %s" %
                 (mm_hotel_code, mm_room_code)), SlackAlertService.INVENTORY_CHANNEL)
            return HttpResponse(
                self.get_failure_xml(echo_token),
                content_type='text/xml')

        try:
            web_publishers.publish('hx_availability_push', hx_inventory_xml)
        except Exception:
            logger.exception("Unable to publish inventory to ITS")

        return HttpResponse(
            self.get_success_xml(echo_token),
            content_type='text/xml')
