import logging

from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from django.views.generic.base import RedirectView

from apps.hotels import utils as hotel_utils
from base import log_args
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class HotelDetailRedirectView(RedirectView):
    permanent = True
    query_string = True

    @log_args(logger)
    def get_redirect_url(self, *args, **kwargs):

        hashedHotelId = kwargs.get('hotel_id', None)
        hotelString = kwargs.get('hotelString', None)
        hotelId = hotel_utils.decode_hash(hashedHotelId)
        try:
            hotel = Hotel.all_hotel_objects.get(id=hotelId)
        except Hotel.DoesNotExist as e:
            landing_page_url = reverse('pages:index')
            return landing_page_url
        hotel_slug = slugify(hotel.name)
        locality_slug = slugify(hotel.locality.name)
        city_slug = slugify(hotel.city.name)
        kwargs = {
            'city_slug': city_slug,
            'hotel_slug': hotel_slug,
            'locality_slug': locality_slug,
            'hotel_id': hotelId}
        query_params = self.request.GET.copy()
        if hotel.status == Hotel.ENABLED:
            new_redirect_url = reverse(
                'pages:hotel-details-new',
                kwargs=kwargs) + '?' + query_params.urlencode()
            logger.info(" new_redirect_hd_url %s ", new_redirect_url)
        else:
            kwargs = {'cityString': city_slug}
            new_redirect_url = reverse(
                'pages:hotels_in_city',
                kwargs=kwargs) + '?' + query_params.urlencode()
            logger.info(" new_redirect_city_url %s ", new_redirect_url)
        return new_redirect_url
