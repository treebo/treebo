import logging

from celery import shared_task

from apps.seo.services.common_services.hotel_locality_util import HotelLocalityUtil
from dbcommon.models.hotel import Locality, Hotel, EnableCategory
from dbcommon.models.landmark import SearchLandmark
from apps.hotels.service.hotel_search_service import HotelSearchService

logger = logging.getLogger(__name__)


@shared_task(name='update_localities_landmark_seo')
def check_and_update_hotel_localities_landmark_seo(latitude, longitude):
    try:
        latitude = float(latitude)
        longitude = float(longitude)
        all_localities = HotelLocalityUtil.fetch_hotel_localities(latitude=latitude, longitude=longitude)
        all_landmarks = HotelLocalityUtil.fetch_hotel_landmarks(latitude=latitude, longitude=longitude)

        for locality in all_localities:
            hotels = HotelLocalityUtil.fetch_all_hotels_in_locality(locality)
            hotels_count = len(list(hotels))

            logger.info("Updating locality is_hotel_present for : %s", locality.name)
            # to get the db instance of current locality
            locality_db_model = Locality.objects.filter(id=locality.id).first()
            if hotels_count == 0:
                if locality_db_model.is_hotel_present:
                    locality_db_model.is_hotel_present = False
                    locality_db_model.save()
            elif not locality_db_model.is_hotel_present:
                locality_db_model.is_hotel_present = True
                locality_db_model.save()
            logger.info("Update completed for locality is_hotel_present : %s", locality.name)

        for landmark in all_landmarks:
            hotels = HotelLocalityUtil.fetch_all_hotels_in_landmarks(landmark)
            hotels_count = len(list(hotels))
            logger.info("Updating landmark is_hotel_present for : %s", landmark.seo_url_name)
            # to get the db instance of current landmark
            landmark_db_model = SearchLandmark.objects.filter(id=landmark.id).first()
            if hotels_count == 0:
                if landmark_db_model.is_hotel_present:
                    landmark_db_model.is_hotel_present = False
                    landmark_db_model.save()
            elif not landmark_db_model.is_hotel_present:
                landmark_db_model.is_hotel_present = True
                landmark_db_model.save()
            logger.info("Update completed for landmark is_hotel_present : %s", landmark.seo_url_name)
    except Exception as e:
        logger.exception('exception occured check_and_update_hotel_localities_landmark_seo :' + str(e))


@shared_task(name='update_localities_seo_if_hotel_unavailable')
def check_and_update_locality_seo_if_hotel_unavailable(locality_id):
    try:
        locality = Locality.objects.filter(id=int(locality_id)).first()
        hotel_search_service = HotelSearchService(city='',
                                                  latitude=locality.latitude,
                                                  longitude=locality.longitude,
                                                  distance_cap=locality.distance_cap)
        hotel_result = hotel_search_service.search()
        hotels_count = len(hotel_result)
        # to get the db instance of current locality
        locality_db_model = Locality.objects.filter(id=locality.id).first()
        if hotels_count == 0:
            if locality_db_model.is_hotel_present:
                locality_db_model.is_hotel_present = False
                locality_db_model.save()
        elif not locality_db_model.is_hotel_present:
            locality_db_model.is_hotel_present = True
            locality_db_model.save()
    except Exception as e:
        logger.exception('exception occured check_and_update_locality_seo_if_hotel_unavailable :' + str(e))


@shared_task(name='update_landmark_seo_if_hotel_unavailable')
def check_and_update_landmark_seo_if_hotel_unavailable(landmark_id):
    try:
        landmark = SearchLandmark.objects.filter(id=int(landmark_id)).first()
        hotel_search_service = HotelSearchService(city='',
                                                  latitude=landmark.latitude,
                                                  longitude=landmark.longitude,
                                                  distance_cap=landmark.distance_cap)
        hotel_result = hotel_search_service.search()
        hotels_count = len(hotel_result)
        # to get the db instance of current landmark
        landmark_db_model = SearchLandmark.objects.filter(id=landmark.id).first()
        if hotels_count == 0:
            if landmark_db_model.is_hotel_present:
                landmark_db_model.is_hotel_present = False
                landmark_db_model.save()
        elif not landmark_db_model.is_hotel_present:
            landmark_db_model.is_hotel_present = True
            landmark_db_model.save()
    except Exception as e:
        logger.exception('exception occured check_and_update_landmark_seo_if_hotel_unavailable :' + str(e))


@shared_task(name='update_city_category_seo_if_hotel_unavailable')
def check_and_update_city_category_seo_if_hotel_unavailable(city_id):
    try:
        city_id = int(city_id)
        enabled_list = [item['hotel_type__id'] for item in EnableCategory.objects.filter(city__id=city_id)
            .values('hotel_type__id')]

        hotels = Hotel.objects.filter(city__id=city_id, status=1)
        city_category_list = []
        for hotel in hotels:
            city_category_list.extend(hotel.category.all())

        city_category_list = set(city_category_list)
        city_category_list = [item.id for item in city_category_list]

        # remove None in list if present
        enabled_list = [e for e in enabled_list if e != None]

        # check if any category without any hotel is present
        unwanted_category = (list(set(enabled_list) - set(city_category_list)))
        # check if any category is missing in EnabledCategory
        missing_category = (list(set(city_category_list) - set(enabled_list)))

        if unwanted_category or missing_category:
            enabled_category_instance = EnableCategory.objects.filter(city__id=city_id).first()
            enabled_category_instance.hotel_type = city_category_list
            enabled_category_instance.save()
            logger.info("updated check_and_update_city_category_seo_if_hotel_unavailable for "
                        "city_id : %s, category : %s", city_id, city_category_list)

        logger.info("check_and_update_city_category_seo_if_hotel_unavailable called for city_id : %s", city_id)

    except Exception as e:
        logger.exception('exception occured check_and_update_city_category_seo_if_hotel_unavailable :' + str(e))


@shared_task(name='update_categories_seo_from_category_id')
def check_and_update_categories_seo_from_category_id(category_id):
    try:
        category_id = int(category_id)
        current_category_mapping_cities = EnableCategory.objects.filter(hotel_type__id=category_id).prefetch_related(
            'hotel_type')
        for current_category_mapping_city in current_category_mapping_cities:
            logger.info("check_and_update_categories_seo_from_category_id is_enable for city : %s",
                        current_category_mapping_city.city.name)
            check_and_update_city_category_seo_if_hotel_unavailable(current_category_mapping_city.city.id)

    except Exception as e:
        logger.exception('exception occured check_and_update_categories_seo_from_category_id :' + str(e))
