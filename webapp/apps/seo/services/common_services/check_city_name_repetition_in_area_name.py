from dbcommon.models.location import CityAlias


class CheckCityNameRepetitionInAreaName:

    def __init__(self, area_name, city_name):
        self.area_name = area_name
        self.city_name = city_name

    def check_substring(self):
        city_alias_objects = CityAlias.objects.filter(city__iexact=self.city_name).all()
        city_aliases = [city_alias.alias for city_alias in city_alias_objects]
        city_alias_present_in_area_name = False
        for alias in city_aliases:
            if alias.lower() in self.area_name.lower():
                city_alias_present_in_area_name = True
        if self.city_name.lower() in self.area_name.lower() or city_alias_present_in_area_name:
            return True
        return False

