from django.conf import settings
from kombu import Connection, Exchange
from kombu.pools import producers

from common.utilities.queue_utils import get_rabbit_mq_heartbeat_interval, get_rabbit_mq_retry_policy, \
    get_rabbit_mq_transport_options

connection = Connection(settings.PRICING_BROKER_URL,
                        heartbeat=get_rabbit_mq_heartbeat_interval(),
                        transport_options=get_rabbit_mq_transport_options())
model_exchange = Exchange("web_sync_exchange", type="direct")

routing_keys = {
    'hx_availability_push': 'hx_availability_push'
}


def publish(message_type, payload):
    """
    Publishes payload to web's broker
    :param message_type:
    :param payload:
    """
    routing_key = routing_keys.get(message_type)
    with producers[connection].acquire(block=True) as producer:
        producer.publish(payload,
                         exchange=model_exchange,
                         routing_key=routing_key,
                         declare=[model_exchange],
                         compression='zlib',
                         retry=True,
                         retry_policy=get_rabbit_mq_retry_policy())
