from django.conf.urls import url

from apps.hotels.api.v5.hotel_detail import HotelDetail

app_name = 'hotels_v5'

urlpatterns = [
    url(r"^(?P<hotel_id>[0-9]+)/details/$", HotelDetail.as_view(), name='hotel-detail-v5'),

]
