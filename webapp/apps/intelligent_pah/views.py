from rest_framework.views import APIView
from django.http import HttpResponse
from apps.intelligent_pah.services.pah_services import PAHEnable


class Test(APIView):

    def get(self, response):
        reference = PAHEnable()
        sys = reference.pay_at_hotel_disable_logic()
        print(sys)
        return HttpResponse(sys)
