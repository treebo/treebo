# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('discounts', '0005_auto_20160518_1305'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='constraintexpression',
            options={
                'verbose_name': 'Constraint Expression',
                'verbose_name_plural': 'Constraint Expressions'},
        ),
        migrations.AlterModelOptions(
            name='constraintkeywords',
            options={
                'verbose_name': 'Constraint Keyword',
                'verbose_name_plural': 'Constraint Keywords'},
        ),
        migrations.AlterModelOptions(
            name='constraintoperators',
            options={
                'verbose_name': 'Constraint Operator',
                'verbose_name_plural': 'Constraint Operators'},
        ),
        migrations.AlterModelOptions(
            name='discountcoupon',
            options={
                'verbose_name': 'Discount Coupon',
                'verbose_name_plural': 'Discount Coupons'},
        ),
        migrations.AlterModelOptions(
            name='discountcouponconstraints',
            options={
                'verbose_name': 'Discount Coupon Constraint',
                'verbose_name_plural': 'Discount Coupon Constraints'},
        ),
        migrations.AlterField(
            model_name='discountcouponconstraints',
            name='coupon',
            field=models.ForeignKey(
                to='discounts.DiscountCoupon',
                null=True,
                on_delete=models.DO_NOTHING),
        ),
        migrations.AlterField(
            model_name='discountcouponconstraints',
            name='keyword',
            field=models.ForeignKey(
                to='discounts.ConstraintKeywords',
                null=True,
                on_delete=models.DO_NOTHING),
        ),
        migrations.AlterField(
            model_name='discountcouponconstraints',
            name='operation',
            field=models.ForeignKey(
                to='discounts.ConstraintOperators',
                null=True,
                on_delete=models.DO_NOTHING),
        ),
    ]
