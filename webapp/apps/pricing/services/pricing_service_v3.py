import datetime
import logging
from collections import defaultdict, Counter
from concurrent.futures import ThreadPoolExecutor, as_completed
from datetime import datetime
from decimal import Decimal

import requests
from django.conf import settings
import json
from django.core.exceptions import ObjectDoesNotExist

from apps.pricing.pricing_utils import PricingUtils
from apps.pricing.api.v4 import RatePlanPrice
from apps.pricing.dateutils import date_to_ymd_str, date_range
from apps.pricing.exceptions import SoaError
from base import log_args
from apps.common.slack_alert import SlackAlertService as slack_alert
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class PricingServiceV3(object):
    @log_args(logger)
    def get_search_page_price(self, pdto):
        supported_room_prices = self.get_price(pdto)
        sale_details = PricingUtils.get_sale_details(pdto)
        hotel_wise_cumulative_prices = self._get_cumulative_prices(
            pdto, supported_room_prices, sale_details=sale_details)
        return hotel_wise_cumulative_prices

    @log_args(logger)
    def get_hd_page_price(self, pdto):
        supported_room_prices = self.get_price(pdto)
        sale_details = PricingUtils.get_sale_details(pdto)
        hotel_wise_cumulative_prices = self._get_cumulative_prices(
            pdto, supported_room_prices, sale_details=sale_details)
        return hotel_wise_cumulative_prices

    @log_args(logger)
    def get_itinerary_page_price(self, pdto):
        supported_room_prices = self.get_price(pdto)
        sale_details = PricingUtils.get_sale_details(pdto)
        hotel_wise_cumulative_prices = self._get_cumulative_prices_for_room_and_rateplan(
            pdto, supported_room_prices, sale_details=sale_details)
        return hotel_wise_cumulative_prices.get(pdto.hotel_ids[0])

    def _get_cumulative_prices_for_room_and_rateplan(
            self,
            pdto,
            supported_room_prices,
            sale_details=None
            ):
        """
        supported_room_prices[hotel_id][room_type][rate_plan][(date, room_config)] = rp
        """

        required_room_type = pdto.room_type
        required_rate_plan = pdto.rate_plan
        room_configs = pdto.room_configs
        room_config_counter = Counter(room_configs)
        cumulative_prices = dict()
        for hotel_id, room_wise_prices in list(supported_room_prices.items()):
            room_prices = room_wise_prices.get(required_room_type.lower())
            if not room_prices:
                cumulative_prices[hotel_id] = dict(
                    cumulative_price=None, date_wise_breakup={})
                continue

            cumulative_price = dict()
            date_wise_breakup = dict()
            for rate_plan, date_and_rc_wise_price in list(room_prices.items()):
                cumulative_price[rate_plan] = self._sum_rateplan_wise_prices(
                    pdto, date_and_rc_wise_price, room_config_counter, sale_details=sale_details, hotel_id=hotel_id)
                if rate_plan == required_rate_plan:
                    date_wise_breakup = self._get_date_wise_breakup(
                        pdto, date_and_rc_wise_price, room_config_counter, sale_details=sale_details, hotel_id=hotel_id)
            cumulative_prices[hotel_id] = dict(
                cumulative_price=cumulative_price,
                date_wise_breakup=date_wise_breakup)
        return cumulative_prices

    def _get_cumulative_prices(self, pdto, supported_room_prices, sale_details=None):
        """
        supported_room_prices[hotel_id][room_type][rate_plan][(date, room_config)] = rp
        """
        room_configs = pdto.room_configs
        room_config_counter = Counter(room_configs)
        cumulative_prices = dict()
        for hotel_id, room_wise_prices in list(supported_room_prices.items()):
            room_wise_cumulative_price = dict()
            for room_type, rate_plan_wise_price in list(
                    room_wise_prices.items()):
                rate_plan_cumulative_price = dict()
                for rate_plan, date_and_rc_wise_price in list(
                        rate_plan_wise_price.items()):
                    rate_plan_cumulative_price[rate_plan] = self._sum_rateplan_wise_prices(
                        pdto, date_and_rc_wise_price, room_config_counter, sale_details=sale_details, hotel_id=hotel_id)
                room_wise_cumulative_price[room_type] = rate_plan_cumulative_price
            cumulative_prices[hotel_id] = room_wise_cumulative_price
        return cumulative_prices

    def _sum_rateplan_wise_prices(
            self,
            pdto,
            date_and_rc_wise_price,
            room_config_counter,
            sale_details=None,
            hotel_id=None):
        rate_plan_cumulative_price = None
        rate_plan_meta = None
        for date_config, rate_plan_price in list(
                date_and_rc_wise_price.items()):
            room_count = room_config_counter.get(date_config[1])
            rate_plan_meta = rate_plan_price['meta']
            rate_plan_price = rate_plan_price['prices']
            rate_plan_price = PricingUtils.get_sale_rate_plan(pdto, hotel_id, rate_plan_price, sale_details)
            if rate_plan_price.sell_price <= 0:
                rate_plan_cumulative_price = rate_plan_price
                break
            if room_count > 1:
                rate_plan_price = rate_plan_price.multiply(room_count)
            if rate_plan_cumulative_price:
                rate_plan_cumulative_price = rate_plan_cumulative_price.add(
                    rate_plan_price)
            else:
                rate_plan_cumulative_price = rate_plan_price

        rate_plan_cumulative_price.do_round_off()
        return dict(price=rate_plan_cumulative_price, meta=rate_plan_meta)

    def _get_date_wise_breakup(
            self,
            pdto,
            date_and_rc_wise_price,
            room_config_counter,
            sale_details=None,
            hotel_id=None):
        date_wise_cumulative_price = defaultdict(RatePlanPrice)
        for date_config, rate_plan_price in list(
                date_and_rc_wise_price.items()):
            room_count = room_config_counter.get(date_config[1])
            date = date_config[0]
            rate_plan_price = rate_plan_price['prices']
            rate_plan_price = PricingUtils.get_sale_rate_plan(pdto, hotel_id, rate_plan_price, sale_details)
            if room_count > 1:
                rate_plan_price = rate_plan_price.multiply(room_count)
            price = date_wise_cumulative_price.get(date)
            if price:
                price = price.add(rate_plan_price)
            else:
                price = rate_plan_price
            date_wise_cumulative_price[date] = price

        for date_, price in list(date_wise_cumulative_price.items()):
            price.do_round_off()

        return date_wise_cumulative_price

    def get_price(self, pdto):
        room_config_str = ''
        for config in pdto.room_configs:
            room_config_str += '&room_config=' + config

        hotel_coupon_list = ''
        hotel_discount_map = pdto.hotel_discount_map
        for key, discount_percentage in list(hotel_discount_map.items()):
            hotel_id, room_type, rate_plan = key
            if room_type and str(room_type).lower() != 'none':
                hotel_coupon_list += "%s-%s-%s-%s," % (
                    hotel_id, discount_percentage, str(room_type), str(rate_plan))

        hotel_coupon_list = hotel_coupon_list[0:-1]
        split_range = 3500
        try:
            if len(hotel_coupon_list) > split_range:
                hotel_coupon_lists = PricingServiceV3.split_hotel_coupon_list(hotel_coupon_list, split_range)

                with ThreadPoolExecutor(max_workers=5) as executor:
                    futures = []
                    for i in range(len(hotel_coupon_lists)):
                        i_hotel_coupon_list = hotel_coupon_lists[i]
                        future = executor.submit(self.make_pricing_call, pdto, room_config_str, i_hotel_coupon_list)
                        futures.append(future)

                    response_data = None
                    for future in as_completed(futures):
                        response = future.result()

                        if response_data:
                            response_data.get('data').extend(response.get('data'))
                        else:
                            response_data = response
            else:
                response_data = self.make_pricing_call(pdto, room_config_str, hotel_coupon_list)

            transformed_response = self._transform_pricing_response(
                response_data, pdto.checkin_date, pdto.checkout_date, pdto.room_configs)
            return transformed_response
        except SoaError as e:
            logger.exception("SoaError in method get_price() for pdto: {}".format(pdto.__dict__))
            raise e

    @staticmethod
    def split_hotel_coupon_list(hotel_coupon_list, split_range):
        """
        Split hotel_coupon_list
        :param hotel_coupon_list:
        :param split_range:
        :return: Lists of hotel_coupon_list
        """
        hotel_coupon_lists = []

        while len(hotel_coupon_list) > split_range:
            start_index = 0
            end_index = split_range

            curr_char = hotel_coupon_list[end_index]

            if curr_char != ',':
                end_index -= 1
                curr_char = hotel_coupon_list[end_index]
                while curr_char != ',':
                    end_index -= 1
                    curr_char = hotel_coupon_list[end_index]

            i_hotel_coupon_list = hotel_coupon_list[start_index:end_index]
            hotel_coupon_lists.append(i_hotel_coupon_list)
            end_index += 1
            hotel_coupon_list = hotel_coupon_list[end_index: len(hotel_coupon_list)]

        hotel_coupon_lists.append(hotel_coupon_list)

        return hotel_coupon_lists

    def make_pricing_call(self, pdto, room_configs, hotel_coupons):
        url = settings.BASE_PRICING_URL_V3 + "stay_start={0}&stay_end={1}".format(
            date_to_ymd_str(pdto.checkin_date), date_to_ymd_str(pdto.checkout_date))
        url += room_configs
        url += "&booking_unit_coupon_pairs=" + hotel_coupons

        if pdto.channel:
            url += "&channel={0}".format(pdto.channel)

        if pdto.logged_in_user:
            url += "&user_logged_in=1"

        if pdto.utm_source:
            url += "&utm_source={0}".format(pdto.utm_source)

        response = requests.get(url)
        logger.info(
            "Pricing API V3 called got status %s  %s",
            response.status_code,
            url)
        if response.status_code != requests.codes.ok:
            logger.error(
                "Pricing API V3 Error response:%s %s",
                response.status_code,
                response.content)

            if response.status_code != 400:
                slack_alert.send_slack_alert_for_third_party(status=response.status_code, url=url,
                                                             class_name="PricingServiceV3")

            raise SoaError(
                response.status_code,
                'Unable to fetch prices for the hotel.',
                response.status_code)

        data = response.json()
        if not data.get('data'):
            logger.error(" prices v3 are not available %s ", url)
            raise SoaError(
                response.status_code,
                'Prices are not available for this hotel',
                response.status_code)

        return data

    def _transform_pricing_response(
            self,
            response,
            checkin_date,
            checkout_date,
            room_configs):
        all_booking_units = self._get_all_unique_date_and_room_config_tuple_set_requested(
            checkin_date, checkout_date, room_configs)
        hotel_wise_prices = self._build_hotel_wise_room_wise_prices_from_response(
            response)
        supported_room_prices = self._filter_room_type_supporting_all_room_config(
            all_booking_units, hotel_wise_prices)
        return supported_room_prices

    @staticmethod
    def _get_all_unique_date_and_room_config_tuple_set_requested(
            checkin_date, checkout_date, room_configs):
        all_booking_units = []
        for d in date_range(checkin_date, checkout_date):
            dstr = datetime.strftime(d, '%Y-%m-%d')
            for rc in room_configs:
                all_booking_units.append((dstr, rc))
        all_booking_units = set(all_booking_units)
        return all_booking_units

    @staticmethod
    def _build_hotel_wise_room_wise_prices_from_response(price_response):
        def tree(): return defaultdict(tree)
        prices = price_response.get('data', [])
        hotel_wise_prices = tree()
        for price in prices:
            bu = price['booking_unit']
            hotel_id, room_type, date, room_config = bu['hotel_id'], bu[
                'room_type_code'], bu['date'], bu['room_config']
            rate_plans = price['rate_plans']
            for rp in rate_plans:
                rp['prices'] = RatePlanPrice.build(rp['prices'])
                rp['meta'] = rp['meta']
                hotel_wise_prices[hotel_id][room_type][rp['name']][(
                    date, room_config)] = rp
        return hotel_wise_prices

    @staticmethod
    def _filter_room_type_supporting_all_room_config(
            all_booking_units, hotel_wise_prices):
        def tree(): return defaultdict(tree)
        supported_room_prices = tree()
        for hotel_id, room_wise_prices in list(hotel_wise_prices.items()):
            for room_type, rate_plan_wise_prices in list(
                    room_wise_prices.items()):
                for rate_plan, date_and_rc_wise_prices in list(
                        rate_plan_wise_prices.items()):
                    if not (all_booking_units -
                            set(date_and_rc_wise_prices.keys())):
                        supported_room_prices[hotel_id][room_type][rate_plan] = date_and_rc_wise_prices
        return supported_room_prices

    @staticmethod
    def calculate_total_amount(
            pretax,
            member_discount,
            coupon_discount,
            voucher,
            tax_amount):
        """
        Calculates the total amount using all the components of the price. This is done because of inconsistencies
        in python's floating point calculation.
        :param pretax:
        :param coupon_discount:
        :param member_discount:
        :param voucher:
        :param tax_amount:
        :return: (Decimal) total amount
        """
        return Decimal(pretax) - Decimal(member_discount) - \
            Decimal(coupon_discount) - Decimal(voucher) + Decimal(tax_amount)

    @staticmethod
    def get_adult_occupancy(room_configs):
        try:
            room_config=room_configs[0]
            return int(room_config[0])
        except Exception as e:
            raise ('unable to fetch adult occupancy. Error - %s' % str(e))

    @staticmethod
    def get_hotel_cs_id(hotel_id):
        try:
            hotel = Hotel.all_hotel_objects.get(id=hotel_id)
            return hotel.cs_id
        except ObjectDoesNotExist:
            return None

    @staticmethod
    def get_cs_room_type_mapping(room_type):
        try:
            return settings.CS_ROOM_TYPE_MAPPING[room_type]
        except KeyError:
            return None

    def get_posttax_price(self, room_config, hotel_id, room_type, pre_tax_value, date_value):
        try:
            url = settings.BASE_TAX_URL_V3
            adult_occupancy = self.get_adult_occupancy(room_config)
            hotel_cs_id = self.get_hotel_cs_id(hotel_id)
            cs_room_type = self.get_cs_room_type_mapping(room_type)
            if adult_occupancy and hotel_cs_id and cs_room_type:
                data = {
                    'skus': [
                        {
                            'index': '1',
                            'is_sez': False,
                            'category_id': 'roomnight',
                            'attributes': [
                                {
                                    'key': 'hotel_id',
                                    'value': str(hotel_cs_id)
                                },
                                {
                                    'key': 'occupancy',
                                    'value': str(adult_occupancy)
                                },
                                {
                                    'key': 'room_type_code',
                                    'value': str(cs_room_type)
                                }
                            ],
                            'prices': [
                                {
                                    'index': '1',
                                    'date': str(date_value),
                                    'price': int(pre_tax_value)
                                }
                            ]
                        }
                    ]
                }

                headers = {'Content-type': 'application/json'}
                response = requests.post(url, data=json.dumps(data), headers=headers)
                if response.status_code == 200:
                    try:
                        parsed_response = json.loads(response.content.decode('latin1'))
                        post_tax_price = Decimal(parsed_response['skus'][0]['prices'][0]['post_tax_price'])
                        tax = Decimal(parsed_response['skus'][0]['prices'][0]['tax']['value'])
                        return post_tax_price, tax
                    except KeyError as e:
                        raise ("post tax price not available. Error - %s" % e)
            raise Exception('Post tax amount not available')
        except Exception as e:
            raise Exception('Post tax amount not available. Error - %s' % str(e))
