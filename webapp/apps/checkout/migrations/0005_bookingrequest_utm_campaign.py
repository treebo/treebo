# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0004_bookingrequest_host_ip_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='utm_campaign',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
