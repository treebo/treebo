from apps.hotels.service.hotel_service import HotelService
from apps.seo.models.reasons_to_pick import ReasonsToPick

__author__ = 'ansilkareem'


class AboutHotel():
    def __init__(self, hotel):
        self.hotel = hotel

    def build(self):

        reasons = ReasonsToPick.objects.filter(hotel=self.hotel.id)
        items = []
        if reasons:
            if len(reasons) > 3:
                reasons = reasons[:3]
            items = [reason.reason for reason in reasons]
        address = self.hotel.street + ', ' + \
            self.hotel.city.slug.title() if self.hotel.city.slug else self.hotel.city.name.title()
        address = address + ", " + str(self.hotel.locality.pincode)
        return {
            "about_hotel": {
                "title": "About " + str(HotelService().get_display_name_from_hotel_name(
                    hotel_name=self.hotel.name).title()),

                "content": {
                    "reasons": {
                        "title": "3 reasons to pick this Treebo",
                        "items": items,
                    } if items else {},

                    "coordinates": {
                        "lat": self.hotel.latitude,
                        "lng": self.hotel.longitude,
                    },

                    "description": {
                        "address": address,
                        "about": self.hotel.description,
                    },
                },
            }
        }
