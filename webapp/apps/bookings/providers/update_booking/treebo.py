# -*- coding: utf-8 -*-
import logging

from ths_common.constants.booking_constants import ProfileTypes
from ths_common.value_objects import Address as THSCAddress
from ths_common.value_objects import GSTDetails
from thsc.crs.entities.billing import Bill as THSCBill
from thsc.crs.entities.booking import Booking as THSCBooking
from thsc.crs.entities.customer import Customer

from apps.auth.services.user_profile_service import UserProfileService
from apps.bookings.providers.get_booking.treebo import TreeboCRSGetBooking
from apps.bookings.providers.interfaces.update_booking import UpdateBookingInterface
from apps.bookings.providers.thsc_utils import ThscUtils
from apps.reward.service.loyalty_service import LoyaltyService

logger = logging.getLogger(__name__)


class TreeboCRSUpdateBooking(UpdateBookingInterface):

    def __init__(self, old_booking, new_booking, **kwargs):
        self.__thsc_bill = None
        self.__thsc_booking = None
        ThscUtils.set_thsc_context(new_booking.source.application)
        super(TreeboCRSUpdateBooking, self).__init__(old_booking, new_booking, **kwargs)

    def _update(self):
        """
        Keeping the update light weight as direct has just the use cases of payment and
        soft block update
        """
        logger.info("Inside update_booking's update method for booking {booking_id}".format(
            booking_id=self.new_booking.crs_reference_id))
        if self.new_booking.payments - self.old_booking.payments:
            logger.info("Updating payments for booking_id {crs_reference_id}".format(
                crs_reference_id=self.new_booking.crs_reference_id))
            self.update_payments()
        if self.new_booking.gst_details != self.old_booking.gst_details:
            logger.info("Updating gst for booking_id {crs_reference_id}".format(
                crs_reference_id=self.new_booking.crs_reference_id))
            self.update_gst_details()
        if self.new_booking.expiry_time_stamp != self.old_booking.expiry_time_stamp:
            logger.info("Updating hold_till for booking_id {crs_reference_id}".format(
                crs_reference_id=self.new_booking.crs_reference_id))
            self.update_hold_till()

        return TreeboCRSGetBooking(booking_id=self.old_booking.booking_id).get()

    def update_gst_details(self):
        new_gst_details = self.new_booking.gst_details
        new_address = new_gst_details.address
        loyalty_type = None
        thsc_address = THSCAddress(
            field_1=new_address.line1,
            field_2=new_address.line2,
            city=new_address.city,
            state=new_address.state,
            pincode=new_address.pincode,
            country=new_address.country,
        )

        thsc_gst = GSTDetails(
            legal_name=new_gst_details.legal_name,
            address=thsc_address,
        )

        if new_gst_details.gstin_number:
            thsc_gst.gstin_num = new_gst_details.gstin_number

        customer = Customer.create_instance_for_update(
            customer_id=self.old_booking.gst_details.crs_id)
        customer.gst_details = thsc_gst
        first_name, last_name = self.new_booking.booking_owner.get_name_breakup(
            self.new_booking.booking_owner.name)
        customer.first_name = first_name
        customer.email = self.new_booking.booking_owner.email
        customer.country_code = self.new_booking.booking_owner.country_code
        customer.phone_number = self.new_booking.booking_owner.phone_number

        ups_user_id = UserProfileService().get_user_id(phone_number=customer.phone_number,
                                                       email_id=customer.email,
                                                       name=customer.name)

        ups_user_profile_id = UserProfileService().get_user_profile_id(ups_user_id)
        customer.reference_id = ups_user_id
        customer.user_profile_id = ups_user_profile_id

        if new_gst_details.gstin_number:
            loyaltyservice = LoyaltyService()
            loyalty_type = loyaltyservice.get_loyalty_type(customer.phone_number)

        profile_type = self._get_profile_type(loyalty_type)
        customer.profile_type = profile_type

        thsc_booking = self._thsc_booking_for_update()
        thsc_booking.update_customer(customer=customer)

        return thsc_booking

    def _get_profile_type(self, loyalty_type):
        if not loyalty_type:
            return ProfileTypes.INDIVIDUAL
        profile_dict = {
            "restricted_loyalty": ProfileTypes.INDIVIDUAL,
            "paid_sme": ProfileTypes.SME,
            "corporate": ProfileTypes.CORPORATE
        }
        return profile_dict.get(loyalty_type, ProfileTypes.INDIVIDUAL)

    def update_payments(self):
        thsc_payments = ThscUtils.get_thsc_payments_from_payments(self.new_booking.payments)
        thsc_bill = self._thsc_bill_for_update()
        logger.info("total number of payments are {pay} for booking_id {crs_reference_id}".format(
            pay=len(thsc_payments), crs_reference_id=self.new_booking.crs_reference_id))
        for thsc_payment in thsc_payments:
            thsc_bill.add_payment(thsc_payment)

        return self.__thsc_bill

    def update_hold_till(self):
        thsc_booking = self._thsc_booking_for_update()

        if self.new_booking.expiry_time_stamp is None:
            logger.info("calling thsc confirm booking for booking_id {crs_reference_id}".format(
                crs_reference_id=self.new_booking.crs_reference_id))
            thsc_booking.confirm()
        else:
            thsc_booking.hold_till = self.new_booking.booking_expiry_time
            thsc_booking.update()

    def _thsc_bill_for_update(self):
        thsc_bill = THSCBill.get(self.new_booking.crs_bill_id)
        bill_id, version = thsc_bill.bill_id, thsc_bill.version
        logger.info(
            "bill version inside thsc_bill_for_update {ver} for booking_id {crs_reference_id}".format(
                ver=version, crs_reference_id=self.new_booking.crs_reference_id))
        self.__thsc_bill = THSCBill.create_for_update(bill_id=bill_id, version=version)
        return self.__thsc_bill

    def _thsc_booking_for_update(self):
        if self.__thsc_booking:
            booking_id, version = self.__thsc_booking.booking_id, self.__thsc_booking.version
        else:
            booking_id, version = self.old_booking.crs_reference_id, self.old_booking.crs_version

        self.__thsc_booking = THSCBooking.create_instance_for_update(booking_id, version)
        logger.info(
            "thsc_version inside thsc_booking_for_update method is {ver} for booking_id {crs_reference}".format(
                ver=version, crs_reference=booking_id))
        return self.__thsc_booking
