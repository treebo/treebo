import logging

logger = logging.getLogger(__name__)


class CookieService():
    AUTHENTICATED_USER = 'authenticated_user'

    def update_cookie(self, response, key, data):
        import urllib.request
        import urllib.parse
        import urllib.error
        import json
        logger.info("setting cookie %s %s ", key, data)
        response.set_cookie(key, urllib.parse.quote(json.dumps(data)))
        return response

    def remove_cookie(self, response, key):
        response.set_cookie(key, None)
        return response

    def set_user_info_in_cookie(self, response, user):
        user_data = {
            "userId": user.id,
            "email": user.email,
            "phone": user.phone_number,
            "name": user.first_name,
        }
        self.update_cookie(response, 'authenticated_user', user_data)
