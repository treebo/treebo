import logging
from decimal import Decimal

import requests
from django.conf import settings
from django.db.models import Q
from django.utils.encoding import smart_str

from apps.reviews import constants
from apps.reviews.models import TAHotelIDMapping, HotelTAOverallRatings

logger = logging.getLogger(__name__)


class HotelRatingService(object):
    def ta_rating_ranking(self, location_serializer, ta_loc_id):
        try:
            ta_hotel_map = TAHotelIDMapping.objects.filter(
                ta_location_id=ta_loc_id).first()
            if ta_hotel_map:
                logger.info(
                    "Mapping between hotel %s and ta %s exists in HotelRatingService",
                    ta_hotel_map.hotel.id,
                    ta_hotel_map.ta_location_id)
                location = location_serializer.data
                hotel_overall_rating = HotelTAOverallRatings.objects.filter(
                    hotel_ta_mapping=ta_hotel_map).first()
                if hotel_overall_rating:
                    logger.info(
                        " hotel_overall_rating data Entry exist .. update for hotel %s and ta_loc_id %s",
                        ta_hotel_map.hotel.id,
                        ta_hotel_map.ta_location_id)
                else:
                    logger.info(
                        " hotel_overall_rating data Entry does not exist .. create for hotel %s and ta_loc_id %s",
                        ta_hotel_map.hotel.id,
                        ta_hotel_map.ta_location_id)
                    hotel_overall_rating = HotelTAOverallRatings()
                    hotel_overall_rating.hotel_ta_mapping = ta_hotel_map
                self.create_or_update_ratings(
                    hotel_overall_rating, location, ta_hotel_map)
            else:
                logger.info(
                    "No mapping for the given tid exists %s",
                    ta_loc_id)

        except Exception:
            logger.exception(
                "Exception in creating new entry in overall rating table for %s",
                ta_loc_id)

    def create_or_update_ratings(
            self,
            hotel_overall_rating,
            location,
            ta_hotel_map):
        write_review_url = None
        try:
            if ta_hotel_map.enable_iframe_widget:
                logger.info(
                    "enable_iframe_widget True for hotel %s %s",
                    ta_hotel_map.hotel.id,
                    ta_hotel_map.ta_location_id)
                write_review_url = self.generate_iframe_url(
                    ta_hotel_map.hotel.id)

            if not write_review_url:
                write_review_url = smart_str(location.get('write_review'))

            if not location['rating']:
                if write_review_url:
                    ta_hotel_map.write_review_url = write_review_url
                    ta_hotel_map.save()
            else:
                hotel_overall_rating.overall_rating = Decimal(
                    location['rating'])
                hotel_overall_rating.url_overall_rating_image = smart_str(
                    location['rating_image_url'])
                hotel_overall_rating.write_review_url = write_review_url
                hotel_overall_rating.read_review_url = smart_str(
                    location['web_url']) + constants.READ_REVIEW_URL
                hotel_overall_rating.geo_location_name = smart_str(
                    location['ranking_data']['geo_location_name'])
                if smart_str(location['ranking_data']['ranking']):
                    hotel_overall_rating.overall_ranking = int(
                        location['ranking_data']['ranking'])
                if smart_str(location['ranking_data']['ranking_out_of']):
                    hotel_overall_rating.overall_ranking_out_of = int(
                        location['ranking_data']['ranking_out_of'])
                if smart_str(location['num_reviews']):
                    hotel_overall_rating.num_reviews_count = int(
                        location['num_reviews'])
                hotel_overall_rating.overall_ranking_string = constants.RANKING_STRING.format(
                    smart_str(
                        location['ranking_data']['ranking']), smart_str(
                        location['ranking_data']['ranking_out_of']), smart_str(
                        location['ranking_data']['geo_location_name']))
                hotel_overall_rating.save()
        except Exception:
            logger.exception(
                "Exception in saving overall ratings object for %s",
                smart_str(
                    location['location_id']))

    def fetch_latest_overall_ratings(self, ta_loc_id=None, hotel_id=None):
        try:
            if ta_loc_id or hotel_id:
                latest_ratings = HotelTAOverallRatings.objects.filter(
                    Q(hotel_ta_mapping__ta_location_id=ta_loc_id) | Q(hotel_ta_mapping__hotel__id=hotel_id)).first()
                if latest_ratings:
                    return latest_ratings
                else:
                    logger.error(
                        "No ratings exist for ta_loc_id %s and hotel_id %s",
                        ta_loc_id,
                        hotel_id)
                    return None
            else:
                logger.error("both ta_loc_id and hotel_id are None")
                return None
        except Exception:
            logger.exception(
                "Exception in fetching the ratings  for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)
            return None

    def delete_ratings(self, ta_loc_id=None, hotel_id=None):
        try:
            latest_rating = self.fetch_latest_overall_ratings(
                ta_loc_id=ta_loc_id, hotel_id=hotel_id)
            for rating in latest_rating:
                rating.delete()
                logger.info(
                    "Deleted successfully for ta_loc_id %s and hotel-id %s",
                    ta_loc_id,
                    hotel_id)
            else:
                logger.error(
                    "No ratings exist for ta_loc_id %s and hotel_id %s",
                    ta_loc_id,
                    hotel_id)
        except Exception:
            logger.exception(
                "Exception in deleting ratings for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)

    def generate_iframe_url(self, hotel_id, email=None, name=None):
        review_reminder_url = None
        try:
            iframe_api_url = settings.TREEBO_WEB_URL + \
                settings.REVIEW_REMINDER_IFRAME_API_URL.format(hotel_id, email, name)
            response = requests.get(iframe_api_url)
            response_json = response.json()
            logger.info(
                "Generate Review-Reminder-URL %s API Response %s status code %s",
                iframe_api_url,
                response_json,
                response.status_code)

            if response_json.get('status').lower(
            ) == 'success' and response.status_code == 200:
                iframe_url = response_json.get('url')
                # # # TODO: complete redirect url
                # # redirect_url = settings.REDIRECT_URL.format(settings.TREEBO_WEB_URL)
                review_reminder_url = iframe_url  # + redirect_url
                logger.info(
                    "iframe_url %s  review_reminder_url %s",
                    iframe_url,
                    review_reminder_url)
        except Exception:
            logger.exception(
                "Exception in generating the write review url for %s",
                hotel_id)
        return review_reminder_url
