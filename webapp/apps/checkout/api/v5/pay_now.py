# pylint: disable-msg=no-member

import json
import logging

from django.conf import settings
from ipware.ip import get_ip
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.bookingstash.service.availability_service import get_availability_service
from apps.bookingstash.utils import StashUtils
from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.constants import PREPAID_BOOKING_MODE, PAY_AT_HOTEL_BOOKING_MODE, DebitModes
from apps.checkout.models import BookingRequest, PaymentModes
from apps.checkout.serializers import json_dateserialiser
from apps.checkout.service import checkout_service
from apps.checkout.service.analytics.order_analytics import confirm_booking_analytics
from apps.checkout.service.booking_confirmation_service import BookingConfirmationService
from apps.checkout.service.checkout_service import save_booking_state, sanitise_data
from apps.checkout.service.initiate_booking_handler import InitiateBooking
from apps.checkout.service.upfront_part_pay_service import UpfrontPartPayService
from apps.common import error_codes
from apps.common.exceptions.custom_exception import InitiateBookingValidationError
from apps.common.json_encoder import CustomBytesEncoder
from apps.common.slack_alert import SlackAlertService
from apps.discounts.exceptions import InvalidCouponException
from apps.featuregate.constants import FEATURE_PAGE_NAMES
from apps.payments.payment_order_helper import PaymentOrderHelper
from apps.payments.service.payment_service_v2 import PaymentServiceV2
from apps.reward.service.reward import RewardService
from base.logging_decorator import log_args
from base.renderers import TreeboJSONRenderer
from base.views.api_v2 import TreeboAPIView2
from common.exceptions.treebo_exception import TreeboException
from common.services.feature_toggle_api import FeatureToggleAPI

logger = logging.getLogger(__name__)


class PayNow(TreeboAPIView2):
    renderer_classes = (TreeboJSONRenderer,)

    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    reward_service = RewardService()
    payment_service = PaymentServiceV2()
    payment_handler = PaymentOrderHelper()
    upfront_partpay_service = UpfrontPartPayService()

    booking_confirmation_service = BookingConfirmationService()

    def __freeze_booking_request(self, booking_data, request, instant_booking, booking_mode=None):

        booking_data = json.loads(
            json.dumps(
                booking_data,
                cls=CustomBytesEncoder))
        bid = booking_data.get('bid')
        logger.info(
            "__freeze_booking_request for pay@hotel %s ",
            json.dumps(booking_data))

        booking_payload = {
            "bid": bid,
            # Hardcoding value 0, TODO: make a map for paid and unpaid nad set
            # the value for it
            "pay_at_hotel": PaymentModes.if_pay_at_hotel(booking_data.get('payment_mode')),
            "name": booking_data.get('name', ''),
            "email": booking_data.get('email'),
            "country_code": '91' if booking_data.get('country_code', '91') == '' else booking_data.get('country_code', '91'),
            "phone": booking_data.get('mobile', ''),
            "organization_name": booking_data.get('organization_name', ''),
            "organization_address": booking_data.get('organization_address', ''),
            "organization_taxcode": booking_data.get('organization_taxcode', ''),
            "user_signup_channel": request.COOKIES.get("user_signup_channel"),
            "message": booking_data.get('message', ""),
            "check_in_time": booking_data.get('check_in_time'),
            "check_out_time": booking_data.get('check_out_time'),
            "user": request.user,
            "instant_booking": instant_booking,
            'http_host': request.META['HTTP_HOST'],
            "payment_mode": PaymentModes.get(booking_data.get('payment_mode')) if PaymentModes.get(
                booking_data.get('payment_mode')) else PaymentModes.PAID.value,
            "utm_source": str(request.COOKIES.get('utm_source')) if request.COOKIES.get(
                'utm_source') else "",
            "utm_medium": str(request.COOKIES.get('utm_medium')) if request.COOKIES.get(
                'utm_medium') else "",
            "referral_click_id": str(request.COOKIES.get('avclk')) if request.COOKIES.get(
                'avclk') else "",
            "utm_campaign": str(request.COOKIES.get('utm_campaign')) if request.COOKIES.get(
                'utm_campaign') else "",
            "host_ip_address": str(get_ip(request)) if get_ip(request) else ""
        }

        logger.info(
            "Booking Payload for freezing booking request: %s",
            booking_payload)
        booking_request = save_booking_state(booking_payload, booking_mode)
        return booking_request

    def __booking_handler(self, booking_request):
        '''

        :param is_secure: For https connections
        :param booking_request: booking request object
        :param payment_gateway: Payment gateway name
        :return: Map of payment gateway details and order id
        '''
        booking_request.comments = checkout_service.build_special_preference(
            booking_request)
        booking_request.save()
        logger.info("calling initiate booking for bid %s ", booking_request.id)
        # booking = BookingClient.initiate_booking(booking_request)
        InitiateBooking.internal_booking_initiate(booking_request)
        # mocking the booking status as completed to ensure that the inventory is updated
        StashUtils.check_booking_status_and_update_inventory(booking_request,
                                                             BookingRequest.COMPLETED)
        booking_request.status = BookingRequest.PAYMENT_IN_PROGRESS
        booking_request.save()
        logger.info(
            "initiate booking success for bid %s have order id %s  ",
            booking_request.id,
            booking_request.order_id)
        payment_booking_details = {}
        payment_booking_details.update({
            "order_id": booking_request.order_id,
            'email': booking_request.email,
            'name': booking_request.name,
            'contact': booking_request.phone,
            'bid': booking_request.id
        })
        return payment_booking_details

    def __check_availability(self, booking_request):
        available_rooms = get_availability_service().get_available_rooms(
            booking_request.hotel_id,
            booking_request.checkin_date,
            booking_request.checkout_date,
            booking_request.room_config)
        for room, availability in list(available_rooms.items()):
            if booking_request.room_type.lower(
            ) == room.room_type_code.lower() and availability > 0:
                return True
        return False

    @log_args(logger)
    def post(self, request, *args, **kwargs):  # pylint: disable-msg=R0914,R0911,R0912,R0915
        booking_data = request.data
        booking_data = sanitise_data(booking_data)
        bid = booking_data.get('bid')
        logger.info("Incoming paynow request with "
                    "data:{}".format(booking_data))
        try:
            booking_request = BookingRequest.objects.get(id=bid)
            if booking_request.status in [BookingRequest.COMPLETED, BookingRequest.PART_SUCCESS]:
                context = {
                    'error': {
                        'msg': error_codes.get(error_codes.DUPLICATE_BOOKING_REQUEST)['msg'],
                        'code': error_codes.get(error_codes.DUPLICATE_BOOKING_REQUEST)['code']
                    }
                }
                return Response(context, status=HTTP_400_BAD_REQUEST)
            if FeatureToggleAPI.is_enabled(settings.DOMAIN, "pricing_revamp", False):
                is_available = self.payment_service.is_available(booking_request)
            else:
                is_available = self.__check_availability(booking_request)

        except BookingRequest.DoesNotExist:
            context = {
                'available': False,
                'error': {
                    'msg': 'Booking Request Does not exist',
                    'code': "5005"}}
            return Response(context, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            context = {
                'available': False,
                'error': {
                    'msg': e,
                    'code': "5001"}}
            SlackAlertService.web_alert("Error in availability : " + str(bid))
            return Response(context, status=HTTP_500_INTERNAL_SERVER_ERROR)

        if not is_available:
            context = {
                'available': is_available,
                'error': {
                    'msg': 'Room is sold out',
                    'code': "5001"}}
            SlackAlertService.web_alert(
                "Room is sold out for bid : " + str(bid))
            return Response(context, status=status.HTTP_400_BAD_REQUEST)

        logger.debug("PayNow: Post request made")

        payment_gateway = booking_data.get('gateway')

        if booking_data.get('payment_mode', None) and booking_data[
            'payment_mode'] == PaymentModes.PART_PAID.value:
            booking_mode = PAY_AT_HOTEL_BOOKING_MODE
        else:
            booking_mode = PREPAID_BOOKING_MODE

        try:
            if FeatureToggleAPI.is_enabled("booking", "instant_booking", True):
                booking_request = self.__freeze_booking_request(
                    booking_data, request, True, booking_mode=booking_mode)
            else:
                booking_request = self.__freeze_booking_request(
                    booking_data, request, False, booking_mode=booking_mode)
        except InvalidCouponException as exc:
            logger.exception(exc)
            context = {
                'coupon_applicable': False,
                'error': {
                    'msg': str(exc),
                    'code': "5004"}}
            return Response(context, status=status.HTTP_400_BAD_REQUEST)

        if payment_gateway:
            payment_gateway = str(booking_data.get('gateway')).lower()
        external_payment_gateway_share = self.booking_confirmation_service.get_external_payment_gateway_share(
            booking_request)

        if external_payment_gateway_share and (
            payment_gateway not in settings.AVAILABILE_GATEWAY):
            return Response(
                {
                    "error": {
                        "msg": error_codes.get(
                            error_codes.GATEWAY_NOT_FOUND)['msg'] %
                               payment_gateway,
                        "code": error_codes.get(
                            error_codes.GATEWAY_NOT_FOUND)['code']}},
                status=HTTP_400_BAD_REQUEST)
        request.is_secure()
        organization_taxcode = request.data.get('organization_taxcode', None)

        if not BookingHelper.validate_gstin_for_booking(
            organization_taxcode, bid=bid, api_call='paynow post v3'):
            return Response(
                {
                    "error": {
                        "msg": error_codes.get(
                            error_codes.INVALID_GSTIN_NUMBER)['msg'],
                        "code": error_codes.get(
                            error_codes.INVALID_GSTIN_NUMBER)['code']}},
                status=HTTP_400_BAD_REQUEST)

        debit_mode = DebitModes.PRE_TAX
        # the wallet balance sanity...
        if (
            booking_request and booking_request.wallet_applied and booking_request.wallet_deduction >
            0 and request.user.is_authenticated()):
            _, _, _, wallet_spendable_balance, _, _, debit_mode, _ = \
                self.reward_service.get_wallet_balance(request.user, booking_request.booking_channel)
            if float(round(
                booking_request.wallet_deduction,
                2)) > wallet_spendable_balance:
                logger.error("Insufficient funds in wallet, wallet has %s, deduction is %s",
                             str(wallet_spendable_balance), str(booking_request.wallet_deduction))
                context = {
                    'coupon_applicable': False,
                    'error': {
                        'msg': "The wallet doesn't have sufficient funds.",
                        'code': "5006"}}
                return Response(context, status=status.HTTP_400_BAD_REQUEST)

        amount_to_send = int(booking_request.total_amount * 100)
        if booking_request.payment_offer_price:
            amount_to_send = int(booking_request.payment_offer_price * 100)

        # Changes are part of loyalty service
        if booking_request.wallet_applied and debit_mode == DebitModes.POST_TAX:
            # what is the unit of currency here?
            amount_to_send = amount_to_send - int(booking_request.wallet_deduction * 100)



        if PaymentModes.get(booking_data.get('payment_mode')) == PaymentModes.PARTIAL_RESERVE.value \
            and booking_request.utm_source and booking_request.booking_channel:
            partpay_amount = self.upfront_partpay_service.validate_and_get_partpay_amount(
                amount_to_send / 100.00, booking_request.utm_source,
                booking_request.booking_channel,
                booking_request.hotel_id, FEATURE_PAGE_NAMES.ITINERARY)
            amount_to_send = partpay_amount
        else:
            amount_to_send = amount_to_send / 100.00

        if booking_request.payment_mode == PaymentModes.PART_PAID.value and booking_request.part_pay_amount:
            amount_to_send = booking_request.part_pay_amount

        try:
            order_payment_details = self.__booking_handler(booking_request)
            redirect_url_on_gatewaypayment_completion = \
                self.payment_service.generate_ui_redirect_urls_to_be_used_upon_payment_confirmation(
                    order_payment_details["order_id"], bid)

            payment_order, payment_details = self.payment_service.initiate_order_from_paymentservice(
                amount_to_send, payment_gateway, bid, redirect_url_on_gatewaypayment_completion,
                booking_order_id=order_payment_details["order_id"],
                utm_source=booking_request.utm_source)

            logger.info('Initiating booking for order id %s', booking_request)
            self.payment_handler.update_orderid_in_paymentorder(payment_order,
                                                                booking_order_id=booking_request.order_id)

            response = {
                "bid": bid,
                "pg_order_id": payment_order.pg_order_id if payment_order else '',
                "ps_order_id": payment_order.ps_order_id if payment_order else '',
                "order_id": order_payment_details.get('order_id'),
                "redirect_url": payment_details.ps_payment.pg_meta.get('redirect_url', '') if
                payment_details and payment_details.ps_payment else '',
                "pg_meta": payment_details.ps_payment.pg_meta.get('pg_meta', {}) if
                payment_details and payment_details.ps_payment and payment_details.ps_payment.pg_meta else {},
                "order_event_args": confirm_booking_analytics(request, booking_request)
            }
            logger.info("response outgoing for bid:{} "
                        "is {}".format(bid, json.dumps(response, default=json_dateserialiser)))
            return Response(response)
        except InitiateBookingValidationError as e:
            logger.exception("error while initiating the booking for input request data:{} due "
                             "to:{}".format(booking_data, str(e)))
            return self.get_simple_response_from_error_code_for_booking(
                error_codes.INITIATE_BOOKING_VALIDATION_ERROR)
        except TreeboException as e:
            SlackAlertService.send_slack_alert_for_exceptions(status=500,
                                                              request_param=request.data,
                                                              message=e.message,
                                                              dev_msg=e.developer_message if e.developer_message else
                                                              "initiate_order_from_paymentservice error amount {0} "
                                                              "gateway {1}  bid {2}".format(
                                                                  amount_to_send,
                                                                  payment_gateway, bid),
                                                              class_name=self.__class__.__name__)
            logger.exception(
                "initiate_order_from_paymentservice error amount %s gateway %s  bid %s ",
                amount_to_send,
                payment_gateway,
                bid)
            return self.get_simple_response_from_treebo_exception_for_booking(
                e)
        except Exception as e:
            SlackAlertService.send_slack_alert_for_exceptions(status=500,
                                                              request_param=request.data,
                                                              message=e.__str__(),
                                                              class_name=self.__class__.__name__)
            logger.exception(
                "Unable to initiate booking for bid %s ",
                booking_request.id)
            return self.get_simple_response_from_error_code_for_booking(
                error_codes.BOOKING_INITIATE_ERROR)
