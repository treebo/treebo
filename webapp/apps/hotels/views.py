import logging

from django.shortcuts import render
from rest_framework.views import APIView

# import search

logger = logging.getLogger(__name__)
isMobile = False


class Page404(APIView):
    """

    """
    template_name = 'hotels/page404.html'

    def get(self, request):
        """

        :param request:
        :return:
        """
        context = {'page_name': 'Page Not Found'}
        return render(request, self.template_name, context)
