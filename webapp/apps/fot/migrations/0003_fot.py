# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('fot', '0002_fotreservationsrequest'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('credits_consumed', models.IntegerField()),
                ('score', models.DecimalField(max_digits=8, decimal_places=2)),
                ('status', models.CharField(max_length=100)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_fot',
            },
        ),
    ]
