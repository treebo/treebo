import requests
from django.conf import settings
from django.core.cache import cache


class HotelReviews(object):
    CACHE_TIMEOUT = 24 * 60 * 60

    @staticmethod
    def get_cache_key(hotel_id):
        return 'apicache_hotel_reviews_{0}'.format(hotel_id)

    @staticmethod
    def _get_hotel_reviews(hotel):
        try:
            data = cache.get(HotelReviews.get_cache_key(hotel.id))
        except Exception as e:
            data = None
        if data:
            return data
        review_url = settings.TRIP_ADVISOR_BASE_URL + '/location/'
        review_url = review_url + 'hotel_id?key=' + settings.TRIP_ADVISOR_API_KEY
        response = requests.get(review_url)
        data = response.json()
        cache.set('hotel_review', data, HotelReviews.CACHE_TIMEOUT)
        return data

    @staticmethod
    def get_all_reviews(hotels):
        results = []
        for hotel in hotels:
            data = HotelReviews._get_hotel_reviews(hotel)
            results.append(data)
        return results

    @staticmethod
    def get_hotel_rating(hotels):
        hotel_ratings = {}
        for hotel in hotels:
            data = HotelReviews._get_hotel_reviews(hotel)
            rating = {
                'rating': data['rating'],
                'rating_image_url': data['rating_image_url']
            }
            hotel_ratings[hotel.id] = rating
        return rating
