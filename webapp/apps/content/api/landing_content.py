import logging
from django.core.cache import cache
from rest_framework.response import Response

from apps.hotels.helpers import CityHotelHelper
from base.views.api import TreeboAPIView
from common.constants import common_constants
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from services.restclient.contentservicerestclient import ContentServiceClient
from webapp.data_services.city_repository import CityRepository


logger = logging.getLogger(__name__)

class LandingContentAPI(TreeboAPIView):
    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'apicache_landingcontent'

    @staticmethod
    def invalidate_cache():
        cache.delete(LandingContentAPI.CACHE_KEY)

    def get(self, request):
        """
        No parameter required. Gives Landing content for the website.
        """
        result = None
        try:
            result = cache.get(LandingContentAPI.CACHE_KEY)
        except Exception as e:
            logger.exception("Exception in getting cache data for key %s", LandingContentAPI.CACHE_KEY)

        if result:
            cities = CityHotelHelper.getCityHotels()
            result['cities'] = cities
            return Response(result)

        landingbanner = ContentServiceClient.getValueForKey(
            request, 'landing_banner_list', 1)
        offers = ContentServiceClient.getValueForKey(request, 'offers_list', 1)
        content_block = ContentServiceClient.getValueForKey(
            request, 'top_cities', 1)
        promises = ContentServiceClient.getValueForKey(
            request, 'promises_list', 1)
        awards = ContentServiceClient.getValueForKey(
            request, 'awards_recognitions_list', 1)
        cities = CityHotelHelper.getCityHotels()
        budget_hotels = CityHotelHelper.get_hotels_with_url_prefix(url_prefix="budget_hotels_in_city")
        couple_friendly_hotels = CityHotelHelper.get_hotels_with_url_prefix(url_prefix="couple_friendly_hotels_in_city")
        testimonials_list = ContentServiceClient.getValueForKey(
            request, 'testimonial_list', 1)
        initiatives = ContentServiceClient.getValueForKey(
            request, 'trending_initiatives', 1)
        press_release_content = ContentServiceClient.getValueForKey(
            request, 'press_releases', 1)
        brand_video = ContentServiceClient.getValueForKey(
            request, 'brand_video', 1)
        joinus_banner = ContentServiceClient.getValueForKey(
            request, 'joinus_banner', 1)
        feedback_email = ContentServiceClient.getValueForKey(request, 'feedback_email', 1)

        city_repository = RepositoriesFactory.get_city_repository()

        hotel_repository = RepositoriesFactory.get_hotel_repository()
        city_count = city_repository.get_enabled_cities_count()
        hotels_count = hotel_repository.get_total_hotel_count(
            only_active=True)
        #hotels_count = Hotel.objects.filter(status=Hotel.ENABLED).count()
        room_count = hotel_repository.get_aggregate_value(
            {'status': Hotel.ENABLED}, 'sum', 'room_count')
        #room_count = Hotel.objects.filter(status=Hotel.ENABLED).aggregate(total_room_count=Sum('room_count'))['total_room_count']
        subtitles = content_block["subtitles"]
        d = {
            'room_count': room_count,
            'city_count': city_count,
            'hotel_count': hotels_count}
        updated_subtitles = []
        for subtitle in subtitles:
            updated_subtitles.append(subtitle % d)
        content_block["subtitles"] = updated_subtitles

        banner_city_offers_dict= {
            "banners": landingbanner,
            "content_block": content_block,
            "offers": offers,
            "promises": promises,
            "awards": awards,
            "cities": cities,
            "budget": budget_hotels,
            "couple_friendly_hotels": couple_friendly_hotels,
            "testimonials": testimonials_list,
            "initiatives": initiatives,
            "brand_video": brand_video,
            "press_release": press_release_content,
            "joinus_banner": joinus_banner,
            "feedback_email": feedback_email
        }

        cache.set(LandingContentAPI.CACHE_KEY, banner_city_offers_dict,
                  LandingContentAPI.CACHE_TIMEOUT)
        return Response(banner_city_offers_dict)
