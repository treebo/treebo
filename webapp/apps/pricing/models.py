# -*- coding: utf-8 -*-
from django.db import models
from djutil.models import TimeStampedModel
from jsonfield import JSONField

from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.models.hotel import Hotel
from dbcommon.models.location import State
from dbcommon.models.room import Room

__author__ = 'baljeet'


class PreferredPromo(TimeStampedModel, DefaultPermissions):
    hotel = models.ForeignKey(
        Hotel,
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING)
    description = models.TextField()
    type = models.CharField(max_length=100, null=False)
    min_advance_days = models.IntegerField(default=-1, null=False)
    max_advance_days = models.IntegerField(default=-1, null=False)
    min_nights = models.IntegerField(default=1)
    refundable = models.BooleanField()
    stay_start = models.DateField()
    stay_end = models.DateField()
    booking_start = models.DateField()
    booking_end = models.DateField()
    booking_days_bitmap = models.IntegerField(default=-1, null=False)
    stay_days_bitmap = models.IntegerField(default=-1, null=False)
    discountType = models.CharField(
        default='Percent', null=False, max_length=100)

    PROMO_ACTIVE = 0
    PROMO_INACTIVE = 1
    PROMO_STATUS_CHOICES = (
        (PROMO_ACTIVE, 'Active'),
        (PROMO_INACTIVE, 'Expired')
    )
    status = models.IntegerField(
        default=PROMO_ACTIVE,
        choices=PROMO_STATUS_CHOICES)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_preferredpromo'
        verbose_name = 'Preferred Promo'
        verbose_name_plural = 'Preferred Promos'

    def __unicode__(self):
        return '%s [%s-%s]' % (self.hotel, self.stay_start, self.stay_end)

    def __str__(self):
        return '%s [%s-%s]' % (self.hotel, self.stay_start, self.stay_end)


class PromoConditions(TimeStampedModel, DefaultPermissions):
    condition_name = models.CharField(max_length=200, null=False)
    condition_params = JSONField()
    promo = models.ForeignKey(
        PreferredPromo,
        blank=False,
        null=False,
        related_name='conditions',
        on_delete=models.CASCADE)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_promoconditions'
        verbose_name = 'Promo Condition'
        verbose_name_plural = 'Promo Conditions'

    def __unicode__(self):
        return '%s %s' % (self.promo, self.condition_name)

    def __str__(self):
        return '%s %s' % (self.promo, self.condition_name)


class ChannelManagerRatePlan(TimeStampedModel, DefaultPermissions):
    room = models.ForeignKey(
        Room,
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING)
    hotel = models.ForeignKey(
        Hotel,
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING)
    is_active = models.BooleanField(default=False)
    start_date = models.DateField()
    end_date = models.DateField()
    base_price = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    price_increment = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_channelmanagerrateplan'
        verbose_name = 'Channel Manager Rate Plan'
        verbose_name_plural = 'Channel Manager Rate Plans'

    def __unicode__(self):
        return '%s %s - %s' % (self.room, self.hotel, self.start_date)

    def __str__(self):
        return '%s %s - %s' % (self.room, self.hotel, self.start_date)


class RoomRatePlan(TimeStampedModel, DefaultPermissions):
    room = models.ForeignKey(
        Room,
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING)
    hotel = models.ForeignKey(
        Hotel,
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING)
    is_active = models.BooleanField(default=False)
    start_date = models.DateField()
    end_date = models.DateField()
    base_price = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    price_increment = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_roomrateplan'
        verbose_name = 'Room Rate Plan'
        verbose_name_plural = 'Room Rate Plans'

    def __unicode__(self):
        return '%s %s - %s' % (self.room, self.hotel, self.start_date)

    def __str__(self):
        return '%s %s - %s' % (self.room, self.hotel, self.start_date)


class LuxuryTax(TimeStampedModel, DefaultPermissions):
    start_range = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    end_range = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        default=100000)
    tax_value = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    state = models.ForeignKey(State, on_delete=models.DO_NOTHING)
    hotel = models.ForeignKey(
        Hotel,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_luxurytax'
        verbose_name = 'Luxury Tax'
        verbose_name_plural = 'Luxury Taxes'

    def __unicode__(self):
        return '%s-%s:%s' % (self.state.name,
                             str(self.start_range),
                             str(self.end_range))

    def __str__(self):
        return '%s-%s:%s' % (self.state.name,
                             str(self.start_range),
                             str(self.end_range))

class StateTax(TimeStampedModel, DefaultPermissions):
    state = models.ForeignKey(State, on_delete=models.DO_NOTHING)
    TAX_TYPE_CHOICES = (
        ('ST', 'Service Tax'),
        ('SBC', 'Swachh Bharat Cess'),
        ('KKC', 'Krishi Kalyan Cess')
    )
    tax_type = models.CharField(
        max_length=5,
        default="ST",
        choices=TAX_TYPE_CHOICES)
    tax_value = models.DecimalField(
        max_digits=20, decimal_places=10, default=8.4)
    luxury_tax_on_base = models.BooleanField(default=False)
    effective_date = models.DateField(null=True)
    expiry_date = models.DateField(null=True)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'State Tax'
        verbose_name_plural = 'State Taxes'

    def __unicode__(self):
        return '%s [%s, %s]' % (self.state.name, self.tax_type, self.tax_value)

    def __str__(self):
        return '%s [%s, %s]' % (self.state.name, self.tax_type, self.tax_value)


class RoomPrice(TimeStampedModel, DefaultPermissions):
    date = models.DateField(null=False, db_index=True)
    room = models.ForeignKey(
        Room,
        blank=False,
        null=False,
        db_index=True,
        on_delete=models.DO_NOTHING)
    guest_count = models.IntegerField(default=1, null=False)
    total_amount = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    tax_amount = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    rack_rate = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    promo_applied = models.BooleanField(default=False)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Room Price'
        verbose_name_plural = 'Room Prices'

    def __unicode__(self):
        return '%s %s %s' % (self.date, self.room, self.rack_rate)

    def __str__(self):
        return '%s %s %s' % (self.date, self.room, self.rack_rate)


class RoomPriceV2(TimeStampedModel, DefaultPermissions):
    date = models.DateField(null=False, db_index=True)
    room = models.ForeignKey(
        Room,
        blank=False,
        null=False,
        db_index=True,
        on_delete=models.DO_NOTHING)
    guest_count = models.IntegerField(default=1, null=False)
    base_price = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    extra_adult_rate = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    extra_kid_rate = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)

    class Meta(DefaultPermissions.Meta):
        db_table = 'pricing_roomprice_v2'
        verbose_name = 'Room Price V2'
        verbose_name_plural = 'Room Prices V2'
        index_together = [
            ['room', 'date', 'guest_count'],
        ]

    def __unicode__(self):
        return '%s %s %s %s' % (self.date, self.room,
                                self.guest_count, self.base_price)

    def __str__(self):
        return '%s %s %s %s' % (self.date, self.room,
                                self.guest_count, self.base_price)

class FeatureToggle(TimeStampedModel, DefaultPermissions):
    namespace = models.CharField(max_length=100, null=False)
    feature = models.CharField(max_length=200, null=False)
    environment = models.CharField(
        max_length=200, null=False, default='preprod')
    is_enabled = models.BooleanField(default=False)

    class Meta(DefaultPermissions.Meta):
        db_table = 'feature_toggle'
        verbose_name = 'Feature Toggle'
        verbose_name_plural = 'Feature Toggles'

    def __unicode__(self):
        return '%s %s %s' % (self.namespace, self.feature, self.is_enabled)

    def __str__(self):
        return '%s %s %s' % (self.namespace, self.feature, self.is_enabled)
