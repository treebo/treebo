# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
import logging

from apps.common.data_classes.base_data_class import BaseDataClass
from apps.common.data_classes.container import Container
from apps.common.data_classes.user import User

logger = logging.getLogger(__name__)


class AgeGroup(BaseDataClass):

    def __init__(self, name, start_age, end_age):
        self.name = name
        self.start_age = start_age
        self.end_age = end_age
        self.range = range(start_age, end_age + 1, 1)

    def __contains__(self, item):
        return item in self.range

    def __repr__(self):
        return '{n} ({s} -> {e})'.format(
            n=self.name,
            s=self.start_age,
            e=self.end_age
        )

    def __str__(self):
        return self.name


Adult = AgeGroup('Adult', 13, 200)
Child = AgeGroup('Child', 6, 12)
Infant = AgeGroup('Infant', 0, 5)

AgeGroups = [Adult, Child, Infant]


class Guest(User):

    def __init__(self, uid, name, email, phone_number, age=Adult.end_age):
        self.age = int(age)
        super(Guest, self).__init__(uid, name, email, phone_number)

    @classmethod
    def guest_with_no_details(cls):
        return Guest('', '', '', '')


class Guests(Container):
    pass
