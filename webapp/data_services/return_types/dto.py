import json
import datetime
from uuid import UUID

from django.db.models.fields.files import FieldFile


class DTO:
    def __init__(self, properties_dict):
        for k, v in list(properties_dict.items()):
            self.__setattr__(k, v)

    def toJSON(self):
        try:
            return json.dumps(
                self,
                default=custom_encoder,
                check_circular=True)
        except Exception as e:
            raise e
            # return str(self)


def custom_encoder(obj):
    import decimal
    if isinstance(obj, datetime.datetime):
        return obj.isoformat()
    if isinstance(obj, bytes):
        decoded_str = obj.decode('utf-8')
        return decoded_str
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    if isinstance(obj, DTO):
        return obj.__dict__
    if isinstance(obj, FieldFile):
        return obj.name
    if isinstance(obj, datetime.time):
        return obj.isoformat()
    if isinstance(obj, UUID):
        return obj.hex
    else:
        raise TypeError(
            "Unserializable object {} of type {}".format(
                obj, type(obj)))
