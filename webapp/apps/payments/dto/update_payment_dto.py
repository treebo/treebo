from rest_framework import serializers


class UpdatePaymentDTO(serializers.Serializer):
    order_id = serializers.CharField(max_length=40)
    pg_payment_id = serializers.CharField(max_length=40)
    ps_order_id = serializers.CharField(max_length=40)
