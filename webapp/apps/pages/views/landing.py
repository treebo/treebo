import json
import logging


from apps.common import utils
from apps.hotels.api.v1.get_all_cities import CityInformation
from apps.hotels.helpers import CityHotelHelper
from base.views.template import TreeboTemplateView
from common.constants import common_constants as const
from services.restclient.contentservicerestclient import ContentServiceClient
from apps.common.json_encoder import CustomBytesEncoder

logger = logging.getLogger(__name__)

'''
    Q. How to do http redirection
    A. Simply return return django.http.HttpResponseRedirect object from getMobileData or getData. For eg.

    def getMobileData(self, request, args, kwargs):
        logger.info("calling index get_mobile_data")
        return HttpResponseRedirect('/search', 301)


    Q. How to set cookie or response headers
    A. Just override render_to_response function. Here is an example

    def render_to_response(self, context, **response_kwargs):
        response_kwargs['content-type'] = 'text/xml; charset=utf-8'

        response = super(MyView, self).render_to_response(context, **response_kwargs)
        response.set_cookie("success","success")

        return response
'''


class Index(TreeboTemplateView):
    template_name = "landing/index.html"

    def __get_common_context(self, request):
        user = request.user

        width, height = utils.Utils.getMobileImageSize("home")
        imageCityParams = utils.Utils.build_image_params(
            width, height, const.FIT_TYPE, const.CROP_TYPE, const.QUALITY)
        context = {
            'customerCare': const.CUSTOMER_CARE_NUMBER,
            'imageCityParams': imageCityParams,
            'user': user,
            'seo': {
                "title": "Treebo - Budget Hotels in India | Online Hotel Booking",
                "description": "Treebo Hotels - Fastest growing chain of best budget hotels in India. Book a Treebo hotel online & experience delightful stays at budget prices. Book Now!",
                "keywords": "Treebo hotels, budget hotels, budget hotels in india, affordable hotels, hotels in india, hotels booking, book hotels online, online hotels booking",
                "canonical": "//www.treebo.com/",
                "publisher": "//plus.google.com/105788078549969258020"},
        }
        return context

    def getData(self, request, args, kwargs):
        # template_name = "desktop/landing/index.html"
        logger.debug("calling index getData")
        context = self.__get_common_context(request)

        city_information_view = CityInformation.as_view()
        landing_dict = city_information_view(request)
        landing_dict = json.loads(landing_dict.content.decode('utf-8'))
        marketingBlock = ContentServiceClient.getValueForKey(
            request, 'marketing_block', 1)
        landingBannerList = ContentServiceClient.getValueForKey(
            request, 'landing_banner_list', 1)

        context.update({
            'styles': ['desktop/css/landing.css'],
            'scripts': ['desktop/js/landing.js'],
            'pageName': 'homePage',
            'landing_banner': landingBannerList,
            'marketing_block': marketingBlock,
            'city_count': landing_dict['city_count'],
            'hotel_count': landing_dict['hotel_count'],
            'find_treebo_link': landing_dict['find_treebo_link'],
            'googleMaps': True,
        })

        return context

    def getMobileData(self, request, args, kwargs):
        logger.debug("calling index getMobileData")
        context = self.__get_common_context(request)
        marketing_block = ContentServiceClient.getValueForKey(
            request, 'marketing_block_mobile', 1)
        landing_banner = ContentServiceClient.getValueForKey(
            request, 'landing_banner_list_mobile', 1)

        context.update({
            'landing_banner': landing_banner,
            'marketing_block': marketing_block,
            'styles': ['mobile/css/landing.css'],
            'scripts': ['mobile/js/landing.js']
        })
        return context
