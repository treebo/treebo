import logging

from rest_framework.response import Response

from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from base import log_args
from base.views.api import TreeboAPIView

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)


class Remail(TreeboAPIView):
    """
        Resend email API
    """

    def dispatch(self, *args, **kwargs):
        return super(Remail, self).dispatch(*args, **kwargs)

    def get(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request):
        """
        :param request:
        :return:
        """
        order_id = request.POST.get('order_id')
        email = request.POST.get('email')
        try:
            booking_request = BookingRequest.objects.get(order_id=order_id)
        except BookingRequest.DoesNotExist:
            logger.exception(
                "Resend email booking does not exist for orderid = %s" %
                order_id)
            return Response({"Resend Failed"}, status=404)

        user = booking_request.user_id

        BookingHelper.sendEmail(booking_request, newEmail=email)
        return Response({"msg": "Email Sent"})
