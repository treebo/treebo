# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0006_auto_20160208_0659'),
    ]

    database_operations = [
        migrations.AlterModelTable('Booking', 'bookings_booking'),
        migrations.AlterModelTable('Reservation', 'bookings_reservation'),
        migrations.AlterModelTable('RoomBooking', 'bookings_roombooking')
    ]

    state_operations = [
        migrations.DeleteModel('Booking'),
        migrations.DeleteModel('Reservation'),
        migrations.DeleteModel('RoomBooking')
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            database_operations=database_operations,
            state_operations=state_operations)
    ]
