import logging

from apps.auth.treebo_auth_client import TreeboAuthClient
from base.token_helper import TokenHelper

logger = logging.getLogger(__name__)


def logout_notifier(sender, request, user, **kwargs):
    logger.info("logout notifier ")


def login_notifier(sender, request, user, **kwargs):
    logger.info("login notifier ")
