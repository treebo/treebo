from base import log_args
from dbcommon.models.direction import Directions


class DirectionsWebRepository:

    @log_args()
    def get_all_directions_for_hotel(self, hotel_name=None, hotel_id=None):
        if hotel_id:
            return Directions.objects.filter(hotel=hotel_id)
        elif hotel_name:
            return Directions.objects.filter(hotel__name=hotel_name)
        return None

    @log_args()
    def get_directions_ordered_by_landmark_popularity(self, hotel_id):
        return Directions.objects.filter(
            hotel=hotel_id).order_by("-landmark_obj__popularity")



