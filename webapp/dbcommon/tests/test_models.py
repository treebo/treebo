from django.test import TestCase
from model_mommy import mommy

from dbcommon.models.direction import Directions
from dbcommon.models.discounts import Coupon, Coupon_Constraint, Coupon_Date_Constraint
from dbcommon.models.facilities import Amenity, AmenityMemberShip, Facility
from dbcommon.models.hotel import Hotel, Highlight
from dbcommon.models.location import City, Locality, State, LandmarkType, \
    NotifyNewLocation
from dbcommon.models.miscellanous import Image, Feedback, NotificationArchive
from dbcommon.models.mobileapkbackend import ApkDetails
from dbcommon.models.partner import Corporate, Partner, PartnerInfo
from dbcommon.models.profile import User
from dbcommon.models.room import Room

'''
	Models can be tested out using the model mommy to create data without using fixtures.
	if using fixture, use the manage.py dumpdata to create fixtures.
	Fixtures are created inside fixture directory at project root.
'''
