from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from base import log_args
from base.views.api import TreeboAPIView
from dbcommon.services.web_cs_transformer import WebCsTransformer

import logging

logger = logging.getLogger(__name__)


class WebCsTransformerAPI(TreeboAPIView):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(WebCsTransformerAPI, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        data = request.GET
        try:
            hotel_web_ids = data.get('hotel_ids').strip("'").split(',') if data.get('hotel_ids') else None
            hotel_cs_ids = data.get('cs_ids').strip("'").split(',') if data.get('cs_ids') else None

            if not hotel_web_ids and not hotel_cs_ids:
                return JsonResponse({}, status=HTTP_400_BAD_REQUEST)

            if hotel_web_ids:
                cs_ids = WebCsTransformer.get_hotel_cs_ids_from_web_ids(hotel_web_ids)
                return JsonResponse(cs_ids, status=HTTP_200_OK)
            elif hotel_cs_ids:
                web_ids = WebCsTransformer.get_hotel_web_ids_from_cs_ids(hotel_cs_ids)
                return JsonResponse(web_ids, status=HTTP_200_OK)
        except Exception:
            logger.exception("Couldn't process post api for web-cs-transformer with request {0}".format(data))
            return JsonResponse({}, status=HTTP_400_BAD_REQUEST)
