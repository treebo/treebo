from django.contrib.auth.signals import user_logged_out
from django.contrib.auth.signals import user_logged_in

from apps.auth.api.v2.logout import LogoutApi
from apps.auth.api.v2.signals import logout_notifier, login_notifier

user_logged_out.connect(logout_notifier)
user_logged_in.connect(login_notifier)
