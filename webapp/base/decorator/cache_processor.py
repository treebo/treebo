import functools

from django.core.cache import cache


def cache_process(key_pattern, timeout, key_fields_in_argument):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            key_fields = {key_field: kwargs[key_field] for key_field in key_fields_in_argument if key_field in kwargs}
            key = key_pattern.format(**key_fields)
            if key in cache:
                return cache.get(key)
            else:
                value = func(*args, **kwargs)
                cache.set(key, value, timeout)
                return value
            return

        return wrapper

    return decorator