from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from apps.checkout.api.v1.cancel_booking import CancelBookingAPI
from apps.checkout.api.v1.check_booking_status import CheckBookingStatus
from apps.checkout.api.v1.checkout_actions import initiate_booking, apply_discount, check_availability
from apps.checkout.api.v1.payathotel_handler import PayAtHotelHandler
from apps.checkout.api.v1.payuhandler import PayU
from apps.checkout.api.v1.razorpay_handler import RazorPay
from apps.checkout.api.v1.itinerary_page_api import ItineraryPageAPI
from apps.checkout.api.v1.printpdf import PrintPdf, DisplayPDF
from apps.checkout.api.v1.resendemail import Remail

app_name = 'checkout'
urlpatterns = [
    url(r'^coupon/?$', apply_discount, name='apply-coupon'),
    url(r'^booking/status/?$', csrf_exempt(CheckBookingStatus.as_view()), name='status'),
    url(r'^booking/cancel/?$', csrf_exempt(CancelBookingAPI.as_view()), name='cancel'),
    url(r'^availability/$', check_availability, name='check-availability'),
    url(r'^itinerary/$', ItineraryPageAPI.as_view(), name='itinerary-page'),
    url(r'^initiate-booking/?$', initiate_booking, name='initiate-booking'),
    url(r'^payu/?$', PayU.as_view(), name='payu'),
    url(r'^razorpay/?$', RazorPay.as_view(), name='razorpay'),
    url(r'^pay-later/?$', PayAtHotelHandler.as_view(), name='payathotel'),
    url(r'^resendemail/$', Remail.as_view(), name='resend-email'),
    url(r'^printpdf/$', PrintPdf.as_view(), name='printpdf'),
    url(r'^display/$', DisplayPDF.as_view(), name='displaypdf'),
]
