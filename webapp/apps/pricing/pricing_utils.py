import datetime
import copy
from apps.checkout.service.flat_pricing_service import FlatPricingService
from apps.checkout.service.flash_sale_service import FlashSaleService
from apps.hotels.service.hotel_priority_service import HotelPriorityService
from apps.hotels.service.hotel_provider_service import HotelProviderService


class PricingUtils(object):

    @classmethod
    def get_hotel_ids(cls, pdto):

        if hasattr(pdto, 'hotel_ids'):
            return pdto.hotel_ids

        hotel_discount_map = pdto.hotel_discount_map
        hotel_ids = []
        for key, discount_percentage in list(hotel_discount_map.items()):
            hotel_id = key[0]
            hotel_ids.append(hotel_id)

        return hotel_ids

    @classmethod
    def get_sale_details(cls, pdto):
        sale_details = dict()
        booking_date = datetime.date.today()
        flat_pricing_details = None
        flash_sale_details_combined = None

        hotel_ids = PricingUtils.get_hotel_ids(pdto)
        hotel_ids = list(set(hotel_ids))
        is_flat_pricing_applicable, flat_enabled_property_providers, flat_pricing_range_list = FlatPricingService.get_flat_price_details(booking_date,
                                                                                                        pdto.channel,
                                                                                                        pdto.utm_source)

        if is_flat_pricing_applicable:
            flat_pricing_enabled_hotel_ids = HotelProviderService.get_provider_hotels(hotel_ids, flat_enabled_property_providers)
            flat_pricing_details = dict()
            flat_pricing_details['is_flat_pricing_applicable'] = is_flat_pricing_applicable
            flat_pricing_details['flat_pricing_range_list'] = flat_pricing_range_list
            flat_pricing_details['flat_pricing_enabled_hotel_ids'] = flat_pricing_enabled_hotel_ids

        is_flash_sale_applicable, flash_sale_discount_details = FlashSaleService.get_flash_sale_details(booking_date)
        if is_flash_sale_applicable and flash_sale_discount_details:
            flash_sale_details_combined = FlashSaleService.\
                get_combined_flash_details(flash_sale_discount_details, hotel_ids)

        sale_details['flat_pricing_details'] = flat_pricing_details
        sale_details['flash_sale_details'] = flash_sale_details_combined
        return sale_details

    @classmethod
    def get_sale_rate_plan(cls, pdto, hotel_id, rate_plan, sale_details=None):
        sale_rate_plan = copy.copy(rate_plan)
        if sale_details['flat_pricing_details']:
            sale_rate_plan = FlatPricingService.get_price_override(pdto, hotel_id, sale_rate_plan, sale_details['flat_pricing_details'])

        if sale_details['flash_sale_details']:
            sale_rate_plan = FlashSaleService.get_price_override(pdto, hotel_id, sale_rate_plan, sale_details['flash_sale_details'])

        return sale_rate_plan
