# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0053_auto_20170530_0840'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categories',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('name', models.TextField(max_length=200)),
                ('description', models.TextField(max_length=500, null=True, blank=True)),
                ('status', models.IntegerField(default=1, choices=[(0, 'Disabled'), (1, 'Enabled')])),
            ],
            options={
                'abstract': False,
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AddField(
            model_name='hotel',
            name='category',
            field=models.ForeignKey(blank=True, to='dbcommon.Categories', null=True, on_delete=models.DO_NOTHING),
        ),
    ]
