import $ from 'jquery';
import BaseView from './treebolibs/baseview';
import 'parsleyjs';
import queryString from 'query-string';
import '../css/login.less';


const auth = require('./auth');
const LoginView = Object.create(BaseView);

const parsed = queryString.parse(location.search);

const onLogin = () => {
    const { next } = parsed;
    if(next) {
        location.href = next;
    } else {
        location.reload();
    }
};

LoginView.pageLoaded = () => {
    $('#loginButton').on('click', () => {
        const $form = $('#loginForm');
        const email = $('#loginEmailInput').val().trim();
        const password = $('#loginPassowrdlInput').val().trim();

        const loginParsley = $form.parsley();
        loginParsley.on('form:submit', () => {
            auth.login(email, password).then(onLogin, (treeboError) => {
                $('.login__error').html(treeboError.message).show();
            });
            return false;
        });
    });

    $('#fbLogin').on('click', () => {
        auth
          .fbLogin()
          .always(onLogin);
    });

    const onSignIn = (googleUser) => {
        auth
            .sendGoogleRegistrationDataToServer(googleUser.getAuthResponse().id_token)
            .finally(onLogin);
    };

    const onSignInFailure = () => {
        // Handle sign-in errors
    };

    auth
        .getGoogleHandler()
        .then((auth2) => {
            auth2.attachClickHandler('googleLogin', {}, onSignIn, onSignInFailure);
            auth2.attachClickHandler('googleSignup', {}, onSignIn, onSignInFailure);
        });
};

LoginView.setup({
    name: '',
    widgets: [],
});
