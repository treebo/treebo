import json
import logging

import requests
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db.models.expressions import F
from django.shortcuts import redirect
from django.views.generic.base import TemplateView

from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from apps.checkout.service import checkout_service
from apps.checkout.tasks import send_audit_confirmation_mail, send_booking_confirmation_notification
from apps.discounts.models import DiscountCoupon
from apps.fot.models import FotReservationsRequest, Fot
from apps.hotels.utils import generate_hash
from apps.payments import payment_gateway
from apps.pricing.dateutils import date_to_ymd_str
from common.exceptions.treebo_exception import TreeboException
from data_services.respositories_factory import RepositoriesFactory
from services.restclient.bookingrestclient import BookingClient
from data_services.exceptions import HotelDoesNotExist

__author__ = 'rohitjain'

logger = logging.getLogger(__name__)


class Transaction(TemplateView):
    template_name = "confirmation/transaction.html"

    def get_instant_booking_context(
            self,
            booking_request,
            channel,
            is_secure=True):
        if self.__is_pay_at_hotel(booking_request):
            context = {
                "booking_id": booking_request.booking_id,
                "pay_at_hotel": True,
                "jsvars": json.dumps(
                    {
                        "pay_at_hotel": str(
                            booking_request.pay_at_hotel),
                        "instant_booking": booking_request.instant_booking,
                    })}
            return context
        else:
            # Done later since payu requires the order id
            return {}

    def __initiate_booking_for_pg(self, booking_request, channel, is_secure):
        pg = "PAYU" if channel == "desktop" else "RAZORPAY"
        pg_object = payment_gateway.pg_factory.get(pg)
        payment_gateway_payload = pg_object.build_pg_payload(
            booking_request, is_secure=is_secure)
        payment_gateway_payload["pay_at_hotel"] = False

        if pg == "RAZORPAY":
            jsvars = payment_gateway_payload.copy()
            jsvars.update({
                "pay_at_hotel": str(booking_request.pay_at_hotel),
                "instant_booking": booking_request.instant_booking
            })
            payment_gateway_payload["jsvars"] = json.dumps(jsvars)
        else:
            payment_gateway_payload["jsvars"] = json.dumps({
                "pay_at_hotel": str(booking_request.pay_at_hotel),
                "instant_booking": booking_request.instant_booking,
            })

        return payment_gateway_payload

    def __create_booking(self, booking_request):
        """

        :param apps.checkout.models.BookingRequest:
        :return:
        """
        booking = BookingClient.create_booking(booking_request)
        booking_request.status = BookingRequest.COMPLETED
        booking_request.booking_id = booking["id"]
        booking_request.order_id = booking["order_id"]
        booking_request.save()
        if booking_request.is_audit:
            checkout_service.make_fot_reservation(booking_request)

        if booking_request.coupon_code and booking_request.coupon_apply:
            try:
                coupon = DiscountCoupon.objects.get(
                    code__iexact=booking_request.coupon_code)
                coupon.mark_used_for_transaction()
            except DiscountCoupon.DoesNotExist:
                logger.exception(
                    "Unable to find coupon code %s",
                    booking_request.coupon_code)
            logger.info("Coupon code max available usage is updated")
        return booking

    def __get_context(self, request, channel):
        booking_request_id, payment = self.__parse_request(request)
        booking_request = BookingRequest.objects.get(pk=booking_request_id)
        booking_request.comments = checkout_service.build_special_preference(
            booking_request)
        context = {}
        if channel == 'desktop':
            context.update({'cleanSlate': 'true',
                            'scripts': ['desktop/js/transaction.js'],
                            'styles': ['desktop/css/transaction.css']})
        else:
            context.update({'cleanSlate': 'true',
                            'scripts': ['mobile/js/transaction.js'],
                            'styles': ['mobile/css/transaction.css']})
        return self.process_instant_booking(
            context, booking_request, channel, request.is_secure()), booking_request, payment

    def process_instant_booking(
            self,
            context,
            booking_request,
            channel,
            is_secure):
        try:
            context.update(
                self.get_instant_booking_context(
                    booking_request, channel, is_secure))
            return context
        except requests.HTTPError as http_error:
            logger.exception("Unable to create a booking")
            raise TreeboException()

    def get(self, request, *args, **kwargs):
        # FIXME check for refresh case. Should move to appropriate state of
        # transaction
        try:
            channel = "mobile" if request.is_mobile else "desktop"
            context, booking_request, payment = self.__get_context(
                request, channel)
            if payment is None:
                if self.__is_pay_at_hotel(booking_request):
                    booking = self.__create_booking(booking_request)
                    send_booking_confirmation_notification.delay(
                        booking_request.order_id)

                else:
                    booking = BookingClient.initiate_booking(booking_request)
                    booking_request.status = BookingRequest.PAYMENT_IN_PROGRESS
                    booking_request.order_id = booking["order_id"]
                    booking_request.booking_id = booking["id"]
                    booking_request.save()
                    # Done here because of PayU since it requires the order id
                    # to be passed to it
                    payment_gateway_payload = self.__initiate_booking_for_pg(
                        booking_request, channel, request.is_secure())
                    context.update(payment_gateway_payload)

            context.update({
                "order_id": booking_request.order_id
            })

            if booking_request.instant_booking and self.__is_pay_at_hotel(
                    booking_request):
                if booking_request.is_audit:
                    hotel_repository = RepositoriesFactory.get_hotel_repository()
                    hotels = hotel_repository.filter_hotels(pk=booking_request.hotel_id)
                    if not hotels or len(hotels) != 1:
                        raise HotelDoesNotExist
                    hotel = hotels[0]
                    #hotel = Hotel.objects.prefetch_related('city', 'locality').get(pk=booking_request.hotel_id)
                    return redirect(
                        BookingHelper.build_fot_confirmation_page(
                            booking_request, hotel))

                return redirect(
                    reverse(
                        "pages:order", kwargs={
                            "order_id": booking_request.order_id}))
            else:
                self.__replicate_treebo_template_view_actions(request, context)
                return self.render_to_response(context)
        except TreeboException:
            booking_request_id, payment = self.__parse_request(request)
            booking_request = BookingRequest.objects.get(pk=booking_request_id)
            if booking_request.is_audit:
                return
            return redirect(
                self.build_quickbook_url(
                    booking_request,
                    "Unable to create booking"))

    def __replicate_treebo_template_view_actions(self, request, context):
        if request.is_mobile:
            self.template_name = "mobile/" + self.template_name
        else:
            self.template_name = "desktop/" + self.template_name

        context["GTM_KEY"] = settings.GTM_KEY
        context['environment'] = settings.ENVIRONMENT
        if isinstance(context, dict):
            if (context.get('jsvars') is not None):
                request.jsvars = context.get('jsvars')

    def __parse_request(self, request):
        booking_request_id = request.GET.get('referenceid', None)
        payment = request.GET.get('payment', None)
        return booking_request_id, payment

    def __is_pay_at_hotel(self, booking_request):
        return booking_request.pay_at_hotel in ("1", "2")

    def build_quickbook_url(self, booking_request, error=None):
        """

        :param apps.checkout.models.BookingRequest booking_request:
        :return:
        """
        hotel_id = booking_request.hotel_id
        if not error:
            quickbook_url = BookingHelper.build_itinerary_page_url_from_params(
                hotel_id, booking_request.room_config, booking_request.room_type, date_to_ymd_str(
                    booking_request.checkin_date), date_to_ymd_str(
                    booking_request.checkout_date), booking_request.coupon_code, booking_request.id)
        else:
            quickbook_url = BookingHelper.build_itinerary_page_url_with_error(
                booking_request, error)
        return quickbook_url
