# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0062_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='EnableCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('city', models.ForeignKey(to='dbcommon.City', on_delete=django.db.models.deletion.CASCADE)),
                ('hotel_type', models.ManyToManyField(to='dbcommon.Categories', null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
    ]
