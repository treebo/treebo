import csv

import psycopg2

from datetime import datetime, timezone
from django.conf import settings


dbname = settings.DATABASES['default']['NAME']
dbpass = settings.DATABASES['default']['PASSWORD']
dbuser = settings.DATABASES['default']['USER']
dbhost = settings.DATABASES['default']['HOST']
dbport = settings.DATABASES['default']['PORT']


def get_city(links):
    parts_of_link = links.split("/")
    city_list = parts_of_link[3].split("-")
    city = city_list[2]
    if city == "new":
        city = city_list[3]
    return city


def get_page(links):
    parts_of_link = links.split("/")
    page = (parts_of_link[3] + '/' + parts_of_link[4] + '/')
    return page


def add_faqs():
    conn = psycopg2.connect(database=dbname, user=dbuser, password=dbpass,
                            host=dbhost, port=dbport)
    conn.autocommit = True
    cursor = conn.cursor()
    with open('./faq.csv') as faqs:
        faq_records = [{column_name: data for column_name, data in faq_record.items()}
                         for faq_record in csv.DictReader(faqs, skipinitialspace=True)]
    for faq_record in faq_records:
        question = faq_record.get('Question')
        link = faq_record.get('Links')
        answer = faq_record.get('Answer')
        hotel_name = faq_record.get('Hotel')

        if link == "" and question == "":
            continue

        city = get_city(link)
        page = get_page(link)
        page_type_id = hotel_name + "," + city
        city = city.capitalize()
        cursor.execute('''select id from hotels_city where name= %s''', (city,))
        city_id = cursor.fetchone()
        user = "Treebo "+city+" Team"
        current_date_time = datetime.now(timezone.utc)

        # checking if the current question has been already added for the same hotel.
        cursor.execute('''select id from posts_question where question_data=%s and page = %s''', (question, page,))
        question_already_exist = cursor.fetchall()

        if question_already_exist:
            continue
        # inserting into questions table
        cursor.execute('''insert into posts_question(created_at, modified_at, question_data, "user", state, status,
        submitted_by_user, page) values (%s,%s,%s,%s,'APPROVED',true,true,%s) Returning id;''',
                       (current_date_time, current_date_time, question, user, page,))

        # finding question id of inserted entry
        related_question_id = cursor.fetchone()[0]

        #   inserting into answers table
        cursor.execute('''insert into posts_answer(created_at, modified_at, answer_data, "user", state, verified,
        related_question_id)
        values (%s,%s,%s,%s,'APPROVED',true,%s) Returning id;''', (current_date_time, current_date_time, answer,
                                                                   user, related_question_id,))

        # inserting into qamapping table.
        cursor.execute('''INSERT INTO public.posts_qamapping (created_at, modified_at, page_type, status,
        priority, page_type_id, city_id, question_id) VALUES(%s,%s,'HD',true,1,%s,%s,%s)''',
                       (current_date_time, current_date_time, page_type_id, city_id, related_question_id,))


def main():
    # NOTE: File needs to be present in this directory by name faq.csv
    add_faqs()


if __name__ == '__main__':
    main()