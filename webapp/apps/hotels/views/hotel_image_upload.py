import datetime
import logging
import os
import zipfile

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.shortcuts import redirect
from django.utils.text import slugify
from formtools.wizard.views import SessionWizardView

from apps.hotels.forms.image_upload_forms import HotelImageForm, ConfirmHotelImageUpload
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.miscellanous import Image
from dbcommon.models.room import Room
from dbcommon.models.hotel import Hotel
from django.db.models import Q

logger = logging.getLogger(__name__)


class HotelImageUpload(SessionWizardView):
    TEMPLATES = {
        "hotel_image_upload": "admin/dbcommon/image/upload_image.html",
        "confirm_image_upload": "admin/dbcommon/image/confirm_image.html"}

    form_list = [
        ('hotel_image_upload', HotelImageForm),
        ('confirm_image_upload', ConfirmHotelImageUpload)
    ]

    file_storage = FileSystemStorage(
        location=os.path.join(
            settings.HOTEL_UPLOAD_FILE_LOCATION,
            'image_upload'))

    def get_template_names(self):
        return [self.TEMPLATES[self.steps.current]]

    def get_context_data(self, form, **kwargs):
        context = super(
            HotelImageUpload,
            self).get_context_data(
            form,
            **kwargs)
        if self.steps.current == 'confirm_image_upload':
            context.update(self.get_hotel_live_dashboard())
        return context

    def get_hotel_live_dashboard(self):
        cleaned_data = self.get_cleaned_data_for_step(self.steps.prev)
        hotel_id = cleaned_data['hotel_id']
        images = cleaned_data['images']
        context = {
            "data": {
                "hotel_id": hotel_id,
            }
        }
        return context

    def done(self, form_list, **kwargs):
        cleaned_data = self.get_cleaned_data_for_step(self.steps.first)
        hotel_id = cleaned_data['hotel_id']
        images = cleaned_data['images']
        self.upload_hotel_images(hotel_id, images)
        return redirect('/admin/dbcommon/hotel/upload_hotel_image/')

    def upload_hotel_images(self, hotel_id, images):
        try:
            conn_s3 = S3Connection(host='s3-ap-southeast-1.amazonaws.com')

            bucket = conn_s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
            try:
                hotel = Hotel.all_hotel_objects.get(
                    Q(id=hotel_id) | Q(hotelogix_id=hotel_id))
            except Exception as e:
                hotel = None
            if hotel:
                rooms = Room.objects.filter(hotel_id=hotel.id)
            else:
                logger.info("Hotel does not exist")
                return
            if rooms:
                if not os.path.exists(
                    os.path.join(
                        settings.IMAGE_UPLOAD_PATH,
                        "temp")):
                    os.mkdir(os.path.join(settings.IMAGE_UPLOAD_PATH, "temp"))
                path = os.path.abspath(
                    os.path.join(
                        settings.IMAGE_UPLOAD_PATH,
                        "temp"))

                folder_name = HotelImageUpload.unzip_hotel_images(
                    hotel.name, path, images)
                for root, dirs, files in os.walk(folder_name):
                    for name in files:
                        if name.lower().endswith(".jpg") or name.lower().endswith(
                                ".jpeg") or name.lower().endswith(".png"):
                            bucket_key = Key(bucket)
                            path = hotel.name + "/" + name
                            path = path.replace(" ", "_")
                            bucket_key.key = settings.S3_STATIC_FILE_PATH + path
                            bucket_key.set_contents_from_filename(
                                root + "/" + name, replace=True, policy='public-read')
                            image = Image()
                            image.hotel = rooms[0].hotel
                            image.tagline = hotel.name
                            image.url = "./" + path
                            for room in rooms:
                                if room.room_type_code.lower() in name.lower():
                                    image.room = room
                            image.save()
            else:
                logger.info("Either rooms or hotel does not exist")
        except Exception as e:
            logger.info("Exception in Uploading Images %s", e)

    @staticmethod
    def unzip_hotel_images(hotel_name, path, images):
        logger.info("unzipping file")
        timestamp = datetime.datetime.now().isoformat()
        hotel_name_path = slugify(hotel_name) + "[" + timestamp + "]"
        folder_name = path + '/' + hotel_name_path
        zip = zipfile.ZipFile(images, 'r')
        zip.extractall(folder_name)
        return folder_name
