from rest_framework import serializers


class ChangePasswordTokenDTO(serializers.Serializer):
    old_password = serializers.CharField(max_length=40)
    new_password = serializers.CharField(max_length=40)
