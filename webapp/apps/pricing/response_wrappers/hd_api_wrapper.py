import logging
from collections import defaultdict

from apps.pricing.client.tax_client import SoaTaxBackend
from apps.reward.service.reward import RewardService
from apps.pricing.response_wrappers.base_wrapper import BaseWrapper
from decimal import Decimal
from django.conf import settings
from http import HTTPStatus
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.hotels.service.hotel_sub_brand_service import HotelSubBrandService
from apps.search.constants import INDEPENDENT_HOTEL_BRAND_CODE
from treebo_commons.utils import dateutils
logger = logging.getLogger(__name__)


class HDApiWrapper(BaseWrapper):
    reward_service = RewardService()

    def __init__(self, hotel_id, cs_hotel_id, room_wise_price, room_wise_availability,
                 user, apply_wallet, checkin, checkout, sku_map, application_code):
        self.hotel_id = hotel_id
        self.sku_map = sku_map
        self.cs_hotel_id = cs_hotel_id
        self.room_wise_price = room_wise_price
        self.room_wise_availability = room_wise_availability
        self.checkin = checkin
        self.checkout = checkout
        super().__init__(user, apply_wallet, application_code=application_code)

    def get_default_price(self):
        default_price_component = {
            'available': 0,
            'availability': False,
            'member_discount_applied': False,
            'member_discount_available': False,
            'rate_plans': [],
            'coupon_applied': False,
            'coupon_code': ''
        }
        return default_price_component

    def get_pricing_response(self):
        hotel_id = self.hotel_id
        default_price = self.get_default_price()
        try:
            wallet_balance = self.get_wallet_balance()
            pricing_response = default_price
            wallet_info =  {}
            if self.room_wise_availability and self.room_wise_price:
                price = {
                    'total_price': {},
                    'wallet_info': {}
                }
                room_wise_price = self.room_wise_price.get(hotel_id, {}).get('room_wise_price', {})

                min_price_breakup = list(room_wise_price.values())[0][0]['total_price_breakup']
                min_post_tax_price = min_price_breakup['post_tax']
                min_discount = min_price_breakup['pre_tax']['discounts']
                for room_type, rate_plan_prices in room_wise_price.items():
                    for rate_plan_price in rate_plan_prices:
                        price_details = rate_plan_price['total_price_breakup']
                        if price_details['post_tax'] < min_post_tax_price:
                            min_post_tax_price = price_details['post_tax']
                            min_discount = price_details['pre_tax']['discounts']

                is_independent_hotel = int(hotel_id) in HotelSubBrandService.get_hotels_ids_for_brand(
                        brand_code=INDEPENDENT_HOTEL_BRAND_CODE)
                min_discount_info = self.get_applied_coupon_info(min_discount)

                for room_type, rate_plan_prices in room_wise_price.items():

                    available = self.room_wise_availability.get(hotel_id, {}).get(room_type, 0)
                    availability = False
                    if available:
                        availability = True

                    room_price_details = {
                        'available': available,
                        'availability': availability,
                        'member_discount_available': False,
                        'member_discount_applied': False,
                        'coupon_code': min_discount_info['coupon_code'],
                        'coupon_applied': True if min_discount_info['discount_applied'] else False,
                        'rate_plans': {}
                    }

                    rate_plans = []
                    for rate_plan_price in rate_plan_prices:
                        web_rate_plan = self.get_web_rate_plan(rate_plan_price)
                        price_details = rate_plan_price['total_price_breakup']
                        wallet_info = self.get_user_wallet_info(wallet_balance,
                                                                price_details['post_tax'])
                        if is_independent_hotel:
                            wallet_info['wallet_deductable_amount'] = 0
                            wallet_info['total_wallet_balance'] = 0
                            wallet_info['total_wallet_points'] = 0
                            wallet_info['wallet_applied'] = False
                        applied_wallet_info = wallet_info if self.apply_wallet else self.get_default_wallet_info()

                        coupon_info = self.get_applied_coupon_info(price_details['pre_tax']['discounts'])
                        policy_name = web_rate_plan['policy_name'] if web_rate_plan['policy_name'] else ''
                        same_day_booking = dateutils.today().date() == self.checkin
                        rate_plans.append({
                            'rate_plan': {
                                'code': policy_name,
                                'description': 'Refundable Plan' if not same_day_booking and policy_name.lower() != 'nrp' else '',
                                'refundable': not same_day_booking and policy_name.lower() != 'nrp',
                                'tag': 'Refundable' if not same_day_booking and policy_name.lower() != 'nrp' else ''
                            },
                            'price': {
                                'wallet_applied': applied_wallet_info['wallet_applied'],
                                'member_discount': 0,
                                'total_discount_percent': coupon_info['discount_applied_percent'],
                                'coupon_code': coupon_info['coupon_code'],
                                'tax': price_details['tax']['total_value'],
                                'pretax_price': price_details['pre_tax']['sell_price'],
                                'wallet_type': applied_wallet_info['wallet_type'],
                                'wallet_deduction': applied_wallet_info['wallet_deductable_amount'],
                                'coupon_applied': coupon_info['coupon_code'],
                                'rack_rate': price_details['pre_tax']['list_price'],
                                'sell_price': price_details['post_tax'],
                                'net_payable_amount': Decimal(price_details['post_tax']) - Decimal(
                                    applied_wallet_info['wallet_deductable_amount']),
                            }
                        })

                    room_price_details['rate_plans'] = rate_plans
                    price['total_price'][room_type.lower()] = room_price_details

                price['wallet_info'] = wallet_info
                if wallet_info and wallet_info['debit_mode'] == 'pre_tax':
                    price = self._add_treebo_points_discount(price)
                pricing_response = price
        except Exception as exception:
            logger.exception("Exception in itinerary api wrapper with error {}".format(str(exception)))
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param='',
                                                        dev_msg="HD API Wrapper",
                                                        message=str(exception),
                                                        class_name=self.__class__.__name__)
            pricing_response = default_price

        return pricing_response

    def _add_treebo_points_discount(self, price):
        tax_request = dict()
        sku_charges = []
        sku_room_type_map = dict()
        room_type_throttled_discount = dict()
        treebo_discount_price_applicable = price['wallet_info']['wallet_applied']\
                                           and price['wallet_info']['total_wallet_points']
        if not treebo_discount_price_applicable:
            return price
        for hotel_id, room_wise_prices in self.room_wise_price.items():
            for room_type, room_type_wise_price in room_wise_prices['room_wise_price'].items():
                for room_price in room_type_wise_price:
                    pretax_price = room_price['total_price_breakup']['pre_tax']['sell_price']

                    throttled_discount = self.throttled_treebo_discount_points(
                        price['wallet_info']['total_wallet_points'],
                        pretax_price,
                        price['wallet_info']['debit_percentage']
                    )
                    room_type_throttled_discount[room_type.lower()] = throttled_discount
                    room_night_sku_count = 0
                    for date_wise_price_breakup in room_price['date_wise_price_breakup']:
                        for sku_wise_price in date_wise_price_breakup['sku_wise_prices']:
                            room_night_sku_count += 1

                    for date_wise_price_breakup in room_price['date_wise_price_breakup']:
                        for sku_wise_price in date_wise_price_breakup['sku_wise_prices']:
                            sku_room_type_map[sku_wise_price['sku_code']] = room_type.lower()
                            sku_charge = dict(pretax_price=sku_wise_price['pre_tax_price'] - \
                                                      round(throttled_discount / room_night_sku_count, 2),
                                              sku_code=sku_wise_price['sku_code'],
                                              rate_plan=room_price['policy_name']
                                              )
                            sku_charges.append(sku_charge)

        request_price_per_sku = defaultdict(float)
        updated_sku_charge = []
        for sku_charge in sku_charges:
            request_price_per_sku[sku_charge['sku_code']] += sku_charge['pretax_price']
        is_sku_traversed = dict()

        for sku_charge in sku_charges:
            if not is_sku_traversed.get(sku_charge['sku_code']):
                updated_sku_charge.append(dict(sku_code=sku_charge['sku_code'],
                                               pretax_price=round(request_price_per_sku[sku_charge['sku_code']], 2),
                                               rate_plan=sku_charge['rate_plan']
                                          ))
                is_sku_traversed[sku_charge['sku_code']] = True

        if not updated_sku_charge:
            return price

        hotel_charges = dict(hotel_id=self.cs_hotel_id,
                             sku_charges=updated_sku_charge)

        tax_request['hotel_charges'] = [hotel_charges]
        tax_request['stay_start'] = self.checkin
        tax_request['stay_end'] = self.checkout
        backend_tax_response = SoaTaxBackend.get_tax(tax_request)
        hotel_tax_prices = backend_tax_response[self.cs_hotel_id]
        tax_response = defaultdict(list)
        for sku, sku_taxes in hotel_tax_prices.items():
            web_rate_plan_taxes = dict()
            for rate_plan_code, rate_plan_taxes in sku_taxes.items():
                web_rate_plan_code = self.get_web_rate_plan(
                    {'policy_name': rate_plan_code})['policy_name']
                web_rate_plan_taxes = {web_rate_plan_code: rate_plan_taxes}
            tax_response[sku_room_type_map[sku].lower()].append(web_rate_plan_taxes)

        for room_type, price_details in price['total_price'].items():
            for rate_plan in price_details['rate_plans']:
                treebo_discount_price_applicable = rate_plan['price']['wallet_applied'] and \
                                                   rate_plan['price']['wallet_deduction']
                if not treebo_discount_price_applicable:
                    continue
                rate_plan_code = rate_plan['rate_plan']['code']
                rate_plan['price']['treebo_points_discount'] = room_type_throttled_discount[room_type]
                total_tax = sum([room_type_price[rate_plan_code]['tax'] for
                                 room_type_price in tax_response[room_type]])
                pre_tax_price = sum([room_type_price[rate_plan_code]['pre_tax_price'] for
                                 room_type_price in tax_response[room_type]])
                post_tax_price = sum([room_type_price[rate_plan_code]['post_tax_price'] for
                                 room_type_price in tax_response[room_type]])
                rate_plan['price']['tax'] = total_tax
                rate_plan['price']['pretax_price'] = pre_tax_price
                rate_plan['price']['net_payable_amount'] = post_tax_price
                rate_plan['price']['wallet_applied'] = False
                rate_plan['price']['wallet_deduction'] = 0
        return price
