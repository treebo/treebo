import logging

import requests
from django.core.urlresolvers import reverse

from services.restclient.request_wrapper import wrap_internal_api_call

logger = logging.getLogger(__name__)


class BookingStashClient:
    @staticmethod
    def getCheapestRoomType(
            request,
            checkinDate,
            checkoutDate,
            hotelId,
            roomConfig):
        logger.info("Inside getCheapestRoom")
        url = request.build_absolute_uri(
            reverse('bstash:getcheapestroom') +
            '?checkin={0}&checkout={1}&hotelid={2}&roomconfig={3}'.format(
                checkinDate,
                checkoutDate,
                hotelId,
                roomConfig))

        response = wrap_internal_api_call(request, requests.get, url)
        # response = requests.get(url)
        data = None
        # print response.json()
        if response.status_code == 200 or response.status_code == 304:
            data = response.json()['data']
        returnValue = None
        if data is not None:
            returnValue = data['roomtype']

        return returnValue
