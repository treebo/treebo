# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2022-02-13 20:43
from __future__ import unicode_literals

import caching.base
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0100_image_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImagesPerCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('category', models.CharField(choices=[('Reception', 'Reception'), ('Lobby', 'Lobby'), ('Facade', 'Facade'), ('Nearby', 'Nearby'), ('Lift', 'Lift'), ('Room', 'Room'), ('Washroom', 'Washroom'), ('Restaurant', 'Restaurant'), ('Bar', 'Bar'), ('Banquet Hall', 'Banquet Hall'), ('Gym', 'Gym'), ('Pool', 'Pool'), ('Spa', 'Spa'), ('Roof Top Cafe', 'Roof Top Cafe'), ('Others', 'Others')], default='Others', max_length=20)),
                ('no_of_images', models.IntegerField(default=0)),
                ('hotel', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.DO_NOTHING, to='dbcommon.Hotel')),
            ],
            options={
                'abstract': False,
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AddField(
            model_name='image',
            name='image_callout',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='image',
            name='image_name',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='image',
            name='category',
            field=models.CharField(choices=[('Reception', 'Reception'), ('Lobby', 'Lobby'), ('Facade', 'Facade'), ('Nearby', 'Nearby'), ('Lift', 'Lift'), ('Room', 'Room'), ('Washroom', 'Washroom'), ('Restaurant', 'Restaurant'), ('Bar', 'Bar'), ('Banquet Hall', 'Banquet Hall'), ('Gym', 'Gym'), ('Pool', 'Pool'), ('Spa', 'Spa'), ('Roof Top Cafe', 'Roof Top Cafe'), ('Others', 'Others')], default='Others', max_length=20),
        ),
    ]
