# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0017_hotel_price_increment'),
        ('bookingstash', '0002_availability_hotelroommapper'),
    ]

    operations = [
        migrations.CreateModel(
            name='Availability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('availablerooms', models.PositiveIntegerField(default=0)),
                ('date', models.DateField()),
                ('room', models.ForeignKey(to='dbcommon.Room', on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
