# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0021_auto_20160601_1631'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='image',
            options={
                'ordering': [
                    '-is_showcased',
                    'position',
                    'hotel__name',
                    'room__room_type'],
                'verbose_name_plural': 'Images'},
        ),
    ]
