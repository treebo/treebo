from django.contrib import admin

from apps.seo.models.blog import Blog


@admin.register(Blog)
class BlogAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'title',
        'image_url',
        'blog_url',
        'get_city_name',
        'priority',
        'is_enabled')
    search_fields = ('title',)
    list_filter = ('city',)
    actions = ['delete_selected']

    def get_city_name(self, obj):
        if obj and obj.city:
            return str(obj.city.name)
        else:
            return None

    get_city_name.short_description = 'city'
