import sys
import traceback

import django

from apps.discounts.models import DiscountCoupon, DiscountCouponConstraints, ConstraintKeywords, \
    ConstraintOperators, \
    ConstraintExpression

django.setup()

from django.core.management import BaseCommand

from dbcommon.models.discounts import Coupon, Coupon_Constraint, Coupon_Date_Constraint


# def main():
# 	Command().handle()

class Command(BaseCommand):
    help = 'One time migrate discount script'
    pass

    def handle(self, *args, **options):
        self.__migrate()

    def __migrate(self):
        coupon_count = Coupon.objects.count()
        self.stdout.write("Total Coupon count = %d" % coupon_count)
        coupons = Coupon.objects.all()
        for coupon in coupons:
            mCoupon = False
            try:
                mCoupon = DiscountCoupon.objects.get(code__iexact=coupon.code)
            except DiscountCoupon.DoesNotExist:
                self.stdout.write("Good to migrate coupon =  %s" % coupon.code)

                try:
                    dCoupon = DiscountCoupon()

                    self.__populateCoupon(dCoupon, coupon)
                    self.__populateDiscountConstraints(dCoupon, coupon)
                except Exception as exc:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    self.stdout.write(
                        "{0:s}".format(
                            repr(
                                traceback.format_exception(
                                    exc_type,
                                    exc_obj,
                                    exc_tb))))

    def __populateCoupon(self, dCoupon, coupon):

        dCoupon.code = coupon.code

        if coupon.discount_operation == "%":
            dCoupon.discount_operation = DiscountCoupon.PERCENTAGE
        elif coupon.discount_operation == "-":
            dCoupon.discount_operation = DiscountCoupon.FIXED
        elif coupon.discount_operation == "night":
            dCoupon.discount_operation = DiscountCoupon.NIGHT
        elif coupon.discount_operation == "flatpernight":
            dCoupon.discount_operation = DiscountCoupon.FLATPERNIGHT

        dCoupon.discount_value = coupon.discount_value
        dCoupon.max_available_usages = coupon.max_available_usages
        dCoupon.max_available_discount = coupon.max_available_discount
        dCoupon.expiry = coupon.expiry
        dCoupon.is_active = coupon.is_active
        dCoupon.tagline = coupon.tagline
        dCoupon.terms = coupon.terms

        if coupon.discount_application == coupon.PRETAX:
            dCoupon.discount_application = DiscountCoupon.PRETAX
        elif coupon.discount_application == coupon.POSTTAX:
            dCoupon.discount_application = DiscountCoupon.POSTTAX

        if coupon.coupon_type == coupon.DISCOUNT:
            dCoupon.coupon_type = DiscountCoupon.DISCOUNT
        elif coupon.coupon_type == coupon.VOUCHER:
            dCoupon.coupon_type = DiscountCoupon.VOUCHER
        elif coupon.coupon_type == coupon.CASHBACK:
            dCoupon.coupon_type = DiscountCoupon.CASHBACK

        dCoupon.coupon_message = coupon.coupon_message

        dCoupon.save()

    def __populateDiscountConstraints(self, dCoupon, coupon):
        dConstraintExpression = ""

        # Min cart cost will form a constraint in the new discount design
        if coupon.min_cart_cost is not None and float(
                coupon.min_cart_cost) and float(
                coupon.min_cart_cost) > 0:
            cartCostConstraint = self.__saveCouponConstraint(
                dCoupon, "cartcost", 5, coupon.min_cart_cost)
            dConstraintExpression = self.__buildExpression(
                dConstraintExpression, cartCostConstraint)

        # Min cart NIGHT will form a constraint in the new discount design
        if coupon.min_cart_nights is not None and int(
                coupon.min_cart_nights) and int(
                coupon.min_cart_nights) > 0:
            cartNightConstraint = self.__saveCouponConstraint(
                dCoupon, "cartnight", 5, coupon.min_cart_nights)
            dConstraintExpression = self.__buildExpression(
                dConstraintExpression, cartNightConstraint)

        # Coupon_Constraints if available
        try:
            constraints = coupon.constraints.all()
            if constraints is not None and constraints.__len__() > 0:
                for constraint in constraints:
                    if constraint.type == "Hotel":
                        allHotels = constraint.hotels.all()
                        if allHotels.__len__() > 0:
                            hotelCsv = self.__buildHotelCsv(allHotels)
                            hotelConstraint = self.__saveCouponConstraint(
                                dCoupon, "hotel", 7, hotelCsv)
                            dConstraintExpression = self.__buildExpression(
                                dConstraintExpression, hotelConstraint)

                    elif constraint.type == "User":
                        allUsers = constraint.users.all()
                        if allUsers.__len__() > 0:
                            userCsv = self.__buildUserCsv(allUsers)
                            userConstraint = self.__saveCouponConstraint(
                                dCoupon, "user", 7, userCsv)
                            dConstraintExpression = self.__buildExpression(
                                dConstraintExpression, userConstraint)
        except Coupon_Constraint.DoesNotExist:
            self.stdout.write("Coupon has no constraints")
        except Exception as exc:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            self.stdout.write(
                "{0:s}".format(
                    repr(
                        traceback.format_exception(
                            exc_type,
                            exc_obj,
                            exc_tb))))

        # Coupon_Date_Constraint if available
        try:
            dateConstraints = coupon.date_constraints.all()
            if dateConstraints is not None and dateConstraints.__len__() > 0:
                for i, dateConstraint in enumerate(dateConstraints):
                    constraint = self.__saveCouponConstraint(
                        dCoupon, "checkin", 6, dateConstraint.start_date, dateConstraint.end_date)
                    dConstraintExpression = self.__buildExpression(
                        dConstraintExpression, constraint)
        except Coupon_Date_Constraint.DoesNotExist:
            self.stdout.write("Coupon has no date constraints")
        except Exception as exc:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            self.stdout.write(
                "{0:s}".format(
                    repr(
                        traceback.format_exception(
                            exc_type,
                            exc_obj,
                            exc_tb))))

        self.__saveContraintExpression(dConstraintExpression, dCoupon)

    def __saveCouponConstraint(
            self,
            dCoupon,
            keywordTxt,
            operatorId,
            valOne,
            valTwo=None):
        couponConstraint = DiscountCouponConstraints()
        keyword = ConstraintKeywords.objects.get(keyword__iexact=keywordTxt)
        operator = ConstraintOperators.objects.get(pk=operatorId)  # pk=5 is ge
        couponConstraint.coupon = dCoupon
        couponConstraint.keyword = keyword
        couponConstraint.operation = operator
        couponConstraint.value_one = valOne
        couponConstraint.value_two = valTwo
        couponConstraint.save()
        return couponConstraint

    def __buildExpression(self, dConstraintExpression, constraint):
        if dConstraintExpression is "":
            dConstraintExpression = str(constraint.id)
        else:
            dConstraintExpression = dConstraintExpression + \
                " AND " + str(constraint.id)
        return dConstraintExpression

    def __buildHotelCsv(self, hotels):
        hotelIdTxt = ""
        for hotel in hotels:
            if hotelIdTxt is "":
                hotelIdTxt = str(hotel.id)
            else:
                hotelIdTxt = hotelIdTxt + "," + str(hotel.id)

        return hotelIdTxt

    def __buildUserCsv(self, users):
        userIdTxt = ""
        for user in users:
            if userIdTxt is "":
                userIdTxt = str(user.id)
            else:
                userIdTxt = userIdTxt + "," + str(user.id)

        return userIdTxt

    def __saveContraintExpression(self, dExpression, dCoupon):
        if dExpression is not "":
            expression = ConstraintExpression()
            expression.expression = dExpression
            expression.coupon = dCoupon

            expression.save()

            # if __name__ == '__main__':
            # 	main()
