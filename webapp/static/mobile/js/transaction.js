'use strict';
import $ from "jquery";
import BaseView from "./treebolibs/baseview";
import queryString from "query-string";
import "whatwg-fetch";
import "../css/transaction.less";

let parsed = queryString.parse(location.search);

let TransactionView = Object.create(BaseView);
let progressId = 0;
let jobId = 0;
let leftOffset = 100;
let $bookingProgress = $("#bookingProgress");

let $info = $(".tp__info");
let $warning = $(".tp__warning");
let sourceType = parsed.type;

let $csrfMiddlewareToken = $('input[name="csrfmiddlewaretoken"]')
  .val();

let msgObj = {
  saveconfirm: {
    info: "We are almost done!",
    warning: "We are processing your request. Please wait for few moments.",
  },
  savebooking: {
    info: "Please wait while we redirect you to the secure payment page.",
    warning: "It may take upto a minute. Please don't refresh the page, or hit 'Back' or 'Close' button.",
  },
  confirmbooking: {
    info: "We are almost done!",
    warning: "Please wait while we confirm your booking. Kindly don't refresh the page, or hit 'Back' or 'Close' button.",
  }
};

let methods = {
  init: () => {
    methods.startProgressBar();
    if (rt.instant_booking) {
      methods.processInstantBooking();
    } else {
      methods.processSyncBooking();
    }
  },
  startProgressBar: () => {
    progressId = setInterval(() => {
      methods.frame("start");
    }, 200);
  },
  frame: (location) => {
    leftOffset = location === "end" ? 0 : leftOffset;
    let tx;
    if (leftOffset < 2) {
      clearInterval(progressId);
    } else {
      tx = `translate3d(-${leftOffset}%,0,0)`;
      leftOffset = leftOffset - 0.6;
      $bookingProgress.css("transform", tx);
    }

    if (leftOffset === 0) {
      $bookingProgress.css("transform", "translate3d(0%,0,0)");
    }
  },
  isPayAtHotel: function () {
    return rt.pay_at_hotel !== "0"
  },
  pay: function () {
    var options = {
      "key": rt.razorpay_key,
      "amount": rt.amount_to_send,
      "name": "Treebo Hotels",
      "description": rt.description,
      "image": "https://images.treebohotels.com/images/logo_mobile_payment.png",
      "handler": function (response) {
        $("#rpid").val(response.razorpay_payment_id);
        $("#checkoutformRazorpay").submit();
      },
      "prefill": {
        "name": rt.name,
        "email": rt.email,
        "contact": rt.contact
      },
      "notes": {
        "address": ""
      },
      "theme": {
        "color": "#0eb550"
      },
      "modal": {
        "ondismiss": function () {
          window.location.replace(rt.dismiss_url);
        }
      }
    };
    var rzp1 = new Razorpay(options);
    rzp1.open();
  },
  hasPaid: function () {
    return parsed.payment === "successful"
  },
  processInstantBooking: function () {
    if (!methods.isPayAtHotel()) {
      if (!methods.hasPaid()) {
        methods.pay();
      }
      else {
        // Will never reach here
      }
    }
    else {
      // Will never reach here
    }

  },
  processSyncBooking: function () {
    if (methods.isPayAtHotel()) {
      methods.runPolling();
    }
    else {
      if (!methods.hasPaid()) {
        methods.pay();
      }
      else {
        methods.runPolling();
      }
    }
  },
  pollForStatusCheck: function* statusCheck() {
    while (true) {
      yield fetch('/api/v1/checkout/booking/status/', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'X-CSRFToken': $csrfMiddlewareToken
        },
        body: JSON.stringify({
          reference_id: parsed.referenceid,
          csrfmiddlewaretoken: $csrfMiddlewareToken
        })
      })
        .then(r => r.json())
        .then(resp => // var json = d.json();
          resp.data);
    }
  },
  runPolling: (generator) => {
    if (!generator) {
      generator = methods.pollForStatusCheck();
    }

    setTimeout(() => {
      const p = generator.next();
      p.value.then(data => {
        if (data.jobstatus == "IN_PROGRESS" || data.jobstatus == "Tentative") {
          methods.runPolling(generator);
        } else if (data.jobstatus == "Confirm") {
          methods.frame("end");
          //after save booking redirect to payment getway page
          $("#redirectUrl").val(data.redirectUrl);
          $("#redirectForm").submit();

        } else {
          window.location = data.redirectUrl;
        }
      });
    }, 1000)
  },

};

TransactionView.pageLoaded = () => {
  methods.init();
};

let analyticsConfigfunc = function () {
  return {
    source: rt.source,
    referenceId: rt.referenceid,
    type: rt.type
  }
};
let transactionConfig = {
	name: '',
	widgets: [],
	analyticsConfigfunc
};


let landingView = TransactionView.setup(transactionConfig);
