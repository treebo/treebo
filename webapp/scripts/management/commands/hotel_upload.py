import os
import re
import zipfile

from django.core.management.base import BaseCommand
from django.db import transaction
from openpyxl import load_workbook

from data_services.respositories_factory import RepositoriesFactory
from . import config
from common.constants import common_constants
from dbcommon.models.direction import Directions
from dbcommon.models.facilities import Facility
from dbcommon.models.hotel import Hotel, Highlight
from dbcommon.models.location import Landmark, Locality, City, State
from dbcommon.models.miscellanous import Image
from dbcommon.models.room import Room


class Command(BaseCommand):
    file_path = os.path.dirname(os.path.realpath(__file__))
    help = "Make a hotel live"

    def add_arguments(self, parser):
        parser.add_argument('--file', action="store", dest="file",
                            help="The excel sheet containing the data")

    def handle(self, *args, **options):
        try:
            if options["file"] is None:
                print("No file supplied. Supply file using --file option")
            else:
                print("parsing file")
                self.read_excel(options["file"])

        except Exception as e:
            print(e)

    def parse_directions(self, directions):
        """
        Convert the directions into form that can be emailed
        :param directions:
        :return: The formatted directions
        """
        if re:
            split_directions = re.split(
                "\d+\.", self.strip_unicode_char(directions))
            return "\n".join([dir.strip()
                              for dir in split_directions if len(dir.strip()) > 0])
        else:
            return ""

    def strip_unicode_char(self, input_str):
        """
        replaces the windows left & right single quote with normal single quote
        :param input_str:
        :return:
        """
        if input_str is not None:
            return input_str.replace("\\u2018", "'").replace("\\u2019", "'")
        else:
            ""

    @transaction.atomic
    def read_excel(self, file_path):
        # hotel worksheet and create objects
        """
        Read excel sheet and extract information and save the hotel details in the db
        :return: tuple of rooms extracted from the sheet
        """
        workbook = load_workbook(filename=file_path, read_only=True)
        if workbook is None:
            print("No workbook found")
            raise Exception('No workbook found')
        hotel_sheet = workbook['Hotel']

        for idx, row in enumerate(hotel_sheet.rows):
            if idx == 0 or row[0].value is None:
                continue
            hotel = self.extract_hotel(row)
            facilities = self.extract_facilities(row, hotel)
            highlight = Highlight()
            highlight.description = row[10].value

        address_sheet = workbook['Address']
        for idx, row in enumerate(address_sheet.rows):
            if idx == 0 or row[0].value is None or row[0].value is not hotel.name:
                continue

            state = self.extract_state(row)
            hotel.state = state

            city = self.extract_city(row)

            hotel.city = city
            hotel.street = self.strip_unicode_char(row[8].value.strip())

            locality = self.extract_locality(city, hotel, row)
            if locality:
                hotel.locality = locality
            hotel.landmark = row[10].value if row[10].value else ''
            hotel.save()

            for facility in facilities:
                facility.hotels.add(hotel)
            print("saved facilities")

            highlight.hotel = hotel
            highlight.save()

            print("saved hotel")

            self.extract_landmark_directions(
                city, hotel, locality.latitude, locality.longitude, row)

            self.extract_airport_directions(city, hotel, row)

            self.extract_railway_directions(city, hotel, row)

        rooms = self.extract_room_info(hotel, workbook)
        self.download_images_from_drive(hotel.name, rooms)

    def extract_facilities(self, row, hotel):
        facilities = []
        if row[11].value.lower() == 'yes':
            facility = Facility.objects.get(name='Ironing Board')
            facilities.append(facility)
        if row[12].value.lower() == 'yes':
            facility = Facility.objects.get(name='Guest Laundry')
            facilities.append(facility)
        if row[13].value.lower() == 'yes':
            facility = Facility.objects.get(name='Gym')
            facilities.append(facility)
        if row[14].value.lower() == 'yes':
            facility = Facility.objects.get(name='Travel Desk')
            facilities.append(facility)
        if row[15].value.lower() == 'yes':
            facility = Facility.objects.get(name='Parking')
            facilities.append(facility)
        if row[16].value.lower() == 'yes':
            facility = Facility.objects.get(name='Business Event Hosting')
            facilities.append(facility)
        if row[17].value.lower() == 'yes':
            facility = Facility.objects.get(name='Restaurant On Premises')
            facilities.append(facility)
        if row[18].value.lower() == 'yes':
            facility = Facility.objects.get(name='Room Service')
            facilities.append(facility)
        print("extracted facilities")
        return facilities

    def extract_state(self, row):
        print("Extracting state")
        try:
            state = State.objects.get(name=row[6].value.strip())
        except State.DoesNotExist:
            state = State()
            state.name = row[6].value.strip()
            state.save()
            print("saved state")
        return state

    def extract_room_info(self, hotel, workbook):
        """
        Extract room info and save it in the db
        :param hotel: The hotel associated with these rooms
        :param workbook: The workbook that contains the data
        :return: tuple of rooms extracted from sheet
        """
        rooms_sheet = workbook['Rooms']
        rooms = []
        for idx, row in enumerate(rooms_sheet.rows):
            if idx == 0 or row[0].value is None:
                continue
            room = Room()
            room.hotel = hotel
            room.room_type = row[1].value.strip()
            room.room_type_code = row[2].value.strip()
            room.price = row[3].value
            room.quantity = row[4].value
            room.max_guest_allowed = row[5].value
            room.description = self.strip_unicode_char(row[6].value.strip())
            room.available = room.quantity
            room.base_rate = room.price
            room.save()
            print("saved room")
            rooms.append(room)
        return tuple(rooms)

    def extract_railway_directions(self, city, hotel, row):
        try:
            railway_landmark = Landmark.objects.get(name='Railway', city=city)
        except Landmark.DoesNotExist:
            railway_landmark = Landmark()
            railway_landmark.name = 'Railway'
            railway_landmark.description = 'City Railway Station'
            railway_landmark.status = common_constants.DISPLAY
            railway_landmark.city = city
            railway_landmark.save()
            print("saved railway landmark")

            railway_directions = Directions()
            railway_directions.hotel = hotel
            railway_directions.distance = float(row[17].value)
            railway_directions.landmark_obj = railway_landmark
            railway_directions.directions_for_email_website = self.parse_directions(
                row[18].value.strip())
            railway_directions.directions_for_sms = railway_directions.directions_for_email_website
            railway_directions.save()

            print("saved railway landmark directions")

            if railway_directions.distance < 7:
                facility = Facility.objects.get(
                    name='Easily Accessible from Railway Station')
                facility.hotels.add(hotel)
                facility.save()

    def extract_airport_directions(self, city, hotel, row):
        try:
            airport_landmark = Landmark.objects.get(name='Airport', city=city)
        except Landmark.DoesNotExist:
            airport_landmark = Landmark()
            airport_landmark.name = 'Airport'
            airport_landmark.description = 'City Airport'
            airport_landmark.status = common_constants.DISPLAY
            airport_landmark.city = city
            airport_landmark.save()
            print("saved airport landmark")

            airport_directions = Directions()
            airport_directions.hotel = hotel
            airport_directions.distance = float(row[15].value)
            airport_directions.landmark_obj = airport_landmark
            airport_directions.directions_for_email_website = self.parse_directions(
                row[16].value.strip())
            airport_directions.directions_for_sms = airport_directions.directions_for_email_website
            airport_directions.save()
            print("saved airport directions")

            if airport_directions.distance < 7:
                facility = Facility.objects.get(
                    name='Easily Accessible from Airport')
                facility.hotels.add(hotel)
                facility.save()

    def extract_landmark_directions(self, city, hotel, lat, lon, row):
        if not row[10].value:
            return
        else:
            try:
                landmark = Landmark.objects.get(
                    name=row[10].value.strip(), city=city)
            except Landmark.DoesNotExist:
                landmark = Landmark()
                landmark.name = hotel.landmark
                landmark.description = row[11].value.strip()
                landmark.latitude = lat
                landmark.longitude = lon
                landmark.status = common_constants.DISPLAY
                landmark.popularity = 0
                landmark.city = city
                landmark.save()
                print("saved landmark")

                landmark_directions = Directions()
                landmark_directions.hotel = hotel
                landmark_directions.distance = float(row[13].value)
                landmark_directions.landmark_obj = landmark
                landmark_directions.directions_for_email_website = self.parse_directions(
                    row[14].value.strip())
                landmark_directions.directions_for_sms = landmark_directions.directions_for_email_website
                landmark_directions.save()
                print("saved landmark directions")

    def extract_locality(self, city, hotel, row):
        try:
            locality = Locality.objects.get(
                name=row[7].value.strip(), city=city)
        except Locality.DoesNotExist:
            locality = Locality()
            locality.name = row[7].value.strip()
            locality.city = city
            locality.pincode = row[9].value
            locality.latitude = hotel.latitude
            locality.longitude = hotel.longitude
            locality.save()
            print("saved locality")
        return locality

    def extract_city(self, row):
        from data_services.city_repository import CityRepository, CityDoesNotExist
        try:
            city_data_service = RepositoriesFactory.get_city_repository()
            city = city_data_service.get_single_city(name=row[1].value)
            #city = City.objects.get(name=row[1].value)
        except CityDoesNotExist:
            city = City()
            city.name = row[1].value
            city.description = self.strip_unicode_char(row[2].value)
            city.tagline = self.strip_unicode_char(row[3].value)
            city.subscript = self.strip_unicode_char(row[4].value)
            city.status = common_constants.ENABLED
            lat_lang = row[5].value.split(",")
            city.city_latitude = float(lat_lang[0].strip())
            city.city_longitude = float(lat_lang[1].strip())
            # images left
            city.save()
            print("saved city")
        return city

    def extract_hotel(self, row):
        """
        Extract the hotel from the worksheet
        :param row:
        :return:
        """
        hotel = Hotel()
        hotel.name = row[2].value
        hotel.hotelogix_id = int(row[0].value)
        hotel.hotelogix_name = row[1].value
        hotel.tagline = self.strip_unicode_char(row[3].value)
        hotel.description = self.strip_unicode_char(row[4].value)
        hotel.phone_number = str(row[5].value)
        hotel.room_count = row[6].value
        hotel.checkin_time = row[7].value
        hotel.checkout_time = row[8].value
        hotel.price_increment = 300
        print("extracted hotel")
        return hotel

    def download_images_from_drive(self, hotel_name, rooms):
        """
        Download images from Google Drive
        """
        # print "downloading images from drive"
        # with open(Command.file_path + '/server_secret.json') as key_file:
        #     data = json.load(key_file)
        # credentials = SignedJwtAssertionCredentials(data["client_email"], data["private_key"],
        #                                             scope="https://www.googleapis.com/auth/drive")
        # http = Http()
        # credentials.authorize(http)

        # service = build('drive', 'v2', http=http)
        # param = dict()
        # param["q"] = "title='Hotel Images - Public' and sharedWithMe=true"

        # drive_folder = service.files().list(**param).execute()

        # param["q"] = "title='{0}.zip'".format(hotel_name)
        # hotel_zip = service.children().list(folderId=drive_folder["items"][0]["id"], **param).execute()

        # download_file_location = open(config.DOWNLOAD_FILE_LOCATION + hotel_name.replace(" ", "_") + ".zip", "w")
        # request = service.files().get_media(fileId=hotel_zip["items"][0]["id"])
        # media_request = googleapiclient.http.MediaIoBaseDownload(download_file_location, request)
        # while True:
        #     try:
        #         download_progress, done = media_request.next_chunk()
        #     except errors.HttpError, error:
        #         print 'An error occurred: %s' % error
        #         return
        #     if download_progress:
        #         print 'Download Progress: %d%%' % int(download_progress.progress() * 100)
        #     if done:
        #         print 'Download Complete'
        #         break
        # download_file_location.close()

        folder_name = self.unzip_hotel_images(hotel_name)
        self.upload_to_s3(folder_name, hotel_name, rooms)

    def unzip_hotel_images(self, hotel_name):
        """
        Unzip hotel images to a folder with same name as hotel
        :rtype : basestring
        :returns: Name of folder in which images were unzipped
        """
        print("unzipping file")
        folder_name = config.DOWNLOAD_FILE_LOCATION + hotel_name
        zip = zipfile.ZipFile(folder_name + ".zip", 'r')
        zip.extractall(folder_name)
        return folder_name

    def upload_to_s3(self, source_path, hotel_name, rooms):
        """
        Upload file to s3
        """
        print("uploading to S3")
        # conn_s3 = boto.connect_s3(config.AWS_ACCESS_KEY_ID, config.AWS_SECRET_ACCESS_KEY)
        # bucket = conn_s3.get_bucket(config.BUCKET_NAME)
        print("S3 connection established")
        for root, dirs, files in os.walk(source_path):
            for name in files:
                if name.endswith(".jpg"):
                    # bucket_key = Key(bucket)
                    path = hotel_name + "/" + name
                    path = path.replace(" ", "_")
                    # bucket_key.key = config.S3_STATIC_FILE_PATH + path
                    # bucket_key.set_contents_from_filename(root + "/" + name)
                    image = Image()
                    image.hotel = rooms[0].hotel
                    image.tagline = hotel_name
                    image.url = "./" + path
                    if len(dirs) > 0:
                        for room in rooms:
                            if room.room_type_code.lower() is str(
                                    dirs[0].lower()):
                                image.room = room
                                # image.room = next((room for room in rooms if room.room_type_code.lower() is str(dirs[0].lower())),
                                #                   None)
                    elif "showcase" in name:
                        image.is_showcased = True
                    image.save()
