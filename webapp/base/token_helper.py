from django.utils import timezone
import datetime
from datetime import timedelta

from apps.auth.treebo_auth_client import TreeboAuthClient
from dbcommon.models.profile import AccessToken
import logging

logger = logging.getLogger(__name__)


class TokenHelper(object):

    TOKEN_TYPE = 'Bearer'

    def get_token(self, request):
        auth = self.get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.authenticate_header().lower():
            return None
        if len(auth) == 1:
            return None
        elif len(auth) > 2:
            return None
        elif auth[1] == 'null' or auth[1] == 'true':
            return None
        return auth[1]

    def set_token(self, request, token):
        request.META['HTTP_AUTHORIZATION'] = 'Bearer ' + str(token)

    def authenticate_header(self):
        return self.TOKEN_TYPE

    def get_authorization_header(self, request):
        auth = request.META.get('HTTP_AUTHORIZATION', b'')
        if not auth:
            auth = request.COOKIES.get('token', b'')
        return auth

    def get_active_token_from_user(self, user):
        """
        :param (User) user:
        :return:
        """
        access_token = user.accesstoken_set.filter(
            expires__gte=timezone.now()).order_by('-expires').first()
        return access_token.token

    def save_access_token(self, token_info, user):
        """

        :param token_info:
        :param user:
        :return:
        """
        if not user or not token_info:
            return
        expires = timezone.now() + timedelta(seconds=token_info['expires_in'])
        token, refresh_token = token_info['access_token'], token_info['refresh_token']
        token_type, scope = token_info['token_type'], token_info['scope']
        user.token = token
        AccessToken.objects.get_or_create(
            auth_user=user,
            token=str(token),
            expires=expires,
            scope=str(scope),
            refresh_token=str(refresh_token),
            token_type=str(token_type))

    def refresh_and_update_token(self, token):
        access_token = AccessToken.objects.filter(token=str(token)).first()
        if not access_token:
            return None
        refresh_token = access_token.refresh_token
        user = access_token.auth_user
        token_info = TreeboAuthClient().call_refresh_token(refresh_token)
        self.update_token(access_token, user, token_info)
        return access_token.token

    def update_token(self, token_obj, user, token_info):
        expires = timezone.now() + timedelta(seconds=token_info['expires_in'])
        token, refresh_token = token_info['access_token'], token_info['refresh_token']
        token_type, scope = token_info['token_type'], token_info['scope']
        token_obj.auth_user = user
        token_obj.token = str(token)
        token_obj.expires = expires
        token_obj.scope = str(scope)
        token_obj.refresh_token = str(refresh_token)
        token_obj.token_type = str(token_type)
        token_obj.save()

    def revoke_all_tokens(self, token):
        try:
            accesstoken = AccessToken.objects.get(token=token)
        except AccessToken.DoesNotExist as e:
            return
        self.revoke_all_tokens_for_user(accesstoken.auth_user)

    def revoke_all_tokens_for_user(self, user):
        logger.info("revoke_user_token for user %s  ", user)
        AccessToken.objects.filter(auth_user=user).delete()

    def revoke_single_token(self, token):
        AccessToken.objects.filter(token=token).delete()
