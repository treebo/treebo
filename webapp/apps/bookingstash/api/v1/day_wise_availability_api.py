import logging
import traceback

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.bookingstash.dto.availability_dto import AvailabilityRequestDTO
from apps.bookingstash.service.availability_service import get_availability_service
from base.renderers import TreeboCustomJSONRenderer

logger = logging.getLogger(__name__)


class DayWiseAvailabilityAPI(APIView):
    renderer_classes = (TreeboCustomJSONRenderer,)

    def get(self, request, *args, **kwargs):
        """
        Fetches the availability in a date range for each room type for a list of hotels
        :param request:
        """
        request_data = request.query_params
        try:
            availability_dto = AvailabilityRequestDTO(data=request_data)
            if not availability_dto.is_valid():
                return Response(
                    availability_dto.errors,
                    status=status.HTTP_400_BAD_REQUEST)

            response_data = get_availability_service().get_day_wise_availability(
                availability_dto)
        except Exception as e:
            traceback.print_exc()
            logger.error(
                "error occurred while getting details for "
                "hotel {hotel_id} checkin {checkin}"
                "checkout {checkout} : {err}".format(
                    err=str(e),
                    hotel_id=request_data['hotels'],
                    checkin=request_data['checkin'],
                    checkout=request_data['checkout'],
                ))
            raise e
        return Response(data=response_data)
