import copy
import logging
from datetime import datetime, timedelta

from django.core.urlresolvers import reverse
from django.db.models import Min, Sum
from django.db.models.query_utils import Q
from pyquery import PyQuery as pQ

from apps.bookingstash.utils import StashUtils
from apps.common.exceptions.custom_exception import HotelNotFoundException, RoomNotAvailableException
from apps.hotels import utils as hotel_utils
from base import log_args
from common.constants import common_constants as constants, error_codes
from common.exceptions.treebo_exception import TreeboValidationException
from apps.common import date_time_utils
from common.services.feature_toggle_api import FeatureToggleAPI
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel, HotelAlias
from dbcommon.models.room import Room
from services import maps
from services.restclient.bookingrestclient import BookingClient
from django.conf import settings

logger = logging.getLogger(__name__)


class HotelService(object):

    @log_args(logger)
    def get_quickbook_url_dict_by_hotel(
            self,
            hotels,
            checkin_date,
            checkout_date,
            roomConfig,
            cheapest_room_types):
        roomConfigString = ','.join(
            ['-'.join(map(str, c)) for c in roomConfig])
        quickbookSuffix = "&checkin={0}&checkout={1}&roomconfig={2}&couponcode=".format(
            checkin_date.strftime(
                date_time_utils.DATE_FORMAT), checkout_date.strftime(
                date_time_utils.DATE_FORMAT), str(roomConfigString))
        quickbookUrlDict = {}
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        for hotel in hotels:
            rooms = hotel_repository.get_rooms_for_hotel(hotel)
            quickbook_url = reverse("pages:itinerary")

            if hotel.id in cheapest_room_types:
                cheapest_room_type = cheapest_room_types[hotel.id]['room_type']
                quickbookUrlDict[hotel.id] = quickbook_url + '?hotel_id=' + str(
                    hotel.id) + quickbookSuffix + '&roomtype=' + cheapest_room_type
            else:
                sorted(rooms, key=lambda room: room.price)
                quickbookUrlDict[hotel.id] = quickbook_url + '?hotel_id=' + \
                    str(hotel.id) + quickbookSuffix + '&roomtype='
        return quickbookUrlDict

    @log_args(logger)
    def get_cheapest_room_type(
            self,
            hotels,
            checkInDate,
            checkOutDate,
            roomConfig,
            request=None):
        logger.info(
            'Getting cheapest room type Using getCheapestRoomTypeFromAvailabilitySync')
        return StashUtils.get_cheapest_room_type_from_availability_sync(
            hotels, checkInDate, checkOutDate, roomConfig)

    def getAvailabilityAndPriceFromDb(
            self,
            request,
            hotels,
            checkInDate,
            checkOutDate):
        if checkInDate > checkOutDate:
            raise TreeboValidationException(
                "Check in date is greater than checkout date")

        delta = timedelta(days=1)
        # minRooms = constants.MIN_ROOMS_AVAILABLE

        MIN_VALUE = 1000000
        checkInDateCopy = copy.copy(checkInDate)
        checkOutDateCopy = copy.copy(checkOutDate)

        hotelsBookings = {}
        for hotel in hotels:
            hotelsBookings[hotel.id] = []

        while checkInDateCopy < checkOutDateCopy:
            jsonR = BookingClient.getHotelReservations(
                request,
                datetime.strftime(
                    checkInDateCopy,
                    '%Y-%m-%d %H:%M:%S'),
                datetime.strftime(
                    checkOutDateCopy,
                    '%Y-%m-%d %H:%M:%S'),
                constants.RESERVED,
                "room_unit",
                "hotel_id")
            if jsonR:
                for hotel_rooms in jsonR['data']:
                    if hotel_rooms['hotel_id'] is not None:
                        if hotel_rooms['hotel_id'] in list(
                                hotelsBookings.keys()):
                            hotelsBookings[hotel_rooms['hotel_id']].append(
                                hotel_rooms['room_sum'])
                        else:
                            hotelsBookings[hotel_rooms['hotel_id']] = [
                                hotel_rooms['room_sum']]
                checkInDateCopy += delta

        for hotel in hotels:
            hotel.availability = False
            hotel.minRate = MIN_VALUE  # Deliberate large values
            hotel.minRooms = MIN_VALUE
            hotel.baseRate = MIN_VALUE

            for room in hotel.rooms.all():
                if room.price < hotel.minRate:
                    hotel.minRate = room.price
                    hotel.baseRate = room.base_rate

            allBookings = hotelsBookings[hotel.id]
            if bool(allBookings):
                maxBooked = max(allBookings)
            else:
                maxBooked = 0
            minAvailable = hotel.room_count - maxBooked
            hotel.availability = True
            if minAvailable < 0:
                minAvailable = 0
            hotel.minRooms = minAvailable
            if minAvailable <= 0:
                hotel.availability = False

            if hotel.minRate == MIN_VALUE:
                hotel.minRate = "N/A"
                hotel.minRooms = None
                hotel.availability = False
                hotel.baseRate = "N/A"
        return hotels

    def __setMinAvailableRooms(self, hotelModel, minRooms, rooms):
        """
        Determine the number of rooms available at the hotel
        :param hotelModel:
        :param minRooms:
        :param rooms:
        """
        for room_pq in rooms:
            room = pQ(room_pq)
            if int(room.attr('availableroom') <= int(minRooms)):
                hotelModel.minRooms = int(room.attr('availableroom'))
                break

    def getLastBookingTime(self, request, hotel):
        """
        Get the last booking time for a hotel
        :param hotel: Hotel object
        """
        # fetch all books done in the last 3 hours
        lastBookingTime = BookingClient.getLastBookedTime(
            request, hotel.id, constants.LAST_BOOKING_TIME)
        if lastBookingTime:
            currentTime = datetime.now()
            hours, minutes = date_time_utils.get_time_diff(
                currentTime, lastBookingTime)
            timeString = date_time_utils.build_time_string(hours, minutes)
            return timeString

        return ""

    def __setHotelMinRate(self, checkIn, checkOut, hotelogixHotel, hotelModel):
        """
        Sets the minimum rate for the hotel
        :param checkIn:
        :param checkOut:
        :param hotelogixHotel: Hotel info returned by Hotelogix
        :param hotelModel: Django hotel model
        """
        if hotelogixHotel('minRate') is not None:
            rates = hotelogixHotel('rate')
            for rate_pq in rates:
                rate = pQ(rate_pq)
                if rate.attr("title").text().strip() == constants.PROMOTIONAL_WEB_PACKAGE or rate.attr(
                        "title").text().strip() == constants.WEB_PACKAGE:
                    hotelModel.availability = True
                    totalRate = float(rate.attr("price").text()) + \
                        float(rate.attr("tax").text())
                    if hotelModel.minRate == "N/A":
                        hotelModel.minRate = totalRate
                    elif hotelModel.minRate > totalRate:
                        hotelModel.minRate = totalRate

            days = (checkOut - checkIn).days
            if hotelModel.minRate != "N/A":
                hotelModel.minRate = round(float(hotelModel.minRate) / days)
            else:
                minPrice = hotelModel.rooms.all().aggregate(
                    Min('price'))['price__min']
                hotelModel.minRate = minPrice

    def getRoomAvailabilityAndPrice(
            self,
            hotel,
            checkInDate,
            checkOutDate,
            hotelDetailsXml):
        """
        Get the availability and price of rooms in the hotel. Enriches the model with isAvailable,
        hotelogixPrice, hotelogixRateId, availableRooms, maxPax
        :param hotel:
        :param checkInDate:
        :param checkOutDate:
        :param hotelDetailsXml:
        :return:
        """
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        rooms = hotel_repository.get_rooms_for_hotel(hotel)
        rooms = sorted(rooms, key=lambda room: room.price)
        hotelXml = pQ(hotelDetailsXml)
        roomTypes = hotelXml('roomtype')
        # TODO: Discuss what to do in this scenario
        # if len(roomTypes) is 0:
        #	raise ExternalServiceException("No room types found for hotel " + hotel.name + " in hotelogix")

        for hotelogixRoomPq in roomTypes:
            hotelogixRoom = pQ(hotelogixRoomPq)
            for i, room in enumerate(rooms):
                room.isAvailable = False
                room.availableRooms = 0

                if hotelogixRoom.attr(
                        "title").strip() == room.room_type.strip():

                    minRate, rateId, tax, availability = hotel_utils.getMinimumRate(
                        hotelogixRoom)
                    room.isAvailable = False
                    room.availableRooms = 0

                    if availability:
                        if hotel.status:
                            room.isAvailable = True
                        room.hotelogixPrice = round(
                            (minRate + tax) / (checkOutDate - checkInDate).days)
                        room.hotelogixRateId = rateId
                        room.availableRooms = int(
                            hotelogixRoom.attr("availableroom"))
                        room.maxPax = hotelogixRoom.attr("maxpax")
        return rooms

    def getRoomAvailabilityAndPriceFromBasicSearch(
            self, hotel, checkInDate, checkOutDate, hotelDetailsXml):
        """
        Get the availability and price of rooms in the hotel. Enriches the model with isAvailable,
        hotelogixPrice, hotelogixRateId, availableRooms, maxPax
        :param hotel:
        :param checkInDate:
        :param checkOutDate:
        :param hotelDetailsXml:
        :return:
        """
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        rooms = hotel_repository.get_rooms_for_hotel(hotel)
        rooms = sorted(rooms, key=lambda room: room.price)
        #rooms = hotel.rooms.all().order_by('price')
        hotelXml = pQ(hotelDetailsXml)
        roomTypes = hotelXml('roomtype')
        # TODO: Discuss what to do in this scenario
        # if len(roomTypes) is 0:
        #	raise ExternalServiceException("No room types found for hotel " + hotel.name + " in hotelogix")
        roomTypeMap = {}
        for hotelogixRoomPq in roomTypes:
            hotelogixRoom = pQ(hotelogixRoomPq)

            roomTypeMap[hotelogixRoom.attr("roomTypeName").strip()] = int(
                hotelogixRoom.attr("availableroom"))

        for singleRoom in rooms:
            singleRoom.availableRooms = roomTypeMap.get(
                singleRoom.room_type.strip(), 0)

        return rooms

    def get_room_types_for_hotel(self, hotel_id):
        if not hotel_id:
            raise HotelNotFoundException("hotel id %s cant be null" % hotel_id)
        room_repository = RepositoriesFactory.get_room_repository()
        room_types = room_repository.get_room_types_for_hotel(hotel_id=hotel_id)
        #rooms = room_repository.filter_rooms(hotel_id=hotel_id)
        # rooms = Room.objects.filter(hotel_id=hotel_id).values_list('room_type_code', flat=True)
        if not room_types:
            logger.error("No room types found for hotel %s" % hotel_id)
            raise RoomNotAvailableException(
                'rooms not found for hotel_id %s' % hotel_id)
        return room_types

    def getNearbyHotels(self, hotel):
        """
        Find the hotels near by to the passed hotel
        :param hotel:
        :return: list of hotels each with a distance attr
        :rtype list of Hotel
        """
        nearByHotels = Hotel.objects.filter(
            ~Q(
                id=hotel.id), Q(
                city=hotel.city.id), Q(
                status=1)).prefetch_related(
                    'rooms', 'image_set', 'hotel_highlights').select_related(
                        'state', 'locality', 'city')
        maps.Maps().getHotelDistance(
            nearByHotels,
            hotel.city,
            hotel.latitude,
            hotel.longitude,
            None)
        return nearByHotels

    def get_all_hotel_by_state(self, state):
        hotels = Hotel.objects.filter(state=state)
        return hotels

    def get_hotel_by_hotel_id(self, hotel_id):
        try:
            hotel = Hotel.objects.get(id=hotel_id)
            return hotel
        except Hotel.DoesNotExist:
            raise HotelNotFoundException(
                "Hotel (id = %d) not found" % hotel_id)

    def get_room_counts_by_hotel_ids(self, hotel_ids):
        try:
            hotel_rooms = Room.objects.filter(hotel_id__in=hotel_ids).all()
            return hotel_rooms
        except Exception as e:
            raise HotelNotFoundException(
                "Rooms not found for hotels " %
                str(hotel_ids))

    def get_total_rooms_by_hotel_ids(
            self,
            hotel_ids,
            max_adults,
            max_adults_with_children):
        try:
            hotels_rooms_summary = Room.objects.values('hotel').filter(
                hotel_id__in=hotel_ids,
                max_guest_allowed__gte=max_adults,
                max_adult_with_children__gte=max_adults_with_children) .annotate(
                total_room_count=Sum('quantity')).all()
            # transforming the summary result (list of dicts) to a map with
            # hotel_id as the key
            hotels_room_count_map = {}
            for hotel_rooms_count in hotels_rooms_summary:
                hotels_room_count_map[hotel_rooms_count['hotel']
                                      ] = hotel_rooms_count['total_room_count']

            return hotels_room_count_map
        except Room.DoesNotExist:
            raise HotelNotFoundException(
                "Rooms not found for hotels " %
                str(hotel_ids))

    @log_args()
    def get_hotel_details_using_cs_id(self, cs_id):
        try:
            hotel_detail = Hotel.objects.get(cs_id=cs_id)
        except Hotel.DoesNotExist as e:
            logger.error(e)
            raise HotelNotFoundException("hotel not found for cs_id {0}".format(str(cs_id)))
        return hotel_detail

    @staticmethod
    def get_display_name(hotel, hotel_alias=None):
        # TODO: Hits Hotel Alias query for every hotel. This method is called in for loop
        if not hotel_alias:
            hotel_aliases = hotel.hotel_alias.all()
            hotel_alias = hotel_aliases[0] if len(hotel_aliases) > 0 else None

        return hotel_alias.name if hotel_alias and hotel_alias.is_enabled else hotel.name

    @staticmethod
    def get_display_name_from_hotel_name(hotel_name):
        hotel = Hotel.objects.filter(name=hotel_name).first()
        return HotelService.get_display_name(hotel) if hotel else hotel_name

    def get_post_booking_display_name(self, hotel):

        name = hotel.name
        display_name = self.get_display_name(hotel)
        if display_name != name:
            name = name + " ({0})".format(display_name)

        return name

    def get_post_booking_display_name_from_hotel_name(self, hotel_name):

        name = hotel_name
        display_name = self.get_display_name_from_hotel_name(hotel_name)
        if display_name != hotel_name:
            name = name + " ({0})".format(display_name)

        return name


class HotelEnquiryValidator:
    def __init__(self, checkInDate, checkOutDate, roomsConfig):
        try:
            self.__checkInDate = checkInDate
            self.__checkOutDate = checkOutDate
            self.__roomsConfig = roomsConfig
        except Exception:
            raise TreeboValidationException(error_codes.INVALID_DATA)

    def validate(self):
        yesterday = datetime.now() - timedelta(days=1)
        if self.__checkInDate >= self.__checkOutDate:
            raise TreeboValidationException(error_codes.INVALID_DATES)
        elif self.__checkInDate < yesterday or self.__checkOutDate < yesterday:
            raise TreeboValidationException(
                error_codes.CHECK_DATE_GREATER_THAN_CURRENT_DATE)
        elif len(self.__roomsConfig) < 1:
            raise TreeboValidationException(error_codes.INVALID_ROOMS)
        else:
            for singleRoomConfig in self.__roomsConfig:
                # TODO: Temp change only. Review
                if int(singleRoomConfig[0]) < 1:
                    raise TreeboValidationException(
                        error_codes.INVALID_GUESTS_SUPPLIED)
                # TODO: Verify
                elif len(singleRoomConfig) > 1 and int(singleRoomConfig[2]) < 0:
                    raise TreeboValidationException(
                        error_codes.INVALID_GUESTS_SUPPLIED)
        return True
