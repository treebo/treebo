import logging

from django.db.models import Q

from apps.reviews.models import TAHotelIDMapping
from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class HotelTAMappingService(object):
    def create_or_update_mapping(self, hotel_id, ta_loc_id):
        try:
            hotel = Hotel.objects.get(
                Q(id=hotel_id) | Q(hotelogix_id=hotel_id))
            if hotel:
                ta_hotel_map = TAHotelIDMapping.objects.filter(
                    hotel=hotel).first()
                if not ta_hotel_map:
                    logger.info(
                        "Mapping between hotel %s and ta %s exists .. ",
                        ta_hotel_map.hotel.id,
                        ta_hotel_map.ta_location_id)
                    ta_hotel_map = TAHotelIDMapping()
                    ta_hotel_map.hotel = hotel
                    ta_hotel_map.ta_location_id = ta_loc_id
                    ta_hotel_map.save()
                elif ta_hotel_map.ta_location_id != ta_loc_id:
                    logger.info(
                        "Update the location ID for hotel %s", hotel_id)
                    ta_hotel_map.ta_location_id = ta_loc_id
                    ta_hotel_map.save()
                else:
                    logger.info(
                        "Entry already exists with same hotel id %s %s and mapped ta id %s %s",
                        hotel.id,
                        hotel_id,
                        ta_hotel_map.ta_location_id,
                        ta_loc_id)

            else:
                logger.error(
                    "No such hotel %s with status = ENABLED exists",
                    hotel_id)
        except Exception:
            logger.exception(
                "Exception in creating new entry in ta hotel mapping table for %s",
                hotel_id)

    def fetch_mapping(self, hotel_id=None, ta_loc_id=None):
        try:
            if hotel_id or ta_loc_id:
                ta_hotel_mapping = TAHotelIDMapping.objects.filter(
                    Q(hotel__id=hotel_id) | Q(ta_location_id=ta_loc_id)).first()
                if ta_hotel_mapping:
                    return ta_hotel_mapping
                else:
                    return None
        except Exception:
            logger.exception(
                "Exception in fetching the mapping of hotel id %s and ta id %s",
                hotel_id,
                ta_loc_id)
            return None

    def delete_mapping(self, hotel_id, ta_loc_id):
        try:
            TAHotelIDMapping.objects.filter(
                hotel__id=hotel_id, ta_location_id=ta_loc_id).delete()
            logger.info(
                "Deleted successfully for hotel %s and ta id %s",
                hotel_id,
                ta_loc_id)
        except Exception:
            logger.exception(
                "Exception in deleting hotel %s ta mapped object %s",
                hotel_id,
                ta_loc_id)
