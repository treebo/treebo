# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0037_auto_20160830_0443'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ApkDetails',
        ),
    ]
