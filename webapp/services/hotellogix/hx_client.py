import datetime
import logging

from pyquery import PyQuery as pQ

from apps.common import metrics_logger
from base import log_args
from common.constants import common_constants
from common.exceptions.treebo_exception import ExternalServiceException
from dbcommon.models.profile import UserMetaData
from services.hotellogix.web_services import Hotelogix

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)


class HxClient:
    @log_args(logger)
    def renewAuthToken(self, consumer_key, consumer_secret):
        try:
            xml = Hotelogix.wsauth(consumer_key)
        except Exception as exp:
            logger.exception("Exception raised in wsauth hx call")
            xml = common_constants.EMPTY_XML
        xml = self._callHotelogix(
            "wsauth", xml, None, consumer_key, consumer_secret)
        return Hotelogix.parseAuthResponse(xml)

    @log_args(logger)
    def changeAccessKeyXml(self, xml, accessKey):
        """
        :type xml: str
        :param xml:
        :param accessKey:
        """
        return xml.replace(Hotelogix.KEY_STRING, accessKey)

    @log_args(logger)
    def _callHotelogix(self, api, xml, hx_keys, consumer_key, consumer_secret):
        if api == "wsauth":
            st = datetime.datetime.now()
            signature = Hotelogix.createSignature(
                api, xml, "", consumer_secret)
            xml_response = Hotelogix.makeHotelogixCall(api, xml, signature)
            et = datetime.datetime.now()
            logger.debug("HX %s took %s ms" %
                         (api, int((et - st).total_seconds() * 1000)))
            metrics_logger.log(
                "HX API {api} took {seconds} ms", {
                    "api": api, "seconds": int(
                        (et - st).total_seconds() * 1000)})
            logger.info(
                "HX API {hx} took {seconds} ms", extra={
                    "hx": api, "seconds": int(
                        (et - st).total_seconds() * 1000)})
            return xml_response
        else:
            xml_response = None
            counter = common_constants.HOTELOGIX_RETRY_COUNT  # Retry 3 times
            while counter > 0:
                if isinstance(hx_keys["accesskey"], UserMetaData):
                    access_key, access_secret = hx_keys["accesskey"].value, hx_keys["accesssecret"].value
                    if not access_key:
                        self.__update_auth_parameters(
                            hx_keys, consumer_key, consumer_secret)
                        access_key, access_secret = hx_keys["accesskey"].value, hx_keys["accesssecret"].value
                else:
                    access_key, access_secret = hx_keys["accesskey"], hx_keys["accesssecret"]
                retry_count = (
                    common_constants.HOTELOGIX_RETRY_COUNT - counter) + 1

                if counter != common_constants.HOTELOGIX_RETRY_COUNT:
                    replacedXml = self.changeAccessKeyXml(xml, access_key)
                    signature = Hotelogix.createSignature(
                        api, replacedXml, access_secret, consumer_secret)

                logger.info(
                    "Calling hotelogix for %s, retry count = %s" %
                    (api, retry_count))
                metrics_logger.log(
                    "Retries for {api}, {retries}", {
                        "api": api, "retries": retry_count})
                logger.info(
                    "Retries for {hx}, {retries}", extra={
                        "hx": api, "retries": retry_count})

                replacedXml = self.changeAccessKeyXml(xml, access_key)
                signature = Hotelogix.createSignature(
                    api, replacedXml, access_secret, consumer_secret)
                try:
                    st = datetime.datetime.now()
                    xml_response = Hotelogix.makeHotelogixCall(
                        api, replacedXml, signature)
                    et = datetime.datetime.now()
                    metrics_logger.log(
                        "HX API {api} took {seconds} ms", {
                            "api": api, "seconds": int(
                                (et - st).total_seconds() * 1000)})
                    logger.info(
                        "HX API {hx} took {seconds} ms", extra={
                            "hx": api, "seconds": int(
                                (et - st).total_seconds() * 1000)})
                except ExternalServiceException:
                    logger.error("Hotelogix call failed")
                    success_flag = False
                    # appsCommonUtils.trackApiResponseTiming(request, st, api, "hotelogix","ApiError")
                    self.__update_auth_parameters(
                        hx_keys, consumer_key, consumer_secret)
                    counter -= 1
                    continue
                status_tags = xml_response('status')
                logger.debug("status tags = %s" % status_tags)
                success_flag = True
                for status_pq in status_tags:
                    status = pQ(status_pq)
                    message = status.attr("message")
                    if message and (message == Hotelogix.invalidAccessKey or message ==
                                    Hotelogix.invalidDataCart or Hotelogix.invalidSignature in message):
                        logger.debug("Hotelogix call failed")
                        success_flag = False
                        # appsCommonUtils.trackApiResponseTiming(request, api_call_start_time, api, "hotelogix","ApiError")
                        self.__update_auth_parameters(
                            hx_keys, consumer_key, consumer_secret)
                        break
                if success_flag:
                    # appsCommonUtils.trackApiResponseTiming(request, api_call_start_time, api, "hotelogix","ApiTiming")
                    break
                else:
                    # TODO: what to do when no. of retries is exausted - Amith
                    pass
                counter -= 1

            return xml_response

    @log_args(logger)
    def __update_auth_parameters(self, hx_keys, consumer_key, consumer_secret):

        accesskey, accesssecret = self.renewAuthToken(
            consumer_key, consumer_secret)

        if isinstance(hx_keys["accesskey"], UserMetaData):
            access_key_meta = hx_keys['accesskey']
            access_key_meta.value = accesskey
            access_key_meta.save()
            access_secret_meta = hx_keys['accesssecret']
            access_secret_meta.value = accesssecret
            access_secret_meta.save()
            hx_keys['accesskey'] = access_key_meta
            hx_keys['accesssecret'] = access_secret_meta
        else:
            hx_keys['accesskey'] = accesskey
            hx_keys['accesssecret'] = accesssecret
