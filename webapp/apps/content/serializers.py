from rest_framework import serializers

from apps.content.models import ContentStore


class ContentStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentStore
        fields = ('name', 'value')
