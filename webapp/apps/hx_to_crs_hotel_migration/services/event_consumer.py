import logging
from kombu.mixins import ConsumerMixin

from apps.common.exceptions.custom_exception import EmptyPayloadInCRSMigrationEventException, \
    BookingUpdateFailedInCRSIntegrationException
from apps.hx_to_crs_hotel_migration.services.message_processor import IntegrationEventMessageProcessor

logger = logging.getLogger(__name__)
message_processor = IntegrationEventMessageProcessor()


class IntegrationEventConsumer(ConsumerMixin):

    def __init__(self, connection, queues):
        self.connection = connection
        self.queues = queues

    def listen(self, message, message_meta):
        try:
            logger.info("CRS Migration :: Received message from CRS Integration event for exchange {0}, "
                        "for queue {1}, routing key {2}"
                        .format(message_meta.delivery_info['exchange'],
                                self.queues.name,
                                message_meta.delivery_info['routing_key']
                                )
                        )

            self.process_message(message)
            logger.info("CRS Migration :: Message processed from CRS Integration event for exchange {0}, "
                        "for queue {1}, routing key {2}"
                        .format(message_meta.delivery_info['exchange'],
                                self.queues.name,
                                message_meta.delivery_info['routing_key']
                                )
                        )
        except (EmptyPayloadInCRSMigrationEventException, BookingUpdateFailedInCRSIntegrationException, Exception) as e:
            logger.exception("CRS Migration :: ")
        finally:
            message_meta.ack()

    @staticmethod
    def process_message(message):
        logger.info("CRS Migration :: Processing message: {0}".format(message))

        message_processor.process(message)

    def get_consumers(self, consumer, channel):
        return [consumer(queues=self.queues,
                         callbacks=[self.listen])]
