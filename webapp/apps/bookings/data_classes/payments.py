# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
import uuid
from collections import namedtuple
from decimal import Decimal

from treebo_commons.money import Money

from apps.common.data_classes.base_data_class import BaseDataClass
from apps.common.data_classes.container import Container
from apps.common.date_time_utils import maybe_convert_string_to_datetime, is_naive_datetime, \
    application_tz_now, LocalTimeZoneObject
from apps.common.utils import quantize_and_round_off

PaymentMode = namedtuple('PaymentMode', ['name'])
PaymentStatus = namedtuple('PaymentStatus', ['name'])
PaymentType = namedtuple('PaymentType', ['Name'])

PrepaidPaymentMode = PaymentMode('Prepaid')
WalletPaymentMode = PaymentMode('Wallet')
PaymentOfferPaymentMode = PaymentMode('PaymentOffer')

ConfirmedPaymentStatus = PaymentStatus('confirmed')
CancelledPaymentStatus = PaymentStatus('cancelled')

NormalPaymentType = PaymentType('payment')
RefundPaymentType = PaymentType('refund')

PaymentModes = [
    PrepaidPaymentMode,
]

PaymentStatuses = [
    ConfirmedPaymentStatus,
    CancelledPaymentStatus
]


def payment_mode_from_string(value):
    try:
        return [mode for mode in PaymentModes if mode.name.lower() == value.lower()][0]
    except IndexError:
        raise ValueError("Invalid Payment mode: {v}".format(v=value))


def payment_status_from_string(value):
    try:
        return [status for status in PaymentStatuses if status.name.lower() == value.lower()][0]
    except IndexError:
        raise ValueError("Invalid Payment status: {v}".format(v=value))


class Payment(BaseDataClass):

    def __init__(self, uid, ref_id, date, amount, mode, status, type=None):
        self.uid = uid
        self.reference_id = ref_id
        _date = maybe_convert_string_to_datetime(date)
        if is_naive_datetime(_date):
            raise ValueError('Need timezone aware datetime')

        self.date = _date

        if isinstance(amount, Money):
            amount = amount.amount
        elif not isinstance(amount, Decimal):
            amount = Decimal(amount)

        self.amount = quantize_and_round_off(amount)

        self.mode = mode
        self.status = status
        self.type = type

    def __hash__(self):
        return hash(tuple([
            self.uid,
            self.reference_id,
            self.date.astimezone(LocalTimeZoneObject).replace(microsecond=0).isoformat(),
            self.amount,
            self.mode.name,
            self.status.name,
        ]))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __repr__(self):
        return "{u}.Payment - {r} - amount:{a} ({d}, {m}, {s})".format(
            u=self.uid,
            r=self.reference_id,
            a=self.amount,
            d=self.date.date(),
            m=self.mode.name,
            s=self.status.name,
        )

    def __str__(self):
        return "{u}.Payment - amount:{a} ({d}, {m}, {s})".format(
            u=self.uid,
            a=self.amount,
            d=self.date.date(),
            m=self.mode.name,
            s=self.status.name,
        )


class Payments(Container):
    pass


def pseudo_payment(amount, mode):
    uid = uuid.uuid4()
    _payment = Payment(
        uid=uid,
        ref_id="b2b_pseudo_{id}".format(id=uid),
        date=application_tz_now(),
        amount=amount,
        mode=mode,
        status=ConfirmedPaymentStatus,
    )
    return _payment
