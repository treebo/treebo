import csv
import logging

from django.utils.encoding import smart_str

from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from dbcommon.models.facilities import FacilityCategory, Facility
from data_services.exceptions import HotelDoesNotExist


logger = logging.getLogger(__name__)


class FacilitiesDataScript(object):
    def upload_hotel_facilities_mapping(self, ):
        with open('facilities_mapping.csv') as csvfile:
            file = csv.reader(csvfile, delimiter=',')
            for row in file:
                if row[0] != "Category":
                    try:
                        category = smart_str(row[0])
                        name = smart_str(row[1])
                        to_show = smart_str(row[2])
                        mapped_name = smart_str(row[3])
                        if to_show == "Y":
                            to_show = True
                        if to_show == "N":
                            to_show = False
                        logger.info(
                            "Creating facilities mapping for %s %s %s %s",
                            category,
                            name,
                            to_show,
                            mapped_name)
                        fac_category_obj, fac_category_create = FacilityCategory.objects.get_or_create(
                            name=category)
                        fac_obj, fac_create = Facility.objects.get_or_create(
                            name=name)
                        logger.info(" fac obj %s", fac_obj.name)
                        if mapped_name:
                            logger.info(
                                "mapped name %s is present", mapped_name)
                            fac_obj.catalog_mapped_name = mapped_name
                        fac_obj.to_be_shown = to_show
                        if fac_create:
                            fac_obj.url = "#"
                        logger.info("category added %s", fac_category_obj.name)
                        fac_obj.categories.add(fac_category_obj)
                        fac_obj.save()
                    except Exception:
                        logger.exception(
                            'Exception while updating facilities and their categories mapping from csv for entry %s',
                            row)

    def upload_other_facilities_mapping(self, ):
        with open('other_facilities.csv') as csvfile:
            file = csv.reader(csvfile, delimiter=',')
            facility_lcd = Facility.objects.get(
                catalog_mapped_name="lcd_projector")
            facility_audio = Facility.objects.get(
                catalog_mapped_name="audio_equipments")
            facility_meeting_room = Facility.objects.get(
                catalog_mapped_name="conference_hall")
            facility_power_backup = Facility.objects.get(
                catalog_mapped_name="24_hour_power_supply")
            for row in file:
                if row[2] != "Hotel":
                    try:
                        hotel_id = int(row[1])
                        hotel_name = smart_str(row[2])
                        lcd = smart_str(row[3]).lower()
                        audio = smart_str(row[4]).lower()
                        meeting_room = smart_str(row[5]).lower()
                        power_backup = smart_str(row[6]).lower()
                        hotel_obj = Hotel.objects.get(
                            name__contains=hotel_name, id=hotel_id)

                        logger.info(
                            "hotel obj name %s and csv hotel name %s",
                            hotel_obj.name,
                            hotel_name)
                        logger.info("row %s", row)
                        if "yes" in lcd or (
                            lcd and lcd not in [
                                "no",
                                "n",
                                "n/a"]):
                            facility_lcd.hotels.add(hotel_obj)
                            logger.info("adding lcd")
                            facility_lcd.save()
                        if "yes" in audio or (
                            audio and audio not in [
                                "no", "n", "n/a"]):
                            facility_audio.hotels.add(hotel_obj)
                            logger.info("adding audio")
                            facility_audio.save()
                        if "yes" in meeting_room or (
                            meeting_room and meeting_room not in [
                                "no", "n", "n/a"]):
                            facility_meeting_room.hotels.add(hotel_obj)
                            logger.info("adding meeting_room")
                            facility_meeting_room.save()
                        if "yes" in power_backup or (
                            power_backup and power_backup not in [
                                "no", "n", "n/a"]):
                            facility_power_backup.hotels.add(hotel_obj)
                            logger.info("adding power_backup")
                            facility_power_backup.save()
                    except HotelDoesNotExist:
                        logger.error("hotel %s doesnot exist ", row)
                    except Exception:
                        logger.exception(
                            'Exception while updating facilities and their categories mapping from csv for entry %s',
                            row)
