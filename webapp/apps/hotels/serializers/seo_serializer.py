from rest_framework import serializers

from dbcommon.models.hotel import SeoDetails

__author__ = 'Rajdeep'


class HotelSeoDetailsSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='titleDescription')
    description = serializers.CharField(source='metaContent')
    keyword = serializers.SerializerMethodField('getHotelKeyword')
    canonical_url = serializers.CharField(source='hotel_canonical_url')

    def getHotelKeyword(self, seo):
        return "Treebo hotels, budget hotels, budget hotels in india, affordable hotels, hotels in india, hotels booking, book hotels online, online hotels booking"

    class Meta:
        model = SeoDetails
        fields = ('title', 'description', 'keyword', 'canonical_url')
