from apps.seo.components.base_component import BaseComponent
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.serializers.serializer import PopularCitiesSerializer
from data_services.city_repository import CityRepository
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.location import City

__author__ = 'ansilkareem'


class HillStations(BaseComponent):

    city_data_service = RepositoriesFactory.get_city_repository()

    def __init__(self, current_city, footer_data):
        super(HillStations, self).__init__(current_city, footer_data, [])

    def build(self):
        self.exclude_list.insert(0, self.current_city.id)
        self.exclude_list = self.exclude_list[:self.count_cap]
        if self.current_city and self.current_city.enable_for_seo:
            hill_stations = self.city_data_service.get_popular_cities(
                popular_cities=self.footer_data['hill_stations'],
                city_exclude_list=self.exclude_list)
        else:
            hill_stations = []

        data = {'key': 'city',
                'title': 'HILL STATIONS',
                'content': AutoCompleteResultBuilder.build_city_result(hill_stations)
                } if self.footer_data else {'key': 'city', 'title': 'HILL STATIONS', 'content': []
                                            }
        serialized_obj = PopularCitiesSerializer(data=data)
        if serialized_obj.is_valid():
            return serialized_obj.validated_data
        else:
            return {'key': 'city', 'title': 'HILL STATIONS', 'content': []
                    }
