import logging
from datetime import datetime, timedelta

from django.conf import settings

from apps.discounts import constants
from .discount_manager import DiscountManager
from .models import DiscountCoupon, DiscountCouponConstraints, ConstraintExpression, ConstraintOperators, \
    ConstraintKeywords

logger = logging.getLogger(__name__)


def isAutoPromo(couponCode):
    logger.info("Inside isAutoPromo")
    isAuto = False
    try:
        coupon = DiscountCoupon.objects.get(code=couponCode)
        if coupon.is_auto_applicable:
            isAuto = True
    except DiscountCoupon.DoesNotExist:
        pass

    return isAuto


def getAutoPromos(**kwargs):
    booking_date = kwargs['booking_date'] if kwargs.get(
        'booking_date', None) else None
    rooms = kwargs['rooms'] if kwargs.get('rooms', None) else None

    logger.info("+++++++++++++++++++++")
    logger.info("booking date type %s " % type(booking_date))
    logger.info("booking date = %s" % booking_date)
    logger.info("+++++++++++++++++++++")

    coupons = DiscountCoupon.objects.filter(
        is_active=True,
        expiry__gte=booking_date,
        start_date__lte=booking_date,
        max_available_usages__gt=0,
        is_auto_applicable=True)

    context = {}
    if len(coupons) > 0:
        max_discount = 0
        code = ""
        for coupon in coupons:
            kwargs['code'] = coupon.code
            discountManager = DiscountManager(**kwargs)
            coupon, message = discountManager.validate()
            if message == constants.SUCCESS:
                discount = discountManager.apply(coupon, rooms)
                coupon_code, message, discountType, terms = coupon.code, coupon.coupon_message,\
                    coupon.coupon_type, coupon.terms
                if discount > max_discount:
                    max_discount = discount
                    code = coupon_code
                logger.debug(
                    code +
                    " " +
                    str(max_discount) +
                    " " +
                    discountType +
                    " " +
                    terms)
                context = {
                    "coupon_code": code,
                    'discount': max_discount,
                    'code': 200,
                    'message': constants.DISCOUNT_APPLIED,
                    'terms': terms,
                    'coupon_message': message,
                    'coupon_type': discountType}
    else:
        logger.debug("No Discount Coupons Available")
        context = {
            "status": constants.FAILURE,
            "code": 400,
            "msg": "No Discount Coupons Available"}
    return context


def parseRequest(request):
    """
    Parse all request params
    :type request: GET
    """
    params = {'code': request.GET['code'] if 'code' in request.GET else "",
              'checkin': request.GET[
                  'checkin'] if 'checkin' in request.GET else datetime.now().strftime("%Y-%m-%d"),
              'checkout': request.GET['checkout'] if 'checkout' in request.GET else (
                  datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d"),
              'hotel_id': request.GET['hotel_id'] if 'hotel_id' in request.GET else 0,
              'user': request.GET['user'] if 'user' in request.GET else 0,
              'no_bookings': request.GET['no_bookings'] if 'no_bookings' in request.GET else 0,
              'rooms': request.GET['rooms'] if 'rooms' in request.GET else 1,
              'booking_date': request.GET[
                  "booking_date"] if 'booking_date' in request.GET else datetime.now().date(),
              'totalcost': request.GET['total_cost'] if 'total_cost' in request.GET else 0,
              'total_pretax_cost': request.GET[
                  'total_pretax_cost'] if 'total_pretax_cost' in request.GET else 0,
              'paytype': request.GET['paytype'] if 'paytype' in request.GET else "",
              'days': request.GET['days'] if 'days' in request.GET else "",
              'weeks': request.GET['weeks'] if 'weeks' in request.GET else "",
              'months': request.GET['months'] if 'months' in request.GET else "",
              'corporate': request.GET['corporate'] if 'corporate' in request.GET else "",
              'nights': request.GET['nights'] if 'nights' in request.GET else 1,
              'room_config': request.GET['room_config'] if 'room_config' in request.GET else ""}

    utm_source = request.COOKIES.get('utm_source', '')
    if utm_source:
        params['utm_source'] = utm_source[0] if isinstance(
            utm_source, list) else utm_source

    return params


def createUniqueCouponCode(user, credits_redeem):
    firstName = user.first_name
    lastName = user.last_name
    email = user.email
    prefixString = str(firstName) + str(lastName) + str(email)
    prefixString = prefixString.strip()
    prefixString = prefixString.upper()
    prefix = prefixString
    if len(prefixString) > 9:
        prefix = prefixString[:9]
    suffix = str(credits_redeem)
    allPrefixesCount = DiscountCouponConstraints.objects.filter(
        coupon__code__startswith=prefix).count()
    discountCount = DiscountCoupon.objects.filter(
        code__startswith=prefix).count()
    allPrefixesCount += discountCount
    if allPrefixesCount > 0:
        couponCount = allPrefixesCount - 1
        prefixSuffix = baseN(couponCount, 26)
        prefix += str(prefixSuffix)
        prefix = prefix.upper()
    return prefix + suffix[0]


def createCouponForUser(user, code, credits_redeem, expiry):
    userDiscount = DiscountCoupon(
        code=code,
        discount_operation='FOTREDEEM',
        discount_value=credits_redeem,
        max_available_usages=1,
        max_available_discount=100000,
        expiry=expiry,
        is_active=True,
        discount_application=1,
        coupon_type=DiscountCoupon.VOUCHER,
        is_internal=False)
    userDiscount.save()

    user_keyword_id = ConstraintKeywords.objects.get(keyword='user').id
    user_operation_id = ConstraintOperators.objects.get(operator='in').id
    userDiscountConstraint = DiscountCouponConstraints(
        coupon=userDiscount,
        keyword_id=user_keyword_id,
        operation_id=user_operation_id,
        value_one=user.id)
    userDiscountConstraint.save()

    checkin_keyword_id = ConstraintKeywords.objects.get(keyword='checkin').id
    checkin_operation_id = ConstraintOperators.objects.get(
        operator='exclude_range').id
    firstDateRangeDiscountConstraint = DiscountCouponConstraints(
        coupon=userDiscount,
        keyword_id=checkin_keyword_id,
        operation_id=checkin_operation_id,
        value_one=settings.FIRST_BLOCKOUT_RANGE_START,
        value_two=settings.FIRST_BLOCKOUT_RANGE_END)
    firstDateRangeDiscountConstraint.save()

    secondDateRangeDiscountConstraint = DiscountCouponConstraints(
        coupon=userDiscount,
        keyword_id=checkin_keyword_id,
        operation_id=checkin_operation_id,
        value_one=settings.SECOND_BLOCKOUT_RANGE_START,
        value_two=settings.SECOND_BLOCKOUT_RANGE_END)
    secondDateRangeDiscountConstraint.save()

    logger.info(
        'Constraint %s, %s and %s added for coupon %s',
        userDiscountConstraint.id,
        firstDateRangeDiscountConstraint.id,
        secondDateRangeDiscountConstraint.id,
        userDiscount.id)
    discountExpression = ConstraintExpression(coupon=userDiscount,
                                              expression=str(userDiscountConstraint.id) + ' AND ' +
                                              str(firstDateRangeDiscountConstraint.id) + ' AND ' +
                                              str(secondDateRangeDiscountConstraint.id))
    discountExpression.save()

    logger.info('ConstraintExpression %s added', discountExpression.id)
    return userDiscount


def baseN(num, b, numerals="abcdefghijklmnopqrstuvwxyz"):
    return ((num == 0) and numerals[0]) or (
        baseN(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])
