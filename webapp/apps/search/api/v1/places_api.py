from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from apps.search.services.places_search_service import PlacesSearchService
from base.views.api import TreeboAPIView


class PlacesAPI(TreeboAPIView):

    def get(self, request):

        try:
            place_id = request.GET.get('place_id')
            session_token = request.GET.get('session_token', None)
            place_location = PlacesSearchService().get_place_location_details(place_id, session_token)
            return Response(data={'place_id': place_id,
                                  'lat': place_location.get('geo_location').get('lat'),
                                  'lng': place_location.get('geo_location').get('lng'),
                                  'address_components': place_location.get('address_components'),
                                  'session_token': session_token})

        except ValidationError as e:
            return Response(status=e.status_code, data={"errors": e.detail})

        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={"errors": e.__str__()})
