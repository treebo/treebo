import logging
from django.conf import settings
from django.core.cache import cache
from django.db.models import Q
from django.utils.text import slugify

from apps.content.constants import PAGES
from apps.posts.service import PostService
from apps.seo.components.breadcrumb import BreadCrumb
from apps.seo.components.categories import Category
from apps.seo.components.cities_nearby import CitiesNearby
from apps.seo.components.city_coordinate_component import CityCoordinateComponent
from apps.seo.components.left_nav import LeftNav
from apps.seo.components.popular_cities import PopularCities
from apps.seo.components.top_reviews import TopReviews
from apps.seo.components.weather_widget import WeatherWidget
from apps.seo.pages.pages import PAGES
from dbcommon.models.location import Locality, LocalitySeoDetails
from services.restclient.contentservicerestclient import ContentServiceClient
from apps.seo.components.hotel_category_in_location import HotelCategoryInLocation
from apps.seo.services.common_services.check_city_name_repetition_in_area_name import \
    CheckCityNameRepetitionInAreaName

__author__ = 'ansilkareem'
logger = logging.getLogger(__name__)


class LocalityPage(PAGES):

    def __init__(self, page, query, is_amp):
        self.is_amp = is_amp
        city = query.split('-')[-1]
        self.locality_exclude_list = []
        locality_name = ' '.join(query.split('-')[:-1])
        self.locality = Locality.objects.filter(Q(name__iexact=locality_name) & Q(
            Q(city__name__iexact=city) | Q(city__slug__iexact=city))).first()
        self.city = self.locality.city
        self.locality_exclude_list.append(self.locality.id)
        self.about_key = 'about_' + '_'.join(locality_name.lower().split()) + '_' + '_'.join(
            self.city.name.lower().split())
        self.description = ContentServiceClient.getValueForKey(None, self.about_key, 1)

        super(LocalityPage, self).__init__(page, query, locality_name)

    def update_meta(self):
        min_price_key = str(self.city.name) + '_min_price'
        min_sell_price_key = str(self.city.name) + '_min_sell_price'
        min_rack_rate_key = str(self.city.name) + '_min_rack_rate'
        max_discount_key = str(self.city.name) + '_max_discount'
        min_price = cache.get(min_price_key)
        min_sell_price = cache.get(min_sell_price_key)
        min_rack_rate = cache.get(min_rack_rate_key)
        max_discount = cache.get(max_discount_key)
        if max_discount:
            total_discount = str(max_discount) + "%"
        else:
            total_discount = str(settings.DEFAULT_DISCOUNT) + "%"
        temp_dict = {
            "locality_name": self.locality.name,
            "city_name": self.locality.city.slug.title() if self.locality.city.slug else self.locality.city.name.title(),
            "price": str(min_price) if min_price else str(settings.DEFAULT_PRICE),
            "sell_price": str(min_sell_price) if min_sell_price else str(
                settings.DEFAULT_SELL_PRICE),
            "rack_rate": str(min_rack_rate) if min_rack_rate else str(settings.DEFAULT_RACK_RATE),
            "total_discount": total_discount
        }
        locality_seo_details = LocalitySeoDetails.objects.filter(locality=self.locality).first()
        if self.locality.free_text_url:
            full_canonical_url = settings.SITE_HOST_NAME + 'hotels-in-' + slugify(
                self.locality.free_text_url) + '/'
        else:
            full_canonical_url = settings.SITE_HOST_NAME + 'hotels-in-' + slugify(
                self.locality.name) \
                                 + '-' + slugify(self.city.slug) + '/'
        amp_url = full_canonical_url + 'amp/'
        if locality_seo_details:
            title = locality_seo_details.locality_seo_title % temp_dict
            description = locality_seo_details.locality_seo_description % temp_dict
            self.override_seo_content(title,
                                      description,
                                      locality_seo_details.locality_seo_keyword,
                                      full_canonical_url, self.locality.name, is_amp=self.is_amp,
                                      amp_url=amp_url)
        else:
            title = None
            description = None
            if CheckCityNameRepetitionInAreaName(temp_dict['locality_name'],
                                                 self.city.name).check_substring():
                temp_dict['city_name'] = ''
            if 'locality_title' in self.footer_data:
                title = self.footer_data['locality_title'] % temp_dict
            if 'locality_description' in self.footer_data:
                description = self.footer_data['locality_description'] % temp_dict
            if title and description:
                self.override_seo_content(title,
                                          description,
                                          None,
                                          full_canonical_url, is_amp=self.is_amp, amp_url=amp_url)

    def build_meta(self, *args):
        for arg in args:
            ta_review_count = arg.get('ta_review_count')
        self.update_meta()
        temp_dict = {
            "locality_name": self.locality.name.upper(),
            "city_name": self.locality.city.name.upper()
        }
        if 'locality_about_title' in self.footer_data:
            about_title = self.footer_data['locality_about_title'] % temp_dict
        else:
            about_title = ''
        self.meta_content['content'] = {
            'description': {'title': about_title.title(),
                            'content': self.description['text']
                            } if self.description else {}
        }
        min_price_key = str(self.city.name) + '_min_sell_price'
        min_price_value = cache.get(min_price_key)
        self.meta_content['content'].update(
            {'min_price': min_price_value if min_price_value else settings.DEFAULT_SELL_PRICE})
        top_reviews = TopReviews(self.city, self.footer_data, ta_review_count, [])
        self.meta_content['content'].update(top_reviews.
                                            build_reviews_with_area_radius(page=PAGES.LOCALITY,
                                                                           city_name=self.city.name,
                                                                           latitude=self.locality.latitude,
                                                                           longitude=self.locality.longitude,
                                                                           distance_cap=self.locality.distance_cap,
                                                                           locality_name=self.locality.name))
        left_nav = LeftNav(self.city, self.footer_data, [])
        self.meta_content['content'].update(left_nav.build())
        self.meta_content['content'].update({
            'city_image': self.city.seo_image if self.city.seo_image else self.city_image_url
        })
        cities_nearby = CitiesNearby(self.city, self.footer_data)
        self.meta_content['content'].update({
            'links': [cities_nearby.build()],
            'breadcrumbs': BreadCrumb(self.city, self.footer_data, self.page,
                                      self.item_name).build()
        })

        # seo ph2
        area = {}
        area['page'] = self.page
        area['identifier'] = self.locality
        weather_widget = WeatherWidget(area, self.footer_data)
        self.meta_content['content'].update(weather_widget.build())
        city_coordinate = CityCoordinateComponent(area, self.footer_data)
        self.meta_content['content'].update(city_coordinate.build())
        popular_cities = PopularCities(
            self.city, self.footer_data, [
                city.id for city in cities_nearby.sorted_nearby_cities])
        self.meta_content['content']['links'].append(popular_cities.build())
        category = Category(self.city, self.footer_data, [])
        self.meta_content['content']['links'].append(category.build())

        if self.locality.name and self.city:
            hotel_category_locality_mapping = HotelCategoryInLocation(self.locality)
            self.meta_content['content']['links'].append(hotel_category_locality_mapping.build())

    @staticmethod
    def delete_page_meta_cache(slugify_locality_name, slugify_city):
        logger.info(
            'deleting locality meta cache : ' + 'meta_locality_' + slugify_locality_name + '-' + slugify_city + '_v3')
        cache.delete('meta_locality_' + slugify_locality_name + '-' + slugify_city + '_v3')
