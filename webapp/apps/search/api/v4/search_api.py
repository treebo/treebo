import logging

from django.conf import settings
from django.core.cache import cache
from rest_framework import status
from rest_framework.response import Response

from apps.bookingstash.service.availability_service import ITSAvailabilityService
from apps.common.api_response import APIResponse
from apps.common.exceptions.custom_exception import InvalidBookingDates, NoHotelsFoundException
from apps.common.slack_alert import SlackAlertService
from apps.hotels.repositories.hotels_repository import HotelsRepository
from apps.hotels.serializers.image_serializer import ImageSerializer
from apps.hotels.service.hotel_policy_service import HotelPolicyService
from apps.hotels.service.hotel_service import HotelService
from apps.hotels.service.hotel_sub_brand_service import HotelSubBrandService
from apps.pricing.services.pricing_sorting_service import PricingSortingService
from apps.reviews.cache.cache_service import CacheService
from apps.reviews.cache.page_config import PageConfig
from apps.reviews.models import ReviewAppConfiguration
from apps.reviews.services.trip_advisor.review_config_service import ReviewAppConfigService
from apps.search.constants import DIRECT, GDC, DEFAULT_SEARCH_CHANNEL
from apps.search.dto.hotel_serarch_dto import HotelSearchQuery
from apps.search.serializers import SearchSerializer
from apps.search.services.categories.categories_services import CategoriesServices
from apps.search.services.search_v2 import SearchService
from apps.search.services.sku_search_cache_service import SkuSearchCacheService
from base import log_args
from base.decorator.request_validator import validate_request
from base.views.api import TreeboAPIView
from common.services.feature_toggle_api import FeatureToggleAPI
from conf.gdc import TREEBO_BASE_HOST_URL as GDC_BASE_HOST_URL
from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)


class SearchResult(object):
    def __init__(self, hotels):
        self.hotels = hotels

    @property
    def hotel_ids(self):
        return [hotel.id for hotel in self.hotels]

    @property
    def hotel_web_cs_id_dict(self):
        return {hotel.id: hotel.cs_id for hotel in self.hotels}

    def filter_hotel(self, hotel_id):
        for hotel in self.hotels:
            if hotel.id == hotel_id:
                return hotel


class SearchApiV4(TreeboAPIView):
    CACHE_TIMEOUT = 24 * 60 * 60
    GDC_CACHE_KEY = 'apicache_searchpage_gdc_0_jank_{0}'
    DIRECT_CACHE_KEY = 'apicache_searchpage_0_jank_{0}'
    validationSerializer = SearchSerializer
    search_landmark_repository = RepositoriesFactory.get_landmark_repository()

    @staticmethod
    def invalidate_cache():
        cache.delete_pattern(SearchApiV4.DIRECT_CACHE_KEY.format('*'))
        cache.delete_pattern(SearchApiV4.GDC_CACHE_KEY.format('*'))

    @staticmethod
    def get_cache_key(request_param, search_dto):
        key = "hotel_id_{0}_locality_name_{1}_city_name_{2}_state_name_{3}_country_name_{4}_" \
              "location_type_{5}_nearby_{6}_landmark_name_{7}_distance_cap_{8}_category_{9}_" \
              "amenity_{10}_meta_key_{11}_latitude_{12}_longitude_{13}_radius_{14}_checkin_{15}_" \
              "checkout_{16}_room_config_{17}_meta_hotel_id_{18}" \
            .format(request_param.get('hotel_id', ''),
                    request_param.get('locality', ''),
                    request_param.get('city', ''),
                    request_param.get('state', ''),
                    request_param.get('country', ''),
                    request_param.get('location_type', ''),
                    request_param.get('nearby', False),
                    request_param.get('landmark', ''),
                    request_param.get('distance_cap', 0),
                    request_param.get('category', ''),
                    request_param.get('amenity', ''),
                    request_param.get('meta_key', ''),
                    request_param.get('latitude', 0),
                    request_param.get('longitude', 0),
                    request_param.get('radius', 0),
                    request_param.get('checkin', ''),
                    request_param.get('checkout', ''),
                    request_param.get('roomconfig', ''),
                    request_param.get('meta_hotel_id', ''))

        key = key.replace(" ", "_")

        if search_dto.search_channel == GDC:
            return SearchApiV4.GDC_CACHE_KEY.format(key)
        else:
            return SearchApiV4.DIRECT_CACHE_KEY.format(key)

    @staticmethod
    def make_response(hotels, category_dict, sort_result, search_type):
        result = []
        response = {
            'sort': {
                'by': search_type
            },
            'result': []
        }

        policies_all_hotels = HotelPolicyService.get_all_hotel_policies(hotels)
        brand_list_all_hotels = HotelSubBrandService.get_brands_for_hotels(
            [hotel.id for hotel in hotels])
        hotel_wise_alias = HotelsRepository().get_hotel_aliases([hotel.id for hotel in hotels])

        for hotel in hotels:
            item = {
                'id': hotel.id,
                'cs_id': hotel.cs_id,
                'sort_index': sort_result[hotel.id]['sort_index'],
                'available_room_types': sort_result[hotel.id]['available_room_types'],
                'distance': round(hotel.distance) if hotel.distance else None,
                'hotelName': HotelService().get_display_name(hotel, hotel_wise_alias.get(hotel.id)),
                'is_active': True if hotel.status else False,
                'area': {
                    'pincode': hotel.locality.pincode,
                    'locality': hotel.locality.name,
                    'city': hotel.city.name,
                    'state': hotel.state.name,
                    'country': 'India'
                },
                'amenities': [facility.id for facility in hotel.facility_set.all()],
                'images': ImageSerializer(instance=hotel.get_categorised_images()[:10], many=True).data,
                'coordinates': {
                    'lat': hotel.latitude,
                    'lng': hotel.longitude
                },
                'hotel_policies': policies_all_hotels.get(hotel.id, []),
                'isFrequentlyBooked': category_dict[hotel.hotelogix_id]['is_frequently_booked'],
                'isValueForMoney': category_dict[hotel.hotelogix_id]['is_value_for_money'],
                'isFourPlusRated': category_dict[hotel.hotelogix_id]['is_four_plus_rated'],
                'property': {
                    'provider': hotel.provider_name,
                    'type': hotel.property_type
                },
                'brands': brand_list_all_hotels.get(hotel.id, []),
                'promoted': sort_result[hotel.id].get('promoted', False),
                'hygiene_shield_name': hotel.hygiene_shield_name,
                'slugify_url': hotel.get_slugified_url()
            }

            result.append(item)
        response['result'] = result
        return response

    @validate_request(validationSerializer)
    @log_args(logger)
    def get(self, request, *args, **kwargs):
        """
        Search API to get list of available hotels for Search Page.
        Model dependency: State, City, Locality, CityAlias, Hotel, Image, Facility
        hotel_id -- Hotel Id
        hotel_name -- Hotel Name
        locality_name -- Locality Name
        city_name -- City Name
        state_name -- State Name
        country_name -- Country Name
        location_type -- Location Type (Select one out of - hotel/locality/city/state/country)
        nearby -- Search Near By Hotels (true/false)
        """
        try:
            search_dto = self.parse_request(request)
        except InvalidBookingDates as err:
            response = APIResponse.treebo_exception_error_response(err)
            return response

        except Exception as e:
            return Response({
                'error': e.__str__(),
                'result': [],
                'sort': {},
            }, status=status.HTTP_400_BAD_REQUEST)

        cache_key = self.get_cache_key(request_param=request.GET, search_dto=search_dto)

        result = cache.get(cache_key)
        if result:
            result = self._update_review_data(result)
            return Response(result)

        try:
            search_result = self._search_hotels(search_dto)
            category_dict = CategoriesServices().category_dict(hotels=search_result.hotels,
                                                               hotel_ids=search_result.hotel_ids)

            sort_type = self._get_sort_type(search_dto)
            sorted_hotels = self._apply_sorting(search_dto, search_result, sort_type)

            response = self.make_response(hotels=search_result.hotels, category_dict=category_dict,
                                          sort_result=sorted_hotels, search_type=sort_type)
            response = self._update_review_data(response)

            cache.set(cache_key, response, SearchApiV4.CACHE_TIMEOUT)

            return Response(response)

        except NoHotelsFoundException as e:
            SlackAlertService.send_slack_alert_for_search_seo(
                request_param=request.GET, api_name=self.__class__.__name__)
            return Response(data={
                'error': e.message,
                'result': [],
                'filters': {},
                'sort': {},
                'sort_params': "",
            }, status=e.status_code)

        except Exception as e:
            SlackAlertService.send_slack_alert_for_exceptions(
                status=500,
                request_param=request.GET,
                message=e.__str__(),
                class_name=self.__class__.__name__)
            return Response({
                'error': e.__str__,
                'result': [],
                'sort': {},
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def _apply_sorting(self, search_dto, search_result, sort_type):
        sku_search_cache_service = SkuSearchCacheService()
        pricing_sorting_service = PricingSortingService()

        room_wise_availability_per_hotel = self._get_availability(
            search_dto.checkin_date, search_dto.checkout_date, search_result.hotels,
            search_dto.room_config, sku_search_cache_service)

        if sort_type == 'distance':
            specific_hotel_searched = None
            if search_dto.hotel_id:
                search_result.hotels = [h for h in search_result.hotels if
                                        h.id != search_dto.hotel_id]
                if not search_dto.nearby:
                    specific_hotel_searched = search_result.filter_hotel(search_dto.hotel_id)

            sorted_hotels = pricing_sorting_service.sort_by_distance_hotels(
                search_result.hotels, room_wise_availability_per_hotel,
                search_result.hotel_web_cs_id_dict, specific_hotel_searched)

            if specific_hotel_searched:
                search_result.hotels = [specific_hotel_searched] + search_result.hotels

        else:
            search_cache_service_result = \
                sku_search_cache_service.get_min_hotels_price_from_cache(
                    search_result.hotel_ids, room_wise_availability_per_hotel,
                    search_result.hotel_web_cs_id_dict)

            sorted_hotels = pricing_sorting_service.sort_recommended_hotels(
                search_cache_service_result, search_result.hotel_ids, search_dto.checkin_date,
                search_dto.checkout_date, search_dto.room_config, search_dto.search_channel,
                room_availability_per_hotel=room_wise_availability_per_hotel,
                hotel_web_cs_id_dict=search_result.hotel_web_cs_id_dict,
                meta_hotel_id=search_dto.meta_hotel_id)
        return sorted_hotels

    @staticmethod
    def _get_availability(check_in_date, check_out_date, hotels, room_config,
                          sku_search_cache_service):
        cs_ids = [hotel.cs_id for hotel in hotels]
        if FeatureToggleAPI.is_enabled(settings.DOMAIN, "sku_based_search", False):
            return sku_search_cache_service.get_availability_data(cs_ids, check_in_date,
                                                                  check_out_date, room_config)
        else:
            return ITSAvailabilityService.get_availability_data(cs_ids, check_in_date,
                                                                check_out_date, room_config)

    def _search_hotels(self, search_dto):
        if FeatureToggleAPI.is_enabled(settings.DOMAIN, "search_repo_pattern", False):
            hotels = HotelsRepository().search(search_dto)
        else:
            search_service = self.get_search_service_object(search_dto)
            hotels = search_service.search()
        return SearchResult(hotels)

    def parse_request(self, request):
        request_param = request.GET
        header_info = request.META.get('HTTP_ORIGIN', DIRECT)

        hotel_id = request_param.get('hotel_id')
        city_name = request_param.get('city')
        landmark_name = request_param.get('landmark')

        if landmark_name and not city_name:
            landmark_name = str(landmark_name)
            landmark_objs = self.search_landmark_repository.get_landmarks_by_freetext_url(
                query_string=landmark_name)
            landmark_obj = landmark_objs[0] if landmark_objs else None
            if not landmark_obj:
                items = landmark_name.split()
                city_name = items[-1]
                landmark_name = ' '.join(items[:-1])
            else:
                city_name = landmark_obj.city.name
                landmark_name = landmark_obj.seo_url_name

        nearby = request_param.get('nearby')
        distance_cap = request_param.get('distance_cap')
        nearby = True if nearby and nearby.lower() == 'true' else False
        distance_cap = True if distance_cap and distance_cap.lower() == 'true' else False

        validated_data = self.serializerObject.validated_data

        search_dto = HotelSearchQuery(
            hotel_id=int(hotel_id) if hotel_id else None,
            locality_name=request_param.get('locality'),
            landmark_name=landmark_name, city_name=city_name, state_name=request_param.get('state'),
            country_name=request_param.get('country'),
            location_type=request_param.get('location_type'), nearby=nearby,
            category=' '.join(request_param.get('category').split('-')) if request_param.get(
                'category') else None,
            amenity=' '.join(request_param.get('amenity').split('-')) if request_param.get(
                'amenity') else None,
            meta_keys=request_param.get('meta_key'),
            latitude=request_param.get('latitude'),
            longitude=request_param.get('longitude'),
            radius=int(request_param.get('radius', settings.DEFAULT_SEO_LANDMARK_RADIUS)),
            distance_cap=distance_cap,
            hotel_name=request_param.get('hotel'),
            channel=request_param.get('channel', DEFAULT_SEARCH_CHANNEL),
            checkin_date=validated_data['checkin'], checkout_date=validated_data['checkout'],
            room_config=validated_data['roomconfig'],
            meta_hotel_id=validated_data.get('meta_hotel_id', None),
            search_channel=GDC if GDC_BASE_HOST_URL in header_info else DIRECT)
        return search_dto

    @staticmethod
    def get_search_service_object(search_dto):
        return SearchService(
            hotel_id=search_dto.hotel_id, hotel_name=search_dto.hotel_name,
            locality_name=search_dto.locality_name,
            landmark_name=search_dto.landmark_name, city_name=search_dto.city_name,
            state_name=search_dto.state_name, country_name=search_dto.country_name,
            location_type=search_dto.location_type, nearby=search_dto.nearby,
            category=search_dto.category, amenity=search_dto.amenity,
            meta_keys=search_dto.meta_keys, latitude=search_dto.latitude,
            longitude=search_dto.longitude, radius=search_dto.radius,
            distance_cap=search_dto.distance_cap)

    @staticmethod
    def _get_sort_type(search_dto):
        if search_dto.hotel_id or search_dto.landmark_name or search_dto.locality_name:
            return 'distance'
        elif search_dto.latitude and search_dto.longitude and search_dto.radius and \
            search_dto.location_type == 'geo':
            return 'distance'
        else:
            return 'recommended'

    @staticmethod
    def _update_review_data(data):
        try:
            review_app_config = ReviewAppConfigService()
            cache_service = CacheService()
            config = ReviewAppConfiguration.objects.first()
            hotels_review_status = [
                (item['id'], review_app_config.check_toggle_settings(config=config,
                                                                     hotel_code=item['id']))
                for item in data['result']]

            hotels_with_review_enabled = {h[0] for h in hotels_review_status if h[1]}

            hotel_reviews = cache_service.get_reviews_from_cache(hotels_with_review_enabled,
                                                                 PageConfig.SEARCH_PAGE)

            for item in data['result']:
                hotel_code = item['id']
                if hotel_code in hotels_with_review_enabled:
                    review_response = hotel_reviews.get(hotel_code, dict())
                    item['ta_review'] = review_response.get('ta_reviews', dict())
                else:
                    item['ta_review'] = {'ta_enabled': False}
        except Exception as ex:
            logger.exception(
                'Exception occured in updating review data in search page')
        return data

