# pylint: disable=no-member

import json
import logging
from decimal import Decimal
from http import HTTPStatus

from web_sku.availability.services.availability_service import AvailabilityService
from web_sku.catalog.services.cs_type_mappers import CsTypeMappers
from web_sku.catalog.services.sku_request_service import SkuRequestService
from web_sku.catalog.services.sku_service import SkuService
from web_sku.common.custom_exception import CouponNotAppliedException
from web_sku.common.dateutils import get_iso_date
from web_sku.pricing.services.pricing_service import PricingService

from apps.checkout.constants import DISCOUNT_PREPAID_PAYMODE, PAY_AT_HOTEL_BOOKING_MODE, \
    PREPAID_BOOKING_MODE, PREPAID_WITH_PAYMENT_OFFER_BOOKING_MODE
from apps.checkout.models import BookingRequest, BookingRequestUserMapForWallet, CommittedPrice, \
    BookingRequestPrice
from apps.checkout.service.checkout_service import create_booking_request
from apps.checkout.service.upfront_part_pay_service import UpfrontPartPayService
from apps.common.exceptions.booking_exceptions import BookingNotFoundException, \
    InvalidBookingRequestException, BookedHotelNotFound
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.discounts.services.promotion_service import PromotionService
from apps.featuregate.constants import FEATURE_PAGE_NAMES
from apps.hotels import utils as hotel_utils
from apps.pages.helper import PageHelper
from apps.pricing.response_wrappers.itinerarary_api_wrapper import ItineraryApiWrapper
from common.services.feature_toggle_api import FeatureToggleAPI
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from apps.payments.offers.services.payment_offer import PaymentOfferService
from webapp.apps.bookings.services.booking_utils import get_room_codes

logger = logging.getLogger(__name__)


def get_itinerary_data_from_booking_id(booking_id):
    try:
        booking_request = BookingRequest.objects.get(booking_id=booking_id)
    except Exception:
        message = 'Booking request not found booking _id: %s', booking_id
        logger.exception(message)
        raise BookingNotFoundException(message)
    try:
        hotel_info = Hotel.objects.get(id=booking_request.hotel_id)
    except Exception:
        message = 'Hotel not found for booking request: %s and hotel id: %s' \
                  '', booking_request.id, booking_request.hotel_id
        logger.exception(message)
        raise BookedHotelNotFound(message)

    return {
        "bid": booking_id,
        "checkin": booking_request.checkin_date,
        "checkout": booking_request.checkout_date,
        "city": hotel_info.city.name,
        "hotel_id": booking_request.hotel_id,
        "rateplan": booking_request.rate_plan,
        "roomconfig": booking_request.room_config,
        "roomtype": booking_request.room_type,
        "name": booking_request.name,
        "email": booking_request.email,
        "phone": booking_request.phone
    }


class ItineraryHandlerV2:
    hotel_repository = RepositoriesFactory.get_hotel_repository()
    availability_service = AvailabilityService()
    pricing_service = PricingService()
    sku_service = SkuService()
    sku_request_service = SkuRequestService()
    cs_type_mappers = CsTypeMappers()
    upfront_partpay_service = UpfrontPartPayService()
    promotion_service = PromotionService()

    def get_web_price(self, pricing_request_data, sku_availability):

        room_config_wise_price = self.pricing_service.get_room_wise_price_from_availability(
            pricing_request_data, sku_availability)
        web_transformed_price = self.pricing_service.get_web_transformed_price(
            pricing_request_data['cs_web_id_map'], room_config_wise_price)

        return web_transformed_price

    def get_price(self, request, page_data, availability_request_data, pricing_request_data,
                  apply_wallet=False, checkin=None, checkout=None, booking_request=None):

        discount_prepaid_price, discount_prepaid_availability, \
        payment_offfer_web_transformed_price = {}, {}, {}

        sku_availability = self.availability_service.get_sku_availability(availability_request_data)
        room_wise_availability = self.availability_service. \
            get_room_wise_availability_from_sku_availability(availability_request_data,
                                                             sku_availability)

        web_transformed_availability = self.availability_service.get_web_transformed_availability(
            availability_request_data['cs_web_id_map'], room_wise_availability)

        payment_offer = None
        booking_request_coupon_code = booking_request.coupon_code if booking_request else None
        if pricing_request_data['apply_coupon']:
            # check for auto apply coupon code only when any coupon code is not added already
            if not pricing_request_data['coupon_code'] and not booking_request_coupon_code:
                payment_offer = PaymentOfferService().get_applicable_payment_offer(
                    page_data.get('utm_source'), page_data.get('channel'))
            else:
                payment_offer = PaymentOfferService().get_payment_offer_for_coupon_code(
                    pricing_request_data['coupon_code'], page_data.get('utm_source'),
                    page_data.get('channel'))

        if payment_offer:
            pricing_request_data['coupon_code'] = payment_offer.coupon_code
            payment_offfer_web_transformed_price = self.get_web_price(
                pricing_request_data, sku_availability)
            pricing_request_data['coupon_code'] = None

        web_transformed_price = self.get_web_price(pricing_request_data, sku_availability)

        prepaid_promotion = self.promotion_service.get_discount_prepaid_promotion(
            booking_request=booking_request)

        if prepaid_promotion:
            discount_prepaid_request_data = pricing_request_data
            discount_prepaid_request_data['paymode'] = DISCOUNT_PREPAID_PAYMODE
            discount_prepaid_price = self.get_web_price(discount_prepaid_request_data,
                                                        sku_availability)

        part_pay_applicable = False
        if FeatureToggleAPI.is_enabled(FEATURE_PAGE_NAMES.ITINERARY, "upfront_partpay"):
            part_pay_applicable = self.upfront_partpay_service.is_applicable(
                FEATURE_PAGE_NAMES.ITINERARY,
                pricing_request_data.get('utm', {}).get(
                    'source'),
                pricing_request_data.get('channel'),
                page_data['hotel_id'])

        itinerary_api_wrapper = ItineraryApiWrapper(
            hotel_id=page_data['hotel_id'],
            cs_hotel_id=availability_request_data['hotels'][0],
            room_config_wise_price=web_transformed_price,
            room_wise_availability=web_transformed_availability,
            discount_prepaid_price=discount_prepaid_price,
            rate_plan=page_data['rateplan'],
            part_pay_applicable=part_pay_applicable,
            user=getattr(request, 'user', None),
            apply_wallet=apply_wallet,
            checkin=request.query_params.get('checkin') if not checkin else checkin,
            checkout=request.query_params.get('checkout') if not checkout else checkout,
            promotion=prepaid_promotion,
            application_code=page_data['channel'],
            payment_offer_price=payment_offfer_web_transformed_price,
            payment_offer=payment_offer
        )

        return itinerary_api_wrapper.get_pricing_response()

    def get_pricing_data_from_booking_request_and_coupon(self, booking_request,
                                                         coupon_code):  # pylint: disable=C0103

        cs_web_id_map = {}
        hotel_id = str(booking_request.hotel_id)
        for hotel in self.hotel_repository.get_hotels_for_id_list([hotel_id]):
            if hotel.cs_id is not None:
                cs_web_id_map[hotel.cs_id.strip()] = hotel.id

        room_codes = get_room_codes([booking_request.room_type])
        request_skus = self.sku_request_service.get_web_request_skus(
            booking_request.room_config, list(cs_web_id_map.keys()), room_codes)

        availability_data = {
            'skus': request_skus,
            'checkin': booking_request.checkin_date,
            'checkout': booking_request.checkout_date,
            'hotels': list(cs_web_id_map.keys()),
            'occupancy_config': booking_request.room_config,
            'channel': 'direct',
            'subchannel': self.cs_type_mappers.get_cs_sub_channel(booking_request.booking_channel,
                                                                  booking_request.utm_source),
            'cs_web_id_map': cs_web_id_map
        }

        pricing_data = {
            'skus': request_skus,
            'from_date': get_iso_date(booking_request.checkin_date),
            'to_date': get_iso_date(booking_request.checkout_date),
            'mode': 'BROWSE',
            'policy': ['rp'],
            'apply_coupon': bool(coupon_code),
            'coupon_code': coupon_code,
            'channel': 'direct',
            'subchannel': self.cs_type_mappers.get_cs_sub_channel(booking_request.booking_channel,
                                                                  booking_request.utm_source),
            'application': self.cs_type_mappers.get_cs_application(
                booking_request.booking_application),
            'utm': {'source': booking_request.utm_source},
            'customer_info': {},
            'pricing_type': 'itinerary',
            'cs_web_id_map': cs_web_id_map,
        }

        page_data = {
            'hotel_id': hotel_id,
            'rateplan': booking_request.rate_plan,
            'channel': booking_request.booking_channel,
            'utm_source': booking_request.utm_source
        }

        return {
            'page_data': page_data,
            'availability_data': availability_data,
            'pricing_data': pricing_data
        }

    def get_pricing_request_data(self, page_data, request_skus, cs_web_id_map, paymode=None):
        pricing_request_data = {
            'skus': request_skus,
            'from_date': get_iso_date(page_data['checkin_date']),
            'to_date': get_iso_date(page_data['checkout_date']),
            'mode': 'BROWSE',
            'policy': ['rp'],
            'apply_coupon': True,
            'coupon_code': page_data.get('coupon_code', ''),
            'channel': 'direct',
            'subchannel': self.cs_type_mappers.get_cs_sub_channel(page_data.get('channel'),
                                                                  page_data.get('utm_source')),
            'application': self.cs_type_mappers.get_cs_application(page_data.get('channel')),
            'utm': {'source': self.cs_type_mappers.get_cs_utm_source(page_data.get('utm_source'))},
            'customer_info': {},
            'pricing_type': 'itinerary',
            'cs_web_id_map': cs_web_id_map,
            'paymode': paymode
        }
        return pricing_request_data

    def get_availability_request_data(self, page_data, request_skus, cs_web_id_map):
        if cs_web_id_map:
            cs_hotel_id = list(cs_web_id_map.keys())[0]
        else:
            cs_hotel_id = ''

        availability_request_data = {
            'skus': request_skus,
            'checkin': page_data['checkin_date'],
            'checkout': page_data['checkout_date'],
            'hotels': [cs_hotel_id],
            'occupancy_config': page_data['room_config'],
            'channel': 'direct',
            'subchannel': self.cs_type_mappers.get_cs_sub_channel(page_data.get('channel'),
                                                                  page_data.get('utm_source')),
            'cs_web_id_map': cs_web_id_map
        }

        return availability_request_data

    def update_booking_request_user_map(self, booking_request, user):
        bid_user = None
        if booking_request:
            try:
                bid_user = BookingRequestUserMapForWallet.objects.get(
                    booking_request=booking_request)
            except BookingRequestUserMapForWallet.DoesNotExist:
                pass
        if not bid_user:
            bid_user = BookingRequestUserMapForWallet()

        bid_user.booking_request = booking_request
        bid_user.user = user
        bid_user.save()

    def get_user_by_booking_request(self, booking_request):
        user = None
        if not booking_request:
            return user

        try:
            bid_user = BookingRequestUserMapForWallet.objects.get(
                booking_request=booking_request)
            user = bid_user.user
        except BookingRequestUserMapForWallet.DoesNotExist:
            pass

        return user

    def get_or_update_booking_request(self, request, page_data):  # pylint: disable=R0912
        user = request.user
        booking_request = None
        # checking if the input booking request (if any) has a booking created against it
        if page_data.booking_request_id:
            try:
                booking_request = BookingRequest.objects.get(pk=page_data.booking_request_id)
            except BookingRequest.DoesNotExist:
                raise InvalidBookingRequestException("Invalid booking request id.")

            if booking_request.order_id:
                # repeat booking attempt! let's clear the page data booking id
                page_data.booking_request_id = None

        if not page_data.booking_request_id:
            try:
                booking_request = create_booking_request(request, page_data)
                page_data.booking_request_id = booking_request.id
            except Exception as e:
                logging.exception(" error while creating booking request ")
                raise InvalidBookingRequestException(e)
        else:
            # booking_request = BookingRequest.objects.get(pk=page_data.booking_request_id)
            if not page_data.room_type or not page_data.hashed_hotel_id:
                if not page_data.room_type:
                    page_data.room_type = booking_request.room_type
                if not page_data.hashed_hotel_id:
                    page_data.hashed_hotel_id = hotel_utils.generate_hash(
                        booking_request.hotel_id)
        if page_data.checkin_date != booking_request.checkin_date:
            booking_request.checkin_date = page_data.checkin_date
        if page_data.checkout_date != booking_request.checkout_date:
            booking_request.checkout_date = page_data.checkout_date
        if page_data.room_type != booking_request.room_type:
            booking_request.room_type = page_data.room_type
        if page_data.room_config != booking_request.room_config:
            booking_request.room_config = page_data.room_config
        if page_data.rate_plan != booking_request.rate_plan:
            booking_request.rate_plan = page_data.rate_plan
        if page_data.apply_wallet != booking_request.wallet_applied:
            booking_request.wallet_applied = page_data.apply_wallet

        if page_data.utm_source and not booking_request.utm_source:
            booking_request.utm_source = page_data.utm_source

        if not booking_request.coupon_code:
            booking_request.coupon_code = page_data.coupon_code
        booking_request.user_login_state = 'LOGGEDIN' if user and user.is_authenticated() else 'LOGGEDOUT'
        booking_request.user = user if user and user.is_authenticated() else None
        booking_request.adults, booking_request.children, booking_request.room_required = \
            PageHelper.parseRoomConfig(page_data.room_config)

        booking_request.save()

        if user and user.is_authenticated() and booking_request.wallet_applied:
            self.update_booking_request_user_map(booking_request, user)
        else:
            self.update_booking_request_user_map(booking_request, None)

        return booking_request

    def add_booking_request_price(self, booking_request, price_dict, booking_mode, coupon_code,
                                  rate_plan_meta, wallet_info):

        try:
            booking_request_price = BookingRequestPrice.objects.get(booking_request=booking_request,
                                                                    booking_mode=booking_mode,
                                                                    rate_plan=rate_plan_meta[
                                                                        'code'])
        except BookingRequestPrice.DoesNotExist:
            booking_request_price = BookingRequestPrice()
            booking_request_price.booking_request = booking_request
            booking_request_price.booking_mode = booking_mode

        booking_request_price.pretax_amount = price_dict['final_pretax_price']
        booking_request_price.discount_id = price_dict["discount_id"]
        booking_request_price.discount_value = price_dict["coupon_discount"]
        booking_request_price.tax_amount = price_dict["tax"]
        wallet_deduction = price_dict.get("treebo_points_discount") or price_dict[
            "wallet_deduction"]
        booking_request_price.wallet_deduction = wallet_deduction if wallet_deduction else Decimal(
            0.0)
        wallet_applied = bool(
            price_dict.get("treebo_points_discount") or price_dict["wallet_applied"])
        booking_request_price.wallet_applied = wallet_applied
        booking_request_price.rate_plan = rate_plan_meta['code']

        if rate_plan_meta:
            booking_request_price.rate_plan_meta = rate_plan_meta

        if booking_mode == PAY_AT_HOTEL_BOOKING_MODE:
            booking_request_price.part_pay_amount = price_dict['part_pay_amount']

        booking_request_price.coupon_code = ""
        booking_request_price.coupon_apply = False
        if price_dict.get("coupon_discount") or price_dict.get("voucher_amount"):
            booking_request_price.coupon_code = coupon_code
        if booking_request_price.coupon_code:
            booking_request_price.coupon_apply = True

        # in case of post tax wallet deductions, wallet amount would be treated as separate payment
        # in case of prepaid wallet deductions, wallet amount would be deducted from booking amount
        if wallet_info.get("debit_mode") == "post_tax":
            booking_request_price.total_amount = float(price_dict.get(
                'net_payable_amount')) + float(booking_request_price.wallet_deduction)
        else:
            booking_request_price.total_amount = price_dict.get('net_payable_amount')

        booking_request_price.datewise_pricing = json.dumps(
            self.get_date_and_room_wise_pricing(price_dict))
        booking_request_price.save()

    def update_booking_request_with_prices(
        self,
        booking_request,
        prices,
        current_user,
        coupon_code='',
        rate_plan_detail='',
        wallet_info=None,
        payment_offer=None
    ):
        """
        rate_plan_detail will be dict of this form:
                {
                    "code": selected_rate_plan,
                    "description": rate_plan_meta.get("description"),
                    "tag": rate_plan_meta.get("tag")
                },
        :param booking_request:
        :param prices:
        :param current_user:
        :param coupon_apply:
        :param date_wise_pricing:
        :param coupon_code:
        :param rate_plan_detail:
        :param wallet_info:
        :return:
        """

        rate_plan_prices = prices
        self.add_booking_request_price(booking_request=booking_request,
                                       price_dict=rate_plan_prices['price'],
                                       booking_mode=PAY_AT_HOTEL_BOOKING_MODE,
                                       coupon_code=coupon_code,
                                       rate_plan_meta=rate_plan_detail, wallet_info=wallet_info)

        if rate_plan_prices['discount_prepaid_price']:
            prepaid_price_dict = rate_plan_prices['discount_prepaid_price']
        else:
            prepaid_price_dict = rate_plan_prices['price']

        self.add_booking_request_price(booking_request=booking_request,
                                       price_dict=prepaid_price_dict,
                                       booking_mode=PREPAID_BOOKING_MODE, coupon_code=coupon_code,
                                       rate_plan_meta=rate_plan_detail, wallet_info=wallet_info)

        if rate_plan_prices.get('payment_offer_price'):
            payment_offer_price_dict = rate_plan_prices['payment_offer_price']
            payment_offer_coupon_code = rate_plan_prices['payment_offer_price']['coupon_code']

            self.add_booking_request_price(booking_request=booking_request,
                                           price_dict=payment_offer_price_dict,
                                           booking_mode=PREPAID_WITH_PAYMENT_OFFER_BOOKING_MODE,
                                           coupon_code=payment_offer_coupon_code,
                                           rate_plan_meta=rate_plan_detail, wallet_info=wallet_info)

        booking_request.user_login_state = 'LOGGEDIN' if current_user and current_user.is_authenticated() \
            else 'LOGGEDOUT'
        booking_request.user = current_user if current_user and current_user.is_authenticated() else None
        booking_request.rack_rate = rate_plan_prices['price']["base_price"]
        booking_request.mm_promo = rate_plan_prices['price']["promo_discount"]
        booking_request.member_discount = rate_plan_prices['price']['member_discount']
        booking_request.voucher_amount = rate_plan_prices['price'].get('voucher_amount')
        booking_request.coupon_code = coupon_code
        if payment_offer:
            booking_request.payment_offer_id = payment_offer['id']
        else:
            booking_request.payment_offer_id = None
        booking_request.save()

    @staticmethod
    def get_date_and_room_wise_pricing(prices):
        date_and_room_wise_pricing = {}
        if 'nights_and_config_wise_breakup' in prices:
            for date_and_room_wise_price_breakup in prices['nights_and_config_wise_breakup']:
                room_wise_pricing = date_and_room_wise_pricing.get(
                    date_and_room_wise_price_breakup['date'])
                if not room_wise_pricing:
                    room_wise_pricing = dict()

                room_wise_pricing[date_and_room_wise_price_breakup['room_config']] = {
                    'sell_price': float(date_and_room_wise_price_breakup['sell_price'])
                }

                date_and_room_wise_pricing[
                    date_and_room_wise_price_breakup['date']] = room_wise_pricing

        return date_and_room_wise_pricing

    def add_committed_price_for_booking_request(self, booking_request, prices):
        """
        Add committed price for a booking request
        :param booking_request: booking_request
        :param prices: prices
        :return:
        """
        if 'nights_and_config_wise_breakup' in prices:
            date_and_room_wise_pricing = self.get_date_and_room_wise_pricing(prices)
            committed_price = CommittedPrice.objects.filter(booking_request=booking_request).first()
            if not committed_price:
                committed_price = CommittedPrice()
                committed_price.booking_request = booking_request
            committed_price.datewise_pricing = json.dumps(date_and_room_wise_pricing)
            committed_price.save()

    def apply_coupon(self, request, bid, coupon_code=None, apply_wallet=False):
        try:
            booking_request = BookingRequest.objects.get(pk=bid)
        except BookingRequest.DoesNotExist:
            raise BookingNotFoundException('bid %d not presend ' % bid)

        checkin = booking_request.checkin_date
        checkout = booking_request.checkout_date
        # utm_source = booking_request.utm_source
        current_user = getattr(request, 'user', None)
        # logged_in_user = True if current_user and current_user.is_authenticated() else False
        data = self.get_pricing_data_from_booking_request_and_coupon(booking_request, coupon_code)
        price = self.get_price(request, data['page_data'], data['availability_data'],
                               data['pricing_data'],
                               apply_wallet, checkin, checkout, booking_request=booking_request)

        payment_offer_coupon_code = None
        if price["selected_rate_plan"].get("payment_offer_price"):
            payment_offer_coupon_code = price["selected_rate_plan"]["payment_offer_price"][
                "coupon_code"]

        applied_coupon_code = price["selected_rate_plan"]["price"]["coupon_code"]
        apply_coupon = bool(data['pricing_data']['coupon_code'])
        valid_coupon_code_applied = coupon_code == applied_coupon_code or \
                                    coupon_code == payment_offer_coupon_code
        if (coupon_code and apply_coupon and not applied_coupon_code) or (
            coupon_code and applied_coupon_code and not valid_coupon_code_applied):
            developer_message = "Pricing returned without applied coupon code {}".format(
                coupon_code)
            logger.exception(developer_message)
            raise CouponNotAppliedException(developer_message=developer_message,
                                            response_status_code=HTTPStatus.INTERNAL_SERVER_ERROR)

        self.update_booking_request_with_prices(
            booking_request,
            price["selected_rate_plan"],
            current_user,
            coupon_code=applied_coupon_code,
            rate_plan_detail=price["selected_rate_plan"]["rate_plan"],
            wallet_info=price["wallet_info"],
            payment_offer=price['payment_offer']
        )
        self.add_committed_price_for_booking_request(
            booking_request,
            price["selected_rate_plan"]["price"]
        )
        return price

    def select_wallet(self, request, bid, apply_wallet):
        try:
            booking_request = BookingRequest.objects.get(pk=bid)
        except BookingRequest.DoesNotExist as e:
            raise BookingNotFoundException('bid %d not presend ' % bid)

        # referral_click_id = str(request.COOKIES.get(
        #     'avclk')) if request.COOKIES.get('avclk') else None
        # utm_source = booking_request.utm_source
        payment_offer_coupon_code = None
        try:
            current_user = getattr(request, 'user', None)
            logged_in_user = bool(current_user and current_user.is_authenticated())
            data = self.get_pricing_data_from_booking_request_and_coupon(booking_request,
                                                                         booking_request.coupon_code)
            price = self.get_price(request, data['page_data'], data['availability_data'],
                                   data['pricing_data'],
                                   apply_wallet, booking_request=booking_request)

            if price["selected_rate_plan"].get("payment_offer_price"):
                payment_offer_coupon_code = price["selected_rate_plan"]["payment_offer_price"][
                    "coupon_code"]

            self.update_booking_request_with_prices(
                booking_request,
                price["selected_rate_plan"],
                current_user,
                coupon_code=price["selected_rate_plan"]["price"]["coupon_code"],
                rate_plan_detail=price["selected_rate_plan"]["rate_plan"],
                wallet_info=price["wallet_info"],
                payment_offer=price['payment_offer']
            )
            self.add_committed_price_for_booking_request(
                booking_request,
                price["selected_rate_plan"]["price"]
            )

            if logged_in_user and booking_request.wallet_applied:
                self.update_booking_request_user_map(booking_request, current_user)
            else:
                self.update_booking_request_user_map(booking_request, None)
        except Exception as e:
            price = {}
            logger.exception("Exception occurred in select wallet for bid {bid}".format(bid=bid))
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param=request.GET,
                                                        dev_msg="Select Wallet",
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)

        if payment_offer_coupon_code:
            price["coupon_code"] = payment_offer_coupon_code

        return price
