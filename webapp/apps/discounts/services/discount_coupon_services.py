import logging
from datetime import date

import requests
from django.conf import settings
from django.db.models import Q

from apps.bookings.services.booking_request import BookingRequestServices
from apps.discounts import CouponValidationParams
from apps.discounts import constants
from apps.discounts.discount_manager import DiscountManager
from apps.discounts.exceptions import InvalidCouponException, AvailableCouponsException
from apps.discounts.models import DiscountCoupon, ConstraintKeywords
from apps.pricing.dateutils import date_to_ymd_str
from apps.pricing.services.price_request_dto import PriceRequestDto
from apps.pricing.services.pricing_service import PricingService
from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.pricing.transformers.coupon_api_transformer import CouponAPITransformer
from data_services.respositories_factory import RepositoriesFactory
from common.services.feature_toggle_api import FeatureToggleAPI
from django.conf import settings

logger = logging.getLogger(__name__)


class DiscountCouponServices(object):
    @staticmethod
    def validate_coupon_and_get_discount(
        booking_request,
        coupon_code,
        referral_click_id,
        logged_user_id,
        current_user):
        guest_email = str(booking_request.email)
        guest_phone = str(booking_request.phone)

        coupon = DiscountCoupon.objects.filter(
            code__iexact=coupon_code).exclude(
            discount_operation=DiscountCoupon.FOTREDEEM,
            coupon_type=DiscountCoupon.VOUCHER).first()
        if not coupon:
            raise InvalidCouponException("Coupon does not exist")
        coupon_desc = str(coupon.terms.replace('\\n', ' '))

        if coupon.is_referral:
            if referral_click_id:
                DiscountCouponServices.validate_referral_coupon(
                    coupon_code, guest_email, guest_phone, logged_user_id)
            else:
                raise InvalidCouponException("Coupon does not exist")

        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotel = hotel_repository.get_hotel_by_id_from_web(
            booking_request.hotel_id)
        coupon_validation_params = DiscountCouponServices.get_coupon_validation_param_from_booking(
            booking_request, coupon_code, hotel, current_user)
        return DiscountCouponServices.validate_and_apply_coupon_code(
            coupon_validation_params)

    @staticmethod
    def validate_and_apply_coupon_code(coupon_validation_params):
        discountManager = DiscountManager(**coupon_validation_params.__dict__)
        coupon, message = discountManager.validate()
        percentage = 0
        discount_error = ""
        discount = 0
        room_count = coupon_validation_params.no_bookings
        if coupon:
            if coupon.terms:
                coupon_desc = str(coupon.terms.replace('\\n', ' '))
            discount = discountManager.apply(coupon, room_count)
            # percentage = (float(discount) / float(kwargs["total_pretax_cost"])) * 100
        else:
            discount_error = message

        return coupon, discount, discount_error

    @staticmethod
    def validate_referral_coupon(
        coupon_code,
        guest_email,
        guest_phone,
        logged_user_id='',
        otp_verified_number=''):
        logger.info("Checking coupon validity")
        if coupon_code and not FeatureToggleAPI.is_enabled(settings.DOMAIN, "pricing_revamp", False):
            try:
                coupon = DiscountCoupon.objects.get(code__iexact=coupon_code)
            except DiscountCoupon.DoesNotExist:
                logger.exception("Coupon does not exist %s", coupon_code)
                raise InvalidCouponException("Coupon does not exist")
            if coupon.is_referral:
                booking_request_service = BookingRequestServices()
                booking_request = booking_request_service.get_existing_booking_request(
                    guest_email, guest_phone, logged_user_id, otp_verified_number)

                if booking_request:
                    logger.exception(
                        "Guest %s is not eligible for coupon code %s" %
                        (guest_email, coupon_code))
                    raise InvalidCouponException(
                        "%s is not eligible for coupon code %s" %
                        (guest_email, coupon_code))

    @classmethod
    def build_discount_manager_params(cls, booking_request, user):
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        checkin = date_to_ymd_str(booking_request.checkin_date)
        checkout = date_to_ymd_str(booking_request.checkout_date)
        hotel = hotel_repository.get_hotel_by_id_from_web(
            booking_request.hotel_id)
        # hotel = Hotel.objects.get(id=booking_request.hotel_id)
        room_count = len(booking_request.room_config.split(','))

        kwargs = {
            'checkin': checkin,
            'room_config': booking_request.room_config,
            'checkout': checkout,
            'hotel_id': hotel.id,
            'user': booking_request.user,
            'no_bookings': room_count,
            'booking_date': date.today(),
            'total_pretax_cost': booking_request.pretax_amount,
            'totalcost': booking_request.total_amount,
            'rack_rate': booking_request.rack_rate,
            'rooms': booking_request.room_type,
            'city': hotel.city,
            'booking_channel': booking_request.booking_channel,
            'current_user': user,
            'paytype': booking_request.pay_at_hotel,
            'utm_source': booking_request.utm_source}
        return kwargs

    @classmethod
    def validate_coupon_code(cls, coupon_code, **kwargs):
        if not coupon_code:
            return True, ''
        try:
            coupon = DiscountCoupon.objects.get(code__iexact=coupon_code)
        except (DiscountCoupon.DoesNotExist, DiscountCoupon.MultipleObjectsReturned):
            return False, "Coupon does not exist"
        return cls.validate_coupon(coupon, **kwargs)

    @classmethod
    def post_booking_validate(cls, coupon_code):
        if not coupon_code:
            return True, ''
        try:
            coupon = DiscountCoupon.objects.get(code__iexact=coupon_code)
        except (DiscountCoupon.DoesNotExist, DiscountCoupon.MultipleObjectsReturned):
            return False, "Coupon does not exist"
        message = DiscountManager().validate_coupon(coupon)
        if message != constants.SUCCESS:
            return False, message
        return True, ''

    @classmethod
    def validate_coupon(cls, coupon, **kwargs):
        """
            coupon: it is of type DiscountCoupon.
            kwargs: these are same build_discount_manager_params
        """
        kwargs['code'] = coupon.code
        discount_manager = DiscountManager(**kwargs)
        coupon, message = discount_manager.validate()
        if coupon:
            return True, message
        return False, message

    @classmethod
    def validate_coupons(cls, coupons, booking_request, user):
        params = cls.build_discount_manager_params(booking_request, user)
        coupons = [c for c in coupons if cls.validate_coupon(c, **params)[0]]
        return coupons[:settings.AVAILABLE_COUPONS_LIMIT]

    @classmethod
    def visible_coupons(cls):
        booking_date = date.today()
        return DiscountCoupon.visible_coupons(booking_date)

    @classmethod
    def referral_coupons(cls, booking_request, user, referral=False):
        if not referral:
            return []
        coupons = cls.visible_coupons().filter(is_referral=True)
        email, phone = str(booking_request.email), str(booking_request.phone)
        logged_user_id = str(
            user.id) if user and user.is_authenticated() else ''
        filtered_coupons = []
        for coupon in coupons:
            try:
                cls.validate_referral_coupon(
                    coupon.code, email, phone, logged_user_id)
                filtered_coupons.append(coupon)
            except InvalidCouponException:
                pass
        return cls.validate_coupons(filtered_coupons, booking_request, user)

    @classmethod
    def percentage_coupons(cls):
        return cls.visible_coupons().filter(
            discount_operation=DiscountCoupon.PERCENTAGE).order_by('-discount_value')

    @classmethod
    def fixed_coupons(cls):
        return cls.visible_coupons().filter(
            discount_operation=DiscountCoupon.FIXED).order_by('-discount_value')

    @classmethod
    def percentage_exclude_referral_coupons(cls, booking_request, user):
        coupons = cls.percentage_coupons().filter(is_referral=False)
        return cls.validate_coupons(coupons, booking_request, user)

    @classmethod
    def fixed_exclude_referral_coupons(cls, booking_request, user):
        coupons = cls.fixed_coupons().filter(is_referral=False)
        return cls.validate_coupons(coupons, booking_request, user)

    @classmethod
    def available_coupons(cls, booking_request, **options):
        referral, user = options.get('referral', False), options.get('user')
        coupons = cls.referral_coupons(
            booking_request, user, referral=referral)
        coupons += cls.percentage_exclude_referral_coupons(
            booking_request, user)
        if len(coupons) >= settings.AVAILABLE_COUPONS_LIMIT:
            return coupons[:settings.AVAILABLE_COUPONS_LIMIT]
        coupons += cls.fixed_exclude_referral_coupons(booking_request, user)
        return coupons[:settings.AVAILABLE_COUPONS_LIMIT]

    @classmethod
    def fetch_available_coupons(cls, booking_request, user, referral=False):
        """
        Finds all coupons which are applicable for the booking
        :rtype: [DiscountCoupon]
        :param booking_request:
        :param user:
        :param referral: is this a referral request
        :return: list of coupons
        """
        coupons = cls.referral_coupons(
            booking_request, user, referral=referral)
        coupons += cls.percentage_exclude_referral_coupons(
            booking_request, user)
        if len(coupons) >= settings.AVAILABLE_COUPONS_LIMIT:
            return coupons[:settings.AVAILABLE_COUPONS_LIMIT]
        coupons += cls.fixed_exclude_referral_coupons(booking_request, user)
        return coupons[:settings.AVAILABLE_COUPONS_LIMIT]

    @classmethod
    def get_price_from_soa(cls, coupon, booking_request):
        try:
            # init variables
            checkin = date_to_ymd_str(booking_request.checkin_date)
            checkout = date_to_ymd_str(booking_request.checkout_date)
            discount_percentage = coupon.discount_percentage_for_booking(
                booking_request.pretax_amount)
            # get response
            response = PricingService.get_price_from_soa(
                checkin=checkin,
                checkout=checkout,
                hotel_ids=[
                    booking_request.hotel_id],
                room_config=booking_request.room_config,
                coupon_code=coupon.code,
                coupon_value=discount_percentage,
                coupon_type="Percentage",
                include_price_breakup=True,
                get_from_cache=False,
                channel=booking_request.booking_channel,
                utm_source=booking_request.utm_source)
            return response["data"]["hotels"][0]["rooms"]
        except Exception as e:
            logger.exception("Unable to fetch price for coupon")
            raise AvailableCouponsException('Error fetching prices from Soa')

    @classmethod
    def get_price_for_room(cls, room_prices, room_type):
        required_room = {}
        for room in room_prices:
            if room["room_type"].lower() == room_type.lower():
                required_room = room
        return required_room

    @classmethod
    def get_coupon_amount_from_soa(
        cls,
        coupon,
        booking_request,
        logged_in_user=False):
        room_prices = cls.get_price_from_soa(coupon, booking_request)
        required_room_price = cls.get_price_for_room(
            room_prices, booking_request.room_type)
        if logged_in_user:
            return required_room_price.get(
                'price', {}).get(
                'member_price_coupon_discount', {})
        else:
            return required_room_price.get(
                'price', {}).get(
                'coupon_discount', {})

    @classmethod
    def get_coupon_amount(
        cls,
        coupon,
        booking_request,
        discount_percentage,
        logged_in_user=False,
        utm_source=None):
        room_configs = booking_request.room_config.split(",")

        price_request_dto = PriceRequestDto(
            booking_request.checkin_date, booking_request.checkout_date, [booking_request.hotel_id],
            room_configs, room_type=booking_request.room_type, rate_plan=booking_request.rate_plan,
            coupon_code=coupon.code, coupon_value=discount_percentage, coupon_type="Percentage",
            channel=booking_request.booking_channel, logged_in_user=logged_in_user, utm_source=utm_source)

        room_prices = PricingServiceV2().get_itinerary_page_price(price_request_dto)

        coupon_discount = CouponAPITransformer().retrieve_coupon_discount(
            room_prices, booking_request.rate_plan)
        return coupon_discount

    @staticmethod
    def get_discount_coupons_from_utmsource_applicable_within_range(
        utm_source, start_date, end_date):
        utm_source_keyward = ConstraintKeywords.objects.filter(
            keyword='utm_source')
        coupons = DiscountCoupon.objects.filter(Q(is_active=True),
                                                Q(start_date__lte=start_date, expiry__gte=start_date),
                                                Q(start_date__lte=end_date, expiry__gte=end_date)).filter(
            discountcouponconstraints__value_one__iexact=utm_source,
            discountcouponconstraints__keyword=utm_source_keyward)
        if len(coupons) > 1:
            # slack_alert.publish_alert(
            #     'got more than 1 coupons `%s` for utm `%s` st `%s` and end `%s` ' % (utm_source, start_date, end_date, coupons),
            #     channel=slack_alert.AUTH_ALERTS)

            logger.error(
                "got more than 1 coupons %s for utm %s st %s and end %s ",
                utm_source,
                start_date,
                end_date,
                coupons)
        logger.debug(
            "get_discount_coupons_from_utmsource %s for st %s and end %s , coupons %s",
            utm_source,
            start_date,
            end_date,
            coupons)
        coupons_list = coupons.values_list('code', flat=True)
        return coupons_list

    @staticmethod
    def check_coupon_validity_basis_date_range(
        coupon_code, start_date, end_date):
        coupons = DiscountCoupon.objects.filter(
            code=coupon_code).filter(
            start_date__lte=start_date,
            expiry__gte=end_date)
        coupons_list = coupons.values_list('code', flat=True)
        if coupons_list:
            return True
        return False

    @staticmethod
    def get_discount_percentage(
        discount,
        pretax,
        member_discount,
        total_price):
        if not pretax:
            return 0
        discount_percent = (float(discount) / float(pretax)) * 100
        return round(discount_percent, 2)

    @staticmethod
    def get_coupon_validation_param_from_booking(
        booking_request, coupon_code, hotel, current_user):
        return CouponValidationParams(
            checkin_date=booking_request.checkin_date,
            checkout_date=booking_request.checkout_date,
            room_config=booking_request.room_config,
            coupon_code=coupon_code,
            hotel_id=hotel.id,
            city_id=hotel.city,
            user=booking_request.user,
            current_user=current_user,
            pretax_amount=booking_request.pretax_amount -
                          booking_request.discount_value,
            member_discount=booking_request.member_discount,
            total_amount=booking_request.total_amount,
            rack_rate=booking_request.rack_rate,
            room_type=booking_request.room_type,
            booking_channel=booking_request.booking_channel,
            paytype=booking_request.pay_at_hotel,
            utm_source=booking_request.utm_source)

    @staticmethod
    def validate_and_get_utm_souce(discount_coupon):
        allowed_constraints_are_present = discount_coupon.discountcouponconstraints_set.all()
        utm_keywards_constrainst = ConstraintKeywords.objects.filter(
            keyword__in=['utm_source']).first()
        if not utm_keywards_constrainst:
            return '', 'utm_source keyword not present'
        if not allowed_constraints_are_present:
            return '', 'no constraint found for coupon'
        utm_constrainst = allowed_constraints_are_present.filter(
            coupon__discountcouponconstraints__keyword=utm_keywards_constrainst).first()
        if not utm_constrainst:
            return '', 'no constraint found for keyword utm'
        utm_source = str(utm_constrainst.value_one)
        operation_value = str(utm_constrainst.operation.operator)
        if operation_value != 'eq':
            return '', 'operator value for constraint(utm_source) has to be eq'
        return utm_source, ''

    @staticmethod
    def get_overlapping_utm_coupons(coupon_code, start_date, end_date):
        try:
            discount_coupon = DiscountCoupon.objects.get(code=coupon_code)
        except DiscountCoupon.DoesNotExist as e:
            raise InvalidCouponException()
        utm_keywards_constrainst = ConstraintKeywords.objects.filter(
            keyword__in=['utm_source']).first()
        if not utm_keywards_constrainst:
            logger.error(" keyward utm_source not present in keywards ")
            return [discount_coupon]
        overlaping_coupons = DiscountCoupon.objects.filter(
            Q(
                is_active=True), Q(
                discountcouponconstraints__keyword=utm_keywards_constrainst), (Q(
                start_date__lte=start_date, expiry__gt=start_date)) | (
                                                                                  Q(
                                                                                      start_date__lte=end_date,
                                                                                      expiry__gt=end_date)) | (
                                                                                  Q(
                                                                                      start_date__gt=start_date,
                                                                                      expiry__lt=end_date))).exclude(
            id=discount_coupon.id)
        utm_source, err_msg = DiscountCouponServices.validate_and_get_utm_souce(
            discount_coupon)
        utm_overlaping_coupons = []
        if err_msg:
            return [discount_coupon]
        for overlaping_coupon in overlaping_coupons:
            other_coupon_utm_source, err_msg = DiscountCouponServices.validate_and_get_utm_souce(
                overlaping_coupon)
            if other_coupon_utm_source == utm_source:
                utm_overlaping_coupons.append(overlaping_coupon)
        return utm_overlaping_coupons


class FeaturedCouponsSku():

    def __init__(self):
        # TODO:Remove link from here to configuration
        self.DISCOUNT_URL = settings.DISCOUNT_URL

    def get_coupon_codes(self, booking_request, user):
        pricing_coupons = self.get_coupons_from_pricing(booking_request)
        return pricing_coupons

    def get_coupons_from_pricing(self, booking_request):
        coupons_response = self.get_response_from_pricing(booking_request)
        overall_coupons = []
        for coupon_response in coupons_response:
            overall_coupons.append(SkuCoupons(coupon_response).get())
        return overall_coupons

    def get_response_from_pricing(self, booking_request):
        coupons = []
        params = {
            "channel": 'direct',
            "application": booking_request.booking_application,
            "sub_channel": booking_request.booking_subchannel
        }
        headers = {'Content-Type': 'Application/json'}
        response = requests.get(
            url=self.DISCOUNT_URL,
            params=params,
            headers=headers)
        if response.status_code == 200:
            coupons = response.json()["data"]
        else:
            logger.error("Discount Counpons Fetch from Discount Api Failed")

        return coupons


class SkuCoupons(object):
    def __init__(self, coupon_response):
        self.code = coupon_response["code"]
        self.tagline = coupon_response["name"]
        self.coupon_amount = coupon_response["max_incentive_value"]
        # TODO:is_prepaid check
        self.is_prepaid = True
        self.description = coupon_response["description"]
        self.expiry = coupon_response["validity_end"]
        self.is_auto_applicable = coupon_response["is_auto_applicable"]

    def get(self):
        return self
