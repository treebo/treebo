# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0076_auto_20180424_1152'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='locality',
            name='seo_url_name',
        ),
        migrations.AddField(
            model_name='locality',
            name='free_text_url',
            field=models.CharField(default=b'', max_length=500, null=True, blank=True),
        ),
    ]
