import json


class TreeboCRSException(Exception):

    def __init__(self, errors, message=None):

        self.errors = errors

        if not message:
            message = ','.join([str(e.get('developer_message') or '') for e in errors])

        if not message:
            message = ','.join([json.dumps(e) for e in errors])

        super(TreeboCRSException, self).__init__('TreeboCRSError: {m}'.format(m=message))


class GuestNotAllocated(Exception):

    def __init__(self, message):
        super(GuestNotAllocated, self).__init__('Guest Stay has no allocated guest: {m}'.format(m=message))


class BookingAlreadyCancelledException(Exception):
    def __init__(self, booking_id):
        """
        raised when the booking is already cancelled

        :param booking_id:
        """
        # saving the params in case we need a custom error message emitted to the user
        self.booking_id = booking_id

        message = "Booking '{bid}' is already cancelled".format(bid=booking_id)

        super(BookingAlreadyCancelledException, self).__init__(message)
