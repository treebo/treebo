import sys
import traceback

from django.views.debug import ExceptionReporter

from services.tasks import exception_mailer


def send_exception_mail(e, subject=None, request=None):
    exc_info = sys.exc_info()
    reporter = ExceptionReporter(request, is_email=True, *exc_info)
    if not subject:
        subject = str(e).replace('\n', '\\n').replace('\r', '\\r')[:989]
    if request:
        message = "%s\n\n%s" % (
            '\n'.join(traceback.format_exception(*exc_info)),
            reporter.filter.get_request_repr(request)
        )
    else:
        message = "%s\n\n" % (
            '\n'.join(traceback.format_exception(*exc_info))
        )
    traceback_html = get_string_output_from_input(reporter.get_traceback_html())
    exception_mailer.delay(subject, message, traceback_html)

def get_string_output_from_input(input):
    if type(input) == bytes:
        return input.decode()
    return input
