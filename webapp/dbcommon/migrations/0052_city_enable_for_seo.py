# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0051_auto_20170524_0743'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='enable_for_seo',
            field=models.BooleanField(default=False),
        ),
    ]
