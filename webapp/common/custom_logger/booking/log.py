# coding=utf-8
"""
    CustomBookingLogger
"""
import logging

booking_logger = logging.getLogger(__name__)


def logger_isa(l, p, max=1000):
    """
    :param l:
    :param p:
    :param max:
    :return:
    """
    this, seen = l, set()
    for _ in range(max):
        if this == p:
            return True
        else:
            if this in seen:
                raise RuntimeError(
                    'Logger {0!r} parents recursive'.format(l),
                )
            seen.add(this)
            this = this.parent
            if not this:
                break
    else:
        raise RuntimeError('Logger hierarchy exceeds {0}'.format(max))
    return False


def get_booking_logger(name):
    """
    :param name:
    :return:
    """
    logger = logging.getLogger(name)
    if not logger_isa(logger, booking_logger):
        logger.parent = booking_logger
    return logger
