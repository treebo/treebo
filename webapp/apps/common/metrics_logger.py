import logging

logger = logging.getLogger("metrics")


def log(message, info):
    logger.info(message, extra=info)
