import datetime
import hmac
import json
from hashlib import sha1

import requests
from celery.utils.log import get_task_logger
from celery import shared_task
from django.conf import settings

from base.logging_decorator import log_args
from apps.hotels.service.update_min_prices import UpdateMinPriceService
from apps.hotels.service.update_min_prices_sku import UpdateMinPriceServiceSKU
from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from django.conf import settings

logger = get_task_logger(__name__)


# logger = logging.getLogger(__name__)
# @shared_task


@log_args(logger)
def email_hx(hotelogix_id, count=1):
    hotelogix_admin_url = settings.HOTELOGIX_ADMIN_URL
    consumer_key = settings.CONSUMER_KEY
    consumer_secret = settings.CONSUMER_SECRET
    logger.info(
        "consumer_key = %s \n consumer_secret = %s",
        consumer_key,
        consumer_secret)
    try:
        access_key, access_secret = hx_wsauth_api(
            hotelogix_admin_url, consumer_key, consumer_secret)
        save_email_config_api(
            hotelogix_admin_url,
            access_key,
            access_secret,
            hotelogix_id)
    except Exception as e:
        logger.exception("Exception - Retrying %d time", count)
        if count < 3:
            count += 1
            email_hx(hotelogix_id, count)
        else:
            raise e


def hx_wsauth_api(hotelogix_url, consumer_key, consumer_secret):
    logger.info("First - calling wsauth API")
    auth_json = ws_auth_json(consumer_key)
    logger.info("auth json %s", json.dumps(auth_json))
    auth_signature = ws_auth_signature(auth_json, consumer_secret)
    logger.info("auth_signature %s", auth_signature)
    a_key, a_secret = get_access_keys(
        hotelogix_url, auth_json, auth_signature, "wsauth")
    return a_key, a_secret


def save_email_config_api(
    hotelogix_admin_url,
    access_key,
    access_secret,
    hotelogix_id):
    logger.info("Second calling save email config api")
    save_email_json = save_email_config_json(hotelogix_id, access_key)
    logger.info("save_email_json %s", json.dumps(save_email_json))
    save_email_signature = ws_auth_signature(save_email_json, access_secret)
    logger.info("auth_signature %s", save_email_signature)
    update_email_for_hotel(
        hotelogix_admin_url,
        save_email_json,
        save_email_signature,
        "saveemailconfiguration")


def update_email_for_hotel(
    hotelogix_admin_url,
    save_email_json,
    save_email_signature,
    method):
    headers = {
        "Content-Type": "application/json",
        "X-HAPI-Signature": save_email_signature
    }
    data = json.dumps(save_email_json)
    response = requests.post(
        hotelogix_admin_url +
        method,
        data=data,
        headers=headers)
    if response.status_code == 200:
        logger.info("update_email_for_hotel response.text %s", response.text)
        json_response = json.loads(response.text)
        if json_response['hotelogix']['response']:
            save_email_config_response = json_response['hotelogix']['response']
            if save_email_config_response['status']['message'] == 'success':
                if save_email_config_response['hotels'][0]['updateStatus']['message'] == 'success':
                    logger.info("SAVE EMAIL CONFIGURATION API SUCCESSFUL")


def ws_auth_json(consumer_key):
    time = datetime.datetime.utcnow().replace(microsecond=0).isoformat()
    auth_json = {
        "hotelogix": {
            "version": "1.0",
            "datetime": time,
            "request": {
                "method": "wsauth",
                "key": consumer_key
            }
        }
    }
    return auth_json


def ws_auth_signature(auth_json, consumer_secret):
    logger.info("secret key used in signature function %s", consumer_secret)
    try:
        signature = hmac.new(consumer_secret, json.dumps(auth_json), sha1)
    except Exception:
        signature = hmac.new(
            key=bytearray(consumer_secret, 'utf-8'),
            msg=json.dumps(auth_json).encode('utf-8'),
            digestmod=sha1
        )
    ws_auth_signature = signature.hexdigest()
    logger.info("ws_auth_signature  is %s", ws_auth_signature)
    return ws_auth_signature


def get_access_keys(hotelogix_url, auth_json, auth_signature, method):
    headers = {
        "Content-Type": "application/json",
        "X-HAPI-Signature": auth_signature
    }
    data = json.dumps(auth_json)
    response = requests.post(
        hotelogix_url + method,
        data=data,
        headers=headers)
    logger.info("__get_access_keys response.text %s", response.text)
    json_response = json.loads(response.text)
    a_key = json_response['hotelogix']['response']['accesskey']
    a_secret = json_response['hotelogix']['response']['accesssecret']
    logger.info("access key %s  and access secret %s", a_key, a_secret)
    return a_key, a_secret


def save_email_config_json(hotelogix_id, access_key):
    time = datetime.datetime.utcnow().replace(microsecond=0).isoformat()
    save_email_json = {
        "hotelogix": {
            "version": "1.0",
            "datetime": time,
            "request": {
                "method": "saveemailconfiguration",
                "key": access_key,
                "data": {
                    "host": settings.EMAIL_HOST_HOTEL_SMTP,
                    "auth": True,
                    "port": settings.EMAIL_PORT_HOTEL_SMTP,
                    "username": settings.EMAIL_HOST_USER_HOTEL_SMTP,
                    "password": settings.EMAIL_HOST_PASSWORD_HOTEL_SMTP,
                    "encrptionType": "ssl",
                    "hotels": [
                        {
                            "id": hotelogix_id
                        }
                    ]
                }
            }
        }
    }
    return save_email_json


@shared_task(name='update_prices_in_cache')
def update_prices_in_cache():
    try:
        min_sku_price_service = UpdateMinPriceServiceSKU()
        min_sku_price_service.update_min_price_in_cache()
        min_sku_price_service.update_max_price_in_cache()
    except Exception as err:
        logger.exception("Couldn't update new sku price %s", str(err))
