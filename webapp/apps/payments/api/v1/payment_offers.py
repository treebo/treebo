# -*- coding:utf-8 -*-
"""
    payment_detailsPaymentInfo  API
"""
from base.logging_decorator import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from apps.common.api_response import APIResponse as api_response
from common.custom_logger.booking.log import get_booking_logger
from apps.payments.offers.services.payment_offer import PaymentOfferService
logger = get_booking_logger(__name__)


class PaymentOffers(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = []

    @log_args(logger)
    def get(self, request):
        try:
            utm_source = request.GET['utm_source']
            payment_offers = PaymentOfferService().get_offer_api_response(utm_source)
            return api_response.prep_success_response(payment_offers)

        except IndexError as exp:
            msg = "Wrong request received", exp
            logger.exception(msg)
            response = api_response.invalid_request_error_response(msg,
                                                                   api_name=PaymentOffers.__name__)
        except Exception as e:
            msg = "Failed to get payment offers for utm source {req_msg} with exception " \
                  "{exc}".format(req_msg=str(request.GET), exc=e)
            logger.exception(
                "Failed to get payment offers for utm source %s with exception %s ",
                request.GET, str(e))
            response = api_response.internal_error_response(msg, api_name=PaymentOffers.__name__)
        return response
