from django.db import models
from .intelligent_pah_categories import Category


class Occupancy(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    start = models.FloatField(blank=False)
    end = models.FloatField(blank=False)
    value = models.BooleanField(default=False)

    def __str__(self):
        return str(self.start) + " to " + \
            str(self.end) + " " + str(self.category)

    class Meta:
        verbose_name = "occupancy"
        verbose_name_plural = "occupancies"
