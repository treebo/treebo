# Task logger logs the task id and task name for each task

from celery.app import shared_task
from celery.signals import task_prerun
from celery.utils.log import get_task_logger

from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from apps.checkout.service.notification.bookingnotification import BookingNotificationService
from apps.common import metrics_logger
from apps.common.exceptions.payment_exception import InvalidTransactionException
from apps.common.slack_alert import SlackAlertService
from apps.fot.emailers import audit_confirmation
from apps.payments.service.razor_pay import RazorPayService
from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from base.filters import CeleryRequestIDFilter
from base.logging_decorator import log_args
from common.constants import common_constants

logger = get_task_logger(__name__)


# This is executed before every task. Here we can set the request_id and
# user_id in LogFilter
@task_prerun.connect()
def set_request_id(
        signal=None,
        sender=None,
        task_id=None,
        task=None,
        args=None,
        **kwargs):
    logger.addFilter(
        CeleryRequestIDFilter(
            kwargs.get('user_id'),
            kwargs.get('request_id'),
            task_id,
            task.name))
    logger.info(
        "set_request_id called with kwargs: %s, and task_id: %s",
        kwargs,
        task_id)


# log_args decorator should be added below the shared_task decorator.
# Ordering is important here.
@shared_task
@log_args(logger)
def add(x, y, user_id=None, request_id=None):
    return x + y


@shared_task
@log_args(logger)
def sleeptask(i, user_id=None, request_id=None):
    from time import sleep
    sleep(i)
    return i


@shared_task(max_retries=3)
@log_args(logger)
def capture_razorpay_payment(
        stored_total_amount,
        razorpay_secret,
        razorpay_id,
        razorpay_payment_id,
        user_id=None,
        request_id=None):
    try:
        razorpay_service = RazorPayService()
        razparpay_captured_amount = razorpay_service.get_razorpay_payment_details(
            razorpay_payment_id)
        razorpay_service.capture_razorpay_payment(
            razparpay_captured_amount, razorpay_payment_id, user_id, request_id)
    except Exception as e:
        logger.error(" capture_razorpay_payment task %s ", str(e))
        try:
            raise capture_razorpay_payment.retry(
                exc=InvalidTransactionException(), countdown=10)
        except InvalidTransactionException as ei:
            SlackAlertService.publish_alert(
                "RazorPay payment capture failed for payment_id %s " %
                (razorpay_payment_id))


@shared_task
@log_args(logger)
def send_audit_confirmation_mail(booking_id, referrerUrl=None):
    try:
        audit_confirmation(booking_id)
    except Exception as exc:
        logger.exception(
            "Exception occurred while creating FotReservationRequest")
        raise


@shared_task
@log_args(logger)
def send_booking_confirmation_notification(order_id):
    """
    Send special preference mail to Guest delight team and confirmation notification
    to guest
    :param order_id:
    :return:
    """

    try:
        booking_request = BookingRequest.objects.get(order_id=order_id)
        BookingNotificationService.sendSpecialPrefEmail(booking_request)

    except BookingRequest.DoesNotExist:
        logger.error("Unable to send notifications for order_id {}".format(order_id))

    except Exception as e:
        logger.exception(
            "Unable to send special preference mail to Guest delight team")
        metrics_logger.log(
            "Unable to special preference mail to Guest delight team", {
                "order_id": order_id})
    try:
        is_confirmation_notification_enable = FeatureToggleAPI.is_enabled(
            "booking", "confirmation_notification_enable", False)
        logger.info(
            "sending booking confirmation email for order %s ",
            order_id)
        if is_confirmation_notification_enable:
            if common_constants.SEND_SMS:
                BookingNotificationService().send_booking_confirmation_message(
                    booking_request)
            BookingHelper.sendEmail(booking_request)
            logger.info("Booking notifications sent")

    except Exception as e:
        logger.exception(
            "Unable to send booking confirmation email for order %s ",
            order_id)
        metrics_logger.log("Unable to send booking confirmation email", {
            "order_id": order_id
        })
