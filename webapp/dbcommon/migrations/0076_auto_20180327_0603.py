# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0075_auto_20180201_1000'),
    ]

    operations = [
        migrations.CreateModel(
            name='AmenitySeoDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('amenity_seo_title', models.CharField(max_length=500)),
                ('amenity_seo_description', models.TextField(null=True, blank=True)),
                ('amenity_seo_keyword', models.TextField(null=True, blank=True)),
                ('amenity', models.ForeignKey(to='dbcommon.Facility')),
                ('city', models.ForeignKey(to='dbcommon.City')),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'hotels_amenityseodetails',
                'verbose_name': 'Amenity SEO Details',
                'verbose_name_plural': 'Amenity SEO Details',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='CityAmenityMapforAutoPageCreation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('is_enabled', models.BooleanField(default=True)),
                ('amenity', models.ForeignKey(blank=True, to='dbcommon.Facility', null=True)),
                ('city', models.ForeignKey(to='dbcommon.City')),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'City_Amenity_AutoCreation',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AlterField(
            model_name='hotelmetadata',
            name='key',
            field=models.CharField(max_length=100, choices=[(b'b2b_blacklist', b'b2b_blacklist'), (b'sorted_value', b'sorted_value')]),
        ),
    ]
