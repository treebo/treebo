import $ from 'jquery';
import React from 'react';
import { render } from 'react-dom';
import 'parsleyjs';
import cookies from 'js-cookie';
import { _ } from 'underscore';
import BaseView from './treebolibs/baseview';
import treejax from './treebolibs/treejax';
import auth from './auth';
import '../css/referral.less';

const ReferralView = Object.create(BaseView);
const $loader = $('#commonLoader');
const contextData = $('#context-data').val() || '{}';
const referralData = JSON.parse(contextData);
const isReferralFlow = !_.isEmpty(referralData);


const ReferralBanner = ({ campaignText }) => (
    <div className="referral__gift">
        <i className="icon-gift referral__gift-icon"></i>
        <p className="referral__gift-text">{ campaignText }</p>
    </div>
);

class FormLogin extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.props.form;
    }

    componentDidMount() {
        $('#signup-form').parsley().on('form:submit', (e) => {
            e.submitEvent.preventDefault();
            this.props.setFormDetails(this.state);
            this.props.signUp();
            return false;
        });
    }

    handleFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        return (
            <form id="signup-form">
                <div id="signup-error" className="error"></div>
                <div className="float-labels">
                    <input onChange={this.handleFieldChange}
                           className="signup__full float-labels__input" name="name" type="text"
                           placeholder="Name" pattern=".+" required="true" data-parsley-required="true"
                           data-parsley-required-message="Please enter name here" value={this.state.name}
                    />
                    <label className="float-labels__label">Name</label>
                </div>

                <div className="login__full float-labels">
                    <input onChange={this.handleFieldChange}
                           className="signup__full float-labels__input" name="mobile" pattern=".+"
                           value={this.state.mobile} placeholder="Mobile Number" data-parsley-type="number"
                           data-parsley-trigger="change" data-parsley-type-message="Enter valid mobile number"
                           data-parsley-required-message="Please enter valid mobile number" type="number"
                           data-parsley-length-message="Please enter 10 digit mobile number"
                           data-parsley-minlength="10" data-parsley-maxlength="10" required="true"
                    />
                    <label className="float-labels__label">Mobile Number</label>
                </div>
                <div className="float-labels">
                    <input onChange={this.handleFieldChange}
                           className="signup__full float-labels__input" name="email"
                           type="email" pattern=".+" value={this.state.email} placeholder="Email ID"
                           required="true" data-parsley-type="email" data-parsley-trigger="change"
                           data-parsley-type-message="Enter valid email"
                           data-parsley-required-message="Please enter a valid email address"
                    />
                    <label className="float-labels__label">Email ID</label>
                </div>
                <div className="float-labels">
                    <input onChange={this.handleFieldChange}
                           className="signup__full float-labels__input" name="password"
                           pattern=".+" placeholder="Password" required="true"
                           data-parsley-minlength="6" type="password" value={this.state.password}
                           data-parsley-required-message="Oops! You forgot to enter your password"
                    />
                    <label className="float-labels__label">Password</label>
                </div>
                <button type="submit" className="btn signup__full signup__button">
                    Signup
                </button>
            </form>
        );
    }
}

class SocialLogin extends React.Component {
    constructor(props) {
        super(props);
        this.googleSignup = this.props.googleSignup;
        this.fbSignup = this.props.fbSignup;
    }

    componentDidMount () {
        auth.getGoogleHandler().then((res) => {
            res.attachClickHandler('googleSignup', {}, this.googleSignup.bind(this));
        });
    }

    render() {
        return (
            <div className="referral__social">
                <button className="btn login__facebook btn--facebook" onClick={this.fbSignup}>
                    <div className="btn__flex-wrapper">
                        <i className="icon-fb"></i><span className="social-pipe">|</span><span>FACEBOOK</span>
                    </div>
                </button>
                <button className="btn login__google js-google-login btn--google" id="googleSignup">
                    <div className="btn__flex-wrapper">
                        <i className="icon-google"></i>
                        <span className="social-pipe">|</span>
                        <span>GOOGLE</span>
                    </div>
                </button>
            </div>
        );
    }
}

const OtpScreen = ({ handleFieldChange, resendOtp, val, mobile, editNumber }) => {
    return (<div className="otp__container">
        <h2 className="otp__title">Enter your One Time Password</h2>
        <p className="otp__subtitle">
            OTP sent to <strong>{mobile}</strong>
            <small onClick={editNumber} className="otp__edit"> change number</small>
        </p>
        <div className="float-labels">
            <input className="float-labels__input otp__code" onChange={handleFieldChange}
                   pattern=".+" required="true" data-parsley-type="number"
                   data-parsly-trigger="change" data-parsley-type-message="Enter 4 digit code"
                   data-parsley-required="true" data-parsley-required-message="Please enter OTP here"
                   name="otp" type="number" placeholder="Enter OTP" value={val} />
        </div>
        <p className="otp__text">
            Did not receive? <a href="#" className="otp__action" onClick={resendOtp} >Resend code</a>
        </p>
    </div>);
};

const NumScreen = ({ handleFieldChange, val }) => {
    return (
        <div className="otp__container">
            <h2 className="otp__title">Enter Mobile Number</h2>
            <p className="otp__text">We will send an SMS with an OTP code</p>
            <div className="float-labels">
                <input className="float-labels__input otp__phone" onChange={handleFieldChange}
                       pattern=".+" required="true" data-parsley-type="number"
                       data-parsley-trigger="change" data-parsley-type-message="Enter valid mobile number"
                       data-parsley-required-message="Please enter a valid mobile number"
                       data-parsley-length-message="Please enter a 10 digit mobile number"
                       data-parsley-minlength="10" data-parsley-maxlength="10"
                       name="mobile" type="number" placeholder="Enter Mobile number" value={val} />
            </div>
        </div>
    );
};

class Otp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mobile: this.props.form.mobile,
        };
    }

    componentDidMount() {
        $('#otp-form').parsley().on('form:submit', (e) => {
            e.submitEvent.preventDefault();
            this.props.updateOtp(this.state);
            return false;
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ mobile: nextProps.form.mobile });
    }

    handleFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    render() {
        const { showPhone, resendOtp, showOtp, editNumber } = this.props;
        const { mobile, otp } = this.state;
        return (
            <div className={`modal flex-column otp ${!showOtp ? 'hide' : ''}`}>
                <div className="modal__body">
                    <form id="otp-form">
                        {
                            showPhone ?
                                <NumScreen handleFieldChange={this.handleFieldChange} val={mobile} /> :
                                <OtpScreen
                                    handleFieldChange={this.handleFieldChange}
                                    resendOtp={resendOtp} val={otp} mobile={mobile}
                                    editNumber={editNumber}
                                />
                        }
                        <button type="submit" className="btn btn--block btn--large">Done</button>
                    </form>
                </div>
            </div>
        );
    }
}

class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            showOtp: false,
            showPhone: false,
            form: {
                name: '',
                mobile: '',
                email: '',
                password: '',
                referral: referralData.referral_code,
            },
        };
    }

    setFormDetails = (data) => {
        this.setState({
            form: {
                ...this.state.form,
                ...data,
            }
        });
    };

    signUp = () => {
        const $error = $('#signup-error');
        $error.html('');

        $loader.removeClass('hide');
        auth.signup(this.state.form).then(() => {
            if (!isReferralFlow) {
                window.location = '/';
            } else {
                this.setState({ showOtp: true });
            }
        }, (err) => {
            if (err.redirect) {
                window.location = '/';
            } else {
                $error.html(err.message);
            }
        }).finally(() => {
            $loader.addClass('hide');
        });
    };

    updatePhone = (mobile) => {
        $loader.removeClass('hide');
        treejax({
            url: '/api/v1/auth/otp/',
            type: 'POST',
            data: {
                mobile,
                csrfmiddlewaretoken: cookies.get('csrftoken'),
            }
        }).then(() => {
            this.setState({
                form: {
                    ...this.state.form,
                    mobile
                },
                showPhone: false
            });
        }).finally(() => {
            $loader.addClass('hide');
        });
    };

    confirmOtp = (otp) => {
        $loader.removeClass('hide');
        treejax({
            url: '/api/v1/auth/verify/',
            type: 'POST',
            data: {
                otp,
                mobile: this.state.form.mobile,
                csrfmiddlewaretoken: cookies.get('csrftoken'),
            }
        }).then(() => {
            window.location = '/';
        }).catch((err) => {
            throw err;
        }).finally(() => {
            $loader.addClass('hide');
        });
    };

    updateOtp = ({ mobile, otp }) => {
        if(this.state.showPhone) {
            this.updatePhone(mobile);
        } else {
            this.confirmOtp(otp);
        }
    };

    resendOtp = () => {
        this.updatePhone(this.state.form.mobile);
    };

    googleSignup = (googleUser) => {
        auth.sendGoogleRegistrationDataToServer(googleUser.getAuthResponse().id_token)
            .then(({ register }) => {
                if(!register || !isReferralFlow) {
                    window.location = '/';
                } else {
                    this.setState({ showOtp: true, showPhone: true });
                }
            });
    };

    fbSignup = () => {
        auth.fbLogin().then(({ register }) => {
            if(!register || !isReferralFlow) {
                window.location = '/';
            } else {
                this.setState({ showOtp: true, showPhone: true });
            }
        });
    };

    editNumber = () => {
        this.setState({ showPhone: true });
    };

    openLogin = () => {
        $('.js-headerlogin').trigger('click');
    };

    render() {
        return (
            <div className="referral">
                <div className="referral__login">
                    <h2 className="referral__title">Signup with Treebo</h2>
                    <h3 className="referral__label">Easily using</h3>
                    <SocialLogin googleSignup={this.googleSignup} fbSignup={this.fbSignup} />
                    <h3 className="referral__label">Or using email</h3>
                    <FormLogin form={this.state.form} signUp={this.signUp} setFormDetails={this.setFormDetails} />
                    {
                        isReferralFlow ?
                            <Otp
                                resendOtp={this.resendOtp} updateOtp={this.updateOtp}
                                form={this.state.form} editNumber={this.editNumber}
                                showOtp={this.state.showOtp} showPhone={this.state.showPhone}
                            /> :
                            null
                    }
                    <p className="referral__text">
                        Already have an account? <a className="referral__link" onClick={this.openLogin}>Login!</a>
                    </p>
                </div>
            </div>
        );
    }
}

class App extends React.Component {
    render() {
        return (
            <div>
                <Login />
            </div>
        );
    }
}

const methods = {
    init() {
        render(
            <App />, document.getElementById('referral-content')
        );
    },
};

ReferralView.pageLoaded = () => {
    rt.page = isReferralFlow ? 'referral' : '';
    methods.init();
};

ReferralView.setup({ name: 'Referral Page', widgets: [] });
