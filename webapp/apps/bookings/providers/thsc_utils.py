# pylint: disable=invalid-name
import functools
import logging
import sys

from django_currentuser.middleware import get_current_user
from ths_common.constants.billing_constants import ChargeBillToTypes, ChargeTypes, PaymentModes, PaymentStatus, \
    PaymentChannels, PaymentTypes, PaymentReceiverTypes
from ths_common.constants.booking_constants import AgeGroup
from ths_common.exceptions import CRSException
from thsc.crs import context as thsc_context
from thsc.crs.entities.billing import Payment as THSCPayment
from thsc.crs.entities.booking import GuestStay
from thsc.crs.entities.booking import Price
from thsc.crs.entities.customer import Customer

from apps.auth.services.user_profile_service import UserProfileService
from apps.bookings.constants import THSC_CONTEXT_DEFAULT_APPLICATION, THSC_CONTEXT_USER
# from common.utils.misc import get_request_id
from apps.bookings.data_classes.guest import Adult, Child, Infant
from apps.bookings.data_classes.payment_type import (WebPrepaid, PayAtHotel)
from apps.bookings.data_classes.payments import (PrepaidPaymentMode, ConfirmedPaymentStatus,
                                                 CancelledPaymentStatus, NormalPaymentType,
                                                 RefundPaymentType, WalletPaymentMode,
                                                 PaymentOfferPaymentMode)
from apps.bookings.models import Booking as BookingModel
from apps.bookings.providers.exceptions import TreeboCRSException

logger = logging.getLogger(__name__)


class ThscUtils(object):

    @classmethod
    def thsc_prices_from_prices(cls, prices, payment_type):
        bill_to, credit_type = cls.get_bill_to_and_credit_type_from_payment_type(payment_type)

        thsc_prices = [Price(pretax_amount=price.pre_tax,
                             applicable_date=price.date,
                             bill_to_type=bill_to,
                             type=credit_type,
                             )
                       for price in prices]
        return thsc_prices

    @classmethod
    def thsc_guest_stay_from_guest(cls, guest):
        thsc_customer = cls._thsc_customer_from_guest(guest)
        age_group = cls._age_group_from_guest(guest)
        guest_stay = GuestStay(age_group=age_group)
        if thsc_customer:
            guest_stay.guest = thsc_customer
        return guest_stay

    @classmethod
    def thsc_guest_stay_from_guest_stay(cls, guest_stay):
        thsc_guest_stay = cls.thsc_guest_stay_from_guest(guest_stay.guest)
        thsc_guest_stay.checkin_date = guest_stay.checkin
        thsc_guest_stay.checkout_date = guest_stay.checkout
        return thsc_guest_stay

    @classmethod
    def _thsc_customer_from_guest(cls, guest):
        if not guest.first_name:
            return

        thsc_customer = Customer(first_name=guest.first_name)

        if guest.last_name:
            thsc_customer.last_name = guest.last_name

        if guest.email:
            thsc_customer.email = guest.email

        if guest.phone_number:
            thsc_customer.phone_number = guest.phone_number
            ups_user_id = UserProfileService().get_user_id(phone_number=thsc_customer.phone_number)
            ups_user_profile_id = UserProfileService().get_user_profile_id(ups_user_id)
            thsc_customer.reference_id = ups_user_id
            thsc_customer.user_profile_id = ups_user_profile_id

        return thsc_customer

    @classmethod
    def thsc_bill_to_from_payment_type(cls, payment_type):
        mapping = {
            WebPrepaid: ChargeBillToTypes.GUEST,
            PayAtHotel: ChargeBillToTypes.GUEST,
        }

        return mapping[payment_type]

    @classmethod
    def thsc_credit_type_from_payment_type(cls, payment_type):
        mapping = {
            WebPrepaid: ChargeTypes.NON_CREDIT,
            PayAtHotel: ChargeTypes.NON_CREDIT,
        }

        return mapping[payment_type]

    @classmethod
    def get_bill_to_and_credit_type_from_payment_type(cls, payment_type):
        bill_to = cls.thsc_bill_to_from_payment_type(payment_type)
        credit_type = cls.thsc_credit_type_from_payment_type(payment_type)

        return bill_to, credit_type

    @classmethod
    def set_thsc_context(cls, application):
        current_user = get_current_user()
        thsc_context.user = THSC_CONTEXT_USER
        if current_user:
            thsc_context.email = current_user.email
            thsc_context.user = current_user
        thsc_context.application = application if application else THSC_CONTEXT_DEFAULT_APPLICATION
        return thsc_context

    @classmethod
    def _age_group_from_guest(cls, guest):
        if guest.age in Adult:
            return AgeGroup.ADULT
        elif guest.age in Child:
            return AgeGroup.CHILD
        elif guest.age in Infant:
            return AgeGroup.INFANT
        logger.warning('Invalid age: {}'.format(guest.age))
        return AgeGroup.ADULT

    @classmethod
    def get_thsc_payment_mode_from_payment_mode(cls, payment_mode):
        if payment_mode == PrepaidPaymentMode:
            return PaymentModes.PAYMENT_SERVICE
        if payment_mode == WalletPaymentMode:
            return PaymentModes.TREEBO_POINTS
        if payment_mode == PaymentOfferPaymentMode:
            return PaymentModes.OTHER

        else:
            raise ValueError('Invalid payment mode:  {p}'.format(p=payment_mode))

    @classmethod
    def get_payment_mode_from_thsc_payment_mode(cls, thsc_payment_mode):
        if thsc_payment_mode == PaymentModes.TREEBO_POINTS:
            return WalletPaymentMode
        if thsc_payment_mode == PaymentModes.OTHER:
            return PaymentOfferPaymentMode
        else:
            return PrepaidPaymentMode

    @classmethod
    def get_thsc_payment_status_from_payment_status(cls, payment_status):
        if payment_status == ConfirmedPaymentStatus:
            return PaymentStatus.DONE
        elif payment_status == CancelledPaymentStatus:
            return PaymentStatus.CANCELLED
        return PaymentStatus.PENDING

    @classmethod
    def get_payment_status_from_thsc_payment_status(cls, thsc_payment_status):
        if thsc_payment_status == PaymentStatus.DONE or thsc_payment_status == PaymentStatus.POSTED:
            return ConfirmedPaymentStatus
        elif thsc_payment_status == PaymentStatus.CANCELLED:
            return CancelledPaymentStatus
        else:
            raise ValueError('Invalid payment status:  {p}'.format(p=thsc_payment_status))

    @classmethod
    def get_thsc_payments_from_payments(cls, payments, refund=False):
        return [THSCPayment(amount=payment.amount,
                            date_of_payment=payment.date,
                            payment_ref_id=payment.reference_id,
                            payment_channel=PaymentChannels.ONLINE,
                            payment_type=PaymentTypes.PAYMENT if not refund else PaymentTypes.REFUND,
                            payment_mode=ThscUtils.get_thsc_payment_mode_from_payment_mode(payment.mode),
                            paid_to=PaymentReceiverTypes.TREEBO,
                            paid_by=PaymentReceiverTypes.GUEST,
                            status=ThscUtils.get_thsc_payment_status_from_payment_status(payment.status),
                            )
                for payment in payments]

    @classmethod
    def get_payment_type_from_thsc_payment_type(cls, thsc_payment_type):
        if thsc_payment_type == PaymentTypes.PAYMENT:
            return NormalPaymentType
        elif thsc_payment_type == PaymentTypes.REFUND:
            return RefundPaymentType
        else:
            raise ValueError('Invalid payment type: {p}'.format(p=thsc_payment_type))


def thsc_exception_transformer(function):
    """ This is required since CRSException does not set message every time which is expected by many packages"""

    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        try:
            function(*args, **kwargs)
        except CRSException as e:
            # raises TreeboCRSException with same traceback as CRSException.
            # pylint: disable=old-raise-syntax
            raise TreeboCRSException(e.extra_payload, e.message)(None).with_traceback(sys.exc_info()[2])

    return wrapper


def get_extra_information_from_booking(booking):
    return {'payment_type': booking.payment_type.name,
            'pricing_scheme': booking.pricing_scheme.name()}


def crs_booking_to_booking_status_mapping():
    return {
        "temporary": BookingModel.TEMP_BOOKING_CREATED,
        "reserved": BookingModel.RESERVED,
        "confirmed": BookingModel.RESERVED,
        "cancelled": BookingModel.CANCEL
    }
