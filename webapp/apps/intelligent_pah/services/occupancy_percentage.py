from apps.bookingstash.models import Availability
from django.db.models import Min

from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository
from data_services.room_repository import RoomRepository
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room


class GetOccupancyPercentage():

    def get_occupancy_percentage_v2(
            self,
            hotel_id,
            check_in_date,
            check_out_date):
        # total_rooms = hotel_repository.get_hotel_by_id(
        #     hotel_id=hotel_id).room_count
        total_rooms = Hotel.objects.filter(id=hotel_id).first().room_count
        rooms = Room.objects.filter(hotel_id=hotel_id)
        # room_repository.filter_rooms(hotel_id=hotel_id)
        #room_ids = Room.objects.filter(hotel_id= hotel_id)
        available_rooms = 0
        for room in rooms:
            available_rooms_as_per_type = Availability.objects.filter(
                room_id=room.id, date=check_in_date).first()
            available_rooms = available_rooms + available_rooms_as_per_type.availablerooms
        net_occupied = total_rooms - available_rooms
        percentage_occupied = float(net_occupied) / float(total_rooms)
        return percentage_occupied * 100
