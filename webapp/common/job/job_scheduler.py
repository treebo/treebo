from common.job.job_entity import JobEntity
from common.job.job_repository import JobRepository
from common.job.job_writer import JobWriter


class JobScheduler(object):
    def __init__(self):
        self.job_writer = JobWriter()
        self.job_repository = JobRepository()

    def schedule(self, job_entity):
        job_entity = self.job_writer.write(job_entity)
        return job_entity

    def schedule_soft_booking_notification(self, crs_booking_id, eta):
        job_entity = JobEntity(
            job_name='SOFT_BOOKING_PAYMENT_DROPOFFS_NOTIFICATION',
            data=dict(booking_id=crs_booking_id),
            eta=eta,
            crs_booking_id=crs_booking_id
        )
        if self.job_repository.is_booking_present(crs_booking_id):
            return
        self.schedule(job_entity)
