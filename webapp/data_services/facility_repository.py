import logging

logger = logging.getLogger(__name__)

class FacilityRepository:
    def __init__(self, facility_web_repository, cs_api_client, is_cs_api_querying_enabled=True):
        self.cs_api_client = cs_api_client
        self.is_cs_api_querying_enabled = is_cs_api_querying_enabled

    def get_facilities_for_hotel(self, hotel_id):
        return self.cs_api_client.get_facilities_from_hotel_id(hotel_id)
