import logging
from django.core.cache import cache
from django.db.models import Q
from django.template.defaultfilters import slugify

from apps.search.services.auto_completion_service import AutoCompletionService
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)


class LandmarkBasedAutoCompletionService(AutoCompletionService):
    CACHE_TIMEOUT = 24 * 60 * 60

    def apply(self, search_string, **kwargs):
        try:
            result = cache.get('autocomplete_cache_landmark_' + slugify(search_string))
        except Exception as e:
            logger.exception("Exception in getting cache value for " +
                             'autocomplete_cache_landmark_' + slugify(search_string))
            result = None
        if result:
            return result

        search_string_arr = search_string.split(" ")
        landmark_query = (Q(search_label__istartswith=search_string))
        landmark_query |= (Q(search_label__icontains=' '+search_string))
        for string_count in range(1, len(search_string_arr)):
            curr_search_string = ' '.join(search_string_arr[0:len(search_string_arr)-string_count])
            landmark_query |= (Q(search_label__istartswith=curr_search_string))
            landmark_query |= (Q(search_label__icontains=' '+curr_search_string))

        landmark_data_service = RepositoriesFactory.get_landmark_repository()
        landmarks = landmark_data_service.filter_landmarks_for_query(query=landmark_query)

        landmark_results = AutoCompleteResultBuilder.build_landmark_result(
            landmarks)

        if landmark_results:
            cache.set('autocomplete_cache_landmark_' + slugify(search_string),
                      landmark_results, LandmarkBasedAutoCompletionService.CACHE_TIMEOUT)

        return landmark_results
