import logging

from django.db import models
from djutil.models import TimeStampedModel

from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class FeedbackPositives(TimeStampedModel):
    name = models.TextField(null=True, blank=True, unique=True)

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    class Meta:
        db_table = 'seo_feedbackpositives'
        verbose_name = 'Feedback Positives'
        verbose_name_plural = 'Feedback Positives'


class PositivesCategory(TimeStampedModel):
    name = models.TextField(null=True, blank=True, unique=True)

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    class Meta:
        db_table = 'seo_positivescategory'
        verbose_name = 'Positives Category'
        verbose_name_plural = 'Positives Category'


class PositiveCategoryContentMapping(TimeStampedModel):
    positive = models.ForeignKey(
        FeedbackPositives,
        null=True,
        blank=True,
        on_delete=models.CASCADE)
    category = models.ForeignKey(
        PositivesCategory,
        null=True,
        blank=True,
        on_delete=models.CASCADE)
    content = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return str(self.positive.name) + "-" + \
            str(self.category.name) + "-" + str(self.content)

    def __str__(self):
        return str(self.positive.name) + "-" + \
            str(self.category.name) + "-" + str(self.content)

    class Meta:
        unique_together = ('positive', 'category', 'content')
        db_table = 'seo_positivecategorycontentmapping'
        verbose_name = 'Positives Category Content Mapping'
        verbose_name_plural = 'Positives Category Content Mapping'


class FeatureToggle(TimeStampedModel):
    num_of_days = models.IntegerField(
        default=180,
        help_text='Enter the number of days for which positives have to be fetched')
    num_of_positives_shown = models.IntegerField(
        default=4, help_text='Enter the number of positives that we have to show on FE')
    username = models.CharField(max_length=100, default='USERNAME')
    password = models.CharField(max_length=100, default='PASSWORD')
    restaurant_dist_cap = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        help_text='distance cap for restaurants nearby',
        default=20)
    closest_landmark_dist_cap = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        help_text='distance cap for most popular landmarks',
        default=20)
    closest_landmark_number_cap = models.IntegerField(
        default=5,
        help_text='max number of poi entries that should be shown for closest landmark')
    most_popular_landmark_number_cap = models.IntegerField(
        default=5,
        help_text='max number poi of entries that should be shown for most popular landmark')
    restaurants_number_cap = models.IntegerField(
        default=5,
        help_text='max number of poi entries that should be shown for restaurants and bar')
    airport_number_cap = models.IntegerField(
        default=5,
        help_text='max number of poi entries that should be shown for airport and railway')

    class Meta:
        db_table = 'seo_featuretoggle'
        verbose_name = 'SEO Feature Toggle'
        verbose_name_plural = 'SEO Feature Toggle'


class HotelContentCountMapping(TimeStampedModel):
    hotel = models.ForeignKey(
        Hotel,
        null=True,
        blank=True,
        on_delete=models.CASCADE)
    feedback_content = models.ForeignKey(
        PositiveCategoryContentMapping,
        null=True,
        blank=True,
        on_delete=models.CASCADE)
    count = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return str(self.hotel.name) + "-" + \
            str(self.feedback_content.positive.name) + "-" + str(self.count)

    def __str__(self):
        return str(self.hotel.name) + "-" + \
            str(self.feedback_content.positive.name) + "-" + str(self.count)

    class Meta:
        db_table = 'seo_hotelcontentcountmapping'
        verbose_name = 'Hotel Content Count Mapping'
        verbose_name_plural = 'Hotel Content Count Mapping'
