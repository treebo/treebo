import datetime
from bs4 import BeautifulSoup


# from celery import shared_task
# from celery.signals import task_prerun
# from celery.utils.log import get_task_logger
from collections import defaultdict
from django.db.models import Q, Sum, Max

import apps.pricing.utils as priceUtils
from apps.pricing import dateutils
from apps.pricing import pricing_api_impl
from apps.pricing.event_publishers import publish_price, publish_prices
from apps.pricing.models import ChannelManagerRatePlan, RoomPriceV2, StateTax, LuxuryTax
from apps.pricing.utils import PriceUtils
from base import log_args
from base.filters import CeleryRequestIDFilter
from common.utilities.date_time_utils import DATE_FORMAT
from dbcommon.models.hotel import Hotel

STANDARD_PRICE_INCREMENT = 300

# logger = get_task_logger(__name__)

# This is executed before every task. Here we can set the request_id and
# user_id in LogFilter


# @task_prerun.connect()
# def set_request_id(
#         signal=None,
#         sender=None,
#         task_id=None,
#         task=None,
#         args=None,
#         **kwargs):
#     logger.addFilter(
#         CeleryRequestIDFilter(
#             kwargs.get('user_id'),
#             kwargs.get('request_id'),
#             task_id,
#             task.name))
#     logger.info(
#         "set_request_id called with kwargs: %s, and task_id: %s",
#         kwargs,
#         task_id)


# @shared_task
# @log_args(logger)
def updatePrices(
        hotel_id,
        start_date_str,
        end_date_str,
        user_id=None,
        request_id=None):
    pass
    # try:
    #     hotel = Hotel.objects.get(id=hotel_id)
    #     date_wise_state_tax = {}
    #     start = dateutils.ymd_str_to_date(start_date_str)
    #     end = dateutils.ymd_str_to_date(end_date_str)
    #     start_temp = start
    #     while start < end:
    #         logger.info('Start Date in loopp: %s', start)
    #         taxObj = StateTax.objects.filter(Q(state=hotel.state) & Q(effective_date__lte=start) & (
    #             Q(expiry_date__gte=start) | Q(expiry_date__isnull=True))).aggregate(
    #             Sum('tax_value'))
    #         date_wise_state_tax[start] = taxObj['tax_value__sum']
    #         start = start + datetime.timedelta(days=1)
    #
    #     start = start_temp
    #     allRooms = hotel.rooms.all()
    #     for singleRoom in allRooms:
    #         maxAdults = singleRoom.max_guest_allowed
    #         occupancy = 1
    #         while occupancy <= maxAdults:
    #             PriceUtils.sync_roomprice_from_channelmanagerrateplan(
    #                 start, end, singleRoom, occupancy, date_wise_state_tax)
    #             occupancy += 1
    # except Exception as e:
    #     logger.exception("Update Price task failed")
    #     raise e
    # return True


# @shared_task
def updateRatePlanFromChannelManager(
        xmlRatePlanRequest,
        request_id=None,
        user_id=None):
    pass
    # xmlRequest = BeautifulSoup(xmlRatePlanRequest, 'xml')
    # ratePlanRq = xmlRequest.find('OTA_HotelRateAmountNotifRQ')
    # logger.debug('ChannelManagerRatePlan XML: %s', ratePlanRq)
    # rateAmountMessages = xmlRequest.find('RateAmountMessages')
    # hotelCode = int(rateAmountMessages['HotelCode'])
    # hotel = Hotel.objects.get(id=hotelCode)
    # hotelRoomDict = {}
    # for room in hotel.rooms.all():
    #     hotelRoomDict[room.room_type_code] = room
    # allRateAmountMessages = xmlRequest.findAll('RateAmountMessage')
    # for rateAmountMessage in allRateAmountMessages:
    #     statusApplicationControl = rateAmountMessage.find(
    #         'StatusApplicationControl')
    #     roomTypeCode = statusApplicationControl['InvTypeCode']
    #     room = hotelRoomDict[roomTypeCode]
    #     start_date_str = statusApplicationControl['Start']
    #     end_date_str = statusApplicationControl['End']
    #     allBaseAmountTags = rateAmountMessage.findAll('BaseByGuestAmt')
    #     singleAmount = None
    #     doubleAmount = None
    #     for baseAmountTag in allBaseAmountTags:
    #         if baseAmountTag['NumberOfGuests'] == '1':
    #             amountAfterTax = baseAmountTag.get('AmountAfterTax')
    #             amountBeforeTax = baseAmountTag.get('AmountBeforeTax')
    #             if amountBeforeTax is None:
    #                 if amountAfterTax is None:
    #                     logger.info('Amount tag is missing')
    #                     continue
    #                 else:
    #                     singleAmount = amountAfterTax
    #             else:
    #                 singleAmount = amountBeforeTax
    #         elif baseAmountTag['NumberOfGuests'] == '2':
    #             amountAfterTax = baseAmountTag.get('AmountAfterTax')
    #             amountBeforeTax = baseAmountTag.get('AmountBeforeTax')
    #             if amountBeforeTax is None:
    #                 if amountAfterTax is None:
    #                     logger.info(
    #                         'Amount tag for double occupancy is missing')
    #                 else:
    #                     doubleAmount = amountAfterTax
    #             else:
    #                 doubleAmount = amountBeforeTax
    #     singleAmount = float(singleAmount)
    #
    #     # Add LuxuryTaxes to be passed to getTaxOnPrice method
    #     luxuryTaxes = LuxuryTax.objects.all()
    #     luxuryTaxDict = defaultdict(list)
    #     for luxuryTax in luxuryTaxes:
    #         luxuryTaxDict[luxuryTax.state_id].append(luxuryTax)
    #
    #     # TODO: startDate is being sent here, considering it as checkInDate.
    #     # Get taxObj to be passed to getTaxOnPrice method
    #     taxObj = StateTax.objects.filter(
    #         Q(state=hotel.state) & Q(effective_date__lte=start_date_str) & (
    #             Q(expiry_date__gte=start_date_str) | Q(expiry_date__isnull=True))).aggregate(
    #         Sum('tax_value'))
    #
    #     taxToBeApplied = taxObj.get('tax_value__sum', 0)
    #
    #     singleAmountTax = priceUtils.PriceUtils.getTaxOnPrice(
    #         taxToBeApplied, hotel, singleAmount, luxuryTaxDict.get(hotel.state_id, []))
    #     totalSingleAmount = singleAmount + singleAmountTax
    #     baseAmount = totalSingleAmount
    #     increment = STANDARD_PRICE_INCREMENT
    #
    #     if doubleAmount is not None:
    #         doubleAmount = float(doubleAmount)
    #         doubleAmountTax = priceUtils.PriceUtils.getTaxOnPrice(
    #             taxToBeApplied, hotel, doubleAmount, luxuryTaxDict.get(hotel.state_id, []))
    #         totalDoubleAmount = doubleAmount + doubleAmountTax
    #         increment = totalDoubleAmount - baseAmount
    #
    #     start_date = datetime.datetime.strptime(
    #         start_date_str, DATE_FORMAT).date()
    #     end_date = datetime.datetime.strptime(end_date_str, DATE_FORMAT).date()
    #     channelManagerRatePlan = ChannelManagerRatePlan()
    #     channelManagerRatePlan.hotel = hotel
    #     channelManagerRatePlan.room = room
    #     channelManagerRatePlan.is_active = True
    #     channelManagerRatePlan.start_date = start_date
    #     channelManagerRatePlan.end_date = end_date
    #     channelManagerRatePlan.base_price = baseAmount
    #     channelManagerRatePlan.price_increment = increment
    #     channelManagerRatePlan.save()
    #
    #     updatePrices(hotel.id, start_date_str, end_date_str, user_id=user_id,
    #                  request_id=request_id)


# @shared_task
# @log_args(logger)
# def update_rates_from_hx(request_data, user_id=None, request_id=None):
#     pricing_api_impl.update_rate_plan_from_hx(request_data)


# @shared_task
# @log_args(logger)
def sync_prices(
        hotel_id,
        start_date_str,
        end_date_str,
        user_id=None,
        request_id=None):
    pass
    # start_date = datetime.datetime.strptime(start_date_str, "%Y-%m-%d").date()
    # end_date = datetime.datetime.strptime(end_date_str, "%Y-%m-%d").date()
    # d = start_date
    # while d <= end_date:
    #     max_prices = RoomPriceV2.objects.filter(
    #         room__hotel_id=hotel_id,
    #         date=d).values(
    #         'room_id',
    #         'date',
    #         'guest_count').annotate(
    #         price_id=Max('id'))
    #     max_price_ids = [result['price_id'] for result in max_prices]
    #     logger.info("RoomPriceV2 ids to publish: %s", max_price_ids)
    #     room_prices = RoomPriceV2.objects.filter(id__in=max_price_ids)
    #     publish_price(hotel_id, room_prices)
    #     d = d + datetime.timedelta(days=1)
