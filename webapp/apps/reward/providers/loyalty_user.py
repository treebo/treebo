import logging

import requests
from django.conf import settings

from dbcommon.models.profile import User

logger = logging.getLogger(__name__)


class LoyaltyUser:
    def get_user_data(self, phone_number):
        loyalty_user_type_endpoint = settings.REWARD_URL + 'get-user-loyalty-type?auth_id={' \
                                                               'auth_id}&phone_number={' \
                                                               'phone_number}'.format(auth_id='', phone_number=phone_number)
        response = requests.get(loyalty_user_type_endpoint,  timeout=10)
        if response.status_code == 200:
            json_data = response.json()['data']['loyalty_user']
            data = dict(phone_number=json_data.get('phone_number'),
                        loyalty_type=json_data.get('loyalty_type'))
        else:
            logger.error("Unable to fetch loyalty type for {auth_id}".format(auth_id=User.auth_id))
            data = dict(phone_number=None,
                        loyalty_type=None)
        return data
