import datetime
import json
import logging

from django.conf import settings

from apps.reviews.services.trip_advisor.build_ta_response import BuildTAReviewResponse
logger = logging.getLogger(__name__)


class BaseCacheDataBuilder(object):

    '''builds the object to be saved in cache'''

    def __init__(self,):
        self.json = {}
        self.key = None
        self.page = None
        self.hotel_id = None

    def set_key(self, hotel_id, page):
        self.key = settings.REVIEW_CACHE_KEY_PREFIX + \
            '_' + str(page) + '_' + str(hotel_id)
        self.page = page
        self.hotel_id = hotel_id

    def build(self,):
        try:
            ta_review_obj = BuildTAReviewResponse(self.hotel_id)
            data = self.build_response(ta_review_obj)
            data['created_date'] = str(datetime.datetime.now().isoformat())
            self.json = json.dumps(data)
        except BaseException:
            logger.exception('Exception occured in cache data builder')

    def build_response(self, ta_review_obj):
        raise NotImplementedError
