import logging
from http import HTTPStatus
from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.checkout.service.itinerary_handler_v2 import ItineraryHandlerV2
from base.decorator.request_validator import validate_request
from base.views.api_v2 import TreeboAPIView2
from apps.common.api_response import APIResponse as api_response

logger = logging.getLogger(__name__)


class RemoveWalletSerializer(serializers.Serializer):
    bid = serializers.IntegerField()


class RemoveWallet(TreeboAPIView2):
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    checkout_service = ItineraryHandlerV2()

    @validate_request(RemoveWalletSerializer)
    def post(self, request, *args, **kwargs):
        """
        Gets the discount for a booking id and coupon code.
        bid -- BookingRequest Id
        """

        try:
            validated_data = kwargs['validated_data']
            bid = validated_data['bid']
            price_data = self.checkout_service.select_wallet(
                request, bid, apply_wallet=False)
            response = api_response.prep_success_response(price_data)
        except Exception as exception:
            logger.exception('Exception occurred in remove wallet')
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param=request.GET,
                                                        dev_msg="Remove Wallet",
                                                        message=exception.__str__(),
                                                        class_name=self.__class__.__name__)
            response = api_response.prep_simple_error_response(msg="There was an error on removing wallet.",
                                                               status_code=400, code=400)
        return response
