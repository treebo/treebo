import datetime
import logging

from bs4 import BeautifulSoup
from django.conf import settings
from django.db.models import Max
from kombu import Connection, Exchange, Queue
from kombu.pools import connections

from data_services.respositories_factory import RepositoriesFactory
from apps.bookingstash.models import HotelRoomMapper
from apps.pricing import event_publishers
from apps.pricing.models import RoomPriceV2
from common.exceptions.treebo_exception import TreeboValidationException, TreeboException
from apps.common import date_time_utils
from common.services.feature_toggle_api import FeatureToggleAPI
from collections import defaultdict
from base.logging_decorator import log_args
from data_services.room_repository import RoomRepository
from common.utilities.queue_utils import get_rabbit_mq_heartbeat_interval, get_rabbit_mq_retry_policy, \
    get_rabbit_mq_transport_options
from . import models
from .dateutils import date_range, ymd_str_to_date
from .promotion import DiscountUtils, TaxUtils

HTNG_CHILD_CODE = 8

HTNG_ADULT_CODE = 10

RATE_PLAN_CODE = 'RP001'  # RackRate Plancode as per maximojo contract

logger = logging.getLogger(__name__)

delta = datetime.timedelta(days=1)
room_data_service = RepositoriesFactory.get_room_repository()


class PriceUtilsV2:
    def __init__(self):
        pass

    @staticmethod
    @log_args(logger=logger, only_on_exception=True)
    def getRoomPriceForDateRange(
            hotel,
            start_date,
            end_date,
            room,
            occupancy,
            coupon_percent=None):
        booking_date = datetime.date.today()

        if isinstance(start_date, str):
            start_date = ymd_str_to_date(start_date)
        if isinstance(end_date, str):
            end_date = ymd_str_to_date(end_date)
        occupancy = int(occupancy)
        occupancy = 1 if occupancy <= 0 else occupancy
        date_wise_prices = {}
        total_prices = {
            'sell': 0.0,
            'tax': 0.0,
            'pretax_rackrate': 0.0,
            'rack_rate': 0.0,
            'pretax': 0.0,
            'discount': 0.0}
        promotion_applied_final = False
        luxury_tax = TaxUtils.get_luxury_taxes_for_state(hotel.state.id)
        promotions = DiscountUtils.get_promos_for_hotel(hotel, booking_date)
        promotions = DiscountUtils.filter_applicable_promos(
            promotions, room.room_type_code, booking_date, start_date, end_date)
        for current_stay_date in date_range(start_date, end_date):
            state_tax = TaxUtils.get_state_tax_for_date(
                current_stay_date, hotel.state.id)
            room_price = models.RoomPriceV2.objects.filter(
                room_id=room.id, date=current_stay_date, guest_count=occupancy).select_related(
                "room", "room__hotel").order_by('-id').first()
            if not room_price:
                room_price = RoomPriceV2()
                room_price.base_price = room.price + \
                    (hotel.price_increment * (occupancy - 1))
                room_price.room = room
                room_price.guest_count = occupancy
                room_price.date = current_stay_date
            price_details, promotion_applied = PriceUtilsV2.__get_room_price_with_auto_promotion_coupon_tax(
                current_stay_date, room_price, state_tax, luxury_tax, promotions, coupon_percent)
            if not promotion_applied_final:
                promotion_applied_final = promotion_applied
            date_wise_prices[current_stay_date] = price_details
            total_prices['sell'] += price_details['sell']
            total_prices['tax'] += price_details['tax']
            total_prices['rack_rate'] += price_details['rack_rate']
            total_prices['pretax'] += price_details['pretax']
            total_prices['pretax_rackrate'] += price_details['pretax_rackrate']
            total_prices['discount'] += price_details['discount']

        return total_prices, date_wise_prices, promotion_applied_final

    # Used for Pricing on Search Page
    @staticmethod
    @log_args(logger=logger, only_on_exception=True)
    def getPriceForMultipleHotelsWithRoomConfigDateRange(
            checkin, checkout, hotels, room_config, cheapest_room_types):
        hotel_id_wise_prices = {}
        for hotel in hotels:
            hotel_id_wise_prices[hotel.id] = {
                'pretax': 0.0, 'tax': 0.0, 'sell': 0.0, 'rack_rate': 0.0}
        hotel_state_dict = {hotel: hotel.state.id for hotel in hotels}
        states = set(hotel_state_dict.values())
        checkin_date = checkin.date()
        checkout_date = checkout.date()
        luxury_tax_by_state_wise = TaxUtils.get_luxury_taxes_by_states_wise(
            states)
        tax_by_hotel_date_wise = TaxUtils.get_state_tax_by_hotel_date_wise(
            checkin_date, checkout_date, hotel_state_dict)
        hotel_ids = [hotel.id for hotel in hotels]
        promotions = DiscountUtils.get_promos_for_hotels_group_by(
            hotel_ids, datetime.date.today())
        occupancy_list = [config[0] for config in room_config]
        date_occupancy_date_hotel_price_map = PriceUtilsV2.__for_each_hotel_date_range_find_cheapest_roomprice(
            checkin_date, checkout_date, hotels, cheapest_room_types, occupancy_list)
        for stay_date in date_range(checkin_date, checkout_date):
            checkin_str = stay_date.strftime(date_time_utils.DATE_FORMAT)
            hotel_to_room_price_map_for_date = date_occupancy_date_hotel_price_map[stay_date]
            stay_date_state_tax = tax_by_hotel_date_wise[stay_date]
            hotel_id_wise_prices_for_date = PriceUtilsV2.getPriceForMultipleHotelsWithRoomConfig_For_Date(
                stay_date,
                hotels,
                room_config,
                cheapest_room_types,
                stay_date_state_tax,
                luxury_tax_by_state_wise,
                hotel_to_room_price_map_for_date,
                promotions)  # TODO see if it can be optimized
            for hotel_id, price_dict in list(
                    hotel_id_wise_prices_for_date.items()):
                prices = hotel_id_wise_prices[hotel_id]
                prices['pretax'] += price_dict['pretax']
                prices['tax'] += price_dict['tax']
                prices['sell'] += price_dict['sell']
                prices['rack_rate'] += price_dict['rack_rate']
                breakup = {
                    checkin_str: {
                        'pretax': price_dict['pretax'],
                        'tax': price_dict['tax'],
                        'sell': price_dict['sell'],
                        'rack_rate': price_dict['rack_rate']}}
                if 'breakup' not in prices:
                    prices['breakup'] = {}
                prices['breakup'].update(breakup)
            logger.debug(
                "Hotel Wise Price after checkInDate: %s == %s",
                stay_date,
                hotel_id_wise_prices)

            for hotel in hotels:
                hotel_id_wise_prices[hotel.id]["discount_percent"] = int(
                    ((hotel_id_wise_prices[hotel.id]["rack_rate"] - hotel_id_wise_prices[hotel.id]["sell"]) / hotel_id_wise_prices[hotel.id]["rack_rate"]) * 100)

        return hotel_id_wise_prices

    @staticmethod
    @log_args(logger=logger, only_on_exception=True)
    def getPriceForMultipleHotelsWithRoomConfig_For_Date(
            stay_date,
            hotels,
            room_config,
            cheapest_room_types,
            stay_date_state_tax,
            luxury_tax_by_state_wise,
            hotel_to_room_price_map_for_date,
            promotions,
            booking_date=datetime.date.today()):
        hotel_wise_prices = {
            hotel.id: {
                'pretax': 0,
                'tax': 0,
                'sell': 0,
                'pretax_rackrate': 0,
                'rack_rate': 0} for hotel in hotels}
        logger.info(
            'Room Config Sent :%s.. Type room config: %s',
            room_config,
            type(room_config))
        for singleRoomConfig in room_config:
            hotels_to_room_price_map = hotel_to_room_price_map_for_date[singleRoomConfig[0]]
            for hotel in hotels:
                cheap_room_price = hotels_to_room_price_map[hotel.id]
                filtered_promos = DiscountUtils.filter_applicable_promos(promotions[hotel.id],
                                                                         cheapest_room_types[hotel.id]['room_type'],
                                                                         booking_date, stay_date, stay_date + delta)
                state_tax = stay_date_state_tax[hotel]
                luxury_taxes = luxury_tax_by_state_wise[hotel.state.id]
                price_details, promotion_applied = PriceUtilsV2.__get_room_price_with_auto_promotion_coupon_tax(
                    stay_date, cheap_room_price, state_tax, luxury_taxes, filtered_promos)
                hotel_wise_prices[hotel.id]['pretax'] = float(
                    hotel_wise_prices[hotel.id]['pretax']) + float(price_details['pretax'])
                hotel_wise_prices[hotel.id]['tax'] = float(
                    hotel_wise_prices[hotel.id]['tax']) + float(price_details['tax'])
                hotel_wise_prices[hotel.id]['sell'] = float(
                    hotel_wise_prices[hotel.id]['sell']) + float(price_details['sell'])
                hotel_wise_prices[hotel.id]['rack_rate'] = float(
                    hotel_wise_prices[hotel.id]['rack_rate']) + float(price_details['rack_rate'])
                hotel_wise_prices[hotel.id]['pretax_rackrate'] = float(
                    hotel_wise_prices[hotel.id]['pretax_rackrate']) + float(price_details['pretax_rackrate'])

        return hotel_wise_prices

    @staticmethod
    def __get_room_price_with_auto_promotion_coupon_tax(
            stay_date,
            room_price,
            state_tax,
            luxury_taxes,
            promotions,
            coupon_percent=0.0):
        # 1.Apply Auto Promotion
        discount_amount, tax_after_discount, promotion_applied = DiscountUtils.get_auto_promotion_discount(
            room_price.base_price, promotions, luxury_taxes, stay_date, state_tax)
        price_after_discount = float(
            room_price.base_price) - float(discount_amount)
        sell_price = float(price_after_discount) + TaxUtils.get_tax_on_price(
            price_after_discount, state_tax, luxury_taxes)
        # 2.Apply Coupon Discount
        coupon_discount_amount = DiscountUtils.calculate_coupon_discount(
            price_after_discount, coupon_percent)
        price_after_coupon = float(
            price_after_discount) - float(coupon_discount_amount)
        # 3.Apply Tax
        tax_amount = TaxUtils.get_tax_on_price(
            price_after_coupon, state_tax, luxury_taxes)
        tax_on_rack_rate = TaxUtils.get_tax_on_price(
            room_price.base_price, state_tax, luxury_taxes)
        room_price_details = {
            'pretax_rackrate': float(
                room_price.base_price),
            'rack_rate': float(
                room_price.base_price) +
            float(tax_on_rack_rate),
            'pretax': price_after_discount,
            'sell': sell_price,
            'tax': tax_amount,
            'discount': float(coupon_discount_amount)}
        return room_price_details, promotion_applied

    @staticmethod
    def __for_each_hotel_date_range_find_cheapest_roomprice(
            start_date, end_date, hotels, cheapest_room_types, occupancy_list):
        # TODO cheapest room type to be calculated after auto promo
        hotel_by_id = {hotel.id: hotel for hotel in hotels}
        hotel_id_to_cheap_room = PriceUtilsV2.__filter_cheapest_room_type(
            cheapest_room_types, hotels)
        price_ids_query_set = models.RoomPriceV2.objects.filter(
            room_id__in=list(
                hotel_id_to_cheap_room.values()),
            date__range=[
                start_date,
                end_date],
            guest_count__in=occupancy_list).values(
                'room_id',
                'date',
                'guest_count').annotate(
            price_id=Max('id'))
        price_ids = [result['price_id'] for result in price_ids_query_set]
        room_prices = models.RoomPriceV2.objects.filter(
            id__in=price_ids).select_related(
            "room", "room__hotel").order_by('-id')
        room_price_date_map = defaultdict(list)
        room_price_date_occupancy_map = defaultdict(dict)
        for room_price in room_prices:
            room_price_date_map[room_price.date].append(room_price)
        for date, room_prices in list(room_price_date_map.items()):
            room_occupancy_map = defaultdict(list)
            for room_price in room_prices:
                room_occupancy_map[room_price.guest_count].append(room_price)
            room_price_date_occupancy_map[date].update(room_occupancy_map)

        date_hotel_price_map_result = defaultdict(dict)
        for stay_date in date_range(start_date, end_date):
            occupancy_price_result = defaultdict(dict)
            for occupancy in occupancy_list:
                room_with_missing_price = dict(hotel_id_to_cheap_room)
                hotel_with_missing_price = dict(hotel_by_id)
                hotel_price_map = {}
                if room_price_date_occupancy_map.get(stay_date):
                    occupancy_map = room_price_date_occupancy_map.get(
                        stay_date)
                    if occupancy_map.get(occupancy):
                        price_obj = occupancy_map.get(occupancy)
                        for price in price_obj:
                            hotel = hotel_by_id[price.room.hotel.id]
                            hotel_price_map[hotel.id] = price
                            del room_with_missing_price[hotel.id]
                            del hotel_with_missing_price[hotel.id]
                logger.info(
                    'Room id with missing price: %s', list(
                        room_with_missing_price.keys()))
                logger.info(
                    'Hotel id with missing price: %s', list(
                        hotel_with_missing_price.keys()))

                hotels_default_price_map = PriceUtilsV2.__find_default_room_price(
                    room_with_missing_price, list(
                        hotel_with_missing_price.values()), stay_date, occupancy)
                hotel_price_map.update(hotels_default_price_map)
                occupancy_price_result[occupancy].update(hotel_price_map)
            date_hotel_price_map_result[stay_date].update(
                occupancy_price_result)
        return date_hotel_price_map_result

    @staticmethod
    def __for_each_hotel_find_cheapest_roomprice(
            date, hotels, cheapest_room_types, occupancy=1):
        # TODO cheapest room type to be calculated after auto promo
        hotel_by_id = {hotel.id: hotel for hotel in hotels}
        hotel_id_to_cheap_room = PriceUtilsV2.__filter_cheapest_room_type(
            cheapest_room_types, hotels)
        price_ids_query_set = models.RoomPriceV2.objects.filter(
            room_id__in=list(
                hotel_id_to_cheap_room.values()),
            date=date,
            guest_count=occupancy).values(
            'room_id',
            'date',
            'guest_count').annotate(
            price_id=Max('id'))
        price_ids = [result['price_id'] for result in price_ids_query_set]
        room_prices = models.RoomPriceV2.objects.filter(
            id__in=price_ids).select_related(
            "room", "room__hotel").order_by('-id')
        room_price_by_room_id = {}
        room_with_missing_price = dict(hotel_id_to_cheap_room)
        hotel_with_missing_price = dict(hotel_by_id)
        hotel_price_map = {}
        for price in room_prices:
            hotel = hotel_by_id[price.room.hotel.id]
            room_price_by_room_id[price.room.id] = price
            hotel_price_map[hotel.id] = price
            del room_with_missing_price[hotel.id]
            del hotel_with_missing_price[hotel.id]
        logger.info(
            'Room id with missing price: %s', list(
                room_with_missing_price.keys()))
        logger.info(
            'Hotel id with missing price: %s', list(
                hotel_with_missing_price.keys()))

        hotels_default_price_map = PriceUtilsV2.__find_default_room_price(
            room_with_missing_price, list(hotel_with_missing_price.values()), date, occupancy)
        hotel_price_map.update(hotels_default_price_map)
        return hotel_price_map

    @staticmethod
    def __filter_cheapest_room_type(cheapest_room_types, hotels):
        hotel_cheap_room_dict = {}
        for hotel in hotels:
            cheapest_room_type = cheapest_room_types.get(hotel.id)
            all_rooms = hotel.rooms
            hotel_cheap_room_dict[hotel.id] = next(
                (room for room in all_rooms if cheapest_room_type['room_type'] == room.room_type_code), None)
        return hotel_cheap_room_dict

    @staticmethod
    def __find_default_room_price(
            hotels_with_cheapest_room_id,
            hotels,
            date,
            occupancy):
        hotel_price_map = {}
        for hotel in hotels:
            price = None
            rooms = hotel.roomset
            room = None
            for room in rooms:
                if room.id == hotels_with_cheapest_room_id[hotel.id]:
                    price = room.price
                    break
                else:
                    if price is None or price > room.price:
                        price = room.price
            if room:
                room_price = RoomPriceV2()
                room_price.base_price = price + \
                    (hotel.price_increment * (occupancy - 1))
                room_price.room = room
                room_price.guest_count = occupancy
                room_price.date = date
                hotel_price_map[hotel.id] = room_price
        return hotel_price_map


def update_rate_plan_from_hx(rate_plan_update_request):
    try:
        hotel_code, price_updates = parse_rate_plan_update_request(
            rate_plan_update_request)
        if FeatureToggleAPI.is_enabled(
                "pricing", "ratepush_success_log", False):
            publish_rateplan_message(
                rate_plan_update_request,
                'ratepush_success_queue',
                'ratepush_success')
    except BaseException:
        logger.exception("Data validation - parsing failed")
        if FeatureToggleAPI.is_enabled("pricing", "ratepush_error_log", False):
            publish_rateplan_message(
                rate_plan_update_request,
                'ratepush_error_queue',
                'ratepush_error')
        raise TreeboValidationException("Data validation - parsing failed")
    try:
        # TODO Need a simple direct mapping for hotels & room types
        hotel_rooms_mappers = HotelRoomMapper.objects.filter(
            mm_hotel_code=hotel_code).all()
        if not hotel_rooms_mappers:
            raise TreeboValidationException(
                "Data validation - mm Hotel %s does not have room mappings" %
                hotel_code)
        room_id = hotel_rooms_mappers.first().room_id
        hotel = None
        rooms_with_hotel = room_data_service.get_room_with_related_entities(
            select_list=['hotel'], prefetch_list=None, filter_params={'id': room_id})
        if rooms_with_hotel:
            hotel = rooms_with_hotel[0].hotel
        if not hotel:
            raise TreeboValidationException(
                "Data validation - Hotel does not exist for room id %s " %
                room_id)
        all_hotel_rooms = hotel.rooms.all()
        room_by_id = {
            hotel_room.id: hotel_room for hotel_room in all_hotel_rooms}

        room_by_room_code = {}
        for mapper in hotel_rooms_mappers:
            room_by_room_code[mapper.mm_room_code] = room_by_id[mapper.room_id]

        room_prices = []
        for price_update in price_updates:
            if price_update.room_type_code in room_by_room_code:
                room_price = convert_to_room_price(
                    price_update, room_by_room_code[price_update.room_type_code])
                room_price.save()
                room_prices.append(room_price)

        event_publishers.publish_price(hotel.id, room_prices)
    except:
        logger.exception("Handling Rate Push Message Failed")
        if FeatureToggleAPI.is_enabled("pricing", "ratepush_error_log", False):
            publish_rateplan_message(
                rate_plan_update_request,
                'ratepush_error_queue',
                'ratepush_error')
        raise TreeboException("Handling Rate Push Message Failed")


def publish_rateplan_message(
        rate_plan_update_request,
        queue_name,
        routing_key):
    try:
        connection = Connection(settings.PRICING_BROKER_URL,
                                heartbeat=get_rabbit_mq_heartbeat_interval(),
                                transport_options=get_rabbit_mq_transport_options())
        exchange = Exchange("ratepush_exchange")
        queue = Queue(queue_name, exchange=exchange, routing_key=routing_key)
        with connections[connection].acquire(block=True) as conn:
            producer = conn.Producer()
            producer.publish(rate_plan_update_request,
                             exchange=exchange, routing_key=routing_key,
                             declare=[exchange, queue],
                             retry=True,
                             retry_policy=get_rabbit_mq_retry_policy())
    except BaseException:
        logger.exception(
            "Unable to publish Ratepush API error message to error queue ")



def convert_to_room_price(price_update, room):
    room_price = RoomPriceV2()
    room_price.room = room
    room_price.date = price_update.date
    room_price.base_price = price_update.base_price
    room_price.guest_count = price_update.num_of_guests
    room_price.extra_adult_rate = price_update.extra_adult_price
    room_price.extra_kid_rate = price_update.extra_kid_price
    return room_price


def parse_rate_plan_update_request(rate_plan_update_request):
    xml_request = BeautifulSoup(rate_plan_update_request, features="xml")
    # This Hotelcode is MM hotelcode
    hotel_code = xml_request.RatePlans['HotelCode']
    rate_plan_nodes = xml_request.find_all(
        'RatePlan', {'RatePlanCode': RATE_PLAN_CODE})
    price_updates = []
    for rate_plan_node in rate_plan_nodes:
        # Guaranteed by Hx that rates will be per day basis.
        start = ymd_str_to_date(rate_plan_node['Start'])
        end = ymd_str_to_date(
            rate_plan_node['End']) + datetime.timedelta(days=1)
        for current_stay_date in date_range(start, end):
            for rate_node in rate_plan_node.Rates.find_all('Rate'):
                room_type_code = rate_node['InvTypeCode']
                room_price_nodes = rate_node.find_all('BaseByGuestAmt')
                extra_adult_price, extra_kid_price = extract_additional_price(
                    rate_node)
                for room_price_node in room_price_nodes:
                    number_of_guests = int(room_price_node['NumberOfGuests'])
                    age_code = int(room_price_node['AgeQualifyingCode'])
                    # As of now there is no kids only room supported
                    if age_code is HTNG_ADULT_CODE:
                        price_update = PriceUpdate(hotel_code, room_type_code)
                        price_update.num_of_guests = number_of_guests
                        # AmountAfterTax has pretax value as per maximojo
                        # contract with Hx
                        price_update.base_price = float(
                            room_price_node['AmountAfterTax'])
                        price_update.extra_adult_price = extra_adult_price
                        price_update.extra_kid_price = extra_kid_price
                        price_update.date = current_stay_date
                        price_updates.append(price_update)
    return hotel_code, price_updates


def extract_additional_price(rate_node):
    extra_adult_price = 0
    extra_kid_price = 0
    extra_price_nodes = rate_node.find_all('AdditionalGuestAmount')
    for extra_price_node in extra_price_nodes:
        age_code = int(extra_price_node['AgeQualifyingCode'])
        if age_code is HTNG_ADULT_CODE:
            extra_adult_price = float(extra_price_node['AmountAfterTax'])
        elif age_code is HTNG_CHILD_CODE:
            extra_kid_price = float(extra_price_node['AmountAfterTax'])
        else:
            continue
    return extra_adult_price, extra_kid_price


class PriceUpdate(object):
    def __init__(self, hotel_code, room_type_code):
        self.hotel_code = hotel_code
        self.room_type_code = room_type_code
        self.num_of_guests = None
        self.base_price = None
        self.extra_adult_price = None
        self.extra_kid_price = None
