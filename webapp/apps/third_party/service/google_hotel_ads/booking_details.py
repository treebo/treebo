

import logging

import requests
from django.conf import settings

logger = logging.getLogger(__name__)


class FetchBookingDetails(object):
    def fetch_booking_status(self, wrid_list):
        try:
            wrid_lists = self.create_chunks_of_wrid(wrid_list)
            for list_of_wrid in wrid_lists:
                data = ",".join(list_of_wrid)
                url = settings.FETCH_BOOKING_STATUS + str(data)
                logger.info("url - %s", url)
                headers = {
                    "Content-Type": "application/json"
                }
                response = requests.get(url=url, headers=headers)
                logger.info("response.text %s", response.text)
                response = response.json()
                return response
        except Exception:
            logger.exception(
                "Exception in calling the fetch booking status API of growth")
            return None

    def create_chunks_of_wrid(self, wrid_list):
        wrid_lists = [wrid_list[i:i + 5] for i in range(0, len(wrid_list), 5)]
        logger.info("chunks of wrid lists %s", wrid_lists)
        return wrid_lists
