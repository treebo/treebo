from common.exceptions.treebo_exception import TreeboException


class HotelUploadException(TreeboException):
    """
    Handles all exceptions raised during hotel upload process

    """

    def __init__(self, *args):
        super(HotelUploadException, self).__init__(*args)
