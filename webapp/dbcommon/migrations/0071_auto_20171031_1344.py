# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0070_auto_20171031_0846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hotelmetadata',
            name='key',
            field=models.CharField(
                max_length=100,
                choices=[
                    ('b2b_blacklist',
                     'b2b_blacklist')]),
        ),
    ]
