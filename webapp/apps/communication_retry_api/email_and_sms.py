import logging
from rest_framework.response import Response
from apps.checkout.tasks import send_booking_confirmation_notification
from base.views.api import TreeboAPIView


logger = logging.getLogger(__name__)


class EmailAndSms(TreeboAPIView):

    def post(self, request, *args, **kwargs):
        request_data = request.data
        order_ids = request_data['order_ids']
        for order_id in order_ids:
            print(order_id)
            logger.info("Sending confirmation for order %s" % str(order_id))
            send_booking_confirmation_notification.delay(order_id)
            logger.info("Enqueued order %s" % str(order_id))
        return Response(status=200)
