.PHONY: clean-pyc pre-commit

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +

pre-commit:
	echo "Exporting locale settings"
	export LC_ALL=en_US.UTF-8
	export LANG=en_US.UTF-8
	export PYLINTRC=pylintrc
	pip install pre-commit==1.18.1

	echo "Installing pre-commit hook"
	pre-commit install -f
	pre-commit autoupdate
