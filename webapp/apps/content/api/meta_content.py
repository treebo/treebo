import logging
import json
from django.utils.text import slugify
from django.db.models import Q
from rest_framework.response import Response
import geopy
import geopy.distance

from apps.content.services.meta_content_service import MetaContentService
from apps.content.services.schema_builder import SchemaBuilder


from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Categories
from dbcommon.models.landmark import SearchLandmark
from dbcommon.models.location import City
from services.restclient.contentservicerestclient import ContentServiceClient
from django.conf import settings
from dbcommon.models.location import Locality
from webapp.data_services.hotel_repository import HotelRepository
from data_services.city_repository import CityRepository
from data_services.search_landmark_repository import SearchLandMarkRepository

logger = logging.getLogger(__name__)


class MetaContentAPI(TreeboAPIView):

    service_obj = MetaContentService()
    hotel_repository = RepositoriesFactory.get_hotel_repository()
    search_landmark_repository = RepositoriesFactory.get_landmark_repository()
    city_repository = RepositoriesFactory.get_city_repository()

    def get(self, request, *args, **kwargs):
        page_name = request.GET.get('page')
        query = request.GET.get('q')

        city_count = self.city_repository.get_enabled_cities_count()
        hotel_count = self.hotel_repository.get_total_hotel_count(
            only_active=True)
        #hotel_count = Hotel.objects.filter(status=Hotel.ENABLED).count()
        meta_content = ContentServiceClient.getValueForKey(
            request, 'meta_content', 1)
        seo_pages = ContentServiceClient.getValueForKey(
            request, 'seo_pages', 1)
        data = {'city_count': city_count, 'hotel_count': hotel_count}

        meta_content = json.loads(json.dumps(meta_content) % data)
        seo_pages = json.loads(json.dumps(seo_pages) % data)
        schema_obj = SchemaBuilder(page_name, query)
        meta_content['schema'] = schema_obj.build()
        meta_content = self.update_seo_footer_data(
            request, meta_content, page_name, query)
        meta_content = self.service_obj.update_meta(
            meta_content, page_name, query, seo_pages)

        return Response(meta_content)

    def update_seo_footer_data(self, request, meta_content, page_name, query):

        # footer content
        try:
            footer_data = None
            current_city = None
            item_name = None
            meta_content['content'] = {}
            footer_data = ContentServiceClient.getValueForKey(
                request, 'footer_data', 1)
            if page_name.lower() == 'city' or page_name.lower() == 'locality' or page_name.lower(
            ) == 'landmark' or page_name.lower() == 'category':
                about_key = None
                current_city = None
                locality_obj = None
                landmark_obj = None
                category_obj = None
                landmark_exclude_list = []
                locality_exclude_list = []
                category_exclude_list = []
                description = None
                if page_name.lower() == 'locality':
                    city = query.split('-')[-1]
                    locality_name = ' '.join(query.split('-')[:-1])
                    about_key = '_'.join(locality_name.lower().split())
                    locality_obj = Locality.objects.filter(Q(name__iexact=locality_name) & Q(
                        Q(city__name__iexact=city) | Q(city__slug__iexact=city))).first()
                    if locality_obj:
                        current_city = locality_obj.city
                        locality_exclude_list.append(locality_obj.id)
                        item_name = locality_obj.name
                if page_name.lower() == 'landmark':
                    # check whether query is free_text_url

                    landmark_objs = self.search_landmark_repository.get_landmarks_by_freetext_url(
                        query_string=' '.join(query.split('-')))
                    if landmark_objs:
                        landmark_obj = landmark_obj[0]
                    else:
                        landmark_obj = None
                    if landmark_obj:
                        landmark_name = landmark_obj.free_text_url
                    else:
                        city = query.split('-')[-1]
                        landmark_name = ' '.join(query.split('-')[:-1])
                        landmark_obj = self.search_landmark_repository.get_landmark_for_seo_url_and_city_name(
                            city_name=city, landmark_name=landmark_name)
                    about_key = '_'.join(landmark_name.lower().split())
                    if landmark_obj:
                        current_city = landmark_obj.city
                        landmark_exclude_list.append(landmark_obj.id)
                        item_name = landmark_obj.free_text_url if landmark_obj.free_text_url else landmark_obj.seo_url_name
                if page_name.lower() == 'city':
                    about_key = query.lower()
                    key = 'search_' + query.lower() + '_desc'
                    description = ContentServiceClient.getValueForKey(
                        request, key, 1)
                    # filter_cities_by_string_matching
                    from data_services.city_repository import CityRepository
                    city_search_args = []
                    city_search_args.append(
                        {'name': 'name', 'value': query, 'case_sensitive': False, 'partial': False})
                    city_search_args.append(
                        {'name': 'slug', 'value': query, 'case_sensitive': False, 'partial': False})
                    current_cities = self.city_repository.filter_cities_by_string_matching(
                        city_search_args, {'status': 1})
                    if current_cities:
                        current_city = current_cities[0]
                    else:
                        current_city = None
                    if current_city:
                        item_name = current_city.name
                if page_name.lower() == 'category':
                    city = query.split('-')[-1].strip('-')
                    category_name = ' '.join(query.split('-')[:-1]).strip()
                    about_key = '_'.join(category_name.lower().split())
                    category_obj = Categories.objects.filter(
                        status=1, name__iexact=category_name).first()
                    city_search_args = []
                    city_search_args.append(
                        {'name': 'name', 'value': query, 'case_sensitive': False, 'partial': False})
                    city_search_args.append(
                        {'name': 'slug', 'value': query, 'case_sensitive': False, 'partial': False})
                    current_cities = self.city_repository.filter_cities_by_string_matching(
                        city_search_args, {'status': 1})
                    if current_cities:
                        current_city = current_cities[0]
                    else:
                        current_city = None
                    if category_obj:
                        category_exclude_list.append(category_obj.id)
                        item_name = category_obj.name
                if not description:
                    key = 'about_' + about_key + '_' + \
                        '_'.join(current_city.name.lower().split())
                    description = ContentServiceClient.getValueForKey(
                        request, key, 1)
                links = []
                if footer_data:
                    distance_cap = int(footer_data['distance_cap'])
                    count_cap = int(footer_data['no_of_hotels_cap'])
                else:
                    distance_cap = 50
                    count_cap = 50

                links.append(
                    {
                        'key': 'locality',
                        'title': 'LOCALITIES',
                        'content': AutoCompleteResultBuilder.build_locality_result(
                            Locality.objects.filter(
                                city__name__iexact=current_city.name,
                                enable_for_seo=True,
                                city__enable_for_seo=True).exclude(
                                id__in=locality_exclude_list))} if footer_data and current_city else {
                        'key': 'locality',
                        'title': 'LOCALITIES',
                        'content': []})

                links.append(
                    {
                        'key': 'landmark',
                        'title': 'LANDMARKS',
                        'content': AutoCompleteResultBuilder.build_landmark_result(
                            self.search_landmark_repository.get_landmarks_for_city_with_exclusion(
                                city_name=current_city.name,
                                enable_for_seo=True,
                                city_enabled_for_seo=True,
                                excluded_ids_list=landmark_exclude_list))} if footer_data and current_city else {
                        'key': 'landmark',
                        'title': 'LANDMARKS',
                        'content': []})

                links.append(
                    {
                        'key': 'category',
                        'title': 'Hotel Types',
                        'content': AutoCompleteResultBuilder.build_category_result(
                            Categories.objects.filter(
                                hotel__city=current_city,
                                enable_for_seo=True,
                                hotel__city__enable_for_seo=True).distinct(). exclude(
                                id__in=category_exclude_list),
                            current_city)})

                near_by_cities = dict()
                sorted_nearby_cities = []
                if current_city and current_city.enable_for_seo:
                    all_cities = [city for city in self.city_repository.filter_cities(
                        enable_for_seo=True, status=1) if city.id != current_city.id]
                    current_city_point = geopy.Point(
                        current_city.city_latitude, current_city.city_longitude)
                    for city in all_cities:
                        pt2 = geopy.Point(
                            city.city_latitude, city.city_longitude)
                        distance = geopy.distance.distance(
                            current_city_point, pt2).km
                        if distance <= distance_cap:
                            # adding to near_by cities only if the distance cap
                            # matches
                            near_by_cities[str(city.name)] = (distance, city)
                    # Now sorting the dict of near_by_cities
                    sorted_nearby_cities = [
                        seq[1][1] for seq in sorted(
                            list(
                                near_by_cities.items()),
                            key=lambda x: x[1][0])]

                    links.append({'key': 'city',
                                  'title': 'CITIES NEARBY',
                                  'content': AutoCompleteResultBuilder.build_city_result(
                                      sorted_nearby_cities[:count_cap]
                                  )
                                  } if sorted_nearby_cities else {'key': 'city',
                                                                  'title': 'CITIES NEARBY',
                                                                  'content': []})
                else:
                    links.append(
                        {'key': 'city', 'title': 'CITIES NEARBY', 'content': []})
                city_exclude_list = [
                    city.id for city in sorted_nearby_cities[:count_cap]]
                if current_city:
                    city_exclude_list.append(current_city.id)
                if footer_data and current_city:
                    popular_cities = self.city_repository.get_popular_cities(
                        footer_data['popular_cities'], city_exclude_list=city_exclude_list)
                else:
                    popular_cities = []

                links.append({'key': 'city',
                              'title': 'POPULAR CITIES',
                              'content': AutoCompleteResultBuilder.build_city_result(popular_cities)
                              } if footer_data else {'key': 'city', 'title': 'POPULAR CITY', 'content': []})

                meta_content['content'].update({
                    'description': description['text'] if description else '',
                    'links': links
                })
                # change title
                if footer_data:
                    title = None
                    description = None
                    full_canonical_url = None
                    if locality_obj:
                        temp_dict = {
                            "locality_name": locality_obj.name,
                            "city_name": locality_obj.city.name
                        }
                        if 'locality_title' in footer_data:
                            title = footer_data['locality_title'] % temp_dict
                        if 'locality_description' in footer_data:
                            description = footer_data['locality_description'] % temp_dict
                        full_canonical_url = settings.SITE_HOST_NAME + 'hotels-in-' + \
                            slugify(locality_obj.name) + '-' + slugify(current_city.slug) + '/'
                    elif landmark_obj:
                        temp_dict = {
                            "landmark_name": landmark_obj.seo_url_name,
                            "city_name": landmark_obj.city.name
                        }
                        if 'landmark_title' in footer_data:
                            title = footer_data['landmark_title'] % temp_dict
                        if 'landmark_description' in footer_data:
                            description = footer_data['landmark_description'] % temp_dict
                        if landmark_obj.free_text_url:
                            full_canonical_url = settings.SITE_HOST_NAME + \
                                'hotels-near-' + slugify(landmark_obj.free_text_url) + '/'
                        else:
                            full_canonical_url = settings.SITE_HOST_NAME + 'hotels-near-' + slugify(
                                landmark_obj.seo_url_name) + \
                                '-' + slugify(current_city.slug) + '/'
                    elif category_obj:
                        temp_dict = {
                            "category_name": category_obj.name,
                            "city_name": current_city.name
                        }
                        if 'category_title' in footer_data:
                            title = footer_data['category_title'] % temp_dict
                        if 'category_description' in footer_data:
                            description = footer_data['category_description'] % temp_dict
                        full_canonical_url = settings.SITE_HOST_NAME + \
                            slugify(category_obj.name) + '-in-' + slugify(current_city.slug) + '/'
                    if title and description:
                        self.service_obj.override_seo_content(
                            meta_content, title, description, None, full_canonical_url)
                    elif full_canonical_url:
                        meta_content['link'] = [{'rel': 'canonical',
                                                 'href': full_canonical_url
                                                 }]
            elif page_name.lower() == 'hd':
                hotel = None
                try:
                    hotel = self.hotel_repository.get_single_hotel(id=int(query))
                except Exception as e:
                    pass
                #hotel = Hotel.objects.filter(id=int(query)).first()
                if hotel:
                    current_city = hotel.city
                    item_name = hotel.name
            meta_content['content'].update(
                {
                    'breadcrumbs': self.build_breadcrumb(
                        current_city,
                        footer_data,
                        page_name,
                        item_name,
                        query) if current_city and footer_data else []})

            return meta_content
        except Exception as ex:
            logger.exception('Exception occured while updating footer data')
            return meta_content

    def build_breadcrumb(
            self,
            current_city,
            footer_data,
            page_name,
            item_name,
            query):
        item = None
        city_item = False
        breadcrumbs = []
        if 'breadcrumbs' in footer_data:
            if page_name.lower() == 'landmark':
                if 'landmark' in footer_data['breadcrumbs']:
                    item = footer_data['breadcrumbs']['landmark'] % item_name
            elif page_name.lower() == 'locality':
                if 'locality' in footer_data['breadcrumbs']:
                    item = footer_data['breadcrumbs']['locality'] % item_name
            elif page_name.lower() == 'category':
                if 'category' in footer_data['breadcrumbs']:
                    item = footer_data['breadcrumbs']['category'] % (
                        item_name, current_city.slug if current_city.slug else current_city.name)
            elif page_name.lower() == 'hd':
                item = item_name
            elif page_name.lower() == 'city':
                if 'city' in footer_data['breadcrumbs']:
                    city_item = True
                    item = footer_data['breadcrumbs']['city'] % current_city.slug if current_city.slug else current_city.name
        if item:
            breadcrumbs.append(
                {
                    "label": "Home",
                    "destination": "/",
                    "schema": "Treebo.com",
                    "position": 1,
                }
            )
            if city_item:
                breadcrumbs.append(
                    {
                        "label": item,
                        "destination": "",
                        "schema": item.title(),
                        "position": 2,
                    }
                )
            else:
                breadcrumbs.extend([
                    {
                        "label": current_city.slug if current_city.slug else current_city.name,
                        "destination": "/hotels-in-" + current_city.slug + "/" if current_city.slug else current_city.name,
                        "schema": current_city.slug.title() if current_city.slug else current_city.name.title(),
                        "position": 2,
                    },
                    {
                        "label": item,
                        "destination": "",
                        "schema": item.title(),
                        "position": 3,
                    }
                ])

        return breadcrumbs
