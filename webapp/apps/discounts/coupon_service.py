from apps.discounts.tasks import update_coupon_constraints
from base.middlewares.set_request_id import get_current_user, get_current_request_id


class CouponService(object):
    @staticmethod
    def update_constraints(coupon_constraints):
        print(coupon_constraints)
        for constraint in coupon_constraints:
            coupon_code, checkin_start, checkout_start = constraint
            update_coupon_constraints.delay(
                coupon_code.strip(),
                checkin_start.strip(),
                checkout_start.strip(),
                user_id=get_current_user(),
                request_id=get_current_request_id())
