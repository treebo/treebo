from celery.app import shared_task
from celery.signals import task_prerun

import requests
# This is executed before every task. Here we can set the request_id and
# user_id in LogFilter
from celery.utils.log import get_task_logger
from django.conf import settings

from base.filters import CeleryRequestIDFilter
from base.logging_decorator import log_args

logger = get_task_logger(__name__)


@task_prerun.connect()
def set_request_id(
        signal=None,
        sender=None,
        task_id=None,
        task=None,
        args=None,
        **kwargs):
    logger.addFilter(
        CeleryRequestIDFilter(
            kwargs.get('user_id'),
            kwargs.get('request_id'),
            task_id,
            task.name))
    logger.info(
        "set_request_id called with kwargs: %s, and task_id: %s",
        kwargs,
        task_id)


# log_args decorator should be added below the shared_task decorator.
# Ordering is important here.
@shared_task
@log_args(logger)
def add(x, y, user_id=None, request_id=None):
    return x + y


@shared_task
@log_args(logger)
def sleeptask(i, user_id=None, request_id=None):
    from time import sleep
    sleep(i)
    return i


@shared_task
@log_args(logger)
def update_third_party_availability(payload, request_id=None):
    try:
        for key, url in list(settings.AVAILABILITY_HOOK_API.items()):
            logger.debug(
                "Updating url " +
                url +
                " with payload: " +
                str(payload))
            requests.get(url=url, params=payload, timeout=0.5)
    except requests.RequestException as err:
        logger.error(err)
