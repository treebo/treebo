# from apps.hotels.data_classes import Hotel
# from apps.hotels.data_classes.sku_category import SkuCategory
#
#
# class TaxRequest(object):
#
#     def __init__(self, hotel, price, sku_category):
#         assert isinstance(hotel, Hotel)
#
#         from apps.pricing import data_classes
#         assert isinstance(price, data_classes.Price)
#         assert isinstance(sku_category, SkuCategory)
#         self.hotel = hotel
#         self.price = price
#         self.sku_category = sku_category
#
#     @property
#     def uid(self):
#         return self.__hash__()
#
#     def __hash__(self):
#         return hash((self.hotel, self.price, self.sku_category))
