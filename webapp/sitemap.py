import logging

from django.core.cache import cache
from django.utils.text import slugify
from django.contrib.sitemaps import Sitemap
from dbcommon.models.landmark import SearchLandmark
from dbcommon.models.location import City, Locality
from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)

hotel_repository = RepositoriesFactory.get_hotel_repository()
city_repository = RepositoriesFactory.get_city_repository()
search_landmark_repository = RepositoriesFactory.get_landmark_repository()


class HomeSitemap(Sitemap):
    priority = 1.0
    changefreq = "daily"
    protocol = "https"

    def items(self):
        return ['/']

    def location(self, item):
        return (item)


class StaticViewSitemap(Sitemap):
    priority = 0.50
    changefreq = "weekly"
    protocol = "https"

    def items(self):
        return [
            '/joinus/',
            '/aboutus/',
            '/fot/',
            '/faq/',
            '/terms/',
            '/policy/',
            '/contactus/',
            '/fot/register/']

    def location(self, item):
        return (item)


class CitySitemap(Sitemap):
    protocol = "https"
    changefreq = "daily"
    priority = 0.90

    def items(self):
        return city_repository.filter_cities(status=1)

    def location(self, obj):
        return obj.get_sitemap_url()


class HotelSitemap(Sitemap):
    protocol = "https"
    changefreq = "daily"
    priority = 0.70

    def items(self):
        return hotel_repository.get_active_hotel_list()

    def location(self, obj):
        """

        :param dbcommon.models.hotel.Hotel obj:
        :return:
        """
        return obj.get_sitemap_url()


class LandmarkSitemap(Sitemap):
    protocol = "https"
    changefreq = "weekly"
    priority = 0.80

    def items(self):
        return SearchLandmark.objects.filter(enable_for_seo=True, city__enable_for_seo=True, is_hotel_present=True,
                                             city__status=1)

    def location(self, obj):
        return obj.get_sitemap_url()


class LocalitySitemap(Sitemap):
    protocol = "https"
    changefreq = "weekly"
    priority = 0.80

    def items(self):
        from dbcommon.models.hotel import Locality
        return Locality.objects.filter(enable_for_seo=True, city__enable_for_seo=True, is_hotel_present=True)

    def location(self, obj):
        return obj.get_sitemap_url()


class CategorySitemap(Sitemap):
    protocol = "https"
    changefreq = "weekly"
    priority = 0.80

    def __init__(self):
        self.cities = city_repository.filter_cities(
            status=1, enable_for_seo=True)

    def items(self):
        category_city_mapping = []
        from dbcommon.models.hotel import Categories, EnableCategory
        categories = Categories.objects.filter(enable_for_seo=True)
        for category in categories:
            for city in self.cities:
                try:
                    enable_cat = EnableCategory.objects.prefetch_related(
                        'hotel_type').get(city=city)
                    if category in enable_cat.hotel_type.all():
                        hotels = hotel_repository.filter_hotels(
                            city=city.id, category=category)
                        if hotels:
                            category_city_mapping.append(
                                {'category': category, 'city_name': city.slug if city.slug else city.name})
                except EnableCategory.DoesNotExist:
                    pass
                except Exception as e:
                    logger.exception(e)
        return category_city_mapping

    def location(self, obj):
        return obj['category'].get_sitemap_url(obj['city_name'])


class NearMeSitemap(Sitemap):
    protocol = "https"
    changefreq = "daily"
    priority = 0.90

    def items(self):
        return ['/hotels-near-me/']

    def location(self, item):
        return item


class LocalityCategorySitemap(Sitemap):
    protocol = "https"
    changefreq = "daily"
    priority = 0.60

    def __init__(self):
        self.localities = Locality.objects.filter(enable_for_seo=True, city__enable_for_seo=True, is_hotel_present=True)

    def items(self):
        sitemap_list = []
        from apps.seo.components.hotel_category_in_location import HotelCategoryInLocation
        for locality in self.localities:
            key = HotelCategoryInLocation.get_cache_key(locality.name, locality.city.name)
            data = cache.get(key)
            if not data:
                HotelCategoryInLocation.update_locality_key_in_cache(locality)
                data = cache.get(key)

            content = data['content']
            for item in content:
                category_name = slugify(item['category'])
                locality_name = slugify(item['area']['locality'])
                city_slug = item['area']['city_slug']
                sitemap = '/' + category_name + '-in-' + locality_name + '-' + city_slug + '/'
                sitemap_list.append(sitemap)
        return sitemap_list

    def location(self, item):
        return item


class LandmarkCategorySitemap(Sitemap):
    protocol = "https"
    changefreq = "daily"
    priority = 0.60

    def __init__(self):
        self.landmarks = SearchLandmark.objects.filter(enable_for_seo=True, city__enable_for_seo=True,
                                                       is_hotel_present=True, city__status=1)

    def items(self):
        sitemap_list = []
        from apps.seo.components.hotel_category_near_landmark import HotelCategoryNearLandmark
        for landmark in self.landmarks:
            key = HotelCategoryNearLandmark.get_cache_key(landmark.seo_url_name, landmark.city.name)
            data = cache.get(key)
            if not data:
                HotelCategoryNearLandmark.update_landmark_key_in_cache(landmark)
                data = cache.get(key)

            content = data['content']
            for item in content:
                category_name = slugify(item['category'])
                landmark_name = item['area']['landmark_slug']
                sitemap = '/' + category_name + '-near-' + landmark_name + '/'
                sitemap_list.append(sitemap)
        return sitemap_list

    def location(self, item):
        return item


class CityAmenitySitemap(Sitemap):
    protocol = "https"
    changefreq = "daily"
    priority = 0.60

    def items(self):
        city_amenity_map = []
        from dbcommon.models.facilities import CityAmenityMapforAutoPageCreation
        from dbcommon.models.hotel import Hotel
        try:
            city_amenity_map_objects = CityAmenityMapforAutoPageCreation.objects.filter(is_enabled=True)
            for obj in city_amenity_map_objects:
                hotels = Hotel.objects.filter(city=obj.city, facility__name=obj.amenity.name)
                if hotels:
                    city_amenity_map.append({'amenity': obj.amenity, 'city_name': obj.city.slug if obj.city.slug else obj.city.name})
        except Exception as e:
            logger.exception(e)
        return city_amenity_map

    def location(self, obj):
        return obj['amenity'].get_sitemap_url(obj['city_name'])

