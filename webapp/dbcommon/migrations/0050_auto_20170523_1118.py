# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0049_auto_20170412_1052'),
    ]

    operations = [
        migrations.AddField(
            model_name='landmark',
            name='no_of_hotels_to_show',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='locality',
            name='no_of_hotels_to_show',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
