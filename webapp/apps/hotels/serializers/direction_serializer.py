from rest_framework import serializers

from dbcommon.models.direction import Directions

__author__ = 'Rajdeep'


class DirectionSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_landmark_name')
    image_url = serializers.SerializerMethodField('get_direction_image_url')

    # css_class = serializers.SerializerMethodField('get_css_class')

    def get_landmark_name(self, direction_object):
        return direction_object.landmark_obj.name

    def get_css_class(self, direction_object):
        if direction_object.landmark_obj.name.lower().find('airport'):
            return 'airport'
        if direction_object.landmark_obj.name.lower().find('railway'):
            return 'railway'
        if direction_object.landmark_obj.name.lower().find('bus '):
            return 'busstand'
        return ''

    def get_direction_image_url(self, direction_object):
        return ""

    class Meta:
        model = Directions
        fields = (
            'directions_for_email_website',
            'directions_for_sms',
            'distance',
            'name',
            'hotel',
            'polyline',
            'mode',
            'image_url')
