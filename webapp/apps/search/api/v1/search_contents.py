import logging

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from apps.featuregate.manager import FeatureManager
from base import log_args
from base.views.api import TreeboAPIView
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'baljeet'

logger = logging.getLogger(__name__)


class SearchWidgets(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SearchWidgets, self).dispatch(*args, **kwargs)

    def get(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request):
        locality = request.data.get('locality', '')
        pageName = 'searchwidgets'
        varDict = {
            'search_carousal': 'search_carousal',
            'search_city_widget': 'search_city_widget',
            'search_quality_widget': 'search_quality_widget'
        }
        overRiddenValues = {}
        if bool(locality):
            featureManager = FeatureManager(
                pageName, None, None, None, None, locality, None)
            overRiddenValues = featureManager.get_overriding_values()
        for overRiddenVarKey in list(overRiddenValues.keys()):
            varDict[overRiddenVarKey] = overRiddenValues[overRiddenVarKey]

        # print varDict

        carousal = ContentServiceClient.getValueForKey(
            request, varDict.get('search_carousal', 'search_carousal'), 1)
        cityWidget = ContentServiceClient.getValueForKey(
            request, varDict.get('search_city_widget', 'search_city_widget'), 1)
        qualityWidget = ContentServiceClient.getValueForKey(
            request, varDict.get('search_quality_widget', 'search_quality_widget'), 1)

        resultList = []
        if carousal is not None:
            resultList.append(carousal)
        if cityWidget is not None:
            resultList.append(cityWidget)
        if qualityWidget is not None:
            resultList.append(qualityWidget)

        context = {'results': resultList}
        return Response(context)
