from django.conf.urls import url

from apps.auth.api.v2.change_password import ChangePasswordApi
from apps.auth.api.v2.email_verification import EmailVerificationApi
from apps.auth.api.v2.external_app_login import ExternalLoginApi
from apps.auth.api.v2.forgot_password import ForgotPasswordApi
from apps.auth.api.v2.google_login import GoogleLoginApi
from apps.auth.api.v2.login import LoginApi
from apps.auth.api.v2.login_via_token import TokenLoginApi
from apps.auth.api.v2.logout import LogoutApi
from apps.auth.api.v2.otp_isverify import OTPIsVerifyApi
from apps.auth.api.v2.otp_login import OTPLoginApi
from apps.auth.api.v2.otp_login_verify import OTPLoginVerifyApi
from apps.auth.api.v2.otp_verify import OTPVerifyApi
from apps.auth.api.v2.register import RegisterApi
from apps.auth.api.v2.reset_password import ResetPasswordApi

urlpatterns = [
    url(r'^token/detail/$', TokenLoginApi.as_view(), name="token-login-api-v2"),
    url(r'^login/email/$', LoginApi.as_view(), name="login-api-v2"),
    url(r"^logout/", LogoutApi.as_view(), name='logout-api-v2'),
    url(r'^register/$', RegisterApi.as_view(), name="register-api-v2"),
    url(r"^change-password/$", ChangePasswordApi.as_view(), name='change-password-token-v2'),
    url(r"^forgot-password/$", ForgotPasswordApi.as_view(), name='forgot-password-v2'),
    url(r"^reset-password/$", ResetPasswordApi.as_view(), name='reset-password-v2'),
    url(r"^login/otp/verify/$", OTPLoginVerifyApi.as_view(), name='otp-login-verify-v2'),
    url(r"^login/otp/$", OTPLoginApi.as_view(), name='otp-login-v2'),
    url(r"^otp/is-verified/$", OTPIsVerifyApi.as_view(), name='otp-is-verified-v2'),
    url(r"^login/google/$", GoogleLoginApi.as_view(), name='google-login-v2'),
    url(r"^login/(?P<app_name>[\w-]+)/$", ExternalLoginApi.as_view(), name='facebook-login-v2'),
    url(r"^email-verify/(?P<email_hash>[\w-]+)/$", EmailVerificationApi.as_view(), name='email-verification-v2'),
    url(r"^otp/send/$", OTPLoginApi.as_view(), name='otp-login-v2'),
    url(r"^otp/verify/$", OTPVerifyApi.as_view(), name='otp-verify-v2')
]
