import csv
import io
import logging

from django.conf import settings

from webapp.apps.third_party.exceptions import IxigoCsvDataGenerationError, TrivagoCsvDataGenerationError

logger = logging.getLogger(__name__)

TRIVAGO_API_VERSION = 4
TRIVAGO_API_LANGUAGE_DEFAULT = 'en_IN'
TRIVAGO_API_CURRENCY_DEFAULT = 'INR'
MAX_IMAGES = 20

common_fields = [
    'partner_reference',
    'name',
    'address',
    'address2',
    'postcode',
    'city',
    'state',
    'country',
    'country_id',
    'longitude',
    'latitude',
    'phone',
    'fax',
    'web',
    'email',
    'currency']


def format_ixigo_csv(trivago_hotel, hotel_detail):
    try:
        ixigo_hotel_csv = format_trivago_csv(trivago_hotel, hotel_detail)
        ixigo_hotel_csv.append(
            trivago_hotel['room_types']['acacia']['room_count'] if 'acacia' in trivago_hotel['room_types'] else 0)
        ixigo_hotel_csv.append(
            trivago_hotel['room_types']['mahogany']['room_count'] if 'mahogany' in trivago_hotel['room_types'] else 0)
        ixigo_hotel_csv.append(
            trivago_hotel['room_types']['maple']['room_count'] if 'maple' in trivago_hotel['room_types'] else 0)
        ixigo_hotel_csv.append(
            trivago_hotel['room_types']['oak']['room_count'] if 'oak' in trivago_hotel['room_types'] else 0)

        list_of_images = add_images_to_csv_data(trivago_hotel['images'])
        ixigo_hotel_csv.extend(list_of_images)
        return ixigo_hotel_csv
    except BaseException:
        logger.exception('formatting csv failed for TrivagoCsvHotelData')
        raise


def format_trivago_csv(trivago_hotel, hotel_detail):
    try:
        trivago_hotel_csv = []
        trivago_hotel_csv.append(trivago_hotel['partner_reference'])
        trivago_hotel_csv.append(trivago_hotel['name'])
        trivago_hotel_csv.append(hotel_detail['complete_address'])
        trivago_hotel_csv.append(' ')  # address 2 - not mandatory
        trivago_hotel_csv.append(trivago_hotel['postal_code'])
        trivago_hotel_csv.append(trivago_hotel['city'])
        trivago_hotel_csv.append(trivago_hotel['state'])
        trivago_hotel_csv.append(trivago_hotel['country'])
        trivago_hotel_csv.append(' ')  # country id - not mandatory
        trivago_hotel_csv.append(trivago_hotel['longitude'])
        trivago_hotel_csv.append(trivago_hotel['latitude'])
        trivago_hotel_csv.append(str(trivago_hotel['phone']))
        trivago_hotel_csv.append(trivago_hotel['fax'])
        trivago_hotel_csv.append(trivago_hotel['url'])
        trivago_hotel_csv.append(trivago_hotel['email'])
        # trivago_hotel_csv.append(hotel_detail['city']) - # Refer to city
        # id todo
        trivago_hotel_csv.append(TRIVAGO_API_CURRENCY_DEFAULT)
        return trivago_hotel_csv
    except BaseException:
        logger.exception('formatting csv failed for TrivagoCsvHotelData')
        raise


def add_images_to_csv_data(images_by_category):
    try:
        image_urls = []
        if not images_by_category:
            return image_urls
        image_urls.append(images_by_category["common"][0]["url"])
        for category in images_by_category:
            image_urls.extend(image['url'] for image in images_by_category[category][:5])
        return image_urls[:MAX_IMAGES]
    except Exception as e:
        logger.exception("Exception while getting the images from the hotel detail data to\
                         append in csv, %s", e.__str__())
        return []


def build_trivago_csv_file(trivago_hotel_list):
    try:
        # TODO city_id is in sample but not in documentation
        csv_file = io.StringIO()
        writer = csv.writer(csv_file, delimiter=';')
        writer.writerow(common_fields)
        for item in trivago_hotel_list:
            writer.writerow(item)
        csv_file.seek(0)
        return csv_file
    except BaseException as e:
        logger.exception('csv build failed for TrivagoCsvHotelData')
        raise TrivagoCsvDataGenerationError(e.message)


def build_ixigo_csv_file(trivago_hotel_list):
    try:
        fields = common_fields + [
            'acacia',
            'mahogany',
            'maple',
            'oak']
        for index in range(settings.MAX_NUMBER_OF_IMAGES_TO_BE_USED):
            fields.append('image_' + str(index))
        # TODO city_id is in sample but not in documentation
        csv_file = io.StringIO()
        writer = csv.writer(csv_file, delimiter=';')
        writer.writerow(fields)
        for item in trivago_hotel_list:
            writer.writerow(item)
        csv_file.seek(0)
        return csv_file
    except BaseException as e:
        logger.exception('csv build failed for TrivagoCsvHotelData')
        raise IxigoCsvDataGenerationError(e.message)
