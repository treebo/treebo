import datetime
import re

from rest_framework import serializers

from apps.common.exceptions.custom_exception import InvalidBookingDates
from dbcommon.models.hotel import Hotel

room_config_regex = re.compile(r'^(?:(\d-\d),?)+$')


def validate_roomconfig(value):
    if not room_config_regex.match(value):
        raise serializers.ValidationError(
            'Invalid room_config string provided.')


def validate_date_of_request(request_date):
    if request_date < datetime.datetime.today().date():
        raise serializers.ValidationError('Invalid date - past date not allowed')


def validate_hotel_active_status(hotel_id):
    hotel_object = Hotel.objects.filter(id = hotel_id).first()
    if hotel_object is None:
        raise serializers.ValidationError('Hotel is inactive')


def validate_booking_dates(checkin_date, checkout_date):
    if checkout_date.date() <= checkin_date.date():
        raise InvalidBookingDates('Checkout-date same or less than Checkin-date')
