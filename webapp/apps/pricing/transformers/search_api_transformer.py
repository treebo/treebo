import logging
import datetime
from django.conf import settings

from apps.bookingstash.service.availability_service import get_availability_service
from apps.common.utils import round_to_nearest_integer
from apps.discounts import CouponValidationParams
from apps.discounts.services.discount_coupon_services import DiscountCouponServices
from apps.discounts.services.hotel_coupon_auto_apply_service import HotelCouponAutoApplyService
from apps.hotels.service.hotel_service import HotelService
from apps.pricing.api.v4 import RoomPrice
from apps.pricing.services.price_request_dto import PriceRequestDTO_V3
from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.pricing.services.pricing_service_v3 import PricingServiceV3
from apps.pricing.services.pricing_sorting_service import PricingSortingService
from apps.pricing.exceptions import SoaError
from apps.reward.service.reward import RewardService
from common.utilities.date_time_utils import get_day_difference

logger = logging.getLogger(__name__)


class SearchAPITransformer(object):
    reward_service = RewardService()
    hotel_autoapply_service = HotelCouponAutoApplyService()
    discount_coupon_service = DiscountCouponServices()
    pricing_service = PricingServiceV3()
    pricing_service_v2 = PricingServiceV2()
    hotel_service = HotelService()

    @staticmethod
    def _default_price_component(hotel_id, room_type="oak"):
        default_price_component = {
            "cheapest_room": {
                "room_type": room_type if room_type else HotelService().get_room_types_for_hotel(hotel_id)[0],
                "availability": 0},
            "net_payable_amount": 0,
            "wallet_applied": False,
            "wallet_deduction": 0,
            "wallet_type": settings.WALLET_TYPE,
            "tax": 0,
            "pretax_price": 0,
            "rack_rate": 0,
            "sell_price": 0,
            "total_discount_percent": 0,
            "member_discount_applied": False,
            "member_discount_available": False,
            "rate_plan": {},
            "coupon_applied": False,
            "coupon_code": ''}
        return default_price_component

    def get_auto_apply_coupon_details(self, hotel_id_list, utm_source):
        # TODO: replace the following hard-code hack with actual auto apply
        # logic
        coupon_applied = False
        coupon_code = None
        discount_cap = 0
        discount_percentage = 0
        if utm_source:
            coupon_code = "TRBYAAY"
            coupon_applied = True
            discount_cap = 100.0
            discount_percentage = 5.0

        res = {}
        hotel_coupon_info = {}
        hotel_coupon_info['coupon_code'] = coupon_code
        hotel_coupon_info['coupon_applied'] = coupon_applied
        hotel_coupon_info['discount_cap'] = discount_cap
        hotel_coupon_info['discount_percentage'] = discount_percentage

        for hotel_id in hotel_id_list:
            res[hotel_id] = hotel_coupon_info

        return res

    def transform_pricing_for_display(self, hotel_wise_cumulative_prices, hotel_id_list,
                                      room_config, checkin_date, checkout_date,
                                      user, apply_wallet, room_availability_per_hotel=None,
                                      utm_source=None, logged_in_user=False,
                                      sort_criterion='price',source_of_invoke=None, channel=None,
                                      recommended_required=True):
        """
        Transforms the prices returned from pricing service to what the display requires
        :param pricing_data: Pricing Service response
        :param hotel_id_list: List of hotel ids
        :param room_config: room config
        :param check_in: check in
        :param check_out: check out
        :return:
        """

        if source_of_invoke:
            final_data, hotel_ids = self.calculate_cheapest_hotel_with_coupon_and_wallet_for_trivago(apply_wallet, checkin_date,
                                                                                     checkout_date, hotel_id_list,
                                                                                     hotel_wise_cumulative_prices,
                                                                                     logged_in_user, room_config, user,
                                                                                     utm_source, room_availability_per_hotel,
                                                                                     source_of_invoke, channel)
        else:
            final_data, hotel_ids = self.calculate_cheapest_hotel_with_coupon_and_wallet(apply_wallet, checkin_date,
                                                                                         checkout_date, hotel_id_list,
                                                                                         hotel_wise_cumulative_prices,
                                                                                         logged_in_user, room_config,
                                                                                         user,
                                                                                         utm_source,
                                                                                         room_availability_per_hotel,
                                                                                         source_of_invoke, channel)  # final_data
        if sort_criterion == 'recommended' and recommended_required:
            pricing_sorting_service = PricingSortingService()
            pricing_sorting_service.sort_recommended_hotels(final_data, hotel_ids, checkin_date, checkout_date,
                                                            room_config, room_availability_per_hotel)
        return final_data

    def calculate_cheapest_hotel_with_coupon_and_wallet_for_trivago(self, apply_wallet, checkin_date, checkout_date, hotel_id_list,
                                                        hotel_wise_cumulative_prices, logged_in_user, room_config, user,
                                                        utm_source, room_availability_per_hotel,source_of_invoke,
                                                        channel=None):
        if not room_availability_per_hotel:
            room_availability_per_hotel = get_availability_service().get_available_rooms_for_hotels(hotel_id_list, checkin_date,
                                                                                checkout_date, room_config)
        logger.info("Room Availability per hotel: %s", room_availability_per_hotel)

        hotel_ids = [int(hotel_id) for hotel_id in hotel_id_list]
        wallet_applied, total_wallet_balance, total_wallet_points, total_wallet_spendable_amount = self.reward_service.get_wallet_balance(
            user)
        hotel_price_details = self.get_hotel_room_rateplan_map_for_trivago(hotel_ids, hotel_wise_cumulative_prices,
                                                               room_availability_per_hotel)

        hotel_price_details_from_auto_apply, hotel_coupon_map = self.apply_auto_coupon(hotel_price_details, hotel_ids,
                                                                                       utm_source, checkin_date,
                                                                                       checkout_date, room_config, user,
                                                                                       logged_in_user,
                                                                                       room_availability_per_hotel,
                                                                                       source_of_invoke, channel)
        logger.info("hotel_price_details_from_auto_apply %s ", hotel_price_details_from_auto_apply)
        logger.info("hotel_price_details %s ", hotel_price_details)
        final_list_data = []
        final_data={}
        for key, room_price in list(hotel_price_details.items()):
            hotel_id, room_type, rate_plan = key
            if not room_price:
                final_data[hotel_id] = self._default_price_component(hotel_id, room_type)
                continue
            minimal_available_room_price = hotel_price_details_from_auto_apply.get(key, None)
            if not minimal_available_room_price:
                minimal_available_room_price = hotel_price_details.get(key, None)
            price = minimal_available_room_price.price
            if price.sell_price <= 0.00:
                final_data[hotel_id] = self._default_price_component(hotel_id, room_type)
                logger.error("getting sellprice zero from pricing team %s ", hotel_id)
                # slack_alert.publish_alert('getting zero price for hotel `%s`' % (hotel_id), channel=slack_alert.WEB_CHANNEL)
                continue
            total_discount = price.promo_discount + price.member_discount + price.coupon_discount
            aa_coupon_applied = False
            if price.coupon_discount > 0:
                aa_coupon_applied = True
            aa_coupon_code = hotel_coupon_map.get(int(hotel_id), '') if aa_coupon_applied else ''
            discount_percent = round_to_nearest_integer(total_discount / price.base_price * 100)
            member_discount_applied = price.member_discount_applied
            member_discount_available = price.member_discount_available
            meta = minimal_available_room_price.meta

            rate_plan_dict = dict(code=minimal_available_room_price.rate_plan)

            wallet_deductable_amount, wallet_deduction = self.reward_service.get_wallet_breakup(apply_wallet,
                                                                                                total_wallet_spendable_amount,
                                                                                                price.sell_price)
            available_rooms = room_availability_per_hotel.get((int(hotel_id), room_type), 0)

            if meta:
                rate_plan_dict.update(meta)
            final_data={}
            final_data[hotel_id] = {
                "cheapest_room": {
                    "room_type": minimal_available_room_price.room_type,
                    "availability": available_rooms
                },
                "net_payable_amount": price.sell_price - wallet_deduction,
                "wallet_applied": apply_wallet and wallet_applied,
                "wallet_deduction": wallet_deduction,
                "wallet_type": settings.WALLET_TYPE,
                "tax": price.tax,
                "member_discount": price.member_discount,
                "pretax_price": price.pretax_price,
                "rack_rate": price.rack_rate,
                "sell_price": price.sell_price,
                "total_discount_percent": discount_percent,
                "member_discount_applied": member_discount_applied,
                "member_discount_available": member_discount_available,
                "rate_plan": rate_plan_dict,
                "coupon_applied": aa_coupon_applied,
                "coupon_code": aa_coupon_code
            }

            final_list_data.append(final_data)
        return final_list_data, hotel_ids


    def calculate_cheapest_hotel_with_coupon_and_wallet(self, apply_wallet, checkin_date, checkout_date, hotel_id_list,
                                                        hotel_wise_cumulative_prices, logged_in_user, room_config, user,
                                                        utm_source, room_availability_per_hotel,source_of_invoke,
                                                        channel=None):
        if not room_availability_per_hotel:
            room_availability_per_hotel = get_availability_service().get_available_rooms_for_hotels(hotel_id_list, checkin_date,
                                                                                checkout_date, room_config)
        logger.info("Room Availability per hotel: %s", room_availability_per_hotel)
        final_data = {}
        hotel_ids = [int(hotel_id) for hotel_id in hotel_id_list]
        wallet_applied, total_wallet_balance, total_wallet_points, total_wallet_spendable_amount = self.reward_service.get_wallet_balance(
            user)
        hotel_price_details = self.get_hotel_room_rateplan_map(hotel_ids, hotel_wise_cumulative_prices,
                                                                   room_availability_per_hotel)

        try:
            hotel_price_details_from_auto_apply, hotel_coupon_map = self.apply_auto_coupon(hotel_price_details, hotel_ids,
                                                                                       utm_source, checkin_date,
                                                                                       checkout_date, room_config, user,
                                                                                       logged_in_user,
                                                                                       room_availability_per_hotel,source_of_invoke)
        except SoaError as e:
            hotel_price_details_from_auto_apply = hotel_price_details
            hotel_coupon_map = {}

        logger.info("hotel_price_details_from_auto_apply %s ", hotel_price_details_from_auto_apply)
        logger.info("hotel_price_details %s ", hotel_price_details)
        for key, room_price in list(hotel_price_details.items()):
            hotel_id, room_type, rate_plan = key
            if not room_price:
                final_data[hotel_id] = self._default_price_component(
                    hotel_id, room_type)
                continue
            minimal_available_room_price = hotel_price_details_from_auto_apply.get(
                key, None)
            if not minimal_available_room_price:
                minimal_available_room_price = hotel_price_details.get(
                    key, None)
            price = minimal_available_room_price.price
            if price.sell_price <= 0.00:
                final_data[hotel_id] = self._default_price_component(
                    hotel_id, room_type)
                logger.error(
                    "getting sellprice zero from pricing team %s ",
                    hotel_id)
                # slack_alert.publish_alert('getting zero price for hotel `%s`' % (hotel_id), channel=slack_alert.WEB_CHANNEL)
                continue
            total_discount = price.promo_discount + \
                price.member_discount + price.coupon_discount
            aa_coupon_applied = False
            if price.coupon_discount > 0:
                aa_coupon_applied = True
            aa_coupon_code = hotel_coupon_map.get(
                int(hotel_id), '') if aa_coupon_applied else ''
            discount_percent = round_to_nearest_integer(
                total_discount / price.base_price * 100)
            member_discount_applied = price.member_discount_applied
            member_discount_available = price.member_discount_available
            meta = minimal_available_room_price.meta

            rate_plan_dict = dict(code=minimal_available_room_price.rate_plan)

            wallet_deductable_amount, wallet_deduction = self.reward_service.get_wallet_breakup(apply_wallet,
                                                                                                total_wallet_spendable_amount,
                                                                                                price.sell_price)
            available_rooms = room_availability_per_hotel.get((int(hotel_id), room_type), 0)

            if meta:
                rate_plan_dict.update(meta)
            final_data[hotel_id] = {
                "cheapest_room": {
                    "room_type": minimal_available_room_price.room_type,
                    "availability": available_rooms
                },
                "net_payable_amount": price.sell_price - wallet_deduction,
                "wallet_applied": apply_wallet and wallet_applied,
                "wallet_deduction": wallet_deduction,
                "wallet_type": settings.WALLET_TYPE,
                "tax": price.tax,
                "member_discount": price.member_discount,
                "pretax_price": price.pretax_price,
                "rack_rate": price.rack_rate,
                "sell_price": price.sell_price,
                "total_discount_percent": discount_percent,
                "member_discount_applied": member_discount_applied,
                "member_discount_available": member_discount_available,
                "rate_plan": rate_plan_dict,
                "coupon_applied": aa_coupon_applied,
                "coupon_code": aa_coupon_code
            }

        return final_data, hotel_ids

    def apply_auto_coupon(self, hotel_price_details, hotel_ids, utm_source, checkin_date, checkout_date, room_config, user, logged_in_user,
                          room_availability_per_hotel,source_of_invoke=None, channel=None):
        if not utm_source:
            return hotel_price_details, {}
        hotel_coupon_map = self.hotel_autoapply_service.get_hotel_coupons_basis_hotel_ids_and_utm_source(hotel_ids, utm_source, checkin_date,
                                                                                                         checkout_date)
        hotel_percentage_map = self.get_discount_percetage_for_auto_apply(hotel_price_details, hotel_coupon_map, checkin_date, checkout_date,
                                                                          room_config, utm_source, user)

        if hotel_percentage_map:
            room_config_list = room_config.split(",")
            price_request_dto = PriceRequestDTO_V3(checkin_date, checkout_date, hotel_percentage_map, room_config_list,
                                                   logged_in_user=logged_in_user, utm_source=utm_source)
            hotel_wise_cumulative_prices = self.pricing_service.get_search_page_price(price_request_dto)
            if source_of_invoke:
                hotel_price_details = self.get_hotel_room_rateplan_map_for_trivago(hotel_ids, hotel_wise_cumulative_prices, room_availability_per_hotel)
            else:
                hotel_price_details = self.get_hotel_room_rateplan_map(hotel_ids, hotel_wise_cumulative_prices,
                                                                       room_availability_per_hotel)
            logger.info("hotel_percentage_map new for auto apply %s  ", hotel_price_details)
        return hotel_price_details, hotel_coupon_map

    def get_discount_percetage_for_auto_apply(self, hotel_price_details, hotel_coupon_map, checkin_date, checkout_date, room_config, utm_source,
                                              user):
        discount_percentage_map = dict()
        if not hotel_coupon_map:
            return discount_percentage_map
        for key, minimal_available_room_price in list(
                hotel_price_details.items()):
            hotel_id, room_type, rate_plan = key
            aa_coupon_code = hotel_coupon_map.get(int(hotel_id), None)
            if not hotel_coupon_map.get(int(hotel_id), None):
                discount_percentage_map[key] = 0
                continue
            if not minimal_available_room_price:
                discount_percentage_map[key] = 0
                continue
            city_id = None
            price = minimal_available_room_price.price
            pretax_price = price.pretax_price
            member_discount = price.member_discount
            total_amount = price.sell_price
            rack_rate = price.rack_rate
            room_type = None
            booking_channel = None
            paytype = None
            param = CouponValidationParams(
                checkin_date,
                checkout_date,
                room_config,
                aa_coupon_code,
                hotel_id,
                city_id,
                user,
                user,
                pretax_price +
                member_discount,
                member_discount,
                total_amount,
                rack_rate,
                room_type,
                booking_channel,
                paytype,
                utm_source)
            coupon, discount_amount, discount_error = DiscountCouponServices.validate_and_apply_coupon_code(
                param)
            if discount_error:
                # slack_alert.publish_alert(
                #     'error in srp while applying coupon `%s` for hotel `%s` with error `%s`' % (aa_coupon_code, hotel_id, discount_error),
                #     channel=slack_alert.AUTH_ALERTS)
                logger.error(
                    "error while applying coupon on srp for param %s with error %s  ",
                    param,
                    discount_error)
            discount_percent = DiscountCouponServices.get_discount_percentage(
                discount_amount, pretax_price, member_discount, total_amount)
            discount_percentage_map[key] = discount_percent
        return discount_percentage_map

    def get_hotel_room_rateplan_map_for_trivago(self, hotel_ids, hotel_wise_cumulative_prices, room_availability_per_hotel):
        final_data = dict()
        for hotel_id in hotel_ids:
            room_wise_price = hotel_wise_cumulative_prices.get(hotel_id, dict())

            if room_wise_price:
                for room_type, rate_plan_price in list(room_wise_price.items()):
                    minimal_available_room_price = None
                    minimal_available_room_type = None
                    minimal_available_rate_plan = None
                    for rate_plan, rate_plan_price in list(rate_plan_price.items()):
                        room_price = RoomPrice(room_type, rate_plan, rate_plan_price.get('price'),
                                               rate_plan_price.get('meta'))
                        room_type = room_price.room_type
                        available_rooms = room_availability_per_hotel.get((int(hotel_id), room_type), 0)
                        if available_rooms > 0 and room_price.price.base_price > 0 and (not minimal_available_room_price or
                                minimal_available_room_price.price.sell_price > room_price.price.sell_price):

                            minimal_available_room_price = room_price
                            minimal_available_rate_plan = rate_plan
                            minimal_available_room_type = room_type
                    key = hotel_id, minimal_available_room_type, minimal_available_rate_plan
                    final_data[key] = minimal_available_room_price

            else:
                logger.error("room_wise_price is not available for hotel %s ", hotel_id)


        return final_data

    def get_hotel_room_rateplan_map(self, hotel_ids, hotel_wise_cumulative_prices, room_availability_per_hotel):
        final_data = dict()
        for hotel_id in hotel_ids:
            room_wise_price = hotel_wise_cumulative_prices.get(
                hotel_id, dict())
            minimal_available_room_price = None
            minimal_available_room_type = 'oak'
            minimal_available_rate_plan = None
            if room_wise_price:
                for room_type, rate_plan_price in list(
                        room_wise_price.items()):
                    for rate_plan, rate_plan_price in list(
                            rate_plan_price.items()):
                        room_price = RoomPrice(
                            room_type,
                            rate_plan,
                            rate_plan_price.get('price'),
                            rate_plan_price.get('meta'))
                        room_type = room_price.room_type
                        available_rooms = room_availability_per_hotel.get(
                            (int(hotel_id), room_type), 0)
                        if available_rooms > 0 and room_price.price.base_price > 0 and (
                             not minimal_available_room_price or minimal_available_room_price.price.sell_price > room_price.price.sell_price):
                            minimal_available_room_price = room_price
                            minimal_available_rate_plan = rate_plan
                            minimal_available_room_type = room_type
            else:
                logger.error(
                    "room_wise_price is not available for hotel %s ",
                    hotel_id)

            key = hotel_id, minimal_available_room_type, minimal_available_rate_plan
            final_data[key] = minimal_available_room_price
        return final_data

    # TODO:nrx Have to call this for trivago
    def fetch_cheapest_room_prices_available(self, validated_data, hotel_ids, source_of_invoke=None, sort_criterion=None,
                                             user=None, apply_wallet=False, recommended_required=True):
        logged_in_user = True if user and user.is_authenticated() else False

        checkin_date, checkout_date, room_config, utm_source, channel = validated_data['checkin'],\
                                                                validated_data['checkout'], \
                                                                validated_data['roomconfig'], \
                                                                str(validated_data['utm_source']).strip().lower(), \
                                                                validated_data['channel']

        room_config_list = room_config.split(",")  # StashUtils.room_config_string_to_list(room_config)
        room_availability_per_hotel = get_availability_service().get_available_rooms_for_hotels(hotel_ids, checkin_date,
                                                                                checkout_date, room_config)
        if not room_availability_per_hotel and source_of_invoke:
            return

        if not room_availability_per_hotel:
            transformed_price = dict()
            for hotel_id in hotel_ids:
                transformed_price[hotel_id] = self._default_price_component(hotel_id=hotel_id)
            return transformed_price

        hotel_discount_map = self.construct_hotel_discount_map(room_availability_per_hotel, hotel_ids)
        price_request_dto = PriceRequestDTO_V3(checkin_date, checkout_date, hotel_discount_map, room_config_list,
                                            logged_in_user=logged_in_user, channel=channel, utm_source=utm_source)
        try:
            try:
                hotel_wise_cumulative_prices = self.pricing_service.get_search_page_price(price_request_dto)
            except SoaError as e:
                hotel_wise_cumulative_prices = {}
                logger.error("Error fetching price for " + str(price_request_dto) + " :: " + str(e))

            transformed_price = self.transform_pricing_for_display(hotel_wise_cumulative_prices, hotel_ids, room_config,
                                                                checkin_date, checkout_date, user, apply_wallet, room_availability_per_hotel,
                                                                utm_source, logged_in_user, sort_criterion, source_of_invoke,
                                                                recommended_required=recommended_required)
        except Exception as e:
            transformed_price =None
        return transformed_price

    def construct_hotel_discount_map(self, room_availability_per_hotel, hotel_ids):
        hotel_discount_map = {}
        if not room_availability_per_hotel:
            for hotel_id in hotel_ids:
                key = hotel_id, 'oak', 0
                hotel_discount_map[key] = 0
        else:
            for room_config,room_avialable in list(room_availability_per_hotel.items()):
                if room_avialable > 0:
                    hotel_id,room_type = room_config
                    for rate_plan in ['rp','nrp']:
                        key = hotel_id, room_type, rate_plan
                        hotel_discount_map[key] = 0

        return hotel_discount_map
