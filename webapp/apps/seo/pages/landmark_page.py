from django.conf import settings
from django.utils.text import slugify
from django.db.models import Q
from apps.seo.components.breadcrumb import BreadCrumb

from apps.seo.pages.pages import PAGES
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.landmark import SearchLandmark, LandmarkSeoDetails
from services.restclient.contentservicerestclient import ContentServiceClient
from apps.seo.components.categories import Category
from apps.seo.components.cities_nearby import CitiesNearby
from apps.seo.components.landmarks import Landmarks
from apps.seo.components.localities import Localities
from apps.seo.components.popular_cities import PopularCities
from data_services.search_landmark_repository import SearchLandMarkRepository

__author__ = 'ansilkareem'


class LandmarkPage(PAGES):

    search_landmark_repository = RepositoriesFactory.get_landmark_repository()

    def __init__(self, page, query):
        landmarks = self.search_landmark_repository.get_landmarks_by_freetext_url(
            query_string=' '.join(query.split('-')))
        if landmarks:
            self.landmark = landmarks[0]
        else:
            self.landmark = None
        if self.landmark:
            landmark_name = self.landmark.free_text_url
        else:
            city = query.split('-')[-1]
            landmark_name = ' '.join(query.split('-')[:-1])
            self.landmark = self.search_landmark_repository.get_landmark_for_seo_url_and_city_name(
                landmark_name=landmark_name, city_name=city, free_text_url_check_for_landmark=False)
        self.city = self.landmark.city
        self.about_key = 'about_' + '_'.join(landmark_name.lower().split()) + '_' + '_'.join(
            self.city.name.lower().split())
        self.description = ContentServiceClient.getValueForKey(
            None, self.about_key, 1)
        super(LandmarkPage, self).__init__(page, query, landmark_name)

    def update_meta(self, ):
        landmark_seo_details = LandmarkSeoDetails.objects.filter(
            landmark=self.landmark).first()
        if self.landmark.free_text_url:
            full_canonical_url = settings.SITE_HOST_NAME + \
                'hotels-near-' + slugify(self.landmark.free_text_url) + '/'
        else:
            full_canonical_url = settings.SITE_HOST_NAME + 'hotels-near-' + slugify(
                self.landmark.seo_url_name) + \
                '-' + slugify(self.city.slug) + '/'
        if landmark_seo_details:
            self.override_seo_content(
                landmark_seo_details.landmark_seo_title,
                landmark_seo_details.landmark_seo_description,
                landmark_seo_details.landmark_seo_keyword,
                full_canonical_url,
                self.landmark.free_text_url if self.landmark.free_text_url else self.landmark.seo_url_name)
        else:
            title = None
            description = None
            temp_dict = {
                "landmark_name": self.landmark.seo_url_name.title(),
                "city_name": self.landmark.city.name
            }
            if 'landmark_title' in self.footer_data:
                title = self.footer_data['landmark_title'] % temp_dict
            if 'landmark_description' in self.footer_data:
                description = self.footer_data['landmark_description'] % temp_dict
            if title and description:
                self.override_seo_content(title,
                                          description,
                                          None,
                                          full_canonical_url)

    def build_meta(self):
        self.update_meta()
        self.meta_content['content'] = {
            'description': self.description['text'] if self.description else ''
        }
        category = Category(self.city, self.footer_data, [])
        landmark = Landmarks(self.city, self.footer_data, [self.landmark.id])
        locality = Localities(self.city, self.footer_data, [])
        cities_nearby = CitiesNearby(self.city, self.footer_data)
        self.meta_content['content'].update(
            {
                'links': [
                    locality.build(),
                    landmark.build(),
                    category.build(),
                    cities_nearby.build()],
                'breadcrumbs': BreadCrumb(
                    self.city,
                    self.footer_data,
                    self.page,
                    self.item_name).build()})
        popular_cities = PopularCities(
            self.city, self.footer_data, [
                city.id for city in cities_nearby.sorted_nearby_cities])
        self.meta_content['content']['links'].append(popular_cities.build())
