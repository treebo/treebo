import logging
from collections import namedtuple
from datetime import datetime, timedelta
from django.utils import dateformat
from django.conf import settings
from dateutil import parser

import pytz
from pytz import timezone

logger = logging.getLogger(__name__)

DATE_FORMAT = "%Y-%m-%d"

LocalTimeZoneObject = pytz.timezone(settings.TIME_ZONE)


def get_day_difference(first_date, second_date):
    """
    Gets the difference in days between 2 strings
    :param (str) first_date:
    :param (str) second_date:
    :return: (int) The difference
    """
    checkin = parse_date(first_date, DATE_FORMAT).date()
    checkout = parse_date(second_date, DATE_FORMAT).date()
    return (checkout - checkin).days


def parse_date(date_str, date_format, tz=pytz.utc):
    """
    Parses the date using the specified format and sets the timezone on the datetime object
    :rtype: (datetime) datetime: datetime object with the passed timezone. UTC by default
    :param (str) date_str:
    :param (str) date_format:
    :param (tzinfo) tz: Time Zone
    """
    dt = datetime.strptime(date_str, date_format)
    return tz.localize(dt)


def is_valid_date(date, date_format):
    """
    Validates if the passed date is in the format passed
    :param (str) date:
    :param (str) date_format:
    :return (bool):
    """
    try:
        date = datetime.strptime(date, date_format)
        return True
    except BaseException:
        logger.debug("Inside exception : Not a valid date")
        return False


def get_time_diff(from_date, to_date):
    try:
        time_diff = from_date - to_date
        seconds = time_diff.total_seconds()
        minutes = int(seconds) / 60
        hours = int(minutes) / 60
        days = int(hours) / 24
        return hours, minutes
    except Exception as exc:
        logger.debug(exc)
        return '', ''


def build_time_string(hours, minutes):
    time_string = ''
    hour_string = ' hour' if hours <= 1 else ' hours'
    minute_string = ' minute' if minutes < 1 else ' minute'
    try:
        time_string = str(
            hours) + hour_string if hours > 0 else str(minutes) + minute_string
        return time_string
    except Exception as exc:
        logger.exception(exc)
        return time_string


def build_date(input_date):
    """
    Formats the input_date into human friendly format. E.g. 1st Nov 2015, 23rd Nov 2015
    :param input_date:
    :return:
    """
    formatted_date = dateformat.format(input_date, "jS M y")
    logger.info("Formatted date for input date %s is %s", input_date, formatted_date)
    return formatted_date


def get_utc_date_time():
    """
    Get utc date time object
    :return (datetime):
    """
    return datetime.now(pytz.utc)


def get_current_ist_date_time():
    """
    Get today's date in India Standard Time
    :return: datetime
    """
    return datetime.now(get_ist_time_zone())


def get_ist_time_zone():
    """
    Get IST timezone
    :return: timezone
    """
    return timezone('Asia/Kolkata')


def format_date(date_time):
    """
    Formats dates into common string date format
    :param (datetime) date_time:
    :return:
    """
    return date_time.strftime(DATE_FORMAT)


def is_range_overlapping(start_1, end_1, start_2, end_2):
    """
    Function that finds if ranges are overlapping
    :param (date) start_1:
    :param (date) end_1:
    :param (date) start_2:
    :param (date) end_2:
    :return:
    """
    Range = namedtuple('Range', ['start', 'end'])
    range1 = Range(start=start_1, end=end_1)
    range2 = Range(start=start_2, end=end_2)
    latest_start = max(range1.start, range2.start)
    earliest_end = min(range1.end, range2.end)
    return True if (earliest_end - latest_start).days > 0 else False


def localize(timestamp, time_zone='utc'):
    """
     localizes given naive datetime to current timezone.
     Use this instead of timestamp.replace(tzinfo=pytz.timezone(settings.TIME_ZONE))
     :param: timestamp - A naive python datetime object
     :param: timezone - time zone string: Ex: 'Asia/Kolkata'
     :return: A timezone aware python datetime localized to timezone in settings
    """
    tz = pytz.timezone(time_zone)
    return tz.localize(timestamp)


def get_current_ist_date():
    return format_date(get_current_ist_date_time())


def is_naive_datetime(d):
    return d.tzinfo is None or d.tzinfo.utcoffset(d) is None


def utc_now():
    return localize(datetime.utcnow())


def maybe_convert_string_to_datetime(date, ignoretz=False):
    if isinstance(date, str):
        return parser.parse(date, ignoretz=ignoretz)
    return date


def application_tz_now():
    utc_with_timezone = utc_now()
    return utc_with_timezone.astimezone(LocalTimeZoneObject)


def daterange(start_date, end_date):
    """
    Returns a list of dates between start date and end date
    Type casting to date is done because start date can be 24th july 12 PM
    and end date can be 25th july 11AM. days diff becomes 0.

    :param start_date: date/datetime
    :param end_date: date/datetime
    :return: list of datetime in between including start date but not end date
    """
    _start_date = start_date
    if isinstance(start_date, datetime):
        _start_date = start_date.date()

    _end_date = end_date
    if isinstance(end_date, datetime):
        _end_date = end_date.date()

    first_date = min(start_date, end_date)  # helps if the end date is less than start date to not mess up the order
    for delta in range(int((_end_date - _start_date).days)):
        yield first_date + timedelta(delta)


def blindly_replace_with_application_timezone(naive_timestamp):
    logger.warning("Replacing naive timestamp {d}'s tzinfo with {tz}".format(d=naive_timestamp, tz=settings.TIME_ZONE))
    return localize(naive_timestamp, time_zone=settings.TIME_ZONE)
