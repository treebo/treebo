import logging

from rest_framework.response import Response

from apps.auth.services.user_profile_service import UserProfileService
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class CreateOrUpdateUPSUser(TreeboAPIView):

    @log_args(logger)
    def post(self, request):
        user_data = request.data
        try:
            UserProfileService().create_or_update_user_for_primary_guest(user_data, request.user)
            return Response(dict(status="success", msg='User Created successfully'))
        except Exception as e:
            return Response(dict(error="Failed creating user in UPS", status=400))
