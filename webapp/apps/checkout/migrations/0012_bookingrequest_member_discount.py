# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0011_bookingrequest_rate_plan_meta'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='member_discount',
            field=models.DecimalField(
                default=0,
                null=True,
                max_digits=9,
                decimal_places=4),
        ),
    ]
