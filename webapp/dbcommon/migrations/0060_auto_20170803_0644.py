# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0059_categoryseodetails_city'),
    ]

    operations = [
        migrations.CreateModel(
            name='FacilityCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('name', models.TextField(null=True, blank=True)),
                ('status', models.BooleanField(default=True)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'hotels_facilitycategory',
                'verbose_name': 'Facility Category',
                'verbose_name_plural': 'Facility Categories',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AddField(
            model_name='facility',
            name='to_be_shown',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='facility',
            name='categories',
            field=models.ManyToManyField(to='dbcommon.FacilityCategory', blank=True),
        ),
    ]
