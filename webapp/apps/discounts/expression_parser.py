import logging
from common.constants import common_constants
from common.exceptions.treebo_exception import TreeboValidationException

__author__ = 'amithsoman'

"""
Usage
expressions = ['72541 AND 72542 OR 72543',
               '',
               '(72541) AND 72542 OR 72543',
               '(72541 AND 72542) OR 72543',
               '72541 OR (72542 AND 72543)',
            ]
constraintValidator = ConstraintValidator(**params)
for e in expressions:
    print ExpressionParser(expression, constraintValidator)
"""


def hasPrecedence(operator1, operator2):
    if operator2 == '(' or operator2 == ')':
        return False
    return True


class ExpressionParser(object):
    """
    Using the logic defined in this tutorial
    http://www.geeksforgeeks.org/expression-evaluation/
    """

    def __init__(self, expression, constraintValidator):
        expression = expression.replace('(', ' ( ').replace(')', ' ) ').strip()
        self.tokens = expression.split()
        self.constraintValidator = constraintValidator
        self.value_stack = []
        self.operator_stack = []

    def apply_operation(self, operand_one, operand_two, operator):
        operands = [operand_one, operand_two]
        if operator == 'AND':
            return next(iter([x for x in operands if not x[0]]), (True, ''))
        else:
            return next(iter([x for x in operands if x[0]]),
                        (False, operand_one[1]))

    def evaluate_one_operator(self):
        operator = self.operator_stack.pop()
        operand_one, operand_two = self.value_stack.pop(), self.value_stack.pop()
        value, message = self.apply_operation(
            operand_one, operand_two, operator)
        self.value_stack.append((value, message))

    def evaluate_partial_expression(self, token):
        while self.operator_stack and hasPrecedence(
                token, self.operator_stack[-1]):
            self.evaluate_one_operator()

    def _is_constraint(self, token):
        try:
            int(token)
            return True
        except ValueError:
            return False

    def evaluate_constraint(self, token):
        self.constraintValidator.validatorFactory(token)
        if self.constraintValidator.message != common_constants.SUCCESS:
            return False, self.constraintValidator.message
        return True, ''

    def evaluate_and_convert_to_postfix(self):
        for token in self.tokens:
            if self._is_constraint(token):
                value, message = self.evaluate_constraint(token)
                self.value_stack.append((value, message))
            elif token == '(':
                self.operator_stack.append(token)
            elif token == ')':
                """
                it will evaluate the expression till the first left parenthesis
                """
                self.evaluate_partial_expression(token)
                self.operator_stack.pop()  # pops the left parenthesis and discards it
            else:
                """
                it will evaluate till operator stack is not empty or the operators in operator stack
                have less precedence than this operator
                """
                self.evaluate_partial_expression(token)
                self.operator_stack.append(token)

    def evaluate_postfix(self):
        while self.operator_stack:
            self.evaluate_one_operator()

    def parse_expression(self):
        self.evaluate_and_convert_to_postfix()
        self.evaluate_postfix()
        if not self.value_stack:
            raise TreeboValidationException('No constraint provided')
        value, message = self.value_stack[0]
        if not value:
            raise TreeboValidationException(message)
        return value, message
