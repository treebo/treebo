import logging

logger = logging.getLogger(__name__)


class PageHelper:
    def __init__(self):
        pass

    @staticmethod
    def getRoomConfigData(roomConfig):
        rooms = roomConfig.split(",")
        roomConfigData = []
        for room in rooms:
            if "-" in room:
                roomSplit = room.split("-")
                roomConfigData.append(
                    {'adults': int(roomSplit[0]), 'children': int(roomSplit[1])})
            else:
                roomConfigData.append({'adults': int(room), 'children': 0})
        return roomConfigData

    @staticmethod
    def parseRoomConfig(roomConfig):
        adults = children = roomRequired = 0
        try:
            rooms = roomConfig.split(",")
            roomRequired = len(rooms)
            adults = 0
            children = 0
            for room in rooms:
                if "-" in room:
                    roomSplit = room.split("-")
                    adults += int(roomSplit[0])
                    children += int(roomSplit[1])
                else:
                    adults += int(room)
        except ValueError:
            logger.error("Invalid Room Config received :" + roomConfig)
            adults = 1
            children = 0
            roomRequired = 1
        return adults, children, roomRequired
