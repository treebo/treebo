import logging

from base.views.template import TreeboTemplateView

__author__ = 'devang'

logger = logging.getLogger(__name__)


class Iesupport(TreeboTemplateView):
    template_name = "ie_not_supported/index.html"
    redirect_mobile_to_desktop_view = True

    def getData(self, request, args, kwargs):
        return {'scripts': [], 'styles': ['desktop/css/ie_not_supported.css']}
