from common.services.feature_toggle_api import FeatureToggleAPI
from web_sku.catalog.services.sku_repository import SkuRepository


class FeatureImplementation:

    @staticmethod
    def add_property_sku(cs_id, room_config):
        property_skus = {}
        if FeatureToggleAPI.is_enabled('sku', 'itinerary_api', False):
            property_skus = SkuRepository(room_configs=room_config, cs_property_ids=cs_id).get_property_skus()
        return property_skus

