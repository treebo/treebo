import logging

from rest_framework import serializers

from dbcommon.models.room import Room

logger = logging.getLogger(__name__)

__author__ = 'Rajdeep'


class RoomSerializer(serializers.ModelSerializer):
    amenities = serializers.SerializerMethodField('newField')
    room_size = serializers.SerializerMethodField('size')
    price_per_night = serializers.CharField(source='price')
    occupancy = serializers.SerializerMethodField('get_occ')
    internal_type = serializers.SerializerMethodField('get_internal')
    room_type = serializers.CharField(source='room_type_code')
    max_adult = serializers.SerializerMethodField('getMaxGuest')
    max_children = serializers.SerializerMethodField()
    available = serializers.SerializerMethodField('getRoomAvailability')
    room_image_url = serializers.SerializerMethodField('getImageForRoom')

    def getRoomAvailability(self, room):
        if room.available > 0:
            return True
        return False

    def get_max_children(self, room):
        return room.max_adult_with_children - 1

    def getMaxGuest(self, room):
        return room.max_guest_allowed

    def get_internal(self, room):
        room_type = "Other"
        if room.room_type_code == 'Acacia':
            room_type = "solo"
        elif room.room_type_code == 'Oak':
            room_type = "standard"
        elif room.room_type_code == 'Maple':
            room_type = "deluxe"
        elif room.room_type_code == 'Mahogany':
            room_type = "premium"

        return room_type

    def getImageForRoom(self, room):
        message = "No Images for room"
        singleRoomImage = ""
        try:
            image = room.image_set.first()
            singleRoomImage = str(image.url)
            message = "success"
            logger.debug(message + " Image found for room")
        except Exception as exc:
            logger.debug(message)

        return singleRoomImage

    def get_occ(self, room):
        occupancy_message = "%(guest_count)s Guests (Max %(adult_count)s %(adult_string)s)"
        occupancy_dict = {
            'guest_count': room.max_adult_with_children,
            'adult_count': room.max_guest_allowed,
            'adult_string': ''}
        if room.max_guest_allowed > 1:
            occupancy_dict['adult_string'] = "Adults"
        else:
            occupancy_dict['adult_string'] = "Adult"
        return occupancy_message % occupancy_dict

    def size(self, room):
        return str(room.room_area) + ' SQ FT'

    def newField(self, room):
        allFacilitiesInHotel = room.facility_set.all()
        hotelFacilities = []
        for singleFacility in allFacilitiesInHotel:
            hotelFacilities.append(singleFacility.name)

        amenities = ', '.join(hotelFacilities)
        return amenities

    class Meta:
        model = Room
        fields = (
            'id',
            'room_type',
            'amenities',
            'room_size',
            'price_per_night',
            'occupancy',
            'internal_type',
            'max_adult',
            'available',
            'room_image_url',
            'max_adult_with_children',
            'max_children')
