# Map Actions to events and associate common actions if needed
DEVICE_TO_CONFIG_MAP = {
    "0": "W",
    "1": "M"
}

ACTION_TO_EVENTS_MAPPER = {
    "DEFAULT": {
    },
    "M": {
        "__default__": {"page_id": 1, "include_list": []},
        "__main__": {"page_id": 2, "include_list": []},
        "__header__": {"page_id": 3, "include_list": []},
        "__search__": {"page_id": 4, "include_list": []},
        "/": {"page_id": 11, "include_list": [1, 2, 3, 4]},
    },
    "W": {

    }
}

# Detailed config for page events and available elements on page.
# Use currentElement where-ever $(this) is needed.
CLIENT_SIDE_EVENTS_LOAD = {
    1: {"available_elements": {
        "element_name": "clientAnalyticsObj.getElementName(currentElement)",
        "click_count": 1
    },
        "element_override": {},
        "events_list": [
            {
                "event_category": "click_event",
                "event_name": "Clicked Element",
                "event_selector": ".analytics_capture_click",
                "event_source": "Clicked Element",
                "event_action": "click",
                "event_variables": "element_name,click_count",
                "event_computed_variables": {},
            },
    ]
    },
    2: {"available_elements": {
    },
        "element_override": {},
        "events_list": [
            {
                "event_category": "click_event",
                "event_name": "Search bar click",
                "event_selector": ".analytics_search_bar",
                "event_source": "Clicked On Home Page Search Bar",
                "event_action": "click",
                "event_variables": "element_name,click_count",
                "event_computed_variables": {},
            },
    ]
    },
    3: {"available_elements": {
    },
        "element_override": {},
        "events_list": [
            {
                "event_category": "click_event",
                "event_name": "Home menu click",
                "event_selector": ".analytics_home_menu",
                "event_source": "Clicked On Home Menu",
                "event_action": "click",
                "event_variables": "element_name,click_count",
                "event_computed_variables": {},
            },
    ]
    },
    4: {"available_elements": {
        "search_query": "$('#searchInput').val()",
        "checkin_date": "$('#checkIn').val()",
        "checkout_date": "$('#checkOut').val()",
        "guest_count": "$('#guests').val()",
        "rooms_count": "$('#rooms').val()"
    },
        "element_override": {},
        "events_list": [
            {
                "event_category": "button_event",
                "event_name": "Search a Treebo",
                "event_selector": ".analytics_search_a_treebo",
                "event_source": "Clicked On Search a Treebo Button",
                "event_action": "click",
                "event_variables": "search_query,checkin_date,checkout_date,guest_count,rooms_count",
                "event_computed_variables": {},
            },
    ]
    },
    11: {"available_elements": {
    },
        "element_override": {},
        "events_list": [
    ]
    },
}
