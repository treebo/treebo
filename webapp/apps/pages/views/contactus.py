import logging

from base.views.template import TreeboTemplateView
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'devang'

logger = logging.getLogger(__name__)


class Contactus(TreeboTemplateView):
    template_name = "contactus/index.html"
    redirect_mobile_to_desktop_view = True

    def getData(self, request, args, kwargs):
        seo = ContentServiceClient.getValueForKey(request, 'seo_pages', 1)
        return {
            'scripts': ['desktop/js/contactus.js'],
            'styles': ['desktop/css/contactus.css'],
            'googleMaps': True,
            'seo': seo['contactus'],
            'footerCopyrightDisabled': True}

    def getMobileData(self, request, args, kwargs):
        return {'styles': ['css/terms.css']}
