from rest_framework import serializers
from apps.bookings.serializers import JSONSerializerField


class BookingConfirmationDTO(serializers.Serializer):
    order_id = serializers.CharField(max_length=50)
    pg_payment_id = serializers.CharField(max_length=50)
    pg_payment_signature = serializers.CharField(max_length=100)
    pg_order_id = serializers.CharField(max_length=50)
    ps_order_id = serializers.CharField(max_length=50)


class BookingConfirmationV5(serializers.Serializer):
    ps_order_id = serializers.CharField(
        max_length=50,
        allow_null=True,
        allow_blank=True,
        default="")
    order_id = serializers.CharField(max_length=40)
    gateway = serializers.CharField(
        max_length=40,
        allow_null=True,
        allow_blank=True,
        default="")
    pg_meta = JSONSerializerField(required=False, default={})
