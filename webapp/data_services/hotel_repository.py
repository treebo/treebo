import logging

from apps.common.exceptions.custom_exception import HotelNotFoundException
from apps.hotels.serializers.hotel_serializer import HotelSerializer, BasicHotelSerializer
from data_services.decorators import SingletonDecorator
from data_services.exceptions import HotelDoesNotExist

logger = logging.getLogger(__name__)


@SingletonDecorator
class HotelRepository:
    def __init__(self, hotel_web_repository, cs_api_client, is_cs_api_querying_enabled=True):
        self.is_cs_api_querying_enabled = is_cs_api_querying_enabled
        self.hotel_web_repository = hotel_web_repository
        self.cs_api_client = cs_api_client

    def get_active_hotel_list(self):
        return self.hotel_web_repository.get_active_hotels()

    def filter_hotels(self, related_entities_list: list = None, **kwargs):
        return self.hotel_web_repository.filter_hotels(related_entities_list, **kwargs)

    def get_all_hotels(self):
        return self.hotel_web_repository.get_all_hotels()

    def get_hotels_for_id_list(self, hotel_ids):
        return self.hotel_web_repository.get_hotels_for_id_list(hotel_ids=hotel_ids)

    def get_enabled_hotels_for_city(self, city_id):
        return self.hotel_web_repository.get_enabled_hotels_for_city(city_id)

    def get_single_hotel(self, related_entities_list=None, **kwargs):
        hotel_from_direct = self.hotel_web_repository.get_single_hotel(related_entities_list, **kwargs)
        logger.debug("The hotel returned from direct db is %s", hotel_from_direct.name)
        if hotel_from_direct and hotel_from_direct.cs_id:
            logger.debug("Trying to get hotel from CS with cs id %s", hotel_from_direct.cs_id)
            hotel_from_cs = self.get_hotel_from_cs(hotel_from_direct.cs_id)
            return hotel_from_cs if hotel_from_cs else hotel_from_direct
        return hotel_from_direct

    def get_hotel_by_id(self, hotel_id, related_entities_list=None):
        hotel_from_direct = self.hotel_web_repository.get_hotel_by_id(hotel_id, related_entities_list)
        if not hotel_from_direct or hotel_from_direct.status == 0:
            return hotel_from_direct
        if hotel_from_direct and hotel_from_direct.cs_id:
            hotel_from_cs = self.get_hotel_from_cs(hotel_from_direct.cs_id)
            return hotel_from_cs if hotel_from_cs else hotel_from_direct
        return hotel_from_direct

    def get_hotel_by_name(self, hotel_name, related_entities_list=None):
        hotels_from_direct = self.hotel_web_repository.filter_hotels(name=hotel_name,
                                                                     related_entities_list=related_entities_list)
        if hotels_from_direct:
            hotel_from_direct = hotels_from_direct[0]
            if hotel_from_direct and hotel_from_direct.cs_id:
                hotel_from_cs = self.get_hotel_from_cs(hotel_from_direct.cs_id)
                return hotel_from_cs if hotel_from_cs else hotel_from_direct
            return hotel_from_direct

    def get_hotel_details_using_cs_id_with_all_related_objects(self, cs_hotel_id):
        hotel_from_direct = self.hotel_web_repository.get_hotel_details_using_cs_id_with_all_related_objects(cs_hotel_id)
        logger.info("Hotel returned form direct is %s, with id %d",  hotel_from_direct.name, hotel_from_direct.id)
        hotel_from_cs = None
        if self.is_cs_api_querying_enabled and hotel_from_direct.cs_id:
            hotel_from_cs = self.cs_api_client.get_hotel_with_amenities(hotel_from_direct.cs_id)
        return hotel_from_cs if hotel_from_cs else hotel_from_direct

    def get_hotel_details_with_all_related_objects(self, hotel_id, direct=False):
        hotel_from_direct = self.hotel_web_repository.get_hotel_details_with_all_related_objects(hotel_id)
        logger.info("Hotel returned form direct is %s, with id %d",  hotel_from_direct.name, hotel_from_direct.id)
        if direct:
            return hotel_from_direct
        hotel_from_cs = None
        if self.is_cs_api_querying_enabled and hotel_from_direct.cs_id:
            hotel_from_cs = self.cs_api_client.get_hotel_with_amenities(hotel_from_direct.cs_id)
        return hotel_from_cs if hotel_from_cs else hotel_from_direct

    def get_all_hotelogix_ids(self, ids_list=None):
        return self.hotel_web_repository.get_all_hotelogix_ids(ids_list=ids_list)

    def get_aggregate_value(self, filter_list, aggregate_function_name, aggregate_field_name):
        return self.hotel_web_repository.get_aggregate_value(filter_list, aggregate_function_name=aggregate_function_name,
                                                             aggregate_field_name=aggregate_field_name)

    def get_total_hotel_count(self, only_active):
        return self.hotel_web_repository.get_total_hotel_count(only_active)

    def get_all_active_hotel_ids(self):
        return self.hotel_web_repository.get_all_active_hotel_ids()

    def get_rooms_for_hotel(self, hotel):
        if hasattr(hotel, 'rooms'):
            try:
                return hotel.rooms.all()  # hotel is django model
            except Exception as e:
                return hotel.rooms
        return []

    def get_serialized_data_for_hotel(self, hotel):
        from dbcommon.models.hotel import Hotel
        if hasattr(hotel, 'toJSON()'):
            return hotel.toJson()
        if isinstance(hotel, Hotel):
            return HotelSerializer(instance=hotel).data

    def get_image_set_for_hotel(self, hotel):
        if hasattr(hotel, 'image_set'):
            try:
                return hotel.image_set.all()
            except Exception as e:
                return hotel.image_set
        return []

    def get_sitemap_url(self, hotel):
        if hasattr(hotel, 'sitemap_url'):
            return hotel.sitemap_url
        return hotel.get_sitemap_url()

    def get_showcased_image_url(self, hotel):
        try:
            if hasattr(hotel, 'showcased_image_url'):
                showcased_image_url = hotel.showcased_image_url
            else:
                showcased_image_url = hotel.get_showcased_image_url()
            if hasattr(showcased_image_url, 'url') and showcased_image_url.url:
                return showcased_image_url.url
            return ""
        except Exception as e:
            return ''

    def get_directions_set_for_hotel(self, hotel):
        if hasattr(hotel, 'directions_set'):
            try:
                return hotel.directions_set.all()
            except Exception as e:
                return hotel.directions_set
        return []

    def get_facilities_for_hotel(self, hotel):
        if hasattr(hotel, 'facility_set'):
            try:
                return hotel.facility_set.all()
            except Exception as e:
                return hotel.facility_set
        return []

    def get_highlights_for_hotel(self, hotel):
        if hasattr(hotel, 'hotel_highlights'):
            try:
                return hotel.hotel_highlights.all()
            except Exception as e:
                return hotel.hotel_highlights
        return []

    def get_minimum_serialized_data(self, hotel):
        from dbcommon.models.hotel import Hotel
        if hasattr(hotel, 'toJSON()'):
            hotel_data = hotel.toJson()
            return self.get_minumum_hotel_data(hotel_data)
        if isinstance(hotel, Hotel):
            return BasicHotelSerializer(instance=hotel).data

    def get_location_url(self, hotel):
        if hasattr(hotel, 'location_url'):
            return hotel.location_url
        try:
            return hotel.get_location_url
        except Exception as e:
            return None

    def get_consumer_key(self, hotel):
        if hasattr(hotel, 'consumer_key'):
            return hotel.consumer_key
        try:
            return hotel.get_consumer_key()
        except Exception as e:
            return None

    def get_consumer_secret(self, hotel):
        if hasattr(hotel, 'consumer_secret'):
            return hotel.consumer_secret
        try:
            return hotel.get_consumer_secret()
        except Exception as e:
            return None

    def get_canonical_url(self, hotel):
        canonical_url = getattr(hotel, 'hotel_canonical_url', None)
        if not canonical_url:
            try:
                canonical_url = hotel.get_hotel_canonical_url()
            except Exception as e:
                canonical_url = None

        return canonical_url

    def get_city_name(self, hotel):
        city_name = getattr(hotel, 'city_name', None)
        if not city_name:
            try:
                city_name = hotel.get_city_name()
            except Exception as e:
                city_name = hotel.city.name
        return city_name

    def get_address(self, hotel):
        address = getattr(hotel, 'address', None)
        if not address:
            try:
                return hotel.address()
            except Exception as e:
                address = None
        return address

    def get_minumum_hotel_data(self, hotel_json):
        result = {'id': hotel_json['id'], 'name': hotel_json['name'], 'city': hotel_json['city']['name'],
                  'state': hotel_json['state']['name'], 'hotelogix_id': hotel_json['hotelogix_id'],
                  'phone_number': hotel_json['phone_number'], 'room_count': hotel_json['room_count'],
                  'checkin_time': hotel_json['checkin_time'], 'checkout_time': hotel_json['checkout_time'],
                  'street': hotel_json['street'], 'latitude': hotel_json['latitude'],
                  'longitude': hotel_json['longitude'], 'hotelogix_name': hotel_json['hotelogix_name'],
                  'status': hotel_json['status']}
        return result

    def get_hotel_from_cs(self, cs_id):
        if not self.is_cs_api_querying_enabled:
            logger.debug("CS API querying is not enabled, returning none")
            return None
        cs_hotel = None
        try:
            cs_hotel = self.cs_api_client.get_hotel_data_from_cs(cs_id)
        except Exception as e:
            logger.exception(e)
        return cs_hotel

    def get_hotel_by_id_from_web(self, hotel_id):
        return self.hotel_web_repository.get_hotel_by_id(hotel_id)

    def get_hotel_by_id_from_web_or_raise(self, hotel_id):
        hotels = self.hotel_web_repository.filter_hotels(id=hotel_id)
        if not hotels:
            raise HotelDoesNotExist()
        return hotels[0]
