from apps.payments.providers.treebo_realization import GrowthServiceProvider


class TreeboRealizationClient:

    def get_partpay_amount(self, post_tax_amount, check_in, check_out, channel_code):
        params = dict(post_tax_amount=post_tax_amount,
                      check_in=check_in,
                      check_out=check_out,
                      channel_code=channel_code)
        part_pay_amount_data = GrowthServiceProvider().partpay_amount_api_call(params)
        part_pay_amount = part_pay_amount_data['data']['payable_amount'] if part_pay_amount_data else 0
        part_pay_percent = part_pay_amount_data['data']['percentage_to_pay'] if part_pay_amount_data else 0
        return part_pay_amount, part_pay_percent

    def generate_payment_link(self, crs_booking_id, hotel_id, part_payment=False):
        data = dict(group_code=crs_booking_id, hotel_code=hotel_id, full_payment=not part_payment)
        payment_link_data = GrowthServiceProvider().generate_payment_link_api_call(data)
        payment_url = payment_link_data['data']['payment_url']
        payable_amount = payment_link_data['data']['payable_amount']
        return payment_url, payable_amount
