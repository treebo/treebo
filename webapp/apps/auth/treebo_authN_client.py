import logging

import requests
from django.conf import settings

from apps.auth.pms_user import PmsUser
from apps.common.exceptions.custom_exception import AuthNClientException

logger = logging.getLogger(__name__)


class TreeboAuthNClient:
    def create_user(self, first_name, last_name, email, pin, phone_number=None):
        data = dict(
            first_name=first_name,
            last_name=last_name,
            password=pin,
            app_type="PMS",
            phone_number=None,
            email=email,
            redirect_url=".",
        )
        url = settings.TREEBO_AUTH_SERVER + "treeboauth/profile/v1/register"
        response = requests.post(url, json=data)

        if response.status_code != 201:
            logger.error('Error while creating user: {text} with status {status_code}'.format(text=response.text,
                                                                                              status_code=response.status_code))
            raise AuthNClientException(message=response.text)

        data = response.json()["data"]

        response_data = PmsUser(data['auth_id'],
                                email,
                                first_name,
                                last_name,
                                phone_number)
        return response_data

    def get_user_by_email(self, email, phone_number=None):
        url = settings.TREEBO_AUTH_SERVER + 'treeboauth/profile/v1/user/validation/email/'
        params = dict(email=email)
        logger.info("Getting user info from AuthN: {url} {params}".format(url=url, params=params))

        response = requests.get(url, params=params)
        if response.status_code == 404:
            logger.error("User {email} not found in Authn".format(email=email))
            return None

        if response.status_code != 200:
            logger.error('Error while getting details from AuthN: {text} with status code {status_code}'.format(text=response.text,
                                                                                                                status_code=response.status_code
                                                                                                                ))
            raise AuthNClientException(message=response.text)

        data = response.json()["data"]
        response_data = PmsUser(data['id'],
                                data['email'],
                                data['first_name'],
                                data['last_name'],
                                data.get('phone_number', None))
        return response_data

    def get_user_by_phone(self, phone_number):
        url = settings.TREEBO_AUTH_SERVER + 'treeboauth/profile/v1/user/validation/phone-number/'
        data = dict(phone_number=phone_number)
        logger.info("Getting user info from AuthN: {url}, {data}".format(url=url, data=data))
        response = requests.post(url, json=data)
        if response.status_code == 404:
            logger.error('User {phone_number} not found in Authn'.format(phone_number=phone_number))
            return None
        if response.status_code != 200:
            logger.error('Error while getting details from AuthN: {text} with status code {status_code}'.format(text=response.text,
                                                                                                                status_code=response.status_code
                                                                                                                ))
            raise AuthNClientException(message=response.text)

        data = response.json()['data']
        response_data = PmsUser(data['id'],
                                data['email'],
                                data['first_name'],
                                data['last_name'],
                                data.get('phone_number', None))

        return response_data, data

    def update_user_details(self, authn_id, first_name, last_name, email, phone_number):
        url = settings.TREEBO_AUTH_SERVER + 'treeboauth/profile/v1/update/'
        data = dict(
            auth_id=authn_id,
            first_name=first_name,
            last_name=last_name,
            email=email,
            phone_number=phone_number,
            gender=""
        )
        response = requests.put(url, json=data)
        if response.status_code != 200:
            logger.error('Error while updating details {text} with status code {status_code}'.format(text=response.text,
                                                                                                     status_code=response.status_code))
            raise AuthNClientException(message=response.text)
        return True

    def reset_auth_password(self, email, pin):
        auth_url = settings.TREEBO_AUTH_SERVER + 'treeboauth/profile/passwordupdate'
        data_dict = {'email': email, 'password': pin}
        headers = {
            'Authorization': 'basic ' + settings.AUTH_BASIC_HEADER,
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        logger.info("Calling auth reset password with payload:{data_dict}, headers:{headers}".format(data_dict=data_dict,
                                                                                                     headers=headers))
        response = requests.post(auth_url, data=data_dict, headers=headers)
        if response.status_code != 201:
            logger.error('Error when resetting password {text} with status code {status_code}'.format(text=response.text,
                                                                                                      status_code=response.status_code))
            raise AuthNClientException(message=response.text)
        return response.json()['status']
