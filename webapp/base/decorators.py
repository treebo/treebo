import functools
from django.conf import settings
from rest_framework.status import HTTP_400_BAD_REQUEST

from apps.common.exceptions.custom_exception import AuthException


def read_api_key_from_header(request, request_headers=None):
    api_key = request.META.get("HTTP_X_API_KEY") if not request_headers else request_headers.get("HTTP_X_API_KEY")
    return api_key

def authorize_api_key(func):
    @functools.wraps(func)
    def wrapper(self, request, *args, **kwargs):
        """
        Wrapper
        :param args:
        :param kwargs:
        :return:
        """
        api_key = read_api_key_from_header(request)
        if api_key != settings.API_KEY:
            raise AuthException('Invalid token.', HTTP_400_BAD_REQUEST)
        r_val = func(self, request, *args, **kwargs)
        return r_val
    return wrapper