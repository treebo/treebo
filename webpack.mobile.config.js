/* eslint-disable */
var webpack = require('webpack');
var merge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ManifestPlugin = require('webpack-manifest-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');
var glob = require('glob');

var commonConfig = {
	context: path.resolve(__dirname, 'webapp/static/mobile'),

	entry: {
		aboutus: ['./js/aboutus.js'],
		account: ['./js/account.js'],
		app: ['./js/app.js'],
		confirmation: ['./js/confirmation.js'],
		contactus: ['./js/contactus.js'],
		details: ['./js/details.js'],
		faq: ['./js/faq.js'],
		forgotpassword: ['./js/forgotpassword.js'],
		itinerary: ['./js/itinerary.js'],
		joinus: ['./js/joinus.js'],
		landing: ['./js/landing.js'],
		login: ['./js/login.js'],
		policy: ['./js/policy.js'],
		referral: ['./js/referral.js'],
		register: ['./js/register.js'],
		resetpassword: ['./js/resetpassword.js'],
		searchresults: ['./js/searchresults.js'],
		terms: ['./js/terms.js'],
		transaction: ['./js/transaction.js'],
		'fot/landing_fot': ['./js/fot/landing_fot.js'],
		'fot/signup_fot': ['./js/fot/signup_fot.js'],
		'fot/search_audit_fot': ['./js/fot/search_audit_fot.js'],
		// chunks
		common: ['./css/common/common.less'],
		// vendor: ['jquery', 'moment', 'parsleyjs', 'react', 'react-dom'],
		// no entry
		confirmationerror: ['./css/webpackentry/confirmationerror.less'],
		voucher: ['./css/webpackentry/voucher.less'],
		'ie_not_supported': ['./css/webpackentry/ie_not_supported.less'],
		images: glob.sync('./images/**/*', { cwd: 'webapp/static/mobile', nodir: true })
	},

	output: {
		path: path.resolve(__dirname, 'webapp/dist/mobile'),
		publicPath: '/dist/mobile/'
	},

	resolve: {
		extensions: ['', '.js']
	},

	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.NoErrorsPlugin(),
		new CopyWebpackPlugin([{ from: 'docs', to: 'docs' }], { copyUnmodified: true })
	],

	module: {
		loaders: [
			{ test: /\.js$/, exclude: /node_modules/, loaders: ['babel'] },
			{ test: /\.(css|less)$/, loader: ExtractTextPlugin.extract('style', 'css!postcss!less') },
			{ test: /\.(woff(2)?|ttf|eot|svg)(\?[a-z0-9=&.]+)?$/, loaders: ['file?name=fonts/[name].[ext]'] },
			{ test: /.*\.(gif|png|jpe?g|svg)$/i, loaders: ['file?name=[path][name].[ext]'] }
		]
	},

	postcss: [
		require('autoprefixer'),
		// require('stylelint')
	]
};

if (process.env.NODE_ENV === 'production') {
	module.exports = merge.smart(commonConfig, {
		devtool: 'cheap-module-source-map',
		output: {
			filename: 'js/[name]-[chunkhash:8].min.js'
		},
		plugins: [
			new webpack.optimize.DedupePlugin(),
			new webpack.DefinePlugin({
				'process.env': {
					NODE_ENV: '"production"'
				}
			}),
			new webpack.optimize.UglifyJsPlugin({
				compress: {
					warnings: false
				}
			}),
			new webpack.optimize.CommonsChunkPlugin({
				name: 'common',
				filename: 'js/common-[chunkhash:8].min.js',
				minChunks: 2,
				// chunks: Object.keys(commonConfig.entry).filter((key) => key !== 'vendor')
			}),
			// new webpack.optimize.CommonsChunkPlugin({
			// 	name: 'vendor',
			// 	filename: 'js/vendor-[hash:8].min.js'
			// }),
			new ExtractTextPlugin('css/[name]-[contenthash:8].min.css'),
			new ManifestPlugin({
				fileName: 'mapper.json'
			})
		]
	});
} else {
	module.exports = merge.smart(commonConfig, {
		devtool: 'inline-source-map',
		output: {
			filename: 'js/[name].js'
		},
		plugins: [
			new webpack.optimize.DedupePlugin(),
			new webpack.optimize.CommonsChunkPlugin({
				name: 'common',
				filename: 'js/common.js',
				minChunks: 2
			}),

			new ExtractTextPlugin('css/[name].css'),
		]

	});
}
