from base import log_args
from dbcommon.models.location import State


class StateWebRepository(object):

    @log_args()
    def get_all_states(self):
        return State.objects.all()

    @log_args()
    def get_single_state(self, *args, **kwargs):
        return State.objects.get(**kwargs)

    @log_args()
    def filter_states(self, *args, **kwargs):
        return State.objects.filter(**kwargs)
