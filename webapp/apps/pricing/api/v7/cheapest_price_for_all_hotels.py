import logging
from http import HTTPStatus
from django.conf import settings
from django.core.validators import RegexValidator
from django.http import JsonResponse
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.pricing.exceptions import CheckinDateError
from base.views import validators
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from web_sku.availability.services.availability_service import AvailabilityService
from web_sku.pricing.services.pricing_service import PricingService
from web_sku.catalog.services.sku_service import SkuService
from web_sku.catalog.services.sku_request_service import SkuRequestService
from web_sku.catalog.services.cs_type_mappers import CsTypeMappers
from apps.pricing.response_wrappers.search_api_wrapper import SearchApiWrapper
from web_sku.common.dateutils import get_iso_date


logger = logging.getLogger(__name__)


def validate_checkin_checkout(checkin, checkout):
    if checkin >= checkout:
         raise CheckinDateError("Checkin Date is Greater than or qual to Checkout date", HTTP_400_BAD_REQUEST)


class PricingSerializer(serializers.Serializer):
    hotel_id = serializers.CharField(
        max_length=1000,
        validators=[
            RegexValidator(
                regex='^(?:\d+,?)+$',
                message='Invalid value for hotel_id parameter. '
                'Please provide a comma-separated list of hotel_ids',
                code='invalid_hotel_ids')])
    checkin = serializers.DateField(validators = [
        validators.validate_date_of_request])
    checkout = serializers.DateField()
    roomconfig = serializers.CharField(max_length=100, validators=[validators.validate_roomconfig])
    apply_wallet = serializers.BooleanField(default=settings.APPLY_WALLET, required=False)
    utm_source = serializers.CharField(max_length=100, default=None, allow_null=True, allow_blank=True)
    channel = serializers.CharField(max_length=100, default=None, allow_null=True, allow_blank=True)

    def validate(self, data):
        if data['checkout'] <= data['checkin']:
            raise serializers.ValidationError("Checkout date must be after checkin date.")
        return data


class CheapestPriceAPIView(TreeboAPIView):
    availability_service = AvailabilityService()
    pricing_service = PricingService()
    sku_service = SkuService()
    sku_request_service = SkuRequestService()
    validationSerializer = PricingSerializer
    hotel_repository = RepositoriesFactory.get_hotel_repository()
    cs_type_mappers = CsTypeMappers()

    def get(self, request, *args, **kwargs):

        try:
            validated_data = self.serializerObject.validated_data
            validate_checkin_checkout(validated_data['checkin'], validated_data['checkout'])
            user = request.user
            hotel_ids = validated_data['hotel_id'].split(",")
            cs_web_id_map = {}
            for hotel in self.hotel_repository.get_hotels_for_id_list(hotel_ids):
                if hotel.cs_id is not None:
                    cs_web_id_map[hotel.cs_id.strip()] = hotel.id

            cs_hotel_ids = []
            if cs_web_id_map:
                cs_hotel_ids = list(cs_web_id_map.keys())
            apply_wallet = bool(validated_data['apply_wallet'])

            if not len(cs_hotel_ids):
                raise Exception('No hotels found')

        except Exception as e:
            return JsonResponse(
                dict(
                    error=dict(
                        code=HTTP_400_BAD_REQUEST,
                        msg=str(e))),
                status=HTTP_400_BAD_REQUEST)

        try:
            request_skus = validated_data['skus'] if hasattr(validated_data, 'skus') else \
                self.sku_request_service.get_web_request_skus(validated_data['roomconfig'], cs_hotel_ids)
            availability_request_data = self.get_availability_request_data(validated_data, cs_hotel_ids,
                                                                           request_skus)
            sku_availability = self.availability_service.get_sku_availability(availability_request_data)
            room_wise_availability = self.availability_service.\
                get_room_wise_availability_from_sku_availability(availability_request_data, sku_availability)

            pricing_request_data = self.get_pricing_request_data(validated_data, request_skus)
            room_wise_price = self.pricing_service.get_room_wise_price_from_availability(pricing_request_data,
                                                                                         sku_availability)
            web_transformed_price = self.pricing_service.get_web_transformed_price(cs_web_id_map, room_wise_price)
            web_transformed_availability = self.availability_service.\
                get_web_transformed_availability(cs_web_id_map, room_wise_availability)
            search_api_wrapper = SearchApiWrapper(hotel_ids, cs_web_id_map, web_transformed_price, web_transformed_availability,
                                                  user, apply_wallet, validated_data['checkin'],
                                                  validated_data['checkout'], request_skus,
                                                  validated_data['channel'])
            pricing_response = search_api_wrapper.get_pricing_response()
            return Response(pricing_response)
        except Exception as e:
            logger.exception("Error fetching price for Cheapest Pricing v7 for request %s", str(request.GET))
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param=request.GET,
                                                        dev_msg="Cheapest Pricing v7", message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            return JsonResponse(
                {
                    "error": {
                        "code": HTTP_500_INTERNAL_SERVER_ERROR,
                        "msg": e.message if hasattr(e, 'message') else ''
                    }
                },
                status=HTTP_500_INTERNAL_SERVER_ERROR
            )

    def get_pricing_request_data(self, page_data, pricing_request_skus):
        pricing_request_data = {
            'skus': pricing_request_skus,
            'from_date': get_iso_date(page_data['checkin']),
            'to_date': get_iso_date(page_data['checkout']),
            'mode': 'BROWSE',
            'policy': ['rp'],
            'apply_coupon': True,
            'coupon_code': '',
            'channel': 'direct',
            'subchannel': self.cs_type_mappers.get_cs_sub_channel(page_data.get('channel'), page_data.get('utm_source')),
            'application': self.cs_type_mappers.get_cs_application(page_data.get('channel')),
            'utm': {'source': self.cs_type_mappers.get_cs_utm_source(page_data.get('utm_source'))},
            'customer_info': {},
            'pricing_type': 'cheapest'
        }

        return pricing_request_data

    def get_availability_request_data(self, page_data, cs_hotel_ids, availability_request_skus):
        availability_request_data = {
            'skus': availability_request_skus,
            'checkin': page_data['checkin'],
            'checkout': page_data['checkout'],
            'hotels': cs_hotel_ids,
            'occupancy_config': page_data['roomconfig'],
            'channel': 'direct',
            'subchannel': self.cs_type_mappers.get_cs_sub_channel(page_data.get('channel'), page_data.get('utm_source')),
        }

        return availability_request_data
