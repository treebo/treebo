import logging

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from dbcommon.models.profile import UserReferral

from apps.referral import tasks
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class RegisterSignUp(TreeboAPIView):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(RegisterSignUp, self).dispatch(*args, **kwargs)

    def post(self, request, format=None):
        user_id = request.data.get("user_id")
        return Response(RegisterSignUp.referral_conversion_event(user_id))

    @staticmethod
    def referral_conversion_event(user_id):
        logger.info('Conversion event for User id %s', user_id)
        user_referral = UserReferral.objects.select_related(
            'user').get(user_id=user_id)
        tasks.send_sign_up_event.delay(user_referral.user.id)
        return {
            "status": 200,
            "data": ""
        }
