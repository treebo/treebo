import logging
from decimal import Decimal

from django.db.models import Q
from django.utils.encoding import smart_str

from apps.reviews.models import TAHotelIDMapping, TAHotelSubratingMapping
from apps.reviews.services.trip_advisor.subratings import SubratingService

logger = logging.getLogger(__name__)


class HotelSubRatingMappingService(object):
    def hotel_subrating_mapping(self, location_serializer, ta_loc_id):
        try:
            ta_hotel_map = TAHotelIDMapping.objects.filter(
                ta_location_id=ta_loc_id).first()
            if ta_hotel_map:
                logger.info(
                    "Mapping between hotel %s and ta %s exists in HotelSubRatingMappingService",
                    ta_hotel_map.hotel.id,
                    ta_hotel_map.ta_location_id)
                location = location_serializer.data
                self.create_or_update_subratings_mappings(
                    ta_hotel_map, location)
            else:
                logger.info(
                    "No mapping for the given tid exists in subrating %s",
                    ta_loc_id)

        except Exception:
            logger.exception(
                "Exception in creating new entry in sub rating table for %s",
                ta_loc_id)

    def create_or_update_subratings_mappings(self, ta_hotel_map, location):
        try:
            sub_rating = SubratingService()
            for subrating in location["subratings"]:
                subrating_name = smart_str(subrating["name"])
                subrating_obj = sub_rating.fetch_subrating(subrating_name)
                if not subrating_obj:
                    sub_rating.create_or_update_subrating(
                        subrating_name=subrating_name,
                        subrating_localized_name=smart_str(
                            subrating["localized_name"]))
                    subrating_obj = sub_rating.fetch_subrating(subrating_name)
                try:
                    hotel_subrating_map, create = TAHotelSubratingMapping.objects.get_or_create(
                        subrating=subrating_obj, hotel_ta_mapping=ta_hotel_map)
                    if create:
                        logger.info(
                            "create %s in Subrating Mapping data .. create/update for hotel %s and ta_loc_id %s",
                            create,
                            ta_hotel_map.hotel.id,
                            ta_hotel_map.ta_location_id)
                except TAHotelSubratingMapping.MultipleObjectsReturned:
                    logger.info(
                        "MultipleObjectsReturned Exception in updating subrating mapping for hotel id %s and ta_loc_id %s",
                        ta_hotel_map.hotel.id,
                        ta_hotel_map.ta_location_id)
                    hotel_subrating_map = TAHotelSubratingMapping.objects.filter(
                        subrating=subrating_obj, hotel_ta_mapping=ta_hotel_map).first()
                if smart_str(subrating["value"]):
                    hotel_subrating_map.subrating_value = Decimal(
                        subrating["value"])
                hotel_subrating_map.url_subrating = smart_str(
                    subrating["rating_image_url"])
                hotel_subrating_map.save()

        except Exception:
            logger.exception(
                "Exception in saving sub ratings object for %s",
                smart_str(
                    location['location_id']))

    def fetch_latest_sub_ratings_mappings(self, ta_loc_id=None, hotel_id=None):
        try:
            if ta_loc_id or hotel_id:
                latest_subratings = TAHotelSubratingMapping.objects.filter(
                    Q(hotel_ta_mapping__ta_location_id=ta_loc_id) | Q(hotel_ta_mapping__hotel__id=hotel_id))
                if latest_subratings:
                    return latest_subratings
                else:
                    logger.error(
                        "No sub ratings exist for ta_loc_id %s and hotel_id %s",
                        ta_loc_id,
                        hotel_id)
                    return None
            else:
                logger.debug(
                    "both ta_loc_id and hotel_id are None in fetching subratings")
        except Exception:
            logger.exception(
                "Exception in fetching the  sub ratings  for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)
            return None

    def delete_sub_ratings_mappings(self, ta_loc_id=None, hotel_id=None):
        try:
            latest_subrating = self.fetch_latest_sub_ratings_mappings(
                ta_loc_id=ta_loc_id, hotel_id=hotel_id)
            for subrating in latest_subrating:
                subrating.delete()
                logger.info(
                    "Deleted subrating successfully for ta_loc_id %s and hotel-id %s",
                    ta_loc_id,
                    hotel_id)
            else:
                logger.error(
                    "No sub ratings exist for ta_loc_id %s and hotel_id %s",
                    ta_loc_id,
                    hotel_id)
        except Exception:
            logger.exception(
                "Exception in deleting sub ratings for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)

    def fetch_top_enabled_subratings(
            self,
            hotel_id=None,
            ta_loc_id=None,
            count_to_fetch=6):
        try:
            hotel_subratings = TAHotelSubratingMapping.objects.filter(
                hotel_ta_mapping__ta_location_id=ta_loc_id,
                hotel_ta_mapping__hotel__id=hotel_id,
                subrating__is_enabled=True).order_by('-subrating_value')[
                :count_to_fetch]

            return hotel_subratings
        except Exception:
            logger.exception(
                "Exception in fetching the top %s subratings for hotel_id %s ta_loc_id %s",
                count_to_fetch,
                hotel_id,
                ta_loc_id)
            return None
