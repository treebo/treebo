import logging

from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.profiles import utils as profile_utils
from apps.profiles.service.user_registration import UserRegistration
from common.exceptions.treebo_exception import TreeboException
from dbcommon.models.profile import User

logger = logging.getLogger(__name__)


class UserService:

    @staticmethod
    def register_guest_user(phone_number, first_name, last_name, email):
        user_dto = {
            'first_name': first_name,
            'last_name': last_name,
            'phone_number': phone_number,
            'email': email,
            'password': profile_utils.generateRandomPassword()
        }

        guest_user = UserService.register_internally(user_dto)

        logger.info("Registered guest user with id: %s", guest_user.id)
        return guest_user

    @staticmethod
    def set_otp_verified_for_user(phone_number, first_name, last_name, email):
        guest_user = User.objects.get_existing_user_by_phone_number(phone_number)
        if not guest_user:
            guest_user = UserService.register_guest_user(phone_number, first_name, last_name, email)

        guest_user.is_otp_verified = True
        guest_user.save()

    @staticmethod
    def register_internally(user_dto):
        auth_client = TreeboAuthClient()
        user_registration = UserRegistration()

        token_info = {}
        try:
            token_info = auth_client.register(user_dto)
        except TreeboException as e:
            logger.error(" register error " + str(e))

        user = user_registration.register(
            token_info.get('auth_id', None),
            user_dto.get('email'),
            user_dto.get('phone_number'),
            user_dto.get('first_name'),
            user_dto.get('last_name'))
        return user
