from django.conf.urls import url

from apps.search.api.v2.search_api import SearchAPI

urlpatterns = [
    url(r'', SearchAPI.as_view(), name='search_api'),
]
