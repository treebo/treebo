from logging_config.log_configuration import LogConfiguration


class LogConfig(LogConfiguration):

    def get_loggers(self):
        self.loggers.update({
            'queue_listeners': {
                "handlers": ['debug_handler', 'info_handler', 'warn_handler', 'error_handler'],
                "level": "INFO",
                "propagate": True
            }
        })
        return self.loggers

    def __init__(self, log_root):
        super(LogConfig, self).__init__(log_root)
