# Task logger logs the task id and task name for each task
import shutil

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from celery.app import shared_task
from celery.signals import task_prerun
from celery.utils.log import get_task_logger
from django.conf import settings

from apps.third_party.service.dynamic_ads_cache_set import DynamicAdsCacheSetService
from apps.third_party.service.dynamic_ads_factory import DynamicAdsFactory
from apps.third_party.service.dynamic_ads_feeds_service import DynamicAdsService
from apps.third_party.service.third_party_hotel_service import ThirdPartyHotelService
from apps.third_party.service.trivago_upload_service import TrivagoAutomaticHotelUploadService
from base.filters import CeleryRequestIDFilter
from base.logging_decorator import log_args
from webapp.apps.third_party.utm_source_mapping import UTM

logger = get_task_logger(__name__)


# This is executed before every task. Here we can set the request_id and
# user_id in LogFilter
@task_prerun.connect()
def set_request_id(
    signal = None,
    sender = None,
    task_id = None,
    task = None,
    args = None,
    **kwargs):
    logger.addFilter(
        CeleryRequestIDFilter(
            kwargs.get('user_id'),
            kwargs.get('request_id'),
            task_id,
            task.name))
    logger.info(
        "set_request_id called with kwargs: %s, and task_id: %s",
        kwargs,
        task_id)


# log_args decorator should be added below the shared_task decorator.
# Ordering is important here.
@shared_task
@log_args(logger)
def add(x, y, user_id = None, request_id = None):
    return x + y


@shared_task
@log_args(logger)
def sleeptask(i, user_id = None, request_id = None):
    from time import sleep
    sleep(i)
    return i


@shared_task
@log_args(logger)
def trivago_fetch_hotel_data_and_email(url_prefix, to_email, source):
    logger.info("Starting the hotel data csv generation process for source:{}".format(source))
    ThirdPartyHotelService.email_hotel_data_in_csv(url_prefix, to_email, source)


@shared_task(name = settings.CELERY_TRIVAGO_HOTEL_DETAILS)
def trivago_save_hotel_data_in_csv(url_prefix, source = 'trivago'):
    try:
        logger.info("starting the async process of generating the csv and save hotel data")
        csv_data = ThirdPartyHotelService.fetch_hotel_data_in_csv(url_prefix, source = source)
        logger.info("saving file to %s ", settings.TRIVAGO_CSV_LINK)
        with open(settings.TRIVAGO_CSV_LINK, 'w') as fd:
            csv_data.seek(0)
            shutil.copyfileobj(csv_data, fd)

        conn_s3 = S3Connection(host = 's3-ap-southeast-1.amazonaws.com')
        bucket = conn_s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
        bucket_key = Key(bucket)
        path = UTM[source]["csv_path_name"]
        bucket_key.key = settings.S3_STATIC_EXTERNAL_FILE_PATH + path
        bucket_key.set_contents_from_filename(settings.TRIVAGO_CSV_LINK, replace = True, policy = 'public-read')

    except Exception as e:
        logger.exception("trivago_save_hotel_data_in_csv exception ", e)


@shared_task
@log_args(logger)
def trivago_upload_hotel_data_automatic(url_prefix):
    try:
        TrivagoAutomaticHotelUploadService.automatic_hotel_detail_upload_to_trivago(
            url_prefix)
        TrivagoAutomaticHotelUploadService.send_status_mail_on_inventory_upload(
            status = 'SUCCESS')
    except BaseException:
        logger.exception('Automatic hotel upload trigger failed')
        TrivagoAutomaticHotelUploadService.send_status_mail_on_inventory_upload(
            status = 'FAILED')


@shared_task(name = settings.CELERY_DYNAMIC_ADS_TASK_NAME)
@log_args(logger)
def set_cache_for_feed_component_or_build_csv(type,value):
    """
    Creation of CSV
    :param query_set:
    :param proxy_obj:
    :return:
    """
    logger.info("Inside Dynamic Ads task")
    if type=='cache':
        logger.info("Recieved api get request for %s",value)
        if value == "price":
            DynamicAdsCacheSetService().set_pricing_values()
        else:
            DynamicAdsCacheSetService().set_ta_rating()
    else:
        proxy_object = DynamicAdsFactory.get_proxy(value["feed_type"])
        DynamicAdsService.perform_build_csv_operations(proxy_object)
