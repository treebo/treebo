import logging
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.serializer.user_serializer import UserSerializer
from apps.auth.services.password_service import PasswordService
from apps.common.api_response import APIResponse as api_response
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes

logger = logging.getLogger(__name__)


class EmailVerificationApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    password_service = PasswordService()

    def post(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def get(self, request, email_hash):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            user, token = self.password_service.verify_email(email_hash)
            user_dto = UserSerializer(instance=user)
            user_data = user_dto.data
            user_data['token'] = token
            response = api_response.prep_success_response(user_data)
        except TreeboException as e:
            logger.exception("treebo email verify exception %s ", email_hash)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception(
                "internal error email verify  exception %s ",
                email_hash)
            response = api_response.internal_error_response(error_codes.get(
                error_codes.EMAIL_NOT_VERIFIED)['msg'], EmailVerificationApi.__name__)
        return response
