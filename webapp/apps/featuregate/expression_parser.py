from common.constants import common_constants

__author__ = 'amithsoman'

"""
Here are the symbolic tokens that we'll process:
Please add additional tokens as and when required along with
appropriate behavior in the parse_expression/parse_term functions.
"""
AND = 'AND'
OR = 'OR'
LPAREN = '('
RPAREN = ')'
EOF = 'EOF'


def main():
    """Some test code."""


# test of parsing: "5*4+300/(5-2)*(6+4)+4"
# print ExpressionParser([
# 	1, AND, 2, AND, 3, AND,
# 	LPAREN, 4, OR, 5, RPAREN,
# 	AND, LPAREN, 6, OR, 7, RPAREN,
# 	AND, 8, EOF]).parse_expression()
# print ExpressionParser([
# 	1, AND, 2, AND, 3, AND, 4, EOF
# ]).parse_expression()
# print ExpressionParser([
# 	LPAREN, 1, AND, 2, RPAREN, AND, LPAREN, 3, AND,
# 	4, RPAREN, EOF]).parse_expression()

class ExpressionParser:
    """Parses a token list and returns a nested expression structure in
    prefix form."""

    def __init__(self, tokens, constraintValidator=None):
        self.__tokens = tokens
        self.__constraintValidator = constraintValidator

    # self.__constraintValidator = ConstraintValidator('TREEBO20','2015-10-16','2015-10-15','2000',7,None,datetime.now().date())

    def peek_token(self):
        """Looks at the next token in our token stream."""
        return self.__tokens[0]

    def consume_token(self):
        """Pops off the next token in our token stream."""
        next_token = self.__tokens[0]
        del self.__tokens[0]
        return next_token

    def parse_expression(self):
        """Returns a parsed expression.  An expression may consist of a
        bunch of chained factors."""
        expression = self.parse_term()
        while True:
            if self.peek_token() == 'AND':
                if isinstance(expression, int):
                    self.__constraintValidator.validatorFactory(expression)
                    if self.__constraintValidator.message != common_constants.SUCCESS:
                        # raise TreeboValidationException(self.__constraintValidator.message)
                        break
                operation = self.consume_token()
                factor = self.parse_term()
                if not isinstance(factor, list):
                    self.__constraintValidator.validatorFactory(factor)
                    if self.__constraintValidator.message != common_constants.SUCCESS:
                        # raise TreeboValidationException(self.__constraintValidator.message)
                        break
                expression = [operation, expression, factor]
            elif self.peek_token() == 'OR':
                operation = self.consume_token()
                factor = self.parse_term()
                expression = [operation, expression, factor]
            else:
                if isinstance(expression, int):
                    self.__constraintValidator.validatorFactory(expression)
                    if self.__constraintValidator.message != common_constants.SUCCESS:
                        return self.__constraintValidator.message
                break
        return expression

    def parse_term(self):
        """Returns a parsed term.  A term may consist of a number, or a
        parenthesized expression."""
        if self.peek_token() != LPAREN:
            return int(self.consume_token())
        else:
            self.consume_token()  # eat up the lparen
            expression = self.parse_expression()
            # validate coupon constraint id
            self.__constraintValidator.validatorFactory(expression)
            # if self.__constraintValidator.message != common_constants.SUCCESS:
            # raise TreeboValidationException(self.__constraintValidator.message)

            self.consume_token()  # eat up the rparen
            return expression
