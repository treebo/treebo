import logging

from ab_client import utils
from rest_framework.exceptions import MethodNotAllowed
from apps.ab.services.ab_client_services import ABClientServices
from base.decorator.ab_request import ab_request
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from apps.common.api_response import APIResponse as api_response
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class AssignAllApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    ab_services = ABClientServices()

    def post(self, request, format=None):
        raise MethodNotAllowed('post')

    @ab_request()
    def get(self, request, format=None):
        """
                :param request:
                :param format:
                :return:
                """
        try:
            data = utils.get_thread_variable('bucket_assignments')
            buckets = {}
            payloads = {}
            ab_id = ''
            if data:
                buckets = {
                    experiment: assignment['bucket'] for experiment,
                    assignment in list(
                        data.items()) if 'bucket' in assignment}
                ab_id = request.ab_id
                payloads = {experiment:assignment['payload'] for experiment, assignment in list(data.items()) if 'payload' in assignment}
            response_data = {}
            response_data['abExperiments'] = buckets
            response_data['abUserId'] = ab_id
            response_data['payloads'] = payloads
            response = api_response.prep_success_response(response_data)

        except TreeboException as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        message=e.__str__(), dev_msg="TreeboException",
                                                        class_name=self.__class__.__name__)
            logger.exception("trebo error assign all api %s ", request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception("internal error assign api  %s", request.data)
            response = api_response.internal_error_response(error_codes.get(
                error_codes.ASSIGNMENT_ERROR)['msg'], AssignAllApi.__name__)
        return response
