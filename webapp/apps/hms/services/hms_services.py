import ast
import logging


from apps.content.models import ContentStore
from apps.growth.services.growth_services import GrowthServices
from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from apps.profiles.service.profile_service import UserProfileService
from apps.hms import constants

from dbcommon.models.profile import UserReferral
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

logger = logging.getLogger(__name__)


class HmsServices():
    def handle_user_and_send_checkout_email(self, reservations):
        """
        Register user if user doesn't exist then send checkout email to the guest
        :param reservations:
        :return:
        """
        logger.info('handle_new_user_and_send_checkout_email called')
        guest_shareurl = ''
        enabled_source_list = []

        try:
            is_referral_enable_for_all = FeatureToggleAPI.is_enabled(
                "referral", "checkout_email", True)

            if not is_referral_enable_for_all:
                content_object = ContentStore.objects.filter(
                    name="enabled_source_list_for_referral", version=1).first()
                if content_object:
                    content_object_value = content_object.value
                    enabled_source_list = ast.literal_eval(
                        content_object_value)

            for reservation in reservations:
                if reservation["RESSTATUS"] == "CHECKOUT" and (is_referral_enable_for_all or reservation["SOURCE"].lower(
                ) in enabled_source_list or reservation["GSTAYSOURCE"].lower() in enabled_source_list):
                    logger.debug(
                        "Checkout booking event received for %s",
                        reservation)
                    guest = self.fetch_primary_guest(reservation)

                    booking_detail = {}
                    checkout_data = {}
                    booking_detail = self.fetch_booking_detail(reservation)
                    if guest:
                        guest_data = {}
                        guest_data['first_name'] = str(guest['FIRSTNAME'])
                        guest_data['last_name'] = str(guest['LASTNAME'])
                        guest_data['email'] = str(guest['EMAIL'])
                        guest_data['phone'] = ''

                        is_vaild_email = self.validate_email_id(
                            guest_data['email'])
                        if is_vaild_email:
                            try:
                                guest_user = UserProfileService.register_user(
                                    guest_data)
                                logger.debug(
                                    "Registered user email %s and phone %s",
                                    guest_user.email,
                                    guest_user.phone_number)
                                guest_referral_object = UserReferral.objects.filter(
                                    user=guest_user).first()
                                if guest_referral_object:
                                    guest_shareurl = guest_referral_object.shareurl
                                    fb_share_url = settings.FB_SHARE_URL + guest_shareurl
                                    twitter_share_url = settings.TWITTER_SHARE_URL + \
                                        constants.TWITTER_REFERRAL_SHARE_MESSAGE + guest_shareurl
                                    email_share_body = constants.EMAIL_REFERRAL_SHARE_MESSAGE % {
                                        "guest_shareurl": guest_shareurl,
                                        "first_name": guest_data['first_name'],
                                        "nl": "%0D%0A",
                                        "plus": "%2B"}
                                    email_share_url = "mailto:?subject=" + \
                                        guest_data['first_name'] + "'s gift to you" + "&body=" + email_share_body
                                else:
                                    guest_shareurl = ''
                                    fb_share_url = ''
                                    twitter_share_url = ''
                                    email_share_url = ''

                                checkout_data['contact_email'] = guest_data['email']
                                checkout_data['contact_phone'] = guest_data['phone']
                                checkout_data['user'] = 'referral'
                                checkout_data['payload'] = {
                                    'first_name': guest_data['first_name'],
                                    'share_url': guest_shareurl,
                                    'fb_share_url': fb_share_url,
                                    'twitter_share_url': twitter_share_url,
                                    'email_share_url': email_share_url,
                                }

                                checkout_data['payload'].update(booking_detail)
                                logger.debug(
                                    "Checkout email payload %s", checkout_data)
                                growth_service = GrowthServices()
                                growth_service.send_checkout_email(
                                    checkout_data)

                            except Exception as exc:
                                logger.exception(
                                    "Some exception occurred in while registering the guest")

                        else:
                            logger.debug(
                                "User not registered because email id is not valid %s", reservation)

                    else:
                        logger.debug(
                            "No primary guest is found for booking %s",
                            reservation['WRID'])

        except Exception as exc:
            logger.exception(exc)
            logger.debug('Referral task failed in hms sync')

    def fetch_primary_guest(self, reservation):
        """
        fetch primary guest for a booking
        :param reservation:
        :return:
        """
        guest_details = reservation['GUESTDETAIL']

        if len(guest_details) > 0:
            for guest in guest_details:
                if guest['ISPRIMARY']:
                    logger.debug(
                        "primary guest for booking %s is %s",
                        reservation['WRID'],
                        guest)
                    return guest
        return None

    def fetch_booking_detail(self, reservation):
        """
        fetch booking details
        :param reservation:
        :return:
        """
        booking_detail = {}
        booking_detail['wrid'] = reservation['WRID']
        booking_detail['checkin'] = reservation['CHECKIN']
        booking_detail['checkout'] = reservation['CHECKOUT']
        booking_detail['hotel_code'] = reservation['HOTELID']

        if reservation['GID']:
            booking_detail['group_code'] = reservation['GID']
        else:
            booking_detail['group_code'] = reservation['ID']

        return booking_detail

    def validate_email_id(self, email):
        """
        validate email id
        :param email:
        :return:
        """
        logger.info('Checking email validity')
        try:
            validate_email(email)
        except ValidationError as e:
            logger.info('email %s is not valid', email)
            return False
        else:
            logger.info('email %s is valid', email)
            return True
