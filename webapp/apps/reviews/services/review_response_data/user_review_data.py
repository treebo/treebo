import logging
from apps.reviews.services.review_response_data.base_review_data import BaseReviewData
from apps.reviews.services.trip_advisor.hotel_reviews_data import HotelReviewsDataService

logger = logging.getLogger(__name__)


class UserReviewData(BaseReviewData):
    def __init__(self, hotel_id, ta_loc_id):
        super(UserReviewData, self).__init__(hotel_id, ta_loc_id)

    def build(self, ):
        top_reviews = []
        try:
            review_data = HotelReviewsDataService()
            reviews = review_data.fetch_user_reviews(
                hotel_id=self.hotel_id, ta_loc_id=self.ta_location_id)
            if reviews:
                for rev in reviews:
                    if rev.review_is_active:
                        review_dict = self.get_top_review_serializer(rev)
                        top_reviews.append(review_dict)
        except BaseException:
            logger.exception(
                'Exception occured in getting top reviews for hotel_id %s',
                self.hotel_id)
        return top_reviews
