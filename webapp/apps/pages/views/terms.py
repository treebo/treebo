import logging

from base.views.template import TreeboTemplateView
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'devang'

logger = logging.getLogger(__name__)


class Terms(TreeboTemplateView):
    template_name = "terms/index.html"

    # redirect_mobile_to_desktop_view = True

    def getData(self, request, args, kwargs):
        template_name = "terms/index.html"
        seo = ContentServiceClient.getValueForKey(request, 'seo_pages', 1)
        return {
            'scripts': [],
            'terms': {},
            'styles': ['desktop/css/terms.css'],
            'scripts': ['desktop/js/terms.js'],
            'seo': seo['terms'],
            'footerCopyrightDisabled': True}

    def getMobileData(self, request, args, kwargs):
        template_name = "terms/index.html"
        return {
            'styles': ['mobile/css/terms.css'],
            'scripts': ['mobile/js/terms.js']}
