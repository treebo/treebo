import logging

from apps.auth.api.mixins import DDosMixin
from apps.auth.constants import APP_USER_AGENT
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.otp_login_dto import OtpLoginDTO
from apps.auth.services.otp_service import OTPService
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import AuthException, OTPSendException
from base import log_args
from apps.common import error_codes
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class OTPLoginApi(TreeboAPIView, DDosMixin):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    otp_service = OTPService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            if self.is_otp_ddos(request):
                return api_response.prep_success_response({
                    'message': 'OTP sent successfully.',
                    'otp_request_attempts': 1,
                    'max_otp_request_attempts': 6
                })

            otp_login_dto = OtpLoginDTO(data=request.data)
            if not otp_login_dto.is_valid():
                logger.error("otp login invalid request %s ", request.data)
                return api_response.invalid_request_error_response(
                    otp_login_dto.errors, OTPLoginApi.__name__)
            send_otp_response = self.otp_service.send_otp(otp_login_dto.data)
            response = api_response.prep_success_response({
                'message': 'OTP sent successfully.',
                'otp_request_attempts': send_otp_response.get('otp_request_attempts'),
                'max_otp_request_attempts': send_otp_response.get('max_otp_request_attempts')
            })
        except OTPSendException as e:
            logger.exception("treebo otp login exception %s ", request.data)
            response = api_response.otp_not_sent_exception_error_response(e)
        except TreeboException as e:
            logger.exception("treebo otp login exception %s ", request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception(
                "internal error otp login exception %s",
                request.data)
            response = api_response.internal_error_response(error_codes.get(
                error_codes.OTP_SENT_FAILED)['msg'], OTPLoginApi.__name__)
        return response
