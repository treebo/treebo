# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.content.api.landing_content import LandingContentAPI
from apps.content.api.meta_content_v2 import MetaContentAPI
from apps.content.api.views import GetContent

urlpatterns = [
    url(r'^$', GetContent.as_view(), name='content'),
    url(r'^meta/$', MetaContentAPI.as_view(), name='meta-content'),
    url(r"^landing-page/$", LandingContentAPI.as_view(), name='landing-content'),
]
