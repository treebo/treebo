# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('discounts', '0006_auto_20160523_0950'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discountcoupon',
            name='series_value',
            field=models.CharField(default='', max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='discountcoupon',
            name='tagline',
            field=models.CharField(default='', max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='discountcoupon',
            name='terms',
            field=models.CharField(default='', max_length=5000, null=True, blank=True),
        ),
    ]
