from apps.seo.models.blog import Blog
from apps.seo.constants import MAX_BLOG_TO_DISPLAY


class BlogService():

    def blog_service(self, city_id):
        blogs = self.fetch_blogs_for_city(city_id)
        blogs = sorted(blogs, key=lambda x: x.priority)
        blog_list = []
        for blog in blogs:
            temp = {}
            temp['name'] = blog.name
            temp['title'] = blog.title
            temp['image_url'] = blog.image_url
            temp['blog_url'] = blog.blog_url
            temp['city'] = blog.city.slug if blog.city.slug else blog.city.name
            temp['priority'] = blog.priority
            blog_list.append(temp)
        return blog_list

    def fetch_blogs_for_city(self, city_id):
        blogs = Blog.objects.filter(city=city_id, is_enabled=True)
        return blogs[:MAX_BLOG_TO_DISPLAY]

