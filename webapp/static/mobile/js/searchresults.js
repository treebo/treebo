'use strict';

import $ from 'jquery';
import {_} from 'underscore';
import BaseView from './treebolibs/baseview';
import searchWidget from './widgets/search';
import queryString from 'query-string';
import React from 'react';
import {render} from 'react-dom';
import { createStore} from 'redux';
import PriceDetail from './widgets/pricedetails';
import history from 'html5-history-api';
import '../css/searchresults.less';

let $loader = $("#commonLoader");

$(window).on('click', e => {
    const target = $(e.target);
    if (target.closest('.results-filter__item--sort').length ==0 && target.closest('#filtersDd').length ==0){
        store.dispatch({type: "CLOSE_DD_SORT"});
    }

});



let lastSort = '';
let SearchResultView = Object.create(BaseView);
let store = {};
let newParams = {};
let methods = {

    initFilters : (e)=>{
        let filterData = JSON.parse($("#hiddenFilterData").val());
        const parsed = queryString.parse(location.search);

        let {budgetfilter, categoryfilter, amenityfilter, localityfilter} = parsed;
        let localityArr = [];
        if(localityfilter){
            localityArr = localityfilter.split(",");

        }

        let locality = filterData.filters.locality.map((v, i) => ({
            key : v,
            value : v,
            selected:localityArr.indexOf(v) >= 0
        }));

        if(locality.length >3 ){
            filterData.localityFilterViewMore = true;
            filterData.localityFilterViewMoreStatus = false;
        }


        let categoryArr =[];

        if(categoryfilter){
            categoryArr = categoryfilter.split(",");
        }

        let category = filterData.filters.category.map((v, i) => {
            let isLocalitySelected= false;

            return {
                key : v,
                value : v,
                selected: categoryArr.indexOf(v) >= 0
            }
        });

        if(category.length >3 ){
            filterData.categoryFilterViewMore = true;
            filterData.categoryFilterViewMoreStatus = false;
        }

        let amenityArr = [];
        if(amenityfilter){
            amenityArr = amenityfilter.split(",");
        }

        let amenity = filterData.filters.amenity.map((v, i) => ({
            key : v,
            value : v,
            selected: amenityArr.indexOf(v) >= 0
        }));

        if(amenity.length >3 ){
            filterData.amenityFilterViewMore = true;
            filterData.amenityFilterViewMoreStatus = false;
        }

        let budget = filterData.filters.budget;

        if(budgetfilter){
            let budgetfilterArr = budgetfilter.split(",");
            budget = filterData.filters.budget.map((v, i) => {
                if(budgetfilterArr.indexOf(v.key.join("-"))>= 0){
                    v.selected = true;
                }
                return v;
            })

        }

        filterData = Object.assign({}, filterData,{
                filters : Object.assign({}, filterData.filters, {
                    locality
                },{budget}, {category},{amenity})
            }
        )


        let initialState = filterData || {
            sortddVisibility: false,
            filterddVisibility: false,
            sortArr : [],
            sortOptions : {

            },
            filterArr : [],
            filters: {
            },
            showOnlyAvailable : false,
            showOnlyAvailableFilter : true //$(".hotelAvailibility-False").length > 0
        };



        const filtersReducer = (state = initialState, action) => {
            let rooms;
            let udaptedRoom;
            let index;
            switch (action.type) {
                case 'TOGGLE_DD_SRT':
                    return Object.assign({}, state, {
                        sortddVisibility : !state.sortddVisibility
                    });
                case 'CLOSE_DD_SORT':
                    return Object.assign({}, state, {
                        sortddVisibility : false
                    });
                case 'TOGGLE_SHOWAVAILABLE':
                    return Object.assign({}, state, {
                        showOnlyAvailable : !state.showOnlyAvailable
                    });
                case 'CLEAR_FILTERS':
                    let updatedObj = _.mapObject(state.filters, (value,key)=>{
                        return value.map((v,i)=>{
                            return Object.assign({}, v, {
                                selected : false
                            });
                        })
                    });
                    return Object.assign({}, state, {filters: updatedObj, showOnlyAvailable : false});
                case 'VIEW_MORE_FILTER_OPTION':
                    let moreObj = {};
                    moreObj[`${action.option}FilterViewMoreStatus`] = true;
                    return Object.assign({}, state, moreObj);

                case 'TOGGLE_FILTER':

                    let [fltkey, id] = action.id.split("-");
                    let fltOptions = state.filters[fltkey];
                    id = +id;
                    let updateFlt = Object.assign({}, fltOptions[id], {
                        selected: !fltOptions[id].selected
                    });

                    let updateFltObj = [
                        ...fltOptions.slice(0, id),
                        updateFlt,
                        ...fltOptions.slice(id + 1)
                    ];
                    let obj = {};
                    obj[fltkey] = updateFltObj;
                    let updatedFiltersObj = Object.assign({}, state.filters, obj);

                    return Object.assign({}, state, {filters: updatedFiltersObj});

                case 'UPDATE_OPTION':
                    let updatedOption =  {};
                    updatedOption = _.mapObject(state.sortOptions, (v, k) => {
                        const toReturn = v;
                            toReturn.selected = (k==action.id);

                        return toReturn;
                    })
                    return Object.assign({}, state, updatedOption, {sortddVisibility: false});
                default:
                    return state;
            }
        };


        store = createStore(filtersReducer);

        const FiletersApp = React.createClass({
            render() {
                let {state} = this.props;
                return (
                    <div className="results-filter clearfix ">
                        <SortApp options={state.sortArr} ddVisbility={state.sortddVisibility} />
                        <ResultsFilterApp options={state.filterArr} />
                    </div>
                )
            }
        });

        const done = () => {
            $('body').trigger('modal:close');
            _.defer((params) => {
                $("#filteredResultCount").text($('.results__row:visible').length)
                history.replaceState(null, null,
                    `${location.pathname}?${$.param(params)}`);
            }, newParams);
        };

        const ResultsFilterApp = React.createClass({
            render() {
                const {options} = this.props;
                let state = store.getState();
                let filterCount  = methods.getFilterCount();
                let ddClass = 'dd-arrow icon-funnel results-filter__item__icon';

                return (
                    <div className="results-filter__item lfloat results-filter__item--flt pos-rel">
                        <div className="filters__title" data-toggle="modal" data-target="#filtersDd">
                            <i className={ddClass}></i>
                            <span className="filters__title--text">Filters</span>
                        </div>
                        <div id="filtersDd" className="modal modal-container hide pos-abs filters__dd box-shadow pos-abs">
                            <div className="modal__header pos-fix filters__header">Filters
                                <div className="hand icon icon-close pos-abs" onClick={done}>
                                    <i className="icon-cross"></i>
                                </div>
                            </div>
                            <div className="modal__body filters__container">
                                <ul className="filters__item__options" id="showOnlyAvailable">
                                    <li className="checkbox flt-item">
                                        <label className="checkbox__label"><input className="checkbox__input checkbox__input-field" checked={state.showOnlyAvailable} type="checkbox" name="showOnlyAvailable" onChange= {(e) => {
                                            store.dispatch({type: "TOGGLE_SHOWAVAILABLE"});
                                        }}/>Show Only Available</label>
                                    </li>
                                </ul>

                                {
                                    options.map((option,i) =>{
                                        if(state.filters[option].length){
                                            return (
                                                <FilterTypeApp key={i} keyid={option} option={option}/>
                                            )
                                        }else {return ;}
                                    })
                                }
                            </div>
                            <div className="filters__actions  modal__footer pos-fix clearfix">
                                <div className="anchor filters__actions__clear lfloat" onClick={(e) => {
                                        store.dispatch({type: "CLEAR_FILTERS"});
                                    }}>Clear All Filters</div>
                                <button type="button" className="btn filters__actions__apply rfloat" onClick={done} name="button">Done</button>
                            </div>
                        </div>
                    </div>
                )
            }
        });

        const FilterTypeApp = React.createClass({
            render() {
                const {option, keyid} = this.props;
                let state = store.getState();
                let filterOptions = state.filters[option];

                if(!state[`${option}FilterViewMoreStatus`]){
                    filterOptions = filterOptions.slice(0,3);
                }
                return (
                    <div className={`filters__item filters__item--${option}`}>
                        <div className="filters__item__title ">{option} { (()=>{
                            if(option == "budget"){
                                return  <span className="filter__inr"> (in <i className="icon-rupee"></i>.)</span>;
                            }
                        })()}
                        </div>
                        <ul className="filters__item__options">
                            {
                                filterOptions.map((fltOP,i) =>{
                                    return (
                                        <FilterOptionApp key={i} keyid={i} fltType={option} option={fltOP}/>
                                    )
                                })
                            }
                            { (()=>{
                                if(state[`${option}FilterViewMore`] && !state[`${option}FilterViewMoreStatus`]){
                                    return  <li className="flt-item filters__view-more" onClick={() => {
                                            store.dispatch({type: "VIEW_MORE_FILTER_OPTION", option});
                                        }}>View More</li>;
                                }
                            })()}
                        </ul>


                    </div>
                );
            }
        });

        const FilterOptionApp = React.createClass({
            render() {
                const {option, keyid, fltType} = this.props;
                let state = store.getState();


                return (
                    <li className="checkbox flt-item">
                        <label className="checkbox__label"><input className="checkbox__input checkbox__input-field" checked={option.selected} type="checkbox" name={option.key} value={option.key} onChange= {(e) => {
                            store.dispatch({type: "TOGGLE_FILTER", id: `${fltType}-${keyid}`});
                        }}/>{option.value}</label>
                    </li>
                );
            }
        });



        const SortApp = React.createClass({
            render() {
                const {options, ddVisbility} = this.props;
                let state = store.getState();
                let seletedOption = state.sortArr.filter((v, i) => state.sortOptions[v]['selected']);
                let ddClass= `dd-arrow results-filter__item__icon icon-returnarrows dd-arrow--${state.sortddVisibility ? 'open' : 'close'}`
                let ddSortClass= `results-filter__item results-filter__item--sort  lfloat pos-rel results-filter__item--sort--${state.sortddVisibility ? 'open' : 'close'}`

                return (
                    <div className={ddSortClass}>
                        <div className="sort__title uc" onClick={() => {
                                store.dispatch({type: "TOGGLE_DD_SRT"});
                            }}><i className= {ddClass}></i><span className="sort__text">Sort</span></div>
                        <div className={`sort__dd pos-abs ${ddVisbility ? 'sort__dd--show' : ''}`}>

                            <ul className="sort__options dd">
                                {
                                    options.map((option,i) =>{
                                        return (
                                            <SortOption key={i} keyid={option} option = {option}/>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                )
            }
        });
        const SortOption = React.createClass({
            render() {
                const {option, keyid} = this.props;
                let state = store.getState();
                let sortOp = state.sortOptions[option];
                let sClass  = sortOp.selected ? 'dd__item--selected' : '' ;

                return (
                    <li className={`dd__item ${sClass}`} onClick= { ()=> {
                            store.dispatch({
                                type: "UPDATE_OPTION",
                                id: option
                            })
                        }}>
                        {sortOp.value}
                    </li>
                );
            }
        });

        const renderFilters = () => {
            render(
                <FiletersApp state={store.getState()}/>,document.getElementById('resultFilters')
            )
        };

        const renderDom = (firstRender) => {
            let state = store.getState();
            let sortby = _.findKey(state.sortOptions, (v, i) => v.selected);

            methods.sortBy(sortby);
            methods.applyFilters(state, firstRender);
        };


        store.subscribe(renderFilters);
        store.subscribe(renderDom);
        renderFilters();
        renderDom(true);
    },

    applyFilters(state, firstRender){
        let filters = state.filters;
        const $mbanner = $("#marketingBanner");
        const $noFilterResult = $("#noFilterResult");


        if(methods.getFilterCount() == 0 && !state.showOnlyAvailable){
            $('.results__row').show();
            $noFilterResult.addClass('hide');
            // if($mbanner.length){
            //     $mbanner.show().css("order", initialIndex);
            // }
            return;
        }
        if (firstRender && methods.getFilterCount() != 0) {
            $('.results__row').show();
            $noFilterResult.addClass('hide');
            if($mbanner.length){
                $mbanner.show();
            }
            firstRender = false;
        } else if (firstRender) {
            return;
        }

        let budgetFilter = filters.budget.filter((v, i) => v.selected);

        let budgetFilterStr = budgetFilter.map(v => v.key.join("-")).join(",");

        let localities = filters.locality.filter((v, i) => v.selected);

        let localityFilterStr = localities.map(v => v.key).join(",");

        let categories = filters.category.filter((v, i) => v.selected);

        let categoryFilterStr = categories.map(v => v.key).join(",");

        let amenities = filters.amenity.filter((v, i) => v.selected);

        let amenityFilterStr = amenities.map(v => v.key).join(",");

        newParams = _.extend({}, queryString.parse(location.search), {
            budgetfilter: budgetFilterStr,
            localityfilter: localityFilterStr,
            categoryfilter: categoryFilterStr,
            amenityfilter: amenityFilterStr,
        });

        // history.pushState(null, null, `${location.pathname}?${$.param(newParams)}${location.hash}`);

        $('.results__row').each((i, v) => {
            $(v).hide();
            let thisLocal = $(v).data("locality");
            let hasLocality = _.findIndex(localities, v => thisLocal == v.value) >= 0;
            hasLocality = localities.length > 0 ? hasLocality : true;

            let thisCategory = $(v).data("category").split("||");
            let hascategory = _.findIndex(categories, v => thisCategory.indexOf(v.value) >= 0) >= 0;
            hascategory = categories.length > 0 ? hascategory : true;

            let hasbudget =  budgetFilter.length < 1;
            let price = +$(v).data("price");
            budgetFilter.forEach((val, i) => {
                let [minPrice, maxPrice] = val.key;
                hasbudget = hasbudget || (price >= minPrice && price <= maxPrice);
            });

            let thisAmenity = $(v).find(".js-hidden-amenities").val().split("||");

            let hasamenity = _.findIndex(amenities, v => thisAmenity.indexOf(v.value) >= 0) >= 0;
            hasamenity = amenities.length > 0 ? hasamenity : true;

            if(hasbudget && hasLocality && hascategory && hasamenity){
                if(state.showOnlyAvailable){
                    if($(v).hasClass("hotelAvailibility-True")){
                        $(v).show();
                    }
                } else {
                    $(v).show();
                }
            }

        });
        let visbleRowLength = $('.results__row:visible').length;
        if(visbleRowLength){
            $("#marketingBanner").removeClass('hide');
            $noFilterResult.addClass('hide');
        } else {
            $("#marketingBanner").addClass('hide');
            $noFilterResult.removeClass('hide');
        }
        $("#filteredResultCount").text(visbleRowLength);
        //$("#filteredResultCount").text($('.results__row:visible').length);
    },
    getFilterCount(){
        let state = store.getState();
        let filterCount  = [];
        _.each(state.filters, (val,key)=>{
            let selCount = val.filter((v,i)=>{
                return v.selected;
            }).length;
            filterCount.push(selCount);
        });
        filterCount = filterCount.reduce((p,n)=>{
            return +p + +n;
        },0);
        return filterCount;
    },
    // sortByDistance(lat, lon) {
    //     this.setDistanceOnHotel(lat, lon);
    //     $(".search-results-container").html($('.hotel-result').sort(function(a, b) {
    //         var val = parseFloat($(a).attr('distance')) - parseFloat($(b).attr('distance'));
    //         return -val;
    //     }));
    // },
    sortBy(type) {
        if(lastSort && lastSort === type) {
            return;
        }
        lastSort = type;
        let sortArr = $('.results__row').sort((a, b) => (+ $(a).data(type) >= + $(b).data(type) ? 1 : -1));
        sortArr = _.groupBy( sortArr, item => $(item).attr('soldout') );

        sortArr = {True: sortArr.True, False:sortArr.False};

        sortArr = _.map(sortArr, (val, key) => val);
        sortArr = sortArr.reduce((p,n)=>{
            if(n){
                return [...p,...n]
            }
            return [...p];
        },[])
        // if(sortArr.length === 2){
        //     sortArr = [...sortArr[0], ...sortArr[1]];
        // } else {
        //     sortArr = [...sortArr[0]];
        // }


        $("#searchResults").html(sortArr);
    }

}


SearchResultView.pageLoaded = () => {
    methods.initFilters();

    $(".js-hotel-quickbook, .js-hotel-link").on('click',(e)=>{
        $loader.removeClass('hide');

    })
}


let analyticsConfigfunc = function(){
    return  {
        destination:$('#searchInput').val(),
        checkin:rt.getCheckin(),
        checkout:rt.getCheckout(),
        adults:rt.getAdults(),
        kids:rt.getKids(),
        rooms:rt.getRooms(),
        num_results:$('.analytics-count').attr('count'),
        sort_by:rt.sortBy | {},
        hrental_pagetype: 'searchresults',
        hrental_startdate: rt.getCheckin(),
        hrental_enddate: rt.getCheckout(),
    }
}



let resultConfig = { name:'Search Page', widgets:[searchWidget], analyticsConfigfunc};
let searchResultView = SearchResultView.setup(resultConfig);
