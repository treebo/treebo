from django.conf import settings

from apps.common.utils import round_to_nearest_integer
from apps.discounts import CouponValidationParams
from apps.discounts.services.discount_coupon_services import DiscountCouponServices
from apps.discounts.services.hotel_coupon_auto_apply_service import HotelCouponAutoApplyService
from apps.pricing.api.v4 import RoomPrice
from apps.pricing.services.price_request_dto import PriceRequestDTO_V3
from apps.pricing.services.pricing_service_v3 import PricingServiceV3
from apps.reward.service.reward import RewardService
import logging
import datetime
from apps.common.slack_alert import SlackAlertService as slack_alert
from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)


class HDPageAPITransformer(object):
    reward_service = RewardService()
    hotel_autoapply_service = HotelCouponAutoApplyService()
    discount_coupon_service = DiscountCouponServices()
    pricing_service = PricingServiceV3()

    @staticmethod
    def _default_price_component():
        default_price_component = {
            "available": 0,
            "availability": False,
            "member_discount_applied": False,
            "member_discount_available": False,
            "rate_plans": [],
            "coupon_applied": False,
            "coupon_code": ''

        }
        return default_price_component

    def transform_pricing_for_display(
            self,
            room_wise_prices,
            hotel,
            apply_wallet,
            user,
            checkin_date,
            checkout_date,
            hotel_id,
            utm_source,
            room_config,
            logged_in_user):
        """
        Transforms the prices returned from pricing service to what the display requires
        :param pricing_data: Pricing Service response
        :param hotel_id_list: List of hotel ids
        :param room_config: room config
        :param check_in: check in
        :param check_out: check out
        :return:
        """

        total_prices = {}
        wallet_applied, total_wallet_balance, total_wallet_points, total_wallet_spendable_amount = self.reward_service.get_wallet_balance(
            user)

        room_wise_prices_auto_apply, hotel_coupon_map = self.apply_auto_coupon(
            hotel_id, room_wise_prices, utm_source, checkin_date, checkout_date, room_config, user, logged_in_user)
        aa_coupon_applied = False
        aa_coupon_code = ''
        for room_type, rate_plan_price in list(
                room_wise_prices_auto_apply.items()):
            room_type_code = room_type.lower()
            total_prices[room_type_code] = {}
            rate_plan_prices = []
            member_discount_available = False
            member_discount_applied = False
            for rate_plan, rate_plan_price in list(rate_plan_price.items()):
                room_price = RoomPrice(
                    room_type,
                    rate_plan,
                    rate_plan_price.get('price'),
                    rate_plan_price.get('meta'))
                price = room_price.price
                meta = room_price.meta
                if price.sell_price <= 0.00:
                    total_prices[room_type_code] = self._default_price_component()
                    logger.error(
                        "getting sellprice zero from pricing team %s ", hotel_id)
                    # slack_alert.publish_alert('getting zero price for hotel `%s`' % (hotel_id), channel=slack_alert.WEB_CHANNEL)
                    continue
                total_discount = price.promo_discount + \
                    price.member_discount + price.coupon_discount
                if price.coupon_discount > 0:
                    aa_coupon_applied = True
                aa_coupon_code = hotel_coupon_map[hotel_id] if (aa_coupon_applied and hotel_coupon_map) else ''

                total_discount_percent = round_to_nearest_integer(
                    total_discount / price.base_price * 100)

                member_discount_applied = member_discount_applied or price.member_discount_applied
                member_discount_available = member_discount_available or price.member_discount_available

                wallet_deductable_amount, wallet_deduction = self.reward_service.get_wallet_breakup(
                    apply_wallet, total_wallet_spendable_amount, price.sell_price)

                rate_plan_dict = dict(code=rate_plan)
                if meta:
                    rate_plan_dict.update(meta)
                rate_plan_price = {
                    'rate_plan': rate_plan_dict,
                    'price': {
                        "net_payable_amount": price.sell_price -
                        wallet_deduction,
                        "wallet_applied": apply_wallet and wallet_applied,
                        "wallet_deduction": wallet_deduction,
                        "wallet_type": settings.WALLET_TYPE,
                        'sell_price': price.sell_price,
                        'tax': price.tax,
                        'member_discount': price.member_discount,
                        'pretax_price': price.pretax_price,
                        'rack_rate': price.rack_rate,
                        'total_discount_percent': total_discount_percent,
                        'coupon_applied': aa_coupon_applied,
                        'coupon_code': aa_coupon_code,
                    }}

                rate_plan_prices.append(rate_plan_price)

            total_prices[room_type_code] = {
                "available": 0,
                "availability": False,
                'member_discount_applied': member_discount_applied,
                'member_discount_available': member_discount_available,
                "rate_plans": rate_plan_prices,
                'coupon_applied': aa_coupon_applied,
                'coupon_code': aa_coupon_code,
            }

        if not total_prices:
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            for room in hotel_repository.get_rooms_for_hotel(hotel):
                total_prices[room.room_type_code.lower(
                )] = self._default_price_component()
            return total_prices

        wallet_info = {
            'wallet_type': settings.WALLET_TYPE,
            'wallet_applied': apply_wallet and wallet_applied,
            'wallet_deductable_amount': total_wallet_spendable_amount,
            'total_wallet_balance': total_wallet_balance,
            'total_wallet_points': total_wallet_points,
            'user_id': user.id if user and user.is_authenticated() else '',
        }
        return total_prices, wallet_info

    def apply_auto_coupon(
            self,
            hotel_id,
            room_wise_prices,
            utm_source,
            checkin_date,
            checkout_date,
            room_config,
            user,
            logged_in_user):
        if not utm_source:
            return room_wise_prices, {}
        hotel_coupon_map = self.hotel_autoapply_service.get_hotel_coupons_basis_hotel_ids_and_utm_source(
            [hotel_id], utm_source, checkin_date, checkout_date)
        discount_percentage_map = self.get_discount_percentage_map_for_auto_apply(
            hotel_id,
            room_wise_prices,
            hotel_coupon_map,
            checkin_date,
            checkout_date,
            room_config,
            utm_source,
            user)
        if discount_percentage_map:
            room_config_list = room_config.split(",")
            price_request_dto = PriceRequestDTO_V3(
                checkin_date,
                checkout_date,
                discount_percentage_map,
                room_config_list,
                logged_in_user=logged_in_user,
                utm_source=utm_source)
            hotel_wise_cumulative_prices = self.pricing_service.get_hd_page_price(
                price_request_dto)
            room_wise_prices = hotel_wise_cumulative_prices.get(hotel_id)
        return room_wise_prices, hotel_coupon_map

    def get_discount_percentage_map_for_auto_apply(
            self,
            hotel_id,
            room_wise_prices,
            hotel_coupon_map,
            checkin_date,
            checkout_date,
            room_config,
            utm_source,
            user):
        discount_percentage_map = dict()
        if not hotel_coupon_map:
            return discount_percentage_map
        for room_type, rate_plan_price in list(room_wise_prices.items()):
            for rate_plan, rate_plan_price in list(rate_plan_price.items()):
                room_price = RoomPrice(
                    room_type,
                    rate_plan,
                    rate_plan_price.get('price'),
                    rate_plan_price.get('meta'))

                city_id = None
                price = room_price.price
                pretax_price = price.pretax_price
                member_discount = price.member_discount
                total_amount = price.sell_price
                rack_rate = price.rack_rate
                booking_channel = None
                paytype = None
                aa_coupon_code = hotel_coupon_map[hotel_id]
                param = CouponValidationParams(
                    checkin_date,
                    checkout_date,
                    room_config,
                    aa_coupon_code,
                    hotel_id,
                    city_id,
                    user,
                    user,
                    pretax_price +
                    member_discount,
                    member_discount,
                    total_amount,
                    rack_rate,
                    room_type,
                    booking_channel,
                    paytype,
                    utm_source)
                coupon, discount_amount, discount_error = DiscountCouponServices.validate_and_apply_coupon_code(
                    param)
                discount_percent = DiscountCouponServices.get_discount_percentage(
                    discount_amount, pretax_price, member_discount, total_amount)
                discount_percentage_map[hotel_id,
                                        room_type, rate_plan] = discount_percent
        return discount_percentage_map
