import logging

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR

from apps.content.services.content_store_service import ContentStoreService
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class OTPResendDurationAPI(TreeboAPIView):
    CONTENT_STORE_KEY = 'resend_otp_duration_in_ms'
    DEFAULT_RESEND_OTP_DURATION = 30000

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OTPResendDurationAPI, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        try:
            content_store_object = ContentStoreService.get_content(key=self.CONTENT_STORE_KEY)
            resend_otp_duration = content_store_object.value
            return JsonResponse({'resend_otp_duration': resend_otp_duration}, status=HTTP_200_OK)
        except Exception as e:
            logger.exception('Exception in fetching resend otp duration')
            return JsonResponse({'resend_otp_duration': self.DEFAULT_RESEND_OTP_DURATION},
                                status=HTTP_200_OK)
