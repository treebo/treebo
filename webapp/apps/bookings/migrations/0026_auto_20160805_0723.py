# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0025_auto_20160720_1000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='mm_promo',
            field=models.DecimalField(null=True, max_digits=20, decimal_places=10),
        ),
        migrations.AlterField(
            model_name='booking',
            name='rack_rate',
            field=models.DecimalField(null=True, max_digits=20, decimal_places=10),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='mm_promo',
            field=models.DecimalField(null=True, max_digits=20, decimal_places=10),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='pretax_amount',
            field=models.DecimalField(null=True, max_digits=20, decimal_places=10),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='rack_rate',
            field=models.DecimalField(null=True, max_digits=20, decimal_places=10),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='tax_amount',
            field=models.DecimalField(null=True, max_digits=20, decimal_places=10),
        ),
    ]
