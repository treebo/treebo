import datetime
import logging

from common.job.exceptions import JobNotRegistered
from common.job.job_repository import JobRepository

logger = logging.getLogger(__name__)


class JobWriter(object):
    def __init__(self):
        self.job_repository = JobRepository()
        from common.job.jobs.register_job import job_registry
        self.job_register = job_registry

    def write(self, job_entity):
        if not self.job_register.is_job_registered(job_entity.job_name):
            raise JobNotRegistered(job_entity.job_name)
        job_entity.job_id = self.random_job_id_generator(job_entity.crs_booking_id)
        self.job_repository.save(job_entity)
        logger.info('Created a job_entity with id: %s', job_entity.job_id)
        return job_entity

    @staticmethod
    def random_job_id_generator(crs_booking_id):
        created_at = datetime.datetime.now(datetime.timezone.utc).strftime("%d%m%y%H%M")
        random_id = ['JOB', crs_booking_id, created_at]
        return '-'.join(random_id)
