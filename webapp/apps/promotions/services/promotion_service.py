import sys
from apps.promotions.models import PromotedHotels
from apps.promotions.models import MetaRedirectionHotels
from apps.promotions.hotel_redirection_type import HotelRedirectionType


class PromotionsService:

    def get_promoted_hotel(self, hotels):
        promoted_hotel = None
        all_promoted_hotels = PromotedHotels.objects.all()
        max_hotel_priority = sys.maxsize
        hotel_priority = max_hotel_priority
        promoted_hotel_priority_dict = {}
        for hotel_info in all_promoted_hotels:
            promoted_hotel_priority_dict[hotel_info.hotel_id] = hotel_info.priority

        for hotel in hotels:
            if hotel in promoted_hotel_priority_dict.keys() and promoted_hotel_priority_dict.get(
                    hotel, max_hotel_priority + 1) <= hotel_priority:
                hotel_priority = promoted_hotel_priority_dict[hotel]
                promoted_hotel = hotel

        return promoted_hotel

    def get_hotel_redirection_type(self, hotel_cs_id):
        hotel_redirection_type = HotelRedirectionType.DETAILS.value
        try:
            hotel_redirection_info = MetaRedirectionHotels.objects.get(hotel_id=hotel_cs_id)
        except MetaRedirectionHotels.DoesNotExist:
            hotel_redirection_info = None

        if hotel_redirection_info and hotel_redirection_info.redirection_type == HotelRedirectionType.SEARCH.value:
            hotel_redirection_type = hotel_redirection_info.redirection_type

        return hotel_redirection_type
