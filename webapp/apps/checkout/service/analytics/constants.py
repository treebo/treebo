from ths_common.constants.base_enum import BaseEnum


class SegmentEvent(BaseEnum):
    ADWORDS_COMPLETED_ORDER_PAH = "Adwords Completed Order PAH"
    COMPLETED_ORDER = "Completed Order"
