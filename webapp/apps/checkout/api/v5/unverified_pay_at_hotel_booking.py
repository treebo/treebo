import logging
import re

from django.core.cache import cache
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from apps.bookings.models import UnVerifiedPayAtHotelBooking
from apps.common.api_response import APIResponse
from apps.common.date_time_utils import get_current_ist_date
from apps.common.exceptions.custom_exception import InvalidRequestParamInAPIException
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class IsUnverifiedBookingMade(TreeboAPIView):
    phone_pattern = re.compile('^(6|7|8|9)\d{9}$')

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(IsUnverifiedBookingMade, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        phone_number = request.query_params.get('phone_number')
        if not phone_number:
            logger.error('Bad request in API api/v5/checkout/payathotel/is-unverified-booking-done/.'
                         'Param: phone_number not found in query parameters')

            error_message = 'Phone number is mandatory'
            return APIResponse.error_response_from_error_message(error_message=error_message)
        try:
            match = self.phone_pattern.match(str(phone_number))
            if not match:
                raise InvalidRequestParamInAPIException("Invalid Phone number")

            response_data = {
                "unverified_booking_made": UnverifiedPAHBookingUtil.is_unverified_booking_made(phone_number)
            }
            return APIResponse.prep_success_response(message=response_data)

        except InvalidRequestParamInAPIException as e:
            logger.exception('Exception while fetching if unverified booking was made in last 24 hrs. '
                             'for phone number {0}'.format(phone_number))

            error_message = e.developer_message
            return APIResponse.error_response_from_error_message(error_message=error_message)
        except Exception:
            logger.exception('Exception while fetching if unverified booking was made in last 24 hrs. '
                             'for phone number {0}'.format(phone_number))

            error_message = 'Oops! Something went wrong. Please retry after sometime'
            return APIResponse.error_response_from_error_message(error_message=error_message,
                                                                 status_code=HTTP_500_INTERNAL_SERVER_ERROR)


class UnverifiedPAHBookingUtil:
    CACHE_KEY = 'UNVERIFIED_BOOKING_WITH_PHONE_NUMBER_{0}'
    CACHE_TIME_OUT = 86400  # 24 hrs in seconds

    @classmethod
    def is_unverified_booking_made(cls, phone_number):
        """
        Is unverified pay at hotel made for a phone number
        :param phone_number: phone number
        :return:
        """
        last_unverified_booking_date = cache.get(cls.CACHE_KEY.format(str(phone_number)))
        return True if last_unverified_booking_date else False

    @classmethod
    def set_unverified_booking_made(cls, booking_order_id, booking_owner_phone_number):
        """
        Set unverified pay at hotel made for a phone number
        :param booking_order_id: order_id of Booking
        :param booking_owner_phone_number: Booking owner's phone number
        :return:
        """
        current_date = get_current_ist_date()

        # Setting in cache
        cache.set(
            key=cls.CACHE_KEY.format(str(booking_owner_phone_number)),
            value=current_date,
            timeout=cls.CACHE_TIME_OUT)

        # Saving the entry in DB
        unverified_pay_at_hotel_booking = UnVerifiedPayAtHotelBooking.create(booking_order_id)
        unverified_pay_at_hotel_booking.save()
