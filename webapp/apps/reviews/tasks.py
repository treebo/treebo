from celery import shared_task
import logging
from apps.reviews.cache.cache_service import CacheService
from apps.third_party.service.trip_advisor.fetch_reviews_service import TAReviewAPIService

from webapp.apps.reviews.cache.page_config import PageConfig
import collections
logger = logging.getLogger(__name__)


@shared_task(name='update_cache')
def fetch_and_update_reviews(key):
    external_service = TAReviewAPIService()
    cache_service = CacheService()
    items = key.split('_')
    hotel_id = items[-1]
    page = '_'.join(items[1:-1])
    external_service.fetch_review_and_update_audit(hotel_id)
    cache_service.get_or_update_cache(hotel_id, page)


@shared_task(name='update_cache_from_cron')
def fetch_and_update_reviews_through_cron(hotel_id):
    external_service = TAReviewAPIService()
    cache_service = CacheService()
    external_service.fetch_review_and_update_audit(hotel_id)
    config = PageConfig()
    pages = [attr for attr in dir(config) if not isinstance(
        getattr(config, attr), collections.Callable) and not attr.startswith("__")]
    for page in pages:
        logger.info("Updating cache for hotel : %s, Page : %s", hotel_id, page)
        cache_service.get_or_update_cache(hotel_id, page)
        logger.info(
            "Cache Update Complete for hotel : %s, Page : %s",
            hotel_id,
            page)
