# -*- coding: utf-8 -*-



from django.views.decorators.cache import cache_page
from django.conf.urls import url
from apps.content.api.v3.content import ContentAPI
from apps.content.api.v3.meta_content_v3 import MetaContentAPIV3
from apps.content.constants import META_CACHE_PAGE_TIMEOUT
from webapp.services.common_services.random_number_generator import RandomNumberGenerator



urlpatterns = [
    url(r"^$", ContentAPI.as_view(), name='content-v3'),
    #Change as part of WEB-1727
    url(r'^meta/$', MetaContentAPIV3.as_view(), name='meta-content')
]