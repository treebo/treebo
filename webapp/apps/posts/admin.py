from django.contrib import admin
from apps.posts.models import Question, Answer, QAToggle, QAMapping
from dbcommon.admin import ReadOnlyAdminMixin
from django.conf import settings


@admin.register(Question)
class QuestionAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'submitted_by_user',
        'status',
        'state',
        'user',
        'question_data',
        'page')

    search_fields = (
        'id', 'submitted_by_user', 'status', 'state', 'user', 'question_data'
    )

    list_filter = (
        'submitted_by_user',
        'status',
        'state',
        'user',
        'question_data',
        'id',
        'page')
    ordering = ('-created_at',)


@admin.register(Answer)
class AnswerAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id', 'answer_data', 'state', 'user', 'related_question_id'
    )

    search_fields = (
        'id', 'answer_data', 'state', 'user', 'related_question_id'
    )

    list_filter = ('id', 'answer_data', 'state', 'user', 'related_question_id')
    ordering = ('-created_at',)


@admin.register(QAToggle)
class QAToggleAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id', 'enable_qa', 'enable_for_all_pages'
    )

    search_fields = (
        'id', 'enable_qa', 'enable_for_all_pages'
    )

    list_filter = ('id', 'enable_qa', 'enable_for_all_pages')
    ordering = ('-created_at',)


@admin.register(QAMapping)
class QAMappingAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id', 'question_id', 'page_type', 'status', 'priority', 'page_type_id'
    )

    search_fields = (
        'id', 'question_id', 'page_type', 'status', 'priority', 'page_type_id'
    )

    list_filter = (
        'id',
        'question_id',
        'page_type',
        'status',
        'priority',
        'page_type_id')
    ordering = ('-created_at',)

    class Media:
        js = ['admin/js/qamapping_action_change.js']
