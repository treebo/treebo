import logging
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.forgot_password_dto import ForgotPasswordDTO
from apps.auth.services.password_service import PasswordService
from apps.common.api_response import APIResponse as api_response
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes

logger = logging.getLogger(__name__)


class ForgotPasswordApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    password_service = PasswordService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            forgot_password_dto = ForgotPasswordDTO(data=request.data)
            if not forgot_password_dto.is_valid():
                logger.error(
                    "forgot password invalid request %s ",
                    request.data)
                return api_response.invalid_request_error_response(
                    forgot_password_dto.errors, ForgotPasswordApi.__name__)
            self.password_service.send_forgot_password_email(
                forgot_password_dto.data)
            response = api_response.prep_success_response(
                'Email sent successfully')
        except TreeboException as e:
            logger.exception(
                "treebo forgot password exception %s ",
                request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception(
                "internal error forgot password exception %s ",
                request.data)
            response = api_response.internal_error_response(error_codes.get(
                error_codes.EMAIL_NOT_SENT)['msg'], ForgotPasswordApi.__name__)
        return response
