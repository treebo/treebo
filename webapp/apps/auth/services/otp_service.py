import logging

from apps.auth.dto.otp_login_dto import NOT_AVAILABLE
from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.profiles.service.profile_service import UserProfileService
from apps.profiles.service.user_registration import UserRegistration
from base.token_helper import TokenHelper

logger = logging.getLogger(__name__)


class OTPService(object):
    auth_client = TreeboAuthClient()
    token_helper = TokenHelper()
    user_registration = UserRegistration()
    user_profile_service = UserProfileService()

    def send_otp(self, otp_login_dto):
        phone_number = otp_login_dto['phone_number']
        channel = otp_login_dto.get('channel', NOT_AVAILABLE)
        logger.debug(" otp sending otp to %s ", phone_number)
        return self.auth_client.login_via_otp(phone_number, channel)

    def verify_otp(self, otp_verify_dto):
        phone_number = otp_verify_dto['phone_number']
        verification_code = otp_verify_dto['verification_code']
        channel = otp_verify_dto.get('channel', NOT_AVAILABLE)
        logger.debug(
            " otp verfiy for %s , code %s ",
            phone_number,
            verification_code)
        token_info = self.auth_client.login_via_otp_verify(
            phone_number, verification_code, channel)
        user, user_data = self.user_registration.register_user_with_token(
            token_info['access_token'])
        return token_info, user, user_data

    def verify_otp_and_store_token(self, otp_verify_dto):
        phone_number = otp_verify_dto['phone_number']
        token_info, user, user_data = self.verify_otp(otp_verify_dto)
        logger.debug(
            " otp verfiy success for  %s , token_info %s ",
            phone_number,
            token_info)
        self.token_helper.save_access_token(token_info, user)
        return user, token_info, user_data

    def is_verified(self, otp_is_verify_dto, user):
        phone_number = otp_is_verify_dto['phone_number']
        is_staff = self.user_profile_service.is_staff(user)
        logger.debug(
            " otp is verified %s and user %s  and staff status %s ",
            phone_number,
            user,
            is_staff)
        if not is_staff:
            otp_info = self.auth_client.otp_is_verified(phone_number)
            logger.debug(
                " otp is verified result  %s, otp_info %s ",
                phone_number,
                otp_info)
            is_verified = otp_info['is_verified']
        else:
            logger.debug(
                "staff user %s for phone number  %s",
                user,
                phone_number)
            is_verified = True
        return is_verified
