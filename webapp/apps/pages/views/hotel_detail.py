import copy
import json
import logging
from datetime import datetime, timedelta

import requests
from django.core.urlresolvers import reverse
from django.http import Http404
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect
from django.template.defaultfilters import slugify
from django.utils import dateformat

from apps.checkout.bookinghelper import BookingHelper
from apps.featuregate.manager import FeatureManager
from apps.hotels import utils as hotel_utils
from apps.hotels.service.hotel_service import HotelService, HotelEnquiryValidator
from base import log_args
from base.views.template import TreeboTemplateView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboValidationException
from apps.common import date_time_utils
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from data_services.exceptions import HotelDoesNotExist

logger = logging.getLogger(__name__)


class HotelDetail(TreeboTemplateView):
    template_name = "details/index.html"

    __hotelService = HotelService()
    __pagename = 'hotel_details'
    defaultDetailUrl = ''
    defaultDetailUrlCreated = False

    @log_args(logger)
    def __getHotelData(self, request, args, kwargs):
        city_slug = kwargs.get('city_slug', None)
        hotel_slug = kwargs.get('hotel_slug', None)
        locality_slug = kwargs.get('locality_slug', None)

        hotel_id, checkInDate, checkOutDate, guest, roomConfig, roomConfigString = self.__parseRequest(
            request, args, kwargs)
        if not hotel_id.isdigit():
            redirect_url = reverse(
                'pages:hotel-details-new',
                kwargs={
                    "hotel_id": hotel_utils.decode_hash(hotel_id),
                    'city_slug': city_slug,
                    'hotel_slug': hotel_slug,
                    'locality_slug': locality_slug}) + "?checkin={0}&checkout={1}&roomconfig={2}&roomtype={3}".format(
                request.GET.get('checkin'),
                request.GET.get('checkout'),
                request.GET.get('roomconfig'),
                request.GET.get('roomtype'))
            return HttpResponseRedirect(redirect_url)
        hashed_hotel_id = hotel_utils.generate_hash(int(hotel_id))
        logger.info('Hotel Id: %s', hotel_id)
        roomRequired = len(roomConfig)
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotels = hotel_repository.filter_hotels(pk=hotel_id)
        if not hotels or len(hotels) != 1:
            raise Http404
        hotel = hotels[0]

        if hotel.status == Hotel.DISABLED:
            return redirect(
                reverse(
                    'pages:hotels_in_city',
                    kwargs={
                        'cityString': slugify(
                            hotel.city.name)}))
        city_search_args = []
        city_data_service = RepositoriesFactory.get_city_repository()
        city_search_args.append({'name': 'name', 'value': city_slug.replace(
            '-', ' '), 'case_sensitive': False, 'partial': True})
        current_cities = city_data_service.filter_cities_by_string_matching(
            city_search_args)
        if current_cities:
            city = current_cities[0]
        else:
            city = None
        #city = City.objects.filter(name__icontains=city_slug.replace('-', ' ')).first()
        if not city or hotel.city.id != city.id:
            logger.error(
                "City in hotel: %s not matching city passed: %s",
                hotel.city.id,
                city.id)
            raise Http404

        try:
            HotelEnquiryValidator(
                checkInDate,
                checkOutDate,
                roomConfig).validate()
        except TreeboValidationException as e:
            logger.info("Invalid dates on hotel details page: %s", str(e))
            if e.code == error_codes.INVALID_DATES:
                # NOTE: Should we show error page instead here?
                date_delta = abs((checkOutDate - checkInDate).days)
                return redirect(
                    HotelDetail.build_invalid_request_redirect_url(
                        city_slug,
                        hotel_id,
                        hotel_slug,
                        locality_slug,
                        date_delta,
                        request.GET.get('roomtype'),
                        roomConfigString))
            raise e

        hotel_details_api_url = reverse('hotels:details', kwargs={
            'hashedHotelId': hashed_hotel_id
        }) + '?checkin={0}&checkout={1}&roomconfig={2}'.format(checkInDate.strftime('%Y-%m-%d'),
                                                               checkOutDate.strftime('%Y-%m-%d'),
                                                               roomConfigString)

        # TODO: User HotelRestClient to pass cookie with this request
        response = requests.get(
            request.build_absolute_uri(hotel_details_api_url))
        hotelDetailsDict = response.json()

        directions = self.__populateDirectionCssClasses(hotelDetailsDict)

        roomTypes = hotelDetailsDict['hotel'].get('rooms', [])
        roomTypes = sorted(roomTypes, key=lambda k: k['price_per_night'])

        no_of_nights = (checkOutDate - checkInDate).days
        queryParams = copy.deepcopy(dict(request.GET))
        queryParams.update({
            # Using django.utils.dateformat module, to format date as: 1st
            # May'16 (Adding st, nd is something that python datetime.strftime
            # can't do
            'checkIn': dateformat.format(checkInDate, "jS M'y"),
            'checkOut': dateformat.format(checkOutDate, "jS M'y"),
            'guests': guest,
            'no_of_nights': no_of_nights,
            # TODO : get rooms as well
            'rooms': roomRequired
        })

        if queryParams.get('roomtype', False):
            queryParams['roomtype'] = queryParams['roomtype'][0]

        context = {
            "name": hotelDetailsDict['hotel']['name'],
            "address": hotelDetailsDict['hotel']['complete_address'],
            "rooms_ramaining": None,
            "reviews": None,
            "tags": [],
            "images": hotelDetailsDict['hotel']['image_set'],
            "description": hotelDetailsDict['hotel']['description'],
            "ameneties": hotelDetailsDict['hotel']['facility_set'],
            "accessibilities": directions,
            "rules": hotelDetailsDict['rules'],
            "policies": hotelDetailsDict['policies'],
            "similar_treebos": hotelDetailsDict['similar_treebos'],
            "room_types": roomTypes,
            "deal": None,
            "deal_block": hotelDetailsDict['deal_block'],
            "trilights": hotelDetailsDict['hotel']['hotel_highlights'],
            'queryParams': queryParams
        }

        if bool(hotelDetailsDict['hotel']['seodetails_set']):
            kwargs = {'city_slug': city_slug, 'hotel_slug': hotel_slug,
                      'locality_slug': locality_slug, 'hotel_id': hotel_id}
            context['seo'] = {
                "canonical": request.build_absolute_uri(
                    reverse('pages:hotel-details-new', kwargs=kwargs)),
                "title": hotelDetailsDict['hotel']['name'] + " | " +
                hotelDetailsDict['hotel']['seodetails_set'][0]['title'],
                "description": hotelDetailsDict['hotel']['seodetails_set'][0]['description'],
                "keywords": hotelDetailsDict['hotel']['seodetails_set'][0]['keyword']
            }
        else:
            context['seo'] = {}

        quickbook_base_url = BookingHelper.build_itinerary_page_url_from_params(
            hotel_id, roomConfigString, roomTypes[0]["room_type"], checkInDate, checkOutDate)
        hotelName = hotelDetailsDict['hotel']['name']
        hotelName = str(hotelName.replace("'", ""))
        context['jsvars'] = str(json.dumps({
            'hotelId': hotel.id,
            'rooms': roomTypes,
            'hotelName': hotelName,
            'deal': context['deal'],
            'quickbook_base_url': quickbook_base_url,
            'checkInDate': checkInDate.strftime('%Y-%m-%d'),
            'checkOutDate': checkOutDate.strftime('%Y-%m-%d'),
        }))

        featureManager = FeatureManager(
            self.__pagename,
            checkInDate,
            checkOutDate,
            hotel_id,
            None,
            None,
            None)
        overriddenValues = featureManager.get_overriding_values()
        for overriddenKey in list(overriddenValues.keys()):
            context[overriddenKey] = overriddenValues[overriddenKey]
        return context

    @staticmethod
    def build_invalid_request_redirect_url(
            city_slug,
            hotel_id,
            hotel_slug,
            locality_slug,
            time_delta,
            room_type,
            room_config):
        redirect_kwargs = {
            'city_slug': city_slug,
            'hotel_slug': hotel_slug,
            'locality_slug': locality_slug,
            'hotel_id': hotel_id}
        redirect_check_in_date = datetime.now()
        redirect_check_out_date = redirect_check_in_date + \
            timedelta(days=time_delta)
        return reverse(
            'pages:hotel-details-new',
            kwargs=redirect_kwargs) + "?checkin={0}&checkout={1}&roomconfig={2}&roomtype={3}".format(
            redirect_check_in_date.strftime(
                date_time_utils.DATE_FORMAT),
            redirect_check_out_date.strftime(
                date_time_utils.DATE_FORMAT),
            str(room_config),
            room_type)

    def getData(self, request, args, kwargs):
        self.__pagename = 'hotel_details'
        try:
            context = self.__getHotelData(request, args, kwargs)
        except Exception as exc:
            logger.exception("Exception in __getHotelData")
            # Why redirecting to 404 page here?
            # now redirecting to search city oof disabled hotel
            # logger.info("check_here_path %s" % request.path)
            city_slug = kwargs.get('city_slug', None)
            # logger.info("city here is %s" % city_slug)
            if city_slug is None:
                raise Http404
            else:
                kwargs = {'cityString': city_slug}
                return HttpResponseRedirect(
                    reverse('pages:hotels_in_city', kwargs=kwargs))
        context['styles'] = ['desktop/css/details.css']
        context['scripts'] = ['desktop/js/details.js']
        # context['googleMaps'] = True
        return context

    def __populateDirectionCssClasses(self, hotelDetailsDict):
        directions = hotelDetailsDict['hotel']['directions_set']
        for elem in directions:
            if elem['name'].lower().find('airport') != -1:
                elem['css_class'] = 'airport'
            elif elem['name'].lower().find('railway') != -1:
                elem['css_class'] = 'railway'
            elif elem['name'].lower().find('bus') != -1:
                elem['css_class'] = 'bustand'
            else:
                elem['css_class'] = ''
        return directions

    def getMobileData(self, request, args, kwargs):
        self.__pagename = 'hotel_details_mobile'
        try:
            context = self.__getHotelData(request, args, kwargs)
        except Exception as exc:
            logger.exception("Exception in __getHotelData")
            city_slug = kwargs.get('city_slug', None)
            # logger.info("city here is %s" % city_slug)
            if city_slug is None:
                raise Http404
            else:
                kwargs = {'cityString': city_slug}
                return HttpResponseRedirect(
                    reverse('pages:hotels_in_city', kwargs=kwargs))
        context['styles'] = ['mobile/css/details.css']
        context['scripts'] = ['mobile/js/details.js']
        return context

    def __parseRequest(self, request, args, kwargs):
        d = request.GET
        hotelId = kwargs['hotel_id']

        try:
            checkInDate = datetime.strptime(
                d.get('checkin'), date_time_utils.DATE_FORMAT)
        except Exception:
            logger.info(
                "Invalid date format received for checkIn :" +
                d.get(
                    'checkin',
                    "None"))
            checkInDate = datetime.now()
        try:
            checkOutDate = datetime.strptime(
                d.get('checkout'), date_time_utils.DATE_FORMAT)
        except Exception:
            logger.info(
                "Invalid date format received for checkOut :" +
                d.get(
                    'checkout',
                    "None"))
            checkOutDate = checkInDate + timedelta(days=1)

        try:
            guest = int(d.get('guests', 1))
        except ValueError:
            logger.error("Invalid guests received :" + d.get('guests', 1))
            guest = 1
        roomConfig = []

        roomConfigString = '1-0'
        roomTypeString = ''
        try:
            roomTypeString = d.get('roomtype')
            roomConfigString = d.get('roomconfig', '1-0')
            roomWiseConfigs = roomConfigString.split(',')
            logger.debug("Room Wise Config: %s", roomWiseConfigs)
            for room in roomWiseConfigs:
                roomTuple = room.split('-')
                adults = int(roomTuple[0])
                if len(roomTuple) > 1:
                    children = int(roomTuple[1])
                else:
                    children = 0
                roomConfig.append((adults, children))
        except ValueError:
            logger.exception("Invalid roomConfig Format")
            roomConfig = [(1, 0)]
        self.roomConfigStr = roomConfigString

        # NOTE: Commenting below line, as it doesn't look to do something useful
        # self.defaultDetailUrl, self.defaultDetailUrlCreated = self.__createUrl(request, args if len(args) else [],
        #																	   kwargs, hashedHotelId,
        #																	   self.defaultDetailUrlCreated)

        return hotelId, checkInDate, checkOutDate, guest, roomConfig, roomConfigString

    # NOTE: What's the use of this funciton? It's not creating a default url,
    # but only the url from the query parameters passed here.
    def __createUrl(self, request, args, kwargs, hashedHotelId, isUrlCreated):
        addedAndSign = False
        attributeDict = {}
        attributeUrl = "/?"
        baseUrl = "/hotels/" + kwargs['hotelString'] + "-h" + hashedHotelId
        d = request.GET
        checkin = d.get('checkin')
        checkout = d.get('checkout')
        roomconfig = d.get('roomconfig')
        roomtype = d.get('roomtype')

        attributeDict['checkin'] = checkin
        attributeDict['checkout'] = checkout
        attributeDict['roomconfig'] = roomconfig
        attributeDict['roomtype'] = roomtype

        if isUrlCreated is False:
            attributeDict['checkin'] = datetime.now().strftime(
                date_time_utils.DATE_FORMAT)
            attributeDict['checkout'] = (
                datetime.now() +
                timedelta(
                    days=1)).strftime(
                date_time_utils.DATE_FORMAT)
            isUrlCreated = True

        if checkin is None or roomconfig is None or checkout is None:
            return baseUrl, isUrlCreated

        for attribute, attvalue in list(attributeDict.items()):
            if attvalue is not None:
                if addedAndSign is False:
                    attributeUrl = attributeUrl + attribute + "=" + attvalue
                    addedAndSign = True
                else:
                    attributeUrl = attributeUrl + "&" + attribute + "=" + attvalue

        return baseUrl + attributeUrl, isUrlCreated
