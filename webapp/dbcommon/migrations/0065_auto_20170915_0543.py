# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0064_auto_20170911_0517'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='room',
            options={
                'ordering': ['hotel__name'],
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read')},
        ),
    ]
