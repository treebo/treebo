# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2022-02-28 03:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0102_auto_20220217_1219'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='imagespercategory',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'ordering': ['ranking']},
        ),
        migrations.AddField(
            model_name='imagespercategory',
            name='ranking',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AlterUniqueTogether(
            name='imagespercategory',
            unique_together=set([('hotel', 'ranking')]),
        ),
    ]