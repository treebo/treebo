from apps.seo.components.base_component import BaseComponent
from apps.seo.services.blog.blog_service import BlogService
from apps.seo.serializers.blog_serializer import BlogComponentSerializer
import logging

logger = logging.getLogger(__name__)


class BlogComponent(BaseComponent):

    def __init__(self, city, footer_data):
        super(BlogComponent, self).__init__(city, footer_data, [])

    def build(self):
        content = BlogService().blog_service(city_id=self.current_city.id)
        if len(content) == 0:
            data = {}
        else:
            temp_dict = {'city_name': self.current_city.slug.upper(
            ) if self.current_city.slug else self.current_city.name.upper()}
            if 'blog_title' in self.footer_data:
                title = self.footer_data['blog_title'] % temp_dict
            else:
                title = ''
            data = {
                'title': title,
                'content': content
            }
        serialized_obj = BlogComponentSerializer(data=data)
        if serialized_obj.is_valid():
            return {
                'blogs': serialized_obj.validated_data
            }
        else:
            logger.error(
                'Serializer error in blog component: %s',
                serialized_obj.errors)
        return {
            'blogs': {}
        }
