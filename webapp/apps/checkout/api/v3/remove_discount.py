import logging
from decimal import Decimal, ROUND_UP

from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

from apps.checkout.models import BookingRequest
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.pricing.dateutils import date_to_ymd_str
from apps.pricing.exceptions import RequestError
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.services.pricing_service import PricingService, SoaError
from base.views.api import TreeboAPIView
from apps.checkout.service.discount_service import DiscountService

logger = logging.getLogger(__name__)


class RemoveDiscountSerializer(serializers.Serializer):
    bid = serializers.IntegerField()


class RemoveDiscount(TreeboAPIView):
    validationSerializer = RemoveDiscountSerializer
    discount_service = DiscountService()

    def get(self, request, *args, **kwargs):
        """
        Removes any discount applied on the given booking_request_id
        bid -- BookingRequest Id
        """
        validated_data = self.serializerObject.validated_data
        booking_request_id = validated_data['bid']
        booking_request = BookingRequest.objects.filter(
            pk=booking_request_id).first()
        if not booking_request:
            return Response({"error": error_message(
                codes.INVALID_BOOKING)}, status=HTTP_400_BAD_REQUEST)

        if hasattr(
                request,
                'user') and request.user and request.user.is_authenticated():
            logger.info("request.user %s is logged in ", request.user)
            logged_in_user = True
        else:
            logged_in_user = False

        if FeatureToggleAPI.is_enabled("pricing", "soa", False):
            try:
                prices = self.get_from_soa(
                    booking_request, logged_in_user=logged_in_user)
            except RequestError as e:
                return Response({"error": error_message(
                    codes.INVALID_COUPON)}, status=HTTP_400_BAD_REQUEST)
            except SoaError as e:
                logger.exception(
                    " SoaError Exception occurred while applying coupon code using SOA API")
                return Response({"error": error_message(
                    codes.INVALID_COUPON)}, status=e.status)
            except Exception as e:
                logger.exception(
                    "unknown Exception occurred while applying coupon code using SOA API")
                return Response({"error": error_message(
                    codes.INVALID_COUPON)}, status=HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            try:
                prices = self.discount_service.get_from_local(
                    request, booking_request, coupon_code="")
            except BaseException:
                logger.exception(
                    "Exception occurred while applying coupon code using local Pricing API")
                return Response({"error": error_message(
                    codes.INVALID_COUPON)}, status=HTTP_400_BAD_REQUEST)

        self.__update_booking_request_price(
            booking_request, prices, logged_in_user)
        context = {
            "pretax_price": prices['price'],
            "total_tax": prices['tax'],
            "actual_total_cost": booking_request.total_amount,
            "nights_breakup": prices['nights'],
            "final_price": booking_request.total_amount,
            "member_discount": prices['member_discount'],
            "member_discount_applied": prices['member_discount_applied'],
            "member_discount_available": prices['member_discount_available']
        }
        return Response(context)

    def __update_booking_request_price(
            self,
            booking_request,
            prices,
            logged_in_user=False):
        booking_request.pretax_amount = prices['price']
        booking_request.tax_amount = prices['tax']
        booking_request.member_discount = prices['member_discount']
        booking_request.discount_value = 0
        booking_request.voucher_amount = 0
        booking_request.coupon_code = ""
        booking_request.coupon_apply = False
        booking_request.calculate_total_amount()
        booking_request.save()

    def get_from_soa(self, booking_request, logged_in_user=False):
        checkin = date_to_ymd_str(booking_request.checkin_date)
        checkout = date_to_ymd_str(booking_request.checkout_date)

        room_type = booking_request.room_type
        response = PricingService.get_price_from_soa(
            checkin=checkin,
            checkout=checkout,
            hotel_ids=[
                booking_request.hotel_id],
            room_config=booking_request.room_config,
            include_price_breakup=True,
            get_from_cache=False)

        room_prices = response["data"]["hotels"][0]["rooms"]
        required_room = {}
        for room in room_prices:
            if room["room_type"].lower() == room_type.lower():
                required_room = room

        nights_breakup = {}
        for room_config_price in required_room["room_config_wise_prices"]:
            for date_wise in room_config_price["date_wise_prices"]:
                if logged_in_user:
                    if date_wise["date"] not in list(nights_breakup.keys()):
                        nights_breakup[date_wise["date"]] = {
                            "date": date_wise["date"],
                            "base_price": date_wise["price"]["base_price"],
                            "tax": date_wise["price"]["member_price_tax"],
                            "sell_rate": date_wise["price"]["member_sell_price"],
                            "rack_rate": date_wise["price"]["rack_rate"],
                            "pretax_rack_rate": date_wise["price"]["base_price"],
                            "final_price": date_wise["price"]["member_sell_price"],
                            "pretax_price": date_wise["price"]["autopromo_price_without_tax"]
                        }
                    else:
                        nights_breakup[date_wise["date"]
                                       ]["base_price"] += date_wise["price"]["base_price"]
                        nights_breakup[date_wise["date"]
                                       ]["tax"] += date_wise["price"]["member_price_tax"]
                        nights_breakup[date_wise["date"]
                                       ]["sell_rate"] += date_wise["price"]["member_sell_price"]
                        nights_breakup[date_wise["date"]
                                       ]["rack_rate"] += date_wise["price"]["rack_rate"]
                        nights_breakup[date_wise["date"]
                                       ]["pretax_rack_rate"] += date_wise["price"]["base_price"]
                        nights_breakup[date_wise["date"]
                                       ]["final_price"] += date_wise["price"]["member_sell_price"]
                        nights_breakup[date_wise["date"]
                                       ]["pretax_price"] += date_wise["price"]["autopromo_price_without_tax"]
                else:
                    if date_wise["date"] not in list(nights_breakup.keys()):
                        nights_breakup[date_wise["date"]] = {
                            "date": date_wise["date"],
                            "base_price": date_wise["price"]["base_price"],
                            "tax": date_wise["price"]["tax"],
                            "sell_rate": date_wise["price"]["sell_price"],
                            "rack_rate": date_wise["price"]["rack_rate"],
                            "pretax_rack_rate": date_wise["price"]["base_price"],
                            "final_price": date_wise["price"]["sell_price"],
                            "pretax_price": date_wise["price"]["autopromo_price_without_tax"]
                        }
                    else:
                        nights_breakup[date_wise["date"]
                                       ]["base_price"] += date_wise["price"]["base_price"]
                        nights_breakup[date_wise["date"]
                                       ]["tax"] += date_wise["price"]["tax"]
                        nights_breakup[date_wise["date"]
                                       ]["sell_rate"] += date_wise["price"]["sell_price"]
                        nights_breakup[date_wise["date"]
                                       ]["rack_rate"] += date_wise["price"]["rack_rate"]
                        nights_breakup[date_wise["date"]
                                       ]["pretax_rack_rate"] += date_wise["price"]["base_price"]
                        nights_breakup[date_wise["date"]
                                       ]["final_price"] += date_wise["price"]["sell_price"]
                        nights_breakup[date_wise["date"]
                                       ]["pretax_price"] += date_wise["price"]["autopromo_price_without_tax"]

        nights_breakup_list = []
        for v in list(nights_breakup.values()):
            nights_breakup_list.append(v)

        if required_room["price"]["member_price_discount"]:
            member_discount_applied = True
            member_discount_available = True
        else:
            member_discount_applied = False
            member_discount_available = False

        if logged_in_user and member_discount_applied:
            prices = {
                "price": required_room["price"]["autopromo_price_without_tax"],
                "tax": required_room["price"]["member_price_tax"],
                "final_price": required_room["price"]["member_sell_price"],
                "nights": nights_breakup_list,
                "member_discount": required_room["price"]['member_price_discount'],
                "member_discount_applied": member_discount_applied,
                "member_discount_available": member_discount_available}
        else:
            prices = {
                "price": required_room["price"]["autopromo_price_without_tax"],
                "tax": required_room["price"]["tax"],
                "final_price": required_room["price"]["sell_price"],
                "nights": nights_breakup_list,
                "member_discount": 0,
                "member_discount_applied": False,
                "member_discount_available": member_discount_available

            }
        return prices
