# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.bookingstash.api.v1.get_cheapest_room import CheapestRoom
from apps.bookingstash.api.v1.getroomtypeavailability import RoomTypeAvailability, RoomTypeAvailabilityAll
from apps.bookingstash.api.v1.updateavailability import UpdateAvailability
app_name = 'bstash'
urlpatterns = [url(r'^getcheapestroom',
                   CheapestRoom.as_view(),
                   name='getcheapestroom'),
               url(r'^updateavailability/$',
                   UpdateAvailability.as_view(),
                   name='getavailability'),
               url(r'^mincount/$',
                   RoomTypeAvailability.as_view(),
                   name='availability-min-room'),
               url(r'^mincountall/$',
                   RoomTypeAvailabilityAll.as_view(),
                   name='availability-min-room-all'),
               ]
