import logging
from collections import defaultdict

from apps.pricing.client.tax_client import SoaTaxBackend
from apps.reward.service.reward import RewardService
from apps.pricing.response_wrappers.base_wrapper import BaseWrapper
from django.conf import settings
from decimal import Decimal
from http import HTTPStatus
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.hotels.service.hotel_sub_brand_service import HotelSubBrandService
from apps.search.constants import INDEPENDENT_HOTEL_BRAND_CODE

logger = logging.getLogger(__name__)


class SearchApiWrapper(BaseWrapper):
    reward_service = RewardService()

    def __init__(self, hotel_ids, cs_web_id_map, room_wise_price,
                 room_wise_availability, user, apply_wallet, checkin, checkout,
                 sku_map, application_code):
        self.hotel_ids = hotel_ids
        self.cs_hotel_ids = cs_web_id_map.keys()
        self.cs_web_id_map = cs_web_id_map
        self.room_wise_price = room_wise_price
        self.sku_map = sku_map
        self.checkin = checkin
        self.checkout = checkout
        self.room_wise_availability = room_wise_availability
        super().__init__(user, apply_wallet, application_code=application_code)

    def get_default_price(self):
        default_price_component = {
            'cheapest_room': {
                'room_type': 'Standard',
                'availability': 0
            },
            'net_payable_amount': 0,
            'wallet_applied': False,
            'wallet_deduction': 0,
            'wallet_type': settings.WALLET_TYPE,
            'tax': 0,
            'pretax_price': 0,
            'rack_rate': 0,
            'sell_price': 0,
            'total_discount_percent': 0,
            'member_discount': 0,
            'member_discount_applied': False,
            'member_discount_available': False,
            'rate_plan': {},
            'coupon_applied': False,
            'coupon_code': ''
        }
        return default_price_component

    def get_pricing_response(self):
        pricing_response = {}
        wallet_balance = self.get_wallet_balance()
        independent_hotel_list = HotelSubBrandService.get_hotels_ids_for_brand(brand_code=INDEPENDENT_HOTEL_BRAND_CODE)
        for hotel_id in self.hotel_ids:
            price = self.get_default_price()
            try:
                if self.room_wise_availability and self.room_wise_price and self.room_wise_price.get(hotel_id):
                    room_wise_price = self.room_wise_price.get(hotel_id, {}).get('room_wise_price', {})
                    room_type_list = list(room_wise_price.keys())
                    if room_type_list:
                        room_type = room_type_list[0]
                        rate_plan_list = room_wise_price[room_type]
                        if rate_plan_list:
                            web_rate_plan = self.get_web_rate_plan(rate_plan_list[0])
                            price_details = web_rate_plan['total_price_breakup']

                            coupon_info = self.get_applied_coupon_info(price_details['pre_tax']['discounts'])
                            wallet_info = self.get_user_wallet_info(wallet_balance, price_details['post_tax'])
                            if int(hotel_id) in independent_hotel_list:
                                wallet_info['wallet_deductable_amount'] = 0
                            applied_wallet_info = wallet_info if self.apply_wallet else self.get_default_wallet_info()

                            policy_name = web_rate_plan['policy_name']
                            price = {
                                'cheapest_room': {
                                    'room_type': room_type,
                                    'availability': self.room_wise_availability.get(hotel_id, {}).get(room_type, 0)
                                },
                                'net_payable_amount': Decimal(price_details['post_tax']),
                                'wallet_applied': applied_wallet_info['wallet_applied'],
                                'wallet_deduction': applied_wallet_info['wallet_deductable_amount'],
                                'wallet_type': applied_wallet_info['wallet_type'],
                                'tax': price_details['tax']['total_value'],
                                'pretax_price': price_details['pre_tax']['sell_price'],
                                'rack_rate': price_details['pre_tax']['list_price'],
                                'sell_price': price_details['post_tax'],
                                'rate_plan': {
                                    'tag': 'Refundable' if policy_name.lower() == 'ep' else '',
                                    'code': policy_name,
                                    'refundable': False if policy_name.lower() == 'nrp' else True,
                                    'description': 'Refundable Plan' if policy_name.lower() == 'ep' else '',
                                },
                                'coupon_code': coupon_info['coupon_code'],
                                'coupon_applied': True if coupon_info['coupon_code'] else False,
                                'total_discount_percent': coupon_info['discount_applied_percent'],
                                'member_discount': 0,
                                'member_discount_applied': False,
                                'member_discount_available': False,
                            }
            except Exception as exception:
                logger.exception("Exception in search api wrapper with error {}".format(str(exception)))
                slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                            request_param='',
                                                            dev_msg="Search API Wrapper",
                                                            message=str(exception),
                                                            class_name=self.__class__.__name__)
            pricing_response[hotel_id] = price

        if wallet_balance and wallet_balance['debit_mode'] == 'pre_tax':
            pricing_response = self._add_treebo_points_discount(pricing_response, wallet_balance)

        return pricing_response

    def _add_treebo_points_discount(self, all_hotel_price, wallet_info):

        tax_request = dict()
        sku_room_type_map = dict()
        hotel_throttled_discount = dict()
        hotel_charges = []
        if not wallet_info:
            return all_hotel_price
        for hotel_id, room_wise_prices in self.room_wise_price.items():
            sku_charges = []

            for room_type, room_type_wise_price in room_wise_prices['room_wise_price'].items():
                for room_price in room_type_wise_price:
                    pretax_price = room_price['total_price_breakup']['pre_tax']['sell_price']

                    throttled_discount = self.throttled_treebo_discount_points(
                        wallet_info['total_wallet_points'],
                        pretax_price,
                        wallet_info['debit_percentage']
                    )
                    hotel_throttled_discount[hotel_id] = throttled_discount
                    room_night_sku_count = 0
                    for date_wise_price_breakup in room_price['date_wise_price_breakup']:
                        for sku_wise_price in date_wise_price_breakup['sku_wise_prices']:
                            room_night_sku_count += 1

                    for date_wise_price_breakup in room_price['date_wise_price_breakup']:
                        for sku_wise_price in date_wise_price_breakup['sku_wise_prices']:
                            sku_room_type_map[sku_wise_price['sku_code']] = room_type.lower()
                            sku_charge = dict(pretax_price=sku_wise_price['pre_tax_price'] - \
                                                      round(throttled_discount / room_night_sku_count, 2),
                                              sku_code=sku_wise_price['sku_code'],
                                              rate_plan=room_price['policy_name']
                                              )
                            sku_charges.append(sku_charge)

            request_price_per_sku = defaultdict(float)
            updated_sku_charge = []
            for sku_charge in sku_charges:
                request_price_per_sku[sku_charge['sku_code']] += sku_charge['pretax_price']
            is_sku_traversed = dict()

            for sku_charge in sku_charges:
                if not is_sku_traversed.get(sku_charge['sku_code']):
                    updated_sku_charge.append(dict(sku_code=sku_charge['sku_code'],
                                                   pretax_price=round(request_price_per_sku[sku_charge['sku_code']], 2),
                                                   rate_plan=sku_charge['rate_plan']
                                                   ))
                    is_sku_traversed[sku_charge['sku_code']] = True

            if not updated_sku_charge:
                continue
            cs_hotel_id = [cs_id for cs_id, web_hotel_id in
                           self.cs_web_id_map.items() if web_hotel_id == int(hotel_id)]
            if not cs_hotel_id:
                continue
            cs_hotel_id = cs_hotel_id[0]
            hotel_charges.append(dict(hotel_id=cs_hotel_id,
                                 sku_charges=updated_sku_charge))

        if not hotel_charges:
            return all_hotel_price
        tax_request['hotel_charges'] = hotel_charges
        tax_request['stay_start'] = self.checkin
        tax_request['stay_end'] = self.checkout
        tax_backend_response = SoaTaxBackend.get_tax(tax_request)
        tax_response = dict()

        for hotel_id, hotel_tax_prices in tax_backend_response.items():
            price = all_hotel_price[str(self.cs_web_id_map[hotel_id])]
            treebo_discount_price_applicable = price['wallet_applied'] and price['wallet_deduction']
            if not treebo_discount_price_applicable:
                continue
            room_type = price['cheapest_room']['room_type'].lower()
            rate_plan = price['rate_plan']['code']
            room_tax = 0
            room_pre_tax = 0
            room_post_tax = 0
            for sku, sku_taxes in hotel_tax_prices.items():
                rate_plan_code = list(sku_taxes.keys())[0]
                web_rate_plan_code = self.get_web_rate_plan(
                    {'policy_name': rate_plan_code})['policy_name']
                price['treebo_points_discount'] = hotel_throttled_discount[
                    str(self.cs_web_id_map[hotel_id])]
                if not (web_rate_plan_code == rate_plan and sku_room_type_map[sku] == room_type):
                    continue
                tax_response[sku_room_type_map[sku].lower()] = sku_taxes
                room_tax += sku_taxes[rate_plan_code]['tax']
                room_post_tax += sku_taxes[rate_plan_code]['post_tax_price']
                room_pre_tax += sku_taxes[rate_plan_code]['pre_tax_price']
            price['tax'] = round(room_tax, 2)
            price['pretax_price'] = round(room_pre_tax, 2)
            price['net_payable_amount'] = round(room_post_tax, 2)
            price['wallet_applied'] = False
            price['wallet_deduction'] = 0

        return all_hotel_price
