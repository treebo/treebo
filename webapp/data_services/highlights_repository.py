from data_services.decorators import SingletonDecorator


@SingletonDecorator
class HighlightsRepository(object):

    def __init__(self, highlights_web_repository):
        self.highlights_web_repository = highlights_web_repository

    def get_all_highlights(self):
        return self.highlights_web_repository.get_all_highlights()

    def filter_localities(self, *args, **kwargs):
        return self.highlights_web_repository.filter_localities(**kwargs)
