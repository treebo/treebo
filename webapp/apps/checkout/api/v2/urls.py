from django.conf.urls import url

from apps.checkout.api.v2.booking_confirmation import ConfirmBooking
# from apps.checkout.api.v2.cancel_booking import CancelBookingApi
from apps.checkout.api.v2.pay_at_hotel import PayAtHotel
from apps.checkout.api.v2.pay_now import PayNow
from apps.checkout.api.v1.checkout_actions import check_availability
from apps.checkout.api.v1.itinerary_page_api import ItineraryPageAPI
from apps.checkout.api.v1.get_discount import ApplyDiscount
from apps.checkout.api.v1.remove_discount import RemoveDiscount
from apps.checkout.api.v1.confirmed_booking_details import ConfirmedBookingDetail
from apps.checkout.api.v2.razorpay_handler import RazorPay

app_name = 'checkout_v2'
urlpatterns = [
    url(r'^coupon/$', ApplyDiscount.as_view(), name='apply-coupon-v2'),
    url(r'^coupon/remove/$', RemoveDiscount.as_view(), name='remove-coupon-v2'),
    url(r'^availability/$', check_availability, name='check-availability'),
    url(r'^itinerary/$', ItineraryPageAPI.as_view(), name='itinerary-page-v2'),
    url(r'^payathotel/$', PayAtHotel.as_view(), name='paylater'),
    url(r'^paynow/$', PayNow.as_view(), name='paynow'),
    url(r'^razorpay/?$', RazorPay.as_view(), name='razorpay'),
    url(r'^confirmed-booking/(?P<order_id>[\w-]+)/',
        ConfirmedBookingDetail.as_view(),
        name='confirmed-booking'),
    url(r'^confirmbooking/$', ConfirmBooking.as_view(), name='confirmbooking'),
    # url(r'^booking/cancel/?$', login_required(CancelBookingApi.as_view(), redirect_field_name="next",
    #                                           login_url="/login"), name='booking-cancel-v2'),

]
