MAX_SMALL_INTEGER = 32768


class RoomType:
    ACACIA = 'Acacia'
    OAK = 'Oak'
    MAPLE = 'Maple'
    MAHOGANY = 'Mahogany'

    ROOM_TYPE_ORDER = [ACACIA, OAK, MAPLE, MAHOGANY]
    ROOM_TYPE_ORDER_API = [
        [ACACIA, 0],
        [OAK, 1],
        [MAPLE, 2],
        [MAHOGANY, 3]
    ]
    ROOM_TYPE_DICT = {ACACIA: 'Solo', OAK: 'Standard', MAPLE: 'Deluxe', MAHOGANY: 'Premium'}
    ROOM_COLLECTIVE_CATEGORY = ['Room', 'Washroom']


LEAST_PRIORITY_ROOM_INDEX = len(RoomType.ROOM_TYPE_ORDER)


class Category:
    FACADE = 'Facade'
    RECEPTION = 'Reception'
    LOBBY = 'Lobby'
    RESTAURANT = 'Restaurant'
    ROOM = 'Room'
    WASHROOM = 'Washroom'
    DINING = 'Dining'
    LIFT = 'Lift'
    NEARBY = 'Nearby'
    OTHERS = 'Others'
    OTHER_AMENITIES = 'OTHER_AMENITIES'

    CATEGORY_LIST = [ROOM, FACADE, RECEPTION, LOBBY, RESTAURANT, WASHROOM, OTHER_AMENITIES, DINING,
                     LIFT, NEARBY, OTHERS]

    CATEGORY_ORDER = {key: MAX_SMALL_INTEGER + value for value, key in enumerate(CATEGORY_LIST)}
