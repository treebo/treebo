from apps.payments.models import PaymentMethod as PaymentMethodModel
from apps.payments.offers.dataclasses.payment_method import PaymentMethod


class PaymentMethodRepository:

    def get_payment_methods_for_utm(self, utm_source):
        payment_methods = PaymentMethodModel.objects.filter(
            utm_source=utm_source)
        if not payment_methods:
            return None
        return [self._convert_to_dataclass(payment_method) for payment_method in payment_methods]

    def _convert_to_dataclass(self, payment_method):
        return PaymentMethod(uid=payment_method.id,
                      name=payment_method.name,
                      category=payment_method.category,
                      image_url=payment_method.image_url,
                      gateway_code=payment_method.gateway_code,
                      ranking=payment_method.ranking
                      )
