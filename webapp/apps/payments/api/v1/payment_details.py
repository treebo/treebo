# -*- coding:utf-8 -*-
"""
    payment_detailsPaymentInfo  API
"""
from apps.bookings.models import PaymentOrder
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.payments.serializer import PaymentOrderSerializer
from base.logging_decorator import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger
from common.exceptions.treebo_exception import TreeboException
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = get_booking_logger(__name__)


class PaymentDetails(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = []

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def get(self, request, ps_orderid, *args, **kwargs):
        try:
            try:
                payment_order = PaymentOrder.objects.get(
                    ps_order_id=ps_orderid)
            except PaymentOrder.DoesNotExist:
                logger.error(
                    " invalid request for payment details %s ",
                    request.data)
                return api_response.error_response_from_error_code(
                    error_codes.PAYMENT_ORDER_NOT_FOUND, api_name=PaymentDetails.__name__)
            payment_serializer = PaymentOrderSerializer(payment_order)
            response = api_response.prep_success_response(
                payment_serializer.data)
        except TreeboException as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        message=e.__str__(), dev_msg="TreeboException",
                                                        class_name=self.__class__.__name__)
            logger.exception(
                "treebo payment_details payment v1 %s ",
                request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception(
                "internal payment_details payment v1 internal error %s ",
                request.data)
            response = api_response.internal_error_response(
                error_codes.get(
                    error_codes.UNABLE_TO_FETCH_PAYMENT)['msg'],
                api_name=PaymentDetails.__name__)
        return response
