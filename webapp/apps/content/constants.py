class PAGES():
    CITY = 'city'
    CATEGORY = 'category'
    LANDMARK = 'landmark'
    LOCALITY = 'locality'
    HD = 'hd'
    SEARCH = 'search'
    COMMON = 'common'
    NEAR_ME = 'near'
    CITY_AMENITY = 'city-amenity'
    LOCALITY_CATEGORY = 'locality-category'  # for locality x category page
    LANDMARK_CATEGORY = 'landmark-category'  # for landmark x category page


GOOGLEMAP_URL = 'https://www.google.com/maps/place/%s,%s'

META_CACHE_TIMEOUT = 86400
META_CACHE_PAGE_TIMEOUT = 300
DEFAULT_TA_REVIEW_COUNT = 10
POSTS_CACHE_TIMEOUT = 86400