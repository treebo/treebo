from django.conf.urls import url

from apps.reviews.views import TAReviewData, JCRUrlGeneration, TANonJCRReviewData, UpdateReviews

from webapp.apps.reviews.views import TripAdvisorReviewData

app_name = 'reviews'
urlpatterns = [
    url(r"^tripadvisor/hotel_review_data/", TAReviewData.as_view(), name="tripadvisor-hotelreviews"),
    url(r"^tripadvisor/jcr_url_generation/$", JCRUrlGeneration.as_view(), name="tripadvisor-jcrurlgeneration"),
    url(r"^tripadvisor/hotel_non_jcr_review_data/", TANonJCRReviewData.as_view(), name="tripadvisor-nonjcrreviews"),
    url(r"^tripadvisor/update_reviews/", UpdateReviews.as_view(), name="tripadvisor-updatereviews"),
    url(r"^tripadvisor/reviews/", TripAdvisorReviewData.as_view(), name="tripadvisor-reviewdata")

]
