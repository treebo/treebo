import os

bind = '0.0.0.0:8000'

reload = False
workers = os.environ.get('GUNICORN_WORKER')
worker_class = 'gevent'
max_requests = 2400
max_requests_jitter = 20
timeout = os.environ.get('GUNICORN_TIMEOUT')
keepalive = 65

worker_connections = 1000
pidfile = '/usr/src/gunicorn.pid'

accesslog = os.environ.get(
    'LOG_ROOT',
    '/var/log/website/') + '/gunicorn_access.log'
errorlog = os.environ.get(
    'LOG_ROOT',
    '/var/log/website/') + '/gunicorn_error.log'
loglevel = 'debug'
proc_name = 'web_direct_backend_service'
