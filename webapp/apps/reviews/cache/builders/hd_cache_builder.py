import json
import datetime
import logging

from apps.reviews.cache.builders.base_cache_data_builder import BaseCacheDataBuilder
from apps.reviews.services.trip_advisor.build_ta_response import BuildTAReviewResponse

logger = logging.getLogger(__name__)


class CacheDataBuilderHD(BaseCacheDataBuilder):

    def __init__(self, ):
        super(CacheDataBuilderHD, self).__init__()

    def set_key(self, hotel_id, page):
        super(CacheDataBuilderHD, self).set_key(hotel_id, page)

    def build_response(self, ta_review_obj):
        return ta_review_obj.build_response_hd()
