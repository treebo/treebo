import logging
from datetime import datetime, timedelta

import requests
from django.conf import settings
from django.core.mail.message import EmailMessage
from django.core.management.base import BaseCommand

from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Update Price Dates'

    def handle(self, *args, **options):
        try:
            print("Fetching rack rate")
            today = datetime.today()
            tomorrow = datetime.today() + timedelta(days=1)
            response = requests.get(
                settings.TREEBO_WEB_URL +
                '/rest/v1/pricing/hotels/?checkin={0}&checkout={1}&roomconfig=1-0'. format(
                    today.strftime('%Y-%m-%d'),
                    tomorrow.strftime('%Y-%m-%d')))

            data = response.json()['data']
            print(data)

            hotel_repository = RepositoriesFactory.get_hotel_repository()
            hotels_in_bengaluru = hotel_repository.get_enabled_hotels_for_city(city_id=1)
            print(hotels_in_bengaluru)
            rack_rate_count = [sum(1 if data[str(hotel.id)]['rack_rate'] == data[str(
                hotel.id)]['sell'] else 0) for hotel in hotels_in_bengaluru]

            if rack_rate_count > 15:
                # production issues channel
                email = EmailMessage(
                    "***URGENT*** More than {0} hotels in Bengaluru are being sold at rack rate!" .format(
                        str(rack_rate_count)),
                    "Please restart server",
                    settings.SERVER_EMAIL,
                    settings.RESERVATION_FAILURE_EMAIL_LIST,
                )
                email.send(fail_silently=True)
            print("Finished checking rack rate")
        except Exception as exc:
            logger.exception("Unable to start command")


if __name__ == "__main__":
    Command().handle()
