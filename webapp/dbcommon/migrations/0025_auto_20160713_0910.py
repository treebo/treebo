# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0024_auto_20160712_0547'),
    ]

    operations = [
        migrations.AddField(
            model_name='userreferralreward',
            name='friend',
            field=models.OneToOneField(
                related_name='friend',
                default=823,
                to=settings.AUTH_USER_MODEL,
                on_delete=models.DO_NOTHING),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userreferralreward',
            name='reward_id',
            field=models.CharField(
                max_length=25,
                null=True,
                blank=True),
        ),
    ]
