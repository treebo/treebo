import collections
import logging
from datetime import datetime

from django.db.models import Min, Sum

from apps.bookingstash.models import Availability
from dbcommon.models.room import Room
from apps.common import date_time_utils as dt
from apps.hotels import utils as hotelUtils
from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.pricing_api_impl import PriceUtilsV2
from apps.pricing.utils import PriceUtils
from availability.services.its_client import ITSSearchService
from availability.services.models import TDHotelAvailabilitySearchParams
from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)
hotel_repository = RepositoriesFactory.get_hotel_repository()
room_data_service = RepositoriesFactory.get_room_repository()
availability_service_impl = None


def use_its_apis():
    from django.conf import settings
    return settings.USE_ITS_APIS


def unset_availability_service():
    global availability_service_impl
    availability_service_impl = None


def get_availability_service():
    global availability_service_impl
    if availability_service_impl is None:
        if use_its_apis():
            availability_service_impl = ITSAvailabilityService()
        else:
            availability_service_impl = DirectAvailabilityService()
    return availability_service_impl


def get_max_adults_with_children(room_configs):
    room_config_list = room_configs.split(',')
    max_adults = 0
    max_adults_with_children = 0

    for room_configuration in room_config_list:
        adults_children_list = room_configuration.split('-')
        if int(adults_children_list[0]) > max_adults:
            max_adults = int(adults_children_list[0])
        if int(adults_children_list[0]) + int(adults_children_list[1]) > max_adults_with_children:
            max_adults_with_children = int(adults_children_list[0]) + int(adults_children_list[1])
    return max_adults, max_adults_with_children


def get_max_allowed_room_config_mapping(hotel_ids):
    max_allowed_room_config_mapping = {}
    max_allowed_room_config_list = Room.objects.filter(
        hotel_id__in=hotel_ids).values('hotel_id', 'room_type_code', 'max_guest_allowed', 'max_adult_with_children')

    for hotel_room_config in max_allowed_room_config_list:
        if hotel_room_config['hotel_id'] not in max_allowed_room_config_mapping:
            max_allowed_room_config_mapping[hotel_room_config['hotel_id']] = {}
        max_allowed_room_config_mapping[hotel_room_config['hotel_id']][hotel_room_config['room_type_code'].lower()] = {
            'max_guest_allowed': hotel_room_config['max_guest_allowed'],
            'max_adult_with_children': hotel_room_config['max_adult_with_children']
        }

    return max_allowed_room_config_mapping


def get_formatted_checkin_checkout_date(check_in_date, check_out_date):
    check_in_date = datetime.strptime(check_in_date, "%Y-%m-%d").date()
    check_out_date = datetime.strptime(check_out_date, "%Y-%m-%d").date()
    return check_in_date, check_out_date


def room_config_supported(room, room_configs):
    return all(hotelUtils.isOccupancySupported(room, single_room_config[0], single_room_config[1])
               for single_room_config in room_configs)


def get_price_utils():
    if FeatureToggleAPI.is_enabled("pricing", "v2_impl_enabled", False):
        return PriceUtilsV2
    return PriceUtils


def get_room_type_min_availability_for_hotel_from_availability_sync(hotel, check_in_date,
                                                                    check_out_date, rooms_config):
    all_room_availabilities = get_availability_service().get_room_type_min_availability_for_hotel(
        hotel.id, check_in_date, check_out_date)

    availability_list = []
    all_rooms = hotel_repository.get_rooms_for_hotel(hotel)
    requested_room_count = len(rooms_config)
    room_dict = {room.room_type_code.title(): room for room in all_rooms}

    for singleRoom in all_room_availabilities:
        room_type = singleRoom[1]
        available_room_count = singleRoom[2]
        availability = False

        room = room_dict.get(room_type)
        if room and available_room_count >= requested_room_count and room_config_supported(room, rooms_config):
            availability = True

        availability_list.append(
            {
                'roomtype': room_type,
                'available': availability,
            })

    return availability_list


def room_config_string_to_list(room_config_string):
    rooms_config = [(int(room.split('-')[0]), int(room.split('-')[1]) if len(room.split('-')) > 1 else 0)
                    for room in room_config_string.split(',')]
    return rooms_config


def check_availability(hotel, room_type, room_config, checkin_date_string, checkout_date_string):
    room_config_list = room_config_string_to_list(room_config)

    room_availabilities = get_room_type_min_availability_for_hotel_from_availability_sync(
        hotel, checkin_date_string, checkout_date_string, room_config_list)

    if room_availabilities:
        for room_availability in room_availabilities:
            if room_availability['roomtype'] == room_type:
                return room_availability['available']

    return False

class ITSAvailabilityService:
    """
    This class returns the data from ITS API's
    """

    @staticmethod
    def get_day_wise_availability(availability_dto):
        """
        Get the availability for each room in the date range for the passed list of hotels from
        its web services
        :param (apps.bookingstash.dto.availability_dto.AvailabilityRequestDTO) availability_dto:
        :returns
        {
            "2": {
              "2017-10-21": {
                "oak": 20,
                "maple": 4
              },
              "2017-10-20": {
                "oak": 20,
                "maple": 4
              },
              "2017-10-22": {
                "oak": 20,
                "maple": 4
              }
            },
            "63": {
              "2017-10-21": {
                "oak": 0,
                "maple": 0
              },
              "2017-10-20": {
                "oak": 0,
                "maple": 0
              },
              "2017-10-22": {
                "oak": 0,
                "maple": 0
              }
            }
        }

        """
        dto_data = availability_dto.validated_data
        hotels_ids = dto_data['hotels']

        search_params = TDHotelAvailabilitySearchParams(hotels_ids, dto_data['checkin'],
                                                        dto_data['checkout'],
                                                        None)

        availability = ITSSearchService.date_wise_room_type_availability(search_params)
        result = {
            hotel_data.hotel_id: {
                date_available:
                    {
                        rm_type.room_type.lower(): rm_type.rooms_available
                        for rm_type in hotel_data.data[date_available]
                    }
                for date_available in hotel_data.data.keys()
            }
            for hotel_data in availability.data
        }
        return result

    @staticmethod
    def get_total_available_rooms_for_hotels(
        hotel_ids, check_in_date, check_out_date, room_config):
        # Get data from ITS API's
        search_param = TDHotelAvailabilitySearchParams(hotel_ids,
                                                       check_in_date,
                                                       check_out_date,
                                                       room_config)
        availability_across_configs = \
            ITSSearchService.get_availability_across_config(search_param)

        max_adults, max_adult_with_children = get_max_adults_with_children(room_config)

        availability_across_configs = ITSAvailabilityService.get_availability_across_config_based_on_max_guests_allowed(
            hotel_ids, room_config, availability_across_configs)

        # Parse through the data to get in the same format as from DB
        available_rooms = {(hotel_availability.hotel_id, rm_type.room_type): rm_type.rooms_available
                           for hotel_availability in availability_across_configs
                           for rm_type in hotel_availability.room_type_availability}
        return available_rooms, max_adults, max_adult_with_children

    @staticmethod
    def get_available_rooms_for_hotels(hotel_ids, check_in_date, check_out_date, room_config):
        """
                Gets the availability for each room for a list of hotels
                :param hotel_ids: list of hotel ids
                :param check_in_date: check in date
                :param check_out_date: check out date
                :param room_config: room configuration requested
                :return: dict in the following format
                {
                    (<hotel_id>, '<room_type>'): <numbers>,
                    (166, 'maple'): 13,
                    (341, 'maple'): 19,
                    (166, 'mahogany'): 7
                }
        """
        # Get data from ITS API's
        search_param = TDHotelAvailabilitySearchParams(hotel_ids,
                                                       check_in_date,
                                                       check_out_date,
                                                       room_config)
        availability_across_configs = ITSSearchService.get_availability_across_config(search_param)

        availability_across_configs = ITSAvailabilityService.get_availability_across_config_based_on_max_guests_allowed(
            hotel_ids, room_config, availability_across_configs)

        # Parse through the data to get in the same format as from DB
        available_rooms = {(hotel_availability.hotel_id, rm_type.room_type): rm_type.rooms_available
                           for hotel_availability in availability_across_configs
                           for rm_type in hotel_availability.room_type_availability}

        return available_rooms

    @staticmethod
    def get_available_rooms(hotel_id, check_in_date, check_out_date, room_config):
        available_rooms = collections.defaultdict(int)
        room_code = room_data_service.get_roomtypes_in_hotel([hotel_id])

        search_param = TDHotelAvailabilitySearchParams([hotel_id],
                                                       check_in_date,
                                                       check_out_date,
                                                       room_config)
        availability_across_configs = ITSSearchService.get_availability_across_config(search_param)

        availability_across_configs = ITSAvailabilityService.get_availability_across_config_based_on_max_guests_allowed(
            [hotel_id], room_config, availability_across_configs)

        if availability_across_configs.__len__() != 0:
            availability = availability_across_configs[0]
            room_types = room_code.get(hotel_id)
            for rm_type in availability.room_type_availability:
                room_id = room_types.get(rm_type.room_type)
                if room_id:
                    available_rooms[room_data_service.get_single_Room(pk=room_id)] = rm_type.rooms_available
        return available_rooms

    @staticmethod
    def get_room_type_min_availability_for_all_hotels(check_in_date, check_out_date):
        """
                :param checkInDate: The date for check-in
                :param checkOutDate: The date for checkout
                :return:
                [
                    [
                        <Hotelid>,
                        "<Room_type>",
                        <Availability>
                    ],
                    [
                        324,
                        "Oak",
                        7
                    ],
                    [
                        222,
                        "Oak",
                        24
                    ],
                    [
                        380,
                        "Oak",
                        16
                    ]
                ]
        """
        hotel_ids = hotel_repository.get_all_active_hotel_ids()
        check_in_date, check_out_date = get_formatted_checkin_checkout_date(check_in_date, check_out_date)
        rooms = []
        # Assuming default config of 1-0, since it would fetch the information of
        # all availabilities in all hotels
        room_config = "1-0"
        search_param = TDHotelAvailabilitySearchParams(hotel_ids,
                                                       check_in_date,
                                                       check_out_date,
                                                       room_config)
        availability_across_configs = \
            ITSSearchService.get_availability_across_config(search_param)
        for hotel in availability_across_configs:
            for room_type in hotel.room_type_availability:
                room_det = [hotel.hotel_id, room_type.room_type.title(), room_type.rooms_available]
                rooms.append(room_det)
        return rooms

    @staticmethod
    def get_room_type_min_availability_for_hotel(hotel, check_in_date, check_out_date):
        """
        Get the room types available in the hotel for the check-in date and
        checkout dates requested in the given hotel
        :param hotel:
        :param checkInDate:
        :param checkOutDate:
        :return: [
            (<hotel_id>, <Room_type>,<availability>)
            (341, 'Maple', 19),
            (341, 'Oak', 38)
            ]
        """
        check_in_date, check_out_date = get_formatted_checkin_checkout_date(check_in_date, check_out_date)
        rooms = []
        default_room_config = '1-0'
        search_param = TDHotelAvailabilitySearchParams([hotel],
                                                       check_in_date,
                                                       check_out_date,
                                                       default_room_config)
        availability_across_configs = \
            ITSSearchService.get_availability_across_config(search_param)[0]
        for room_type in availability_across_configs.room_type_availability:
            rooms.append((hotel, room_type.room_type.title(), room_type.rooms_available))
        return rooms

    @staticmethod
    def get_room_type_min_availability(hotel_id, check_in_date, check_out_date, room_type):
        """
                Returns the number of room of the particular class available
                :param hashed_hotel_id:
                :param checkInDate:
                :param checkOutDate:
                :param roomType: Roomtype viz. Mahogany, oak etc...
                :return: If the room type doesn't exists, then returns zero or integer value of number
                        of rooms of the requested type available
                """
        hotel_id = hotelUtils.decode_hash(hotel_id)
        check_in_date, check_out_date = get_formatted_checkin_checkout_date(check_in_date, check_out_date)
        # Get data from ITS API's
        # Default config of 1-0 would fetch data for all rooms as it
        # will match everything. From there we can exract the data we
        # are interested in
        default_room_config = '1-0'
        search_param = TDHotelAvailabilitySearchParams([hotel_id],
                                                       check_in_date,
                                                       check_out_date,
                                                       default_room_config)
        availability_across_configs = \
            ITSSearchService.get_availability_across_config(search_param)[0]
        roomType = room_type.lower().strip()
        for room_type in availability_across_configs.room_type_availability:
            if room_type.room_type == roomType:
                return room_type.rooms_available
        return 0

    @staticmethod
    def get_availability_across_config_based_on_max_guests_allowed(hotel_ids_list,
                                                                   room_config, availability_across_configs):

        max_adults, max_adult_with_children = get_max_adults_with_children(room_config)

        max_allowed_room_config_mapping = get_max_allowed_room_config_mapping(hotel_ids_list)
        availability_across_configs_copy = availability_across_configs.copy()

        hotels_available_counter = 0

        for hotels_available in availability_across_configs_copy:
            hotels_room_type_availability = hotels_available.room_type_availability
            hotel_room_type_counter = 0
            for hotel_room_type in hotels_room_type_availability:
                try:
                    max_config = max_allowed_room_config_mapping[hotels_available.hotel_id][hotel_room_type.room_type]
                except KeyError:
                    max_config = {'max_guest_allowed': 0, 'max_adult_with_children': 0}
                if max_config['max_guest_allowed'] < max_adults or max_config[
                        'max_adult_with_children'] < max_adult_with_children:
                    availability_across_configs[hotels_available_counter].room_type_availability.pop(
                        hotel_room_type_counter)
                hotel_room_type_counter = hotel_room_type_counter + 1
            hotels_available_counter = hotels_available_counter + 1

        return availability_across_configs

    @staticmethod
    def get_availability_data(cs_ids, check_in_date, check_out_date, room_config):

        search_param = TDHotelAvailabilitySearchParams(checkin=check_in_date,
                                                       checkout=check_out_date,
                                                       room_config=room_config, cs_ids=cs_ids)

        availability_data = ITSSearchService.get_availability_across_room_config(
            search_param=search_param)
        availability_data = ITSAvailabilityService.transform_availability_data(
            availability_data=availability_data['data'])

        return availability_data

    @staticmethod
    def transform_availability_data(availability_data):
        """
        :param availability_data: list of available hotels with room config
        [
        {
            "availability": [
                {
                    "availability": 3,
                    "room_type": "RT03"
                },
                {
                    "availability": 15,
                    "room_type": "RT02"
                }
            ],
            "hotel_id": "9906504"
        }
    ]
        :return: dict of hotels_ids with room_type and availability
        {
            "9906504": {
                "RT02": 15,
                "RT03": 3
            }
        }
        """
        result_dict = {}
        for hotel_availability in availability_data:
            hotel_id = hotel_availability['hotel_id']
            room_wise_availabilities = hotel_availability['availability']
            availability_dict = {}

            for room_wise_availability in room_wise_availabilities:
                availability_dict[room_wise_availability['room_type']] = room_wise_availability['availability']

            result_dict[hotel_id] = availability_dict

        return result_dict


class DirectAvailabilityService:
    """
    This API gets the information from Treebo DB
    """

    @staticmethod
    def get_day_wise_availability(availability_dto):
        """
        Get the availability for each room in the date range for the passed list of hotels from db
        :param (apps.bookingstash.dto.availability_dto.AvailabilityRequestDTO) availability_dto:
        :returns
        {
            "2": {
              "2017-10-21": {
                "oak": 20,
                "maple": 4
              },
              "2017-10-20": {
                "oak": 20,
                "maple": 4
              },
              "2017-10-22": {
                "oak": 20,
                "maple": 4
              }
            },
            "63": {
              "2017-10-21": {
                "oak": 0,
                "maple": 0
              },
              "2017-10-20": {
                "oak": 0,
                "maple": 0
              },
              "2017-10-22": {
                "oak": 0,
                "maple": 0
              }
            }
        }

        """
        dto_data = availability_dto.validated_data
        availability_per_room = Availability.objects.filter(
            room__hotel_id__in=dto_data['hotels'],
            date__gte=dto_data['checkin'],
            date__lt=dto_data['checkout']).select_related("room").values(
            'room__hotel_id',
            'room__room_type_code',
            'availablerooms',
            'date').order_by(
            "room__hotel_id",
            "date")

        availabilities = dict()
        for availability in availability_per_room:
            # Find hotel or add a new entry for a hotel
            hotel_availability = availabilities.get(
                availability["room__hotel_id"], {})

            availability_date = dt.format_date(availability['date'])

            # Find date or add an entry for date
            hotel_availability_date = hotel_availability.get(availability_date, {})
            hotel_availability_date.update({
                availability["room__room_type_code"].lower(): availability["availablerooms"]
            })
            hotel_availability.update({
                availability_date: hotel_availability_date
            })
            availabilities.update({
                availability["room__hotel_id"]: hotel_availability
            })
        return availabilities

    @staticmethod
    def get_total_available_rooms_for_hotels(hotel_ids, check_in_date,
                                             check_out_date, room_config):
        max_adults, max_adults_with_children = get_max_adults_with_children(room_config)
        room_config_list = room_config.split(',')
        number_of_rooms = len(room_config_list)

        qs = Availability.objects.filter(
            room__hotel_id__in=hotel_ids,
            date__gte=check_in_date,
            date__lt=check_out_date,
            room__max_guest_allowed__gte=max_adults,
            room__max_adult_with_children__gte=max_adults_with_children)
        room_availabilities = qs.values(
            'room__hotel_id',
            'room_id',
            'room__room_type_code').annotate(
            min_rooms=Sum('availablerooms'))

        available_rooms = {}
        for availability in room_availabilities:
            if availability['min_rooms'] >= number_of_rooms:
                # room = Room.objects.select_related('hotel').get(pk=availability['room_id'])
                # available_rooms[(room.hotel_id, room.room_type_code.lower())] = availability['min_rooms']
                available_rooms[(availability['room__hotel_id'],
                                 availability['room__room_type_code'].lower())] = availability['min_rooms']
        return available_rooms, max_adults, max_adults_with_children

    @staticmethod
    def get_available_rooms_for_hotels(hotel_ids, check_in_date, check_out_date, room_config):
        """
                Gets the availability for each room for a list of hotels
                :param hotel_ids: list of hotel ids
                :param check_in_date: check in date
                :param check_out_date: check out date
                :param room_config: room configuration requested
                :return: dict in the following format
                {
                    (<hotel_id>, '<room_type>'): <numbers>,
                    (166, 'maple'): 13,
                    (341, 'maple'): 19,
                    (166, 'mahogany'): 7
                }
        """
        room_config_list = room_config.split(',')
        max_adults = 0
        max_adults_with_children = 0
        available_rooms = {}
        for room_configuration in room_config_list:
            try:
                adults_children_list = room_configuration.split('-')
                if int(adults_children_list[0]) > max_adults:
                    max_adults = int(adults_children_list[0])
                if int(
                    adults_children_list[0]) + int(adults_children_list[1]) > max_adults_with_children:
                    max_adults_with_children = int(
                        adults_children_list[0]) + int(adults_children_list[1])
            except BaseException:
                raise

        number_of_rooms = len(room_config_list)

        qs = Availability.objects.filter(
            room__hotel_id__in=hotel_ids,
            date__gte=check_in_date,
            date__lt=check_out_date,
            room__max_guest_allowed__gte=max_adults,
            room__max_adult_with_children__gte=max_adults_with_children)
        room_availabilities = qs.values(
            'room__hotel_id',
            'room_id',
            'room__room_type_code').annotate(
            min_rooms=Min('availablerooms'))

        for availability in room_availabilities:
            if availability['min_rooms'] >= number_of_rooms:
                available_rooms[(availability['room__hotel_id'],
                                 availability['room__room_type_code'].lower())] = availability['min_rooms']
        return available_rooms

    @staticmethod
    def get_available_rooms(hotel_id, check_in_date, check_out_date, room_config):

        max_adults, max_adults_with_children = get_max_adults_with_children(room_config)
        available_rooms = collections.defaultdict(int)
        room_config_list = room_config.split(',')
        number_of_rooms = len(room_config_list)
        # Gets the minimum number of rooms available for each type in the dates
        # between check-in and checkout we need
        availabilities = Availability.objects.filter(
            room__hotel_id=hotel_id,
            date__gte=check_in_date,
            date__lt=check_out_date,
            room__max_guest_allowed__gte=max_adults,
            room__max_adult_with_children__gte=max_adults_with_children).values('room_id').annotate(
            min_rooms=Min('availablerooms'))
        for availability in availabilities:
            if availability['min_rooms'] >= number_of_rooms:
                available_rooms[room_data_service.get_single_Room(
                    pk=availability['room_id'])] = availability['min_rooms']
        return available_rooms

    @staticmethod
    def get_room_type_min_availability_for_all_hotels(check_in_date, check_out_date):
        """
                :param checkInDate: The date for check-in
                :param checkOutDate: The date for checkout
                :return:
                [
                    [
                        <Hotelid>,
                        "<Room_type>",
                        <Availability>
                    ],
                    [
                        324,
                        "Oak",
                        7
                    ],
                    [
                        222,
                        "Oak",
                        24
                    ],
                    [
                        380,
                        "Oak",
                        16
                    ]
                ]
                """
        hotel_ids = hotel_repository.get_all_active_hotel_ids()
        check_in_date, check_out_date = get_formatted_checkin_checkout_date(check_in_date, check_out_date)
        rooms = Availability.objects.filter(
            room__hotel_id__in=hotel_ids,
            date__gte=check_in_date,
            date__lt=check_out_date).values_list(
            'room__hotel_id',
            'room__room_type_code').annotate(
            Min('availablerooms')).order_by('room__hotel_id')
        return rooms

    @staticmethod
    def get_room_type_min_availability_for_hotel(hotel, check_in_date, check_out_date):
        """
        Get the room types available in the hotel for the check-in date and
        checkout dates requested in the given hotel
        :param hotel:
        :param checkInDate:
        :param checkOutDate:
        :return: [
            (<hotel_id>, <Room_type>,<availability>)
            (341, 'Maple', 19),
            (341, 'Oak', 38)
            ]
        """
        check_in_date, check_out_date = get_formatted_checkin_checkout_date(check_in_date, check_out_date)
        rooms = Availability.objects.filter(
            room__hotel=hotel,
            date__gte=check_in_date,
            date__lt=check_out_date).values_list(
            'room__hotel_id',
            'room__room_type_code').annotate(
            Min('availablerooms')).order_by('room__hotel_id')
        return rooms

    @staticmethod
    def get_room_type_min_availability(hashed_hotel_id, check_in_date, check_out_date, room_type):
        """
            Returns the number of room of the particular class available
            :param hashed_hotel_id:
            :param checkInDate:
            :param checkOutDate:
            :param roomType: Roomtype viz. Mahogany, oak etc...
            :return: If the room type doesn't exists, then returns zero or integer value of number
                    of rooms of the requested type available
        """
        hotel_id = hotelUtils.decode_hash(hashed_hotel_id)
        check_in_date, check_out_date = get_formatted_checkin_checkout_date(check_in_date, check_out_date)
        try:
            minroom = Availability.objects.filter(
                room__hotel_id=hotel_id,
                room__room_type_code__iexact=room_type,
                date__gte=check_in_date,
                date__lt=check_out_date).aggregate(
                Min('availablerooms'))
            return minroom['availablerooms__min'] if minroom['availablerooms__min'] else 0
        except Availability.DoesNotExist:
            return 0
