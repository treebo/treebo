from apps.search.constants import AUTOCOMPLETE_RESULTS_MAX_COUNT
from apps.search.services.auto_completion_service_factory import AutoCompletionServiceFactory
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.search.services.google_auto_completion_service import GoogleAutoCompletionService
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework import status

city_data_service = RepositoriesFactory.get_city_repository()


class AutoCompletionAPI(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        """
        AutoCompletion API
        search_string -- Search String to auto-complete
        """
        try:
            search_string = request.GET.get('search_string')
            session_token = request.GET.get('session_token', None)
            search_string = search_string.lower()
            search_string = search_string.replace('hotels in', '')
            search_string = search_string.replace('hotel in', '')
            search_string = search_string.replace('hotel near', '')
            search_string = search_string.replace('hotels near', '')
            search_string = search_string.strip()

            meta_key = request.GET.get('meta_key')

            if not search_string:
                cities = city_data_service.get_all_cities()
                hotel_repository = RepositoriesFactory.get_hotel_repository()
                hotels = hotel_repository.get_active_hotel_list()
                # TODO: Let the hotel service give a proper dto
                city_ids = [hotel.city.id for hotel in hotels]
                from collections import Counter, OrderedDict
                hotels_count_for_city = Counter(city_ids)
                sorted_hotels_count_for_city = OrderedDict(
                    sorted(hotels_count_for_city.items()))
                # hotel_counter = Hotel.objects.filter(status=Hotel.ENABLED).values('city_id').annotate(
                #     total=Count('city_id')).order_by('city_id')
                #hotel_count_by_city = {counter['city_id']: counter['total'] for counter in sorted_hotels_count_for_city}
                cities = sorted(
                    cities, key=lambda c: sorted_hotels_count_for_city.get(
                        c.id, 0), reverse=True)
                city_results = AutoCompleteResultBuilder.build_city_result(
                    cities, hotel_count_dict=sorted_hotels_count_for_city)
                city_results = [x for x in city_results if x['hotel_count'] > 0]
                return Response(city_results)

            auto_completion_services = AutoCompletionServiceFactory.get_autocompletion_services()
            results = []
            for service in auto_completion_services:
                results.extend(
                    service.apply(
                        search_string,
                        session_token=session_token,
                        current_results=results,
                        meta_keys=meta_key))

            results = AutoCompletionServiceFactory().sort_autocomplete_results(
                search_string=search_string, results=results)
            # Hit google search only when results from db are less than a particular number
            if len(results) < AUTOCOMPLETE_RESULTS_MAX_COUNT:
               results.extend(
                   GoogleAutoCompletionService().apply(
                       search_string,
                       session_token=session_token,
                       current_results=results,
                       meta_keys=meta_key))

            return Response(results)

        except ValidationError as e:
            return Response(status=e.status_code, data={"errors": e.detail})

        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={"errors": e.__str__()})
