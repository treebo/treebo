from rest_framework.response import Response

from django.db.models import Q

from apps.hotels.serializers.hotel_serializer import BasicHotelSerializer
from base.views.api import TreeboAPIView
from dbcommon.models.hotel import Hotel

class HotelList(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        hotel_id = kwargs.get('hotel_id')
        if hotel_id:
            hotels = Hotel.objects.filter(Q(pk=hotel_id) | Q(hotelogix_id=hotel_id) | Q(cs_id=hotel_id))
        else:
            hotels = Hotel.objects.all()
        serialized_data = BasicHotelSerializer(instance=hotels, many=True).data
        return Response(serialized_data)
