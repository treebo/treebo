var eventListLen,eventListIdx,treeboPageEvents,clientAnalyticsObj,
    $ = require('jquery'),
    l = require('./logger'),
    _ = require('underscore'),
    cookies = require('js-cookie');

module.exports = function(){
    $(document).ready(function(){

        clientAnalyticsObj = new ClientSideAnalytics();
        rt.clientAnalyticsObj = clientAnalyticsObj;
        analytics.page();

        try {
            treeboPageEvents=rt.analytics.config;
            if (! _.isEmpty(treeboPageEvents)) {
                    eventListLen=treeboPageEvents.events_list.length;
                    for (eventListIdx = 0; eventListIdx < eventListLen; eventListIdx++) {
                        bindAnalyticsEvents(eventListIdx);
                    }
                }
            }
            catch(err){
                l.log(err);
            }
        }
    )
    return clientAnalyticsObj;
}


function bindAnalyticsEvents(indexTreeboObj) {
            var generateVariables,eventArgs,idxIterator,jsElement,eventComputedVariables,iterLen ,jsElementVal,
             processElement = treeboPageEvents.events_list[indexTreeboObj] ,
             elementOverride = treeboPageEvents.element_override,
             currentElement;
            $(processElement['event_selector']).bind(processElement['event_action'], function () {
                    generateVariables = processElement['event_variables'].split(',');
                    eventArgs = {};
                    iterLen = generateVariables.length;
                    currentElement = $(this);
                    _.each(generateVariables,function(element){
                        jsElement = treeboPageEvents.available_elements[element];
                        try {
                            jsElementVal = eval(jsElement);
                        }
                        catch(err){
                            l.log(err);
                            jsElementVal = "";
                        }
                        elementOverride[element] === undefined ? eventArgs[element] = jsElementVal : eventArgs[elementOverride[element]] = jsElementVal
                    })

                    eventComputedVariables = processElement['event_computed_variables'];
                    if(! _.isEmpty(eventComputedVariables)){
                        _.map(eventComputedVariables,function(val,key){
                            jsElement = eventComputedVariables[key];
                            try {
                                jsElementVal = eval(jsElement);
                            }
                            catch(err){
                                l.log(err);
                                jsElementVal = "";
                            }
                            elementOverride[key] === undefined ? eventArgs[key] = jsElementVal : eventArgs[elementOverride[key]] = jsElementVal
                        })
                    }
                    eventArgs['event_category'] = processElement['event_category'];
                    eventArgs['event_source'] = processElement['event_source'];
                    clientAnalyticsObj.sendAnalyticsEvents(processElement['event_name'], eventArgs);
                }
            )
}

function ClientSideAnalytics(){
    this.eventName=this.treeboIdentifier="";
    this.eventArgs=this.userIdentifiedData={};
    this.currentUrl=this.getPageUrl();
    this.blackListedIps = ['106.51.37.208', '106.51.64.153', '111.93.154.1'];
    this.ignoreAnalyticsCheck();
    this.analyticsIdentifier();
    this.analyticsECommerce();
}

        ClientSideAnalytics.prototype.ignoreAnalyticsCheck = function(){
          const ignoreAnalytics = () => {
            const ignoreAnalyticsCookie = cookies.get('ignore_analytics');
            if (ignoreAnalyticsCookie === 'false') {
              return false;
            } else if(ignoreAnalyticsCookie === 'true' || this.blackListedIps.some((IP) => IP === rt.analytics.IP)) {
              return true;
            }
            return false;
          }

          if (ignoreAnalytics()) {
            const noop = () => {};
            mixpanel.register({ '$ignore': true });
            analytics.identify = noop;
            analytics.track = noop;
            analytics.page = noop;
            analytics.alias = noop;
            console.log('ignoring analytics');
          }
        }

        ClientSideAnalytics.prototype.analyticsECommerce = function(){
            var pageUrl = this.currentUrl.split("#"),pageHotelName,pageCategory;
            if(pageUrl[0] == "city"){
                //this.sendAnalyticsEvents("Viewed Product Category" , {"category" : $('.city-bannerOverlay>div>h1>span').text() } );
            }
            else if (pageUrl[0] == "hotels" && ["order","confirmation","traveller"].indexOf(pageUrl[1]) == -1 ){
                pageHotelName = $('.name').text().split(',')[0].trim();
                pageCategory = $('.locality').text().split(',')
                pageCategory = pageCategory[pageCategory.length -1 ].trim().split(" ")[0]
                /*this.sendAnalyticsEvents('Viewed Product', {
                    "id": pageHotelName,
                    "sku": pageHotelName,
                    "name": pageHotelName,
                    "price": 0,
                    "category": pageCategory
                }); */
            }
        }

        // Object to pass events to Analytics provider SEGMENT
        ClientSideAnalytics.prototype.setEventData = function( eventName , eventArgs ) {
          this.eventName = eventName;
          this.eventArgs = eventArgs;
        }

        ClientSideAnalytics.prototype.sendEventToSegmentIO = function (){
            this.eventArgs['user_login_state'] = rt.loggedin ?  'LOGGEDIN' : 'LOGGEDOUT';
            this.eventArgs['user_signup_channel'] = cookies.get('user_signup_channel') ? cookies.get('user_signup_channel') : "";
            this.eventArgs['user_signup_date'] = cookies.get('user_signup_date') ? cookies.get('user_signup_date') : "";
            this.eventArgs['is_mobile'] = rt.analytics['ISMOBILE'];
            this.eventArgs['client_ip'] = rt.analytics['IP'];
            this.eventArgs['timestamp'] = rt.analytics['TIMESTAMP'];
            this.eventArgs['event_date'] = rt.analytics['event_date'];
            this.eventArgs['event_time'] = rt.analytics['event_time'];

            analytics.track(this.eventName, this.eventArgs,{
                      integrations: {
                        'All': true,
                        'Mixpanel': true,
                        'Google Analytics': true
                      }
            });
        }

        ClientSideAnalytics.prototype.analyticsIdentifier = function(){
            var isTreeboUser = _.isFinite(parseInt(rt.userId,10));

            if(isTreeboUser && rt.loggedin){
                this.setUserIdentifiedData({
                    "email": rt.user.email,
                    "firstName": rt.user.firstName,
                    "phone": rt.user.phone,
                    "userid":rt.userId,
                    "signup_date":cookies.get('user_signup_date'),
                    "signup_channel":cookies.get('user_signup_channel')
                });
                this.identifySession(rt.userId);
            }
        }

        ClientSideAnalytics.prototype.setPeopleProperty = function(key,value){
            mixpanel.people.set(key,value);
        }

        ClientSideAnalytics.prototype.increment = function(key,incrementBy){
            mixpanel.people.increment(key,incrementBy || 1);
        }

        ClientSideAnalytics.prototype.trackCharge = function(revenue){
            mixpanel.people.track_charge(revenue,{
                '$time':new Date()
            });
        }

        ClientSideAnalytics.prototype.union = function(key,value){
            mixpanel.people.union(key,value);
        }

        ClientSideAnalytics.prototype.setPeopleEvents = function (){

        }
        ClientSideAnalytics.prototype.alias = function (userId){
            analytics.alias(userId);
        }

        ClientSideAnalytics.prototype.aliasSession = function (newIdentifier){
            analytics.alias(newIdentifier);
        }

        ClientSideAnalytics.prototype.identifySession = function(userIdentifier){
            analytics.identify(userIdentifier,this.userIdentifiedData);
        }

        ClientSideAnalytics.prototype.setUserIdentifiedData = function (userIdentifiedData){
            this.userIdentifiedData = userIdentifiedData;
        }

        ClientSideAnalytics.prototype.getPageUrl =  function(){
                            var urlLocation = window.location.pathname.replace(/^\//,'').replace(/\/$/,'').replace('/','#');
                            return urlLocation == "" ? "home" : urlLocation;
        }

        ClientSideAnalytics.prototype.getElementName= function(element){
            try {
                return element.attr('name') ? element.attr('name') : (element.attr('id') ? element.attr('id') : element.attr('class'));
            }
            catch(err){
                l.log(err);
                return "default" ;
            }
        }

        ClientSideAnalytics.prototype.addCurrentUrlToEvent =function(){
                this.eventArgs.page_url = this.currentUrl;
        }

        ClientSideAnalytics.prototype.sendAnalyticsEvents = function(eventName , eventArgs ){
                this.setEventData(eventName , eventArgs );
                this.addCurrentUrlToEvent();
                this.sendEventToSegmentIO();
        }

function get_element_name(element){
    try{
        return element.attr('name') ? element.attr('name') : (element.attr('id') ? element.attr('id') : element.attr('class'));
    }
    catch(err){
        l.log(err);
        return "default";
    }
}
