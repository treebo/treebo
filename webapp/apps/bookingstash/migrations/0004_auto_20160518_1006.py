# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookingstash', '0003_availability'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='availability',
            options={
                'verbose_name': 'Availability',
                'verbose_name_plural': 'Availabilities'},
        ),
        migrations.AlterModelOptions(
            name='hotelroommapper',
            options={
                'verbose_name': 'Hotel Room Mapper',
                'verbose_name_plural': 'Hotel Room Mappers'},
        ),
        migrations.AlterModelOptions(
            name='reservationbooking',
            options={
                'verbose_name': 'Reservation Booking',
                'verbose_name_plural': 'Reservation Bookings'},
        ),
        migrations.AlterModelOptions(
            name='reservationbookingstay',
            options={
                'verbose_name': 'Reservation Booking Stay',
                'verbose_name_plural': 'Reservation Booking Stays'},
        ),
    ]
