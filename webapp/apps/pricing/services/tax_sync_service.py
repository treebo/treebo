import logging

from apps.pricing.models import StateTax, LuxuryTax
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.location import State
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class TaxSyncService(object):
    def get_state_wise_taxes(cls, state_id):
        state = State.objects.get(pk=state_id)
        state_taxes = StateTax.objects.filter(state_id=state.id)
        luxury_taxes = LuxuryTax.objects.filter(
            state_id=state.id, hotel_id__isnull=True)
        logger.info(
            "state taxes - %s state wise luxury taxes %s",
            state_taxes,
            luxury_taxes)
        return state_taxes, luxury_taxes

    def get_hotel_wise_taxes(cls, hotel_id):
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotel = hotel_repository.get_hotel_by_id_from_web(hotel_id)
        state = hotel.state
        luxury_taxes = LuxuryTax.objects.filter(
            state_id=state.id, hotel_id=hotel_id)
        logger.info("state %s hotel wise luxury taxes %s", state, luxury_taxes)
        return state, luxury_taxes
