import logging
import sys

import pdfkit
from django.conf import settings
# TODO: This has changed in python3
from django.core.urlresolvers import reverse

from apps.checkout.service.notification.bookingnotification import BookingNotificationService
from apps.common import date_time_utils
from apps.common import metrics_logger
from apps.common.slack_alert import SlackAlertService
from apps.common.utils import trackAnalyticsServerEvent, validate_gstin
from data_services.respositories_factory import RepositoriesFactory

from webapp.apps.pages.print_pdf_helper import PdfDataHelper

logger = logging.getLogger(__name__)


class BookingHelper:
    @staticmethod
    def sendEmail(booking_request, newEmail=None):
        """
        Send Email
        :param apps.checkout.models.BookingRequest booking_request:
        :return:
        """
        if settings.CONFIRMATION_MAIL:
            email = booking_request.email if not newEmail else newEmail
            name = booking_request.name
            pdf_name = BookingHelper.generatePdfName(
                name, booking_request.order_id)
            error = None
            try:
                BookingHelper.__getConfirmationPdf(booking_request, pdf_name)
            except Exception as exc:
                logger.exception(
                    "Error occurred in BookingHelper#getConfirmationPdf for email %s(%s) ",
                    email,
                    booking_request.order_id)
                return
            BookingNotificationService.bookingConfirmationMail(
                booking_request, email, pdf_name, error)
            logger.info(
                "Email successfully sent to %s(%s)" %
                (email, booking_request.order_id))

    @staticmethod
    def __getConfirmationPdf(booking_request, pdf_name):
        """
        :param apps.checkout.models.BookingRequest booking_request:
        :param pdf_name:
        :return:
        """
        if settings.ENVIRONMENT is "local":
            config = pdfkit.configuration(
                wkhtmltopdf=settings.WK_HTML_TO_PDF_PATH)
        else:
            config = None
        try:
            pdfkit.from_string(
                PdfDataHelper().getData(booking_request.order_id),
                "webapp/static/files/" +
                pdf_name,
                configuration=config)
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            logger.exception(
                "Unable to generate pdf for order id " + str(booking_request.order_id))

    @staticmethod
    def constructItineraryUrlOnError(booking_request, error=None):
        return BookingHelper.build_itinerary_page_url_with_error(
            booking_request, error)

    @staticmethod
    def track_booking_failure(reason, booking_request, request):
        """

        :param reason:
        :param apps.checkout.models.BookingRequest booking_request:
        :param request:
        """
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotel = hotel_repository.get_hotel_by_id_from_web(booking_request.hotel_id)
        #hotel = Hotel.objects.get(pk=booking_request.hotel_id)
        analytics_data = {
            'hotel_id': hotel.id, 'hotel_name': hotel.name,
            'checkin': booking_request.checkin_date,
            'checkout': booking_request.checkout_date,
            'num_nights': (booking_request.checkout_date - booking_request.checkin_date).days,
            'adults': booking_request.adults,
            'kids': booking_request.children,
            'rooms': len(booking_request.room_config.split(",")),
            'room_type': booking_request.room_type,
            'total_price': booking_request.total_amount,
            'discount_coupon_applied': booking_request.coupon_code,
            'discount_value': booking_request.discount_value,
            'user_login_state': 'LOGGEDIN' if request.user and request.user.is_authenticated() else 'LOGGEDOUT',
            'user_signup_channel': request.COOKIES.get('user_signup_channel'),
            'user_signup_date': request.COOKIES.get('user_signup_channel'),
            'special_prefs': booking_request.comments}
        trackAnalyticsServerEvent(
            request,
            reason,
            analytics_data,
            channel=booking_request.booking_channel)
        metrics_logger.log(reason, analytics_data)

    @staticmethod
    def generatePdfName(guestName, order_id=None):
        """
        Generate the name of the pdf which will be saved for the booking.
        :param guestName: Name of the guest
        :param order_id
        :return:
        """
        if guestName and len(guestName.split()) > 0:
            return guestName.split()[0] + str(order_id) + ".pdf"
        else:
            return 'guest' + str(order_id) + ".pdf"

    @staticmethod
    def build_itinerary_page_url_from_params(
            hotel_id,
            room_config,
            room_type,
            check_in_str,
            check_out_str,
            coupon_code="",
            bid=""):
        itinerary_page_url = reverse(
            "pages:itinerary") + "?hotel_id={0}&roomconfig={1}&roomtype={2}&checkin={3}&checkout={4}".format(
            hotel_id, room_config, room_type, check_in_str, check_out_str)
        if coupon_code and len(coupon_code.strip()) > 0:
            itinerary_page_url += "&couponcode=" + coupon_code
        if bid and bid > 0:
            itinerary_page_url += "&bid=" + str(bid)
        return itinerary_page_url

    @staticmethod
    def build_itinerary_page_url(quickbook_data, booking_request):
        itinerary_page_url = BookingHelper.build_itinerary_page_url_from_params(
            quickbook_data.hotel_id,
            quickbook_data.room_config,
            quickbook_data.room_type,
            quickbook_data.checkin_date_string,
            quickbook_data.checkout_date_string,
            quickbook_data.coupon_code,
            booking_request.id if booking_request else None)
        logger.debug(
            "Redirecting to Itinerary Page url: %s",
            itinerary_page_url)
        return itinerary_page_url

    @staticmethod
    def build_itinerary_page_url_with_error(booking_request, error):
        from apps.hotels import utils
        from apps.pricing.dateutils import date_to_ymd_str
        hashed_hotel_id = utils.generate_hash(booking_request.hotel_id)
        itinerary_page_url = BookingHelper.build_itinerary_page_url_from_params(
            hashed_hotel_id,
            booking_request.room_config,
            booking_request.room_type,
            date_to_ymd_str(
                booking_request.checkin_date),
            date_to_ymd_str(
                booking_request.checkout_date),
            booking_request.coupon_code,
            booking_request.id)
        itinerary_page_url += "&error=" + error
        logger.debug(
            "Redirecting to Itinerary Page url: %s",
            itinerary_page_url)
        return itinerary_page_url

    @staticmethod
    def build_fot_confirmation_page(booking_request, hotel, error=None):
        url = "{0}?locality={1}&lat={2}&long={3}&query={4}&checkin={5}&checkout={6}&roomconfig={7}".format(
            reverse('pages:SearchAudit'),
            hotel.city.name, hotel.latitude, hotel.longitude, hotel.city.name,
            (booking_request.checkin_date).strftime(
                date_time_utils.DATE_FORMAT),
            (booking_request.checkout_date).strftime(
                date_time_utils.DATE_FORMAT), booking_request.room_config)
        if error is not None:
            url += "&error=" + error
        else:
            url += "&confirm=true"
        return url

    @staticmethod
    def validate_gstin_for_booking(organization_taxcode, bid='', api_call=''):
        if organization_taxcode and not validate_gstin(organization_taxcode):
            logger.error(
                'invalid gstin number for bookign %s and gst %s during %s',
                bid,
                organization_taxcode,
                api_call)
            SlackAlertService.publish_alert(
                'invalid gstin number for booking `%s` and gst `%s` during `%s` ' %
                (bid, organization_taxcode, api_call), channel=SlackAlertService.AUTH_ALERTS)
            return False
        return True
