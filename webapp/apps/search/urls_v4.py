from django.conf.urls import url

from apps.search.api.v4.search_api import SearchApiV4

urlpatterns = [
    url(r'', SearchApiV4.as_view(), name='search_api_v4'),
]
