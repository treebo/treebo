# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
from apps.common.data_classes.base_data_class import BaseDataClass


class PaymentType(BaseDataClass):

    def __init__(self, uid, name, short_name, description, is_credit_type=False):
        self.uid = str(uid)
        self.name = name
        self.short_name = short_name
        self.description = description
        self.is_credit_type = is_credit_type

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return hash(self) == hash(other)
        return NotImplemented

    def __hash__(self):
        return hash(tuple([self.uid, self.name]))

    def __repr__(self):
        return '<{kls} ({u}-{n} category:{c})>'.format(
            kls=self.__class__.__name__,
            u=self.uid,
            n=self.name,
        )

    def __str__(self):
        return self.name


PayAtHotel = PaymentType(
    uid=1,
    name='PayAtHotel',
    short_name='Pay at Hotel',
    description='The guest has to pay the balance amount at checkout'
)

WebPrepaid = PaymentType(
    uid=2,
    name='Web Prepaid',
    short_name='Prepaid',
    description='The guest need not pay for things that have been booked',
    is_credit_type=True,
)


class PaymentTypesContainer(object):
    items = [
        PayAtHotel,
        WebPrepaid
    ]

    def from_name(self, name):
        try:
            return [item for item in self.items if str(item.name).lower() == str(name).lower()][0]
        except IndexError:
            raise ValueError("Invalid Payment Type Name: {n}".format(n=name))

    def from_uid(self, uid):
        try:
            return [item for item in self.items if str(item.uid) == str(uid)][0]
        except IndexError:
            raise ValueError("Invalid Payment Type uid: {n}".format(n=uid))

    def __iter__(self):
        return iter(self.items)


PaymentTypes = PaymentTypesContainer()
#
# CorpPaymentTypes = [pt for pt in PaymentTypes if pt.category == CorpCategory]
# TaPaymentTypes = [pt for pt in PaymentTypes if pt.category == TaCategory]
