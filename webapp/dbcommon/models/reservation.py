# from django.db import models
# from djutil.models import TimeStampedModel
#
# from profile import User
# from common.constants import common_constants as const
# from hotel import Hotel
# from room import Room
#
#
# class Booking(TimeStampedModel):
# 	order_id = models.CharField(max_length=200)
# 	checkin_date = models.DateField()
# 	checkout_date = models.DateField()
# 	adult_count = models.PositiveIntegerField()
# 	child_count = models.PositiveIntegerField()
# 	hotel = models.ForeignKey(Hotel, blank=True, null=True)
# 	room = models.ForeignKey(Room, blank=True, null=True)
# 	total_amount = models.DecimalField( max_digits=20, decimal_places=10)
# 	user_id = models.ForeignKey(User, blank=True, null=True)
# 	guest_name = models.CharField(max_length=200)
# 	guest_email = models.EmailField(max_length=200)
# 	guest_mobile = models.CharField(max_length=20)
# 	is_complete = models.BooleanField(default=False)
# 	payment_amount = models.DecimalField(max_digits=20, decimal_places=10, default=0)
# 	is_traveller = models.BooleanField(default=False)
# 	booking_code = models.CharField(max_length=200, null=True, blank=True)
# 	comments = models.TextField(default="")
# 	coupon_apply = models.BooleanField(default=False)
# 	coupon_code = models.CharField(max_length=200, null=True, blank=True)
# 	payment_mode = models.CharField(max_length=200, null=True, blank=True, default="Not Paid")
# 	discount = models.DecimalField(max_digits=20, decimal_places=10, default=0)
# 	group_code = models.CharField(max_length=200, null=True, blank=True, default='')
# 	CONFIRM = const.BOOKING_CONFIRM
# 	CANCEL = const.BOOKING_CANCEL
# 	BOOKING_STATUS = const.BOOKING_STATUS
# 	status = models.IntegerField(default= CONFIRM, choices=BOOKING_STATUS)
#
# 	class Meta:
# 		db_table = 'hotels_booking'
#
# 	def __str__(self):              # __unicode__ on Python 2
# 		return self.hotel.name + " " + self.guest_name + " " +self.guest_mobile + " " + str(self.checkin_date) + " " + str(self.checkout_date) + "  status = " + str(self.status)
#
#
#
# class RoomBooking(TimeStampedModel):
# 	room = models.ForeignKey(Room, blank=True, null=True)
# 	Booking =  models.ForeignKey(Booking, blank=True, null=True)
# 	reservation_id = models.CharField(max_length=200)
# 	price = models.DecimalField(max_digits=8, decimal_places=2, null= True, blank= True)
#
# 	class Meta:
# 		db_table = 'hotels_roombooking'
#
# 	def __str__(self):              # __unicode__ on Python 2
# 		return self.room.room_type + " " + self.Booking.group_code + " " + self.reservation_id
#
#
# class Reservation(TimeStampedModel):
# 	reservation_id = models.CharField(max_length=200)
# 	reservation_status = models.CharField(max_length=200)
# 	reservation_checkin_date = models.DateField()
# 	reservation_checkout_date = models.DateField()
# 	adult_count = models.PositiveIntegerField(default=0)
# 	child_count = models.PositiveIntegerField(default=0)
# 	room_unit = models.PositiveIntegerField()
# 	total_amount = models.DecimalField(max_digits=20, decimal_places=10, default=0)
# 	hotel = models.ForeignKey(Hotel, blank=True, null=True)
# 	room = models.ForeignKey(Room, blank=True, null=True)
# 	room_block_code = models.CharField(max_length=100, null=True, blank=True, default='')
# 	guest_name = models.CharField(max_length=200, null=True, blank=True, default='')
# 	guest_email = models.EmailField(max_length=200, null=True, blank=True, default='')
# 	guest_mobile = models.CharField(max_length=200, null=True, blank=True, default='')
# 	reservation_create_time = models.DateTimeField()
#
# 	class Meta:
# 		db_table = 'hotels_reservation'
#
# 	def __str__(self):  # __unicode__ on Python 2
# 		if self.hotel:
# 			hotel_name = self.hotel.name
# 		else:
# 			hotel_name = "no hotel"
#
# 		if self.room:
# 			room_type = self.room.room_type
# 		else:
# 			room_type = "no room"
#
# 		return str(self.reservation_id) + " " + hotel_name + " " + room_type + " " + str(
# 			self.reservation_checkin_date) + " " + str(self.reservation_checkout_date) + " " + str(
# 			self.reservation_create_time)
