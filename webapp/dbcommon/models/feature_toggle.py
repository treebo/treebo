from django.db import models
from djutil.models import TimeStampedModel

from dbcommon.models.default_permissions import DefaultPermissions


class FeatureToggle(TimeStampedModel, DefaultPermissions):
    namespace = models.CharField(max_length=100, null=False)
    feature = models.CharField(max_length=200, null=False)
    environment = models.CharField(
        max_length=200, null=False, default='preprod')
    is_enabled = models.BooleanField(default=False)

    class Meta(DefaultPermissions.Meta):
        db_table = 'dbcommon_feature_toggle'
        verbose_name = 'Feature Toggle'
        verbose_name_plural = 'Feature Toggles'

    def __unicode__(self):
        return '%s %s %s' % (self.namespace, self.feature, self.is_enabled)

    def __str__(self):
        return '%s %s %s' % (self.namespace, self.feature, self.is_enabled)