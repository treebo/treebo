import logging

from django.contrib.auth.models import AnonymousUser
from django.utils import timezone

from apps.auth.dto.user_register_dto import UserRegisterDTO
from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.profiles.service.user_registration import UserRegistration
from base.token_helper import TokenHelper
from dbcommon.models.profile import User

logger = logging.getLogger(__name__)


class UserService(object):
    user_registration = UserRegistration()
    token_helper = TokenHelper()
    auth_client = TreeboAuthClient()

    def validate_credentials(self, email, password):
        logger.info(" validation cred for email %s ", email)
        user, clean_email, phone = User.objects.get_existing_user(
            email, None, None)
        token_info = self.auth_client.login(clean_email, password)
        logger.info(
            " user login success for email %s with token info %s ",
            email,
            token_info)
        user, user_data = self.user_registration.register_user_with_token(
            token_info['access_token'])
        self.token_helper.save_access_token(token_info, user)
        return user, token_info['access_token'], user_data

    def login_user(self, request, user):
        logger.info(" login user %s ", user)
        click_id, referral_code = self.user_registration.fetch_referral_details_from_cookie(
            request)
        self.user_registration.create_referral(user, referral_code, click_id)
        user.last_login = timezone.now()
        user.is_active = True
        user.save(update_fields=['last_login', 'is_active'])
        request.user = user
        return user

    def register_user(self, user_simple_register_dto):
        name = user_simple_register_dto['name']
        name_arr = name.split(" ", 2)
        first_name = name_arr[0].strip()
        last_name = name_arr[1].strip() if len(name_arr) is 2 else ""
        user_register_dto = UserRegisterDTO(data={
            'first_name': first_name,
            'last_name': last_name,
            'email': user_simple_register_dto['email'],
            'password': user_simple_register_dto['password'],
            'phone_number': user_simple_register_dto['phone_number']
        })
        user_register_dto.is_valid()
        logger.info(
            " register user %s , phone %s ",
            user_simple_register_dto['email'],
            user_simple_register_dto['phone_number'])
        token_info = self.auth_client.register(user_register_dto.data)
        logger.info(
            " user register success %s ",
            user_simple_register_dto['email'])
        user, user_details = self.user_registration.register_user_with_token(
            token_info['access_token'])
        self.token_helper.save_access_token(token_info, user)
        return user, token_info['access_token']

    def validate_token(self, token):
        logger.info(" validating token %s ", token)
        user_details = self.auth_client.validate_token(token)
        logger.info(" valid token %s for user %s  ", token, user_details)
        user, email, phone = User.objects.get_existing_user(
            user_details['email'], user_details['phone_number'], user_details['id'])
        logger.info(" got user %s ", user)
        if not user:
            user, user_details = self.user_registration.register_user_with_token(token)
        return user

    def logout_by_token(self, request, token):
        logger.info("logout_by_token %s  ", token)
        self.auth_client.logout(token)
        logger.info("logout_by_token successfull %s  ", token)
        request.user = AnonymousUser()
        self.token_helper.revoke_single_token(token)
