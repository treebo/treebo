import logging

from apps.reward.providers.loyalty_user import LoyaltyUser

logger = logging.getLogger(__name__)


class LoyaltyService:
    def get_loyalty_type(self, phone_number):
        try:
            loyaltyuser = LoyaltyUser()
            user_data = loyaltyuser.get_user_data(phone_number)
            loyalty_type = user_data["loyalty_type"]
        except:
            loyalty_type = None
        return loyalty_type
