import errno
import logging
from socket import socket

from django.conf import settings
from django.core.mail.message import EmailMessage
from django.template import loader
from django.template.context import Context
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from apps.profiles import utils as profile_utils
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import common_constants

logger = logging.getLogger(__name__)


class Feedback(TreeboAPIView):
    template_name = 'mobile/feedback/index.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(Feedback, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request):
        context = {'scripts': ['js/feedback.js'], 'footer': False}
        if request.user and request.user.is_active and request.user.is_authenticated():
            context['name'] = request.user.get_full_name()
            context['email'] = request.user.email
        return Response(context)

    @log_args(logger)
    def post(self, request):
        data = request.data
        device = data.get('device') if "device" in data else "Mobile"
        email = data.get('email')
        name = data.get('name')
        comments = data.get('comments')
        referrer = data.get('referrer')

        feedbackTemplate = loader.get_template('profiles/feedback_mail.html')
        data = Context({"name": name,
                        "email": email,
                        "comments": comments,
                        'referrer': referrer,
                        'device': device})
        content = feedbackTemplate.render(data)
        try:
            profile_utils.saveFeedback(
                email, [settings.FEEDBACK_RECEIVER], comments, referrer, device)
        except Exception as exc:
            logger.error("Error saving feedback from " + str(email))
        context = {}
        try:
            subject = common_constants.FEEDBACK_SUBJECT
            email = EmailMessage(subject, content, settings.FEEDBACK_SENDER,
                                 [settings.FEEDBACK_RECEIVER])
            email.content_subtype = common_constants.EMAIL_FORMAT
            email.send()
        except socket.error as v:
            logger.error("Error sending feedback email to " + email)
            errorCode = v[0]
            if errorCode == errno.ECONNREFUSED:
                return Response(context, status=500)
        return Response(context)
