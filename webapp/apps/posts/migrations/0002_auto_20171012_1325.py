# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0068_auto_20170927_1040'),
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer', name='related_question', ), migrations.AddField(
            model_name='answer', name='related_question_id', field=models.IntegerField(
                default=1), preserve_default=False, ), migrations.AddField(
                    model_name='qamapping', name='city', field=models.ForeignKey(
                        blank=True, to='dbcommon.City', null=True, on_delete=models.DO_NOTHING), ), ]
