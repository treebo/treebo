from typing import List

from apps.search.services.auto_complete_result_builder import SEARCH_RESULT_TYPE
from apps.search.services.hotel_name_auto_completion_service import HotelBasedAutoCompletionService
from apps.search.services.landmark_auto_completion_services import \
    LandmarkBasedAutoCompletionService
from apps.search.services.locality_auto_completion_service import LocalityBasedAutoCompletionService
from fuzzywuzzy import process


class AutoCompletionServiceFactory(object):
    @staticmethod
    def get_autocompletion_services():
        """
        The factory that returns the AutoCompletionServices to be applied in the specified order.
        This will decide on which order the auto_completion will happen, based on analytics service, or any other criteria
        """
        services = [LocalityBasedAutoCompletionService(),
                    HotelBasedAutoCompletionService(),
                    LandmarkBasedAutoCompletionService()]
        return services

    def _get_positional_search_result(self, sorted_results, search_strings, result=None):
        # Note: The sorting is based on the most relevant match by position of letters
        # Sorted result is mix of data of search query after split based on space
        # Ex Search String : MG Road, Sorted result is having both data
        result_to_be_considered_for_sorting = []
        result_to_be_forwarded_for_sorting = []
        result = [] if not result else result
        if not (sorted_results and search_strings):
            return result

        search_key = ' '.join(search_strings)
        for sorted_result in sorted_results:
            if search_key in sorted_result['label'].lower():
                result_to_be_considered_for_sorting.append(sorted_result)
            else:
                result_to_be_forwarded_for_sorting.append(sorted_result)
        result_to_be_considered_for_sorting.sort(
            key=lambda search_result: (search_result['label'].lower()).index(
                search_key.lower()))
        result.extend(result_to_be_considered_for_sorting)
        result = self._get_positional_search_result(result_to_be_forwarded_for_sorting,
                                                    search_strings[:len(search_strings)-1], result)
        return result

    def _get_prioritize_search_results(self, sorted_results, search_string,
                                       result_limit=None) -> List:
        # NOTE: Currently the result is ordered by city, hotel, locality, landmark
        # and others in search results
        # Ex.https://www.treebo.com/api/v1/search/autocomplete/?search_string={search_string}
        sorted_results = self._get_positional_search_result(sorted_results,
                                                            search_string.split(" "))
        city_sorted_result, hotel_sorted_result, locality_sorted_result, landmark_sorted_result, others = [], [], [], [], []

        for sorted_res in sorted_results:
            if sorted_res['type'] == SEARCH_RESULT_TYPE['CITY']:
                city_sorted_result.append(sorted_res)
            elif sorted_res['type'] == SEARCH_RESULT_TYPE['LANDMARK']:
                landmark_sorted_result.append(sorted_res)
            elif sorted_res['type'] == SEARCH_RESULT_TYPE['LOCALITY']:
                locality_sorted_result.append(sorted_res)
            elif sorted_res['type'] == SEARCH_RESULT_TYPE['HOTEL']:
                hotel_sorted_result.append(sorted_res)
            else:
                others.append(sorted_res)

        result = city_sorted_result + hotel_sorted_result + locality_sorted_result + landmark_sorted_result + others
        # returning just top 10 result
        return result[:result_limit]

    def sort_autocomplete_results(self, search_string, results):
        label_result_dict = {result['label']: result for result in results}
        if not label_result_dict:
            return []
        # sorted_labels = process.extract(search_string, label_result_dict.keys(), limit=10)
        # sorted_results = [label_result_dict[label[0]] for label in sorted_labels if label]
        return self._get_prioritize_search_results(list(label_result_dict.values()), search_string,
                                                   result_limit=10)
