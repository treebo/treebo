import logging
from datetime import datetime
from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.bookings.integrations.booking.treebo import TreeboCRSWrapper
from apps.profiles.dto.update_profile_dto import UpdateProfileDTO
from webapp.apps.profiles.service.user_registration import UserRegistration
from django.db.models import Q
from apps.bookings.models import Booking
from apps.profiles import utils as profile_utils
from dbcommon.models.profile import User, UserEmail


logger = logging.getLogger(__name__)

__author__ = 'varunachar'


class UserProfileService():
    treebo_auth_client = TreeboAuthClient()

    BOOKING_START_DATE = '2016-01-01 12:00:00'

    @staticmethod
    def get_all_bookings(user):
        """
        Finds all the bookings of the a user

        :type user: UserId
        :rtype : set of Booking
        :param user:
        """
        bookings = TreeboCRSWrapper.fetch_bookings_using_phone_or_email(phone_number=user.phone_number,
                                                                        email=user.email)
        return bookings

    @staticmethod
    def register_user(guest):
        """
        Register guest to web and return
        :param guest:
        :return:
        """
        logger.info("Register user to web called for guest %s", guest)
        user_registration = UserRegistration()
        user_dto = {
            'email': guest['email'],
            "first_name": guest['first_name'],
            "last_name": guest['last_name'],
            "phone_number": guest['phone'],
            "password": profile_utils.generateRandomPassword(),
        }
        user = user_registration.register_internally(user_dto)
        return user

    def get_all_users(self, start_with_userid):
        users = User.objects.filter(id__lte=start_with_userid).exclude(
            Q(email__icontains='booking') | Q(email__icontains='treebohotels')).order_by('-created_at')
        return users

    def get_all_internal_users(self):
        users = User.objects.filter(
            email__icontains='treebohotels').order_by('-created_at')
        return users

    def get_user_by_id(self, user_id):
        return User.objects.filter(id=user_id).first()

    def update_profile(
            self,
            user,
            first_name,
            last_name,
            gender,
            dob,
            anniversary,
            email,
            phone,
            city,
            work_email):
        self.update_profile_in_auth(
            user, first_name, last_name, email, phone, gender)
        self.update_profile_local(
            user,
            first_name,
            last_name,
            gender,
            dob,
            anniversary,
            email,
            phone,
            city,
            work_email)

    def update_profile_local(
            self,
            user,
            first_name,
            last_name,
            gender,
            dob,
            anniversary,
            email,
            phone,
            city,
            work_email):
        user.first_name = first_name
        user.last_name = last_name
        user.gender = gender
        user.dob = dob
        user.email = email
        user.phone_number = phone
        user.city = city
        user.anniversary_date = anniversary
        user.save()
        userEmail, created = UserEmail.objects.get_or_create(user=user)
        userEmail.email = work_email
        userEmail.email_type = "W"
        userEmail.save()

    def update_profile_in_auth(
            self,
            user,
            first_name,
            last_name,
            email,
            phone,
            gender):
        update_profile_dto = UpdateProfileDTO(data={
            "first_name": first_name,
            "phone_number": phone,
            "email": email,
            "last_name": last_name,
            "gender": gender,
            "auth_id": user.auth_id
        })
        update_profile_dto.is_valid()
        self.treebo_auth_client.update_profile(update_profile_dto.data)

    def is_staff(self, user):
        if user and user.is_authenticated() and user.is_staff:
            return True
        return False
