# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0063_city_seo_image'),
        ('dbcommon', '0066_auto_20170921_0957'),
        ('dbcommon', '0065_auto_20170915_0543'),
    ]

    operations = [
    ]
