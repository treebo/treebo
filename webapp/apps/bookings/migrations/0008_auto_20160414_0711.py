# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0007_auto_20160413_1355'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='checkin_time',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='checkout_time',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
