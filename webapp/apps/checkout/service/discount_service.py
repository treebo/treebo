from apps.pricing.dateutils import date_to_ymd_str
from services.restclient.pricingrestclient import PricingClient


class DiscountService:

    def get_from_local(self, request, booking_request, coupon_code):
        prices = PricingClient.getDiscountPricingDetails(
            request,
            date_to_ymd_str(
                booking_request.checkin_date),
            date_to_ymd_str(
                booking_request.checkout_date),
            coupon_code,
            booking_request.hotel_id,
            booking_request.room_config,
            booking_request.room_type)
        return prices
