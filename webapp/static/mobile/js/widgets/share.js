import $ from 'jquery';
import React from 'react';
import 'parsleyjs';
import cookies from 'js-cookie';
import queryString from 'query-string';
import treejax from '../treebolibs/treejax';
import '../../css/sharewidget.less';
import '../auth';

class EmailInvite extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			emails: '',
		};
	}

	componentDidMount() {
		$('#emailsForm').parsley().on('form:submit', (e) => {
			e.submitEvent.preventDefault();
			this.props.onSendEmails(this.state.emails);
			return false;
		});
	}

	handleFieldChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	}

	render() {
		return (
			<div className="email-invite">
				<div className="modal pos-abs">
					<div className="modal__body">
						<i className="icon-cross" onClick={() => this.props.onShowEmailInvite(false)}></i>
						<p className="email-invite__title">Invite through Email</p>
						<p className="email-invite__subtitle">Please enter the email addresses separated by commas</p>
						<form id="emailsForm">
							<div className="float-labels">
								<input
									className="float-labels__input" onChange={this.handleFieldChange} name="emails"
									type="text" value={this.state.emails || ''} placeholder="ravi@gmail.com, gita@yahoo.com"
									pattern="^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},?\s*)+$" data-parsley-error-message="Please enter valid email address(es)"
									required="true" data-parsly-trigger="change" data-parsley-required="true" data-parsley-required-message="Please enter the list of emails here" />
								<label className="float-labels__label">Email List</label>
							</div>
							<button type="submit" className="btn email-invite__submit">Send</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

class Share extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showEmailInvite: false,
		};
	}

	onShowEmailInvite = (status) => {
		this.setState({
			showEmailInvite: status,
		});
	}

	onSendEmails = (emails) => {
		treejax({
			url: '/api/v1/referral/share/',
			type: 'POST',
			data: {
				emails,
				csrfmiddlewaretoken: cookies.get('csrftoken'),
			},
		})
		.then(() => this.onShowEmailInvite(false));
	}

	onFBShare = () => {
		FB.ui({
			method: 'share',
			mobile_iframe: true,
			href: this.props.shareUrl,
			quote: this.props.shareSocial.fb_share_msg,
		}, (res) => {});
	}

	render() {
		const whatsappText = queryString.stringify({ text: `${this.props.shareSocial.whatsapp_share_msg} ${this.props.shareUrl}` });
		return (
			<div className="referrals-share">
				<button className="btn btn--facebook" onClick={this.onFBShare}>FACEBOOK</button>
				<a className="btn btn--whatsapp" href={`whatsapp://send?${whatsappText}`}>WHATSAPP</a>
				<button className="btn btn--google" onClick={() => this.onShowEmailInvite(true)}>EMAIL</button>
				{this.state.showEmailInvite ? <EmailInvite onSendEmails={this.onSendEmails} onShowEmailInvite={this.onShowEmailInvite} /> : null}
			</div>
		);
	}
}

export default Share;
