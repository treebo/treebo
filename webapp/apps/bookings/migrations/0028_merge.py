# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0026_auto_20160804_1800'),
        ('bookings', '0027_merge'),
        ('bookings', '0026_bookingrequest_voucher_amount'),
    ]

    operations = [
    ]
