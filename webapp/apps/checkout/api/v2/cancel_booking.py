# import logging
#
# from rest_framework.status import HTTP_404_NOT_FOUND
#
# from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
# from apps.auth.dto.change_password_dto import ChangePasswordTokenDTO
# from apps.auth.services.password_service import PasswordService
# from apps.bookings.models import Booking
# from apps.checkout.dto.cancel_booking_dto import CancelBookingDTO
# from apps.common.api_response import APIResponse as api_response
# from base import log_args
# from base.renderers import TreeboCustomJSONRenderer
# from base.views.api import TreeboAPIView
# from common.exceptions.treebo_exception import TreeboException
# from django.utils.decorators import method_decorator
# from django.views.decorators.csrf import csrf_exempt
# from rest_framework import serializers
# from rest_framework.response import Response
# from django.db.models import F
#
# from apps.checkout.models import BookingRequest
# from apps.common.utils import Utils
# from apps.fot.emailers import audit_cancellation
# from apps.fot.models import FotReservationsRequest, Fot
# from base import log_args
# from base.middlewares.set_request_id import get_current_user, get_current_request_id
# from base.views.api import TreeboAPIView
# from common.constants import common_constants as constants, error_codes
# from services.restclient.bookingrestclient import BookingClient
#
# logger = logging.getLogger(__name__)
#
#
# class CancelBookingApi(TreeboAPIView):
#     renderer_classes = [TreeboCustomJSONRenderer, ]
#     authentication_classes = [CsrfExemptSessionAuthentication, ]
#     password_service = PasswordService()
#
#     def get(self, request, format=None):
#         raise Exception('Method not supported')
#
#     @log_args(logger)
#     def post(self, request, format=None):
#         """
#         :param request:
#         :param format:
#         :return:
#         """
#         try:
#             cancel_booking_dto = CancelBookingDTO(data=request.data)
#             if not cancel_booking_dto.is_valid():
#                 return api_response.invalid_request_error_response(cancel_booking_dto.errors)
#             order_id = cancel_booking_dto['order_id']
#             booking_request = Booking.objects.get(order_id=order_id)
#             BookingClient.cancel_booking(booking_request.order_id, None)
#             response = api_response.prep_success_response('Your cancellation request has been Accepted.')
#         except BookingRequest.DoesNotExist as e:
#             response = api_response.error_response(None, 'Booking Not Found.', HTTP_404_NOT_FOUND)
#         except Exception as e:
#             logger.exception(" change password v2 ")
#             response = api_response.internal_error_response(e.message)
#         return response
