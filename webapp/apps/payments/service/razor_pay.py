import requests
from django.conf import settings
import logging
import json

from rest_framework.status import HTTP_200_OK

from apps.common.exceptions.payment_exception import InvalidTransactionException
from apps.common.slack_alert import SlackAlertService

logger = logging.getLogger(__name__)


class RazorPayService:
    def capture_razorpay_payment(
            self,
            amount_to_send,
            razorpay_payment_id,
            user_id=None,
            request_id=None):
        url = settings.RAZORPAY_API + razorpay_payment_id + '/capture'
        resp = requests.post(
            url,
            data={
                'amount': amount_to_send},
            auth=(
                settings.RAZORPAY_KEYS,
                settings.RAZORPAY_SECRET))
        logger.info(
            'RazorPay capture response for payment_id %s: %s , %s',
            razorpay_payment_id,
            resp.text,
            resp.status_code)
        if resp.status_code != HTTP_200_OK:
            data = resp.json()
            error = data.get('error', {})
            if error:
                desc = error['description']
                if 'already been captured' in desc:
                    logger.info(
                        "Payment already captured %s ",
                        razorpay_payment_id)
                    return
            logger.error(
                "RazorPay captured failed for paymentid %s and amount %s",
                razorpay_payment_id,
                amount_to_send)
            raise InvalidTransactionException()
        else:
            logger.debug(
                " razorpay captured success for :  %s  ",
                razorpay_payment_id)

    def get_razorpay_payment_details(self, razorpay_payment_id):
        payment_url = settings.RAZORPAY_API + razorpay_payment_id
        resp = requests.get(
            payment_url,
            data={},
            auth=(
                settings.RAZORPAY_KEYS,
                settings.RAZORPAY_SECRET))
        logger.info(
            'RazorPay payment details response for payment_id %s: %s , %s ',
            razorpay_payment_id,
            resp.text,
            resp.status_code)
        if resp.status_code == HTTP_200_OK:
            razorpay_response_data = resp.json()
            amount = self.validate_razorpay_payment(razorpay_response_data)
        else:
            SlackAlertService.publish_alert(
                "payment failure for prepaid booking %s " %
                (razorpay_payment_id))
            raise InvalidTransactionException()
        return amount

    def validate_razorpay_payment(self, razorpay_response_data):
        logger.info(
            "validate_razorpay_payment %s  ",
            json.dumps(razorpay_response_data))
        payment_id = razorpay_response_data.get('id', None)
        status = razorpay_response_data.get('status', None)
        amount = razorpay_response_data.get('amount', 0)
        error_description = razorpay_response_data.get(
            'error_description', None)
        notes = razorpay_response_data.get('notes', None)

        if status and str(status) in ['authorized', 'captured']:
            return amount
        else:
            SlackAlertService.publish_alert(
                " razorpay payment validation failed for payment %s for amount %s because %s for order %s " %
                (payment_id, amount, error_description, str(notes)))
            raise InvalidTransactionException()
