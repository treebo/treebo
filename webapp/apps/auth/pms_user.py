class PmsUser:
    def __init__(self, auth_id, email, first_name, last_name, phone_number):
        self.authn_id = auth_id
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.email = email
