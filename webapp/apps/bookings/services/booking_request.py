import logging
import re
from apps.checkout.models import BookingRequest
from django.db.models.query_utils import Q
from base import log_args
from dbcommon.models.profile import User

from common.custom_logger.booking.log import get_booking_logger
logger = get_booking_logger(__name__)


class BookingRequestServices(object):

    def __init__(self):
        pass

    @log_args(logger)
    def get_existing_booking_request(
            self,
            guest_email,
            guest_phone,
            logged_user_id='',
            otp_verified_number=''):
        """
        Returns completed booking request object for email or phone if exist
        :param email:
        :param phone:
        :return:
        """

        sanitized_email = User.objects.strip_gmail_special_chars(guest_email)
        booking_request = None
        base_query = Q(
            email=guest_email) | Q(
            email=sanitized_email) | Q(
            phone=guest_phone)
        if "gmail" in guest_email:
            stripped_email = guest_email.replace("@gmail.com", "")
            stripped_email = re.sub(
                r"\+.*", "", stripped_email, flags=re.IGNORECASE)
            base_query |= Q(
                email__iregex='^' +
                stripped_email +
                '\+*.*' +
                '@gmail.com$')

        if logged_user_id:
            base_query |= Q(user_id=logged_user_id)

        if otp_verified_number:
            base_query |= Q(otp_verified_number=otp_verified_number)

        try:
            booking_request = BookingRequest.objects.filter(
                base_query & Q(status=BookingRequest.COMPLETED)).first()

            if booking_request:
                logger.debug(
                    "Booking request object found with completed status %s",
                    booking_request.order_id)

        except Exception as exc:
            logger.exception(
                "Error occurred while fetching booking request for %s %s",
                guest_email,
                guest_phone)
        return booking_request
