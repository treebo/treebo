import logging

from django.template import Context

from services.notification.base_notification_service import NotificationService

logger = logging.getLogger(__name__)


class RegistrationEmailService(NotificationService):
    template_name = "profiles/registration_mail.html"

    def __init__(
            self,
            notification_type,
            content_subtype,
            to_address,
            subject,
            data):
        super(
            RegistrationEmailService,
            self).__init__(
            notification_type,
            content_subtype,
            to_address,
            subject,
            data)

    def sendMessage(self):
        NotificationService._sendMessage(self)

    def createMessage(self):
        return NotificationService._createMessage(self)

    def collateEmailData(self, first_name, couponPresent, uniqueCouponCode):
        data = Context({"first_name": first_name,
                        "couponPresent": couponPresent,
                        "couponCode": uniqueCouponCode})
        self._setData(data)
