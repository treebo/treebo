# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0054_auto_20170605_1058'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hotel',
            name='category',
        ),
        migrations.AddField(
            model_name='hotel',
            name='category',
            field=models.ManyToManyField(
                to='dbcommon.Categories',
                null=True,
                blank=True),
        ),
    ]
