# -*- coding: utf-8 -*-


import django.contrib.postgres.fields.hstore
from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('content', '0001_first'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContentStore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('name', models.CharField(max_length=200)),
                ('data', django.contrib.postgres.fields.hstore.HStoreField()),
                ('version', models.PositiveIntegerField(default=1)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
