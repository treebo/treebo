import mock
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.test import TestCase, RequestFactory
from pyquery import PyQuery as pQ

# from apps.bookings.v1 import bookingutils
from common.constants import common_constants


class SaveBookingTest(TestCase):
    def setUp(self):
        self.url = reverse('bookings_v1:save-booking')
        self.factory = RequestFactory()
        # self.mock_get_booking_from_db_patcher = mock.patch.object(savebooking.SaveBooking,
        #                                                          '_SaveBooking__getBookingOrderFromDB')
        # self.mock_parse_request_patcher = mock.patch.object(savebooking.SaveBooking,
        #                                                    '_SaveBooking__parseRequest')
        # self.mock_save_booking_hotelogix_patcher = mock.patch.object(savebooking.SaveBooking,
        #                                                             '_SaveBooking__saveBookingHotelogix')
        # self.mock_load_cart_hotelogix_patcher = mock.patch.object(savebooking.SaveBooking,
        #                                                          '_SaveBooking__loadCartHotelogix')

        #self.mock_get_booking_from_db = self.mock_get_booking_from_db_patcher.start()
        #self.mock_parse_request = self.mock_parse_request_patcher.start()
        #self.mock_save_booking_hotelogix = self.mock_save_booking_hotelogix_patcher.start()
        #self.mock_load_cart_hotelogix = self.mock_load_cart_hotelogix_patcher.start()

    def tearDown(self):
        # self.mock_get_booking_from_db_patcher.stop()
        # self.mock_parse_request_patcher.stop()
        # self.mock_save_booking_hotelogix_patcher.stop()
        # self.mock_load_cart_hotelogix_patcher.stop()
        pass

    def get_not_supported(self):
        request = self.factory.get(self.url)
        view = savebooking.SaveBooking.as_view()
        with self.assertRaises(Exception):
            response = view(request=request)

    #@mock.patch.object(savebooking.ConfirmBooking, 'confirm')
    # def test_with_booking_id_in_session_pay_at_hotel(self, mock_confirm_booking):
    #    pay_at_hotel = "1"
    #    post_data = ['coupon_code', pay_at_hotel, 'TEST_123', 'Rohit', 'test@gmail.com',
    #                 '9999112231', '', '']
    #    self.mock_parse_request.return_value = post_data
    #    mock_booking = mock.Mock()
    #    self.mock_get_booking_from_db.return_value = mock_booking
    #    mock_confirm_booking.return_value = mock.MagicMock(spec=HttpResponse)

    #    request = self.factory.post(self.url)
    #    request.session = {'booking_id': 'TEST_123'}
    #    view = savebooking.SaveBooking.as_view()
    #    response = view(request=request)

    #    self.mock_get_booking_from_db.assert_called_once_with('TEST_123')
    #    mock_confirm_booking.assert_called_once_with(mock.ANY, mock_booking)
    #    self.mock_save_booking_hotelogix.assert_not_called()
    #    self.mock_load_cart_hotelogix.assert_not_called()

    #@mock.patch.object(bookingutils, 'constructItineraryUrlOnError')
    #@mock.patch.object(savebooking.ConfirmBooking, 'confirm')
    # def test_no_booking_id_in_session_pay_at_hotel_load_cart_error(self, mock_confirm_booking,
    #                                                               mock_construct_itinerary_error):
    #    pay_at_hotel = "1"
    #    post_data = ['coupon_code', pay_at_hotel, 'TEST_123', 'Rohit', 'test@gmail.com',
    #                 '9999112231', '', '']
    #    self.mock_parse_request.return_value = post_data
    #    mock_booking = mock.Mock()
    #    self.mock_get_booking_from_db.return_value = mock_booking
    #    mock_confirm_booking.return_value = mock.MagicMock(spec=HttpResponse)
    #    self.mock_load_cart_hotelogix.return_value = None
    #    mock_construct_itinerary_error.return_value = 'test_url'

    #    request = self.factory.post(self.url)
    #    request.session = {}
    #    view = savebooking.SaveBooking.as_view()
    #    response = view(request=request)
    #    self.assertEqual(response.status_code, 302)

    #    self.assertEqual(response.status_code, 302)
    #    self.mock_load_cart_hotelogix.assert_called_once_with(mock.ANY)
    #    self.mock_get_booking_from_db.assert_not_called()
    #    mock_confirm_booking.assert_not_called()
    #    self.mock_save_booking_hotelogix.assert_not_called()

    #@mock.patch.object(bookingutils, 'constructItineraryUrlOnError')
    #@mock.patch.object(savebooking.ConfirmBooking, 'confirm')
    #@mock.patch.object(savebooking.SaveBooking, '_SaveBooking__modifyDiscountAmount')
    # def test_no_booking_id_in_session_pay_at_hotel_modify_discount_error(self, mock_modify_discount,
    #                                                                     mock_confirm_booking,
    #                                                                     mock_construct_itinerary_error):
    #    pay_at_hotel = "1"
    #    post_data = ['coupon_code', pay_at_hotel, 'TEST_123', 'Rohit', 'test@gmail.com',
    #                 '9999112231', '', '']
    #    self.mock_parse_request.return_value = post_data
    #    mock_booking = mock.Mock()
    #    self.mock_get_booking_from_db.return_value = mock_booking
    #    mock_confirm_booking.return_value = mock.MagicMock(spec=HttpResponse)
    #    self.mock_load_cart_hotelogix.return_value = mock.Mock()
    #    mock_construct_itinerary_error.return_value = 'test_url'
    #    mock_modify_discount.return_value = 'FAILURE'

    #    request = self.factory.post(self.url)
    #    request.session = {}
    #    view = savebooking.SaveBooking.as_view()
    #    response = view(request=request)
    #    self.assertEqual(response.status_code, 302)

    #    self.mock_load_cart_hotelogix.assert_called_once_with(mock.ANY)
    #    mock_modify_discount.assert_called_once_with(mock.ANY,
    #                                                 self.mock_load_cart_hotelogix.return_value)
    #    self.mock_get_booking_from_db.assert_not_called()
    #    mock_confirm_booking.assert_not_called()
    #    self.mock_save_booking_hotelogix.assert_not_called()

    #@mock.patch.object(savebooking.BookingService, 'saveBooking')
    #@mock.patch.object(profile_views, 'guestRegister')
    #@mock.patch.object(profile_utils, 'getCustomerCare')
    #@mock.patch.object(bookingutils, 'constructItineraryUrlOnError')
    #@mock.patch.object(savebooking.ConfirmBooking, 'confirm')
    #@mock.patch.object(savebooking.SaveBooking, '_SaveBooking__modifyDiscountAmount')
    # def test_no_booking_id_in_session_pay_at_hotel_failed_save_booking_for_authenticated_user(self,
    #                                                                                          mock_modify_discount,
    #                                                                                          mock_confirm_booking,
    #                                                                                          mock_construct_itinerary_error,
    #                                                                                          mock_get_customer_care,
    #                                                                                          mock_guest_register,
    #                                                                                          mock_booking_service_save_booking):
    #    pay_at_hotel = "1"
    #    post_data = ['coupon_code', pay_at_hotel, 'TEST_123', 'Rohit', 'test@gmail.com',
    #                 '9999112231', '', '']
    #    self.mock_parse_request.return_value = post_data
    #    mock_booking = mock.Mock()
    #    self.mock_get_booking_from_db.return_value = mock_booking
    #    mock_confirm_booking.return_value = mock.MagicMock(spec=HttpResponse)
    #    self.mock_load_cart_hotelogix.return_value = mock.Mock()
    #    mock_construct_itinerary_error.return_value = 'test_url'
    #    mock_modify_discount.return_value = common_constants.SUCCESS
    #    mock_get_customer_care.return_value = mock.Mock()
    #    self.mock_save_booking_hotelogix.return_value = pQ(
    #        "<status message='{0}'></status>".format('FAILURE'))

    #    request = self.factory.post(self.url)
    #    request.session = {}
    #    request.user = mock.Mock(**{'is_authenticated.return_value': True, 'id': 4})
    #    request._dont_enforce_csrf_checks = True
    #    view = savebooking.SaveBooking.as_view()
    #    response = view(request=request)

    #    self.assertEqual(response.status_code, 302)

    #    self.mock_load_cart_hotelogix.assert_called_once_with(mock.ANY)
    #    mock_modify_discount.assert_called_once_with(mock.ANY,
    #                                                 self.mock_load_cart_hotelogix.return_value)
    #    self.mock_get_booking_from_db.assert_not_called()
    #    mock_confirm_booking.assert_not_called()
    #    mock_booking_service_save_booking.assert_not_called()
    #    mock_get_customer_care.assert_called_once_with(4)
    #    mock_guest_register.assert_not_called()
    #    mock_construct_itinerary_error.assert_called_once_with(mock.ANY, "Unable to save booking")

    #@mock.patch.object(savebooking.BookingService, 'saveBooking')
    #@mock.patch.object(profile_views, 'guestRegister')
    #@mock.patch.object(profile_utils, 'getCustomerCare')
    #@mock.patch.object(bookingutils, 'constructItineraryUrlOnError')
    #@mock.patch.object(savebooking.ConfirmBooking, 'confirm')
    #@mock.patch.object(savebooking.SaveBooking, '_SaveBooking__modifyDiscountAmount')
    # def test_no_booking_id_in_session_pay_at_hotel_success_save_booking_for_authenticated_user(self,
    #                                                                                           mock_modify_discount,
    #                                                                                           mock_confirm_booking,
    #                                                                                           mock_construct_itinerary_error,
    #                                                                                           mock_get_customer_care,
    #                                                                                           mock_guest_register,
    #                                                                                           mock_booking_service_save_booking):
    #    pay_at_hotel = "1"
    #    post_data = ['coupon_code', pay_at_hotel, 'TEST_123', 'Rohit', 'test@gmail.com',
    #                 '9999112231', '', '']
    #    self.mock_parse_request.return_value = post_data
    #    mock_booking = mock.Mock()
    #    self.mock_get_booking_from_db.return_value = mock_booking
    #    mock_confirm_booking.return_value = mock.MagicMock(spec=HttpResponse,
    #                                                       **{'status_code': 200})
    #    self.mock_load_cart_hotelogix.return_value = mock.Mock()
    #    mock_construct_itinerary_error.return_value = 'test_url'
    #    mock_modify_discount.return_value = common_constants.SUCCESS
    #    mock_get_customer_care.return_value = mock.Mock()
    #    self.mock_save_booking_hotelogix.return_value = pQ(
    #        "<status message='{0}'></status>".format(common_constants.SUCCESS))
    #    mock_booking_service_save_booking.return_value = 'TEST_321'

    #    request = self.factory.post(self.url)
    #    request._dont_enforce_csrf_checks = True
    #    request.session = {}
    #    request.user = mock.Mock(**{'is_authenticated.return_value': True, 'id': 4})
    #    view = savebooking.SaveBooking.as_view()
    #    response = view(request=request)

    #    self.assertEqual(response.status_code, 200)

    #    self.mock_load_cart_hotelogix.assert_called_once_with(mock.ANY)
    #    mock_modify_discount.assert_called_once_with(mock.ANY,
    #                                                 self.mock_load_cart_hotelogix.return_value)

    #    self.assertTrue(self.mock_save_booking_hotelogix.called)
    #    self.mock_get_booking_from_db.assert_called_once_with('TEST_321')
    #    mock_confirm_booking.assert_called_once_with(mock.ANY, mock_booking)
    #    mock_get_customer_care.assert_called_once_with(4)
    #    mock_guest_register.assert_not_called()
    #    mock_construct_itinerary_error.assert_not_called()
    #    mock_booking_service_save_booking.assert_called_once_with(
    #        self.mock_save_booking_hotelogix.return_value, mock.ANY)

    # def test_no_booking_id_in_session_payment_gateway(self):
    #    post_data = ['coupon_code', '0', 'TEST_123', 'Rohit', 'test@gmail.com', '9999112231', '',
    #                 '']
    #    pass

    # def test_with_booking_id_in_session_payment_gateway(self):
    #    pay_at_hotel = "0"
    #    post_data = ['coupon_code', pay_at_hotel, 'TEST_123', 'Rohit', 'test@gmail.com',
    #                 '9999112231', '', '']
