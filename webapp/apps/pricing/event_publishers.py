import logging
from apps.pricing.serializers import StateTaxSyncSerializer, LuxuryTaxSyncSerializer, PromoSyncSerializer
from base.message_queues import pricing_publishers
from collections import defaultdict

logger = logging.getLogger(__name__)


def publish_promo(hotel_id, promos):
    for promo in promos:
        serializer = PromoSyncSerializer(promo)
        serialized_promo = serializer.data
        serialized_promo["conditions"] = [
            dict(condition) for condition in serialized_promo["conditions"]]
        payload = {
            "type": "promo_sync",
            "data": {
                "hotel_id": hotel_id,
                "promo": serialized_promo
            }
        }
        pricing_publishers.publish('promo', payload)


def publish_promo_deletion(promo_ids):
    payload = {
        "type": "promo_deletion",
        "data": {
            "method": "DELETE",
            "promo_ids": promo_ids,
        }
    }
    pricing_publishers.publish('promo_deletion', payload)


def publish_taxes(state_id, state_taxes, luxury_taxes):
    serialized_state_taxes = StateTaxSyncSerializer(
        state_taxes, many=True).data
    serialized_luxury_taxes = LuxuryTaxSyncSerializer(
        luxury_taxes, many=True).data
    payload = {
        "type": "tax_sync", "data": {
            "state_id": state_id, "state_taxes": [
                dict(state_tax) for state_tax in serialized_state_taxes], "luxury_taxes": [
                dict(luxury_tax) for luxury_tax in serialized_luxury_taxes]}}
    pricing_publishers.publish('tax', payload)


def publish_hotel_wise_luxury_taxes(state_id, hotel_id, luxury_taxes):
    serialized_luxury_taxes = LuxuryTaxSyncSerializer(
        luxury_taxes, many=True).data
    payload = {
        "type": "luxury_tax_sync",
        "data": {
            "state_id": state_id,
            "hotel_id": hotel_id,
            "luxury_taxes": [
                dict(luxury_tax) for luxury_tax in serialized_luxury_taxes]}}
    pricing_publishers.publish('luxury_tax_sync', payload)


# Publish RoomPriceV2
def publish_price(hotel_id, room_prices):
    # Group RoomPrice by Date
    room_price_by_date = defaultdict(list)
    for price in room_prices:
        room_price_by_date[price.date].append(price)

    # For RoomPrices in every group, group them by room_type_code
    room_price_group_by_date_by_room_type = {}
    for k, v in list(room_price_by_date.items()):
        price_by_type = defaultdict(list)
        for price in v:
            price_by_type[price.room.room_type_code].append(price)
        room_price_group_by_date_by_room_type[k] = price_by_type

    for date, price_by_type in list(
            room_price_group_by_date_by_room_type.items()):
        rooms = []
        for room_type, prices in list(price_by_type.items()):
            rooms.append({"room_type": room_type.lower(),
                          "prices": [{"guest_count": price.guest_count,
                                      "base_price": float(price.base_price)} for price in prices]})
        payload = {
            "type": "price_sync",
            "data": {
                "hotel_id": hotel_id,
                "date": date.strftime("%Y-%m-%d"),
                "rooms": rooms
            }
        }
        logger.debug("Publishing Payload: %s", payload)
        pricing_publishers.publish('price', payload)


# Publish old RoomPrice. Not used right now
def publish_prices(hotel_id, date, prices_by_room_type):
    payload = {"type": "price_sync",
               "data": {"hotel_id": hotel_id,
                        "date": date,
                        "rooms": [{"room_type": room_type.lower(),
                                   "prices": [{"guest_count": occupancy,
                                               "base_price": room_price.base_price} for occupancy,
                                              room_price in list(price_by_occupancy.items())]} for room_type,
                                  price_by_occupancy in list(prices_by_room_type.items())]}}
    pricing_publishers.publish('price', payload)
