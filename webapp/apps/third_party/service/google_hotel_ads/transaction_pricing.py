
import datetime
import logging

from django.conf import settings
from django.core.cache import cache
from django.db.models import Prefetch
from lxml import etree
from lxml import etree as et

from apps.bookingstash.service.availability_service import get_availability_service
from apps.pricing.dateutils import ymd_str_to_date
from apps.third_party.service.google_hotel_ads import constants
from apps.third_party.service.google_hotel_ads.price_adaptors.non_rate_plan_prices import NonRatePlanPriceAdaptor
from apps.third_party.service.google_hotel_ads.price_adaptors.rate_plan_prices import RatePlanPriceAdaptor
from dbcommon.models.facilities import Facility
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room

logger = logging.getLogger(__name__)


class TransactionService(object):
    price_adaptor = None
    CACHE_TIMEOUT = 24 * 60 * 60
    CACHE_KEY = 'apicache_transaction_service_{0}'

    def get_cache_key(self, hotel_id):
        return TransactionService.CACHE_KEY.format(hotel_id)

    def invalidate_cache(self, hotel_id):
        if hotel_id:
            cache.delete(TransactionService.CACHE_KEY.format(hotel_id))
        else:
            cache.delete_pattern(TransactionService.CACHE_KEY.format('*'))

    def read_from_query_xml(self, data):
        if settings.NRP_ENABLED:
            self.price_adaptor = RatePlanPriceAdaptor()
        else:
            self.price_adaptor = NonRatePlanPriceAdaptor()

        try:
            tree = etree.fromstring(data)
            checkin_date, num_of_nights, hotel_ids_list = self.read_pricing_query(
                tree)

            non_cached_hotel_ids = []
            root = et.Element('Transaction')
            root.set('timestamp', str(datetime.datetime.now().isoformat()))
            root.set("id", str(datetime.datetime.now().isoformat()))

            for hotel_id in hotel_ids_list:
                try:
                    hotel_data_response = cache.get(self.get_cache_key(hotel_id))
                except Exception as e:
                    hotel_data_response = None
                if hotel_data_response:
                    logger.info(
                        "Data found in Cache for hotel id %s", hotel_id)
                    hotel_data_response = etree.fromstring(hotel_data_response)
                    root.append(hotel_data_response)
                else:
                    non_cached_hotel_ids.append(hotel_id)

            hotels_data = Hotel.objects.select_related('city').prefetch_related(
                Prefetch('rooms', queryset=Room.objects.filter(hotel_id__in=hotel_ids_list)
                         .prefetch_related(Prefetch("image_set")))).filter(id__in=hotel_ids_list)

            remaining_hotels_data = [
                x for x in hotels_data if str(
                    x.id) in str(non_cached_hotel_ids)]

            if non_cached_hotel_ids:
                hotels_parking = Facility.objects.get(
                    name="Parking").hotels.all()
                self.generate_property(
                    root, remaining_hotels_data, hotels_parking)

            transaction_response = self.generate_xml(
                root, hotels_data, checkin_date, num_of_nights, hotel_ids_list)
            return transaction_response

        except Exception as e:
            logger.exception(
                "Exception in generating Transaction File for GHA")
            raise e

    def read_pricing_query(self, tree):
        hotel_ids_list = []

        checkin_date = tree.find('Checkin').text
        num_of_nights = tree.find('Nights').text

        first_date = tree.find('FirstDate')
        if first_date is not None:
            first_date = first_date.text
        if tree.find('LastDate'):
            last_Date = tree.find('LastDate').text
        if tree.find('AffectedNights'):
            affected_nights = tree.find('AffectedNights').text
        property_list = tree.find('PropertyList')
        for property in property_list.findall('Property'):
            hotel_ids_list.append(property.text)
        if tree.find('LatencySensitive'):
            latency_sensitive = tree.find('LatencySensitive').text
        if tree.find('DeadlineMS'):
            deadline_ms = tree.find('DeadlineMS').text

        return checkin_date, num_of_nights, hotel_ids_list

    def generate_property(self, root, hotels_data, hotels_parking):
        for hotel in hotels_data:
            logger.info("generate property tag for hid %s", hotel.id)
            property_data_set = et.SubElement(root, 'PropertyDataSet')
            property = et.SubElement(property_data_set, 'Property')
            property.text = str(hotel.id)

            for room in hotel.rooms.all():
                room_data = et.SubElement(property_data_set, 'RoomData')
                room_id = et.SubElement(room_data, 'RoomID')
                room_id.text = room.room_type_code
                name = et.SubElement(room_data, 'Name')
                text = et.SubElement(name, "Text")
                text.set("text", room.room_type)
                text.set("language", "en")
                description = et.SubElement(room_data, "Description")
                text = et.SubElement(description, "Text")
                text.set("text", room.description if room.description else "")
                text.set("language", "en")

                for image in room.image_set.all():
                    photo_url = et.SubElement(room_data, 'PhotoURL')
                    caption = et.SubElement(photo_url, 'Caption')
                    text = et.SubElement(caption, "Text")
                    text.set("text", room.room_type)
                    text.set("language", "en")
                    url = et.SubElement(photo_url, 'URL')
                    url.text = str(image.url)

                capacity = et.SubElement(room_data, "Capacity")
                capacity.text = str(room.max_adult_with_children)
                occupancy = et.SubElement(room_data, "Occupancy")
                occupancy.text = str(room.max_guest_allowed)

            package_data = et.SubElement(property_data_set, 'PackageData')
            package_id = et.SubElement(package_data, 'PackageID')
            package_id.text = constants.TREEBO_PACKAGE
            name = et.SubElement(package_data, 'Name')
            text = et.SubElement(name, "Text")
            text.set("text", constants.TREEBO_PACKAGE_TEXT)
            text.set("language", "en")
            description = et.SubElement(package_data, "Description")
            text = et.SubElement(description, "Text")
            text.set("text", constants.PACKAGE_DESCRIPTION_TEXT)
            text.set("language", "en")
            refundable = et.SubElement(package_data, "Refundable")
            refundable.set("available", "true")
            refundable.set("refundable_until_days",
                           constants.REFUNDABLE_UNTIL_DAYS)
            refundable.set("refundable_until_time",
                           constants.REFUNDABLE_UNTIL_TIME)
            breakfast_included = et.SubElement(
                package_data, 'BreakfastIncluded')
            breakfast_included.text = "true"
            if hotels_parking.filter(id=hotel.id).exists():
                parking_included = et.SubElement(
                    package_data, 'ParkingIncluded')
                parking_included.text = "true"
            internet_included = et.SubElement(package_data, 'InternetIncluded')
            internet_included.text = "true"

            cache_property_data_set = etree.tostring(property_data_set)
            cache.set(self.get_cache_key(hotel.id), cache_property_data_set,
                      TransactionService.CACHE_TIMEOUT)

    def generate_xml(
            self,
            root,
            hotels_data,
            checkin_date,
            num_of_nights,
            hotel_ids_list):
        num_of_days = datetime.timedelta(days=int(num_of_nights))
        checkout_date = str(
            num_of_days +
            datetime.datetime.strptime(
                checkin_date,
                "%Y-%m-%d").date())
        logger.info("checkout date transaction pricing %s", checkout_date)

        checkin = ymd_str_to_date(checkin_date)
        checkout = (checkin + num_of_days)
        hotel_prices = self.price_adaptor.fetch_price(
            hotel_ids_list, checkin, checkout, constants.ROOM_CONFIG)

        room_availability_per_hotel = get_availability_service().get_available_rooms_for_hotels(
            hotel_ids_list,
            ymd_str_to_date(checkin_date),
            ymd_str_to_date(checkout_date),
            constants.ROOM_CONFIG)

        logger.debug(
            "room_availability_per_hotel %s",
            room_availability_per_hotel)
        basic_rooms = self.get_basic_rooms_for_hotels(
            room_availability_per_hotel, hotel_ids_list)
        logger.debug("basic_rooms %s", basic_rooms)

        for hotel in hotels_data:
            hotel_price = hotel_prices.get(hotel.id)
            if hotel_price is not None:
                logger.info("generate prices for hotel with hid %d", hotel.id)
                result = et.SubElement(root, "Result")
                property = et.SubElement(result, "Property")
                property.text = str(hotel.id)
                checkin = et.SubElement(result, "Checkin")
                checkin.text = checkin_date
                nights = et.SubElement(result, "Nights")
                nights.text = num_of_nights
                if basic_rooms[hotel.id] is not None:
                    room_id = et.SubElement(result, 'RoomID')
                    room_id.text = basic_rooms[hotel.id].title()
                base_rate = et.SubElement(result, "Baserate")
                base_rate.set("currency", constants.CURRENCY)
                tax = et.SubElement(result, "Tax")
                tax.set("currency", constants.CURRENCY)

                # hotel_rooms = {price['room_type']: price for price in hotel_price}

                base_rate.text = str(self.price_adaptor.read_pretax_price(
                    hotel_price, basic_rooms[hotel.id], default=-1))
                tax.text = str(self.price_adaptor.read_tax(
                    hotel_price, basic_rooms[hotel.id], default=0))

                other_fees = et.SubElement(result, "OtherFees")
                other_fees.set("currency", constants.CURRENCY)
                other_fees.text = "0"
                # allowable_points_of_sale = et.SubElement(
                #     result, "AllowablePointsOfSale")
                # point_of_sale = et.SubElement(
                #     allowable_points_of_sale, "PointOfSale")
                # point_of_sale.set("id", slugify(hotel.name))
                custom_one = et.SubElement(result, "Custom1")
                custom_one.text = hotel.get_sitemap_url()
                # TODO : replace & with str and not as entities
                custom_two = et.SubElement(result, "Custom2")
                custom_two.text = "city=" + hotel.city.name + "&hotel_id=" + str(hotel.id) + "&q=" + hotel.name.replace(
                    " ", "+")
                # escape("< & >")
                logger.debug(" hotel.rooms.all()%s", hotel.rooms.all())
                for room in hotel.rooms.all():
                    room_type_code = room.room_type_code.lower()
                    if room_type_code in hotel_price and room_availability_per_hotel.get(
                        (int(hotel.id), room_type_code), 0) > 0:
                        roombundle = et.SubElement(result, "RoomBundle")
                        room_id = et.SubElement(roombundle, 'RoomID')
                        room_id.text = room.room_type_code
                        rate_plan_id = et.SubElement(roombundle, 'RatePlanID')
                        rate_plan_id.text = ""
                        package_id = et.SubElement(roombundle, 'PackageID')
                        package_id.text = constants.TREEBO_PACKAGE
                        occupancy = et.SubElement(roombundle, "Occupancy")
                        occupancy.text = str(room.max_guest_allowed)
                        base_rate = et.SubElement(roombundle, "Baserate")
                        base_rate.set("currency", constants.CURRENCY)
                        tax = et.SubElement(roombundle, "Tax")
                        tax.set("currency", constants.CURRENCY)
                        base_rate.text = str(self.price_adaptor.read_pretax_price(hotel_price, room_type_code,
                                                                                  default=-1))
                        tax.text = str(self.price_adaptor.read_tax(hotel_price, room_type_code, default=0))
                        other_fees = et.SubElement(roombundle, "OtherFees")
                        other_fees.set("currency", constants.CURRENCY)
                        other_fees.text = "0"
                        custom_one = et.SubElement(roombundle, "Custom1")
                        custom_one.text = hotel.get_sitemap_url()
                        custom_two = et.SubElement(roombundle, "Custom2")
                        custom_two.text = "city=" + hotel.city.name + "&hotel_id=" + str(
                            hotel.id) + "&q=" + hotel.name.replace(
                            " ", "+")

        tree = et.ElementTree(root)
        # tree.write('transaction_message.xml', pretty_print=True, xml_declaration=True, encoding="UTF-8")
        # tree = etree.tostring(tree)
        return tree

    def get_basic_rooms_for_hotels(
            self,
            room_availability_per_hotel,
            hotel_ids_list):
        basic_rooms = {}
        for hotel_id in hotel_ids_list:
            if constants.ROOM_CONFIG == '1-0':
                if room_availability_per_hotel.get(
                        (int(hotel_id), 'acacia'), 0) > 0:
                    basic_rooms[int(hotel_id)] = 'acacia'
            if constants.ROOM_CONFIG == '2-0' or room_availability_per_hotel.get((int(hotel_id), 'acacia'), 0) == 0:
                if  room_availability_per_hotel.get((int(hotel_id), 'acacia'), 0) > 0:
                    basic_rooms[int(hotel_id)] = 'acacia'
                elif room_availability_per_hotel.get((int(hotel_id), 'oak'), 0) > 0:
                    basic_rooms[int(hotel_id)] = 'oak'
                elif room_availability_per_hotel.get((int(hotel_id), 'maple'), 0) > 0:
                    basic_rooms[int(hotel_id)] = 'maple'
                elif room_availability_per_hotel.get((int(hotel_id), 'mahogany'), 0) > 0:
                    basic_rooms[int(hotel_id)] = 'mahogany'
                else:
                    basic_rooms[int(hotel_id)] = None
        return basic_rooms
