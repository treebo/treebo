from django.conf import settings
from django.core.cache import cache

from dbcommon.models.feature_toggle import FeatureToggle


class FeatureToggleAPI(object):
    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'apicache_featuretoggleapi_{0}'

    @staticmethod
    def invalidate_cache(namespace=None, feature=None):
        if namespace:
            cache.delete(
                FeatureToggleAPI.CACHE_KEY.format(
                    namespace +
                    '_' +
                    feature +
                    '_' +
                    settings.FEATURE_TOGGLE_ENV))
        else:
            cache.delete_pattern(FeatureToggleAPI.CACHE_KEY.format('*'))

    @staticmethod
    def is_enabled(namespace, feature, default_value=False):
        cache_key = FeatureToggleAPI.CACHE_KEY.format(
            namespace + '_' + feature + '_' + settings.FEATURE_TOGGLE_ENV)
        try:
            result = cache.get(cache_key)
        except Exception as e:
            result = None
        if result:
            return result

        try:
            if not settings.FEATURE_TOGGLE_ENV:
                result = default_value
            else:
                feature_toggle = FeatureToggle.objects.filter(
                    namespace=namespace,
                    feature=feature,
                    environment=settings.FEATURE_TOGGLE_ENV).order_by('-id').first()
                if not feature_toggle:
                    result = default_value
                else:
                    result = feature_toggle.is_enabled
        except BaseException:
            result = default_value
        cache.set(cache_key, result, FeatureToggleAPI.CACHE_TIMEOUT)
        return result
