import logging

from ths_common.exceptions import CRSException
from thsc.crs.exceptions import CRSAPIException

from apps.bookings.providers.exceptions import BookingAlreadyCancelledException
from apps.bookings.integrations.exceptions import CrsApiFailed
from apps.bookings.providers.cancel_booking.treebo import TreeboCRSCancelBooking
from apps.bookings.providers.create_booking.treebo import TreeboCRSCreateBooking
from apps.bookings.providers.get_booking.db import DBGetBooking
from apps.bookings.providers.get_booking.treebo import TreeboCRSGetBooking
from apps.bookings.providers.update_booking.treebo import TreeboCRSUpdateBooking

logger = logging.getLogger(__name__)


class TreeboCRSWrapper:
    @classmethod
    def create_booking(cls, booking_id, user, user_login_state):
        try:
            booking = DBGetBooking(booking_id=booking_id).get()
            return TreeboCRSCreateBooking(booking, user=user, user_login_state=user_login_state).create()
        except CRSException as e:

            logger.exception(e)
            raise CrsApiFailed('Treebo CRS', 'confirm booking', "{m}: {e}".format(m=e.message, e=e.extra_payload))

    @classmethod
    def update_booking(cls, booking_id):
        try:
            new_booking = DBGetBooking(booking_id=booking_id).get()
            old_booking = TreeboCRSGetBooking(booking_id=booking_id).get()
            return TreeboCRSUpdateBooking(old_booking, new_booking).update()
        except CRSException as e:
            logger.exception(e)
            raise CrsApiFailed('Treebo CRS', 'update booking', "{m}: {e}".format(m=e.message, e=e.extra_payload))

    @classmethod
    def cancel_booking(cls, booking_id, **kwargs):
        try:
            booking = TreeboCRSGetBooking(booking_id=booking_id).get()
            return TreeboCRSCancelBooking(booking, **kwargs).cancel()
        except CRSAPIException as e:
            logger.exception(e)
            if str(e.extra_payload[0]['code']) == str('04010112'):
                raise BookingAlreadyCancelledException(booking_id)
            raise CrsApiFailed('Treebo CRS', 'cancel booking', "{m}: {e}".format(m=e.message, e=e.extra_payload))

    @classmethod
    def fetch_bookings_using_phone_or_email(cls, phone_number=None, email=None):
        try:
            return TreeboCRSGetBooking.search_bookings_by_phone_or_email(phone_number=phone_number, email=email)
        except CRSAPIException as e:
            logger.exception(e)
            raise CrsApiFailed('Treebo CRS', 'fetch bookings', "{m}: {e}".format(m=e.message, e=e.extra_payload))
