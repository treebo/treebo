import datetime
import json
import logging
from collections import defaultdict

from django.db.models import Q, Sum

from . import models
from .dateutils import date_range
from django.db import connection

DISCOUNT_PERCENT = 'Percent'
DEFAULT_COUPON_PERCENT = 0.0
DEFAULT_LUXURY_TAX = 0.0
DEFAULT_STATE_TAX = 0.0

logger = logging.getLogger(__name__)


class TaxUtils:
    def __init__(self):
        pass

    @staticmethod
    def get_luxury_taxes_for_state(state_id):
        return models.LuxuryTax.objects.filter(state__id=state_id)

    @classmethod
    def get_luxury_taxes_by_states_wise(cls, states):
        luxury_taxes = models.LuxuryTax.objects.filter(
            state__in=states).select_related("state")
        state_wise_luxury_tax = defaultdict(list)
        for luxury_tax in luxury_taxes:
            state_wise_luxury_tax[luxury_tax.state.id].append(luxury_tax)
        return state_wise_luxury_tax

    @staticmethod
    def get_state_tax_for_date(date, state_id):
        tax_record = models.StateTax.objects.filter(Q(state__id=state_id) & Q(effective_date__lte=date) & (
            Q(expiry_date__gte=date) | Q(expiry_date__isnull=True))).aggregate(Sum('tax_value'))
        tax = tax_record.get('tax_value__sum', DEFAULT_STATE_TAX)
        return tax if tax else DEFAULT_STATE_TAX

    @staticmethod
    def get_state_tax_by_date_wise(stay_start, stay_end, state):
        date_wise_state_tax = {}
        for current_date in date_range(stay_start, stay_end):
            logger.info('Start Date in loop: %s', current_date)
            tax_record = models.StateTax.objects.filter(Q(state=state) & Q(effective_date__lte=current_date) & (
                Q(expiry_date__gte=current_date) | Q(expiry_date__isnull=True))).aggregate(Sum('tax_value'))
            date_wise_state_tax[current_date] = tax_record['tax_value__sum']
        return date_wise_state_tax

    def __get_state_tax_result_list(self, checkInDate, checkoutDate, states):
        cursor = connection.cursor()
        state_tax_query = '''
            SELECT dt::date AS date,
                   state_id,
                   sum(tax_value)
            FROM generate_series(%(checkInDate)s, %(checkoutDate)s, '1 day'::interval) dates(dt)
            LEFT JOIN pricing_statetax ON state_id = ANY(%(states)s)
            AND ((dt::date BETWEEN effective_date AND expiry_date)
                 OR (dt::date > effective_date
                     AND expiry_date IS NULL))
            GROUP BY state_id,
                     dt::date;
        '''
        cursor.execute(state_tax_query,
                       {'checkInDate': checkInDate,
                        'checkoutDate': checkoutDate,
                        'states': list(states)})
        results = cursor.fetchall()
        return results

    @staticmethod
    def get_state_tax_by_hotel_date_wise(
            checkInDate, checkoutDate, hotel_state_dict):
        # Takes input of hotel and state object in following format
        # {<hotel_obj>: <state_obj>, <hotel_obj>: <state_obj>}
        # TODO: Correct the abstraction, TaxUtils should not be considering
        # hotel as input
        states = set(hotel_state_dict.values())
        tax_utils_obj = TaxUtils()
        date_state_tax_list = tax_utils_obj.__get_state_tax_result_list(
            checkInDate, checkoutDate, states)

        date_state_tax_list_map = defaultdict(dict)
        for current_date, state_id, taxes in date_state_tax_list:
            date_state_tax_list_map[current_date].update({state_id: taxes})

        date_hotel_wise_state_tax_result = defaultdict(dict)
        for current_date, state_tax_dict in list(
                date_state_tax_list_map.items()):
            hotel_wise_state_tax = {}
            for hotel, state_id in list(hotel_state_dict.items()):
                hotel_wise_state_tax[hotel] = state_tax_dict[state_id]
            date_hotel_wise_state_tax_result[current_date].update(
                hotel_wise_state_tax)
        return date_hotel_wise_state_tax_result

    @staticmethod
    def get_tax_on_price(price, state_tax, luxury_taxes):
        applicable_percent = state_tax = state_tax or DEFAULT_STATE_TAX
        for luxuryTax in luxury_taxes:
            if luxuryTax.start_range <= price <= luxuryTax.end_range:
                luxury_tax = luxuryTax.tax_value if luxuryTax.tax_value else DEFAULT_LUXURY_TAX
                applicable_percent = float(luxury_tax) + float(state_tax)
                break
        tax = (float(price) * (float(applicable_percent))) / 100.00
        logger.debug(
            'Applicable Percentage: %s, tax: %s',
            applicable_percent,
            tax)
        return tax


class DiscountUtils:
    def __init__(self):
        pass

    @staticmethod
    def calculate_coupon_discount(pretaxAmount, couponPercent):
        if not couponPercent:
            return 0.0
        coupon_discount_value = (
            float(pretaxAmount) *
            float(couponPercent) /
            100.0)
        return coupon_discount_value

    @staticmethod
    def __get_discount_amount(
            pretax_price,
            discount,
            discount_application_type):
        # TODO discount_application_type to enum
        if discount_application_type == DISCOUNT_PERCENT:
            discount_value = (float(pretax_price) * float(discount)) / 100.00
        else:
            discount_value = discount
        return discount_value

    @staticmethod
    def get_promos_for_hotel(hotel, booking_date=datetime.date.today()):
        promos_for_date = models.PreferredPromo.objects.filter(
            Q(
                booking_start__lte=booking_date), Q(
                booking_end__gte=booking_date), Q(
                status=1), Q(
                    hotel_id__in=[
                        hotel.id])).prefetch_related('conditions').select_related('hotel')
        return promos_for_date

    @staticmethod
    def get_promos_for_hotels_group_by(
            hotel_ids, booking_date=datetime.date.today()):
        promos_for_date = models.PreferredPromo.objects.filter(
            Q(
                booking_start__lte=booking_date), Q(
                booking_end__gte=booking_date), Q(
                status=1), Q(
                    hotel__in=list(hotel_ids))).prefetch_related('conditions').select_related('hotel')
        group_promos_by_hotels = defaultdict(list)
        for promo in promos_for_date:
            group_promos_by_hotels[promo.hotel.id].append(promo)
        return group_promos_by_hotels

    @staticmethod
    def filter_applicable_promos(
            promotions,
            room_type_code,
            booking_date,
            stayDate,
            stayEnd):
        applicable_promos = []
        if promotions is None:
            return applicable_promos
        for promo in promotions:
            conditions = promo.conditions.all()
            conditionDict = {}
            for condition in conditions:
                conditionDict[condition.condition_name] = condition

            roomTypeList = json.loads(
                conditionDict['RoomTypeList'].condition_params)
            if room_type_code not in roomTypeList:
                continue
            if promo.type == 'Basic':
                applicable_promos.append(promo)
            elif promo.type == 'MinStay':
                if stayEnd is not None:
                    if (stayEnd - stayDate).days >= promo.min_nights != -1:
                        applicable_promos.append(promo)
            elif promo.type == 'EarlyBird':
                if (stayDate - booking_date).days >= promo.min_advance_days != -1:
                    applicable_promos.append(promo)
            elif promo.type == 'LastMinute':
                if (stayDate - booking_date).days <= promo.max_advance_days != -1:
                    applicable_promos.append(promo)
        return applicable_promos

    @staticmethod
    def get_auto_promotion_discount(
            base_price,
            applicable_promos,
            luxury_taxes,
            stay_date,
            state_tax):
        dayList = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday']
        promotion_applied = False
        pretax_price = float(base_price)
        tax = TaxUtils.get_tax_on_price(pretax_price, state_tax, luxury_taxes)
        best_discount_amount = float(0)
        for promo in applicable_promos:
            if stay_date < promo.stay_start or stay_date > promo.stay_end:
                continue
            conditions = promo.conditions.all()
            condition_by_name = {}
            for condition in conditions:
                condition_by_name[condition.condition_name] = condition
            dayWiseDiscounts = condition_by_name['DayWiseDiscountValues']
            dayWiseDiscountDict = json.loads(dayWiseDiscounts.condition_params)
            dayOfWeek = stay_date.weekday()
            discountApplicationValue = dayWiseDiscountDict[dayList[dayOfWeek]]
            temp_discount_amount = DiscountUtils.__get_discount_amount(
                pretax_price, discountApplicationValue, promo.discountType)
            temp_tax = TaxUtils.get_tax_on_price(
                pretax_price - temp_discount_amount, state_tax, luxury_taxes)
            if (pretax_price - temp_discount_amount +
                    temp_tax) < (pretax_price - best_discount_amount + tax):
                tax = temp_tax
                best_discount_amount = temp_discount_amount
                promotion_applied = True
        logger.debug(
            "rackRate %s temp_discount_amount %s",
            base_price,
            best_discount_amount)
        return best_discount_amount, tax, promotion_applied
