__author__ = 'baljeet'
import datetime
import hmac
import logging
from hashlib import sha1

import psycopg2
import requests
from django.conf import settings
from django.core.management import BaseCommand
from pyquery import PyQuery as pQ
from apps.hotels.service.hotel_service import HotelService

from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)

commandObject = None

NAME = settings.DATABASES['default']['NAME']
USER = settings.DATABASES['default']['USER']
PASS = settings.DATABASES['default']['PASSWORD']
HOST = settings.DATABASES['default']['HOST']
PORT = settings.DATABASES['default']['PORT']


def searchWorker(dateRange):
    checkInDate = datetime.datetime.strptime(dateRange, "%Y-%m-%d").date()
    singleDayDelta = datetime.timedelta(days=1)
    __hotelService = HotelService()

    checkOutDate = checkInDate + singleDayDelta
    auth_xml = commandObject.ws_auth_xml()
    ws_auth_signature = commandObject.ws_auth_signature(auth_xml)
    # set what your server acceptsprint headers
    headers = {'Content-Type': 'text/xml',
               'X-HAPI-Signature': ws_auth_signature}

    response = requests.post(
        commandObject.hotelogix_url +
        "wsauth",
        data=auth_xml,
        headers=headers)

    d = pQ(response.content)
    accesskey = d("accesskey").attr("value")
    accesssecret = d("accesssecret").attr("value")
    conn = psycopg2.connect(
        database=NAME,
        host=HOST,
        user=USER,
        password=PASS,
        port=PORT)
    conn.set_isolation_level(0)
    cur = conn.cursor()
    srch_xml = commandObject.basic_search_xml(accesskey, checkInDate)
    search_signature = commandObject.search_signature(accesssecret, srch_xml)
    search_headers = {
        'Content-Type': 'text/xml',
        'X-HAPI-Signature': search_signature}  # set what your server accepts
    st = int(datetime.datetime.now().strftime('%s'))

    responseXml = requests.post(
        commandObject.hotelogix_url +
        "basicratesearch",
        data=srch_xml,
        headers=search_headers)
    et = int(datetime.datetime.now().strftime('%s'))
    logger.info("HX %s took %s seconds" % ('search', et - st))

    responseXml = pQ(responseXml.content)
    logger.info(responseXml)
    responses = responseXml('response')
    singleResponse = responses[0]
    singleResponseXml = pQ(singleResponse)
    hotelogixHotels = singleResponseXml('hotel')

    for singleHotelogixHotel in hotelogixHotels:
        singleHotelogixHotel = pQ(singleHotelogixHotel)
        hotelogixName = singleHotelogixHotel.attr('title')
        logger.info(singleHotelogixHotel)
        logger.info(hotelogixName)
        if hotelogixName is not None:
            hotelogixName = hotelogixName.strip()
            currentHotelModel = commandObject.hotelNameWiseMapping.get(
                hotelogixName)
            if currentHotelModel is None:
                continue
            roomsData = __hotelService.getRoomAvailabilityAndPriceFromBasicSearch(
                currentHotelModel, checkInDate, checkOutDate, singleHotelogixHotel)

            for singleRoom in roomsData:
                logger.info(singleRoom)
                currentQuery = """WITH upsert AS (UPDATE bookingstash_availability SET created_at=now(), modified_at=now(),
                    availablerooms=%(availablerooms)s,date='%(date)s',room_id=%(roomId)s WHERE date='%(date)s' AND room_id=%(roomId)s RETURNING *)
                    INSERT INTO bookingstash_availability (created_at, modified_at,date,availablerooms,room_id)
                    SELECT now(),now(),'%(date)s','%(availablerooms)s','%(roomId)s' WHERE NOT EXISTS (SELECT * FROM upsert);"""
                try:
                    cur.execute(currentQuery, {
                        'date': checkInDate.strftime("%Y-%m-%d"),
                        'availablerooms': singleRoom.availableRooms if hasattr(singleRoom, 'availableRooms') else 0,
                        'roomId': singleRoom.id})
                    conn.commit()
                except Exception as exc:
                    conn.rollback()
    conn.close()


class Command(BaseCommand):
    help = 'Fetch Availability from Hotelogix and save in Database'
    consumer_key = settings.CONSUMER_KEY
    consumer_secret = settings.CONSUMER_SECRET
    hotelogix_url = settings.HOTELOGIX_URL
    bookingdetails_url = settings.BOOKING_DETAILS_URL
    __hotelService = None
    hotelNameWiseMapping = {}
    hotelAvailabilities = []

    def handle(self, *args, **options):
        start = options.get('start')
        delta = int(options.get('delta', 30))
        if start is None:
            startDate = datetime.datetime.now()
        else:
            startDate = datetime.datetime.strptime(start, "%d-%m-%Y").date()

        try:
            self.updateAvailability(startDate, delta)
        except Exception as exc:
            logger.exception(exc)

    def updateAvailability(self, start, delta):
        self.hotelAvailabilities = []

        global commandObject
        commandObject = self

        availabilityPeriod = delta
        singleDayDelta = datetime.timedelta(days=1)
        date_min = start
        date_max = datetime.datetime.combine(
            datetime.date.today() +
            datetime.timedelta(
                days=availabilityPeriod),
            datetime.time.max)
        allHotels = Hotel.objects.all()
        allHotels = list(allHotels)
        for singleHotel in allHotels:
            self.hotelNameWiseMapping[singleHotel.hotelogix_name] = singleHotel
        dateRanges = []
        while date_min < date_max:
            dateRanges.append(date_min.strftime("%Y-%m-%d"))
            date_min = date_min + singleDayDelta
        for curDate in dateRanges:
            searchWorker(curDate)
            # p = multiprocessing.Pool(processes=1)
            # worker = searchWorker
            # p.map(worker, dateRanges)

    def ws_auth_xml(self):
        time = datetime.datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + \
              "'><request method='wsauth' key='" + Command.consumer_key + "'></request></hotelogix>"
        return xml

    def ws_auth_signature(self, xml):
        signature = hmac.new(Command.consumer_secret, xml, sha1)
        ws_auth_signature = signature.hexdigest()
        return ws_auth_signature

    def cancel_xml(self, accesskey, order_id, reservation_id):
        time = datetime.datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + "'><request method='cancel' key='" + accesskey + "'><orderId value='" + order_id + \
              "'/><reservationId value='" + reservation_id + "'/><cancelCharge amount='0'/><cancelDescription>This is a test cancel from webservice</cancelDescription></request></hotelogix>"
        return xml

    def create_signature(self, accesssecret, xml):
        signature = hmac.new(
            str(accesssecret),
            str(xml),
            digestmod=sha1).hexdigest()
        return signature

    def search_xml(self, accesskey, in_date):
        checkin_date = in_date.strftime("%Y-%m-%d")
        checkout_date = (
            in_date +
            datetime.timedelta(
                days=1)).strftime("%Y-%m-%d")
        time = datetime.datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + "'><request method='search' key='" + accesskey + "'><stay checkindate='" + checkin_date + \
              "' checkoutdate='" + checkout_date + "'/><pax adult='1' child='0' infant='0'/><roomrequire value='1'/><limit value='100' offset='0' hasResult='0'/></request></hotelogix>"
        return xml

    def basic_search_xml(self, accesskey, in_date):
        checkin_date = in_date.strftime("%Y-%m-%d")
        checkout_date = (
            in_date +
            datetime.timedelta(
                days=1)).strftime("%Y-%m-%d")
        time = datetime.datetime.utcnow().isoformat()
        hotels = Hotel.objects.filter(status=1)
        hotelogixIdList = []
        for hotel in hotels:
            hotelogixIdList.append(hotel.hotelogix_id)
        hotelsList = """"""
        for hotelogixId in hotelogixIdList:
            hotelsList += """<hotel id="%(hotelogixId)s"/>""" % {
                'hotelogixId': hotelogixId}
        xml = """<?xml version="1.0"?><hotelogix version="1.0" datetime="%(time)s"><request method="basicratesearch" key="%(key)s" languagecode="en"><stay checkindate="%(checkin)s" checkoutdate="%(checkout)s"/><pax adult="1" child="0" infant="0"/><minvaluetype value="1"/><hotels>%(hotelsList)s</hotels></request></hotelogix>""" % {
            'time': time, 'checkin': checkin_date, 'checkout': checkout_date, 'hotelsList': hotelsList,
            'key': accesskey}

        logger.info(xml)
        return xml

    def search_signature(self, accesssecret, xml):
        signature = hmac.new(
            str(accesssecret),
            str(xml),
            digestmod=sha1).hexdigest()
        return signature
