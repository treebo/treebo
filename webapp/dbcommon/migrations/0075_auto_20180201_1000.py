# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0074_auto_20180118_1140'),
    ]

    operations = [
        migrations.CreateModel(
            name='HotelPolicyMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('hotel_id', models.ForeignKey(to='dbcommon.Hotel')),
            ],
            options={
                'ordering': ['id'],
                'abstract': False,
                'verbose_name_plural': 'hotel_policy_map',
                'db_table': 'hotels_hotel_policy_map',
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'verbose_name': 'hotel_policy_map',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Policy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('policy_type', models.CharField(max_length=200)),
                ('title', models.CharField(max_length=200)),
                ('description', models.CharField(max_length=500)),
                ('display_in_need_to_know', models.BooleanField(default=False)),
                ('display_in_policy', models.BooleanField(default=False)),
                ('is_global', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['id'],
                'abstract': False,
                'verbose_name_plural': 'Policies',
                'db_table': 'hotels_policy',
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'verbose_name': 'Policy',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AddField(
            model_name='hotelpolicymap',
            name='policy_id',
            field=models.ForeignKey(to='dbcommon.Policy'),
        ),
        migrations.AddField(
            model_name='hotel',
            name='policies',
            field=models.ManyToManyField(to='dbcommon.Policy', null=True, through='dbcommon.HotelPolicyMap', blank=True),
        ),
    ]
