import logging

from django.core.management import BaseCommand
from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.common.exceptions.custom_exception import UserAlreadyPresentException
from apps.profiles.service.profile_service import UserProfileService
from apps.profiles.service.user_registration import UserRegistration
from common.exceptions.treebo_exception import TreeboException
from dbcommon.models.profile import User

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    auth_client = TreeboAuthClient()
    user_profile_service = UserProfileService()
    user_registration = UserRegistration()

    def add_arguments(self, parser):
        parser.add_argument(
            '--users',
            default='0',
            type=str,
            help='users id list, to which we want to register')
        parser.add_argument(
            '--remove_users',
            default='0',
            type=str,
            help='users id list, to which we want do not want to register')
        parser.add_argument(
            '--register_all',
            default=False,
            type=bool,
            help='to register all users ')
        parser.add_argument(
            '--internal_emp',
            default=False,
            type=bool,
            help='to internal emp all users ')
        parser.add_argument(
            '--start_with_userid',
            default='0',
            type=int,
            help='to start with ')
        parser.add_argument('--limit', default='1000', type=int, help='limit ')

    def handle(self, *args, **options):
        users_id_str = options.get('users')
        remove_users_str = options.get('remove_users')
        users_id_list = [int(userid)
                         for userid in str(users_id_str).strip().split(',')]
        remove_users_list = [int(userid1) for userid1 in str(
            remove_users_str).strip().split(',')]
        register_all = options.get('register_all')
        internal_emp = options.get('internal_emp')
        limit = options.get('limit')
        start_with_userid = options.get('start_with_userid')
        logger.info("users_id_list %s ", users_id_list)
        logger.info(
            "remove_users_list %s %s",
            remove_users_list,
            type(remove_users_list))
        logger.info("register_all %s ", register_all)
        logger.info("start_with_userid %s ", start_with_userid)
        logger.info("limit %s ", limit)

        remove_users = self.fetch_user_from_id(remove_users_list)
        if internal_emp:
            users = list(self.user_profile_service.get_all_internal_users())
        elif register_all:
            users = list(
                self.user_profile_service.get_all_users(start_with_userid))
        else:
            users = self.fetch_user_from_id(users_id_list)
        # logger.info(" users total with removal %d %s ", len(users), users)
        logger.info(
            " users total with removal %d %s ",
            len(users),
            type(users))
        logger.info(
            " remove_users total  %d %s ",
            len(remove_users),
            type(remove_users))
        for user in remove_users:
            if user in users:
                users.remove(user)
        logger.info(" users left %d   ", len(users))
        if len(users) < 20:
            logger.info(" users left details %s  ", users)
        self.register_user_to_auth(users, limit)

    def register_user_to_auth(self, users, limit):
        userid_failed_with_user_already_present = []
        userid_failed_with_exception = []
        userid_failed_with_unknown_exception = []
        userid_sucess = []
        logger.info(" total user found %d for registration ", len(users))
        for index, user in enumerate(users):
            if index == limit:
                logger.info(" limit reached ")
                break
            try:
                self.register_user_in_auth(user)
                userid_sucess.append(user.id)
            except UserAlreadyPresentException as e:
                userid_failed_with_user_already_present.append(user.id)
                logger.error(
                    " auth register failed with user %s (%s) already present %s ",
                    user,
                    user.id,
                    str(e))
            except TreeboException as e:
                userid_failed_with_exception.append(user.id)
                logger.exception(
                    " auth register failed with user %s (%s) with exception %s ",
                    user,
                    user.id,
                    str(e))
            except Exception as e:
                userid_failed_with_unknown_exception.append(user.id)
                logger.exception(
                    " auth register failed with user %s (%s) with unknown exception %s ",
                    user,
                    user.id,
                    str(e))

        logger.info(
            "Registration complete user_already_present  %d\n treebo exception %d\n userid_unknown_exception %d ",
            len(userid_sucess),
            len(userid_failed_with_exception),
            len(userid_failed_with_unknown_exception))
        logger.info("userid_failed_with_user_already_present %s  ",
                    userid_failed_with_user_already_present)
        logger.info(
            "userid_failed_with_exception %s  ",
            userid_failed_with_exception)
        logger.info(
            "userid_failed_with_unknown_exception %s  ",
            userid_failed_with_unknown_exception)
        logger.info("userid_sucess %s  ", userid_sucess)

    def register_user_in_auth(self, user):
        logger.info(" creating user in v2  ")
        phone_number = user.phone_number if user.phone_number else None
        user.email = str(user.email).lower()

        user_dto = {
            'email': user.email,
            "first_name": user.first_name if user.first_name else 'Guest',
            "last_name": user.last_name if user.last_name else '',
            "phone_number": phone_number,
            "password": User.objects.make_random_password(),
        }
        user_details = self.auth_client.user_details(user_dto)
        auth_id = user_details['id']
        email = user_details['email']
        phone = user_details['phone_number']
        first_name = user_details['first_name']
        last_name = user_details['last_name']

        logger.info(
            " user in auth (email %s , phone  %s) auth id %s, user in web ( %s ,%s)   ",
            email,
            phone,
            auth_id,
            user.email,
            user.phone_number)

        try:
            user.auth_id = auth_id
            user.email = email
            user.last_name = last_name
            user.first_name = first_name
            user.phone_number = phone
            user.save()
        except Exception as e:
            logger.error(
                " user %s user save exception not able to create user with ( %s ,%s %s)   ",
                user_details,
                auth_id,
                email,
                phone_number)

    def fetch_user_from_id(self, users_id_list):
        users = list()
        for user_id in users_id_list:
            user = self.user_profile_service.get_user_by_id(user_id)
            if user:
                users.append(user)
        return users
