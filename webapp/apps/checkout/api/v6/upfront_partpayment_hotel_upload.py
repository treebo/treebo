from base.views.api import TreeboAPIView
from base.middlewares.request import get_domain
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED
from django.shortcuts import render
from rest_framework.response import Response
from apps.discounts.constants import UNAUTHORIZED_REQUEST_MESSAGE
from io import StringIO
from apps.checkout.dto.partpay_hotel_enable_upload_request import HotelUploadEntry
from dbcommon.models.hotel import Hotel
from apps.checkout.service.upfront_part_pay_service import UpfrontPartPayService
import csv
import logging

logger = logging.getLogger(__name__)
class UpfrontPartPaymentHotelUpload(TreeboAPIView):
    upfront_partpay_service = UpfrontPartPayService()

    def get(self, request):
        action_url = get_domain() + reverse('upfront_partpay_hotel_upload')
        context = {'action_url': action_url}
        res = render(
            request=request,
            template_name='file_upload.html',
            context=context)
        return HttpResponse(res)

    def post(self, request, *args, **kwargs):
        try:
            csvfile = request.FILES.get('uploaded_file', None)
            user = request.user
            if not (user and user.is_authenticated() and user.is_staff):
                return Response(
                    {"error": UNAUTHORIZED_REQUEST_MESSAGE}, status=HTTP_401_UNAUTHORIZED)
            readCSV = csv.reader(StringIO(csvfile.read().decode()), delimiter=',')
            failed_rows = []
            success_rows = []
            hotel_list_to_enable = []
            hotel_list_to_disable = []
            headers = next(readCSV)
            if not self.validate(headers):
                return Response({"error": 'uploaded file is in incorrect format'}, status=HTTP_400_BAD_REQUEST)
            for row in readCSV:
                try:
                    hotel_upload_record = HotelUploadEntry(hotel_id=row[0], hotel_name=row[1], enable=row[2])
                    Hotel.objects.get(pk=int(hotel_upload_record.hotel_id))
                    if hotel_upload_record.enable.lower() == 'true':
                        hotel_list_to_enable.append(int(hotel_upload_record.hotel_id))
                    elif hotel_upload_record.enable.lower() == 'false':
                        hotel_list_to_disable.append(int(hotel_upload_record.hotel_id))
                    success_rows.append(row)
                except Hotel.DoesNotExist:
                    error_dict = {'row': row, 'error': "No hotel found for the hotel id provided in row"}
                    failed_rows.append(error_dict)
                except Exception as e:
                    logger.exception("Exception while processing record for upfront part pay hotel "
                                     "upload:{row}".format(row=str(row)))
                    error_dict = {'row': row, 'error': str(e)}
                    failed_rows.append(error_dict)
            self.upfront_partpay_service.update_hotel_constraint(hotel_list_to_enable, hotel_list_to_disable)
            return Response({"success_list": success_rows,
                             "failure_list": failed_rows}
                            )
        except Exception:
            logger.exception("Exception while processing the uploaded file")
            return Response({"error": 'Error while processing the uploaded file'}, status=HTTP_500_INTERNAL_SERVER_ERROR)

    def validate(self, row):
        if len(row) < 3:
            return False
        if row[0]!='hotel_id':
            return False
        if row[1] != 'hotel_name':
            return False
        if row[2] != 'enable':
            return False
        return True


