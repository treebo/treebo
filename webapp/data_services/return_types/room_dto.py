from data_services.return_types.dto import DTO


class RoomDto(DTO):

    def __init__(self, properties_dict):
        super(RoomDto, self).__init__(properties_dict)

    def get_occupancy_string(self):
        return self.occupancy_string

    def get_mm_hotel_code(self):
        return self.mm_hotel_code

    def get_mm_room_code(self):
        return self.mm_room_code


class ExternalRoomDetailDto(DTO):

    def __init__(self, external_room_detail_dict):
        super(ExternalRoomDetailDto, self).__init__(external_room_detail_dict)
