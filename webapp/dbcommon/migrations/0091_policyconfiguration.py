# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-09-12 11:49
from __future__ import unicode_literals

import caching.base
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0090_auto_20180904_1402'),
    ]

    operations = [
        migrations.CreateModel(
            name='PolicyConfiguration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('provider', models.CharField(max_length=200)),
                ('enable_global_policy', models.BooleanField(default=True)),
            ],
            options={
                'db_table': 'hotels_policy_configuration',
                'abstract': False,
                'default_permissions': ('add', 'change', 'delete', 'read'),
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
    ]
