# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0016_auto_20160422_0739'),
    ]

    operations = [
        migrations.AddField(
            model_name='hotel',
            name='price_increment',
            field=models.DecimalField(
                default=0,
                max_digits=8,
                decimal_places=2),
            preserve_default=False,
        ),
    ]
