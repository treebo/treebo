import os
import sys

from django.conf import settings

from apps.bookings.models import Reservation

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapp.conf.settings")
from django.core.management import execute_from_command_line

execute_from_command_line(sys.argv)

import logging
from hashlib import sha1
from bs4 import BeautifulSoup
import hmac
from datetime import datetime, timedelta
import requests
from dbcommon.models.room import Room
from dbcommon.models.hotel import Hotel
from data_services.respositories_factory import RepositoriesFactory

from data_services.room_repository import RoomRepository

# Get an instance of a logger
logger = logging.getLogger(__name__)

consumer_key = settings.CONSUMER_KEY
consumer_secret = settings.CONSUMER_SECRET
hotelogix_url = settings.HOTELOGIX_URL
# "http://hotelogix.stayezee.com/ws/webv2/"
booking_details_url = settings.BOOKING_DETAILS_URL
sender_email = 'kulizadev@gmail.com'
recipients = [
    'baljeet.kumar@treebohotels.com',
    'vinay.mishra@treebohotels.com',
    'rahul@treebohotels.com',
    'saurabh.rustagi@treebohotels.com']
username = 'kulizadev@gmail.com'
password = 'kuliza123'


def ws_auth_xml():
    time = datetime.utcnow().isoformat()
    xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + \
        "'><request method='wsauth' key='" + consumer_key + "'></request></hotelogix>"
    return xml


def ws_auth_signature(xml):
    signature = hmac.new(consumer_secret, xml, sha1)
    ws_auth_signature = signature.hexdigest()
    return ws_auth_signature


def search_xml(accesskey, start_date, end_date):
    current_time = datetime.utcnow().isoformat()

    xml = '''<?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing">
  <soap:Header xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:htng="http://htng.org/PWSWG/2007/02/AsyncHeaders">
    <wsa:MessageID>4ce4bec7-8fd2-e495-f023-82231bad9bd9</wsa:MessageID>
    <htng:CorrelationID>4ce4bec7-8fd2-e495-f023-82231bad9bd8</htng:CorrelationID>
    <wsa:To>https://partners.url.com</wsa:To>
    <wsa:Action>http://hotelogix.local/ws/webv2/getreservations</wsa:Action>
    <wsa:ReplyTo>
      <wsa:Address>https://partners.url.com</wsa:Address>
    </wsa:ReplyTo>
    <htng:ReplyTo>
      <wsa:Address/>
    </htng:ReplyTo>
    <wsse:Security xmlns:wsu="http://schemas.xmlsoap.org/ws/2003/06/utility" soap:mustUnderstand="true">
      <wsse:UsernameToken>
        <wsse:Username>''' + accesskey + '''</wsse:Username>
        <wsse:Password/>
        <wsu:Created>''' + current_time + '''</wsu:Created>
      </wsse:UsernameToken>
    </wsse:Security>
  </soap:Header>
  <soap:Body>
    <OTA_ReadRQ TimeStamp="''' + current_time + '''" Version="1">
      <ReadRequests>
        <HotelReadRequest>
          <SelectionCriteria Start="''' + start_date + '''" End="''' + end_date + '''"/>
        </HotelReadRequest>
      </ReadRequests>
    </OTA_ReadRQ>
  </soap:Body>
</soap:Envelope>'''

    return xml


def create_signature(accesssecret, xml):
    signature = hmac.new(
        str(accesssecret),
        str(xml),
        digestmod=sha1).hexdigest()
    return signature


def get_reservations():
    start_date = (datetime.now() + timedelta(days=-1)).strftime("%Y-%m-%d")
    end_date = (datetime.now() + timedelta(days=1)).strftime(
        "%Y-%m-%d")  # datetime.now().strftime("%Y-%m-%d")
    auth_xml = ws_auth_xml()
    auth_signature = ws_auth_signature(auth_xml)
    # set what your server acceptsprint headers
    headers = {'Content-Type': 'text/xml', 'X-HAPI-Signature': auth_signature}

    response = requests.post(
        hotelogix_url + "wsauth",
        data=auth_xml,
        headers=headers)
    resp_text = response.text
    print(resp_text)
    xml = BeautifulSoup(resp_text, "xml")
    accesskey = xml.hotelogix.response.accesskey["value"]
    accesssecret = xml.hotelogix.response.accesssecret["value"]

    print((accesskey, accesssecret))

    srch_xml = search_xml(accesskey, start_date, end_date)
    print(srch_xml)
    search_signature = create_signature(accesssecret, srch_xml)
    search_headers = {
        'Content-Type': 'text/xml',
        'X-HAPI-Signature': search_signature}  # set what your server accepts

    url = booking_details_url + "getreservations"
    response = requests.post(url, data=srch_xml, headers=search_headers)
    print((response.status_code))
    processed = 0
    if response.status_code == 200:
        xml = BeautifulSoup(response.text, "xml")
        reservations = xml.findAll("HotelReservation")
        for reservation in reservations:
            processed += 1
            print(reservation)
            print((reservation["ResStatus"] + " " +
                   reservation.UniqueID["ID"] + " "))
            save_reservations(reservation)
    else:
        print("Oops! something went wrong")
    print(("Total reservations processed = " + str(processed)))


def save_reservations(reservation):
    reservation_obj = Reservation()
    reservation_obj.reservation_id = reservation.UniqueID["ID"]
    reservation_obj.reservation_status = reservation["ResStatus"]
    reservation_obj.reservation_checkin_date = datetime.strptime(
        reservation.find('TimeSpan')["Start"], '%Y-%m-%d')
    reservation_obj.reservation_checkout_date = datetime.strptime(
        reservation.find('TimeSpan')["End"], '%Y-%m-%d')
    reservation_obj.adult_count = reservation.find('GuestCount')["Count"]
    reservation_obj.total_amount = reservation.find(
        'Rate').Base["AmountAfterTax"]
    reservation_obj.room_unit = reservation.find("RoomRate")["NumberOfUnits"]
    hotelogix_id = reservation.find(
        reservation.find("BasicPropertyInfo")["HotelCode"])
    try:
        hotel = Hotel.objects.get(hotelogix_id=hotelogix_id)
        #hotel = hotel_repository.get_single_hotel(hotelogix_id=hotelogix_id)
    except Exception as e:
        message = "exception is " + \
            str(e) + " " + str(hotelogix_id) + " " + reservation.UniqueID["ID"]
        print(message)
        hotel = None
    reservation_obj.hotel = hotel
    if reservation_obj.hotel is not None:
        reservation_obj.room = get_room_from_hotelogix(
            reservation.find("RoomRate")["RoomTypeCode"], reservation_obj.hotel.id)
    reservation_obj.guest_name = reservation.find(
        "Customer").PersonName.GivenName.string
    reservation_obj.guest_email = reservation.find("Customer").Email.string
    reservation_obj.guest_mobile = get_telephone_number(
        reservation.findAll("Telephone"))
    reservation_obj.reservation_create_time = datetime.strptime(
        reservation["CreateDateTime"], '%Y-%m-%d %H:%M:%S')
    savedReservation = get_saved_reservation(reservation_obj.reservation_id)
    if savedReservation is None:
        print("Saving reservation , creating new entry")
        reservation_obj.save()
    else:
        print(("Updating status of already present reservation , ReservationId " +
               str(reservation_obj.reservation_id)))
        savedReservation.reservation_status = reservation_obj.reservation_status
        savedReservation.save()


def get_room_from_hotelogix(room_type_code, hotel_id):
    try:
        room_data_service = RepositoriesFactory.get_room_repository()
        print((str(hotel_id) + " " + str(room_type_code)))
        string_matching_args = {
            'name': room_type_code,
            'value': room_type_code,
            'case_sensitive': False,
            'partial': False}
        return room_data_service.filter_room_by_string_matching(
            fields_list=[string_matching_args], filter_params={'hotel': hotel_id})
    except Room.DoesNotExist:
        print("Room not found")
        return None


def get_saved_reservation(reservation_id):
    try:
        return Reservation.objects.get(reservation_id=reservation_id)
    except Reservation.DoesNotExist:
        print("Reservation not found")
        return None


def get_telephone_number(telephones):
    for telephone in telephones:
        try:
            return telephone["PhoneNumber"]
        except Exception as exc:
            print(exc)


def main():
    get_reservations()


if __name__ == '__main__':
    main()
