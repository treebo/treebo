from django.db import models
from djutil.models import TimeStampedModel
from jsonfield import JSONField
from django.db.models.signals import post_save
from django.core.cache import cache

from dbcommon.models.default_permissions import DefaultPermissions

__author__ = 'baljeet'


class ContentStore(TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200)
    value = JSONField(default='')
    version = models.PositiveIntegerField(default=1)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Content Store'
        verbose_name_plural = 'Content Stores'

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s - %s' % (self.name, self.version)

    def __str__(self):  # __unicode__ on Python 2
        return '%s - %s' % (self.name, self.version)


def clear_cache(sender, instance, **kwargs):
    cache.delete_pattern("meta_*")


post_save.connect(clear_cache, sender=ContentStore)
