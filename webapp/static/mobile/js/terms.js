'use strict';

import BaseView from './treebolibs/baseview';
import logger from './treebolibs/logger';
import '../css/terms.less';

let TermsView = Object.create(BaseView);

TermsView.pageLoaded = () => {
    logger.log(" Terms page loaded");
}

let termsConfig = { name:'', widgets:[]};
let termsView = TermsView.setup(termsConfig);
