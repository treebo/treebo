# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('content', '0003_auto_20160518_1305'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contentstore',
            options={
                'verbose_name': 'Content Store',
                'verbose_name_plural': 'Content Stores'},
        ),
    ]
