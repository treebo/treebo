import $ from 'jquery';
import BaseView from './treebolibs/baseview';
import parsley from 'parsleyjs';

let auth = require('./auth');
let SignupView = Object.create(BaseView);

SignupView.pageLoaded = () => {
    $("#signupButton").on('click', (e)=>{
        const $form = $('#signupForm');
        const data = {};
        $form.serializeArray().forEach((x) => {
            data[x.name] = x.value.trim();
        });

        $form.parsley().on('form:submit', () => {
            e.preventDefault();
            let p = auth.signup(data, treeboError => {
                $('.signup__error').html(treeboError.message).show();
            });
            p.then(()=>{
                location.reload();
            })
            return false;
        });
    })

    $("#fbSignup").on('click', ()=>{
        auth
            .fbLogin()
            .always(() => location.reload());
    })

    const onSignIn = (googleUser) => {
        auth
            .sendGoogleRegistrationDataToServer(googleUser.getAuthResponse().id_token)
            .finally(() => location.reload());
    };

    const onSignInFailure = () => {
        // Handle sign-in errors
    };

    auth
        .getGoogleHandler()
        .then((auth2) => {
            auth2.attachClickHandler('googleLogin', {}, onSignIn, onSignInFailure);
            auth2.attachClickHandler('googleSignup', {}, onSignIn, onSignInFailure);
        });

};


let signupConfig = {
    name: '',
    widgets: []
};
let signupView = SignupView.setup(signupConfig);
