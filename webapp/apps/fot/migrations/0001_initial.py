# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0022_auto_20160608_1040'),
    ]

    operations = [
        migrations.CreateModel(
            name='FotHotel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('credits_per_week', models.IntegerField()),
                ('status', models.CharField(max_length=100)),
                ('hotel', models.ForeignKey(to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_fothotel',
            },
        ),
    ]
