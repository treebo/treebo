import logging
from difflib import SequenceMatcher

from caching.base import CachingManager
from django.db.models import Q

logger = logging.getLogger(__name__)


class CityManager(CachingManager):

    def get_queryset(self):
        return super(CachingManager, self). \
            get_queryset()

    def get_by_slug(self, slug):
        from dbcommon.models.location import CityAlias, City

        try:
            city_name_from_slug = self.get(slug=slug).name
            city_name = city_name_from_slug.replace('-', ' ')

        except BaseException:
            city_name = slug.replace('-', ' ')

        city_name = city_name.title()
        try:
            city_name_from_alias = CityAlias.objects.get(alias=city_name).city
            try:
                city = City.objects.get(name__iexact=city_name_from_alias)
            except City.DoesNotExist:
                return None
        except CityAlias.DoesNotExist:
            try:
                city = City.objects.get(name=city_name)
            except City.DoesNotExist:
                return None
        except BaseException:
            return None
        return city

    @staticmethod
    def _build_in_contains_query(query_params: [] = None):
        # NOTE: This will be build the query to match record with list
        # and case insensitive on name column
        query_to_be_run = (Q(name__icontains=query_params[0]))
        for query_param in query_params[1:]:
            query_to_be_run |= Q(name__icontains=query_param)
        return query_to_be_run

    @staticmethod
    def get_higher_similarity_score_match(alias, city, search_query):
        # NOTE: Closest match between alias and city with search query
        closet_match_score = 0.0
        closet_match = None
        for value in (alias, city):
            score = SequenceMatcher(None, search_query, value).ratio()
            if score > closet_match_score:
                closet_match = value
                closet_match_score = score
        return closet_match

    def get_all_cities_for_search_string(self, query, alias_query, original_search_query=None):
        city = None
        try:
            from dbcommon.models.location import CityAlias, City
            city_aliases = CityAlias.objects.filter(alias_query)
            if not city_aliases:
                return []
            closest_match_city_aliases = set()
            for city_alias in city_aliases:
                if city_alias.alias.lower() != city_alias.city.lower():
                    closest_match_city_aliases.add(self.get_higher_similarity_score_match(
                        city_alias.alias.lower(),
                        city_alias.city.lower(),
                        original_search_query.lower()
                    ))
                else:
                    closest_match_city_aliases.add(city_alias.city.lower())

            closest_match_city_aliases = self._build_in_contains_query(
                list(closest_match_city_aliases))
            city = City.objects.filter(closest_match_city_aliases)
            return city
        except Exception as e:
            logger.exception(
                "Exception in searching city name for auto complete for query %s and alias_query %s",
                query,
                alias_query)
        return city
