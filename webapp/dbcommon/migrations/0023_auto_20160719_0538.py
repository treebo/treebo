# -*- coding: utf-8 -*-


import uuid

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0022_auto_20160608_1040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hotel',
            name='uuid',
            field=models.UUIDField(
                default=uuid.uuid4,
                max_length=32,
                null=True,
                blank=True),
        ),
    ]
