# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0027_auto_20160713_1525'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userreferralreward',
            name='friend',
        ),
        migrations.RemoveField(
            model_name='userreferralreward',
            name='user',
        ),
        migrations.AddField(
            model_name='userreferralreward',
            name='referee',
            field=models.ForeignKey(
                related_name='referee',
                db_column='referee',
                default=823,
                to=settings.AUTH_USER_MODEL,
                on_delete=models.DO_NOTHING),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userreferralreward',
            name='referrer',
            field=models.ForeignKey(
                db_column='referrer',
                default=823,
                to=settings.AUTH_USER_MODEL,
                on_delete=models.DO_NOTHING),
            preserve_default=False,
        ),
    ]
