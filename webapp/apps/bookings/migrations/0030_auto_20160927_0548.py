# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0029_bookingrequest_otp_verified_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='voucher_amount',
            field=models.DecimalField(
                default=0,
                null=True,
                max_digits=20,
                decimal_places=2),
        ),
        migrations.AlterField(
            model_name='booking',
            name='booking_status',
            field=models.CharField(
                default='Tentative',
                max_length=50,
                null=True,
                blank=True,
                choices=[
                        ('Tentative',
                         'Tentative'),
                        ('Awaiting HX Confirmation',
                         'Awaiting HX Confirmation'),
                        ('Confirm',
                         'Confirm'),
                        ('Cancel',
                         'Cancel'),
                        ('Cancellation Pending',
                         'Cancellation Pending'),
                        ('Failure',
                         'Failure')]),
        ),
        migrations.AlterField(
            model_name='booking',
            name='comments',
            field=models.TextField(
                default='',
                blank=True),
        ),
    ]
