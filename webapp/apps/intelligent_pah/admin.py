from django.contrib import admin
from .models.intelligent_pah_categories import Category
from .models.intelligent_pah_occupancy_range import Occupancy
from .models.intelligent_pah_config import Configuration
from .models.intelligent_pah_auditing import Audit
from dbcommon.admin import ReadOnlyAdminMixin


@admin.register(Configuration)
class ConfigurationAdmin(admin.ModelAdmin):
    list_display = (
        'enable_app',
        'white_list',
        'enable_for_all_hotels',
        'black_list')


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'days_to_check_in', 'start_time', 'end_time')
    search_fields = ('name',)


@admin.register(Occupancy)
class OccupancyAdmin(admin.ModelAdmin):
    list_display = ('category', 'start', 'end')


@admin.register(Audit)
class AuditAdmin(admin.ModelAdmin):
    list_display = (
        'check_in_date',
        'bid',
        'hotel_id',
        'timestamp',
        'occupancy',
        'is_pah_enabled')
