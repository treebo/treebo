import logging
from datetime import datetime

import requests
from django.conf import settings
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR
from apps.bookingstash.utils import StashUtils
from apps.hotels.service.hotel_service import HotelEnquiryValidator
from apps.pricing.exceptions import SoaError, RequestError
from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.pricing_api_impl import PriceUtilsV2
from apps.pricing.utils import PriceUtils
from apps.common import date_time_utils
from common.exceptions.treebo_exception import TreeboValidationException
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from data_services.exceptions import HotelDoesNotExist
from apps.common.slack_alert import SlackAlertService as slack_alert

import json

logger = logging.getLogger(__name__)


class PricingService():

    def __init__(self):
        pass

    @staticmethod
    def get_price_from_soa(
            checkin,
            checkout,
            hotel_ids,
            room_config,
            coupon_code=None,
            coupon_value=None,
            coupon_type=None,
            include_price_breakup=True,
            get_from_cache=False,
            channel=None,
            utm_source=None):
        room_config_str = ''
        room_config_list = room_config.split(',')
        for config in room_config_list:
            room_config_str += '&room_config=' + config

        hotel_repository = RepositoriesFactory.get_hotel_repository()
        final_hotel_ids = [
            hotel.id for hotel in hotel_repository.get_hotels_for_id_list(
                hotel_ids=hotel_ids)]

        checkin_date = datetime.strptime(checkin, '%Y-%m-%d')
        checkout_date = datetime.strptime(checkout, '%Y-%m-%d')

        if len(final_hotel_ids) == 0:
            raise RequestError('Hotel id not present', HTTP_400_BAD_REQUEST)

        try:
            # TODO: Changed it to avoid error in testing. Verify if it needs to be so
            # TODO: Temp change only. Review
            HotelEnquiryValidator(
                checkin_date,
                checkout_date,
                room_config_list).validate()
        except TreeboValidationException as e:
            raise RequestError(str(e), HTTP_400_BAD_REQUEST)
        except Exception as e:
            raise RequestError(str(e), HTTP_500_INTERNAL_SERVER_ERROR)

        hotel_list = ''
        for hid in final_hotel_ids:
            hotel_list += '&hotel_id=' + str(hid)

        url = settings.BASE_PRICING_URL_V1 + \
            "stay_start={0}&stay_end={1}".format(checkin, checkout) + room_config_str
        url += hotel_list

        if coupon_code and coupon_value and coupon_type:
            url += ("&coupon_type={0}&coupon_code={1}&coupon_value={2}".format(
                coupon_type, coupon_code, round(coupon_value, 2)))

        if include_price_breakup:
            price_breakup = "1"
        else:
            price_breakup = "0"

        if get_from_cache:
            use_cache = "1"
        else:
            use_cache = "0"

        url += "&use_cache=%s&price_breakup=%s" % (use_cache, price_breakup)
        if channel:
            url += "&channel=%s" % channel
        if utm_source:
            url += "&utm_source=%s" % utm_source

        response = requests.get(url)
        data = response.json()
        logger.info("Pricing api called %s and response %s ", url, data)
        if response.status_code != requests.codes.ok:

            if response.status_code != 400:
                slack_alert.send_slack_alert_for_third_party(status=response.status_code, url=url,
                                                             class_name="PricingService")

            raise SoaError(
                response.status_code,
                data['errors'],
                response.status_code)

        return data

    @staticmethod
    def get_from_local(checkin, checkout, hotel_ids, roomconfig):
        room_config_list = StashUtils.room_config_string_to_list(roomconfig)

        checkin_date = datetime.strptime(checkin, date_time_utils.DATE_FORMAT)
        checkout_date = datetime.strptime(
            checkout, date_time_utils.DATE_FORMAT)
        if hotel_ids:
            hotel_ids = [int(hotel_id) for hotel_id in hotel_ids]
            hotels = Hotel.objects.filter(id__in=hotel_ids).select_related(
                'state').prefetch_related('rooms').all()
        else:
            hotels = Hotel.objects.filter(status=1).select_related(
                'state').prefetch_related('rooms')
            #hotels = hotel_repository.filter_hotels(related_entities_list=['state', 'rooms'], status=1)
        cheapest_room_type_with_availability = StashUtils.get_cheapest_room_type_from_availability_sync(
            hotels, checkin_date, checkout_date, room_config_list)

        if FeatureToggleAPI.is_enabled("pricing", "v2_impl_enabled", False):
            hotel_wise_prices = PriceUtilsV2.getPriceForMultipleHotelsWithRoomConfigDateRange(
                checkin_date, checkout_date, hotels, room_config_list, cheapest_room_type_with_availability)
        else:
            hotel_wise_prices = PriceUtils.getPriceForMultipleHotelsWithRoomConfigDateRange(
                checkin_date, checkout_date, hotels, room_config_list, cheapest_room_type_with_availability)
        for k, v in list(hotel_wise_prices.items()):
            v['cheapest_room'] = cheapest_room_type_with_availability.get(k)

        return hotel_wise_prices
