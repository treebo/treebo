# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0047_auto_20170410_1247'),
    ]

    operations = [
        migrations.CreateModel(
            name='LandmarkSeoDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('landmark_seo_title', models.CharField(max_length=500)),
                ('landmark_seo_description', models.TextField(null=True, blank=True)),
                ('landmark_seo_keyword', models.TextField(null=True, blank=True)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'hotels_landmarkseodetails',
                'verbose_name': 'Landmark SEO Details',
                'verbose_name_plural': 'Landmark SEO Details',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AddField(
            model_name='searchlandmark',
            name='status',
            field=models.IntegerField(default=1, choices=[(0, 'Disabled'), (1, 'Enabled')]),
        ),
        migrations.AlterField(
            model_name='searchdistance',
            name='hotel',
            field=models.ForeignKey(related_name='landmarks', to='dbcommon.Hotel', on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='landmarkseodetails',
            name='landmark',
            field=models.ForeignKey(to='dbcommon.SearchLandmark', on_delete=django.db.models.deletion.CASCADE),
        ),
    ]
