from rest_framework import serializers

from apps.bookings.models import PaymentOrder


class PaymentOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentOrder
        fields = (
            'id',
            'order_id',
            'status',
            'amount',
            'gateway_type',
            'ps_order_id',
            'pg_order_id')
