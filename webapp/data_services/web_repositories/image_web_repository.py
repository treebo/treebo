from base import log_args
from dbcommon.models.miscellanous import Image


class ImageWebRepository():

    @log_args()
    def get_all_images(self):
        return Image.objects.all()

    @log_args()
    def filter_images(self, *args, **kwargs):
        return Image.objects.filter(**kwargs)

    @log_args()
    def get_single_image(self, *args, **kwargs):
        return Image.objects.get(**kwargs)

    @log_args()
    def get_images_for_hotels(self, hotels, **kwargs):
        return Image.objects.filter(hotel_id__in=hotels)
