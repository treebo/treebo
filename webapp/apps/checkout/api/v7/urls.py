from django.conf.urls import url

from apps.checkout.api.v7.apply_coupon import ApplyCoupon
from apps.checkout.api.v7.remove_coupon import RemoveCoupon
from apps.checkout.api.v7.apply_wallet import ApplyWallet
from apps.checkout.api.v7.remove_wallet import RemoveWallet
from apps.checkout.api.v7.itinerary_page_api import ItineraryPageAPI

app_name = 'checkout_v7'

urlpatterns = [
    url(r'^coupon/$', ApplyCoupon.as_view(), name='apply-coupon-v7'),
    url(r'^coupon/remove/$', RemoveCoupon.as_view(), name='remove-coupon-v7'),
    url(r'^wallet/apply/$', ApplyWallet.as_view(), name='wallet-apply-v7'),
    url(r'^wallet/remove/$', RemoveWallet.as_view(), name='wallet-remove-v7'),
    url(r'^itinerary/$', ItineraryPageAPI.as_view(), name='itinerary-page-v7'),
]
