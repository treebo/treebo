import logging
import uuid

from django.conf import settings
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from apps.hotels.api.v2.hotel_statuses import HotelStatus
from apps.hotels.api.v2.localities import Localities

from apps.hotels.helpers import CityHotelHelper
from apps.hotels.service.hotel_policy_service import HotelPolicyService
from apps.hotels.api.v3.hotel_detail import HotelDetail

from dbcommon.models.landmark import SearchLandmark
from apps.search.services.search import SearchService
from dbcommon import event_publishers
from dbcommon.models.facilities import Facility, CityAmenityMapforAutoPageCreation
from dbcommon.models.hotel import Hotel, HotelMetaData, HotelPolicyMap, PolicyConfiguration
from dbcommon.models.location import Locality, City, State, CityAlias
from dbcommon.models.miscellanous import Image
from dbcommon.models.room import Room

from dbcommon.models.hotel import Categories
from apps.seo.components.hotel_category_in_location import HotelCategoryInLocation
from apps.seo.components.hotel_category_near_landmark import HotelCategoryNearLandmark
from dbcommon.models.miscellanous import CityCategoryMapforAutoPageCreation
from apps.hotels.api.v1.facilities import AllFacilitiesAPI
from apps.seo.tasks import check_and_update_hotel_localities_landmark_seo, \
    check_and_update_locality_seo_if_hotel_unavailable, \
    check_and_update_landmark_seo_if_hotel_unavailable, \
    check_and_update_city_category_seo_if_hotel_unavailable, \
    check_and_update_categories_seo_from_category_id
from dbcommon.models.feature_toggle import FeatureToggle
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.reviews.models import ReviewAppConfiguration
from apps.seo.components.top_reviews import TopReviews
from apps.content.models import ContentStore
from apps.content.services.content_store_service import ContentStoreService

logger = logging.getLogger(__name__)


def invalidate_caches_on_hotel_update(instance, created):
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    from apps.search.services.auto_completion_service import AutoCompletionService
    from apps.hotels.api.v3.hotel_detail import HotelDetail
    from apps.seo.pages_v3.city_page import CityPage
    AutoCompletionService.invalidate_hotel_cache()
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()
    SearchService.invalidate_cache()
    if not created:
        HotelDetail.invalidate_cache(instance.id)
    HotelStatus.invalidate_cache()

    category_location_mapping_list_to_delete = HotelCategoryInLocation.generate_hotel_mapping_list_to_delete(instance)
    HotelCategoryInLocation.delete_all_key_in_cache(category_location_mapping_list_to_delete)

    category_landmark_mapping_list_to_delete = HotelCategoryNearLandmark.generate_hotel_mapping_list_to_delete(instance)
    HotelCategoryNearLandmark.delete_all_key_in_cache(category_landmark_mapping_list_to_delete)

    CityPage.delete_page_meta_cache(instance.city.name)
    AllFacilitiesAPI.invalidate_cache()


@receiver([post_save], sender=Hotel, dispatch_uid=uuid.uuid4)
def on_hotel_updated(sender, instance, created, *args, **kwargs):
    logger.debug("On hotel updated signal..")
    try:
        invalidate_caches_on_hotel_update(instance=instance, created=created)
    except Exception as e:
        logger.exception(e)
    event_publishers.publish_hotel(instance=instance)
    check_and_update_hotel_localities_landmark_seo.delay(str(instance.latitude), str(instance.longitude))
    check_and_update_city_category_seo_if_hotel_unavailable.delay(str(instance.city.id))


@receiver([post_delete], sender=Hotel, dispatch_uid=uuid.uuid4)
def on_hotel_deleted(sender, instance, *args, **kwargs):
    from apps.seo.pages_v3.city_page import CityPage
    category_location_mapping_list_to_delete = HotelCategoryInLocation.generate_hotel_mapping_list_to_delete(instance)
    HotelCategoryInLocation.delete_all_key_in_cache(category_location_mapping_list_to_delete)

    category_landmark_mapping_list_to_delete = HotelCategoryNearLandmark.generate_hotel_mapping_list_to_delete(instance)
    HotelCategoryNearLandmark.delete_all_key_in_cache(category_landmark_mapping_list_to_delete)

    CityPage.delete_page_meta_cache(instance.city.name)
    check_and_update_hotel_localities_landmark_seo.delay(str(instance.latitude), str(instance.longitude))
    check_and_update_city_category_seo_if_hotel_unavailable.delay(str(instance.city.id))


@receiver([post_save], sender=Locality, dispatch_uid=uuid.uuid4)
@receiver([post_save], sender=City)
@receiver([post_save], sender=State)
def on_locality_updated(sender, instance, created, *args, **kwargs):
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    from apps.search.services.auto_completion_service import AutoCompletionService
    from apps.hotels.api.v2.cities import Cities
    from apps.seo.pages_v3.city_page import CityPage

    AutoCompletionService.invalidate_locality_cache()
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()
    SearchService.invalidate_cache()
    CityHotelHelper.invalidate_cache()
    Localities.invalidate_cache()
    if sender == City:
        Cities.invalidate_cache()
        check_and_update_city_category_seo_if_hotel_unavailable.delay(str(instance.id))
    if sender == Locality and not created:
        HotelCategoryInLocation.invalidate_hotel_category_with_locality_cache(instance.name,instance.city.name)
        check_and_update_locality_seo_if_hotel_unavailable.delay(str(instance.id))
        CityPage.delete_page_meta_cache(instance.city.name)


@receiver([post_save], sender=CityAlias)
def on_cityalias_updated(sender, instance, created, *args, **kwargs):
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()
    SearchService.invalidate_cache()


# @receiver([m2m_changed], sender=Facility.hotels.through)
# @receiver([m2m_changed], sender=Facility.rooms.through)
# def on_facility_update(sender, instance, *args, **kwargs):
#    logger.info("Type of instance: %s", type(instance))

@receiver([post_save], sender=Facility)
@receiver([post_save], sender=Image)
@receiver([post_save], sender=Room)
def on_hotel_relations_updated(sender, instance, created, *args, **kwargs):
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    from apps.hotels.api.v2.hotel_detail import HotelDetail
    if sender == Room:
        event_publishers.publish_hotel(instance.hotel)
    if sender != Room and sender != Facility:
        SearchAPI.invalidate_cache()
        SearchApiV4.invalidate_cache()
        SearchService.invalidate_cache()

    logger.info(
        "Hotel Relation Updated Signal for sender: %s and Instance ID: %s",
        sender,
        instance.id)
    # if sender != Facility:
    # #     hotels = instance.hotels.all()
    # #     logger.info(
    # #         "Invalidating hotel id: %s", [
    # #             hotel.id for hotel in hotels])
    # #     for hotel in hotels:
    # #         HotelDetail.invalidate_cache(hotel.id)
    #     HotelDetail.invalidate_cache(instance.hotel.id)


# DELETE Signals

@receiver([post_delete], sender=Locality)
@receiver([post_delete], sender=City)
@receiver([post_delete], sender=State)
def on_locality_deleted(sender, instance, *args, **kwargs):
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    from apps.search.services.auto_completion_service import AutoCompletionService
    from apps.hotels.api.v2.cities import Cities
    from apps.seo.pages_v3.city_page import CityPage
    AutoCompletionService.invalidate_locality_cache()
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()
    Localities.invalidate_cache()
    if sender == City:
        Cities.invalidate_cache()
        check_and_update_city_category_seo_if_hotel_unavailable.delay(str(instance.id))
    if sender == Locality:
        HotelCategoryInLocation.invalidate_hotel_category_with_locality_cache(instance.name, instance.city.name)
        CityPage.delete_page_meta_cache(instance.city.name)


@receiver([post_delete], sender=CityAlias)
def on_cityalias_deleted(sender, instance, *args, **kwargs):
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()
    Localities.invalidate_cache()


# @receiver([m2m_changed], sender=Facility.hotels.through)
# @receiver([m2m_changed], sender=Facility.rooms.through)
# def on_facility_update(sender, instance, *args, **kwargs):
#    logger.info("Type of instance: %s", type(instance))

@receiver([post_delete], sender=Facility)
@receiver([post_delete], sender=Image)
@receiver([post_delete], sender=Room)
def on_hotel_relations_deleted(sender, instance, *args, **kwargs):
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    from apps.hotels.api.v2.hotel_detail import HotelDetail
    if sender == Room:
        event_publishers.publish_hotel(instance.hotel)
    if sender != Room:
        SearchAPI.invalidate_cache()
        SearchApiV4.invalidate_cache()

    logger.info(
        "Hotel Relation Updated Signal for sender: %s and Instance ID: %s",
        sender,
        instance.id)
    if sender == Facility:
        hotels = instance.hotels.all()
        logger.info(
            "Invalidating hotel id: %s", [
                hotel.id for hotel in hotels])
        for hotel in hotels:
            HotelDetail.invalidate_cache(hotel.id)
    else:
        HotelDetail.invalidate_cache(instance.hotel.id)


@receiver([post_save, post_delete], sender=HotelMetaData)
def on_hotel_meta_data_updated(sender, instance, *args, **kwargs):
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    from apps.search.services.auto_completion_service import AutoCompletionService
    AutoCompletionService.invalidate_hotel_cache()
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()
    SearchService.invalidate_cache()


@receiver([post_delete], sender=HotelPolicyMap, dispatch_uid=uuid.uuid4)
def on_hotel_policy_delete(sender, instance, *args, **kwargs):
    HotelPolicyService.send_email_on_hotel_policy_update(
        instance.hotel_id_id,
        instance.policy_id_id,
        'email/booking/detach_hotel_policy.html',
        settings.HOTEL_POLICY_SUBJECT_ON_DETACH)
    from apps.hotels.api.v3.hotel_detail import HotelDetail
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    HotelDetail.invalidate_cache(instance.hotel_id_id)
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()


@receiver([post_save], sender=HotelPolicyMap, dispatch_uid=uuid.uuid4)
def on_hotel_policy_add(sender, instance, *args, **kwargs):
    HotelPolicyService.send_email_on_hotel_policy_update(
        instance.hotel_id_id,
        instance.policy_id_id,
        'email/booking/add_hotel_policy.html',
        settings.HOTEL_POLICY_SUBJECT_ON_ADD)
    from apps.hotels.api.v3.hotel_detail import HotelDetail
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4
    HotelDetail.invalidate_cache(instance.hotel_id_id)
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()


@receiver([post_save], sender=Categories,  dispatch_uid=uuid.uuid4)
def on_dbcommon_categories_update(sender, instance, *args, **kwargs):
    if instance is not None:
        category_location_mapping_list_to_delete = HotelCategoryInLocation.generate_category_mapping_list_to_delete(instance)
        HotelCategoryInLocation.delete_all_key_in_cache(category_location_mapping_list_to_delete)
        #HotelCategoryInLocation.update_all_key_in_cache(category_location_mapping_list_to_delete)

        category_landmark_mapping_list_to_delete = HotelCategoryNearLandmark.generate_category_mapping_list_to_delete(instance)
        HotelCategoryNearLandmark.delete_all_key_in_cache(category_landmark_mapping_list_to_delete)
        #HotelCategoryNearLandmark.update_all_key_in_cache(category_landmark_mapping_list_to_delete)

        check_and_update_categories_seo_from_category_id.delay(str(instance.id))


@receiver([post_delete], sender=Categories,  dispatch_uid=uuid.uuid4)
def on_dbcommon_categories_delete(sender, instance, *args, **kwargs):
    if instance is not None:
        category_location_mapping_list_to_delete = HotelCategoryInLocation.generate_category_mapping_list_to_delete(instance)
        HotelCategoryInLocation.delete_all_key_in_cache(category_location_mapping_list_to_delete)

        category_landmark_mapping_list_to_delete = HotelCategoryNearLandmark.generate_category_mapping_list_to_delete(instance)
        HotelCategoryNearLandmark.delete_all_key_in_cache(category_landmark_mapping_list_to_delete)

        check_and_update_categories_seo_from_category_id.delay(str(instance.id))


@receiver([post_save], sender=SearchLandmark, dispatch_uid=uuid.uuid4)
def on_landmark_updated(sender, instance, created, *args, **kwargs):
    from apps.seo.pages_v3.city_page import CityPage
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4

    if not created:
        HotelCategoryNearLandmark.invalidate_hotel_category_with_landmark_cache(instance.seo_url_name, instance.city.name)
        check_and_update_landmark_seo_if_hotel_unavailable.delay(str(instance.id))
        CityPage.delete_page_meta_cache(instance.city.name)
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()
    SearchService.invalidate_cache()


@receiver([post_delete], sender=SearchLandmark, dispatch_uid=uuid.uuid4)
def on_landmark_deleted(sender, instance, *args, **kwargs):
    from apps.seo.pages_v3.city_page import CityPage
    from apps.search.api.v2.search_api import SearchAPI
    from apps.search.api.v4.search_api import SearchApiV4

    HotelCategoryNearLandmark.invalidate_hotel_category_with_landmark_cache(instance.seo_url_name, instance.city.name)
    check_and_update_landmark_seo_if_hotel_unavailable.delay(str(instance.id))
    CityPage.delete_page_meta_cache(instance.city.name)
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()
    SearchService.invalidate_cache()

@receiver([post_save], sender=CityAmenityMapforAutoPageCreation, dispatch_uid=uuid.uuid4)
def on_city_amenity_map_updated(sender, instance, created, *args, **kwargs):
    from apps.seo.pages_v3.city_page import CityPage
    CityPage.delete_page_meta_cache(instance.city.name)


@receiver([post_delete], sender=CityAmenityMapforAutoPageCreation, dispatch_uid=uuid.uuid4)
def on_city_amenity_map_deleted(sender, instance, *args, **kwargs):
    from apps.seo.pages_v3.city_page import CityPage
    CityPage.delete_page_meta_cache(instance.city.name)


@receiver([post_save], sender=CityCategoryMapforAutoPageCreation, dispatch_uid=uuid.uuid4)
def on_city_category_map_updated(sender, instance, created, *args, **kwargs):
    category_location_mapping_list_to_delete = HotelCategoryInLocation.generate_city_mapping_list_to_delete(instance)
    HotelCategoryInLocation.delete_all_key_in_cache(category_location_mapping_list_to_delete)

    category_landmark_mapping_list_to_delete = HotelCategoryNearLandmark.generate_city_mapping_list_to_delete(instance)
    HotelCategoryNearLandmark.delete_all_key_in_cache(category_landmark_mapping_list_to_delete)


@receiver([post_delete], sender=CityCategoryMapforAutoPageCreation, dispatch_uid=uuid.uuid4)
def on_city_category_map_deleted(sender, instance, *args, **kwargs):
    category_location_mapping_list_to_delete = HotelCategoryInLocation.generate_city_mapping_list_to_delete(instance)
    HotelCategoryInLocation.delete_all_key_in_cache(category_location_mapping_list_to_delete)

    category_landmark_mapping_list_to_delete = HotelCategoryNearLandmark.generate_city_mapping_list_to_delete(instance)
    HotelCategoryNearLandmark.delete_all_key_in_cache(category_landmark_mapping_list_to_delete)


@receiver([post_save, post_delete], sender=FeatureToggle)
def on_feature_toggle_update(sender, instance, created, *args, **kwargs):
    FeatureToggleAPI.invalidate_cache(
        namespace=instance.namespace,
        feature=instance.feature)

@receiver([post_save, post_delete], sender=ContentStore)
def on_content_store_update(sender, instance, created, *args, **kwargs):
    ContentStoreService.invalidate_cache(key=instance.name)


@receiver([post_save, post_delete], sender=PolicyConfiguration)
def on_policy_configuration_change(sender, instance, created, *args, **kwargs):
    HotelPolicyService.invalidate_cache(provider=instance.provider)
    HotelDetail.invalidate_cache()


@receiver([post_save, post_delete], sender=ReviewAppConfiguration)
def on_review_app_config_change(sender, instance, created, *args, **kwargs):
        TopReviews.invalidate_cache()
