# -*- coding: utf-8 -*-
import abc

from apps.bookings.data_classes import Booking


class GetBookingInterface(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, booking_id, asynchronous=False):
        self.booking_id = booking_id
        self.asynchronous = asynchronous

    def get(self):
        booking = self._get()
        assert isinstance(booking, Booking)
        return booking

    @abc.abstractmethod
    def _get(self, ):
        pass
