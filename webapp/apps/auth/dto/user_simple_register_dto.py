from rest_framework import serializers
from apps.common.validators import CustomValidator as custom_validator


class UserSimpleRegisterDTO(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    email = serializers.EmailField(max_length=200)
    phone_number = serializers.CharField(max_length=200, allow_null=True)
    password = serializers.CharField(max_length=200)

    def validate_email(self, email):
        return custom_validator.normalise_email(email)

    def validate_phone_number(self, phone_number):
        return custom_validator.normalise_phone_number(phone_number)
