import logging
import time

from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist

from apps.hotels.service.hotel_service import HotelService
from common.exceptions.treebo_exception import TreeboValidationException
from dbcommon.models.hotel import Hotel, Policy, PolicyConfiguration, HotelPolicyMap
from services.tasks import send_email

logger = logging.getLogger(__name__)


class HotelPolicyService(object):
    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = '{0}_global_policy_config'

    @staticmethod
    def add_global_policies(hotel_policy_list, provider):
        global_policy_config = HotelPolicyService.get_global_policy_configuration_for_provider(provider)
        if global_policy_config:
            global_policies = Policy.objects.filter(is_global=True)
            for policy in global_policies:
                policy_map = {
                    "policy_type": policy.policy_type,
                    "title": policy.title,
                    "description": policy.description,
                    "display_in_need_to_know": policy.display_in_need_to_know,
                    "display_in_policy": policy.display_in_policy}
                hotel_policy_list.append(policy_map)

    @staticmethod
    def get_hotel_policies(hotel_id):
        try:
            hotel = Hotel.all_hotel_objects.get(id=hotel_id)
        except Hotel.DoesNotExist as e:
            raise e
        hotel_policies = []
        HotelPolicyService.add_global_policies(hotel_policies, hotel.provider_name)
        for policy in set(hotel.policies.all()):
            if policy.is_global is True:
                continue
            if 'is_couple_friendly'==policy.policy_type:
                policy_map = {
                    "policy_type": policy.policy_type,
                    "title": policy.title,
                    "description": HotelService().get_display_name(hotel=hotel) + " " + policy.description,
                    "display_in_need_to_know": policy.display_in_need_to_know,
                    "display_in_policy": policy.display_in_policy}
            else:
                policy_map = {
                    "policy_type": policy.policy_type,
                    "title": policy.title,
                    "description": policy.description,
                    "display_in_need_to_know": policy.display_in_need_to_know,
                    "display_in_policy": policy.display_in_policy}
            hotel_policies.append(policy_map)
        return hotel_policies

    @staticmethod
    def get_all_hotel_policies(hotel_objects):
        hotel_policies_map = {}
        hotel_ids = []
        policy_config_names = []
        policy_config_objects = PolicyConfiguration.objects.filter().all()
        for policy_config in policy_config_objects:
            policy_config_names.append(policy_config.provider)
        for hotel in hotel_objects:
            hotel_ids.append(hotel.id)
        hotel_policy_objects = HotelPolicyMap.objects.filter(hotel_id__in=hotel_ids).select_related('hotel_id', 'policy_id').all()
        for hotel_policy in hotel_policy_objects:
            if not hotel_policies_map.get(hotel_policy.hotel_id.id):
                hotel_policies_map[hotel_policy.hotel_id.id] = []
            policy_map = {
                "policy_type": hotel_policy.policy_id.policy_type,
                "title": hotel_policy.policy_id.title,
                "description": "",
                "display_in_need_to_know": hotel_policy.policy_id.display_in_need_to_know,
                "display_in_policy": hotel_policy.policy_id.display_in_policy}
            if hotel_policy.policy_id.is_global is True and hotel_policy.hotel_id.provider_name in policy_config_names:
                policy_map["description"] = hotel_policy.policy_id.description
                continue
            if hotel_policy.policy_id.policy_type == 'is_couple_friendly':
                policy_map["description"] = hotel_policy.hotel_id.name + " " + hotel_policy.policy_id.description
            else:
                policy_map["description"] = hotel_policy.policy_id.description
            hotel_policies_map[hotel_policy.hotel_id.id].append(policy_map)
        return hotel_policies_map

    @staticmethod
    def send_email_on_hotel_policy_update(
        hotel_id, policy_id, template_path, subject):
        """
        :param str hotel_id:
        :param str policy_id:
        """
        hotel = Hotel.all_hotel_objects.get(id=hotel_id)
        policy = Policy.objects.get(id=policy_id)

        emails = settings.HOTEL_POLICIES_EMAILS
        context = {'hotel_name': hotel.name,
                   'hotel_hx_id': hotel.hotelogix_id,
                   'local_id_or_couple_friendly_type': policy.title,
                   'time': time.strftime("%A, %d %B, %Y")
                   }
        subject = subject.format(hotel.name, hotel.hotelogix_id)
        content_type = "html"
        try:
            HotelPolicyService.__validateEmailDetails(emails, subject)
            for email in emails:
                send_email(
                    [email],
                    subject,
                    template_path,
                    context,
                    content_type)
        except Exception as e:
            logger.error(e)

    @staticmethod
    def __validateEmailDetails(emails, subject):
        if not emails:
            raise TreeboValidationException("Email 'recipients' are missing")
        return True

    @staticmethod
    def get_global_policy_configuration_for_provider(provider, default_result=True):
        if provider == settings.DEFAULT_PROVIDER_NAME:
            return default_result
        cache_key = HotelPolicyService.CACHE_KEY.format(provider)
        policy_config_result = cache.get(cache_key)
        if policy_config_result:
            return policy_config_result
        policy_config_object = None
        try:
            policy_config_object = PolicyConfiguration.objects.filter(provider=provider).first()
        except (ObjectDoesNotExist, Exception) as e:
            logger.exception(e.__str__())
        if policy_config_object:
            policy_config_result = policy_config_object.enable_global_policy
            cache.set(cache_key, policy_config_result, HotelPolicyService.CACHE_TIMEOUT)
        return policy_config_result

    @staticmethod
    def invalidate_cache(provider=None):
        if provider:
            cache.delete(HotelPolicyService.CACHE_KEY.format(provider))
        else:
            cache.delete_pattern(HotelPolicyService.CACHE_KEY.format('*'))

