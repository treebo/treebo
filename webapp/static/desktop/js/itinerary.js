'use strict';

import $ from 'jquery';
import {Observable} from "rx";
import {_} from 'underscore';
import "slick-carousel";
import BaseView from './treebolibs/baseview';
import searchWidget from './widgets/search';
import auth from './auth';
import queryString from 'query-string';
import cookies from 'js-cookie';

import React from 'react';
import { render } from 'react-dom';
import { createStore, combineReducers } from 'redux';
import Discount from './widgets/discount';
import PriceDetail from './widgets/pricedetails';
import Pay from './widgets/pay';
import '../css/itinerary.less';

let treejax = require('./treebolibs/treejax');
let $loader = $("#commonLoader");
let $travellerloader = $("#travellerLoader");
let ItineraryView = Object.create(BaseView);

let $csrfMiddlewareToken = $('input[name="csrfmiddlewaretoken"]').val();

let bookingData = $("#hiddenData").val().split("||");

let parsed = queryString.parse(location.search);

let [hotelId, rooms, roomType, guest, checkin, checkout, maxpax, totalNights] = bookingData;
let total = rt.total;

const contextData = $('.context-data').val();
const [ isAuthenticatedString, userName, userNumber, userEmail ] = JSON.parse(contextData || '[]');
const isAuthenticated = isAuthenticatedString === 'True';


let methods = {
    init : ()=>{
        let payathotelEvents = Rx.Observable.fromEvent($('#payatHotel'), 'click');
        let subscription = payathotelEvents.subscribe(e => {
            $("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="1"/>');
            methods.payuserlogin("payatHotel");
        });

        let payNowEvents = Rx.Observable.fromEvent($('#payNow'), 'click');
        let payNowsubscription = payNowEvents.subscribe(e => {
            var totalAmount = document.getElementById('grandTotal').innerHTML;
            if (totalAmount == 0) {
              $("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="1"/>');
              methods.payuserlogin("payatHotel");
            } else {
              $("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="0"/>');
              methods.payuserlogin("payNow");
            }
        });
        $("#discountFromCoupon").addClass('hide');

    },
    initLogin: ()=>{
        let loginEvents = Rx.Observable.fromEvent($('#guestLoginBtn'), 'click');
        let subscription = loginEvents.subscribe(e => {
            e.preventDefault();
            $('#travellerLoginError').addClass('hide');
            const travellerLoginParsley = $('#travellerLogin').parsley();

            travellerLoginParsley.validate();
            if(travellerLoginParsley.isValid()){
                $travellerloader.removeClass('hide');
                let email = $("#guestLoginEmail").val().trim();
                let password = $("#guestLoginPassword").val().trim();
                let loginPromise = auth.login(email, password).then(() => {
                    location.reload();
                }, (treeboError) => {
                    $('#travellerLoginError').html(treeboError.message).removeClass('hide');
                });
                loginPromise.finally(() => {
                    $travellerloader.addClass('hide');
                });
            }
        });

        let fbloginEvents = Rx.Observable.fromEvent($('#guestFBLogin'), 'click');
        let fbsubscription = fbloginEvents.subscribe(e => {
            auth.fbLogin().then(() => location.reload());
        });

        let googleloginEvents = Rx.Observable.fromEvent($('#guestFBLogin'), 'click');
        let googlesubscription = googleloginEvents.subscribe(e => {

        });


        let countinueGuestEvents = Rx.Observable.fromEvent($('#countinueGuest'), 'click');
        let countinueGuestSubscription = countinueGuestEvents.subscribe(e => {
            $("#guestLogin").hide();
            $("#guestLoggedin").show();
        });

        let loginBackEvents = Rx.Observable.fromEvent($('#loginBack'), 'click');
        let loginBackSubscription = loginBackEvents.subscribe(e => {
            $("#guestLogin").show();
            $("#guestLoggedin").hide();
        });

        let changeUserEvents = Rx.Observable.fromEvent($('#changeUser'), 'click');
        let changeUserSubscription = changeUserEvents.subscribe(e => {
            auth.logout();
        });

        let guestRegisterEvents = Rx.Observable.fromEvent($('#guestRegister'), 'click');
        let guestRegisterSubscription = guestRegisterEvents.subscribe(e => {

        $('.js-headersignup').click();
        });
    },
    initDiscount : ()=> {
        let data = {
            'query_param': {
                'couponcode':parsed.couponcode,
                'bid': parsed.bid,
            },
            'couponcode':parsed.couponcode,
            'discount': rt.discount,
            'voucherAmount':rt.voucherAmount,
            'couponDesc': rt.couponDesc,
            'autopromo': rt.autopromo,
            'url': rt.discount_url + "?"
        };
        console.log("Discount data in itinerary page: " + JSON.stringify(data));
        render(<Discount data={data}/>, document.getElementById('discountCouponContainer'));
    },
    payuserlogin: (idClicked)=> {
        const payParsley = $('#travellerValidations').parsley();
        payParsley.validate();
        if(payParsley.isValid()){
            $loader.removeClass('hide')
            let booking_request_id = $("#booking_request_id").val();
            var url = '/api/v1/checkout/availability/?';
            var data = {
                'bid': booking_request_id,
            };

            var query_param = $.param(data);
            var p = treejax({
                    url: url + query_param,
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRFToken", $csrfMiddlewareToken);
                    }
                }).then(function (data, status, xhr) {
                    $loader.addClass('hide')
                    if (data["available"]) {
                        console.log("Submitting Form");
                        $("#travellerValidations").submit();
                    } else {
                        $("#soldOutPopup").removeClass("hide");
                        return false;
                    }
                },function (treeboError) {
                    $loader.addClass('hide')
                    $("#soldOutPopup").removeClass("hide");
                    console.log(treeboError);
                    return false;
                });
        } else {
            return false;
        }

        //payParsley.on('form:submit', () => // Don't submit form for this demo
        //false)

    },

    updateDiscount : (discount)=>{
        if(discount.code){
            //$('#grandTotal').text(Math.round(discount.priceobj.price + discount.priceobj.tax - discount.value));
            $('#grandTotal').text(discount.priceobj.final_price);
            $('#final_hidden_coupon_code').val(discount.code);
            if(discount.voucher_value) {
              $("#voucherValue").text(discount.voucher_value);
            } else {
              $("#discountValue").text(discount.value || 0);
            }
            $('#final_hidden_discount_value').val(discount.value);
        } else { // Removed the discount
            //$('#grandTotal').text(Math.round(discount.priceobj.price + discount.priceobj.tax - discount.value));
            $('#grandTotal').text(discount.priceobj.final_price);
            $('#final_hidden_coupon_code').val('');
            if(discount.voucher_value) {
              $("#voucherValue").text(discount.voucher_value);
            } else {
              $("#discountValue").text(discount.value || 0);
            }
            $('#final_hidden_discount_value').val(discount.value || 0);
        }
        if(discount.priceobj) {
            $('#totalTax').text(discount.priceobj.tax);
            $('#roomPrice').text(discount.priceobj.price);
        }
    }

}


ItineraryView.pageLoaded = () => {
    methods.init();
    methods.initLogin();
    methods.initDiscount();
    if(window.location.pathname == '/itinerary/')
        $("#price-detail-itinerary").addClass('show');

    let $itineraryError = $('#itineraryError');
    //methods.initResults();
    $('body').on("discount:removed", (event, param) => {
        //methods.initDiscount();
        methods.updateDiscount(param);
    });
    $('body').on("discount:applied", (event, param) => {
        methods.updateDiscount(param);
    });

    if(parsed.error){
        $itineraryError.removeClass('hide').find('.alert__msg').html(parsed.error);
    }

    const renderApp = () => {
        render(
            <Checkout />, document.getElementById('checkout')
        );
    };
    store.subscribe(renderApp);
    renderApp();

};

rt.roomtype = parsed.roomtype;

let analyticsConfigfunc = function(){
    return  {
        checkin:parsed.checkin,
        checkout:parsed.checkout,
        room_type:parsed.roomtype,
        adults:rt.getAdults(),
        kids:rt.getKids(),
        rooms:rt.getRooms(),
        total_price:$('#grandTotal').html(),
        'PAH-status':rt.pahStatus,
        hrental_id: hotelId,
        hrental_pagetype: 'conversionintent',
        hrental_startdate: checkin,
        hrental_enddate: checkout,
        hrental_totalvalue: rt.total,
    }
};

function sendAnalytics(step, data = {}) {
    const events = {
        '2': {
            name: 'Checkout step 1 - Contact Info',
            value: { type: 'GUEST' },
        },
        '3': {
            name: 'Checkout step 2 - Traveller info',
            value: {},
        },
        '4': {
            name: 'Checkout step 3 - Special requests',
            value: {},
        },
        '5': {
            name: 'Checkout step 4 - Payments',
            value: {},
        },
        '6': {
            name: 'Checkout step 3 - OTP',
            value: {},
        }
    };
    const { name, value } = events[step];
    rt.clientAnalyticsObj.sendAnalyticsEvents(name, {
        ...value,
        ...data,
        ...analyticsConfigfunc(),
    });
}

$("#price-detail-itinerary").on('click',e => {
        const stringPriceDetail = {nights:rt.nights};
        $("#price-detail").removeClass('hide');
        render(<PriceDetail data={stringPriceDetail}/>, $('#price-detail')[0]);
});

const initialLogin = () => {
    const initial = {
        existingAccount: false,
        isEditable: false,
        isVisible: true,
        section: 'login',
    };

    if (isAuthenticated) {
        initial.isOpen = false;
        initial.isEditable = true;
        initial.summary = {
            title: `Welcome ${userName}`,
            subtitle: `Booking confirmation will be sent to ${userEmail}`,
        };
        initial.email = userEmail;
    } else {
        initial.isOpen = true;
        initial.summary = {};
    }

    return initial;
};

const login = (state = initialLogin(), action) => {
    switch (action.type) {
        case 'CONTINUE_LOGIN':
            return {
                ...state,
                isEditable: true,
                summary: {
                    title: `Welcome ${userName || 'Guest'}`,
                    subtitle: `Booking confirmation will be sent to ${action.email}`,
                },
                email: action.email,
            };
        case 'EXISTING_ACCOUNT':
            return {
                ...state,
                existingAccount: action.value,
            };
        case 'TOGGLE_PANEL':
            return {
                ...state,
                isOpen: action.value === 'login',
            };
        default:
            return state;
    }
};

const initialDetails = {
    isOpen: isAuthenticated,
    isEditable: false,
    isVisible: true,
    section: 'details',
    name: userName,
    mobile: userNumber,
    email: userEmail,
    summary: {
        title: 'Primary Traveller',
        subtitle: 'Who is the main traveller',
    },
};

const details = (state = initialDetails, action) => {
    switch (action.type) {
        case 'CONTINUE_LOGIN':
            return {
                ...state,
                email: action.email,
            };
        case 'CONTINUE_DETAILS':
            const { name, mobile } = action.data;
            return {
                ...state,
                isEditable: true,
                summary: {
                    title: 'Primary Traveller',
                    subtitle: `${name}, ${mobile}`,
                },
                ...action.data,
            };
        case 'TOGGLE_PANEL':
            return {
                ...state,
                isOpen: action.value === 'details',
            };
        default:
            return state;
    }
};

const initialSpecial = {
    isOpen: false,
    isEditable: false,
    isVisible: true,
    section: 'special',
    summary: {
        title: 'Mention Any Special Requests',
        subtitle: 'Arriving early, Need something special. ' +
            'Let us know here, we will try our best to get it done',
    },
    check_in_time: '12pm',
    check_out_time: '11am',
    message: '',
};

const special = (state = initialSpecial, action) => {
    switch (action.type) {
        case 'TOGGLE_PANEL':
            return {
                ...state,
                isOpen: action.value === 'special',
            };
        case 'CONTINUE_DETAILS':
            return {
                ...state,
                form: action.form,
            };
        case 'PAY_NOW':
              const { check_in_time: checkInTime, check_out_time: checkOutTime, message } = action.value;
              return {
                  ...state,
                  isEditable: true,
                  summary: {
                      title: 'Special Request',
                      subtitle: `Check In at ${checkInTime}, Check Out at ${checkOutTime}.\n${message}`,
                  },
                  ...action.value,
              };
        default:
            return state;
    }
};

const initialPayment = {
    isOpen: false,
    isEditable: false,
    isVisible: false,
    section: 'payment',
    summary: {
        title: 'Payment options',
        subtitle: '',
    },
    data: {},
};

const payment = (state = initialPayment, action) => {
    switch (action.type) {
        case 'TOGGLE_PANEL':
            return {
                ...state,
                isOpen: action.value === 'payment',
            };
        case 'PAY_NOW':
            return {
                ...state,
                data: action.data,
                isVisible: true,
            };
        default:
            return state;
    }
};

const reducer = combineReducers({ login, details, special, payment });

const store = createStore(reducer);

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: '',
        };
    }

    componentDidMount() {
        const { email } = this.props.login;
        this.setState({ email });
    }

    handleFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    updateAccount = (e) => {
        store.dispatch({
            type: 'EXISTING_ACCOUNT',
            value: e.target.checked,
        });
    };

    onLogin = (existingAccount, e) => {
        e.preventDefault();
        $loader.addClass('hide');
        const travellerLoginParsley = $(this.refs.travellerLogin).parsley();

        travellerLoginParsley.validate();
        if (travellerLoginParsley.isValid()) {
            const email = this.state.email;
            if (existingAccount) {
                const password = this.state.password;

                $loader.removeClass('hide');
                auth.login(email, password).then(({ phone }) => {
                    sendAnalytics(2, {
                        type: 'LOGGEDIN',
                        email,
                        mobile: phone,
                    });
                    location.reload();
                }, (treeboError) => {
                    this.setState({ error: treeboError.message });
                }).finally(() => {
                    $loader.addClass('hide');
                });
            } else {
                sendAnalytics(2, {
                    email,
                });
                store.dispatch({
                    type: 'CONTINUE_LOGIN',
                    email,
                });
                store.dispatch({
                    type: 'TOGGLE_PANEL',
                    value: 'details',
                });
            }
        }
    };

    onContinue = () => {
        store.dispatch({
            type: 'TOGGLE_PANEL',
            value: 'details',
        });
    };

    render() {
        const { existingAccount } = this.props.login;
        return (
            <div>
                <h2 className="checkout__section-title checkout__section-title--active">Your Email Address</h2>
                <p className="checkout__section-subtitle">Where do you want the booking confirmation.</p>
                <div className="flex-row">
                    <div className="guest-login__form-container pos-rel">
                        {
                            isAuthenticated ?
                            <div>
                                <p className="guest-login__logged">
                                    You are logged in as <strong>{userName}</strong>, <a href="/logout/">Change User</a>
                                </p>
                                <button
                                    onClick={this.onContinue}
                                    className="analytics-emaillogin checkout__action checkout__action--continue btn btn--round"
                                >
                                    Continue
                                </button>
                            </div> :
                            <form className="" ref="travellerLogin" action="" method="post">
                                <div className="float-labels guest-login__name">
                                    <input
                                        onChange={this.handleFieldChange} value={this.state.email}
                                        type="text" name="email"
                                        pattern=".+" required="true" className="float-labels__input"
                                        data-parsley-required="true" data-parsley-type="email"
                                        data-parsley-trigger="change" data-parsley-type-message="Enter valid email"
                                        data-parsley-required-message="Please enter a valid email ID"
                                        placeholder="Email"
                                    />
                                    <label className="float-labels__label">Email</label>
                                </div>
                                {
                                    existingAccount &&
                                    <div className="float-labels guest-login__password">
                                        <input
                                            onChange={this.handleFieldChange} value={this.state.password}
                                            type="password" name="password"
                                            pattern=".+" required="true" className="float-labels__input"
                                            placeholder="Password"
                                        />
                                        <label className="float-labels__label">Password</label>
                                    </div>
                                }
                                <div className="guest-login__check pos-rel">
                                    <label className="flex-row flex--align-center">
                                        <input className="guest-login__input-field"
                                            type="checkbox"
                                            onChange={this.updateAccount} name="account"
                                        />
                                        <span className="guest-login__have-account">I have a Treebo account</span>
                                    </label>
                                </div>
                                {
                                    this.state.error &&
                                    <div className="travller-login-error login__error alert alert--error">
                                        {this.state.error}
                                    </div>
                                }
                                <button
                                    type="submit" name="button" onClick={(e) => this.onLogin(existingAccount, e)}
                                    className="analytics-emaillogin checkout__action checkout__action--continue btn btn--round"
                                >
                                    Continue
                                </button>
                            </form>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

class Details extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount = () => {
        const { name, email, mobile } = this.props.details;
        this.setState({ name, email, mobile });
    }

    handleFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    onContinue = (e) => {
        e.preventDefault();
        const traveller = $('#travellerValidations').parsley();
        const travellerForm = $('#travellerValidations').clone();
        traveller.validate();
        if (traveller.isValid()) {
            sendAnalytics(3, { email: this.state.email });
            store.dispatch({
                type: 'CONTINUE_DETAILS',
                form: travellerForm,
                data: this.state,
            });
            store.dispatch({
                type: 'TOGGLE_PANEL',
                value: 'special',
            });
        }
    };

    render() {
        return (
            <div>
                <h2 className="checkout__section-title checkout__section-title--active">Enter Details Of Primary Traveller</h2>
                <p className="checkout__section-subtitle">Who is the main traveller</p>
                <form method="post" action="" id="travellerValidations" className="traveller-form">
                    <input type="hidden" id="channel" name="channel" value="desktop" />
                    <div id="hiddenrow"></div>
                    <div className="">
                        <div id="guestWidget">
                            <div className="guest-details__row flex-column">
                                <div className="float-labels guest-details__name">
                                    <input
                                        onChange={this.handleFieldChange} value={this.state.name}
                                        type="text" name="name" pattern=".+"
                                        required="true" className="float-labels__input"
                                        placeholder="Name" data-parsley-required="true"
                                        data-parsley-required-message="Please enter valid name"
                                    />
                                    <label className="float-labels__label">Name</label>
                                </div>
                                <div className="float-labels guest-details__mobile">
                                    <input
                                        onChange={this.handleFieldChange} value={this.state.mobile}
                                        type="number" name="mobile" pattern=".+" required="true"
                                        className="float-labels__input" placeholder="Mobile number"
                                        data-parsley-type="number" data-parsley-trigger="change"
                                        data-parsley-type-message="Enter valid mobile number"
                                        data-parsley-required-message="Please enter valid phone number"
                                        data-parsley-minlength-message="Please enter 10 digit mobile number"
                                        data-parsley-maxlength-message="Please enter 10 digit mobile number"
                                        data-parsley-minlength="10" data-parsley-maxlength="10"
                                    />
                                        <label className="float-labels__label">Mobile Number</label>
                                </div>
                                <div className="float-labels guest-details__email">
                                    <input
                                        onChange={this.handleFieldChange} value={this.state.email}
                                        type="text" name="email" pattern=".+" required="true"
                                        className="float-labels__input" placeholder="Email address"
                                        data-parsley-required="true" data-parsley-type="email"
                                        data-parsley-trigger="change" data-parsley-type-message="Enter valid email"
                                        data-parsley-required-message="Please enter valid email ID"
                                    />
                                    <label className="float-labels__label">Email ID</label>
                                </div>
                                <button
                                    type="submit" name="button" onClick={this.onContinue}
                                    className="checkout__action checkout__action--continue btn btn--round"
                                >
                                    Continue</button>

                                <p className="guest-details__msg-header">Important Notice</p>
                                <p className="guest-details__msg">
                                    Our hotels reserve the right to admission to ensure safety and
                                    comfort of other guests. This may include cases such as local residents,
                                    unmarried and unrelated couples among others.
                                    &nbsp;<a href="/faq/#right-to-admission" className="link" target="_blank">Know more</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}


//########################## OTP ##########################
const OtpScreen = ({ handleFieldChange, resendOtp, val, mobile, editNumber, otperror }) => {
    return (<div className="otp__container">
        <h2 className="otp__title">Verify your Mobile Number</h2>
        <div className="otp__subtitle">
            <p>
                A One Time Password (OTP) has been sent via SMS to <span>{mobile}</span>
                <small onClick={editNumber} className="otp__edit"> Change Number</small>
            </p>
        </div>
        <div className="float-labels">
            <input className="float-labels__input otp__code" onChange={handleFieldChange}
                   pattern=".+" required="true" data-parsley-type="number"
                   data-parsly-trigger="change" data-parsley-type-message="Enter 4 digit code"
                   data-parsley-required="true" data-parsley-required-message="Please enter OTP here"
                   name="otp" type="number" placeholder="Enter OTP" value={val} />
            <div className="otp__error-msg">{otperror}</div>
        </div>
        <p className="otp__text">
            Did not receive? <a href="#" className="otp__action" onClick={resendOtp} >Resend Sms</a>
        </p>
    </div>);
};

const NumScreen = ({ handleFieldChange, val }) => {
    return (
        <div className="otp__container">
            <h2 className="otp__title">Enter Mobile Number</h2>
            <p className="otp__text">A One Time Password (OTP) will be  sent via SMS
                to verify your number.</p>
            <div className="float-labels">
                <input className="float-labels__input otp__phone" onChange={handleFieldChange}
                       pattern=".+" required="true" data-parsley-type="number"
                       data-parsley-trigger="change" data-parsley-type-message="Enter valid mobile number"
                       data-parsley-required-message="Please enter a valid mobile number"
                       data-parsley-length-message="Please enter a 10 digit mobile number"
                       data-parsley-minlength="10" data-parsley-maxlength="10"
                       name="mobile" type="number" placeholder="Enter Mobile number" value={val} />
            </div>
        </div>
    );
};

class Otp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mobile: this.props.user.mobile,
            otp: '',
        };
    }

    componentDidMount() {

        $('#otp-form').parsley().on('form:submit', (e) => {
            e.submitEvent.preventDefault();
            this.props.updateOtp(this.state);
            return false;
        });
    }

    handleFieldChange = (e) => {
        this.props.onOtpError('');
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    render() {
        const { showPhone, resendOtp, showOtp, editNumber, closeOtp, otperror} = this.props;
        const { mobile, otp } = this.state;

        return (
            <div className={`modal flex-column otp ${!showOtp ? 'hide' : ''}`}>
                <div className="modal__body">
                    <form id="otp-form">
                        {
                            showPhone ?
                                <NumScreen handleFieldChange={this.handleFieldChange} val={mobile} /> :
                                <OtpScreen
                                    handleFieldChange={this.handleFieldChange}
                                    resendOtp={resendOtp} val={otp} mobile={mobile}
                                    editNumber={editNumber}
                                    otperror={otperror}
                                />
                        }
                        <div className="text-center">
                          <button type="submit" className="btn otp__verify-btn btn--large">Done</button>
                        </div>
                        <div className="text-center">
                          <button paytype="PAYNOW" type="button" onClick={() => this.props.submitForm(0)}
                          className="analytics-pay otp__paynow-btn itinerary-view__action__item checkout__action--continue">
                            SKIP, I WILL PAY NOW
                          </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}


class Special extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showOtp: false,
            showPhone: false,
            otpNotVerified:false,
            otperror:"",
            user: {
                name: this.props.name,
                mobile: this.props.mobile,
                email: this.props.email,
            },
        };
    }

    componentDidMount() {
        const { check_in_time, check_out_time, message } = this.props.special;
        this.setState({ check_in_time, check_out_time, message });

        $('body').on('discount:applied discount:removed', () => {
            const { payAtHotel } = this.state;
            if (!_.isUndefined(payAtHotel)) {
                this.getPayment(payAtHotel);
            }
        });
    }

    handleFieldChange = (e, key) => {
        this.setState({
            [key || e.target.name]: e.target.value,
        });
    };

    pollForStatusCheck = function* statusCheck() {
        while (true) {
            yield fetch('/checkstatus/', {
                method: 'post',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'X-CSRFToken' : cookies.get('csrftoken'),
                },
                body: JSON.stringify({
                    jobid: this.jobid,
                    referenceid: this.referenceid,
                    csrfmiddlewaretoken: cookies.get('csrftoken'),
                })
            })
                .then(r => r.json())
                .then(resp => resp.data);
        }
    };

    runPolling = (res, generator) => {
        if (!generator) {
            generator = this.pollForStatusCheck();
        }

        $loader.removeClass('hide');
        setTimeout(() => {
            const p = generator.next();
            p.value.then(data => {
                if (data.jobstatus === 'PENDING') {
                    this.runPolling(res, generator);
                } else if (data.jobstatus === 'SUCCESS') {
                    if (data.result.isSuccess) {
                        $loader.addClass('hide');
                        if (data.result.jobname === 'savebooking') {
                            this.dispatchPayNow({
                                ...res,
                                bid: data.result.bid,
                            });
                        }
                    }
                }
            });
        }, 1000);
    };

    onPayAtHotel = (res) => {
        const { handler_url: handlerUrl, booking_id: bookingId, redirect_url: redirectUrl } = res;

        $loader.removeClass('hide');
        if (redirectUrl) {
            window.location = redirectUrl;
        } else {
            $('<form>', {
                action: handlerUrl,
                method: 'POST',
            }).append(
                $('<input />', {
                    type: 'hidden',
                    name: 'csrfmiddlewaretoken',
                    value: cookies.get('csrftoken'),
                }),
                $('<input />', {
                    type: 'hidden',
                    name: 'booking_id',
                    value: bookingId,
                })
            ).appendTo('body').submit();
        }
    };

    dispatchPayNow = (res) => {
        store.dispatch({
            type: 'PAY_NOW',
            data: res,
            value: this.state,
        });
        store.dispatch({
            type: 'TOGGLE_PANEL',
            value: 'payment',
        });
    };

    onPayNow = (res) => {
        if (res.instant_booking) {
            this.dispatchPayNow(res);
        } else {
            this.jobid = res.jobid;
            this.referenceid = res.reference_id;
            this.runPolling(res);
        }
    };

    submitForm = (payAtHotel) => {
        this.setState({ payAtHotel });
        this.getPayment(payAtHotel);
    };
    updatePhone = (mobile) => {
        $loader.removeClass('hide');
        var mobile = mobile;
        treejax({
            url: '/rest/v1/otp/',
            type: 'POST',
            data: {
                mobile,
                csrfmiddlewaretoken: cookies.get('csrftoken'),
            }
        }).then(() => {
            this.setState({
                user: {
                    ...this.state.user,
                    mobile
                },
                showPhone: false
            });
        }).finally(() => {
            $loader.addClass('hide');
        });
    };

    confirmOtp = (otp) => {
        $loader.removeClass('hide');
        treejax({
            url: '/rest/v1/otp/verify/',
            type: 'POST',
            data: {
                otp,
                mobile: this.state.user.mobile,
                csrfmiddlewaretoken: cookies.get('csrftoken'),
            }
        }).then(() => {
          sendAnalytics(6, {
              email: this.state.user.email,
              mobile: this.state.user.mobile,
          });
          $("#final_hidden_otp_verified_number").val(this.state.user.mobile);
          this.closeOtp();
          this.setState({ payAtHotel:1, });
          this.getPayment(1);
        }).catch((err) => {
            this.onOtpError("Please enter correct OTP.");
            $loader.addClass('hide');
        }).finally(() => {
        });
    };

    onOtpError = (error) => {
      this.setState({
        otperror:error,
      });
    };

    editNumber = () => {
        this.setState({ showPhone: true });
    };

    updateOtp = ({ mobile, otp }) => {
        if(this.state.showPhone) {
            this.updatePhone(mobile);
        } else {
            this.confirmOtp(otp);
        }
    };

    resendOtp = () => {
        this.updatePhone(this.state.user.mobile);
    };

    checkOtpVerified = () => {
      var otpEnabled = rt['isOtpEnabled'];
      if (otpEnabled) {
          var mobile = this.state.user.mobile;
          var email = this.state.user.email;
          var name = this.state.user.name;
          $loader.removeClass('hide');
          treejax({
              url: '/rest/v1/otp/isverified/',
              type: 'POST',
              data: {
                  mobile,
                  name,
                  csrfmiddlewaretoken: cookies.get('csrftoken'),
              }
          }).then((res) => {
            if (res.status == "success") {
              this.closeOtp();
              this.setState({ payAtHotel:1, });
              this.getPayment(1);
            } else if (res.status == "failed") {
              this.setState({
                  otpNotVerified:true,
                  showOtp:true,
                  showPhone:false,
              });
              $loader.addClass('hide');
            }
          }).catch((err) => {
              throw err;
          }).finally(() => {
          });
      } else {
        this.setState({ payAtHotel:1, });
        this.getPayment(1);
      }
    };

    closeOtp = () => {
      this.setState({
        otpNotVerified: false,
      });
    }

    getPayment = (payAtHotel) => {
        const { form } = this.props.special;
        const additionalData = $('.form-data').clone();
        const data = {
            pay_at_hotel: payAtHotel,
            ...this.state,
        };
        form.append(additionalData);

        form.serializeArray().forEach((x) => {
            data[x.name] = x.value;
        });
        $loader.removeClass('hide');
        treejax({
            url: '/booking/d/payment/',
            type: 'POST',
            data,
        }).finally(() =>
            $loader.addClass('hide')
        ).then((res) => {
            sendAnalytics(4, {
                email: data.email,
                payment_type: payAtHotel ? 'PAH' : 'PayNow',
                checkin_time: data.check_in_time,
                checkout_time: data.check_out_time,
            });
            payAtHotel ? this.onPayAtHotel(res) : this.onPayNow(res);
        }, () => $('#soldOutPopup').removeClass('hide'));
    };

    render() {
        return (
            <div ref="form">
                <h2 className="checkout__section-title checkout__section-title--active">Mention Special Requests</h2>
                <p className="checkout__section-subtitle">
                    Arriving early, Need something special. Let us know here, we will try our best to get it done.
                </p>
                <div className="request-details flex-row">
                    <div className="request-details__item flex-row flex--space-between request-details__cio">
                        <div className="flex-column flex--align-center">
                            <label className="request-details__item__label label-text">
                                Check-In
                            </label>
                            <div className="select medium">
                                <select
                                    className="" name="check_in_time" value={this.state.check_in_time}
                                    onChange={this.handleFieldChange}
                                >
                                    <option value="12pm">12pm</option>
                                    <option value="7am">07am</option>
                                    <option value="8am">08am</option>
                                    <option value="9am">09am</option>
                                    <option value="10am">10am</option>
                                    <option value="11am">11am</option>
                                    <option value="12pm">12pm</option>
                                    <option value="1pm">01pm</option>
                                    <option value="2pm">02pm</option>
                                    <option value="3pm">03pm</option>
                                    <option value="4pm">04pm</option>
                                    <option value="5pm">05pm</option>
                                    <option value="6pm">06pm</option>
                                    <option value="7pm">07pm</option>
                                    <option value="8pm">08pm</option>
                                    <option value="9pm">09pm</option>
                                    <option value="10pm">10pm</option>
                                    <option value="11pm">11pm</option>
                                </select>
                            </div>
                        </div>
                        <div className="flex-column flex--align-center">
                            <label className="request-details__item__label label-text">
                                Check-Out
                            </label>
                            <div className="select medium">
                                <select
                                    className="" name="check_out_time" value={this.state.check_out_time}
                                    onChange={this.handleFieldChange}
                                >
                                    <option value="1am">01am</option>
                                    <option value="2am">02am</option>
                                    <option value="3am">03am</option>
                                    <option value="4am">04am</option>
                                    <option value="5am">05am</option>
                                    <option value="6am">6am</option>
                                    <option value="7am">07am</option>
                                    <option value="8am">08am</option>
                                    <option value="9am">09am</option>
                                    <option value="10am">10am</option>
                                    <option value="11am">11am</option>
                                    <option value="12pm">12pm</option>
                                    <option value="1pm">01pm</option>
                                    <option value="2pm">02pm</option>
                                    <option value="3pm">03pm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <p className="request-details__info">
                        <span>
                            Early check-in (between 6AM and 12PM) and late check-out (upto 3PM)
                            can be accommodated at no extra charges, but will be subject to availability
                        </span>&nbsp;
                        <a href="/faq/#checkin-checkout-policy" className="link" target="_blank">Know more</a>
                    </p>
                </div>
                <div className="request-details__item request-details__specific">
                    <textarea
                        name="message" rows="8" cols="40" value={this.state.message}
                        placeholder="Other requests. Mention it here" onChange={this.handleFieldChange}
                    />
                </div>

                <div className="flex-row itinerary-view__action">
                    <button
                        paytype="PAYNOW" type="submit" onClick={() => this.submitForm(0)}
                        className="analytics-pay itinerary-view__action__item checkout__action--continue btn btn--round"
                    >
                        Pay Now
                    </button>

                    <button
                        paytype="PAH" type="submit" onClick={() => this.checkOtpVerified()}
                        className="analytics-pay itinerary-view__action__item btn btn--secondary btn--round"
                    >
                        Pay At hotel
                    </button>
                    {
                        this.state.otpNotVerified ?
                            <Otp
                                resendOtp={this.resendOtp} updateOtp={this.updateOtp}
                                user={this.state.user} editNumber={this.editNumber}
                                showOtp={this.state.showOtp} showPhone={this.state.showPhone}
                                closeOtp={this.closeOtp} otperror={this.state.otperror} onOtpError={this.onOtpError}
                                submitForm={this.submitForm}
                            /> :
                            null
                    }
                </div>
                <div className="request-details__offer flex-column">
                    <p className="request-details__msg-header">FLAT 10% Cashback (MobiKwik)</p>
                    <p className="request-details__msg">
                        Use code KWIK10 for 10% Cashback (Max Rs.250) when you pay using MobiKwik. T&C apply.
                    </p>
                </div>
            </div>
        );
    }
}

class Payment extends React.Component {
    constructor() {
        super();
        this.state = {
            error: '',
        };
    }

    onSuccessPay = (razorpayId) => {
        $loader.removeClass('hide');
        sendAnalytics(5, { email: this.props.data.email });

        const form = $(this.refs.razorpay);
        form.append(`<input type="hidden" name="razorpay_payment_id" value="${razorpayId}" />`);
        form.submit();
    };

    onErrorPay = (error) => {
        this.setState({ error });
    };

    render() {
        const {
            amount_to_send: amountToSend,
            razorpay_key: razorpayKey,
            bid,
            amount_to_store: amountToStore,
            email,
            contact,
            reference_id: referenceId,
        } = this.props.data;
        const config = {
            key: razorpayKey,
            image: 'https://s3-ap-southeast-1.amazonaws.com/treebo/static/files/new_treebo_gateway_logo.png',
        };

        return (
            <div>
                <h2 className="checkout__section-title checkout__section-title--active">
                    Payment Options
                </h2>
                <p className="error checkout__pay-error">{this.state.error}</p>
                <div className="checkout__payment">
                    <Pay
                        bid={bid}
                        amount={amountToSend}
                        user={{ email, contact }}
                        config={config}
                        onSuccessPay={this.onSuccessPay}
                        onErrorPay={this.onErrorPay}
                    />
                </div>
                <form className="hide" action="/razorpay_checkout/" method="post" ref="razorpay">
                    <input type="hidden" name="csrfmiddlewaretoken" value={cookies.get('csrftoken')} />
                    <input type="hidden" name="booking_id" value={bid} />
                    <input type="hidden" name="reference_id" value={referenceId} />
                    <input type="hidden" name="type" value="RAZORPAY" />
                    <input type="hidden" name="status" value="Authorized" />
                    <input type="hidden" name="amount_to_store" value={amountToStore} />
                    <input type="hidden" name="amount_to_send" value={amountToSend} />
                </form>
            </div>
        );
    }
}

class Section extends React.Component {
    toggleSection = (section) => {
        store.dispatch({
            type: 'TOGGLE_PANEL',
            value: section,
        });
    };

    render() {
        const { summary, isOpen, isVisible, isEditable, section } = this.props.data;

        if (!this.props.data || !isVisible) {
            return null;
        }

        return (
            <div className="checkout__section">
                <div className="checkout__index">
                {
                    isEditable ?
                        <label><input type="checkbox" defaultChecked disabled /></label> :
                        <span
                            className={`checkout__badge ${isOpen ? 'checkout__badge--active' : ''}`}
                        >{this.props.index}</span>
                }
                </div>
                {
                    isOpen ?
                        this.props.children :
                        <div className="flex-row flex--space-between flex--align-center">
                            <div>
                                <h2 className="checkout__section-title">{summary.title}</h2>
                                <p className="checkout__section-subtitle">{summary.subtitle}</p>
                            </div>
                            {isEditable &&
                                <button
                                    className="checkout__edit btn btn--secondary"
                                    onClick={_.partial(this.toggleSection, section)}
                                >Edit</button>
                            }
                        </div>
                }
            </div>
        );
    }
}

class Checkout extends React.Component {
    render() {
        const state = store.getState();
        return (
            <div className="checkout box-shadow">
                <ul>
                    <li><Section data={state.login} index="1">
                        <Login login={state.login} />
                    </Section></li>
                    <li><Section data={state.details} index="2">
                        <Details details={state.details} />
                    </Section></li>
                    <li><Section data={state.special} index="3">
                        <Special special={state.special} mobile={state.details.mobile} email={state.details.email} name={state.details.name}/>
                    </Section></li>
                    <li><Section data={state.payment} index="4">
                        <Payment data={state.payment.data} />
                    </Section></li>
                </ul>
            </div>
        );
    }
}


let itineraryConfig = { name: 'Itinerary Page', widgets: [], analyticsConfigfunc };
let itineraryView = ItineraryView.setup(itineraryConfig);
