# -*- coding: utf-8 -*-
from caching.base import CachingManager, CachingMixin
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.text import slugify
from djutil.models import TimeStampedModel

from common.constants import common_constants as const
from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.managers.city_manager import CityManager


class State(CachingMixin, TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200, null=True, blank=True, default='')
    service_tax = models.DecimalField(
        max_digits=20, decimal_places=10, default=8.7)
    luxury_tax_on_base = models.BooleanField(default=False)
    cs_id = models.CharField(max_length=200, null=True, blank=True, default='')

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_state'
        verbose_name = 'State'
        verbose_name_plural = 'States'

    def __unicode__(self):
        return '%s' % self.name

    def __str__(self):
        return '%s' % self.name


class City(CachingMixin, TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=75, unique=True, null=True, blank=True)
    description = models.TextField(max_length=2000, null=True, blank=True)
    tagline = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        default='')
    subscript = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        default='')
    DISABLED = const.DISABLED
    ENABLED = const.ENABLED
    STATUS_CHOICES = const.STATUS_CHOICES
    status = models.IntegerField(default=ENABLED, choices=STATUS_CHOICES)
    portrait_image = models.FileField(
        upload_to='city/',
        max_length=200,
        null=True,
        blank=True)
    landscape_image = models.FileField(
        upload_to='city/',
        max_length=200,
        null=True,
        blank=True)
    cover_image = models.FileField(
        upload_to='city/',
        max_length=200,
        null=True,
        blank=True)
    city_latitude = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        null=True,
        blank=True,
        default=12.9667)
    city_longitude = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        null=True,
        blank=True,
        default=77.5667)

    # TODO: Give a proper value here
    state = models.ForeignKey(
        State,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True)
    cs_id = models.CharField(max_length=200, null=True, blank=True)

    objects = CityManager()
    all_cities = CachingManager()
    enable_for_seo = models.BooleanField(default=False)
    seo_image = models.TextField(null=True, blank=True, default='')

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_city'
        verbose_name = 'City'
        verbose_name_plural = 'Cities'
        ordering = ['name']

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(City, self).save(*args, **kwargs)

    def as_json(self):
        return dict(
            name=self.name,
            description=self.description,
            tagline=self.tagline,
            subscript=self.subscript,
            status=self.status,
            portrait_image=self.portrait_image.url if self.portrait_image else None,
            landscape_image=self.landscape_image.url if self.landscape_image else None,
            cover_image=self.cover_image.url if self.cover_image else None,
            city_latitude=float(
                self.city_latitude),
            city_longitude=float(
                self.city_longitude))

    def __unicode__(self):  # __unicode__ on Python 2
        return self.name

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    def get_sitemap_url(self):
        city_string = slugify(self.slug)
        kwargs = {'cityString': city_string + '/'}
        return reverse('pages:hotels_in_city', kwargs=kwargs)


class Locality(CachingMixin, TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200)
    # TODO: Give a proper value for on_delete
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    free_text_url = models.CharField(max_length=500, null=True, blank=True, default='')
    pincode = models.PositiveIntegerField()
    latitude = models.DecimalField(max_digits=20, decimal_places=10)
    longitude = models.DecimalField(max_digits=20, decimal_places=10)
    distance_cap = models.IntegerField(blank=True, null=True)
    enable_for_seo = models.BooleanField(default=False)
    is_hotel_present = models.NullBooleanField(default=True)
    cs_id = models.CharField(max_length=200, null=True, blank=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_locality'
        verbose_name = 'Locality'
        verbose_name_plural = 'Localities'

    def __unicode__(self):  # __unicode__ on Python 2
        return self.name

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    def get_sitemap_url(self):
        locality_name = slugify(str(self.name))
        if self.free_text_url:
            return '/' + 'hotels-in-' + slugify(str(self.free_text_url)) + '/'
        return '/' + 'hotels-in-' + locality_name + '-' + str(self.city.slug) + '/'

    def get_locality_slug(self):
        city_slug = str(self.city.slug)
        locality_slug = slugify(self.free_text_url if self.free_text_url else self.name+'-'+city_slug)
        return locality_slug


class LandmarkType(CachingMixin, TimeStampedModel, DefaultPermissions):
    AIRPORT = 'AIRPORT'
    RAILWAYSTATION = 'RAILWAYSTATION'
    BUSSTOP = 'BUSSTOP'
    OTHER = 'OTHER'
    LANDMARK_TYPE_CHOICES = (
        (AIRPORT, 'Airport'),
        (RAILWAYSTATION, 'RailwayStation'),
        (BUSSTOP, 'BusStop'),
        (OTHER, 'Other'),
    )
    name = models.CharField(max_length=20,
                            choices=LANDMARK_TYPE_CHOICES,
                            default=OTHER)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_landmarktype'
        verbose_name = 'Landmark Type'
        verbose_name_plural = 'Landmark Types'

    def __unicode__(self):  # __unicode__ on Python 2
        return self.name

    def __str__(self):  # __unicode__ on Python 2
        return self.name


class Landmark(CachingMixin, TimeStampedModel, DefaultPermissions):
    type = models.CharField(max_length=200, null=True, blank=True, default='')
    name = models.CharField(max_length=200)
    description = models.TextField(max_length=2000, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    latitude = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        null=True,
        blank=True,
        default=12.9667)
    longitude = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        null=True,
        blank=True,
        default=77.5667)
    DISPLAY = const.DISPLAY
    HIDE = const.HIDE
    DISPLAY_CHOICES = const.DISPLAY_CHOICES
    status = models.IntegerField(default=HIDE, choices=DISPLAY_CHOICES)
    type = models.ForeignKey(
        LandmarkType,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True)
    popularity = models.PositiveIntegerField(default=0)
    cs_id = models.CharField(max_length=200, null=True, blank=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_landmark'
        verbose_name = 'Landmark'
        verbose_name_plural = 'Landmarks'
        ordering = ['name']

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s %s' % (self.name, self.city.name)

    def __str__(self):  # __unicode__ on Python 2
        return '%s %s' % (self.name, self.city.name)


class CityBlog(CachingMixin, TimeStampedModel, DefaultPermissions):
    highlight = models.CharField(max_length=200)
    image_url = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(max_length=2000, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    link = models.CharField(max_length=200)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_cityblog'
        verbose_name = 'City Blog'
        verbose_name_plural = 'City Blogs'

    def __unicode__(self):  # __unicode__ on Python 2
        if self.city.name:
            return "City %s Highlight %s" % (self.city.name, self.highlight)
        else:
            return "Highlight " % self.highlight

    def __str__(self):  # __unicode__ on Python 2
        if self.city.name:
            return "City %s Highlight %s" % (self.city.name, self.highlight)
        else:
            return "Highlight " % self.highlight


class NotifyNewLocation(CachingMixin, TimeStampedModel, DefaultPermissions):
    location = models.CharField(max_length=200)
    email = models.EmailField()
    query = models.CharField(max_length=200, null=True, blank=True, default='')

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_notifynewlocation'
        verbose_name = 'Notify New Location'
        verbose_name_plural = 'Notify New Location'

    def __unicode__(self):
        return '%s' % self.email

    def __str__(self):
        return '%s' % self.email


class CityAlias(CachingMixin, TimeStampedModel, DefaultPermissions):
    alias = models.CharField(max_length=100, null=False)
    city = models.CharField(max_length=100, null=False)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_cityalias'
        verbose_name = 'City Alias'
        verbose_name_plural = 'City Aliases'

    def __unicode__(self):
        return '%s - %s' % (self.city, self.alias)

    def __str__(self):
        return '%s - %s' % (self.city, self.alias)


class LocalitySeoDetails(CachingMixin, TimeStampedModel, DefaultPermissions):
    locality = models.ForeignKey(Locality, on_delete=models.CASCADE)
    locality_seo_title = models.CharField(max_length=500)
    locality_seo_description = models.TextField(null=True, blank=True)
    locality_seo_keyword = models.TextField(null=True, blank=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_localityseodetails'
        verbose_name = 'Locality SEO Details'
        verbose_name_plural = 'Locality SEO Details'

    def __unicode__(self):
        return '%s %s' % (self.locality.name, self.locality_seo_title)

    def __str__(self):
        return '%s %s' % (self.locality.name, self.locality_seo_title)
