import logging

from django.contrib import admin

from apps.reviews.models import TAHotelIDMapping, TASubrating, TAHotelSubratingMapping, \
    TAReviewRatingCountMapping, TAAwardslist, TAAwardsHotelMapping, TAHotelReviews, ReviewAppConfiguration, \
    TAAPIAuditTable, HotelTAOverallRatings
from dbcommon.admin import ReadOnlyAdminMixin

logger = logging.getLogger(__name__)


@admin.register(TAHotelIDMapping)
class TAHotelIDMappingAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'get_hotel_id',
        'get_hotel_name',
        'get_hotelogix_id',
        'ta_location_id',
        'write_review_url',
        'enable_iframe_widget')

    search_fields = (
        'hotel__id',
        'hotel__hotelogix_id',
        'hotel__name',
        'ta_location_id',
        'write_review_url')

    list_filter = ('hotel__name', 'enable_iframe_widget')
    ordering = ('-created_at',)

    def get_hotel_id(self, obj):
        return obj.hotel.id

    def get_hotelogix_id(self, obj):
        return obj.hotel.hotelogix_id

    def get_hotel_name(self, obj):
        return obj.hotel.name

    get_hotel_id.short_description = 'Hotel ID'
    get_hotelogix_id.short_description = 'Hotelogix ID'
    get_hotel_name.short_description = 'Hotel Name'


@admin.register(HotelTAOverallRatings)
class HotelTAOverallRatingsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'get_hotel_id',
        'get_hotel_name',
        'get_hotelogix_id',
        'get_ta_location_id',
        'overall_rating',
        'url_overall_rating_image',
        'write_review_url',
        'read_review_url',
        'overall_ranking',
        'overall_ranking_out_of',
        'overall_ranking_string',
        'geo_location_name',
        'num_reviews_count')

    search_fields = (
        'hotel_ta_mapping__hotel__id',
        'hotel_ta_mapping__hotel__hotelogix_id',
        'hotel_ta_mapping__hotel__name',
        'hotel_ta_mapping__ta_location_id',
        'overall_ranking',
        'overall_rating',
        'num_reviews_count',
        'write_review_url',
        'read_review_url',
        'overall_ranking',
        'geo_location_name')

    list_filter = (
        'hotel_ta_mapping__hotel__name',
        'overall_rating',
        'geo_location_name')
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False

    def get_hotel_id(self, obj):
        return obj.hotel_ta_mapping.hotel.id

    def get_hotelogix_id(self, obj):
        return obj.hotel_ta_mapping.hotel.hotelogix_id

    def get_hotel_name(self, obj):
        return obj.hotel_ta_mapping.hotel.name

    def get_ta_location_id(self, obj):
        return obj.hotel_ta_mapping.ta_location_id

    get_hotel_id.short_description = 'Hotel ID'
    get_hotelogix_id.short_description = 'Hotelogix ID'
    get_hotel_name.short_description = 'Hotel Name'


@admin.register(TASubrating)
class TASubratingAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'subrating_name',
        'subrating_localized_name',
        'is_enabled')
    list_filter = ('subrating_name', 'subrating_localized_name', 'is_enabled')
    search_fields = (
        'subrating_name',
        'subrating_localized_name',
        'is_enabled')
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False


@admin.register(TAHotelSubratingMapping)
class TAHotelSubratingMappingAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'get_hotel_id',
        'get_hotel_name',
        'get_hotelogix_id',
        'get_ta_location_id',
        'get_subrating_name',
        'get_subrating_localized_name',
        'subrating_value',
        'url_subrating')

    search_fields = (
        'hotel_ta_mapping__hotel__id',
        'hotel_ta_mapping__hotel__hotelogix_id',
        'hotel_ta_mapping__hotel__name',
        'subrating__subrating_name',
        'subrating__localized_name',
        'subrating_value',
        'hotel_ta_mapping__ta_location_id')

    list_filter = (
        'hotel_ta_mapping__hotel__name',
        'subrating_value',
        'subrating__subrating_name',
        'subrating__subrating_localized_name')
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False

    def get_hotel_id(self, obj):
        return obj.hotel_ta_mapping.hotel.id

    def get_hotelogix_id(self, obj):
        return obj.hotel_ta_mapping.hotel.hotelogix_id

    def get_hotel_name(self, obj):
        return obj.hotel_ta_mapping.hotel.name

    def get_ta_location_id(self, obj):
        return obj.hotel_ta_mapping.ta_location_id

    def get_subrating_name(self, obj):
        return obj.subrating.subrating_name

    def get_subrating_localized_name(self, obj):
        return obj.subrating.subrating_localized_name

    get_hotel_id.short_description = 'Hotel ID'
    get_hotelogix_id.short_description = 'Hotelogix ID'
    get_hotel_name.short_description = 'Hotel Name'
    get_subrating_name.short_description = 'Subrating Name'
    get_subrating_localized_name.short_description = 'Subrating Localized Name'
    get_ta_location_id.short_description = 'TA Loc ID'


@admin.register(TAReviewRatingCountMapping)
class TAReviewRatingHotelMappingAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'get_hotel_id',
        'get_hotel_name',
        'get_hotelogix_id',
        'get_ta_location_id',
        'review_rating',
        'count_of_customers',
        'percentage_of_customers')

    search_fields = (
        'hotel_ta_mapping__hotel__id',
        'hotel_ta_mapping__hotel__hotelogix_id',
        'hotel_ta_mapping__hotel__name',
        'review_rating',
        'percentage_of_customers',
        'count_of_customers',
        'hotel_ta_mapping__ta_location_id')

    list_filter = (
        'hotel_ta_mapping__hotel__name',
        'review_rating',
        'percentage_of_customers')
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False

    def get_hotel_id(self, obj):
        return obj.hotel_ta_mapping.hotel.id

    def get_hotelogix_id(self, obj):
        return obj.hotel_ta_mapping.hotel.hotelogix_id

    def get_hotel_name(self, obj):
        return obj.hotel_ta_mapping.hotel.name

    def get_ta_location_id(self, obj):
        return obj.hotel_ta_mapping.ta_location_id

    get_hotel_id.short_description = 'Hotel ID'
    get_hotelogix_id.short_description = 'Hotelogix ID'
    get_hotel_name.short_description = 'Hotel Name'
    get_ta_location_id.short_description = 'TA Loc ID'


@admin.register(TAAwardslist)
class TAAwardslistAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'award_name',
        'award_category',
        'is_enabled')
    list_filter = ('award_name', 'award_category', 'is_enabled')
    search_fields = ('award_name', 'award_category', 'is_enabled')
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False


@admin.register(TAAwardsHotelMapping)
class TAAwardsHotelMappingAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'get_hotel_id',
        'get_hotel_name',
        'get_hotelogix_id',
        'get_ta_location_id',
        'get_award_name',
        'get_award_category',
        'award_image_url',
        'award_year')

    search_fields = (
        'hotel_ta_mapping__hotel__id',
        'hotel_ta_mapping__hotel__hotelogix_id',
        'hotel_ta_mapping__hotel__name',
        'award__award_name',
        'award__award_category',
        'award_image_url',
        'award_year',
        'hotel_ta_mapping__ta_location_id')

    list_filter = (
        'hotel_ta_mapping__hotel__name',
        'award_year',
        'award__award_name',
        'award__award_category')
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False

    def get_hotel_id(self, obj):
        return obj.hotel_ta_mapping.hotel.id

    def get_hotelogix_id(self, obj):
        return obj.hotel_ta_mapping.hotel.hotelogix_id

    def get_hotel_name(self, obj):
        return obj.hotel_ta_mapping.hotel.name

    def get_ta_location_id(self, obj):
        return obj.hotel_ta_mapping.ta_location_id

    def get_award_name(self, obj):
        return obj.award.award_name

    def get_award_category(self, obj):
        return obj.award.award_category

    get_hotel_id.short_description = 'Hotel ID'
    get_hotelogix_id.short_description = 'Hotelogix ID'
    get_hotel_name.short_description = 'Hotel Name'
    get_award_name.short_description = 'Award Name'
    get_award_category.short_description = 'Award Category'
    get_ta_location_id.short_description = 'TA Loc ID'


@admin.register(TAHotelReviews)
class TAHotelReviewsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id', 'created_at', 'modified_at', 'get_hotel_id', 'get_hotel_name', 'get_hotelogix_id', 'get_ta_location_id',
        'ta_review_id', 'review_text', 'review_title', 'review_published_date', 'user_rating', 'user_rating_image_url',
        'user_rating_text_url', 'user_name', 'user_image_url', 'owner_rating_reply_text',
        'owner_description', 'owner_reply_published_date', 'review_is_active', 'helpful_votes',
        'review_is_user_generated', 'review_is_crawlable')

    search_fields = (
        'hotel_ta_mapping__hotel__id',
        'hotel_ta_mapping__hotel__hotelogix_id',
        'hotel_ta_mapping__hotel__name',
        'user_rating',
        'owner_description',
        'owner_rating_reply_text',
        'review_text',
        'review_title',
        'owner_reply_published_date',
        'review_published_date',
        'helpful_votes',
        'hotel_ta_mapping__ta_location_id')

    list_filter = (
        'hotel_ta_mapping__hotel__name', 'user_rating', 'owner_description', 'review_is_active',
        'review_is_user_generated', 'review_is_crawlable')
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False

    def get_hotel_id(self, obj):
        return obj.hotel_ta_mapping.hotel.id

    def get_hotelogix_id(self, obj):
        return obj.hotel_ta_mapping.hotel.hotelogix_id

    def get_hotel_name(self, obj):
        return obj.hotel_ta_mapping.hotel.name

    def get_ta_location_id(self, obj):
        return obj.hotel_ta_mapping.ta_location_id

    get_hotel_id.short_description = 'Hotel ID'
    get_hotelogix_id.short_description = 'Hotelogix ID'
    get_hotel_name.short_description = 'Hotel Name'
    get_ta_location_id.short_description = 'TA Loc ID'


@admin.register(ReviewAppConfiguration)
class ReviewAppConfigurationAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'enable_review_app',
        'enable_for_all_hotels',
        'disable_for_hotel_list',
        'enable_for_hotel_list',
        'is_audit_enable',
        'percent_for_ranking_visibility',
        'num_subratings_to_show',
        'award_description',
        'num_non_user_reviews_to_show',
        'num_user_reviews_to_show',
        'disable_for_category_list')
    list_filter = (
        'enable_for_all_hotels',
        'num_non_user_reviews_to_show',
        'num_user_reviews_to_show')
    search_fields = (
        'enable_for_all_hotels',
        'disable_for_hotel_list',
        'percent_for_ranking_visibility',
        'num_subratings_to_show',
        'award_description',
        'num_non_user_reviews_to_show',
        'num_user_reviews_to_show')
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False


@admin.register(TAAPIAuditTable)
class TAAPIAuditTableAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'get_hotel_name',
        'get_ta_location_id',
        'get_hotel_id',
        'get_hotelogix_id',
        'location_api_url',
        'location_api_url_response_json',
        'location_api_url_response_status',
        'location_reviews_api_url',
        'location_reviews_api_url_response_json',
        'location_reviews_api_url_response_status',
        'user_location_reviews_api_url',
        'user_location_reviews_api_url_response_json',
        'user_location_reviews_api_url_response_status')
    list_filter = (
        'location_api_url_response_status',
        'location_api_url_response_status',
        'hotel_ta_mapping__hotel__name',
        'location_reviews_api_url_response_status',
        'location_reviews_api_url_response_status',
        'user_location_reviews_api_url_response_status')

    search_fields = (
        'hotel_ta_mapping__hotel__name',
        'hotel_ta_mapping__ta_location_id',
        'hotel_ta_mapping__hotel__id',
        'hotel_ta_mapping__hotel__hotelogix_id',
        'location_api_url_response_json',
        'location_reviews_api_url_response_json',
        'user_location_reviews_api_url_response_json')
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False

    def get_hotel_id(self, obj):
        return obj.hotel_ta_mapping.hotel.id

    def get_hotelogix_id(self, obj):
        return obj.hotel_ta_mapping.hotel.hotelogix_id

    def get_hotel_name(self, obj):
        return obj.hotel_ta_mapping.hotel.name

    def get_ta_location_id(self, obj):
        return obj.hotel_ta_mapping.ta_location_id

    get_hotel_id.short_description = 'Hotel ID'
    get_hotelogix_id.short_description = 'Hotelogix ID'
    get_hotel_name.short_description = 'Hotel Name'
    get_ta_location_id.short_description = 'TA Loc ID'
