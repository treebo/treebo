# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('answer_data', models.TextField(null=True, blank=True)),
                ('user', models.TextField(null=True, blank=True)),
                ('state', models.CharField(default=b'NOT_PROCESSED', max_length=100, choices=[(b'NOT_PROCESSED', b'NOT_PROCESSED'), (b'APPROVED', b'APPROVED'), (b'REJECTED', b'REJECTED')])),
                ('verified', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='QAMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('page_type', models.CharField(max_length=100, choices=[(b'', b''), (b'CITY', b'CITY'), (b'LANDMARK', b'LANDMARK'), (b'CATEGORY', b'CATEGORY'), (b'LOCALITY', b'LOCALITY'), (b'HD', b'HD')])),
                ('status', models.BooleanField(default=False)),
                ('priority', models.DecimalField(max_digits=6, decimal_places=5, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)])),
                ('page_type_id', models.CharField(max_length=50)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='QAToggle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('enable_qa', models.BooleanField(default=True)),
                ('enable_for_all_pages', models.BooleanField(default=False)),
                ('count_of_questions', models.PositiveIntegerField(default=0)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('question_data', models.TextField(null=True, blank=True)),
                ('user', models.TextField(null=True, blank=True)),
                ('state', models.CharField(default=b'NOT_PROCESSED', max_length=100, choices=[(b'NOT_PROCESSED', b'NOT_PROCESSED'), (b'APPROVED', b'APPROVED'), (b'REJECTED', b'REJECTED')])),
                ('status', models.BooleanField(default=False)),
                ('submitted_by_user', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='qamapping',
            name='question',
            field=models.ForeignKey(blank=True, to='posts.Question', null=True, on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='answer',
            name='related_question',
            field=models.ForeignKey(blank=True, to='posts.Question', null=True, on_delete=models.DO_NOTHING),
        ),
    ]
