from apps.seo.components.base_component import BaseComponent
from apps.seo.services.near_by_poi.fetch_poi_for_hotel import FetchPOIForHotel
from apps.seo.serializers.poi_serializer import NearbyPOISerializer
import logging

logger = logging.getLogger(__name__)


class NearbyPOI(BaseComponent):

    def __init__(self, city, footer_data, hotel):
        self.hotel = hotel
        super(NearbyPOI, self).__init__(city, footer_data, [])

    def build(self):
        content = FetchPOIForHotel().fetch_poi_for_hotel(hotel_id=self.hotel.id)
        if len(content) == 0:
            data = {}
        else:
            data = {
                'title': "Nearby places and landmarks",
                'content': content
            }
        serialized_obj = NearbyPOISerializer(data=data)
        if serialized_obj.is_valid():
            data = serialized_obj.validated_data
            return {
                'nearby': data
            }
        else:
            logger.info("serializer validation error")
            logger.info(serialized_obj.errors)
        return {
            'nearby': {}
        }
