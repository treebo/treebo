import logging

from base.views.api import TreeboAPIView
from rest_framework.response import Response
from django.core.management import call_command

logger = logging.getLogger(__name__)


class UpdateCachePrice(TreeboAPIView):

    def get(self, request, *args, **kwargs):
        """
        call update_min_price command
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            call_command('update_cache_prices')
            response = {'message': 'Updating cache prices done successfully'}
        except Exception as e:
            logger.error(str(e))
            response = {'message': 'Error in updating cached prices'}
        return Response(response)

