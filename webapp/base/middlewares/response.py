import urllib.parse

from django.conf import settings


class ResponseMiddleWare(object):
    def process_response(self, request, response):
        ''' This code is for conditional pixel
            Setting utm_source and utm_medium in cookies if they are present in url '''
        request_full_path = request.get_full_path()
        parameters_list = urllib.parse.urlparse(request_full_path)
        utm_campaign = urllib.parse.parse_qs(
            parameters_list.query).get(
            'utm_campaign', None)
        utm_source = urllib.parse.parse_qs(
            parameters_list.query).get(
            'utm_source', None)
        utm_medium = urllib.parse.parse_qs(
            parameters_list.query).get(
            'utm_medium', None)
        avclk = urllib.parse.parse_qs(parameters_list.query).get('avclk', None)

        expires = max_age = 30 * 24 * 60 * 60  # one month
        if utm_source:
            ResponseMiddleWare.set_cookie(
                response, 'utm_source', utm_source[0], max_age, expires)

        if utm_medium:
            ResponseMiddleWare.set_cookie(
                response, 'utm_medium', utm_medium[0], max_age, expires)

        if utm_campaign:
            ResponseMiddleWare.set_cookie(
                response, 'utm_campaign', utm_campaign[0], max_age, expires)

        if avclk:
            ResponseMiddleWare.set_cookie(
                response, 'avclk', avclk[0], max_age, expires)

        return response

    @staticmethod
    def set_cookie(response, key, value, max_age, expires):
        response.set_cookie(key, value, max_age=max_age, expires=expires,
                            domain=settings.SESSION_COOKIE_DOMAIN,
                            secure=settings.SESSION_COOKIE_SECURE or None)
