from apps.payments.models import PaymentOffer as PaymentOfferModel
from apps.payments.offers.dataclasses.payment_method import PaymentMethod
from apps.payments.offers.dataclasses.offer import PaymentOffer


class PaymentOfferRespository:
    def get_payment_offer_for_coupon_code(self, payment_offer_coupon_code, utm_source, channel):
        try:
            payment_offer = PaymentOfferModel.objects.get(
                coupon_code=payment_offer_coupon_code,
                utm_source=utm_source,
                channel=channel
            )
            return self._convert_to_dataclass(payment_offer)
        except PaymentOfferModel.DoesNotExist as exc:
            return None

    def auto_applicable_offers_for_payment_method(self, payment_method_id, utm_source):
        payment_offers = PaymentOfferModel.objects.filter(
            payment_method_id=payment_method_id, utm_source=utm_source, auto_apply=True)
        if not payment_offers:
            return []
        return [self._convert_to_dataclass(payment_offer) for payment_offer in payment_offers]

    def get_auto_applicable_payment_offers_for_utm(self, utm_source, channel):
        payment_offers = PaymentOfferModel.objects.filter(
            utm_source=utm_source, auto_apply=True, channel=channel).order_by('-id')
        if not payment_offers:
            return None
        return [self._convert_to_dataclass(payment_offer) for payment_offer in payment_offers]

    def get_offer_for_offer_id(self, payment_offer_id):
        try:
            payment_offer = PaymentOfferModel.objects.get(
                id=payment_offer_id)
            return self._convert_to_dataclass(payment_offer)
        except PaymentOfferModel.DoesNotExist as exc:
            return None

    def get_offers_for_utm(self, utm_source):
        payment_offers = PaymentOfferModel.objects.filter(
            utm_source=utm_source)
        if not payment_offers:
            return []
        return [self._convert_to_dataclass(payment_offer) for payment_offer in payment_offers]

    def _convert_to_dataclass(self, payment_offer):
        return PaymentOffer(uid=payment_offer.id,
                            payment_method=PaymentMethod(
                                uid=payment_offer.payment_method.id,
                                name=payment_offer.payment_method.name,
                                category=payment_offer.payment_method.category,
                                image_url=payment_offer.payment_method.image_url,
                                gateway_code=payment_offer.payment_method.gateway_code,
                                ranking=payment_offer.payment_method.ranking
                            ),
                            title=payment_offer.offer_title,
                            offer_percentage=payment_offer.offer_percentage,
                            credit_mode=payment_offer.credit_mode,
                            minimum_transaction=payment_offer.minimum_transaction,
                            ranking=payment_offer.ranking,
                            description=payment_offer.description,
                            terms_conditions=payment_offer.terms_conditions,
                            gateway_offer_id=payment_offer.gateway_offer_id,
                            image_url=payment_offer.image_url,
                            coupon_code=payment_offer.coupon_code
                            )
