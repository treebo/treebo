# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0013_auto_20160304_0738'),
    ]

    operations = [
        migrations.AddField(
            model_name='facility',
            name='rooms',
            field=models.ManyToManyField(to='dbcommon.Room'),
        ),
    ]
