from rest_framework import serializers

from apps.auth.dto.otp_login_dto import NOT_AVAILABLE
from apps.common.validators import CustomValidator as custom_validator


class OtpVerifyDTO(serializers.Serializer):
    phone_number = serializers.CharField(max_length=50)
    verification_code = serializers.CharField(max_length=50)
    channel = serializers.CharField(max_length=20, required=False, default=NOT_AVAILABLE)

    def validate_phone_number(self, phone_number):
        return custom_validator.normalise_phone_number(phone_number)
