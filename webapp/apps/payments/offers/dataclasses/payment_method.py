from apps.common.data_classes.base_data_class import BaseDataClass


class PaymentMethod(BaseDataClass):
    def __init__(self, uid, name, category, image_url=None, gateway_code=None, ranking=None):
        self.uid = uid
        self.name = name
        self.category = category
        self.image_url = image_url
        self.gateway_code = gateway_code
        self.ranking = ranking
