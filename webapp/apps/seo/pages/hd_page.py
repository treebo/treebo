from django.conf import settings
from django.core.cache import cache
from django.utils.text import slugify

from apps.hotels.serializers.seo_serializer import HotelSeoDetailsSerializer
from apps.hotels.service.hotel_service import HotelService
from apps.seo.components.about_hotel import AboutHotel
from apps.seo.components.breadcrumb import BreadCrumb
from apps.seo.components.categories import Category
from apps.seo.components.categorized_facilities import CategorizedFacilities
from apps.seo.components.near_by_locality import NearbyLocalities
from apps.seo.components.nearby_landmarks import NearbyLandmarks
from apps.seo.components.nearby_poi import NearbyPOI
from apps.seo.components.things_guest_love import ThingsGuestLove
from apps.seo.components.top_cities import TopCities
from apps.seo.components.top_rated import TopRated
from apps.seo.pages.pages import PAGES

from apps.hotels.serializers.seo_serializer import HotelSeoDetailsSerializer
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import SeoDetails
from django.utils.text import slugify

__author__ = 'ansilkareem'

hotel_repository = RepositoriesFactory.get_hotel_repository()


class HDPage(PAGES):

    def __init__(self, page, query, is_amp):
        self.is_amp = is_amp
        self.max_locations_to_show = settings.HD_LOCATION_CAP
        self.hotel_id = int(query)
        self.hotel = hotel_repository.get_hotel_by_id(hotel_id=self.hotel_id)
        self.hotel_name = HotelService().get_display_name_from_hotel_name(self.hotel.name)
        super(HDPage, self).__init__(page, query, self.hotel_name)

    def update_meta(self):
        min_price_key = str(self.hotel_id) + '_min_price'
        min_sell_price_key = str(self.hotel_id) + '_min_sell_price'
        min_rack_rate_key = str(self.hotel_id) + '_min_rack_rate'
        max_discount_key = str(self.hotel_id) + '_max_discount'
        min_price = cache.get(min_price_key)
        min_sell_price = cache.get(min_sell_price_key)
        min_rack_rate = cache.get(min_rack_rate_key)
        max_discount = cache.get(max_discount_key)
        if max_discount:
            total_discount = str(max_discount) + "%"
        else:
            total_discount = str(settings.DEFAULT_DISCOUNT) + "%"
        temp_dict = {
            "hotel_name": self.hotel.name.title(),
            "city_name": self.hotel.city.slug.title() if self.hotel.city.slug else self.hotel.city.name.title(),
            "locality_name": self.hotel.locality.name.title(),
            "price": str(min_price) if min_price else str(settings.DEFAULT_PRICE),
            "sell_price": str(min_sell_price) if min_sell_price else str(
                settings.DEFAULT_SELL_PRICE),
            "rack_rate": str(min_rack_rate) if min_rack_rate else str(settings.DEFAULT_RACK_RATE),
            "total_discount": total_discount
        }
        amp_url = settings.SITE_HOST_NAME + 'hotels-in-' + \
                  slugify(self.hotel.city.slug) + '/' + slugify(self.hotel.name) + '-' + slugify(
            self.hotel.locality.name) \
                  + '-' + str(self.hotel.id) + '/amp/'
        seo_details = SeoDetails.objects.filter(hotel_id=self.hotel.id).first()
        if seo_details:
            serialized_seo_details = HotelSeoDetailsSerializer(instance=seo_details).data
            canonical_url = serialized_seo_details.get("canonical_url")
            if canonical_url:
                full_canonical_url = settings.SITE_HOST_NAME + canonical_url
            else:
                full_canonical_url = settings.SITE_HOST_NAME + 'hotels-in-' + \
                                     slugify(self.hotel.city.slug) + '/' + slugify(
                    self.hotel.name) + '-' + slugify(
                    self.hotel.locality.name) \
                                     + '-' + str(self.hotel.id) + '/'
            title = serialized_seo_details["title"] % temp_dict
            description = serialized_seo_details["description"] % temp_dict
            self.override_seo_content(title,
                                      description,
                                      serialized_seo_details["keyword"],
                                      full_canonical_url, is_amp=self.is_amp, amp_url=amp_url)
        else:
            full_canonical_url = settings.SITE_HOST_NAME + 'hotels-in-' + \
                                 slugify(self.hotel.city.slug) + '/' + slugify(
                self.hotel.name) + '-' + slugify(
                self.hotel.locality.name) \
                                 + '-' + str(self.hotel.id) + '/'
            title = None
            description = None
            if 'hd_title' in self.footer_data:
                title = self.footer_data['hd_title'] % temp_dict
            if 'hd_description' in self.footer_data:
                description = self.footer_data['hd_description'] % temp_dict
            if title and description:
                self.override_seo_content(title,
                                          description,
                                          None,
                                          full_canonical_url, is_amp=self.is_amp, amp_url=amp_url)

    def build_meta(self, *args):
        self.update_meta()
        about_hotel = AboutHotel(self.hotel)
        top_rated = TopRated(self.hotel.city, self.footer_data, [self.hotel.id])
        top_cities = TopCities(self.hotel.city, self.footer_data, [self.hotel.city.id])
        categories = Category(self.hotel.city, self.footer_data, [self.hotel.id])
        things_guest_love = ThingsGuestLove(self.hotel.city, self.footer_data, self.hotel)
        facilities = CategorizedFacilities(self.hotel.city, self.footer_data, self.hotel)
        near_by_poi = NearbyPOI(self.hotel.city, self.footer_data, self.hotel)
        nearby_localities = NearbyLocalities(self.hotel.city, self.footer_data, self.hotel.latitude,
                                             self.hotel.longitude, self.max_locations_to_show)
        nearby_landmarks = NearbyLandmarks(self.hotel.city, self.footer_data, self.hotel.latitude,
                                           self.hotel.longitude, self.max_locations_to_show)
        self.meta_content['content'] = {
            'links': [top_rated.build(), top_cities.build(), nearby_localities.build(),
                      nearby_landmarks.build(),
                      categories.build()],
            'breadcrumbs': BreadCrumb(self.hotel.city, self.footer_data, self.page,
                                      self.item_name).build(),
        }
        self.meta_content['content'].update(about_hotel.build())
        self.meta_content['content'].update(things_guest_love.build())
        self.meta_content['content'].update(near_by_poi.build())
        self.meta_content['content'].update(facilities.build())
