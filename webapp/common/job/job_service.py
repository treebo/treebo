import datetime
import logging

from django.conf import settings
from easyjoblite.constants import API_LOCAL
from easyjoblite.orchestrator import Orchestrator
from ths_common.constants.scheduled_job_constants import JobStatus

from common.job import job_executor
from common.job.job_repository import JobRepository

logger = logging.getLogger(__name__)


class JobService:
    def __init__(self):
        self.job_orchestrator = Orchestrator(rabbitmq_url=settings.BROKER_URL,
                                             config_file=settings.EASY_JOB_LITE_SETTINGS_PATH)
        self.job_repository = JobRepository()

    def schedule_oldest_unscheduled_job_for_execution(self):
        job_entity = self.job_repository.get_oldest_schedulable_job()
        if not job_entity:
            return False

        try:
            job_entity = self.schedule_for_execution(job_entity, job_executor.execute_job)
        except Exception as e:
            logger.exception('Error while scheduling job with id: %s', job_entity.job_id)
            raise

        self.job_repository.update(job_entity)
        return job_entity.status == JobStatus.PROCESSING

    def schedule_for_execution(self, job_entity, handler_function):
        try:
            self.job_orchestrator.setup_entities()
            self.job_orchestrator.enqueue_job(api=handler_function,
                                              type=API_LOCAL,
                                              tag=job_entity.job_id,
                                              data=dict(job_id=job_entity.job_id,
                                                        job_name=job_entity.job_name,
                                                        job_args=job_entity.data))
            job_entity.status = JobStatus.PROCESSING
            job_entity.picked_at = datetime.datetime.now(datetime.timezone.utc)
            return job_entity
        except Exception as e:
            job_entity.status = JobStatus.FAILED
            job_entity.failure_message = str(e)
            return job_entity
