# -*- coding: utf-8 -*-

from __future__ import division

import uuid

from apps.common.data_classes.base_data_class import BaseDataClass
from apps.common.data_classes.container import Container
from apps.bookings.data_classes.guest_stay import GuestStays
from apps.bookings.data_classes.status import Status, status_in_active_statuses, InitiatedStatus
from apps.hotels.data_classes.room_types import RoomType
from apps.pricing.data_classes import Price


# pylint: disable=too-many-instance-attributes
class RoomStay(BaseDataClass):
    def __init__(self, uid, room_type):
        self.uid = str(uid)
        self._generated_uid = False
        if not self.uid:
            self.uid = uuid.uuid4()
            self._generated_uid = True

        assert isinstance(room_type, RoomType)
        self.type = room_type

        self._guest_stays = frozenset()
        self._price = None

        self._status = InitiatedStatus

        self.charge_ids = set()

    @property
    def name(self):
        return str(self.type.name).lower()

    @property
    def room_type_id(self):
        return str(self.type.uid)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        assert isinstance(status, Status)
        self._status = status

    @property
    def is_active(self):
        return status_in_active_statuses(self.status)

    @property
    def price(self):
        pass

    @price.setter
    def price(self, price):
        assert isinstance(price, Price)
        self._price = price

    @price.getter
    def price(self):
        """ Use this to add prices if prices move to guest stay level"""
        return self._price

    @property
    def guest_stays(self):
        return self._guest_stays

    @property
    def active_guest_stays(self):
        return [gs for gs in self._guest_stays if gs.is_active]

    @guest_stays.setter
    def guest_stays(self, guest_stays):
        self._guest_stays = GuestStays(guest_stays)

    @property
    def guests(self):
        return set([guest_stay.guest for guest_stay in self.guest_stays])

    @property
    def checkin(self):
        if not self.guest_stays:
            raise RuntimeError('No guest stays included')
        checkins = [gs.checkin for gs in self.active_guest_stays]
        if not checkins:
            # happens if the whole room stay is cancelled
            checkins = [gs.checkin for gs in self.guest_stays]

        return min(checkins)

    @property
    def checkout(self):
        if not self.guest_stays:
            raise RuntimeError('No guest stays included')

        checkouts = [gs.checkout for gs in self.active_guest_stays]
        if not checkouts:
            # happens if the whole room stay is cancelled
            checkouts = [gs.checkout for gs in self.guest_stays]

        return max(checkouts)

    def __eq__(self, other):
        if isinstance(other, RoomStay):
            return hash(self) == hash(other)
        return NotImplemented

    def __hash__(self):
        # self.name is an alias for room_type_id and it should be readonly
        return hash(tuple([self.uid, self.room_type_id, self._guest_stays, self._price]))

    def __repr__(self):
        return '<{kls} ({p} {ci} -> {co})>'.format(kls=self.__class__.__name__,
                                                   p=self.uid,
                                                   ci=self.checkin,
                                                   co=self.checkout,
                                                   )

    def __str__(self):
        return '{n}: {ci} -> {co}, {gs}'.format(n=self.name,
                                                ci=self.checkin.date(),
                                                co=self.checkout.date(),
                                                gs=','.join(map(str, self.guest_stays)),
                                                )


class RoomStays(Container):
    def __sub__(self, other):
        ret = super(RoomStays, self).__sub__(other)
        if ret is not NotImplemented:
            return [item for item in ret if item.is_active]
        return NotImplemented
