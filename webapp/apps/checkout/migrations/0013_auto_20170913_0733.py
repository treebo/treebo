# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0012_bookingrequest_member_discount'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='booking_channel',
            field=models.CharField(
                default='',
                max_length=50,
                blank=True,
                choices=[
                    ('msite',
                     'msite'),
                    ('website',
                     'website'),
                    ('app',
                     'app'),
                    ('gdc',
                     'gdc'),
                    ('android',
                     'android'),
                    ('ios',
                     'ios')]),
        ),
    ]
