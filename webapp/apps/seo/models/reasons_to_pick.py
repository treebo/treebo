from dbcommon.models.hotel import Hotel

__author__ = 'ansilkareem'

from djutil.models import TimeStampedModel
from django.db import models


class ReasonsToPick(TimeStampedModel):

    reason = models.TextField(null=True, blank=True)
    hotel = models.ForeignKey(
        Hotel,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING)
