import mock
from django.test import SimpleTestCase

from apps.bookings.models import Booking
from apps.checkout.models import BookingRequest
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room
from services.restclient import bookingrestclient
from services.restclient.bookingrestclient import BookingClient


class BookingClientTest(SimpleTestCase):
    def setUp(self):
        pass

    #@mock.patch.object(bookingrestclient.BookingService, 'create_booking')
    #@mock.patch.object(bookingrestclient.BookingService, 'build_special_preference')
    #@mock.patch.object(bookingrestclient.BookingClient, '_BookingClient__get_booking_data')
    #@mock.patch.object(bookingrestclient, 'BookingRequestSerializer')
    # def test_create_booking(self, mock_serializer, mock_get_booking_data, mock_build_special_preference,
    #                        mock_create_booking, mock_get_room, mock_get_hotel):
    #    mock_booking_request = mock.Mock(spec=BookingRequest, **{"voucher_amount": 100})
    #    mock_serializer.return_value = mock.Mock()
    #    mock_serializer.return_value.data = {"hotel_id": 1, "room_type": "Oak"}
    #    mock_get_booking_data.return_value = dict()
    #    mock_build_special_preference.return_value = "Test Comment"
    #    mock_create_booking.return_value = mock.Mock(spec=Booking)
    #    mock_get_room.return_value = mock.Mock(spec=Room)
    #    mock_get_hotel.return_value = mock.Mock(spec=Hotel)
    #    booking = BookingClient.create_booking(mock_booking_request)
    #    self.assertEqual(booking.payment_amount, mock_booking_request.voucher_amount)
    #    self.assertEqual(booking.hotel, mock_get_hotel.return_value)
