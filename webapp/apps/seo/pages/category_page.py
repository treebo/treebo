from django.conf import settings
from django.utils.text import slugify
from django.db.models import Q
from apps.seo.components.breadcrumb import BreadCrumb

from apps.seo.pages.pages import PAGES
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Categories, CategorySeoDetails
from apps.seo.components.categories import Category
from apps.seo.components.cities_nearby import CitiesNearby
from apps.seo.components.landmarks import Landmarks
from apps.seo.components.localities import Localities
from apps.seo.components.popular_cities import PopularCities
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'ansilkareem'


class CategoryPage(PAGES):

    def __init__(self, page, query):
        city = query.split('-')[-1].strip('-')
        self.category_name = ' '.join(query.split('-')[:-1]).strip()
        self.category = Categories.objects.filter(
            status=1, name__iexact=self.category_name).first()
        city_data_service = RepositoriesFactory.get_city_repository()
        city_search_args = []
        city_search_args.append(
            {'name': 'name', 'value': city, 'case_sensitive': False, 'partial': False})
        city_search_args.append(
            {'name': 'slug', 'value': city, 'case_sensitive': False, 'partial': False})
        current_cities = city_data_service.filter_cities_by_string_matching(
            city_search_args, {'status': 1})
        if current_cities:
            self.current_city = current_cities[0]
        else:
            self.current_city = None
        #self.current_city = City.objects.filter(Q(name__iexact=city, status=1) | Q(slug__iexact=city, status=1)).first()
        self.about_key = 'about_' + '_'.join(self.category_name.lower(
        ).split()) + '_' + '_'.join(self.current_city.name.lower().split())
        self.description = ContentServiceClient.getValueForKey(
            None, self.about_key, 1)
        super(CategoryPage, self).__init__(page, query, self.category_name)

    def update_meta(self, ):
        category_seo_details = CategorySeoDetails.objects.filter(
            category=self.category, city=self.current_city).first()
        full_canonical_url = settings.SITE_HOST_NAME + \
            slugify(self.category.name) + '-in-' + slugify(self.current_city.slug) + '/'
        if category_seo_details:
            self.override_seo_content(
                category_seo_details.category_seo_title,
                category_seo_details.category_seo_description,
                category_seo_details.category_seo_keyword,
                full_canonical_url)
        else:
            title = None
            description = None
            temp_dict = {
                "category_name": self.category.name,
                "city_name": self.current_city.name
            }
            if 'category_title' in self.footer_data:
                title = self.footer_data['category_title'] % temp_dict
            if 'category_description' in self.footer_data:
                description = self.footer_data['category_description'] % temp_dict
            if title and description:
                self.override_seo_content(title,
                                          description,
                                          None,
                                          full_canonical_url)

    def build_meta(self):
        self.update_meta()
        self.meta_content['content'] = {
            'description': self.description['text'] if self.description else ''
        }
        category = Category(
            self.current_city, self.footer_data, [
                self.category.id])
        landmark = Landmarks(self.current_city, self.footer_data, [])
        locality = Localities(self.current_city, self.footer_data, [])
        cities_nearby = CitiesNearby(self.current_city, self.footer_data)
        self.meta_content['content'].update(
            {
                'links': [
                    locality.build(),
                    landmark.build(),
                    category.build(),
                    cities_nearby.build()],
                'breadcrumbs': BreadCrumb(
                    self.current_city,
                    self.footer_data,
                    self.page,
                    self.item_name).build()})
        popular_cities = PopularCities(
            self.current_city, self.footer_data, [
                city.id for city in cities_nearby.sorted_nearby_cities])
        self.meta_content['content']['links'].append(popular_cities.build())
