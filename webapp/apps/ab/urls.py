# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.ab.api.v1.assign import AssignAllApi

urlpatterns = [
    # url(r"^assign/", AssignApi.as_view(), name='assign-v1'),
    url(r"^assign_all/", AssignAllApi.as_view(), name='assign_all-v1'),
]
