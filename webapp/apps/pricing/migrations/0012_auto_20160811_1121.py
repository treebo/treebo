# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pricing', '0011_featuretoggle_environment'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='roompricev2',
            index_together=set([('room', 'date', 'guest_count')]),
        ),
    ]
