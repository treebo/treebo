import logging
from http import HTTPStatus
from django.conf import settings
from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.checkout.service.itinerary_handler_v2 import ItineraryHandlerV2
from base.decorator.request_validator import validate_request
from base.views.api_v2 import TreeboAPIView2
from apps.common.api_response import APIResponse as api_response
logger = logging.getLogger(__name__)


class RemoveCouponSerializer(serializers.Serializer):
    couponcode = serializers.CharField(max_length=15, allow_blank=True, required=False)
    bid = serializers.IntegerField()
    apply_wallet = serializers.BooleanField(default=settings.APPLY_WALLET, required=False)


class RemoveCoupon(TreeboAPIView2):
    authentication_classes = (BasicAuthentication, CsrfExemptSessionAuthentication)
    checkout_service = ItineraryHandlerV2()

    @validate_request(RemoveCouponSerializer)
    def post(self, request, *args, **kwargs):
        """
        Gets the discount for a booking id and coupon code.
        couponcode -- Coupon Code
        bid -- BookingRequest Id
        """

        try:
            validated_data = kwargs['validated_data']
            bid, apply_wallet = validated_data['bid'], validated_data['apply_wallet']
            price_data = self.checkout_service.apply_coupon(request, bid, None, apply_wallet)
            price_data['coupon_desc'] = ""
            price_data['is_prepaid'] = ""
            response = api_response.prep_success_response(price_data)
        except Exception as exception:
            logger.exception('Exception occurred in remove coupon')
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param=request.GET,
                                                        dev_msg="Remove coupon",
                                                        message=exception.__str__(),
                                                        class_name=self.__class__.__name__)
            response = api_response.prep_simple_error_response(msg="There was an error removing this code. "
                                                                   "Please try another coupon.",
                                                               status_code=400, code=400)
        return response
