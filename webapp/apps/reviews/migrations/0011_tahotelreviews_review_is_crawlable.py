# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0009_auto_20170908_0701'),
    ]

    operations = [
        migrations.AddField(
            model_name='tahotelreviews',
            name='review_is_crawlable',
            field=models.BooleanField(default=False),
        ),
    ]
