const $ = require('jquery');

import _ from 'underscore';
import "slick-carousel";
import {Observable} from "rx";
import BaseView from './treebolibs/baseview';
import logger from './treebolibs/logger';
import treejax from './treebolibs/treejax';
import * as toastr from 'toastr';
import '../css/joinus.less';

let $loader = $("#commonLoader");
let $csrfMiddlewareToken = $('input[name="csrfmiddlewaretoken"]').val();
let JoinView = Object.create(BaseView);
JoinView.pageLoaded = () => {

    $(".play-image").click(e => {
      $("#video").removeClass("hide");
    });

  const withPrior = $('.prior-experience').clone().removeClass('hide');
  const noPrior = $('.no-prior-experience').clone().removeClass('hide');
  $('.prior-container').empty();

  $('input[type=radio][name=priorExperience]').on('change', function()  {
    const value = this.value;

    if (value === 'YES') {
      $('.prior-container').empty().append(withPrior);
    } else if (value === 'NO') {
      $('.prior-container').empty().append(noPrior);
    }
  });

  $('.slide-show').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
    });
    logger.log(" Join page loaded");
}

$(document).keyup(e => {
  		if (e.keyCode == 27)
  			$("#video").addClass("hide");   // esc
        $('#youtube_player')[0].contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
});

$('#stop-cross').click(() => {
    $('#youtube_player')[0].contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
});

let submitFeedbackEvent = Rx.Observable.fromEvent($('#feedbackSubmitForm'), 'click');
let submitFeedbacksubscription = submitFeedbackEvent.subscribe(e => {


    const feedbackParsley = $('#joinusForm').parsley();
    feedbackParsley.on('form:submit',() => {
      $loader.removeClass('hide')
    	submitFeedback(e);
        return false;
    });
    return false;
});

let submitFeedback = ()=>{
    const data = $(".joinusForm__form").serializeArray();
    console.log(data);
    const joinus_json = _.object(_.map(data, _.values));
    joinus_json['form-type'] = "joinusForm__form";
    joinus_json['csrfmiddlewaretoken'] = $csrfMiddlewareToken;

    const p = treejax({
            url: '/rest/v1/joinussave/',
            type: 'POST',
            data: joinus_json
        }).then((data, status, xhr) => {
              toastr.success('Your form is submitted successfully. We will get in touch with you shortly', 'Thank You!', {timeOut: 5000, closeButton: true});

        },treeboError => {
              toastr.error('Your form could not be submitted. Please try again later.', 'Sorry!', {timeOut: 5000, closeButton: true});
        }).finally(() => {
          $loader.addClass('hide');
          $("#joinusForm")[0].reset();
        });
    return p;
}

let config = { name:'', widgets:[]};
let joinView = JoinView.setup(config);
