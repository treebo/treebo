# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fot', '0009_auto_20161110_0902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fot',
            name='user_credits',
            field=models.PositiveIntegerField(
                default=0,
                db_column='credits_available'),
        ),
    ]
