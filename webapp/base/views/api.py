import datetime
import logging

from django.conf import settings
from django.http import JsonResponse
from rest_framework.exceptions import ValidationError
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.views import APIView

from base.renderers import TreeboJSONRenderer

logger = logging.getLogger(__name__)


class TreeboValidationMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.method == 'GET':
            data = request.GET
        else:
            data = request.POST

        method = request.method.lower()
        if self.validationSerializer:
            if isinstance(
                    self.validationSerializer,
                    dict) and method in self.validationSerializer and self.validationSerializer[method]:
                self.serializerObject = self.validationSerializer[method](
                    data=data)
            else:
                self.serializerObject = self.validationSerializer(data=data)

            try:
                self.serializerObject.is_valid(raise_exception=True)
            except ValidationError as e:
                return JsonResponse(
                    {'errors': self.serializerObject.errors}, status=400)
        return super(
            TreeboValidationMixin,
            self).dispatch(
            request,
            *
            args,
            **kwargs)


class TreeboAPIView(TreeboValidationMixin, APIView):
    renderer_classes = (TreeboJSONRenderer,)

    validationSerializer = None
    serializerObject = None

    def initialize_request(self, request, *args, **kwargs):
        request = super(
            TreeboAPIView,
            self).initialize_request(
            request,
            *
            args,
            **kwargs)
        if settings.DEBUG:
            self.renderer_classes += (BrowsableAPIRenderer,)
        return request

    def dispatch(self, request, *args, **kwargs):

        st = datetime.datetime.now()
        dispatcherResponse = super(
            TreeboAPIView,
            self).dispatch(
            request,
            *args,
            **kwargs)
        et = datetime.datetime.now()

        logger.info(
            "API {api} took {time} ms", extra={
                "api": str(
                    self.__class__.__name__), "time": int(
                    (et - st).total_seconds() * 1000)})
        return dispatcherResponse
