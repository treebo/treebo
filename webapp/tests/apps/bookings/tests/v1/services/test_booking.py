import mock
from datetime import datetime, time
from django.test import SimpleTestCase

from services.hotellogix.exceptions import AddToCartException, EmptyRateIdException
from tests.apps.bookings.tests.test_factory import ResponseXmlFactory
from apps.bookings.models import Booking, RoomBooking
from apps.checkout.models import BookingRequest
from apps.bookings.services import booking
from apps.bookings.services.booking import BookingService


# class BookingServiceAddToCartTest(SimpleTestCase):
#    def setUp(self):
#        self.loadcart_patcher = mock.patch.object(booking.HxService, 'loadCartHotelogix')
#        self.addtocart_patcher = mock.patch.object(booking.HxService, 'addToCartHotelId')
#        self.deletefromcart_patcher = mock.patch.object(booking.HxService, 'deleteFromCart')
#        self.mock_hx_loadcart = self.loadcart_patcher.start()
#        self.mock_hx_addtocart = self.addtocart_patcher.start()
#        self.mock_hx_deletefromcart = self.deletefromcart_patcher.start()
#
#    def tearDown(self):
#        self.loadcart_patcher.stop()
#        self.addtocart_patcher.stop()
#        self.deletefromcart_patcher.stop()
#
#    def test_addtocart_raise_exception(self):
#        self.mock_hx_loadcart.side_effect = Exception()
#        with self.assertRaises(AddToCartException):
#            BookingService.addToCart(mock.Mock(), mock.Mock())
#
#    def test_addtocart_with_data_in_cart(self):
#        self.mock_hx_loadcart.return_value = ResponseXmlFactory.build_cart_xml_with_item()
#        self.mock_hx_addtocart.return_value = ResponseXmlFactory.build_cart_xml_with_item()
#        responseXml, hotel_id = BookingService.addToCart(mock.Mock(), mock.Mock())
#        self.assertEqual(responseXml, self.mock_hx_addtocart.return_value)
#        self.assertEqual(hotel_id, "0qg8r-GlSwiUUzqkQB08fplO6jDLRCv6JVuTpYDUIVI|")
#        self.assertEquals(self.mock_hx_deletefromcart.call_count, 1)
#        self.assertEquals(self.mock_hx_addtocart.call_count, 1)
#
#    def test_addtocart_on_empty_cart(self):
#        self.mock_hx_loadcart.return_value = ResponseXmlFactory.build_empty_cart()
#        self.mock_hx_addtocart.return_value = ResponseXmlFactory.build_cart_xml_with_item()
#        responseXml, hotel_id = BookingService.addToCart(mock.Mock(), mock.Mock())
#        self.assertEqual(responseXml, self.mock_hx_addtocart.return_value)
#        self.assertEqual(hotel_id, "0qg8r-GlSwiUUzqkQB08fplO6jDLRCv6JVuTpYDUIVI|")
#        self.assertEquals(self.mock_hx_deletefromcart.call_count, 0)
#        self.assertEquals(self.mock_hx_addtocart.call_count, 1)


# class BookingServiceGetRateIdTest(SimpleTestCase):
#    @mock.patch.object(booking.BookingService, '_BookingService__parse_rate_id_xml_for_all_roomtypes')
#    def test_get_rate_ids_empty_rate(self, mock_parse_rate_id):
#        mock_parse_rate_id.return_value = dict()
#        with self.assertRaises(EmptyRateIdException):
#            BookingService.getMultipleRateIds(mock.Mock(), mock.Mock(), mock.Mock(),
#                                              mock.Mock(), mock.Mock(), [(1,0), (2,0)], mock.Mock())
#
#    @mock.patch.object(booking.BookingService, '_BookingService__parse_rate_id_xml_for_all_roomtypes')
#    def test_get_rate_ids_valid_rates(self, mock_parse_rate_id):
#        mock_room_type = mock.Mock()
#        mock_parse_rate_id.return_value = {mock_room_type: {1: "rate_id_one", 2: "rate_id_two", 3: "rate_id_three"}}
#        rate_ids = BookingService.getMultipleRateIds(mock.Mock(), mock.Mock(), mock.Mock(),
#                                                     mock.Mock(), mock_room_type, [(1,0), (2,0)], mock.Mock())
#        self.assertEquals(rate_ids, ["rate_id_one", "rate_id_two"])


# class BookingServiceParseRateIdTest(SimpleTestCase):
#    def test_parse_empty_rate_xml(self):
#        xml = ResponseXmlFactory.build_empty_rate_id_xml()
#        parsed_rate_ids = BookingService._BookingService__parse_rate_id_xml_for_all_roomtypes(xml)
#        for rate_id in parsed_rate_ids.values():
#            self.assertEqual(rate_id, {})
#
#    def test_parse_rate_xml_without_treebo_promotional(self):
#        xml = ResponseXmlFactory.build_rate_id_xml_without_treebo_promotional()
#        parsed_rate_ids = BookingService._BookingService__parse_rate_id_xml_for_all_roomtypes(xml)
#        for rate_id in parsed_rate_ids.values():
#            self.assertEqual(rate_id, {})
#
#    def test_parse_rate_xml_valid_rates(self):
#        xml = ResponseXmlFactory.build_valid_rate_id_xml()
#        parsed_rate_ids = BookingService._BookingService__parse_rate_id_xml_for_all_roomtypes(xml)
#        self.assertEqual(parsed_rate_ids, {"Oak": {1: "one-zero-rate-id", 2: "two-zero-rate-id", 3: "three-zero-rate-id"}})


class BookingServiceCreateBookingTest(SimpleTestCase):
    def test_create_booking_invalid_data(self):
        invalid_booking_data = {}
        result = BookingService.create_booking(invalid_booking_data)
        self.assertIsNone(result)

    @mock.patch.object(booking, 'BookingCreationSerializer')
    def test_create_booking_minimum_valid_data(self, mock_booking_serializer):
        mock_booking = mock.Mock(
            spec=Booking, **{'id': 5, 'order_id': 'TRB-1232341233'})
        mock_booking_serializer_instance = mock.Mock(
            **{'is_valid.return_value': True, 'save.return_value': mock_booking})
        mock_booking_serializer.return_value = mock_booking_serializer_instance
        result = BookingService.create_booking({})
        self.assertEqual(result, mock_booking)


# class BookingServiceConfirmBookingTest(SimpleTestCase):
#    def setUp(self):
#        self.booking_manager_patcher = mock.patch.object(Booking, 'objects')
#        self.mock_booking_manager = self.booking_manager_patcher.start()
#        self.mock_booking = mock.Mock(spec=Booking, **{'id': 2})
#        self.mock_booking_manager.get.return_value = self.mock_booking
#
#        self.booking_request_manager_patcher = mock.patch.object(BookingRequest, 'objects')
#        self.mock_booking_request_manager = self.booking_request_manager_patcher.start()
#        self.mock_booking_request = mock.Mock(spec=BookingRequest, **{'id': 2})
#        self.mock_booking_request_manager.get.return_value = self.mock_booking_request
#
#        self.save_booking_patcher = mock.patch.object(Booking, 'save')
#        self.mock_save_booking = self.save_booking_patcher.start()
#        self.save_booking_request_patcher = mock.patch.object(BookingRequest, 'save')
#        self.mock_save_booking_request = self.save_booking_request_patcher.start()
#        #self.booking_confirmation_notification_patcher = mock.patch.object(BookingService, 'send_booking_confirmation_notification')
#        #self.mock_send_booking_confirmation_notification = self.booking_confirmation_notification_patcher.start()
#        self.create_hx_booking_patcher = mock.patch('apps.bookings.tasks.confirm_booking')
#        self.mock_create_hx_booking = self.create_hx_booking_patcher.start()
#
#    def tearDown(self):
#        self.booking_manager_patcher.stop()
#        self.booking_request_manager_patcher.stop()
#        self.save_booking_patcher.stop()
#        self.save_booking_request_patcher.stop()
#        #self.booking_confirmation_notification_patcher.stop()
#        #self.create_hx_booking_patcher.stop()
#
#    def test_booking_status_should_change_to_Awating(self):
#        self.mock_booking.configure_mock(**{'is_audit': False, 'coupon_code': None, 'order_id': 'TRB-TEST_BOOKING'})
#        BookingService.confirm_booking(self.mock_booking)
#        self.assertEquals(self.mock_booking.booking_status, Booking.AWAITING_HX_CONFIRMATION)
#
#    #def test_booking_request_status_should_change_to_complete(self):
#    #    self.mock_booking.configure_mock(**{'is_audit': False, 'coupon_code': None, 'order_id': 'TRB-TEST_BOOKING'})
#    #    BookingService.confirm_booking(self.mock_booking)
#    #    self.assertEquals(self.mock_booking_request.status, BookingRequest.COMPLETED)
#
#    #def test_booking_confirmation_notification_should_be_sent(self):
#    #    self.mock_booking.configure_mock(**{'is_audit': False, 'coupon_code': None})
#    #    BookingService.confirm_booking(5)
#    #    self.mock_send_booking_confirmation_notification.assert_called_once_with(5)
#
#    #@mock.patch.object(booking.DiscountCoupon, 'save')
#    #@mock.patch.object(booking.DiscountCoupon, 'objects')
#    #def test_coupon_usage_is_decreased(self, mock_discountcoupon_manager, mock_save_discount_coupon):
#    #    from apps.discounts.models import DiscountCoupon
#    #    current_max_available_usage = 1
#    #    mock_discount_coupon = mock.Mock(spec=DiscountCoupon, **{'max_available_usages': current_max_available_usage, 'save': mock_save_discount_coupon})
#    #    mock_discountcoupon_manager.get.return_value = mock_discount_coupon
#    #    self.mock_booking.configure_mock(**{'is_audit': False, 'coupon_code': 'TEST_COUPON', 'coupon_apply': True})
#    #    BookingService.confirm_booking(5)
#    #    self.assertEquals(mock_save_discount_coupon.call_count, 1)
#    #    self.assertEquals(mock_discount_coupon.max_available_usages, current_max_available_usage - 1)
#
#    @mock.patch('apps.bookings.tasks.send_audit_confirmation_mail')
#    @mock.patch('apps.bookings.v1.services.booking.F')
#    @mock.patch('apps.fot.models.FotReservationsRequest.objects')
#    @mock.patch('apps.fot.models.Fot.objects')
#    @mock.patch('apps.fot.models.Fot.save')
#    def test_confirm_fot_booking(self, mock_save_fot, mock_fot_manager, mock_fot_reservations_request_manager, F_mocked,
#                                 mock_send_audit_confirmation):
#        mock_fot_user = mock.Mock(**{'save': mock_save_fot})
#        mock_fot_manager.get.return_value = mock_fot_user
#        mock_fot_reservations_request_manager.create.return_value = mock.Mock()
#        total_audits, future_audits = 5, 10
#        F_mocked.side_effect = [total_audits, future_audits]
#        self.mock_booking.configure_mock(**{
#                    'is_audit': True, 'coupon_code': None, 'user_id': mock.Mock(**{'id': 1}), 'hotel_id': 1,
#                    'checkin_date': datetime(2016, 8, 20), 'checkout_date': datetime(2016, 8, 25), 'adult_count': 5
#                })
#        BookingService.confirm_booking(5)
#
#        self.assertEquals(mock_fot_reservations_request_manager.create.call_count, 1)
#        mock_fot_manager.get.assert_called_once_with(user_id=self.mock_booking.user_id.id)
#        self.assertEquals(mock_save_fot.call_count, 1)
#        self.assertEquals(mock_fot_user.total_audits_scheduled, total_audits + 1)
#        self.assertEquals(mock_fot_user.future_audits, future_audits + 1)
#        mock_send_audit_confirmation.delay.assert_called_once_with(5)
#
#    #def test_create_hx_booking_task_should_be_called(self):
#    #    self.mock_booking.configure_mock(**{'is_audit': False, 'coupon_code': None})
#    #    BookingService.confirm_booking(5)
#    #    self.mock_send_booking_confirmation_notification.assert_called_once_with(5)
#    #    self.mock_create_hx_booking.delay.assert_called_once_with(self.mock_booking.id, user_id=mock.ANY, request_id=mock.ANY)


# class BookingServiceBuildSpecialPreferenceTest(SimpleTestCase):
#    def setUp(self):
#        self.booking_request_manager_patcher = mock.patch.object(BookingRequest, 'objects')
#        self.mock_booking_request_manager = self.booking_request_manager_patcher.start()
#        self.mock_booking_request = mock.Mock(spec=BookingRequest, **{
#            'id': 2,
#            'checkin_time': None, 'checkout_time': None,
#            'comments': 'Test Comments', 'coupon_code': None, 'children': 0, 'is_audit': False
#        })
#        self.mock_booking_request_manager.get.return_value = self.mock_booking_request
#
#    def tearDown(self):
#        self.booking_request_manager_patcher.stop()
#
#    def test_special_preference_should_just_have_user_comments(self):
#        special_preference = BookingService.build_special_preference(self.mock_booking_request)
#        self.assertEquals(special_preference, " " + self.mock_booking_request.comments)
#
#    def test_special_preference_should_have_checkin_time(self):
#        self.mock_booking_request.configure_mock(**{
#            'checkin_time': time(12, 0), 'checkout_time': time(20, 0),
#        })
#        special_preference = BookingService.build_special_preference(self.mock_booking_request)
#        self.assertTrue('Checkin Time' in special_preference)
#        self.assertTrue('Checkout Time' in special_preference)
#
#    #def test_special_preference_should_not_have_SUMMER_coupon(self):
#    #    special_preference = BookingService.build_special_preference(self.mock_booking_request)
#    #    self.assertTrue('coupon = SUMMER' not in special_preference)
#
#    def test_special_preference_should_have_SUMMER_coupon(self):
#        self.mock_booking_request.configure_mock(**{'coupon_code': 'Summer'})
#        special_preference = BookingService.build_special_preference(self.mock_booking_request)
#        self.assertTrue('coupon = SUMMER' in special_preference)
#
#    def test_special_preference_should_have_children_count(self):
#        self.mock_booking_request.configure_mock(**{'children': 2})
#        special_preference = BookingService.build_special_preference(self.mock_booking_request)
#        self.assertTrue('Number of Children: 2' in special_preference)
#
#    def test_special_preference_should_have_FOT_booking(self):
#        self.mock_booking_request.configure_mock(**{'is_audit': True})
#        special_preference = BookingService.build_special_preference(self.mock_booking_request)
#        self.assertTrue('FOT Booking' in special_preference)


class BookingServiceSingleFlowMethodsTest(SimpleTestCase):
    def setUp(self):
        self.booking_manager_patcher = mock.patch.object(Booking, 'objects')
        self.mock_booking_manager = self.booking_manager_patcher.start()
        self.mock_booking = mock.Mock(spec=Booking, **{'id': 2})
        self.mock_booking_manager.get.return_value = self.mock_booking

    def tearDown(self):
        self.booking_manager_patcher.stop()

    def yield_room_booking(self, mock_save_roombooking):
        for i in range(20):
            yield mock.Mock(spec=RoomBooking, **{'save': mock_save_roombooking})

    #@mock.patch.object(RoomBooking, 'save')
    #@mock.patch('apps.bookings.v1.services.booking.RoomBooking')
    # def test_save_room_booking(self, mock_room_booking, mock_save_roombooking):
    #    mock_room_booking.side_effect = self.yield_room_booking(mock_save_roombooking)
    #    # The booking XML returned contains 2 <booking> tag. So 2 RoomBooking should be created
    #    save_booking_xml = ResponseXmlFactory.build_save_booking_response_xml()
    #    mock_booking = mock.Mock(spec=Booking, **{'room': mock.Mock()})
    #    BookingService.saveRoomBooking(save_booking_xml, mock_booking)
    #    self.assertEquals(mock_room_booking.call_count, 2)
    #    self.assertEquals(mock_save_roombooking.call_count, 2)

    #@mock.patch.object(BookingService, 'saveRoomBooking')
    #@mock.patch.object(Booking, 'save')
    # def test_update_booking_detail_from_hx(self, mock_save_booking, mock_save_roombooking):
    #    save_booking_xml = ResponseXmlFactory.build_save_booking_response_xml()
    #    BookingService.update_booking_detail_from_hx(2, save_booking_xml)
    #    mock_save_roombooking.assert_called_once_with(save_booking_xml, self.mock_booking)

    def test_finalize_booking_confirmation(self):
        BookingService.finalize_booking_confirmation(20)
        self.assertTrue(self.mock_booking.is_complete)
        self.assertEqual(self.mock_booking.booking_status, Booking.CONFIRM)

    #@mock.patch('apps.bookings.v1.services.booking.BookingNotificationService')
    #@mock.patch('apps.bookings.v1.services.booking.BookingHelper')
    #@mock.patch.object(BookingRequest, 'objects')
    # def test_send_booking_confirmation_notification(self, mock_bookingrequest_manager, mock_booking_helper,
    #                                                mock_booking_notification_service):
    #    mock_booking_request = mock.Mock()
    #    mock_bookingrequest_manager.get.return_value = mock_booking_request
    #    BookingService.send_booking_confirmation_notification(20)
    #    mock_booking_notification_service.sendSpecialPrefEmail.assert_called_once_with(self.mock_booking, mock_booking_request)
    #    mock_booking_notification_service.bookingConfirmationSms.assert_called_once_with(self.mock_booking)
    #    mock_booking_helper.sendEmail.assert_called_once_with(self.mock_booking)

    def test_generate_hx_order_id(self):
        import re
        hx_order_id = BookingService.generate_order_id()
        self.assertTrue(re.match(r'TRB-\d{10}', hx_order_id))
