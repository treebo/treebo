from apps.content.meta_cache_service import MetaCacheService

__author__ = 'ansilkareem'

import logging
from rest_framework.response import Response

from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class MetaContentAPI(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        page_name = request.GET.get('page')
        query = request.GET.get('q')
        meta_content = MetaCacheService(page_name, query).get_meta_data()

        return Response(meta_content)
