from django.core.cache import cache


class AutoCompletionService(object):
    @staticmethod
    def invalidate_hotel_cache():
        cache.delete_pattern('cs_autocomplete_cache_hotel*')
        cache.delete_pattern('cs_autocomplete_cache_googleapi*')

    @staticmethod
    def invalidate_locality_cache():
        cache.delete_pattern('cs_autocomplete_cache_locality*')
        cache.delete_pattern('cs_autocomplete_cache_googleapi*')

    def apply(self, search_string, **kwargs):
        pass
