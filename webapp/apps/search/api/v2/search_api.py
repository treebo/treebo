import logging

from django.core.cache import cache
from rest_framework import status
from rest_framework.response import Response
from django.conf import settings

from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.hotels.serializers.image_serializer import ImageSerializer
from apps.hotels.service.hotel_policy_service import HotelPolicyService
from apps.reviews.cache.cache_service import CacheService
from apps.reviews.cache.page_config import PageConfig
from apps.reviews.models import ReviewAppConfiguration
from apps.reviews.services.trip_advisor.review_config_service import ReviewAppConfigService
from apps.search.constants import DEFAULT_SEARCH_CHANNEL
from apps.search.services.search_v2 import SearchService
from apps.search.services.categories.categories_services import CategoriesServices
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.location import Landmark, City, Locality
from apps.common.exceptions.custom_exception import NoHotelsFoundException
from django.db.models import Q

logger = logging.getLogger(__name__)


class SearchAPI(TreeboAPIView):
    CACHE_TIMEOUT = 24 * 60 * 60
    CACHE_KEY = 'cs_apicache_searchpage_{0}'
    search_landmark_repository = RepositoriesFactory.get_landmark_repository()

    @staticmethod
    def invalidate_cache():
        cache.delete_pattern(SearchAPI.CACHE_KEY.format('*'))

    @staticmethod
    def getLandmarkDetailsFromCityName(cityObject):
        landmarkSet = []
        try:
            landmarks = cityObject.landmark_set.filter(status=Landmark.HIDE)
            if landmarks:
                for landmark in landmarks:
                    landmarkSet.append({
                        'name': landmark.name,
                        'latitude': landmark.latitude,
                        'longitude': landmark.longitude
                    })
            return landmarkSet, cityObject
        except City.DoesNotExist:
            logger.info("No city found")

        return landmarkSet, ""

    def get_search_service_object(self, hotel_id, hotel_name, locality_name, landmark_name, city_name, state_name, country_name,
                                    location_type, nearby, category, amenity, meta_keys, latitude, longitude,
                                    radius, distance_cap):
        return SearchService(hotel_id=hotel_id, hotel_name=hotel_name, locality_name=locality_name, landmark_name=landmark_name,
                             city_name=city_name, state_name=state_name, country_name=country_name,
                             location_type=location_type, nearby=nearby, category=category, amenity=amenity,
                             meta_keys=meta_keys, latitude=latitude, longitude=longitude, radius=radius, distance_cap=distance_cap)

    def get(self, request, *args, **kwargs):
        """
        Search API to get list of available hotels for Search Page.
        Model dependency: State, City, Locality, CityAlias, Hotel, Image, Facility
        hotel_id -- Hotel Id
        hotel_name -- Hotel Name
        locality_name -- Locality Name
        city_name -- City Name
        state_name -- State Name
        country_name -- Country Name
        location_type -- Location Type (Select one out of - hotel/locality/city/state/country)
        nearby -- Search Near By Hotels (true/false)
        """
        try:
            result = cache.get(SearchAPI.CACHE_KEY.format(request.GET.urlencode()))
        except Exception as e:
            logger.exception("Exception in getting cached data from redis in search")
            logger.error(e)
            result = None
        if result:
            result = self.update_review_data(result)
            return Response(result)
        logger.info("We did not get any data from redis in search!")
        request_data = request.GET
        try:
            (hotel_id, hotel_name, locality_name, city_name,
             state_name, country_name, location_type, nearby,
             landmark_name, distance_cap, category, amenity, meta_keys,
             latitude, longitude, radius, channel) = self.parse_request(request_data)
        except Exception as e:
            return Response(data={
                'error': e.__str__(),
                'result': [],
                'filters': {},
                'sort': {},
                'sort_params': "",
            }, status=status.HTTP_400_BAD_REQUEST)

        try:
            search_service = self.get_search_service_object(hotel_id, hotel_name, locality_name,
                                                            landmark_name, city_name, state_name,
                                                            country_name, location_type, nearby, category,
                                                            amenity, meta_keys, latitude, longitude, radius, distance_cap
                                                            )
            hotels = search_service.search()
            # TODO: Move landmark based search to search service
            # if latitude and longitude and city_name is not None:
            #     landmarkSet, cityObject = SearchView.getLandmarkDetailsFromCityName(cityObj)
            #     __myMap = maps.Maps()
            #     hotels = __myMap.getHotelDistanceWithoutGMAP(hotels, cityObject, latitude, longitude,
            #                                                  subLocality)
            hotel_ids = [hotel.id for hotel in hotels]
            category_dict = CategoriesServices().category_dict(hotels=hotels, hotel_ids=hotel_ids)
            result = []
            for hotel in hotels:
                images = hotel.image_set.all()[:10]
                facilities = [facility.id for facility in hotel.facility_set.all()]
                policies = HotelPolicyService.get_hotel_policies(hotel.id)
                image_serializer = ImageSerializer(instance=images, many=True)
                image_set = image_serializer.data
                logger.info(category_dict[hotel.hotelogix_id])
                area = {
                    'pincode': hotel.locality.pincode,
                    'locality': hotel.locality.name,
                    'city': hotel.city.name,
                    'state': hotel.state.name,
                    'country': 'India'
                }
                coordinates = {
                    'lat': hotel.latitude,
                    'lng': hotel.longitude
                }

                item = {
                    'id': hotel.id,
                    'hotelName': hotel.name,
                    'is_active': True if hotel.status else False,
                    'area': area,
                    'amenities': facilities,
                    'images': image_set,
                    'coordinates': coordinates,
                    'hotel_policies': policies,
                    'isFrequentlyBooked': category_dict[hotel.hotelogix_id]['is_frequently_booked'],
                    'isValueForMoney': category_dict[hotel.hotelogix_id]['is_value_for_money'],
                    'isFourPlusRated': category_dict[hotel.hotelogix_id]['is_four_plus_rated'],
                    'property': {
                        'provider': hotel.provider_name,
                        'type': hotel.property_type
                    }
                }
                result.append(item)
            response = {
                'result': result,
                'sort_params': search_service.sort_params,
            }

            response = self.update_review_data(response)
            flag_sort = False  # common flag for all params' occurance indication - except for city
            flag_city = False  # flag specific to the occurance of the city name

            if city_name or state_name:
                response['filters'] = {
                    'distance_cap': None,
                }
                response['sort'] = {
                    'by': 'recommended',
                    'coordinates': {
                        'lat': '',
                        'lng': ''
                    }

                }
                flag_city = True

            if landmark_name:
                landmark_obj = self.search_landmark_repository.get_landmark_for_seo_url_and_city_name(
                    city_name=city_name.lower(), landmark_name=landmark_name, free_text_url_check_for_landmark=True)
                if landmark_obj:
                    if distance_cap:
                        response['filters'] = {
                            "distance_cap": int(
                                landmark_obj.distance_cap) *
                            1000 if landmark_obj.distance_cap else None}
                    else:
                        response['filters'] = {}

                    response['sort'] = {
                        'by': 'distance',
                        'coordinates': {
                            'lat': landmark_obj.latitude,
                            'lng': landmark_obj.longitude
                        }
                    }
                flag_sort = True

            if locality_name and not flag_sort:
                locality_obj = Locality.objects.filter(Q(Q(city__name__iexact=city_name.lower()) | Q(
                    city__slug__iexact=city_name.lower())) & Q(name__iexact=locality_name.lower())).first()
                if locality_obj:
                    if distance_cap:
                        response['filters'] = {
                            "distance_cap": int(
                                locality_obj.distance_cap) *
                            1000 if locality_obj.distance_cap else None}
                    else:
                        response['filters'] = {}

                    response['sort'] = {
                        'by': 'distance',
                        'coordinates': {
                            'lat': locality_obj.latitude,
                            'lng': locality_obj.longitude
                        }
                    }
                flag_sort = True

            # default
            if not flag_sort and not flag_city:
                response['filters'] = {
                    'distance_cap': None,
                }
                response['sort'] = {
                    'by': 'price',
                    'coordinates': {
                        'lat': '',
                        'lng': ''
                    }
                }
                flag_sort = True
            # response['result'] =
            cache.set(
                SearchAPI.CACHE_KEY.format(
                    request.GET.urlencode()),
                response,
                SearchAPI.CACHE_TIMEOUT)

        except NoHotelsFoundException as e:
            slack_alert.send_slack_alert_for_search_seo(request_param=request.GET, api_name=self.__class__.__name__)
            return Response(data={
                'error': e.message,
                'result': [],
                'filters': {},
                'sort': {},
                'sort_params': "",
            }, status=e.status_code)

        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        message=e.__str__(), class_name=self.__class__.__name__)

            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={"errors": e.__str__()})

        return Response(response)

    def parse_request(self, request_param):
        hotel_id = request_param.get('hotel_id')
        hotel_name = request_param.get('hotel')
        locality_name = request_param.get('locality')
        city_name = request_param.get('city')
        state_name = request_param.get('state')
        country_name = request_param.get('country')
        location_type = request_param.get('location_type')
        nearby = request_param.get('nearby')
        landmark_name = request_param.get('landmark')
        distance_cap = request_param.get('distance_cap')
        category = ' '.join(request_param.get('category').split('-')) if request_param.get('category') else None
        amenity = ' '.join(request_param.get('amenity').split('-')) if request_param.get('amenity') else None
        meta_keys = request_param.get('meta_key')
        latitude = request_param.get('latitude')
        longitude = request_param.get('longitude')
        radius = int(request_param.get('radius')) if request_param.get('radius') \
            else int(settings.DEFAULT_SEO_LANDMARK_RADIUS)
        channel = request_param.get('channel', DEFAULT_SEARCH_CHANNEL)

        if landmark_name and not city_name:
            landmark_name = str(landmark_name)
            landmark_objs = self.search_landmark_repository.get_landmarks_by_freetext_url(
                query_string=landmark_name)
            if landmark_objs:
                landmark_obj = landmark_objs[0]
            else:
                landmark_obj = None
            if not landmark_obj:
                items = landmark_name.split()
                city_name = items[-1]
                landmark_name = ' '.join(items[:-1])
            else:
                city_name = landmark_obj.city.name
                landmark_name = landmark_obj.seo_url_name

        if hotel_id:
            hotel_id = int(hotel_id)
        if nearby:
            nearby = nearby.lower()
        nearby = True if nearby == 'true' else False
        if distance_cap:
            distance_cap = distance_cap.lower()
        distance_cap = True if distance_cap == 'true' else False
        return (hotel_id, hotel_name, locality_name, city_name, state_name,
                country_name, location_type, nearby, landmark_name, distance_cap, category, amenity,
                meta_keys, latitude, longitude, radius, channel)

    def get_hotel_showcased_image_url(self, hotels):
        image_data_services = RepositoriesFactory.get_image_repository()
        image_obj_list = image_data_services.get_images_for_hotels(
            hotels=hotels, is_showcased=True)
        hotel_img_dict = {
            image_obj.hotel_id: image_obj.url for image_obj in image_obj_list}
        return hotel_img_dict

    def update_review_data(self, data):
        try:
            review_app_config = ReviewAppConfigService()
            fetch_hotel_reviews = CacheService()
            config = ReviewAppConfiguration.objects.first()
            for item in data['result']:
                if 'ta_review' in item:
                    if not item['ta_review']:
                        if review_app_config.check_toggle_settings(config=config,
                                                                   hotel_code=str(item['id'])):
                            review_response = fetch_hotel_reviews.get_or_update_cache(
                                item['id'], PageConfig.SEARCH_PAGE)
                            item['ta_review'] = review_response['ta_reviews']
                        else:
                            item['ta_review'] = {}
                            item['ta_review']['ta_enabled'] = False
                    else:
                        if not review_app_config.check_toggle_settings(config=config,
                                                                       hotel_code=str(item['id'])):
                            item['ta_review'] = {}
                            item['ta_review']['ta_enabled'] = False
                else:
                    if review_app_config.check_toggle_settings(config=config,
                                                               hotel_code=str(item['id'])):
                        review_response = fetch_hotel_reviews.get_or_update_cache(
                            item['id'], PageConfig.SEARCH_PAGE)
                        item['ta_review'] = review_response['ta_reviews'] if review_response and 'ta_reviews' in review_response else {}
                    else:
                        item['ta_review'] = {}
                        item['ta_review']['ta_enabled'] = False
        except Exception as ex:
            logger.exception(
                'Exception occured in updating review data in search page')
        return data
