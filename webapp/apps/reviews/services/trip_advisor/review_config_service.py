import logging

from apps.reviews.models import ReviewAppConfiguration

logger = logging.getLogger(__name__)


class ReviewAppConfigService(object):
    def check_toggle_settings(self, hotel_code=None, config=None):
        if not config:
            config = self.fetch_app_config()
        if not hotel_code and self.is_app_enabled(config):
            return True

        elif self.is_app_enabled(config) and self.is_enabled_for_hotel(config, hotel_code):
            return True

        else:
            return False

    def is_enabled_for_hotel(self, config, hotel_code=None):
        try:
            if config:
                if str(hotel_code) in config.disable_for_hotel_list.split(','):
                    is_enabled = False
                elif config.enable_for_all_hotels or str(hotel_code) in config.enable_for_hotel_list.split(','):
                    is_enabled = True
                else:
                    is_enabled = False
            else:
                is_enabled = True
            return is_enabled
        except BaseException:
            logger.exception(
                "Review - Config Check failed for hotel %s",
                hotel_code)
            return False

    def is_app_enabled(self, config):
        if config and config.enable_review_app:
            return True
        return False

    def is_audit_enabled(self, config):
        if config and config.is_audit_enable:
            return True
        return False

    def fetch_app_config(self, ):
        config = ReviewAppConfiguration.objects.all().first()
        return config

    def is_disabled_for_category(self, config, category):
        if category in config.disable_for_category_list:
            return True
        return False
