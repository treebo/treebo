
class AutoApplyHotelCouponPairDto(object):

    def __init__(
            self,
            hotel_id,
            coupon_code,
            start_date,
            end_date,
            is_enabled):
        self.hotel_id = hotel_id
        self.coupon_code = coupon_code
        self.start_date = start_date
        self.end_date = end_date
        self.is_enabled = is_enabled
