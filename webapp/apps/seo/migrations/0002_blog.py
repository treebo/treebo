# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0063_city_seo_image'),
        ('seo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('title', models.TextField()),
                ('image_url', models.TextField()),
                ('blog_url', models.TextField()),
                ('priority', models.IntegerField()),
                ('is_enabled', models.BooleanField(default=True)),
                ('city', models.ForeignKey(to='dbcommon.City', on_delete=models.DO_NOTHING)),
            ],
            options={
                'verbose_name': 'Blog',
                'verbose_name_plural': 'Blogs',
            },
        ),
    ]
