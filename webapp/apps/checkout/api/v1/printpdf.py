import logging

import pdfkit
from django.http import HttpResponse, HttpResponseRedirect
from rest_framework.response import Response

from apps.pages.print_pdf_helper import PdfDataHelper
import subprocess
from tempfile import NamedTemporaryFile
from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from apps.common.utils import Utils
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes


__author__ = 'amithsoman'

logger = logging.getLogger(__name__)


# TODO: Check if this is used. If used, then fix the session part
class PrintPdf(TreeboAPIView):
    """
        PDF Generation API
    """

    def dispatch(self, *args, **kwargs):
        return super(PrintPdf, self).dispatch(*args, **kwargs)

    def post(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def get(self, request):
        """
        :param request:
        :return:
        """
        name = request.GET.get('name', 'Booking')
        # TODO Change the Urls to Mobile when code bases sync

        pdf_base_url = "{0}{1}?order_id={2}&pretax_price={3}&total_tax={4}"

        if request.user and ('order_id' in request.GET):
            order_id = str(request.GET.get('order_id'))
        else:
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.NO_ORDER_ID_FOUND))

        try:
            html = PdfDataHelper().getData(order_id, request.user)
            html = html.encode('utf-8')
            html_file = NamedTemporaryFile(delete=False, suffix='.html')
            html_file.write(html)
            pdf_file = NamedTemporaryFile(delete=False, suffix='.pdf')
            subprocess.run(['wkhtmltopdf', '-q', html_file.name, pdf_file.name])
            pdf_name = BookingHelper.generatePdfName(name, order_id)
            response = HttpResponse(pdf_file, content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename=' + pdf_name
            html_file.close()
            return response

        except Exception as e:
            logger.exception(
                "Booking Does Not exist due to Exception : %s for order id : %s" %
                (e, order_id))
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.INVALID_ORDER_ID))


class DisplayPDF(TreeboAPIView):
    def dispatch(self, *args, **kwargs):
        return super(DisplayPDF, self).dispatch(*args, **kwargs)

    def post(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def get(self, request):
        """
        :param request:
        :return:
        """

        if request.user and ('order_id' in request.GET):
            order_id = str(request.GET.get('order_id'))
        else:
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.NO_ORDER_ID_FOUND))

        try:
            return HttpResponse(PdfDataHelper().getData(order_id, request.user))

        except BookingRequest.DoesNotExist:
            logger.exception("Booking Does not exist")
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.INVALID_ORDER_ID))
