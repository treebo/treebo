# -*- coding:utf-8 -*-
"""
    ConfirmBooking API
"""

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR
from django.conf import settings

from apps.bookings.models import Booking
from apps.bookings.serializers import BookingSerializer
from apps.bookings.services.booking import BookingService
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from base import log_args
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger
from apps.common.slack_alert import SlackAlertService as slack_alert
from common.services.feature_toggle_api import FeatureToggleAPI
from data_services.respositories_factory import RepositoriesFactory

__author__ = 'rohitjain'

logger = get_booking_logger(__name__)


class ConfirmBooking(TreeboAPIView):
    __booking_service = BookingService()
    authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ConfirmBooking, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        data = request.data
        order_id = data.get("order_id", None)
        if not order_id:
            return JsonResponse({"error": error_message(
                codes.NO_ORDER_ID_FOUND)}, status=HTTP_400_BAD_REQUEST)
        try:
            try:
                booking = Booking.objects.get(order_id=order_id)
            except Booking.DoesNotExist as e:
                logger.error("bookng not found for order %s ", )
                return JsonResponse({"error": error_message(
                    codes.BOOKING_NOT_FOUND)}, status=HTTP_400_BAD_REQUEST)
            # Check if existing booking exists with given booking request id
            if booking.booking_status == Booking.CONFIRM:
                return JsonResponse(
                    BookingSerializer(booking).data,
                    status=status.HTTP_201_CREATED)

            self.__booking_service.confirm_booking(booking)
            return JsonResponse(
                BookingSerializer(booking).data,
                status=status.HTTP_201_CREATED)
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.POST,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception(
                "booking confirm failed with internal error for order_id %s ",
                order_id)
            return JsonResponse(
                {
                    "error": error_message(
                        codes.INVALID_BOOKING), "developer_message": str(
                        e)}, status=HTTP_500_INTERNAL_SERVER_ERROR)
