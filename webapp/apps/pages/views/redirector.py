import logging

from django.http.response import HttpResponseRedirect

from base.views.template import TreeboTemplateView

__author__ = 'baljeet'

logger = logging.getLogger(__name__)


class Redirector(TreeboTemplateView):
    def postData(self, request, args, kwargs):
        redirectUrl = request.POST['redirectUrl']
        return HttpResponseRedirect(redirectUrl)

    def postMobileData(self, request, args, kwargs):
        redirectUrl = request.POST['redirectUrl']
        return HttpResponseRedirect(redirectUrl)

        # return {'scripts': ['desktop/js/transaction.js'],
        # 'styles':['desktop/css/transaction.css']}
