from apps.bookings.services.booking_utils import convert_utc_to_localtime
from apps.discounts.integrations.koopan import Koopan
from apps.hotels.service.hotel_service import HotelService


class PromotionService:

    @staticmethod
    def get_promotions(booking_request):

        applicable_promotions = []
        if booking_request.booking_channel not in ('msite', 'android', 'ios', 'website'):
            return applicable_promotions

        abw = (booking_request.checkin_date - (
            convert_utc_to_localtime(booking_request.created_at)).date()).days
        hotel_id = booking_request.hotel_id
        cs_id = HotelService().get_hotel_by_hotel_id(hotel_id).cs_id
        channel = 'direct'
        application = booking_request.booking_application
        sub_channel = booking_request.booking_subchannel
        koopan = Koopan(abw=abw, channel=channel, sub_channel=sub_channel, application=application,
                        cs_id=cs_id)

        applicable_promotions = koopan.get_applicable_promotions()

        return applicable_promotions

    def is_discount_prepaid_applicable(self, booking_request):

        promotions = self.get_promotions(booking_request=booking_request)

        for promotion in promotions:
            if promotion['is_prepaid'] and promotion['incentive_value'] > 0:
                return True

        return False

    def get_discount_prepaid_promotion(self, booking_request):

        prepaid_promotions = {}
        prepaid_promotion = None
        prepaid_promotions_incentive_dict = {}
        promotions = self.get_promotions(booking_request=booking_request)

        for promotion in promotions:
            if promotion['is_prepaid'] and promotion['incentive_value'] > 0:
                prepaid_promotions[promotion['name']] = promotion
                prepaid_promotions_incentive_dict[promotion['name']] = promotion['incentive_value']

        if prepaid_promotions:
            promotion_name = sorted(prepaid_promotions_incentive_dict.keys())[-1:][0]
            prepaid_promotion = prepaid_promotions[promotion_name]

        return prepaid_promotion
