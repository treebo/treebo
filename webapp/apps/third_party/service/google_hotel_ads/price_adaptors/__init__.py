import abc
import logging

logger = logging.getLogger(__name__)


class GHAPriceAdaptor(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def fetch_price(self, hotel_ids_list, checkin, checkout, room_config):
        return

    @abc.abstractmethod
    def read_pretax_price(self, hotel_price, room_type, default=0):
        return

    @abc.abstractmethod
    def read_tax(self, hotel_price, room_type, default=0):
        return
