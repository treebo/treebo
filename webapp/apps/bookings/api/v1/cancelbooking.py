# -*- coding:utf-8 -*-
"""
    CancelBookingAPI
"""

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from rest_framework.response import Response

from apps.bookings.services.booking import BookingService
from apps.common.utils import Utils
from apps.fot.emailers import audit_cancellation
from apps.fot.models import FotReservationsRequest, Fot
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import common_constants as constants, error_codes
from common.custom_logger.booking.log import get_booking_logger
from apps.common.slack_alert import SlackAlertService as slack_alert

__author__ = 'rohitjain'

logger = get_booking_logger(__name__)


class CancelBookingValidator(serializers.Serializer):
    order_id = serializers.CharField(required=True, error_messages={
        'required': error_codes.PROMOTIONS_NOT_PRESENT,
        'blank': error_codes.PROMOTIONS_NOT_PRESENT
    })
    email = serializers.EmailField(required=True, error_messages={
        'required': error_codes.INVALID_EMAIL_ORDER,
        'blank': error_codes.INVALID_EMAIL_ORDER
    })


class CancelBookingAPI(TreeboAPIView):
    validationSerializer = CancelBookingValidator
    authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CancelBookingAPI, self).dispatch(*args, **kwargs)

    def get(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request):
        """

        :param request:
        :return:
        """
        data = request.data
        order_id = data.get('order_id')
        try:
            # Removing this condition because logged-in user should be able to cancel booking for different another guest email booking id
            # if booking.user_id.email == request.POST.get('email'):
            booking = BookingService.cancel_booking(order_id)

            if not booking:
                return Response(
                    Utils.buildErrorResponseContext(
                        error_codes.INVALID_BOOKING))

            # TODO: MOVE THIS OUT OF BOOKING ENGINE
            if booking.is_audit:
                Fotreservation = FotReservationsRequest.objects.get(
                    booking=booking)
                Fotreservation.status = 'CANCELLED'
                Fotreservation.save()

                audit_cancellation(booking.id)
                try:
                    fotUser = Fot.objects.get(user_id=Fotreservation.user_id)
                    fotUser.free_audit_slot()
                except Fot.DoesNotExist:
                    logger.exception("No fot user found")
            context = {
                'status': constants.SUCCESS,
                'message': 'Your cancellation request has been Accepted.',
            }
            return Response(context)
            # return
            # Response(Utils.buildErrorResponseContext(error_codes.INVALID_BOOKING))
        except Exception as exc:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.POST,
                                                        message=exc.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.info('in cancel booking API')
            logger.exception(exc)
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.INVALID_BOOKING))
