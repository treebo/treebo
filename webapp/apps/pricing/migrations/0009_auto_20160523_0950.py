# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('pricing', '0008_auto_20160518_1305'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='channelmanagerrateplan',
            options={
                'verbose_name': 'Channel Manager Rate Plan',
                'verbose_name_plural': 'Channel Manager Rate Plans'},
        ),
        migrations.AlterModelOptions(
            name='luxurytax',
            options={
                'verbose_name': 'Luxury Tax',
                'verbose_name_plural': 'Luxury Taxes'},
        ),
        migrations.AlterModelOptions(
            name='preferredpromo',
            options={
                'verbose_name': 'Preferred Promo',
                'verbose_name_plural': 'Preferred Promos'},
        ),
        migrations.AlterModelOptions(
            name='promoconditions',
            options={
                'verbose_name': 'Promo Condition',
                'verbose_name_plural': 'Promo Conditions'},
        ),
        migrations.AlterModelOptions(
            name='roomprice',
            options={
                'verbose_name': 'Room Price',
                'verbose_name_plural': 'Room Prices'},
        ),
        migrations.AlterModelOptions(
            name='roomrateplan',
            options={
                'verbose_name': 'Room Rate Plan',
                'verbose_name_plural': 'Room Rate Plans'},
        ),
        migrations.AlterModelOptions(
            name='statetax',
            options={
                'verbose_name': 'State Tax',
                'verbose_name_plural': 'State Taxes'},
        ),
    ]
