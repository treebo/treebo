import logging

from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from apps.bookingstash.utils import StashUtils
from apps.bookingstash.service.availability_service import get_availability_service
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.common.utils import round_to_two_decimal, round_to_nearest_integer
from apps.pricing.dateutils import date_to_ymd_str
from apps.pricing.exceptions import RequestError
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.services.pricing_service import PricingService, SoaError
from base.views import validators
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from apps.hotels.service.price_service import PriceService

logger = logging.getLogger(__name__)

hotel_repository = RepositoriesFactory.get_hotel_repository()


class HotelRoomPricesSerializer(serializers.Serializer):
    checkin = serializers.DateField()
    checkout = serializers.DateField()
    roomconfig = serializers.CharField(
        max_length=100, validators=[
            validators.validate_roomconfig])


class HotelRoomPrices(TreeboAPIView):
    validationSerializer = HotelRoomPricesSerializer
    price_service = PriceService()

    def get(self, request, *args, **kwargs):
        """
        API to fetch room prices for all rooms for the given hotel.
        checkin -- Checkin Date (Use format: yyyy-MM-dd)
        checkout -- Checkout Date (Use format: yyyy-MM-dd)
        roomconfig -- Room Configuration in format: 1-0,2-0,2-1 (For a-c, 'a' is adult count, and 'c' is children count)
        """
        try:
            validated_data = self.serializerObject.validated_data
            price_info = self.process_validated_data(validated_data, kwargs)
            return Response(price_info)
        except RequestError as e:
            return Response({"error": error_message(
                codes.INVALID_REQUEST)}, status=HTTP_400_BAD_REQUEST)
        except SoaError as e:
            logger.exception(
                " SoaError Exception occurred while applying coupon code using SOA API")
            return Response({"error": error_message(
                codes.INVALID_REQUEST)}, status=e.status)
        except Exception as e:
            logger.exception(
                "unknown Exception occurred while applying coupon code using SOA API")
            return Response({"error": error_message(
                codes.REQUEST_FAILED)}, status=HTTP_500_INTERNAL_SERVER_ERROR)

    def process_validated_data(self, validated_data, kwargs):
        check_in_date, check_out_date, room_config = validated_data[
            'checkin'], validated_data['checkout'], validated_data['roomconfig']
        room_config_list = StashUtils.room_config_string_to_list(room_config)

        hotel_id = int(kwargs["hotel_id"])

        hotels = hotel_repository.filter_hotels(id=hotel_id)
        hotel = hotels[0] if hotels else None
        if not hotel:
            raise RequestError(
                status=HTTP_400_BAD_REQUEST,
                message=codes.INVALID_HOTEL_ID)

        if FeatureToggleAPI.is_enabled("pricing", "soa", False):
            checkin_date_str = date_to_ymd_str(check_in_date)
            checkout_date_str = date_to_ymd_str(check_out_date)
            total_prices = self.get_from_soa(
                hotel, checkin_date_str, checkout_date_str, room_config)
        else:
            total_prices = self.price_service.get_from_local(
                hotel, check_in_date, check_out_date, room_config_list)

        available_rooms = get_availability_service().get_available_rooms(
            int(hotel_id), check_in_date, check_out_date, room_config)

        for room in list(available_rooms.keys()):
            if total_prices[room.room_type_code.lower()]['sell']:
                total_prices[room.room_type_code.lower()
                             ]['availability'] = True
                total_prices[room.room_type_code.lower(
                )]['available'] = available_rooms[room]

        price_info = {
            "total_price": total_prices
        }
        return price_info

    def get_from_soa(
            self,
            hotel,
            checkin,
            checkout,
            room_config,
            coupon_code=None,
            coupon_value=None,
            coupon_type=None):
        prices = PricingService.get_price_from_soa(
            checkin=checkin,
            checkout=checkout,
            hotel_ids=[
                hotel.id],
            room_config=room_config,
            coupon_code=coupon_code,
            coupon_value=coupon_value,
            coupon_type=coupon_type,
            include_price_breakup=False,
            get_from_cache=True)
        total_prices = {}
        if prices["data"]["hotels"]:
            for room in prices["data"]["hotels"][0]["rooms"]:
                room_type_code = room["room_type"].lower()
                if room["price"]["rack_rate"]:
                    discount_percent = round_to_nearest_integer(
                        room["price"]["promo_discount"] * 100 / room["price"]["base_price"])
                else:
                    discount_percent = 0
                if not FeatureToggleAPI.is_enabled("pricing", "pretax", False):
                    total_prices[room_type_code] = {
                        "sell": room["price"]["sell_price"],
                        "post_tax": room["price"]["sell_price"],
                        "rack": room["price"]["rack_rate"],
                        "pretax": (
                            room["price"]).get(
                            'autopromo_price_without_tax',
                            0),
                        "tax": (
                            room["price"]).get(
                            'tax',
                            0),
                        "discount_percent": discount_percent,
                        "availability": False,
                        "available": 0}
                else:
                    total_prices[room_type_code] = {
                        "sell": room["price"]["pretax_price"],
                        "post_tax": room["price"]["sell_price"],
                        "rack": room["price"]["base_price"],  # rack_rate
                        "pretax": (room["price"]).get('autopromo_price_without_tax', 0),
                        "tax": (room["price"]).get('tax', 0),
                        "discount_percent": discount_percent,
                        "availability": False,
                        "available": 0
                    }
        else:
            rooms = hotel_repository.get_rooms_for_hotel(hotel)
            for room in rooms:
                total_prices[room.room_type_code.lower()] = {
                    "sell": 0,
                    "rack": 0,
                    "pretax": 0,
                    "tax": 0,
                    "discount_percent": 0,
                    "availability": False,
                    "available": 0
                }
        return total_prices
