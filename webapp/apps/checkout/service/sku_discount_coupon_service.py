import logging

import requests
from django.conf import settings

logger = logging.getLogger(__name__)


class SkuDiscountCouponService:
    
    @classmethod
    def update_discount_coupon_in_pricing(self, booking_request):
        coupon_id = booking_request.discount_id
        response = self.update_coupon_usage(coupon_id)
        return response
    
    @classmethod
    def update_coupon_usage(self, coupon_id):
        headers = {'Content-type': 'Application/json'}
        url = self.get_pricing_url(coupon_id)
        data = {
            "discount_id": str(coupon_id)
        }
        response = requests.post(url=url, headers=headers, data=data)
        if response.status_code != 200:
            logger.exception(
                'Coupon Count Update Failed for coupon_id {}, data {}, response {}'.format(coupon_id, str(data),
                                                                                           str(response.json())))
            raise Exception('Coupon Count Update Failed')
        return response.json()
    
    @classmethod
    def get_pricing_url(self, coupon_id):
        # todo:Remove pricing staging url
        PRICING_URL = (settings.COUPON_PRICING_URL).format(
            coupon_id)
        return PRICING_URL
