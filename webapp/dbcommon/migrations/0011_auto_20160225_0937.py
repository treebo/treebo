# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0008_auto_20160215_0853'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='max_adult_with_children',
            field=models.PositiveIntegerField(default=1),
        ),
        migrations.AddField(
            model_name='room',
            name='max_children',
            field=models.PositiveIntegerField(default=1),
        ),
    ]
