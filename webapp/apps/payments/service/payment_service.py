from decimal import Decimal
from django.db.models import Q

from apps.bookings import constants
from apps.bookings.models import Booking, PaymentOrder
from apps.common.exceptions.booking_exceptions import BookingNotFoundException, BookingCodeNotFoundException
from apps.common.exceptions.custom_exception import HotelNotFoundException
from apps.common.slack_alert import SlackAlertService
from apps.payments.service.razor_pay import RazorPayService
from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository

import logging

from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class PaymentService():
    razorpay_service = RazorPayService()

    def validate_razorpay_payment(
            self,
            stored_total_amount,
            razorpay_payment_id):
        try:
            razparpay_captured_amount = self.razorpay_service.get_razorpay_payment_details(
                razorpay_payment_id)
            logger.info(
                " razparpay_captured_amount %s  , stored_total_amount %s  ",
                razparpay_captured_amount,
                stored_total_amount)
            if abs(
                (razparpay_captured_amount) /
                100 -
                    stored_total_amount) > 5:
                logger.error("Issue in payment: %s", razorpay_payment_id)
                SlackAlertService.publish_alert(
                    "Issue in payment: %s (stored_total_amount: %s,razparpay_captured_amount: %s)" %
                    (razorpay_payment_id, stored_total_amount, razparpay_captured_amount / 100))
                return False
        except Exception as e:
            logger.exception("validate_razorpay_payment  ")
            return False
        return True

    def get_paymentorder_details(self, hotel_code, group_code, order_id):
        logger.info("Fetching payment details and breakup for hote code:{hotel_code}, "
                    "group_code:{group_code}, "
                    "order_id:{order_id}".format(hotel_code=hotel_code, group_code=group_code, order_id=order_id))
        booking = None
        if order_id:
            booking = Booking.objects.filter(order_id=order_id).first()
        if not booking:
            if not hotel_code:
                raise HotelNotFoundException(
                    'hotel code %s not present ' % (hotel_code))
            if not group_code:
                raise BookingCodeNotFoundException()
            booking = Booking.objects.filter(
                (Q(
                    group_code=group_code) | Q(
                    booking_code__icontains=group_code)) & Q(
                    hotel__hotelogix_id__contains=hotel_code)).first()
        if not booking:
            raise BookingNotFoundException(
                'booking %s not present in db ' %
                (group_code))
        number_of_bookings = len(str(booking.room_config).strip(',').split(','))

        if booking.booking_code:
            booking_codes = str(booking.booking_code).split('|')
        else:
            booking_codes = [str(i+1) for i in range(number_of_bookings)]

        payment_detail = {}
        payment_detail_breakup = {}
        payment_amount_per_booking = Decimal(booking.payment_amount) / Decimal(number_of_bookings)
        paymentorders = PaymentOrder.objects.filter(booking_order_id=booking.order_id)
        for booking_code in booking_codes:
            if booking_code:
                payment_detail[booking_code] = payment_amount_per_booking
                payment_detail_breakup[booking_code] = {
                    "paid_amount": payment_amount_per_booking}
                payment_detail_breakup[booking_code]["paid_amount_breakup"] = self._create_payment_breakup(
                    paymentorders, number_of_bookings)
        logger.info(
            " payment details info found booking %s ,having room config %s and number of rooms %d , "
            "payment %s and payment breakup %s", booking, booking.room_config, number_of_bookings,
            booking.payment_amount, str(payment_detail_breakup))
        return paymentorders, payment_detail, payment_detail_breakup, booking

    def _create_payment_breakup(self, payment_orders, number_of_bookings):
        payment_breakup = []
        if not payment_orders:
            return payment_breakup
        for payment_order in payment_orders:
            if payment_order.is_verified():
                payment_breakup.append({"gateway": payment_order.gateway_type,
                                        "amount": Decimal(payment_order.amount)/Decimal(number_of_bookings)
                                        })
        return payment_breakup

    @staticmethod
    def get_payments_for_booking(order_id, return_only_success=False):
        """
        Get the payments associated with a given booking
        :param order_id: booking order id (TRB id).
        :param return_only_success: if set to true then return only the payments which are in success status.
        :return: a list of payment order objects
        """

        assert isinstance(order_id, str)

        try:
            payments = PaymentOrder.objects.select_for_update().filter(booking_order_id=order_id)
        except Exception as e:
            logger.exception("get_payments_for_booking-exception")
            raise e

        if not return_only_success:
            return payments

        filtered_payments = []
        for payment in payments:
            if payment.status == PaymentOrder.CAPTURED:
                filtered_payments.append(payment)

        return filtered_payments

    @staticmethod
    def get_non_wallet_payments_for_booking(order_id, return_only_success=False):
        """
        Get the payments associated with a given booking
        :param order_id: booking order id (TRB id).
        :param return_only_success: if set to true then return only the payments which are in success status.
        :return: a list of payment order objects
        """

        try:
            payments = PaymentOrder.objects.select_for_update().filter(
                booking_order_id=order_id).exclude(gateway_type=constants.WALLET_GATEWAY_TYPE)
        except Exception as e:
            logger.exception("exception in method get_non_wallet_payments_for_booking")
            raise e

        if not return_only_success:
            return payments

        filtered_payments = []
        for payment in payments:
            if payment.status == PaymentOrder.CAPTURED:
                filtered_payments.append(payment)

        return filtered_payments

    @staticmethod
    def get_wallet_payments_for_booking(order_id, return_only_success=False):
        """
        Get the payments associated with a given booking
        :param order_id: booking order id (TRB id).
        :param return_only_success: if set to true then return only the payments which are in success status.
        :return: a list of payment order objects
        """

        try:
            payments = PaymentOrder.objects.select_for_update().filter(
                booking_order_id=order_id, gateway_type=constants.WALLET_GATEWAY_TYPE).first()
        except Exception as e:
            logger.exception("exception in method get_non_wallet_payments_for_booking")
            raise e

        if not return_only_success:
            return payments

        filtered_payments = []
        for payment in payments:
            if payment.status == PaymentOrder.CAPTURED:
                filtered_payments.append(payment)

        return filtered_payments

    @staticmethod
    def get_paid_amound_for_booking(order_id):
        """
        returns the current paid amount for a given booking
        :param order_id: booking order id (TRB id).
        :return: amount in float for payments in completed state
        """

        successful_payments = PaymentService.get_payments_for_booking(
            order_id, True)
        total_paid_amount = 0

        for payment in successful_payments:
            total_paid_amount += payment.amount

        return round(total_paid_amount, 2)
