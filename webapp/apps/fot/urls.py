from django.conf.urls import url

from apps.fot.initiate_audit import InitiateAudit
from apps.fot.views import (
    CreditsRedemption,
    UserConfirmation,
    FOTCreditRedemption,
    AuditsDashboard,
    FotUsersDashboard,
    CreditsDashboard,
    NotificationsDashboard,
    SlotsPerHotelDashboard,
    SlotsPerUserDashboard,
    HotelBlockOutDatesDashboard,
    FotConfigDashboard,
    UpdateFotHotel,
    DashboardCsvExporter)

# Dashboard related api
app_name = "fot"
urlpatterns = [

    url(r"^api/v1/discount/$", FOTCreditRedemption.as_view(), name='FotDiscount'),
    url(r'^api/v1/redeemcredits/$', CreditsRedemption.as_view(), name="CreditsRedemption"),
    url(r'^api/v1/userconfirmation/', UserConfirmation.as_view(), name="UserConfirmation"),
    url(r"^api/audits/$", AuditsDashboard.as_view(), name='AuditsDashboard'),
    url(r"^api/users/$", FotUsersDashboard.as_view(), name='FotUsersDashboard'),
    url(r"^api/credits/$", CreditsDashboard.as_view(), name='CreditsDashboard'),
    url(r"^api/notifications/$", NotificationsDashboard.as_view(), name='NotificationsDashboard'),
    url(r"^api/slotsperhotel/$", SlotsPerHotelDashboard.as_view(), name='SlotsPerHotelDashboard'),
    url(r"^api/slotsperuser/$", SlotsPerUserDashboard.as_view(), name='SlotsPerUserDashboard'),
    url(r"^api/hotelblockout/$", HotelBlockOutDatesDashboard.as_view(), name='HotelBlockOutDatesDashboard'),
    url(r"^api/otherconfig/$", FotConfigDashboard.as_view(), name='FotConfigDashboard'),
    url(r"^api/updatefothotel/$", UpdateFotHotel.as_view(), name='UpdateFotHotel'),
    url(r"^api/csvdownloader/$", DashboardCsvExporter.as_view(), name='DashboardCsvExporter'),
    url(r"^api/initiate_audit/$", InitiateAudit.as_view(), name='initiate-audit'),

]
