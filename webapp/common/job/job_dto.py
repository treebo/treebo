class ScheduledJobDTO:
    def __init__(self, job_name, data, crs_booking_id=None, eta=None):
        self.job_name = job_name
        self.data = data
        self.crs_booking_id = crs_booking_id
        self.eta = eta
