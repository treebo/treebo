# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0058_categoryseodetails_localityseodetails'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoryseodetails',
            name='city',
            field=models.ForeignKey(
                default=1,
                to='dbcommon.City',
                on_delete=django.db.models.deletion.CASCADE),
            preserve_default=False,
        ),
    ]
