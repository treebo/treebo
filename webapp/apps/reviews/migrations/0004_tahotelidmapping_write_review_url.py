# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0003_auto_20170426_1948'),
    ]

    operations = [
        migrations.AddField(
            model_name='tahotelidmapping',
            name='write_review_url',
            field=models.TextField(null=True, blank=True),
        ),
    ]
