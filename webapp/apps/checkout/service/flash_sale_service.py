from apps.discounts.models import FlashDiscount
from apps.hotels.service.hotel_priority_service import HotelPriorityService
from apps.hotels.service.hotel_provider_service import HotelProviderService


class FlashSaleService:

    @staticmethod
    def get_price_override(pdto, hotel_id, rate_plan, flash_sale_details):

        prf_details = flash_sale_details.get('prf_sale_details', None)
        if prf_details and (str(hotel_id) in prf_details.get('priority_hotels_list', None)):
            flash_sale_details = prf_details
        else:
            flash_sale_details = flash_sale_details.get('non_prf_sale_details', None)

        if hotel_id in flash_sale_details.get('flash_sale_enabled_hotel_ids', None):
            pretax_post_coupon_price = rate_plan.pretax_price
            inflated_pretax_price = 100 * pretax_post_coupon_price / (100 - flash_sale_details.get('flash_sale_discount', 0))
            coupon_discount = inflated_pretax_price - pretax_post_coupon_price
            # rate_plan.pretax_price = inflated_pretax_price - coupon_discount
            rate_plan.coupon_discount = coupon_discount

        return rate_plan

    @staticmethod
    def get_flash_sale_details(booking_date):
        flash_discount_details = FlashDiscount.objects.filter(start_date__lte=booking_date,
                                                              end_date__gte=booking_date)
        for flash_discount_object in flash_discount_details:
            if not(flash_discount_object and 0 < flash_discount_object.discount_percentage < 100):
                return False, None
        return True, flash_discount_details

    @staticmethod
    def get_combined_flash_details(flash_sale_discount_details, hotel_ids, is_flash_sale_applicable=True):
        flash_sale_details_combined = {
            "prf_sale_details": {},
            "non_prf_sale_details": {},
        }
        prf_hotels_list = HotelPriorityService. \
            get_priority_hotels(hotel_ids=hotel_ids)
        for flash_details in flash_sale_discount_details:
            if not flash_details.priority_level:
                flash_sale_details = flash_sale_details_combined.get('non_prf_sale_details', None)
            else:
                flash_sale_details = flash_sale_details_combined.get('prf_sale_details', None)
                flash_sale_details['priority_hotels_list'] = prf_hotels_list

            flash_enabled_property_providers = flash_details.enabled_property_providers
            flash_sale_enabled_hotel_ids = HotelProviderService.get_provider_hotels(
                hotel_ids,flash_enabled_property_providers)

            flash_sale_details['is_flash_sale_applicable'] = is_flash_sale_applicable
            flash_sale_details['flash_sale_discount'] = flash_details.discount_percentage
            flash_sale_details['flash_sale_enabled_hotel_ids'] = flash_sale_enabled_hotel_ids

        return flash_sale_details_combined

    @staticmethod
    def get_flash_discount_percentage(booking_date, hotel_id):

        is_flash_active, flash_details = FlashSaleService.get_flash_sale_details(booking_date)

        if not is_flash_active:
            return False, None

        for flash_detail in flash_details:
            if flash_detail.priority_level and HotelPriorityService.check_priority(hotel_id):
                return is_flash_active, flash_detail.discount_percentage

            if not flash_detail.priority_level:
                return is_flash_active, flash_detail.discount_percentage
