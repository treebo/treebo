from django.db import models
from djutil.models import TimeStampedModel
from dbcommon.models.default_permissions import DefaultPermissions
from apps.promotions.hotel_redirection_type import HotelRedirectionType


class PromotedHotels(TimeStampedModel, DefaultPermissions):
    hotel_id = models.CharField(null=False, blank=False, db_index=True, unique=True, max_length=50)
    priority = models.IntegerField(default=0)


class MetaRedirectionHotels(TimeStampedModel, DefaultPermissions):
    hotel_id = models.CharField(null=False, blank=False, db_index=True, unique=True, max_length=50)
    redirection_type = models.CharField(default=HotelRedirectionType.SEARCH.value, max_length=50)

