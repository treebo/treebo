
__author__ = 'ansilkareem'


class BaseComponent(object):

    def __init__(self, current_city, footer_data, exclude_list):
        self.current_city = current_city
        self.footer_data = footer_data
        self.distance_cap = int(self.footer_data['distance_cap'])
        self.count_cap = int(self.footer_data['no_of_hotels_cap'])
        self.exclude_list = exclude_list

    def build(self):
        raise NotImplementedError
