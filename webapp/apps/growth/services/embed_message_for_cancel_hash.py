import logging

from apps.content.models import ContentStore
from apps.growth import constants

logger = logging.getLogger(__name__)


class EmbedMessage(object):
    def embed_message_in_booking_data(self, api_response):
        try:
            if api_response['data']['booking_status']:
                booking_status = str(
                    api_response['data']['booking_status']['status'])
                content_store_obj = ContentStore.objects.filter(
                    name=constants.STATUS_MAPPING_TO_MESSAGE, version=1).first()
                if content_store_obj:
                    content_store_val = content_store_obj.value
                    if content_store_val.get(booking_status):
                        message = content_store_val[booking_status]
                        api_response['data']['booking_status']['message'] = str(
                            message)
                    else:
                        api_response['data']['booking_status']['message'] = None
                else:
                    api_response['data']['booking_status']['message'] = None
            return api_response
        except Exception as e:
            logger.exception(
                "Exception in updating the message as per status in data")
            raise e
