from django.test import TestCase, Client


class TestDizcounts(TestCase):
    def test_dizcount(self):
        c = Client()
        response = c.post('/rest/v1/discount/',
                          {'coupon_code': 'TREEBO20',
                           'hotel_id': '7',
                           'checkout': 'aOogwoqe',
                           'checkout': '2015-10-16',
                           'checkin': '2015-10-15',
                           'total_cost': '2000'})
