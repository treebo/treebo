import datetime
import json
import logging
from decimal import Decimal

from django.db.models import Q, Sum

from apps.discounts.models import DiscountCoupon
from apps.pricing.models import RoomPrice, StateTax, ChannelManagerRatePlan, PreferredPromo, \
    LuxuryTax
from apps.common import date_time_utils

# Get an instance of a logger
from collections import defaultdict

from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)


class PriceUtils:
    @staticmethod
    def getRoomPriceForDateRange(
            startDate,
            endDate,
            room,
            occupancy,
            couponPercent=None,
            coupon=None):
        logger.info(
            'Called room price date range  startDate: %s, endDate: %s',
            startDate,
            endDate)
        if isinstance(startDate, str):
            startDate = datetime.datetime.strptime(
                startDate, '%Y-%m-%d').date()
        if isinstance(endDate, str):
            endDate = datetime.datetime.strptime(endDate, '%Y-%m-%d').date()
        dateWisePrices = {}
        finalDateWisePrices = {}
        delta = datetime.timedelta(days=1)

        hotel = room.hotel
        date_wise_state_tax = {}
        start = startDate
        end = endDate
        while start < end:
            logger.info('Start Date in loopp: %s', start)
            taxObj = StateTax.objects.filter(Q(state=hotel.state) & Q(effective_date__lte=start) & (
                Q(expiry_date__gte=start) | Q(expiry_date__isnull=True))).aggregate(
                Sum('tax_value'))
            date_wise_state_tax[start] = taxObj['tax_value__sum']
            start = start + datetime.timedelta(days=1)

        logger.debug('Date Wise State Tax: %s', date_wise_state_tax)

        total_prices = {
            'sell': 0,
            'tax': 0,
            'rack_rate': 0,
            'pretax': 0,
            'pretax_rackrate': 0,
            'discount': 0}
        currentDate = startDate
        promo_applied = False
        while currentDate < endDate:
            # TODO: Combine these date wise calculation in sinelg method call.
            logger.info(
                'Calling Room Price for Date for date: %s, end_date: %s',
                currentDate,
                endDate)
            prices, preferredPromo = PriceUtils.__getRoomPriceForDate(
                currentDate, room, occupancy, date_wise_state_tax, couponPercent, coupon)
            # FIXED: Avoid overwriting the preferredPromo, once it is set to True
            # This was resulting in get_price API returning mmpromo as False, for a date range spanning more than 1 day, and the later day having no auto_promo
            # As a result, modifyamount API was not fired on HX, to apply
            # auto_promo for previous day
            if not promo_applied and preferredPromo:
                promo_applied = True
            dateWisePrices[currentDate] = prices
            total_prices['pretax'] += prices['pretax']
            total_prices['tax'] += prices['tax']
            total_prices['sell'] += prices['sell']
            total_prices['rack_rate'] += prices['rack_rate']
            total_prices['pretax_rackrate'] += prices['pretax_rackrate']
            total_prices['discount'] += prices['discount']
            currentDate = currentDate + delta

        return total_prices, dateWisePrices, promo_applied

    # Called from Pricing Tasks.py for maximojo pricing sync
    @staticmethod
    def sync_roomprice_from_channelmanagerrateplan(
            start_date, end_date, room, occupancy, date_wise_state_tax):
        date = start_date
        while date < end_date:
            room_price = PriceUtils.__create_new_room_price(
                date, room, date_wise_state_tax, occupancy)
            logger.debug('Room Price created with id: %s', room_price.id)
            date = date + datetime.timedelta(days=1)

    @staticmethod
    def __create_new_room_price(
            date,
            room,
            date_wise_state_tax,
            occupancy,
            couponPercent=None):
        startDate = date
        endDate = date + datetime.timedelta(days=1)
        hotel = room.hotel
        channelManagerRatePlan = ChannelManagerRatePlan.objects.filter(
            start_date__lte=date,
            end_date__gte=date,
            room=room,
            is_active=True).order_by('-created_at').first()

        if channelManagerRatePlan is None:
            min_rate = room.price + \
                ((occupancy - 1) * room.hotel.price_increment)
        else:
            min_rate = float(channelManagerRatePlan.base_price) + (
                (occupancy - 1) * float(channelManagerRatePlan.price_increment))

        promosApplicableOnDate = PreferredPromo.objects.filter(
            Q(booking_start__lte=datetime.datetime.today()),
            Q(booking_end__gte=datetime.datetime.today()),
            Q(status=1), Q(hotel=room.hotel)).prefetch_related(
            'conditions')
        luxuryTaxes = LuxuryTax.objects.filter(state_id=room.hotel.state_id)

        hotelPriceDict, preferredPromo = PriceUtils.__getPriceAfterAutoPromo(
            date_wise_state_tax, datetime.datetime.today(), startDate, endDate, hotel, room, {
                date: min_rate}, promosApplicableOnDate, luxuryTaxes, couponPercent)
        value = hotelPriceDict[date]
        logger.debug("Inserting new room price")
        roomPrice = RoomPrice()
        roomPrice.date = date
        roomPrice.room = room
        roomPrice.guest_count = occupancy
        roomPrice.total_amount = value['sell_price']
        roomPrice.tax_amount = value['tax']
        roomPrice.rack_rate = value['rack_rate']
        roomPrice.promo_applied = preferredPromo
        roomPrice.save()
        return roomPrice

    # Used by Pricing Availability API and Hotel Detail Page
    @staticmethod
    def __getRoomPriceForDate(
            date,
            room,
            occupancy,
            date_wise_state_tax,
            couponPercent,
            coupon):
        occupancy = int(occupancy)
        if occupancy <= 0:
            occupancy = 1

        roomPrice = RoomPrice.objects.filter(
            room_id=room.id, date=date, guest_count=occupancy).select_related(
            "room", "room__hotel").order_by('-id').first()

        if not roomPrice:
            logger.info(
                "Room Price not found for date: %s. Creating new entry", date)
            roomPrice = PriceUtils.__create_new_room_price(
                date, room, date_wise_state_tax, occupancy, couponPercent)
            logger.info('New Room Price created: %s', roomPrice.id)
        else:
            logger.info('Room Price found: %s', roomPrice.id)

        prices = {}
        prices['sell'] = float(roomPrice.total_amount)
        prices['tax'] = float(roomPrice.tax_amount)
        prices['rack_rate'] = float(roomPrice.rack_rate)
        prices['pretax'] = float(roomPrice.total_amount) - \
            float(roomPrice.tax_amount)
        prices['discount'] = 0

        # Apply Coupon on the top of this
        hotel = room.hotel
        taxToBeApplied = date_wise_state_tax.get(date, 0)
        logger.info(
            "StateTax to be applied for date: %s = %s",
            date,
            taxToBeApplied)
        luxuryTaxes = LuxuryTax.objects.filter(state_id=room.hotel.state_id)
        rack_rate = prices['rack_rate']
        pretax_rackrate = PriceUtils.__getPretaxPrice(
            taxToBeApplied, hotel, rack_rate, luxuryTaxes)
        prices['pretax_rackrate'] = float(pretax_rackrate)

        logger.info("Prices for date: %s ==== %s", date, prices)

        if couponPercent is not None:
            logger.info(
                "Updating with coupon: %s and couponPercent: %s",
                coupon.code,
                couponPercent)
            if coupon.price_component_applicable == DiscountCoupon.RACK_RATE:
                pretaxAmount = float(prices['pretax_rackrate'])
                tax = float(rack_rate) - pretaxAmount
                prices['sell'] = rack_rate
                prices['tax'] = tax
                logger.info(
                    "Rack rate: %s, Tax on rack rate: %s, Rack Rate pretax amount = %s",
                    rack_rate,
                    tax,
                    pretaxAmount)
            else:
                pretaxAmount = float(roomPrice.total_amount) - \
                    float(roomPrice.tax_amount)
                logger.info("Sale Rate pretax amount = %s", pretaxAmount)
            discount = float(couponPercent) * pretaxAmount / 100
            prices['pretax'] = pretaxAmount
            prices['discount'] = discount

            # NOTE: All the below logic seems to be incorrect. If we update the tax value here, then the formula:
            #               pretax + tax - discount = final_price
            # will become incorrect. The LHS and RHS won't match. Since, we
            # changed tax, but not pretax value.
            finalPretaxAmount = pretaxAmount - discount
            logger.info("Post discount pretax amount = %s", finalPretaxAmount)
            finalTax = PriceUtils.getTaxOnPrice(
                taxToBeApplied, hotel, finalPretaxAmount, luxuryTaxes)
            logger.info("Final Tax: %s", finalTax)
            prices['tax'] = float(finalTax)

            logger.info("Final prices for date: %s === %s", date, prices)
        return prices, roomPrice.promo_applied

    @staticmethod
    def __getApplicationPromoFromAllPromos(
            bookingDate,
            stayDate,
            stayEnd,
            room,
            promosApplicableOnDates):
        bookingDate = bookingDate.date()
        applicablePromos = []
        if promosApplicableOnDates is None:
            promosApplicableOnDates = []
        for promo in promosApplicableOnDates:
            conditions = promo.conditions.all()
            conditionDict = {}
            for condition in conditions:
                conditionDict[condition.condition_name] = condition

            roomTypeList = json.loads(
                conditionDict['RoomTypeList'].condition_params)
            if room.room_type_code in roomTypeList:
                if promo.type == 'Basic':
                    applicablePromos.append(promo)
                elif promo.type == 'MinStay':
                    if stayEnd is not None:
                        if (stayEnd - stayDate).days >= promo.min_nights and promo.min_nights != -1:
                            applicablePromos.append(promo)
                elif promo.type == 'EarlyBird':
                    if (stayDate - bookingDate).days >= promo.min_advance_days and promo.min_advance_days != -1:
                        applicablePromos.append(promo)
                elif promo.type == 'LastMinute':
                    if (stayDate - bookingDate).days <= promo.max_advance_days and promo.max_advance_days != -1:
                        applicablePromos.append(promo)
        return applicablePromos

    @staticmethod
    def __getPretaxPrice(taxToBeApplied, hotel, price, luxuryTaxes=None):
        taxPercentages = {}
        if not taxToBeApplied:
            taxToBeApplied = 0

        for luxuryTax in luxuryTaxes:
            taxValue = float(luxuryTax.tax_value) + float(taxToBeApplied)
            startRange = (float(luxuryTax.start_range)
                          * (taxValue + 100)) / 100
            endRange = (float(luxuryTax.end_range) * (taxValue + 100)) / 100
            taxPercentages[(startRange, endRange)] = taxValue
        if hotel is not None:
            applicablePercentage = taxToBeApplied
        else:
            applicablePercentage = 0
        for rangeValue in list(taxPercentages.keys()):
            if price >= rangeValue[0]:
                if price <= rangeValue[1] or rangeValue[1] < 0:
                    applicablePercentage = taxPercentages[rangeValue]
                    break

        pretaxPrice = (float(price) * 100.00) / \
            (100.00 + float(applicablePercentage))
        return pretaxPrice

    @staticmethod
    def getTaxOnPrice(taxToBeApplied, hotel, price, luxuryTaxes):
        logger.info(
            'PricingUtils#getTaxOnPrice called with taxToBeApplied: %s, hotel: %s, price: %s, luxuryTax: %s',
            taxToBeApplied,
            hotel,
            price,
            luxuryTaxes)
        if not taxToBeApplied:
            taxToBeApplied = 0

        if hotel is not None:
            applicablePercentage = taxToBeApplied
        else:
            applicablePercentage = 0
        for luxuryTax in luxuryTaxes:
            if price >= luxuryTax.start_range and price <= luxuryTax.end_range:
                tax_value = luxuryTax.tax_value if luxuryTax.tax_value else 0
                applicablePercentage = tax_value + taxToBeApplied
                break
        tax = (float(price) * (float(applicablePercentage))) / 100.00

        logger.debug(
            'Applicable Percentage: %s, tax: %s',
            applicablePercentage,
            tax)
        return tax

    @staticmethod
    def __getDiscountedPrice(
            pretaxPrice,
            taxToBeApplied,
            hotel,
            price,
            discount,
            discountApplication,
            luxuryTaxes=None):
        logger.info(
            '__getDiscountedPrice called with pretaxPrice: %s, taxToBeApplied: %s, discount: %s, price: %s',
            pretaxPrice,
            taxToBeApplied,
            discount,
            price)
        discountValue = 0
        if discountApplication == 'Percent':
            discountValue = (float(pretaxPrice) * float(discount)) / 100.00
        else:
            discountValue = discount
        finalPretaxPrice = float(pretaxPrice) - float(discountValue)
        logger.debug(
            'Discount Value: %s, Final Pretax Price: %s',
            discountValue,
            finalPretaxPrice)
        tax = PriceUtils.getTaxOnPrice(
            taxToBeApplied, hotel, finalPretaxPrice, luxuryTaxes)
        return finalPretaxPrice, tax

    @staticmethod
    def __getPriceAfterAutoPromo(
            date_wise_state_tax,
            bookingDate,
            stayStart,
            stayEnd,
            hotel,
            room,
            datewiseCurrentPrice,
            promosApplicableOnDate,
            luxuryTaxes,
            couponPercent=None):
        applicablePromos = PriceUtils.__getApplicationPromoFromAllPromos(
            bookingDate, stayStart, stayEnd, room, promosApplicableOnDate)
        finalDatewisePrice = {}
        dayList = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday']
        delta = datetime.timedelta(days=1)
        currentDate = stayStart
        preferredPromo = False
        while currentDate < stayEnd:
            if currentDate in datewiseCurrentPrice:
                taxToBeApplied = date_wise_state_tax.get(currentDate)
                if not taxToBeApplied:
                    taxToBeApplied = 0
                tax = 0
                rackRate = datewiseCurrentPrice[currentDate]
                sell_price = rackRate
                pretaxPrice = PriceUtils.__getPretaxPrice(
                    taxToBeApplied, hotel, sell_price, luxuryTaxes)
                for promo in applicablePromos:
                    if currentDate < promo.stay_start or currentDate > promo.stay_end:
                        continue
                    else:
                        conditions = promo.conditions.all()
                        conditionDict = {}
                        for condition in conditions:
                            conditionDict[condition.condition_name] = condition
                        dayWiseDiscounts = conditionDict['DayWiseDiscountValues']
                        dayWiseDiscountDict = json.loads(
                            dayWiseDiscounts.condition_params)
                        dayOfWeek = currentDate.weekday()
                        discountApplicationValue = dayWiseDiscountDict[dayList[dayOfWeek]]
                        tempDiscountedPrice, tempTax = PriceUtils.__getDiscountedPrice(
                            pretaxPrice, taxToBeApplied, hotel, rackRate, discountApplicationValue, promo.discountType, luxuryTaxes)
                        if tempDiscountedPrice + tempTax < sell_price:
                            sell_price = tempDiscountedPrice + tempTax
                            tax = tempTax
                        # TODO add preferred promo flag here
                        preferredPromo = True
                if tax == 0:
                    # NOTE: What's the idea behind below logic?
                    # And we've go a new_sell_price, why aren't we storing it
                    # in dict below. We still store old `sell_price`?
                    new_sell_price = PriceUtils.__getPretaxPrice(
                        taxToBeApplied, hotel, sell_price, luxuryTaxes)
                    tax = float(sell_price) - float(new_sell_price)

                finalDatewisePrice[currentDate] = {
                    'sell_price': sell_price, 'tax': tax, 'rack_rate': rackRate}
            currentDate = currentDate + delta
        logger.debug("final datawise price %s" % finalDatewisePrice)

        logger.debug('Started discount application')
        # taxObj = models.StateTax.objects.filter(Q(state=hotel.state) & Q(effective_date__lte=checkInDate) & (
        # Q(expiry_date__gte=checkInDate) |
        # Q(expiry_date__isnull=True))).aggregate(Sum('tax_value'))

        """
        Potential Bug here ================
        """
        # TODO: Why doing another Tax Calulation on CheckInDate???
        # logger.debug('date_wise_state_tax: %s', date_wise_state_tax)
        # checkInDate = stayStart
        # taxToBeApplied = date_wise_state_tax.get(checkInDate, 0)
        # logger.debug('tax to be applied: %s', taxToBeApplied)
        ## taxToBeApplied = taxObj['tax_value__sum']
        # if couponPercent is not None:
        #    for dayPriceKey in finalDatewisePrice.keys():
        #        pretaxAmount = float(finalDatewisePrice[dayPriceKey]['sell_price']) - float(finalDatewisePrice[dayPriceKey]['tax'])
        #        logger.info("pretaxAmount = %s" % pretaxAmount)
        #        finalPretaxAmount = float(pretaxAmount) - (float(couponPercent) * float(pretaxAmount) / float(100))
        #        logger.info("final pretax amount = %s" % finalPretaxAmount)

        #        finalTax = PriceUtils.getTaxOnPrice(taxToBeApplied, hotel, finalPretaxAmount, luxuryTaxes)
        #        finalDatewisePrice[dayPriceKey]['sell_price'] = float(finalPretaxAmount) + float(finalTax)
        #        finalDatewisePrice[dayPriceKey]['tax'] = finalTax
        # logger.debug("final datawise price after coupon application %s" % finalDatewisePrice)
        return finalDatewisePrice, preferredPromo

    # Used for Pricing on Search Page
    @staticmethod
    def getPriceForMultipleHotelsWithRoomConfigDateRange(
            checkin, checkout, hotels, roomConfig, cheapest_room_types):
        logger.info(
            'PricingUtils#getPriceForMultipleHotelsWithRoomConfigDateRange called with args: [checkin: %s, checkout: %s, hotels: %s, roomConfig: %s]',
            checkin,
            checkout,
            hotels,
            roomConfig)
        delta = datetime.timedelta(days=1)
        # datetime.datetime.strptime(checkin, "%Y-%m-%d").date()
        checkInDate = checkin
        # datetime.datetime.strptime(checkout, "%Y-%m-%d").date()
        checkoutDate = checkout

        hotel_date_wise_state_tax = defaultdict(dict)
        states = set([hotel.state.id for hotel in hotels])
        start = checkInDate
        end = checkoutDate
        while start < end:
            date_wise_state_tax = {}
            taxObj = StateTax.objects.values('state').filter(
                Q(state_id__in=states) & Q(effective_date__lte=start) & (
                    Q(expiry_date__gte=start) | Q(expiry_date__isnull=True))).annotate(
                Sum('tax_value'))
            for stateTax in taxObj:
                date_wise_state_tax[start.date()] = stateTax['tax_value__sum']
                for hotel in hotels:
                    if hotel.state.id == stateTax['state']:
                        hotel_date_wise_state_tax[hotel].update(
                            date_wise_state_tax)
            start = start + datetime.timedelta(days=1)

        hotelWisePrices = {}
        for hotel in hotels:
            hotelWisePrices[hotel.id] = {
                'pretax': 0, 'tax': 0, 'sell': 0, 'rack_rate': 0}

        TWO_DECIMAL_PLACES = Decimal('.01')
        while checkInDate < checkoutDate:
            checkin_str = checkInDate.strftime(date_time_utils.DATE_FORMAT)
            hotel_wise_prices = PriceUtils.__getPriceForMultipleHotelsWithRoomConfig_For_Date(
                hotel_date_wise_state_tax, checkin_str, hotels, roomConfig, cheapest_room_types)

            for hotel_id, price_dict in list(hotel_wise_prices.items()):
                prices = hotelWisePrices[hotel_id]
                prices['pretax'] += price_dict['pretax']
                prices['tax'] += price_dict['tax']
                prices['sell'] += price_dict['sell']
                prices['rack_rate'] += price_dict['rack_rate']
                breakup = {
                    checkin_str: {
                        'pretax': price_dict['pretax'],
                        'tax': price_dict['tax'],
                        'sell': price_dict['sell'],
                        'rack_rate': price_dict['rack_rate']}}
                if 'breakup' not in prices:
                    prices['breakup'] = {}
                prices['breakup'].update(breakup)
            logger.debug(
                "Hotel Wise Price after checkInDate: %s == %s",
                checkInDate,
                hotelWisePrices)
            checkInDate = checkInDate + delta

        for hotel in hotels:
            if hotelWisePrices[hotel.id]["rack_rate"] == 0:
                hotelWisePrices[hotel.id]["discount_percent"] = 0
            else:
                hotelWisePrices[hotel.id]["discount_percent"] = int(
                    ((hotelWisePrices[hotel.id]["rack_rate"] - hotelWisePrices[hotel.id]["sell"]) / hotelWisePrices[hotel.id]["rack_rate"]) * 100)
        return hotelWisePrices

    @staticmethod
    def __getPriceForMultipleHotelsWithRoomConfig_For_Date(
            hotel_date_wise_state_tax, date, hotels, roomConfig, cheapest_room_types):
        logger.info(
            'PricingUtils#getPriceForMultipleHotelsWithRoomConfig called with args: [date: %s, total hotels: %s, roomConfig: %s]',
            date,
            len(hotels),
            roomConfig)
        hotelWisePrices = {}
        for hotel in hotels:
            hotelWisePrices[hotel.id] = {
                'pretax': 0, 'tax': 0, 'sell': 0, 'rack_rate': 0}

        logger.info(
            'Room Config Sent.............############# :%s.. Type room roncif: %s',
            roomConfig,
            type(roomConfig))

        for singleRoomConfig in roomConfig:
            logger.info('Single Room RConfig.... #####: %s', singleRoomConfig)
            hotels = PriceUtils.__getMinPriceForMultipleHotels(
                hotel_date_wise_state_tax, date, hotels, cheapest_room_types, singleRoomConfig[0])
            for hotel in hotels:
                hotelWisePrices[hotel.id]['pretax'] = float(
                    hotelWisePrices[hotel.id]['pretax']) + float(hotel.minrate) - float(hotel.tax)
                hotelWisePrices[hotel.id]['tax'] = float(
                    hotelWisePrices[hotel.id]['tax']) + float(hotel.tax)
                hotelWisePrices[hotel.id]['sell'] = float(
                    hotelWisePrices[hotel.id]['sell']) + float(hotel.minrate)
                hotelWisePrices[hotel.id]['rack_rate'] = float(
                    hotelWisePrices[hotel.id]['rack_rate']) + float(hotel.rack_rate)
        return hotelWisePrices

    @staticmethod
    def __getMinPriceForMultipleHotels(
            hotel_date_wise_state_tax,
            date,
            hotels,
            cheapest_room_types,
            occupancy=1):
        logger.info(
            'PricingUtils#__getMinPriceForMultipleHotels called with args: [date: %s, hotels: %s, occupancy: %s]',
            date,
            hotels,
            occupancy)
        resultHotelList = []

        occupancy = int(occupancy)
        delta = datetime.timedelta(days=1)
        checkinDate = datetime.datetime.strptime(date, "%Y-%m-%d").date()
        checkoutDate = checkinDate + delta
        hotelRoomIdMap = {}
        hotel_by_id = {}
        hotelPricePreCalculated = {}
        for hotel in hotels:
            hotelPricePreCalculated[hotel.id] = False
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            allRooms = hotel_repository.get_rooms_for_hotel(hotel)
            hotelRoomIdMap[hotel.id] = None
            hotel_by_id[hotel.id] = hotel
            cheapest_room_type = cheapest_room_types.get(hotel.id)

            for room in allRooms:
                if cheapest_room_type['room_type'] == room.room_type_code:
                    hotelRoomIdMap[hotel.id] = room.id
                    break
        logger.debug('Hotel RoomId map created: %s', hotelRoomIdMap)
        channelManagerPlanDict = {}
        minRatePlan = {}
        minChannelManagerPlan = {}
        orderedRoomDict = {'Acacia': 0, 'Oak': 1, 'Maple': 2, 'Mahogany': 3}

        logger.debug(
            'Getting RoomPrice for roomIDs: %s', list(
                hotelRoomIdMap.values()))
        roomPrices = RoomPrice.objects.filter(
            room_id__in=list(
                hotelRoomIdMap.values()),
            date=date,
            guest_count=occupancy).select_related(
            "room",
            "room__hotel").order_by('-id')
        room_price_by_roomId = {}
        room_with_missing_price = dict(hotelRoomIdMap)
        hotel_with_missing_price = dict(hotel_by_id)
        logger.info(
            'Room id with missing price: %s', list(
                room_with_missing_price.keys()))
        logger.info(
            'Hotel id with missing price: %s', list(
                hotel_with_missing_price.keys()))
        for price in roomPrices:
            hotel = hotel_by_id[price.room.hotel.id]
            if price.room.id not in list(room_price_by_roomId.keys()):
                room_price_by_roomId[price.room.id] = price
                hotel.minrate = price.total_amount
                hotel.tax = price.tax_amount
                hotel.rack_rate = price.rack_rate
                hotelPricePreCalculated[hotel.id] = True
                resultHotelList.append(hotel)
                del room_with_missing_price[hotel.id]
                del hotel_with_missing_price[hotel.id]

        logger.info(
            'Room id with missing price: %s', list(
                room_with_missing_price.keys()))
        logger.info(
            'Hotel id with missing price: %s', list(
                hotel_with_missing_price.keys()))

        try:
            if len(room_with_missing_price) > 0:
                # TODO: Should we create DB index on start_date and end_date on ChannelManager and RoomRatePlan
                # The below query takes 160-180 msec for single date range
                allChannelManagerRatePlans = ChannelManagerRatePlan.objects.values(
                    'hotel_id', 'base_price', 'price_increment', 'room_id').filter(
                    Q(
                        start_date__lte=checkinDate), Q(
                        end_date__gte=checkinDate), Q(
                        is_active=True), room_id__in=list(
                        room_with_missing_price.values())).order_by('-created_at')
                hotel_wise_price = {}

                for rate_plan in allChannelManagerRatePlans:
                    if rate_plan['hotel_id'] not in hotel_wise_price:
                        hotel_wise_price[rate_plan['hotel_id']] = rate_plan

                logger.info('Hotel wise price rate plan: %s', hotel_wise_price)

                for hotel in list(hotel_with_missing_price.values()):
                    price = None
                    rooms = hotel.rooms
                    if hotel.id in hotel_wise_price:
                        rate_plan = hotel_wise_price[hotel.id]
                        hotel.minrate = rate_plan['base_price'] + \
                            (occupancy - 1) * rate_plan['price_increment']
                    else:
                        for room in rooms:
                            if hotelRoomIdMap[hotel.id] is not None:
                                if room.id == hotelRoomIdMap[hotel.id]:
                                    price = room.price
                                    break
                            else:
                                if price is None or price > room.price:
                                    price = room.price

                        hotel.minrate = price + \
                            (hotel.price_increment * (occupancy - 1))
                promosApplicableOnDate = PreferredPromo.objects.filter(
                    Q(booking_start__lte=datetime.datetime.today()),
                    Q(booking_end__gte=datetime.datetime.today()),
                    Q(status=1), Q(hotel_id__in=list(hotel_with_missing_price.keys()))).prefetch_related(
                    'conditions').select_related('hotel')
                hotelWisePromoApplicableOnDate = {}
                for promo in promosApplicableOnDate:
                    hotelId = promo.hotel.id
                    if hotelId in hotelWisePromoApplicableOnDate:
                        hotelWisePromoApplicableOnDate[hotelId].append(promo)
                    else:
                        hotelWisePromoApplicableOnDate[hotelId] = [promo]
                luxuryTaxes = LuxuryTax.objects.all()
                luxuryTaxDict = {}
                for luxuryTax in luxuryTaxes:
                    if luxuryTax.state_id not in list(luxuryTaxDict.keys()):
                        luxuryTaxDict[luxuryTax.state_id] = [luxuryTax]
                    else:
                        luxuryTaxDict[luxuryTax.state_id].append(luxuryTax)
                for hotel in list(hotel_with_missing_price.values()):
                    if hotelPricePreCalculated[hotel.id]:
                        continue
                    cheapestRoomId = hotelRoomIdMap[hotel.id]
                    hotel_repository = RepositoriesFactory.get_hotel_repository()
                    rooms = hotel_repository.get_rooms_for_hotel(hotel)
                    if cheapestRoomId is None:
                        roomForDiscount = None

                        for room in rooms:
                            if roomForDiscount is None or orderedRoomDict[room.room_type_code] < \
                                    orderedRoomDict[roomForDiscount.room_type_code]:
                                roomForDiscount = room
                    else:
                        roomForDiscount = [
                            room for room in rooms if room.id == cheapestRoomId][0]

                    date_wise_state_tax = hotel_date_wise_state_tax.get(
                        hotel, {})
                    hotelPriceDict, preferredPromo = PriceUtils.__getPriceAfterAutoPromo(
                        date_wise_state_tax, datetime.datetime.today(), checkinDate,
                        checkoutDate, hotel, roomForDiscount, {checkinDate: hotel.minrate},
                        hotelWisePromoApplicableOnDate.get(hotel.id, []),
                        luxuryTaxDict.get(hotel.state.id, []))
                    totalPrice = 0
                    totalTax = 0
                    totalRackRate = 0
                    roomPriceList = []
                    for key in hotelPriceDict:
                        totalPrice += hotelPriceDict[key]['sell_price']
                        totalTax += hotelPriceDict[key]['tax']
                        totalRackRate += hotelPriceDict[key]['rack_rate']
                        try:
                            roomPrice = RoomPrice()
                            roomPrice.date = key
                            roomPrice.room = roomForDiscount
                            roomPrice.guest_count = occupancy
                            roomPrice.total_amount = hotelPriceDict[key]['sell_price']
                            roomPrice.tax_amount = hotelPriceDict[key]['tax']
                            roomPrice.rack_rate = hotelPriceDict[key]['rack_rate']
                            roomPrice.promo_applied = preferredPromo
                            roomPrice.save()

                        except Exception:
                            logger.exception("unable to update prices")

                    hotel.minrate = totalPrice
                    hotel.tax = totalTax
                    hotel.rack_rate = totalRackRate
                    resultHotelList.append(hotel)
                    # logger.info(resultHotelList)
        except Exception as excp:
            logger.exception("Exception in __getMinPriceForMultipleHotels")
        return resultHotelList
