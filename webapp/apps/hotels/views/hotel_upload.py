import json
import logging
import os

import requests
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import DatabaseError
from django.http.response import HttpResponse, HttpResponseRedirect
from formtools.wizard.views import SessionWizardView

from apps.fot.models import FotHotel
from apps.hotels import tasks
from apps.hotels.forms.hotel_upload_forms import HotelLiveForm, ConfirmHotelForm, RoomAmenityForm
from apps.hotels.service.hotel_upload_service import HotelUploadService
from apps.third_party.service.google_hotel_ads.transaction_pricing_request import TransactionRequestService

log = logging.getLogger(__name__)

# form_steps
hotel_file_upload = '0'
room_amenities_upload = '1'
confirm_tax_upload = '2'


class HotelUploadWizard(SessionWizardView):
    TEMPLATES = {hotel_file_upload: "admin/dbcommon/hotel/upload_xls.html",
                 room_amenities_upload: "admin/dbcommon/hotel/upload_xls.html",
                 confirm_tax_upload: "admin/dbcommon/hotel/hotel_confirm.html"
                 }

    form_list = [
        (hotel_file_upload, HotelLiveForm),
        (room_amenities_upload, RoomAmenityForm),
        (confirm_tax_upload, ConfirmHotelForm)
    ]

    file_storage = FileSystemStorage(
        location=os.path.join(
            settings.HOTEL_UPLOAD_FILE_LOCATION,
            'hotel_upload'))

    def get_template_names(self):
        log.info("templates %s and step %s",
                 [self.TEMPLATES[self.steps.current]],
                 self.steps.current)
        return [self.TEMPLATES[self.steps.current]]

    def render_goto_step(self, goto_step, **kwargs):
        self.storage.current_step = goto_step
        form = self.get_form(
            data=self.storage.get_step_data(self.steps.current),
            files=self.storage.get_step_files(self.steps.current))
        return self.render(form)

    def get_context_data(self, form, **kwargs):
        context = super(
            HotelUploadWizard,
            self).get_context_data(
            form=form,
            **kwargs)
        if self.steps.current == confirm_tax_upload:
            context.update(self.get_hotel_live_dashboard())
        return context

    def get_hotel_live_dashboard(self):
        log.info(" in get_hotel_live_dashboard")
        cleaned_data = self.get_cleaned_data_for_step('0')
        room_amenity_data = self.get_cleaned_data_for_step('1')
        hotel_excel = cleaned_data['excel']
        images = cleaned_data['images']
        facilities = cleaned_data['facilities']
        hotelogix_id = cleaned_data['hotelogix_id']
        hotelogix_name = cleaned_data['hotelogix_name']
        acacia_price = cleaned_data['acacia_room_price']
        oak_room_price = cleaned_data['oak_room_price']
        maple_room_price = cleaned_data['maple_room_price']
        mahogany_room_price = cleaned_data['mahogany_room_price']
        city_desc = cleaned_data['city_desc']
        hotel_upload_service = HotelUploadService(
            settings, hotel_excel, images)
        log.info(
            "type room_amenity_data %s  data %s ",
            type(room_amenity_data),
            room_amenity_data)

        airport_directions, airport_landmark, city, city_alias, facilities, highlights, hotel, landmark_1, \
            landmark_1_directions, landmark_2, landmark_2_directions, locality, railway_directions, railway_landmark, \
            rooms, state, rooms_amenities = hotel_upload_service.extract_excel(
                facilities, hotelogix_id, hotelogix_name, acacia_price, oak_room_price,
                maple_room_price,
                mahogany_room_price, room_amenity_data)

        warnings = hotel_upload_service.validate(
            airport_directions,
            airport_landmark,
            city,
            city_desc,
            city_alias,
            facilities,
            highlights,
            hotel,
            landmark_1,
            landmark_1_directions,
            landmark_2,
            landmark_2_directions,
            locality,
            railway_directions,
            railway_landmark,
            rooms,
            state)

        context = {
            "data": {
                "hotel": hotel,
                "rooms": rooms,
                "locality": locality,
                "city": city,
                "state": state,
                "facilities": facilities,
                "highlights": highlights,
                "landmark_1": landmark_1,
                "landmark_1_directions": landmark_1_directions,
                "landmark_2": landmark_2,
                "landmark_2_directions": landmark_2_directions,
                "airport": airport_landmark,
                "airport_directions": airport_directions,
                "railway": railway_landmark,
                "railway_directions": railway_directions,
                "hotel_mm_code": cleaned_data['hotel_mm_code'],
                "hotel_mm_acacia_room_code": cleaned_data['hotel_mm_acacia_room_code'],
                "hotel_mm_oak_room_code": cleaned_data['hotel_mm_oak_room_code'],
                "hotel_mm_maple_room_code": cleaned_data['hotel_mm_maple_room_code'],
                "hotel_mm_mahogany_room_code": cleaned_data['hotel_mm_mahogany_room_code'],
                "hotel_seo_meta_title": cleaned_data['hotel_seo_meta_title'],
                "hotel_seo_meta_tags": cleaned_data['hotel_seo_meta_tags'],
                "city_desc": city_desc,
                "city_seo_meta_title": cleaned_data['city_seo_meta_title'],
                "city_seo_meta_tags": cleaned_data['city_seo_meta_tags'],
                "rooms_amenities": rooms_amenities,
            },
            "warnings": warnings}

        return context

    def done(self, form_list, **kwargs):
        cleaned_data = self.get_cleaned_data_for_step('0')
        room_amenity_data = self.get_cleaned_data_for_step('1')
        hotel_excel = cleaned_data['excel']
        images = cleaned_data['images']
        hotelogix_id = cleaned_data['hotelogix_id']
        hotelogix_name = cleaned_data['hotelogix_name']
        facilities = cleaned_data['facilities']
        acacia_price = cleaned_data['acacia_room_price']
        oak_room_price = cleaned_data['oak_room_price']
        maple_room_price = cleaned_data['maple_room_price']
        mahogany_room_price = cleaned_data['mahogany_room_price']
        hotel_mm_code = cleaned_data['hotel_mm_code']
        hotel_mm_acacia_room_code = cleaned_data['hotel_mm_acacia_room_code']
        hotel_mm_oak_room_code = cleaned_data['hotel_mm_oak_room_code']
        hotel_mm_maple_room_code = cleaned_data['hotel_mm_maple_room_code']
        hotel_mm_mahogany_room_code = cleaned_data['hotel_mm_mahogany_room_code']
        hotel_seo_meta_title = cleaned_data['hotel_seo_meta_title']
        hotel_seo_meta_tags = cleaned_data['hotel_seo_meta_tags']
        city_desc = cleaned_data['city_desc']
        city_image = cleaned_data['city_image']
        city_seo_meta_title = cleaned_data['city_seo_meta_title']
        city_seo_meta_tags = cleaned_data['city_seo_meta_tags']

        hotel_upload_service = HotelUploadService(
            settings, hotel_excel, images)

        try:
            hotel, city, room, state = hotel_upload_service.process_upload(hotel_seo_meta_title,
                                                                           hotel_seo_meta_tags,
                                                                           city_desc,
                                                                           city_image,
                                                                           city_seo_meta_title,
                                                                           city_seo_meta_tags,
                                                                           facilities,
                                                                           hotelogix_id,
                                                                           hotelogix_name,
                                                                           hotel_mm_code,
                                                                           hotel_mm_acacia_room_code,
                                                                           hotel_mm_oak_room_code,
                                                                           hotel_mm_maple_room_code,
                                                                           hotel_mm_mahogany_room_code,
                                                                           acacia_price, oak_room_price,
                                                                           maple_room_price,
                                                                           mahogany_room_price, room_amenity_data
                                                                           )
            self.__upload_to_external_systems(hotel, city)

        except Exception as e:
            log.exception("Unable to save hotel")
            return HttpResponse(json.dumps({
                "success": False,
                "message": "Hotel uploaded failed!!"
            }))

        return HttpResponseRedirect(
            reverse(
                'admin:dbcommon_image_change',
                args=[
                    hotel.id]))



    def upload_to_fot(self, hotel):
        try:
            fot_hotel = FotHotel()
            fot_hotel.hotel_id = hotel.id
            fot_hotel.slots_allowed = 2
            fot_hotel.save()
            log.info(
                "uploaded on fot hotel with hotelogix_id = %s and fot id = %s",
                hotel.hotelogix_id,
                fot_hotel.id)
        except DatabaseError:
            log.exception(
                "Database error in FOT upload for hotel id %s and hotelogix_id %s",
                hotel.id,
                hotel.hotelogix_id)
        except Exception:
            log.exception(
                "upload to FOT failed for hotel id %s and hotelogix_id %s",
                hotel.id,
                hotel.hotelogix_id)
