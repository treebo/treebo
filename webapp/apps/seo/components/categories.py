
from apps.seo.components.base_component import BaseComponent
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.serializers.serializer import HotelTypesSerializer
from dbcommon.models.hotel import Categories, EnableCategory
from dbcommon.models.location import City

__author__ = 'ansilkareem'


class Category(BaseComponent):

    def __init__(self, current_city, footer_data, exclude_list):
        super(Category, self).__init__(current_city, footer_data, exclude_list)

    def build(self):
        enabled_list = [
            item['hotel_type__id'] for item in EnableCategory.objects.filter(
                city=self.current_city.id) .values('hotel_type__id')]

        data = {
            'key': 'category',
            'title': 'Hotel types',
            'content': AutoCompleteResultBuilder.build_category_result(
                Categories.objects.filter(
                    hotel__city=self.current_city.id,
                    enable_for_seo=True,
                    hotel__city__enable_for_seo=True,
                    id__in=enabled_list).distinct(). exclude(
                    id__in=self.exclude_list),
                self.current_city)}

        serialized_obj = HotelTypesSerializer(data=data)
        if serialized_obj.is_valid():
            return serialized_obj.validated_data
        else:
            return {
                'key': 'category',
                'title': 'Hotel types',
                'content': []
            }
