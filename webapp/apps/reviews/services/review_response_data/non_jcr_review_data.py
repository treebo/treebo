import logging
from django.utils.encoding import smart_str
from decimal import Decimal
from datetime import datetime
from apps.reviews import constants
from apps.reviews.serializers.review_serializers import UserDTO, ResponseDetailDTO

logger = logging.getLogger(__name__)


class NonJCRReviewData:
    def __init__(self, review_data, hotel_id, hotel_ta_location_id):
        self.review_data = review_data
        self.hotel_id = hotel_id
        self.hotel_ta_location_id = hotel_ta_location_id

    def __get_formatted_text(self, _str):
        if _str:
            return smart_str(_str.replace('"', ''))
        else:
            return ''

    def __get_top_review_serializer(self, review):
        try:
            review_dict = dict()
            review_dict['user'] = {}
            user = UserDTO(data={
                'user_name': review['user_name'],
                'image_url': review.get('user_image_url', constants.DEFAULT_USER_IMAGE_URL).replace(
                    "http:", "https:")
            })
            if user.is_valid():
                review_dict['user'] = user.validated_data

            review_dict["response_detail"] = {}
            response_detail = ResponseDetailDTO(data={
                'author': review.get('owner_description', ''),
                'text': review.get('owner_rating_reply_text', ''),
                'response_published_date': review.get('owner_reply_published_date', '')
            })
            if response_detail.is_valid():
                review_dict["response_detail"] = response_detail.validated_data
            review_dict["review_detail"] = {
                "title": review.get('review_title', ''),
                "text": review.get('review_text', ''),
                "user_rating": review.get('user_rating', ''),
                "review_date": ((datetime.strptime(review.get('review_published_date'),
                                                   "%Y-%m-%dT%H:%M:%S%z")).replace(
                    tzinfo=None)).strftime('%d %b %y') if review.get(
                    'review_published_date') else None,
                "image": review.get('user_rating_image_url').replace("http:", "https:"),
                "text_url": review.get('user_rating_text_url'),
                "is_crawlable": False,
                "is_jcr": False
            }
            return review_dict
        except Exception as e:
            logger.exception('Exception in building top review serializer for hotel %s',
                             self.hotel_id)
            logger.error('Error: %s', str(e))
            return {}

    def __create_review_object(self, review):
        review_dict_obj = dict()
        review_dict_obj['hotel_ta_mapping'] = self.hotel_ta_location_id
        review_dict_obj['ta_review_id'] = smart_str(review.get('id'))
        review_dict_obj['review_text'] = self.__get_formatted_text(review.get('text'))
        review_dict_obj['review_title'] = self.__get_formatted_text(review.get('title'))
        review_dict_obj['review_published_date'] = smart_str(review.get('published_date'))
        review_dict_obj['helpful_votes'] = smart_str(review.get('helpful_votes')) if smart_str(
            review.get('helpful_votes')) else 0
        review_dict_obj['user_rating'] = smart_str(review.get('rating'))
        review_dict_obj['user_rating_image_url'] = smart_str(review.get('rating_image_url'))
        review_dict_obj['user_rating_text_url'] = smart_str(review.get('url'))
        user_name = review['user'].get('username')
        if user_name:
            review_dict_obj['user_name'] = self.__get_formatted_text(user_name)
        else:
            review_dict_obj['user_name'] = constants.DEFAULT_GUEST_NAME
        if review.get('owner_response'):
            review['owner_rating_reply_text'] = self.__get_formatted_text(
                review['owner_response'].get('text'))
            review['owner_description'] = self.__get_formatted_text(
                review['owner_response'].get('author'))
            review['owner_reply_published_date'] = self.__get_formatted_text(
                review['owner_response'].get(
                    'published_date'))
        return review_dict_obj

    def build(self):
        review_list = []
        for review in self.review_data:
            review_list.append(self.__create_review_object(review))
        reviews_sorted_list = sorted(review_list,
                                     key=lambda object1: Decimal(object1['user_rating']),
                                     reverse=True)
        top_reviews = []
        for review in reviews_sorted_list:
            review_dict = self.__get_top_review_serializer(review)
            if bool(review_dict):
                top_reviews.append(review_dict)
        return top_reviews
