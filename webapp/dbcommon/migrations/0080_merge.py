# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0076_auto_20180327_0603'),
        ('dbcommon', '0079_locality_free_text_url'),
    ]

    operations = [
    ]
