import logging
from decimal import Decimal
from apps.reviews import constants
from apps.content.models import ContentStore
from apps.reviews.serializers.review_serializers import RatingPercentageDTO, OverallRatingDTO, SubratingDTO, \
    OverallRankingDTO
from apps.reviews.services.review_response_data.base_review_data import BaseReviewData
from apps.reviews.services.trip_advisor.hotel_review_count_mapping import HotelReviewCountService
from apps.reviews.services.trip_advisor.hotel_reviews_data import HotelReviewsDataService
from apps.reviews.services.trip_advisor.subrating_hotel_mapping_service import HotelSubRatingMappingService

logger = logging.getLogger(__name__)


class HDReviewData(BaseReviewData):
    def __init__(self, hotel_id, ta_loc_id):
        self.rating = None
        self.image = None
        self.rating_percent = None
        self.overall_ranking = None
        self.overall_rating = None
        self.write_review_url = None
        self.read_reviews_url = None
        self.num_reviews = None
        self.awards = None
        self.subratings = None
        self.top_reviews = None
        super(HDReviewData, self).__init__(hotel_id, ta_loc_id)

    def build(self):
        try:
            if self.overall_rating_obj:
                self.write_review_url = self.overall_rating_obj.write_review_url
            elif self.ta_hotelid_mapping:
                self.write_review_url = self.ta_hotelid_mapping.write_review_url
            if self.overall_rating_obj:
                self.overall_ranking = self.__get__overall_ranking()
                if self.overall_rating_obj:
                    self.num_reviews = self.overall_rating_obj.num_reviews_count
                self.subratings = self.__get_subratings()
                self.awards = self.get_awards_list()
                self.read_reviews_url = self.__get_read_review_url()
                self.top_reviews = self.__get_top_reviews()
                self.overall_rating = self.__get_overall_rating()
        except:
            logger.exception('Exception occured in HDReviewData build %s:%s', self.hotel_id, self.ta_location_id)

    def __get_read_review_url(self):

        if self.overall_rating_obj:
            return self.overall_rating_obj.read_review_url
        else:
            return ''

    def __get_overall_rating(self):

        temp_rating = {'rating': 0, 'image': '', 'rating_percent': []}

        if self.overall_rating_obj:
            temp_rating['rating'] = self.overall_rating_obj.overall_rating
            temp_rating['image'] = self.overall_rating_obj.url_overall_rating_image.replace("http:","https:")
            rating_percent_list = self.__create_review_rating_count()
            temp_rating['rating_percent'] = rating_percent_list
        overall_rating = OverallRatingDTO(data=temp_rating)
        if overall_rating.is_valid():
            return overall_rating.validated_data
        else:
            return temp_rating

    def __create_review_rating_count(self, ):
        try:
            review_rating_count = []
            review_count = HotelReviewCountService()
            review_count_obj = review_count.fetch_review_count(ta_loc_id=self.ta_location_id, hotel_id=self.hotel_id)
            if review_count_obj:
                for cnt in range(5):
                    review_count_dict = {}
                    count_value = review_count_obj.filter(review_rating=str(abs(5 - cnt)),
                                                          hotel_ta_mapping__ta_location_id=self.ta_location_id,
                                                          hotel_ta_mapping__hotel__id=self.hotel_id).first()
                    review_count_dict["rating"] = str(abs(5 - cnt))
                    count = 0
                    percent = 0
                    if count_value:
                        count = count_value.count_of_customers
                        percent = count_value.percentage_of_customers

                    review_count_dict["count"] = str(count)
                    review_count_dict["percent"] = str(percent)
                    if count == 1:
                        review_str = str(count) + " " + "Review"
                    else:
                        review_str = str(count) + " " + "Reviews"
                    review_count_dict['review_str'] = review_str
                    review_rating_count.append(RatingPercentageDTO(review_count_dict).data)
            return review_rating_count
        except Exception:
            logger.exception("In ceating review rating count part of the response")
            return []

    def __get__overall_ranking(self, ):
        overall_ranking = {
            'rank_string': '',
            'rank_out_of': 0,
            'rank': 0
        }
        try:
            percentage = self.config.percent_for_ranking_visibility
            rank = self.overall_rating_obj.overall_ranking
            rank_out_of = self.overall_rating_obj.overall_ranking_out_of
            rank_percent = ((Decimal(rank) / Decimal(rank_out_of)) * 100)
            if (rank_percent) <= percentage:
                overall_ranking["rank_string"] = str(self.overall_rating_obj.overall_ranking_string)
                overall_ranking["rank_out_of"] = rank_out_of
                overall_ranking["rank"] = rank
            overall_ranking = OverallRankingDTO(data=overall_ranking)
            if overall_ranking.is_valid():
                return overall_ranking.validated_data
            else:
                return overall_ranking
        except Exception:
            logger.exception("exception in creating overall ranking part of the dict")
            return overall_ranking

    def __get_subratings(self, ):
        try:
            subratings = []
            hotel_subrating = HotelSubRatingMappingService()
            count_of_subratings = self.config.num_subratings_to_show
            top_subratings = hotel_subrating.fetch_top_enabled_subratings(self.hotel_id, self.ta_location_id,
                                                                          count_of_subratings)
            if top_subratings:
                for sub_rating in top_subratings:
                    sub = dict()
                    sub['image'] = str(sub_rating.url_subrating).replace("http:","https:")
                    if sub_rating.subrating.subrating_localized_name:
                        sub['name'] = str(sub_rating.subrating.subrating_localized_name)
                    else:
                        try:
                            sub_name = sub_rating.subrating.subrating_name.split('_')[1]
                        except ValueError:
                            sub_name = sub_rating.subrating.subrating_name
                        sub['name'] = str(sub_name)
                    sub['rating'] = str(sub_rating.subrating_value)
                    sub_serializer = SubratingDTO(data=sub)
                    if sub_serializer.is_valid():
                        subratings.append(sub_serializer.validated_data)
                    else:
                        subratings.append({})
            return subratings
        except Exception:
            logger.exception("Exception in creating subratings list part of the hd response")
            return []

    def __get_top_reviews(self, ):
        try:
            top_reviews = []
            review_data = HotelReviewsDataService()
            user_reviews = review_data.fetch_user_reviews(hotel_id=self.hotel_id, ta_loc_id=self.ta_location_id)
            if user_reviews:
                user_reviews = user_reviews[:self.config.num_user_reviews_to_show]
            reviews = list(user_reviews) if user_reviews else None
            if reviews:
                reviews_sorted_list = sorted(reviews, key=lambda object1: Decimal(object1.user_rating),
                                             reverse=True)
                default_ta_review_threshold = ContentStore.objects.filter(
                    name=constants.DEFAULT_TA_REVIEW_THRESHOLD_KEY, version=1).first()
                if default_ta_review_threshold:
                    review_threshold = default_ta_review_threshold.value
                else:
                    review_threshold = constants.DEFAULT_TA_REVIEW_THRESHOLD
                for rev in reviews_sorted_list:
                    review_dict = self.get_top_review_serializer(rev,review_threshold)
                    top_reviews.append(review_dict)
            return top_reviews
        except Exception as e:
            logger.exception('Exception occured in getting top reviews for hotel_id %s', self.hotel_id)
            logger.error('Error in getting top reviews due to %s', e.message)
            return []
