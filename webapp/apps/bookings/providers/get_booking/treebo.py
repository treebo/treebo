# pylint: disable=no-member,protected-access
import logging

from thsc.crs.entities.billing import Bill as THSCBill
from thsc.crs.entities.booking import Booking as THSCBooking
from thsc.crs.entities.booking import BookingSearchQuery
from apps.bookings.data_classes.guest import Adult, Child
from apps.bookings.providers.thsc_utils import ThscUtils
from apps.bookings.models import Booking as BookingModel
from apps.common.data_classes.address import Address
from apps.bookings.data_classes import Booking, Guest, Guests
from apps.bookings.data_classes.charges import Charge
from apps.bookings.data_classes.gst_details import GstDetails
from apps.bookings.data_classes.guest_stay import GuestStay
from apps.bookings.data_classes.payments import Payment
from apps.bookings.data_classes.room_stay import RoomStay
from apps.bookings.data_classes.source import Source
from apps.bookings.data_classes.status import status_from_value
from apps.bookings.providers.interfaces.get_booking import GetBookingInterface
from apps.common.exceptions.booking_exceptions import BookingNotFoundException
from apps.hotels.data_classes.room_types import RoomType
from apps.pricing.data_classes import DatePrice, Price

logger = logging.getLogger(__name__)


class TreeboCRSGetBooking(GetBookingInterface):
    def _get(self):
        booking_model = BookingModel.objects.get(order_id=self.booking_id)  # pylint: disable=no-member
        booking = self.get_from_model(booking_model)
        return booking

    @classmethod
    def get_from_model(cls, booking_model):
        thsc_booking = THSCBooking.get(booking_model.external_crs_booking_id)
        booking = cls.get_from_thsc_booking(thsc_booking)
        booking.created_timestamp = booking_model.created_at
        return booking

    @classmethod
    def get_from_thsc_booking(cls, thsc_booking):
        thsc_bill = THSCBill.get(thsc_booking.bill_id)

        hotel_id = thsc_booking.hotel_id

        booking = Booking(uid=thsc_booking.reference_number,
                          hotel_id=hotel_id,
                          gst_details=cls.gst_details(thsc_booking),
                          source=cls.source(thsc_booking),
                          comments=thsc_booking.comments,
                          )

        booking.status = status_from_value(thsc_booking.status.value)
        booking.created_timestamp = thsc_booking.created_at
        booking.cancelled_timestamp = thsc_booking.cancellation_datetime
        booking.expiry_time_stamp = thsc_booking.hold_till
        booking.crs_version = thsc_booking.version
        booking.crs_reference_id = thsc_booking.booking_id
        booking.crs_bill_id = thsc_booking.bill_id
        booking.crs_bill_version = thsc_bill.version

        booking.charges = cls.charges(thsc_bill)
        booking.room_stays = cls.room_stays(thsc_booking, thsc_bill)
        booking._booking_owner = cls.booking_owner(thsc_booking)

        if not thsc_booking.extra_information:
            thsc_booking.extra_information = {}

        booking.payments = [Payment(payment.payment_ref_id,
                                    payment.payment_ref_id,
                                    payment.date_of_payment,
                                    payment.amount,
                                    mode=ThscUtils.get_payment_mode_from_thsc_payment_mode(payment.payment_mode),
                                    status=ThscUtils.get_payment_status_from_thsc_payment_status(payment.status),
                                    type=ThscUtils.get_payment_type_from_thsc_payment_type(payment.payment_type)
                                    )
                            for payment in thsc_bill.payments]

        stay_source = thsc_booking.extra_information.get('stay_source')
        booking.stay_source = stay_source
        booking.paid_amount = thsc_bill.net_paid_amount.amount
        booking.payable_amount = thsc_bill.net_payable.amount
        booking.tax_amount = thsc_bill.total_tax_amount.amount
        booking.pre_tax_amount = thsc_bill.total_pretax_amount.amount
        booking.total_amount = thsc_bill.total_posttax_amount.amount

        return booking

    @classmethod
    def booking_owner(cls, thsc_booking):
        owner = [customer for customer in thsc_booking.customers
                 if customer.customer_id == thsc_booking.booking_owner_id][0]
        return owner

    @classmethod
    def guests(cls, thsc_booking):
        thsc_guests = [customer for customer in thsc_booking.customers
                       if not customer.customer_id == thsc_booking.booking_owner_id]

        guests = [Guest(uid=thsc_guest.customer_id,
                        name=(thsc_guest.first_name, thsc_guest.last_name),
                        email=thsc_guest.email or '',
                        phone_number=thsc_guest.phone_number or '',
                        )
                  for thsc_guest in thsc_guests]

        return Guests(guests)

    @classmethod
    def gst_details(cls, thsc_booking):
        owner = cls.booking_owner(thsc_booking)
        gst_details = owner.gst_details

        address = None
        legal_name = ''
        gst_number = ''
        if gst_details:
            address = gst_details.address
            legal_name = gst_details.legal_name
            gst_number = gst_details.gstin_num
        if address:
            address = Address(line1=address.field1,
                              line2=address.field2,
                              city=address.city or 'NA',
                              state=address.state or 'NA',
                              pincode=address.pincode or 'NA',
                              country=address.country or 'NA',
                              )
        else:
            address = Address.empty()

        gst_details = GstDetails(legal_name,
                                 gst_number,
                                 address=address)

        gst_details.crs_id = owner.customer_id
        return gst_details

    @classmethod
    def charges(cls, thsc_bill):
        charges = []
        for thsc_charge in thsc_bill.charges:
            charge = Charge(thsc_charge.charge_id,
                            thsc_charge.item.name,
                            thsc_charge.type,
                            thsc_charge.applicable_date,
                            thsc_charge.pretax_amount,
                            thsc_charge.posttax_amount - thsc_charge.pretax_amount,
                            str(thsc_charge.status),
                            )
            charges.append(charge)
        return charges

    @classmethod
    def room_stays(cls, thsc_booking, thsc_bill):
        guests = cls.guests(thsc_booking)

        thsc_charges = {chrg.charge_id: chrg for chrg in thsc_bill.charges}

        room_stays = []
        for thsc_room_stay in thsc_booking.rooms:
            room_type = RoomType(uid=thsc_room_stay.room_type_id, name='', max_occupancy=0)
            room_stay = RoomStay(thsc_room_stay.room_stay_id, room_type)

            charge_ids, prices = cls._room_stay_charges_and_prices(thsc_room_stay, thsc_charges)
            room_stay.guest_stays = cls._room_stays_guest_stays(thsc_room_stay, guests)
            room_stay.charge_ids = set(charge_ids)
            room_stay.price = Price(prices)
            room_stays.append(room_stay)
            room_stay.status = status_from_value(thsc_room_stay.status)

            room_stays.append(room_stay)

        return room_stays

    @classmethod
    def _room_stays_guest_stays(cls, thsc_room_stay, guests):
        guest_stays = []

        for thsc_guest_stay in thsc_room_stay.guests:
            try:
                guest = guests[thsc_guest_stay.guest_id]
            except KeyError:
                guest = Guest.guest_with_no_details()
            if thsc_guest_stay.age_group.value == 'adult':
                guest.age = Adult.end_age
            if thsc_guest_stay.age_group.value == 'child':
                guest.age = Child.end_age
            guest_stay = GuestStay(thsc_guest_stay.guest_stay_id,
                                   guest,
                                   thsc_guest_stay.checkin_date,
                                   thsc_guest_stay.checkout_date,
                                   )
            guest_stay.status = status_from_value(thsc_guest_stay.status)
            guest_stays.append(guest_stay)
        return guest_stays

    @classmethod
    def _room_stay_charges_and_prices(cls, thsc_room_stay, thsc_charges):
        charge_ids, prices = [], []
        for charge in thsc_room_stay.date_wise_charge_ids:
            charge_id = charge['charge_id']
            thsc_charge = thsc_charges[charge_id]
            price = DatePrice(thsc_charge.applicable_date,
                              thsc_charge.pretax_amount,
                              thsc_charge.posttax_amount - thsc_charge.pretax_amount)
            prices.append(price)
            charge_ids.append(charge_id)
        return charge_ids, prices

    @classmethod
    def source(cls, thsc_booking):
        source = Source(channel=thsc_booking.source.channel_id,
                        sub_channel=thsc_booking.source.sub_channel_id,
                        application=thsc_booking.source.application_id)
        return source

    @classmethod
    def search_from_booking_id(cls, thsc_booking_id_or_reference_id):
        """
        :param booking_id: Can be a reference id or a thsc booking id
        :return: booking object
        """
        thsc_booking = THSCBooking.search(BookingSearchQuery(query=thsc_booking_id_or_reference_id))
        bookings_list = thsc_booking.bookings
        if bookings_list:
            booking = cls.get_from_thsc_booking(bookings_list[0])
        else:
            raise BookingNotFoundException(developer_message="Booking not found in the crs")
        return booking

    @classmethod
    def search_bookings_by_phone_or_email(cls, phone_number, email):
        query = phone_number if phone_number else email
        if not query:
            logger.error("Neither phone number nor email found for querying CRS")
            return
        thsc_search_result = THSCBooking.search(BookingSearchQuery(query=query, sort_by="checkin", limit=50))
        booking_list = thsc_search_result.bookings
        while thsc_search_result.total > len(booking_list):
            booking_list.extend(
                THSCBooking.search(BookingSearchQuery(query=query, sort_by="checkin", limit=50,
                                                      offset=len(booking_list))).bookings)
        result = []
        for booking in booking_list:
            result.append(cls.get_from_thsc_booking(booking))
        return result

