# pylint: disable=no-member
import datetime
import logging

from apps.checkout.models import BookingRequest
from apps.common.exceptions.custom_exception import KeyNotFoundInContentStore
from apps.content.models import ContentStore
from apps.content.services.content_store_service import ContentStoreService
from apps.intelligent_pah.constants import GDC_SITE, BULK_BOOKING_CONTENT_KEY, PAH_OVERRIDE_CHANNEL
from .model_services import ModelService

logger = logging.getLogger(__name__)


class PAHEnable:

    def intelligent_pah(self, check_in, hotel_id, bid):
        model_service = ModelService()
        app_configurations = model_service.get_app_configurations()
        content_object = ContentStore.objects.filter(name="intelligent_pah_text", version=1).first()

        if content_object:
            pah_message = str(content_object.value)
        else:
            pah_message = ''
        source_of_booking = BookingRequest.objects.filter(id=bid).first()
        if source_of_booking:
            source_of_booking = source_of_booking.meta_http_host
        else:
            logger.info('Intelligent pah: bid %s not found in request table', bid)
            source_of_booking = ''

        if str(source_of_booking).strip(' ').lower() == GDC_SITE.lower():
            logger.info("Intelligent PAH disabled for bid : %s as it was done by GDC", bid)
            return True, ''

        if not app_configurations.enable_app:
            logger.info("The app : Intelligent PAH was disabled for hotel id : %s on check in date %s BID : %s",
                        hotel_id, check_in, bid)
            return True, pah_message
        logger.info("The app : Intelligent PAH was enabled for hotel id : %s on check in date %s BID : %s",
                    hotel_id, check_in, bid)
        if app_configurations.enable_for_all_hotels:
            logger.info("Intelligent PAH was enabled for all hotels CHECKING BLACK LIST hotel id : %s on check in"
                        " date %s BID : %s", hotel_id, check_in, bid)
            return self.check_black_list(check_in, hotel_id, bid, app_configurations), pah_message
        logger.info("Intelligent PAH was disabled for all hotels CHECKING WHITE LIST hotel id : %s on check in"
                    " date %s BID : %s", hotel_id, check_in, bid)
        return self.check_white_list(check_in, hotel_id, bid, app_configurations), pah_message

    def is_pah_enabled(self, check_in, hotel_id, bid, get_app_configurations):
        model_service = ModelService()
        check_in_timestamp = self.string_to_timestamp(check_in, 12)
        occupancy = model_service.get_occupancy_percentage(
            check_in_timestamp.date(), hotel_id)
        booking_timestamp = self.get_booking_timestamp()
        date_category = model_service.categorize_via_date(
            booking_timestamp, check_in_timestamp)
        occupancy_category = model_service.categorize_via_occupancy(
            occupancy, date_category)
        is_pah_enabled = model_service.is_pah_enabled(occupancy_category)
        is_audit_enabled = get_app_configurations.enable_auditing
        model_service.audit(
            is_audit_enabled,
            check_in,
            bid,
            hotel_id,
            occupancy,
            is_pah_enabled)
        return is_pah_enabled

    def string_to_timestamp(self, date, time):
        date = str(date).split("-")
        timestamp = datetime.datetime(
            int(date[0]), int(date[1]), int(date[2]), time)
        return timestamp

    def get_booking_timestamp(self):
        return datetime.datetime.now() + datetime.timedelta(hours=5, minutes=30)

    def check_black_list(self, check_in, hotel_id, bid, app_configurations):
        black_list = app_configurations.black_list.split(',')
        for black_listed_hotel in black_list:
            try:
                int(black_listed_hotel)
            except ValueError:
                logger.info('The hotel id : "%s" is not in a valid hotel id format', black_listed_hotel)
                continue
            if black_listed_hotel != '' and int(black_listed_hotel) == hotel_id:
                logger.info("Intelligent PAH : BLACKLIST FOUND : hotel id : %s on check in date %s BID : %s",
                            hotel_id, check_in, bid)
                return True
        return self.is_pah_enabled(check_in, hotel_id, bid, app_configurations)

    def check_white_list(self, check_in, hotel_id, bid, app_configurations):
        white_list = app_configurations.white_list.split(',')
        for white_listed_hotel in white_list:
            try:
                int(white_listed_hotel)
            except ValueError:
                logger.info(
                    'The hotel id : "%s" is not in a valid hotel id format',
                    white_listed_hotel)
                continue
            if white_listed_hotel != '' and int(
                white_listed_hotel) == hotel_id:
                logger.info(
                    "Intelligent PAH : WHITELIST FOUND : hotel id : %s on check in date %s BID : %s",
                    hotel_id,
                    check_in,
                    bid)
                return self.is_pah_enabled(
                    check_in, hotel_id, bid, app_configurations)
        return True

    @staticmethod
    def is_bulk_booking(check_in, check_out, room_count):
        date_format = "%Y-%m-%d"
        stay_duration = datetime.datetime.strptime(check_out, date_format) - datetime.datetime.strptime(check_in,
                                                                                                        date_format)
        try:
            bulk_booking_params = ContentStoreService.get_content(key=BULK_BOOKING_CONTENT_KEY)
            return stay_duration.days > bulk_booking_params.value['room_nights'] or room_count > \
                   bulk_booking_params.value['room_count']
        except KeyNotFoundInContentStore:
            logger.error("Bulk booking parameters not found in the content store")
            return stay_duration.days > 9 or room_count > 4

    @staticmethod
    def is_channel_override(channel):
        try:
            override_channel_list = ContentStoreService.get_content(key=PAH_OVERRIDE_CHANNEL).value
            return channel in override_channel_list
        except KeyNotFoundInContentStore:
            logger.error("Channel override key not found in the content store")
            return False

    def pay_at_hotel_status(self, check_in, check_out, room_count, hotel_id, bid, channel, feature_gate_override=True):
        is_bulk_booking = self.is_bulk_booking(check_in=check_in, check_out=check_out, room_count=room_count)
        intelligent_pah, pah_message = self.intelligent_pah(check_in=check_in, hotel_id=hotel_id, bid=bid)
        channel_override = self.is_channel_override(channel) if channel else False
        return (not is_bulk_booking and intelligent_pah and feature_gate_override) or channel_override, pah_message

