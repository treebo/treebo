import logging

from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.hotels.serializers.hotel_serializer import MinimalDetailsHotelSerializer
from apps.hotels.service.wifi_service import WifiService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class HotelWifiInfoApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    wifi_service = WifiService()

    def post(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            hotelogix_id = kwargs['hotelogix_id']
            is_enabled, hotel, location_key = self.wifi_service.is_bhaifi_enabled(
                hotelogix_id)
            hotel_data = {}
            if hotel:
                #hotel_data = json.loads(jsonpickle.encode(hotel))
                hotel_data = MinimalDetailsHotelSerializer(instance=hotel).data
            response = api_response.prep_success_response({
                'is_bhaifi_enabled': is_enabled,
                "hotel_details": hotel_data,
                "location_key": location_key
            })
        except TreeboException as e:
            logger.exception("treebo hotel wifi exception ")
            slack_alert.publish_alert(
                'hotel wifi info error during `%s` : %s ' %
                (HotelWifiInfoApi.__name__, str(
                    e)), channel=slack_alert.AUTH_ALERTS)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception("internal error hotel wifi exception")
            response = api_response.internal_error_response(error_codes.get(
                error_codes.HOTEL_WIFI_INFO_ERROR)['msg'], HotelWifiInfoApi.__name__)
        return response
