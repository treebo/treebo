import logging

from django.conf import settings
from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.checkout.service.itinerary_handler import ItineraryHandler
from apps.common.api_response import APIResponse as api_response
from base.decorator.request_validator import validate_request
from base.views.api_v2 import TreeboAPIView2

logger = logging.getLogger(__name__)


class RemoveCouponSerializer(serializers.Serializer):
    bid = serializers.IntegerField()
    apply_wallet = serializers.BooleanField(
        default=settings.APPLY_WALLET, required=False)


class RemoveCoupon(TreeboAPIView2):
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    checkout_service = ItineraryHandler()

    @validate_request(RemoveCouponSerializer)
    def post(self, request, *args, **kwargs):
        """
        Gets the discount for a booking id and coupon code.
        couponcode -- Coupon Code
        bid -- BookingRequest Id
        """
        validated_data = kwargs['validated_data']
        bid, apply_wallet = validated_data['bid'], validated_data['apply_wallet']
        price_data = self.checkout_service.remove_coupon(
            request, bid, apply_wallet)
        response = api_response.prep_success_response(price_data)
        return response
