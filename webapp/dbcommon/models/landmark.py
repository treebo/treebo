from caching.base import CachingMixin
from django.db import models
from djutil.models import TimeStampedModel
from django.utils.text import slugify

from common.constants import common_constants as const
from dbcommon.models.location import City
from dbcommon.models.default_permissions import DefaultPermissions


class SearchLandmark(CachingMixin, TimeStampedModel, DefaultPermissions):
    seo_url_name = models.CharField(max_length=500, default='')
    search_label = models.TextField(max_length=2000, null=True, blank=True)
    DISABLED = const.DISABLED
    ENABLED = const.ENABLED
    STATUS_CHOICES = const.STATUS_CHOICES
    status = models.IntegerField(default=ENABLED, choices=STATUS_CHOICES)
    latitude = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        null=True,
        blank=True,
        default=12.9667)
    longitude = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        null=True,
        blank=True,
        default=77.5667)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    distance_cap = models.IntegerField(blank=True, null=True)
    enable_for_seo = models.BooleanField(default=False)
    free_text_url = models.CharField(
        max_length=500, null=True, blank=True, default='')
    cs_id = models.CharField(max_length=200, null=True, blank=True)
    # Adding these fields because we are doing away with hotels_landmark table.
    # These are the extra fields there. Which we might need
    # name = models.CharField(max_length=500, null=True, blank=True, default='')
    # type = models.CharField(max_length=200, null=True, blank=True, default='')
    # popularity = models.PositiveIntegerField(default=0)
    is_hotel_present = models.NullBooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'search_landmark'
        verbose_name = 'Search Landmark'
        verbose_name_plural = 'Search Landmarks'
        ordering = ['seo_url_name']

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s %s %s' % (
            self.seo_url_name, self.search_label, self.city.name)

    def get_sitemap_url(self):
        landmark_name = self.free_text_url
        if not self.free_text_url or self.free_text_url.strip() == '':
            landmark_name = self.seo_url_name + '-' + str(self.city.slug)
        return '/'+'hotels-near-' + slugify(landmark_name)+'/'

    def get_landmark_slug(self):
        landmark_slug = self.free_text_url
        if not self.free_text_url or self.free_text_url.strip() == '':
            landmark_slug = self.seo_url_name + '-' + str(self.city.slug)
        return slugify(landmark_slug)


class LandmarkSeoDetails(CachingMixin, TimeStampedModel, DefaultPermissions):
    landmark = models.ForeignKey(SearchLandmark, on_delete=models.CASCADE)
    landmark_seo_title = models.CharField(max_length=500)
    landmark_seo_description = models.TextField(null=True, blank=True)
    landmark_seo_keyword = models.TextField(null=True, blank=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_landmarkseodetails'
        verbose_name = 'Landmark SEO Details'
        verbose_name_plural = 'Landmark SEO Details'

    def __unicode__(self):
        return '%s %s' % (
            self.landmark.free_text_url if self.landmark.free_text_url else self.landmark.seo_url_name, self.landmark_seo_title)
