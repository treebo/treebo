# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0006_auto_20170418_1355'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='email',
            field=models.EmailField(
                db_index=True,
                max_length=200,
                null=True,
                blank=True),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='phone',
            field=models.CharField(
                db_index=True,
                max_length=20,
                null=True,
                blank=True),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='status',
            field=models.CharField(
                default='Initiated',
                choices=[
                    ('Initiated',
                     'Initiated'),
                    ('Payment In Progress',
                     'Payment In Progress'),
                    ('Completed',
                     'Completed')],
                max_length=50,
                blank=True,
                null=True,
                db_index=True),
        ),
    ]
