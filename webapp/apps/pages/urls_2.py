

from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required

# from apps.fot.views import FotLandingPage, SearchAudit, FotRegistration, FotLogin, FotLogout
from apps.pages.views import (
    aboutus,
    bookings,
    booking_history,
    city,
    contactus,
    faq,
    feedback,
    hotel_detail,
    hotel_detail_old,
    iesupport,
    itinerary,
    join,
    landing,
    offers,
    order,
    payment_redirect_view,
    policy,
    redirector,
    hotels_in_city,
    settings,
    terms,
    transaction,
    voucher,
    forgot_password,
    reset_password,
    rewards_terms)
from apps.pages.views.login import Login
from apps.pages.views.logout import Logout
from apps.pages.views.referral import Referral
from apps.pages.views.register import Registration
from apps.pages.views.search import Search
app_name = 'pages'
urlpatterns = [
    url(r"^$", landing.Index.as_view(), name='index'),
    url(r"^aboutus/?$", aboutus.Aboutus.as_view(), name='aboutus'),
    url(r"^account/",
        login_required(
            booking_history.BookingHistory.as_view(),
            redirect_field_name="next",
            login_url="/login"),
        name='account'),
    # url(r"^booking/$", confirmation.Confirmation.as_view(), name='confirmation'),
    url(r"^itinerary/?$", itinerary.ItineraryPage.as_view(), name='itinerary'),
    url(r"^city/(?P<city_string>.*)-c(?P<hashed_city_id>\w+)/?$",
        city.CityDetails.as_view(), name='city'),
    url(r"^hotels-in-(?P<cityString>[\w-]+)/?$",
        hotels_in_city.HotelsInCity.as_view(),
        name='hotels_in_city'),
    url(r"^couple-friendly-hotels-in-(?P<cityString>[\w-]+)/?$",
        hotels_in_city.HotelsInCity.as_view(),
        name='couple_friendly_hotels_in_city'),
    url(r"^budget-hotels-in-(?P<cityString>[\w-]+)/?$",
        hotels_in_city.HotelsInCity.as_view(),
        name='budget_hotels_in_city'),
    url(r"^contactus/?$", contactus.Contactus.as_view(), name='contact-us'),
    url(r"^faq/?$", faq.Faq.as_view(), name='faq'),
    url(r"^feedback/?$", feedback.Feedback.as_view(), name='feedback'),
    url(r"^hotels/(?P<hotelString>.*)-h(?P<hotel_id>\w+)/?$", hotel_detail_old.HotelDetailRedirectView.as_view(),
        name='hotel-details'),
    url(r'^hotels-in-(?P<city_slug>[\w-]+)/(?P<hotel_slug>[\w-]+)-(?P<locality_slug>[\w-]+)-(?P<hotel_id>\w+)/?$',
        hotel_detail.HotelDetail.as_view(), name='hotel-details-new'),
    # ------ old url-------------------
    # TODO: Remove below url after checking and making it backward compatible
    # Being used in Trivago Hotel availability
    url(r'^hotels-in-(?P<city_slug>[\w-]+)/(?P<hotel_slug>[\w-]+)-(?P<locality_slug>[\w-]+)-(?P<hashed_hotel_id>\w+)/$',
        hotel_detail.HotelDetail.as_view(), name='hotel-details-new-deprecated'),
    # ---------------------------------
    url(r"^booking/(?P<order_id>[\w-]+)/confirmation/?$",
        order.Order.as_view(), name='order'),
    url(r"^iesupport/?$", iesupport.Iesupport.as_view(), name='iesupport'),
    url(r"^joinus/?$", join.Join.as_view(), name='joinus'),
    url(r"^offers/?$", offers.Offers.as_view(), name='offers'),
    url(r"^policy/?$", policy.Policy.as_view(), name='policy'),
    url(r"^search/?$", Search.as_view(), name='search-hotels'),
    url(r'^signup/?$', Referral.as_view(), name="signup"),
    url(r'^login/?$', Login.as_view(), name="login"),
    url(r'^logout/?$', Logout.as_view(), name='logout'),
    url(r'^register/?$', Registration.as_view(), name="register"),
    url(r'^user/forgot-password/?$', forgot_password.ForgotPassword.as_view(),
        name="forgot-password"),
    # url(r'^fot/$', FotLandingPage.as_view(), name="FotLandingPage"),
    # url(r'^fot/search/$', SearchAudit.as_view(), name="SearchAudit"),
    # url(r'^fot/signup/$', FotRegistration.as_view(), name="FotRegistration"),
    # url(r'^fot/login/$', FotLogin.as_view(), name="FotLogin"),
    # url(r'^fot/logout/$', FotLogout.as_view(), name="FotLogout"),
    url(r"^user/reset-password/(?P<slug>[-@!=&\w]+)/?$", reset_password.ResetPassword.as_view(),
        name='reset-password'),
    url(r"^terms/?$", terms.Terms.as_view(), name='terms'),
    url(r"^terms-of-rewards/?$", rewards_terms.RewardsTerms.as_view(), name='terms'),

    url(r'^user/booking-history/?$',
        login_required(
            bookings.Bookings.as_view(),
            redirect_field_name="next"),
        name="booking_history"),
    url(r"^user/settings/?$",
        login_required(
            settings.Settings.as_view(),
            redirect_field_name="next"),
        name='settings'),
    url(r"^voucher/?$", voucher.Voucher.as_view(), name='voucher'),
    url(r"^transaction/?$", transaction.Transaction.as_view(), name='transaction'),
    url(r"^redirector/?$", redirector.Redirector.as_view(), name='redirector'),
    url(r"^booking/payment/?$",
        payment_redirect_view.PaymentRedirectView.as_view(),
        name='payment'),
    url(r'^pages/', include('apps.pages.urls')),
]
