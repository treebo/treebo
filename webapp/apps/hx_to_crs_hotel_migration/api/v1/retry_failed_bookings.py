import logging

from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from apps.bookings.api.v3.retry_failed_bookings import RetryFailedBookings
from apps.bookings.models import AsyncJobStatus
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)
BOOKING_FAILED_DURING_HOTEL_MIGRATION_ERR_MSG = settings.BOOKING_FAILED_DURING_HOTEL_MIGRATION_ERR_MSG


class RetryFailedBookingsDuringHotelMigration(TreeboAPIView):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(RetryFailedBookingsDuringHotelMigration, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        """
        Retry failed bookings during HX to CRS hotel migration
        :param request:
        :return:
        """
        async_job_ids = AsyncJobStatus.objects\
            .filter(message__contains=BOOKING_FAILED_DURING_HOTEL_MIGRATION_ERR_MSG)\
            .values_list('job_id', flat=True)

        job_ids = ','.join(async_job_id for async_job_id in async_job_ids)

        return RetryFailedBookings.retry(job_ids)

