from django.http.response import HttpResponse
from django.shortcuts import render

# Create your views here.
from apps.posts.models import QAMapping
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Categories
from dbcommon.models.landmark import SearchLandmark
from dbcommon.models.location import Locality, City
from operator import attrgetter
from dbcommon.models.facilities import Facility
import json


def get_page_choices(request):
    page_type = request.GET['page_type']
    city = request.GET['city']
    result = []
    city_repository = RepositoriesFactory.get_city_repository()
    if page_type == QAMapping.CITY:
        cities = [
            city.slug for city in city_repository.filter_cities(
                status=1, id=int(city))]
        cities = sorted(cities)
        result = [','.join(city) for city in cities]
    elif page_type == QAMapping.LANDMARK:
        search_landmark_repository = RepositoriesFactory.get_landmark_repository()
        search_landmark_items = search_landmark_repository.get_all_active_landmarks_for_city(
            city_id=int(city))
        search_landmark_items = sorted(search_landmark_items, key=lambda search_landmark: search_landmark.seo_url_name)

        result = [
            ','.join(
                [
                    item.free_text_url if item.free_text_url else item.seo_url_name,
                    item.city.slug]) for item in search_landmark_items]
    elif page_type == QAMapping.LOCALITY:
        result = [','.join([item['name'], item['city__slug']
                            ]) for item in
                  Locality.objects.filter(city__id=int(city)).order_by('name').
                  values('id', 'name', 'city__slug')]
    elif page_type == QAMapping.CATEGORY:
        cities_slug = sorted(
            [city.slug for city in city_repository.filter_cities(status=1, id=int(city))])
        cities = [{'slug': city_slug} for city_slug in cities_slug]
        categories = Categories.objects.filter(
            status=1).values(
            'id', 'name').order_by('name')
        result = [','.join([x['name'], y['slug']])
                  for x in categories for y in cities]
    elif page_type == QAMapping.HD:
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotels_for_city = hotel_repository.get_enabled_hotels_for_city(
            city_id=int(city))
        sorted_hotels_for_city = sorted(
            hotels_for_city, key=attrgetter('name'))
        result = [','.join([item.name, item.city.slug])
                  for item in sorted_hotels_for_city]
    elif page_type == QAMapping.CITY_AMENITY:
        cities = City.objects.filter(status=1, id=int(city)).values('slug').order_by('slug')
        amenities = Facility.objects.filter(to_be_shown=True).values('id', 'name').order_by('name')
        result = [','.join([x['name'], y['slug']]) for x in amenities for y in cities]
    elif page_type == QAMapping.LOCALITY_CATEGORY:
        localities = Locality.objects.filter(city__id=int(city)).order_by('name').values('id', 'name')
        categories = Categories.objects.filter(enable_for_seo=True).values('id', 'name').order_by('name')
        result = [','.join([x['name'], y['name']]) for x in categories for y in localities]
    elif page_type == QAMapping.LANDMARK_CATEGORY:
        landmark = SearchLandmark.objects.filter(status=1, city__id=int(city)).order_by('seo_url_name'). \
            values('id', 'free_text_url', 'seo_url_name')
        categories = Categories.objects.filter(enable_for_seo=True).values('id', 'name').order_by('name')
        result = [','.join([x['name'], y['free_text_url'] if y['free_text_url'] else y['seo_url_name']])
                  for x in categories for y in landmark]
    json_data = json.dumps(result)
    return HttpResponse(json_data)



