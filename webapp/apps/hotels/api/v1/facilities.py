from dbcommon.models.facilities import Facility
from base.views.api import TreeboAPIView
from rest_framework.response import Response
import logging
from django.core.cache import cache
from django.utils.text import slugify
from apps.seo.pages.pages import PAGES
from dbcommon.models.location import City
from dbcommon.models.hotel import  Hotel
from dbcommon.models.hotel import Categories
from django.db.models import Q
from dbcommon.models.landmark import SearchLandmark
from dbcommon.models.location import Locality
from apps.hotels.service.hotel_search_service import HotelSearchService

logger = logging.getLogger(__name__)

class AllFacilitiesAPI(TreeboAPIView):

    CACHE_KEY = 'seo_facilities_{0}'

    @staticmethod
    def invalidate_cache():
        cache.delete_pattern(AllFacilitiesAPI.CACHE_KEY.format('*'))

    @staticmethod
    def get_cache_key(page, query):
        # if no query params are passed then page and query field will be None
        # and is going to be slugified to 'none' in the key.
        return "seo_facilities_" + slugify(page) + "_" + slugify(query)

    def get(self, request, *args, **kwargs):

        page = request.GET.get('page')
        query = request.GET.get('q')

        data = cache.get(AllFacilitiesAPI.get_cache_key(page, query))
        if data:
            return Response(data)

        hotel_ids = []
        if page == PAGES.CITY:
            hotel_ids = self.get_facility_hotel_ids_for_city_page(query)
        elif page == PAGES.HD:
            hotel_ids = self.get_facility_hotel_ids_for_hd_page(query)
        elif page == PAGES.CATEGORY:
            hotel_ids = self.get_facility_hotel_ids_for_category_page(query)
        elif page == PAGES.LANDMARK:
            hotel_ids = self.get_facility_hotel_ids_for_landmark_page(query)
        elif page == PAGES.LOCALITY:
            hotel_ids = self.get_facility_hotel_ids_for_locality_page(query)
        elif page == PAGES.CITY_AMENITY:
            hotel_ids = self.get_facility_hotel_ids_for_city_amenity_page(query)
        elif page == PAGES.LOCALITY_CATEGORY:
            hotel_ids = self.get_facility_hotel_ids_for_locality_category_page(query)
        elif page == PAGES.LANDMARK_CATEGORY:
            hotel_ids = self.get_facility_hotel_ids_for_landmark_category_page(query)

        if page is not None and query is not None:
            facilities = Facility.objects.filter(hotels__id__in=hotel_ids, css_class__isnull=False).distinct()
        else:
            facilities = Facility.objects.filter(css_class__isnull=False)

        facilities_list = []
        for facility in facilities:
                facilities_list.append(
                    {
                        "id": facility.id,
                        "name": facility.name
                    }
                )

        cache.set(AllFacilitiesAPI.get_cache_key(page, query), facilities_list, timeout=None)

        return Response(facilities_list)


    def get_facility_hotel_ids_for_landmark_category_page(self, query):
        slugified_category_name = str(query.split('-near-')[0])
        category_name = ' '.join(slugified_category_name.split('-')).title()
        slugified_landmark_name = query.split('-near-')[1]
        query_landmark_name = ' '.join(slugified_landmark_name.split('-'))
        landmark = SearchLandmark.objects.filter(free_text_url__iexact=query_landmark_name,
                                                 status=1).first()
        if landmark is None:
            city_name = query.split('-')[-1]
            landmark_name = ' '.join(str(query.split('-near-')[1]).split('-')[0:-1])
            landmark = SearchLandmark.objects.filter(Q(seo_url_name__iexact=landmark_name) & Q(status=1) &
                                                     Q(Q(city__name__iexact=city_name) | Q(
                                                         city__slug__iexact=city_name))).first()
        city = landmark.city
        hotel_ids = AllFacilitiesAPI.build_facilities_with_area_radius(city,
                                                                       landmark.latitude,
                                                                       landmark.longitude,
                                                                       landmark.distance_cap,
                                                                       category_name=category_name)
        return hotel_ids

    def get_facility_hotel_ids_for_locality_category_page(self, query):
        slugified_category_name = str(query.split('-in-')[0])
        category_name = ' '.join(slugified_category_name.split('-')).title()
        city_name = query.split('-')[-1]
        locality_name = ' '.join(str(query.split('-in-')[1]).split('-')[0:-1])
        locality = Locality.objects.filter(Q(name__iexact=locality_name) & Q(Q(city__name__iexact=city_name) |
                                                                             Q(
                                                                                 city__slug__iexact=city_name))).first()
        city = locality.city
        hotel_ids = AllFacilitiesAPI.build_facilities_with_area_radius(city,
                                                                       locality.latitude,
                                                                       locality.longitude,
                                                                       locality.distance_cap,
                                                                       category_name=category_name)
        return hotel_ids

    def get_facility_hotel_ids_for_city_amenity_page(self, query):
        city_name = query.split('-')[-1].strip('-')
        amenity_name = ' '.join(query.split('-')[:-1]).strip()
        amenity = Facility.objects.filter(name__iexact=amenity_name).first()
        city = City.objects.filter(
            Q(name__iexact=city_name, status=1) | Q(slug__iexact=city_name, status=1)).first()
        hotel_ids = AllFacilitiesAPI.build_facilities_using_area_name(city, amenity_name=amenity.name)
        return hotel_ids

    def get_facility_hotel_ids_for_locality_page(self, query):
        city_name = query.split('-')[-1]
        locality_name = ' '.join(query.split('-')[:-1])
        locality = Locality.objects.filter(Q(name__iexact=locality_name) & Q(Q(city__name__iexact=city_name) |
                                                                             Q(
                                                                                 city__slug__iexact=city_name))).first()
        city = locality.city
        hotel_ids = AllFacilitiesAPI.build_facilities_with_area_radius(city,
                                                                       locality.latitude,
                                                                       locality.longitude,
                                                                       locality.distance_cap)
        return hotel_ids

    def get_facility_hotel_ids_for_landmark_page(self, query):
        landmark = SearchLandmark.objects.filter(free_text_url__iexact=' '.join(query.split('-')),
                                                 status=1).first()
        if landmark is None:
            city_name = query.split('-')[-1]
            landmark_name = ' '.join(query.split('-')[:-1])
            landmark = SearchLandmark.objects.filter(Q(seo_url_name__iexact=landmark_name) & Q(status=1) &
                                                     Q(Q(city__name__iexact=city_name) | Q(
                                                         city__slug__iexact=city_name))).first()
        city = landmark.city
        hotel_ids = AllFacilitiesAPI.build_facilities_with_area_radius(city,
                                                                       landmark.latitude,
                                                                       landmark.longitude,
                                                                       landmark.distance_cap)
        return hotel_ids

    def get_facility_hotel_ids_for_category_page(self, query):
        city_name = query.split('-')[-1].strip('-')
        category_name = ' '.join(query.split('-')[:-1]).strip()
        category = Categories.objects.filter(status=1, name__iexact=category_name).first()
        city = City.objects.filter(
            Q(name__iexact=city_name, status=1) | Q(slug__iexact=city_name, status=1)).first()
        hotel_ids = AllFacilitiesAPI.build_facilities_using_area_name(city, category.name)
        return hotel_ids

    def get_facility_hotel_ids_for_hd_page(self, query):
        hotel = Hotel.all_hotel_objects.filter(id=int(query)).first()
        hotel_ids = [hotel.id]
        return hotel_ids

    def get_facility_hotel_ids_for_city_page(self, query):
        city = City.objects.get_by_slug(query)
        hotel_ids = AllFacilitiesAPI.build_facilities_using_area_name(city)
        return hotel_ids

    @staticmethod
    def build_facilities_using_area_name(city, category_name=None, amenity_name=None):
        search_service = HotelSearchService(city,
                                            category_name=category_name,
                                            amenity_name=amenity_name)
        hotels = search_service.search()
        hotel_ids = [int(hotel.id) for hotel in hotels]
        return hotel_ids


    @staticmethod
    def build_facilities_with_area_radius(city, latitude, longitude, distance_cap,
                                          category_name=None, amenity_name=None):
        search_service = HotelSearchService(city,
                                            latitude=latitude,
                                            longitude=longitude,
                                            distance_cap=distance_cap,
                                            category_name=category_name,
                                            amenity_name=amenity_name)
        hotels = search_service.search()
        hotel_ids = [int(hotel.id) for hotel in hotels]
        return hotel_ids
