# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discounts', '0017_hotelcouponmapforautoapply'),
    ]

    operations = [
        migrations.CreateModel(
            name='HotelCouponMapForAutoApplyUploadLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('status', models.CharField(max_length=20)),
                ('error_message', models.CharField(default=None, max_length=200)),
                ('update_attempted', models.CharField(max_length=100)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
