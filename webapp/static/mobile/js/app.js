import "expose?$!expose?jQuery!jquery";
const lazy = require('./libs/jquery.lazyload.js');
import $ from 'jquery';
// import cookies from 'js-cookie';
import history from 'html5-history-api';
// import { _ } from 'underscore';
// const auth = require('./auth');
$('.lazy').lazyload({
  effect: 'fadeIn',
  threshold: 300,
});

const modals = [];
const body = $('body');
const $window = $(window);
const location = history.location || window.location;


if ((location.hash).indexOf("modal") > -1) {
    history.pushState('', document.title, location.pathname + location.search);
}

body.on('click', 'input:text, input:password', function cb() {
  const elementOffset = $(this).offset().top;
  $('#pageContainer').animate({ scrollTop: elementOffset }, 500);
});

// Reload page if back-forward cached
$window.bind('pageshow', (event) => {
  if (event.originalEvent.persisted) {
    window.location.reload();
  }
});

// Show loader when navigating out of a page
$window.bind('beforeunload', () => {
  const href = $(document.activeElement).attr('href') || '';
  if (!href.match(/^(mailto:|tel:|whatsapp:)/)) {
    $('#commonLoader').removeClass('hide');
  }
});

body.on('click', '[data-toggle="modal"]', function cb() {
  const targetSelector = this.getAttribute('data-target');
  const $target = $(targetSelector);

  modals.push($target);
  $target.removeClass('hide');
  body.addClass('modal--open');

  history.pushState({ target: targetSelector }, null,
    `${location.href.replace(location.hash, '')}#modal${modals.length}`
  );
});

$window.on('popstate', () => {
  if (modals.length) {
    modals.pop().addClass('hide');
    if (modals.length === 0) {
      body.removeClass('modal--open');
    }
  }
});

function closeModal() {
  if (location.hash) {
    history.back();
  }
}

body.on('click', '.js-modal__close', closeModal);
body.on('modal:close', closeModal);

$('#hmIcon').on('click', (ev) => {
  ev.stopPropagation();
  ev.preventDefault();

  $('.page').toggleClass('page--open');
  body.toggleClass('modal--open');
});

const WebFont = require('webfontloader');

WebFont.load({
    google: {
        families: ['Roboto:400,500', 'Open Sans:800']
    }
});
