import unittest

from common.utilities import number_utils


class TestBasic(unittest.TestCase):
    """
    Use basic unit tests for API testing.
    write all kinds of positive and negative assertions to test out the API
    """

    def test_hotels_view_is_valid_number(self):
        self.assertTrue(number_utils.is_valid_number(5))

    def test_hotels_view_not_valid_number(self):
        self.assertFalse(number_utils.is_valid_number("a"))
