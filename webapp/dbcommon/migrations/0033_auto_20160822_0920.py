# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0032_auto_20160804_1800'),
    ]

    operations = [
        migrations.AddField(
            model_name='hotel',
            name='CONSUMER_KEY',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='hotel',
            name='CONSUMER_SECRET',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
