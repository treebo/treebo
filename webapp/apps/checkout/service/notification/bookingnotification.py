import logging
from datetime import datetime

from django.conf import settings

from apps.checkout.constants import BOOKING_CONFIRMATION_MESSAGE_TEMPLATE
from apps.checkout.service.notification.email.confirmationemail import ConfirmationEmailService
from apps.checkout.service.notification.email.specialpreference import SpecialPrefsEmailService
from apps.checkout.service.notification.sms.confirmationsms import BookingPartPaySmsService
from apps.bookings.models import Booking
from data_services.respositories_factory import RepositoriesFactory
from services.restclient.communication_client import CommunicationClient

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)

hotel_repository = RepositoriesFactory.get_hotel_repository()


class BookingNotificationService:

    def __init__(self):
        pass

    def send_booking_confirmation_message(self, booking_request):
        """
        :param apps.checkout.models.BookingRequest booking_request:
        """
        try:
            logger.info("Sending sms for booking request %s", booking_request.order_id)
            sms_service = BookingPartPaySmsService()
            communication_client = CommunicationClient()
            message_payload = sms_service.get_sms_payload(booking_request)

            message_template = self._get_confirmation_message_template(booking_request)
            communication_client.send_communication(identifier=message_template,
                                                    context_data=message_payload,
                                                    receivers=[message_payload['guest_mobile']])

        except Exception as exc:
            logger.exception("Unable to send booking confirmation sms - {0}".format(exc))

    @staticmethod
    def _get_confirmation_message_template(booking_request):
        pay_at_hotel = int(booking_request.pay_at_hotel) and booking_request.payment_mode \
                       == Booking.NOT_PAID

        if pay_at_hotel:
            template = BOOKING_CONFIRMATION_MESSAGE_TEMPLATE['PAY_AT_HOTEL_MESSAGE_TEMPLATE']

        elif booking_request.wallet_applied and int(booking_request.wallet_deduction) > 0:
            template = BOOKING_CONFIRMATION_MESSAGE_TEMPLATE['PREPAID_USING_POINTS_MESSAGE_TEMPLATE']

        elif booking_request.payment_mode == Booking.PAID:
            template = BOOKING_CONFIRMATION_MESSAGE_TEMPLATE['PREPAID_MESSAGE_TEMPLATE']
        else:
            message = "Template not found for booking - %s", booking_request.order_id
            raise Exception(message)

        logger.info('Message template for booking %s is %s', booking_request.order_id, template)
        return template


    @staticmethod
    def bookingConfirmationMail(booking_request, email, pdf_name, error):
        """
        booking Confirmation Mail
        :param apps.checkout.models.BookingRequest booking_request:
        :param email:
        :param pdf_name:
        :param error:
        :return:
        """
        try:
            hotel = hotel_repository.get_hotel_by_id(
                hotel_id=booking_request.hotel_id)
            #hotel = hotel_repository.filter_hotels(id=booking_request.hotel_id)[0]
            subject = "Booking Confirmation - " + \
                str(hotel.name) + " - " + booking_request.order_id
            email_service = ConfirmationEmailService(
                ConfirmationEmailService.NOTIFICATION_EMAIL, "html", [email], subject, None)

            email_service.collateEmailData(booking_request)
            email_service.createMessage()

            # TODO: py - fix the css bit for the booking voucher and uncomment the following attachment logic
            #if not error:
            #    email_service.addAttachments(
            #        common_constants.PDF_PATH, pdf_name)

            #email_service.addAttachments(common_constants.PDF_PATH, "GuaranteePolicy.pdf")
            email_service.sendMessage()

        except Exception as exc:
            logger.exception("BookingConfirmation Mail send failed")

    @staticmethod
    def __isNotDefault(checkInTime, checkOutTime):
        if len(checkInTime) > 0 and datetime.strptime(
            checkInTime, '%H%p').hour != 12:
            return True
        elif len(checkOutTime) > 0 and datetime.strptime(checkOutTime, '%H%p').hour != 11:
            return True
        else:
            return False

    @staticmethod
    def sendSpecialPrefEmail(booking_request):
        """
        send special preferences email filled by the guest
        :param apps.checkout.models.BookingRequest booking_request:
        :return:
        """
        try:
            if len(
                booking_request.comments) > 0 or BookingNotificationService.__isNotDefault(
                booking_request.checkin_time,
                booking_request.checkout_time):
                hotel = hotel_repository.get_hotel_by_id(
                    hotel_id=booking_request.hotel_id)
                #hotel = hotel_repository.filter_hotels(id=booking_request.hotel_id)[0]
                subject = settings.GUEST_PREF_SUBJECT % {
                    'guestname': booking_request.name,
                    'bookingid': booking_request.order_id,
                    'hotelname': hotel.name}
                email_service = SpecialPrefsEmailService(
                    ConfirmationEmailService.NOTIFICATION_EMAIL,
                    "html",
                    settings.GUEST_PREF_EMAIL_LIST,
                    subject,
                    None)
                email_service.collateEmailData(booking_request)
                email_service.createMessage()
                email_service.sendMessage()
        except Exception as e:
            logger.exception(
                "Special Preferences email send failed with exception")
