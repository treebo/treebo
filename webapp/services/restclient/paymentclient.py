import json
import logging

import requests
from django.core.urlresolvers import reverse

from apps.common.exceptions.custom_exception import PaymentGenerationException, PaymentUpdateException
from base.middlewares.request import get_domain

logger = logging.getLogger(__name__)


class PaymentClient:
    def __init__(self):
        pass

    def __get_payment_api(self, page_name, *args):
        return get_domain() + \
            reverse('payments_v1:{0}'.format(page_name), args=args)

    def __make_call(self, page_name, data, *args):
        return requests.post(
            self.__get_payment_api(
                page_name,
                *args),
            data=data,
            allow_redirects=True)

    def __make_get_call(self, page_name, data, *args):
        return requests.get(
            self.__get_payment_api(
                page_name,
                *args),
            params=data,
            allow_redirects=True)

    def generate_payment_details(self, amount, bid, hotel_id, payment_mode):
        """
        Creates a new payment in Hx
        :param payment_request:
        :return:
        """
        # update-payment_v1
        data = {
            "amount": amount,
            "bid": bid,
            "hotel_id": hotel_id,
            "payment_mode": payment_mode,
        }
        response = self.__make_call("generate-payment_v1", data)
        if not response.ok:
            logger.exception(
                " generate_payment_details exception from data %s ",
                json.dumps(data))
            raise PaymentGenerationException(
                'generate api exception', '', response.status_code)
        return response.json()['data']

    def update_payment_details(self, order_id, ps_order_id, pg_payment_id):
        data = {
            "order_id": order_id,
            "ps_order_id": ps_order_id,
            "pg_payment_id": pg_payment_id,
        }
        response = self.__make_call("update-payment_v1", data)
        if not response.ok:
            logger.exception(
                " update_payment_details exception from data %s ",
                json.dumps(data))
            raise PaymentUpdateException(
                'update api exception', response.status_code)
        return response.json()['data']

    def get_payment_details(self, ps_order_id):
        data = {
            "ps_order_id": ps_order_id,
        }
        response = self.__make_get_call(
            "payment-details_v1", data, ps_order_id)
        if not response.ok:
            logger.error(
                " payment-details exception from data %s ",
                json.dumps(data))
            raise PaymentUpdateException(
                'payment-details api exception',
                response.status_code)
        return response.json()['data']
