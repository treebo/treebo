import datetime
import logging
from apps.checkout.models import BookingRequest
from apps.intelligent_pah.models.intelligent_pah_config import Configuration
from apps.intelligent_pah.models.intelligent_pah_occupancy_range import Occupancy
from apps.intelligent_pah.models.intelligent_pah_categories import Category
from apps.intelligent_pah.models.intelligent_pah_auditing import Audit
from .occupancy_percentage import GetOccupancyPercentage

logger = logging.getLogger(__name__)


class ModelService():

    def get_occupancy_percentage(self, checkin_date, hotel_id):
        try:
            occ = GetOccupancyPercentage().get_occupancy_percentage_v2(
                hotel_id, checkin_date, checkin_date + datetime.timedelta(days=1))
            # we dont need the checkout date for now hence.. checkout date =
            # check in date + 1
            return occ
        except BaseException:
            return 50

    def categorize_via_date(self, booking_ts, check_in_ts):
        time_delta = check_in_ts - booking_ts
        date_category = Category.objects.filter(
            days_to_check_in=time_delta.days)
        max_days_to_check_in = self.get_max_days_to_check_in()
        if time_delta.days > max_days_to_check_in:
            return Category.objects.filter(
                days_to_check_in=max_days_to_check_in)
        time = self.seconds_to_time(time_delta)
        date_category = date_category.filter(
            start_time__lte=time, end_time__gt=time)
        return date_category

    def categorize_via_occupancy(self, current_occupancy, date_category):
        occupancy_category = Occupancy.objects.filter(
            start__lte=current_occupancy,
            end__gt=current_occupancy,
            category=date_category)
        return occupancy_category

    def is_pah_enabled(self, occupancy_category):
        result = occupancy_category
        if result.count() == 0:
            return True
        return result[0].value

    def audit(
            self,
            audit_enabled,
            check_in,
            bid,
            hotel_id,
            occupancy,
            is_pah_enabled):
        if audit_enabled:
            booking_request = BookingRequest.objects.filter(id=bid).first()
            audit_obj = Audit()
            audit_obj.check_in_date = check_in
            audit_obj.bid = booking_request
            logger.info("The hotel id is %d", hotel_id)
            audit_obj.hotel_id_id = hotel_id
            audit_obj.occupancy = occupancy
            audit_obj.is_pah_enabled = is_pah_enabled
            o = audit_obj.save()
            logger.info("Booking audited bid %s, hotel id %s", bid, hotel_id)
        else:
            logger.info("Auditing is set to False")

    def seconds_to_time(self, timedelta):
        seconds = timedelta.seconds
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        return datetime.time(h, m, s)

    def get_max_days_to_check_in(self):
        days_list = Category.objects.all().values_list('days_to_check_in', flat=True)
        return max(days_list)

    def get_app_configurations(self):
        return Configuration.objects.filter(id=1).first()
