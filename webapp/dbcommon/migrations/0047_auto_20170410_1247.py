# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0046_auto_20161110_0902'),
    ]

    operations = [
        migrations.CreateModel(
            name='SearchDistance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('distance', models.IntegerField()),
                ('hotel', models.ForeignKey(to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'search_distance',
                'verbose_name': 'Search Distance',
                'verbose_name_plural': 'Search Distance',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='SearchLandmark',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(max_length=2000, null=True, blank=True)),
                ('city', models.ForeignKey(to='dbcommon.City', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'search_landmark',
                'verbose_name': 'Search Landmark',
                'verbose_name_plural': 'Search Landmarks',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.AddField(
            model_name='searchdistance',
            name='landmark',
            field=models.ForeignKey(to='dbcommon.SearchLandmark', on_delete=models.DO_NOTHING),
        ),
    ]
