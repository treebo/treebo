# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contentstore',
            options={
                'verbose_name': 'Content Store',
                'verbose_name_plural': 'Content Stores'},
        ),
    ]
