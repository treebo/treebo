import logging

from django.conf import settings

from apps.third_party.service.trivago.price_adaptors import TrivagoPriceAdaptor
from apps.pricing.api.v5.hotel_prices import HotelRoomPricesSerializer, HotelRoomPrices

logger = logging.getLogger(__name__)


class RatePlanPriceAdaptor(TrivagoPriceAdaptor):
    def hit_and_fetch_prices_for_room(self, hotel_data_params, hotel):
        try:
            kwargs = {
                'hotel_id': hotel.id,
                'checkin': hotel_data_params['check_in_date'],
                'checkout': hotel_data_params['check_out_date'],
                'roomconfig': hotel_data_params['rooms_config'],
                'utm_source': settings.TRIVAGO_EXTERNAL_UTM_SOURCE if settings.ENABLE_TRIVAGO_AUTO_APPLY else None,
            }
            hotel_room_prices_obj = HotelRoomPrices()
            serializer_obj = HotelRoomPricesSerializer(data=kwargs)
            serializer_obj.is_valid(raise_exception=True)
            validated_data = serializer_obj.validated_data
            pricing_response_data = hotel_room_prices_obj.process_validated_data(
                validated_data, kwargs)
            pricing_response_data = pricing_response_data.get(
                'total_price', {})
        except BaseException:
            logger.exception('Fetching prices failed for Trivago availability')
            pricing_response_data = {}
        return pricing_response_data

    def calculate_prices_as_per_trivago(self, room_details, hotel_data_params):
        try:
            number_of_rooms = hotel_data_params.get('number_of_rooms', '1')
            number_of_days = abs(
                (hotel_data_params.get('check_in_date') -
                 hotel_data_params.get('check_out_date')).days)

            PRICE_FACTOR_FOR_TRIVAGO = float(
                number_of_rooms * int(number_of_days))

            # rate_plans = sorted(room_details.get('rate_plans'), key=lambda rp: rp['price']['sell_price'])
            # cheapest_rate_plan = rate_plans[0]
            # price = cheapest_rate_plan.get('price')

            final_price = round(float(room_details.get('sell_price', 0)) / PRICE_FACTOR_FOR_TRIVAGO, 2)
            tax_price = round(float(room_details.get('tax', 0)) / PRICE_FACTOR_FOR_TRIVAGO, 2)
            pre_tax_price = round(float(room_details.get('pretax_price', 0)) / PRICE_FACTOR_FOR_TRIVAGO, 2)
        except:
            logger.exception('Pricing failed for %s, %s' % (str(room_details), str(hotel_data_params)))
            final_price = tax_price = pre_tax_price = 0

        return final_price, tax_price, pre_tax_price
