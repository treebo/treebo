'use strict';

import $ from 'jquery';
import {Observable} from "rx";
import {_} from 'underscore';
import "slick-carousel";
import BaseView from './treebolibs/baseview';
import searchWidget from './widgets/search';
import queryString from 'query-string';

import React from 'react';
import {render} from 'react-dom';
import { createStore, combineReducers } from 'redux';
import PriceDetail from './widgets/pricedetails';
import '../css/searchresults.less';

let treejax = require('./treebolibs/treejax');

$(window).on('click', e => {
    const target = $(e.target);
    if (target.closest('.results-filter__item--sort').length ==0){
        store.dispatch({type: "CLOSE_DD_SORT"});
    }
    if (target.closest('.results-filter__item--flt').length ==0){
        store.dispatch({type: "CLOSE_DD_FLT"});
    }

});

$(window).bind('beforeunload', () => {
  const href = $(document.activeElement).attr('href') || '';
  if (!href.match(/^(mailto:|tel:|whatsapp:)/)) {
    $('#commonLoader').removeClass('hide');
  }
});


if($("#marketingBanner").length){
    var initialIndex = +$("#marketingBanner").css("order");
}

$(".results__row").on('click',"#price-detail-search",e => {
    const $room = $(e.target).closest('.results__row');
    const data = $room.find('.js-pricedetailsdata').val();
    const stringPriceDetail = JSON.parse(data);
    const priceDetailsData = {nights:stringPriceDetail};

    $("#price-detail").removeClass('hide');
    render(<PriceDetail data={priceDetailsData}/>, $('#price-detail')[0]);
});

let SearchResultView = Object.create(BaseView);
let store = {};
let methods = {
    initResults : ()=>{


        const $slickElement = $(".carousel")
        .on('init', slick => {
            $('.carousel').fadeIn(3000);
        })
        .slick({
            fade: true,
            focusOnSelect: true,
            dots: true,
            lazyLoad: 'ondemand',
            slidesToShow: 1,
            slidesToScroll: 1,
            customPaging(slider, i) {
                // this example would render "tabs" with titles
                return `${i+1} of ${slider.$slides.length}` ;
            }
        });

        const parsed = queryString.parse(location.search);
    },

    initFilters : (e)=>{
        let filterData = JSON.parse($("#hiddenFilterData").val());
        const parsed = queryString.parse(location.search);

        let {budgetfilter, categoryfilter, amenityfilter, localityfilter} = parsed;
        let localityArr = [];
        if(localityfilter){
            localityArr = localityfilter.split(",");
        }

        let locality = filterData.filters.locality.map((v, i) => ({
            key : v,
            value : v,
            selected:localityArr.indexOf(v) >= 0
        }));
        let categoryArr =[];
        if(categoryfilter){
            categoryArr = categoryfilter.split(",");
        }

        let category = filterData.filters.category.map((v, i) => {
            let isLocalitySelected= false;

            return {
                key : v,
                value : v,
                selected: categoryArr.indexOf(v) >= 0
            }
        });
        let amenityArr = [];
        if(amenityfilter){
            amenityArr = amenityfilter.split(",");
        }

        let amenity = filterData.filters.amenity.map((v, i) => ({
            key : v,
            value : v,
            selected: amenityArr.indexOf(v) >= 0
        }));


        let budget = filterData.filters.budget;


        if(budgetfilter){
            let budgetfilterArr = budgetfilter.split(",");
            budget = filterData.filters.budget.map((v, i) => {
                if(budgetfilterArr.indexOf(v.key.join("-"))>= 0){
                    v.selected = true;
                }
                return v
            })

        }

        filterData = Object.assign({}, filterData,{
            filters : Object.assign({}, filterData.filters, {
                    locality
                },{budget}, {category},{amenity})
            }

        )



        let initialState = filterData || {
            sortddVisibility: false,
            filterddVisibility: false,
            sortArr : [],
            sortOptions : {

            },
            filterArr : [],
            filters: {
            },
            showOnlyAvailable : false,
            showOnlyAvailableFilter : true //$(".hotelAvailibility-False").length > 0
        };



        const filtersReducer = (state = initialState, action) => {
            let rooms;
            let udaptedRoom;
            let index;
            switch (action.type) {
                case 'TOGGLE_DD_SRT':
                    return Object.assign({}, state, {
                        sortddVisibility : !state.sortddVisibility
                    }, {
                        filterddVisibility : false
                    });
                case 'CLOSE_DD_FLT':
                    return Object.assign({}, state, {
                        filterddVisibility : false
                    });
                case 'CLOSE_DD_SORT':
                    return Object.assign({}, state, {
                        sortddVisibility : false
                    });
                case 'TOGGLE_DD_FLT':
                    return Object.assign({}, state, {
                        filterddVisibility : !state.filterddVisibility
                    }, {
                        sortddVisibility : false
                    });
                case 'TOGGLE_SHOWAVAILABLE':
                    return Object.assign({}, state, {
                        showOnlyAvailable : !state.showOnlyAvailable
                    }, {
                        filterddVisibility : false,
                        sortddVisibility : false
                    });
                case 'CLEAR_FILTERS':
                    let updatedObj = _.mapObject(state.filters, (value,key)=>{
                        return value.map((v,i)=>{
                            return Object.assign({}, v, {
                                selected : false
                            });
                        })
                    });

                    return Object.assign({}, state, {filters: updatedObj});
                case 'APPLY_FILTER':
                    return Object.assign({}, state, {
                        filterddVisibility : false
                    });
                case 'TOGGLE_FILTER':
                    let [fltkey, id] = action.id.split("-");
                    let fltOptions = state.filters[fltkey];
                    id = +id;
                    let updateFlt = Object.assign({}, fltOptions[id], {
                        selected: !fltOptions[id].selected
                    });


                    let updateFltObj = [
                        ...fltOptions.slice(0, id),
                        updateFlt,
                        ...fltOptions.slice(id + 1)
                    ];
                    let obj = {};
                    obj[fltkey] = updateFltObj;
                    let updatedFiltersObj = Object.assign({}, state.filters, obj);

                    return Object.assign({}, state, {filters: updatedFiltersObj});

                case 'UPDATE_OPTION':
                    let updatedOption =  {};
                    updatedOption = _.mapObject(state.sortOptions, (v, k) => {
                        const toReturn = v;
                            toReturn.selected = (k==action.id);

                        return toReturn;
                    })
                    return Object.assign({}, state, updatedOption, {sortddVisibility: false});
                default:
                    return state;
            }
        };


        store = createStore(filtersReducer);

        const FiletersApp = React.createClass({
        	render() {
                let {state} = this.props;
                return (
                    <div className="results-filter flex-row flex--align-center">
                        <SortApp options={state.sortArr} ddVisbility={state.sortddVisibility}/>
                        <ResultsFilterApp options={state.filterArr} ddVisbility={state.filterddVisibility}/>
                        <div className="results-filter__item results-filter__item--available flex-column flex--justify-center">
                                <label className="checkbox__label"><input className="checkbox__input checkbox__input-field" checked={state.showOnlyAvailable} type="checkbox" name="showOnlyAvailable" onChange= {(e) => {
                                    store.dispatch({type: "TOGGLE_SHOWAVAILABLE"});
                                }}/>Show Only Available</label>
                            </div>

                    </div>
                )
            }
        });

        const ResultsFilterApp = React.createClass({
        	render() {
                const {options, ddVisbility} = this.props;
                let state = store.getState();
                let filterCount  = methods.getFilterCount();
                let ddClass= `dd-arrow icon-down-arrow dd-arrow--${state.filterddVisibility ? 'open' : 'close'}`

                return (
                    <div className="results-filter__item results-filter__item--flt flex-column pos-rel flex--justify-center">
                        <div className="filters__title" onClick={() => {
                                store.dispatch({type: "TOGGLE_DD_FLT"});
                            }}>
                            <span>
                                <span className="filters__title--text">Filters</span>  <br />
                                <span className="filters__title--fltcount">{filterCount != 0? `${filterCount} Filters Selected` : ''}</span>
                            </span>
                            <i className={ddClass}></i>

                        </div>
                        <div className={`flex-column filters__dd box-shadow pos-abs ${ddVisbility ? 'filters__dd--show' : ''}`}>
                            <div className="flex-row filters__container">
                                {
                                    options.map((option,i) =>{
                                        if(state.filters[option].length){
                                            return (
                                                <FilterTypeApp key={i} keyid={option} option={option}/>
                                            )
                                        }else {return ;}
                                    })
                                }
                            </div>
                            <div className="filters__actions flex-row flex--align-center">
                                <div className="anchor filters__actions__clear" onClick={(e) => {
                                        store.dispatch({type: "CLEAR_FILTERS"});
                                    }}>Clear All Filters</div>
                                <button type="button" className="btn filters__actions__apply" onClick={() => {
                                        store.dispatch({type: "APPLY_FILTER"});
                                    }}name="button">Done</button>
                            </div>
                        </div>
                    </div>
                )
            }
        });

        const FilterTypeApp = React.createClass({
        	render() {
                const {option, keyid} = this.props;
                let state = store.getState();
                let filterOptions = state.filters[option];

        		return (
                    <div className={`filters__item filters__item--${option}`}>
                        <div className="filters__item__title flex-row">{option} { (()=>{
                                if(option == "budget"){
                                    return  <span className="filter__inr"> (in <i className="icon-rupee"></i>.)</span>;
                                }
                        })()
                    }</div>
                        <ul className="filters__item__options">
                            {
                                filterOptions.map((fltOP,i) =>{
                                    return (
                                        <FilterOptionApp key={i} keyid={i} fltType={option} option={fltOP}/>
                                    )
                                })
                            }
                        </ul>
                    </div>
        		);
            }
        });

        const FilterOptionApp = React.createClass({
        	render() {
                const {option, keyid, fltType} = this.props;
                let state = store.getState();


        		return (
                    <li className="checkbox flt-item">
                        <label className="checkbox__label"><input className="checkbox__input" checked={option.selected} type="checkbox" name={option.key} value={option.key} onChange= {(e) => {
                            store.dispatch({type: "TOGGLE_FILTER", id: `${fltType}-${keyid}`});
                        }}/>{option.value}</label>
                    </li>
        		);
            }
        });



        const SortApp = React.createClass({
        	render() {
                const {options, ddVisbility} = this.props;
                let state = store.getState();
                let seletedOption = state.sortArr.filter((v, i) => state.sortOptions[v]['selected']);
                let ddClass= `dd-arrow icon-down-arrow dd-arrow--${state.sortddVisibility ? 'open' : 'close'}`

                return (
                    <div className="results-filter__item results-filter__item--sort flex-column pos-rel flex--justify-center">
                        <div className="sort__title uc" onClick={() => {
                                store.dispatch({type: "TOGGLE_DD_SRT"});
                            }}><span className="sort__text">Sort By:</span><span>{seletedOption}</span><i className= {ddClass}></i></div>
                        <div className={`flex-column box-shadow sort__dd pos-abs ${ddVisbility ? 'sort__dd--show' : ''}`}>

                            <ul className="sort__options dd">
                            {
                                options.map((option,i) =>{
                                    return (
                                        <SortOption key={i} keyid={option} option = {option}/>
                                    )
                                })
                            }
                            </ul>
                        </div>
                    </div>
                )
            }
        });
        const SortOption = React.createClass({
        	render() {
                const {option, keyid} = this.props;
                let state = store.getState();
                let sortOp = state.sortOptions[option];
                let sClass  = sortOp.selected ? 'dd__item--selected' : '' ;

        		return (
                    <li className={`dd__item ${sClass}`} onClick= { ()=> {
                            store.dispatch({
                                type: "UPDATE_OPTION",
                                id: option
                            })
                        }}>
                        {sortOp.value}
                    </li>
        		);
            }
        });

        const renderFilters = () => {
          render(
            <FiletersApp state={store.getState()}/>,document.getElementById('resultFilters')
          )
        };

        const renderDom = (firstRender) => {
            let state = store.getState();
            let sortby = _.findKey(state.sortOptions, (v, i) => v.selected)
            methods.sortBy(sortby);
            methods.applyFilters(state, firstRender);
        };


        store.subscribe(renderFilters);
        store.subscribe(renderDom);
        renderFilters();
        renderDom(true);
    },

    applyFilters(state, firstRender){
        let filters = state.filters;
        const $mbanner = $("#marketingBanner");
        const $noFilterResult = $("#noFilterResult");

        $("#filteredResultCount").text($('.results__row:visible').length);


        if(methods.getFilterCount() == 0 && !state.showOnlyAvailable){
            $('.results__row').show();
            $noFilterResult.addClass('hide');
            if($mbanner.length){
                $mbanner.show().css("order", initialIndex);
            }
            return;
        }
        if (firstRender && methods.getFilterCount() != 0) {
            $('.results__row').show();
            $noFilterResult.addClass('hide');
            if($mbanner.length){
                $mbanner.show();
            }
            firstRender = false;
        } else if (firstRender) {
            return;
        }

        let budgetFilter = filters.budget.filter((v, i) => v.selected);

        let budgetFilterStr = budgetFilter.map(v => v.key.join("-")).join(",");

        let localities = filters.locality.filter((v, i) => v.selected);

        let localityFilterStr = localities.map(v => v.key).join(",");

        let categories = filters.category.filter((v, i) => v.selected);

        let categoryFilterStr = categories.map(v => v.key).join(",");

        let amenities = filters.amenity.filter((v, i) => v.selected);

        let amenityFilterStr = amenities.map(v => v.key).join(",");

        const parsed = queryString.parse(location.search);
        const newParams = _.extend({},parsed,{budgetfilter:budgetFilterStr,localityfilter:localityFilterStr,categoryfilter:categoryFilterStr, amenityfilter: amenityFilterStr});

        history.pushState(null, null, `${location.pathname}?${$.param(newParams)}`);

        $('.results__row').each((i, v) => {
            $(v).hide();
            let thisLocal = $(v).data("locality");
            let hasLocality = _.findIndex(localities, v => thisLocal == v.value) >= 0;
            hasLocality = localities.length > 0 ? hasLocality : true;

            let thisCategory = $(v).data("category").split("||");
            let hascategory = _.findIndex(categories, v => thisCategory.indexOf(v.value) >= 0) >= 0;
            hascategory = categories.length > 0 ? hascategory : true;

            let hasbudget =  budgetFilter.length < 1;
            let price = +$(v).data("price");
            budgetFilter.forEach((val, i) => {
                let [minPrice, maxPrice] = val.key;
                hasbudget = hasbudget || (price >= minPrice && price <= maxPrice);
            });

            let thisAmenity = $(v).find(".js-hidden-amenities").val().split("||");

            let hasamenity = _.findIndex(amenities, v => thisAmenity.indexOf(v.value) >= 0) >= 0;
            hasamenity = amenities.length > 0 ? hasamenity : true;

            if(hasbudget && hasLocality && hascategory && hasamenity){
                if(state.showOnlyAvailable){
                    if($(v).hasClass("hotelAvailibility-True")){
                        $(v).show();
                    }
                } else {
                    $(v).show();
                }
            }

        });
        let visbleRowLength = $('.results__row:visible').length;
        if(visbleRowLength){
            if($mbanner.length){
                $("#marketingBanner").show();
                if(visbleRowLength <= 4){
                    $("#marketingBanner").css("order", 100);
                } else {
                    $("#marketingBanner").css("order", initialIndex);
                }

            }
            $noFilterResult.addClass('hide');
        } else {
            if($mbanner.length){
                $("#marketingBanner").hide();
            }
            $noFilterResult.removeClass('hide');
        }
        $("#filteredResultCount").text(visbleRowLength);
    },
    getFilterCount(){
        let state = store.getState();
        let filterCount  = [];
        _.each(state.filters, (val,key)=>{
            let selCount = val.filter((v,i)=>{
                console.log();
                return v.selected;
            }).length;
            filterCount.push(selCount);
        });
        filterCount = filterCount.reduce((p,n)=>{
            return +p + +n;
        },0);
        return filterCount;
    },
    sortByDistance(lat, lon) {
        this.setDistanceOnHotel(lat, lon);
        $(".search-results-container").html($('.hotel-result').sort((a, b) => {
            const val = parseFloat($(a).attr('distance')) - parseFloat($(b).attr('distance'));
            return -val;
        }));
    },
    sortBy(type) {
        const sortArr = $('.results__row').sort((a, b) => (+($(a).data(type)) > +($(b).data(type)) ? 1 : -1));

        const len = $('.results__row').length;
        sortArr.each((i, v) => {
            if($(v).hasClass('hotelAvailibility-False')){
                $(v).css("order", len+i);
            } else {
                $(v).css("order", i);
            }
        })
    },
    initSidewidgets(){
        const self = this;
        const p = treejax({
                url: '/api/v1/search/widgets/',
                type: 'POST',
                data: {
                    keyStr:"offer,searchwidget2,searchwidget3",
                    locality:rt.city,
                    csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val()
                }
            }).then((data, status, xhr) => {
                const results = data.results;
                render(<SideWidgets data={results}/>, document.getElementById('sideWidgets'));
                $(".js-deals-carousel").slick();
            },treeboError => {
                console.log(treeboError);
            });
    }

}

$.fn.inView = function(inViewType){
    const viewport = {};
    viewport.top = $(window).scrollTop();
    viewport.bottom = viewport.top + $(window).height();
    const bounds = {};
    bounds.top = this.offset().top;
    bounds.bottom = bounds.top + this.outerHeight();
    switch(inViewType){
      case 'bottomOnly':
        return ((bounds.bottom <= viewport.bottom) && (bounds.bottom >= viewport.top));
      case 'topOnly':
        return ((bounds.top <= viewport.bottom) && (bounds.top >= viewport.top));
      case 'both':
        return ((bounds.top >= viewport.top) && (bounds.bottom <= viewport.bottom));
      default:
        return ((bounds.top >= viewport.top) && (bounds.bottom <= viewport.bottom));
    }
};

SearchResultView.isTopInView = () =>{
    if(!$('.seo-bottom')) return;
    return $('.seo-bottom').inView('topOnly');
}

SearchResultView.isBottomInView = () =>{
    if(!$('.seo-bottom')) return;
    return $('.seo-bottom').inView('bottomOnly');
}

$(window).on("scroll", e => {
    const mainWrapperBottomEdge = $('.main-wrapper').offset().top + $('.main-wrapper').height();
    const sidebarBottomEdge = $('#sideWidgets').offset().top + $('#sideWidgets').height();

    if(($('.left-part').height() > ($('#sideWidgets').height()+100)) && $('#sideWidgets').height() != 0) {
        // debugger;
      if ($(window).scrollTop() > 230 && SearchResultView.isTopInView()==false && SearchResultView.isBottomInView()==false) {
        $('#sideWidgets').css({position: 'fixed', top: '20px', width:'340px'});
      } else if (SearchResultView.isTopInView()==true) {
        $('#sideWidgets').css({position: 'absolute', top: ($('.main-wrapper').height() +20 - ($('#sideWidgets').height()))});
      } else {
        $('#sideWidgets').css({position: 'absolute', top:'0px'});
      }
    }
});

let SideWidgets = React.createClass({
    render(){
        const widgets = [];
        console.log(" Props data ",this.props.data);
        for (let i=0; i < this.props.data.length; i++) {
            if(this.props.data[i].type=='image'){
                widgets.push(<ImageWidget key={i} data={this.props.data[i]} />);
            }else{
                widgets.push(<CarousalWidget key={i} data={this.props.data[i]} />);
            }

        };
        //widgets.push(<CarousalWidget data={[1,2,3]}/>)
        // console.log(widgets);
        return (
            <div>
                {widgets}
            </div>
        )
    }

});

// {(()=>{
//         return slides[i].tag ? slides[i].tag : ''
//     }
//
// })()

let CarousalWidget = React.createClass({
    render(){
        const items = [];

        const slides = this.props.data.items;
        for (let i=0; i < slides.length; i++) {
            items.push(
                <div key={i} className="pos-rel side-widget__carousel__item">
                    <a className="side-widget__carousel__link" href={slides[i].targetUrl||'' }>
                    <img className="side-widget__carousel__img" src={slides[i].url}/>
                    <div className="pos-abs side-widget__carousel__item__text">
                        <div className="side-widget__carousel__item__title">{slides[i].tag ? slides[i].tag: ''}</div>
                        {
                            slides[i].callToActionText ? <div className="side-widget__carousel__item__action btn btn--secondary btn--round btn--transparent">slides[i].callToActionText</div>: ''
                        }

                    </div>
                </a>
                </div>
            );
        }

        return (
            <div className="js-deals-carousel carousel image-carousel pos-rel side-widget__carousel">

                   {items}

             </div>
        )
    }

});


let ImageWidget = React.createClass({
    render(){
		const { url, type, targetUrl } = this.props.data;
		const isUrl = url && url.trim();
		const isTargetUrl = targetUrl && targetUrl.trim();
		return (
			isUrl ?
        	<div type={this.props.data.type} className="side-widget__image">
            {
                <a href={isTargetUrl || 'javascript:void(0);'} target="_blank">
                    <img width="350px" src={this.props.data.url}/>
                </a>
            }
        	</div> : <span></span>
		);

    }
});


SearchResultView.pageLoaded = () => {
    window.scrollTo(0,0);
    $(window).on('popstate', e => {
        //$("#editBookingModal").addClass('hide');
        methods.initFilters();
    });
    methods.initFilters();
    methods.initResults();
    methods.initSidewidgets();
    // SearchResultView.bindSideBar();
}


let analyticsConfigfunc = function(){
    return  {
        destination:$('#searchInput').val(),
        checkin:rt.getCheckin(),
        checkout:rt.getCheckout(),
        adults:rt.getAdults(),
        kids:rt.getKids(),
        rooms:rt.getRooms(),
        num_results:$('.analytics-count').attr('count'),
        sort_by:rt.sortBy | {},
        hrental_pagetype: 'searchresults',
        hrental_startdate: rt.getCheckin(),
        hrental_enddate: rt.getCheckout(),
    }
}


let pageName = ( rt.isHotelInCityPage ?  `Hotels in City Page` : 'Search Page');

let resultConfig = { name:pageName, widgets:[searchWidget], analyticsConfigfunc};
let searchResultView = SearchResultView.setup(resultConfig);
