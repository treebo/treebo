from django.core.cache import cache
from django.template.defaultfilters import slugify
from rest_framework.response import Response

from apps.hotels.serializers.city_serializer import CitySerializer
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.location import City, CityAlias
from services.restclient.contentservicerestclient import ContentServiceClient
from data_services.city_repository import CityRepository

city_data_service = RepositoriesFactory.get_city_repository()


class Cities(TreeboAPIView):
    CACHE_TIMEOUT = 24 * 60 * 60
    CACHE_KEY = 'cs_apicache_cities'

    @staticmethod
    def invalidate_cache():
        cache.delete(Cities.CACHE_KEY)
        cache.delete_pattern(Cities.CACHE_KEY + '*')

    def get(self, request, *args, **kwargs):
        city_slug_name = request.GET.get('name')
        if city_slug_name:
            try:
                result = cache.get(Cities.CACHE_KEY + '_' + city_slug_name)
            except Exception as e:
                result = None
            if result:
                return Response(result)
            city_search_args = []
            city_search_args.append({'name': 'name', 'value': city_slug_name.replace(
                '-', ''), 'case_sensitive': False, 'partial': True})
            current_cities = city_data_service.filter_cities_by_string_matching(
                city_search_args)
            if current_cities:
                city = current_cities[0]
            else:
                city = None
            #city = City.objects.filter(name__icontains=city_slug_name.replace('-', ' ')).first()
            if not city:
                city_alias = CityAlias.objects.filter(
                    alias__icontains=city_slug_name.replace('-', ' ')).first()
                city = city_data_service.get_single_city(name=city_alias.city)

            city_slug = slugify(city.name)
            seo_key = "search_" + city_slug + "_" + "desc"
            try:
                city_seo_description = ContentServiceClient.getValueForKey(
                    request, seo_key, 1)
                city_seo_description = city_seo_description['text']
            except Exception as exc:
                city_seo_description = ""

            response_data = CitySerializer(city).data
            response_data["description"] = city_seo_description
            cache.set(
                Cities.CACHE_KEY +
                '_' +
                city_slug_name,
                response_data,
                Cities.CACHE_TIMEOUT)
        else:
            try:
                result = cache.get(Cities.CACHE_KEY)
            except Exception as e:
                result = None
            if result:
                return Response(result)
            cities = city_data_service.get_all_cities()
            response_data = CitySerializer(cities, many=True).data
            cache.set(Cities.CACHE_KEY, response_data, Cities.CACHE_TIMEOUT)
        return Response(response_data)
