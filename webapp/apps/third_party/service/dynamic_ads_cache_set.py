import datetime
import json
import logging

import requests
from django.core.cache import cache

from conf.base import TRIPADVISOR_STAR_RATING_URL
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)
PRICING_URL = 'https://www.treebo.com/api/web/v6/pricing/hotels/'
DEFAULT_STAR_RATING = 3
DEFAULT_TOTAL_RATING = 1
UTM_SOURCE_FOR_APP = 'directTwo'


class DynamicAdsCacheSetService():
    CACHE_TIMEOUT = 60 * 60 * 24 * 3

    def get_active_hotels(self):
        hotel_obj_query_set = Hotel.objects.filter(
            status=Hotel.ENABLED).order_by('id').all()
        return hotel_obj_query_set

    def get_pricing_from_api(self, hotel_id):
        try:
            today = datetime.date.today()
            data = {
                'hotel_id':   hotel_id,
                'channel':    'website',
                'checkin':    today + datetime.timedelta(days=1),
                'checkout':   today + datetime.timedelta(days=2),
                'roomconfig': '1-0',
                'utm_source': 'direct'
            }
            response = requests.get(url=PRICING_URL, params=data)
            return response.json()["data"].get(str(hotel_id))
        except BaseException:
            raise Exception

    @staticmethod
    def get_pricing_for_app(hotel_id):
        try:
            today = datetime.date.today()
            data = {
                'hotel_id':   hotel_id,
                'channel':    'android',
                'checkin':    today + datetime.timedelta(days=1),
                'checkout':   today + datetime.timedelta(days=2),
                'roomconfig': '1-0',
                'utm_source': UTM_SOURCE_FOR_APP
            }
            response = requests.get(url=PRICING_URL, params=data)
            return response.json().get("data").get(str(hotel_id))
        except BaseException:
            raise Exception

    def set_cache_for_hotel_price_response(self, hotel_objects):
        for hotel_obj in hotel_objects:
            key = "PRICING_" + str(hotel_obj.id)
            try:
                price_response = self.get_pricing_from_api(hotel_obj.id)
                if cache.get(key) != price_response or (not cache.get(key)):
                    cache.set(key, price_response, self.CACHE_TIMEOUT)
                else:
                    continue
            except Exception as e:
                logger.exception(
                    "Couldn't set cache for pricing of %s %s", key, e)

            key = "PRICING_APP_" + str(hotel_obj.id)
            try:
                price_response = DynamicAdsCacheSetService.get_pricing_for_app(hotel_obj.id)
                if cache.get(key) != price_response or (not cache.get(key)):
                    cache.set(key, price_response, self.CACHE_TIMEOUT)
                else:
                    continue
            except Exception as e:
                logger.exception(
                    "Couldn't set cache for pricing of %s %s", key, e)

    def set_pricing_values(self):
        all_active_hotels = self.get_active_hotels()
        self.set_cache_for_hotel_price_response(all_active_hotels)

    def set_ta_rating(self):
        all_active_hotels = self.get_active_hotels()
        self.set_cache_for_ta_rating(all_active_hotels)

    def set_cache_for_ta_rating(self, hotel_objects):
        for hotel_obj in hotel_objects:
            key = "TA_RATING_" + str(hotel_obj.id)
            try:
                rating_response = self.get_hotel_overall_star_rating(hotel_obj.id)
                if cache.get(key) != rating_response or (not cache.get(key)):
                    cache.set(key, rating_response, self.CACHE_TIMEOUT)
                else:
                    continue
            except Exception as e:
                logger.exception(
                    "Couldn't set cache for pricing of %s %s", key, e)

    def get_hotel_overall_star_rating(self, hotel_id):
        star_rating = DEFAULT_STAR_RATING
        total_ratings = DEFAULT_TOTAL_RATING
        try:
            response = requests.get(TRIPADVISOR_STAR_RATING_URL + str(hotel_id))
            if response.status_code == 200:
                logger.info("Trip Advisor request %s", TRIPADVISOR_STAR_RATING_URL + str(hotel_id))
                result = json.loads(response._content)
                logger.info("Trip Advisor Hote Star Rating %s", result)

                if result["data"]["ta_reviews"]["ta_enabled"] == True:
                    star_rating = float(str(result["data"]["ta_reviews"]["overall_rating"]["rating"]))
                    total_ratings = int(float(str(result["data"]["ta_reviews"]["num_reviews"])))

        except Exception as e:
            logger.exception("Trip Advisor Rating fetch failed %s", hotel_id)

        return {"star_rating":   star_rating,
                "total_ratings": total_ratings
                }
