# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0015_auto_20160407_0944'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='anniversary_date',
            field=models.DateField(
                null=True,
                verbose_name='anniversary',
                blank=True),
        ),
        migrations.AlterField(
            model_name='highlight',
            name='description',
            field=models.CharField(
                max_length=300),
        ),
    ]
