import logging
import uuid

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_200_OK

from apps.true_caller.services.cache_service import CacheService
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class GetRequestNonce(TreeboAPIView):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetRequestNonce, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        request_nonce = str(uuid.uuid1())  # setting request_nonce to a new UUID
        CacheService.set(request_nonce)

        response_data = {"request_nonce": request_nonce}
        return JsonResponse({"data": response_data}, status=HTTP_200_OK)
