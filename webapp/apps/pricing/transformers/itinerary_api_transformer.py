from rest_framework.status import HTTP_400_BAD_REQUEST
from django.conf import settings
from apps.reward.service.reward import RewardService
from apps.pricing.exceptions import NoPriceForRatePlan, SoaError
from apps.checkout.service.upfront_part_pay_service import UpfrontPartPayService
from apps.common.exceptions.custom_exception import PartpayNotApplicable


class ItineraryAPITransformer(object):
    reward_service = RewardService()
    upfront_partpay_service = UpfrontPartPayService()

    @staticmethod
    def _default_price_component():
        default_price_component = {
            "rate_plan": {
                "code": None,
                "description": None,
                "tag": None
            },
            "price": {
                "net_payable_amount": 0,
                "wallet_applied": False,
                "wallet_deduction": 0,
                "wallet_type": settings.WALLET_TYPE,
                "base_price": 0,
                "rack_rate": 0,
                "wallet_deductable_amount": 0,
                "base_price_tax": 0,
                "voucher_amount": 0,
                "member_discount": 0,
                "coupon_discount": 0,
                "promo_discount": 0,
                "pretax_price": 0,
                "final_pretax_price": 0,
                "tax": 0,
                "sell_price": 0,
                "total_discount_percent": 0,
                "nights_breakup": []
            }
        }
        return default_price_component

    def transform_price_for_display(self, prices, selected_rate_plan, user=None, apply_wallet=False,
                                    coupon_code='',
                                    channel=None, part_pay_applicable=False):
        total_price = prices.get('cumulative_price')
        if not total_price:
            raise SoaError(
                code=HTTP_400_BAD_REQUEST,
                status=HTTP_400_BAD_REQUEST,
                msg='Prices are not available for this roomconfig.')
        date_wise_breakup = prices.get('date_wise_breakup')
        price_breakup = self._build_price_breakup(date_wise_breakup)
        date_and_room_config_wise_breakup = prices.get('date_and_room_wise_price')
        price_breakup_date_and_room_wise = self._build_price_breakup_date_and_room_wise(
            date_and_room_config_wise_breakup
        )
        price = {}
        rate_plan_detail = total_price.get(selected_rate_plan)
        member_discount_available = False
        member_discount_applied = False
        wallet_applied, total_wallet_balance, total_wallet_points, total_wallet_spendable_amount, \
            debit_sources, debit_percentage, loyalty_type = self.reward_service.get_wallet_balance(user)
        if total_price and rate_plan_detail:
            wallet_deductable_amount, wallet_deduction = self.reward_service.get_wallet_breakup(
                apply_wallet, total_wallet_spendable_amount,
                rate_plan_detail.get('price').sell_price)
            member_discount_available = member_discount_available or rate_plan_detail.get(
                'price').member_discount_available
            member_discount_applied = member_discount_applied or rate_plan_detail.get(
                'price').member_discount_applied
            params_to_price_dict = dict(rate_plan=selected_rate_plan,
                                        rate_plan_detail=rate_plan_detail,
                                        apply_wallet=apply_wallet,
                                        wallet_applied=wallet_applied,
                                        coupon_code=coupon_code,
                                        part_pay_applicable=part_pay_applicable,
                                        total_wallet_spendable_amount=total_wallet_spendable_amount)

            selected_rate_plan_price = self.generate_price_dict(**params_to_price_dict)
            selected_rate_plan_price["price"]["nights_breakup"] = price_breakup
            selected_rate_plan_price["price"][
                "nights_and_config_wise_breakup"] = price_breakup_date_and_room_wise
            price['selected_rate_plan'] = selected_rate_plan_price
        else:
            raise NoPriceForRatePlan(selected_rate_plan)

        all_rate_plans = []
        for rate_plan, rate_plan_detail in list(total_price.items()):
            params_to_price_dict = dict(rate_plan=rate_plan,
                                        rate_plan_detail=rate_plan_detail,
                                        apply_wallet=apply_wallet,
                                        wallet_applied=wallet_applied,
                                        coupon_code=coupon_code,
                                        part_pay_applicable=part_pay_applicable,
                                        total_wallet_spendable_amount=total_wallet_spendable_amount)
            rate_plan = self.generate_price_dict(**params_to_price_dict)
            all_rate_plans.append(rate_plan)

        wallet_info = {
            'wallet_type': settings.WALLET_TYPE,
            'wallet_applied': apply_wallet and wallet_applied,
            'total_wallet_balance': total_wallet_balance,
            'total_wallet_points': total_wallet_points,
            'wallet_deductable_amount': wallet_deductable_amount,
            'user': user.id if user and user.is_authenticated() else '',
        }
        price['all_rate_plans'] = all_rate_plans,
        price['member_discount_applied'] = member_discount_applied
        price['member_discount_available'] = member_discount_available
        price['wallet_info'] = wallet_info

        return price

    def generate_price_dict(self, rate_plan, rate_plan_detail, apply_wallet,
                            wallet_applied, coupon_code, part_pay_applicable,
                            total_wallet_spendable_amount):

        rp_price = rate_plan_detail.get('price')
        rate_plan_meta = rate_plan_detail.get('meta')

        total_discount_percent = rp_price.get_total_discount_percent()
        rate_plan_dict = dict(code=rate_plan)
        if rate_plan_meta:
            rate_plan_dict.update(rate_plan_meta)
        coupon_applied = True if coupon_code else False
        wallet_deductable_amount, wallet_deduction = self.reward_service.get_wallet_breakup(
            apply_wallet, total_wallet_spendable_amount, rp_price.sell_price)
        rate_plan = {
            "rate_plan": rate_plan_dict,
            "price": {
                "net_payable_amount": rp_price.sell_price - wallet_deduction,
                "wallet_applied": apply_wallet and wallet_applied,
                "wallet_deduction": wallet_deduction,
                "wallet_type": settings.WALLET_TYPE,
                "wallet_deductable_amount": wallet_deductable_amount,
                "base_price": rp_price.base_price,
                "rack_rate": rp_price.rack_rate,
                "base_price_tax": rp_price.base_price_tax,
                "voucher_amount": 0,
                "member_discount": rp_price.member_discount,
                "coupon_discount": rp_price.coupon_discount,
                "promo_discount": rp_price.promo_discount,
                "pretax_price": rp_price.pretax_price + rp_price.coupon_discount,
                "final_pretax_price": rp_price.pretax_price,
                "tax": rp_price.tax,
                "sell_price": rp_price.sell_price,
                "total_discount_percent": total_discount_percent,
                "coupon_applied": coupon_applied,
                "coupon_code": coupon_code,
                "part_pay_applicable": part_pay_applicable,
            }
        }

        if part_pay_applicable:
            try:
                partpay_amount = self.upfront_partpay_service.get_partpay_amount(
                    rate_plan["price"]["net_payable_amount"])
                rate_plan["price"]["net_payable_reserve"] = partpay_amount
            except PartpayNotApplicable:
                pass
        return rate_plan

    @staticmethod
    def _build_price_breakup(date_wise_price_breakup):
        price_breakup = []
        for date_, price in list(date_wise_price_breakup.items()):
            price_breakup.append(dict(sell_price=price.sell_price,
                                      pretax_price=price.pretax_price,
                                      total_discount=price.promo_discount,
                                      date=date_,
                                      tax=price.tax,
                                      base_price=price.base_price))

        sorted_price_breakup = sorted(price_breakup, key=lambda p: p['date'])
        return sorted_price_breakup

    @staticmethod
    def _build_price_breakup_date_and_room_wise(date_wise_price_breakup):
        price_breakup_date_and_room_wise = []
        for date_and_room_config, price in list(date_wise_price_breakup.items()):
            price_breakup_date_and_room_wise.append(dict(sell_price=price.sell_price,
                                                         pretax_price=price.pretax_price,
                                                         total_discount=price.promo_discount,
                                                         date=date_and_room_config[0],
                                                         room_config=date_and_room_config[1],
                                                         tax=price.tax,
                                                         base_price=price.base_price)
                                                    )
        sorted_price_breakup = sorted(price_breakup_date_and_room_wise, key=lambda p: p['date'])
        return sorted_price_breakup
