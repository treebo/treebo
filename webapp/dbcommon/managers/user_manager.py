# -*- coding: utf-8 -*-

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import UserManager
from django.utils.crypto import get_random_string
from django.db.models.query_utils import Q
import logging
import re

logger = logging.getLogger(__name__)
from apps.common.exceptions.custom_exception import UserWithPhoneAlreadyExist, UserWithEmailAlreadyExist, \
    UserWithAuthIDAlreadyExist


class CustomUserManager(UserManager):
    def _create_user(
            self,
            email,
            password,
            is_staff,
            is_superuser,
            is_active,
            **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            is_staff=is_staff,
            is_superuser=is_superuser,
            is_active=is_active,
            **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **kwargs):
        return self.create_user(
            email=email,
            password=password,
            is_staff=True,
            is_superuser=True,
            is_active=True,
            **kwargs)

    def create_user(
            self,
            email,
            password=None,
            is_staff=False,
            is_superuser=False,
            is_active=True,
            **kwargs):
        return self._create_user(
            email,
            password,
            is_staff,
            is_superuser,
            is_active,
            **kwargs)

    def create_unconfirmed_user(
            self,
            email=None,
            password=None,
            account_type=None,
            **extra_fields):
        """
        :rtype: profiles.models.User
        """
        if password is None:
            password = make_password(None)
        return super(CustomUserManager, self).create_user(
            email, password, account_type=account_type,
            is_active=False, **extra_fields
        )

    def generate_random_username(self):
        """
        Usernames that start with '!' are generated randomly and
        cannot be used for login until changed by the user.
        """
        return "!{}".format(get_random_string(length=16))

    def get_existing_user(self, email, phone, auth_id, throw_exception=False):
        sanitized_email = self.strip_gmail_special_chars(email)
        phone = self.parse_phone_number(phone)
        users = []
        if auth_id:
            users = self.filter(auth_id=auth_id)

        if throw_exception and len(users) > 0:
            raise UserWithAuthIDAlreadyExist()

        if not users and email:
            users = self.filter(Q(email=sanitized_email) | Q(email=email))

        if throw_exception and len(users) > 0:
            raise UserWithEmailAlreadyExist()

        if not users and phone:
            users = self.filter(phone_number=phone)

        if throw_exception and len(users) > 0:
            raise UserWithPhoneAlreadyExist()

        if len(users) > 1:
            logger.error(
                " multiple user found with same credential %s %s  ",
                sanitized_email,
                phone)
        if len(users) == 0:
            return None, email, phone
        return users.first(), email, phone

    def get_existing_user_by_phone_number(self, phone, throw_exception=False):
        phone = self.parse_phone_number(phone)

        users = self.filter(phone_number=phone)
        if throw_exception and len(users) > 0:
            raise UserWithPhoneAlreadyExist()

        if not users:
            return None

        return users.first()

    def get_user_by_auth_id(self, auth_id):
        return self.filter(auth_id=auth_id).first()

    def strip_gmail_special_chars(self, email):
        import re
        if not email:
            return None
        else:
            email = str(email).strip().lower()
        # if "gmail" in email:
        #     stripped_email = email.replace("@gmail.com", "")
        #     stripped_email = re.sub(r"\+.*", "", stripped_email, flags=re.IGNORECASE)
        #     return stripped_email.replace(".", "") + "@gmail.com"
        return email

    def parse_phone_number(self, phone):
        """function takes a phone number and converts it into  a valid one"""
        if not phone:
            return None
        try:
            temp_phone = re.sub("[^0-9]", "", phone)
            temp_phone = temp_phone.lstrip('0')
            if len(temp_phone) == 10:
                temp_phone = temp_phone
            elif temp_phone:
                pass
        except Exception:
            logger.exception(
                "Exception Occured in get_modified_phone_number in verification Task")
            temp_phone = phone
        return temp_phone
