# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0061_facility_catalog_mapped_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='CatalogRoomMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('catalog_room_id', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'db_table': 'seo_catalogroommapping',
                'verbose_name': 'Catalog Room Mapping',
                'verbose_name_plural': 'Catalog Room Mapping',
            },
        ),
        migrations.CreateModel(
            name='FeatureToggle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('num_of_days', models.IntegerField(default=180, help_text=b'Enter the number of days for which positives have to be fetched')),
                ('num_of_positives_shown', models.IntegerField(default=4, help_text=b'Enter the number of positives that we have to show on FE')),
                ('username', models.CharField(default=b'USERNAME', max_length=100)),
                ('password', models.CharField(default=b'PASSWORD', max_length=100)),
                ('restaurant_dist_cap', models.DecimalField(default=20, help_text=b'distance cap for restaurants nearby', max_digits=12, decimal_places=2)),
                ('closest_landmark_dist_cap', models.DecimalField(default=20, help_text=b'distance cap for most popular landmarks', max_digits=12, decimal_places=2)),
                ('closest_landmark_number_cap', models.IntegerField(default=5, help_text=b'max number of poi entries that should be shown for closest landmark')),
                ('most_popular_landmark_number_cap', models.IntegerField(default=5, help_text=b'max number poi of entries that should be shown for most popular landmark')),
                ('restaurants_number_cap', models.IntegerField(default=5, help_text=b'max number of poi entries that should be shown for restaurants and bar')),
                ('airport_number_cap', models.IntegerField(default=5, help_text=b'max number of poi entries that should be shown for airport and railway')),
            ],
            options={
                'db_table': 'seo_featuretoggle',
                'verbose_name': 'SEO Feature Toggle',
                'verbose_name_plural': 'SEO Feature Toggle',
            },
        ),
        migrations.CreateModel(
            name='FeedbackPositives',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('name', models.TextField(unique=True, null=True, blank=True)),
            ],
            options={
                'db_table': 'seo_feedbackpositives',
                'verbose_name': 'Feedback Positives',
                'verbose_name_plural': 'Feedback Positives',
            },
        ),
        migrations.CreateModel(
            name='HotelContentCountMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('count', models.PositiveIntegerField(default=0)),
            ],
            options={
                'db_table': 'seo_hotelcontentcountmapping',
                'verbose_name': 'Hotel Content Count Mapping',
                'verbose_name_plural': 'Hotel Content Count Mapping',
            },
        ),
        migrations.CreateModel(
            name='POI',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('poi_name', models.TextField()),
                ('poi_location', models.TextField(null=True, blank=True)),
                ('poi_city', models.TextField(null=True, blank=True)),
                ('poi_subcategory', models.TextField(null=True, blank=True)),
                ('poi_lat', models.DecimalField(max_digits=9, decimal_places=6)),
                ('poi_lng', models.DecimalField(max_digits=9, decimal_places=6)),
            ],
            options={
                'verbose_name': 'POI',
                'verbose_name_plural': 'POIs',
            },
        ),
        migrations.CreateModel(
            name='POICategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category_name', models.TextField()),
                ('is_locality_mapped', models.BooleanField(default=False)),
                ('is_city_mapped', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'POI Category',
                'verbose_name_plural': 'POI Categories',
            },
        ),
        migrations.CreateModel(
            name='PositiveCategoryContentMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('content', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'seo_positivecategorycontentmapping',
                'verbose_name': 'Positives Category Content Mapping',
                'verbose_name_plural': 'Positives Category Content Mapping',
            },
        ),
        migrations.CreateModel(
            name='PositivesCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('name', models.TextField(unique=True, null=True, blank=True)),
            ],
            options={
                'db_table': 'seo_positivescategory',
                'verbose_name': 'Positives Category',
                'verbose_name_plural': 'Positives Category',
            },
        ),
        migrations.CreateModel(
            name='ReasonsToPick',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('reason', models.TextField(null=True, blank=True)),
                ('hotel', models.ForeignKey(blank=True, to='dbcommon.Hotel', null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='WebCatalogHotelMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('catalog_hotel_id', models.CharField(max_length=200, null=True, blank=True)),
                ('hotel', models.ForeignKey(blank=True, to='dbcommon.Hotel', null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'seo_webcataloghotelmapping',
                'verbose_name': 'Web Hotel and Catalog Hotel Mapping',
                'verbose_name_plural': 'Web Hotel and Catalog Hotel Mapping',
            },
        ),
        migrations.AddField(
            model_name='positivecategorycontentmapping',
            name='category',
            field=models.ForeignKey(blank=True, to='seo.PositivesCategory', null=True, on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='positivecategorycontentmapping',
            name='positive',
            field=models.ForeignKey(blank=True, to='seo.FeedbackPositives', null=True, on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='poi',
            name='category',
            field=models.ForeignKey(to='seo.POICategory', on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='poi',
            name='city',
            field=models.ForeignKey(default=1, to='dbcommon.City', on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='poi',
            name='locality',
            field=models.ForeignKey(to='dbcommon.Locality', null=True, on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='hotelcontentcountmapping',
            name='feedback_content',
            field=models.ForeignKey(blank=True, to='seo.PositiveCategoryContentMapping', null=True, on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='hotelcontentcountmapping',
            name='hotel',
            field=models.ForeignKey(blank=True, to='dbcommon.Hotel', null=True, on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='catalogroommapping',
            name='hotel_map',
            field=models.ForeignKey(blank=True, to='seo.WebCatalogHotelMapping', null=True, on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='catalogroommapping',
            name='room',
            field=models.ForeignKey(blank=True, to='dbcommon.Room', null=True, on_delete=models.DO_NOTHING),
        ),
        migrations.AlterUniqueTogether(
            name='positivecategorycontentmapping',
            unique_together=set([('positive', 'category', 'content')]),
        ),
    ]
