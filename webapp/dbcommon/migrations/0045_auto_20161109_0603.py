# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0044_auto_20161026_0659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facility',
            name='hotels',
            field=models.ManyToManyField(to='dbcommon.Hotel', blank=True),
        ),
        migrations.AlterField(
            model_name='facility',
            name='rooms',
            field=models.ManyToManyField(to='dbcommon.Room', blank=True),
        ),
    ]
