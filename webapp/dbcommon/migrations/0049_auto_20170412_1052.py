# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0048_auto_20170412_0712'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='searchdistance',
            name='hotel',
        ),
        migrations.RemoveField(
            model_name='searchdistance',
            name='landmark',
        ),
        migrations.RenameField(
            model_name='searchlandmark',
            old_name='description',
            new_name='search_label',
        ),
        migrations.RemoveField(
            model_name='searchlandmark',
            name='name',
        ),
        migrations.AddField(
            model_name='searchlandmark',
            name='latitude',
            field=models.DecimalField(default=12.9667, null=True, max_digits=20, decimal_places=10, blank=True),
        ),
        migrations.AddField(
            model_name='searchlandmark',
            name='longitude',
            field=models.DecimalField(default=77.5667, null=True, max_digits=20, decimal_places=10, blank=True),
        ),
        migrations.AddField(
            model_name='searchlandmark',
            name='seo_url_name',
            field=models.CharField(default='', max_length=500),
        ),
        migrations.DeleteModel(
            name='SearchDistance',
        ),
    ]
