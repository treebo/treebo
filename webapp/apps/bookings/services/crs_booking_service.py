import json
import logging
import time

from datetime import timedelta, datetime
from decimal import Decimal
from django.conf import settings
from django.db import transaction
from ths_common.constants.billing_constants import ChargeBillToTypes, ChargeTypes, PaymentReceiverTypes, PhonePeTypes, \
    AmazonPayTypes
from ths_common.constants.billing_constants import PaymentChannels, PaymentStatus, PaymentModes, PaymentTypes
from ths_common.constants.booking_constants import ProfileTypes, AgeGroup, BookingStatus
from ths_common.exceptions import CRSException
from thsc.crs import context
from thsc.crs.entities.booking import Booking as crs_booking_object
from thsc.crs.entities.billing import Payment, Bill
from thsc.crs.entities.booking import Booking as CRSBooking, Address, GuestStay, Room, Price, Source
from thsc.crs.entities.customer import Customer, GSTDetails
from apps.bookings import constants
from apps.bookings.constants import THSC_CONTEXT_USER, THSC_CONTEXT_DEFAULT_APPLICATION, RAZORPAY_GATEWAY_TYPE, \
    AMAZON_GATEWAY_TYPE, PHONEPE_APP_GATEWAY_TYPE, PHONEPE_GATEWAY_TYPE, WALLET_GATEWAY_TYPE
from apps.bookings.dto.partpay_confirm_event import PartpayStatusForGrowth, PartpayConfirmReasons, PartpayConfirmEvent
from apps.bookings.models import Booking, RoomBooking
from apps.bookings.repositories.booking_repository import BookingServiceRepository
from apps.bookings.services.abstract_external_booking_service import AbstractExternalBookingService
from apps.bookings.services.booking_failure import BookingFailureHandler
from apps.bookings.services.booking_utils import get_response, ist_timezone, get_cs_room_type_code, \
    get_exception_mail, convert_date_to_datetime, convert_date_to_checkin_datetime, convert_date_to_checkout_datetime, \
    add_time_zone, get_cs_application_id, get_cs_sub_channel_id, convert_utc_to_localtime
from apps.checkout.models import BookingRequest, CommittedPrice
from apps.common.exceptions.booking_exceptions import CommittedPriceNotSetException, InvalidBookingRequestException
from apps.payments.service.payment_service import PaymentService
from base.db_decorator import db_retry_decorator

logger = logging.getLogger(__name__)

# The booking is held for settings.HOLD_TIME_IN_MINS_TO_CONFIRM_BOOKING minutes to confirm.
# This is the time given to the customer to complete his/her payment
default_hold_time = timedelta(minutes=settings.HOLD_TIME_IN_MINS_TO_CONFIRM_BOOKING)


class CRSBookingService(AbstractExternalBookingService):
    """
        CRS Implementation for ExternalBookingService APIs
    """
    payment_status_map_direct_to_crs = {
        'ISSUED': PaymentStatus.PENDING,
        'CAPTURED': PaymentStatus.DONE,
        'FAILED': PaymentStatus.CANCELLED
    }
    
    def save_booking_on_external_reservation_system(self, booking, room_configs, rate_name, special_rates_enabled):
        """
        Create a temp booking on CRS
        :param booking:
        :param room_configs:
        :param rate_name:
        :param special_rates_enabled:
        :param channel_id:
        :param sub_channel_id:
        :param application_id:
        :return:
        """
        try:
            logger.info("Entering method save_booking_on_client for order id : {0}".format(booking.order_id))
            
            try:
                booking_request = BookingRequest.objects.get(booking_id=booking.id)
            except BookingRequest.DoesNotExist:
                logger.exception("Booking request not found for booking id and order id {} {}".format(str(booking.id),
                                                                                                      str(booking.order_id) if booking.order_id else None))

                raise InvalidBookingRequestException("Booking request not found for booking id {0}".format(booking.id))
            
            context.user = THSC_CONTEXT_USER
            context.application = booking_request.booking_channel
            
            crs_booking_owner = Customer(first_name=booking.guest_name,
                                         email=booking.guest_email,
                                         phone_number=booking.guest_mobile,
                                         reference_id=str(booking.user_id.id),
                                         profile_type=ProfileTypes.INDIVIDUAL
                                         )
            
            crs_rooms = self.get_crs_room_details(booking, room_configs, booking_request)
            
            crs_source = Source(channel_id='direct',
                                sub_channel_id=get_cs_sub_channel_id(booking_request.booking_channel,
                                                                     booking_request.utm_source),
                                application_id=get_cs_application_id(booking_request.booking_channel))
            
            crs_hold_till = datetime.now(ist_timezone) + default_hold_time  # Setting default hold_till (15 minutes).
            crs_booking_dto = CRSBooking(hotel_id=booking.hotel.cs_id,
                                         rooms=crs_rooms,
                                         source=crs_source,
                                         booking_owner=crs_booking_owner,
                                         reference_number=booking.order_id,  # Setting reference_number to order_id
                                         comments=booking.comments,
                                         status=BookingStatus.TEMPORARY,
                                         hold_till=crs_hold_till)
            
            crs_create_booking_response = crs_booking_dto.create()

            logger.info(
                "Response for save_booking_on_external_reservation_system crs_booking_dto: {}, crs_create_booking_response: {}, booking: {}".format(
                    str(self.get_crs_pretty_object_info(crs_booking_dto)), str(self.get_crs_pretty_object_info(crs_create_booking_response)), str(booking)))
            return crs_create_booking_response
        
        finally:
            context.user = None
            context.application = None
            logger.info("Exiting method save_booking_on_client for order id : {0}".format(booking.order_id))

    def get_crs_pretty_object_info(self, obj):
        info = {}
        for key, value in obj.__dict__.items():
            if str(type(value)).find("thsc.crs.entities") != -1:
                try:
                    info[key] = str(vars(value))
                except Exception as exception:
                    info[key] = value
            else:
                info[key] = value
        return info

    def get_crs_room_details(self, booking, room_configs, booking_request):
        """
        Get rooms detail from CRS
        :param booking:
        :param room_configs:
        :param booking_request:
        :return:
        """
        try:
            logger.debug("Entering method get_crs_room_details for order id : {0}".format(booking.order_id))
            
            committed_price = CommittedPrice.objects.filter(booking_request=booking_request).order_by('-id').first()
            if not committed_price:
                raise CommittedPriceNotSetException(
                    "Committed price not set for booking request with id {0}".format(booking_request.id))
            
            date_and_room_wise_prices = json.loads(committed_price.datewise_pricing)
            
            cs_room_type_id = get_cs_room_type_code(booking.room.room_type_code.upper())
            
            room_config_to_frequency = {}
            for room_config in room_configs:
                frequency = 1
                value = room_config_to_frequency.get(room_config)
                if value:
                    frequency = value + 1
                room_config_to_frequency[room_config] = frequency
            
            crs_rooms = []
            diff = booking.checkout_date - booking.checkin_date
            for room_config, frequency in room_config_to_frequency.items():
                adult_count = int(room_config[0])
                guest_stays = [GuestStay(age_group=AgeGroup.ADULT)
                               for _ in range(adult_count)]
                child_count = int(room_config[1])
                guest_stays.extend([GuestStay(age_group=AgeGroup.CHILD) for _ in range(child_count)])
                
                prices = []
                for day in range(diff.days):
                    applicable_date = booking.checkin_date + timedelta(days=day)
                    
                    price_for_date_and_room_conf = date_and_room_wise_prices[applicable_date.__str__()][
                        str(room_config[0]) + '-' + str(room_config[1])]
                    
                    prices.append(
                        Price(posttax_amount=Decimal(price_for_date_and_room_conf['sell_price']) / frequency,
                              applicable_date=add_time_zone(convert_date_to_datetime(applicable_date)),
                              bill_to_type=ChargeBillToTypes.GUEST,
                              type=ChargeTypes.NON_CREDIT)
                    )
                
                for _ in range(frequency):
                    crs_rooms.append(
                        Room.create_instance(
                            room_type_id=cs_room_type_id,
                            guests=guest_stays,
                            prices=prices,
                            checkin_date=add_time_zone(convert_date_to_checkin_datetime(booking.checkin_date)),
                            checkout_date=add_time_zone(convert_date_to_checkout_datetime(booking.checkout_date)))
                    )
            
            return crs_rooms
        
        finally:
            logger.debug("Exiting method get_crs_room_details for order id : {0}".format(booking.order_id))
    
    def save_booking_to_db(self, booking, save_booking_response):
        """
        update booking detail received from CRS to DB
        :param booking:
        :param save_booking_response:
        :return:
        """
        try:
            logger.debug("Entering method save_booking_to_db for order id : {0}".format(booking.order_id))
            
            booking.external_crs_booking_id = save_booking_response.booking_id
            booking.external_crs_booking_version = save_booking_response.version
            extra_info = {'crs_booking_bill_id': save_booking_response.bill_id}
            booking.external_crs_booking_extra_info = json.dumps(extra_info)
            booking.booking_status = Booking.TEMP_BOOKING_CREATED
            BookingServiceRepository.save_booking(booking)
        finally:
            logger.debug("Exiting method save_booking_to_db for order id : {0}".format(booking.order_id))
    
    def save_room_booking_to_db(self, booking, save_booking_response):
        """
        update RoomBooking detail received from CRS to DB
        :param booking:
        :param save_booking_response:
        :return:
        """
        try:
            logger.debug("Entering method save_room_booking_to_db for order id : {0}".format(booking.order_id))
            
            for room in save_booking_response.rooms:
                
                # Since CRS doesn't provide us with reservation_id, hence computing the same as
                # booking_id + '::' + room_stay_id
                # The about expression will result in a unique string
                reservation_id = save_booking_response.booking_id + '::' + str(room.room_stay_id)
                
                room_bookings = RoomBooking.objects.filter(
                    booking=booking, reservation_id=reservation_id).count()
                if room_bookings > 0:
                    continue
                
                room_booking = RoomBooking()
                room_booking.booking = booking
                room_booking.price = booking.room.price
                room_booking.reservation_id = reservation_id
                room_booking.room = booking.room
                BookingServiceRepository.save_room_booking(room_booking)
        finally:
            logger.debug("Exiting method save_room_booking_to_db for order id : {0}".format(booking.order_id))
    
    def create_save_booking_response(self, save_booking_response, booking_id):
        """
        Generate save booking response
        :param save_booking_response:
        :param booking_id:
        :return:
        """
        try:
            logger.debug("Entering method create_save_booking_response for booking_id : {0}".format(booking_id))
            
            guest_ids = []
            for customer in save_booking_response.customers:
                guest_ids.append(customer.customer_id)
            
            response = {
                'guest_ids': guest_ids,
                'booking_ids': [save_booking_response.booking_id],
                'group_ids': [save_booking_response.booking_id]}
            
            return response
        
        finally:
            logger.debug("Exiting method create_save_booking_response for booking_id : {0}".format(booking_id))
    
    def confirm_booking_on_external_reservation_system(self, booking, paid_source):
        """
        Confirms a temporary booking in CRS.
        :param booking:
        :param paid_source:
        :return:
        """
        try:
            logger.debug("Entering method confirm_booking_on_client for order_id : {0}".format(booking.order_id))
            
            booking_request = BookingRequest.objects.filter(booking_id=booking.id).last()
            context.user = THSC_CONTEXT_USER
            context.application = booking_request.booking_channel if booking_request else \
                THSC_CONTEXT_DEFAULT_APPLICATION
            
            if not booking.external_crs_booking_id:
                logger.error("Booking has not been initiated %s", booking.order_id)
                raise CRSException(message="Booking has not been initiated so cannot confirm")
            logger.info(
                "booking crs_booking_id %s and external_crs_booking_version %s and paid_source %s and order_id %s",
                booking.external_crs_booking_id, booking.external_crs_booking_version, paid_source, booking.order_id)
            # fetching CRS booking for update
            crs_booking = CRSBooking.create_instance_for_update(
                booking_id=booking.external_crs_booking_id,
                booking_version=booking.external_crs_booking_version)
            if not crs_booking:
                crs_booking = crs_booking_object.get(booking.external_crs_booking_id)
            if not crs_booking.extra_information:
                crs_booking.extra_information = {'stay_source': paid_source}
            else:
                crs_booking.extra_information['stay_source'] = paid_source

            try:
                crs_booking.update()
            except Exception as exception:
                logger.info(
                    "Update failure response for confirm_booking_on_external_reservation_system booking: {}, crs_booking: {} due to {}".format(
                        str(booking), str(self.get_crs_pretty_object_info(crs_booking)), str(exception)))
                raise exception

            logger.info(
                "Update response for confirm_booking_on_external_reservation_system booking: {}, crs_booking: {}".format(
                    str(booking), str(self.get_crs_pretty_object_info(crs_booking))))

            crs_booking.refresh_booking()
            crs_booking.confirm()
            
            logger.info('Confirm booking response {0}'.format(crs_booking.__dict__))
            
            saved_booking = self.finalize_booking_confirmation(booking.id, crs_booking)
            
            guest_ids = []
            for customer in crs_booking.customers:
                guest_ids.append(customer.customer_id)
            
            response_data = {
                'guestids': guest_ids,
                'booking_status': saved_booking.booking_status,
                'wallet_applied': saved_booking.wallet_applied
            }
            logger.info("response_data for crs finalise booking %s", response_data)
            return response_data
        
        finally:
            context.user = None
            context.application = None
            logger.debug("Exiting method confirm_booking_on_client for order_id : {0}".format(booking.order_id))
    
    def get_data_to_update_upfront_partpay_in_growth(self, booking):
        # not sending booking_Code because all the records with the given group code should be confirmed
        return PartpayConfirmEvent(
            group_code=booking.external_crs_booking_id,
            hotel_code=booking.hotel.cs_id,
            booking_code=None,
            status=PartpayStatusForGrowth.CONFIRM,
            confirmation_reason=PartpayConfirmReasons.UPFRONT_PARTPAY
        )
    
    @db_retry_decorator()
    def cancel_booking_on_external_reservation_system(self, booking, room_bookings, order_id):
        """
        Cancel a booking in CRS
        :param booking:
        :param room_bookings: Parameter Not Used
        :param order_id:
        :return:
        """
        try:
            logger.debug("Entering method cancel_booking_on_client for order_id : {0}".format(booking.order_id))
            
            booking_request = BookingRequest.objects.filter(booking_id=booking.id).last()
            context.user = THSC_CONTEXT_USER
            context.application = booking_request.booking_channel if booking_request else \
                THSC_CONTEXT_DEFAULT_APPLICATION
            
            crs_booking = CRSBooking.create_instance_for_update(booking_id=booking.external_crs_booking_id,
                                                                booking_version=booking.external_crs_booking_version)
            crs_booking.cancel("Cancelled by customer")
            logger.info("Booking canceled in CRS for booking%s" % crs_booking)
            
            current_date = datetime.datetime.now().date()
            booking.booking_status = booking.CANCEL
            booking.cancellation_datetime = current_date
            booking.save()
            logger.debug('Cancellation successful for Booking ID: %s', booking.order_id)
        
        finally:
            context.user = None
            context.application = None
            logger.debug("Exiting method cancel_booking_on_client for order_id : {0}".format(booking.order_id))
    
    @db_retry_decorator()
    @transaction.atomic
    def finalise_booking(self, booking_id, user_id=None, request_id=None):
        """
        Finalize a temporary booking in CRS.
        Basically a (confirm_booking + update_payment_for_booking)
        :param booking_id:
        :param user_id:
        :param request_id:
        :param is_secure:
        :return:
        """
        try:
            logger.debug("Entering method finalise_booking for booking_id : {0}".format(booking_id))
            
            confirm_response = self.confirm_booking(booking_id, user_id=user_id, request_id=request_id)
            logger.info("Confirm response type: %s", type(confirm_response))
            logger.info("confirm_response %s ", str(confirm_response))
            
            return confirm_response
        
        finally:
            logger.debug("Exiting method finalise_booking for booking_id : {0}".format(booking_id))
    
    @db_retry_decorator()
    @transaction.atomic
    def update_payment_for_booking_on_external_reservation_system(self, booking_id):
        """
        Update payment details for a booking
        :param booking_id:
        :return:
        """
        try:
            logger.debug("Entering method update_payment_for_booking for booking_id : {0}".format(booking_id))
            
            booking_request = BookingRequest.objects.filter(booking_id=booking_id).last()
            context.user = THSC_CONTEXT_USER
            context.application = booking_request.booking_channel if booking_request else \
                THSC_CONTEXT_DEFAULT_APPLICATION
            
            try:
                booking = Booking.objects.select_related('hotel').get(pk=booking_id)
            except Exception as e:
                logger.error("unable to fetch booking with hotel details for {0}".format(booking_id), exc_info=e)
                return get_response(500, "Booking not found for id {0}".format(booking_id))
            
            payment_orders = PaymentService.get_non_wallet_payments_for_booking(booking.order_id)
            # payment_orders_wallet = PaymentService.get_wallet_payments_for_booking(booking.order_id)
            
            if len(payment_orders) == 0 and not booking.wallet_applied:
                logger.info("No payment for booking %s", booking_id)
                return get_response(200, "Booking doesn't have payments : {0}".format(booking_id))
            
            payment_amount = 0
            
            # fetching Bill for adding payments
            booking_extra_info = json.loads(booking.external_crs_booking_extra_info)
            crs_bill = Bill.get(booking_extra_info['crs_booking_bill_id'])
            
            for payment_order in payment_orders:
                if payment_order.updated_in_booking_service:
                    logger.info(
                        "paymentorder %s for booking %s is already updated for bid %s ",
                        payment_order.ps_order_id,
                        payment_order.booking_order_id,
                        booking_id)
                    continue
                if not payment_order.is_verified():
                    logger.info(
                        "paymentorder is not verified ps_orderid %s booking_order_id %s for bid %s ",
                        payment_order.ps_order_id,
                        payment_order.booking_order_id,
                        booking_id)
                    continue
                
                payment_amount += Decimal(payment_order.amount)
                response = self.update_payment_on_crs(
                    crs_bill,
                    self.create_crs_payment_object(payment_order)
                )
                logger.info(
                    "response for updating payment on crs for payment order id %s is %s ",
                    payment_order.id,
                    response)
                
                payment_order.updated_in_booking_service = True
                payment_order.save()
            
            # if booking.wallet_applied:
            #     payment_amount += Decimal(booking.wallet_deduction)
            #
            #     response = self.update_payment_on_crs(
            #         crs_bill,
            #         self.create_crs_payment_object_for_wallet_payment_from_booking(booking, payment_orders_wallet)
            #     )
            #     logger.info(
            #         "response for updating wallet payment on crs for order id %s is %s ",
            #         booking.order_id,
            #         response)
            
            booking.payment_amount = payment_amount
            crs_booking = CRSBooking.get(booking.external_crs_booking_id)
            booking.external_crs_booking_version = crs_booking.version
            booking.save()
            
            return get_response(200, "Payments are updated in CRS for Booking_id : {0}".format(booking_id))
        
        except CRSException as e:
            logger.exception("CRSException occurred while updating payment in CRS for Order Id {0}"
                             .format(booking.order_id))
            
            exception_mail = get_exception_mail(e,
                                                "Unable to update guest details in CRS (PMS). Order Id: {0}"
                                                .format(booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                "Unable to update payment in CRS (PMS)",
                "Please Update Payment in CRS (PMS) / Do not create booking for order id :")
            return get_response(500, str(e), [exception_mail, track_failure])
        except Exception as e:
            logger.error("Error occurred while updating payment in CRS for order id {0}"
                         .format(booking.order_id), exc_info=e)
            
            exception_mail = get_exception_mail(e,
                                                'Unable to update payment details in CRS (PMS). Order Id: {0}'
                                                .format(booking.order_id))
            
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                'Unable to update payment in CRS (PMS)',
                'Please Update Payment in CRS (PMS) / Do not create booking for order id :'
            )
            return get_response(400, str(e), [exception_mail, track_failure])
        
        finally:
            context.user = None
            context.application = None
            logger.debug("Exiting method update_payment_for_booking for booking_id : {0}".format(booking_id))

    def create_crs_payment_object_for_wallet_payment_from_booking(self, booking, payment_orders_wallet):

        if not payment_orders_wallet:
            time.sleep(2)
            payment_orders_wallet = PaymentService.get_wallet_payments_for_booking(booking.order_id)

        payment_details = {
            'direct_payment_mode': settings.HX_GATEWAY_CONFIG.get(constants.WALLET_GATEWAY_TYPE).get('payment_mode'),
            'direct_payment_type': settings.HX_GATEWAY_CONFIG.get(constants.WALLET_GATEWAY_TYPE).get('payment_type'),
        }
        time_zone = convert_utc_to_localtime(booking.modified_at)

        payment = Payment.create_instance(
            amount=Decimal(booking.wallet_deduction),
            date_of_payment=time_zone,
            paid_to=PaymentReceiverTypes.TREEBO,
            payment_channel=PaymentChannels.ONLINE,
            payment_mode=PaymentModes.TREEBO_POINTS,
            payment_type=PaymentTypes.PAYMENT,
            status=PaymentStatus.DONE,
            paid_by=PaymentReceiverTypes.GUEST,
            payment_details=payment_details,
            payment_ref_id=str(payment_orders_wallet.ps_payment_id))
        
        return payment
    
    def create_crs_payment_object(self, payment_order):
        """
        Create CRS (THSC) Payment object from Payment Order
        :param payment_order:
        :return:
        """
        payment_mode_sub_type = None
        gateway = str(payment_order.gateway_type).lower().strip()
        if gateway == RAZORPAY_GATEWAY_TYPE:
            payment_mode = PaymentModes.RAZORPAY_PAYMENT_GATEWAY
        elif gateway == AMAZON_GATEWAY_TYPE:
            payment_mode = PaymentModes.AMAZON_PAY
            payment_mode_sub_type = AmazonPayTypes.AMAZONPAY_WEB
        elif gateway == PHONEPE_GATEWAY_TYPE:
            payment_mode = PaymentModes.PHONE_PE
            payment_mode_sub_type = PhonePeTypes.PHONEPE_WEB
        elif gateway == PHONEPE_APP_GATEWAY_TYPE:
            payment_mode = PaymentModes.PHONE_PE
            payment_mode_sub_type = PhonePeTypes.PHONEPE_APP
        elif gateway == WALLET_GATEWAY_TYPE:
            payment_mode = PaymentModes.TREEBO_POINTS
        
        payment_details = {
            'direct_booking_order_id': payment_order.booking_order_id,
            'direct_payment_mode': settings.HX_GATEWAY_CONFIG[gateway]['payment_mode'],
            'direct_payment_type': settings.HX_GATEWAY_CONFIG[gateway]['payment_type'],
            'direct_status': payment_order.status,
            'direct_payment_id': payment_order.id,
            'ps_payment_id': str(payment_order.ps_order_id),
        }
        time_zone = convert_utc_to_localtime(payment_order.created_at)
        payment = Payment.create_instance(
            amount=Decimal(payment_order.amount),
            date_of_payment=time_zone,
            paid_to=PaymentReceiverTypes.TREEBO,
            payment_channel=PaymentChannels.ONLINE,
            payment_mode=payment_mode,
            payment_mode_sub_type=payment_mode_sub_type,
            payment_type=PaymentTypes.PAYMENT,
            status=self.payment_status_map_direct_to_crs.get(payment_order.status),
            paid_by=PaymentReceiverTypes.GUEST,
            payment_details=payment_details,
            payment_ref_id=str(payment_order.ps_payment_id))

        
        return payment
    
    @db_retry_decorator()
    def update_payment_on_crs(self, crs_bill, crs_payment):
        """
        Update payment details for a booking in CRS
        :param crs_bill:
        :param crs_payment:
        :return:
        """
        try:
            logger.debug("Entering method update_payment_on_crs for payment ref id: {0}"
                         .format(crs_payment.payment_ref_id))
            
            response = crs_bill.add_payment(crs_payment)
            return response
        finally:
            logger.debug("Exiting method update_payment_on_crs for payment ref id: {0}"
                         .format(crs_payment.payment_ref_id))
    
    def finalize_booking_confirmation(self, booking_id, crs_booking):
        """
        Finalizing/Confirming booking in DB
        :param booking_id:
        :param crs_booking:
        :return:
        """
        booking = Booking.objects.get(pk=booking_id)
        logger.info(
            "final confirm booking_id %s wallet_applied %s booking.wallet_deduction %s ",
            booking.id,
            booking.wallet_applied,
            booking.wallet_deduction)
        
        booking.external_crs_booking_version = crs_booking.version
        if self.is_wallet_applied_and_wallet_payment_failed(booking):
            booking.booking_status = Booking.PART_SUCCESS
        else:
            booking.booking_status = Booking.CONFIRM
        booking.is_complete = True
        BookingServiceRepository.save_booking(booking)
        logger.info(
            "Inside finalize_booking_confirmation. Finalizing Booking Confirmation for booking_id: %s with status %s",
            booking_id,
            booking.booking_status)
        
        return booking
    
    def get_subject_for_track_failure(self):
        return 'URGENT: Reservation Attempt Failure in CRS (PMS). Order ID: '
    
    def get_external_reservation_system_name(self):
        return 'CRS (PMS)'
    
    @db_retry_decorator()
    def update_gst_detail_of_booking_owner_on_external_reservation_system(self, booking):
        """
        update GST Detail of the booking owner on CRS
        :param apps.bookings.models.Booking booking:
        :return:
        """
        try:
            logger.debug("Entering method update_gst_detail_of_booking_owner_on_external_reservation_system "
                         "for booking_id : {0}".format(booking.id))
            
            booking_request = BookingRequest.objects.filter(booking_id=booking.id).last()
            context.user = THSC_CONTEXT_USER
            context.application = booking_request.booking_channel if booking_request else \
                THSC_CONTEXT_DEFAULT_APPLICATION
            
            crs_booking = CRSBooking.get(booking_id=booking.external_crs_booking_id)
            booking_owner_id = crs_booking.booking_owner_id
            
            booking_owner = Customer.create_instance_for_update(booking_owner_id)
            booking_owner.first_name = booking.guest_name
            booking_owner.email = booking.guest_email
            booking_owner.phone_number = booking.guest_mobile
            booking_owner.reference_id = str(booking.user_id.id)
            booking_owner.profile_type = ProfileTypes.INDIVIDUAL
            booking_owner.gst_details = GSTDetails(address=Address(country='NA',
                                                                   state='NA',
                                                                   field1=booking.organization_address),
                                                   gstin_num=booking.organization_taxcode,
                                                   legal_name=booking.organization_name
                                                   )
            crs_booking.update_customer(booking_owner)
            
            booking.external_crs_booking_version = crs_booking.version
            BookingServiceRepository.save_booking(booking)
            
            return get_response(200, "Updated Guest Details : {0}".format(booking.id))
        except CRSException as e:
            logger.exception("CRSException occurred while updating guest details in CRS for Order Id {0}"
                             .format(booking.order_id))
            
            exception_mail = get_exception_mail(e,
                                                "Unable to update guest details in CRS (PMS). Order Id: {0}"
                                                .format(booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                "updating gstin details",
                "Please Update GSTIN details in CRS (PMS) / Do not create booking for order id : ")
            return get_response(500, str(e), [exception_mail, track_failure])
        finally:
            context.user = None
            context.application = None
            logger.debug("Exiting method update_gst_detail_of_booking_owner_on_external_reservation_system "
                         "for booking_id : {0}".format(booking.id))
