from apps.common.data_classes.base_data_class import BaseDataClass
from apps.payments.offers.dataclasses.payment_method import PaymentMethod


class PaymentOffer(BaseDataClass):
    def __init__(self, uid, payment_method, offer_percentage, credit_mode,
                 minimum_transaction, ranking, title=None, description=None,
                 terms_conditions=None, gateway_offer_id=None, image_url=None, coupon_code=None):
        self.uid = uid
        self.payment_method = payment_method
        assert isinstance(payment_method, PaymentMethod)
        self.offer_percentage = offer_percentage
        self.credit_mode = credit_mode
        self.minimum_transaction = minimum_transaction
        self.ranking = ranking
        self.description = description
        self.terms_conditions = terms_conditions
        self.gateway_offer_id = gateway_offer_id
        self.image_url = image_url
        self.coupon_code = coupon_code
        self.title = title
