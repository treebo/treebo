import logging
import traceback
from datetime import datetime

from apps.common import date_time_utils
from apps.discounts.constraint_validators import ConstraintValidator
from apps.discounts.expression_parser import ExpressionParser
from apps.discounts.models import DiscountCoupon
from apps.discounts import constants
from common.exceptions.treebo_exception import TreeboValidationException

logger = logging.getLogger(__name__)


class DiscountManager:
    def __init__(self, **kwargs):
        """
        Unpaking kwargs
        :param code:
        :param checkin:
        :param checkout:
        :param hotel_id:
        :param user:
        :param rooms:
        :param booking_date:
        :param total_pretax_cost:
        :param totalcost:
        :param nights:
        :param no_bookings:
        :param paytype:
        :param days:
        :param weeks:
        :param months:
        :param corporate:
        :return:
        """
        self.__code = kwargs.get('code')
        self.__checkin = kwargs.get('checkin')
        self.__checkout = kwargs.get('checkout')
        self.__today = datetime.now().date()
        self.__total_pretax_cost = kwargs.get('total_pretax_cost')
        self.__rackrate = kwargs.get('rack_rate')
        self.__pretax_rackrate = kwargs.get('pretax_rack_rate')
        if not self.__pretax_rackrate:
            logger.warning("Pretax Rack Rate not passed in DiscountManager")
        if not self.__rackrate:
            logger.warning("Rack Rate not passed in DiscountManager")
        self.__totalcost = kwargs.get('totalcost')
        self.__nights = kwargs.get('nights')
        self.__paytype = kwargs['paytype'] if kwargs.get(
            'paytype', None) else ""
        self.params = kwargs

    def validate_paytype(self, coupon):
        paytype_map = {'': constants.PAY_NOW,
                       '0': constants.PAY_NOW,
                       '1': constants.PAY_AT_HOTEL
                       }
        paytype = paytype_map[self.__paytype]
        if coupon.is_prepaid:
            if paytype != constants.PAY_NOW:
                return False, constants.PAY_TYPE_CONSTRAINT % constants.PAY_NOW
        return True, constants.SUCCESS

    def validate(self):
        logger.debug("message is success")
        constraint_validator = ""
        try:
            coupon = DiscountCoupon.objects.get(code=self.__code)
            self.params['coupon'] = coupon
            message = self.validate_coupon(coupon)
            if message != constants.SUCCESS:
                logger.debug("message is not success")
                return False, message
            logger.debug("message is success")
            valid, message = self.validate_paytype(coupon)
            if not valid:
                return False, message
            if coupon.constraintexpression_set.count() == 0:
                return coupon, constants.SUCCESS
            else:
                for constraint_expression in coupon.constraintexpression_set.all():
                    expression = constraint_expression.expression
                    constraint_validator = ConstraintValidator(**self.params)
                    ExpressionParser(
                        expression, constraint_validator).parse_expression()
                return coupon, constants.SUCCESS

        except DiscountCoupon.DoesNotExist:
            return False, constants.COUPON_NOT_FOUND
        except TreeboValidationException as e:
            return False, str(e)
        except Exception as e:
            logger.exception("Error applying coupon %s", self.__code)
            return False, 'Error applying coupon'

    def validate_coupon(self, coupon):
        if not coupon.is_active:
            return constants.COUPON_NOT_ACTIVE
        if not coupon.start_date or coupon.start_date > self.__today:
            return constants.COUPON_NOT_ACTIVE
        elif coupon.expiry and coupon.expiry < self.__today:
            return constants.COUPON_EXPIRED
        elif coupon.max_available_usages <= 0:
            return constants.COUPON_EXHAUSTED
        else:
            return constants.SUCCESS

    def apply(self, coupon, rooms):
        discount = 0
        self.__nights = room_nights = date_time_utils.get_day_difference(
            self.__checkin, self.__checkout)
        if coupon.price_component_applicable == DiscountCoupon.RACK_RATE:
            operation_amount = self.__rackrate
        elif coupon.discount_application == DiscountCoupon.POSTTAX:
            operation_amount = self.__totalcost
        else:
            operation_amount = self.__total_pretax_cost

        if coupon.discount_operation == DiscountCoupon.PERCENTAGE:
            discount = float(coupon.discount_value) / \
                100 * float(operation_amount)
        elif coupon.discount_operation == DiscountCoupon.FIXED:

            discount = float(coupon.discount_value)
        elif coupon.discount_operation == DiscountCoupon.NIGHT:
            night_multiplier = 1 if int(
                coupon.discount_value) <= 1 else int(
                coupon.discount_value)
            discount = float(operation_amount) / \
                int(room_nights) * night_multiplier
        elif coupon.discount_operation == DiscountCoupon.FLATPERNIGHT:
            total_nights = int(room_nights) * int(rooms)
            discount = coupon.discount_value * total_nights
        elif coupon.discount_operation == DiscountCoupon.FLATCOST:
            discount = operation_amount - float(coupon.discount_value)
        else:
            logger.info("Invalid Coupon Operation")

        if discount > coupon.max_available_discount:
            discount = coupon.max_available_discount
        if discount > operation_amount:
            discount = operation_amount
        if str(coupon.coupon_type) != DiscountCoupon.DISCOUNT:
            discount = 0

        return discount
