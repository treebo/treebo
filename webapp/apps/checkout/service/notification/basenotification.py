import logging
import urllib.request
import urllib.parse
import urllib.error

import requests
from django.conf import settings
from django.core.mail import EmailMessage
from django.template import loader

from common.exceptions.treebo_exception import TreeboValidationException

logger = logging.getLogger(__name__)


class NotificationService(object):
    NOTIFICATION_EMAIL = "EMAIL"
    NOTIFICATION_SMS = "SMS"
    template_name = None

    def __init__(
            self,
            notification_type,
            content_subtype,
            to_address,
            subject=None,
            data=None):
        self.__msg = None
        self.__recipients = to_address
        self.__from = settings.SERVER_EMAIL
        self.__subject = subject
        self.__data = data
        self.__notification_type = notification_type
        self.__sub_type = content_subtype

    # self.template_name = None

    def __getTemplate(self):
        logger.info("calling _get_template")
        if self.template_name is None:
            raise TreeboValidationException("Template or HTML not found.")
        else:
            return self.template_name

    def _sendMessage(self):
        if self.__notification_type == NotificationService.NOTIFICATION_EMAIL:
            self.__msg.send()
            logger.info("email sent successfully")
        else:
            sms_url = str(
                settings.SMS_VENDOR_URL) + "?method=SendMessage&msg_type=TEXT&userid=" + str(
                settings.SMS_VENDOR_ID) + "&auth_scheme=plain&password=" + str(
                settings.SMS_VENDOR_PASSWORD) + "&v=1.1&format=text&send_to=" + str(
                self.__recipients) + "&msg=" + str(self.__msg)
            logger.info(sms_url)
            logger.info(requests.get(sms_url).text)
            logger.info("sms sent successfully")

    def _createMessage(self):
        """

        """
        if self.__notification_type == NotificationService.NOTIFICATION_EMAIL:
            try:
                self.__validateEmailDetails()
            except TreeboValidationException as e:
                raise e

            template = loader.get_template(self.__getTemplate())
            content = template.render(self.__data)
            self.__msg = EmailMessage(
                self.__subject,
                content,
                self.__from,
                self.__recipients)
            self.__msg.content_subtype = self.__sub_type
        else:
            sms_template = self.__getTemplate()
            try:
                self.__validateSmsDetails()
                content = sms_template % self.__data
                self.__msg = urllib.parse.quote_plus(content)
            except TreeboValidationException as e:
                raise e

    def _addAttachments(self, pdf_path, pdf_name):
        try:
            logger.info("Attaching %s located at %s" % (pdf_name, pdf_path))
            attachment = open(pdf_path + pdf_name, "rb").read()
            self.__msg.attach(pdf_name, attachment, 'application/pdf')
        except Exception as exc:
            logger.debug("Exception on _addAttachments = %s" % exc)

    def _setRecipients(self, receiver):
        self._recipients = receiver

    def _setFrom(self, from_address):
        self._from = from_address

    def _setSubject(self, subject):
        self.__subject = subject

    def _setData(self, data):
        self.__data = data

    def __validateEmailDetails(self):
        if self.__from is None:
            raise TreeboValidationException("email 'from' is empty")
        elif len(self.__subject) < 1:
            raise TreeboValidationException("email 'subject' is empty")
        elif self.__recipients is None or len(self.__recipients) == 0:
            raise TreeboValidationException("Email 'recipients' are missing")
        return True

    def __validateSmsDetails(self):
        if self.__data is None:
            raise TreeboValidationException("SMS message is empty")
        elif self.__recipients is None:
            raise TreeboValidationException("SMS 'recipients' are missing")
        return True
