import logging

from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.profiles.tasks import send_registration_email
from apps.referral.service.service import ReferralService
from common.exceptions.treebo_exception import TreeboException
from dbcommon.models.profile import User

logger = logging.getLogger(__name__)


class UserRegistration:
    auth_client = TreeboAuthClient()

    def register_user_with_token(self, token):
        logger.info(" register user with token %s ", token)
        user_data = self.auth_client.validate_token(token)
        email = user_data['email']
        phone = user_data['phone_number']
        first_name = user_data['first_name']
        last_name = user_data['last_name']
        auth_id = user_data['id']
        user = self.register(auth_id, email, phone, first_name, last_name)
        return user, user_data

    def register_internally(self, user_dto, click_id="", referral_code=""):
        logger.info(" creating user in v2  ")
        token_info = {}
        try:
            token_info = self.auth_client.register(user_dto)
        except TreeboException as e:
            logger.error(" register error " + str(e))
        user = self.register(
            token_info.get(
                'auth_id',
                None),
            user_dto['email'],
            user_dto['phone_number'],
            user_dto['first_name'],
            user_dto['last_name'])
        self.create_referral(user, referral_code, click_id)
        return user

    def register(
        self,
        auth_id,
        email,
        phone,
        first_name,
        last_name,
        throw_exception_on_existing_user=False):
        logger.info(
            " creating user in  email %s , phone %s , firstname %s and auth id %s",
            email,
            phone,
            first_name,
            auth_id)
        user, clean_email, phone = User.objects.get_existing_user(
            email, phone, auth_id, throw_exception_on_existing_user)
        if user:
            logger.info(
                " got user from search (%s %s %s) ",
                user.email,
                user.phone_number,
                user.auth_id)
        else:
            logger.info(" user not found (%s %s %s) ", email, phone, auth_id)
        user, is_new = self.create_or_update_user(
            user, auth_id, clean_email, phone, first_name, last_name)
        logger.info(
            "got user after creation %s %s %s ",
            user.email,
            user.phone_number,
            user.auth_id)
        if is_new:
            self.__send_communication_to_guest(user)
        return user

    def create_or_update_user(
        self,
        user_object,
        auth_id,
        email,
        phone_number,
        first_name,
        last_name):
        if user_object:
            if not user_object.auth_id:
                user_object.auth_id = auth_id
            is_new = False
        else:
            user_object, is_new = User.objects.get_or_create(
                email=email, auth_id=auth_id)
        user_object.phone_number = phone_number
        user_object.first_name = first_name
        user_object.last_name = last_name
        user_object.is_otp_verified = False
        user_object.save()
        return user_object, is_new

    def __send_communication_to_guest(self, user_object, send_email=True):
        try:
            if user_object and send_email:
                send_registration_email.delay(user_object.id)
        except Exception as e:
            logger.exception("  send_registration_email ")

    def fetch_referral_details_from_cookie(self, request):
        click_id = ''
        referral_code = ''
        referral_click_id = str(request.COOKIES.get(
            'avclk')) if request.COOKIES.get('avclk') else ""
        if referral_click_id:
            referral_code = referral_click_id.split('-')[1]
            click_id = referral_click_id
        return click_id, referral_code

    def create_referral(self, user, referrer_code, click_id):
        try:
            ReferralService.generate_referral_code(
                user, referrer_code=referrer_code, share_link_identifier=click_id)
        except Exception as e:
            logger.exception(
                " referral failed for  user %s %s %s  with error %s ",
                user,
                referrer_code,
                click_id)
