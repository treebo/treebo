class CrsApiFailed(RuntimeError):
    def __init__(self, crs, api, error_message):
        self.crs = crs
        self.api = api
        self.error_message = error_message

        message = "CRS({impl}) API '{a}' failed: {msg}".format(impl=crs, a=api, msg=error_message)
        super(CrsApiFailed, self).__init__(message)
