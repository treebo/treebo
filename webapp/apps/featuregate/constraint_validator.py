import logging
from datetime import datetime
from decimal import Decimal

from apps.featuregate import constants

logger = logging.getLogger(__name__)


class ConstraintValidator:
    def __init__(
            self,
            checkin,
            checkout,
            hotel_id,
            totalcost,
            locality,
            city,
            constraints,
            utm_source='',
            utm_medium=''):
        self.__constraints = constraints
        self.__checkin = checkin
        self.__checkout = checkout
        self.__hotel_id = hotel_id
        self.__totalcost = totalcost
        self.__locality = locality
        self.__city = city
        self.message = constants.SUCCESS
        self.__utm_source = utm_source
        self.__utm_medium = utm_medium

        self.__funcDict = {
            'hotel': self.__hotelValidator,
            'checkin': self.__checkinDateValidator,
            'checkout': self.__checkoutDateValidator,
            'cartcost': self.__cartCostValidator,
            'locality': self.__localityValidator,
            'city': self.__cityValidator,
            'utm_source': self.__utmSourceValidator,
            'utm_medium': self.__utmMediumValidator,
        }

    def validatorFactory(self, parsedTerm):

        if isinstance(parsedTerm, list):
            flag = False
            operand = parsedTerm[0]
            del parsedTerm[0]
            # if operand == "OR":
            for term in parsedTerm:
                result = self.validatorDelegate(term)
                if operand == "OR":
                    flag = flag or result
                elif operand == "AND":
                    flag = flag and result
            if flag:
                self.message = constants.SUCCESS
        else:
            result = self.validatorDelegate(parsedTerm)
            if result:
                self.message = constants.SUCCESS

    def validatorDelegate(self, constraintId):

        # aahmed : WEB-1916
        constraint = self.__constraints.get(constraintId)
        if not constraint:
            return False
        validatorFunction = self.__funcDict[constraint.keyword.keyword]

        return validatorFunction(
            constraint.operation.operator,
            constraint.value_one,
            constraint.value_two)

    def __hotelValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation, self.__hotel_id, vOne, vTwo, int):
            return True
        else:
            self.message = constants.FAILURE
            return False

    def __parser(self, date_string):
        return datetime.strptime(date_string, "%Y-%m-%d").date()

    def __checkinDateValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                self.__checkin,
                vOne,
                vTwo,
                self.__parser):
            return True
        else:
            self.message = constants.FAILURE
            return False

    def __checkoutDateValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                self.__checkout,
                vOne,
                vTwo,
                self.__parser):
            return True
        else:
            self.message = constants.FAILURE
            return False

    def __cartCostValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                self.__totalcost,
                vOne,
                vTwo,
                Decimal):
            return True
        else:
            self.message = constants.FAILURE
            return False

    def __localityValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation, self.__locality, vOne, vTwo, str):
            return True
        else:
            self.message = constants.FAILURE
            return False

    def __cityValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(operation, self.__city, vOne, vTwo, str):
            self.message = constants.SUCCESS
            return True
        else:
            self.message = constants.FAILURE
            return False

    def __utmSourceValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                self.__utm_source,
                vOne,
                vTwo,
                str):
            return True
        else:
            self.message = constants.FAILURE
            return False

    def __utmMediumValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                self.__utm_medium,
                vOne,
                vTwo,
                str):
            return True
        else:
            self.message = constants.FAILURE
            return False

    def __operatorSelection(self, operation, sourceVal, valOne, valTwo, cast):
        if operation == "eq":
            return cast(sourceVal) == cast(valOne)
        elif operation == "ls":
            return cast(sourceVal) < cast(valOne)
        elif operation == "gr":
            return cast(sourceVal) > cast(valOne)
        elif operation == "ge":
            return cast(sourceVal) >= cast(valOne)
        elif operation == "le":
            return cast(sourceVal) <= cast(valOne)
        elif operation == "between":
            return cast(valOne) <= cast(sourceVal) <= cast(valTwo)
        elif operation == "in":
            valList = [cast(val.strip()) for val in valOne.split(",")]
            return cast(sourceVal) in valList
        elif operation == "textmatch":
            return sourceVal.lower() == valOne.lower()
        elif operation == 'not_in':
            valList = [cast(val.strip()) for val in valOne.split(",")]
            return cast(sourceVal) in valList
        elif operation == 'ne':
            return cast(sourceVal) == cast(valOne)
        elif operation == 'exclude_range':
            return not (cast(sourceVal) < cast(valOne)
                        or cast(sourceVal) > cast(valTwo))
