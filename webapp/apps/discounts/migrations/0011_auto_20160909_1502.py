# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discounts', '0010_auto_20160804_1800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discountcouponconstraints',
            name='value_one',
            field=models.CharField(max_length=600, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='discountcouponconstraints',
            name='value_two',
            field=models.CharField(max_length=600, null=True, blank=True),
        ),
    ]
