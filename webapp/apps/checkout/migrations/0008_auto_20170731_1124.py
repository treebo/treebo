# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0007_auto_20170503_1358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='booking_channel',
            field=models.CharField(
                default='',
                max_length=50,
                blank=True,
                choices=[
                    (b'msite',
                     b'msite'),
                    (b'website',
                     b'website'),
                    (b'app',
                     b'app'),
                    (b'gdc',
                     b'gdc')]),
        ),
    ]
