from apps.seo.components.base_component import BaseComponent
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.components.base_component import BaseComponent
from apps.seo.serializers.serializer import LandmarkSerializer
from data_services.respositories_factory import RepositoriesFactory

__author__ = 'ansilkareem'


class Landmarks(BaseComponent):

    search_landmark_repository = RepositoriesFactory.get_landmark_repository()

    def __init__(self, current_city, footer_data, exclude_list):
        super(
            Landmarks,
            self).__init__(
            current_city,
            footer_data,
            exclude_list)

    def build(self):
        data = {
            'key': 'landmark',
            'title': 'LANDMARKS',
            'content': AutoCompleteResultBuilder.build_landmark_result(
                self.search_landmark_repository.get_landmarks_for_city_with_exclusion(
                    city_name=self.current_city.name,
                    enable_for_seo=True,
                    city_enabled_for_seo=True,
                    excluded_ids_list=self.exclude_list))} if self.footer_data and self.current_city else {
            'key': 'landmark',
            'title': 'LANDMARKS',
            'content': []}

        serialized_obj = LandmarkSerializer(data=data)
        if serialized_obj.is_valid():
            return serialized_obj.validated_data
        else:
            return {'key': 'landmark',
                    'title': 'LANDMARKS',
                    'content': []
                    }
