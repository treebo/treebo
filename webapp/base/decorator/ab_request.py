import functools
import logging
from apps.ab.services.ab_client_services import ABClientServices
logger = logging.getLogger(__name__)


def ab_request():
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, request, *args, **kwargs):
            ABClientServices().get_user_buckets_from_ab(request)
            return func(cls, request, *args, **kwargs)
        return wrapper
    return decorator

