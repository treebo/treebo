from django.db import models


class ErrorCodes(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)
    message = models.CharField(max_length=150)

    class Meta:
        db_table = 'common_error_codes'
        verbose_name = 'Errorcode'
        verbose_name_plural = 'Errorcodes'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
