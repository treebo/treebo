# -*- coding: utf-8 -*-


from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps import views as sitemap_views
from django.views.decorators.cache import cache_page

from apps.third_party.api.v1.hotel_priority_flag_upload import HotelPriorityFlag
from .apps.hotels.api.v1.facilities import AllFacilitiesAPI
from .apps.hotels.api.v1.get_all_cities import CityInformation
from .apps.hotels.api.v1.get_hotel_map_url import HotelMapLocationUrl
from .apps.hotels.api.v2.cities import Cities
from .apps.phone_number_check.api import PhoneNumberValidation
from .apps.hotels.api.v1.state_sync import StateSyncTrigger
from .apps.pricing.api.v1.views.tax_sync import TaxSyncTrigger, LuxuryTaxSyncTrigger
from .apps.pricing.api.v1.views.promo_sync import PromoSyncTrigger
from .apps.discounts.api.v2.hotel_coupon_for_auto_apply_upload import HotelCouponForAutoApply
from .apps.checkout.api.v6.upfront_partpayment_hotel_upload import UpfrontPartPaymentHotelUpload
from .apps.posts.views import get_page_choices
from .base.middlewares.exception import handle404
from django.contrib.sitemaps.views import sitemap
from django.conf.urls import url
from .apps.bookings.api.v1.booking_detail import BookingDetail

admin.autodiscover()

CACHE_TIME_INTERVAL = 60 * 15

handler404 = handle404
# handler500 = 'apps.common.views.handler500'

from .sitemap import HotelSitemap, LandmarkSitemap, LocalitySitemap, CategorySitemap, NearMeSitemap, CityAmenitySitemap
from .sitemap import CitySitemap, LocalityCategorySitemap, LandmarkCategorySitemap
from .sitemap import StaticViewSitemap, HomeSitemap

sitemaps = dict(stat=StaticViewSitemap(), hotel=HotelSitemap(),
                category=CategorySitemap(), landmark=LandmarkSitemap(), locality=LocalitySitemap(), city=CitySitemap(),
                home=HomeSitemap(),
                nearme=NearMeSitemap(), locality_category=LocalityCategorySitemap(),
                landmark_category=LandmarkCategorySitemap(), city_amenity=CityAmenitySitemap())

urlpatterns = [
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/discounts/autoapply/upload',
        HotelCouponForAutoApply.as_view(),
        name="auto_apply_upload"),
    url(r'^admin/upfront/partpay/hotel/upload',
        UpfrontPartPaymentHotelUpload.as_view(),
        name="upfront_partpay_hotel_upload"),
    url(r'^admin/third_party/hotelpriorityflag/upload$', HotelPriorityFlag.as_view(),
        name="hotel_priority_flag_upload"),
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/cities/$', CityInformation.as_view(), name='cities'),
    url(r'^api/v2/cities/$', Cities.as_view(), name='cities_v2'),
    url(r'^api/v1/facilities/$', AllFacilitiesAPI.as_view(), name='facilities'),
    url(r'^api/v1/states/(?P<state_id>\d+)/sync$',
        StateSyncTrigger.as_view(), name='state_sync_trigger'),
    url(r'^api/v1/taxes/(?P<state_id>\d+)/sync$',
        TaxSyncTrigger.as_view(), name='tax_sync_trigger'),
    url(r'^api/v1/taxes/hotels/(?P<hotel_id>\d+)/sync$',
        LuxuryTaxSyncTrigger.as_view(), name='tax_sync_trigger'),
    url(r'^api/v1/promo/(?P<hotel_id>\d+)/sync/$',
        PromoSyncTrigger.as_view(), name='promo_sync_trigger'),
    # Need short API format to be used in Mobile SMS
    url(r'^maps/(?P<hotel_id>\d+)/$',
        HotelMapLocationUrl.as_view(),
        name='hotel_map_location'),
    url(r'^api/v1/booking_detail/(?P<booking_id>[\w\-]+)/$', BookingDetail.as_view(), name='booking_detail'),
    url(r'^api/v1/is_valid_phone_number?$', PhoneNumberValidation.as_view(), name='phone_number_validation'),

    # TODO: DEPRECATE BELOW
    # Moved to hotels.api.v1.urls
    # url(r"^detail/(?P<hashedHotelId>\w+)/$", HotelDetails.as_view(), name='details'),
    # url(r"^room/(?P<hashed_hotel_id>\w+)/$", HotelRooms.as_view(), name='room'),

    url('', include('apps.pages.urls_2')),
    url('', include('django.contrib.auth.urls')),

    url(r'^api/v1/auth/', include('apps.auth.api.v1.urls')),
    url(r'^api/v1/ab/', include('apps.ab.urls')),
    url(r'^api/v2/auth/', include('apps.auth.api.v2.urls')),
    url(r'^api/v2/otp', include('apps.auth.api.v2.urls_otp')),
    url(r'^api/v3/otp', include('apps.auth.api.v3.urls')),
    url(r'^api/v1/contents/', include('apps.content.urls')),
    url(r'^api/v2/contents/', include('apps.content.api.v2.urls')),
    url(r'^api/v3/contents/', include('apps.content.api.v3.urls')),
    url(r'^api/v1/hotels/', include('apps.hotels.api.v1.urls')),
    url(r'^api/v2/hotels/', include('apps.hotels.api.v2.urls')),
    url(r'^api/v3/hotels/', include('apps.hotels.api.v3.urls')),
    url(r'^api/v4/hotels/', include('apps.hotels.api.v4.urls')),
    url(r'^api/v5/hotels/', include('apps.hotels.api.v5.urls')),
    url(r'^api/v1/referral/', include('apps.referral.urls')),
    url(r'^api/v1/search/', include('apps.search.urls')),
    url(r'^api/v2/search/', include('apps.search.urls_v2')),
    url(r'^api/v3/search/', include('apps.search.urls_v3')),
    url(r'^api/v4/search/', include('apps.search.urls_v4')),
    url(r'^api/v1/profile/', include('apps.profiles.urls')),
    url(r'^api/v1/discount/', include('apps.discounts.urls', namespace='discount')),
    url(r'^api/v1/feature-gate/', include('apps.featuregate.urls')),
    url(r'^api/v1/booking/', include('apps.bookings.urls')),
    url(r'^api/v1/payment/', include('apps.payments.api.v1.urls')),
    url(r'^api/v2/payment/', include('apps.payments.api.v2.urls', )),
    url(r'^api/v1/pricing/', include('apps.pricing.api.v1.urls', )),
    url(r'^api/v2/pricing/', include('apps.pricing.api.v2.urls', )),
    url(r'^api/v3/pricing/', include('apps.pricing.api.v3.urls', )),
    url(r'^api/v1/checkout/', include('apps.checkout.api.v1.urls', )),
    url(r'^api/v2/checkout/', include('apps.checkout.api.v2.urls', )),
    url(r'^api/v3/checkout/', include('apps.checkout.api.v3.urls', )),
    url(r'^api/v4/checkout/', include('apps.checkout.api.v4.urls', )),
    url(r'^api/v5/checkout/', include('apps.checkout.api.v5.urls', )),
    url(r'^api/v6/checkout/', include('apps.checkout.api.v6.urls', )),
    url(r'^api/v7/checkout/', include('apps.checkout.api.v7.urls', )),
    url(r'^api/v1/promotions/', include('apps.promotions.api.v1.urls', )),
    url(r'^api/v1/common/', include('apps.common.urls', )),
    url(r'^api/v1/reviews/', include('apps.reviews.urls', )),
    url(r'^api/v1/growth/', include('apps.growth.urls', )),
    url(r'^api/v1/posts/', include('apps.posts.urls', )),
    url(r'^api/v2/discount/', include('apps.discounts.api.v2.urls', )),
    url(r'^api/v1/discount-promotions/', include('apps.discounts.api.v1.urls', )),
    url(r'^api/v2/booking/', include('apps.bookings.api.v2.urls', )),
    url(r'^api/v3/booking/', include('apps.bookings.api.v3.urls', )),
    url(r'^api/v4/pricing/', include('apps.pricing.api.v4.urls', )),
    url(r'^api/v5/pricing/', include('apps.pricing.api.v5.urls', )),
    url(r'^api/v6/pricing/', include('apps.pricing.api.v6.urls')),
    url(r'^api/v7/pricing/', include('apps.pricing.api.v7.urls')),
    url(r'^api/v1/availability/', include('apps.bookingstash.api.v1.urls', )),
    url(r'^api/v1/reward/', include('apps.reward.urls', )),
    url(r'^api/v1/hotel-migration/', include('apps.hx_to_crs_hotel_migration.api.v1.urls', )),
    url(r'^api/v1/true-caller/', include('apps.true_caller.api.v1.urls', )),
    url(r'^api/dbcommon/', include('webapp.dbcommon.urls', )),
    url(r'^api/v1/jobs/', include('webapp.common.job.jobs.urls', )),

    # External URLs, moved to pricing.api.v1.urls.
    # TODO: Update the urls on third-party vendors
    url(r'^/', include('apps.pricing.urls', )),
    url(r"^fot/", include('apps.fot.urls', )),
    url(r'^rest/v1/bookingstash/', include('apps.bookingstash.urls', )),
    url(r'^rest/v1/', include('apps.apiv1.urls', )),
    url(r'^api/v1/external/', include('apps.third_party.urls', )),

    url(r'^qa/page_type/choices', get_page_choices, name='qa_page_types'),
    url(r'^api/v2/discounts/', include('apps.discounts.api.v2.urls', )),
    url(r'^api/retryCommunication/', include('apps.communication_retry_api.urls')),
    # url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^sitemap.xml$', cache_page(5000)(sitemap_views.index),
        {'sitemaps': sitemaps, 'sitemap_url_name': 'sitemaps'}),
    url(r'^sitemap-(?P<section>.+).xml$', cache_page(5000)(sitemap_views.sitemap), {'sitemaps': sitemaps},
        name='sitemaps'),
    url(r'^robots.txt$', include('robots.urls')),

    # url(r"^hotels-in-(?P<cityString>.*)/$", seo_friendly.SeoFriendly.as_view()),

    # --------- Deprecated urls --------------
    url(r'', include('deprecated_urls', )),

    # ---------------- Health Check API ----------------
    url(r'^api/health/$', include('health_check.urls')),
]

if settings.DEBUG:
    urlpatterns += [url(r'^apidocs/', include('rest_framework_swagger.urls'))]

# if settings.DEBUG:
#     schema_view = get_swagger_view(title='API')
#
#     urlpatterns += [
#         url(r'^apidocs/', schema_view)
#     ]
if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
                      url(r'^__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# TODO: fix the suffix format issue - Kaddy
# urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])
#
# from django.template.base import add_to_builtins
#
# add_to_builtins('apps.hotels.templatetags.url_tag')
