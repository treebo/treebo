import logging

from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django_currentuser.middleware import get_current_user

from apps.common.utils import mangle_emails

logger = logging.getLogger(__name__)


# pylint: disable=too-many-instance-attributes
class Email:

    def __init__(self, subject, to_list, cc_list=None, bcc_list=None):
        self.subject = subject

        self._to_list = []
        self._cc_list = []
        self._bcc_list = []

        self.to_list = to_list
        self.cc_list = cc_list
        self.bcc_list = bcc_list
        self.text = ''
        self.html = ''

        self.attachments = []
        self.attachment_urls = []

    @property
    def to_list(self):
        return [eml for eml in self._to_list if self.is_valid_email(eml)]

    @property
    def cc_list(self):
        return [eml for eml in self._cc_list if self.is_valid_email(eml)]

    @property
    def bcc_list(self):
        return [eml for eml in self._bcc_list if self.is_valid_email(eml)]

    @to_list.setter
    def to_list(self, value):
        if value is None:
            return
        self._to_list = set(value)

    @cc_list.setter
    def cc_list(self, value):
        if value is None:
            return

        value = set(email for email in value if email not in self.to_list)
        self._cc_list = set(value)

    @bcc_list.setter
    def bcc_list(self, value):
        if value is None:
            return

        value = set(email for email in value if (email not in self.to_list and
                                                 email not in self.cc_list))
        self._bcc_list = set(value)

    def mangle_emails(self):
        self.to_list = mangle_emails(self.to_list) + [get_current_user().email]
        self.cc_list = mangle_emails(self.cc_list)
        self.bcc_list = mangle_emails(self.bcc_list)

    def mangle_subject(self, env):
        self.subject = '[{e}] {s}'.format(e=env, s=self.subject)

    @classmethod
    def is_valid_email(cls, email):
        try:
            validate_email(email)
            return True
        except ValidationError:
            logger.warn('Discarding invalid email: {e}'.format(e=email))
