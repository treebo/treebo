
let treejax = require('./treebolibs/treejax');
let $ = require('jquery');
let queryString = require('query-string');
let cookies = require('js-cookie');
let _ = require('underscore');
let parsed = queryString.parse(location.search);
let $loader = $("#commonLoader");
let $csrfmiddlewaretoken= $('input[name="csrfmiddlewaretoken"]').val();

let fbDefer = $.Deferred();
let googleDefer = $.Deferred();

const auth = {
    login(email, password, reject, showLoader) {
        $loader.removeClass("hide");
        const p = treejax({
                url: '/api/v1/auth/login/',
                type: 'POST',
                data: {
                 email,
                 password,
                 csrfmiddlewaretoken:$csrfmiddlewaretoken
                }
            }).then((data, status, xhr) => {
                $('input[name="csrfmiddlewaretoken"]').val(cookies.get('csrftoken'));
                return data;
            });

            p.finally(() => {
                $loader.addClass("hide");
            });
            return p;
    },
    signup(data, reject) {
        var reject = reject || function (treeboError) { console.log(treeboError); }
        const self = this;
        $loader.removeClass("hide");
        const p = treejax({
                url: '/api/v1/auth/register/',
                type: 'POST',
                data: {
                    name: data.name,
                    mobile: data.mobile,
                    email: data.email,
                    password: data.password,
                    csrfmiddlewaretoken:$csrfmiddlewaretoken,
										page: rt.page
                }
            }).then((data, status, xhr) => {
								self.setSignupChannelCookie('EMAIL');
								rt.userId = data.userId;
								self.alias();
								$('input[name="csrfmiddlewaretoken"]').val(cookies.get('csrftoken'));
								return data;
						},reject);
						p.finally(() => {
								$loader.addClass("hide");
						});
						return p;
    },
    forgotpassword(email, reject) {
        var reject = reject || function (treeboError) { console.log(treeboError); }
        $loader.removeClass("hide");

        const p = treejax({
                url: '/api/v1/auth/forgot-password/',
                type: 'POST',
                data: {
                    email,
                    csrfmiddlewaretoken:$csrfmiddlewaretoken
                }
            }).then((data, status, xhr) => {
                $('.alert').addClass('hide');
                $(`.forgot__${data.status}`).html(data.msg).removeClass('hide');
            },reject);

            p.finally(() => {
                $loader.addClass("hide");
            });
        return p;
    },
	attemptLogin(response) {
        if (response.authResponse)
        {
            const accessToken = response.authResponse.accessToken; //get access token
            const userId = response.authResponse.userID; //get FB UID
            let imageUrl;
            const self = this;
            FB.api(`/${userId}/picture?type=large`, response => {
                imageUrl = response.data.url;
                FB.api('/me?fields=name,email,gender', response => {
                    self.sendRegistrationDataToServer(imageUrl, response, accessToken)
                });
            });
		}else {

        }
    },
    sendRegistrationDataToServer(imageUrl, response, accessToken) {
        const userEmail = response.email; //get user email
        let p ='';
        const self = this;
        let page = rt.page;
        if (userEmail) {
            $loader.removeClass("hide");
            p = treejax({
                type: 'POST',
                url: '/api/v1/auth/fbLogin/',
                data: {
                    imageUrl,
                    email: userEmail,
                    accessToken,
                    csrfmiddlewaretoken:$csrfmiddlewaretoken,
										page: rt.page
                }
            }).then((data, status, xhr) => {
                    $('input[name="csrfmiddlewaretoken"]').val(cookies.get('csrftoken'));
                    if(data.register){
                        self.setSignupChannelCookie('FB');
                        rt.userId = data.userId;
                        self.alias();
                    }
										fbDefer.resolve(data);
                },data => {
                    // analytics.track('e_cs_err#fb_signin_error', {error_msg: data['message']});
										fbDefer.reject(data);
                }
            );

            p.finally(() => {
                $loader.addClass("hide");
            });

        }
        else {
            // TODO: Show some response if permission denied
        }
    },
    sendGoogleRegistrationDataToServer(authToken) {
        const self = this;
        $loader.removeClass("hide");
        let  p = treejax({
            type: 'POST',
            url: '/api/v1/auth/googleLogin/',
            data: {
                auth_token: authToken,
								csrfmiddlewaretoken:$csrfmiddlewaretoken,
								page: rt.page
            }
        }).then((data, status, xhr) => {

                $('input[name="csrfmiddlewaretoken"]').val(cookies.get('csrftoken'));
                // analytics.track('e_cs_resp#fb_signin_auth_success', {success_code: data['msg']});
                if(data.register){
                    self.setSignupChannelCookie('GOOGLE');
                    rt.userId = data.userId;
                    self.alias();
                }
                return data;
            },data => {
                // analytics.track('e_cs_err#fb_signin_error', {error_msg: data['message']});
            }
        );
        return p;
    },
    fbLogin() {
        FB.login(_.bind(this.attemptLogin,this), {scope: 'email', auth_type: 'rerequest'});
				return fbDefer.promise();
    },
    loadFB() {
    	window.fbAsyncInit = function () {
		    FB.init({
		        appId: window.rt.fbAppId, //localhost app id
		        oauth: true,
		        status: true, // check login status
		        cookie: true, // enable cookies to allow the server to access the session
		        xfbml: true, // parse XFBML
		        version: 'v2.5'
		    });
		  };
		// Load the SDK asynchronously
  		(function (d, s, id) {
            let js;
            let fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(window.document, 'script', 'facebook-jssdk'));
    },
    loadGoogle() {
        window.initGoogleSignIn = function() {
            gapi.load('auth2', () => {
                const auth2 = gapi.auth2.init({cookie_policy: 'single_host_origin', requestvisibleactions: '//schemas.google.com/AddActivity'});
                googleDefer.resolve(auth2);
            });
        };
    },
    getGoogleHandler() {
        return googleDefer.promise();
    },
    bindEvents() {

    },
    alias() {
        rt.clientAnalyticsObj.alias(rt.userId);
    },
    setSignupChannelCookie(channel) {
        cookies.set('user_signup_channel',channel);
        cookies.set('user_signup_date',new Date());
    }
};

auth.loadFB();
auth.loadGoogle();

module.exports = auth;
