

from django.conf.urls import url
from apps.pricing.api.v1.views import channel_manager
app_name = 'pricing'
urlpatterns = [
    url(r"^updateRateAmount", channel_manager.UpdateRateAmount.as_view(), name='updateRateAmount'),
    url(r"^updateHotelAvailability", channel_manager.UpdateHotelAvailability.as_view(),
        name='updateRateAmount'),
    url(r"^getHotelRatePlan", channel_manager.GetHotelRatePlan.as_view(), name='getHotelRatePlan'),
    url(r"^getPromotions", channel_manager.GetPromotions.as_view(), name='getPromotions'),
    url(r"^setPromotions", channel_manager.SetPromotions.as_view(), name='setPromotions'),
    url(r"^deletePromotions", channel_manager.DeletePromotions.as_view(), name='deletePromotions'),
]
