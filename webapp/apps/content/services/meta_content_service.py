from django.conf import settings
from django.db.models import Q

from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Categories, CategorySeoDetails, CitySeoDetails, SeoDetails
from dbcommon.models.landmark import SearchLandmark, LandmarkSeoDetails
from dbcommon.models.location import Locality, LocalitySeoDetails, City
from django.http import Http404
from django.utils.text import slugify
from apps.hotels.serializers.seo_serializer import HotelSeoDetailsSerializer
from apps.content import constants

city_data_service = RepositoriesFactory.get_city_repository()
search_landmark_repository = RepositoriesFactory.get_landmark_repository()


class MetaContentService(object):

    def override_seo_content(
            self,
            meta_content,
            title,
            description,
            keywords=None,
            canonical_url=None,
            city_name=None):
        page = meta_content["page"]
        page["title"] = title
        if canonical_url:
            # Making a list to add more links for different rel
            meta_content['link'] = [{'rel': 'canonical',
                                     'href': canonical_url
                                     }]
        for meta in meta_content["meta"]:
            if meta.get("property") == "og:title":
                meta["content"] = title
            elif (meta.get("property") == "og:description" or meta.get("name") == "description"):
                meta["content"] = description
            elif meta.get("name") == "keywords" and keywords:
                meta["content"] = keywords

        for i, schema in enumerate(meta_content["schema"]):
            if city_name:
                schema["name"] = 'Treebo Hotels in {}'.format(city_name)
            else:
                schema["name"] = title
            schema["description"] = description
            schema["url"] = canonical_url or schema.get('url', '')

    def update_meta(self, meta_content, page_name, query, seo_pages):
        if page_name == constants.PAGES.HD:
            hotel_id = query
            seo_details = SeoDetails.objects.filter(hotel_id=hotel_id).first()
            if seo_details:
                serialized_seo_details = HotelSeoDetailsSerializer(
                    instance=seo_details).data
                full_canonical_url = ''
                canonical_url = serialized_seo_details.get("canonical_url")
                if canonical_url:
                    full_canonical_url = settings.SITE_HOST_NAME + canonical_url

                self.override_seo_content(
                    meta_content,
                    serialized_seo_details["title"],
                    serialized_seo_details["description"],
                    serialized_seo_details["keyword"],
                    full_canonical_url)
        elif page_name == constants.PAGES.CITY:
            city_slug = query
            city = city_data_service.get_by_slug(city_slug)
            if city:
                city_seo_details = CitySeoDetails.objects.get(city=city)
                full_canonical_url = settings.SITE_HOST_NAME + \
                    'hotels-in-' + slugify(city.slug) + '/'
                if city_seo_details:
                    self.override_seo_content(
                        meta_content,
                        city_seo_details.citySeoTitleContent,
                        city_seo_details.citySeoDescriptionContent,
                        city_seo_details.citySeoKeywordContent,
                        full_canonical_url,
                        city.name)
                else:
                    meta_content['link'] = [{'rel': 'canonical',
                                             'href': full_canonical_url
                                             }]

        elif page_name == constants.PAGES.LANDMARK:
            landmarks = search_landmark_repository.get_landmarks_by_freetext_url(
                query_string=' '.join(query.split('-')))
            if landmarks:
                landmark = landmarks[0]
            else:
                landmark = None

            if not landmark:
                city = query.split('-')[-1]
                landmark_name = ' '.join(query.split('-')[:-1])
                landmarks = search_landmark_repository.get_landmark_for_seo_url_and_city_name(
                    landmark_name=landmark_name, city_name=city, )
                if landmarks:
                    landmark = landmarks[0]
                else:
                    landmark = None

            if landmark:
                current_city = landmark.city
                landmark_seo_details = LandmarkSeoDetails.objects.filter(
                    landmark=landmark).first()
                if landmark.free_text_url:
                    full_canonical_url = settings.SITE_HOST_NAME + \
                        'hotels-near-' + slugify(landmark.free_text_url) + '/'
                else:
                    full_canonical_url = settings.SITE_HOST_NAME + 'hotels-near-' + slugify(
                        landmark.seo_url_name) + \
                        '-' + slugify(current_city.slug) + '/'
                if landmark_seo_details:
                    self.override_seo_content(
                        meta_content,
                        landmark_seo_details.landmark_seo_title,
                        landmark_seo_details.landmark_seo_description,
                        landmark_seo_details.landmark_seo_keyword,
                        full_canonical_url,
                        landmark.free_text_url if landmark.free_text_url else landmark.seo_url_name)
                else:
                    meta_content['link'] = [{'rel': 'canonical',
                                             'href': full_canonical_url
                                             }]
        elif page_name == constants.PAGES.LOCALITY:
            city = query.split('-')[-1]
            locality_name = ' '.join(query.split('-')[:-1])
            locality = Locality.objects.filter(Q(name__iexact=locality_name) & Q(
                Q(city__name__iexact=city) | Q(city__slug__iexact=city))).first()
            if locality:
                current_city = locality.city
                locality_seo_details = LocalitySeoDetails.objects.filter(
                    locality=locality).first()
                full_canonical_url = settings.SITE_HOST_NAME + 'hotels-in-' + \
                    slugify(locality.name) + '-' + slugify(current_city.slug) + '/'
                if locality_seo_details:

                    self.override_seo_content(
                        meta_content,
                        locality_seo_details.locality_seo_title,
                        locality_seo_details.locality_seo_description,
                        locality_seo_details.locality_seo_keyword,
                        full_canonical_url,
                        locality.name)
                else:

                    meta_content['link'] = [{'rel': 'canonical',
                                             'href': full_canonical_url
                                             }]
        elif page_name == constants.PAGES.CATEGORY:
            city = query.split('-')[-1]
            category_name = ' '.join(query.split('-')[:-1])
            category = Categories.objects.filter(
                name__iexact=category_name).first()
            if category:
                city_search_args = []
                city_search_args.append(
                    {'name': 'name', 'value': query, 'case_sensitive': False, 'partial': False})
                city_search_args.append(
                    {'name': 'slug', 'value': query, 'case_sensitive': False, 'partial': False})
                current_cities = city_data_service.filter_cities_by_string_matching(
                    city_search_args)
                if current_cities:
                    current_city = current_cities[0]
                else:
                    current_city = None
                category_seo_details = CategorySeoDetails.objects.filter(
                    category=category, city=current_city).first()
                full_canonical_url = settings.SITE_HOST_NAME + \
                    slugify(category.name) + '-in-' + slugify(current_city.slug) + '/'
                if category_seo_details:
                    self.override_seo_content(
                        meta_content,
                        category_seo_details.category_seo_title,
                        category_seo_details.category_seo_description,
                        category_seo_details.category_seo_keyword,
                        full_canonical_url,
                        category.name)
                else:
                    meta_content['link'] = [{'rel': 'canonical',
                                             'href': full_canonical_url
                                             }]

        elif page_name in seo_pages:
            seo_page_details = seo_pages[page_name]
            self.override_seo_content(meta_content,
                                      seo_page_details.get('title'),
                                      seo_page_details.get('description'))
        return meta_content
