from django.db import models


# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=30)
    days_to_check_in = models.IntegerField()
    time_dependant = models.BooleanField(default=False)
    start_time = models.TimeField(default="00:00:00")
    end_time = models.TimeField(default="00:00:00")

    def __str__(self):
        return " || " + self.name + " | " + \
            str(self.start_time) + " - " + str(self.end_time) + " || "

    class Meta:
        verbose_name = "category"
        verbose_name_plural = "categories"
