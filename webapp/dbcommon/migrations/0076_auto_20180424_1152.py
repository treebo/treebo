# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0075_auto_20180201_1000'),
    ]

    operations = [
        migrations.AddField(
            model_name='locality',
            name='seo_url_name',
            field=models.CharField(default=b'', max_length=500),
        ),
        migrations.AlterField(
            model_name='hotelmetadata',
            name='key',
            field=models.CharField(max_length=100, choices=[(b'b2b_blacklist', b'b2b_blacklist'), (b'sorted_value', b'sorted_value')]),
        ),
    ]
