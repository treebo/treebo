# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('fot', '0003_fot'),
    ]

    operations = [
        migrations.CreateModel(
            name='FotCredits',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('earned', models.IntegerField()),
                ('fot', models.ForeignKey(to='fot.Fot', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_fotcredits',
            },
        ),
    ]
