# pylint: disable = no-member
# -*- coding:utf-8 -*-
"""
     Booking Service provides api for
     : create_booking    : combines create temp booking and confirms it in hotelogix
     : initiate_booking  : create temp booking in hotelogix
     : confirm_booking   : confirms booking in hotelogix
     : cancel_booking    : cancel booking in hotelogix
     Note : all booking flows are async
"""
import logging
import random
import string
from decimal import Decimal

from apps.auth.services.user_profile_service import UserProfileService
from apps.bookings.integrations.booking.treebo import TreeboCRSWrapper
from apps.bookings.models import Booking
from apps.bookings.serializers import BookingCreationSerializer
from apps.common.exceptions.custom_exception import CreateBookingValidationError

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)


class BookingService:
    """
        BookingService
    """

    def __init__(self):
        pass

    @staticmethod
    def create_booking(booking_data, booking_request=None):
        """
        Creates a booking in central booking service
        :rtype: Booking
        """
        logger.info(
            "BookingService:create_booking called with booking_data: %s",
            str(booking_data))

        # if Decimal(booking_data.get('total_amount')) == 0:
        #     return None

        booking = Booking.objects.filter(order_id=booking_data.get('order_id')).first()

        # if booking object already present
        if booking:
            booking_data["order_id"] = booking.order_id
            serializer = BookingCreationSerializer(data=booking_data)
            if serializer.is_valid():
                booking = serializer.update(booking, serializer.validated_data)
            else:
                raise CreateBookingValidationError(serializer.errors)
        else:
            booking_data["order_id"] = BookingService.generate_order_id()
            booking_data['booking_status'] = Booking.INITIATED
            logger.info(
                "Creating new Booking with order id %s ",
                booking_data["order_id"])
            serializer = BookingCreationSerializer(data=booking_data)
            if serializer.is_valid():
                booking = serializer.create(serializer.validated_data)
            else:
                raise CreateBookingValidationError(serializer.errors)
        booking_request.order_id = booking.order_id
        booking_request.booking_id = booking.id
        booking_request.save()
        TreeboCRSWrapper.create_booking(booking.order_id, booking_request.user, booking_request.user_login_state)

        logger.info(
            "Booking created with [id, order_id]=[%s, %s]",
            booking.id,
            booking.order_id,
        )
        booking = Booking.objects.filter(order_id=booking_data.get('order_id')).first()
        return booking

    @staticmethod
    def confirm_booking(booking):
        """
        Confirms a temporary booking in central booking service
        :param apps.bookings.models.Booking booking:
        :return:
        """
        logger.info(
            "confirm_booking method called with order_id: %s",
            booking.order_id)
        booking = TreeboCRSWrapper.update_booking(booking.order_id)
        return booking

    @staticmethod
    def update_gst_detail(booking_id):
        """
        Updates the gst detail in External Reservation System associated with a booking
        :param booking_id: booking id
        :param job_id: create (for PAH) / confirm (for Pay Now) booking async task's job id
        :return:
        """
        TreeboCRSWrapper.update_booking(booking_id)

        logger.info("Updated gst detail for booking id: {0}"
                    .format(booking_id))

    @staticmethod
    def cancel_booking(booking, **kwargs):
        """
        cancel a booking in the central booking service
        :param order_id: order id of the booking
        :return:
        """
        logger.info(
            "cancel_booking method called with order_id: %s",
            booking.order_id)
        booking = TreeboCRSWrapper.cancel_booking(booking.order_id, **kwargs)
        return booking

    @staticmethod
    def generate_order_id():
        """
        :return: order_id
        """
        order_id = "TRB-{0}{1}".format(
            BookingService.__generate_unique_id(5),
            BookingService.__generate_unique_id(6))
        return order_id

    @staticmethod
    def __generate_unique_id(size=5):

        return ''.join(random.choice(string.digits) for _ in range(size))
