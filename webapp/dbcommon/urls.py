
from django.conf.urls import include, url
from django.conf.urls.static import static

from dbcommon.api.web_cs_api import WebCsTransformerAPI

urlpatterns = [
    url(r'^hotels$',WebCsTransformerAPI.as_view(),name='web-cs-transformer')]