# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.payments.api.v1.generate_payment_info import GeneratePaymentInfo
from apps.payments.api.v1.payment_details import PaymentDetails
from apps.payments.api.v1.paymentorder_details import PaymentOrderDetails
from apps.payments.api.v1.update_payment_info import UpdatePaymentInfo
from apps.payments.api.v1.payment_offers import PaymentOffers
from apps.payments.api.v1.payment_methods import PaymentMethods
from apps.payments.api.v1.apply_payment_offer import ApplyPaymentOffer

urlpatterns = [url(r'^update-payments/$',
                   UpdatePaymentInfo.as_view(),
                   name="update-payment_v1"),
               url(r'^generate-payments/$',
                   GeneratePaymentInfo.as_view(),
                   name="generate-payment_v1"),
               url(r'^payments-details/$',
                   PaymentOrderDetails.as_view(),
                   name="paymentorder-details_v1"),
               url(r'^payments-details/(?P<ps_orderid>ORD-[0-9A-Z]+)/$',
                   PaymentDetails.as_view(),
                   name="payment-details_v1"),
               url(r'^payment-offers/$',
                   PaymentOffers.as_view(),
                   name="payment-offers_v1"),
               url(r'^payment-method/price/$',
                   ApplyPaymentOffer.as_view(),
                   name="payment-method_price_v1"),
               url(r'^payment-methods/$',
                   PaymentMethods.as_view(),
                   name="payment-methods_v1"),

               ]
