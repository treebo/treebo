import logging

from common.exceptions.treebo_exception import TreeboValidationException
from services.tasks import send_email

logger = logging.getLogger(__name__)


class EmailService():
    @staticmethod
    def send_email_from_template(
            emails,
            subject,
            template_path,
            context,
            content_type="html"):
        """
        :param [str] emails: Array of email strings
        :param str subject:
        :param str template_path:
        :param dict context:
        :param str content_type:
        """
        try:
            EmailService.__validateEmailDetails(emails, subject)
            for email in emails:
                send_email.delay(
                    [email],
                    subject,
                    template_path,
                    context,
                    content_type)
        except TreeboValidationException as e:
            logger.error(e)

    @staticmethod
    def __validateEmailDetails(emails, subject):
        if subject < 1:
            raise TreeboValidationException("email 'subject' is empty")
        if emails is None or len(emails) == 0:
            raise TreeboValidationException("Email 'recipients' are missing")
        return True
