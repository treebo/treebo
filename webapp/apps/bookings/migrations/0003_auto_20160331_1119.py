# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0002_auto_20160218_0256'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='pretax_amount',
            field=models.DecimalField(
                null=True,
                max_digits=20,
                decimal_places=10),
        ),
        migrations.AddField(
            model_name='booking',
            name='tax_amount',
            field=models.DecimalField(
                null=True,
                max_digits=20,
                decimal_places=10),
        ),
    ]
