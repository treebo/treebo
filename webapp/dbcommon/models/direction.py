from django.db import models
from djutil.models import TimeStampedModel

from caching.base import CachingMixin
from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.models.hotel import Hotel
from dbcommon.models.location import Landmark
from dbcommon.models.landmark import SearchLandmark


class Directions(CachingMixin, TimeStampedModel, DefaultPermissions):
    hotel = models.ForeignKey(
        Hotel,
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING)
    directions_for_email_website = models.TextField(default="")
    directions_for_sms = models.TextField(default="")
    landmark_obj = models.ForeignKey(
        Landmark,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING)
    distance = models.DecimalField(
        max_digits=8,
        decimal_places=2,
        default=999999)
    WALKING = "WALKING"
    DRIVING = "DRIVING"
    MODE_CHOICES = (
        (WALKING, 'WALKING'),
        (DRIVING, 'DRIVING'),
    )
    mode = models.CharField(
        max_length=10,
        default=DRIVING,
        choices=MODE_CHOICES)
    polyline = models.CharField(max_length=5000, null=True, blank=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_directions'
        verbose_name = 'Direction'
        verbose_name_plural = 'Directions'

    def __unicode__(self):  # __unicode__ on Python 2
        return self.hotel.name

    def __str__(self):
        return self.hotel.name


class DirectionsNew(CachingMixin, TimeStampedModel, DefaultPermissions):
    hotel = models.ForeignKey(
        Hotel,
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING)
    directions_for_email_website = models.TextField(default="")
    directions_for_sms = models.TextField(default="")
    landmark_obj = models.ForeignKey(
        SearchLandmark,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING)
    distance = models.DecimalField(
        max_digits=8,
        decimal_places=2,
        default=999999)
    WALKING = "WALKING"
    DRIVING = "DRIVING"
    MODE_CHOICES = (
        (WALKING, 'WALKING'),
        (DRIVING, 'DRIVING'),
    )
    mode = models.CharField(
        max_length=10,
        default=DRIVING,
        choices=MODE_CHOICES)
    polyline = models.CharField(max_length=5000, null=True, blank=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_directions_new'
        verbose_name = 'DirectionNew'
        verbose_name_plural = 'DirectionsNew'

    def __unicode__(self):  # __unicode__ on Python 2
        return self.hotel.name

    def __str__(self):
        return self.hotel.name