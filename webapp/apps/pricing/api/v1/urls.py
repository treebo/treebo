

from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from apps.pricing.api.v1.views.RatePlanUpdateAPI import UpdateRateAmount
from apps.pricing.api.v2.cheapest_room_and_price_for_all_hotels import PricingAPIView
from apps.pricing.api.v1.views.get_price import Pricing
from apps.pricing.api.v1.views import channel_manager
from apps.pricing.api.v1.views.price_sync import PriceSyncTrigger

app_name = 'pricing_v1'
urlpatterns = [
    url(r"^updateRateAmount", channel_manager.UpdateRateAmount.as_view(), name='updateRateAmount'),
    url(r"^updateHotelAvailability", channel_manager.UpdateHotelAvailability.as_view(),
        name='updateRateAmount'),
    url(r"^getHotelRatePlan", channel_manager.GetHotelRatePlan.as_view(), name='getHotelRatePlan'),
    url(r"^getPromotions", channel_manager.GetPromotions.as_view(), name='getPromotions'),
    url(r"^setPromotions", channel_manager.SetPromotions.as_view(), name='setPromotions'),
    url(r"^deletePromotions", channel_manager.DeletePromotions.as_view(), name='deletePromotions'),
    url(r'^rates/$', csrf_exempt(UpdateRateAmount.as_view()), name='rate_push_api'),
    url(r'^availability$', Pricing.as_view(), name='pAvailability'),
    url(r'^hotels/$', PricingAPIView.as_view(), name='cheapest_prices_for_hotels'),
    url(r'^sync$', PriceSyncTrigger.as_view(), name='price_sync_trigger'),
]
