import logging, copy
from collections import defaultdict
from decimal import Decimal
from http import HTTPStatus

from django.conf import settings

from apps.checkout.service.upfront_part_pay_service import UpfrontPartPayService
from apps.common.exceptions.custom_exception import PartpayNotApplicable
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.growth.constants import PARTPAY_CHANNEL_CODE
from apps.growth.services.growth_services import GrowthServices
from apps.hotels.service.hotel_sub_brand_service import HotelSubBrandService
from apps.pricing.client.tax_client import SoaTaxBackend
from apps.pricing.response_wrappers.base_wrapper import BaseWrapper
from apps.reward.service.reward import RewardService
from apps.search.constants import INDEPENDENT_HOTEL_BRAND_CODE
from apps.payments.offers.services.payment_offer import PaymentOfferService
from treebo_commons.utils import dateutils
logger = logging.getLogger(__name__)


class ItineraryApiWrapper(BaseWrapper):
    reward_service = RewardService()
    upfront_partpay_service = UpfrontPartPayService()
    growth_service = GrowthServices()

    def __init__(self, hotel_id, cs_hotel_id, room_config_wise_price, room_wise_availability,
                 discount_prepaid_price, rate_plan, part_pay_applicable, user, apply_wallet,
                 checkin, checkout, promotion, application_code=None,
                 payment_offer_price=None, payment_offer=None):
        self.hotel_id = hotel_id
        self.room_wise_price = room_config_wise_price.get(hotel_id, {}).get('room_wise_price', {})
        self.room_wise_availability = room_wise_availability
        self.discount_prepaid_room_wise_price = discount_prepaid_price.get(hotel_id, {}).get(
            'room_wise_price', {})
        self.payment_offer_room_wise_price = payment_offer_price.get(hotel_id, {}).get(
            'room_wise_price', {})
        self.selected_rate_plan = rate_plan
        self.part_pay_applicable = part_pay_applicable
        self.cs_hotel_id = cs_hotel_id
        self.checkin = checkin
        self.checkout = checkout
        self.is_independent_hotel = False
        self.wallet_info = {}
        self.promotion = promotion
        self.payment_offer_price = payment_offer_price
        self.payment_offer = payment_offer
        super().__init__(user, apply_wallet, application_code)

    def get_default_price(self):
        default_price_component = {
            'selected_rate_plan': {
                'rate_plan': {
                    'code': None,
                    'description': None,
                    'tag': None
                },
                'price': {
                    'net_payable_amount': 0,
                    'part_pay_amount': 0,
                    'part_pay_percent': 0,
                    'wallet_applied': False,
                    'wallet_deduction': 0,
                    'wallet_type': settings.WALLET_TYPE,
                    'base_price': 0,
                    'rack_rate': 0,
                    'wallet_deductable_amount': 0,
                    'base_price_tax': 0,
                    'voucher_amount': 0,
                    'member_discount': 0,
                    'coupon_code': '',
                    'coupon_discount': 0,
                    'promo_discount': 0,
                    'pretax_price': 0,
                    'final_pretax_price': 0,
                    'tax': 0,
                    'sell_price': 0,
                    'total_discount_percent': 0,
                    'nights_breakup': [],
                    'nights_and_config_wise_breakup': [],
                    'discount_id': ''
                },
                "discount_prepaid_price": {}
            },
            'all_rate_plans': {},
            'wallet_info': {},
            'member_discount_applied': False,
            'member_discount_available': False
        }
        return default_price_component

    def get_pricing_response(self):  # pylint: disable=R0913,R0914,R1702,R0915
        default_price = self.get_default_price()
        try:  # pylint: disable=R1702
            wallet_balance = self.get_wallet_balance()
            pricing_response = default_price
            if self.room_wise_availability and self.room_wise_price:
                price = {
                    'member_discount_applied': False,
                    'member_discount_available': False,
                    'all_rate_plans': [],
                    'selected_rate_plan': {},
                    'wallet_info': {},
                    'coupon_desc': '',
                    'is_prepaid': False
                }
                rate_plan_price_dict, dp_rate_plan_price_dict, \
                    offer_payment_rate_plan_price_dict = {}, {}, {}

                if int(self.hotel_id) in HotelSubBrandService.get_hotels_ids_for_brand(
                    brand_code=INDEPENDENT_HOTEL_BRAND_CODE):
                    self.is_independent_hotel = True

                room_wise_price, dp_room_wise_price, payment_offer_room_wise_price = \
                    self.room_wise_price, self.discount_prepaid_room_wise_price, \
                    self.payment_offer_room_wise_price,

                room_type = list(room_wise_price.keys())[0]

                rate_plan_wise_price = room_wise_price[room_type]
                dp_rate_plan_wise_price = dp_room_wise_price.get(room_type, [])
                payment_offer_rate_plan_wise_price = payment_offer_room_wise_price.get(
                    room_type, [])

                for rate_plan_price in rate_plan_wise_price:
                    rate_plan_price_dict[rate_plan_price['policy_name']] = rate_plan_price

                for dp_rate_plan_price in dp_rate_plan_wise_price:
                    dp_rate_plan_price_dict[dp_rate_plan_price['policy_name']] = dp_rate_plan_price

                for offer_payment_rate_plan_price in payment_offer_rate_plan_wise_price:
                    offer_payment_rate_plan_price_dict[
                        offer_payment_rate_plan_price['policy_name']] = \
                        offer_payment_rate_plan_price

                for rate_plan in rate_plan_price_dict:
                    web_rate_plan_price = self.get_web_rate_plan(rate_plan_price_dict[rate_plan])
                    web_dp_rate_plan_price = self.get_web_rate_plan(
                        dp_rate_plan_price_dict.get(rate_plan, {}))
                    web_payment_offer_price_rate_plan_price = self.get_web_rate_plan(
                        offer_payment_rate_plan_price_dict.get(rate_plan, {}))


                    policy_name = web_rate_plan_price['policy_name']
                    web_price_breakup = self.get_price_breakup(web_rate_plan_price, wallet_balance)
                    web_dp_price_breakup = self.get_price_breakup(web_dp_rate_plan_price,
                                                                  wallet_balance)
                    web_payment_offer_price_breakup = self.get_price_breakup(
                        web_payment_offer_price_rate_plan_price, wallet_balance)

                    price_details_breakup = self.get_rate_plan_pricing_dict(
                        policy_name, web_price_breakup, web_dp_price_breakup,
                        web_payment_offer_price_breakup, self.checkin)

                    price['all_rate_plans'].append(copy.deepcopy(price_details_breakup))

                    if policy_name == self.selected_rate_plan:

                        price_details_breakup['price']['nights_and_config_wise_breakup'] = \
                            self.get_nights_and_config_wise_breakup(
                                web_rate_plan_price['date_wise_price_breakup'])
                        price_details_breakup['price']['nights_breakup'] = []

                        if price_details_breakup['discount_prepaid_price']:
                            price_details_breakup['discount_prepaid_price'][
                                'nights_and_config_wise_breakup'] = \
                                self.get_nights_and_config_wise_breakup(
                                    web_dp_rate_plan_price['date_wise_price_breakup'])
                            price_details_breakup['discount_prepaid_price']['nights_breakup'] = []

                        if price_details_breakup['payment_offer_price']:
                            price_details_breakup['payment_offer_price'][
                                'nights_and_config_wise_breakup'] = \
                                self.get_nights_and_config_wise_breakup(
                                    web_payment_offer_price_rate_plan_price['date_wise_price_breakup'])
                            price_details_breakup['payment_offer_price']['nights_breakup'] = []


                        price['selected_rate_plan'] = price_details_breakup

                price['wallet_info'] = self.wallet_info
                if self.wallet_info and self.wallet_info['debit_mode'] == 'pre_tax':
                    price = self._add_treebo_points_discount(price)
                price = self._update_part_pay_details(price)
                price = self.add_payment_offer(price)
                price = self.add_prepaid_promotion_message(price)
                pricing_response = price
        except Exception as exception:
            logger.exception(
                "Exception in itinerary api wrapper with error {}".format(str(exception)))
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param='',
                                                        dev_msg="Itinerary API Wrapper",
                                                        message=str(exception),
                                                        class_name=self.__class__.__name__)
            pricing_response = default_price

        return pricing_response

    def add_payment_offer(self, price):
        if not self.payment_offer:
            price['payment_offer'] = {}
            return price
        discount_removal_alert = self.discount_removal_alert_text(
            self.payment_offer.coupon_code,
            self.payment_offer.payment_method.name,
            self.payment_offer.payment_method.category
        )

        price['payment_offer'] = {
            "id": self.payment_offer.uid,
            "type": self.payment_offer.payment_method.category,
            "name": self.payment_offer.title,
            "payment_method_id": self.payment_offer.payment_method.uid,
            "offer_percentage": self.payment_offer.offer_percentage,
            "credit_mode": self.payment_offer.credit_mode,
            "minimum_transaction": self.payment_offer.minimum_transaction,
            "terms_and_conditions": self.payment_offer.terms_conditions,
            "description": self.payment_offer.description,
            "ranking": self.payment_offer.ranking,
            "gateway_offer_id": self.payment_offer.gateway_offer_id,
            "image_url": self.payment_offer.image_url,
            "discount_removal_alert": discount_removal_alert
        }
        if self.payment_offer.offer_percentage:
            promotion_message = "Paynow and get extra {discount_per}% discount".format(
                discount_per=int(self.payment_offer.offer_percentage))
            price['selected_rate_plan']['payment_offer_price'][
                'promotion_message'] = promotion_message
            price['all_rate_plans'][0]['payment_offer_price'][
                'promotion_message'] = promotion_message
        return price

    def discount_removal_alert_text(self, coupon_code, payment_mode_name, payment_category):
        discount_removal_alert_text = "Coupon code {coupon_code} is only applicable on " \
                                      "{payment_mode_name} {payment_mode_category}."
        payment_category = payment_category.replace('_', ' ').title()
        payment_mode_name = payment_mode_name.upper()
        return discount_removal_alert_text.format(coupon_code=coupon_code,
                                                  payment_mode_name=payment_mode_name,
                                                  payment_mode_category=payment_category)

    def get_price_breakup(self, web_rate_plan_price, wallet_balance):

        if not web_rate_plan_price:
            return web_rate_plan_price

        total_price = web_rate_plan_price['total_price_breakup']

        if not self.is_independent_hotel:
            self.wallet_info = self.get_user_wallet_info(wallet_balance, total_price['post_tax'])

        coupon_info = self.get_applied_coupon_info(total_price['pre_tax']['discounts'])
        applied_wallet_info = self.wallet_info if self.apply_wallet and not self.is_independent_hotel else \
            self.get_default_wallet_info()

        price_breakup = self.get_pricing_dict(total_price=total_price, coupon_info=coupon_info,
                                              wallet_info=applied_wallet_info)
        if self.part_pay_applicable:
            try:
                partpay_amount = self.upfront_partpay_service.get_partpay_amount(
                    price_breakup['net_payable_amount'])
                price_breakup['net_payable_reserve'] = partpay_amount
            except PartpayNotApplicable:
                pass

        return price_breakup

    def _update_part_pay_details(self, price):
        selected_rate_plan_price = price['selected_rate_plan']['price']
        partpay_amount, partpay_percent = self.growth_service.get_part_pay_amount(
            post_tax_amount=selected_rate_plan_price['net_payable_amount'], check_in=self.checkin,
            check_out=self.checkout, channel_code=PARTPAY_CHANNEL_CODE)
        price['selected_rate_plan']['price']['part_pay_amount'] = partpay_amount
        price['selected_rate_plan']['price']['part_pay_percent'] = partpay_percent
        price['all_rate_plans'][0]['price']['part_pay_amount'] = partpay_amount
        price['all_rate_plans'][0]['price']['part_pay_percent'] = partpay_percent
        return price

    def get_nights_and_config_wise_breakup(self, date_wise_price_breakup):
        nights_and_config_wise_breakup = []
        for date_wise_price in date_wise_price_breakup:
            nights_and_config_wise_breakup.extend(date_wise_price['sku_wise_prices'])
        return nights_and_config_wise_breakup

    def get_pricing_dict(self, total_price, coupon_info, wallet_info):

        return {
            'wallet_deductable_amount': self.wallet_info.get('wallet_deductable_amount', 0),
            'member_discount': 0,
            'total_discount_percent': coupon_info['discount_applied_percent'],
            'rack_rate': total_price['pre_tax']['list_price'],
            'discount_id': coupon_info['discount_id'],
            'coupon_discount': coupon_info['discount_applied'],
            'wallet_type': wallet_info['wallet_type'],
            'coupon_code': coupon_info['coupon_code'],
            'wallet_applied': wallet_info['wallet_applied'],
            'wallet_deduction': round(min(Decimal(wallet_info['wallet_deductable_amount']),
                                          Decimal(total_price['post_tax'])), 2),
            'pretax_price': round(Decimal(total_price['pre_tax']['sell_price']) +
                                  Decimal(coupon_info['discount_applied']), 2),
            'sell_price': round(total_price['post_tax'], 2),
            'net_payable_amount': Decimal(total_price['post_tax']) - Decimal(
                wallet_info['wallet_deductable_amount']),
            'part_pay_amount': 0,
            'part_pay_percent': 0,
            'base_price': 0,
            'promo_discount': 0,
            'final_pretax_price': total_price['pre_tax']['sell_price'],
            'voucher_amount': 0,
            'coupon_applied': bool(coupon_info['discount_applied']),
            'tax': round(total_price['tax']['total_value'], 2),
            'base_price_tax': 0,
            'part_pay_applicable': self.part_pay_applicable,
            'promotion_message': ""
        }

    @staticmethod
    def get_rate_plan_pricing_dict(policy_name, web_price_breakup,
                                   web_dp_price_breakup, web_payment_offer_price_breakup, checkin):
        same_day_booking = str(dateutils.today().date()) == checkin
        price = {
            'rate_plan': {
                'code': policy_name,
                'description': 'Refundable Plan' if not same_day_booking and policy_name.lower() != 'nrp' else '',
                'refundable': not same_day_booking and policy_name.lower() != 'nrp',
                'tag': 'Refundable' if not same_day_booking and policy_name.lower() != 'nrp' else ''
            },
            'price': web_price_breakup,
            'discount_prepaid_price': web_dp_price_breakup,
            'payment_offer_price': web_payment_offer_price_breakup
        }

        return price

    def _add_treebo_points_discount(self, price):  # pylint: disable=R0914,R1702
        treebo_discount_price_applicable = price['selected_rate_plan']['price']['wallet_applied'] \
                                           and price['selected_rate_plan']['price'][
                                               'wallet_deduction']
        if not treebo_discount_price_applicable:
            return price

        selected_rate_plan_price = price['selected_rate_plan']['price']
        all_rate_plan_price = price['all_rate_plans'][0]['price']
        rate_plan_code = price['selected_rate_plan']['rate_plan']['code']

        price['selected_rate_plan']['price'], price['all_rate_plans'][0]['price'] = self. \
            _modify_price_breakup_for_wallet_discount(selected_rate_plan_price, all_rate_plan_price,
                                                      rate_plan_code)

        dp_selected_rate_plan_price = price['selected_rate_plan']['discount_prepaid_price']
        dp_all_rate_plan_price = price['all_rate_plans'][0]['discount_prepaid_price']

        if dp_selected_rate_plan_price and dp_all_rate_plan_price:
            price['selected_rate_plan']['discount_prepaid_price'], price['all_rate_plans'][0]['discount_prepaid_price'] \
                = self._modify_price_breakup_for_wallet_discount(dp_selected_rate_plan_price, dp_all_rate_plan_price,
                                                                 rate_plan_code)

        return price

    def _modify_price_breakup_for_wallet_discount(self, selected_rate_plan_price,
                                                  all_rate_plan_price, rate_plan_code):

        tax_request = dict()
        sku_charges = []

        sku_codes = [night_breakup['sku_code'] for night_breakup
                     in selected_rate_plan_price['nights_and_config_wise_breakup']]

        if not sku_codes:
            return selected_rate_plan_price, all_rate_plan_price

        debit_percentage = self.wallet_info['debit_percentage'] if self.wallet_info else 0

        throttle_discount = self.throttled_treebo_discount_points(
            selected_rate_plan_price["wallet_deductable_amount"],
            selected_rate_plan_price["final_pretax_price"],
            debit_percentage
        )

        for price_breakup in selected_rate_plan_price['nights_and_config_wise_breakup']:
            pretax_price = round(price_breakup['pre_tax_price'] - float(throttle_discount) / len(
                selected_rate_plan_price['nights_and_config_wise_breakup']), 2)
            sku_charge = dict(pretax_price=pretax_price,
                              sku_code=price_breakup["sku_code"],
                              rate_plan=rate_plan_code
                              )

            sku_charges.append(sku_charge)

        request_price_per_sku = defaultdict(float)
        updated_sku_charge = []
        for sku_charge in sku_charges:
            request_price_per_sku[sku_charge['sku_code']] += sku_charge['pretax_price']
        is_sku_traversed = dict()

        for sku_charge in sku_charges:
            if not is_sku_traversed.get(sku_charge['sku_code']):
                updated_sku_charge.append(dict(sku_code=sku_charge['sku_code'],
                                               pretax_price=round(
                                                   request_price_per_sku[sku_charge['sku_code']],
                                                   2),
                                               rate_plan=sku_charge['rate_plan']
                                               ))
                is_sku_traversed[sku_charge['sku_code']] = True

        if not updated_sku_charge:
            return selected_rate_plan_price, all_rate_plan_price

        hotel_charges = dict(hotel_id=self.cs_hotel_id,
                             sku_charges=updated_sku_charge)

        tax_request['hotel_charges'] = [hotel_charges]
        tax_request['stay_start'] = self.checkin
        tax_request['stay_end'] = self.checkout
        tax_backend_response = SoaTaxBackend.get_tax(tax_request)
        hotel_tax_prices = tax_backend_response[self.cs_hotel_id]

        final_pretax_price = 0
        final_post_tax_price = 0
        final_tax = 0
        for _, sku_price in hotel_tax_prices.items():
            for _, rate_pla_prices in sku_price.items():
                final_pretax_price += rate_pla_prices['pre_tax_price']
                final_post_tax_price += rate_pla_prices['post_tax_price']
                final_tax += rate_pla_prices['tax']

        selected_rate_plan_price['treebo_points_discount'] = self.throttled_treebo_discount_points(
            selected_rate_plan_price["wallet_deductable_amount"],
            selected_rate_plan_price["final_pretax_price"],
            self.wallet_info['debit_percentage']
        )

        selected_rate_plan_price['tax'] = final_tax
        selected_rate_plan_price['final_pretax_price'] = round(final_pretax_price, 2)
        selected_rate_plan_price['net_payable_amount'] = round(final_post_tax_price, 2)
        selected_rate_plan_price["wallet_deduction"] = 0
        selected_rate_plan_price['wallet_applied'] = False

        all_rate_plan_price['treebo_points_discount'] = self.throttled_treebo_discount_points(
            all_rate_plan_price["wallet_deductable_amount"],
            all_rate_plan_price["final_pretax_price"],
            self.wallet_info['debit_percentage']
        )

        all_rate_plan_price['tax'] = round(final_tax, 2)
        all_rate_plan_price['final_pretax_price'] = round(final_pretax_price, 2)
        all_rate_plan_price['net_payable_amount'] = round(final_post_tax_price, 2)
        all_rate_plan_price["wallet_deduction"] = 0
        all_rate_plan_price['wallet_applied'] = False

        nights_per_sku = defaultdict(int)
        for night_breakup in selected_rate_plan_price['nights_and_config_wise_breakup']:
            nights_per_sku[night_breakup['sku_code']] += 1

        for night_breakup in selected_rate_plan_price['nights_and_config_wise_breakup']:
            night_breakup['sell_price'] = round(hotel_tax_prices[night_breakup['sku_code']][
                                                    rate_plan_code]['post_tax_price'] /
                                                nights_per_sku[
                                                    night_breakup['sku_code']], 2)

        return selected_rate_plan_price, all_rate_plan_price

    def add_prepaid_promotion_message(self, price):
        if not self.promotion:
            return price

        promotion_message = self.promotion['description']
        price['selected_rate_plan']['discount_prepaid_price'][
            'promotion_message'] = promotion_message
        price['all_rate_plans'][0]['discount_prepaid_price'][
            'promotion_message'] = promotion_message

        return price
