from django.core.exceptions import ValidationError
from django.db import models
from djutil.models import TimeStampedModel

from dbcommon.models.default_permissions import DefaultPermissions
from .hotel import Hotel
from .profile import User


# Create your models here.

class Coupon(TimeStampedModel, DefaultPermissions):
    code = models.CharField(max_length=200, default="", unique=True)
    discount_operation = models.CharField(
        max_length=200, null=True, blank=True)
    discount_value = models.DecimalField(
        max_digits=5, decimal_places=1, default=0)
    max_available_usages = models.IntegerField(default=0)
    max_available_discount = models.PositiveIntegerField(default=0)
    expiry = models.DateField(blank=True, null=True)
    is_active = models.BooleanField(default=False)
    min_cart_cost = models.DecimalField(
        max_digits=10, decimal_places=1, default=0)
    tagline = models.CharField(max_length=200, default="", null=True)
    terms = models.CharField(max_length=5000, default="", null=True)
    min_cart_nights = models.IntegerField(default=0)
    PRETAX = 0
    POSTTAX = 1
    STATUS_CHOICES = (
        (PRETAX, 'Pretax'),
        (POSTTAX, 'Posttax'))
    discount_application = models.IntegerField(
        default=POSTTAX, choices=STATUS_CHOICES)
    DISCOUNT = 0
    VOUCHER = 1
    CASHBACK = 2
    COUPON_TYPE_CHOICES = (
        (DISCOUNT, 'Discount'),
        (VOUCHER, 'Voucher'),
        (CASHBACK, 'Cashback')
    )
    coupon_type = models.IntegerField(
        default=DISCOUNT, choices=COUPON_TYPE_CHOICES)
    coupon_message = models.CharField(default="Discount value", max_length=500)

    class Meta(DefaultPermissions.Meta):
        db_table = 'discounts_coupon'
        verbose_name = 'Coupon'
        verbose_name_plural = 'Coupons'

    def __unicode__(self):  # __unicode__ on Python 2
        return self.code

    def __str__(self):
        return self.code


class Coupon_Constraint(TimeStampedModel, DefaultPermissions):
    coupon = models.ForeignKey(
        Coupon,
        related_name='constraints',
        on_delete=models.DO_NOTHING)
    hotels = models.ManyToManyField(Hotel, blank=True)
    users = models.ManyToManyField(User, blank=True)
    type = models.CharField(max_length=200, null=True, blank=True)
    value = models.CharField(max_length=200, null=True, blank=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'discounts_coupon_constraint'
        verbose_name = 'Coupon Constraint'
        verbose_name_plural = 'Coupon Constraints'

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s constraint %s' % (self.coupon.code, self.type)

    def __str__(self):
        return '%s constraint %s' % (self.coupon.code, self.type)


class Coupon_Date_Constraint(TimeStampedModel, DefaultPermissions):
    coupon = models.ForeignKey(
        Coupon,
        related_name='date_constraints',
        on_delete=models.DO_NOTHING)
    start_date = models.DateField()
    end_date = models.DateField()

    class Meta(DefaultPermissions.Meta):
        db_table = 'discounts_coupon_date_constraint'
        verbose_name = 'Coupon Date Constraint'
        verbose_name_plural = 'Coupon Date Constraints'

    def clean(self):
        if self.start_date > self.end_date:
            raise ValidationError({
                'start_date': [
                    ValidationError(
                        message=("Start date must be before end date"),
                        code='invalid',
                        params={},
                    )
                ]
            })
        super(Coupon_Date_Constraint, self).clean()

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s - %s' % (self.coupon.code, self.start_date)

    def __str__(self):
        return '%s - %s' % (self.coupon.code, self.start_date)
