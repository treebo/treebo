from common.job.jobs.payment_dropoff_notification import payment_drop_offs_notification_job_executor
from common.job.job_registry import JobRegistry

job_registry = JobRegistry()


class RegisterJob:
    def __init__(self):
        job_registry.register('SOFT_BOOKING_PAYMENT_DROPOFFS_NOTIFICATION',
                              payment_drop_offs_notification_job_executor)
