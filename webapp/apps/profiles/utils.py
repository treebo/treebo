import logging
import random
import string

from dbcommon.models.miscellanous import Feedback
from dbcommon.models.profile import UserMetaData
from services.notification.sms.otp_sms import OtpSmsService

# Get an instance of a logger
logger = logging.getLogger(__name__)


def get_customer_care(user_id):
    """

    :param user_id:
    :return:
    """
    try:
        return UserMetaData.objects.get(
            user_id=user_id, key='is_callcenter', value='1')
    except UserMetaData.DoesNotExist:
        return None


def sendMessage(phone, otp, first_name, send):
    if send:
        sms_service = OtpSmsService(
            OtpSmsService.NOTIFICATION_SMS, None, phone)
        sms_service.collateSmsData(first_name, otp)
        try:
            sms_service.createMessage()
        except Exception as exc:
            logger.exception(exc)

        sms_service.sendMessage()
    else:
        return "SMS Sending disabled by admin"


def saveFeedback(sender, receiver, message, page_url, device):
    feedback = Feedback()
    feedback.sender = sender
    feedback.receiver = receiver
    feedback.message = message
    feedback.page_url = page_url
    feedback.device = device
    feedback.save()


def build_name(name):
    name_array = name.split(" ", 1)
    first_name = name_array[0]
    last_name = name_array[1] if len(name_array) > 1 else first_name
    return first_name, last_name


def generateRandomPassword():
    return ''.join(
        random.SystemRandom().choice(
            string.ascii_uppercase +
            string.digits) for _ in range(8))
