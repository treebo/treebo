'use strict';

import $ from 'jquery';
import {Observable} from "rx";
import {_} from 'underscore';
import "slick-carousel";
import BaseView from '../treebolibs/baseview';
import searchFotWidget from '../widgets/search_audit_fot';
import queryString from 'query-string';
import * as toastr from 'toastr';
import React from 'react';
import {render} from 'react-dom';
import { createStore, combineReducers } from 'redux';
import cookies from 'js-cookie';
import '../../css/fot/search_audit_fot.less';

let treejax = require('../treebolibs/treejax');
let $loader = $("#commonLoader");
const moment = require('moment');

let $confirmVoucher = $("#confirmVoucher");
let $generateVoucher = $("#generateVoucher");
let $bookingVoucher = $("#bookingVoucher");
let $popupModal = $("#popupModal");
let $cancelAudit = $("#cancelAudit");
let $auditCancelled = $("#auditCancelled");
let $cancelReject = $("#cancelReject");
let $auditSuccess = $("#auditSuccess");

let hotel_id;
let audit_booking_id;
let cancelButton;
let auditDate;
let nights;
let cheap_room;
let creditsRedeemed;
if($("#marketingBanner").length){
    const initialIndex = +$("#marketingBanner").css("order");
}

$(".hotel__action").on('click', ".scheduleAudit", e => {
    hotel_id = $(e.target).closest(".results__row").attr("data-id");
    cheap_room = $(e.target).closest(".results__row").attr("room-type");
    const hotel = $(e.target).closest(".results__row").attr("hotel");
        auditDate = requests.queryString("checkin");
    if (auditDate && auditDate != "") {
        auditDate = moment(auditDate).format("DD MMM'YY");
    } else {
        const startDay = rt.startDayThreshold ? rt.startDayThreshold : 1;
        auditDate = moment().add(startDay, 'day').format("DD MMM'YY");
    }
    $('#checkHotel').text(hotel);
    $('#checkDate').text(auditDate);
    $("#confirmAudit").removeClass('hide');

});

$(".hotel__action").on('click', ".cancelAudit", e => {
    audit_booking_id = $(e.target).closest(".results__row").attr("audit-id");
    cancelButton = $(e.target).closest(".results__row").find(".cancelAudit");
    const checkInDate = $(e.target).closest(".results__row").attr("checkin-date");
    const tomorrow_time = moment(checkInDate).startOf('day').hour(12);
    const today_time = moment();
    if(moment(tomorrow_time).diff(today_time, "hours") < 12) {
        $popupModal.removeClass('hide');
        $cancelReject.removeClass('hide');
    } else {
        $popupModal.removeClass('hide');
        $cancelAudit.removeClass('hide');
    }
});

$('#cancelNoBtn').on('click', e => {
    $popupModal.addClass('hide');
    $cancelAudit.addClass('hide');
});

$('#cancelledNoBtn').on('click', e => {
    $popupModal.addClass('hide');
    $auditCancelled.addClass('hide');
    $loader.removeClass('hide');
    window.location.href = window.location.origin;
});

function reloadPage() {
    $loader.removeClass('hide');
    let reload_page = "";
    reload_page = requests.queryString("confirm");
    if(reload_page){
        $('#sameDateAuditError').addClass('hide');
        $('#maxAuditBookedError').addClass('hide');
        let parsed_url = queryString.parse(location.search);
        parsed_url = _.omit(parsed_url, 'confirm');
        const new_url = queryString.stringify(parsed_url);
        window.location.search = new_url;
    } else {
        location.reload();
    }
};

$('#cancelledYesBtn').on('click', e => {
    $popupModal.addClass('hide');
    $auditCancelled.addClass('hide');
    reloadPage();
});

$('#cancelAuditMailBtn').on('click', e => {
    $popupModal.addClass('hide');
    $('#cancelReject').addClass('hide');
});
$('#bookAnotherAudit').on('click', e => {
    $('#auditSuccess').addClass('hide');
    reloadPage();
});

$('#cancelYesBtn').on('click', e => {
    $loader.removeClass('hide');
    $popupModal.addClass('hide');
    $cancelAudit.addClass('hide');
    const cancel_audit_id = {};
    cancel_audit_id["order_id"] = audit_booking_id;
    cancel_audit_id["email"] = $('#userEmail').val();
    const p = treejax ({
        url: '/api/v1/checkout/booking/cancel/',
        type:'POST',
        data: cancel_audit_id
    }).then((data, status, xhr) => {
        $loader.addClass('hide');
        $popupModal.removeClass('hide');
        if (data.status == "SUCCESS") {
            $auditCancelled.removeClass('hide');
            cancelButton.css('pointer-events', 'none');
            toastr.success('Your audit booking is successfully cancelled!', 'Thanks!', {timeOut: 5000, closeButton: true});
        } else {
            $('#cancelReject').removeClass('hide');
            toastr.error('Your audit booking cannot be cancelled!', 'Sorry!', {timeOut: 5000, closeButton: true});
        }
    }, treeboError => {
        $loader.addClass('hide');
        $popupModal.removeClass('hide');
        $('#cancelReject').removeClass('hide');
        toastr.error('Your audit booking cannot be cancelled!', 'Sorry!', {timeOut: 5000, closeButton: true});
    });
});

$("#sideWidgets").on('click', '#redeemNights', e => {
    $('#userCredit').text(rt.userCredits);
    $popupModal.removeClass('hide');
    var redemptionStatusToggle = $("#redemptionStatusToggle").val();
    if (redemptionStatusToggle.toLowerCase() == "false") {
      $('#suspendRedemption').removeClass('hide');
    }
    else {
      $generateVoucher.removeClass('hide');
    }

});

$('#generateVoucherBtn').on('click', e => {
    $loader.removeClass('hide');

    var credits = parseInt($('#creditRedeem').val());

    if (credits <= rt['max_allowed_credits']) {
      const creditData = [];
      creditData.push({name: 'credits', value: $('#creditRedeem').val()});
      creditData.push({name: 'csrfmiddlewaretoken', value: $('input[name="csrfmiddlewaretoken"]').val()});

      const p = treejax({
        url: '/fot/api/v1/redeemcredits/',
        type: 'POST',
        data: creditData
      }).then((data, status, xhr) => {
        $loader.addClass('hide');
        if (data.success) {
          creditsRedeemed = data.creditsToBeRedeemed
          $('#creditRedeemed').text(data.creditsToBeRedeemed);
          $('#creditRupees').text(data.creditsToBeRedeemed);
          $confirmVoucher.removeClass('hide');
          $generateVoucher.addClass('hide');
        } else {
          toastr.error(data.msg, 'Sorry!', {timeOut: 5000, closeButton: true});
        }
      }, treeboError => {
        $loader.addClass('hide');
        toastr.error("Something is wrong. Please try again later.", 'Sorry!', {timeOut: 5000, closeButton: true});
      });
    } else {
      $loader.addClass('hide');
      toastr.error("You can redeem at max " + rt['max_allowed_credits'] + " credits.", 'Sorry!', {timeOut: 5000, closeButton: true});
    }

});

$('#confirmVoucherBtn').on('click', e => {
    $loader.removeClass('hide');
    const voucherData = [];
    voucherData.push({name: 'credits', value: $('#creditRedeemed').text()});
    voucherData.push({name: 'user_id', value: $('#userId').val()});
    voucherData.push({name: 'expiry', value: moment(auditDate).add(60, 'day').format("YYYY-MM-DD")});
    voucherData.push({name: 'csrfmiddlewaretoken', value: $('input[name="csrfmiddlewaretoken"]').val()});
    const p = treejax ({
        url: '/fot/api/v1/discount/',
        type:'POST',
        data: voucherData
    }).then((data, status, xhr) => {
        $loader.addClass('hide');
        if (data.success) {
            document.getElementById("userTotalCredits").innerHTML = rt.userCredits - creditsRedeemed;
            rt.userCredits = rt.userCredits - creditsRedeemed;
            $('#voucherCode').text(data.code);
            $('#freeCouponOffered').text(data.creditsRedeem);
            $bookingVoucher.removeClass('hide');
            $confirmVoucher.addClass('hide');
        } else {
            toastr.error("Something is wrong. Please try again later.", 'Sorry!', {timeOut: 5000, closeButton: true});
        }
    }, treeboError => {
        $loader.addClass('hide');
        toastr.error("Something is wrong. Please try again later.", 'Sorry!', {timeOut: 5000, closeButton: true});
    });
});

$('#cancelVoucherBtn').on('click', e => {
    $popupModal.addClass('hide');
    $confirmVoucher.addClass('hide');
    $generateVoucher.addClass('hide');
});

$('#closeModal').on('click', e => {
    $popupModal.addClass('hide');
    $bookingVoucher.addClass('hide');
    $confirmVoucher.addClass('hide');
    $generateVoucher.addClass('hide');
    $cancelAudit.addClass('hide');
    $auditCancelled.addClass('hide');
    $cancelReject.addClass('hide');
});

$("#auditConfirm").on("click", e => {
    if ($('.check-term:checked').length == $('.check-term').length) {
        $('#confirmAudit').addClass('hide');
        $loader.removeClass('hide');

        var checkInAudit = moment(auditDate, 'DD MMM`YY'),
            checkInAudit = checkInAudit.format('YYYY-MM-DD'),
            room_config = $("#fotRoomConfig").val();
        if (!checkInAudit) {
            checkInAudit = moment().add(rt.startDayThreshold, 'day');
        }
        const checkOutAudit = moment(checkInAudit).add(1, 'day').format('YYYY-MM-DD');
        var checkInAudit = moment(checkInAudit).format('YYYY-MM-DD');

        $('#checkin').val(checkInAudit);
        $('#checkout').val(checkOutAudit);
        $('#hotelid').val(hotel_id);
        $('#roomtype').val(cheap_room);
        $("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="2"/>');
        $("#roomconfig").val(room_config);

        $('#fotUserInfoForm').submit();
    } else {
        toastr.error('Please check all the conditions.', 'Sorry!', {timeOut: 5000, closeButton: true});
    }
});

let SearchResultView = Object.create(BaseView);
let store = {};
let methods = {
    initResults : ()=>{

        const $slickElement = $(".carousel")
        .on('init', slick => {
            $('.carousel').fadeIn(3000);
        })
        .slick({
            fade: true,
            focusOnSelect: true,
            dots: true,
            lazyLoad: 'ondemand',
            slidesToShow: 1,
            slidesToScroll: 1,
            customPaging(slider, i) {
                // this example would render "tabs" with titles
                return `${i+1} of ${slider.$slides.length}` ;
            }
        });

        const parsed = queryString.parse(location.search);
    }
}

$.fn.inView = function(inViewType){
    var viewport = {};
    viewport.top = $(window).scrollTop();
    viewport.bottom = viewport.top + $(window).height();
    var bounds = {};
    bounds.top = this.offset().top;
    bounds.bottom = bounds.top + this.outerHeight();
    switch(inViewType){
      case 'bottomOnly':
        return ((bounds.bottom <= viewport.bottom) && (bounds.bottom >= viewport.top));
      case 'topOnly':
        return ((bounds.top <= viewport.bottom) && (bounds.top >= viewport.top));
      case 'both':
        return ((bounds.top >= viewport.top) && (bounds.bottom <= viewport.bottom));
      default:
        return ((bounds.top >= viewport.top) && (bounds.bottom <= viewport.bottom));
    }
};

SearchResultView.isFooterInView = () =>{
    if(!$('.footer')) return;
    return $('.footer').inView('bottomOnly');
}

$(window).on("scroll", function(e) {
    var mainWrapperBottomEdge = $('.main-wrapper').offset().top + $('.main-wrapper').height();
    var sidebarBottomEdge = $('#sideWidgets').offset().top + $('#sideWidgets').height();

    if(($('.left-part').height() > ($('#sideWidgets').height()+100)) && $('#sideWidgets').height() != 0) {
      if ($(window).scrollTop() > 185 && SearchResultView.isFooterInView()==false) {
        $('#sideWidgets').css({position: 'fixed', top: '20px', width:'340px'});
      } else if (SearchResultView.isFooterInView()==true) {
        $('#sideWidgets').css({position: 'absolute', top: ($('.main-wrapper').height() - ($('#sideWidgets').height()))});
      } else {
        $('#sideWidgets').css({position: 'absolute', top:'0px'});
      }
    }
});

let SideWidgets = React.createClass({
    render(){
        return (
            <div>
                <div className="audit-point flex-column">
                    <div className="audit-point__my-audit text-center">
                        MY CREDITS
                    </div>
                    <div className="audit-point__detail flex-column text-center">
                        <div className="">Credits Available</div>
                        <div id="userTotalCredits" className="audit-point__credit-point">{rt.userCredits}</div>
                        <div id="redeemNights" className="audit-point__info anchor"> Click here to redeem your credits </div>
                    </div>

                </div>
            </div>
        )
    }

});

let requests = {
    queryString(item) {
        const svalue = location.search.match(new RegExp(`[\?\&]${item}=([^\&]*)(\&?)`,"i"));
        return svalue ? svalue[1] : svalue;
    }
};

SearchResultView.pageLoaded = () => {
    window.scrollTo(0,0);
    const audit_success = requests.queryString("confirm");
    const audit_fail = requests.queryString("error");
    const validate = requests.queryString("validate");
    if(audit_success == "true") {
        $('#sameDateAuditError').addClass('hide');
        $('#maxAuditBookedError').addClass('hide');
        $('#auditSuccess').removeClass('hide');
        toastr.success('At a time, you can book maximum 2 audits in future.', 'Thank You!', {timeOut: 5000, closeButton: true});
    } else if (audit_success == "false") {
        $('#sameDateAuditError').addClass('hide');
        $('#maxAuditBookedError').addClass('hide');
        $('#auditFail').removeClass('hide');
    }else if (audit_fail) {
        $('#auditFail').removeClass('hide');
    }else if (validate == "false") {
        $('#validateFail').removeClass('hide');
    };

    methods.initResults();
    const results = {};
    render(<SideWidgets data={results}/>, $('#sideWidgets')[0]);
    $(".js-deals-carousel").slick();

    if(rt.dateRangeFailed == true ) {
        toastr.error(`You cannot schedule for less than ${rt.startDayThreshold} day or for more than ${rt.endDayThreshold} days.`, 'Sorry!', {timeOut: 3000, closeButton: true});
        setTimeout(() => {
            window.location= rt.urlRedirect;
        }, 2000);
    };
}

let analyticsConfigfunc = function(){
    return  {
        city: rt.city,
        // 'destination':$('#searchInput').val(),
        // 'checkin':rt.getCheckin(),
        // 'checkout':rt.getCheckout(),
        // 'adults':rt.getAdults(),
        // 'kids':rt.getKids(),
        // 'rooms':rt.getRooms(),
        // 'num_results':$('.analytics-count').attr('count'),
        // 'sort_by':rt.sortBy | {}
    }
}

let pageName = rt.isHotelInCityPage ?  `Hotels in City Page` : '';

let resultConfig = { name:pageName, widgets:[searchFotWidget]};
let searchResultView = SearchResultView.setup(resultConfig);
