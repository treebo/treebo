import logging
import json
from kombu.mixins import ConsumerMixin

from apps.common.slack_alert import SlackAlertService
from dbcommon.data_services import models_updater
from queue_listeners.cs_message_processor import CSMessagesProcessor

logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)
# console_handler = logging.StreamHandler()
# console_handler.setLevel(logging.DEBUG)
# formatter = logging.Formatter(
#     '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# console_handler.setFormatter(formatter)
# logger.addHandler(console_handler)



class CSImageListener(ConsumerMixin):

    def __init__(self, connection, queues):
        self.connection = connection
        self.queues = queues

    def get_consumers(self, Consumer, channel):
        return [Consumer(queues=self.queues,
                         callbacks=[self.on_message])]

    def on_message(self, body, message):
        try:
            logger.info('Got message: {0}'.format(body))
            message.ack()
        except Exception as e:
            logger.error(e)

    def process_room_message(self, body, message):
        cs_messages_processor = CSMessagesProcessor()
        msg_dict = cs_messages_processor.get_image_from_body(body)
        cs_msg = models_updater.store_dump_of_incoming_message(msg_dict['cs_property_id'],
                                                               body, message.delivery_info['routing_key'],
                                                               message.delivery_info['exchange'], self.queues.name)
        is_processed, failure_msg  = cs_messages_processor.process_image_json(msg_dict['data'], msg_dict['cs_property_id'])
        try:
            if cs_msg:
                cs_msg.is_processed = is_processed
                cs_msg.failure_message = failure_msg[0:190]
                cs_msg.save()
                if not is_processed:
                    try:
                        SlackAlertService.publish_alert(
                            'Exception while storing cs property for room message'
                            'with cs id %s' %
                            (msg_dict['cs_property_id']),
                            SlackAlertService.CS_MESSAGES_ALERT)
                    except Exception as e:
                        logger.error(e)
        except Exception as e:
            logger.error(e)

