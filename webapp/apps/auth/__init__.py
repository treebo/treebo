from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'apps.auth'
    # <-- this is the important line - change it to anything other than the default, which is the module name ('foo' in this case)
    label = 'apps.authentication'


default_app_config = 'apps.auth.AuthConfig'
