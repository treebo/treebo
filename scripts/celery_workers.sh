#!/usr/bin/env bash
echo "starting celery worker for $CELERY_WORKER"
newrelic-admin run-program celery worker -c $GUNICORN_WORKER -A webapp -l info -Q $CELERY_WORKER  -f /var/log/$CELERY_WORKER.log