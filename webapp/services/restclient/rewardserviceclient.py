from django.conf import settings
import logging
from treebo_rewards.constants import DEVELOPMENT, LOCAL, PRODUCTION, STAGING, PREPROD
from treebo_rewards.rewards import TreeboRewards
from treebo_rewards.exceptions import UnableToFetchBalance

logger = logging.getLogger(__name__)


class RewardServiceClient():
    rewards_env_map = {
        'local': LOCAL,
        'staging': STAGING,
        'prod': PRODUCTION,
        'dev': DEVELOPMENT,
        'preprod': PREPROD,
        'conversion': STAGING,
        'traffic': STAGING,
        'growth': STAGING,
    }

    def __init__(self):
        self.treebo_rewards = TreeboRewards(source_name=settings.REWARDS_SOURCE_NAME,
                                            source_type=settings.REWARDS_SOURCE_TYPE,
                                            env=self.rewards_env_map[settings.ENVIRONMENT])

    def get_wallet_statement(self, user_id, wallet_type='TP'):
        wallet_statement = []
        try:
            statement = self.treebo_rewards.get_wallet_statement(
                user_id, wallet_type)
            wallet_statement = statement.detailed_statement
        except UnableToFetchBalance as e:
            pass
        return wallet_statement

    def get_wallet_info(self, user, wallet_type=None, application_code=None):
        wallet_balance_amount = 0
        wallet_debit_percentage = None
        debit_sources = None
        debit_mode = None
        loyalty_type = None
        try:
            if user and hasattr(user, 'auth_id') and user.auth_id:
                wallet_balance_instance = self.treebo_rewards.get_balance_for_wallet(
                    user.auth_id, wallet_type)
                wallet_balance_amount = wallet_balance_instance.wallet_balance
                wallet_debit_percentage = wallet_balance_instance.debit_percentage
                debit_sources = wallet_balance_instance.debit_sources
                debit_mode = wallet_balance_instance.debit_mode
                loyalty_type = wallet_balance_instance.loyalty_type
            else:
                logger.error(
                    "get_wallet_info have null user of null auth id  %s ", user)
        except UnableToFetchBalance as e:
            pass
        # TODO: py - apply conversion policy through rewards client here
        wallet_spendable_amount = wallet_balance_amount if debit_sources and (application_code in debit_sources) else 0
        wallet_points = wallet_balance_amount
        wallet_balance = wallet_balance_amount
        return wallet_balance, wallet_points, wallet_spendable_amount, \
               wallet_debit_percentage, debit_sources, debit_mode, loyalty_type

    def get_wallet_types(self):
        return [settings.WALLET_TYPE]
