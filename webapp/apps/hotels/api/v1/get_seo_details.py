import json
import logging

from apps.hotels import utils as hotel_utils
from apps.hotels.serializers.seo_city_serializer import CitySeoDetailsSerializer
from apps.hotels.serializers.seo_serializer import HotelSeoDetailsSerializer
from base.views.api import TreeboAPIView
from dbcommon.models.hotel import SeoDetails as HotelSeoDetails, SeoDetails

logger = logging.getLogger(__name__)


class SeoDetails(TreeboAPIView):
    def getSeoData(self, hashedHotelId="", hashedCityId=""):
        response = {}

        if hashedHotelId != "":
            hotelId = hotel_utils.decode_hash(hashedHotelId)
            result, message = self.__getSeoDetailsForHotel(hotelId)
        elif hashedCityId != "":
            city_id = hotel_utils.decode_hash(hashedCityId)
            result, message = self.__getSeoDetailsForCity(city_id)
        else:
            response['seo'] = self.__defaultKeywords()
            return response

        response['seo'] = result

        return response

    def __getSeoDetailsForHotel(self, hotelId):
        result = {}
        message = ""
        try:
            message = "success"
            seo = HotelSeoDetails.objects.filter(hotel_id=hotelId)
            serialData = HotelSeoDetailsSerializer(seo, many=True)
            serialData = json.dumps(serialData.data)
            serialData = json.loads(serialData)
            result = serialData

        except Exception as exc:
            result = self.__defaultKeywords()
            logger.debug('No details found so using default details')
        return result, message

    def __getSeoDetailsForCity(self, city_id):
        result = {}
        message = ""
        try:
            message = "success"
            seo = SeoDetails.objects.get(city_id=city_id)
            serialData = CitySeoDetailsSerializer(seo, many=True)
            serialData = json.dumps(serialData.data)
            serialData = json.loads(serialData)
            result = serialData

        except Exception as exc:
            logger.debug('No details found so using default details')
            result = self.__defaultKeywords()

        return result, message

    def __defaultKeywords(self):

        result = {
            'title': 'Treebo - Budget Hotels in India | Online Hotel Booking',
            'description': "Treebo Hotels - Fastest growing chain of best budget hotels in India. Book a Treebo hotel online & experience delightful stays at budget prices. Book Now!",
            'keyword': 'Treebo hotels, budget hotels, budget hotels in india, affordable hotels, hotels in india, hotels booking, book hotels online, online hotels booking'}

        return result
