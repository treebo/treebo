# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discounts', '0009_auto_20160624_0455'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='constraintexpression',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Constraint Expression',
                'verbose_name_plural': 'Constraint Expressions'},
        ),
        migrations.AlterModelOptions(
            name='constraintkeywords',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Constraint Keyword',
                'verbose_name_plural': 'Constraint Keywords'},
        ),
        migrations.AlterModelOptions(
            name='constraintoperators',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Constraint Operator',
                'verbose_name_plural': 'Constraint Operators'},
        ),
        migrations.AlterModelOptions(
            name='discountcoupon',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Discount Coupon',
                'verbose_name_plural': 'Discount Coupons'},
        ),
        migrations.AlterModelOptions(
            name='discountcouponconstraints',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Discount Coupon Constraint',
                'verbose_name_plural': 'Discount Coupon Constraints'},
        ),
    ]
