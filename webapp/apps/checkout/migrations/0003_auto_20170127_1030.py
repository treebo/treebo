# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0002_auto_20160927_0555'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='utm_medium',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='utm_source',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
