from apps.discounts.models import FlatPricingConfig
from apps.discounts.models import FlatPricingRange
from apps.common.property_service import PropertyService


class FlatPricingService:

    @staticmethod
    def get_price_override(pdto, hotel_id, rate_plan, flat_pricing_details):
        if hotel_id in flat_pricing_details['flat_pricing_enabled_hotel_ids']:
            pretax_post_coupon_price = rate_plan.pretax_price
            flat_pricing_range_list = flat_pricing_details['flat_pricing_range_list']
            flat_pricing_range_object = None

            for flat_pricing_range in flat_pricing_range_list:
                range_start = flat_pricing_range['range_start']
                range_end = flat_pricing_range['range_end']
                pre_tax = flat_pricing_range['pre_tax']
                if range_start <= pretax_post_coupon_price <= range_end and pretax_post_coupon_price >= pre_tax:
                    flat_pricing_range_object = flat_pricing_range
                    break

            if flat_pricing_range_object:
                pre_tax = flat_pricing_range_object['pre_tax']
                tax = flat_pricing_range_object['tax']
                coupon_discount = pretax_post_coupon_price - pre_tax
                rate_plan.pretax_price = pre_tax
                rate_plan.coupon_discount = coupon_discount
                rate_plan.tax = tax
                rate_plan.sell_price = pre_tax + tax

        return rate_plan

    @staticmethod
    def get_flat_price_details(booking_date, booking_channel, utm_source):
        flat_pricing_config_object = FlatPricingConfig.objects.filter(start_date__lte=booking_date,
                                                                      end_date__gte=booking_date).first()

        if flat_pricing_config_object:
            accepted_booking_channel_list = flat_pricing_config_object.accepted_booking_channel.split(',')
            accepted_utm_source_list = flat_pricing_config_object.accepted_utm_source.split(',')
            if (booking_channel and booking_channel.lower() in accepted_booking_channel_list) or \
                    (utm_source and utm_source.lower() in accepted_utm_source_list):
                flat_pricing_range_list = FlatPricingRange.objects.values()
                return True, flat_pricing_config_object.enabled_property_providers, flat_pricing_range_list
        return False, None, None
