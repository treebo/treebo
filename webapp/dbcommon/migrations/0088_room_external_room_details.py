# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-08-30 10:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0087_externalproviderroomdetails'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='external_room_details',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='dbcommon.ExternalProviderRoomDetails'),
        ),
    ]
