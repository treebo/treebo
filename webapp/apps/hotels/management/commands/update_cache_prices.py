__author__ = 'ansilkareem'

import logging

from django.core.management.base import BaseCommand
from webapp.apps.hotels.tasks import update_prices_in_cache

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "command to trigger update min price celery task"

    def handle(self, *args, **options):
        logger.info("calling celery task for updating min prices")
        try:
            update_prices_in_cache.delay()
        except Exception as e:
            logger.exception(str(e))
