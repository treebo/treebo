import logging

from apps.hotels.api.v1.get_all_cities import CityInformation
from apps.hotels.api.v1.get_hotels import HotelInformation
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class CityDetails(TreeboAPIView):
    def get(self, request, hashedCityId="", format=None):
        print("city id in city details")
        print((type(hashedCityId)))
        cityObject = CityInformation()
        hotelObject = HotelInformation()
        cityResult = cityObject.get(self, request, hashedCityId)
        hotelsResult = hotelObject.get(self, request, hashedCityId)
        response = self.__generateResponse(cityResult, hotelsResult)

        return response

    def __generateResponse(self, cityResult, hotelsResult):
        response = {}

        if hotelsResult['message'] == 'success' and cityResult['message'] == 'success':
            response['message'] = 'success'
            response['status'] = 1
            response['hotels_information'] = hotelsResult['hotels_information']
            response['city_information'] = cityResult['city_information']

        return response
