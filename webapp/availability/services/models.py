# -*-coding:utf-8-*-
import attr


@attr.s
class TDHotelAvailabilitySearchParams(object):
    """
    This is the pojo for holding the search criteria for Hotel availability. It basically takes the
    checkin(datetime.date), checkout(datetime.date), the hotel ids being searched for(List of ids)
    the room config(string) the person is interested in, the channel(string) from where the requests
    have originated and the sub-channel(string) from where the request have originated
    """
    WEB_CHANNEL = "web"
    DIRECT_SUB_CHANNEL = "direct"
    hotels = attr.ib(default=[])
    checkin = attr.ib(default=None)
    checkout = attr.ib(default=None)
    room_config = attr.ib(default=None)
    channel = attr.ib(default=WEB_CHANNEL)
    sub_channel = attr.ib(default=DIRECT_SUB_CHANNEL)
    hotel_cs_ids_map = attr.ib(default=None)
    cs_ids = attr.ib(default=[])


@attr.s
class TDRoomTypeDet(object):
    """
    This is the pojo containing the number of rooms of a particular type
    """
    room_type = attr.ib(default=None)
    rooms_available = attr.ib(default=None)


@attr.s
class TDRoomConfigDet(object):
    """
    This is the pojo containing the list of TDRoomTypeDet pertaining to a particular config
    """
    room_config = attr.ib(default=None)
    # This is a list of TDRoomTypeDet
    room_types_available = attr.ib(default=None)


@attr.s
class TDConfigAvailabilityOnDate(object):
    """
    This is the Pojo containing information of config types that are available on a date.
    It contains a date and a list of TDRoomConfigDets of the date
    """
    date = attr.ib(default=None)
    # This is the list of TDRoomConfigDet available on the date
    room_config_list = attr.ib(default=None)


@attr.s
class TDHotelAvailability(object):
    """
    This is the pojo for holding the date wise availability of a Hotel
    """
    hotel_id = attr.ib(default=None)
    # The catalogue service id of the room
    hotel_cs_id = attr.ib(default=None)
    # This is the list of TDConfigAvailabilityOnDate signifying the availability of
    # the hotel on a sequence of dates requested.
    config_wise_availability = attr.ib(default=None)
    # This is the list of TDRoomTypeDet signifying the availability of
    # the types of rooms available in a Hotel on the range of dates specified
    room_type_availability = attr.ib(default=None)


@attr.s
class TDAvailabilityAcrossConfig(object):
    """
    This is the pojo for getting the availability result from ITS for a particular set of dates
    with different configuration
    """
    # This confirms to the class TDHotelAvailabilitySearchParams
    request_data = attr.ib(default=None)
    # This is the list of TDHotelAvailability signifying the available
    # types of rooms in all the Hotel on the range of dates specified
    data = attr.ib(default=None)


class TDRoomTypeAvailabilityOnDate(object):
    """
    This is the pojo for getting the availability result from ITS for a particular set of dates
    with different configuration
    """
    hotel_id = attr.ib(default=None)
    # The catalogue service id of the room
    hotel_cs_id = attr.ib(default=None)
    # This is the dict signifying the available
    # types of rooms in a hotel (TDRoomTypeDet) on a specified date
    data = attr.ib(default={})

