import logging

from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from apps.bookings.serializers import BookingSerializer
from apps.bookings.services.booking import BookingService
from apps.checkout.serializers import BookingRequestSerializer
from apps.checkout.service.booking_request_details import BookingRequestParser
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.common.exceptions.custom_exception import InitiateBookingValidationError
from apps.common.slack_alert import SlackAlertService
from common.exceptions.treebo_exception import TreeboException
from data_services.exceptions import HotelDoesNotExist
from data_services.respositories_factory import RepositoriesFactory
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.bookings.services.axis_rooms_booking_service import AxisRoomsBookingService
from django.conf import settings
from apps.bookings.constants import HOMESTAY_PROVIDER_WT, HOMESTAY_PROPERTY_TYPE
from base.middlewares.set_request_id import get_current_user, get_current_request_id

logger = logging.getLogger(__name__)


class InitiateBooking:

    @staticmethod
    def get_booking_service(hotel):

        logger.info("Booking initiate api called for hotel %s and property type %s", hotel.name, hotel.property_type)
        if hotel.provider_name == HOMESTAY_PROVIDER_WT:
            return AxisRoomsBookingService()
        else:
            return BookingService()

    @staticmethod
    def internal_booking_initiate(booking_request):
        """
        For internal booking method
        :param booking_request:
        :return:
        """
        payload = BookingRequestSerializer(instance = booking_request).data
        booking_data = BookingRequestParser.booking_request_handler(payload)
        logger.info("Parsed booking_data from booking_request as : %s", booking_data)
        hotel_id = booking_data.get('hotel_id')
        room_type = booking_data.get("room_type")
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotels = hotel_repository.filter_hotels(id = hotel_id)
        if not hotels:
            logger.error("Could not find hotel with id {}".format(hotel_id))
            raise HotelDoesNotExist
        hotel = hotels[0]
        rooms = hotel_repository.get_rooms_for_hotel(hotel)
        room = next((room for room in rooms if room.room_type_code == room_type), None)
        booking_data["hotel"] = hotel_id
        booking_data["room"] = room.id if room else None

        booking_service = BookingService()

        try:
            logger.info("Booking request for booking request id {}: {}".format(
                str(get_current_request_id()) if get_current_request_id() else None,
                str(booking_request) if booking_request else None))
            booking = booking_service.create_booking(booking_data, booking_request)
            if not booking:
                logger.error(
                    "booking initiate failed error for data %s ",
                    booking_data)
                return Response(data = {"error": error_message(
                    codes.BOOKING_NOT_FOUND)}, status = HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                output_serializer = BookingSerializer(instance = booking)
                return output_serializer.data
        except InitiateBookingValidationError as e:
            logger.exception("Invalid booking details for initialization %s", booking_data)
            SlackAlertService.send_slack_alert_for_exceptions(status=400, request_param=booking_data,message=e.message,
                                                              dev_msg="internal_booking_initiate due to InitiateBookingValidationError")
            raise InitiateBookingValidationError(e.message)
        except Exception as e:
            SlackAlertService.send_slack_alert_for_exceptions(status = 500, request_param = booking_data,
                                                              message = e.__str__(),
                                                              dev_msg = "internal_booking_initiate")
            logger.exception(
                "booking initiate failed with internal error for data %s ",
                booking_data)
            raise TreeboException
