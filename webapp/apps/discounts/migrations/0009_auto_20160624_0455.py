# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('discounts', '0008_discountcoupon_price_component_applicable'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discountcoupon',
            name='price_component_applicable',
            field=models.CharField(
                default=b'Sale Price',
                max_length=50,
                null=True,
                blank=True,
                choices=[
                    (b'Sale Price',
                     b'Sale Price'),
                    (b'Rack Rate',
                     b'Rack Rate')]),
        ),
    ]
