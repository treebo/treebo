import logging
import decimal

from base.views.api import TreeboAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR
from webapp.apps.checkout.service.upfront_part_pay_service import UpfrontPartPayStatus
from apps.common.slack_alert import SlackAlertService

from apps.bookings.models import Booking
from apps.checkout.service.upfront_part_pay_service import UpfrontPartPayService
from apps.checkout.service.confirmation_booking_handler import get_total_captured_amount


logger = logging.getLogger(__name__)

class UpfrontPartPaymentStatus(TreeboAPIView):

    upfront_partpay_service = UpfrontPartPayService()

    def get(self, request, *args, **kwargs):
        order_id = kwargs["order_id"]
        logger.info('Recieved get upfront payment status/booking status request for '
                    'order id:{}'.format(kwargs["order_id"]))
        if not order_id:
            error = {'msg': 'Order Id is missing'}
            return Response({'error': error}, status=HTTP_400_BAD_REQUEST)
        return self.get_booking_confirmation_status(order_id)

    def get_booking_confirmation_status(self, order_id):
        try:
            booking = Booking.objects.get(order_id=order_id)
            total_payable_amount = int(booking.total_amount)
            partpay_amount = decimal.Decimal(0.0)
            if booking.wallet_applied:
                total_payable_amount = total_payable_amount - int(booking.wallet_deduction)
                partpay_amount += decimal.Decimal(booking.wallet_deduction)
            if booking.payment_mode == Booking.PARTIAL_RESERVE:
                partpay_amount += self.upfront_partpay_service.get_partpay_amount(total_payable_amount)
                if get_total_captured_amount(booking.order_id, booking.wallet_applied, booking.wallet_deduction) == partpay_amount:
                    return Response(data=dict(status=UpfrontPartPayStatus.SUCCESS))
            return Response(data=dict(status=UpfrontPartPayStatus.PENDING))
        except Booking.DoesNotExist:
            logger.error("No Booking found for order_id:{order_id}".format(order_id=order_id))
            error = {'msg': 'No Boooking found for the provided order id'}
            return Response({"error": error}, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.exception("Exception While fetching the booking status from database for "
                             "order_id:{order_id}".format(order_id=order_id))
            error = {"msg": 'Error While fethcing the booking status due to:{error}'.format(error=str(e))}
            SlackAlertService.send_slack_alert_for_exceptions(status=500,
                                                              request_param={"order_id": order_id},
                                                              message='Error While fethcing the booking status '
                                                                      'due to:{error}'.format(error=str(e)),
                                                              class_name=self.__class__.__name__)
            return Response({"error": error}, status=HTTP_500_INTERNAL_SERVER_ERROR)
