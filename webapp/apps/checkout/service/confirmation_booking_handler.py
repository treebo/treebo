import logging
from concurrent.futures import ThreadPoolExecutor
from decimal import Decimal

from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

from django.conf import settings
from apps.bookings.models import Booking
from apps.bookings.serializers import BookingSerializer
from apps.bookings.services.booking import BookingService
from apps.checkout.models import BookingRequest
from apps.checkout.service.trivago_conversion_tracking import TrivagoConversionTracking
from apps.checkout.tasks import send_booking_confirmation_notification
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.common.exceptions.booking_exceptions import BookingConfirmationException
from apps.common.exceptions.custom_exception import AxisRoomBookingValidationError
from apps.common.slack_alert import SlackAlertService
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.bookings.services.axis_rooms_booking_service import AxisRoomsBookingService
from data_services.respositories_factory import RepositoriesFactory
from apps.bookings.constants import HOMESTAY_PROPERTY_TYPE, HOMESTAY_PROVIDER_WT
from axis_rooms.exceptions import AxisRoomsApiFailure
from apps.payments.service.payment_service_v2 import PaymentServiceV2
from apps.bookings.repositories.booking_repository import BookingServiceRepository
from apps.payments.service.payment_service import PaymentService

logger = logging.getLogger(__name__)


def confirm_booking(booking_request, status):
    if booking_request.status != BookingRequest.COMPLETED:
        logger.info('Confirming the Booking for '
                    'order_id %s', booking_request.order_id)
        confirm_booking_handler(booking_request, status)
    logger.info('Booking is already in confirmed state '
                'order_id %s ',
                booking_request.order_id)


def confirm_booking_handler(booking_request, status):
    order_id = booking_request.order_id
    logger.info("calling confirm booking for bid %s and order %s ", booking_request.id,
                booking_request.order_id)

    # BookingClient.confirm_booking(order_id)
    internal_booking_confirm(booking_request, order_id)
    logger.info("confirm booking success for bid %s and order %s ",booking_request.id,
                booking_request.order_id)

    booking_request.status = status
    booking_request.payment_amount = get_total_captured_amount(booking_request.order_id,
                                                               booking_request.wallet_applied,
                                                               booking_request.wallet_deduction)
    booking_request.save()
    send_booking_confirmation_notification.delay(booking_request.order_id)
    return order_id


def get_total_captured_amount(order_id, wallet_applied, wallet_deduction):
    payment_amount = 0
    payment_orders = BookingServiceRepository.get_paymentorders_by_booking_order_id_except_wallet(order_id)
    payment_amount += sum(Decimal(payment_order.amount) for payment_order in payment_orders if payment_order.is_verified())
    return payment_amount


def get_booking_service(hotel):

    logger.info("Booking confirm api called for hotel %s and property type %s", hotel.name, hotel.property_type)
    if hotel.provider_name == HOMESTAY_PROVIDER_WT:
        return AxisRoomsBookingService()
    else:
        return BookingService()


def internal_booking_confirm(booking_request, order_id=None):
    booking = None
    if not order_id:
        logger.error("order_id not recieved %s", order_id)
        return Response(data = {"error": error_message(
            codes.NO_ORDER_ID_FOUND)}, status = HTTP_400_BAD_REQUEST)
    try:
        try:
            booking = Booking.objects.get(order_id = order_id)
        except Booking.DoesNotExist as e:
            logger.error("bookng not found for order %s ", order_id)
            raise BookingConfirmationException("Booking Doesn't Exist")

        # Check if existing booking exists with given booking request id
        if booking.booking_status == Booking.CONFIRM:
            return BookingSerializer(booking).data

        booking_service = BookingService()
        if FeatureToggleAPI.is_enabled(namespace=settings.HOMESTAY_NAMESPACE,
                                       feature=settings.HOMESTAY_FEATURE_NAME):
            hotel_id = booking.hotel_id
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            hotels = hotel_repository.filter_hotels(id=hotel_id)
            hotel = hotels[0]
            booking_service = get_booking_service(hotel)

        booking_service.confirm_booking(booking)
        booking = Booking.objects.get(order_id=order_id)
        trivago_conversion_tracking = TrivagoConversionTracking(booking=booking, booking_request=booking_request)
        if trivago_conversion_tracking.is_trivago_ack_required():
            with ThreadPoolExecutor() as executor:
                executor.submit(trivago_conversion_tracking.send_booking_confirmation_ack)

        return BookingSerializer(booking).data

    except AxisRoomBookingValidationError as err:
        PaymentServiceV2().initiate_refund(booking.order_id)
        booking.booking_status = booking.TEMP_BOOKING_CREATED
        booking.save()
        SlackAlertService.send_slack_alert_for_exceptions(status=400, request_param=str(order_id),
                                                          message=err.__str__(),
                                                          dev_msg="Error in AxisRoom Booking Validation")
        raise AxisRoomBookingValidationError(message=err.message)

    except AxisRoomsApiFailure as err:
        PaymentServiceV2().initiate_refund(booking.order_id)
        booking.booking_status = booking.TEMP_BOOKING_CREATED
        booking.save()
        SlackAlertService.send_slack_alert_for_exceptions(status=500, request_param=str(order_id),
                                                          message=err.message,
                                                          dev_msg="Exception in AxisRoom API")
        raise AxisRoomsApiFailure(message=err.message, status=err.status)

    except Exception as e:
        logger.exception(
            "booking confirm failed with internal error for order_id %s ",
            order_id)
        SlackAlertService.send_slack_alert_for_exceptions(status = 500, request_param = {
            "order_id": order_id}, message = e.__str__(), dev_msg = "internal_booking_confirm")
        raise BookingConfirmationException("Booking couldn't be created")
