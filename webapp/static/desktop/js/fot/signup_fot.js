'use strict';

import $ from 'jquery';
import BaseView from '../treebolibs/baseview';
import logger from '../treebolibs/logger';
import parsley from 'parsleyjs';
import _ from 'underscore';
import * as toastr from 'toastr';
import treejax from '../treebolibs/treejax';
import selectize from 'selectize';
import '../../css/fot/signup_fot.less';


let SignupFotView = Object.create(BaseView);
let $loader = $("#commonLoader");

SignupFotView.pageLoaded = () => {
    $('#cityList').selectize();
    $('#profession').selectize();
    const isFotSignup = $('#fotSignup').val();
    const isAuthenticate = $('#isAuthenticate').val();
    if(isFotSignup == "False" && isAuthenticate == "True") {
        $loader.removeClass('hide');
        submitSignupDetailForm();
    }
    logger.log(" Sign-up Fot page loaded");
}

$(".accordion").on('click', ".accordion__action", e => {
    const $parentElem = $(e.target).closest('.signup-page__section');
    $parentElem.find('.signup-page__enter-detail').toggleClass('hide');
    $parentElem.find('.signup-page__edit-info').toggleClass('hide');
});

$(".accordion").on('click', ".fotContinue", function(e){
    var current_form = $(this).closest('.signup-page__section').find('.test-form');
    e.preventDefault();
    var submitTestParsley = $(current_form).parsley();
    submitTestParsley.validate();
    if (submitTestParsley.isValid()){
        window.scroll(200,200);
        var $parentElem = $(e.target).closest('.signup-page__section');
        $parentElem.next().find('.signup-page__enter-detail').removeClass('hide');
        $parentElem.next().find('.signup-page__edit-info').addClass('hide');
        $parentElem.find('.signup-page__enter-detail').toggleClass('hide');
        $parentElem.find('.signup-page__edit-info').toggleClass('hide');
    } else {
        toastr.error('Everyone makes mistakes! Please correct the information wherever highlighted in red :)', 'Sorry!', {timeOut: 5000, closeButton: true});
    }
});

$('#signupSubmit').click(e => {
        e.preventDefault();
        const submitSignupParsley = $(".signup-detail-form").parsley();
        submitSignupParsley.validate();
        if (submitSignupParsley.isValid()){
            $loader.removeClass('hide');
            submitSignupDetailForm(e);
            const $parentElem = $(e.target).closest('.signup-page__section');
            $parentElem.find('.signup-page__enter-detail').addClass('hide');
        }
});

let submitSignupDetailForm = ()=>{
        const data = $(".signup-detail-form").serializeArray();
        const signupDetailJson = _.object(_.pluck(data, 'name'), _.pluck(data, 'value'));
        signupDetailJson['form-type'] = "fot-profile-form";
        signupDetailJson['csrfmiddlewaretoken'] = $('input[name="csrfmiddlewaretoken"]').val();
        const p = treejax({
            url: '/fot/signup/',
            type: 'POST',
            data: signupDetailJson
        }).then((data, status, xhr) => {
            console.log(data);
            $loader.addClass('hide');
            $(".fot-success-fail").removeClass('hide');
            if(data.success && !data.alreadySignup) {
                $("#signupSuccess").removeClass('hide');
                $(".signup-page__signup-success").removeClass('hide');
            } else if (data.alreadySignup) {
                $("#alreadySignup").removeClass('hide');
                $(".signup-page__already-signup").removeClass('hide');
            } else {
                $("#signupFail").removeClass('hide');
            }
        },treeboError => {
            $loader.addClass('hide');
            $(".fot-success-fail").removeClass('hide');
            $("#signupFail").removeClass('hide');
            console.log("fail");
        }).finally(() => {
           $loader.addClass('hide');
        });
    window.scroll(0,0);
    return p;
}


$('#alreadySignupLogin').click(e => {
    $(".fot-success-fail").addClass('hide');
    $("#alreadySignup").addClass('hide');
});

$('#continueToForm').click(e => {
    location.reload();
    $('#personal-info').removeClass('hide');
    $(".fot-success-fail").addClass('hide');
    $("#signupSuccess").addClass('hide');
});

$('#redirectHomeBtn').click(e => {
    $loader.removeClass('hide');
    $(".fot-success-fail").addClass('hide');
    $("#signupFail").addClass('hide');
});

$('#testSubmit').click(function (e) {
        var current_form = $(this).closest('.signup-page__section').find('.test-form');
        e.preventDefault();
        var submitTestParsley = $(current_form).parsley();
        submitTestParsley.validate();
        if (submitTestParsley.isValid()){
            $loader.removeClass('hide');
            submitTestForm(e);
            $('.signup-page__edit-info').addClass('hide');
            $(e.target).closest('.signup-page__section').find('.signup-page__enter-detail').addClass('hide');
        } else {
            toastr.error('Everyone makes mistakes! Please correct the information wherever highlighted in red :)', 'Sorry!', {timeOut: 5000, closeButton: true});
        }
});


let submitTestForm = ()=>{
        window.scroll(0, 0);
        var data = $("#testFormOne, #testFormTwo, #testFormThree").serializeArray();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        data.push({name: 'email', value: email});
        data.push({name: 'mobile', value: mobile});
        data.push({name: 'csrfmiddlewaretoken', value: $('input[name="csrfmiddlewaretoken"]').val()})
        const p = treejax({
            url: '/fot/signup/',
            type: 'POST',
            data
        }).then((data, status, xhr) => {
            console.log("success");
            $loader.addClass('hide');
            $(".fot-success-fail").removeClass('hide');
            if (data.success) {
                $("#testSuccess").removeClass('hide');
            } else {
                $("#testFail").removeClass('hide');
            }
        },treeboError => {
            $loader.addClass('hide');
            $(".fot-success-fail").removeClass('hide');
            $("#testFail").removeClass('hide');
            console.log("fail");
        }).finally(() => {
           $loader.addClass('hide');
        });
    return p;
}

$(".schedule-first-audit").click(e => {
    $loader.removeClass('hide');
    $(".fot-success-fail").addClass('hide');
    $("#testSuccess").addClass('hide');
});

$(".make-booking").click(e => {
    $loader.removeClass('hide');
    $(".fot-success-fail").addClass('hide');
    $("#testFail").addClass('hide');
});

let signupfotConfig = { name:'', widgets:[]};
let signupfotView = SignupFotView.setup(signupfotConfig);
