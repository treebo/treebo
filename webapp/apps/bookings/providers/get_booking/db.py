# -*- coding: utf-8 -*-
import json
from datetime import datetime, timedelta

from apps.bookings.data_classes import Booking, Guest, Guests
from apps.bookings.data_classes.gst_details import GstDetails
from apps.bookings.data_classes.guest import Adult, Child
from apps.bookings.data_classes.guest_stay import GuestStay
# from apps.bookings.data_classes.payment_type import PaymentTypes
from apps.bookings.data_classes.payments import Payment, PrepaidPaymentMode, \
    ConfirmedPaymentStatus, WalletPaymentMode, PaymentOfferPaymentMode
from apps.bookings.data_classes.room_stay import RoomStay
from apps.bookings.data_classes.source import Source
from apps.bookings.data_classes.status import status_from_value, status_from_db_name
from apps.bookings.models import Booking as BookingModel
from apps.bookings.providers.interfaces.get_booking import GetBookingInterface
from apps.bookings.services.booking_utils import get_cs_room_type_code
from apps.bookingstash.utils import StashUtils
from apps.checkout.models import BookingRequest
from apps.common.data_classes.address import Address
from apps.common.data_classes.user import User
from apps.common.date_time_utils import (application_tz_now,
                                         blindly_replace_with_application_timezone)
from apps.hotels.data_classes.room_types import RoomType
from apps.pricing.data_classes import Price, DatePrice


# from apps.pricing.services import PricingSchemes


class DBGetBooking(GetBookingInterface):
    def _get(self):
        booking_model = BookingModel.objects.get(order_id=self.booking_id)
        booking = self.get_from_model(booking_model)
        return booking

    @classmethod
    def get_from_model(cls, booking_model):

        booking = Booking(uid=booking_model.order_id,
                          hotel_id=booking_model.hotel.cs_id,
                          gst_details=cls.gst_details(booking_model),
                          source=cls.source(booking_model),
                          comments=booking_model.comments,
                          )
        booking.status = status_from_db_name(booking_model.booking_status)
        booking.expiry_time_stamp = Booking.expiry_time_stamp_from_db_model(booking_model)
        booking.stay_source = Booking.stay_source_from_db_model(booking_model)

        booking.crs_version = booking_model.external_crs_booking_version
        booking.crs_reference_id = booking_model.external_crs_booking_id
        booking.crs_bill_id = ''
        if booking_model.external_crs_booking_extra_info:
            booking.crs_bill_id = json.loads(booking_model.external_crs_booking_extra_info).get(
                'crs_booking_bill_id')
        booking.crs_bill_version = ''

        booking.created_timestamp = booking_model.created_at

        booking.charges = []

        booking.hotel_id = booking_model.hotel.cs_id
        booking.is_user_logged_in = cls.is_user_logged_in(booking_model.order_id)
        booking.room_stays = cls.room_stays(booking_model, booking.is_user_logged_in)
        booking.booking_owner = cls.booking_owner(booking_model)

        booking.payments = cls.get_payments_from_model(booking_model)

        return booking

    @classmethod
    def booking_owner(cls, booking_model):
        return User(uid=booking_model.user_id.id,
                    name=booking_model.guest_name,
                    email=booking_model.guest_email,
                    phone_number=booking_model.guest_mobile,
                    country_code=booking_model.guest_country_code)

    @classmethod
    def guests(cls, thsc_booking):
        guests = []
        thsc_guests = [customer for customer in thsc_booking.customers
                       if not customer.customer_id == thsc_booking.booking_owner_id]
        for thsc_guest in thsc_guests:
            guest = Guest(uid=thsc_guest.customer_id,
                          name=(thsc_guest.first_name, thsc_guest.last_name),
                          email=thsc_guest.email,
                          phone_number=thsc_guest.phone_number,
                          )
            guests.append(guest)
        return Guests(guests)

    @classmethod
    def gst_details(cls, booking_model):

        gst_details = GstDetails(address=Address(country='NA',
                                                 state='NA',
                                                 city='NA',
                                                 pincode='NA',
                                                 line1=booking_model.organization_address,
                                                 line2=''),
                                 gstin_number=booking_model.organization_taxcode,
                                 legal_name=booking_model.organization_name
                                 )

        return gst_details

    @classmethod
    def room_stays(cls, booking_model, is_user_logged_in):
        room_configs = StashUtils.room_config_string_to_list(booking_model.room_config)
        committed_price = booking_model.get_committed_price()

        date_and_room_wise_prices = json.loads(committed_price.datewise_pricing)

        cs_room_type_id = get_cs_room_type_code(booking_model.room.room_type_code.upper())

        room_config_to_frequency = {}
        for room_config in room_configs:
            frequency = 1
            value = room_config_to_frequency.get(room_config)
            if value:
                frequency = value + 1
            room_config_to_frequency[room_config] = frequency

        room_stays = []
        count = 0
        guest = Guest(uid='',
                      name=('', ''),
                      email='',
                      phone_number='',
                      age=Adult.end_age
                      )
        diff = booking_model.checkout_date - booking_model.checkin_date
        for room_config, frequency in room_config_to_frequency.items():
            if is_user_logged_in and count == 0:
                user = cls.get_logged_in_user_details(booking_model.order_id)
                if user.phone_number != booking_model.guest_mobile:
                    first_name, last_name = User.get_name_breakup(booking_model.guest_name)
                    adult_guest = Guest(uid='',
                                        name=(first_name, last_name),
                                        email=booking_model.guest_email,
                                        phone_number=booking_model.guest_mobile,
                                        age=Adult.end_age
                                        )
                else:
                    adult_guest = guest

            if count > 1 or not is_user_logged_in:
                adult_guest = guest
            child_guest = Guest(uid='',
                                name=('', ''),
                                email='',
                                phone_number='',
                                age=Child.end_age
                                )

            checkin_datetime = cls.checkin_datetime(booking_model)
            checkout_datetime = cls.checkout_datetime(booking_model)
            guest_stays = [GuestStay(uid='', guest=adult_guest,
                                     checkin=checkin_datetime,
                                     checkout=checkout_datetime)
                           for _ in range(int(room_config[0]))
                           ]

            guest_stays.extend([GuestStay(uid='', guest=child_guest,
                                          checkin=checkin_datetime,
                                          checkout=checkout_datetime)
                                for _ in range(int(room_config[1]))
                                ])

            date_wise_prices = []

            for day in range(diff.days):
                applicable_date = booking_model.checkin_date + timedelta(days=day)

                price_for_date_and_room_conf = date_and_room_wise_prices[applicable_date.__str__()][
                    str(room_config[0]) + '-' + str(room_config[1])]

                applicable_datetime = datetime.combine(applicable_date, datetime.min.time())
                zone_aware_applicate_date = blindly_replace_with_application_timezone(
                    applicable_datetime)

                date_wise_prices.append(DatePrice(
                    date=zone_aware_applicate_date, pre_tax=None,
                    tax=None,
                    post_tax=price_for_date_and_room_conf['sell_price'] / frequency))

            for _ in range(frequency):
                room_stay = RoomStay(uid='', room_type=RoomType(
                    uid=cs_room_type_id,
                    name=booking_model.room.room_type_code.upper(),
                    max_occupancy=0
                ))
                room_stay.price = Price(date_wise_prices)
                room_stay.guest_stays = guest_stays
                room_stay.charge_ids = set()
                room_stay.status = status_from_db_name(booking_model.booking_status)
                room_stays.append(room_stay)
        count += 1
        return room_stays

    @classmethod
    def _room_stays_guest_stays(cls, booking_model, booked_room):
        guest_stays = []

        guests = booked_room.additional_guests() + [booked_room.guest]
        for guest in guests:
            _guest = Guest(uid=guest.id,
                           name=(guest.first_name, guest.last_name),
                           email=guest.email,
                           phone_number=guest.phone,
                           )

            start_date = cls.checkin_datetime(booking_model)
            end_date = cls.checkout_datetime(booking_model)

            guest_stay = GuestStay(guest.id, _guest, start_date, end_date)
            guest_stay.status = status_from_value(booking_model.status)
            guest_stays.append(guest_stay)
        return guest_stays

    @classmethod
    def source(cls, booking_model):
        source = Source.from_booking_model(booking_model)
        return source

    @classmethod
    def checkin_datetime(cls, booking_model):
        date = datetime.combine(booking_model.checkin_date, datetime.min.time())
        date = blindly_replace_with_application_timezone(date)
        date = date.replace(hour=12)
        return date

    @classmethod
    def checkout_datetime(cls, booking_model):
        date = datetime.combine(booking_model.checkout_date, datetime.min.time())
        date = blindly_replace_with_application_timezone(date)
        date = date.replace(hour=11)
        return date

    @classmethod
    def get_payments_from_model(cls, booking_model):
        payments = booking_model.get_online_payments()
        online_payments = [Payment(uid=payment.ps_payment_id,
                                   ref_id=payment.ps_payment_id,
                                   date=application_tz_now(),
                                   amount=payment.amount,
                                   mode=PrepaidPaymentMode,
                                   status=ConfirmedPaymentStatus,
                                   ) for payment in payments]

        payments = booking_model.get_wallet_payments()
        wallet_payments = [Payment(uid=payment.ps_payment_id,
                                   ref_id=payment.ps_payment_id,
                                   date=application_tz_now(),
                                   amount=payment.amount,
                                   mode=WalletPaymentMode,
                                   status=ConfirmedPaymentStatus,
                                   ) for payment in payments]
        payments = booking_model.get_offer_price_payments()
        payment_offer_payments = [Payment(uid=payment.ps_payment_id,
                                          ref_id=payment.ps_payment_id,
                                          date=application_tz_now(),
                                          amount=payment.amount,
                                          mode=PaymentOfferPaymentMode,
                                          status=ConfirmedPaymentStatus,
                                          ) for payment in payments]

        payments = online_payments + wallet_payments + payment_offer_payments
        return payments

    @staticmethod
    def is_user_logged_in(booking_id):
        booking_request = BookingRequest.objects.get(order_id=booking_id)
        return booking_request.user_login_state == "LOGGEDIN"

    @staticmethod
    def get_logged_in_user_details(booking_id):
        booking_request = BookingRequest.objects.get(order_id=booking_id)
        return booking_request.user
