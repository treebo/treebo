import mock
from django.test import TestCase, RequestFactory

from apps.search.services.search import SearchService
from dbcommon.models.hotel import Hotel


class SearchServiceTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.searchService = SearchService()
        self.search_result = {
            'searchResults': [],
            'nearbyResults': [],
            'parentQuery': None,
            'isLocalitySearch': False,
            'cityOrStateOrHotelNotSearchedExplicitly': False,
            'city': None,
            'locality': None}

    def test_empty_result(self):
        hotels = self.searchService.search('')
        self.assertEqual(hotels, self.search_result)

    @mock.patch('dbcommon.models.hotel.Hotel.objects')
    def test_result_for_single_value_india(self, mock_hotel_queryset):
        mock_hotels = [mock.Mock(spec=Hotel), mock.Mock(spec=Hotel)]
        mock_hotel_queryset.filter.return_value.prefetch_related.return_value.select_related.return_value = mock_hotels

        hotels = self.searchService.search('India')

        self.assertEqual(hotels['searchResults'], mock_hotels)
        mock_hotel_queryset.filter.assert_called_once_with(
            status=Hotel.ENABLED)

    @mock.patch.object(
        SearchService,
        '_SearchService__has_city_or_locality_based_search')
    @mock.patch.object(SearchService, '_SearchService__has_hotel_based_search')
    def test_search_result_based_on_hotel(self, mock_hotel_based_search,
                                          mock_city_or_locality_based_search):
        mock_hotel_based_search.return_value = True

        hotels = self.searchService.search('Test Search Data')

        self.assertEqual(hotels, self.search_result)
        mock_hotel_based_search.assert_called_once_with(
            ['Test Search Data'], self.search_result)

    @mock.patch.object(
        SearchService,
        '_SearchService__has_city_or_locality_based_search')
    @mock.patch.object(SearchService, '_SearchService__has_hotel_based_search')
    def test_search_result_based_on_city_or_locality(
            self, mock_hotel_based_search, mock_city_or_locality_based_search):
        mock_hotel_based_search.return_value = False
        mock_city_or_locality_based_search.return_value = True

        hotels = self.searchService.search('Test Search Data')

        self.assertEqual(hotels, self.search_result)
        mock_hotel_based_search.assert_called_once_with(
            ['Test Search Data'], self.search_result)
        mock_city_or_locality_based_search.assert_called_once_with(
            ['Test Search Data'], self.search_result)

    @mock.patch.object(SearchService, '_SearchService__has_state_based_search')
    @mock.patch.object(
        SearchService,
        '_SearchService__has_city_or_locality_based_search')
    @mock.patch.object(SearchService, '_SearchService__has_hotel_based_search')
    def test_search_result_based_on_state(self, mock_hotel_based_search,
                                          mock_city_or_locality_based_search,
                                          mock_state_based_search):
        mock_hotel_based_search.return_value = False
        mock_city_or_locality_based_search.return_value = False
        mock_state_based_search.return_value = True

        hotels = self.searchService.search('Test Search Data')

        self.assertEqual(hotels, self.search_result)
        mock_hotel_based_search.assert_called_once_with(
            ['Test Search Data'], self.search_result)
        mock_city_or_locality_based_search.assert_called_once_with(
            ['Test Search Data'], self.search_result)
        mock_city_or_locality_based_search.assert_called_once_with(
            ['Test Search Data'], self.search_result)

    @mock.patch.object(SearchService, '_SearchService__has_state_based_search')
    @mock.patch.object(
        SearchService,
        '_SearchService__has_city_or_locality_based_search')
    @mock.patch.object(SearchService, '_SearchService__has_hotel_based_search')
    def test_search_result_on_empty_query_result(
            self,
            mock_hotel_based_search,
            mock_city_or_locality_based_search,
            mock_state_based_search):
        mock_hotel_based_search.return_value = False
        mock_city_or_locality_based_search.return_value = False
        mock_state_based_search.return_value = False

        hotels = self.searchService.search('Test Search Data')

        self.assertEqual(hotels, self.search_result)
        mock_hotel_based_search.assert_called_once_with(
            ['Test Search Data'], self.search_result)
        mock_city_or_locality_based_search.assert_called_once_with(
            ['Test Search Data'], self.search_result)
        mock_city_or_locality_based_search.assert_called_once_with(
            ['Test Search Data'], self.search_result)

    @mock.patch('dbcommon.models.hotel.Hotel.objects')
    def test_has_hotel_based_search_miss(self, mock_hotel_queryset):
        mock_hotel_queryset.filter.return_value.prefetch_related.return_value.select_related.return_value.exists.return_value = False
        search_result = dict(self.search_result)

        hotels = self.searchService._SearchService__has_hotel_based_search(
            ['Search 1', 'Search 2'], search_result)

        self.assertTrue(hotels is False)
        self.assertEqual(search_result, self.search_result)
        mock_hotel_queryset.filter.assert_called_once_with(
            mock.ANY, status=Hotel.ENABLED)

    @mock.patch('dbcommon.models.hotel.Hotel.objects')
    def test_has_hotel_based_search_hit(self, mock_hotel_queryset):
        """
        TODO: Not Working.. Have to fix. Figure out a way to Mock Django QuerySet APIs
        """
        mock_hotel = mock.Mock(spec=Hotel, **{})
        # mock_hotel.configure_mock()
        mock_hotel_queryset.filter.return_value.prefetch_related.return_value.select_related.return_value = [
            mock_hotel]
        mock_hotel_queryset.filter.return_value.prefetch_related.return_value.select_related.return_value.exists.return_value = True
        mock_hotel_queryset.filter.return_value.prefetch_related.return_value.select_related.return_value.first.return_value = mock_hotel
        search_result = dict(self.search_result)

        hotels = self.searchService._SearchService__has_hotel_based_search(
            ['Search 1', 'Search 2'], search_result)

        self.assertTrue(hotels is True)
        self.assertEqual(search_result['searchResults'], [mock_hotel])
        mock_hotel_queryset.filter.assert_called_once_with(
            mock.ANY, status=Hotel.ENABLED)

    @mock.patch('dbcommon.models.location.City.objects')
    @mock.patch('dbcommon.models.location.CityAlias.objects')
    @mock.patch('dbcommon.models.location.Locality.objects')
    def test_has_city_or_locality_based_search(
            self,
            mock_city_queryset,
            mock_cityalias_queryset,
            mock_locality_queryset):
        # TODO: Incomplete
        hotels = self.searchService.search('Test Search Data')

    @mock.patch('dbcommon.models.location.State.objects')
    def test_state_based_search(self, mock_state_queryset):
        # TODO: Incomplete
        hotels = self.searchService.search('Test Search Data')

    def shouldFindAllHotelsForInputWithLocality(self):
        hotels = self.searchService.search(
            'Military Road, Marol, Mumbai, Maharashtra, India',
            'Mumbai',
            '19.1725542',
            '72.94253700000002')
        self.assertTrue(len(hotels) is 4)
        for hotel in hotels:
            self.assertIn('Mumbai', hotel.city.name)
            self.assertIn('Maharashtra', hotel.state.name)

    def shouldFindAllHotelsForState(self):
        hotels = self.searchService.search(
            'Maharashtra, India', '', '19.7514798', '75.71388839999997')

        self.assertTrue(len(hotels) is 7)
        for hotel in hotels:
            self.assertIn('Maharashtra', hotel.state.name)

    def shouldFindHotelsWithoutLocalityAndLatLong(self):
        hotels = self.searchService.search(
            'Military Road, Marol, Mumbai, Maharashtra, India')
        self.assertTrue(len(hotels) is 7)

    def shouldFindAllHotelsInCity(self):
        hotels = self.searchService.search('Hyderabad, Telangana, India')
        self.assertTrue(len(hotels) is 3)

    def shouldFindAllHotelsInLocality(self):
        hotels = self.searchService.search(
            'Banjara Hills, Hyderabad, Telangana, India')
        self.assertTrue(len(hotels) is 3)

    def shouldFindAllHotelsInState(self):
        hotels = self.searchService.search('Karnataka, India')
        self.assertTrue(len(hotels) is 4)
