import logging

import requests
from django.conf import settings

from apps.common.exceptions.custom_exception import UPSClientException
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class UserProfileServiceClient:

    def create_new_user(self, data):
        url = settings.USER_PROFILE_SERVICE_URL + "/ups/v1/users"
        response = requests.post(url, json=data)

        if response.status_code != 201:
            logger.error('Error while creating user: {text} with status {status_code}'.format(text=response.text,
                                                                                              status_code=response.status_code))
            raise UPSClientException(message=response.text)

        data = response.json()["data"]

        return data

    def get_user_details(self, phone_number, email_id=None, name=None):
        url = settings.USER_PROFILE_SERVICE_URL + "/ups/v1/users?phone={phone_number}".format(phone_number=phone_number)
        response = requests.get(url)

        if response.status_code != 200:
            logger.error('Error while getting user: {text} with status {status_code}'.format(text=response.text,
                                                                                             status_code=response.status_code))
            if response.status_code == 500:
                slack_alert.send_slack_alert_for_exceptions(status=500, request_param='GET',
                                                            message="FAILED FETCHING USER FROM UPS",
                                                            class_name=self.__class__.__name__)
            return None

        response = response.json()

        return response['data']

    def create_new_user_profile(self, user_id, data):
        url = settings.USER_PROFILE_SERVICE_URL + "/ups/v1/users/{user_id}/profiles".format(user_id=user_id)
        response = requests.post(url, json=data)

        if response.status_code != 201:
            logger.error(
                'Error while creating user profile: {text} with status {status_code}'.format(text=response.text,
                                                                                             status_code=response.status_code))
            raise UPSClientException(message=response.text)

        response = response.json()

        return response['data']

    def get_user_profile(self, user_id):
        url = settings.USER_PROFILE_SERVICE_URL + "/ups/v1/users/{user_id}/profiles?profile_types=b2c".format(
            user_id=user_id)
        response = requests.get(url)

        if response.status_code != 200:
            logger.error('Error while getting user profile: {text} with status {status_code}'.format(text=response.text,
                                                                                                     status_code=response.status_code))
            if response.status_code == 500:
                slack_alert.send_slack_alert_for_exceptions(status=500, request_param='GET',
                                                            message="FAILED FETCHING USER PROFILE FROM UPS",
                                                            class_name=self.__class__.__name__)
            return None

        response = response.json()

        return response['data']['user_profiles']

    def update_user_profile(self, user_profile, user_id, profile_id):
        url = settings.USER_PROFILE_SERVICE_URL + "/ups/v1/users/{user_id}/profiles/{profile_id}".format(
            user_id=user_id, profile_id=profile_id)
        response = requests.patch(url, json=user_profile)

        if response.status_code not in [200, 201]:
            logger.error(
                'Error while updating user profile: {text} with status {status_code}'.format(text=response.text,
                                                                                             status_code=response.status_code))
            raise UPSClientException(message=response.text)

        response = response.json()

        return response['data']

    def update_user_contact(self, data, user_id, contact_id):
        url = settings.USER_PROFILE_SERVICE_URL + "/ups/v1/users/{user_id}/contacts/{contact_id}".format(
            user_id=user_id, contact_id=contact_id)
        response = requests.patch(url, json=data)

        if response.status_code != 200:
            logger.error(
                'Error while updating user contact: {text} with status {status_code}'.format(text=response.text,
                                                                                             status_code=response.status_code))
            raise UPSClientException(message=response.text)

        response = response.json()

        return response['data']

    def add_user_contact(self, data, user_id):
        url = settings.USER_PROFILE_SERVICE_URL + "/ups/v1/users/{user_id}/contacts-list".format(
            user_id=user_id)
        response = requests.post(url, json=data)

        if response.status_code != 201:
            logger.error(
                'Error while adding user contact: {text} with status {status_code}'.format(text=response.text,
                                                                                           status_code=response.status_code))
            raise UPSClientException(message=response.text)

        response = response.json()

        return response['data']

    def update_user_details(self, user_id, data):
        url = settings.USER_PROFILE_SERVICE_URL + "/ups/v1/users/{user_id}".format(
            user_id=user_id)
        response = requests.patch(url, json=data)

        if response.status_code != 200:
            logger.error(
                'Error while updating user basic details: {text} with status {status_code}'.format(text=response.text,
                                                                                                   status_code=response.status_code))
            raise UPSClientException(message=response.text)

        response = response.json()

        return response['data']
