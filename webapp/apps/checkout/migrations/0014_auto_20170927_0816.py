# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0013_auto_20170913_0733'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='organization_address',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='organization_name',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='organization_taxcode',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
