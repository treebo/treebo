import logging

from django.template import Context

from apps.checkout.service.notification.basenotification import NotificationService
from common.constants import common_constants
from common.exceptions.treebo_exception import TreeboException
from apps.common import date_time_utils

logger = logging.getLogger(__name__)


class CancellationEmailService(NotificationService):
    # template_name = "hotels/mail_booking_confirmation.html"
    template_name = "email/booking/cancellation.html"

    def __init__(
            self,
            notification_type,
            content_subtype,
            to_address,
            subject,
            data):
        super(
            CancellationEmailService,
            self).__init__(
            notification_type,
            content_subtype,
            to_address,
            subject,
            data)
        self.__pagename = "cancellation_email"

    def _setData(self, data):
        NotificationService._setData(self, data)

    def createMessage(self):
        NotificationService._createMessage(self)

    def addAttachments(self, pdf_path, pdf_name):
        NotificationService._addAttachments(self, pdf_path, pdf_name)

    def sendMessage(self):
        NotificationService._sendMessage(self)

    def collateEmailData(self, booking):
        """
        Collate Email Data for Content
        :param booking:
        :param request:
        :return:
        """

        try:
            hotel = booking.hotel
            phoneNumber = hotel.phone_number
            name = booking.guest_name.split()[0]
            bookingId = booking.id
            bookingDate = date_time_utils.build_date(booking.created_at)
            customer_care_number = common_constants.CUSTOMER_CARE_NUMBER
            data = Context({"booking": booking,
                            "phone_number": phoneNumber,
                            "booking_code": booking.order_id,
                            "booking_date": bookingDate,
                            "customer_care_number": customer_care_number,
                            'first_name': name,
                            "room": booking.room})

        except Exception as exc:
            logger.exception(exc)
            raise TreeboException(exc)

        self._setData(data)
