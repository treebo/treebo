# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.bookingstash.api.v1.day_wise_availability_api import DayWiseAvailabilityAPI

app_name = 'availability'
urlpatterns = [
    url(r'^day-wise/$', DayWiseAvailabilityAPI.as_view(), name='day-wise'),
]
