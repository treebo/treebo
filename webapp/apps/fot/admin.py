import csv
import io
import logging

from django.http import HttpResponse
from django.contrib import admin
from django.contrib import messages

from apps.fot.models import FotConfig, Fot, FotReservationsRequest, FotHotel, FotHotelBlackOut
from dbcommon.admin import ReadOnlyAdminMixin

logger = logging.getLogger(__name__)


@admin.register(FotHotel)
class FotHotelAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    search_fields = ['hotel__name']
    list_filter = (
        'credits_per_audit',
        'slots_allowed',
        'interval_between_slots')
    list_display = (
        'id',
        'hotel',
        'credits_per_audit',
        'slots_allowed',
        'interval_between_slots')
    list_editable = ('credits_per_audit', 'slots_allowed')
    ordering = ('-created_at',)


@admin.register(FotHotelBlackOut)
class FotHotelBlackOutAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'fot_hotel',
        'blackout_start_time',
        'blackout_end_time',
        'status')
    #list_editable = ('fot_hotel', 'blackout_start_time', 'blackout_end_time', 'status')
    ordering = ('-created_at',)


@admin.register(FotReservationsRequest)
class FotReservationsRequestAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    search_fields = ['user__email', 'hotel__name', 'booking__order_id']
    list_filter = (
        'created_at',
        'check_in_date',
        'check_out_date',
        'status',
        'no_of_guests')
    list_display = [
        'id',
        'created_at',
        'user',
        'user__email',
        'hotel__name',
        'check_in_date',
        'check_out_date',
        'booking__order_id',
        'no_of_guests',
        'status']
    ordering = ('-created_at',)
    actions = ['download_csv', 'cancel_audit']

    def booking__order_id(self, obj):
        try:
            return str(obj.booking.order_id)
        except BaseException:
            return ''

    def hotel__name(self, obj):
        return str(obj.hotel.name)

    def user__email(self, obj):
        return str(obj.user.email)

    def cancel_audit(self, request, queryset):
        try:
            if len(queryset) == 1:
                obj = queryset.first()
                if obj.status != FotReservationsRequest.SCHEDULED:
                    return messages.error(
                        request, 'You cannot cancel a booking not in SCHEDULED state')
                response = obj.cancel_audit()
                if response.get('status') == 'success':
                    if response.get('data').get('status').lower() == 'success':
                        return messages.success(
                            request, 'Booking successfully cancelled')
                    else:
                        msg = response.get('data').get('msg')
                        return messages.error(
                            request, msg if msg else 'Unknown error')
                else:
                    raise Exception('Response not returned 200')
            else:
                return messages.error(request, 'You cant bulk cancel audits')
        except Exception as e:
            logger.exception('Fot cancel_audit failed for: %s', e)
            return messages.error(
                request, 'Some error occurred in cancellation')

    def download_csv(self, request, queryset):
        try:
            csv_file = io.StringIO()
            writer = csv.writer(csv_file)
            writer.writerow(list(self.list_display))

            for s in queryset.values_list(*self.list_display):
                writer.writerow(s)

            csv_file.seek(0)
            response = HttpResponse(csv_file, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename=fot_reservation_requests.csv'
            return response
        except BaseException:
            return messages.error(
                request, 'Some error occurred in cancellation')


@admin.register(Fot)
class FotUserAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    search_fields = ['user__email', 'user__phone_number']
    list_filter = ('status', 'user_credits')
    list_display = [
        'id',
        'created_at',
        'user',
        'user__email',
        'user__phone_number',
        'user_credits',
        'user_slots',
        'status',
        'total_audits_scheduled',
        'future_audits',
        'no_shows',
        'feedbacks_submitted',
        'qualified_feedbacks',
        'total_free_nights',
        'free_nights_remaining',
        'free_nights_deemed',
        'feedback_dropped_out']
    ordering = ('-created_at',)
    list_editable = ('user_credits', 'user_slots', 'status')
    actions = ['download_csv']

    def user__email(self, obj):
        return str(obj.user.email)

    def user__phone_number(self, obj):
        return str(obj.user.phone_number)

    def download_csv(self, request, queryset):
        try:
            csv_file = io.StringIO()
            writer = csv.writer(csv_file)
            writer.writerow(list(self.list_display))

            for s in queryset.values_list(*self.list_display):
                writer.writerow(s)

            csv_file.seek(0)
            response = HttpResponse(csv_file, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename=fot_reservation_requests.csv'
            return response
        except BaseException:
            return messages.error(
                request, 'Some error occurred in cancellation')


@admin.register(FotConfig)
class FotConfigAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'start_day_threshold',
        'end_day_threshold',
        'max_guests_allowed',
        'score_to_pass',
        'max_credits_to_be_redeemed',
        'credit_equivalent_per_night')
    list_editable = (
        'start_day_threshold',
        'end_day_threshold',
        'max_guests_allowed',
        'score_to_pass',
        'max_credits_to_be_redeemed',
        'credit_equivalent_per_night')
