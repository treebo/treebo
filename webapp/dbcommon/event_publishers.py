import logging
from base.message_queues import pricing_publishers, hotel_update_publishers
from dbcommon.serializers import StateSyncSerializer, HotelSyncSerializer, AvailabilitySerializer

logger = logging.getLogger(__name__)


def publish_state(state):
    serialized_data = StateSyncSerializer(instance=state).data
    payload = {
        "type": "state_sync",
        "data": serialized_data
    }
    pricing_publishers.publish('state', payload)


def publish_hotel(instance):
    try:
        import json
        logger.debug("Publishing hotel to listeners")
        try:
            serialized_data = HotelSyncSerializer(instance).data
        except Exception as err:
            raise Exception(str(err))
        payload = {"type": "hotel_sync", "data": serialized_data}
        logger.debug("The payload is %s" % json.dumps(payload))
        hotel_update_publishers.publish(payload=payload)
        if instance.rooms.first():
            pricing_publishers.publish('hotel', payload)
    except Exception as e:
        logger.error(str(e))
        logger.exception(str(e))


def publish_availability(availabilty):
    logger.info("Inside publish availability function")
    try:
        serialized_data = AvailabilitySerializer(instance=availabilty).data
        payload = {
            "type": "inventory",
            "data": serialized_data
        }
        logger.info("Payload formed for availability is %s" % payload)
        pricing_publishers.publish('availability', payload)
    except BaseException:
        logger.exception("Error in publishing availability to queue")
