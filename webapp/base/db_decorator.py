import functools
import logging

from django.db import DatabaseError, InterfaceError, close_old_connections
from common.custom_logger.booking.log import get_booking_logger
from apps.common.slack_alert import SlackAlertService

logger = get_booking_logger(__name__)
logger.setLevel(logging.DEBUG)


def db_retry_decorator():
    """
    :return:
    """

    def decorator(base_function):
        """
        :param base_function:
        :return:
        """

        @functools.wraps(base_function)
        def wrapper(*args, **kwargs):
            """
            :param args:
            :param kwargs:
            :return:
            """
            for attempt in range(3):
                try:
                    logger.info(
                        'calling {0} with , data {1}'.format(
                            base_function.__name__, args))
                    return base_function(*args, **kwargs)
                except (DatabaseError, InterfaceError) as e:
                    logger.error(
                        "Database error:%s occurred for attempt : {0} , Closing old \
                        connections before retrying " .format(attempt) % e.__str__())
                    try:
                        close_old_connections()
                    except Exception as e:
                        logger.exception(
                            'error %s in {0}'.format(
                                base_function.__name__) % e.__str__())
            logger.error('Database retry failed for function %s' % base_function.__name__)
            SlackAlertService.send_slack_alert_for_exceptions(status=500,
                                                              request_param='',
                                                              message='Database operation failed after retrying',
                                                              class_name=base_function.__name__)
        return wrapper

    return decorator


def db_failure_retry(retry=3):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            total_retry = retry
            while total_retry > 0:
                try:
                    return func(*args, **kwargs)
                except Exception as exception:
                    total_retry = total_retry - 1
            return func(*args, **kwargs)

        return wrapper

    return decorator