import logging

from apps.pricing.services.pricing_service import PricingService
from apps.third_party.service.google_hotel_ads.price_adaptors import GHAPriceAdaptor

logger = logging.getLogger(__name__)


class NonRatePlanPriceAdaptor(GHAPriceAdaptor):

    def fetch_price(self, hotel_ids_list, checkin, checkout, room_config):
        pricing_response = PricingService.get_price_from_soa(
            checkin=checkin,
            checkout=checkout,
            hotel_ids=hotel_ids_list,
            room_config=room_config,
            include_price_breakup=False,
            get_from_cache=True)
        hotel_prices = {hotel["hotel_id"]: hotel["rooms"]
                        for hotel in pricing_response["data"]["hotels"]}
        return hotel_prices

    def read_pretax_price(self, hotel_price, room_type, default=0):
        hotel_rooms = {price['room_type']: price for price in hotel_price}
        if room_type is not None:
            return hotel_rooms[room_type]["price"]["pretax_price"]
        else:
            return default

    def read_tax(self, hotel_price, room_type, default=0):
        hotel_rooms = {price['room_type']: price for price in hotel_price}
        if room_type is not None:
            return hotel_rooms[room_type]["price"]["tax"]
        else:
            return default
