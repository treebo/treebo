from caching.base import CachingManager, CachingMixin
from django.db import models
from djutil.models import TimeStampedModel
from django.utils.text import slugify

from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room
from dbcommon.models.location import City


class Amenity(CachingMixin, TimeStampedModel, DefaultPermissions):
    type = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    extra_info = models.CharField(max_length=200)
    image_url = models.FileField(max_length=200)
    hotels = models.ManyToManyField(Hotel, through='AmenityMemberShip')
    rooms = models.ManyToManyField(Room, through='AmenityMemberShip')

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_amenity'
        verbose_name = 'Amenity'
        verbose_name_plural = "Amenities"

    def __unicode__(self):  # __unicode__ on Python 2
        return self.type

    def __str__(self):  # __unicode__ on Python 2
        return self.type

class AmenityMemberShip(CachingMixin, TimeStampedModel, DefaultPermissions):
    room = models.ForeignKey(
        Room,
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING)
    hotel = models.ForeignKey(Hotel, on_delete=models.DO_NOTHING)
    amenity = models.ForeignKey(Amenity, on_delete=models.DO_NOTHING)
    disclaimer = models.CharField(max_length=200)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_amenitymembership'
        verbose_name = 'Amenity Membership'
        verbose_name_plural = "Amenity Memberships"

    def __unicode__(self):  # __unicode__ on Python 2
        membershipString = ""
        if self.hotel is not None:
            membershipString += (self.hotel.name + " ")
        if self.room is not None:
            membershipString += (self.room.room_type + " ")
        return '%s %s' % (membershipString, self.amenity.type)

    def __str__(self):  # __unicode__ on Python 2
        membershipString = ""
        if self.hotel is not None:
            membershipString += (self.hotel.name + " ")
        if self.room is not None:
            membershipString += (self.room.room_type + " ")
        return '%s %s' % (membershipString, self.amenity.type)


class FacilityCategory(CachingMixin, TimeStampedModel, DefaultPermissions):
    name = models.TextField(null=True, blank=True)
    status = models.BooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_facilitycategory'
        verbose_name = 'Facility Category'
        verbose_name_plural = "Facility Categories"

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Facility(CachingMixin, TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200)
    catalog_mapped_name = models.CharField(
        max_length=500, null=True, blank=True)
    url = models.FileField(max_length=200, blank=True)
    hotels = models.ManyToManyField(Hotel, blank=True)
    rooms = models.ManyToManyField(Room, blank=True)
    categories = models.ManyToManyField(FacilityCategory, blank=True)
    css_class = models.CharField(max_length=200, blank=True, null=True)
    to_be_shown = models.BooleanField(default=False)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_facility'
        verbose_name = 'Facility'
        verbose_name_plural = "Facilities"

    def __unicode__(self):  # __unicode__ on Python 2
        return self.name

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    def get_sitemap_url(self, city_name):
        amenity_name = slugify(str(self.name))
        return '/hotels-in-' + city_name.lower() + '-with-' + amenity_name.lower() + '/'


class CityAmenityMapforAutoPageCreation(CachingMixin, TimeStampedModel, DefaultPermissions):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    amenity = models.ForeignKey(Facility, null=True, blank=True)
    is_enabled = models.BooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        unique_together = ('city', 'amenity')
        db_table = 'dbcommon_cityamenitymapforautopagecreation'
        verbose_name = 'City Amenity Map for AutoPage Creation'
        #verbose_name_plural = 'City Amenity Map for AutoPage Creation'

    def __unicode__(self):
        return self.city.name + ' ' + self.amenity.name

    def __str__(self):
        return self.city.name + ' ' + self.amenity.name




class AmenitySeoDetails(CachingMixin, TimeStampedModel, DefaultPermissions):
    amenity = models.ForeignKey(Facility, on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    amenity_seo_title = models.CharField(max_length=500)
    amenity_seo_description = models.TextField(null=True, blank=True)
    amenity_seo_keyword = models.TextField(null=True, blank=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_amenityseodetails'
        verbose_name = 'Amenity SEO Details'
        verbose_name_plural = 'Amenity SEO Details'

