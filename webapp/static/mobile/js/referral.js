import BaseView from './treebolibs/baseview';
import treejax from './treebolibs/treejax';
import React from 'react';
import { render } from 'react-dom';
import auth from './auth';
import $ from 'jquery';
import 'parsleyjs';
import cookies from 'js-cookie';
import _ from 'underscore';
import '../css/referral.less';

const $commonLoader = $('#commonLoader');
const ReferralView = Object.create(BaseView);
const referrerDetails = JSON.parse($('#referrerDetails').val() || '{}');
const isReferralFlow = !_.isEmpty(referrerDetails);

ReferralView.pageLoaded = () => {
	rt.page = isReferralFlow ? 'referral' : '';
	ReferralView.render();
};

const GiftBanner = () => (
	<div className="gift">
		<div className="gift__icon"><i className="icon-gift"></i></div>
		<div className="gift__text">
			<p>{referrerDetails.campaign_text}</p>
		</div>
	</div>
);

const SocialSignup = ({ onFbSignup }) => {
	return (
		<div className="signup-social row">
			<button onClick={onFbSignup} className="btn btn--facebook col col-1">
				<i className="icon-fb"></i>| Facebook
			</button>
			<button id="googleReferralSignup" className="btn btn--google col col-2">
				<i className="icon-google"></i>| Google
			</button>
		</div>
	);
};

const EmailSignup = ({ onEmailSignup }) => {
	return (
		<div className="signup-social">
			<button onClick={onEmailSignup} className="btn btn--email">
				<i className="icon-mail"></i>| Sign up with Email
			</button>
		</div>
	);
};

class ReferralLanding extends React.Component {
	componentDidMount() {
		auth
			.getGoogleHandler()
			.then((auth2) => {
				auth2.attachClickHandler('googleReferralSignup', {}, this.props.onGoogleSignup.bind(this), this.onGoogleSignupFail.bind(this));
			});
	}

	onGoogleSignupFail(err) {
		throw err;
	}

	render() {
		return (
			<div>
				<h3 className="referral__signup-text">Signup with Treebo</h3>
				<p className="referral__block-text">EASILY USING</p>
				<SocialSignup onFbSignup={this.props.onFbSignup} />
				<p className="referral__block-text">OR USING EMAIL</p>
				<EmailSignup onEmailSignup={this.props.onEmailSignup} />
				{isReferralFlow ? null : <p className="referral__block-text referral__block-text__login">Already have an account? <a className="referral__link" href="/login">Login!</a></p>}
			</div>
		);
	}
}

class SignupForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = this.props.user;
	}

	componentDidMount() {
		$('#signupForm').parsley().on('form:submit', (e) => {
			e.submitEvent.preventDefault();
			const userDetails = this.state;
			this.props.setUserDetails(userDetails);
			this.props.onSignup();
			return false;
		});
	}

	handleFieldChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	}

	render() {
		return (
			<form id="signupForm" className="signup-form">
				<div className="signup__error error"></div>
				<div className="float-labels">
						<input onChange={this.handleFieldChange} name="name" className="float-labels__input" type="text" value={this.state.name || ''} placeholder="Name" pattern=".+" required="true" data-parsley-required="true" data-parsley-required-message="Please enter name here" />
						<label className="float-labels__label">Name</label>
				</div>
				<div className="login__full float-labels">
						<input onChange={this.handleFieldChange} name="mobile" className="float-labels__input" pattern=".+" type="number" value={this.state.mobile || ''} placeholder="Mobile Number" required="true" data-parsley-type="number" data-parsley-trigger="change" data-parsley-type-message="Enter valid mobile number" data-parsley-required-message="Please enter valid mobile number"
								data-parsley-length-message="Please enter 10 digit mobile number" data-parsley-minlength="10" data-parsley-maxlength="10" />
						<label className="float-labels__label">Mobile Number</label>
				</div>
				<div className="float-labels">
					<input onChange={this.handleFieldChange} name="email" className="float-labels__input" type="email" pattern=".+" value={this.state.email || ''} placeholder="Email ID" required="true" data-parsley-type="email" data-parsley-trigger="change" data-parsley-type-message="Enter valid email" data-parsley-required-message="Please enter a valid email address" />
					<label className="float-labels__label">Email ID</label>
				</div>
				<div className="float-labels">
						<input onChange={this.handleFieldChange} name="password" className="float-labels__input" type="password" pattern=".+" value={this.state.password || ''} placeholder="Password" required="true" data-parsley-minlength="6" data-parsley-required-message="Oops! You forgot to enter your password" />
						<label className="float-labels__label">Password</label>
				</div>
				<button type="submit" className="btn signup-form__submit">SIGN UP</button>
			</form>
		);
	}
}

class VerifyOTP extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			otp: '',
			error: '',
		};
	}

	componentDidMount() {
		$('#otpForm').parsley().on('form:submit', (e) => {
			e.submitEvent.preventDefault();
			this.props.onVerifyOTP(this.state.otp);
			return false;
		});
	}

	handleFieldChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
			error: '',
		});
	}

	render() {
		const { user, onEditMobile, onSendOTP } = this.props;
		return (
			<div className="popup">
				<div className="modal pos-abs">
					<div className="modal__body">
						<p className="popup__title">Enter One Time Passcode</p>
						<p className="popup__subtitle">OTP sent to {user.mobile} <small onClick={onEditMobile} className="popup__edit">change number</small></p>
						<form id="otpForm">
							<div className="float-labels">
								<small className="float-labels__input-link" onClick={onSendOTP}>Resend OTP</small>
								<input onChange={this.handleFieldChange} name="otp" className="float-labels__input" type="number" value={this.state.otp || ''} placeholder="One Time Passcode" pattern=".+" required="true" data-parsley-type="number" data-parsly-trigger="change" data-parsley-type-message="Enter 6 digit code" data-parsley-required="true" data-parsley-required-message="Please enter OTP here" />
								<label className="float-labels__label">One Time Passcode</label>
							</div>
							<button type="submit" className="btn popup__submit">Verify</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

class GetMobile extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			mobile: this.props.user.mobile || '',
		};
	}

	componentDidMount() {
		$('#mobileForm').parsley().on('form:submit', (e) => {
			e.submitEvent.preventDefault();
			this.props.onSendOTP(this.state.mobile);
			return false;
		});
	}

	handleFieldChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	}

	render() {
		const { user } = this.props;
		return (
			<div className="popup">
				<div className="modal pos-abs">
					<div className="modal__body">
						<p className="popup__title">Enter Mobile Number</p>
						<p className="popup__subtitle">We will send you an SMS with a One Time Password</p>
						<form id="mobileForm">
							<div className="float-labels">
								<input onChange={this.handleFieldChange} name="mobile" className="float-labels__input"
									pattern=".+" type="number" value={this.state.mobile || ''} placeholder="Mobile Number"
									required="true" data-parsley-type="number" data-parsley-trigger="change" data-parsley-type-message="Enter valid mobile number" data-parsley-required-message="Please enter valid mobile number"
									data-parsley-length-message="Please enter 10 digit mobile number" data-parsley-minlength="10" data-parsley-maxlength="10" />
								<label className="float-labels__label">Mobile Number</label>
							</div>
							<button type="submit" className="btn popup__submit">Submit</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

export class ReferralPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: {
				promo: '', // get promo from context
			},
			showSignupForm: false,
			showOTP: false,
			showMobile: false,
		};
	}

	onFbSignup = () => {
		auth
			.fbLogin()
			.then((data) => {
				if (!data.register || !isReferralFlow) {
					window.location = '/';
				} else {
					this.setState({
						user: {
							...this.state.user,
							name: data.name,
							email: data.email,
						},
						showMobile: true,
					});
				}
			});
	}

	onGoogleSignup = (googleUser) => {
		auth
			.sendGoogleRegistrationDataToServer(googleUser.getAuthResponse().id_token)
			.then((data) => {
				if (!data.register || !isReferralFlow) {
					window.location = '/';
				} else {
					this.setState({
						user: {
							...this.state.user,
							name: data.name,
							email: data.email,
						},
						showMobile: true,
					});
				}
				$commonLoader.addClass('hide');
			});
	}

	onSignup = () => {
		const $signupError = $('.signup__error');
		$signupError.html('');
		auth
			.signup(this.state.user, (err) => {
				if (err.redirect) {
					window.location = '/login/';
				} else {
					$signupError.html(err.message);
				}
				throw err;
			})
			.then(() => {
				if (!isReferralFlow) {
					window.location = '/';
				} else {
					this.setState({
						showOTP: true,
					});
				}
			});
	}

	onEmailSignup = () => {
		this.setState({
			showSignupForm: true,
		});
	}

	onSendOTP = (mobile) => {
		$commonLoader.removeClass('hide');
		mobile = typeof mobile === 'string' ? mobile : this.state.user.mobile;
		treejax({
			url: '/api/v1/auth/otp/',
			type: 'POST',
			data: {
				mobile,
				csrfmiddlewaretoken: cookies.get('csrftoken'),
			},
		}).then(() => {
			this.setState({
				user: {
					...this.state.user,
					mobile,
				},
				showMobile: false,
				showOTP: true,
			});
		}).finally(() => {
			$commonLoader.addClass('hide');
		});
	}

	onEditMobile = () => {
		this.setState({
			showOTP: false,
			showMobile: true,
		});
	}

	onVerifyOTP = (otp) => {
		$commonLoader.removeClass('hide');
		treejax({
			url: '/api/v1/auth/verify/',
			type: 'POST',
			data: {
				otp,
				mobile: this.state.user.mobile,
				csrfmiddlewaretoken: cookies.get('csrftoken'),
			},
		}).then(() => {
			window.location = '/';
		}).catch((err) => {
			throw err;
		}).finally(() => {
			$commonLoader.addClass('hide');
		});
	}

	setUserDetails = (userDetails) => {
		this.setState({
			user: {
				...this.state.user,
				...userDetails,
			},
		});
	}

	render() {
		const { user, showSignupForm, showOTP, showMobile } = this.state;
		return (
			<div className="referral">
				{
					showSignupForm
					? <SignupForm user={user} setUserDetails={this.setUserDetails} onSignup={this.onSignup} />
					: <ReferralLanding onFbSignup={this.onFbSignup} onGoogleSignup={this.onGoogleSignup} onEmailSignup={this.onEmailSignup} />
				}
				{showMobile ? <GetMobile user={user} onSendOTP={this.onSendOTP} /> : null}
				{showOTP ? <VerifyOTP user={user} onEditMobile={this.onEditMobile} onVerifyOTP={this.onVerifyOTP} onSendOTP={this.onSendOTP} /> : null}
			</div>
		);
	}
}

ReferralView.render = () => {
	render(<ReferralPage />, document.getElementById('referralPage'));
};

ReferralView.setup({ name: 'Referral Page', widgets: [] });
