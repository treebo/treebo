import logging

from apps.reviews import constants
from apps.reviews.models import TAHotelIDMapping
from apps.reviews.serializers.review_serializers import AwardsDTO, UserDTO, ResponseDetailDTO, \
    ReviewDetailDTO, \
    TopReviewsDTO
from apps.reviews.services.trip_advisor.award_hotel_mapping import HotelAwardMappingService
from apps.reviews.services.trip_advisor.hotel_overall_rating_service import HotelRatingService
from apps.reviews.services.trip_advisor.review_config_service import ReviewAppConfigService

logger = logging.getLogger(__name__)


class BaseReviewData(object):
    def __init__(self, hotel_id, ta_loc_id):
        self.hotel_id = hotel_id
        self.ta_location_id = ta_loc_id
        self.ta_enabled = False
        self.ratings = HotelRatingService()
        self.config = ReviewAppConfigService().fetch_app_config()
        self.overall_rating_obj = self.__get_overall_rating_object()
        self.ta_hotelid_mapping = TAHotelIDMapping.objects.filter(
            hotel__id=hotel_id, ta_location_id=ta_loc_id).first()

    def __get_overall_rating_object(self):
        overall_rating_obj = self.ratings.fetch_latest_overall_ratings(
            ta_loc_id=self.ta_location_id, hotel_id=self.hotel_id)
        return overall_rating_obj

    def get_awards_list(self, ):
        try:
            award_list = []
            award = dict()
            awards_map_obj = self.fetch_award_for_hotel()
            if awards_map_obj:
                if awards_map_obj.award.is_enabled:
                    award['image'] = str(
                        awards_map_obj.award_image_url).replace(
                        "http:", "https:")
                    award['year'] = str(awards_map_obj.award_year)
                    award['name'] = str(awards_map_obj.award.award_name)
                    award['description'] = str(self.config.award_description)
                    award_serializer = AwardsDTO(data=award)
                    if award_serializer.is_valid():
                        award_list.append(award_serializer.validated_data)
                    else:
                        award_list.append({})
            return award_list
        except Exception:
            logger.exception(
                "Exception in creating awards list of the ta response")
            return []

    def fetch_award_for_hotel(self, ):
        hotel_award_map = HotelAwardMappingService()
        awards_map_obj = hotel_award_map.fetch_award_name_and_hotel_wise(
            award_name=constants.EXCELLENCE_AWARD_NAME,
            hotel_id=self.hotel_id,
            ta_loc_id=self.ta_location_id)
        return awards_map_obj

    def get_top_review_serializer(self, review, default_ta_review_rating_threshold=None):
        """

        :param default_ta_review_rating_threshold:
        :param (TAHotelReviews) review:
        :return:
        """
        try:
            review_dict = dict()
            user = UserDTO(
                data={
                    'user_name': review.user_name,
                    'image_url': review.user_image_url.replace(
                        "http:",
                        "https:") if review.user_image_url else constants.DEFAULT_USER_IMAGE_URL})
            if user.is_valid():
                review_dict["user"] = user.validated_data
            else:
                review_dict["user"] = {}
            response_detail = ResponseDetailDTO(data={
                'author': review.owner_description,
                'text': review.owner_rating_reply_text,
                'response_published_date': review.owner_reply_published_date
            })
            if response_detail.is_valid():
                review_dict["response_detail"] = response_detail.validated_data
            else:
                review_dict["response_detail"] = {}
            review_detail = ReviewDetailDTO(data={
                "title": review.review_title,
                "text": review.review_text,
                "user_rating": review.user_rating,
                "review_date": review.review_published_date if default_ta_review_rating_threshold is None or review.user_rating > default_ta_review_rating_threshold else None,
                "image": review.user_rating_image_url.replace("http:", "https:"),
                "text_url": review.user_rating_text_url,
                "is_jcr": review.review_is_user_generated,
                "is_crawlable": review.review_is_crawlable
            })
            if review_detail.is_valid():
                review_dict["review_detail"] = review_detail.validated_data
            else:
                review_dict["review_detail"] = {}
            top_review = TopReviewsDTO(data=review_dict)
            if top_review.is_valid():
                return top_review.validated_data
            else:
                return {}
        except BaseException:
            logger.exception(
                'Exception in building top review serializer for hotel %s',
                self.hotel_id)
            return {}
