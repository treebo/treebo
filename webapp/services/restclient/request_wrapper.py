import logging

logger = logging.getLogger(__name__)


def wrap_internal_api_call(
        request,
        requests_api,
        uri,
        data=None,
        params=None,
        cookies=None,
        is_json=False,
        files=None):
    headers = {'referer': request.META.get('HTTP_REFERER')}
    logger.debug('Request API: %s calling URL: %s', requests_api, uri)
    logger.debug('Referer header sent with requests: %s', headers['referer'])
    if cookies:
        csrf_token = cookies.get('csrftoken', None)
    else:
        csrf_token = request.COOKIES.get('csrftoken', None)

    if csrf_token:
        headers['X-CSRFToken'] = csrf_token
    if data:
        if is_json:
            return requests_api(
                uri,
                json=data,
                params=params,
                cookies=cookies if cookies else request.COOKIES,
                headers=headers)
        elif not files:
            return requests_api(
                uri,
                data=data,
                params=params,
                cookies=cookies if cookies else request.COOKIES,
                headers=headers)
        else:
            return requests_api(
                uri,
                data=data,
                files=files,
                params=params,
                cookies=cookies if cookies else request.COOKIES,
                headers=headers)
    else:
        return requests_api(
            uri,
            params=params,
            cookies=cookies if cookies else request.COOKIES,
            headers=headers)
