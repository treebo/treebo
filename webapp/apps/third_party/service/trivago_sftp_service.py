import os
import logging
import paramiko

from django.conf import settings

from common.constants import common_constants as const

logger = logging.getLogger(__name__)


class TrivagoSftpService(object):
    host_name = 'sftp.trivago.com'
    user_name = 'treebohotels'
    path_to_encrypted_private_key = '/treebo/trivago/Trivago_private_key.txt'
    pass_phrase_to_decrypt = 'trivago'
    remote_file_path = 'outfiles/'

    @staticmethod
    def initiate_sftp_upload(local_file_path=None, test=None):
        logger.info(
            'Initiated sftp upload with local path: %s and test: %s' %
            (str(local_file_path), str(test)))
        ssh = paramiko.SSHClient()
        try:
            TrivagoSftpService.add_to_known_hosts(ssh)
            ssh = TrivagoSftpService.authenticate_ssh(ssh)
            if bool(test):
                TrivagoSftpService.test_sftp_upload(ssh)
            elif bool(local_file_path):
                TrivagoSftpService.sftp_upload(ssh, local_file_path)
            else:
                raise Exception('No parameters provided')
            return const.SUCCESS
        except Exception as e:
            logger.exception(e)
            return 'SFTP failed'
        finally:
            ssh.close()

    @staticmethod
    def add_to_known_hosts(ssh):
        logger.info('Adding to known hosts')
        try:
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.load_host_keys(
                os.path.expanduser(
                    os.path.join(
                        "~",
                        ".ssh",
                        "known_hosts")))
        except BaseException:
            logger.exception('Adding to known hosts failed')

    @staticmethod
    def authenticate_ssh(ssh):
        logger.info('Authenticating ssh')
        decrypted_private_key = TrivagoSftpService.get_decrypted_key()
        ssh.connect(
            TrivagoSftpService.host_name,
            username=TrivagoSftpService.user_name,
            pkey=decrypted_private_key)
        return ssh

    @staticmethod
    def get_decrypted_key():
        logger.info('Decrypting private key')
        decrypted_private_key = paramiko.RSAKey.from_private_key_file(
            TrivagoSftpService.path_to_encrypted_private_key,
            password=TrivagoSftpService.pass_phrase_to_decrypt)
        return decrypted_private_key

    @staticmethod
    def sftp_upload(ssh, local_file_path):
        logger.info('Putting files using SFTP')
        sftp = ssh.open_sftp()
        sftp.put(local_file_path, TrivagoSftpService.remote_file_path)
        sftp.close()

    @staticmethod
    def test_sftp_upload(ssh):
        logger.info('Testing sftp upload started')
        TrivagoSftpService.test_ssh_connection(ssh)
        TrivagoSftpService.test_sftp_connection(ssh)

    @staticmethod
    def test_ssh_connection(ssh):
        logger.info('Testing ssh connection')
        stdin, stdout, stderr = ssh.exec_command('ls')
        logger.info('SSH conn test')
        for line in stdout.readlines():
            logger.info(line.strip())

    @staticmethod
    def test_sftp_connection(ssh):
        logger.info('Testing sftp connection')
        sftp = ssh.open_sftp()
        sftp.chdir("/tmp/")
        logger.info('SFTP conn test')
        logger.info(sftp.listdir())

# paramiko.util.log_to_file(log_filename)
