import logging

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_200_OK

from apps.true_caller.services.cache_service import CacheService
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)

SUCCESS = 'Success'
PENDING = 'Pending'
FAILED = 'Failed'


class FetchUserDetail(TreeboAPIView):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FetchUserDetail, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        request_nonce = request.query_params.get('request_nonce')
        if not request_nonce:
            logger.error('Bad request in API api/v1/true-caller/user-detail/. '
                         'Param: request_nonce not found in query parameters')
            return self.get_response(status=FAILED, status_code=HTTP_400_BAD_REQUEST)

        value_from_cache = CacheService.get(request_nonce)
        if not value_from_cache:
            logger.error('Bad request in API api/v1/true-caller/user-detail/. '
                         'request_nonce not found in cache')
            return self.get_response(status=FAILED, status_code=HTTP_400_BAD_REQUEST)

        if value_from_cache == CacheService.NOT_SET:
            return self.get_response(status=PENDING)
        elif value_from_cache == CacheService.FAILED_TO_SET:
            return self.get_response(status=FAILED, status_code=HTTP_401_UNAUTHORIZED)
        else:
            response_data = {"user_detail": value_from_cache}
            return self.get_response(data=response_data)

    @staticmethod
    def get_response(status=SUCCESS, data=None, status_code=HTTP_200_OK):
        response_body = {"status": status}
        if data:
            response_body["data"] = data

        return JsonResponse(response_body, status=status_code)
