import logging
import requests
from django.conf import settings

logger = logging.getLogger(__name__)


class SendEmailService():

    @staticmethod
    def send_email(email_payload):

        logger.info(
            "In sending email for coupon generation with payload %s",
            email_payload)
        data = {
            "data": {
                "subject": email_payload["subject"],
                "body_text": email_payload["body_text"],
                "body_html": email_payload["body_html"],  # One of body_text/body_html is required
                "sender": email_payload["sender"],  # E.g: alerts.webbooking@treebohotels.com
                "sender_name": email_payload["sender_name"],  # Optional field to specify sender name
                "receivers": {
                    "to": email_payload["receivers"],
                    "cc": [],
                    "bcc": []
                },
                "consumer": email_payload["consumer"],
                "attachments": email_payload["attachments"]
            }
        }

        response = requests.post(settings.EMAIL_SERVICE_API, json=data)

        json_response = response.json()
        logger.info("Response recieved %s", json_response)
        if json_response['data']['status'] == "success":
            logger.info("Coupon Generation email is sent successfully")
        else:
            logger.info("Coupon Generation email is not sent. Failed!!!!")
        # logger.info('Renderd html --- %s', rendered)
        logger.info('Email sent done')
