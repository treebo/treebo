from django.conf import settings
from django.core.cache import cache
from django.db.models import Q
from django.utils.text import slugify

from apps.posts.service import PostService
from apps.seo.components.breadcrumb import BreadCrumb
from apps.seo.components.categories import Category
from apps.seo.components.cities_nearby import CitiesNearby
from apps.seo.components.city_coordinate_component import CityCoordinateComponent
from apps.seo.components.left_nav import LeftNav
from apps.seo.components.popular_cities import PopularCities
from apps.seo.components.top_reviews import TopReviews
from apps.seo.components.weather_widget import WeatherWidget
from apps.seo.services.common_services.check_city_name_repetition_in_area_name import \
    CheckCityNameRepetitionInAreaName
from dbcommon.models.location import Locality
from apps.seo.pages.pages import PAGES
from services.restclient.contentservicerestclient import ContentServiceClient


class LocalityCategoryPage(PAGES):

    def __init__(self, page, query, is_amp):
        self.is_amp = is_amp
        slugified_category_name = str(query.split('-in-')[0])
        self.category_name = ' '.join(slugified_category_name.split('-')).title()
        city = query.split('-')[-1]
        self.locality_exclude_list = []
        locality_name = ' '.join(str(query.split('-in-')[1]).split('-')[0:-1])
        self.locality = Locality.objects.filter(
            Q(name__iexact=locality_name) & Q(Q(city__name__iexact=city) |
                                              Q(city__slug__iexact=city))).first()

        self.city = self.locality.city
        self.locality_exclude_list = [self.locality.id]
        self.about_key = 'about_' + '_'.join(self.category_name.lower().split()) + '_' + '_'.join(
            locality_name.lower().split()) + '_' + '_'.join(self.city.name.lower().split())
        self.description = ContentServiceClient.getValueForKey(None, self.about_key, 1)
        super(LocalityCategoryPage, self).__init__(page, query, self.category_name)

    def update_meta(self):
        min_price_key = str(self.city.name) + '_min_price'
        min_sell_price_key = str(self.city.name) + '_min_sell_price'
        min_rack_rate_key = str(self.city.name) + '_min_rack_rate'
        max_discount_key = str(self.city.name) + '_max_discount'
        min_price = cache.get(min_price_key)
        min_sell_price = cache.get(min_sell_price_key)
        min_rack_rate = cache.get(min_rack_rate_key)
        max_discount = cache.get(max_discount_key)
        if max_discount:
            total_discount = str(max_discount) + "%"
        else:
            total_discount = str(settings.DEFAULT_DISCOUNT) + "%"
        temp_dict = {
            "locality_name": self.locality.name,
            "city_name": self.locality.city.slug.title() if self.locality.city.slug else self.locality.city.name.title(),
            "category_name": self.category_name,
            "price": str(min_price) if min_price else str(settings.DEFAULT_PRICE),
            "sell_price": str(min_sell_price) if min_sell_price else str(
                settings.DEFAULT_SELL_PRICE),
            "rack_rate": str(min_rack_rate) if min_rack_rate else str(settings.DEFAULT_RACK_RATE),
            "total_discount": total_discount
        }
        if CheckCityNameRepetitionInAreaName(temp_dict["locality_name"],
                                             self.city.name).check_substring():
            temp_dict["city_name"] = ''
        full_canonical_url = settings.SITE_HOST_NAME + slugify(self.category_name) + '-in-' + \
                             slugify(self.locality.name) + '-' + slugify(
            self.locality.city.slug) + '/'
        amp_url = full_canonical_url + 'amp/'
        title = None
        description = None
        if 'locality_category_title' in self.footer_data:
            title = self.footer_data['locality_category_title'] % temp_dict
        if 'locality_category_description' in self.footer_data:
            description = self.footer_data['locality_category_description'] % temp_dict
        if title and description:
            self.override_seo_content(title,
                                      description,
                                      None,
                                      full_canonical_url, is_amp=self.is_amp, amp_url=amp_url)

    def build_meta(self, *args):
        self.update_meta()
        temp_dict = {
            "locality_name": self.locality.name.upper(),
            "city_name": self.locality.city.name.upper(),
            "category_name": self.category_name.upper()
        }
        if 'locality_category_about_title' in self.footer_data:
            about_title = self.footer_data['locality_category_about_title'] % temp_dict
        else:
            about_title = ''
        self.meta_content['content'] = {
            'description': {'title': about_title,
                            'content': self.description['text']
                            } if self.description else {}
        }
        min_price_key = str(self.city.name) + '_min_sell_price'
        min_price_value = cache.get(min_price_key)
        self.meta_content['content'].update(
            {'min_price': min_price_value if min_price_value else settings.DEFAULT_SELL_PRICE})
        # top_reviews = TopReviews(self.city, self.footer_data, [])
        # self.meta_content['content'].update(top_reviews.
        #                                     build_reviews_with_area_radius(page=PAGES.LOCALITY_CATEGORY,
        #                                                                    city_name=self.city.name,
        #                                                                    latitude=self.locality.latitude,
        #                                                                    longitude=self.locality.longitude,
        #                                                                    distance_cap=self.locality.distance_cap,
        #                                                                    locality_name=self.locality.name,
        #                                                                    category_name=self.category_name))
        left_nav = LeftNav(self.city, self.footer_data, [])
        self.meta_content['content'].update(left_nav.build())
        self.meta_content['content'].update({
            'city_image': self.city.seo_image if self.city.seo_image else self.city_image_url
        })
        cities_nearby = CitiesNearby(self.city, self.footer_data)
        self.meta_content['content'].update({
            'links': [cities_nearby.build()],
            'breadcrumbs': BreadCrumb(self.city, self.footer_data, self.page, self.item_name,
                                      self.locality, None).build()
        })
        area = {}
        area['page'] = self.page
        area['identifier'] = self.locality
        weather_widget = WeatherWidget(area, self.footer_data)
        self.meta_content['content'].update(weather_widget.build())
        city_coordinate = CityCoordinateComponent(area, self.footer_data)
        self.meta_content['content'].update(city_coordinate.build())
        popular_cities = PopularCities(self.city, self.footer_data,
                                       [city.id for city in cities_nearby.sorted_nearby_cities])
        self.meta_content['content']['links'].append(popular_cities.build())
        category = Category(self.city, self.footer_data, [])
        self.meta_content['content']['links'].append(category.build())
        locality_category_page_qa_query = ','.join([self.category_name, self.locality.name])
        q_and_a = PostService(self.page, locality_category_page_qa_query,
                              [self.category_name.title(), self.locality.name.title()])
        self.meta_content['content'].update(
            q_and_a.get_post_info(page=PAGES.LOCALITY_CATEGORY,
                                  city_name=self.city.name,
                                  locality_name=self.locality.name,
                                  category_name=self.category_name))
