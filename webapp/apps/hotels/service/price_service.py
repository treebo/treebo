from apps.common.utils import round_to_two_decimal
from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.pricing_api_impl import PriceUtilsV2
from apps.pricing.utils import PriceUtils
from data_services.respositories_factory import RepositoriesFactory


class PriceService:
    def get_from_local(
            self,
            hotel,
            check_in_date,
            check_out_date,
            room_config_list):
        total_prices = {}
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        for room in hotel_repository.get_rooms_for_hotel(hotel):

            sell_price, rack_rate, tax, pretax = 0, 0, 0, 0

            for single_room_config in room_config_list:
                adult = single_room_config[0]
                if FeatureToggleAPI.is_enabled(
                        "pricing", "v2_impl_enabled", False):
                    prices, date_wise_prices, preferred_promo = PriceUtilsV2.getRoomPriceForDateRange(
                        hotel, check_in_date, check_out_date, room, adult)
                else:
                    prices, date_wise_prices, preferred_promo = PriceUtils.getRoomPriceForDateRange(
                        check_in_date, check_out_date, room, adult)
                pretax += prices["pretax"]
                tax += prices["tax"]
                sell_price += prices["sell"]
                rack_rate += prices["rack_rate"]
                pretax += prices["pretax"]
                tax += prices["tax"]
            discount_percent = 0
            if int(rack_rate):
                discount_percent = int(
                    (rack_rate - sell_price) * 100 / rack_rate)
            total_prices[
                room.room_type_code.lower()] = {
                "sell": round_to_two_decimal(sell_price),
                "rack": round_to_two_decimal(rack_rate),
                "pretax": round_to_two_decimal(pretax),
                "tax": round_to_two_decimal(tax),
                "discount_percent": discount_percent,
                "availability": False,
                "available": 0}
        return total_prices
