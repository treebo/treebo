import logging

from django.conf import settings

from apps.pricing.transformers.external_api_transformer import ExternalAPITransformer
from apps.third_party.service.google_hotel_ads.price_adaptors import GHAPriceAdaptor
logger = logging.getLogger(__name__)


class RatePlanPriceAdaptor(GHAPriceAdaptor):
    pricing_service = ExternalAPITransformer()

    def fetch_price(self, hotel_ids_list, checkin, checkout, room_config):
        utm_source = settings.GOOGLE_ADS_UTM_SOURCE
        hotel_prices = self.pricing_service.fetch_hotel_room_prices(
            hotel_ids_list, checkin, checkout, room_config, utm_source)
        return hotel_prices

    def read_pretax_price(self, hotel_price, room_type, default=0):
        if room_type is not None:
            rate_plan_prices = hotel_price[room_type]
            lowest_rate_plan_price = rate_plan_prices.price
            return lowest_rate_plan_price.pretax_price
        else:
            return default

    def read_tax(self, hotel_price, room_type, default=0):
        if room_type is not None:
            rate_plan_prices = hotel_price[room_type]
            lowest_rate_plan_price = rate_plan_prices.price
            return lowest_rate_plan_price.tax
        else:
            return default
