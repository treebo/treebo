import logging

from thsc.crs.entities.hotel import Hotel

from apps.bookings.constants import AGGREGATE_FAILURE_EMAIL_SUBJECT, AsyncJobStates, BOOKING_TAGS_LIST
from apps.bookings.models import PaymentOrder, AsyncJobStatus, Booking
from apps.bookings.services.booking_failure import BookingFailureHandler
from django.conf import settings

logger = logging.getLogger(__name__)


class FailureMailAggregateService:

    @staticmethod
    def aggregate_and_send_booking_failure_emails(failed_bookings):

        for booking_id in failed_bookings:
            booking = Booking.objects.get(id=booking_id)
            is_payment_update_rqd = FailureMailAggregateService.is_payment_update_rqd(booking)
            is_gst_update_rqd = FailureMailAggregateService.is_gst_update_rqd(booking)
            is_crs_booking = FailureMailAggregateService.is_crs_booking(booking)
            subject = FailureMailAggregateService.get_failure_email_subject(
                is_payment_update_rqd=is_payment_update_rqd, is_gst_update_rqd=is_gst_update_rqd,
                booking_failed=True, is_crs_booking=is_crs_booking)

            BookingFailureHandler.track_failure(booking_id=booking_id, reason=subject,
                                                subject=subject,
                                                email_list=settings.RESERVATION_FAILURE_CRON_EMAIL_LIST)

            async_job = failed_bookings[booking_id]
            async_job.status = AsyncJobStates.FAILURE_HANDLED
            async_job.save()

        return

    @staticmethod
    def aggregate_and_send_payment_gst_failure_emails(failed_payments, failed_gst):

        gst_emails_sent = []
        for booking_id in failed_payments:
            booking = Booking.objects.get(id=booking_id)
            is_gst_update_rqd = FailureMailAggregateService.is_gst_update_rqd(booking)

            if not is_gst_update_rqd or (is_gst_update_rqd and booking_id in failed_gst):

                if is_gst_update_rqd:
                    gst_emails_sent.append(booking_id)
                is_crs_booking = FailureMailAggregateService.is_crs_booking(booking)
                subject = FailureMailAggregateService.get_failure_email_subject(
                    is_payment_update_rqd=True, is_gst_update_rqd=is_gst_update_rqd,
                    is_crs_booking=is_crs_booking)

                BookingFailureHandler.track_failure(booking_id=booking_id, reason=subject,
                                                    subject=subject,
                                                    email_list=settings.RESERVATION_FAILURE_CRON_EMAIL_LIST)
                async_job = failed_payments[booking_id]
                async_job.status = AsyncJobStates.FAILURE_HANDLED
                async_job.save()

        for booking_id in failed_gst:
            if booking_id not in gst_emails_sent:
                booking = Booking.objects.get(id=booking_id)
                is_payment_update_rqd = FailureMailAggregateService.is_payment_update_rqd(booking)

                if is_payment_update_rqd:
                    continue
                is_crs_booking = FailureMailAggregateService.is_crs_booking(booking)
                subject = FailureMailAggregateService.get_failure_email_subject(
                        is_payment_update_rqd=False, is_gst_update_rqd=True,
                        is_crs_booking=is_crs_booking)
                BookingFailureHandler.track_failure(booking_id=booking_id, reason=subject,
                                                    subject=subject,
                                                    email_list=settings.RESERVATION_FAILURE_CRON_EMAIL_LIST)

            async_job = failed_gst[booking_id]
            async_job.status = AsyncJobStates.FAILURE_HANDLED
            async_job.save()

    @staticmethod
    def update_email_already_sent_jobs(failed_bookings_dict, failed_dict):
        email_to_be_sent = {}
        for booking_id in failed_dict:
            if booking_id not in failed_bookings_dict:
                booking_async_job = AsyncJobStatus.objects.filter(booking_id=booking_id,
                                                                  tag__in=BOOKING_TAGS_LIST).first()
                if booking_async_job.status == AsyncJobStates.SUCCESS:
                    email_to_be_sent[booking_id] = failed_dict[booking_id]
                    continue
            async_job = failed_dict[booking_id]
            async_job.status = AsyncJobStates.FAILURE_HANDLED
            async_job.save()
        return email_to_be_sent

    @staticmethod
    def is_gst_update_rqd(booking):

        gst_update_rqd = False
        if booking.organization_taxcode and booking.organization_name and \
                booking.organization_address:
            gst_update_rqd = True
        return gst_update_rqd

    @staticmethod
    def is_payment_update_rqd(booking):

        payment_orders = PaymentOrder.objects.filter(booking_order_id=booking.order_id,
                                                     status=PaymentOrder.CAPTURED)
        if payment_orders:
            return True
        return False

    @staticmethod
    def fetch_async_jobs_by_status(status):
        async_jobs = AsyncJobStatus.objects.filter(status=status).all()
        return async_jobs

    @staticmethod
    def update_initiate_booking_status(failed_bookings):
        for booking_id in failed_bookings:
            async_job = failed_bookings[booking_id]
            async_job.status = AsyncJobStates.FAILURE_HANDLED
            async_job.save()
        return

    @staticmethod
    def is_crs_booking(booking):
        if Hotel.is_managed_by_crs(booking.hotel.cs_id):
            return True
        return False

    @staticmethod
    def get_failure_email_subject(is_payment_update_rqd, is_gst_update_rqd, booking_failed=False,
                                  is_crs_booking=True):
        subject = AGGREGATE_FAILURE_EMAIL_SUBJECT["URGENT"]
        if booking_failed:
            subject = subject + AGGREGATE_FAILURE_EMAIL_SUBJECT["CREATE_BOOKING"]
        if is_payment_update_rqd:
            subject = subject + AGGREGATE_FAILURE_EMAIL_SUBJECT["PAYMENT"]
        if is_gst_update_rqd:
            subject = subject + AGGREGATE_FAILURE_EMAIL_SUBJECT["GST"]
        if is_crs_booking:
            subject = subject + AGGREGATE_FAILURE_EMAIL_SUBJECT["CRS"]
        else:
            subject = subject + AGGREGATE_FAILURE_EMAIL_SUBJECT["HX"]
        return subject
