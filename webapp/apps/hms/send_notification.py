from django.conf import settings
from django.core.mail import EmailMessage
import logging
from . import constants
logger = logging.getLogger(__name__)


def notify_hms(sender, **kwargs):
    try:
        if settings.NOTIFY_HMS_EMAIL:
            mail = EmailMessage(
                constants.POS_SUBJECT,
                constants.POS_EMAILBODY,
                settings.SERVER_EMAIL,
                constants.POS_EMAIL)
            mail.send()
    except Exception as e:
        logger.exception("Exception in sending mail")
