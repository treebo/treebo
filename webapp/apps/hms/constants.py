POS_SUBJECT = "TREEBO SCHEMA UPDATE <EOM>"
POS_EMAILBODY = ""
POS_EMAIL = [
    "saumya.dubey@treebohotels.com",
    "amith.soman@treebohotels.com",
    "sourabh.singh@treebohotels.com"]
TWITTER_REFERRAL_SHARE_MESSAGE = "Claim Rs. 500 off on your first stay with " \
                                 "Treebo hotels with 250+ amazing hotels in more than 50 cities across India: "
EMAIL_REFERRAL_SHARE_MESSAGE = "Hi,%(nl)s" \
                               "As a Friend of Treebo, I invite you to stay at India's fastest growing " \
                               "hotel chain with 250%(plus)s " \
                               "amazing hotels in more than 50 cities across India.%(nl)s " \
                               "The stays are amazing. Use this referral link %(guest_shareurl)s and claim Rs. " \
                               "500 off on your first stay with Treebo. " \
                               "The coupon will be auto-applied when you check out. %(nl)s" \
                               "Cheers, %(nl)s" \
                               "%(first_name)s"
