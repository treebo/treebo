import logging

from dbcommon.models.direction import Directions
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room
from django.db.models import Prefetch
from django.db.models.aggregates import Sum, Max, Min, Variance, Count, Avg
from data_services.exceptions import HotelDoesNotExist

logger = logging.getLogger(__name__)

class HotelWebRepository:

    related_entities_map = {
        'city': 'city',
        'state': 'state',
        'locality': 'locality',
        'category': 'category',
        'image': 'image_set',
        'facility': 'facility_set',
        'seodetails': 'seodetails_set',
        'highlights': 'hotel_highlights',
        'directions': 'directions_set',
        'rooms': 'rooms',
        'wifilocationmap': 'wifilocationmap'
    }
    select_list = ['city', 'locality', 'state']
    prefetch_list = [
        'rooms',
        'category',
        'image_set',
        'facility_set',
        'seodetails_set',
        'directions_set',
        'hotel_hightlights',
        'directions_set',
        'wifilocationmap']
    aggregate_function_map = {
        'sum': Sum,
        'max': Max,
        'min': Min,
        'variance': Variance,
        'count': Count,
        'avg': Avg}

    def get_total_hotel_count(self, only_active=True):
        logger.info("Getting the total hotel count")
        query_manager = Hotel.all_hotel_objects
        if only_active:
            query_manager = Hotel.forward_reference_objects
        count = query_manager.count()
        logger.info("The count of hotels returned by db client is %d" % count)
        return count

    def get_active_hotels(self, related_entities_list=None):
        logger.debug("In getting active hotels from db")
        try:
            query_set = Hotel.forward_reference_objects
            if related_entities_list:
                query_set = self.get_query_set_with_related_entities(
                    query_set, related_entities_list)
            return query_set.all()
        except Exception as e:
            logger.debug("Exception in getting active hotels")
            logger.exception(e)

    def get_query_set_with_related_entities(
            self, query_set, related_entities_list):
        if related_entities_list:
            select_list = self.get_select_list(related_entities_list)
            prefetch_list = self.get_prefetch_list(related_entities_list)
            if select_list:
                query_set = query_set.select_related(*select_list)
            if prefetch_list:
                query_set = query_set.prefetch_related(*prefetch_list)
        return query_set

    def get_hotel_by_id(self, hotel_id, related_entities_list=None):
        logger.debug("Getting hotel by id from db %s", hotel_id)
        hotels = self.filter_hotels(related_entities_list, id=hotel_id)
        hotel = hotels[0] if hotels else None
        logger.debug(
            "The hotel returned by get_hotel_by_id from db is %s" % hotel.name if hotel else None)
        return hotel

    def get_hotel_by_cs_id(self, cs_hotel_id, related_entities_list=None):
        logger.debug("Getting hotel by id from db %s", cs_hotel_id)
        hotels = self.filter_hotels(related_entities_list, cs_id=cs_hotel_id)
        hotel = hotels[0]
        logger.debug("The hotel returned by get_hotel_by_id from db is %s" % hotel.name)
        return hotel

    def get_single_hotel(self, related_entities_list=None, **kwargs):
        logger.debug("In getting single hotel from db")
        return Hotel.objects.get(**kwargs)

    def filter_hotels(self, related_entities_list=None, **kwargs):
        logger.debug("In filter hotels from db with args")
        query_set = Hotel.all_hotel_objects
        if kwargs:
            query_set = query_set.filter(**kwargs)
        query_set = self.get_query_set_with_related_entities(
            query_set, related_entities_list)
        hotels = query_set.all()
        return hotels

    def get_select_list(self, related_entities_list):
        select_list = []
        for entity_name in related_entities_list:
            if self.related_entities_map[entity_name] in self.select_list:
                select_list.append(self.related_entities_map[entity_name])
        return select_list

    def get_prefetch_list(self, related_entities_list):
        prefetch_list = []
        if not related_entities_list:
            return prefetch_list
        for entity_name in related_entities_list:
            if self.related_entities_map[entity_name] in self.prefetch_list:
                prefetch_list.append(self.related_entities_map[entity_name])
        return prefetch_list

    def get_all_hotelogix_ids(self, ids_list=None):
        if not ids_list:
            return Hotel.forward_reference_objects.all().values_list('hotelogix_id')
        hotelogix_ids = Hotel.forward_reference_objects.filter(
            id__in=ids_list).values_list('hotelogix_id')
        logger.debug(
            "The hotelogix ids returned by get_all_hotelogix_ids are %s" %
            str(hotelogix_ids))
        return hotelogix_ids

    def get_all_hotels(self):
        logger.debug("In getting all hotels from db")
        return Hotel.all_hotel_objects.all()

    def get_hotel_by_id_or_hx_id(
            self,
            hotel_id,
            only_enabled=False,
            related_entities_list=None):
        from django.db.models import Q
        query = Hotel.all_hotel_objects
        if only_enabled:
            query = Hotel.forward_reference_objects
        if related_entities_list:
            query = self.get_query_set_with_related_entities(
                query, related_entities_list)
        hotel = query.get(Q(id=hotel_id) | Q(hotelogix_id=hotel_id))
        logger.debug(
            "The hotel returned from get_hotel_by_id_or_hx_id is %s" %
            hotel)
        return hotel

    def get_aggregate_value(
            self,
            filter_list,
            aggregate_function_name,
            aggregate_field_name):
        aggregate_func = self.aggregate_function_map[aggregate_function_name]
        aggregate_result = Hotel.forward_reference_objects.filter(
            **filter_list).aggregate(agg_value=aggregate_func(aggregate_field_name))
        result = aggregate_result['agg_value']
        logger.info(
            "The returned value from get_aggregate_value is %s" %
            str(result))
        return result

    def get_hotels_for_id_list(self, hotel_ids):
        logger.debug("In gtting all hotels for ids %s", str(hotel_ids))
        return Hotel.forward_reference_objects.filter(id__in=hotel_ids).all()

    def get_enabled_hotels_for_city(self, city_id, related_entities_list=None):
        logger.debug("In getting enabled hotels for city %d", city_id)
        return Hotel.forward_reference_objects.filter(status=1, city__id=city_id).all()

    def get_hotel_details_with_all_related_objects(self, hotel_id):
        try:
            hotel = Hotel.all_hotel_objects.select_related(
                'city',
                'locality',
                'state').prefetch_related(
                Prefetch(
                    'rooms',
                    queryset=Room.objects.prefetch_related("facility_set")),
                Prefetch(
                    'directions_set',
                    queryset=Directions.objects.select_related("landmark_obj")),
                'image_set',
                'facility_set',
                'seodetails_set',
                'hotel_highlights').get(
                    pk=hotel_id)
            return hotel
        except Hotel.DoesNotExist:
            raise HotelDoesNotExist
        except Exception as e:
            logger.exception(e)

    def get_hotel_details_using_cs_id_with_all_related_objects(self, cs_hotel_id):
        try:
            hotel = Hotel.all_hotel_objects.select_related(
                'city',
                'locality',
                'state').prefetch_related(
                Prefetch(
                    'rooms',
                    queryset=Room.objects.prefetch_related("facility_set")),
                Prefetch(
                    'directions_set',
                    queryset=Directions.objects.select_related("landmark_obj")),
                'image_set',
                'facility_set',
                'seodetails_set',
                'hotel_highlights').get(cs_id=cs_hotel_id)

            return hotel
        except Hotel.DoesNotExist:
            raise HotelDoesNotExist
        except Exception as e:
            logger.exception(e)

    def get_all_active_hotel_ids(self):
        return Hotel.objects.all().values_list('id', flat=True)
