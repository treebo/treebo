# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0043_auto_20161021_0949'),
    ]

    operations = [
        migrations.AlterField(
            model_name='seodetails',
            name='hotel_canonical_url',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
