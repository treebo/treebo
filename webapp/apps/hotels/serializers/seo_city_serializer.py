from rest_framework import serializers

from dbcommon.models.hotel import SeoDetails

__author__ = 'Rajdeep'


class CitySeoDetailsSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='city_seo_title')
    description = serializers.CharField(source='city_seo_description')
    keyword = serializers.CharField(source='city_seo_keyword')

    class Meta:
        model = SeoDetails
        fields = ('title', 'description', 'keyword')
