from apps.content.constants import PAGES
from apps.seo.services.common_services.check_city_name_repetition_in_area_name import CheckCityNameRepetitionInAreaName

class SeoTitleStringUtil:

    def __init__(self):
        pass

    @staticmethod
    def get_qa_page_title(page, city_name, locality_name, landmark_name, category_name, amenity_name, hotel_name):
        title = "Questions & Answers about "
        return SeoTitleStringUtil.generate_page_title(amenity_name, category_name, city_name, landmark_name,
                                                      locality_name, page, title, hotel_name)

    @staticmethod
    def get_jcr_page_title(page, city_name, locality_name, landmark_name, category_name, amenity_name):
        title = "Reviews for "
        return SeoTitleStringUtil.generate_page_title(amenity_name, category_name, city_name, landmark_name,
                                                      locality_name, page, title)

    @staticmethod
    def generate_page_title(amenity_name, category_name, city_name, landmark_name, locality_name, page, title, hotel_name=None):
        temp_dict = {
            'city_name': city_name,
            'locality_name': locality_name,
            'landmark_name': landmark_name,
            'amenity_name': amenity_name,
            'category_name': category_name,
            'hotel_name': hotel_name
        }
        if page == PAGES.CITY:
            title += "%(city_name)s Hotels"
        if page == PAGES.HD:
            title += "%(hotel_name)s"
        elif page == PAGES.CATEGORY:
            title += "%(category_name)s in %(city_name)s"
        elif page == PAGES.LANDMARK:
            title += "Hotels near %(landmark_name)s"
            if not CheckCityNameRepetitionInAreaName(temp_dict["landmark_name"], temp_dict["city_name"]).check_substring():
                title += ",%(city_name)s"
        elif page == PAGES.LOCALITY:
            title += "%(locality_name)s hotels"
            if not CheckCityNameRepetitionInAreaName(temp_dict["locality_name"], temp_dict["city_name"]).check_substring():
                title += ",%(city_name)s"
        elif page == PAGES.CITY_AMENITY:
            title += "%(city_name)s hotels with %(amenity_name)s"
        elif page == PAGES.LOCALITY_CATEGORY:
            title += "%(category_name)s in %(locality_name)s"
            if not CheckCityNameRepetitionInAreaName(temp_dict["locality_name"], temp_dict["city_name"]).check_substring():
                title += ",%(city_name)s"
        elif page == PAGES.LANDMARK_CATEGORY:
            title += "%(category_name)s near %(landmark_name)s"
            if not CheckCityNameRepetitionInAreaName(temp_dict["landmark_name"], temp_dict["city_name"]).check_substring():
                title += ",%(city_name)s"
        return title % temp_dict



