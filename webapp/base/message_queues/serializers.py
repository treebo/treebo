from rest_framework import serializers

from apps.pricing.models import StateTax, LuxuryTax


class StateTaxSerializer(serializers.ModelSerializer):
    class Meta:
        model = StateTax
        fields = (
            'tax_type',
            'tax_value',
            'luxury_tax_on_base',
            'effective_date',
            'expiry_date')


class LuxuryTaxSerializer(serializers.ModelSerializer):
    class Meta:
        model = LuxuryTax
        fields = ('start_range', 'end_range', 'tax_value')


class TaxSyncSerializer(serializers.Serializer):
    state_id = serializers.IntegerField()
    state_taxes = StateTaxSerializer(many=True)
    luxury_taxes = LuxuryTaxSerializer(many=True)
