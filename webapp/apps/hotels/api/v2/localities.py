from django.core.cache import cache
from rest_framework.response import Response

from apps.hotels.serializers.locality_serializer import LocalityAPISerializer
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.location import City, CityAlias
from data_services.city_repository import CityRepository

city_data_service = RepositoriesFactory.get_city_repository()


class Localities(TreeboAPIView):
    CACHE_TIMEOUT = 24 * 60 * 60
    CACHE_KEY = 'cs_apicache_localities'

    @staticmethod
    def invalidate_cache():
        cache.delete(Localities.CACHE_KEY)
        cache.delete_pattern(Localities.CACHE_KEY + '*')

    def get(self, request, *args, **kwargs):
        """
        Finds the localities attached to a city
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        response_data = []
        city_slug_name = request.GET.get('city')
        if city_slug_name:
            # Looking for result in cache
            try:
                result = cache.get(Localities.CACHE_KEY + '_' + city_slug_name)
            except Exception as e:
                result = None
            if result:
                return Response(result)
            # No result in cache
            sanitized_city_name = city_slug_name.replace('-', ' ')

            string_filter_params = {
                'name': 'name',
                'value': sanitized_city_name,
                'case_sensitive': False,
                'partial': True}
            cities = city_data_service.get_city_with_related_entities_by_string_matching(
                prefetch_list=['locality_set'],
                string_fields_list=[string_filter_params],
                select_list=None,
            )
            if cities:
                city = cities[0]
            else:
                city = None
            #city = City.objects.prefetch_related('locality_set').filter(name__icontains=sanitized_city_name).first()
            if not city:
                city_alias = CityAlias.objects.filter(
                    alias__icontains=sanitized_city_name).first()
                if city_alias:
                    city = city_data_service.get_city_with_related_entities(
                        select_list=None, prefetch_list=['locality_set'], filter_params={
                            'name': city_alias.city})
                    if not city:
                        raise Exception
                    #city = City.objects.prefetch_related('locality_set').get(name=city_alias.city)
            if city:
                response_data = LocalityAPISerializer(
                    city.locality_set.all().order_by('name'), many=True).data

            # Setting response in cache
            cache.set(
                Localities.CACHE_KEY +
                '_' +
                city_slug_name,
                response_data,
                Localities.CACHE_TIMEOUT)
        return Response(response_data)
