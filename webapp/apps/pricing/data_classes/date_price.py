# -*- coding: utf-8 -*-
from __future__ import division

import decimal

import datetime
from treebo_commons.money import Money

from apps.common.data_classes.base_data_class import BaseDataClass
from apps.common.date_time_utils import is_naive_datetime
from apps.common.utils import quantize_and_round_off


class DatePrice(BaseDataClass):
    def __init__(self, date, pre_tax=None, tax=None, post_tax=None):
        assert isinstance(date, datetime.datetime)
        if is_naive_datetime(date):
            raise ValueError('Need time zone aware date')

        self.date = date

        if not ((pre_tax is not None and tax is not None) or post_tax):
            raise ValueError('Need one of post tax or pre tax price')

        self.pre_tax = pre_tax
        self.post_tax = post_tax
        self.tax = tax

        if pre_tax is not None and tax is not None:
            _pre_tax = pre_tax if not isinstance(pre_tax, Money) else pre_tax.amount
            self.pre_tax = decimal.Decimal(_pre_tax)

            _tax = tax if not isinstance(tax, Money) else tax.amount
            self.tax = decimal.Decimal(_tax or 0)  # since tax may come off as None sometimes we use or 0.

            self.tax = quantize_and_round_off(self.tax)
            self.pre_tax = quantize_and_round_off(self.pre_tax)

        else:
            _post_tax = post_tax if not isinstance(post_tax, Money) else post_tax.amount
            self.post_tax = decimal.Decimal(_post_tax)
            self.post_tax = quantize_and_round_off(self.post_tax)

    def date_str(self):
        return self.date.strftime('%Y-%m-%d')

    def __add__(self, price):

        if isinstance(price, DatePrice):
            if self.pre_tax is not None and self.tax is not None:
                pre_tax = self.pre_tax + price.pre_tax
                tax = self.tax + price.tax
                post_tax = None
            else:
                post_tax = self.post_tax + price.post_tax
                pre_tax = None
                tax = None

            return self.__class__(
                date=min([self.date, price.date]),
                pre_tax=pre_tax,
                tax=tax,
                post_tax=post_tax
            )
        else:
            try:
                value = decimal.Decimal(price)
                if self.pre_tax is not None and self.tax is not None:
                    pre_tax = self.pre_tax + value
                    tax = self.tax + value
                    post_tax = None
                else:
                    post_tax = self.post_tax + value
                    pre_tax = None
                    tax = None

                return self.__class__(
                    date=self.date,
                    pre_tax=pre_tax,
                    tax=tax,
                    post_tax=post_tax
                )
            except decimal.InvalidOperation:
                raise RuntimeError('Invalid input for Price addition')

    def __radd__(self, price):
        # in case of sum().
        # ref: https://stackoverflow.com/questions/5082190/typeerror-after-overriding-the-add-method
        if price == 0:
            return self
        return self.__add__(price)

    def __div__(self, other):
        return self.__class__(
            date=self.date,
            pre_tax=self.pre_tax / decimal.Decimal(other),
            tax=self.tax / decimal.Decimal(other),
        )

    def __mul__(self, other):
        return self.__class__(
            date=self.date,
            pre_tax=self.pre_tax * decimal.Decimal(other),
            tax=self.tax * decimal.Decimal(other),
        )

    __rmul__ = __mul__

    def __truediv__(self, other):
        return self.__div__(other)

    def __repr__(self):
        return '<{kls} ({d}, pretax:{p}, tax:{t})>'.format(
            kls=self.__class__.__name__,
            d=self.date,
            p=self.pre_tax,
            t=self.tax,
        )

    def __str__(self):
        return '{d}: {p}, {t}'.format(
            d=self.date_str(),
            p=self.pre_tax,
            t=self.tax,
        )

    def __eq__(self, other):
        if isinstance(other, DatePrice):
            return hash(self) == hash(other)
        return NotImplemented

    def __hash__(self):
        return hash((self.date.isoformat(), self.pre_tax, self.tax))
