import logging

from django.db.models import Q
from django.utils.encoding import smart_str

from apps.reviews.models import TAHotelIDMapping, TAAwardsHotelMapping
from apps.reviews.services.trip_advisor.awards import AwardService

logger = logging.getLogger(__name__)


class HotelAwardMappingService(object):
    def hotel_award_mapping(self, location_serializer, ta_loc_id):
        try:
            ta_hotel_map = TAHotelIDMapping.objects.filter(
                ta_location_id=ta_loc_id).first()
            if ta_hotel_map:
                logger.info(
                    "Mapping between hotel %s and ta %s exists in  HotelAwardMappingService",
                    ta_hotel_map.hotel.id,
                    ta_hotel_map.ta_location_id)
                location = location_serializer.data
                self.create_or_update_award_mapping(ta_hotel_map, location)
            else:
                logger.info(
                    "No mapping for the given tid exists %s for awards",
                    ta_loc_id)

        except Exception:
            logger.exception(
                "Exception in creating new entry in award mapping table for %s",
                ta_loc_id)

    def create_or_update_award_mapping(self, ta_hotel_map, location):
        try:
            award_received = AwardService()
            for award in location["awards"]:
                award_type = smart_str(award["award_type"])
                award_obj = award_received.fetch_award(award_name=award_type)
                if not award_obj:
                    category = award["categories"] if award["categories"] else [
                    ]
                    award_received.create_or_update_award(
                        award_name=award_type, award_category=category)
                    award_obj = award_received.fetch_award(
                        award_name=award_type)

                try:
                    hotel_award_map, create = TAAwardsHotelMapping.objects.get_or_create(
                        award=award_obj, hotel_ta_mapping=ta_hotel_map)
                    if create:
                        logger.info(
                            "create %s in Award Mapping Entry  .. create/update for hotel %s and ta_loc_id %s",
                            create,
                            ta_hotel_map.hotel.id,
                            ta_hotel_map.ta_location_id)
                except TAAwardsHotelMapping.MultipleObjectsReturned:
                    logger.info(
                        "MultipleObjectsReturned Exception in updating award mapping for hotel id %s and ta_loc_id %s",
                        ta_hotel_map.hotel.id,
                        ta_hotel_map.ta_location_id)
                    hotel_award_map = TAAwardsHotelMapping.objects.filter(
                        award=award_obj, hotel_ta_mapping=ta_hotel_map).first()

                hotel_award_map.award_year = smart_str(award["year"])
                hotel_award_map.award_image_url = smart_str(
                    award["images"]["small"])
                hotel_award_map.save()
        except Exception:
            logger.exception(
                "Exception in saving award mapping object for %s",
                smart_str(
                    location['location_id']))

    def fetch_awards_mapping(self, ta_loc_id=None, hotel_id=None):
        try:
            if ta_loc_id or hotel_id:
                awards_received = TAAwardsHotelMapping.objects.filter(
                    Q(hotel_ta_mapping__ta_location_id=ta_loc_id) | Q(hotel_ta_mapping__hotel__id=hotel_id))
                if awards_received:
                    return awards_received
                else:
                    logger.error(
                        "No award mapping exist for ta_loc_id %s and hotel_id %s",
                        ta_loc_id,
                        hotel_id)
                    return None
            else:
                logger.debug(
                    "both ta_loc_id and hotel_id are None in fetching award mappings")
                return None
        except Exception:
            logger.exception(
                "Exception in fetching the award mapping for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)
            return None

    def delete_awards_mapping(self, ta_loc_id, hotel_id):
        try:
            awards_received = self.fetch_awards_mapping(
                ta_loc_id=ta_loc_id, hotel_id=hotel_id)
            for award in awards_received:
                award.delete()
                logger.info(
                    "Deleted award successfully for ta_loc_id %s and hotel-id %s",
                    ta_loc_id,
                    hotel_id)
            else:
                logger.error(
                    "No award mapping exist for ta_loc_id %s and hotel_id %s",
                    ta_loc_id,
                    hotel_id)
        except Exception:
            logger.exception(
                "Exception in deleting award mapping for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)

    def fetch_award_name_and_hotel_wise(
            self, award_name, hotel_id=None, ta_loc_id=None):
        try:
            if ta_loc_id or hotel_id:
                award = AwardService()
                award_obj = award.fetch_award(award_name)
                if award_obj:
                    awards_map_obj = TAAwardsHotelMapping.objects.filter(
                        Q(hotel_ta_mapping__ta_location_id=ta_loc_id) | Q(hotel_ta_mapping__hotel__id=hotel_id),
                        award=award_obj).first()
                    if awards_map_obj:
                        return awards_map_obj
                    else:
                        logger.error(
                            "No award mapping exist for ta_loc_id %s and hotel_id %s",
                            ta_loc_id,
                            hotel_id)
                return None
            else:
                logger.debug(
                    "both ta_loc_id and hotel_id are None in fetching award hotel and name wise")
                return None
        except Exception:
            logger.exception(
                "Exception in fetching the award hotel and name wise for ta_loc_id %s and hotel_id %s",
                ta_loc_id,
                hotel_id)
            return None
