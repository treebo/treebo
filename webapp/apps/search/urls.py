from django.conf.urls import url

from apps.search.api.v1.auto_completion import AutoCompletionAPI
from apps.search.api.v1.blog_search import BlogSearch
from apps.search.api.v1.places_api import PlacesAPI
from apps.search.api.v1.search_api import SearchView
from apps.search.api.v1.search_contents import SearchWidgets

urlpatterns = [url(r'^autocomplete/$',
                   AutoCompletionAPI.as_view(),
                   name='search-widget-autocomplete'),
               url(r'^widgets/$',
                   SearchWidgets.as_view(),
                   name='search_widgets'),
               url(r'^$',
                   SearchView.as_view(),
                   name='search_view'),
               url(r'^places/$',
                   PlacesAPI.as_view(),
                   name='search-place'),
               url(r'^hotels/', BlogSearch.as_view(), name='blog-search'),
               ]
