import logging

from dbcommon.models.miscellanous import WifiCommonConfig, WifiLocationMap

logger = logging.getLogger(__name__)


class WifiService():
    def get_wifi_common_config(self):
        config = WifiCommonConfig.objects.all().first()
        return config

    def is_bhaifi_enabled(self, hotelogix_id):
        wifi_hotel_config = WifiLocationMap.objects.filter(
            hotel__hotelogix_id=hotelogix_id).first()

        if wifi_hotel_config and wifi_hotel_config.is_enabled:
            hotel = wifi_hotel_config.hotel
            location_key = wifi_hotel_config.location_key
            return True, hotel, location_key
        return False, None, None
