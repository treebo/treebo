

from django.conf.urls import url

from apps.communication_retry_api.email_and_sms import EmailAndSms
from apps.communication_retry_api.get_long_request import LongRequest

urlpatterns = [
    url(r'^EmailAndSms', EmailAndSms.as_view()),
    #url(r'^getLongRequest', LongRequest.as_view())
]
