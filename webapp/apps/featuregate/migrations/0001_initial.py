# -*- coding: utf-8 -*-


import jsonfield.fields
from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ConstraintExpression',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('expression', models.CharField(default=b'', max_length=2000, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConstraintKeywords',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('keyword', models.CharField(default=b'', max_length=200, null=True)),
                ('type', models.CharField(default=b'', max_length=20, null=True)),
                ('description', models.CharField(default=b'', max_length=1000, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConstraintOperators',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('operator', models.CharField(default=b'', max_length=10, null=True)),
                ('description', models.CharField(default=b'', max_length=100, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Constraints',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('value_one', models.CharField(max_length=200, null=True, blank=True)),
                ('value_two', models.CharField(max_length=200, null=True, blank=True)),
                ('keyword',
                 models.ForeignKey(to='featuregate.ConstraintKeywords', null=True, on_delete=models.DO_NOTHING)),
                ('operation',
                 models.ForeignKey(to='featuregate.ConstraintOperators', null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('page_name', models.CharField(default=b'', max_length=200)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PageVariables',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('name', models.CharField(default=b'', max_length=200)),
                ('type', models.CharField(default=b'String', max_length=200)),
                ('page', models.ForeignKey(related_name='variables', to='featuregate.Page', on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='VariableCondition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('final_value', jsonfield.fields.JSONField()),
                ('active', models.BooleanField(default=True)),
                ('expression', models.ForeignKey(to='featuregate.ConstraintExpression', on_delete=models.DO_NOTHING)),
                ('page_variable', models.ForeignKey(related_name='variable_conditions',
                                                    to='featuregate.PageVariables', on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
