import logging
import json
import urllib.request
import urllib.parse
import urllib.error
import facebook
import requests
from django.conf import settings
from django.contrib.auth import login, authenticate
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response
from rest_framework import status
from rest_framework.status import HTTP_401_UNAUTHORIZED

from apps.auth.dto.social_login_dto import SocialLoginDTO
from apps.auth.services.social_login_service import SocialLoginService
from apps.common import utils
from apps.common.utils import Utils, AnalyticsEventTracker
from apps.profiles import utils as profile_utils
from apps.auth.services.registration import UserRegistrationService
from apps.referral import tasks as referral_tasks
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboException, TreeboValidationException
from dbcommon.models.profile import User, UserMetaData, UserReferral

import logging
import os
import common

logger = logging.getLogger(__name__)


class LoginValidator(serializers.Serializer):
    email = serializers.EmailField(required=True, error_messages={
        'required': error_codes.INVALID_LOGIN_DATA,
        'blank': error_codes.INVALID_LOGIN_DATA
    })
    password = serializers.CharField(required=True, error_messages={
        'required': error_codes.INVALID_LOGIN_DATA,
        'blank': error_codes.INVALID_LOGIN_DATA
    })


logger = logging.getLogger(__name__)


def create_referral_code(user):
    try:
        UserReferral.objects.get(user=user)
    except UserReferral.DoesNotExist:
        referral_tasks.generate_referral_code.delay(user.id)


def extract_referral_details(request):
    click_id = str(request.COOKIES.get('avclk')
                   ) if request.COOKIES.get('avclk') else ''
    is_referral_sign_up = True if click_id else False
    if is_referral_sign_up:
        referral_code = click_id.split('-')[1]
    else:
        referral_code = ''

    return click_id, referral_code, is_referral_sign_up


def set_session_text_for_referral(is_referral, request):
    pass


class LoginAPI(TreeboAPIView):
    # validationSerializer = LoginValidator
    authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LoginAPI, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        raise MethodNotAllowed("GET")

    def post(self, request, format=None):
        """`
        Login a user
        :type request: django.http.request.HttpRequest
        :param request:
        :param format:
        """
        data = request.data
        username = data['email']
        password = data['password']
        user = authenticate(username=username, password=password)
        logger.info("password login with email {}".format(username))
        if user is not None:
            if user.is_active:
                login(request, user)
                utils.trackAnalyticsServerEvent(request,
                                                'Login Successful',
                                                {"medium": 'EMAIL',
                                                 'email': username,
                                                 'userid': user.id,
                                                 'name': user.first_name},
                                                channel=AnalyticsEventTracker.WEBSITE)
                try:
                    create_referral_code(user)
                except BaseException:
                    logger.info("Base Exception while login ")
                    pass
                response = Response({
                    'status': 'success',
                    'msg': 'User logged in successfully',
                    'userId': user.id,
                    'email': user.email,
                    'phone': user.phone_number,
                    'name': user.first_name,
                    'last_name': user.last_name,
                    'isSignUp': False
                })
                authenticated_user_cookie_map = {
                    "userId": user.id,
                    "email": user.email,
                    "phone": user.phone_number,
                    "name": user.first_name,
                }

                response.set_cookie(
                    'authenticated_user', urllib.parse.quote(
                        json.dumps(authenticated_user_cookie_map)))
                return response
            else:
                utils.trackAnalyticsServerEvent(
                    request, 'Inactive User', {
                        "medium": 'EMAIL', 'email': username}, channel=AnalyticsEventTracker.WEBSITE)
                if hasattr(request, 'is_mobile') and request.is_mobile:
                    return Response(
                        Utils.buildErrorResponseContext(
                            error_codes.INACTIVE_USER))
                else:
                    return Response({"error": {"msg": error_codes.get(error_codes.INACTIVE_USER)[
                                    'msg']}}, status=status.HTTP_400_BAD_REQUEST)

        else:
            utils.trackAnalyticsServerEvent(
                request, 'Login Failed', {
                    "medium": 'EMAIL', 'email': username}, channel=AnalyticsEventTracker.WEBSITE)
            if hasattr(request, 'is_mobile') and request.is_mobile:
                return Response(
                    Utils.buildErrorResponseContext(
                        error_codes.INVALID_CREDENTIALS))
            else:
                return Response({"error": {"msg": error_codes.get(
                    error_codes.INVALID_CREDENTIALS)['msg']}}, status=status.HTTP_400_BAD_REQUEST)


class FacebookLoginValidator(serializers.Serializer):
    accessToken = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)


class FacebookLoginAPI(TreeboAPIView):
    # validationSerializer = FacebookLoginValidator
    authentication_classes = []
    social_login_service = SocialLoginService()

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FacebookLoginAPI, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        raise MethodNotAllowed("GET")

    @log_args(logger)
    def post(self, request, format=None):
        """
        Login a user using details from facebook
        :type request: django.http.request.HttpRequest
        :param request:
        :param format:
        """
        try:

            accessToken = request.data.get('accessToken')
            email = request.data.get('email')
            userImage = request.data.get('imageUrl')
            click_id, referral_code, is_referral = extract_referral_details(
                request)
            graph = facebook.GraphAPI(access_token=accessToken)
            accessToken = graph.extend_access_token(
                settings.FACEBOOK_APP_ID, settings.FACEBOOK_APP_SECRET)
            args = {
                'fields': 'first_name, last_name, email, gender'
            }
            user = graph.get_object(id='me', **args)

            social_login_dto = SocialLoginDTO(
                data={'token': str(accessToken['access_token'])})
            social_login_dto.is_valid()
            external_user, token = self.social_login_service.external_app_login(
                social_login_dto.data)
            self.__updateUserMetaData(accessToken, external_user, user, userImage)
            self.__loginUser(external_user, request)
            utils.trackAnalyticsServerEvent(request,
                                            'Login Successful',
                                            {"medium": 'FB',
                                             'email': email,
                                             'userid': external_user.id,
                                             'name': external_user.first_name},
                                            channel=AnalyticsEventTracker.WEBSITE)
            response = Response({
                'status': 'success',
                'msg': 'User logged in successfully',
                'register': False,
                'email': email,
                'phone': external_user.phone_number,
                'name': external_user.first_name,
                'last_name': external_user.last_name,
                'userId': external_user.id
            })

            authenticated_user_cookie_map = {
                "userId": external_user.id,
                "email": external_user.email,
                "phone": external_user.phone_number,
                "name": external_user.first_name,
            }

            response.set_cookie(
                'authenticated_user', urllib.parse.quote(
                    json.dumps(authenticated_user_cookie_map)))
        except Exception as e:
            response = Response({
                'status': 'error',
                'msg': 'User login failed',
                'register': False,
            }, HTTP_401_UNAUTHORIZED)

        return response

    def __updateUserMetaData(self, accessToken, fb_user, user, userImage):
        """
        Update the user's meta data
        :param accessToken:
        :param fb_user:
        :param user:
        :param userImage:
        """
        if user['gender'] == 'male':
            fb_user.gender = 'M'
        else:
            fb_user.gender = 'F'
        if not fb_user.is_active:
            fb_user.is_active = True
        fb_user.save()
        # fb data save in db
        try:
            userMetaData = UserMetaData.objects.get(
                user_id=fb_user, key='from_facebook')
        except UserMetaData.DoesNotExist:
            userMetaData = UserMetaData()
        userMetaData.user_id = fb_user
        userMetaData.key = 'from_facebook'
        userMetaData.value = user['id']
        if userImage:
            userMetaData.image_url = userImage
        userMetaData.save()
        try:
            userMetaData = UserMetaData.objects.get(
                user_id=fb_user, key='access_token')
        except UserMetaData.DoesNotExist:
            userMetaData = UserMetaData()
        userMetaData.user_id = fb_user
        userMetaData.key = 'access_token'
        userMetaData.value = accessToken
        userMetaData.save()

    def __registerUser(
            self,
            accessToken,
            user,
            is_referral_sign_up,
            click_id="",
            referral_code=""):
        firstName = user['first_name']
        lastName = user['last_name']
        userObj = UserRegistrationService.register(
            firstName,
            lastName,
            "",
            user['email'],
            profile_utils.generateRandomPassword(),
            send_sms=is_referral_sign_up,
            click_id=click_id,
            referral_code=referral_code,
            send_email=not is_referral_sign_up)
        gender = user['gender']
        if gender:
            userObj.gender = 'M' if user['gender'] == "male" else 'F'
        userMetaData = UserMetaData()
        userMetaData.user_id = userObj
        userMetaData.key = 'from_facebook'
        userMetaData.value = user['id']
        userMetaData.save()
        userMetaData = UserMetaData()
        userMetaData.user_id = userObj
        userMetaData.key = 'access_token'
        userMetaData.value = accessToken
        userMetaData.save()
        return userObj

    def __loginUser(self, fb_user, request):
        fb_user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, fb_user)


class GooglePlusLoginValidator(serializers.Serializer):
    auth_token = serializers.CharField(required=True)


class GooglePlusLoginAPI(TreeboAPIView):
    # validationSerializer = GooglePlusLoginValidator
    authentication_classes = []
    social_login_service = SocialLoginService()

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GooglePlusLoginAPI, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        raise MethodNotAllowed("GET")

    @log_args(logger)
    def post(self, request, format=None):
        """
        Login a user using details from Google Plus
        :param request: accessToken, email
        :param format:
        :return:
        """
        authToken = request.data.get('auth_token')
        params = {"id_token": authToken}
        response = requests.get(settings.GOOGLE_AUTH_URL, params=params)
        if response.status_code == 200 or response.status_code == 304:
            rJson = response.json()
            email = rJson['email']
        else:
            utils.trackAnalyticsServerEvent(
                request, 'Login Failed', {
                    "medium": 'GOOGLE', 'email': email}, channel=AnalyticsEventTracker.WEBSITE)
            raise TreeboException("Google Sign-in Failed!")

        social_login_dto = SocialLoginDTO(data={'token': authToken})
        social_login_dto.is_valid()
        g_user, token = self.social_login_service.google_login(
            social_login_dto.data)
        self.__updateUserMetaData(authToken, g_user, rJson)
        self.__loginUser(g_user, request)
        try:
            create_referral_code(g_user)
        except BaseException:
            pass
        utils.trackAnalyticsServerEvent(request,
                                        'Login Successful',
                                        {"medium": 'GOOGLE',
                                         'email': email,
                                         'userid': g_user.id,
                                         'name': g_user.first_name},
                                        channel=AnalyticsEventTracker.WEBSITE)

        response = Response({
            'msg': 'User logged in successfully',
            'register': False,
            'userId': g_user.id,
            'email': email,
            'phone': g_user.phone_number,
            'name': g_user.first_name,
            'last_name': g_user.last_name,
        })
        authenticated_user_cookie_map = {
            "userId": g_user.id,
            "email": g_user.email,
            "phone": g_user.phone_number,
            "name": g_user.first_name,
        }
        response.set_cookie(
            'authenticated_user', urllib.parse.quote(
                json.dumps(authenticated_user_cookie_map)))
        return response

    def __updateUserMetaData(self, accessToken, g_user, response):
        """
        Update the user's meta data
        :param accessToken:
        :param g_user:
        """
        g_user.gender = 'M'
        if not g_user.is_active:
            g_user.is_active = True
        g_user.save()
        # google data save in db
        try:
            userMetaData = UserMetaData.objects.get(
                user_id=g_user, key='from_google')
        except UserMetaData.DoesNotExist:
            userMetaData = UserMetaData()
        userMetaData.user_id = g_user
        userMetaData.key = 'from_google'
        userMetaData.value = response['sub']
        if response.get('picture', None):
            userMetaData.image_url = response['picture']
        userMetaData.save()

        try:
            userMetaData = UserMetaData.objects.get(
                user_id=g_user, key='auth_token')
        except UserMetaData.DoesNotExist:
            userMetaData = UserMetaData()
        userMetaData.user_id = g_user
        userMetaData.key = 'auth_token'
        userMetaData.value = accessToken
        userMetaData.save()

    def __registerUser(
            self,
            accessToken,
            response,
            is_referral_sign_up,
            click_id="",
            referral_code=""):
        firstName = response['given_name']
        lastName = response['family_name']
        userObj = UserRegistrationService.register(
            firstName,
            lastName,
            "",
            response['email'],
            profile_utils.generateRandomPassword(),
            send_sms=is_referral_sign_up,
            referral_code=referral_code,
            click_id=click_id,
            send_email=not is_referral_sign_up)
        userObj.gender = 'M'
        userMetaData = UserMetaData()
        userMetaData.user_id = userObj
        userMetaData.key = 'from_google'
        userMetaData.value = response['sub']
        userMetaData.save()

        userMetaData = UserMetaData()
        userMetaData.user_id = userObj
        userMetaData.key = 'auth_token'
        userMetaData.value = accessToken
        userMetaData.save()

        return userObj

    def __loginUser(self, g_user, request):
        g_user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, g_user)
