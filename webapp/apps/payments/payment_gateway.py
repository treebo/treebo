import hashlib
import logging

from django.conf import settings

from apps.checkout.bookinghelper import BookingHelper
from apps.pricing.dateutils import date_to_ymd_str
from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class PaymentGateway(object):
    def build_pg_payload(self, booking_request, is_secure=True):
        """
        :param (apps.checkout.models.BookingRequest) booking_request :
        :param is_secure:
        :return:
        """
        pass


class PayU(PaymentGateway):
    def build_pg_payload(self, booking_request, is_secure=True):
        """
        :param (apps.checkout.models.BookingRequest) booking_request:
        :param is_secure:
        :return:
        """
        payuKey = settings.PAYU_MERCHANT_ID
        salt = settings.PAYU_SALT
        txnid = 'treebo-' + str(booking_request.order_id)
        http_or_https = 'https:' if is_secure else 'http:'
        handler_url = http_or_https + settings.PAYU_HANDLER_URL
        action_url = settings.PAYU_ACTION_URL
        offerKey = 'Axis bank Offer@1858'
        referenceId = booking_request.id
        encryptionString = payuKey + '|' + txnid + '|' + str(int(
            booking_request.total_amount)) + '|' + 'rooms' + '|' + booking_request.name + '|' + booking_request.email + '|' + str(
            referenceId) + '||||||||||' + salt
        hash_object = hashlib.sha512(encryptionString)
        hashValue = hash_object.hexdigest()

        context = {
            'txnid': txnid,
            'amount': int(
                booking_request.total_amount),
            'firstname': booking_request.name,
            'salt': '',
            'key': payuKey,
            'email': booking_request.email,
            'productinfo': 'rooms',
            'hash': hashValue,
            'phone': booking_request.phone,
            'curl': handler_url,
            'surl': handler_url,
            'furl': handler_url,
            'action_url': action_url,
            'offer_key': offerKey,
            'udf1': referenceId}
        return context


class RazorPay(PaymentGateway):
    def build_pg_payload(self, booking_request, is_secure=True):
        """
        :param (apps.checkout.models.BookingRequest) booking_request:
        :param is_secure:
        :return:
        """
        hotel_id = booking_request.hotel_id
        itinerary_page_url = BookingHelper.build_itinerary_page_url_from_params(
            hotel_id, booking_request.room_config, booking_request.room_type, date_to_ymd_str(
                booking_request.checkin_date), date_to_ymd_str(
                booking_request.checkout_date), booking_request.coupon_code, booking_request.id)

        bid = booking_request.id
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotel = hotel_repository.get_hotel_by_id_from_web(booking_request.hotel_id)
        razorpay_key = settings.RAZORPAY_KEYS
        payload = {
            'amount_to_send': int(
                float(
                    booking_request.total_amount) * 100),
            'email': booking_request.email,
            'name': booking_request.name,
            'contact': booking_request.phone,
            'bid': bid,
            'amount_to_store': int(
                float(
                    booking_request.total_amount)),
            'description': hotel.name,
            'razorpay_key': razorpay_key,
            'dismiss_url': itinerary_page_url,
            'order_id': booking_request.order_id}
        context = payload
        return context


pg_factory = {
    "RAZORPAY": RazorPay(),
    "PAYU": PayU(),
}
