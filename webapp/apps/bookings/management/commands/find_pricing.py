from data_services.respositories_factory import RepositoriesFactory

__author__ = 'baljeet'
import datetime
import hmac
import logging
from hashlib import sha1

import psycopg2
import requests
from django.conf import settings
from django.core.management import BaseCommand
from pyquery import PyQuery as pQ

logger = logging.getLogger(__name__)

commandObject = None

NAME = settings.DATABASES['default']['NAME']
USER = settings.DATABASES['default']['USER']
PASS = settings.DATABASES['default']['PASSWORD']
HOST = settings.DATABASES['default']['HOST']
PORT = settings.DATABASES['default']['PORT']

hotel_price_store = {}

hotel_respository = RepositoriesFactory.get_hotel_repository()


def getRoomAvailabilityAndPrice(hotel, hotelDetailsXml):
    """
    Get the availability and price of rooms in the hotel. Enriches the model with isAvailable,
    hotelogixPrice, hotelogixRateId, availableRooms, maxPax
    :param hotel:
    :param hotelDetailsXml:
    :return:
    """
    rooms = hotel_respository.get_rooms_for_hotel(hotel)
    # rooms = hotel.rooms.all().order_by('price')
    hotelXml = pQ(hotelDetailsXml)
    roomTypes = hotelXml('roomtype')
    # TODO: Discuss what to do in this scenario
    # if len(roomTypes) is 0:
    # 	raise ExternalServiceException("No room types found for hotel " + hotel.name + " in hotelogix")

    roomsMap = {

    }
    for hotelogixRoomPq in roomTypes:
        hotelogixRoom = pQ(hotelogixRoomPq)
        for i, room in enumerate(rooms):
            if room.room_type not in roomsMap:
                roomsMap[room.room_type] = {}
            logger.info(
                "Title %s and room type %s",
                hotelogixRoom.attr("title"),
                room.room_type.strip())
            if hotelogixRoom.attr("title").strip() == room.room_type.strip():
                logger.info(
                    "Setting availability for %s, %s, %s",
                    room.room_type.strip(),
                    hotelogixRoom.attr("availableroom"),
                    hotelogixRoom.attr("title"))
                roomsMap[room.room_type]['availability'] = int(
                    hotelogixRoom.attr("availableroom"))
                pre_tax, tax = getMinimumRate(hotelogixRoom)
                roomsMap[room.room_type]['pre_tax'] = pre_tax
                roomsMap[room.room_type]['tax'] = tax
    hotel_price_store.update({
        (hotel.id, hotel.hotelogix_id, hotel.name): roomsMap
    })
    return rooms, roomsMap


def getMinimumRate(hotelogix_room):
    tax = 0
    min_rate = 0
    availability = False
    rate_id = ""
    rates = hotelogix_room('rate')
    for rate_pq in rates:
        rate = pQ(rate_pq)
        logger.info("Title rate %s", rate.attr("title").strip())
        if rate.attr("title").strip() == "Treebo Promotional":
            return rate.attr('price'), rate.attr('tax')
    logger.info("Availability %s", availability)
    return min_rate, tax


def searchWorker(dateRange, hotelsToUpdate):
    checkInDate = datetime.datetime.strptime(dateRange, "%Y-%m-%d").date()
    singleDayDelta = datetime.timedelta(days=1)

    checkOutDate = checkInDate + singleDayDelta
    auth_xml = commandObject.ws_auth_xml()
    ws_auth_signature = commandObject.ws_auth_signature(auth_xml)
    # set what your server acceptsprint headers
    headers = {'Content-Type': 'text/xml',
               'X-HAPI-Signature': ws_auth_signature}

    response = requests.post(
        commandObject.hotelogix_url +
        "wsauth",
        data=auth_xml,
        headers=headers)

    if len(hotelsToUpdate) is 0:
        hotelogix_ids = hotel_respository.get_all_hotelogix_ids()
    else:
        hotelogix_ids = hotel_respository.get_all_hotelogix_ids(
            ids_list=hotelsToUpdate)
        # hotelogix_ids = Hotel.objects.filter(id__in=hotelsToUpdate).values_list('hotelogix_id',
        # flat=True)

    d = pQ(response.content)
    accesskey = d("accesskey").attr("value")
    accesssecret = d("accesssecret").attr("value")
    conn = psycopg2.connect(
        database=NAME,
        host=HOST,
        user=USER,
        password=PASS,
        port=PORT)
    conn.set_isolation_level(0)
    cur = conn.cursor()
    srch_xml = commandObject.search_xml(accesskey, checkInDate, hotelogix_ids)
    search_signature = commandObject.search_signature(accesssecret, srch_xml)
    search_headers = {
        'Content-Type': 'text/xml',
        'X-HAPI-Signature': search_signature}  # set what your server accepts
    st = int(datetime.datetime.now().strftime('%s'))

    logger.info("Searching for check in date " + srch_xml)
    responseXml = requests.post(
        commandObject.hotelogix_url +
        "search",
        data=srch_xml,
        headers=search_headers)
    et = int(datetime.datetime.now().strftime('%s'))
    logger.info("HX %s took %s seconds" % ('search', et - st))

    logger.info("response xml " + responseXml.content)

    responseXml = pQ(responseXml.content)

    hotelogixHotels = responseXml('hotel')
    for singleHotelogixHotel in hotelogixHotels:
        singleHotelogixHotel = pQ(singleHotelogixHotel)
        hotelogixName = singleHotelogixHotel.attr('title')
        if hotelogixName is not None:
            hotelogixName = hotelogixName.strip()
            currentHotelModel = commandObject.hotelNameWiseMapping.get(
                hotelogixName)
            if (currentHotelModel is None) or (
                len(hotelsToUpdate) > 0 and str(
                    currentHotelModel.id) not in hotelsToUpdate):
                continue

            logger.info("Finding the availability for hotel " +
                        str(currentHotelModel.id))
            roomsData, roomsMap = getRoomAvailabilityAndPrice(
                currentHotelModel, singleHotelogixHotel)

    conn.close()


class Command(BaseCommand):
    help = 'Fetch Availability from Hotelogix and save in Database'
    consumer_key = settings.CONSUMER_KEY
    consumer_secret = settings.CONSUMER_SECRET
    hotelogix_url = settings.HOTELOGIX_URL
    bookingdetails_url = settings.BOOKING_DETAILS_URL
    __hotelService = None
    hotelNameWiseMapping = {}
    hotelAvailabilities = []

    def add_arguments(self, parser):
        # Positional arguments

        # Named (optional) arguments
        parser.add_argument(
            '--delta',
            default=1,
            type=int,
            help='Days for which to run the script')
        parser.add_argument(
            '--start',
            default=None,
            help='Start date for sync')

        parser.add_argument(
            '--hotels',
            default='',
            help='comma separated list of hotels to run the script for')

    def handle(self, *args, **options):
        start = options.get('start')
        delta = int(options.get('delta'))
        hotelsString = options.get('hotels')
        hotelsToUpdate = []

        logger.info(
            "Starting fetch for hotels " +
            hotelsString +
            " for # of dates " +
            str(delta))

        if len(hotelsString) > 0:
            hotelsToUpdate = hotelsString.split(',')
        if start is None:
            startDate = datetime.datetime.now()
        else:
            startDate = datetime.datetime.strptime(start, "%d-%m-%Y")

        try:
            self.updateAvailabilityForHotels(startDate, delta, hotelsToUpdate)
            logger.info("Final Pricing %s", hotel_price_store)
            print("Hotel_id, room_type, pre_tax, tax")
            for key, value in list(hotel_price_store.items()):
                for room_type, prices in list(value.items()):
                    print(("{0},{1},{2},{3},{4}".format(key[0], key[2], room_type,
                                                        prices.get('pre_tax', -1),
                                                        prices.get('tax', -1))))

        except Exception as exc:
            logger.exception(exc)

    def updateAvailabilityForHotels(self, start, delta, hotelsToUpdate):
        self.hotelAvailabilities = []

        global commandObject
        commandObject = self

        availabilityPeriod = delta
        singleDayDelta = datetime.timedelta(days=1)
        date_min = start
        date_max = datetime.datetime.combine(
            start +
            datetime.timedelta(
                days=availabilityPeriod),
            datetime.time.max)

        allHotels = hotel_respository.get_active_hotel_list()

        self.hotelNameWiseMapping = {
            hotel.hotelogix_name: hotel for hotel in allHotels}

        dateRanges = []
        while date_min < date_max:
            dateRanges.append(date_min.strftime("%Y-%m-%d"))
            date_min = date_min + singleDayDelta

        for curDate in dateRanges:
            searchWorker(curDate, hotelsToUpdate)

    def ws_auth_xml(self):
        time = datetime.datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + \
            "'><request method='wsauth' key='" + Command.consumer_key + "'></request></hotelogix>"
        return xml

    def ws_auth_signature(self, xml):
        signature = hmac.new(Command.consumer_secret, xml, sha1)
        ws_auth_signature = signature.hexdigest()
        return ws_auth_signature

    def cancel_xml(self, accesskey, order_id, reservation_id):
        time = datetime.datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + "'><request method='cancel' key='" + accesskey + "'><orderId value='" + order_id + \
            "'/><reservationId value='" + reservation_id + "'/><cancelCharge amount='0'/><cancelDescription>This is a test cancel from webservice</cancelDescription></request></hotelogix>"
        return xml

    def create_signature(self, accesssecret, xml):
        signature = hmac.new(
            str(accesssecret),
            str(xml),
            digestmod=sha1).hexdigest()
        return signature

    def search_xml(self, accesskey, in_date, hotelogix_ids):
        checkin_date = in_date.strftime("%Y-%m-%d")
        checkout_date = (
            in_date +
            datetime.timedelta(
                days=1)).strftime("%Y-%m-%d")
        time = datetime.datetime.utcnow().isoformat()
        hotel_ids_xml = "".join(
            ["<hotel id='{0}'/>".format(x) for x in hotelogix_ids])
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + "'><request method='search' key='" + accesskey + "'><stay checkindate='" + checkin_date + "' checkoutdate='" + \
            checkout_date + "'/><pax adult='1' child='0' infant='0'/><roomrequire value='1'/><limit value='100' offset='0' hasResult='0'/><hotels>" + hotel_ids_xml + "</hotels></request></hotelogix>"
        return xml

    def search_signature(self, accesssecret, xml):
        signature = hmac.new(
            str(accesssecret),
            str(xml),
            digestmod=sha1).hexdigest()
        return signature
