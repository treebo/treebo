import datetime
import logging

from apps.referral.service.restclient import ReferralClient
from dbcommon.models.profile import UserReferral, User, UserReferralReward
from services.notification.email.email_service import EmailService
from services.notification.sms.sms_service import SmsService

logger = logging.getLogger(__name__)


class ReferralService(object):
    TRANSACTION_EMAIL_SUBJECT = "Here's your PayTM coupon from Treebo"
    TRANSACTION_SMS = "Hi %(referrer_name)s! %(friend_name)s just had an awesome stay at Treebo. Use code %(" \
                      "coupon_code)s to claim your free Rs 500 PayTM cash. See how to redeem at " \
                      "https://treeboreferral.squarespace.com/."
    TRANSACTION_REFEREE_SMS = "Hi %(referee_name)s, Thank you for choosing to stay with Treebo! Here is your code %(" \
                              "coupon_code)s to claim Rs. 500 PayTM cash as referral reward. See how to redeem at " \
                              "https://treeboreferral.squarespace.com/."
    FRIEND_COUPON_SMS = """Hi %(name)s! Your unique Flat %(amount)s OFF Treebo code is %(coupon_code)s. Redeem now at
    treebo.com"""

    FRIEND_SIGNUP_SMS = "Dear %(referee_name)s, Thank you for signing up on Treebo Hotels - India's top rated Hotel " \
                        "chain. Go ahead and stay at Treebo, we'll send Rs 500 PayTm coupon on your first check out. " \
                        "Book now on https://www.treebo.com"

    REFERRER_SMS = "Dear %(first_name)s, Your friend %(referee_name)s just signed up with Treebo using your link. You " \
                   "will receive Rs. 500 Paytm cash on completion of their 1st stay. Thanks!"

    SHARE_EMAIL_SUBJECT = "%(referrer_name)s's gift to you"

    SIGN_UP_EMAIL_SUBJECT = "%(referrer_name)s says well done"

    @staticmethod
    def generate_referral_code(
            user,
            referrer_code="",
            share_link_identifier=""):
        logger.info(
            'generate_referral_code called for user: %s, with friend referral_code: %s and share_link_identifier: %s',
            user.id,
            referrer_code,
            share_link_identifier)
        try:
            name = user.get_full_name()
            try:
                referral_details = UserReferral.objects.get(user=user)
                logger.debug("User already registered")
            except UserReferral.DoesNotExist:
                response = ReferralClient.generate_referral_code(
                    name, user.email, user.phone_number, str(
                        user.uuid), user.city, referrer_code, share_link_identifier)
                status = response['status']
                logger.debug(
                    "AppVirality response for referral code generation for user %s is %s",
                    user.email,
                    response)
                if status != 200:
                    return None

                data = response['data']
                referral_details, is_new = UserReferral.objects.get_or_create(
                    user=user, userkey=data['userkey'], shareurl=data['shareurl'],
                    referral_code=data['referralcode'], has_referrer=data['hasReferrer'],
                    friend_referral_code=referrer_code, share_link=share_link_identifier
                )
                logger.debug(
                    'UserReferralDetails created with id: %s',
                    referral_details.id)
        except Exception as e:
            logger.exception("Error creating referral code")
            return None
        return referral_details

    @staticmethod
    def update_user(user):
        logger.debug("Updating user details in app virality")
        user_referral = UserReferral.objects.get(user=user)
        response = ReferralClient.update_user(user, user_referral)
        return response

    @staticmethod
    def register_event(user, event_name, extra_info="", transaction_value="0",
                       transaction_unit="Rs"):
        logger.info(
            "register_event called for user: %s and event: %s",
            user.id,
            event_name)
        referral = UserReferral.objects.filter(user=user).first()
        if not referral:
            return None
        response = ReferralClient.register_event(
            referral.userkey,
            event_name,
            extra_info,
            transaction_value,
            transaction_unit)
        status = response['status']
        if status != 200:
            return None

        data = response['data']
        if not data['success']:
            logger.error(
                "Event Registration failed for event: %s and user_id: %s",
                event_name,
                user.email)
        else:
            logger.info("Event: %s registered successfully", event_name)
        return response

    @staticmethod
    def get_user_referral_details(user):
        logger.info('get_user_referral_details called for user: %s', user.id)
        referral_details = UserReferral.objects.filter(user=user).first()
        logger.debug('Found UserReferrer: %s', referral_details)
        return referral_details

    @staticmethod
    def get_referred_friend_rewards(user):
        logger.info(
            'get_friends_referred_by_user called for user: %s',
            user.id)
        referral = UserReferral.objects.filter(
            user=user).values('userkey').first()
        if not referral:
            return None
        user_key = referral['userkey']
        response = ReferralClient.get_friend_rewards(user_key)
        status = response['status']
        if status != 200:
            return None
        data = response['data']
        events = []
        for reward in data['rewards']:
            events.append({
                "email": reward['emailid'],
                "userkey": reward['userkey'],
                "event_name": reward['eventname'],
                "reward_type": reward['reward_type'],
                "reward_date": reward['rewarded_date'],
                "reward_amount": reward['amount'],
                "reward_unit": reward['reward_unit'],
                "status": reward['status'],
                "reward_details": reward['rewarddetails']
            })
        return events

    @staticmethod
    def get_referrer_rewards_for_user(user):
        logger.info(
            'get_referrer_rewards_for_user called for user: %s',
            user.id)
        referrer = UserReferral.objects.get(user=user)
        total_signup = UserReferral.objects.filter(
            friend_referral_code=referrer.referral_code.split("-")[0]).count()
        total_reward_earned = UserReferralReward.objects.filter(
            referrer=user, type=UserReferralReward.ReferralEvents.Transaction).count()
        sign_ups = UserReferralReward.objects.select_related(
            'referrer', 'referee').filter(
            referrer=user, type=UserReferralReward.ReferralEvents.Signup)
        transactions = UserReferralReward.objects.select_related(
            'referrer', 'referee').filter(
            referrer=user, type=UserReferralReward.ReferralEvents.Transaction)
        reward_context = []
        for transaction in transactions:
            content = {
                "email": transaction.referee.get_full_name(),
                "event_name": "Successful",
                "reward_amount": transaction.reward_amount,
            }
            reward_context.append(content)

        rewards = {
            'total_signup': total_signup,
            'total_reward_earned': total_reward_earned,
            "all_referral_rewards": reward_context
        }
        return rewards

    @staticmethod
    def get_campaign_details(campain_id):
        response = ReferralClient.get_campaign_details(campain_id)
        if response['status'] == 200:
            return response['data']
        else:
            logger.error(
                "Campaign id %s details could not be fetched",
                campain_id)
            return None

    @staticmethod
    def get_referrer_detail(share_link_identifier):
        logger.info(
            'get_referrer_detail called for click_id: %s',
            share_link_identifier)
        response = ReferralClient.get_referrer_details(share_link_identifier)
        status = response['status']
        if status != 200:
            return None
        data = response['data']
        return data

    @staticmethod
    def send_coupon_on_sign_up(
            friend_id,
            coupon_code,
            coupon_amount,
            referrer_name,
            referrer_id):
        try:
            friend_user = User.objects.get(uuid=friend_id)
            referrer = User.objects.get(uuid=referrer_id)
            SmsService.send_sms([friend_user.phone_number],
                                ReferralService.FRIEND_COUPON_SMS % {
                                    "name": friend_user.first_name,
                                    "coupon_code": coupon_code,
                                    "amount": coupon_amount
            })

            SmsService.send_sms([referrer.phone_number], ReferralService.REFERRER_SMS % {
                "name": referrer.first_name,
                "friend_name": friend_user.first_name
            })
        except Exception:
            logger.exception("Unable to send sms to %s on sign up", friend_id)

    @staticmethod
    def send_sms_on_sign_up(friend_id, referrer_id):
        try:
            friend_user = User.objects.get(uuid=friend_id)
            referrer = User.objects.get(uuid=referrer_id)
            SmsService.send_sms([friend_user.phone_number],
                                ReferralService.FRIEND_SIGNUP_SMS % {
                                    "referee_name": friend_user.first_name,
            })

            SmsService.send_sms([referrer.phone_number], ReferralService.REFERRER_SMS % {
                "first_name": referrer.first_name,
                "referee_name": friend_user.first_name
            })
        except Exception:
            logger.exception("Unable to send sms to %s on sign up", friend_id)

    @staticmethod
    def share_referral_email(emails, user, campaign_id):
        """

        :param [str] emails:
        :param dbcommon.models.profile.User user:
        """
        subject = ReferralService.SHARE_EMAIL_SUBJECT % {
            "referrer_name": user.first_name
        }
        response = ReferralService.get_campaign_details(campaign_id)
        campaigns = response["AppCampaignsDetails"]
        if not campaigns:
            campaigns = []
        reward = "a discount"
        for campaign in campaigns:
            if campaign_id == campaign["CampaignID"]:
                for rule in campaign["RewardRules"]:
                    if str(rule["RewardUserType"]) == "Friend" and str(
                            rule["EventName"]) == UserReferralReward.ReferralEvents.Signup:
                        reward = rule["Reward"] + " " + rule["RewardUnit"]

        template_path = "email/referral/referee_invite.html"
        user_referral = UserReferral.objects.get(user=user)
        EmailService.send_email_from_template(emails, subject, template_path, {
            "referrer_name": user.first_name,
            "reward": reward,
            "invite_link": user_referral.shareurl
        })

    @staticmethod
    def send_email_on_sign_up(friend_id, coupon_code, amount, referrer_id):
        """

        :param friend_id:
        :param str coupon_code:
        :param str amount:
        :param referrer_id:
        """
        try:
            friend = User.objects.get(uuid=friend_id)
            referrer = User.objects.get(uuid=referrer_id)
            friend_referral = UserReferral.objects.get(user=friend)
            subject = ReferralService.SIGN_UP_EMAIL_SUBJECT % {
                "referrer_name": referrer.first_name
            }
            EmailService.send_email_from_template([friend.email],
                                                  subject,
                                                  "email/referral/referee_signup.html",
                                                  {"name": friend.first_name,
                                                   "coupon_code": coupon_code,
                                                   "amount": amount,
                                                   "referral_code": friend_referral.referral_code})
        except Exception as e:
            logger.exception(
                "Unable to send email to %s on sign up",
                friend_id)

    @staticmethod
    def send_referrer_email_on_transaction(
            friend_uuid, coupon_code, amount, referrer_id):
        try:
            referrer = User.objects.get(uuid=referrer_id)
            subject = ReferralService.TRANSACTION_EMAIL_SUBJECT % {
            }
            friend = User.objects.get(uuid=friend_uuid)
            EmailService.send_email_from_template([referrer.email],
                                                  subject,
                                                  "email/referral/referrer_reward.html",
                                                  {"referrer_name": referrer.first_name,
                                                   "coupon_code": coupon_code,
                                                   "amount": amount,
                                                   "friend_name": friend.first_name})
        except Exception:
            logger.exception(
                "Unable to send email to %s on transaction",
                referrer_id)

    @staticmethod
    def send_referrer_sms_on_transaction(
            friend_uuid, coupon_code, amount, referrer_id):
        try:
            referrer = User.objects.get(uuid=referrer_id)
            friend = User.objects.get(uuid=friend_uuid)
            SmsService.send_sms([referrer.phone_number], ReferralService.TRANSACTION_SMS % {
                "referrer_name": referrer.first_name,
                "amount": amount,
                "coupon_code": coupon_code,
                "friend_name": friend.first_name
            })
        except Exception:
            logger.exception(
                "Unable to send sms to %s on transaction",
                referrer_id)

    @staticmethod
    def send_referee_email_on_transaction(friend_uuid, coupon_code, amount):
        try:
            referee = User.objects.get(uuid=friend_uuid)
            subject = ReferralService.TRANSACTION_EMAIL_SUBJECT % {
            }
            EmailService.send_email_from_template([referee.email],
                                                  subject,
                                                  "email/referral/referee_reward.html",
                                                  {"referee_name": referee.first_name,
                                                   "coupon_code": coupon_code,
                                                   "amount": amount})
        except Exception:
            logger.exception(
                "Unable to send email to %s on transaction",
                friend_uuid)

    @staticmethod
    def send_referee_sms_on_transaction(friend_uuid, coupon_code, amount):
        try:
            referee = User.objects.get(uuid=friend_uuid)
            SmsService.send_sms([referee.phone_number], ReferralService.TRANSACTION_REFEREE_SMS % {
                "referee_name": referee.first_name,
                "amount": amount,
                "coupon_code": coupon_code
            })
        except Exception:
            logger.exception(
                "Unable to send sms to %s on transaction",
                friend_uuid)

    @staticmethod
    def save_referral_reward(
            user_uuid,
            amount,
            coupon_code,
            reward_type,
            friend_uuid,
            reward_id,
            reward_date=datetime.datetime.now):
        """

        :param user_uuid: uuid of user
        :param amount: reward amount
        :param coupon_code: coupon code awarded
        :param reward_date: the date the reward was awarded
        :param reward_id:
        :param friend_uuid:
        """
        try:

            try:
                user_referral_reward = ReferralService.get_referral_reward_details(
                    reward_id)
            except UserReferralReward.DoesNotExist:
                user_referral_reward = UserReferralReward()
            user_referral_reward.amount = amount
            user_referral_reward.coupon_code = coupon_code
            user_referral_reward.type = reward_type
            user_referral_reward.reward_date = reward_date
            user_referral_reward.referrer = User.objects.get(uuid=user_uuid)
            user_referral_reward.referee = User.objects.get(uuid=friend_uuid)
            user_referral_reward.reward_id = reward_id
            user_referral_reward.save()
        except Exception:
            logger.exception(
                "Unable to save referral reward information of user %s on %s with friend %s",
                user_uuid,
                reward_type,
                friend_uuid)

    @staticmethod
    def approve_reward(reward_id):
        try:
            ReferralClient.approve_reward(reward_id)
        except Exception:
            logger.exception("Unable to approve reward %s ", reward_id)

    @staticmethod
    def distribute_reward(reward_id):
        try:
            return ReferralClient.distribute_reward(reward_id)
        except Exception:
            logger.exception("Unable to distribute reward %s", reward_id)

    @staticmethod
    def get_referral_reward_details(reward_id):
        return UserReferralReward.objects.select_related(
            'referrer', 'referee').get(reward_id=reward_id)

    @staticmethod
    def get_referral_transaction_rewards_for_user(uuid):
        return UserReferralReward.objects.select_related('referrer', 'referee').get(
            referee__uuid=uuid, type=UserReferralReward.ReferralEvents.Transaction)
