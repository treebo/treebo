from apps.bookings.models import PaymentOrder, Booking


class FinalBookingStatus(object):
    SUCCESS = 'success'
    PENDING = 'pending'
    FAILURE = 'failure'


def get_payment_status(payment_status):
    if payment_status == PaymentOrder.CAPTURED:
        return FinalBookingStatus.SUCCESS
    elif payment_status == PaymentOrder.ISSUED:
        return FinalBookingStatus.PENDING
    elif payment_status == PaymentOrder.FAILED:
        return FinalBookingStatus.FAILURE
    return FinalBookingStatus.PENDING
