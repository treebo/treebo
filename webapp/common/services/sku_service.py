from web_sku.availability.services.availability_service import AvailabilityService
from web_sku.catalog.services.sku_request_service import SkuRequestService
from web_sku.pricing.services.pricing_service import PricingService
from web_sku.pricing.transformers.pricing_transformer_factory import PricingTypeEnum
from django.conf import settings


class SKUService:
    availability_service = AvailabilityService()
    sku_request_service = SkuRequestService()
    pricing_service = PricingService()

    @staticmethod
    def get_sku_for_hotels(hotel_cs_id_list, room_config, room_types):
        skus = SKUService.sku_request_service.get_web_request_skus(room_config, hotel_cs_id_list, room_types)
        return skus

    @staticmethod
    def get_sku_availability_for_hotels(skus, hotel_cs_id_list, room_config, checkin_date,
                                        checkout_date, channel, sub_channel):
        availability_request = {
            'skus': skus,
            'checkin': checkin_date,
            'checkout': checkout_date,
            'hotels': hotel_cs_id_list,
            'occupancy_config': room_config,
            'channel': channel,
            'subchannel': sub_channel
        }

        sku_availability = SKUService.availability_service.get_sku_availability(availability_request)
        return sku_availability

    @staticmethod
    def get_room_wise_availability_from_sku_availability(skus, sku_availability):
        availability_request = {
            'skus': skus
        }

        room_wise_availability = SKUService.availability_service. \
            get_room_wise_availability_from_sku_availability(availability_request, sku_availability)
        return room_wise_availability

    @staticmethod
    def get_cheapest_price_from_sku_availability(skus, sku_availability, checkin_date,
                                                 checkout_date, channel='direct', sub_channel='website-direct',
                                                 source='direct',
                                                 application='website'):
        if settings.NRP_ENABLED:
            pricing_policy = ['nrp', 'rp']
        else:
            pricing_policy = ['rp']

        pricing_request_data = {
            'skus': skus,
            'from_date': checkin_date,
            'to_date': checkout_date,
            'mode': 'BROWSE',
            'policy': pricing_policy,
            'apply_coupon': True,
            'coupon_code': '',
            'channel': channel,
            'subchannel': sub_channel,
            'application': application,
            'utm': {'source': source},
            'customer_info': {},
            'pricing_type': PricingTypeEnum.DETAILED.value

        }
        room_config_wise_price = SKUService.pricing_service.get_room_wise_price_from_availability(
            pricing_request_data, sku_availability)
        return room_config_wise_price