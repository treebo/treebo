import logging
import ast

from apps.content.models import ContentStore
from apps.growth.services.growth_services import GrowthServices
from apps.hotels.service.hotel_service import HotelService
from common.exceptions.treebo_exception import TreeboValidationException
from django.utils import dateformat

from apps.checkout.service.notification.basenotification import NotificationService
from common.constants import common_constants
from data_services.respositories_factory import RepositoriesFactory
from services.notification.sms.sms_service import SmsService
from django.template import Context, Template
from apps.bookings.models import PaymentOrder
from apps.bookings.constants import WALLET_GATEWAY_TYPE, WALLET_SUCCESS_STATUS
from apps.bookings.constants import HOMESTAY_PROVIDER_WT

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)


class BookingSmsService(NotificationService):
    template_name = '''Dear %(first_name)s, Your booking for %(rooms)s %(room_text)s at %(hotel_name)s is confirmed. Your Booking ID is: %(booking_ids)s Check-in: %(checkin_date)s Check-out: %(checkout_date)s Address: %(address)s Here is the google maps link for directions %(google_link)s Please feel free to call us at +919322800100 for any assistance. Have a pleasant stay!'''

    def __init__(self, notification_type, content_subtype, to_address):
        super(
            BookingSmsService,
            self).__init__(
            notification_type,
            content_subtype,
            to_address,
            None,
            None)

    def _setData(self, data):
        NotificationService._setData(self, data)

    def createMessage(self):
        return NotificationService._createMessage(self)

    def sendMessage(self):
        NotificationService._sendMessage(self)

    def collateSmsData(self, booking_request):
        """

        :param apps.checkout.models.BookingRequest booking_request:
        """
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        booking_ids = booking_request.order_id
        first_name = booking_request.name.split(" ")
        if len(first_name) > 0:
            first_name = first_name[0]
        else:
            first_name = "Guest"
        rooms = len(booking_request.room_config.split(","))
        checkin_date = dateformat.format(
            booking_request.checkin_date, "jS M'y")
        checkout_date = dateformat.format(
            booking_request.checkout_date, "jS M'y")
        hotel = hotel_repository.get_single_hotel(
            id=booking_request.hotel_id)
        #hotel = Hotel.objects.get(pk=booking_request.hotel_id)
        address = hotel_repository.get_address(hotel)
        google_link = common_constants.GOOGLE_MAPS_LINK + \
            str(hotel.latitude) + "," + str(hotel.longitude)
        roomText = "room" if int(rooms) == 1 else "rooms"
        data = {
            "first_name": first_name,
            "rooms": rooms,
            "room_text": roomText,
            "hotel_name": hotel.name,
            "booking_ids": booking_ids,
            "checkin_date": checkin_date,
            "checkout_date": checkout_date,
            "address": address,
            "google_link": google_link}
        self._setData(data)


class BookingPartPaySmsService():
    @staticmethod
    def get_sms_payload(booking_request):

        mobile = str(booking_request.phone)
        hotel_repository = RepositoriesFactory.get_hotel_repository()

        if mobile and mobile.isdigit():
            booking_ids = booking_request.order_id
            first_name = booking_request.name.split(" ")
            if len(first_name) > 0:
                first_name = first_name[0]
            else:
                first_name = "Guest"
            treebo_points_used = 0

            if booking_request.wallet_applied:
                payment_orders = PaymentOrder.objects.filter(booking_order_id=booking_request.order_id)

                for payment_order in payment_orders:
                    if payment_order.gateway_type == WALLET_GATEWAY_TYPE and payment_order.status == WALLET_SUCCESS_STATUS:
                        treebo_points_used = booking_request.wallet_deduction

            rooms = len(booking_request.room_config.split(","))
            checkin_date = dateformat.format(
                booking_request.checkin_date, "jS M y")
            checkout_date = dateformat.format(
                booking_request.checkout_date, "jS M y")

            hotel = hotel_repository.get_single_hotel(
                id=booking_request.hotel_id)
            #hotel = Hotel.objects.get(pk=booking_request.hotel_id)
            address = hotel_repository.get_address(hotel)
            city = hotel.city.name
            google_link = common_constants.GOOGLE_MAPS_LINK + \
                str(hotel.latitude) + "," + str(hotel.longitude)
            roomText = "room" if int(rooms) == 1 else "rooms"
            payment_link, part_pay_amount = None, None
            total_booking_amount = round(booking_request.total_amount)
            content_object = ContentStore.objects.filter(
                name="part_pay_enable_channel", version=1).first()
            if content_object:
                content_object_value = content_object.value
                enabled_channel_list = ast.literal_eval(content_object_value)
            else:
                enabled_channel_list = []

            growth_service = GrowthServices()
            if not (hotel.provider_name == HOMESTAY_PROVIDER_WT):
                if "Not Paid" in booking_request.payment_mode and str(
                        booking_request.booking_channel).lower() in enabled_channel_list:
                    partpay_bookings_list = []
                    partpay_booking_dict = {}
                    partpay_booking_dict['order_id'] = booking_request.order_id
                    partpay_booking_dict['status'] = 'reserved' if booking_request.status.lower(
                    ) == 'completed' else ''
                    partpay_booking_dict['audit'] = booking_request.is_audit
                    partpay_booking_dict['paid_at_hotel'] = booking_request.pay_at_hotel
                    partpay_booking_dict['payment_mode'] = booking_request.payment_mode
                    partpay_booking_dict['hotel_detail'] = {
                        "hotel_code": hotel.hotelogix_id,
                        "hotel_cs_id": hotel.cs_id
                    }
                    partpay_bookings_list.append(partpay_booking_dict)

                    payment_link, part_pay_amount = growth_service.booking_partpay_details(
                        partpay_bookings_list)

            bookings_data_list = []
            bookings_data = dict()
            bookings_data['order_id'] = booking_request.order_id
            bookings_data['hotel_detail'] = {
                "hotel_code": hotel.hotelogix_id,
                "hotel_cs_id": hotel.cs_id
            }
            bookings_data_list.append(bookings_data)
            cancel_hash_url = growth_service.booking_cancel_hash_url(
                bookings_data_list)
            booking_exp_url = growth_service.booking_exp_hash_url(
                booking_request.order_id, hotel.hotelogix_id)
            sms_payload = {
                "first_name": first_name,
                "guest_mobile": mobile,
                "rooms": rooms,
                "room_text": roomText,
                "hotel_name": HotelService().get_post_booking_display_name_from_hotel_name(
                    hotel_name=hotel.name),
                "booking_id": booking_ids,
                "checkin_date": checkin_date,
                "checkout_date": checkout_date,
                "address": address,
                "google_link": google_link,
                "payment_link": payment_link,
                "hotel_city": city,
                "hotel_landmark": hotel.landmark if hotel.landmark else hotel.street,
                "hotel_number": hotel.phone_number if hotel.phone_number else "+91 9322800100",
                "paid_amount": booking_request.payment_amount,
                "balance_amount": round(total_booking_amount - booking_request.payment_amount),
                "part_pay_amount": part_pay_amount,
                'total_amount': round(total_booking_amount),
                'cancel_hash_url': cancel_hash_url,
                'booking_exp_url': booking_exp_url,
                'treebo_points_used': treebo_points_used}
            logger.info("data dict for sending web sms %s", sms_payload)
        else:
            raise TreeboValidationException("Mobile number is not a number")

        return sms_payload
