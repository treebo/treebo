from unittest.case import TestCase

from common.exceptions.treebo_exception import TreeboValidationException
from services.common_services.otp_service import OtpService


class OtpServiceTest(TestCase):
    otp_service = OtpService()

    def test_otp_generation_with_number(self):
        number = OtpService.send_otp("9702620007")
        # TODO: Fix. send_otp method doesn't return anything
        # self.assertIsNotNone(number)

    def test_should_fail_with_non_number(self):
        self.assertRaises(
            TreeboValidationException,
            OtpService.send_otp,
            "asdadasdasda")
