from apps.seo.services.weather_widget.weather_model_service import WeatherModelService

class CityCoordinate():

    def city_coordinate(self, area):
        page = area['page']
        identifier = area['identifier']
        area_lat = None
        area_lng = None
        weather_model_service = WeatherModelService()
        if page == 'city' or page == 'category' or page == 'city-amenity':
            city_obj = weather_model_service.fetch_city_from_city_id(identifier.id)
            if city_obj is not None:
                area_lat = city_obj.city_latitude
                area_lng = city_obj.city_longitude
        elif page == 'locality' or page == 'locality-category':
            locality_obj = weather_model_service.fetch_locality_from_locality_id(identifier.id)
            if locality_obj is not None:
                area_lat = locality_obj.latitude
                area_lng = locality_obj.longitude
        elif page == 'landmark' or page == 'landmark-category':
            landmark_obj = weather_model_service.fetch_landmark_from_landmark_id(identifier.id)
            if landmark_obj is not None:
                area_lat = landmark_obj.latitude
                area_lng = landmark_obj.longitude
        if area_lat is None or area_lng is None:
            return {}
        city_coord = {}
        city_coord['lat'] = area_lat
        city_coord['lng'] = area_lng
        return city_coord
