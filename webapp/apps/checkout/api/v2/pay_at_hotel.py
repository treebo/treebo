import json
import logging

from ipware.ip import get_ip
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.bookings.api.v1.bookingstub import BookingsByOrderId
from apps.bookingstash.service.availability_service import get_availability_service
from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from apps.checkout.service import checkout_service
from apps.checkout.service.analytics.order_analytics import confirm_booking_analytics
from apps.checkout.service.checkout_service import save_booking_state, sanitise_data
from apps.checkout.tasks import send_booking_confirmation_notification
from apps.common.slack_alert import SlackAlertService
from apps.discounts.exceptions import InvalidCouponException
from apps.discounts.models import DiscountCoupon
from apps.growth.services.growth_services import GrowthServices
from common.services.feature_toggle_api import FeatureToggleAPI
from base.logging_decorator import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboValidationException
from services.restclient.bookingrestclient import BookingClient

logger = logging.getLogger(__name__)


class PayAtHotel(TreeboAPIView):
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)

    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    def __freeze_booking_request(self, request, instant_booking):
        json_data = request.data
        logger.info(
            "__freeze_booking_request for pay@hotel %s ",
            json.dumps(json_data))
        bid = json_data.get('bid')
        booking_payload = {
            "bid": bid,
            # Hardcoding value 1, TODO: make a map for paid and unpaid nad set
            # the value for it
            "pay_at_hotel": '1',
            "name": json_data.get('name', ''),
            "email": json_data.get('email', ''),
            "phone": json_data.get('mobile', ''),
            "organization_name": json_data.get('organization_name', ''),
            "organization_address": json_data.get('organization_address', ''),
            "organization_taxcode": json_data.get('organization_taxcode', ''),
            "otp_verified_number": json_data.get('otp_verified_number', ''),
            "user_signup_channel": request.COOKIES.get("user_signup_channel"),
            "message": json_data.get('message', ""),
            "check_in_time": json_data.get('check_in_time'),
            "check_out_time": json_data.get('check_out_time'),
            "user": request.user,
            "instant_booking": instant_booking,
            "http_host": request.META['HTTP_HOST'],
            "payment_mode": "Not Paid",
            "utm_source": str(request.COOKIES.get('utm_source')) if request.COOKIES.get('utm_source') else "",
            "utm_medium": str(request.COOKIES.get('utm_medium')) if request.COOKIES.get('utm_medium') else "",
            "referral_click_id": str(request.COOKIES.get('avclk')) if request.COOKIES.get('avclk') else "",
            "utm_campaign": str(request.COOKIES.get('utm_campaign')) if request.COOKIES.get('utm_campaign') else "",
            "host_ip_address": str(get_ip(request)) if get_ip(request) else ""
        }

        logger.info(
            "Booking Payload for freezing booking request: %s",
            booking_payload)

        booking_request = save_booking_state(booking_payload)
        return booking_request

    def __create_booking(self, booking_request):
        """

        :param apps.checkout.models.BookingRequest:
        :return: booking request object
        """
        booking_request.comments = checkout_service.build_special_preference(
            booking_request)
        booking_request.save()
        booking = BookingClient.create_booking(booking_request)
        booking_request.status = BookingRequest.COMPLETED
        booking_request.booking_id = booking["id"]
        booking_request.order_id = booking["order_id"]
        booking_request.save()
        if booking_request.coupon_code and booking_request.coupon_apply:
            try:
                coupon = DiscountCoupon.objects.get(
                    code__iexact=booking_request.coupon_code)
                coupon.mark_used_for_transaction()
            except DiscountCoupon.DoesNotExist:
                logger.exception(
                    "Unable to find coupon code %s",
                    booking_request.coupon_code)
            logger.info("Coupon code max available usage is updated")
        return booking_request

    def __booking_handler(self, request, booking_request):
        '''

        :param booking_request:
        :return: Booking order id
        '''
        booking = self.__create_booking(booking_request)
        order_id = booking.order_id
        booking_instance = BookingsByOrderId()
        response = booking_instance.get(request, orderId=order_id)

        if response.status_code == 404:
            raise TreeboValidationException(error_codes.INVALID_ORDER_ID)

        booking_dict = response.data
        try:
            growth_service = GrowthServices()
            partial_payment = growth_service.generate_partial_payment_link(
                booking_dict)
        except Exception as e:
            logger.exception(
                "Exeception occurred while generating part pay link for %s",
                order_id)

        send_booking_confirmation_notification.delay(booking_request.order_id)

        return booking.order_id

    def __check_availability(self, bid):
        booking_request = BookingRequest.objects.get(id=bid)
        available_rooms = get_availability_service().get_available_rooms(
            booking_request.hotel_id,
            booking_request.checkin_date,
            booking_request.checkout_date,
            booking_request.room_config)
        for room, availability in list(available_rooms.items()):
            if booking_request.room_type.lower(
            ) == room.room_type_code.lower() and availability > 0:
                return True
        return False

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        json_data = request.data
        json_data = sanitise_data(json_data)
        bid = json_data.get('bid')
        try:
            is_available = self.__check_availability(bid)

        except BookingRequest.DoesNotExist:
            context = {'available': False, 'error': {
                'msg': 'Booking Request Does not exist',
                'code': "5005"
            }}
            SlackAlertService.web_alert(
                "Booking Request Does not exist : " + str(bid))
            return Response(context, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            context = {'available': False, 'error': {
                'msg': str(e),
                'code': "5001"
            }}
            SlackAlertService.web_alert(
                "Availability error for bid : " + str(bid))
            return Response(context, status=HTTP_500_INTERNAL_SERVER_ERROR)

        if not is_available:
            # FIXME: Error codes standardizations
            # FIXME: Metrics loggers here
            context = {'available': False, 'error': {
                'msg': 'Room is sold out',
                'code': "5001"
            }}
            SlackAlertService.web_alert(
                "Room is sold out for bid : " + str(bid))
            return Response(context, status=HTTP_400_BAD_REQUEST)
        organization_taxcode = request.data.get('organization_taxcode', None)
        if not BookingHelper.validate_gstin_for_booking(
                organization_taxcode, bid=bid, api_call='pay@hotel v2'):
            return Response({"error": {
                "msg": error_codes.get(error_codes.INVALID_GSTIN_NUMBER)['msg'],
                "code": error_codes.get(error_codes.INVALID_GSTIN_NUMBER)['code']
            }
            }, status=HTTP_400_BAD_REQUEST)

        try:
            if FeatureToggleAPI.is_enabled("booking", "instant_booking", True):
                booking_request = self.__freeze_booking_request(request, True)
            else:
                booking_request = self.__freeze_booking_request(request, False)

        except InvalidCouponException as exc:
            logger.exception(exc)
            context = {'coupon_applicable': False, 'error': {
                'msg': str(exc),
                'code': "5004"
            }}
            SlackAlertService.web_alert(exc.message + " for bid : " + str(bid))
            return Response(context, status=HTTP_400_BAD_REQUEST)

        try:
            order_id = self.__booking_handler(request, booking_request)
            order_event_args = confirm_booking_analytics(
                request, booking_request)
            return Response({"order_id": order_id,
                             'order_event_args': order_event_args})
        except Exception as e:
            logger.exception("Unable to generate a booking")
            return Response({"error": {
                "msg": 'Unable to generate a booking',
                "code": "5002"
            }
            }, status=HTTP_500_INTERNAL_SERVER_ERROR)
