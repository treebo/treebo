import json
import logging

import requests
from django.conf import settings
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_200_OK

from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class RetryFailedBookings(TreeboAPIView):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(RetryFailedBookings, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        """
        Retry failed bookings
        :param request:
        :return:

        Request data should be an array of AsyncJobStatus job_id. e.g.,

        {
            "job_ids": [
                "1530192405385409-2",
                "153190301493682-8"
                ]
        }

        """
        request_data = request.data
        async_job_ids = request_data.get('job_ids')

        job_ids = ','.join(async_job_id for async_job_id in async_job_ids)

        return self.retry(job_ids)

    @staticmethod
    def retry(async_job_ids):
        """
        :param async_job_ids: comma separated list of AsyncJobStatus job_id e.g, '1530192405385409-2,153190301493682-8'
        :return:
        {
            "results": [
                "SUCCESS : 757571 , ",
                "SUCCESS : 757811 , "
            ]
        }
        """
        url = settings.TREEBO_WEB_URL + '/api/v1/booking/async-tasks/'

        payload = "job_ids={0}".format(async_job_ids)

        headers = {
            'content-type': 'application/x-www-form-urlencoded',
            'x-hapi-signature': 'TGUJHHHS787GLLL6ASIHYT9HT',
            'cache-control': 'no-cache',
            'postman-token': '013052c6-df5b-f480-7f2f-7ffe5e4c6b25'
        }

        response = requests.request("POST", url, data=payload, headers=headers)

        if response.status_code == HTTP_200_OK:
            response_text = json.loads(response.text)
            return JsonResponse({"results": response_text['results']}, status=response.status_code)
        else:
            return response
