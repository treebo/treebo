import logging

from django.contrib.auth import logout, authenticate
from django.http.response import HttpResponseRedirect

from apps.common.utils import AnalyticsEventTracker
from base.views.template import TreeboTemplateView
from apps.common import utils
import sys

__author__ = 'varunachar'
logger = logging.getLogger(__name__)


class Logout(TreeboTemplateView):
    template_name = ''

    def getData(self, request, args, kwargs):
        referer = request.META.get('HTTP_REFERER')
        if request.user is not None:
            user = request.user
            utils.trackAnalyticsServerEvent(request,
                                            'Logout Successful',
                                            {"medium": 'EMAIL',
                                             'email': user.email,
                                             'userid': user.id,
                                             'name': user.first_name},
                                            channel=AnalyticsEventTracker.WEBSITE)
            logout(request)

        if referer is None:
            response = HttpResponseRedirect('/')
            response.set_cookie('authenticated_user', 'false')
            return response
        response = HttpResponseRedirect(referer)
        response.set_cookie('authenticated_user', 'false')
        return response

    def getMobileData(self, request, args, kwargs):

        if request.user is not None:
            user = request.user
            utils.trackAnalyticsServerEvent(request,
                                            'Logout Successful',
                                            {"medium": 'EMAIL',
                                             'email': user.email,
                                             'userid': user.id,
                                             'name': user.first_name},
                                            channel=AnalyticsEventTracker.WEBSITE)

            logout(request)
        currentUrl = request.GET.get('url')
        if currentUrl is None:
            return HttpResponseRedirect('/')
        return HttpResponseRedirect(currentUrl)
