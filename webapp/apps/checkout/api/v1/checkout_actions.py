import logging

import requests
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from rest_framework.decorators import api_view
from rest_framework.response import Response

from apps.bookingstash.service.availability_service import get_availability_service
from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from apps.checkout.service.checkout_service import create_booking_request, \
    save_booking_state, build_pricing_for_booking
from apps.pages.data_model import QuickbookData
from base import log_args
from common.constants import common_constants
from services.restclient.request_wrapper import wrap_internal_api_call

logger = logging.getLogger(__name__)


@api_view(["GET", "POST"])
@log_args(logger)
def initiate_booking(request):
    # TODO: Convert to post in both
    if request.method == "GET":
        data = request.GET
    else:
        data = request.POST
    quickbook_data = QuickbookData(**data.dict())
    if len(quickbook_data.room_config) == 1:
        quickbook_data.room_config += "-0"
    booking_request = create_booking_request(request, quickbook_data)
    # Handle None booking_request
    pay_at_hotel = data.get("pay_at_hotel")
    if pay_at_hotel != "2":
        logger.info("Booking Request ID: %s", booking_request.id)
        itinerary_page_url = BookingHelper.build_itinerary_page_url(
            quickbook_data, booking_request)
        return redirect(itinerary_page_url)
    else:
        freeze_booking_request_data = {
            "bid": booking_request.id,
            "pay_at_hotel": data.get("pay_at_hotel"),
            "is_audit": True,
            "name": data.get("name"),
            "email": data.get("email"),
            "phone": data.get("mobile"),
            "user": request.user,
            "user_signup_channel": data.get("user_signup_channel"),
            "booking_channel": data.get("channel"),
            "message": data.get("message"),
            "check_in_time": data.get("check_in_time"),
            "check_out_time": data.get("check_out_time"),
            "final_hidden_discount_value": data.get("final_hidden_discount_value"),
            "otp_verified_number": None,
            'instant_booking': True,
            "http_host": request.META['HTTP_HOST'],
            "utm_source": str(
                request.COOKIES.get('utm_source')) if request.COOKIES.get('utm_source') else "",
            "utm_medium": str(
                request.COOKIES.get('utm_medium')) if request.COOKIES.get('utm_medium') else "",
            "referral_click_id": str(
                request.COOKIES.get('avclk')) if request.COOKIES.get('avclk') else "",
            "utm_campaign": str(
                    request.COOKIES.get('utm_campaign')) if request.COOKIES.get('utm_campaign') else ""}
        build_pricing_for_booking(request, booking_request)
        booking_request = save_booking_state(freeze_booking_request_data)
        return redirect(reverse("pages:transaction") +
                        "/?referenceid={0}".format(booking_request.id))


@api_view(["GET"])
@log_args(logger)
def apply_discount(request):
    coupon_code = request.GET.get("couponcode")
    booking_request_id = request.GET.get("bid")
    booking_request = BookingRequest.objects.get(pk=booking_request_id)

    try:
        payload = {"bid": booking_request_id, "couponcode": coupon_code}
        url = request.build_absolute_uri(reverse('checkout_v2:apply-coupon'))
        response = wrap_internal_api_call(
            request, requests.get, url, params=payload)
        if response.status_code == 200 or response.status_code == 304:
            prices = response.json()['data']
            context = {
                "couponDesc": prices["coupon_desc"],
                "status": "success",
                "available": True,
                "sellRate": prices['actual_total_cost'],
                "price": prices['pretax_price'],
                "tax": prices['total_tax'],
                "discount": prices['discount_amount'],
                "couponcode": coupon_code,
                "nights": prices['nights_breakup'],
                "final_price": booking_request.total_amount,
                "voucherAmount": prices['discount_amount'],
                "final_price_rounded_two_decimal": round(
                    prices['actual_total_cost'],
                    2),
                "autopromo": False,
            }
            logger.debug('Discount coupon context: %s', context)
            return Response(context)
        else:
            return Response({'message': common_constants.INVALID_DATA,
                             "discountError": common_constants.COUPON_NOT_FOUND}, status=200)
    except BaseException:
        logger.exception("Exception in Reapply Discount")
        return Response({'message': common_constants.INVALID_DATA,
                         "discountError": common_constants.COUPON_NOT_FOUND
                         }, status=200)


@api_view(["GET"])
@log_args(logger)
def check_availability(request):
    """
    Check availability before confirmation
    bid -- Booking Id
    :param request:
    :return:
    """
    logger.info("Logging done")
    booking_request_id = request.GET.get("bid", None)
    if not booking_request_id:
        return Response({"available": False}, status=404)

    booking_request = BookingRequest.objects.filter(
        pk=booking_request_id).first()
    if not booking_request:
        return Response({"available": False}, status=404)
    available_rooms = get_availability_service().get_available_rooms(
        booking_request.hotel_id,
        booking_request.checkin_date,
        booking_request.checkout_date,
        booking_request.room_config)
    for room, availability in list(available_rooms.items()):
        if booking_request.room_type.lower(
        ) == room.room_type_code.lower() and availability > 0:
            return Response({"available": True}, status=200)
    return Response({"available": False}, status=200)
