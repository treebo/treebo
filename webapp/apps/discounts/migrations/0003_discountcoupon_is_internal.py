# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('discounts', '0002_auto_20160215_0935'),
    ]

    operations = [
        migrations.AddField(
            model_name='discountcoupon',
            name='is_internal',
            field=models.BooleanField(default=True),
        ),
    ]
