# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0065_auto_20170911_0524'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='room', options={
                'ordering': ['hotel__name'], 'default_permissions': (
                    'add', 'change', 'delete', 'read')}, ), migrations.AlterField(
            model_name='hotel', name='description', field=models.TextField(
                max_length=10000, null=True, blank=True), ), ]
