import logging

from django.conf import settings

logger = logging.getLogger(__name__)


class CaptureServerErrors(object):
    def process_response(self, request, response):
        logger.debug(
            'In process_response method with response.status_code: %s',
            response.status_code)
        if response.status_code not in settings.SERVER_STATUS_CAPTURE_FILTER:
            event_args = {
                "status_code": response.status_code,
                "origin_url": request.path,
                "request_method": request.method,
                "http_referrer": request.META.get('HTTP_REFERER'),
                "http_user_agent": request.META.get('HTTP_USER_AGENT'),
                "client_ip": request.META.get('REMOTE_ADDR'),
                "ajax_call": request.is_ajax()
            }
        # common_utils.capture_analytics_server_errors(request,'Response Error',event_args)

        return response
