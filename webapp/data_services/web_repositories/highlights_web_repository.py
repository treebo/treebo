from base import log_args
from dbcommon.models.hotel import Highlight


class HighlightsWebRepository(object):

    @log_args()
    def get_all_highlights(self):
        return Highlight.objects.all()

    @log_args()
    def filter_localities(self, *args, **kwargs):
        return Highlight.objects.filter(**kwargs)
