import logging

from django.http import HttpResponse
from rest_framework.response import Response

from apps.third_party.service.id_to_hotelogix_id_mapping import MappingService
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class UpdateMapping(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        try:
            mapping_feed_response = MappingService.mapping_service()
            return HttpResponse(
                mapping_feed_response,
                content_type="application/xml")
        except Exception:
            logger.exception(
                'Error occured in updating latest Mapping of website hotel id to hotelogix hotel id file')
            return Response({"success": False}, status=400)
