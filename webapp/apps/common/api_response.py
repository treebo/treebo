# pylint: disable=len-as-condition,consider-merging-isinstance,unidiomatic-typecheck
# -*- coding: utf-8 -*-
import logging
from rest_framework import status
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST, HTTP_200_OK
from rest_framework.utils.serializer_helpers import ReturnDict
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.common import error_codes

logger = logging.getLogger(__name__)


class APIResponse:
    """
    base class for generating success and error responses
    helps keep the format consistent
    """

    @classmethod
    def treebo_exception_error_response(cls, e):
        return cls.__prep_error_response(
            e.error_code,
            e.message,
            e.developer_message,
            e.status_code,
            "logical_error")

    @classmethod
    def treebo_exception_error_response_from_input(cls, error_code, message, status_code, developer_message=''):
        return cls.__prep_error_response(
            error_code,
            message,
            developer_message,
            status_code,
            "logical_error")

    @classmethod
    def otp_not_sent_exception_error_response(cls, e):
        return cls.__prep_otp_not_sent_response(
            e.error_code,
            e.message,
            e.developer_message,
            e.status_code,
            e.otp_request_attempts,
            e.max_otp_request_attempts,
            "logical_error")

    @classmethod
    def otp_validation_failed_error_response(cls, e):
        return cls.__prep_otp_validation_failed_response(
            e.error_code,
            e.message,
            e.developer_message,
            e.status_code,
            e.failed_validation_attempts,
            e.max_failed_validation_attempts,
            "logical_error")

    @classmethod
    def internal_error_response(
        cls,
        messages,
        api_name='unknown',
        developer_message=''):
        error_code = 'WEB_5000'
        logger.error("internal_error_response %s ", str(messages))
        slack_alert.publish_alert(
            'web internal error during `%s` : %s ' %
            (api_name, str(messages)), channel=slack_alert.AUTH_ALERTS)
        return cls.__prep_error_response(
            error_code,
            messages,
            developer_message,
            HTTP_500_INTERNAL_SERVER_ERROR,
            "unknown_error")

    @classmethod
    def invalid_request_error_response(cls, errors, api_name='unknown'):
        logger.error("invalid_request_error_response %s ", str(errors))
        slack_alert.publish_alert(
            'web invalid request during `%s` : %s ' %
            (api_name, str(errors)), channel=slack_alert.AUTH_ALERTS)
        err_list = []
        errors = errors if isinstance(errors, list) else [errors]
        if isinstance(errors, list):
            for error in errors:
                if type(error) in (dict, ReturnDict):
                    for field, field_errs in list(error.items()):
                        for field_err in field_errs:
                            err_list.append({
                                "message": str(field) + " " + cls._break_error_message(field_err),
                                "error_type": "validation_error"
                            })
                else:
                    err_list.append({
                        "message": error,
                        "error_type": "validation_error"
                    })

        else:
            err_list.append({
                "message": errors,
                "error_type": "validation_error"
            })

        response = Response(
            data={
                "errors": err_list},
            status=HTTP_400_BAD_REQUEST)
        return response

    # prep simple error response
    @classmethod
    def error_response_from_error_code(
        cls,
        code,
        status_code=HTTP_400_BAD_REQUEST,
        api_name='unknown',
        channel=slack_alert.AUTH_ALERTS,
        message=''):
        error_dict = error_codes.get(code)
        if not status_code:
            status_code = error_dict['status_code'] if 'status_code' in error_dict else status_code
        error_code_msg = error_dict['msg']
        msg = ' - '.join(filter(None, (error_code_msg, message)))
        logger.error("error_response_from_error_code %s ", str(msg))
        slack_alert.publish_alert(
            'web error_response_from_error_code `%s` : %s(%s) ' %
            (api_name, str(msg), code), channel=channel)
        return cls.prep_simple_error_response(code, msg, status_code)

    # prep simple error response
    @classmethod
    def error_response_from_exception(
        cls,
        e,
    ):
        msg = str(e)
        code = e.error_code
        status_code = e.status_code
        logger.error("error_response_from_error_code %s ", str(msg))
        return cls.prep_simple_error_response(code, msg, status_code)

    @classmethod
    def prep_simple_error_response(cls, code, msg, status_code):
        error_response = {
            "error": {
                'msg': msg,
                'code': code
            }
        }
        response = Response(data=error_response, status=status_code)
        return response

    @classmethod
    def error_response(cls, error_code, messages, status_code, error_type):
        return cls.__prep_error_response(
            error_code, messages, status_code, error_type)

    @staticmethod
    def error_response_from_error_message(error_message, status_code=HTTP_400_BAD_REQUEST):
        error_response = {
            "errors": [{
                "message": error_message
            }]
        }

        response = Response(data=error_response, status=status_code)
        return response

    @classmethod
    def __prep_error_response(
        cls,
        error_code,
        message,
        developer_message,
        response_code,
        error_type="unknown_error"):
        """
        prepares a Response object in the standard format
        :param errors: error codes and messages explaining/accompanying the error
        :return: Response object
        """
        error_response = {
            "errors": [{
                "code": error_code,
                "message": message,
                "error_type": error_type,
                'developer_message': developer_message
            }]
        }
        response = Response(data=error_response, status=response_code)
        return response

    @classmethod
    def __prep_otp_not_sent_response(
        cls,
        error_code,
        message,
        developer_message,
        response_code,
        otp_request_attempts,
        max_otp_request_attempts,
        error_type="unknown_error"):
        """
        prepares a Response object in the standard format for OTP not sent
        :param errors: error codes and messages explaining/accompanying the error
        :return: Response object
        """
        error = {
            "code": error_code,
            "message": message,
            "error_type": error_type,
            "developer_message": developer_message
        }

        if otp_request_attempts:
            error['otp_request_attempts'] = otp_request_attempts
            error['max_otp_request_attempts'] = max_otp_request_attempts

        response = Response(data={"errors": [error]}, status=response_code)
        return response

    @classmethod
    def __prep_otp_validation_failed_response(
        cls,
        error_code,
        message,
        developer_message,
        response_code,
        failed_validation_attempts,
        max_failed_validation_attempts,
        error_type="unknown_error"):
        """
        prepares a Response object in the standard format for failed OTP validation
        :param errors: error codes and messages explaining/accompanying the error
        :return: Response object
        """
        error = {
            "code": error_code,
            "message": message,
            "error_type": error_type,
            "developer_message": developer_message
        }

        if failed_validation_attempts:
            error['failed_validation_attempts'] = failed_validation_attempts
            error['max_failed_validation_attempts'] = max_failed_validation_attempts

        response = Response(data={"errors": [error]}, status=response_code)
        return response

    @classmethod
    def prep_success_response(cls, message, response_code=HTTP_200_OK):
        """
        vanilla Response object on success
        :param message: what message to embed as part of the response
        :param response_code: what http response code to use
        :return: Response object
        """
        if isinstance(message, dict) or isinstance(message, ReturnDict):
            response = message
        else:
            response = {
                "message": message
            }

        response = Response(data=response, status=response_code)
        return response

    @staticmethod
    def fetch_message_from_response(response_data):
        message = ''
        try:
            errors = response_data['errors']
            if isinstance(errors, list):
                if len(errors) > 0:
                    message = str(errors[0]['message'])
                else:
                    message = ''
            else:
                if 'validation_errors' in errors:
                    validation_errors = errors['validation_errors']
                    for field_name, error_arr in list(
                        validation_errors.items()):
                        if isinstance(error_arr, list):
                            message = field_name + \
                                      APIResponse._break_error_message(error_arr[0])
                        else:
                            message = field_name + \
                                      APIResponse._break_error_message(error_arr)
        except Exception:
            pass
        return message

    @staticmethod
    def _break_error_message(message):
        if not message:
            return ''
        message = str(message)
        if message.startswith('This field'):
            message = message.replace('This field ', '')
        return message

    @classmethod
    def bad_request_error_message(cls, error_message, error_code, error_type="unknown_error"):
        return cls.__prep_error_response(error_code=error_code, message=error_message, developer_message="",
                                         response_code=status.HTTP_400_BAD_REQUEST, error_type=error_type)
