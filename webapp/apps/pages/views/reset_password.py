import json
import logging
from django.http.response import HttpResponseRedirect
from apps.auth.services.password_service import PasswordService
from base.views.template import TreeboTemplateView

__author__ = 'varunachar'

log = logging.getLogger(__name__)


class ResetPassword(TreeboTemplateView):
    template_name = 'resetpassword/index.html'
    reset_password_service = PasswordService()

    def getData(self, request, args, kwargs):
        slug = kwargs.get('slug', '')

        if not slug:
            log.error("No slug received")
            return HttpResponseRedirect("/")

        context = {
            'scripts': ['desktop/js/resetpassword.js'],
            'styles': ['desktop/css/resetpassword.css'],
            'footer': False,
            'jsvars': str(json.dumps({
                'slug': slug
            }))
        }
        return context
