from data_services.respositories_factory import RepositoriesFactory

hotel_repository = RepositoriesFactory.get_hotel_repository()


class PropertyService:

    @staticmethod
    def get_property_details(hotel_id):
        hotel_result = hotel_repository.get_hotels_for_id_list([hotel_id])
        return hotel_result
