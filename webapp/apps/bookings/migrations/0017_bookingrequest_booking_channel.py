# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0016_bookingrequest_meta_http_host'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest', name='booking_channel', field=models.CharField(
                default='', max_length=50, blank=True, choices=[
                    ('msite', 'msite'), ('website', 'website')]), ), ]
