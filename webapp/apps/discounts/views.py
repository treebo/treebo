import datetime
import logging

from django.http import Http404
from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.checkout.models import BookingRequest
from apps.common import error_codes
from apps.common.error_codes import get as error_message
from apps.discounts import constants
from apps.discounts import utils
from apps.discounts.discount_manager import DiscountManager
from apps.discounts.exceptions import AvailableCouponsException
from apps.discounts.models import DiscountCoupon, DiscountCouponConstraints, ConstraintKeywords, \
    ConstraintOperators, \
    ConstraintExpression
from apps.discounts.serializers import DiscountCouponSerializer, DiscountCouponSummarySerializer
from apps.discounts.services.discount_coupon_services import DiscountCouponServices
from common.services.feature_toggle_api import FeatureToggleAPI
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class CreateDiscount(TreeboAPIView):
    """
    Create Discount Coupon
    """

    def get(self, request):
        raise Exception('Method not supported')

    def post(self, request):
        logger.info("inside post of create discount")
        code = request.POST.get("code")
        if DiscountCoupon.objects.filter(code=code).exists() > 0:
            logger.debug(code + "Discount Coupon Already Exist")
            context = {"coupon": code, "status": constants.FAILURE,
                       "msg": "Discount Coupon Already Exist", "code": 400}
            return Response(context)
        else:
            serializer = DiscountCouponSerializer(data=request.POST)
            if serializer.is_valid():
                serializer.save()
                logger.debug(code + "Discount Coupon Created")
                context = {"coupon": code, "status": constants.SUCCESS,
                           "msg": "Discount Coupon Created", "code": 200}
                return Response(context)
            else:
                logger.debug(code + "Validation Error")
                context = {
                    "coupon": code,
                    "status": constants.FAILURE,
                    "msg": "Validation Error",
                    "code": 400}
                return Response(context)


class CreateConstraint(TreeboAPIView):
    """
    Create Discount Coupon Constraint
    """

    def get(self, request):
        raise Exception('Method not supported')

    def post(self, request):
        logger.info("inside post of create constraint")
        code = request.POST["code"]
        keyword = request.POST["keyword"]
        operator = request.POST["operation"]
        value_one = request.POST["value_one"] if 'value_one' in request.POST else ""
        value_two = request.POST["value_two"] if 'value_two' in request.POST else ""

        constraint = DiscountCouponConstraints()
        if DiscountCoupon.objects.filter(code=code).exists() > 0:
            constraint.coupon = DiscountCoupon.objects.get(code=code)
        else:
            logger.debug(code + "Discount Coupon Not Exist")
            context = {"coupon": code, "status": constants.FAILURE,
                       "msg": "Discount Coupon Not Exist", "code": 400}
            return Response(context)

        if ConstraintKeywords.objects.filter(keyword=keyword).exists() > 0:
            constraint.keyword = ConstraintKeywords.objects.get(
                keyword=keyword)
        else:
            logger.debug(code + "Keyword Not Exist")
            context = {
                "keyword": keyword,
                "status": constants.FAILURE,
                "msg": "Keyword Not Exist",
                "code": 400}
            return Response(context)

        if ConstraintOperators.objects.filter(operator=operator).exists() > 0:
            constraint.operation = ConstraintOperators.objects.get(
                operator=operator)
        else:
            logger.debug(code + "Operator Not Exist")
            context = {"operator": operator, "status": constants.FAILURE,
                       "msg": "Operator Not Exist", "code": 400}
            return Response(context)

        constraint.value_one = value_one
        constraint.value_two = value_two
        constraint.save()

        try:
            expression = ConstraintExpression.objects.get(
                coupon=DiscountCoupon.objects.get(code=code))
            prevExpression = expression.expression
            expression.expression = prevExpression + \
                " AND " + str(constraint.id)
            expression.save()
        except ConstraintExpression.DoesNotExist:
            expression = ConstraintExpression()
            expression.coupon = DiscountCoupon.objects.get(code=code)
            expression.expression = str(constraint.id)
            expression.save()

        logger.debug(code + "Discount Coupon Constraint Created")
        context = {"coupon": code, "status": constants.SUCCESS,
                   "msg": "Discount Coupon Constraint Created",
                   "code": 200}
        return Response(context)


class UpdateDiscount(TreeboAPIView):
    """
    Update and Deactivate Discount Coupon
    """

    def get_discount_object(self, pk):
        try:
            discount_object = DiscountCoupon.objects.get(pk=pk)
            return discount_object
        except DiscountCoupon.DoesNotExist:
            raise Http404

    def put(self, request, pk):
        logger.info("inside put of update discount")
        discount = self.get_discount_object(pk)
        code = request.data["code"]
        serializer = DiscountCouponSerializer(discount, data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.debug(code + "Discount Coupon Updated")
            context = {"coupon": code, "status": constants.SUCCESS,
                       "msg": "Discount Coupon Updated", "code": 200}
            return Response(context)
        else:
            logger.debug(code + "Validation Error")
            context = {
                "coupon": code,
                "status": constants.FAILURE,
                "msg": "Validation Error",
                "code": 400}
            return Response(context)

    def delete(self, request, pk):
        logger.info("inside delete of update discount")
        discount = self.get_discount_object(pk)
        discount.is_active = False
        discount.save()
        logger.debug(discount.code + "Discount Coupon Deactivated")
        context = {
            "status": constants.SUCCESS,
            "msg": "Discount Coupon Deactivated",
            "code": 200}
        return Response(context)


class CreateBulkDiscounts(TreeboAPIView):
    """
    Create Bulk Discounts
    """

    def post(self, request):
        logger.info("inside post of create bulk discount")
        bulkcodes = request.POST.get("code")
        codes = bulkcodes.split(",")
        context = []
        for code in codes:
            code = code.strip()
            if DiscountCoupon.objects.filter(code=code).exists() > 0:
                logger.debug(code + "Discount Coupon Already Exist")
                created = {"coupon": code, "status": constants.FAILURE,
                           "msg": "Discount Coupon Already Exist",
                           "code": 200}
            else:
                request.data["code"] = code
                print((request.data))
                serializer = DiscountCouponSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    logger.debug(code + "Discount Coupon Created")
                    created = {"coupon": code, "status": constants.SUCCESS,
                               "msg": "Discount Coupon Created",
                               "code": 200}
                else:
                    logger.debug(code + "Validation Error")
                    created = {"coupon": code, "status": constants.FAILURE,
                               "msg": "Validation Error", "code": 400}
            context.append(created)
        return Response(context)


class ApplicableDiscounts(TreeboAPIView):
    """
    Get Applicable Discounts
    """

    def dispatch(self, *args, **kwargs):
        return super(ApplicableDiscounts, self).dispatch(*args, **kwargs)

    def post(self, request):
        logger.info("Does not support POST")
        raise Exception('Method not supported')

    def get(self, request):
        logger.info("inside get of applicable discounts")
        today = datetime.date.today()
        try:
            coupons = DiscountCoupon.objects.filter(
                is_active=True,
                expiry__gte=today,
                start_date__lte=today,
                max_available_usages__gt=0,
                is_internal=False)
            couponSerializer = DiscountCouponSerializer(coupons, many=True)
            return Response(couponSerializer.data)
        except DiscountCoupon.DoesNotExist:
            logger.debug("No Discount Coupons Available")
            return Response({"status": constants.FAILURE,
                             "code": 400,
                             "msg": "No Discount Coupons Available"},
                            status=404)


class PriorityDiscountValidator(serializers.Serializer):
    """
    Apply Discount Validator
    """

    requiredErrorMessageCheck = {
        'required': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
        'blank': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
        'invalid': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
        'datetime': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
    }

    requiredErrorMessageCost = {
        'required': error_codes.INVALID_TOTAL_COST,
        'blank': error_codes.INVALID_TOTAL_COST,
        'invalid': error_codes.INVALID_TOTAL_COST
    }

    checkin = serializers.DateField(required=True, format="%Y-%m-%d",
                                    error_messages=requiredErrorMessageCheck)
    checkout = serializers.DateField(required=True, format="%Y-%m-%d",
                                     error_messages=requiredErrorMessageCheck)
    booking_date = serializers.DateField(
        required=True,
        format="%Y-%m-%d",
        error_messages=requiredErrorMessageCheck)
    total_cost = serializers.FloatField(
        required=True, error_messages=requiredErrorMessageCost)
    total_pretax_cost = serializers.FloatField(
        required=True, error_messages=requiredErrorMessageCost)


class PriorityDiscount(TreeboAPIView):
    """
    Priority Discount
    """

    validationSerializer = PriorityDiscountValidator

    def dispatch(self, *args, **kwargs):
        return super(PriorityDiscount, self).dispatch(*args, **kwargs)

    def get(self, request):
        kwargs = utils.parseRequest(request)
        coupons = list(
            DiscountCoupon.objects.filter(
                is_active=True,
                expiry__gte=kwargs['booking_date'],
                start_date__lte=kwargs['booking_date'],
                max_available_usages__gt=0))
        context = {}
        if len(coupons) > 0:
            max_discount = 0
            code = ""
            for coupon in coupons:
                kwargs['code'] = coupon.code
                discountManager = DiscountManager(**kwargs)
                coupon, message = discountManager.validate()
                if message == constants.SUCCESS:
                    discount = discountManager.apply(coupon, kwargs['rooms'])
                    coupon_code, message, type, terms = coupon.code, coupon.coupon_message, \
                        coupon.coupon_type, coupon.terms
                    if discount > max_discount:
                        max_discount = discount
                        code = coupon_code
                    logger.debug(
                        code +
                        " " +
                        str(max_discount) +
                        " " +
                        type +
                        " " +
                        terms)
                    context = {
                        "coupon_code": code,
                        'discount': max_discount,
                        'code': 200,
                        'message': constants.DISCOUNT_APPLIED,
                        'terms': terms,
                        'coupon_message': message,
                        'coupon_type': type}
        else:
            logger.debug("No Discount Coupons Available")
            context = {"status": constants.FAILURE, "code": 400,
                       "msg": "No Discount Coupons Available"}
        return Response(context)


class ApplicabilityAndApplyValidator(serializers.Serializer):
    """
    Apply Discount Validator
    """

    requiredErrorMessageCheck = {
        'required': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
        'blank': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
        'invalid': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
        'datetime': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
    }

    requiredErrorMessageCost = {
        'required': error_codes.INVALID_TOTAL_COST,
        'blank': error_codes.INVALID_TOTAL_COST,
        'invalid': error_codes.INVALID_TOTAL_COST
    }

    code = serializers.CharField(
        required=True,
        error_messages={
            'required': error_codes.INVALID_COUPON_CODE,
            'blank': error_codes.INVALID_COUPON_CODE})
    checkin = serializers.DateField(required=True, format="%Y-%m-%d",
                                    error_messages=requiredErrorMessageCheck)
    checkout = serializers.DateField(required=True, format="%Y-%m-%d",
                                     error_messages=requiredErrorMessageCheck)
    total_cost = serializers.FloatField(
        required=True, error_messages=requiredErrorMessageCost)
    total_pretax_cost = serializers.FloatField(
        required=True, error_messages=requiredErrorMessageCost)


class CheckApplicability(TreeboAPIView):
    """
    Checks Applicability
    """

    validationSerializer = ApplicabilityAndApplyValidator

    def dispatch(self, *args, **kwargs):
        return super(CheckApplicability, self).dispatch(*args, **kwargs)

    def get(self, request):
        raise Exception('Method not supported')

    def post(self, request):
        logger.info("inside post of check applicability")
        kwargs = utils.parseRequest(request)
        discountManager = DiscountManager(**kwargs)
        coupon, message = discountManager.validate()
        if message == constants.SUCCESS:
            logger.debug(coupon.code + "is applicable")
            context = {
                "coupon": coupon.code,
                "message": "success",
                "code": 200}
        else:
            context = {"coupon": coupon, "message": message, "code": 400}
        return Response(context)


class ApplyDiscount(TreeboAPIView):
    """
    Apply Discount
    """

    validationSerializer = ApplicabilityAndApplyValidator

    def dispatch(self, *args, **kwargs):
        return super(ApplyDiscount, self).dispatch(*args, **kwargs)

    def get(self, request):
        raise Exception('Method not supported')

    def post(self, request):
        logger.info("inside post of apply discount")
        try:
            kwargs = utils.parseRequest(request)
            discountManager = DiscountManager(**kwargs)
            coupon, message = discountManager.validate()

            if coupon:
                if coupon.terms is not None:
                    terms_list = coupon.terms.split("\\n")
                    coupon.terms = terms_list

                discountApplied = discountManager.apply(
                    coupon, kwargs['rooms'])
                coupon_code, couponMessage, couponType, terms = coupon.code, coupon.coupon_message, \
                    coupon.coupon_type, coupon.terms
                logger.debug(
                    "discount" +
                    str(discountApplied) +
                    "terms" +
                    "coupon msg" +
                    couponMessage)
                context = {
                    'discount': discountApplied,
                    'code': 200,
                    'message': constants.DISCOUNT_APPLIED,
                    'terms': coupon.terms,
                    'coupon_message': couponMessage,
                    'coupon_type': couponType}
            else:
                context = {
                    'discount': 0,
                    'code': 500,
                    'message': message,
                    'terms': None}

        except Exception as exc:
            logger.debug(str(exc))
            context = {
                'discount': 0,
                'code': 500,
                'message': str(exc),
                'terms': None}

        return Response(context)


class AutoPromoValidator(serializers.Serializer):
    """
    Apply Discount Validator
    """

    requiredErrorMessageCheck = {
        'required': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
        'blank': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
        'invalid': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
        'datetime': error_codes.INVALID_CHECK_IN_CHECKOUT_DATE,
    }

    requiredErrorMessageCost = {
        'required': error_codes.INVALID_TOTAL_COST,
        'blank': error_codes.INVALID_TOTAL_COST,
        'invalid': error_codes.INVALID_TOTAL_COST
    }

    checkin = serializers.DateField(required=True, format="%Y-%m-%d",
                                    error_messages=requiredErrorMessageCheck)
    checkout = serializers.DateField(required=True, format="%Y-%m-%d",
                                     error_messages=requiredErrorMessageCheck)
    booking_date = serializers.DateField(
        required=True,
        format="%Y-%m-%d",
        error_messages=requiredErrorMessageCheck)
    total_cost = serializers.FloatField(
        required=True, error_messages=requiredErrorMessageCost)
    total_pretax_cost = serializers.FloatField(
        required=True, error_messages=requiredErrorMessageCost)


class AutoPromo(TreeboAPIView):
    """
    Auto Promo Discount
    """

    validationSerializer = AutoPromoValidator

    def dispatch(self, *args, **kwargs):
        return super(AutoPromo, self).dispatch(*args, **kwargs)

    def get(self, request):
        logger.info("inside get of auto promo")
        kwargs = utils.parseRequest(request)
        coupons = DiscountCoupon.objects.filter(
            is_active=True,
            expiry__gte=kwargs['booking_date'],
            start_date__lte=kwargs['booking_date'],
            max_available_usages__gt=0,
            is_auto_applicable=True)

        context = {}
        if len(coupons) > 0:
            max_discount = 0
            code = ""
            for coupon in coupons:
                kwargs['code'] = coupon.code
                discountManager = DiscountManager(**kwargs)
                coupon, message = discountManager.validate()
                if message == constants.SUCCESS:
                    discount = discountManager.apply(coupon, kwargs['rooms'])
                    coupon_code, message, type, terms = coupon.code, coupon.coupon_message, \
                        coupon.coupon_type, coupon.terms
                    if discount > max_discount:
                        max_discount = discount
                        code = coupon_code
                    logger.debug(
                        code +
                        " " +
                        str(max_discount) +
                        " " +
                        type +
                        " " +
                        terms)
                    context = {
                        "coupon_code": code,
                        'discount': max_discount,
                        'code': 200,
                        'message': constants.DISCOUNT_APPLIED,
                        'terms': terms,
                        'coupon_message': message,
                        'coupon_type': type}
        else:
            logger.debug("No Discount Coupons Available")
            context = {"status": constants.FAILURE, "code": 400,
                       "msg": "No Discount Coupons Available"}
        return Response(context)


class AvailableCouponSerializer(serializers.Serializer):
    bid = serializers.IntegerField()


class AvailableCoupon(TreeboAPIView):
    validationSerializer = AvailableCouponSerializer
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)

    def get(self, request, *args, **kwargs):
        if not FeatureToggleAPI.is_enabled("coupon", "availablecoupon", False):
            coupon_serializer = DiscountCouponSummarySerializer([], many=True)
            return Response(coupon_serializer.data)
        if hasattr(
                request,
                'user') and request.user and request.user.is_authenticated():
            logger.info("request.user %s is logged in ", request.user)
            logged_in_user = True
        else:
            logged_in_user = False
        validated_data = self.serializerObject.validated_data
        booking_request_id = validated_data['bid']
        booking_request = BookingRequest.objects.filter(
            pk=booking_request_id).first()
        if not booking_request:
            return Response({"error": error_message(
                error_codes.INVALID_BOOKING)}, status=400)
        referral = True if request.COOKIES.get('avclk') else False
        # referral = True
        user = request.user
        options = dict(user=user, referral=referral)
        coupons = DiscountCouponServices.available_coupons(
            booking_request, **options)
        try:
            coupon_serializer = DiscountCouponSummarySerializer(
                coupons, booking_request=booking_request, logged_in_user=logged_in_user, many=True)
            return Response(coupon_serializer.data)
        except AvailableCouponsException as e:
            logger.exception(e)
            return Response({"error": error_message(error_codes.ERROR_AVAIL_COUPONS)},
                            status=HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            logger.exception(e)
            return Response({"error": error_message(error_codes.ERROR_AVAIL_COUPONS)},
                            status=HTTP_500_INTERNAL_SERVER_ERROR)
