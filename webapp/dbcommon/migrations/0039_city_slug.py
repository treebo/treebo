# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0038_delete_apkdetails'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='slug',
            field=models.SlugField(
                max_length=75,
                unique=True,
                null=True,
                blank=True),
        ),
    ]
