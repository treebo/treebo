"use strict";
/*
 beforeSend: Put in things like tracking/analytics/timings.
 success: overwrite/parsing the response and converting it to common format accepted by code.
 fail: Tracking and logging error
 always: tracking
 */

/*
 All requests will have a 200 response, with error represented as
 {
 status:'error',
 msg:'',
 code:'',
 data:''
 }
 */

var $ = require('jquery'),
  _ = require('underscore'),
  l = require('./logger'),
  Promise = require("bluebird"),
  TreeboError = require('./error');

// let NProgress = require('nprogress-npm');


/*
 $( document ).ajaxStart(function() {
 NProgress.start();
 });

 $( document ).ajaxStop(function() {
 NProgress.done();
 });
 */
module.exports = (config) => {
  return new Promise(function (resolve, reject) {
    var ajax = $.ajax({
      type: config.type,
      url: config.url,
      data: config.data,
      dataType: (config.dataType !== undefined) ? config.dataType : 'json',
      async: (config.async != undefined) ? config.async : true,
      cache: (config.cache != undefined) ? config.cache : true,
      timeout: config.timeout || 20000
    });

    ajax.done(function (data, textStatus, jqXHR) {
      var data = data.data || data;
      if (data.status === 'error') {
        return reject(new TreeboError(data));
      }
      // Sample success result - {"status":"success","data":{"status":"success","msg":"User logged in successfully","userId":4045,"isSignUp":false}}
      // Sample error result - {"status":"success","data":{"status":"error","msg":"Invalid username and password","userId":4045,"isSignUp":false}}
      resolve(data);
    });

    ajax.fail(function (jqXHR, textStatus, errorThrown) {
      if (jqXHR.responseJSON.error) {
        reject(new TreeboError(jqXHR.responseJSON.error))
      } else {
        reject(new TreeboError(jqXHR.responseJSON.errors[0]));
      }
    })
    ;
  });
};
