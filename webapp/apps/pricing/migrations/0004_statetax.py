# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0014_facility_rooms'),
        ('pricing', '0003_luxurytax'),
    ]

    operations = [
        migrations.CreateModel(
            name='StateTax',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('tax_type', models.CharField(default=b'ST', max_length=5,
                                              choices=[(b'ST', b'Service Tax'),
                                                       (b'SBC', b'Swachh Bharat Cess'),
                                                       (b'KKC', b'Krishi Kalyan Cess')])),
                ('tax_value', models.DecimalField(default=8.4, max_digits=20, decimal_places=10)),
                ('luxury_tax_on_base', models.BooleanField(default=False)),
                ('effective_date', models.DateField(null=True)),
                ('expiry_date', models.DateField(null=True)),
                ('state', models.ForeignKey(to='dbcommon.State', on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
