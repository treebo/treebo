__author__ = 'amithsoman'

from django.apps import AppConfig

default_app_config = 'common.CommonAppConfig'


class CommonAppConfig(AppConfig):
    name = 'common'

    def ready(self):
        if not hasattr(self, 'path'):
            self.path = self._path_from_module('common')
        from common.job.jobs.register_job import RegisterJob
        RegisterJob()
