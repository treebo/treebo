# pylint: disable=too-few-public-methods,invalid-name,old-division,relative-import,too-many-locals,bad-continuation
import json
import logging
import traceback
from collections import defaultdict

import requests
from django.conf import settings
from rest_framework import serializers

# from b2b.consumer.tax.exceptions import HotelNotFound
# from b2b.consumer.tax.interface import BaseTaxBackend
# from b2b.consumer.tax.utils import get_intermediate_dates
# from b2b.models import Hotel
# from constants import InclusionMapping
from apps.common.utils import get_intermediate_dates

logger = logging.getLogger(__name__)

RoomNight = 'RoomNight'

EPCharges = 'ExtraPersonCharges'
VegMealCharges = 'VegMealCharges'
NonVegMealCharges = 'NonVegMealCharges'
CabCharges = 'CabCharges'
LaundryCharges = 'LaundryCharges'
ExtraBedCharges = 'ExtraBedCharges'
ConferenceRoomCharges = 'ConferenceRoomCharges'
ConferenceMealCharges = 'ConferenceMealCharges'

ChargeTypes = (
    (VegMealCharges, VegMealCharges),
    (NonVegMealCharges, NonVegMealCharges),
    (CabCharges, CabCharges),
    (LaundryCharges, LaundryCharges),
    (ExtraBedCharges, ExtraBedCharges),
    (ConferenceRoomCharges, ConferenceRoomCharges),
    (ConferenceMealCharges, ConferenceMealCharges),
    (RoomNight, RoomNight),
)


class _SkuCharge(serializers.Serializer):
    sku_code = serializers.CharField()
    pretax_price = serializers.DecimalField(max_digits=20, decimal_places=2)
    rate_plan = serializers.CharField()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class _HotelCharge(serializers.Serializer):
    hotel_id = serializers.CharField()
    sku_charges = _SkuCharge(many=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class TaxRequestDTO(serializers.Serializer):
    """
    DTO to get tax from tax service.
    Gets tax for combination of multiple rooms and multiple occupancies for a single hotel
    """
    stay_start = serializers.DateField(format='%Y-%m-%d')
    stay_end = serializers.DateField(format='%Y-%m-%d')
    hotel_charges = _HotelCharge(many=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


MEAL = "Meal"
LAUNDRY = "Laundry"
CAB = "Cab"
RoomNight = "RoomNight"
ConferenceRoom = "ConferenceRoom"
ConferenceMeal = "ConferenceMeal"

InclusionMapping = {
    CabCharges: CAB,
    VegMealCharges: MEAL,
    NonVegMealCharges: MEAL,
    LaundryCharges: LAUNDRY,
    RoomNight: RoomNight,
    ConferenceRoomCharges: ConferenceRoom,
    ConferenceMealCharges: ConferenceMeal,

}


class Timeouts(object):
    CONNECTION_TIMEOUT = 6
    RESPONSE_TIMEOUT = 18


class SoaTaxBackend:
    """
    Gets tax details from soa tax backend
    """

    @classmethod
    def get_tax(cls, tax_request):
        """
        Gets the tax details using hotels, room type, start date and end date
        """
        dto = TaxRequestDTO(data=tax_request)
        if not dto.is_valid():
            raise RuntimeError("Invalid TaxRequestDto {err}".format(err=dto.errors))

        tax_request_data = dto.data
        hotel_ids = [attrib['hotel_id'] for attrib in tax_request_data['hotel_charges']]

        try:

            tax_url = settings.TAX_URL

            # Update hotel id with external hotel id
            request_payload = cls._request_payload(tax_request_data)

            # Get the tax data
            headers = {'Content-type': 'application/json'}
            logger.info('Requesting Tax : {tax_request}'.format(tax_request=request_payload))
            logger.info('Received Tax Request DATA: {tax_request} '.format(
                tax_request=json.dumps(request_payload)))
            tax_details_response = requests.post(tax_url, data=json.dumps(request_payload), headers=headers,
                                                 timeout=(Timeouts.CONNECTION_TIMEOUT, Timeouts.RESPONSE_TIMEOUT))
            response_data = tax_details_response.json()
            if tax_details_response.status_code != 200:
                logger.exception("Error getting tax response request - %s and response %s",
                                 (json.dumps(request_payload)),
                                 response_data)
                raise Exception("Error getting tax response due to %s",
                                response_data)

            logger.info('Received Tax Response DATA: {tax_response} '.format(tax_response=response_data))
            return cls.tax_response_data(response_data)
        except Exception as e:
            logger.exception("error occurred while getting tax details for "
                             "hotel id {hotel_id}: {err}".format(hotel_id=','.join(hotel_ids),
                                                                 err=str(e)))
            raise

    @classmethod
    def _request_payload(cls, tax_request_data):

        """

        Args:
            tax_request_data:

        {
            "skus": [
                {
                    "index": "2033365-rp-1-1",
                    "prices": [
                        {
                            "date": "2019-04-01",
                            "index": "54",
                            "sku_prices": [
                                {
                                    "index": "54",
                                    "sku_code": "54",
                                    "sku_price": 1981.31
                                }
                            ]
                        }
                    ],
                    "is_sez": false,
                    "attributes": [
                        {
                            "key": "hotel_id",
                            "value": "2033365"
                        }
                    ]
                },
                {
                    "index": "2033365-rp-2-1",
                    "prices": [
                        {
                            "date": "2019-04-01",
                            "index": "48",
                            "sku_prices": [
                                {
                                    "index": "48",
                                    "sku_code": "48",
                                    "sku_price": 2475
                                }
                            ]
                        }
                    ],
                    "is_sez": false,
                    "attributes": [
                        {
                            "key": "hotel_id",
                            "value": "2033365"
                        }
                    ]
                },
                {
                    "index": "2033365-rp-2-1",
                    "prices": [
                        {
                            "date": "2019-04-01",
                            "index": "48",
                            "sku_prices": [
                                {
                                    "index": "48",
                                    "sku_code": "48",
                                    "sku_price": 2475
                                }
                            ]
                        }
                    ],
                    "is_sez": false,
                    "attributes": [
                        {
                            "key": "hotel_id",
                            "value": "2033365"
                        }
                    ]
                }
            ]
        }
        """

        dates = get_intermediate_dates(tax_request_data['stay_start'], tax_request_data['stay_end'])
        hotel_charges = tax_request_data['hotel_charges']
        tax_request_skus = []
        for i, hotel_charge in enumerate(hotel_charges):
            prices = []
            for date_index, date in enumerate(dates):
                sku_prices = []
                for sku_charge in hotel_charge['sku_charges']:
                    night_wise_price = round(float(sku_charge['pretax_price']) / len(dates), 2)

                    sku_prices.append(dict(index='-'.join((sku_charge['sku_code'],
                                                           sku_charge['rate_plan'])),
                                           sku_code=sku_charge['sku_code'],
                                           sku_price=night_wise_price))
                prices.append(dict(date=date.strftime('%Y-%m-%d'),
                                   index=str(date_index + 1),
                                   sku_prices=sku_prices))

            tax_request_skus.append(dict(
                prices=prices,
                index='-'.join([hotel_charge['hotel_id']]),
                is_sez=False,
                attributes=[
                    {
                        "key": "hotel_id",
                        "value": hotel_charge['hotel_id']
                    }
                ]
            ))

        return dict(skus=tax_request_skus)

    """
    {
        "skus": [
            {
                "index": "2033365-rp-1-1",
                "prices": [
                    {
                        "date": "2019-04-01",
                        "index": "54",
                        "post_tax_price": 2219.07,
                        "pre_tax_price": 1981.31,
                        "tax": [
                            {
                                "breakup": [
                                    {
                                        "percent": 0,
                                        "type": "igst",
                                        "value": 0
                                    },
                                    {
                                        "percent": 6,
                                        "type": "cgst",
                                        "value": 118.88
                                    },
                                    {
                                        "percent": 6,
                                        "type": "sgst",
                                        "value": 118.88
                                    }
                                ],
                                "index": "54",
                                "percent": 12,
                                "value": 237.76
                            }
                        ]
                    },
                    {
                        "date": "2019-04-01",
                        "index": "55",
                        "post_tax_price": 2219.07,
                        "pre_tax_price": 1981.31,
                        "tax": [
                            {
                                "breakup": [
                                    {
                                        "percent": 0,
                                        "type": "igst",
                                        "value": 0
                                    },
                                    {
                                        "percent": 6,
                                        "type": "cgst",
                                        "value": 118.88
                                    },
                                    {
                                        "percent": 6,
                                        "type": "sgst",
                                        "value": 118.88
                                    }
                                ],
                                "index": "55",
                                "percent": 12,
                                "value": 237.76
                            }
                        ]
                    },
                    {
                        "date": "2019-04-02",
                        "index": "54",
                        "post_tax_price": 2219.07,
                        "pre_tax_price": 1981.31,
                        "tax": [
                            {
                                "breakup": [
                                    {
                                        "percent": 0,
                                        "type": "igst",
                                        "value": 0
                                    },
                                    {
                                        "percent": 6,
                                        "type": "cgst",
                                        "value": 118.88
                                    },
                                    {
                                        "percent": 6,
                                        "type": "sgst",
                                        "value": 118.88
                                    }
                                ],
                                "index": "54",
                                "percent": 12,
                                "value": 237.76
                            }
                        ]
                    }
                ]
            }
        ]
    }    
"""

    @classmethod
    def tax_response_data(cls, tax_response):
        tax_details_for_skus = tax_response['skus']
        formatted_tax_response = defaultdict(lambda: defaultdict(dict))
        for hotel_wise_taxes in tax_details_for_skus:
            hotel_id = hotel_wise_taxes['index']
            for date_wise_taxes in hotel_wise_taxes['prices']:
                sku, rate_plan = date_wise_taxes['index'].split('-')
                if not formatted_tax_response[hotel_id][sku].get(rate_plan):
                    formatted_tax_response[hotel_id][sku][rate_plan] = defaultdict(dict)
                pre_tax_price = formatted_tax_response[hotel_id][sku].get(rate_plan, {}).get('pre_tax_price', 0)
                formatted_tax_response[hotel_id][sku][rate_plan]['pre_tax_price'] = date_wise_taxes['pre_tax_price'] \
                                                                                    + pre_tax_price
                post_tax_price = formatted_tax_response[hotel_id][sku].get(rate_plan, {}).get('post_tax_price', 0)
                formatted_tax_response[hotel_id][sku][rate_plan]['post_tax_price'] = date_wise_taxes['post_tax_price'] \
                                                                                     + post_tax_price
                tax = sum([date_wise_tax['value'] for date_wise_tax in date_wise_taxes['tax']])
                existing_tax = formatted_tax_response[hotel_id][sku].get(rate_plan, {}).get('tax', 0)
                formatted_tax_response[hotel_id][sku][rate_plan]['tax'] = tax + existing_tax

        return formatted_tax_response
