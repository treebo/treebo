import logging

from django.db.models import Q

from apps.reviews.models import TASubrating

logger = logging.getLogger(__name__)


class SubratingService(object):
    def create_or_update_subrating(
            self,
            subrating_name,
            subrating_localized_name=None):
        try:
            subrating = TASubrating.objects.filter(
                subrating_name=subrating_name)
            if not subrating:
                logger.info(
                    "Subrating  %s does not exist .. so create new",
                    subrating_name)
                sub_rating = TASubrating()
                if subrating_localized_name:
                    sub_rating.subrating_localized_name = subrating_localized_name
                sub_rating.subrating_name = subrating_name
                sub_rating.save()
            else:
                logger.info(
                    "Entry already exists with same subrating_name %s %s and subrating_localized_name %s %s",
                    subrating.subrating_name,
                    subrating_name,
                    subrating.subrating_localized_name,
                    subrating_localized_name)

        except Exception:
            logger.exception(
                "Exception in creating new entry in subrating table for %s %s",
                subrating_name,
                subrating_localized_name)

    def fetch_subrating(self, subrating_name):
        try:
            subrating = TASubrating.objects.filter(Q(subrating_name=subrating_name) | Q(
                subrating_localized_name=subrating_name)).first()
            if subrating:
                return subrating
            return None
        except Exception:
            logger.exception(
                "Exception in fetching the subrating %s",
                subrating_name)
            return None

    def delete_subrating(self, subrating_name):
        try:
            TASubrating.objects.filter(subrating_name=subrating_name).delete()
            logger.info(
                "Deleted successfully for subrating %s",
                subrating_name)
        except Exception:
            logger.exception(
                "Exception in deleting subrating object %s",
                subrating_name)

    def is_subrating_enable(self, subrating_name):
        try:
            if TASubrating.objects.filter(Q(subrating_name=subrating_name) | Q(
                    subrating_localized_name=subrating_name)).exists():
                logger.info(" subrating %s enabled", subrating_name)
                return True
            return False
        except Exception:
            logger.exception(
                "Exception in deleting subrating object %s",
                subrating_name)
