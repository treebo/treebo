import logging
from apps.seo.components.base_component import BaseComponent
from django.core.cache import cache
from django.utils.text import slugify
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from django.conf import settings
from apps.seo.serializers.serializer import HotelTypesSerializer
from apps.seo.services.common_services.sql_query_helper import SqlQueryHelper
from dbcommon.models.hotel import Hotel, Locality

logger = logging.getLogger(__name__)


class HotelCategoryInLocation(BaseComponent):

    def __init__(self, locality):
        self.locality = locality

    def build(self):
        return self.fetch_and_build_hotel_category_cache(self.locality)

    @staticmethod
    def fetch_and_build_hotel_category_cache(locality):
        logger.info("category X locality mapping, locality_name : %s , city_name: %s " % (locality.name, locality.city.name))
        data = cache.get(HotelCategoryInLocation.get_cache_key(locality.name, locality.city.name))
        if data:
            return data
        distance_query_str = SqlQueryHelper.get_distance_query_str(locality.latitude, locality.longitude)

        if locality.distance_cap is None:
            locality.distance_cap = float(settings.DEFAULT_SEO_LANDMARK_RADIUS)

        sql_query = 'select distinct on (dc."name") dc."name" from hotels_hotel hh' \
                    ' inner join hotels_hotel_category hhc on hhc.hotel_id = hh.id' \
                    ' inner join dbcommon_citycategorymapforautopagecreation ccmap' \
                    ' on ccmap.category_id = hhc.categories_id' \
                    ' inner join dbcommon_categories dc on dc.id = ccmap.category_id' \
                    ' where ' + distance_query_str + ' < ' + str(locality.distance_cap) + ' and'\
                    ' hh.status = 1 and ccmap.is_enabled = true and dc.enable_for_seo = true and' \
                    ' ccmap.city_id = hh.city_id'

        logger.info("locality fetch_and_build_hotel_category_cache sql query : "+sql_query)
        all_categories = SqlQueryHelper.execute_query_and_map_to_dictionary(sql_query)
        response = {
            'key': 'hotel_types',
            'title': 'HOTEL TYPES IN ' + locality.name.upper(),
            'content': AutoCompleteResultBuilder.build_hotel_types_in_locality_result(all_categories, locality)
        }
        serialized_obj = HotelTypesSerializer(data=response)
        if serialized_obj.is_valid():
            response = serialized_obj.validated_data
        else:
            response['content'] = []

        cache.set(HotelCategoryInLocation.get_cache_key(locality.name, locality.city.name), response, timeout=None)
        logger.info("hotel category response for locality : "+locality.name)
        logger.info(response)
        return response

    @staticmethod
    def generate_category_mapping_list_to_delete(category):
        all_hotels = Hotel.objects.filter(category__name=category.name)

        category_location_mapping_list_to_delete = {}
        for hotel in all_hotels:
            category_location_mapping_list_to_delete.update(
                HotelCategoryInLocation.generate_hotel_mapping_list_to_delete(hotel))

        return category_location_mapping_list_to_delete

    @staticmethod
    def generate_hotel_mapping_list_to_delete(hotel):
        category_location_mapping_list_to_delete = {}
        distance_query_str = SqlQueryHelper.get_distance_query_str(hotel.latitude, hotel.longitude)
        sql_query = "select distinct on (hl.name) hl.name as locality, hc.name as city from hotels_locality hl " \
                    "inner join hotels_city hc on hl.city_id = hc.id where " + distance_query_str \
                    + " < case when hl.distance_cap is null then " \
                    + str(settings.DEFAULT_SEO_LANDMARK_RADIUS) + " else hl.distance_cap end " \
                    #+ "and hl.enable_for_seo = true"

        all_locations_category_for_hotel = SqlQueryHelper.execute_query_and_map_to_dictionary(sql_query)
        logger.info("locality generate_hotel_mapping_list_to_delete sql query : " + sql_query)
        logger.info(all_locations_category_for_hotel)

        for locations_category_for_hotel in all_locations_category_for_hotel:
            hash_key = locations_category_for_hotel["locality"] + "_" + locations_category_for_hotel["city"]
            category_location_mapping_list_to_delete[hash_key] = locations_category_for_hotel

        return category_location_mapping_list_to_delete

    @staticmethod
    def generate_city_mapping_list_to_delete(city_category_map_instance):
        category_location_mapping_list_to_delete = {}
        sql_query = "select distinct on (hl.name) hl.name as locality, hc.name as city from hotels_locality hl " \
                    "inner join hotels_city hc on hl.city_id = hc.id where " \
                    "hl.city_id = " + str(city_category_map_instance.city.id)

        all_locations_category_for_hotel = SqlQueryHelper.execute_query_and_map_to_dictionary(sql_query)
        logger.info("locality generate_city_mapping_list_to_delete sql query : " + sql_query)
        logger.info(all_locations_category_for_hotel)

        for locations_category_for_hotel in all_locations_category_for_hotel:
            hash_key = locations_category_for_hotel["locality"] + "_" + locations_category_for_hotel["city"]
            category_location_mapping_list_to_delete[hash_key] = locations_category_for_hotel

        return category_location_mapping_list_to_delete

    @staticmethod
    def get_cache_key(locality, city):
        return "seo_hotel_category_locality_"+slugify(locality)+"_"+slugify(city)

    @staticmethod
    def invalidate_hotel_category_with_locality_cache(locality, city):
        from apps.seo.pages_v3.locality_page import LocalityPage
        logger.info(
            "invalidating cache locality_name : %s , city_name: %s " % (locality, city))
        cache.delete(HotelCategoryInLocation.get_cache_key(locality, city))
        LocalityPage.delete_page_meta_cache(slugify(locality), slugify(city))

    @staticmethod
    def delete_all_key_in_cache(category_location_mapping_list_to_delete):
        for key, category_location_mapping in list(category_location_mapping_list_to_delete.items()):
            HotelCategoryInLocation.invalidate_hotel_category_with_locality_cache(category_location_mapping["locality"],
                                                     category_location_mapping["city"])

    @staticmethod
    def update_all_key_in_cache(category_location_mapping_list_to_delete):
        for key, category_location_mapping in list(category_location_mapping_list_to_delete.items()):
            locality = Locality.objects.get(name=category_location_mapping["locality"],
                                            city__name=category_location_mapping["city"])
            if locality is not None:
                HotelCategoryInLocation.fetch_and_build_hotel_category_cache(locality)

    @staticmethod
    def update_locality_key_in_cache(locality):
        if locality is not None:
            HotelCategoryInLocation.fetch_and_build_hotel_category_cache(locality)