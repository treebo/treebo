# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('featuregate', '0003_auto_20160518_1006'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='constraintexpression',
            options={},
        ),
        migrations.AlterModelOptions(
            name='constraintkeywords',
            options={},
        ),
        migrations.AlterModelOptions(
            name='constraintoperators',
            options={},
        ),
        migrations.AlterModelOptions(
            name='constraints',
            options={},
        ),
        migrations.AlterModelOptions(
            name='pagevariables',
            options={},
        ),
        migrations.AlterModelOptions(
            name='variablecondition',
            options={},
        ),
    ]
