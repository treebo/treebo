# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0018_auto_20160519_1840'),
        ('bookings', '0018_auto_20160518_1305'),
    ]

    operations = [
    ]
