HOTEL_NAME = {'row': 2, 'col': 3}
# Hotelogix id to be added to launch pad
HOTELOGIX_ID = {'row': 1, 'col': 1}
HOTELOGIX_NAME = {'row': 1, 'col': 1}
HOTEL_TAGLINE = {'row': 2, 'col': 3}
HOTEL_STREET = {'row': 13, 'col': 3}
# Is empty -
HOTEL_LANDMARK = {'row': 1, 'col': 1}
HOTEL_DESCRIPTION = {'row': 159, 'col': 3}
HOTEL_PHONE_NUMBER = {'row': 26, 'col': 3}
HOTEL_LAT = {'row': 14, 'col': 3}
HOTEL_LONG = {'row': 15, 'col': 3}

# Standard check in checkout times - 1200 - 1100 hrs
CHECK_IN_TIME = {'row': 1, 'col': 1}
CHECK_OUT_TIME = {'row': 1, 'col': 1}

ACACIA_ROOM_COUNT = {'row': 6, 'col': 6}
ACACIA_MAX_GUEST_COUNT = {'row': 13, 'col': 6}
ACACIA_MAX_ADULT_WITH_CHILDREN = {'row': 14, 'col': 6}
ACACIA_DESCRIPTION = {'row': 45, 'col': 6}

OAK_ROOM_COUNT = {'row': 6, 'col': 7}
OAK_MAX_GUEST_COUNT = {'row': 13, 'col': 7}
OAK_MAX_ADULT_WITH_CHILDREN = {'row': 14, 'col': 7}
OAK_DESCRIPTION = {'row': 45, 'col': 7}

MAPLE_ROOM_COUNT = {'row': 6, 'col': 8}
MAPLE_MAX_GUEST_COUNT = {'row': 13, 'col': 8}
MAPLE_MAX_ADULT_WITH_CHILDREN = {'row': 14, 'col': 8}
MAPLE_DESCRIPTION = {'row': 45, 'col': 8}

MAHOGANY_ROOM_COUNT = {'row': 6, 'col': 9}
MAHOGANY_MAX_GUEST_COUNT = {'row': 13, 'col': 9}
MAHOGANY_MAX_ADULT_WITH_CHILDREN = {'row': 14, 'col': 9}
MAHOGANY_DESCRIPTION = {'row': 45, 'col': 9}

CITY_NAME = {'row': 11, 'col': 3}
CITY_DESCRIPTION = {'row': 1, 'col': 1}
CITY_TAGLINE = {'row': 1, 'col': 1}
CITY_SUBSCRIPT = {'row': 1, 'col': 1}
CITY_LAT = {'row': 14, 'col': 3}
CITY_LONG = {'row': 15, 'col': 3}

HIGHLIGHTS = {'row': 112, 'col': 3}

LOCALITY_NAME = {'row': 8, 'col': 3}

# Locality Pincode to be added to launch pad
LOCALITY_PINCODE = {'row': 1, 'col': 1}

LANDMARK_1_NAME = {'row': 75, 'col': 3}

LANDMARK_1_DISTANCE = {'row': 78, 'col': 3}
LANDMARK_1_EMAIL_DIRECTIONS = {'row': 79, 'col': 3}
LANDMARK_1_LAT = {'row': 76, 'col': 3}
LANDMARK_1_LONG = {'row': 77, 'col': 3}

LANDMARK_2_NAME = {'row': 81, 'col': 3}
LANDMARK_2_DISTANCE = {'row': 84, 'col': 3}
LANDMARK_2_EMAIL_DIRECTIONS = {'row': 85, 'col': 3}
LANDMARK_2_LAT = {'row': 82, 'col': 3}
LANDMARK_2_LONG = {'row': 83, 'col': 3}

LANDMARK_RAILWAY_LAT = {'row': 61, 'col': 3}
LANDMARK_RAILWAY_LONG = {'row': 62, 'col': 3}

LANDMARK_AIRPORT_LAT = {'row': 56, 'col': 3}
LANDMARK_AIRPORT_LONG = {'row': 57, 'col': 3}

DIRECTIONS_RAILWAY_DISTANCE = {'row': 63, 'col': 3}
DIRECTIONS_RAILWAY_EMAIL = {'row': 155, 'col': 3}
RAILWAY_NAME = {'row': 60, 'col': 3}

DIRECTIONS_AIRPORT_DISTANCE = {'row': 58, 'col': 3}
DIRECTIONS_AIRPORT_EMAIL = {'row': 154, 'col': 3}
AIRPORT_NAME = {'row': 55, 'col': 3}

# To be added to launchpad
STATE_NAME = {'row': 10, 'col': 3}
