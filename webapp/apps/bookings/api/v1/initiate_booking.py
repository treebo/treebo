# -*- coding:utf-8 -*-
"""
    InitiateBooking  API
"""

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from apps.bookings.serializers import BookingSerializer
from apps.bookings.services.booking import BookingService
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from base import log_args
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger
from data_services.respositories_factory import RepositoriesFactory
from data_services.exceptions import HotelDoesNotExist
from apps.common.slack_alert import SlackAlertService as slack_alert

__author__ = 'varunachar'

logger = get_booking_logger(__name__)


class InitiateBooking(TreeboAPIView):
    """
        InitiateBooking
    """
    __booking_service = BookingService()
    authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        return super(InitiateBooking, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """


        booking_data = request.data
        hotel_id = booking_data.get('hotel_id')
        room_type = booking_data.get("room_type")
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotels = hotel_repository.filter_hotels(id=hotel_id)
        if not hotels:
            logger.error("Could not find hotel with id {}".format(hotel_id))
            raise HotelDoesNotExist
        hotel = hotels[0]
        #hotel = Hotel.objects.prefetch_related('rooms').get(pk=hotel_id)
        rooms = hotel_repository.get_rooms_for_hotel(hotel)
        room = next((room for room in rooms if room.room_type_code == room_type), None)
        #room = hotel.rooms.filter(room_type_code=room_type).first()
        booking_data["hotel"] = hotel_id
        booking_data["room"] = room.id if room else None
        try:
            booking = self.__booking_service.initiate_booking(booking_data)
            if not booking:
                logger.error(
                    "booking initiate failed error for data %s ",
                    booking_data)
                return JsonResponse({"error": error_message(
                    codes.BOOKING_NOT_FOUND)}, status=HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                output_serializer = BookingSerializer(instance=booking)
                return JsonResponse(output_serializer.data, status=201)
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.POST,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception(
                "booking initiate failed with internal error for data %s ",
                booking_data)
            return JsonResponse(
                {
                    "error": error_message(
                        codes.BOOKING_INITIATE_ERROR),
                    "developer_message": str(e)}, status=HTTP_500_INTERNAL_SERVER_ERROR)
