# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0045_auto_20161109_0603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='facility',
            name='url',
            field=models.FileField(max_length=200, upload_to='', blank=True),
        ),
        migrations.AlterField(
            model_name='hotel',
            name='landmark',
            field=models.CharField(max_length=200, blank=True),
        ),
    ]
