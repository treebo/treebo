import logging

from django.conf import settings

from apps.bookingstash.service.availability_service import get_availability_service
from apps.pricing.transformers.external_api_transformer import ExternalAPITransformer
from apps.third_party.service.trip_advisor import constants
from apps.third_party.service.trip_advisor.price_adaptors import TripAdvisorPriceAdaptor

logger = logging.getLogger(__name__)


class RatePlanPriceAdaptor(TripAdvisorPriceAdaptor):
    pricing_service = ExternalAPITransformer()

    def get_price_and_availability(self, hotel_ids_list, checkin_date, checkout_date, room_config):
        utm_source = settings.TRIPADVISOR_UTM_SOURCE
        hotel_prices = self.pricing_service.fetch_hotel_room_prices(
            hotel_ids_list, checkin_date, checkout_date, room_config, utm_source)
        room_availability_per_hotel = get_availability_service().get_available_rooms_for_hotels(
            hotel_ids_list, checkin_date, checkout_date, room_config)
        return hotel_prices, room_availability_per_hotel

    def create_room_types_data(self, room_types, hotel, hotel_price, checkin_date, checkout_date,
                               room_availability_per_hotel, num_room_requested, ta_url_room_config):
        if not hotel_price:
            return room_types
        for room in hotel.rooms.all():
            room_type_code = room.room_type_code.lower()
            price_group_by_room = hotel_price

            if room_type_code in price_group_by_room and room_availability_per_hotel.get(
                    (int(hotel.id), room_type_code), 0) > 0:
                room_data = dict()
                room_type = room_type_code.title()
                room_types[room_type] = room_data
                room_data["url"] = constants.TA_HOTEL_URL % (
                    constants.BASE_URL, hotel.get_sitemap_url(), checkin_date, checkout_date, ta_url_room_config,
                    room_type, constants.TA_UTM_SOURCE,
                    constants.TA_UTM_MEDIUM,
                    constants.TA_UTM_CAMPAIGN)

                rate_plan_prices = price_group_by_room[room_type_code]
                lowest_rate_plan_price = rate_plan_prices.price
                room_data["price"] = round(float(lowest_rate_plan_price.pretax_price), 2)
                room_data["taxes"] = round(float(lowest_rate_plan_price.tax), 2)
                room_data["fees"] = 0
                room_data["fees_at_checkout"] = 0
                room_data["taxes_at_checkout"] = 0
                room_data["final_price"] = round(float(lowest_rate_plan_price.sell_price), 2)
                discounts = []
                if lowest_rate_plan_price.promo_discount > 0:
                    discounts = self.generate_discounts(discounts, discount_amount=lowest_rate_plan_price.promo_discount,
                                                        base_price=lowest_rate_plan_price.base_price)
                if lowest_rate_plan_price.coupon_discount > 0:
                    discounts = self.generate_discounts(discounts, discount_amount=lowest_rate_plan_price.coupon_discount,
                                                        base_price=lowest_rate_plan_price.pretax_price)

                if len(discounts) > 0:
                    room_data["discounts"] = discounts
                room_data["currency"] = constants.CURRENCY
                room_data["num_rooms"] = num_room_requested
                room_data["room_amenities"] = self.generate_rooms_amenities(room.id)
        return room_types
