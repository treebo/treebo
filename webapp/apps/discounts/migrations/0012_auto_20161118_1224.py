# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discounts', '0011_auto_20160909_1502'),
    ]

    operations = [
        migrations.AlterField(
            model_name='constraintoperators',
            name='operator',
            field=models.CharField(default=b'', max_length=25, null=True),
        ),
    ]
