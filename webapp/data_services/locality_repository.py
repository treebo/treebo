from base import log_args
from dbcommon.models.location import Locality
from data_services.decorators import SingletonDecorator
from django.db.models import Q


@SingletonDecorator
class LocalityRepository(object):

    def __init__(self, locality_web_repository):
        self.locality_web_repository = locality_web_repository

    @log_args()
    def get_all_localities(self):
        return Locality.objects.all()

    @log_args()
    def filter_localities(self, *args, **kwargs):
        return Locality.objects.filter(**kwargs)

    @log_args()
    def get_single_locality(self, *args, **kwargs):
        return Locality.objects.get(**kwargs)

    @log_args()
    def filter_localities_with_query(self, query):
        return Locality.objects.select_related('city', 'city__state').filter(
            query).exclude(name__istartswith='Test')

    @staticmethod
    def filter_localities_with_locality_name_and_city(locality_name, city):
        return Locality.objects.filter(Q(name__iexact=locality_name) & Q(
            Q(city__name__iexact=city) | Q(city__slug__iexact=city))).first()
