import logging

from django.template import Context

from apps.checkout.service.notification.basenotification import NotificationService
from apps.common import date_time_utils
from common.exceptions.treebo_exception import TreeboException
from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)


class SpecialPrefsEmailService(NotificationService):
    template_name = "email/special_preference.html"

    def __init__(
            self,
            notification_type,
            content_subtype,
            to_address,
            subject,
            data):
        super(
            SpecialPrefsEmailService,
            self).__init__(
            notification_type,
            content_subtype,
            to_address,
            subject,
            data)

    def _setData(self, data):
        NotificationService._setData(self, data)

    def createMessage(self):
        NotificationService._createMessage(self)

    def sendMessage(self):
        NotificationService._sendMessage(self)

    def collateEmailData(self, booking_request):
        """
        :param apps.checkout.models.BookingRequest:
        :return:
        """
        try:
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            hotel = hotel_repository.get_hotel_by_id_from_web(booking_request.hotel_id)

            checkin = date_time_utils.build_date(booking_request.checkin_date)
            checkout = date_time_utils.build_date(
                booking_request.checkout_date)
            specialPreferences = booking_request.comments
            data = Context(
                {"guest_name": booking_request.name, "guest_email": booking_request.email,
                 "guest_phone": booking_request.phone, "hotel_name": hotel.name,
                 "checkin": checkin, "checkout": checkout,
                 "special_pref": specialPreferences})
        except Exception as exc:
            logger.exception("Exception = %s " % exc)
            raise TreeboException(exc)

        self._setData(data)
