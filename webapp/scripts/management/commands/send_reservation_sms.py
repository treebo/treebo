import datetime
import logging
import sys
import traceback
import urllib.request
import urllib.parse
import urllib.error

from django.core.mail import EmailMessage
from django.core.management import BaseCommand
from django.db.models import Q
from django.utils import timezone

from apps.profiles import utils as profile_utils
from common.constants import common_constants as const
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.miscellanous import NotificationArchive
from dbcommon.models.reservation import Reservation

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'send sms to all reservations that start from tomorrow'
    sender_email = 'kulizadev@gmail.com'
    recipients = ['veena.ampabathini@treebohotels.com']
    username = 'kulizadev@gmail.com'
    password = 'kuliza123'

    directions_service = RepositoriesFactory.get_directions_repository()

    def handle(self, *args, **options):
        try:
            self.send_sms()
        except Exception as exc:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            self.send_exception_email(
                "Exception raised in Send Reservation SMS", repr(
                    traceback.format_exception(
                        exc_type, exc_obj, exc_tb)))
            logger.exception(
                "sms_notifications %s" %
                repr(
                    traceback.format_exception(
                        exc_type,
                        exc_obj,
                        exc_tb)))

    def send_sms(self):
        try:
            start_date = timezone.now().date()
            end_date = start_date + datetime.timedelta(days=1)
            sms_notifications = NotificationArchive.objects.filter(
                Q(created_at__range=(start_date, end_date)),
                Q(notification_type=NotificationArchive.SMS),
                Q(delivery_status=NotificationArchive.SUCCESS)
            )
            if sms_notifications is not None:
                logger.debug(
                    "sms_notifications %d" %
                    sms_notifications.__len__())
        except NotificationArchive.DoesNotExist:
            logger.info("There are no sms sent today")
            sms_notifications = None
        except Exception as exc:
            logger.exception("get sms notifications from DB: %s" % str(exc))
            exc_type, exc_obj, exc_tb = sys.exc_info()
            self.build_error("get sms notifications from DB",
                             repr(traceback.format_exception(exc_type, exc_obj,
                                                             exc_tb)))

        try:
            date_min = datetime.datetime.combine(
                datetime.date.today() +
                datetime.timedelta(
                    days=1),
                datetime.time.min)
            date_max = datetime.datetime.combine(
                datetime.date.today() +
                datetime.timedelta(
                    days=1),
                datetime.time.max)
            reservations = Reservation.objects.filter(
                Q(reservation_checkin_date__range=(date_min, date_max)),
                Q(reservation_status='Reserved')).distinct("guest_mobile",
                                                           "hotel")
            if reservations is not None:
                logger.debug("Reservation %d" % reservations.__len__())
        except Reservation.DoesNotExit:
            logger.info("No reservations found. Exiting")
            exit()
        except Exception as exc:
            logger.exception(exc)
            exc_type, exc_value, exc_tb = sys.exc_info()
            self.build_error("get reservations from DB",
                             repr(traceback.format_exception(exc_type, exc_obj,
                                                             exc_tb)))
            exit()

        new_reservations = list(reservations)
        if sms_notifications is not None and sms_notifications.__len__() != 0:
            # remove the the guest entries from reservations for whom the sms's
            # have been sent
            for reservation in reservations:
                my_list = [x for x in sms_notifications if x.guest_mobile ==
                           reservation.guest_mobile and x.hotel == reservation.hotel]
                if my_list is not None:
                    new_reservations.remove(reservation)
        logger.info("new_reservations %d" % new_reservations.__len__())

        # now we have a list of valid mobile numbers to send sms
        for candidate in new_reservations:

            try:
                name = candidate.guest_name.split(" ")[0]
                location_to = candidate.hotel.name
                maps_link = const.GOOGLE_MAPS_LINK + \
                    str(candidate.hotel.latitude) + "," + str(candidate.hotel.longitude)
                direction_text = self.get_directions(candidate.hotel.id)
                direction_text = direction_text.replace("\\n", ", ")
                sms_msg = self.build_sms_message(
                    name, location_to, direction_text, maps_link)
                logger.debug("sms_msg %s" % candidate.guest_mobile)
                profile_utils.sendMessage(
                    candidate.guest_mobile, sms_msg, const.SEND_SMS)
                self.archive_notification_record(candidate, None)
            except Exception as exc:
                logger.exception("send %s" % str(exc))
                exc_type, exc_value, exc_tb = sys.exc_info()
                self.archive_notification_record(candidate, repr(
                    traceback.format_exception(exc_type, exc_value,
                                               exc_tb)))

    def build_sms_message(self, name, loc, dir_text, link):
        sms_message = const.NEXT_DAY_RESERVATION_SMS_MESSAGE % (
            name, loc, dir_text, link)
        return urllib.parse.quote_plus(sms_message)

    def get_directions(self, hotel_id):
        directions = self.directions_service.get_directions_ordered_by_landmark_popularity(
            hotel_id=hotel_id)
        direction = directions[0]
        # direction = Directions.objects.filter(hotel__id=hotel_id).order_by(
        #     "-landmark_obj__popularity")[:1].get()
        logger.debug(direction.directions_for_sms)
        return direction.directions_for_sms

    def send_exception_email(self, subject, content):
        # send mail
        # subject = "Exception raised in Send Reservation SMS"
        receiver = Command.recipients
        sender = Command.sender_email
        msg = EmailMessage(subject, content, sender, receiver)
        msg.send()

    #
    def build_error(self, method, exception_text):
        logger.debug(exception_text)
        self.sendExceptionEmail(method, exception_text)

    def archive_notification_record(self, reservation, exception_text):
        archive_record = NotificationArchive()
        archive_record.notification_type = NotificationArchive.SMS
        archive_record.hotel = reservation.hotel
        archive_record.guest_name = reservation.guest_name
        archive_record.guest_mobile = reservation.guest_mobile
        if exception_text is not None:
            archive_record.delivery_status = NotificationArchive.FAILURE
            archive_record.error_text = exception_text
        archive_record.save()
