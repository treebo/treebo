

import logging

import datetime
from lxml import etree as et
from lxml import objectify

from apps.common.file_utils import FileUtils

logger = logging.getLogger(__name__)


class TransactionRequestService(object):
    def generate_transaction_request(self, hotel_id):
        try:

            root = et.Element('Query')
            checkin = et.SubElement(root, 'Checkin')
            checkin_date = datetime.datetime.now().date() + datetime.timedelta(10)
            checkin_date = checkin_date.isoformat()
            checkin.text = checkin_date
            nights = et.SubElement(root, 'Nights')
            nights.text = "2"
            property_list = et.SubElement(root, 'PropertyList')
            property = et.SubElement(property_list, 'Property')
            property.text = str(hotel_id)
            objectify.deannotate(root, xsi_nil=True, cleanup_namespaces=True)
            transaction_request = et.ElementTree(root)
            response = FileUtils.generate_xml_file(
                transaction_request, 'transaction_request.xml')
            return response
        except Exception as e:
            logger.exception(
                "Exception in Generating Transaction request Message")
            raise e
