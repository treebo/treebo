import logging

from django.conf import settings

from rest_framework import serializers
from dbcommon.models.miscellanous import Image
import urllib.parse

logger = logging.getLogger(__name__)

__author__ = 'Rajdeep'


class ImageSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()
    main = serializers.SerializerMethodField()

    class Meta:
        model = Image
        fields = ('id', 'url', 'is_showcased', 'tagline', 'main')

    def get_url(self, obj):
        if not obj.url.url:
            return ''

        url = str(obj.url.url)
        return urllib.parse.unquote(url)

    def get_main(self, obj):
        if not obj.url.url:
            return ''

        url = str(obj.url.url)
        return urllib.parse.unquote(url)



# class ImageSerializer(serializers.ModelSerializer):
#    thumbnail = serializers.SerializerMethodField('getImageThumbnail')
#    main = serializers.CharField(source='url')
#    #room_type = serializers.SerializerMethodField('roomTypeOrOtherTypeImage')
#
#
#    def getImageThumbnail(self, image_object):
#        return ""
#
#    def roomTypeOrOtherTypeImage(self, image_object):
#        if image_object.room_id is not None:
#            try:
#                room_object = Room.objects.get(pk=image_object.room_id)
#                return room_object.room_type_code
#            except Exception as exc:
#                logger.debug("Not a valid Image Object or No Images Present in the Hotel")
#                #return "Not a valid Image Object or No Images Present in the Hotel"
#        else:
#            return "Other"
#
#    class Meta:
#        model = Image
#        fields=('thumbnail', 'main')
