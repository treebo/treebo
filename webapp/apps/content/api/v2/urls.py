# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.content.api.v2.content import ContentAPI

urlpatterns = [
    url(r'^$', ContentAPI.as_view(), name='content'),
]
