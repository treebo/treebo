import logging
from decimal import Decimal

from apps.common import metrics_logger
from apps.common.utils import round_to_two_decimal
from common.constants import common_constants
from dbcommon.models.profile import UserMetaData
from services.hotellogix.exceptions import AddToCartException, SaveBookingException, ConfirmBookingException
from services.hotellogix.hx_client import HxClient
from services.hotellogix.web_services import Hotelogix

hx_api = HxClient()

logger = logging.getLogger(__name__)


class HxService:
    @staticmethod
    def get_auth_tokens(user, consumer_key, consumer_secret):
        hx_keys = user.get_hx_keys()
        if not hx_keys:
            accesskey, accesssecret = HxService.renew_auth_token(
                consumer_key, consumer_secret)
            access_key_meta = UserMetaData.objects.create(
                key='accesskey', value=accesskey, user_id=user)
            access_secret_meta = UserMetaData.objects.create(
                key='accesssecret', value=accesssecret, user_id=user)
            hx_keys = {
                'accesskey': access_key_meta,
                'accesssecret': access_secret_meta}
        logger.debug("HX Access Keys for user: %s = %s", user.id, hx_keys)
        return hx_keys

    @staticmethod
    def renew_auth_token(consumer_key, consumer_secret):
        accesskey, accesssecret = hx_api.renewAuthToken(
            consumer_key, consumer_secret)
        return accesskey, accesssecret

    @staticmethod
    def get_new_auth_tokens(consumer_key, consumer_secret):
        accesskey, accesssecret = HxService.renew_auth_token(
            consumer_key, consumer_secret)
        hx_keys = {'accesskey': accesskey, 'accesssecret': accesssecret}
        return hx_keys

    @staticmethod
    def get_rate_ids_from_hx(
            checkin_date,
            checkout_date,
            hotelogix_id,
            room_type_code,
            hx_keys,
            consumer_key,
            consumer_secret):
        logger.info("HX API call: rateidsforadtocart")
        try:
            request_xml = Hotelogix.build_get_rate_id_xml_for_all_roomtype(
                checkin_date, checkout_date, hotelogix_id)
            response_xml = hx_api._callHotelogix(
                'rateidsforadtocart',
                request_xml,
                hx_keys,
                consumer_key,
                consumer_secret)
            logger.info("rateidsforadtocart API success")
        except BaseException:
            logger.exception(
                "Exception occurred while getting rate_ids from hotelogix")
            response_xml = common_constants.EMPTY_XML
        return response_xml

    @staticmethod
    def loadCartHotelogix(hx_keys, consumer_key, consumer_secret):
        xml = Hotelogix.buildLoadCart()
        response_xml = hx_api._callHotelogix(
            "loadcart", xml, hx_keys, consumer_key, consumer_secret)
        if response_xml('rate').text() is None:
            raise Exception("Rate tag missing in loadcart HX API Response.")
        return response_xml

    @staticmethod
    def deleteFromCart(booking_xml, hx_keys, consumer_key, consumer_secret):
        logger.debug("Deleting old Data from cart")
        item_id = booking_xml.attr("id")
        delete_xml = Hotelogix.buildDeleteCart(item_id)
        hx_api._callHotelogix(
            "deletefromcart",
            delete_xml,
            hx_keys,
            consumer_key,
            consumer_secret)

    @staticmethod
    def addToCartHotelId(
            rate_id_lists,
            hx_keys,
            consumer_key,
            consumer_secret):
        logger.info("HX API call: addtocart")
        rate_id_snippet = ""
        logger.debug("rate id list %s" % rate_id_lists)
        for rate_id in rate_id_lists:
            logger.debug("rate id %s" % rate_id)
            rate_id_snippet = rate_id_snippet + \
                Hotelogix.buildAddToCartItemSnippet(rate_id)
            logger.debug("rate id snippet %s" % rate_id_snippet)
        xml = Hotelogix.buildAddToCart(rate_id_snippet)
        xmlResponse = hx_api._callHotelogix(
            "addtocart", xml, hx_keys, consumer_key, consumer_secret)
        status = xmlResponse('status').attr('message')
        if status != common_constants.SUCCESS:
            raise AddToCartException(
                "addtocart HX API failed. Response Xml: {0}".format(xmlResponse))
        elif not xmlResponse("rate"):
            raise AddToCartException(
                "addtocart HX API failed. Rate not found. Response Xml: {0}".format(xmlResponse))
        return xmlResponse

    @staticmethod
    def modifyDiscountAmount(booking, cart_xml, hx_keys):
        """

        :param cart_xml:
        :param hx_keys:
        """
        logger.info("HX API call: modifyamount")
        status = common_constants.SUCCESS
        couponCode = booking.coupon_code
        preferredPromo = booking.mm_promo
        if couponCode or preferredPromo:
            logger.info(
                "Pretax Price in booking_request: %s, Discount Value: %s",
                booking.pretax_amount,
                booking.discount)
            totalPrice = booking.pretax_amount - booking.discount
            metrics_logger.log("Total Price {price}", {"price": totalPrice})
            promo_xml = Hotelogix.buildPromoAmountXml(cart_xml, totalPrice)
            xml = hx_api._callHotelogix("modifyamount",
                                        promo_xml,
                                        hx_keys,
                                        booking.hotel.get_consumer_key(),
                                        booking.hotel.get_consumer_secret())
            status = xml("status").attr("message")
            if status != common_constants.SUCCESS:
                raise Exception(
                    "modifyamount HX API failure response received. Response Xml: {0}".format(xml))
            logger.info("modifyamount success")
        else:
            logger.info("No promo or coupon_code applied for modifyamount")

    @staticmethod
    def saveBookingHotelogix(
            add_to_cart_hotel_id,
            order_id,
            comments,
            email,
            name,
            phone,
            hx_keys,
            consumer_key,
            consumer_secret):
        logger.info("HX API call: savebooking")
        # hx_api._setAccessKeys(request)
        xml = Hotelogix.build_save_booking(
            order_id, name, email, phone, comments, add_to_cart_hotel_id)
        responseXml = hx_api._callHotelogix(
            "savebooking", xml, hx_keys, consumer_key, consumer_secret)
        status = responseXml('status')
        # Throws exception only when status is not success and the error is not
        # "Custom order code is already exist!"
        if status.attr("message") != common_constants.SUCCESS and status.attr(
                'code') != '1616':
            logger.error(
                "savebooking HX API returned failure response. Could not save the booking on hotelogix")

            raise SaveBookingException(
                "savebooking HX API failed. Response XML: \n{0}".format(responseXml))
        HxService.get_order_amount_from_savebooking_xml(responseXml)
        return responseXml

    @staticmethod
    def get_confirm_booking_amount(
            booking,
            is_pay_at_hotel,
            order_amount_from_hx):
        if not is_pay_at_hotel:
            # Changing this to total_amount, because payment_amount is
            # total_amount rounded to integer, which can't be passed to HX
            amount = float(booking.total_amount)  # + float(booking.discount)
            assumedPaidAmount = float(booking.voucher_amount)
            amount = amount + assumedPaidAmount
        elif booking.is_audit:
            amount = float(booking.total_amount)
        else:
            # If its pay at hotel, changing this to 0 assuming no amount is paid
            # For FOT redemption coupon, payment_amount is set to voucher_amount, in BookingClient.__get_booking_data() method
            # For all other Pay At Hotel bookings, payment_amount will be 0
            amount = booking.payment_amount
        booking_amount_rounded = round_to_two_decimal(amount)
        order_amount_from_hx_rounded = round_to_two_decimal(
            order_amount_from_hx)
        amount_diff = (
            booking_amount_rounded -
            order_amount_from_hx_rounded).copy_abs()
        if amount_diff == Decimal('0.01'):
            return order_amount_from_hx_rounded
        elif amount_diff != Decimal('0.00'):
            logger.error(
                "Pricing difference between savebooking order_amount and booking total_amount for order_id: %s = %s",
                booking.order_id,
                amount_diff)
            if booking.is_audit:
                return order_amount_from_hx_rounded
            return booking_amount_rounded
        else:
            return booking_amount_rounded

    @staticmethod
    def confirm_booking_on_hotelogix(
            booking,
            is_post_paid,
            hx_keys,
            consumer_key,
            consumer_secret,
            retry_count=1):
        """
        :param consumer_secret:
        :param consumer_key:
        :param apps.bookings.models.Booking booking:
        :param is_post_paid:
        :param hx_keys:
        :param retry_count:
        :return:
        """
        logger.info("HX API call: confirmbooking")
        paid_source = "Web (pay@hotel)" if is_post_paid else "Web (prepaid)"
        response_xml = HxService.get_order_from_hotelogix(
            booking.order_id, hx_keys, consumer_key, consumer_secret)
        order_amount_from_hx = HxService.get_order_amount_from_savebooking_xml(
            response_xml)
        amount = HxService.get_confirm_booking_amount(
            booking, is_post_paid, order_amount_from_hx)
        xml = Hotelogix.buildConfirmBooking(
            amount, booking.order_id, paid_source)
        responseXml = hx_api._callHotelogix(
            "confirmbooking", xml, hx_keys, consumer_key, consumer_secret)
        status = responseXml('status')
        response_code = status.attr('code')
        if status.attr('message') != common_constants.SUCCESS and response_code not in [
                "1604", "1600"]:
            if retry_count == 1:
                retry_count -= 1
                return HxService.confirm_booking_on_hotelogix(
                    booking, is_post_paid, hx_keys, consumer_key, consumer_secret, retry_count)
            else:
                logger.error(
                    "confirmbooking on HX failed for order_id: %s. Reason: %s",
                    booking.order_id,
                    status.attr('message'))
                raise ConfirmBookingException(
                    "confirmbooking HX API failed. Response XML: \n{0}".format(responseXml))
        logger.info("confirmbooking success")
        return responseXml

    @staticmethod
    def get_order_from_hotelogix(
            order_id,
            hx_keys,
            consumer_key,
            consumer_secret):
        get_order_xml = Hotelogix.buildGetOrder(order_id)
        response_xml = hx_api._callHotelogix(
            'getorder', get_order_xml, hx_keys, consumer_key, consumer_secret)
        return response_xml

    @staticmethod
    def cancel_reservation(
            order_id,
            reservation_id,
            hx_keys,
            consumer_key,
            consumer_secret):
        logger.info("Cancelling room with reservation ID: %s" % reservation_id)
        xml = Hotelogix.buildcancelBooking(order_id, reservation_id)
        responseXml = hx_api._callHotelogix(
            "cancel", xml, hx_keys, consumer_key, consumer_secret)
        return responseXml

    @staticmethod
    def get_order_amount_from_savebooking_xml(savebooking_xml):
        try:
            response = savebooking_xml('response')
            order = response('order')
            orderamount = order('orderamount')
            amount = orderamount.attr('amount')
            logger.info(
                "get_order_amount_from_savebooking_xml : amount " +
                str(amount))
        except Exception as e:
            logger.error(
                "parse_savebooking_response_get_booking_price: error fetching amount in xml %s " %
                str(e))
            amount = 0
        return amount
