import logging
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.reset_password_dto import ResetPasswordDTO
from apps.auth.serializer.user_serializer import UserSerializer
from apps.auth.services.password_service import PasswordService
from apps.common.api_response import APIResponse as api_response
from apps.profiles.service.user_service import UserService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes

logger = logging.getLogger(__name__)


class ResetPasswordApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    reset_password_service = PasswordService()
    user_service = UserService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            reset_password_dto = ResetPasswordDTO(data=request.data)
            if not reset_password_dto.is_valid():
                logger.error("reset password invalid request ")
                return api_response.invalid_request_error_response(
                    reset_password_dto.errors, ResetPasswordApi.__name__)
            user, token = self.reset_password_service.reset_password(
                reset_password_dto.data)
            self.user_service.login_user(request, user)
            user_dto = UserSerializer(instance=user)
            user_data = user_dto.data
            user_data['token'] = token
            response = api_response.prep_success_response(user_data)
        except TreeboException as e:
            logger.exception("treebo reset password exception ")
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception("internal error reset password exception ")
            response = api_response.internal_error_response(error_codes.get(
                error_codes.PASSWORD_NOT_CHANGED)['msg'], ResetPasswordApi.__name__)
        return response
