import _ from 'underscore';
import React from 'react';
import classNames from 'classnames';
import $ from 'jquery';
var moment = require('moment');

var PriceDetail = React.createClass({
    render(){
        var tbody = [],
            roomPrice = 0,
            taxPrice = 0,
            totalCost = 0,
            inDate = this.props.data.nights[0].date,
            outDate = this.props.data.nights[this.props.data.nights.length - 1].date,

            chkInDate = new Date(inDate),
            chkOutDate = new Date(outDate),
            timeDifference = Math.abs(chkInDate.getTime() - chkOutDate.getTime()),
            stayDays = Math.ceil(timeDifference / (1000 * 3600 * 24)) + 1,
            stayStr = stayDays > 1 ? moment(chkInDate).format("DD MMM'YY") + " - " + moment(chkOutDate).format("DD MMM'YY")+" (" + stayDays + " Nights)" : moment(chkInDate).format("DD MMM'YY") +" (" +stayDays + " Night)";


            var hotelNights = this.props.data;
            _.each(hotelNights.nights,function(night,i){
                roomPrice = roomPrice + night.base_price;
                taxPrice = taxPrice + night.tax;
                totalCost = totalCost + night.final_price;

                var dateFormat = moment(night.date).format("DD MMM'YY");

                tbody.push(<tr className="grayTxt" key={dateFormat}>
                                    <td>{dateFormat}</td>
                                    <td><i className="icon-inr mr5"></i>{night.base_price}</td>
                                    <td><i className="icon-inr mr5"></i>{night.tax}</td>
                                    <td><i className="icon-inr mr5"></i>{night.final_price}</td>
                                </tr>);
            })

            roomPrice = roomPrice.toFixed(2);
            taxPrice = taxPrice.toFixed(2);
            totalCost = Math.round(totalCost);

        // onClick added icon-cross to call react click event later when page is loaded as JS is already loaded
        return (
                <div>
                    <div className="modal__header">
                        <span>DAILY PRICE BREAK UP</span>
                        <div className="hand icon icon-close pos-abs js-modal__close"><i className="icon-cross"></i></div>
                    </div>
                    <div className="modal__body price-popup-modal-body pos-rel">
                        <div className="text-center mb30 price-detail-header grayTxt price-detail-days">
                            {stayStr}
                        </div>
                        <div className="price-table-scroll">
                            <table className="price-detail-table mb30">
                                <thead>
                                <tr className="mb20">
                                    <th>DATE</th>
                                    <th>BASE PRICE</th>
                                    <th>TAX</th>
                                    <th>FINAL PRICE</th>
                                </tr>
                                </thead>
                                <tbody>
                                {tbody}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="modal__footer done-button-block">
                        <div className="flex-row price-detail-footer">
                            <div className="price-detail-footer__room text-center price-detail__item">
                                <p className="price-detail-footer__item">ROOM PRICE</p>
                                <p className="grayTxt"><i className="icon-inr mr5"></i> <span className="price-detail-footer__amount">{roomPrice} </span></p>
                            </div>

                            <div className="price-detail-footer__tax text-center price-detail__item">
                                <p className="price-detail-footer__item">TAX</p>
                                <p className="grayTxt"><i className="icon-inr mr5"></i> <span className="price-detail-footer__amount">{taxPrice} </span></p>
                            </div>

                            <div className="price-detail-footer__cost text-center price-detail__item">
                                <p className="price-detail-footer__item">TOTAL COST</p>
                                <p className="price-detail-footer__total"><i className="icon-inr mr5"></i><span className="price-detail-footer__total-cost"> {totalCost} </span></p>
                                <p className="grayTxt price-detail-footer__inclusive">(inclusive all of taxes) </p>
                            </div>

                        </div>
                        <input type="submit" id="doneDetail" className="btn price-detail-footer__done js-modal__close" value="Done"/>
                    </div>
                </div>
            )
    }
});


module.exports = PriceDetail;
