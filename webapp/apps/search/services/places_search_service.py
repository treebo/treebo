from django.conf import settings
from django.core.cache import cache
from googleplaces import GooglePlaces


class PlacesSearchService:
    google_places = GooglePlaces(settings.GOOGLE_API_SERVER_KEY)
    place_cache_timeout = 30 * 24 * 60 * 60

    def get_place_location_details(self, place_id, session_token):
        place_location_details = cache.get('PLACE-ID-' + place_id) or cache.get(place_id)
        if not place_location_details:
            place = self.google_places.get_place(place_id, session_token=session_token)
            place_location_details = dict(geo_location=place.geo_location,
                                          address_components=place.details.get('address_components'))
            cache.set('PLACE-ID-' + place_id, place_location_details, self.place_cache_timeout)
            return place_location_details
        return place_location_details
