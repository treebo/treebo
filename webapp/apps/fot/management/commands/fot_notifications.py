
from django.core.management.base import BaseCommand
from apps.fot.models import Fot, FotReservationsRequest, FotHotel
from datetime import timedelta
import datetime
import logging
import requests
from apps.fot import emailers as fot_emailers
from django.db import transaction
from django.conf import settings
from django.db.models import F

logger = logging.getLogger(__name__)

BLOCK_FOT_USER_ON_NO_SHOW = False
BLOCK_FOT_USER_ON_FEEDBACK_NOT_SUBMIT = False


class Command(BaseCommand):
    args = 'Arguments is needed. Available are audit, checkout, block, noshow'
    help = 'Django custom management command for FOT notifications.'

    def add_arguments(self, parser):
        parser.add_argument('my_argument', type=str)

    def handle(self, *args, **options):
        if options['my_argument'] == 'audit':
            self.stdout.write(
                "Sending audit reminder mails.Please wait for completion...")
            status = fot_emailers.audit_reminder()
            self.stdout.write(status)
        elif options['my_argument'] == 'checkout':
            self.stdout.write(
                "Sending checkout reminder mails.Please wait for completion...")
            status = fot_emailers.checkout_reminder()
            self.stdout.write(status)
        elif options['my_argument'] == 'block':
            self.stdout.write(
                "Blocking accounts and sending blocked mails.Please wait for completion...")
            status = self.feedback_status_updater()
            self.stdout.write(status)
        elif options['my_argument'] == 'noshow':
            self.stdout.write(
                "Updating no shows in the database. Please wait for completion...")
            status = self.no_show_updater()
            self.stdout.write(status)
        elif options['my_argument'] == 'feedback':
            self.stdout.write(
                "Checking feedbacks in the database. Please wait for completion...")
            status = self.feedback_status_updater()
            self.stdout.write(status)
        elif options['my_argument'] == 'fotactivate':
            self.stdout.write(
                "Updating statuses in the database. Please wait for completion...")
            status = self.fot_user_status_updater()
            self.stdout.write(status)
        else:
            self.stdout.write("Please enter atleast one valid argument")
            self.stdout.write(
                "Arguments is needed. Available are audit, checkout, block, noshow")
            self.stdout.write(
                "For example: " +
                '\033[1m' +
                "python manage.py fot_notifications audit" +
                '\033[0m')  # \033[1m is for bold and \033[0m is for normal text in console

    def no_show_updater(self):
        today = datetime.date.today()
        yesterday = today - timedelta(days=1)
        reservations = FotReservationsRequest.objects.filter(
            status__in=[
                'PENDING',
                'SCHEDULED'],
            check_in_date=yesterday).values_list(
            'id',
            flat=True)
        reservationsList = [x for x in reservations]
        reservations = FotReservationsRequest.objects.filter(
            id__in=reservationsList)
        usersList = reservations.values_list('user_id', flat=True)
        fotUser = Fot.objects.filter(user_id__in=usersList)
        try:
            with transaction.atomic():
                reservations.update(status='NOSHOW')
                if BLOCK_FOT_USER_ON_NO_SHOW:
                    fotUser.update(
                        status='BLOCKED',
                        no_shows=F('no_shows') + 1)
            if BLOCK_FOT_USER_ON_NO_SHOW:
                status = fot_emailers.account_blocking_confirmation(
                    reservations, yesterday)
                logger.info(status)
            return "No shows updated successfully"
        except BaseException:
            logger.exception('No show updater failed')
            return "Updating noshows in database failed"

    def feedback_status_updater(self):
        try:
            reservationStatusList = [
                FotReservationsRequest.CHECKEDIN,
                FotReservationsRequest.CHECKEDOUT,
                FotReservationsRequest.FEEDBACK_NOT_SUBMITTED]
            firstDay = datetime.date.today()
            # changed to 8 days on Amit's request
            lastDay = firstDay - timedelta(days=8)
            reservations = FotReservationsRequest.objects.filter(
                status__in=reservationStatusList,
                check_out_date__range=(
                    lastDay,
                    firstDay)).values(
                'id',
                'hotel__hotelogix_id',
                'check_in_date',
                'user__email',
                'user_id',
                'status',
                'check_out_date',
                'user__phone_number')
            userBlockedIdList = []
            feedbackSubmittedList = []
            feedbackNotSubmittedList = []
            reservationsToSendBlockReminder = []
            for reservation in reservations:
                logger.info(reservation)
                reservationId = reservation['id']
                userId = reservation['user_id']
                email = str(reservation['user__email']).lower()
                checkInDate = reservation['check_in_date']
                checkOutDate = reservation['check_out_date']
                hoteLogixId = reservation['hotel__hotelogix_id']
                phoneNumber = reservation.get('user__phone_number')
                status = reservation.get('status', ' ')
                if not phoneNumber:
                    phoneNumber = ' '
                queryUrl = '?hotelid={0}&email={1}&date={2}&number={3}'
                hitUrl = str(settings.PROWL_FOT_STATUS_UPDATE_URL) + \
                    queryUrl.format(hoteLogixId, email, checkInDate, phoneNumber)
                logger.info(hitUrl)
                response = requests.get(hitUrl)
                responseJson = (response.json()).get('data')
                if responseJson:
                    if responseJson['fot_submissions'] == 'Submission found':
                        feedbackSubmittedList.append(reservationId)
                    elif responseJson['fot_submissions'] == 'No submission exists for given criteria':
                        feedbackNotSubmittedList.append(reservationId)
                        if str(
                                status) == FotReservationsRequest.FEEDBACK_NOT_SUBMITTED and checkOutDate == lastDay:
                            userBlockedIdList.append(userId)
                            reservationsToSendBlockReminder.append(
                                reservationId)
                    else:
                        return 'Some error occured in prowl please check'
                else:
                    logger.error(
                        'Wrong parameters sent for prowl: %s' %
                        str(hitUrl))

            FotReservationsRequest.objects.filter(
                id__in=feedbackNotSubmittedList).update(
                status=FotReservationsRequest.FEEDBACK_NOT_SUBMITTED)
            fotUsersFeedbackNotSubmitted = Fot.objects.filter(
                user_id__in=userBlockedIdList)
            fotUsersFeedbackNotSubmitted.update(
                feedback_dropped_out=F('feedback_dropped_out') + 1)

            if BLOCK_FOT_USER_ON_FEEDBACK_NOT_SUBMIT:
                fotUsersFeedbackNotSubmitted.update(status='BLOCKED')
                reservationsToSendBlockReminder = FotReservationsRequest.objects.filter(
                    id__in=reservationsToSendBlockReminder)
                try:
                    status = fot_emailers.account_blocking_confirmation(
                        reservationsToSendBlockReminder, lastDay)
                    logger.info(status)
                except BaseException:
                    logger.exception('Sending blocked email failed')

            with transaction.atomic():
                reservationObjs = FotReservationsRequest.objects.filter(
                    id__in=feedbackSubmittedList)
                reservationObjs.update(
                    status=FotReservationsRequest.FEEDBACK_SUBMITTED)
                for reservationObj in reservationObjs:
                    user = Fot.objects.get(user_id=reservationObj.user_id)
                    hotelCredits = FotHotel.objects.get(
                        hotel_id=reservationObj.hotel_id).credits_per_audit
                    user.user_credits = F('user_credits') + hotelCredits
                    user.free_nights_remaining = F('free_nights_remaining') + 1
                    user.total_free_nights = F('total_free_nights') + 1
                    user.feedbacks_submitted = F('feedbacks_submitted') + 1
                    user.save()
            try:
                status = fot_emailers.feedback_submission_confirmation(
                    reservationObjs)
                logger.info(status)
            except BaseException:
                logger.exception('Sending feedback submission mail failed')

            return 'Succedded in updating statuses of %d' % len(reservations)

        except Exception as e:
            logger.exception('Feedback status updater failed')
            return "Failed updating feedback statuses due to %s" % str(e)

    @staticmethod
    def fot_user_status_updater():
        try:
            today = datetime.date.today()
            threeMonthsDelta = today - timedelta(days=1)
            reservationsUsersList = FotReservationsRequest.objects.filter(
                created_at__gte=threeMonthsDelta).values_list('user_id', flat=True)
            Fot.objects.filter(
                user_id__in=reservationsUsersList).update(
                is_active=True)
            Fot.objects.exclude(
                user_id__in=reservationsUsersList).update(
                is_active=False)
            return 'Updating done'
        except BaseException:
            logger.exception('user status updater failed')
