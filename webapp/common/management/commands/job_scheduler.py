import logging
import time

from django.core.management.base import BaseCommand

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, *args, **options):
        failures = 0
        sleep_time = 2
        while True:
            try:
                from webapp.common.job.job_service import JobService
                jobs_found = JobService().schedule_oldest_unscheduled_job_for_execution()
                success = True
                if not jobs_found:
                    time.sleep(1)
                    continue
            except Exception as ex:
                logger.exception('Error occurred while scheduling job')
                success = False

            if not success:
                failures += 1
            else:
                failures = 0
                sleep_time = 2

            if failures > 3:
                logger.critical('Job publishing failed for the %s th time', failures)
                if sleep_time < 64:
                    sleep_time = sleep_time * 2
                time.sleep(sleep_time)
