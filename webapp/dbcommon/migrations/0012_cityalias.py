# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0011_auto_20160225_0937'),
    ]

    operations = [
        migrations.CreateModel(
            name='CityAlias',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('alias', models.CharField(max_length=100)),
                ('city', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'hotels_cityalias',
            },
        ),
    ]
