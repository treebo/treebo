import logging
from datetime import datetime

from bs4 import BeautifulSoup
from django.http import HttpResponse
from rest_framework.views import APIView

from apps.pricing import channel_manager_utils
from apps.pricing import parsers
from apps.pricing import tasks
from base import log_args
from base.middlewares.set_request_id import get_current_user, get_current_request_id

logger = logging.getLogger(__name__)


class GetPromotions(APIView):
    parser_classes = (parsers.XMLParser,)

    def post(self, request, format=None):
        logger.info(request.data)
        requestXml = BeautifulSoup(request.data, 'xml')
        hotelId = requestXml.find('Hotel')['id']
        maximojo_response = channel_manager_utils.getPromotionsXmlResponse(
            int(hotelId))
        return HttpResponse(maximojo_response, content_type="application/xml")


class SetPromotions(APIView):
    parser_classes = (parsers.XMLParser,)

    def post(self, request, format=None):
        logger.info(request.data)
        # Try to move this to a celery task
        maximojo_response = channel_manager_utils.updatePreferredPromoFromChannelManager(
            request.data)
        return HttpResponse(maximojo_response, content_type="application/xml")


class DeletePromotions(APIView):
    parser_classes = (parsers.XMLParser,)

    @log_args(logger)
    def post(self, request, format=None):
        logger.info(request.data)
        # channel_manager_utils.updateRatePlanFromChannelManager(request.DATA)
        channel_manager_utils.deletePromotions(request.data)
        timestampString = datetime.now().strftime(
            '%Y-%m-%dT%H:%M:%S.%f')[:-3] + "Z"
        maximojo_response = '<DeletePromotionsResponse TimeStamp="' + \
            timestampString + '" ><Success/></DeletePromotionsResponse>'
        return HttpResponse(maximojo_response, content_type="application/xml")


class UpdateRateAmount(APIView):
    parser_classes = (parsers.XMLParser,)

    @log_args(logger)
    def post(self, request, format=None):
        tasks.updateRatePlanFromChannelManager.delay(
            request.data,
            request_id=get_current_request_id(),
            user_id=get_current_user())
        timestampString = datetime.now().strftime(
            '%Y-%m-%dT%H:%M:%S.%f')[:-3] + "Z"
        maximojo_response = '<OTA_HotelRateAmountNotifRS TimeStamp="' + timestampString + \
            '" Version="0" xmlns="http://www.opentravel.org/OTA/2003/05"><Success/></OTA_HotelRateAmountNotifRS>'
        return HttpResponse(maximojo_response, content_type="application/xml")


class UpdateHotelAvailability(APIView):
    parser_classes = (parsers.XMLParser,)

    def post(self, request, format=None):
        logger.info(request.data)
        maximojo_response = '<OTA_HotelAvailNotifRS  TimeStamp="2014-10-20T08:45:13.3564139Z" Version="0" xmlns="http://www.opentravel.org/OTA/2003/05"><Success/></OTA_HotelAvailNotifRS>'
        return HttpResponse(maximojo_response, content_type="application/xml")


class GetHotelRatePlan(APIView):
    parser_classes = (parsers.XMLParser,)

    def post(self, request, format=None):
        logger.debug(request.data)
        hotelId, echoToken = channel_manager_utils.getHotelCodeAndEchoFromHotelRatePlanRequest(
            request.data)
        maximojo_response = channel_manager_utils.getHotelRatePlanResponse(
            hotelId, echoToken)
        logger.debug(maximojo_response)
        return HttpResponse(maximojo_response, content_type="application/xml")
