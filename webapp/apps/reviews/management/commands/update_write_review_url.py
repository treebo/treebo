import logging

from django.core.management import BaseCommand

from apps.reviews.models import TAHotelIDMapping, HotelTAOverallRatings
from apps.reviews.services.trip_advisor.hotel_overall_rating_service import HotelRatingService
from common.constants import common_constants as const

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        logger.info("Starting to load the Write review url for all the hotels")
        hotel_rating = HotelRatingService()
        try:
            ta_hotel_mapping = TAHotelIDMapping.objects.filter(
                hotel__status=const.ENABLED).order_by('hotel__id')
            for mapping in ta_hotel_mapping:
                logger.info(
                    "updating in TAHotelIDMapping and HotelTAOverallRatings table for hotel : %s",
                    mapping.hotel.id)
                mapping.write_review_url = hotel_rating.generate_iframe_url(
                    mapping.hotel.id, None, None)
                mapping.save()
                overall_rating = HotelTAOverallRatings.objects.filter(
                    hotel_ta_mapping=mapping).first()
                if overall_rating:
                    overall_rating.write_review_url = mapping.write_review_url
                    overall_rating.save()
        except BaseException:
            logger.exception(
                'exception occured in Write review url for all the hotels')
        logger.info('Loading Complete For Write review url for all the hotels')
