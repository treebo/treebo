import logging
from http import HTTPStatus
from django.conf import settings
from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from web_sku.common.custom_exception import CouponNotAppliedException
from apps.checkout.service.itinerary_handler_v2 import ItineraryHandlerV2
from base.decorator.request_validator import validate_request
from base.views.api_v2 import TreeboAPIView2
from apps.common.api_response import APIResponse as api_response
from apps.payments.offers.services.payment_offer import PaymentOfferService

logger = logging.getLogger(__name__)


class ApplyCouponSerializer(serializers.Serializer):
    bid = serializers.IntegerField()
    payment_method_id = serializers.IntegerField(allow_null=True)


class ApplyPaymentOffer(TreeboAPIView2):
    authentication_classes = (BasicAuthentication, CsrfExemptSessionAuthentication)

    @validate_request(ApplyCouponSerializer)
    def post(self, request, *args, **kwargs):
        """
        Gets the discount for a booking id and coupon code.
        couponcode -- Coupon Code
        bid -- BookingRequest Id
        """

        try:
            validated_data = kwargs['validated_data']
            bid, payment_method_id,  = validated_data['bid'], \
                                                    validated_data['payment_method_id']

            price_data = PaymentOfferService().apply_payment_offer(
                request, bid, payment_method_id)
            price_data['coupon_desc'] = ""
            price_data['is_prepaid'] = ""
            response = api_response.prep_success_response(price_data)
        except CouponNotAppliedException:
            response = api_response.error_response_from_error_message(
                error_message="Coupon code not applicable. Please try from any of the other visible"
                              " coupon codes.", status_code=400)
        except Exception as exception:
            logger.exception('Exception occurred in apply coupon')
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param=request.GET,
                                                        dev_msg="Apply coupon",
                                                        message=exception.__str__(),
                                                        class_name=self.__class__.__name__)
            response = api_response.error_response_from_error_message(
                error_message="Coupon code not applicable. Please try from any of the other visible coupon codes.", status_code=400)
        return response
