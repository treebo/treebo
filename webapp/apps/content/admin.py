from django.contrib import admin

from apps.content.models import ContentStore
from dbcommon.admin import ReadOnlyAdminMixin


@admin.register(ContentStore)
class ContentStoreAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'name', 'value', 'version')
    search_fields = ('value',)
