import logging

from django.contrib.auth.models import AnonymousUser
from rest_framework.response import Response
from rest_framework.status import HTTP_405_METHOD_NOT_ALLOWED

from apps.auth.dto.otp_is_verify_dto import OtpIsVerifyDTO
from apps.auth.dto.otp_login_dto import OtpLoginDTO
from apps.auth.dto.otp_verify_dto import OtpVerifyDTO
from apps.auth.services.otp_service import OTPService
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboValidationException
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication

logger = logging.getLogger(__name__)


class OtpAPI(TreeboAPIView):
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    otp_service = OTPService()

    def dispatch(self, *args, **kwargs):
        return super(OtpAPI, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        return Response(status=HTTP_405_METHOD_NOT_ALLOWED)

    def post(self, request):
        mobile = request.data.get('mobile', '')
        name = request.data.get('name', "Guest")
        try:
            otp_login_dto = OtpLoginDTO(data={'phone_number': mobile})
            otp_login_dto.is_valid()
            self.otp_service.send_otp(otp_login_dto.data)
            return Response({
                'status': 'success',
                'msg': 'OTP successfully sent',
            })
        except TreeboValidationException:
            return Response({"error": {"msg": "Please provide a valid mobile number"}
                             }, status=400)


class OtpVerifyAPI(TreeboAPIView):
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    otp_service = OTPService()

    def dispatch(self, *args, **kwargs):
        return super(OtpVerifyAPI, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        return Response(status=HTTP_405_METHOD_NOT_ALLOWED)

    def post(self, request):
        """

        :param rest_framework.request.Request request:
        :return:
        """
        mobile = request.data.get('mobile', '')
        otp = request.data.get('otp', '')
        try:
            otp_verify_dto = OtpVerifyDTO(data={'phone_number': mobile,
                                                'verification_code': otp})
            otp_verify_dto.is_valid()
            self.otp_service.verify_otp(otp_verify_dto.data)
            return Response({
                'status': 'success',
                'msg': 'Otp Successfully verified',
            })
        except Exception as e:
            return Response({"error": {"msg": "Invalid OTP, please try again"}
                             }, status=400)


class OtpIsVerifiedAPI(TreeboAPIView):
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    otp_service = OTPService()

    def dispatch(self, *args, **kwargs):
        return super(OtpIsVerifiedAPI, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        return Response(status=HTTP_405_METHOD_NOT_ALLOWED)

    def post(self, request):
        mobile = request.data.get('mobile', '')
        name = request.data.get('name', 'guest')
        try:
            user = AnonymousUser()
            if hasattr(request, 'user') and request.user:
                user = request.user
            otp_isverify_dto = OtpIsVerifyDTO(data={'phone_number': mobile})
            otp_isverify_dto.is_valid()
            is_verified = self.otp_service.is_verified(
                otp_isverify_dto.data, user)
            if is_verified:
                return Response({
                    'isOtpVerified': True,
                    'msg': 'OTP is already verified',
                })
            else:
                otp_login_dto = OtpLoginDTO(data={'phone_number': mobile})
                otp_login_dto.is_valid()
                self.otp_service.send_otp(otp_login_dto.data)
                return Response({
                    'isOtpVerified': False,
                    'msg': 'OTP is not verified',
                })
        except Exception:
            return Response({"error": {"msg": "Invalid mobile number"}
                             }, status=400)
