# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discounts', '0012_auto_20161118_1224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discountcoupon',
            name='discount_operation',
            field=models.CharField(
                default=b'PERCENTAGE',
                max_length=20,
                choices=[
                    (b'FIXED',
                     b'Fixed'),
                    (b'PERCENTAGE',
                     b'Percentage'),
                    (b'NIGHT',
                     b'Night'),
                    (b'FLATPERNIGHT',
                     b'Flatpernight'),
                    (b'FLATCOST',
                     b'Flatcost'),
                    (b'FOTREDEEM',
                     b'Fotredeem')]),
        ),
    ]
