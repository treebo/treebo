

from django.conf.urls import url

from apps.hx_to_crs_hotel_migration.api.v1.retry_failed_bookings import RetryFailedBookingsDuringHotelMigration

app_name = 'bookings_v3'
urlpatterns = [
    url(r"^retry-failed-bookings/$", RetryFailedBookingsDuringHotelMigration.as_view(), name='retry-failed-bookings'),
]
