# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0058_categoryseodetails_localityseodetails'),
        ('checkout', '0007_auto_20170503_1358'),
    ]

    operations = [
        migrations.CreateModel(
            name='Audit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('check_in_date', models.DateField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('occupancy', models.FloatField()),
                ('is_pah_enabled', models.BooleanField()),
                ('bid', models.ForeignKey(to='checkout.BookingRequest', on_delete=models.CASCADE)),
                ('hotel_id', models.ForeignKey(to='dbcommon.Hotel', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('days_to_check_in', models.IntegerField()),
                ('time_dependant', models.BooleanField(default=False)),
                ('start_time', models.TimeField(default='00:00:00')),
                ('end_time', models.TimeField(default='00:00:00')),
            ],
            options={
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('enable_app', models.BooleanField(default=False, help_text='Enable intelligent PAH app')),
                ('white_list', models.TextField(default='', help_text='included hotel ids', null=True, blank=True)),
                ('enable_for_all_hotels', models.BooleanField(default=False)),
                ('black_list', models.TextField(default='', help_text='excluded hotel ids', null=True, blank=True)),
                ('enable_auditing', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Occupancy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.FloatField()),
                ('end', models.FloatField()),
                ('value', models.BooleanField(default=False)),
                ('category', models.ForeignKey(to='intelligent_pah.Category', on_delete=models.DO_NOTHING)),
            ],
        ),
    ]
