# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0025_auto_20160713_0910'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userreferralreward',
            name='amount',
        ),
        migrations.AddField(
            model_name='userreferralreward',
            name='reward_amount',
            field=models.CharField(
                default='0',
                max_length=4,
                null=True,
                blank=True),
        ),
    ]
