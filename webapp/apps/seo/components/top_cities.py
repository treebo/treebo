from apps.seo.components.base_component import BaseComponent
from apps.reviews.models import TAHotelIDMapping
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.serializers.serializer import TopCitiesSerializer
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from dbcommon.models.location import City
from data_services.city_repository import CityRepository

__author__ = 'ansilkareem'


class TopCities(BaseComponent):

    city_data_service = RepositoriesFactory.get_city_repository()

    def __init__(self, current_city, footer_data, exclude_list):
        super(
            TopCities,
            self).__init__(
            current_city,
            footer_data,
            exclude_list)

    def build(self):
        if self.current_city and self.current_city.enable_for_seo and self.footer_data and 'top_cities' in self.footer_data:
            top_cities = self.city_data_service.get_popular_cities(
                popular_cities=self.footer_data['top_cities'],
                city_exclude_list=self.exclude_list)
        else:
            top_cities = []

        data = {
            'key': 'top_cities',
            'title': 'Top cities',
            'content': AutoCompleteResultBuilder.build_city_result(top_cities)
        }
        serialized_obj = TopCitiesSerializer(data=data)
        if serialized_obj.is_valid():
            return serialized_obj.validated_data
        else:
            return {
                'key': 'top_cities',
                'title': 'Top cities',
                'content': []
            }
