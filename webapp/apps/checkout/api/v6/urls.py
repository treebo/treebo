from django.conf.urls import url
from apps.checkout.api.v7.itinerary_page_api import ItineraryPageAPI
from apps.checkout.api.v6.upfront_partpayment_status import UpfrontPartPaymentStatus
app_name = 'checkout_v6'


urlpatterns = [
    url(r'^itinerary/$', ItineraryPageAPI.as_view(), name='itinerary-page-v7'),
    url(r'^partpayment/status/(?P<order_id>[\w-]+)', UpfrontPartPaymentStatus.as_view(), name='partpayment-status'),
]
