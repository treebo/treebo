import logging

from django.core.management.base import BaseCommand

from apps.search.dto.hotel_serarch_dto import HotelSearchDTO
from apps.search.services.google_location_service import LocaiontService
from apps.search.services.hotel_search_service import HotelSearchService

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Change Email for all Hotels from Hotelogix Email to Treebo Mail"

    def handle(self, *args, **options):
        # self.print_all_city()
        self.print_city()

    def print_city(self, *args, **options):
        gls = LocaiontService()
        state = gls.get_state(12.3244066, 76.6299511)
        # lat1 = 12.911896
        # lon1 = 77.633306
        # lat2 = 12.909247
        # lon2 = 77.643193

        # nova
        lat1 = 12.911934
        lon1 = 77.633302

        # Point two

        lat2 = 12.899022
        lon2 = 77.630951
        dis = gls.get_distance_between_two_coordinate_in_meters(
            lat1, lon1, lat2, lon2)
        hotel_search_dto = HotelSearchDTO(data={
            "latitude": 12.899022,
            "radius": 100.0,
            "longitude": 77.630951
        })
        hotel_search_dto.is_valid()
        hotel_service = HotelSearchService()
        hotel_service.get_hotel_near_by(hotel_search_dto.data)
        # logger.info(" dis 1 %s ", dis)

    def print_all_city(self, *args, **options):
        logger.info(" test command")
        # for hotel in hotels:
        #     logger.info(" %s,%s ", hotel.latitude, hotel.longitude)
        gls = LocaiontService()
        state = gls.get_state(12.9706670000, 77.6006940000)
