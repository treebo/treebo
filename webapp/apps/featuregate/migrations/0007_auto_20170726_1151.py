# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('featuregate', '0006_auto_20160804_1800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='constraintexpression',
            name='expression',
            field=models.TextField(default='', null=True),
        ),
    ]
