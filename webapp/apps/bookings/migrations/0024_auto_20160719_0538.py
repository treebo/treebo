# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0023_auto_20160630_0624'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='booking_status',
            field=models.CharField(default='Tentative', max_length=50, null=True, blank=True,
                                   choices=[('Tentative', 'Tentative'),
                                            ('Awaiting HX Confirmation', 'Awaiting HX Confirmation'),
                                            ('Confirm', 'Confirm'), ('Cancel', 'Cancel'),
                                            ('Cancellation Pending', 'Cancellation Pending')]),
        ),
        migrations.AddField(
            model_name='booking',
            name='mm_promo',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='booking',
            name='rack_rate',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='booking',
            name='room_config',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='checkin_date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='checkout_date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='coupon_apply',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='hotel_id',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='instant_booking',
            field=models.NullBooleanField(default=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='is_audit',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='mm_promo',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='payment_amount',
            field=models.DecimalField(default=0, null=True, max_digits=20, decimal_places=2),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='payment_mode',
            field=models.CharField(default='Not Paid', max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='pretax_amount',
            field=models.DecimalField(null=True, max_digits=20, decimal_places=2),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='rack_rate',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=2),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='room_config',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='room_type',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='status',
            field=models.CharField(default='Initiated', max_length=50, null=True, blank=True,
                                   choices=[('Initiated', 'Initiated'),
                                            ('Payment In Progress', 'Payment In Progress'),
                                            ('Completed', 'Completed')]),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='tax_amount',
            field=models.DecimalField(null=True, max_digits=20, decimal_places=2),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='total_amount',
            field=models.DecimalField(null=True, max_digits=20, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='booking',
            name='status',
            field=models.IntegerField(default=1,
                                      choices=[(3, 'Tentative'), (2, 'Awaiting HX Confirmation'), (1, 'Confirm'),
                                               (0, 'Cancel'),
                                               (-1, 'Cancellation Pending')]),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='discount_value',
            field=models.DecimalField(default=0, max_digits=20, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='email',
            field=models.EmailField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='phone',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='user_login_state',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
    ]
