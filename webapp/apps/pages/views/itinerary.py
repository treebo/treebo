import json
import logging
from datetime import datetime, timedelta

from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.utils import dateformat

from apps.bookingstash.service.availability_service import check_availability
from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from apps.checkout.service import checkout_service
from apps.checkout.service.checkout_service import create_booking_request
from apps.common import utils as common_utils, date_time_utils
from apps.common import utils as trackerUtils
from apps.discounts.models import DiscountCoupon
from apps.featuregate.manager import FeatureManager
from apps.hotels import utils as hotel_utils
from apps.pages.data_model import ItineraryPageData
from apps.pages.helper import PageHelper
from apps.pricing.dateutils import date_to_ymd_str
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.profiles import utils
from base import log_args
from base.views.template import TreeboTemplateView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboValidationException
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)


class ItineraryPage(TreeboTemplateView):
    template_name = "itinerary/index.html"
    __pagename = 'itinerary'
    room_data_service = RepositoriesFactory.get_room_repository()

    def __update_or_get_booking_request(self, page_data):
        booking_request = BookingRequest.objects.get(
            pk=page_data.booking_request_id)
        if page_data.checkin_date != booking_request.checkin_date:
            booking_request.checkin_date = page_data.checkin_date
        if page_data.checkout_date != booking_request.checkout_date:
            booking_request.checkout_date = page_data.checkout_date
        if page_data.room_type != booking_request.room_type:
            booking_request.room_type = page_data.room_type
        if page_data.room_config != booking_request.room_config:
            booking_request.room_config = page_data.room_config
        booking_request.save()
        return booking_request

    def dispatch(self, request, *args, **kwargs):
        page_data = ItineraryPageData(**request.GET.dict())
        if page_data.redirect_url:
            return redirect(page_data.redirect_url)

        try:
            HotelEnquiryValidator(
                page_data.checkin_date,
                page_data.checkout_date,
                page_data.room_config).validate()
        except TreeboValidationException as e:
            logger.warn("Invalid request received %s" % e)
            if e.code in (
                error_codes.INVALID_GUESTS_SUPPLIED,
                    error_codes.INVALID_ROOMS):
                logger.exception("Invalid room config, resetting to 1")
                page_data.room_config = "1-0"
                logger.info("Room config set to: %s", page_data.room_config)
            elif e.code == error_codes.INVALID_DATES:
                logger.exception("Invalid dates, resetting to today")
                page_data.checkin_date_string = date_to_ymd_str(datetime.now())
                page_data.checkout_date_string = date_to_ymd_str(
                    datetime.now() + timedelta(days=1))

            itinerary_page_url = BookingHelper.build_itinerary_page_url(
                page_data, page_data.booking_request_id)
            logger.info(
                "Itinerary Page URL constructed: %s",
                itinerary_page_url)
            return redirect(itinerary_page_url)

        if not page_data.booking_request_id:
            # TODO Single redirect in case of invalid data
            booking_request_id = create_booking_request(request, page_data)
            itinerary_page_url = BookingHelper.build_itinerary_page_url(
                page_data, booking_request_id)
            logger.info(
                "Itinerary Page URL constructed: %s",
                itinerary_page_url)
            return redirect(itinerary_page_url)
        else:
            booking_request = BookingRequest.objects.get(
                pk=page_data.booking_request_id)
            if not page_data.room_type or not page_data.hashed_hotel_id:
                if not page_data.room_type:
                    page_data.room_type = booking_request.room_type
                if not page_data.hashed_hotel_id:
                    page_data.hashed_hotel_id = hotel_utils.generate_hash(
                        booking_request.hotel_id)
                itinerary_page_url = BookingHelper.build_itinerary_page_url(
                    page_data, booking_request.id)
                logger.info(
                    "Itinerary Page URL constructed: %s",
                    itinerary_page_url)
                return redirect(itinerary_page_url)
        return super(ItineraryPage, self).dispatch(request, *args, **kwargs)

    def post_process_context(self, request, context, booking_request):
        hotel_id = booking_request.hotel_id
        context['seo'] = {
            "canonical": request.build_absolute_uri(
                BookingHelper.build_itinerary_page_url_from_params(
                    hotel_id,
                    booking_request.room_config,
                    booking_request.room_type,
                    date_to_ymd_str(
                        booking_request.checkin_date),
                    date_to_ymd_str(
                        booking_request.checkout_date),
                    booking_request.coupon_code,
                    booking_request.id))}
        context["paynow_enabled"] = True
        context["pah_enabled"] = True
        utm_source = str(request.COOKIES.get('utm_source')).lower()
        utm_medium = str(request.COOKIES.get('utm_medium')).lower()
        featureManager = FeatureManager(
            self.__pagename,
            context['checkin_date'],
            context['checkout_date'],
            context['hotel_id'],
            booking_request.total_amount,
            context['hotel'].locality.name,
            context['hotel'].city.id,
            utm_source,
            utm_medium)

        overRiddenValues = featureManager.get_overriding_values()

        for overriddenKey in list(overRiddenValues.keys()):
            context[overriddenKey] = overRiddenValues[overriddenKey]

        if context['sold_out']:
            trackerUtils.trackAnalyticsServerEvent(
                request,
                'Hotel Sold Out',
                {
                    'page': 'Itinerary',
                    'hotel_id': context['hotel_id'],
                    'hotel_name': context['hotel'].name,
                    'checkin': context['checkin'],
                    'checkout': context['checkout'],
                    'adults': context['adults'],
                    'kids': context['children'],
                    'rooms': context['room_count'],
                    'room_type': context['room_type'],
                    'total_price': str(
                        booking_request.total_amount),
                    'user_login_state': 'LOGGEDIN' if request.user and request.user.is_authenticated() else 'LOGGEDOUT',
                    'user_signup_channel': request.COOKIES.get('user_signup_channel'),
                    'user_signup_date': request.COOKIES.get('user_signup_channel')},
                channel=booking_request.booking_channel)

        couponDesc = ""
        couponObj = DiscountCoupon.objects.filter(
            code__iexact=booking_request.coupon_code).first()
        if couponObj:
            couponDesc = str(couponObj.terms.replace('\\n', ' '))
        context['coupon_desc'] = couponDesc
        return context

    def get_itinerary_data(self, request, args, kwargs):
        page_data = ItineraryPageData(**request.GET.dict())
        logger.info("Booking Request ID: %s", page_data.booking_request_id)
        booking_request = self.__update_or_get_booking_request(page_data)

        checkin_date_string, checkout_date_string = date_to_ymd_str(
            booking_request.checkin_date), date_to_ymd_str(
            booking_request.checkout_date)
        adults, children, roomCount = PageHelper.parseRoomConfig(
            booking_request.room_config)
        days = date_time_utils.get_day_difference(
            checkin_date_string, checkout_date_string)

        hotel = get_object_or_404(Hotel, pk=booking_request.hotel_id)
        room = self.room_data_service.get_room_with_related_entities(
            select_list=['hotel'], prefetch_list=None, filter_params={
                'room_type_code': booking_request.room_type, 'hotel_id': hotel.id})
        #room = Room.objects.select_related('hotel').get(hotel_id=hotel.id, room_type_code=booking_request.room_type)

        city_url = "{0}?locality={1}&lat={2}&long={3}&query={4}&checkin={5}&checkout={6}&roomconfig={7}".format(
            reverse('pages:search-hotels'), "", "", "", hotel.city.name, checkin_date_string,
            checkout_date_string, booking_request.room_config)

        kwargs = hotel_utils.get_hotel_url_kwargs(hotel)
        hd_url = "{0}?checkin={1}&checkout={2}&roomconfig={3}".format(
            reverse('pages:hotel-details', kwargs=kwargs),
            checkin_date_string, checkout_date_string, booking_request.room_config)
        roomConfigArray = PageHelper.getRoomConfigData(
            booking_request.room_config)

        checkIn = dateformat.format(booking_request.checkin_date, "jS M'y")
        checkOut = dateformat.format(booking_request.checkout_date, "jS M'y")

        if request.user.is_authenticated() and utils.get_customer_care(request.user.id):
            isCallCenter = True
        else:
            isCallCenter = False

        email, mobile, name = self.__getBookingUserDetails(request)
        available = check_availability(
            hotel,
            booking_request.room_type,
            booking_request.room_config,
            checkin_date_string,
            checkout_date_string)

        context = {
            "bid": booking_request.id,
            "hotel": hotel,
            "hd_url": hd_url,
            "city_url": city_url,
            "page_name": "Itinerary",
            "adults": adults,
            "children": children,
            "room_type": booking_request.room_type,
            "checkin": checkIn,
            "checkout": checkOut,
            "room": room,
            "days": days,
            "is_callcenter": isCallCenter,
            "room_count": roomCount,
            "room_config": roomConfigArray,
            "coupon_code": booking_request.coupon_code,
            "hotel_id": hotel.id,
            "image_url": hotel.get_showcased_image_url(),
            "checkin_date": checkin_date_string,
            "checkout_date": checkout_date_string,
            "pay_at_hotel": True,
            "ask_for_advance": False,
            "guest_name": name,
            "guest_email": email,
            "guest_mobile": mobile,
            "sold_out": not available,
            "comments": "",
            'razorpay_keys': settings.RAZORPAY_KEYS
        }

        prices = checkout_service.build_pricing_for_booking(
            request, booking_request)
        context.update(prices)
        return context, booking_request

    @log_args(logger)
    def __getItineraryData(self, request, args, kwargs):
        """
        Get Itinerary Data
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        context, booking_request = self.get_itinerary_data(
            request, args, kwargs)
        self.post_process_context(request, context, booking_request)
        isOtpEnabled = FeatureToggleAPI.is_enabled(
            "booking", "otp_verification_required", False)
        is_trivago_request = str(request.COOKIES.get('utm_source'))
        is_otp_disabled_for_trivago = FeatureToggleAPI.is_enabled(
            "trivago_otp", "is_otp_disabled_for_trivago", False)
        if is_trivago_request and is_trivago_request.lower(
        ) == 'trivago' and is_otp_disabled_for_trivago:
            isOtpEnabled = False
        logger.debug(
            "is_otp_disabled_for_trivago %s is_trivago_request %s",
            is_otp_disabled_for_trivago,
            is_trivago_request)
        context['jsvars'] = str(json.dumps({
            'page': 'itinerary',
            'hotelId': context['hotel_id'],
            'hotelName': context['hotel'].name,
            'discount': float(common_utils.round_to_two_decimal(booking_request.discount_value)),
            'voucherAmount': float(booking_request.voucher_amount),
            'autopromo': context['promo_discount'],
            'nights': context['nights_breakup'],
            'total': float(booking_request.total_amount),
            'couponDesc': context['coupon_desc'],
            'discount_url': reverse("checkout:apply-coupon"),
            'isOtpEnabled': isOtpEnabled,
        }))
        return context

    def getData(self, request, args, kwargs):
        context = self.__getItineraryData(request, args, kwargs)
        context['styles'] = ['desktop/css/itinerary.css']
        context['scripts'] = ['desktop/js/itinerary.js']
        return context

    def getMobileData(self, request, args, kwargs):
        context = self.__getItineraryData(request, args, kwargs)
        context['styles'] = ['mobile/css/itinerary.css']
        context['scripts'] = ['mobile/js/itinerary.js']
        context['footer'] = 'hide'
        context['googleLogin'] = True
        return context

    def __getBookingUserDetails(self, request):
        logger.debug('Getting Booking User Details')
        email = mobile = name = None
        if request.user.is_authenticated():
            name = request.user.first_name
            email = request.user.email
            mobile = request.user.phone_number
        return email, mobile, name


class HotelEnquiryValidator:
    def __init__(self, checkInDate, checkOutDate, roomsConfig):
        self.__checkInDate = checkInDate
        self.__checkOutDate = checkOutDate
        self.__roomsConfig = roomsConfig

    def validate(self):
        yesterday = (datetime.now() - timedelta(days=1)).date()
        try:
            if self.__checkInDate >= self.__checkOutDate:
                raise TreeboValidationException(error_codes.INVALID_DATES)
            elif self.__checkInDate < yesterday or self.__checkOutDate < yesterday:
                raise TreeboValidationException(error_codes.INVALID_DATES)
        except Exception:
            logger.exception("Exception occurred")
            raise TreeboValidationException(error_codes.INVALID_DATES)

        try:
            if len(self.__roomsConfig) < 1:
                raise TreeboValidationException(error_codes.INVALID_ROOMS)
        except Exception:
            raise TreeboValidationException(error_codes.INVALID_ROOMS)

        try:
            self.__roomsConfig = self.__roomsConfig.split(",")
            for singleRoomConfig in self.__roomsConfig:
                guest = singleRoomConfig
                if int(guest[0]) < 1:
                    raise TreeboValidationException(
                        error_codes.INVALID_GUESTS_SUPPLIED)
                if "-" in guest:
                    if len(singleRoomConfig) > 1 and int(guest[2]) < 0:
                        raise TreeboValidationException(
                            error_codes.INVALID_GUESTS_SUPPLIED)
        except Exception:
            raise TreeboValidationException(
                error_codes.INVALID_GUESTS_SUPPLIED)
