from dbcommon.models.miscellanous import Image
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


class ImageDto(object):
    def __init__(self, kwargs):
        try:
            logger.info("Inside Image DTO")
            acceptable_keys_list = [
                field.name for field in Image._meta.get_fields()]
            for k, v in list(kwargs.items()):
                if k in acceptable_keys_list:
                    self.__setattr__(k, v)
        except Exception as e:
            logger.error(e)
