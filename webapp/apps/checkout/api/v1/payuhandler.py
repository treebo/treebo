import logging

import requests
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.views import APIView

from apps.checkout.api.v1.PaymentHandler import PaymentHandler
from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from apps.checkout.tasks import send_booking_confirmation_notification
from base import log_args
from services.hotellogix.exceptions import send_exception_mail
from services.restclient.bookingrestclient import BookingClient

logger = logging.getLogger(__name__)


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class PayU(APIView, PaymentHandler):
    authentication_classes = (
        CsrfExemptSessionAuthentication,
        BasicAuthentication)

    @log_args(logger)
    def post(self, request):
        status, gatewayPaymentId, mode, amount, booking_request_id = self.__parseRequest(
            request)

        booking_request = BookingRequest.objects.get(pk=booking_request_id)
        if status == 'success':
            return self.process_successful_payment(
                booking_request,
                amount,
                gatewayPaymentId,
                booking_request.order_id,
                booking_request_id,
                request,
                status)
        else:
            return self.process_unsuccessful_payment(
                booking_request, booking_request.order_id, request)

    def process_unsuccessful_payment(self, booking_request, order_id, request):
        BookingHelper.track_booking_failure(
            'Payment Failed', booking_request, request)
        return HttpResponseRedirect(
            BookingHelper.constructItineraryUrlOnError(
                booking_request, "PayU payment failed"))

    def process_successful_payment(
            self,
            booking_request,
            amount,
            gatewayPaymentId,
            order_id,
            referenceid,
            request,
            status):

        self._update_payments(amount, order_id, gatewayPaymentId, "PayU")
        return self._confirm_booking(order_id, referenceid, request)

    def __parseRequest(self, request):
        status = request.data['status']
        gatewayPaymentId = request.data['mihpayid']
        txnId = request.data['txnid']
        mode = request.data['mode'] if request.data['mode'] else ""
        amount = request.data['amount']
        referenceid = request.data['udf1']

        logger.debug(
            "Txn Status = %s, Txn Id = %s, mihpayid = %s, mode = %s, amount = %s" %
            (status, txnId, gatewayPaymentId, mode, amount))

        return status, gatewayPaymentId, mode, amount, referenceid
