import logging
from datetime import datetime, timedelta

from django.template.defaultfilters import slugify
from hashids import Hashids
from pyquery import PyQuery as pQ

from base import log_args
from common.constants import common_constants as const
from services import maps
from data_services.respositories_factory import RepositoriesFactory
from apps.common import date_time_utils
from services.maps import Maps

logger = logging.getLogger(__name__)


@log_args(logger)
def setSearchData(request):
    query = request.GET.get('query') if 'query' in request.GET else ""
    checkin_date = request.GET.get('checkIn') if 'checkIn' in request.GET and request.GET.get(
        'checkIn') else datetime.now().strftime("%Y-%m-%d")
    checkout_date = request.GET.get('checkOut') if 'checkOut' in request.GET and request.GET.get(
        'checkOut') else (datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
    guest = request.GET.get('guest') if 'guest' in request.GET else "1"
    room_required = request.GET.get('rooms') if 'rooms' in request.GET else "1"
    lat = request.GET.get('lat') if 'lat' in request.GET and request.GET.get(
        'lat') != "" else const.BANGALORE_LAT
    long = request.GET.get('long') if 'long' in request.GET and request.GET.get(
        'long') != "" else const.BANGALORE_LONG
    locality = request.GET.get('locality') if 'locality' in request.GET and request.GET.get(
        'locality') != "" else const.DEFAULT_LOCALITY
    return query, checkin_date, checkout_date, guest, room_required, lat, int, locality


#  @staticmethod
@log_args(logger)
def setSearchPostData(request):
    query = request.POST['query'] if 'query' in request.GET else ""
    checkin_date = request.POST['checkin'] if 'checkin' in request.GET and request.GET.get(
        'checkin') else datetime.now().strftime("%Y-%m-%d")
    checkout_date = request.POST['checkout'] if 'checkout' in request.GET and request.GET.get(
        'checkout') else (datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
    guest = request.POST['guest'] if 'guest' in request.GET else "1"
    room_required = request.POST['rooms'] if 'rooms' in request.GET else "1"
    return query, checkin_date, checkout_date, guest, room_required


@log_args(logger)
def buildLocationDataString(cities, localities, unique_locality_cities):
    locationDataString = ""
    comma = ""
    for city in cities:
        locationDataString += comma
        locationDataString += '"' + city.name + '"'
        comma = " , "
    comma = ""
    for index, unique_locality_city in enumerate(unique_locality_cities):
        if (index > 0):
            locationDataString += ' , '
        locationDataString += '"'
        locationDataString += unique_locality_city
        locationDataString += " - "
        comma = ""
        for locality in localities:
            if unique_locality_city == locality.city.name:
                locationDataString += comma
                locationDataString += locality.name
                comma = " , "
        locationDataString += '"'
    return locationDataString


@log_args(logger)
def getMinimumRate(hotelogix_room):
    tax = 0
    min_rate = 0
    availability = False
    rate_id = ""
    rates = hotelogix_room('rate')
    for rate_pq in rates:
        rate = pQ(rate_pq)
        if rate.attr("title").strip() == const.PROMOTIONAL_WEB_PACKAGE:
            availability = True
            min_rate = float(rate.attr("price"))
            rate_id = rate.attr("id")
            tax = float(rate.attr("tax"))
            break
        elif rate.attr("title").strip() == const.WEB_PACKAGE and (
                min_rate == 0 or min_rate > float(rate.attr("price"))):
            availability = True
            min_rate = float(rate.attr("price"))
            rate_id = rate.attr("id")
            tax = float(rate.attr("tax"))
    return min_rate, rate_id, tax, availability


@log_args(logger)
def validateData(checkin_date, checkout_date, guest, room_required):
    try:
        if not guest.isdigit() or not room_required.isdigit() \
                or datetime.strptime(checkin_date, "%Y-%m-%d").date() >= datetime.strptime(
                    checkout_date, "%Y-%m-%d").date() \
                or int(guest) < 1 or int(room_required) < 1 or int(room_required) > int(guest):
            logger.error(
                "checkin_date=" +
                checkin_date +
                "checkout_date=" +
                checkout_date +
                "guests=" +
                guest +
                "room_required=" +
                room_required)
            return False
    except Exception as exc:
        logger.exception(exc)
        # raise Http404
        return False
    return True


def buildHotelAddress(hotel):
    address = hotel.street + ", " + hotel.locality.name + ", " + hotel.city.name
    return address


@log_args(logger)
def get_location_map(landmark, origin_latlog):
    landmark_map = {}
    landmark_map["location"] = landmark.name
    dist_latlog = str(landmark.latitude) + "," + str(landmark.longitude)
    landmark_map["distance"] = maps.Maps().calculateDistanceWithoutGMAP(
        origin_latlog, dist_latlog)
    return landmark_map


def generate_hash(input):
    hashids = Hashids(salt=const.HASHING_SALT, min_length=8)
    hashid = hashids.encode(input)  # 'Mj3'
    return hashid


def decode_hash(hashed_input):
    hashids = Hashids(salt=const.HASHING_SALT, min_length=8)
    id = hashids.decode(hashed_input)  # ('Mj3',0)
    if len(id) == 0:
        return hashed_input
    return id[0]


def isOccupancySupported(room, adults, children):
    return adults >= 1 and (
        adults +
        children) <= room.max_adult_with_children and adults <= room.max_guest_allowed


@log_args(logger)
def getHotelUrl(hotel):
    hotel_string = slugify(hotel.name)
    hotel_string = hotel_string.lower()
    url = hotel_string + '-h' + str(generate_hash(hotel.id))
    return url


def get_hotel_url_kwargs_from_dict(hotel_dict):
    hotel_slug = slugify(hotel_dict.get('hotelName', ''))
    locality_slug = slugify(hotel_dict.get('locality', ''))
    city_slug = slugify(hotel_dict.get('city', ''))
    kwargs = {
        'city_slug': city_slug,
        'hotel_slug': hotel_slug,
        'locality_slug': locality_slug,
        'hotel_id': hotel_dict['id']}
    return kwargs


def get_hotel_kwargs(hotel):
    hotel_slug = slugify(hotel.name)
    locality_slug = slugify(hotel.locality.name)
    city_slug = slugify(hotel.city.name)
    kwargs = {
        'city_slug': city_slug,
        'hotel_slug': hotel_slug,
        'locality_slug': locality_slug,
        'hotel_id': hotel.id}
    return kwargs


def get_hotel_url_kwargs(hotel):
    hotel_string = slugify(hotel.name)
    hotel_string = hotel_string.lower()
    hotel_hash_id = str(generate_hash(hotel.id))
    return {'hotelString': hotel_string, 'hotel_id': hotel.id}


@log_args(logger)
def getNearByHotelUrlWithDatesParams(
        hotel,
        checkInDate,
        checkOutDate,
        roomconfig):
    url = getHotelUrl(hotel)
    url += "/?checkin={0}&checkout={1}&roomconfig={2}".format(
        str(checkInDate.strftime(date_time_utils.DATE_FORMAT)),
        str(checkOutDate.strftime(date_time_utils.DATE_FORMAT)), str(roomconfig))
    return url


@log_args(logger)
def getCityUrl(city):
    city_string = slugify(city.name)
    city_string = city_string.lower()
    url = city_string + "-c" + generate_hash(city.id)
    logger.info("url generated for city" + str(city.name) + "is " + str(url))
    return url


@log_args(logger)
def buildRoomKey(room, checkin_date, checkout_date, guest):
    return room.room_type + "_" + \
        str(guest) + "_" + checkin_date + "_" + checkout_date


def getHotelObject(hashedHotelId):
    hotelId = decode_hash(hashedHotelId)
    try:
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        return hotel_repository.get_single_hotel(id=hotelId, status=1)
    except Exception as e:
        return None


def get_directions_from_landmark(hotelId):
    directions_repository = RepositoriesFactory.get_directions_repository()
    directions = directions_repository.get_directions_ordered_by_landmark_popularity(
        hotel_id=hotelId)
    if directions:
        directions = directions[0]
        # directions = Directions.objects.filter(hotel=hotelId).order_by("-landmark_obj__popularity")[
        #              :1].get()
        text = directions.directions_for_email_website
        text_list = text.split("\\n")
        directions.text = text_list
        logger.debug("Direction Text = %s" % directions.text)
    return directions


def generate_static_map_url(
        hotel,
        directions,
        size="400x450",
        showPolyline=False):
    origin_latlong = str(hotel.latitude) + "," + str(hotel.longitude)
    dest_latlong = str(directions.landmark_obj.latitude) + "," + str(
        directions.landmark_obj.longitude)
    map_obj = Maps()
    directions.distance = map_obj.calculateDistanceWithoutGMAP(
        origin_latlong, dest_latlong)
    static_map_url = map_obj.getStaticMapUrl(
        origin_latlong,
        dest_latlong,
        directions,
        size,
        showPolyline=showPolyline)
    logger.debug("static_map_url=%s" % str(static_map_url))
    return static_map_url
