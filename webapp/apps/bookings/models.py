# -*- coding: utf-8 -*-
"""
    Booking Models
"""
# pylint: disable-msg=invalid-name, no-member

import logging

from decimal import Decimal

from django.db import models
from djutil.models import TimeStampedModel
from jsonfield import JSONField

from apps.bookings.constants import AsyncJobStates, PAYMENT_OFFER_GATEWAY
from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.models.hotel import Hotel
from dbcommon.models.profile import User
from dbcommon.models.room import Room
from django.conf import settings

logger = logging.getLogger(__name__)


class PaymentOrder(TimeStampedModel, DefaultPermissions):
    ISSUED = 'ISSUED'
    CAPTURED = 'CAPTURED'
    FAILED = 'FAILED'
    PASSED = 'PASSED'

    PAYMENT_SERVICE = 'PAYMENT_SERVICE'

    order_id = models.CharField(max_length=50)  # gateway's deprecated
    ps_order_id = models.CharField(max_length=50, null=True)
    status = models.CharField(max_length=30)
    amount = models.CharField(max_length=50)
    gateway_type = models.CharField(max_length=50)
    pg_order_id = models.CharField(max_length=50, null=True)
    ps_payment_id = models.CharField(max_length=50, null=True)
    booking_order_id = models.CharField(max_length=50, null=True)
    updated_in_booking_service = models.NullBooleanField(default=False)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Payment Order'
        verbose_name_plural = 'Payment Orders'

    def __repr__(self):
        return 'PaymentOrder <%s %s>' % (self.order_id, self.gateway_type)

    def __unicode__(self):
        return '%s %s' % (self.order_id, self.gateway_type)

    def __str__(self):
        return '%s %s' % (self.order_id, self.gateway_type)

    def is_verified(self):
        if self.status == PaymentOrder.CAPTURED:
            return True
        return False


class Booking(TimeStampedModel, DefaultPermissions):
    MSITE = 'msite'
    WEBSITE = 'website'
    APP = 'app'
    GDC = 'gdc'
    ANDROID = 'android'
    IOS = 'ios'
    BOOKING_CHANNEL = (
        (MSITE, MSITE),
        (WEBSITE, WEBSITE),
        (APP, APP),
        (GDC, GDC),
        (ANDROID, ANDROID),
        (IOS, IOS)
    )
    TENTATIVE = "Tentative"
    CONFIRM = "Confirm"
    FAILURE = "Failure"
    CANCEL = "Cancel"
    INITIATED = "Initiated"
    AWAITING_HX_CONFIRMATION = "Awaiting HX Confirmation"
    CANCELLATION_PENDING = "Cancellation Pending"
    TEMP_BOOKING_CREATED = "Temp Booking Created"
    PART_SUCCESS = "Part Success"
    RESERVED = 'Reserved'
    CHECKEDOUT = 'Checked Oout'
    NOSHOW = 'Noshow'
    CHECKEDIN = 'Checked In'
    PARTCHECKEDIN = 'Part Checked In'
    PARTCHECKEDOUT = 'Part Checked Out'
    BOOKING_STATUS_CHOICES = (
        (TENTATIVE, TENTATIVE),
        (INITIATED, INITIATED),
        (RESERVED, RESERVED),
        (CONFIRM, CONFIRM),
        (CANCEL, CANCEL),
        (CANCELLATION_PENDING, CANCELLATION_PENDING),
        (FAILURE, FAILURE),
        (TEMP_BOOKING_CREATED, TEMP_BOOKING_CREATED),
        (PART_SUCCESS, PART_SUCCESS)
    )

    OLD_TENTATIVE = 3
    OLD_AWAITING_HX_CONFIRMATION = 2
    OLD_CONFIRM = 1
    OLD_CANCEL = 0
    OLD_CANCELLATION_PENDING = -1
    OLD_BOOKING_STATUS = (
        (OLD_TENTATIVE, "Tentative"),
        (OLD_AWAITING_HX_CONFIRMATION, "Awaiting HX Confirmation"),
        (OLD_CONFIRM, "Confirm"),
        (OLD_CANCEL, "Cancel"),
        (OLD_CANCELLATION_PENDING, "Cancellation Pending")
    )

    NOT_PAID = 'Not Paid'
    PART_PAID = 'PART_PAID'
    RAZORPAY_PARTPAY = 'RAZORPAY_PARTPAY'
    PAID = 'Paid'
    PARTIAL_RESERVE = 'Partial Reserve'
    order_id = models.CharField(max_length=200)
    checkin_date = models.DateField()
    checkout_date = models.DateField()
    adult_count = models.PositiveIntegerField()
    child_count = models.PositiveIntegerField()
    hotel = models.ForeignKey(
        Hotel,
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING,
        related_name="hotelbookings")
    room = models.ForeignKey(
        Room,
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING,
        related_name="roombookings")
    room_config = models.CharField(max_length=200, null=True, blank=True)
    rack_rate = models.DecimalField(
        max_digits=20, null=True, decimal_places=10)
    mm_promo = models.DecimalField(max_digits=20, null=True, decimal_places=10)
    pretax_amount = models.DecimalField(
        max_digits=20, null=True, decimal_places=10)
    tax_amount = models.DecimalField(
        max_digits=20, null=True, decimal_places=10)
    total_amount = models.DecimalField(max_digits=20, decimal_places=10)
    user_id = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING,
        related_name="userbookings")
    guest_name = models.CharField(max_length=200)
    guest_email = models.EmailField(max_length=200, null=True, blank=True)
    guest_country_code = models.CharField(max_length=8, null=True)
    guest_mobile = models.CharField(max_length=20)
    is_complete = models.BooleanField(default=False)
    payment_amount = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    is_traveller = models.BooleanField(default=False)
    booking_code = models.CharField(max_length=200, null=True, blank=True)
    comments = models.TextField(default="", blank=True)
    coupon_apply = models.BooleanField(default=False)
    coupon_code = models.CharField(max_length=200, null=True, blank=True)
    payment_mode = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        default="Not Paid")
    discount = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    group_code = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        default='')
    status = models.IntegerField(
        default=OLD_CONFIRM,
        choices=OLD_BOOKING_STATUS)
    booking_status = models.CharField(
        max_length=50,
        choices=BOOKING_STATUS_CHOICES,
        null=True,
        blank=True,
        default=TENTATIVE)
    paymentorder = models.ForeignKey(
        PaymentOrder,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        default=None)
    cancellation_datetime = models.DateTimeField(blank=True, null=True)
    channel = models.CharField(
        max_length=50,
        choices=BOOKING_CHANNEL,
        blank=True,
        default='')
    is_audit = models.NullBooleanField(null=True, default=False)
    voucher_amount = models.DecimalField(
        max_digits=20, decimal_places=2, default=0, null=True)
    rate_plan = models.CharField(max_length=50, null=True, blank=True)
    rate_plan_meta = JSONField(blank=True, null=True)
    member_discount = models.DecimalField(
        max_digits=9, null=True, decimal_places=4, default=0)
    organization_name = models.CharField(max_length=200, null=True, blank=True)
    organization_address = models.CharField(
        max_length=200, null=True, blank=True)
    organization_taxcode = models.CharField(
        max_length=100, null=True, blank=True)
    wallet_applied = models.NullBooleanField(default=False, null=True)
    wallet_deduction = models.DecimalField(
        max_digits=9, decimal_places=4, default=0, null=True)

    # the above should be null booleanfield coz postgres will do a complete
    # override of the column if no null is provided

    external_crs_booking_id = models.CharField(
        max_length=50,
        null=True,
        blank=True)
    external_crs_booking_version = models.IntegerField(
        null=True)
    external_crs_booking_extra_info = models.TextField(
        null=True,
        blank=True)

    class Meta(DefaultPermissions.Meta):
        pass

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s %s %s %s' % (self.order_id,
                                self.guest_name,
                                self.checkin_date,
                                self.booking_status)

    def __str__(self):
        return '%s %s %s %s' % (self.order_id,
                                self.guest_name,
                                self.checkin_date,
                                self.booking_status)

    BOOKING_MAIL_REPR = """
<html>
<body>
Order ID: {order_id} , <br>
    Guest Name: {guest_name} , <br>
    Guest Email: {guest_email} , <br>
    Guest Phone Number: {guest_number} , <br>
    Hotel Name: {hotel_name} , <br>
    Room Type: {room_type} , <br>
    Total Rooms: {total_rooms} , <br>
    Adult: {adult} , <br>
    Children: {children} , <br>
    Checkin Date: {checkin} , <br>
    Checkout Date: {checkout} , <br>
    Pretax Sell Price: {pretax} , <br>
    Tax Amount: {tax} , <br>
    Discount Coupon Applied: {discount_applied} , <br>
    Discount Value: {discount_value} , <br>
    Wallet Applied: {wallet_applied} , <br>
    Amount Paid: {amount_paid} , <br>
    Total Amount: {total_amount} , <br>
    Payment Mode: {payment_mode} , <br>
    Booking Status: {booking_status} , <br>
    Booking Channel: {booking_channel} , <br>
    Is FOT Audit: {fot} , <br>
    Special Preference: {preference}, <br>
    Organisation Name: {organisation_name} , <br>
    Organisation taxCode: {organisation_taxcode} , <br>
    Organisation Address" {organisation_address} , <br>
    <br>
    Payment details:<br> {payment_details}

</body>
</html>
    """

    def get_mail_representation(self):
        from apps.common import utils
        is_pay_at_hotel = self.payment_mode in ("Not Paid", 'RAZORPAY_PARTPAY')
        amount_paid = utils.round_to_two_decimal(
            Decimal(
                self.total_amount) -
            Decimal(
                self.wallet_deduction)) if not is_pay_at_hotel else 0
        data = {
            "order_id": self.order_id,
            "guest_name": self.guest_name,
            "guest_email": self.guest_email,
            "guest_number": self.guest_mobile,
            "hotel_name": self.hotel.name,
            "room_type": self.room.room_type_code,
            "total_rooms": len(self.room_config.split(",")),
            "adult": self.adult_count,
            "children": self.child_count,
            "checkin": self.checkin_date,
            "checkout": self.checkout_date,
            "pretax": utils.round_to_two_decimal(self.pretax_amount),
            "tax": utils.round_to_two_decimal(self.tax_amount),
            "discount_applied": self.coupon_code,
            "discount_value": utils.round_to_two_decimal(self.discount),
            "amount_paid": amount_paid,
            "wallet_applied": self.wallet_applied,
            "total_amount": utils.round_to_two_decimal(self.total_amount),
            "payment_mode": "Pay At Hotel" if is_pay_at_hotel else "Paid",
            "booking_channel": self.channel,
            "booking_status": self.booking_status,
            "fot": self.is_audit,
            "preference": self.comments,
            "organisation_name": self.organization_name if self.organization_name else '',
            "organisation_taxcode": self.organization_taxcode if self.organization_taxcode else '',
            "organisation_address": self.organization_address if self.organization_address else '',
            "payment_details": self.get_payment_details()
        }
        return Booking.BOOKING_MAIL_REPR.format(**data)

    def get_payment_details(self):
        payment_details = PaymentOrder.objects.filter(booking_order_id=self.order_id).values(
            'gateway_type',
            'status', 'amount')
        result = ''
        for payment_detail in payment_details:
            for key, value in payment_detail.items():
                result += key + ': ' + value + ", "
            result += '<br>'
        return result

    def get_online_payments(self):
        return PaymentOrder.objects.filter(booking_order_id=self.order_id,
                                           status=PaymentOrder.CAPTURED,
                                           ).exclude(gateway_type__in=(
            settings.TREEBO_WALLET_GATEWAY, PAYMENT_OFFER_GATEWAY))

    def get_wallet_payments(self):
        return PaymentOrder.objects.filter(booking_order_id=self.order_id,
                                           status=PaymentOrder.CAPTURED,
                                           gateway_type=settings.TREEBO_WALLET_GATEWAY
                                           )

    def get_offer_price_payments(self):
        return PaymentOrder.objects.filter(booking_order_id=self.order_id,
                                           status=PaymentOrder.CAPTURED,
                                           gateway_type=PAYMENT_OFFER_GATEWAY
                                           )


    def get_wallet_payment_amount(self):
        payment_orders = PaymentOrder.objects.filter(booking_order_id=self.order_id,
                                                     status=PaymentOrder.CAPTURED,
                                                     gateway_type=settings.TREEBO_WALLET_GATEWAY
                                                     )
        return sum(float(payment_order.amount) for payment_order in payment_orders)

    def get_committed_price(self):
        from apps.checkout.models import BookingRequest
        booking_request = BookingRequest.objects.get(order_id=self.order_id)
        from apps.checkout.models import CommittedPrice
        return CommittedPrice.objects.filter(booking_request=booking_request).order_by(
            '-id').first()

    def get_booking_sub_channel(self):
        from apps.checkout.models import BookingRequest
        booking_request = BookingRequest.objects.get(order_id=self.order_id)
        return booking_request.booking_subchannel

    def get_booking_application(self):
        from apps.checkout.models import BookingRequest
        booking_request = BookingRequest.objects.get(order_id=self.order_id)
        return booking_request.booking_application

    @property
    def checkin_time(self):
        time_tuple = self.comments.split(',')
        try:
            return str(time_tuple[0].split(':')[1])
        except Exception:
            logger.exception("Exception during formatting checkin time")
            return ""

    @property
    def checkout_time(self):
        time_tuple = self.comments.split(',')
        try:
            checkout_time = str(time_tuple[1].split(':')[1])
            checkout_time = str(checkout_time.split(']')[0])
            return checkout_time
        except Exception:
            logger.exception("Exception during formatting checkout time")
            return ""

    def booking_type(self):
        return 'G' if self.group_code else 'S'


class RoomBooking(TimeStampedModel, DefaultPermissions):
    room = models.ForeignKey(
        Room,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        related_name="roomroombooking")
    booking = models.ForeignKey(
        Booking,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        related_name="bookingroombooking")
    reservation_id = models.CharField(max_length=200)
    price = models.DecimalField(
        max_digits=8,
        decimal_places=2,
        null=True,
        blank=True)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Room Booking'
        verbose_name_plural = 'Room Bookings'

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s %s' % (self.room.room_type, self.reservation_id)

    def __str__(self):
        return '%s %s' % (self.room.room_type, self.reservation_id)


class Reservation(TimeStampedModel, DefaultPermissions):
    reservation_id = models.CharField(max_length=200)
    reservation_status = models.CharField(max_length=200)
    reservation_checkin_date = models.DateField()
    reservation_checkout_date = models.DateField()
    adult_count = models.PositiveIntegerField(default=0)
    child_count = models.PositiveIntegerField(default=0)
    room_unit = models.PositiveIntegerField()
    total_amount = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    hotel = models.ForeignKey(
        Hotel,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True)
    room = models.ForeignKey(
        Room,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True)
    room_block_code = models.CharField(
        max_length=100, null=True, blank=True, default='')
    guest_name = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        default='')
    guest_email = models.EmailField(
        max_length=200, null=True, blank=True, default='')
    guest_mobile = models.CharField(
        max_length=200, null=True, blank=True, default='')
    reservation_create_time = models.DateTimeField()

    class Meta(DefaultPermissions.Meta):
        pass

    def __unicode__(self):  # __unicode__ on Python 2
        if self.hotel:
            hotel_name = self.hotel.name
        else:
            hotel_name = "no hotel"

        if self.room:
            room_type = self.room.room_type
        else:
            room_type = "no room"
        return '%s %s %s' % (self.reservation_id, hotel_name, room_type)

    def __str__(self):
        if self.hotel:
            hotel_name = self.hotel.name
        else:
            hotel_name = "no hotel"

        if self.room:
            room_type = self.room.room_type
        else:
            room_type = "no room"
        return '%s %s %s' % (self.reservation_id, hotel_name, room_type)


class AsyncJobStatus(TimeStampedModel):
    """
        EasyJobStatus this class stores job status for easyjoblite jobs
    """
    job_id = models.CharField(db_index=True, unique=True, max_length=100)
    booking_id = models.IntegerField(null=True)
    tag = models.CharField(max_length=100)
    data = JSONField()
    code = models.PositiveIntegerField(default=100)
    message = models.CharField(max_length=300, default='')
    status = models.CharField(max_length=100, default=AsyncJobStates.CREATED)
    exception_data = JSONField(null=True)


class UnVerifiedPayAtHotelBooking(TimeStampedModel):
    order_id = models.CharField(max_length=200)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Unverified Pay At Hotel Booking'
        verbose_name_plural = 'Unverified Pay At Hotel Bookings'

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s' % self.order_id

    def __str__(self):
        return '%s' % self.order_id

    @classmethod
    def create(cls, order_id):
        unverified_pay_at_hotel_booking = cls(order_id=order_id)
        return unverified_pay_at_hotel_booking
