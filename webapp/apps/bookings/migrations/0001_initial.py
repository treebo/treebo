# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dbcommon', '0007_auto_20160215_0840'),
    ]

    state_operations = [
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('order_id', models.CharField(max_length=200)),
                ('checkin_date', models.DateField()),
                ('checkout_date', models.DateField()),
                ('adult_count', models.PositiveIntegerField()),
                ('child_count', models.PositiveIntegerField()),
                ('total_amount', models.DecimalField(max_digits=20, decimal_places=10)),
                ('guest_name', models.CharField(max_length=200)),
                ('guest_email', models.EmailField(max_length=200)),
                ('guest_mobile', models.CharField(max_length=20)),
                ('is_complete', models.BooleanField(default=False)),
                (
                    'payment_amount',
                    models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('is_traveller', models.BooleanField(default=False)),
                ('booking_code', models.CharField(max_length=200, null=True, blank=True)),
                ('comments', models.TextField(default='')),
                ('coupon_apply', models.BooleanField(default=False)),
                ('coupon_code', models.CharField(max_length=200, null=True, blank=True)),
                ('payment_mode',
                 models.CharField(default='Not Paid', max_length=200, null=True, blank=True)),
                ('discount', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                (
                    'group_code',
                    models.CharField(default='', max_length=200, null=True, blank=True)),
                ('status',
                 models.IntegerField(default=1, choices=[(1, 'Confirm'), (0, 'Cancel')])),
                ('hotel',
                 models.ForeignKey(related_name='hotelbookings', blank=True, to='dbcommon.Hotel',
                                   null=True, on_delete=models.DO_NOTHING)),
                ('room',
                 models.ForeignKey(related_name='roombookings', blank=True, to='dbcommon.Room',
                                   null=True, on_delete=models.DO_NOTHING)),
                ('user_id', models.ForeignKey(related_name='userbookings', blank=True,
                                              to=settings.AUTH_USER_MODEL, null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('reservation_id', models.CharField(max_length=200)),
                ('reservation_status', models.CharField(max_length=200)),
                ('reservation_checkin_date', models.DateField()),
                ('reservation_checkout_date', models.DateField()),
                ('adult_count', models.PositiveIntegerField(default=0)),
                ('child_count', models.PositiveIntegerField(default=0)),
                ('room_unit', models.PositiveIntegerField()),
                ('total_amount', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('room_block_code',
                 models.CharField(default='', max_length=100, null=True, blank=True)),
                (
                    'guest_name',
                    models.CharField(default='', max_length=200, null=True, blank=True)),
                ('guest_email',
                 models.EmailField(default='', max_length=200, null=True, blank=True)),
                ('guest_mobile',
                 models.CharField(default='', max_length=200, null=True, blank=True)),
                ('reservation_create_time', models.DateTimeField()),
                ('hotel', models.ForeignKey(blank=True, to='dbcommon.Hotel', null=True, on_delete=models.DO_NOTHING)),
                ('room', models.ForeignKey(blank=True, to='dbcommon.Room', null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RoomBooking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('reservation_id', models.CharField(max_length=200)),
                ('price',
                 models.DecimalField(null=True, max_digits=8, decimal_places=2, blank=True)),
                ('Booking', models.ForeignKey(related_name='bookingroombooking', blank=True,
                                              to='bookings.Booking', null=True, on_delete=models.DO_NOTHING)),
                ('room',
                 models.ForeignKey(related_name='roomroombooking', blank=True, to='dbcommon.Room',
                                   null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(state_operations=state_operations)
    ]
