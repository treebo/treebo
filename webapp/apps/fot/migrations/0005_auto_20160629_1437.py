# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0022_booking_is_audit'),
        ('fot', '0004_fotcredits'),
    ]

    operations = [
        migrations.CreateModel(
            name='FotConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('start_day_threshold', models.PositiveIntegerField(default=1)),
                ('end_day_threshold', models.PositiveIntegerField(default=42)),
                ('max_guests_allowed', models.PositiveIntegerField(default=2)),
                ('score_to_pass', models.PositiveIntegerField(default=60)),
                ('max_credits_to_be_redeemed', models.PositiveIntegerField(default=1000)),
                ('credit_equivalent_per_night', models.PositiveIntegerField(default=1000)),
                ('coupon_expiry', models.PositiveIntegerField(default=60)),
            ],
        ),
        migrations.CreateModel(
            name='FotHotelBlackOut',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('blackout_start_time', models.DateTimeField()),
                ('blackout_end_time', models.DateTimeField()),
                ('status', models.CharField(default=b'ACTIVE', max_length=100,
                                            choices=[(b'ACTIVE', b'ACTIVE'),
                                                     (b'BLACKOUT', b'BLACKOUT')])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FotSignupAnswers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('option', models.CharField(max_length=300)),
                ('score', models.IntegerField(default=0)),
                ('value', models.CharField(default=b'A', max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='FotSignupQuestions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('question', models.TextField(null=True)),
                ('question_type', models.CharField(max_length=100, choices=[('TEXT', 'TEXT'), (
                    'CHECKBOX', 'CHECKBOX'), ('RADIO', 'RADIO'), ('DROPDOWN', 'DROPDOWN'), (
                    'TEXTAREA',
                    'TEXTAREA')])),
                ('section', models.CharField(default='PERSONAL', max_length=100,
                                             choices=[('ABOUT_FOT', 'ABOUT_FOT'),
                                                      ('PROBLEM_SOLVING', 'PROBLEM_SOLVING'),
                                                      ('PERSONAL', 'PERSONAL')])),
            ],
        ),
        migrations.RemoveField(
            model_name='fot',
            name='credits_consumed',
        ),
        migrations.RemoveField(
            model_name='fothotel',
            name='credits_per_week',
        ),
        migrations.AddField(
            model_name='fot',
            name='about_yourself',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='answers_answered',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='assets',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='audit_duration',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='city',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='confirmation_key',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='current_address',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='feedbacks_submitted',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='fot',
            name='free_nights_remaining',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='fot',
            name='friend_name',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='future_audits',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='fot',
            name='marital_status',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='no_shows',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='fot',
            name='notification',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='notification_status',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='profession',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='qualified_feedbacks',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='fot',
            name='questions_asked',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='socail_web_profile',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='source_website',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='total_audits_scheduled',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='fot',
            name='total_free_nights',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='fot',
            name='travel_experience',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='user_credits',
            field=models.PositiveIntegerField(default=0, db_column=b'credits_consumed'),
        ),
        migrations.AddField(
            model_name='fot',
            name='user_slots',
            field=models.PositiveIntegerField(default=2),
        ),
        migrations.AddField(
            model_name='fot',
            name='why_treebo',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='fothotel',
            name='credits_per_audit',
            field=models.PositiveIntegerField(default=1000, db_column='credits_per_week'),
        ),
        migrations.AddField(
            model_name='fothotel',
            name='interval_between_slots',
            field=models.CharField(default='WEEK', max_length=100,
                                   choices=[('WEEK', 'WEEK'), ('MONTH', 'MONTH'),
                                            ('YEAR', 'YEAR')]),
        ),
        migrations.AddField(
            model_name='fothotel',
            name='slots_allowed',
            field=models.PositiveIntegerField(default=1),
        ),
        migrations.AddField(
            model_name='fotreservationsrequest',
            name='booking',
            field=models.ForeignKey(to='bookings.Booking', null=True, on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='fotreservationsrequest',
            name='check_out_date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='fotreservationsrequest',
            name='no_of_guests',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='fot',
            name='score',
            field=models.DecimalField(default=0, null=True, max_digits=8, decimal_places=2,
                                      blank=True),
        ),
        migrations.AlterField(
            model_name='fot',
            name='status',
            field=models.CharField(default='IN_PROGRESS', max_length=100,
                                   choices=[('FAILED', 'FAILED'),
                                            ('NOT_CONFIRMED', 'NOT_CONFIRMED'),
                                            ('PASSED', 'PASSED'), ('BLOCKED', 'BLOCKED'),
                                            ('IN_PROGRESS', 'IN_PROGRESS')]),
        ),
        migrations.AlterField(
            model_name='fot',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, db_column='user_id', on_delete=models.DO_NOTHING),
        ),
        migrations.AlterField(
            model_name='fotcredits',
            name='earned',
            field=models.PositiveIntegerField(),
        ),
        migrations.AlterField(
            model_name='fothotel',
            name='status',
            field=models.CharField(default='ACTIVE', max_length=100,
                                   choices=[('ACTIVE', 'ACTIVE'), ('BLACKOUT', 'BLACKOUT')]),
        ),
        migrations.AlterField(
            model_name='fotreservationsrequest',
            name='check_in_date',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='fotreservationsrequest',
            name='status',
            field=models.CharField(default='PENDING', max_length=100,
                                   choices=[('NOSHOW', 'NOSHOW'), ('CHECKEDOUT', 'CHECKEDOUT'),
                                            ('CANCEL_PENDING', 'CANCEL_PENDING'),
                                            ('CHECKEDIN', 'CHECKEDIN'),
                                            ('CANCELLED', 'CANCELLED'),
                                            ('SCHEDULED', 'SCHEDULED'), ('PENDING', 'PENDING'),
                                            ('FEEDBACK_NOT_SUBMITTED', 'FEEDBACK_NOT_SUBMITTED'),
                                            ('FEEDBACK_SUBMITTED', 'FEEDBACK_SUBMITTED')]),
        ),
        migrations.AlterField(
            model_name='fotreservationsrequest',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, db_column='user_id', on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='fotsignupanswers',
            name='question',
            field=models.ForeignKey(to='fot.FotSignupQuestions', on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='fothotelblackout',
            name='fot_hotel',
            field=models.ForeignKey(to='fot.FotHotel', on_delete=models.DO_NOTHING),
        ),
    ]
