__author__ = 'varunachar'

ERROR_STATUS = "error"


def get(key):
    return __code.get(key, None)


__code = dict()

INVALID_CREDENTIALS = '101'
__code[INVALID_CREDENTIALS] = {
    'code': INVALID_CREDENTIALS,
    'msg': 'Invalid Username or Password'
}

INACTIVE_USER = '102'
__code[INACTIVE_USER] = {
    'code': INACTIVE_USER,
    'msg': 'Inactive user'
}

UNKNOWN_USER = '103'
__code[UNKNOWN_USER] = {
    'code': UNKNOWN_USER,
    'msg': 'Unknown email id'
}

INVALID_LOGIN_DATA = '104'
__code[INVALID_LOGIN_DATA] = {
    'code': INVALID_LOGIN_DATA,
    'msg': 'Invalid login information entered'
}

ALREADY_EXISTING_USER = '105'
__code[ALREADY_EXISTING_USER] = {
    'code': ALREADY_EXISTING_USER,
    'msg': 'User already exists'
}

INVALID_EMAIL_HASH = '106'
__code[INVALID_EMAIL_HASH] = {
    'code': INVALID_EMAIL_HASH,
    'msg': 'Invalid hash value'
}
INCORRECT_PASSWORD = '107'
__code[INCORRECT_PASSWORD] = {
    'code': INCORRECT_PASSWORD,
    'msg': 'Entered old password is invalid'

}

INVALID_BOOKING = '108'
__code[INVALID_BOOKING] = {
    'code': INVALID_BOOKING,
    'msg': 'No such booking found for user'
}

INVALID_COUPON_CODE = '109'
__code[INVALID_COUPON_CODE] = {
    'code': INVALID_COUPON_CODE,
    'msg': 'Coupon code is invalid'
}

UNABLE_TO_ADD_TO_CART = '110'
__code[UNABLE_TO_ADD_TO_CART] = {
    'code': UNABLE_TO_ADD_TO_CART,
    'msg': 'Error while trying to add to cart'
}

INVALID_ORDER_ID = '111'
__code[INVALID_ORDER_ID] = {
    'code': INVALID_ORDER_ID,
    'msg': 'No booking found for order id'
}

NO_ORDER_ID_FOUND = '112'
__code[NO_ORDER_ID_FOUND] = {
    'code': NO_ORDER_ID_FOUND,
    'msg': 'Please supply an order id'
}

INVALID_DATES = '113'
__code[INVALID_DATES] = {
    'code': INVALID_DATES,
    'msg': "Check in date is later than check out date"
}

INVALID_MOBILE_NO = '114'
__code[INVALID_MOBILE_NO] = {
    'code': INVALID_MOBILE_NO,
    'msg': 'Please supply a valid mobile number'
}

INVALID_GUESTS = '115'
__code[INVALID_GUESTS] = {
    'code': INVALID_GUESTS,
    'msg': "No of guests should be at least 1"
}

INVALID_ROOMS = '116'
__code[INVALID_ROOMS] = {
    'code': INVALID_ROOMS,
    'msg': "No of rooms should be at least 1"
}

INVALID_DATA = '117'
__code[INVALID_DATA] = {
    'code': INVALID_DATA,
    'msg': "Invalid Data Received"
}
NO_HOTEL_ROOM_ID_SUPPLIED = '118'
__code[NO_HOTEL_ROOM_ID_SUPPLIED] = {
    'code': NO_HOTEL_ROOM_ID_SUPPLIED,
    'msg': 'Invalid hotel id or room type supplied'
}
INVALID_GUESTS_SUPPLIED = '119'
__code[INVALID_GUESTS_SUPPLIED] = {
    'code': INVALID_GUESTS_SUPPLIED,
    'msg': 'No of guests is greater than room capacity'
}
PASSWORDS_DO_NOT_MATCH = '120'
__code[PASSWORDS_DO_NOT_MATCH] = {
    'code': PASSWORDS_DO_NOT_MATCH,
    'msg': 'Passwords do not match'
}

PASSWORD_NOT_ENTERED = '121'
__code[PASSWORD_NOT_ENTERED] = {
    'code': PASSWORD_NOT_ENTERED,
    'msg': 'Please enter password in both fields'
}

PROMOTIONS_NOT_PRESENT = '122'
__code[PROMOTIONS_NOT_PRESENT] = {
    'code': PROMOTIONS_NOT_PRESENT,
    'msg': 'Promotions were not provided'
}

INVALID_EMAIL_ORDER = '123'
__code[INVALID_EMAIL_ORDER] = {
    'code': INVALID_EMAIL_ORDER,
    'msg': 'The order was not made using the provided email'
}

OLD_NEW_PASSWORDS_MATCH = '124'
__code[OLD_NEW_PASSWORDS_MATCH] = {
    'code': OLD_NEW_PASSWORDS_MATCH,
    'msg': 'Old and new passwords cannot be the same'
}

INVALID_CHECK_IN_CHECKOUT_DATE = '125'
__code[INVALID_CHECK_IN_CHECKOUT_DATE] = {
    'code': INVALID_CHECK_IN_CHECKOUT_DATE,
    'msg': 'Invalid check in/checkout dates'
}

INVALID_TOTAL_COST = '126'
__code[INVALID_TOTAL_COST] = {
    'code': INVALID_TOTAL_COST,
    'msg': 'Invalid total cost'
}

INVALID_EMAIL = '127'
__code[INVALID_EMAIL] = {
    'code': INVALID_EMAIL,
    'msg': 'Please enter a valid email'
}

NO_ROOMS_AVAILABLE_FOR_THE_DATES = '128'
__code[NO_ROOMS_AVAILABLE_FOR_THE_DATES] = {
    'code': NO_ROOMS_AVAILABLE_FOR_THE_DATES,
    'msg': 'No rooms are available for the selected dates'
}
INVALID_COUPON = '129'
__code[INVALID_COUPON] = {
    'code': INVALID_COUPON,
    'msg': 'Error applying coupon.'
}

CHECK_DATE_GREATER_THAN_CURRENT_DATE = '130'
__code[CHECK_DATE_GREATER_THAN_CURRENT_DATE] = {
    'code': CHECK_DATE_GREATER_THAN_CURRENT_DATE,
    'msg': 'Check In should be after current date'
}

# availability API error code series starts from 4001 onwards.
INVALID_ROOM_CONFIG_KEY = '4001'
__code[INVALID_ROOM_CONFIG_KEY] = {
    'code': INVALID_ROOM_CONFIG_KEY,
    'msg': 'roomconfig missing.'
}

INVALID_DATE_KEY = '4002'
__code[INVALID_DATE_KEY] = {
    'code': INVALID_DATE_KEY,
    'msg': 'checkin or checkout missing.'
}

INVALID_HOTEL_ID_KEY = '4003'
__code[INVALID_HOTEL_ID_KEY] = {
    'code': INVALID_HOTEL_ID_KEY,
    'msg': 'hotel_id missing.'

}

INVALID_ROOM_TYPE_KEY = '4004'
__code[INVALID_ROOM_TYPE_KEY] = {
    'code': INVALID_ROOM_TYPE_KEY,
    'msg': 'roomtype missing.'
}

INVALID_HOTEL_ID = '4005'
__code[INVALID_HOTEL_ID] = {
    'code': INVALID_HOTEL_ID,
    'msg': 'Invalid Hotel Id.'

}

INVALID_ROOM_TYPE = '4006'
__code[INVALID_ROOM_TYPE] = {
    'code': INVALID_ROOM_TYPE,
    'msg': 'Invalid Room Type.'
}

INVALID_DATE_FORMAT = '4007'
__code[INVALID_DATE_FORMAT] = {
    'code': INVALID_DATE_FORMAT,
    'msg': 'Invalid Date Format.'
}

INVALID_ROOM_CONFIG = '4008'
__code[INVALID_ROOM_CONFIG] = {
    'code': INVALID_ROOM_CONFIG,
    'msg': 'Invalid Room Configuration.'
}
INVALID_COUPON_CODE_KEY = '4009'
__code[INVALID_COUPON_CODE_KEY] = {
    'code': INVALID_COUPON_CODE_KEY,
    'msg': 'couponcode missing.'
}
INVALID_BID_KEY = '4010'
__code[INVALID_BID_KEY] = {
    'code': INVALID_BID_KEY,
    'msg': 'bid missing.'
}

INVALID_ORDER_ID_KEY = '4011'
__code[INVALID_ORDER_ID_KEY] = {
    'code': INVALID_ORDER_ID_KEY,
    'msg': 'order_id missing.'
}

INVALID_CONTENT_KEY = '4012'
__code[INVALID_CONTENT_KEY] = {
    'code': INVALID_CONTENT_KEY,
    'msg': 'key is missing.'
}

INVALID_GSTIN_NUMBER = 4013
__code[INVALID_GSTIN_NUMBER] = {
    'code': INVALID_GSTIN_NUMBER,
    'msg': 'GSTIN is invalid.'
}
