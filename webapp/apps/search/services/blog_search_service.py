from web_sku.catalog.services.sku_request_service import SkuRequestService
from apps.hotels.repositories.hotels_repository import HotelsRepository
from web_sku.pricing.services.pricing_service import PricingService
from datetime import datetime, timedelta
from django.conf import settings
from django.utils.text import slugify
from apps.hotels.serializers.image_serializer import ImageSerializer


class BlogSearchService:
    hotels_repository = HotelsRepository()
    sku_request_service = SkuRequestService()
    pricing_service = PricingService()

    def get_hotels_with_price(self, city):

        hotels_with_price = []
        cs_ids, hotel_ids, ta_review_hotel_id = [], [], {}
        hotels = self.get_blog_hotels(city)
        for hotel in hotels:
            hotel_ids.append(hotel.id)
            cs_ids.append(hotel.cs_id)
        ta_reviews = self.hotels_repository.get_ta_rating_and_rating_url(hotel_ids=hotel_ids)
        for review in ta_reviews:
            ta_review_hotel_id[review['hotel_ta_mapping__hotel_id']] = {
                'overall_rating': review['overall_rating'],
                'url_overall_rating_image': review['url_overall_rating_image']
            }
        couple_friendly_hotel_ids = self.hotels_repository.get_hotels_having_policy_type(
            hotel_ids=hotel_ids, policy_type='is_couple_friendly')

        prices = self.get_prices(cs_ids=cs_ids)

        for hotel in hotels:
            images = hotel.image_set.all()[:1]
            image_serializer = ImageSerializer(instance=images, many=True)
            image_set = image_serializer.data
            for image in image_set:
                image_url = image['url']

            hotels_with_price.append({
                "hotel_name": hotel.name,
                "hotel_rating": 3,
                "hotel_image": image_url,
                "hotel_url": settings.SITE_HOST_NAME + 'hotels-in-' + slugify(hotel.city.slug) + '/'
                             + slugify(hotel.name) + '-' + slugify(hotel.locality.name) + '-' +
                             str(hotel.id),
                "tripadvisor_rating": ta_review_hotel_id[hotel.id]['overall_rating'],
                "tripadvisor_image": ta_review_hotel_id[hotel.id]['url_overall_rating_image'],
                "price": prices[hotel.cs_id],
                "is_couple_friendly": True if hotel.id in couple_friendly_hotel_ids else False,
                "is_pay_at_hotel_allowed": True,
                "is_quality_guarantee": True,
                "is_free_breakfast_available": True,
                "is_free_cancellation_available": True
            })

        return hotels_with_price

    def get_blog_hotels(self, city):

        hotels_ids, hotel_id_dict = [], {}
        hotels = self.hotels_repository.get_city_hotels(city=city)

        for hotel in hotels:
            hotels_ids.append(hotel.id)
            hotel_id_dict[hotel.id] = hotel

        ta_hotels_ids = self.hotels_repository.get_top_rated_ta_hotels(hotel_ids=hotels_ids,
                                                                       rating_gte=4, reviews_gte=10)

        if len(ta_hotels_ids) < 4:
            ta_hotels_ids = self.hotels_repository.get_top_rated_ta_hotels(hotel_ids=hotels_ids,
                                                                           rating_gte=4)
            if len(ta_hotels_ids) < 4:
                ta_hotels_ids = self.hotels_repository.get_top_rated_ta_hotels(hotel_ids=hotels_ids,
                                                                               rating_gte=3.5)

        blog_hotels = [hotel_id_dict[ta_hotel_id] for ta_hotel_id in ta_hotels_ids[:4]]

        return blog_hotels

    def get_prices(self, cs_ids):

        request_skus = self.sku_request_service.get_web_request_skus('2-0', cs_ids)
        cs_id_price = {}

        pricing_request_data = {
            'skus': request_skus,
            'from_date': datetime.today().strftime('%Y-%m-%dT%H:%M:%S.000Z'),
            'to_date': (datetime.today() + timedelta(days=1)).strftime('%Y-%m-%dT%H:%M:%S.000Z'),
            'mode': 'BROWSE',
            'policy': ['rp'],
            'apply_coupon': True,
            'coupon_code': '',
            'channel': 'direct',
            'subchannel': 'website-direct',
            'application': 'website',
            'utm': {'source': 'direct'},
            'customer_info': {},
            'pricing_type': 'cheapest'
        }
        sku_availability = {}
        for cs_id in request_skus:
            skus = {}
            for sku_dict in request_skus[cs_id]:
                skus[next(iter(sku_dict['sku']))] = 1

            sku_availability[cs_id] = skus

        room_wise_price = self.pricing_service.get_room_wise_price_from_availability(
            pricing_request_data, sku_availability)

        for cs_id in room_wise_price:

            prices = room_wise_price[cs_id]['room_wise_price']
            for room_type in prices:
                price_dict = prices[room_type]
                for dict in price_dict:
                    final_price = dict['total_price_breakup']['final_price']
                    cs_id_price[cs_id] = final_price

        return cs_id_price
