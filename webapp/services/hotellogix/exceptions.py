from common.exceptions.treebo_exception import TreeboException
from services.send_exception_mail import send_exception_mail


class EmptyRateIdException(TreeboException):
    def __init__(self, *args):
        super(EmptyRateIdException, self).__init__(*args)
        self.code = 600


class AddToCartException(TreeboException):
    def __init__(self, *args):
        super(AddToCartException, self).__init__(*args)
        self.code = 601


class InitiateBookingException(TreeboException):
    def __init__(self, *args):
        super(InitiateBookingException, self).__init__(*args)
        self.code = 604
        send_exception_mail(self)


class SaveBookingException(TreeboException):
    def __init__(self, *args):
        super(SaveBookingException, self).__init__(*args)
        self.code = 602


class ConfirmBookingException(TreeboException):
    def __init__(self, *args):
        super(ConfirmBookingException, self).__init__(*args)
        self.code = 603
