# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0007_auto_20160215_0840'),
    ]

    database_operations = [
        migrations.AlterModelTable('ConstraintKeywords', 'discounts_constraintkeywords'),
        migrations.AlterModelTable('ConstraintOperators', 'discounts_constraintoperators'),
        migrations.AlterModelTable('DiscountCoupon', 'discounts_discountcoupon'),
        migrations.AlterModelTable('ConstraintExpression', 'discounts_constraintexpression'),
        migrations.AlterModelTable('DiscountCouponConstraints',
                                   'discounts_discountcouponconstraints')
    ]

    state_operations = [
        migrations.DeleteModel('ConstraintKeywords'),
        migrations.DeleteModel('ConstraintOperators'),
        migrations.DeleteModel('DiscountCoupon'),
        migrations.DeleteModel('ConstraintExpression'),
        migrations.DeleteModel('DiscountCouponConstraints')
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            database_operations=database_operations,
            state_operations=state_operations)
    ]
