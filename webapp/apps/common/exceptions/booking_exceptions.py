from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR
from apps.common import error_codes
from common.exceptions.treebo_exception import TreeboException


class BookingNotFoundException(TreeboException):
    def __init__(self, developer_message):
        super(BookingNotFoundException, self).__init__()
        self.message = 'Unable to find booking.'
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + "4002"


class BookingRequestNotFoundException(TreeboException):
    def __init__(self, developer_message):
        super(BookingRequestNotFoundException, self).__init__()
        self.message = 'Unable to find booking.'
        self.developer_message = developer_message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR
        self.error_code = self.app_type + "4007"


class BookingCodeNotFoundException(TreeboException):
    def __init__(self):
        super(BookingCodeNotFoundException, self).__init__()
        self.message = 'Unable to booking code.'
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + "4006"


class InvalidBookingRequestException(TreeboException):
    def __init__(self, developer_message):
        super(InvalidBookingRequestException, self).__init__()
        error = error_codes.get(error_codes.INVALID_REQUEST)
        self.extra_payload = error['msg']
        self.message = error['msg']
        self.description = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class BookingInitiateException(TreeboException):
    def __init__(self, developer_message):
        super(BookingInitiateException, self).__init__()
        error = error_codes.get(error_codes.BOOKING_INITIATE_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class BookingCreationException(TreeboException):
    def __init__(self, developer_message):
        super(BookingCreationException, self).__init__()
        error = error_codes.get(error_codes.BOOKING_CREATION_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class DuplicateBookingCreationException(TreeboException):
    def __init__(self, developer_message):
        super(DuplicateBookingCreationException, self).__init__()
        error = error_codes.get(error_codes.DUPLICATE_BOOKING_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class CommittedPriceNotSetException(TreeboException):
    def __init__(self, developer_message):
        super(CommittedPriceNotSetException, self).__init__()
        error = error_codes.get(error_codes.COMMITTED_PRICE_NOT_SET)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class BookingConfirmationException(TreeboException):
    def __init__(self, developer_message):
        super(BookingConfirmationException, self).__init__()
        error = error_codes.get(error_codes.BOOKING_CONFIRMATION_ERROR)
        self.message = error['msg']
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class PaymentGatewayNotFound(TreeboException):
    def __init__(self, gateway):
        super(PaymentGatewayNotFound, self).__init__()
        error = error_codes.get(error_codes.GATEWAY_NOT_FOUND)
        self.message = error['msg'] % gateway
        self.developer_message = ''
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class BookedHotelNotFound(TreeboException):
    def __init__(self, developer_message):
        super(BookedHotelNotFound, self).__init__()
        self.message = 'Unable to find Hotel corresponding to the booking.'
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + "4002"


class RoomStaysNotFound(TreeboException):
    def __init__(self, developer_message):
        super(RoomStaysNotFound, self).__init__()
        self.message = 'Room stays not found for the corresponding booking.'
        self.developer_message = developer_message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR
        self.error_code = self.app_type + "4002"


class RoomChargesNotFound(TreeboException):
    def __init__(self, developer_message):
        super(RoomStaysNotFound, self).__init__()
        self.message = 'Room charges not found for the corresponding booking.'
        self.developer_message = developer_message
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR
        self.error_code = self.app_type + "4002"
