import logging

from apps.bookings.models import PaymentOrder, Booking
from apps.checkout.dto.booking_confirmation_dto import BookingConfirmationV5
from apps.checkout.models import BookingRequest
from apps.checkout.service.checkout_handler import CheckoutHandler
from apps.checkout.service.confirmation_booking_handler import confirm_booking_handler, \
    confirm_booking
from apps.common import api_response
from apps.common.exceptions.booking_exceptions import InvalidBookingRequestException, \
    BookingNotFoundException
from apps.payments.service.payment_service_v2 import PaymentServiceV2
from apps.common import error_codes
from apps.checkout.constants import DebitModes
from common.exceptions.treebo_exception import TreeboValidationException
from apps.reward.service.reward import RewardService
from services.restclient.paymentclient import PaymentClient
from services.restclient.paymentservice_client import PaymentServiceClient
from apps.common.slack_alert import SlackAlertService as slack_alert
from django.db import transaction

logger = logging.getLogger(__name__)


class BookingConfirmationService():
    payment_client = PaymentClient()
    payment_service_client = PaymentServiceClient()
    checkout_handler = CheckoutHandler()
    payment_service = PaymentServiceV2()

    def confirm_paynow_booking(self, booking_confirmation_dto,
                               booking_request):
        is_already_verified = False
        order_id = booking_confirmation_dto['order_id']
        pg_payment_id = booking_confirmation_dto['pg_payment_id']
        ps_order_id = booking_confirmation_dto['ps_order_id']
        pg_payment_signature = booking_confirmation_dto['pg_payment_signature']
        pg_order_id = booking_confirmation_dto['pg_order_id']
        logger.info(
            "validate_razorpay_payment %s , order id %s, ps_order_id %s, booking req total "
            "amount %s", pg_payment_id, order_id, ps_order_id,
            booking_request.total_amount)
        if not self.is_payment_already_verified_from_payment_client(
            ps_order_id):
            self.payment_service_client.validate_payment(pg_payment_id,
                                                         pg_payment_signature,
                                                         pg_order_id,
                                                         ps_order_id)
            self.payment_client.update_payment_details(order_id, ps_order_id,
                                                       pg_payment_id)

            # need to fix this as well
            confirm_booking_handler(booking_request, status=BookingRequest.COMPLETED)
        else:
            logger.info(" payment %s is already verified ",
                        booking_confirmation_dto)
            is_already_verified = True
        return order_id, is_already_verified

    def confirm_paynow_booking_v5(self, user, booking_confirmation_dto,
                                  booking_request, auth_id=None):
        '''
        :param user:
        :param booking_confirmation_dto:
        :param booking_request:
        :param auth_id:
        :return: orderid, reason
        1)this methods calculates the amount to be deducted from wallet if applied and amount for which confirmation
        from the payment microservice has arrived(phonepe and amazonpay), from treebo website in case of razorpay.
        2) Validates the payment status and creates payment Order record (in case of razorpay validates the payment
        status by calling the payment service)
        3) Confirms the booking and updates the booking request as completed in case the payment status is success
        '''
        order_id = booking_confirmation_dto['order_id']
        ps_order_id = booking_confirmation_dto['ps_order_id']
        pg_payment_meta = booking_confirmation_dto['pg_meta']
        pg_payment_meta['gateway'] = booking_confirmation_dto['gateway']
        logger.info(
            "validate_razorpay_payment, ps_order_id %s, booking req total amount %s",
            ps_order_id, booking_request.total_amount)
        # handling the rare case when the wallet balance is enough to pay for the booking - no external payment required!
        ext_gateway_amount = booking_request.total_amount
        reason = 'wallet not applied'
        confirm_booking_required = True
        _, _, _, wallet_spendable_balance, _, _, debit_mode, _ = \
            RewardService().get_wallet_balance(user, booking_request.booking_channel)
        if booking_request.wallet_applied and debit_mode == DebitModes.POST_TAX:
            ext_gateway_amount = booking_request.total_amount - booking_request.wallet_deduction
        already_verified = False
        payment_order = None
        if ext_gateway_amount > 0:
            # in case of payment failure status the booking should not be confirmed hence
            # confirm_booking_required is false where as it is true by default
            already_verified, confirm_booking_required = self.payment_service.verify_order_from_paymentservice_and_update_paymentorder(
                ps_order_id, pg_payment_meta,
                booking_request.order_id)
        else:
            logger.info(
                "ext payment not found of rs %s for ps_order %s for order_id %s  ",
                ext_gateway_amount,
                ps_order_id,
                order_id)

        if already_verified:
            logger.info(
                " payment order is already verified %s for payment order %s ",
                already_verified,
                payment_order)
            return order_id, reason
        # the wallet payment part now...
        logger.info(
            " ext payment done now calling wallet handler for prepay for user %s bid %s  order %s ",
            user, booking_request.id,
            booking_request.order_id)

        wallet_payment_success = False
        logger.error('wallet applied for orderId: %s is %s', booking_request.order_id,
                     booking_request.wallet_applied)
        if booking_request.payment_offer_price:
            self.payment_service.create_payment_for_payment_offer(
                amount=booking_request.total_amount - booking_request.payment_offer_price,
                booking_order_id=booking_request.order_id,
                ps_payment_id="-".join((str(booking_request.id),
                                        str(booking_request.payment_offer_id)))
            )
        if booking_request.wallet_applied and confirm_booking_required:
            wallet_payment_success, payment_order, reason = \
                self.payment_service.one_step_pay(user,
                                                  amount=booking_request.wallet_deduction,
                                                  booking_order_id=booking_request.order_id,
                                                  particulars="Points used for booking",
                                                  auth_id=auth_id,
                                                  debit_mode=debit_mode)
            logger.info(
                "wallet payment done for prepay for user %s bid %s  order %s with wallet_"
                "payment_success %s  due to reason %s",
                user, booking_request.id,
                booking_request.order_id, wallet_payment_success, reason)

        booking_status = self.checkout_handler.get_booking_status(booking_request, user,
                                                                  wallet_payment_success,
                                                                  auth_id=auth_id or wallet_payment_success)
        logger.info(
            'booking_request status for order_id: {} is {}'.format(order_id, booking_status))

        logger.info(
            'data being passed to confirm booking and booking request for order id:{} is: '
            'confirm_booking_required: {} '
            'booking_status: {}'.format(order_id, confirm_booking_required, booking_status))
        # confirm_booking_required is false in case the payment status is failure hence we need not confirm the booking
        if confirm_booking_required:
            confirm_booking(booking_request, status=booking_status)
        return order_id, reason

    def is_payment_already_verified_from_payment_client(self, ps_order_id):
        payment_details = self.payment_client.get_payment_details(ps_order_id)
        payment_status = payment_details['status']
        if str(payment_status) == PaymentOrder.CAPTURED:
            return True
        return False

    def get_external_payment_gateway_share(self, booking_request):
        razorpay_share = float(booking_request.total_amount)
        if booking_request.wallet_deduction:
            razorpay_share = float(
                booking_request.total_amount) - float(booking_request.wallet_deduction)
        return razorpay_share

    def validate_booking_confiramtion_dto_v5(self, booking_confirmation_dto,
                                             booking_request):
        # checking if the razorpay share is non-zero

        logger.info('validating the payment for order_id %s',
                    booking_confirmation_dto.data['order_id'])
        razorpay_share = float(booking_request.total_amount)
        if booking_request.wallet_deduction:
            razorpay_share = float(
                booking_request.total_amount) - float(booking_request.wallet_deduction)
        invalid_inputs = ""
        is_valid_dto = True
        if not razorpay_share:
            return

        # validating for razorpay specific constraints
        if not booking_confirmation_dto.data['ps_order_id'] or not isinstance(
            booking_confirmation_dto.data['ps_order_id'], str):
            is_valid_dto = False
            invalid_inputs += (", " if invalid_inputs else "") + "ps_order_id"

        if not isinstance(booking_confirmation_dto.data['pg_meta'], type({})):
            is_valid_dto = False
            invalid_inputs += "pg_meta"
            logger.error(
                "validate_booking_confiramtion_dto_v5 failed razorpay_share %s  total amount %s  invalid_inputs %s  ",
                razorpay_share,
                booking_request.total_amount,
                invalid_inputs)
        if not is_valid_dto:
            raise TreeboValidationException("Invalid " + invalid_inputs)

    @transaction.atomic
    def confirm_booking(self, user, pg_meta, order_id, ps_order_id, gateway, auth_id=None):
        booking_confirmation_dto = BookingConfirmationV5(data={'order_id': order_id,
                                                               'ps_order_id': ps_order_id,
                                                               'pg_meta': pg_meta,
                                                               'gateway': gateway})

        if not booking_confirmation_dto.is_valid():
            logger.exception("Invalid request booking confirmation for order %s ", order_id)
            slack_alert.send_slack_alert_for_exceptions(status=400, request_param=str(order_id),
                                                        message="Invalid request for booking confirmation",
                                                        class_name=self.__class__.__name__)
            return api_response.APIResponse.error_response_from_error_code(
                error_codes.INVALID_CONFIRM_BOOKING_REQUEST,
                api_name=BookingConfirmationService.__name__)

        booking = Booking.objects.select_for_update().filter(order_id=order_id).last()
        if not booking:
            logger.error("Booking not found for order_id {0}".format(order_id))
            slack_alert.send_slack_alert_for_exceptions(status=400, request_param=str(order_id),
                                                        message="Booking not found for order_id:",
                                                        class_name=self.__class__.__name__)
            raise BookingNotFoundException("Booking not found for order_id: {0}".format(order_id))

        booking_request = BookingRequest.objects.filter(booking_id=booking.id).last()
        if not booking_request:
            logger.error("Booking request not found for booking_id {0}".format(booking.id))
            slack_alert.send_slack_alert_for_exceptions(status=400, request_param=str(booking.id),
                                                        message="Booking request not found for booking_id:",
                                                        class_name=self.__class__.__name__)
            raise InvalidBookingRequestException(
                "Booking request not found for booking_id: {0}".format(booking.id))

        # custom validation here
        self.validate_booking_confiramtion_dto_v5(booking_confirmation_dto, booking_request)

        order_id, reason = self.confirm_paynow_booking_v5(user,
                                                          booking_confirmation_dto.data,
                                                          booking_request, auth_id)
        return order_id, reason, booking_request
