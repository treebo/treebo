class HotelDoesNotExist(Exception):
    pass


class NoImageForHotelExist(Exception):
    pass


class HTTPException(Exception):
    pass


class CsIDNotFoundError(Exception):
    def __init__(self, message, status):
        self.status = status
        self.message = message


class ExternalClientException(Exception):
    error_code = "0001"
    message = "Something went wrong!"

    def __init__(self, description=None, extra_payload=None):
        self.description = description
        self.extra_payload = extra_payload

    def __str__(self):
        return "exception: error_code=%s message=%s description=%s" % (
            self.error_code,
            self.message,
            self.description,
        )

    def with_description(self, description):
        self.description = description
        return self

    @property
    def code(self):
        return "0501" + self.error_code
