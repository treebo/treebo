from django.conf.urls import url

from apps.auth.api.v1.login import LoginAPI, FacebookLoginAPI, GooglePlusLoginAPI
from apps.auth.api.v1.pms_user_password_update import PmsUserPasswordUpdateAPI
from apps.auth.api.v1.register import RegisterAPI, VerifyUserAPI
from apps.auth.api.v1.pms_user_creation import PmsUserCreationAPI
from apps.profiles.api.v1.change_password import ChangePasswordAPI
from apps.profiles.api.v1.forgot_password import ForgotPasswordAPI
from django.contrib.auth.decorators import login_required
from apps.auth.api.v1.otp_api import OtpIsVerifiedAPI

urlpatterns = [
    url(r'^login/$', LoginAPI.as_view(), name="login-api"),
    # REFACTOR
    #url(r'^logout/$', LogoutAPI.as_view(), name="logout-api"),
    url(r'^fbLogin/$', FacebookLoginAPI.as_view(), name="fb-login-api"),
    url(r'^googleLogin/$',
        GooglePlusLoginAPI.as_view(),
        name="google-login-api"),
    url(r'^register/$', RegisterAPI.as_view(), name="register-api"),
    url(r"^verify/$", VerifyUserAPI.as_view(), name="verify-user"),
    # url(r"^otp/$", UserOtpAPI.as_view(), name="user-otp"),
    # REFACTOR
    #url(r"^otp/$", OtpAPI.as_view(), name="send-otp"),
    url(r"^otp/isverified/$",
        OtpIsVerifiedAPI.as_view(),
        name="is_verified-otp"),
    # REFACTOR
    #url(r"^otp/verify/$", OtpVerifyAPI.as_view(), name="verify-otp"),
    # REFACTOR
    #url(r'^reset-password/?$', ResetPasswordAPI.as_view(), name="reset-password-api"),
    url(r'^change-password/?$', login_required(ChangePasswordAPI.as_view(), redirect_field_name="referrer"),
        name="change-password-api"),
    url(r'^forgot-password/?$',
        ForgotPasswordAPI.as_view(),
        name="forgot-password-api"),
    url(r'^pms-user-creation',
        PmsUserCreationAPI.as_view(),
        name="pms-user-creation"),
    url(r'^pms-user-password-update',
        PmsUserPasswordUpdateAPI.as_view(),
        name="pms-user-password-update")
]
