'use strict';

import $ from 'jquery';
import {Observable} from "rx";
import _ from 'underscore';
import { ReferralShare } from './widgets/share';
import searchWidget from './widgets/search';
import BaseView from './treebolibs/baseview';
import parsley from 'parsleyjs';
import logger from './treebolibs/logger';
import treejax from './treebolibs/treejax';

import * as toastr from 'toastr';
import classNames from 'classnames';
import React from 'react';
import {render} from 'react-dom';
import { createStore, combineReducers } from 'redux';
import Calendar from './libs/calendar';
import Clipboard from 'clipboard';
import cookies from 'js-cookie';
import '../css/account.less';


const moment = require('moment');
const BookingHistoryView = Object.create(BaseView);

let $loader = $("#commonLoader");

function emailLinkHandler(){
    $(".booking-history-email-pdf").toggleClass("dd-open");
};

let BookingDetail = React.createClass({

    getInitialState(){
        return {
            bookingDetailObj: _.extend({},this.props.bookingDetailObj),
            bookingListVisible:false,
            bookingDetailVisible:true,
        }
    },

    backToBookingHandler(){
        this.props.backToHistory();
    },
    componentWillReceiveProps(nextProps){
        this.setState({
            bookingDetailObj:nextProps.bookingDetailObj,
        })
    },
    cancelBookingHandler(){
        const cancel_order_id = this.props.bookingDetailObj.order_id;
        this.props.onCancelDetail(cancel_order_id);
    },
    emailHandler(){
         $(".booking-history-sendPdf-email").on('click', emailLinkHandler());
        let submitpdfEvent = Rx.Observable.fromEvent($('#bookingHistoryPdfmailSubmit'), 'click');
        let submitpdfsubscription = submitpdfEvent.subscribe(e => {
            e.preventDefault();
            const pdfParsley = $('#bookingHistorySendPdfForm').parsley();
            pdfParsley.validate();
            if(pdfParsley.isValid()){
                methods.sendEmail();
            };
        });
    },

    render(){
        const booking_detail = classNames({
            hide: !this.props.visible,
            hotel: true,
            'hotel-detail-page':true
        });
        if(_.isEmpty(this.state.bookingDetailObj)){
            return <div className=""></div>;
        }
        // methods.init();
        let rooms = this.state.bookingDetailObj.room_config.room_count;
        let room_type = this.state.bookingDetailObj.room_config.room_type;
        let adults = this.state.bookingDetailObj.room_config.no_of_adults;
        let children = this.state.bookingDetailObj.room_config.no_of_childs;
        let status = this.state.bookingDetailObj.status;
        let payment= this.state.bookingDetailObj.paid_at_hotel;
        let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
        let paymentStr = (payment == 1) ? "To be paid at hotel" : "Already paid";
        let hotel_address = `${this.state.bookingDetailObj.hotel_detail.locality}, ${this.state.bookingDetailObj.hotel_detail.city}`;
        const checkIndateFormat = moment(this.state.bookingDetailObj.checkin_date).format("DD MMM'YY");
        const checkOutdateFormat = moment(this.state.bookingDetailObj.checkout_date).format("DD MMM'YY");
        const chkInDate = new Date(this.state.bookingDetailObj.checkin_date);
        var chkInDay = chkInDate.getDay();
        var chkInDay = `${days[chkInDay]}, `;
        const chkOutDate = new Date(this.state.bookingDetailObj.checkout_date);
        var chkOutDay = chkOutDate.getDay();
        var chkOutDay = `${days[chkOutDay]}, `;
        let roomPrice = this.state.bookingDetailObj.payment_detail.room_price;
        let taxPrice = this.state.bookingDetailObj.payment_detail.tax;
        let discountPrice = this.state.bookingDetailObj.payment_detail.discount;
        let totalPrice = this.state.bookingDetailObj.payment_detail.total;
        let paidPrice = this.state.bookingDetailObj.payment_detail.paid_amount;
        const flag = this.state.bookingDetailObj.flag;
        let cancellation_date = this.state.bookingDetailObj.cancel_date;
        const audit_status = this.state.bookingDetailObj.audit;
        let hotelName = (this.state.bookingDetailObj.hotel_name).toUpperCase();
        let part_pay_link = this.state.bookingDetailObj.partial_payment_link;
        let part_pay_amount = this.state.bookingDetailObj.partial_payment_amount;
        let audit_div;
        let partpay_div;
        let audit_amount_show_detail;
        let amount_for_booking;

        roomPrice = Math.ceil(roomPrice * 100)/100;
        taxPrice = Math.ceil(taxPrice * 100)/100;
        discountPrice = Math.ceil(discountPrice * 100)/100;
        totalPrice = Math.ceil(totalPrice * 100)/100;
        paidPrice = Math.ceil(paidPrice * 100)/100;

        amount_for_booking = (payment == 1) ? (totalPrice - paidPrice) : totalPrice;

        if(audit_status) {
            audit_div = (<div className="pages__audit-detail-status text-center">FOT Audit</div>);
            audit_amount_show_detail = (<div></div>);

        } else {
            audit_div = (<div></div>);
            audit_amount_show_detail = (<div className="payment">
                            <div className="pages__subtitle">
                                PAYMENT INFORMATION
                            </div>
                            <div className="payment__info">
                                <div className="flex-column">
                                    <div className="flex-row">
                                        <div>Total: </div>
                                        <div className="payment__price payment__total-price"><i className="icon-inr payment__icon-modify"></i><span> {amount_for_booking} </span></div>
                                    </div>
                                </div>
                                <div className="flex-column">
                                    <div className="flex-row">
                                        <div>Payment Mode: </div>
                                        <div className="payment__price">{paymentStr}</div>
                                    </div>
                                </div>
                            </div>
                        </div>);
        }

        if(part_pay_link && !cancellation_date) {
          partpay_div = (<div>
                            <div>
                                <a href={part_pay_link} target="_blank">
                                  <button className="btn pages__partpay-btn submit">
                                    PAY &#x20b9; {part_pay_amount} NOW
                                  </button>
                                </a>
                            </div>
                            <div className="hotel__partpay-text">
                              Part pay to confirm booking and avoid inconvenience
                            </div>
                        </div>);
        } else {
          partpay_div = (<div></div>)
        }

        let cancel_btn;
        if(status == "RESERVED") {
            const today = moment().format('YYYY-MM-DD');
            const check_chkInDate = moment(this.state.bookingDetailObj.checkin_date).format("DD MMM YY");
            const inCompareDate = moment(this.state.bookingDetailObj.checkin_date).format('YYYY-MM-DD');
            const cancel_hash_url = this.state.bookingDetailObj.cancel_hash_url;
            if(audit_status) {
                if((moment(inCompareDate).diff(today, "days") > 1)) {
                    cancel_btn = (<div><div className="separator"></div>
                                    <div className="text-left">
                                      <a href={cancel_hash_url}>
                                        <div onClick={this.cancelBookingHandler} className="btn hotel__cancel-booking-btn submit" value="CANCEL BOOKING">CANCEL BOOKING</div>
                                      </a>
                                    </div>
                                </div>);
                } else if ((moment(inCompareDate).diff(today, "days") == 1)) {
                    const tomorrow_time = moment().add('days', 1).startOf('day').hour(12);
                    const today_time = moment();

                    if((moment(tomorrow_time).diff(today_time, "hours") < 12)) {
                        cancel_btn = (<div><div className="separator"></div>
                                    <div className="text-left flex-row">
                                        <input id="cancelSubmit" type="submit" onClick={this.cancelBookingHandler} className="btn hotel__cancel-booking-btn hotel__cancel-inactive-booking-btn submit" value="CANCEL BOOKING"/>
                                        <div className="hotel__cancelWarning">This booking cannot be cancelled online. Please contact Customer care on <span className="hotel__cutomer-care anchor">+91-9322-800100</span></div>
                                    </div>
                                </div>);
                    } else {
                        cancel_btn = (<div><div className="separator"></div>
                                    <div className="text-left">
                                      <a href={cancel_hash_url}>
                                        <div onClick={this.cancelBookingHandler} className="btn hotel__cancel-booking-btn submit" value="CANCEL BOOKING">CANCEL BOOKING</div>
                                      </a>
                                    </div>
                                </div>);
                    }
                }
            } else {
                if((moment(inCompareDate).diff(today, "days") >= 1) || ((moment(inCompareDate).diff(today, "days") == 0) && payment == 1) ) {
                    cancel_btn = (<div><div className="separator"></div>
                                    <div className="text-left">
                                      <a href={cancel_hash_url}>
                                        <div onClick={this.cancelBookingHandler} className="btn hotel__cancel-booking-btn submit" value="CANCEL BOOKING">CANCEL BOOKING</div>
                                      </a>
                                    </div>
                                </div>);
                } else if(((moment(inCompareDate).diff(today, "days") == 0) && payment == 0)) {
                    cancel_btn = (<div><div className="separator"></div>
                                    <div className="text-left flex-row">
                                        <input id="cancelSubmit" type="submit" onClick={this.cancelBookingHandler} className="btn hotel__cancel-booking-btn hotel__cancel-inactive-booking-btn submit" value="CANCEL BOOKING"/>
                                        <div className="hotel__cancelWarning">This booking cannot be cancelled online. Please contact Customer care on <span className="hotel__cutomer-care anchor">+91-9322-800100</span></div>
                                    </div>
                                </div>);
                } else {
                    cancel_btn = (<div></div>);
                }
            }
        } else {
            cancel_btn = (<div></div>);
        }
        let cancelDate;
        if(cancellation_date){
            if (cancellation_date == "None"){
                cancelDate = (<div className="hotel__cancel_timestamp">
                              <div className="hotel__booking-cancel-text">Booking successfully cancelled.</div>
                          </div>);
            } else {
                cancellation_date = moment(cancellation_date).format("DD MMMM'YY");
                cancelDate = (<div className="hotel__cancel_timestamp">
                                  <div className="hotel__booking-cancel-text">Booking successfully cancelled <span className="hotel__cancel-date">on {cancellation_date}.</span></div>
                              </div>);
            }
        } else {
            cancelDate = (<div></div>);
        }

        room_type =  (room_type != "") ? ` (${room_type}), ` : ", ";
        const roomConfig = children > 0 ? (rooms > 1 ? `${rooms} Rooms${room_type}` : `${rooms} Room${room_type}`) + (adults > 1 ? `${adults} Adults, ` : `${adults} Adult, `) + (children > 1 ? `${children} Children` : `${children} Child`)
                                  : (rooms > 1 ? `${rooms} Rooms${room_type}` : `${rooms} Room${room_type}` ) + (adults > 1 ? `${adults} Adults` : `${adults} Adult`);

        const print_url = `/api/v1/checkout/display?name=${this.state.bookingDetailObj.user_detail.name}&order_id=${this.state.bookingDetailObj.order_id}`;
        const download_pdf_url = `/api/v1/checkout/printpdf?name=${this.state.bookingDetailObj.user_detail.name}&order_id=${this.state.bookingDetailObj.order_id}`;
        const email_url = `/api/v1/checkout/resendemail?email=${this.state.bookingDetailObj.user_detail.name}&order_id=${this.state.bookingDetailObj.order_id}`;

        let bookingDate = new Date(this.state.bookingDetailObj.booking_date);
        bookingDate = moment(bookingDate).format("DD MMM'YY");
        return(
            <div className="booking_info">
            <div className={booking_detail}>
            <div className="flex-row">
                <div id="back-to-history" onClick={this.backToBookingHandler} className="hotel__back-to-history hand">
                    <i className="icon-back hotel__back-mark"></i><span>Back to booking history</span>
                </div>
                <div className="hotel__email-pdf-download pos-rel">
                    <div className="hotel__action flex-row">
                            <a id="" href={print_url} target="_blank"  className="save-pdf"><i className="icon-print"></i><span>print</span></a>
                            <a id="" href={download_pdf_url} target="_blank" className="save-pdf"><i className="icon-download"></i><span>Download</span></a>
                            <div className="booking-history-email-pdf pos-rel">
                                <a id='booking-history-sendPdf' className="booking-history-email-pdf__head booking-history-sendPdf-email hand" onClick={this.emailHandler} href="javascript:void(1);"><i className="icon-mail"></i><span>Email</span></a>
                                <div className="booking-history-email-pdf__body pos-abs flex-column box-shadow">
                                    <div id="emailPdfLoader" className="loader-container loader-container--inline hide">
                                        <div className="loader-box v-center">
                                            <div className="sk-fading-circle small">
                                                <div className="sk-circle1 sk-circle"></div>
                                                <div className="sk-circle2 sk-circle"></div>
                                                <div className="sk-circle3 sk-circle"></div>
                                                <div className="sk-circle4 sk-circle"></div>
                                                <div className="sk-circle5 sk-circle"></div>
                                                <div className="sk-circle6 sk-circle"></div>
                                                <div className="sk-circle7 sk-circle"></div>
                                                <div className="sk-circle8 sk-circle"></div>
                                                <div className="sk-circle9 sk-circle"></div>
                                                <div className="sk-circle10 sk-circle"></div>
                                                <div className="sk-circle11 sk-circle"></div>
                                                <div className="sk-circle12 sk-circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="bookingHistorySendpdfmsg">Email sent.</div>
                                    <form id="bookingHistorySendPdfForm" className="flex-row">
                                        <div className="float-labels">
                                            <label className="float-labels__label">Email</label>
                                            <input type="hidden" id="orderId" name="orderid" value={this.state.bookingDetailObj.order_id}/>
                                            <input type="text" name="emailpdf" pattern=".+" required="true" id="emailpdfEmail" className="float-labels__input" placeholder="Enter Email id" data-parsley-required="true" data-parsley-type="email" data-parsley-trigger="change" data-parsley-type-message="Enter valid email"
                                                data-parsley-required-message="Enter valid email"/>

                                        </div>
                                        <button type="submit" name="button" id="bookingHistoryPdfmailSubmit" className="booking-history-email-pdf__action btn btn--round">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div className="flex-row">
              <div className="flex-column hotel__name-section">
                  <div className="flex-row">
                      <div className="hotel__name">
                         {hotelName}
                      </div>
                      <div>{audit_div}</div>

                  </div>
                  <div className="flex-column flex--space-between">
                      <div>
                          Booking ID: <span className="hotel__id-date-info">{this.state.bookingDetailObj.order_id}</span>
                      </div>

                      <div>
                          Booking Date: <span className="hotel__id-date-info">{bookingDate}</span>
                      </div>
                  </div>
              </div>
              <div className="flex-column hotel__part-pay-section">
                {partpay_div}
              </div>
            </div>
            <div>{cancelDate}</div>
            <div className="flex-column">
                <div>
                    <div className="pages__subtitle">
                        HOTEL INFORMATION
                    </div>
                    <div className="flex-row hotel__booking-detail-info">
                        <div className="pages__booking-detail">
                            <div className="pages__hotel-name">{this.state.bookingDetailObj.hotel_name}</div>
                            <div className="pages__hotel-address"> {hotel_address} </div>
                            <div className="pages__rooms">{roomConfig}</div>
                        </div>
                        <div className="flex-row flex--space-between hotel__checkin-checkout-info">
                            <div className="flex-column text-center">
                                <div>Check In</div>
                                <div className="hotel__date">{checkIndateFormat}</div>
                                <div className="hotel__day">{chkInDay}{this.state.bookingDetailObj.checkin_time}</div>
                            </div>

                            <div className="hotel__returnarrows">
                                <i className="icon-returnarrows text-center"></i>
                            </div>

                            <div className="flex-column text-center">
                                <div>Check Out</div>
                                <div className="hotel__date">{checkOutdateFormat}</div>
                                <div className="hotel__day">{chkOutDay}{this.state.bookingDetailObj.checkout_time}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="traveller">
                    <div className="pages__subtitle">
                        TRAVELLER INFORMATION
                    </div>
                    <div>
                      <div className="flex-row traveller__info">
                          <div className="traveller__icon-modify">
                              <i className="icon-person"></i>
                          </div>
                          <div>
                              <div className="traveller__name">{this.state.bookingDetailObj.user_detail.name}</div>
                              <div>{this.state.bookingDetailObj.user_detail.email}</div>
                          </div>
                          <div className="traveller__number">
                              {this.state.bookingDetailObj.user_detail.contact_number}
                          </div>
                      </div>
                    </div>
                </div>
                <div>{audit_amount_show_detail}</div>
            </div>
                <div>{cancel_btn}</div>
            </div>
            </div>
        )
    }


});

let BookingItem = React.createClass({

    viewDetailHandler(){
        const order = this.props.data.order_id;
        this.props.onViewDetail(order);
    },

    render(){
        let rooms = this.props.data.room_config.room_count;
        let room_type = this.props.data.room_config.room_type;
        let adults = this.props.data.room_config.no_of_adults;
        let children = this.props.data.room_config.no_of_childs;
        let status = this.props.data.status;
        let payment= this.props.data.paid_at_hotel;
        let total_amount = this.props.data.payment_detail.total;
        let paid_amount = this.props.data.payment_detail.paid_amount;
        let part_pay_link = this.props.data.partial_payment_link;
        let part_pay_amount = this.props.data.partial_payment_amount;
        let paymentStr = (payment == 1) ? "To be paid at Hotel" : "Already Paid";
        total_amount = Math.ceil(total_amount * 100)/100;
        paid_amount = Math.ceil(paid_amount * 100)/100;
        let amount_for_pah = total_amount - paid_amount
        const checkIndateFormat = moment(this.props.data.checkin_date).format("DD MMM'YY");
        const checkOutdateFormat = moment(this.props.data.checkout_date).format("DD MMM'YY");
        const booking_type = this.props.booking;
        const isAudit = this.props.data.audit;

        let audit_notification;
        let audit_amount_show;

        if(isAudit) {
            audit_notification = (<div className="pages__audit-status text-center">FOT Audit</div>);
            audit_amount_show = (<div className="pages__money hide "><i className="icon-inr"></i><span>{total_amount} ( <i>{paymentStr}</i> )</span></div>);
        } else if (payment) {
            audit_notification = (<div></div>);
            audit_amount_show = (<div className="pages__money"><i className="icon-inr"></i><span>{amount_for_pah} ( <i>{paymentStr}</i> )</span></div>);
        } else {
            audit_notification = (<div></div>);
            audit_amount_show = (<div className="pages__money"><i className="icon-inr"></i><span>{total_amount} ( <i>{paymentStr}</i> )</span></div>);
        }

        let stat;
        if (status == "RESERVED") {
          if (booking_type == "previous") {
            stat = (<div id="confirm" className="pages__confirm"><span>Completed</span></div>)
          } else {
            if (part_pay_link) {
              stat = (
                <div>
                  <div id="confirm" className="pages__partly-confirm"><span>Temp Booking</span></div>
                </div>);
            } else {
              stat = (<div id="confirm" className="pages__confirm"><span>Confirmed</span></div>);
            }
          }
        } else {
            stat = (<div id="cancelled" className="pages__cancelled"><span>Cancelled</span></div>);
        };

        room_type =  (room_type != "") ? ` (${room_type}), ` : ", ";
        const roomConfig = children > 0 ? (rooms > 1 ? `${rooms} Rooms${room_type}` : `${rooms} Room${room_type}`) + (adults > 1 ? `${adults} Adults, ` : `${adults} Adult, `) + (children > 1 ? `${children} Children` : `${children} Child`)
                                  : (rooms > 1 ? `${rooms} Rooms${room_type}` : `${rooms} Room${room_type}`) + (adults > 1 ? `${adults} Adults` : `${adults} Adult`);


        return(

					<div id="" className="booking_info flex-row pages__booking-info">
							<div className="flex-row pages__city-date">
									<i className="icon-location pages__location-mark"></i>
									<div>
											<div className="pages__city">{this.props.data.hotel_detail.city}</div>
											<div className="pages__dates"> <span>{checkIndateFormat}</span><i className="icon-arrow-right pages__arrow_right"></i> <span>{checkOutdateFormat}</span></div>
											<div>{audit_notification}</div>
									</div>
							</div>

							<div className="pages__booking-detail-mainpage">
									<div className="pages__hotel-name">{this.props.data.hotel_name}</div>
									<div className="pages__booking-id group_id"> Booking ID: <span id="">{this.props.data.order_id}</span> </div>
									<div className="pages__rooms">{roomConfig}</div>
									<div>{audit_amount_show}</div>
							</div>

							<div className="pages__booking-status">
									{stat}
                <div id="" onClick={this.viewDetailHandler} className="pages__view-detail viewDetail anchor pos-rel hand">View Details<i className="pages__icon-right icon-right"></i></div>
							</div>
					</div>

        )
    }

});

let BookingList = React.createClass({
        getInitialState(){
            return {
                data: _.extend({},this.props.data)
            }
        },

        componentWillReceiveProps(nextProps){
            this.setState({
                data:nextProps.data
            })
        },

        render(){
            const bookingShow = classNames({
            hide: !this.props.visible,
            bookingShow:true
            });

            const upcomingStay = classNames({
                hide: !this.props.upVisible,
                upcomings_stay:true
            });

            const previousStay = classNames({
                hide: !this.props.preVisible,
                previous_stay:true
            });

            const upcoming = this.props.data.upcoming;
            const previous = this.props.data.previous;
            const upcoming_widgets = [];
            const previous_widgets = [];

            for (var i=0; i < upcoming.length; i++) {
                upcoming_widgets.push(<BookingItem onViewDetail={this.props.onViewDetail} key={i} data={upcoming[i]} booking="upcoming"/>);

            };

            for (var i=0; i < previous.length; i++) {
                previous_widgets.push(<BookingItem onViewDetail={this.props.onViewDetail} key={i} data={previous[i]} booking="previous"/>);

            };

            return (
            <div className={bookingShow}>
                <div className="pages__heading">BOOKING HISTORY</div>
                <div id="upcomingStay" className={upcomingStay}>
                    <div className="pages__subtitle upcoming">UPCOMING STAYS</div>
                    {upcoming_widgets}
                </div>

                <div id="previousStay" className={previousStay}>
                    <div className="pages__subtitle upcoming">PREVIOUS STAYS</div>
                    {previous_widgets}
                </div>
            </div>

            )
        }

});

const Booking = React.createClass({

    getInitialState(){
        let upcomingStatus;
        let previousStatus;
        upcomingStatus = (this.props.data.upcoming.length == 0) ? false : true;
        previousStatus = (this.props.data.previous.length == 0) ? false : true;

        return {
            bookingListVisible:true,
            bookingDetailVisible:false,
            previousBooking:true,
            previousVisible:previousStatus,
            upcomingVisible:upcomingStatus,
            bookingdetailObj: {

            },
            data:this.props.data,
        }
    },

    showBookingList(){
        let upcomingStatus;
        let previousStatus;
        upcomingStatus = (this.props.data.upcoming.length == 0) ? false : true;
        previousStatus = (this.props.data.previous.length == 0) ? false : true;

        this.setState({
            bookingListVisible:true,
            bookingDetailVisible:false,
            data:this.props.data,
            previousVisible:previousStatus,
            upcomingVisible:upcomingStatus,
        });

    },
    hideBookingLIst(){
        this.setState({
            bookingListVisible:false,
            bookingDetailVisible:true,
            data:this.props.data,
        });
    },
    showBookingDetails(order){
        window.scrollTo(0, 0);
        let detailObj={};
        let flag =0;
        for(var i = 0; i < this.props.data.upcoming.length; i++){
            if(this.props.data.upcoming[i].order_id == order) {
                detailObj = this.props.data.upcoming[i];
                flag = 1;
                break;
            }
        }

        if(flag == 0) {
            for(var i = 0; i < this.props.data.previous.length; i++){
                if(this.props.data.previous[i].order_id == order) {
                    detailObj = this.props.data.previous[i];
                    break;
                }
            }
        }

        this.setState({
            bookingListVisible:false,
            bookingDetailVisible:true,
            bookingdetailObj:detailObj,
            data:this.props.data,
        });
    },
    hideBookingDetails(){

    },

    onCancel(cancel_id){
            const newObj = {};
            let detailObj={};
            let flag =0;
            for(var i = 0; i < this.props.data.upcoming.length; i++){
                if(this.props.data.upcoming[i].order_id == cancel_id) {
                    detailObj = this.props.data.upcoming[i];
                    flag = 1;
                    break;
                }
            }

            if(flag == 0) {
                for(var i = 0; i < this.props.data.previous.length; i++){
                    if(this.props.data.previous[i].order_id == cancel_id) {
                        detailObj = this.props.data.previous[i];
                        break;
                    }
                }
            }

            this.setState({
                bookingDetailObj:detailObj,
                data:this.props.data,
            })
    },

    render(){
        return(
            <div className="">
                <BookingList visible={this.state.bookingListVisible} onViewDetail={this.showBookingDetails} data={this.state.data} upVisible={this.state.upcomingVisible} preVisible={this.state.previousVisible}/>
                <BookingDetail visible={this.state.bookingDetailVisible} onCancelDetail={this.onCancel} bookingDetailObj={this.state.bookingdetailObj} backToHistory={this.showBookingList} data={this.props.bookingDetailObject}/>
            </div>

        );
    }
});

let methods = {
    sendEmail : ()=>{
        let data = JSON.stringify();
        let email = $("#emailpdfEmail").val();
        let orderId = $("#orderId").val();
        $("#emailPdfLoader").removeClass('hide');
        $('#bookingHistorySendpdfmsg').hide();
        const p = treejax({
                url: `/api/v1/checkout/resendemail?email=${email}&order_id=${orderId}`,
                type: 'get'
            }).then((data, status, xhr) => {
                $('#bookingHistorySendpdfmsg').show();
                $("#bookingHistorySendPdfForm")[0].reset();
                $("#emailPdfLoader").addClass('hide');
                setTimeout(() => {
                    $('#bookingHistorySendpdfmsg').hide();
                }, 3000);
            },treeboError => {
                console.log(treeboError);
            }).finally(() => {
                $("#emailPdfLoader").addClass('hide');
            });

        return p;
    }
}

var data = {};
let birth_date;
let anniversary_date;
let dateOfAnniversary;
let dateOfBirth;
let profileData = {};

BookingHistoryView.pageLoaded = () => {

    profileData = $('#profileDetailData').val();
    profileData = JSON.parse(profileData);
    birth_date = profileData.dob;
    anniversary_date = profileData.anniversary;

    const dateBirth = new Date(profileData.dob);
    const yearBirth = dateBirth.getFullYear();

    if(yearBirth == 9999 || profileData.dob == "None"){
        dateOfBirth = "2001-01-01";
        birth_date = "2001-01-01"
    } else {
        dateOfBirth = profileData.dob;
    }
    const calendar_dob = new Calendar({
            element: $('#birthDate'),
            format: {input: 'DD/MM/YYYY'},
            current_date: dateOfBirth,
            latest_date: moment(),
            callback() {
                birth_date  = moment(this.current_date).format('YYYY-MM-DD'),
                $('body').trigger('datechanged');
            }
    });
    const dateAnniversary = new Date(profileData.anniversary);
    const yearAnniversary = dateAnniversary.getFullYear();

    if(yearAnniversary == 9999 || profileData.anniversary == "None"){
        dateOfAnniversary = "2001-01-01";
        anniversary_date = "2001-01-01"
    } else {
        dateOfAnniversary = profileData.anniversary;
    }
    const calendar_anniversary = new Calendar({
            element: $('#anniversaryDate'),
            format: {input: 'DD/MM/YYYY'},
            current_date: dateOfAnniversary,
            latest_date: moment(),
            callback() {
                anniversary_date  = moment(this.current_date).format('YYYY-MM-DD'),
                $('body').trigger('datechanged');
            }
    });
    let data = $('#bookingDetailData').val();
        data = JSON.parse(data);

    if(data.upcoming.length == 0 && data.previous.length == 0) {
        $('.booking_search').removeClass('hide');
        $('.bookinglist').addClass('hide');
    } else {
         render(<Booking data={data}/>, $('.bookinglist')[0]);
    }

    const clipboard = new Clipboard('.referral__copy');

    clipboard.on('success', e => {
        const ele = $(e.trigger);
        const text = ele.html();
        ele.html('Copied!');

        setTimeout(() => {
            ele.html(text);
        }, 2000);
    });

    const referralShare = document.getElementById('referral-share');
    render(
        <ReferralShare
            referralLink={$('.referral__code').html()}
            referralText={referralShare.getAttribute('data-fbshare')}
        />,
        referralShare
    );

    //TODO: Temp hack, needs to be refactored
    if (location.pathname.indexOf('/booking-history') > -1) {
        _.defer(() => {
            $('.tab__link.booking-history').trigger('click');
        });
    } else if (location.pathname.indexOf('/referrals') > -1) {
        _.defer(() => {
            $('.tab__link.referrals').trigger('click');
        });
    } else if (location.pathname.indexOf('/settings') > -1) {
        _.defer(() => {
            $('.tab__link.settings').trigger('click');
        });
    } else if (location.pathname.indexOf('/profile') > -1) {
        _.defer(() => {
            $('.tab__link.profile').trigger('click');
        });
    }
};

// ----------------password change--------------------
$('#submitPasswordChange').on('click',e => {
        e.preventDefault();
        const submitParsley = $(".passwordChangeForm").parsley();
        submitParsley.validate();
        if (submitParsley.isValid()){
            $loader.removeClass('hide');
            submitForm(e);
        }

});

let submitForm = ()=>{
        const data = $(".passwordChangeForm").serializeArray();
        const password_change_json = _.object(_.map(data, _.values));
        password_change_json['form-type'] = "passwordChangeForm";
        password_change_json['csrfmiddlewaretoken'] = $('input[name="csrfmiddlewaretoken"]').val();
        if (password_change_json['newPassword'] == password_change_json['oldPassword']) {
            $loader.addClass('hide');
            toastr.error('Old and new passwords cannot be the same. Please try again.', 'Sorry!', {timeOut: 5000, closeButton: true});
        } else if(password_change_json['newPassword'] == password_change_json['confirmPassword']) {
            var p = treejax({
                url: '/api/v2/auth/change-password/',
                type: 'POST',
                data: {
	                "old_password":password_change_json['oldPassword'],
                  "new_password":password_change_json['newPassword']
                }
            }).then((data, status, xhr) => {
                $loader.addClass('hide');
                toastr.success('Your password is changed successfully.', 'Thank You!', {timeOut: 5000, closeButton: true});
            },treeboError => {
                $loader.addClass('hide');
                toastr.error('Password could not be changed. Please try again.', 'Sorry!', {timeOut: 5000, closeButton: true});
            }).finally(() => {
             $loader.addClass('hide');
             $(".passwordChangeForm")[0].reset();
            });
        } else {
            $loader.addClass('hide');
            toastr.error('Password could not be changed. Please try again.', 'Sorry!', {timeOut: 5000, closeButton: true});
        }
        $loader.addClass('hide');
    return p;
}

// -------------User Profile Detail-------------
$('#submitProfileDetail').click(e => {
        e.preventDefault();
        const submitProfileParsley = $(".profile-detail-form").parsley();
        submitProfileParsley.validate();
        if (submitProfileParsley.isValid()){
            $loader.removeClass('hide');
            submitProfileDetailForm(e);
        }

});

let submitProfileDetailForm = ()=>{
        const data = $(".profile-detail-form").serializeArray();
        const profile_detail_json = _.object(_.map(data, _.values));
        profile_detail_json['form-type'] = "profile-detail-form";
        profile_detail_json['csrfmiddlewaretoken'] = $('input[name="csrfmiddlewaretoken"]').val();
        profile_detail_json['dob'] = birth_date;
        profile_detail_json['anniversary'] = anniversary_date;

        const p = treejax({
            url: '/api/v1/profile/',
            type: 'POST',
            data: profile_detail_json
        }).then((data, status, xhr) => {
            console.log("success");
            $loader.addClass('hide');
            toastr.success('Your profile detail has been saved successfully.', 'Thank You!', {timeOut: 5000, closeButton: true});
        },treeboError => {
            $loader.addClass('hide');
            toastr.error('Your profile detail could not be saved. Please try again later.', 'Sorry!', {timeOut: 5000, closeButton: true});
            console.log("fail");
        }).finally(() => {
           $loader.addClass('hide');
        });
    return p;
};

$('.tab__link').on('click', function () {
    const ele = $(this);
    const target = ele.data('target');

    $('.tab__link').removeClass('tab__link--active');
    ele.addClass('tab__link--active');

    $('.tab__content').removeClass('tab__content--active');
    $(target).addClass('tab__content--active');
});

$('.settings').on('click',e => {
        $(".passwordChangeForm").parsley().reset();
        $(".passwordChangeForm")[0].reset();
});

$('.profile').on('click',e => {
        $("#firstName").val(profileData.first_name);
        $("#lastName").val(profileData.last_name);
        if(profileData.gender) {
            $("#gender").val(profileData.gender);
        }
        $("#birthDate").val(profileData.dob);
        $("#anniversaryDate").val(profileData.anniversary);
        $("#email").val(profileData.email);
        $("#mobile").val(profileData.phone);
        $("#location").val(profileData.city);
        $("#workEmail").val(profileData.work_email);
});

let booking_historyConfig = { name: '', widgets: [searchWidget] };
let booking_historyView = BookingHistoryView.setup(booking_historyConfig);
