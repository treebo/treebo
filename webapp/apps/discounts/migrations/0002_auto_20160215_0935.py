# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('discounts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='discountcoupon',
            name='is_auto_applicable',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='discountcoupon',
            name='series_value',
            field=models.CharField(default='', max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='discountcoupon',
            name='discount_operation',
            field=models.CharField(default='PERCENTAGE', max_length=20,
                                   choices=[('FIXED', 'Fixed'), ('PERCENTAGE', 'Percentage'),
                                            ('NIGHT', 'Night'),
                                            ('FLATPERNIGHT', 'Flatpernight'),
                                            ('FLATCOST', 'Flatcost')]),
        ),
    ]
