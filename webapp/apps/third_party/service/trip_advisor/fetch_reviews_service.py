

import logging
import traceback
from collections import namedtuple

import requests
from django.conf import settings

from apps.reviews.services.trip_advisor.api_audit_service import APIAuditService
from apps.reviews.services.trip_advisor.award_hotel_mapping import HotelAwardMappingService
from apps.reviews.services.trip_advisor.hotel_overall_rating_service import HotelRatingService
from apps.reviews.services.trip_advisor.hotel_review_count_mapping import HotelReviewCountService
from apps.reviews.services.trip_advisor.hotel_reviews_data import HotelReviewsDataService
from apps.reviews.services.trip_advisor.hotel_ta_mapping_service import HotelTAMappingService
from apps.reviews.services.trip_advisor.subrating_hotel_mapping_service import HotelSubRatingMappingService
from apps.third_party.service.trip_advisor.serializers.ta_location_reviews_serializer.location_reviews_api_serializer import \
    LocationReviewsAPISerializer
from apps.third_party.service.trip_advisor.serializers.ta_location_serializer.location_api_serializer \
    import LocationAPISerializer
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class TAReviewAPIService(object):
    def fetch_review_and_update_audit(self, hotel_id):
        try:
            Keys = namedtuple('Keys', 'response url status')
            hotel_ta_map = HotelTAMappingService()
            map_obj = hotel_ta_map.fetch_mapping(hotel_id)
            if map_obj:
                ta_loc_id = map_obj.ta_location_id
                response, url, response_status, serializer_loc = self.call_ta_location_api(
                    ta_loc_id)
                location = Keys(response, url, response_status)
                response, url, response_status, serializer_user_rev = self.call_ta_location_reviews_api(ta_loc_id,
                                                                                                        user_generated=
                                                                                                        "true")
                user_reviews = Keys(response, url, response_status)

                review_audit = APIAuditService()
                review_audit.create_new_audit(location, user_reviews, ta_loc_id)

                hotel_rating = HotelRatingService()
                hotel_rating.ta_rating_ranking(serializer_loc, ta_loc_id)

                hotel_subrating_mapping = HotelSubRatingMappingService()
                hotel_subrating_mapping.hotel_subrating_mapping(
                    serializer_loc, ta_loc_id)

                hotel_award_mapping = HotelAwardMappingService()
                hotel_award_mapping.hotel_award_mapping(
                    serializer_loc, ta_loc_id)

                hotel_review_count = HotelReviewCountService()
                hotel_review_count.hotel_review_count_mapping(
                    serializer_loc, ta_loc_id)

                hotel_review_data = HotelReviewsDataService()
                hotel_review_data.hotel_reviews_data(
                    serializer_user_rev, ta_loc_id, user_generated=True)

            else:
                logger.error(
                    "No such mapping exists for hotel %s",
                    str(hotel_id))
        except Exception as ex:
            logger.exception("Exception in calling TA APIs")
            slack_alert.review_alert(
                "Exception occured while fetching reviews and updating audit for hotel_id : %s \n%s",
                hotel_id, traceback.format_exc())

    def call_ta_location_api(self, hotel_loc_id):
        response = None
        url = None
        response_status = None
        serializer = None
        try:
            url = settings.TA_BASE_URL + \
                str(hotel_loc_id) + settings.TA_URL_PARAMS
            response = requests.get(url=url)
            response_status = response.status_code
            if response_status != 200:
                logger.error(
                    "Error in calling TA Location API %s",
                    response.text)
                response = response.json()
            else:
                # logger.info("response is %s", response.text)
                response = response.json()
                serializer = LocationAPISerializer(data=response)
                logger.info(
                    "TA LOCATION API - for ta_loc_id %s  serializer.is_valid() %s serializer.errors %s ",
                    hotel_loc_id,
                    serializer.is_valid(),
                    serializer.errors)

        except Exception as ex:
            logger.exception("Exception in calling location api of TA")
            slack_alert.review_alert(
                "Exception occured while calling ta location api hotel_loc_id : %s."
                "\n%s ", hotel_loc_id, traceback.format_exc())
        return response, url, response_status, serializer

    def call_ta_location_reviews_api(
            self, hotel_loc_id, user_generated="false"):
        response = None
        url = None
        response_status = None
        serializer = None
        try:
            url = settings.TA_BASE_URL + \
                str(hotel_loc_id) + "/reviews" + settings.TA_URL_PARAMS.format(user_generated.lower())
            response = requests.get(url=url)
            response_status = response.status_code
            if response_status != 200:
                logger.error(
                    "Error in calling TA Location Reviews API %s",
                    response.text)
                response = response.json()
            else:
                # logger.info("response is %s", response.text)
                response = response.json()
                serializer = LocationReviewsAPISerializer(data=response)
                logger.info(
                    "TA LOCATION REVIEWS API - for ta_loc_id %s serializer.is_valid() %s serializer.errors %s",
                    hotel_loc_id,
                    serializer.is_valid(),
                    serializer.errors)

        except Exception as ex:
            logger.exception("Exception in calling location api of TA")
            slack_alert.review_alert(
                "Exception occured while calling ta location reviews api with hotel_loc_id : %s."
                "\n%s ", hotel_loc_id, traceback.format_exc())
        return response, url, response_status, serializer
