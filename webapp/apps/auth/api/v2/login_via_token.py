import logging
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.token_login_dto import TokenLoginDTO
from apps.auth.dto.user_login_dto import UserLoginDTO
from apps.auth.serializer.user_serializer import UserSerializer
from apps.auth.services.cookie_service import CookieService
from apps.common.api_response import APIResponse as api_response
from apps.profiles.service.user_service import UserService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes

logger = logging.getLogger(__name__)


class TokenLoginApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    user_service = UserService()
    cookie_service = CookieService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            token_login_dto = TokenLoginDTO(data=request.data)
            if not token_login_dto.is_valid():
                logger.error("email login invalid request %s ", request.data)
                return api_response.invalid_request_error_response(
                    token_login_dto.errors, TokenLoginApi.__name__)
            token = token_login_dto.data['token']
            user = self.user_service.validate_token(token)
            self.user_service.login_user(request, user)
            user_dto = UserSerializer(instance=user)
            user_data = user_dto.data
            user_data['token'] = token
            response = api_response.prep_success_response(user_data)
            self.cookie_service.set_user_info_in_cookie(response, user)
        except TreeboException as e:
            logger.exception(
                "treebo login via token exception %s",
                request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception(
                "internal error login via token exception %s ",
                request.data)
            response = api_response.internal_error_response(error_codes.get(
                error_codes.LOGIN_FAILED)['msg'], TokenLoginApi.__name__)
        return response
