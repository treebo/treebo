import logging
import csv
import datetime

from apps.discounts.exceptions import HotelAutoApplyCouponInvalidInputException, HotelAutoApplyCouponOverlapException, InvalidCouponException
from apps.discounts.services.hotel_coupon_auto_apply_service import HotelCouponAutoApplyService
from base.views.api import TreeboAPIView
from apps.hotels.service.hotel_service import HotelService
from apps.discounts.dto.auto_apply_hotel_coupon_pair_dto import AutoApplyHotelCouponPairDto
from apps.discounts.constants import UNAUTHORIZED_REQUEST_MESSAGE
from apps.common.exceptions.custom_exception import HotelNotFoundException
from rest_framework.response import Response
from base.middlewares.request import get_domain
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from io import StringIO

logger = logging.getLogger(__name__)

csv_format = {
    'hotel_id': 0,
    'coupon_code': 1,
    'start_date': 2,
    'end_date': 3,
    'is_enabled': 4}


class HotelCouponForAutoApply(TreeboAPIView):
    def __init__(self):
        self.hotel_service = HotelService()
        super(HotelCouponForAutoApply, self).__init__()

    def get(self, request):

        action_url = get_domain() + reverse('auto_apply_upload')
        context = {'action_url': action_url}
        res = render(
            request=request,
            template_name='admin/discounts/hotel_coupon_auto_apply_upload.html',
            context=context)
        return HttpResponse(res)

    def post(self, request, *args, **kwargs):
        # fetching the file from the request
        csvfile = request.FILES.get('auto_apply', None)
        user = request.user
        if not (user and user.is_authenticated() and user.is_staff):
            return Response(
                {"error": UNAUTHORIZED_REQUEST_MESSAGE}, status=401)
        # todo: validate the csv format
        csvfile = StringIO(csvfile.read().decode())
        readCSV = csv.reader(csvfile, delimiter=',')
        is_first_row = True
        failed_rows = []
        success_rows = []
        for row in readCSV:
            # avoiding the header
            if is_first_row:
                is_first_row = False
                continue
            auto_apply_hotel_coupon_pair_dto = self.get_hotel_pair_object_from_csv_row(
                row)
            try:
                hotel = Hotel.objects.get(
                    id=auto_apply_hotel_coupon_pair_dto.hotel_id)
                HotelCouponAutoApplyService.upsert_hotel_coupon_auto_apply(
                    hotel=hotel,
                    coupon_code=auto_apply_hotel_coupon_pair_dto.coupon_code,
                    start_date=auto_apply_hotel_coupon_pair_dto.start_date,
                    end_date=auto_apply_hotel_coupon_pair_dto.end_date,
                    is_enabled=auto_apply_hotel_coupon_pair_dto.is_enabled,
                    user=user
                )
                success_rows.append(row)
                HotelCouponAutoApplyService.insert_hotel_coupon_for_auto_apply_upload_log(
                    str(row))
            except HotelAutoApplyCouponInvalidInputException as e:
                error_dict = {'row': row, 'error': e.developer_message}
                failed_rows.append(error_dict)
                HotelCouponAutoApplyService.insert_hotel_coupon_for_auto_apply_upload_log(
                    str(row), False, e.developer_message)
            except HotelNotFoundException as e:
                error_dict = {'row': row, 'error': e.developer_message}
                failed_rows.append(error_dict)
                HotelCouponAutoApplyService.insert_hotel_coupon_for_auto_apply_upload_log(
                    str(row), False, e.developer_message)
            except HotelAutoApplyCouponOverlapException as e:
                error_dict = {'row': row, 'error': str(e)}
                failed_rows.append(error_dict)
                HotelCouponAutoApplyService.insert_hotel_coupon_for_auto_apply_upload_log(
                    str(row), False, str(e))
            except InvalidCouponException as e:
                error_dict = {'row': row, 'error': str(e)}
                failed_rows.append(error_dict)
                HotelCouponAutoApplyService.insert_hotel_coupon_for_auto_apply_upload_log(
                    str(row), False, str(e))
            except Exception as e:
                error_dict = {'row': row, 'error': str(e)}
                failed_rows.append(error_dict)
                HotelCouponAutoApplyService.insert_hotel_coupon_for_auto_apply_upload_log(
                    str(row), False, str(e))

        return Response({"success_list": success_rows,
                         "failure_list": failed_rows}
                        )

    def get_hotel_pair_object_from_csv_row(self, row):
        date_format = "%d-%m-%Y"
        try:
            hotel_id = int(row[csv_format['hotel_id']])
            coupon_code = row[csv_format['coupon_code']]
            start_date = datetime.datetime.strptime(
                row[csv_format['start_date']], date_format)
            end_date = datetime.datetime.strptime(
                row[csv_format['end_date']], date_format)
            is_enabled = row[csv_format['is_enabled']]

            if is_enabled not in ['TRUE', 'FALSE']:
                raise HotelAutoApplyCouponInvalidInputException(
                    "Invalid input data")

            is_enabled = True if is_enabled == 'TRUE' else False

            res = AutoApplyHotelCouponPairDto(
                hotel_id=hotel_id,
                coupon_code=coupon_code,
                start_date=start_date,
                end_date=end_date,
                is_enabled=is_enabled)
            return res
        except ValueError as e:
            raise HotelAutoApplyCouponInvalidInputException(
                "Invalid input data")
