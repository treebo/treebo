# -*- coding: utf-8 -*-


from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0071_auto_20171031_1344'),
    ]

    operations = [
        migrations.CreateModel(
            name='CSPropertyDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('hotelogix_id', models.CharField(unique=True, max_length=100)),
                ('is_active', models.BooleanField()),
                ('cs_id', models.IntegerField(unique=True)),
                ('cs_data', jsonfield.fields.JSONField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='hotel',
            name='cs_id',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='hotel',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, 'Disabled'), (1, 'Enabled')]),
        ),
    ]
