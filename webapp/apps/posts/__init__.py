from django.apps import AppConfig

default_app_config = 'apps.posts.PostsAppConfig'

class PostsAppConfig(AppConfig):

    name = 'apps.posts'

    def ready(self):
        from . import signals
