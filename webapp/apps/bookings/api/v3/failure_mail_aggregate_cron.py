import logging

from apps.bookings.constants import BOOKING_TAGS_LIST, PAYMENT_TAGS_LIST, GST_TAGS_LIST, INITIATE_BOOKING_TAG
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.bookings.services.failure_mail_aggregate_service import FailureMailAggregateService
from base.views.api import TreeboAPIView
from apps.bookings.constants import AsyncJobStates
from rest_framework.status import HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR
from django.http import JsonResponse

logger = logging.getLogger(__name__)
SUCCESS = 'Success'
FAILED = 'Failed'


class FailureMailAggregateCron(TreeboAPIView):

    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    def post(self, request):
        """
        Cron to aggregate all failures related to a booking (i.e create booking, payment, gst)
        :param request:
        :return:
        """

        try:
            mail_aggregate_service = FailureMailAggregateService

            async_jobs = mail_aggregate_service.fetch_async_jobs_by_status(
                status=AsyncJobStates.FAILURE_HANDLE_INIT)

            failed_bookings, failed_initiate_bookings, failed_payments, failed_gst = {}, {}, {}, {}

            for async_job in async_jobs:
                if not async_job.booking_id:
                    continue
                if async_job.tag in BOOKING_TAGS_LIST:
                    failed_bookings[async_job.booking_id] = async_job
                elif async_job.tag in PAYMENT_TAGS_LIST:
                    failed_payments[async_job.booking_id] = async_job
                elif async_job.tag in GST_TAGS_LIST:
                    failed_gst[async_job.booking_id] = async_job
                elif async_job.tag in INITIATE_BOOKING_TAG:
                    failed_initiate_bookings[async_job.booking_id] = async_job

            mail_aggregate_service.aggregate_and_send_booking_failure_emails(failed_bookings)

            mail_aggregate_service. update_initiate_booking_status(failed_initiate_bookings)

            failed_payments = mail_aggregate_service.update_email_already_sent_jobs(failed_bookings,
                                                                                    failed_payments)
            failed_gst = mail_aggregate_service.update_email_already_sent_jobs(failed_bookings,
                                                                               failed_gst)

            mail_aggregate_service.aggregate_and_send_payment_gst_failure_emails(
                failed_payments, failed_gst)

            response_body = {'status': SUCCESS}
            return JsonResponse(response_body, status=HTTP_200_OK)

        except Exception as e:
            logger.exception("Exception while aggregating and sending failure mails")
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param="",
                                                        api_name=self.__class__.__name__)

            data = {'error_message': str(e)}
            response_body = {
                'status': FAILED,
                'data': data
            }
            return JsonResponse(response_body, status=HTTP_500_INTERNAL_SERVER_ERROR)
