from django.conf import settings
from django.utils.text import slugify
from apps.seo.components.breadcrumb import BreadCrumb

from apps.seo.pages.pages import PAGES
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import CitySeoDetails
from dbcommon.models.location import City
from services.restclient.contentservicerestclient import ContentServiceClient
from apps.seo.components.categories import Category
from apps.seo.components.cities_nearby import CitiesNearby
from apps.seo.components.landmarks import Landmarks
from apps.seo.components.localities import Localities
from apps.seo.components.popular_cities import PopularCities

__author__ = 'ansilkareem'


class CityPage(PAGES):

    def __init__(self, page, query):
        city_data_service = RepositoriesFactory.get_city_repository()
        self.city = city_data_service.get_by_slug(query)
        self.key = 'search_' + query.lower() + '_desc'
        self.description = ContentServiceClient.getValueForKey(
            None, self.key, 1)
        if not self.description:
            self.key = 'about_' + query.lower()
            self.description = ContentServiceClient.getValueForKey(
                None, self.key, 1)
        super(CityPage, self).__init__(page, query, query)

    def update_meta(self, ):
        city_seo_details = CitySeoDetails.objects.filter(
            city=self.city).first()
        full_canonical_url = settings.SITE_HOST_NAME + \
            'hotels-in-' + slugify(self.city.slug) + '/'
        if city_seo_details:
            self.override_seo_content(
                city_seo_details.citySeoTitleContent,
                city_seo_details.citySeoDescriptionContent,
                city_seo_details.citySeoKeywordContent,
                full_canonical_url,
                self.city.name)
        else:
            title = None
            description = None
            temp_dict = {"city_name": self.city.slug.title(
            ) if self.city.slug else self.city.name.title(), }
            if 'city_title' in self.footer_data:
                title = self.footer_data['city_title'] % temp_dict
            if 'city_description' in self.footer_data:
                description = self.footer_data['city_description'] % temp_dict
            if title and description:
                self.override_seo_content(title,
                                          description,
                                          None,
                                          full_canonical_url)

    def build_meta(self):
        self.update_meta()
        self.meta_content['content'] = {
            'description': self.description['text'] if self.description else ''
        }
        category = Category(self.city, self.footer_data, [])
        landmark = Landmarks(self.city, self.footer_data, [])
        locality = Localities(self.city, self.footer_data, [])
        cities_nearby = CitiesNearby(self.city, self.footer_data)
        self.meta_content['content'].update(
            {
                'links': [
                    locality.build(),
                    landmark.build(),
                    category.build(),
                    cities_nearby.build()],
                'breadcrumbs': BreadCrumb(
                    self.city,
                    self.footer_data,
                    self.page,
                    self.item_name).build()})
        popular_cities = PopularCities(
            self.city, self.footer_data, [
                city.id for city in cities_nearby.sorted_nearby_cities])
        self.meta_content['content']['links'].append(popular_cities.build())
