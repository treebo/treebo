from base import log_args
from dbcommon.models.location import State
from data_services.decorators import SingletonDecorator


@SingletonDecorator
class StateRepository(object):

    def __init__(self, state_web_repository):
        self.state_web_repository = state_web_repository

    @log_args()
    def get_all_states(self):
        return State.objects.all()

    @log_args()
    def get_single_state(self, *args, **kwargs):
        return State.objects.get(**kwargs)

    @log_args()
    def filter_states(self, *args, **kwargs):
        return State.objects.filter(**kwargs)
