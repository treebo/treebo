import logging

from django.core.management import BaseCommand
from optparse import make_option

from apps.content.models import ContentStore
from apps.seo import constants
from apps.seo.models import FeatureToggle
from apps.seo.services.facilities.catalog_facilities_mapping import CatalogFacilitiesMapping
from apps.seo.services.facilities.catalog_hotel_and_room_mapping import WebCatalogMapping
from apps.seo.services.facilities.upload_facilities_from_csv_script import FacilitiesDataScript
from apps.seo.services.guest_love_data.get_hotel_positives_hd import FetchHotelsPositives
from apps.seo.services.guest_love_data.upload_positives_mapping_script import PositiveMappingScript

logger = logging.getLogger(__name__)


# ONE TIME DUMP DATA SCRIPT
class Command(BaseCommand):
    option_list = (
        make_option('--component',
                    action='store',
                    default=None,
                    dest='component'),
        make_option('--facilities_choice',
                    action='store',
                    default='hotel',
                    dest='facilities_choice'),
    )

    def handle(self, *args, **option_list):
        try:
            component = option_list['component'].lower()
            if 'guest_love' in component:
                # for positives
                logger.info(
                    "START ONE TIME DUMP \n START WITH POSITIVES \n 1. POSITIVE READING CSV AND UPLOAD")
                positives_map = PositiveMappingScript()
                positives_map.upload_hotel_positives_mapping()
                logger.info("\n 2. Make an entry in seo config table")
                f_toggle_obj, f_toggle_create = FeatureToggle.objects.get_or_create(
                    username='Rajs191jn3', password='Weenwdj1324')
                logger.info("\n 3. CALL PROWL API AND UPDATE DATABASES")
                hotel_positives = FetchHotelsPositives()
                hotel_positives.get_all_hotels_positive()
            if 'facilities_from_csv' in component:
                # for facilities
                logger.info("\n 4. UPLOAD FACILITIES DATA FROM CSV")
                facility_data = FacilitiesDataScript()
                facility_data.upload_hotel_facilities_mapping()
                facility_data.upload_other_facilities_mapping()
            if 'catalog_mapping' in component:
                logger.info(
                    "\n 5. UPDATE CATALOG HOTELS AND ROOMS MAPPING WITH WEBSITE DATA")
                web_catalog_map_obj = WebCatalogMapping()
                web_catalog_map_obj.check_web_and_catalog_hotels()
            if 'facilities_from_catalog' in component:
                # hotel/room
                facilities_choice = option_list['facilities_choice'].lower()
                logger.info("\n 6. UPDATE FACILITIES OF HOTELS AND ROOMS")
                facilities_map = CatalogFacilitiesMapping()
                facilities_map.map_catalog_failities(
                    facilities_choice=facilities_choice)
            if 'content_store_update' in component:
                logger.info("\n 7. UPDATE CONTENT STORE KEYS")
                data = {
                    "positive_feedback": "Things Guest Loves about {0}",
                    "facilities": "Facilities"
                }
                content_store_obj, content_store_create = ContentStore.objects.get_or_create(
                    name=constants.HD_COMPONENT_TITLE, version=1, value=data)
                content_store_facilities_order, facility_order_create = ContentStore.objects.get_or_create(
                    name=constants.FACILITIES_ORDER,
                    version=1, value=constants.DEFAULT_FACILITIES_ORDER_DICT)
                content_store_default_positives, def_pos_create = ContentStore.objects.get_or_create(
                    name=constants.DEFAULT_POSITIVES_ORDER, version=1, value=constants.DEFAULT_POSITIVES_DICT)
            else:
                logger.error("Error in command line arguments %s", component)
        except Exception:
            logger.exception(
                "Exception in fetching/updating the positives and facilities data for meta api")
