# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0007_reviewappconfiguration_num_reviews_to_show'),
    ]

    operations = [
        migrations.AddField(
            model_name='tahotelidmapping',
            name='enable_iframe_widget',
            field=models.BooleanField(default=True),
        ),
    ]
