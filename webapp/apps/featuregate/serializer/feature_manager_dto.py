from rest_framework import serializers


class FeatureManagerSerializer(serializers.Serializer):
    city = serializers.CharField(required=True)
    pagename = serializers.CharField()
    checkin = serializers.CharField()
    checkout = serializers.CharField()
    hotel_id = serializers.IntegerField()
    totalcost = serializers.FloatField()
    locality = serializers.CharField()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        return FeatureManagerDTO(**validated_data)


class FeatureManagerDTO(object):

    def __init__(
            self,
            city,
            pagename,
            checkin,
            checkout,
            hotel_id,
            totalcost,
            locality):
        super(FeatureManagerDTO, self).__init__()
        self.city = city
        self.pagename = pagename
        self.checkin = checkin
        self.checkout = checkout
        self.hotel_id = hotel_id
        self.totalcost = totalcost
        self.locality = locality
