# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0002_auto_20151024_0226'),
    ]

    operations = [
        migrations.RenameField(
            model_name='discountcoupon',
            old_name='tag_line',
            new_name='tagline',
        ),
        migrations.AddField(
            model_name='discountcoupon',
            name='coupon_message',
            field=models.CharField(default='Discount value', max_length=500),
        ),
        migrations.AddField(
            model_name='discountcoupon',
            name='coupon_type',
            field=models.CharField(default='DISCOUNT', max_length=200,
                                   choices=[('DISCOUNT', 'DISCOUNT'), ('CASHBACK', 'CashBack'),
                                            ('ROOMNIGHTS', 'RoomNights'),
                                            ('VOUCHER', 'Voucher')]),
        ),
        migrations.AddField(
            model_name='discountcoupon',
            name='discount_application',
            field=models.IntegerField(default=1, choices=[(0, 'Pretax'), (1, 'Posttax')]),
        ),
        migrations.AlterField(
            model_name='discountcoupon',
            name='discount_operation',
            field=models.CharField(default='PERCENTAGE', max_length=20,
                                   choices=[('FIXED', 'Fixed'), ('PERCENTAGE', 'Percentage'),
                                            ('NIGHT', 'Night')]),
        ),
    ]
