import logging
from http import HTTPStatus

from django.conf import settings
from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication
from web_sku.common.custom_exception import CouponNotAppliedException

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.checkout.service.itinerary_handler import ItineraryHandler
from apps.checkout.service.itinerary_handler_v2 import ItineraryHandlerV2
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import InvalidDiscountCouponException
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.pricing.transformers.itinerary_api_transformer import ItineraryAPITransformer
from base.decorator.request_validator import validate_request
from base.views.api_v2 import TreeboAPIView2

logger = logging.getLogger(__name__)


class ApplyCouponSerializer(serializers.Serializer):
    couponcode = serializers.CharField(max_length=15, allow_blank=True, required=False)
    bid = serializers.IntegerField()
    apply_wallet = serializers.BooleanField(default=settings.APPLY_WALLET, required=False)
    paymode = serializers.CharField(required=False, default=None, allow_blank=True, allow_null=True)


class ApplyCoupon(TreeboAPIView2):
    authentication_classes = (BasicAuthentication, CsrfExemptSessionAuthentication)
    pricing_service = PricingServiceV2()
    checkout_service = ItineraryHandler()
    checkout_service_v2 = ItineraryHandlerV2()
    price_response_transformer = ItineraryAPITransformer()

    @validate_request(ApplyCouponSerializer)
    def post(self, request, *args, **kwargs):
        """
        Gets the discount for a booking id and coupon code.
        couponcode -- Coupon Code
        bid -- BookingRequest Id
        """

        try:
            response = self.apply_coupon_revamp(request, *args, **kwargs)

        except InvalidDiscountCouponException as e:
            logger.error("Invalid Coupon Error => %s ", e.message)
            response = api_response.prep_simple_error_response(msg=e.message, status_code=400,
                                                               code=e.error_code)

        except Exception as e:
            logger.exception("Exception in Apply Coupon API")
            response = api_response.internal_error_response(messages=e.__str__(),
                                                            api_name=self.__class__,
                                                            developer_message="Exception in Apply Coupon API")
        return response

    def apply_coupon_revamp(self, request, *args, **kwargs):
        try:
            validated_data = kwargs['validated_data']
            coupon_code = validated_data['couponcode'].upper() if validated_data.get('couponcode') else None
            bid, apply_wallet = validated_data['bid'], validated_data['apply_wallet']
            price_data = self.checkout_service_v2.apply_coupon(request, bid, coupon_code, apply_wallet)
            price_data['coupon_desc'] = ""
            price_data['is_prepaid'] = ""
            response = api_response.prep_success_response(price_data)
        except CouponNotAppliedException:
            response = api_response.error_response_from_error_message(
                error_message="Coupon code not applicable. Please try from any of the other visible coupon codes.",
                status_code=400)
        except Exception as exception:
            logger.exception('Exception occurred in apply coupon')
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param=request.GET,
                                                        dev_msg="Apply coupon",
                                                        message=exception.__str__(),
                                                        class_name=self.__class__.__name__)
            response = api_response.error_response_from_error_message(
                error_message="Coupon code not applicable. Please try from any of the other visible coupon codes.",
                status_code=400)
        return response
