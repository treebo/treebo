from apps.seo.components.base_component import BaseComponent
from apps.seo.services.weather_widget.weather_service import GetForecast
from apps.seo.serializers.weather_widget_serializer import WeatherWidgetSerializer
import logging

logger = logging.getLogger(__name__)


class WeatherWidget(BaseComponent):

    def __init__(self, area, footer_data):
        super(WeatherWidget, self).__init__(area, footer_data, [])

    def build(self):
        content = GetForecast().get_forecast(area=self.current_city)
        request = self.current_city
        page = request['page']
        identifier = request['identifier']
        city = ""
        if page == 'city' or page == 'category' or page == 'city-amenity':
            city = identifier.name
        elif page == 'locality' or page == 'locality-category':
            city = identifier.city.name
        elif page == 'landmark' or page == 'landmark-category':
            city = identifier.city.name
        if city == "":
            header = 'Weather'
        else:
            header = 'Weather in %s' % (city)
        if len(content) == 0:
            data = {}
            return {
                'weather_widget': {}
            }
        else:
            data = {
                'title': header,
                'content': content
            }
        serialized_obj = WeatherWidgetSerializer(data=data)
        if serialized_obj.is_valid():
            return {
                'weather_widget': serialized_obj.validated_data
            }
        else:
            logger.error(
                'Serializer error in weather widget: %s',
                serialized_obj.errors)
        return {
            'weather_widget': {}
        }
