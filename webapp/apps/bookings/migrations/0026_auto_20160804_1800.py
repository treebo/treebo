# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0025_auto_20160720_1000'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='booking',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read')},
        ),
        migrations.AlterModelOptions(
            name='bookingrequest',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Booking Request',
                'verbose_name_plural': 'Booking Requests'},
        ),
        migrations.AlterModelOptions(
            name='paymentorder',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Payment Order',
                'verbose_name_plural': 'Payment Orders'},
        ),
        migrations.AlterModelOptions(
            name='reservation',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read')},
        ),
        migrations.AlterModelOptions(
            name='roombooking',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Room Booking',
                'verbose_name_plural': 'Room Bookings'},
        ),
    ]
