import logging

from django.core.urlresolvers import reverse
from django.shortcuts import redirect

from apps.hotels import utils as hotel_utils
from apps.hotels.service.hotel_service import HotelService
from base.views.template import TreeboTemplateView

logger = logging.getLogger(__name__)


class CityDetails(TreeboTemplateView):
    template_name = 'city/index.html'
    __mHotelService = HotelService()

    def getData(self, request, args, kwargs):
        city_id, city_string = self.__parseRequest(request, kwargs)
        kwargs = {'cityString': city_string}
        return redirect(reverse('pages:hotels_in_city', kwargs=kwargs))
        # url = settings.TREEBO_BASE_URL + "/search/?locality={0}&lat={1}&long={2}&query={3}&checkin={4}&checkout={5}&roomconfig={6}".format(
        #    '', "", "", kwargs.get('city_string', ''),
        #    datetime.datetime.now().strftime(date_time_utils.DATE_FORMAT),
        #    (datetime.datetime.now() + datetime.timedelta(days=1)).strftime(date_time_utils.DATE_FORMAT), '1')

        # return HttpResponseRedirect(url,status='301')

    def getMobileData(self, request, args, kwargs):
        city_id, city_string = self.__parseRequest(request, kwargs)
        kwargs = {'cityString': city_string}
        return redirect(reverse('pages:hotels_in_city', kwargs=kwargs))
        # mCheckInDate = datetime.date.today()
        # mCheckOutDate = datetime.date.today() + datetime.timedelta(days=1)
        # try:
        #    mCityList = City.objects.get(id=mCityId)
        # except City.DoesNotExist:
        #    raise Exception('No city found')

        # mCity = mCityList
        # mCityOriginal = mCity.name.lower()
        # mCityOriginal = slugify(mCityOriginal)
        # if mCityString != mCityOriginal:
        #    url = mCityOriginal + '-c' + str(hotel_utils.generate_hash(mCityId))
        #    return HttpResponseRedirect('/city/' + url)

        # mHotels = Hotel.objects.filter(city=mCity)

        # mHotels = self.__mHotelService.getAvailabilityAndPriceFromDb(request, mHotels, mCheckInDate, mCheckOutDate)
        # mRestOftCitiesDetails = []
        # try:
        #    mRestOfCities = City.objects.exclude(id=mCityId)
        #    for city in mRestOfCities:
        #        mNumberOfHotels = Hotel.objects.filter(city=city)
        #        url = '/city/' + slugify(city.name.lower()) + '-c' + str(hotel_utils.generate_hash(city.id))
        #        mRestOftCitiesDetails.append({
        #            'hotelCount': len(mNumberOfHotels),
        #            'name': city.name,
        #            'landscapeImage': city.landscape_image,
        #            'latitude': city.city_latitude,
        #            'longitude': city.city_longitude,
        #            'url': url
        #        })
        #    sortedDetails = sorted(mRestOftCitiesDetails, key=operator.itemgetter('hotelCount'), reverse=True)
        # except:
        #    raise Exception('Coming Soon in other cities');

        # if mCity:
        #    mCityName = mCity.name
        #    mCityTagline = mCity.tagline
        #    mCitySubscript = mCity.subscript
        #    mCityDescription = mCity.description
        #    mCityCoverImage = mCity.cover_image
        # mHotelsArray = []
        # for mHotel in mHotels:
        #    imageList = Image.objects.filter(hotel=mHotel, is_showcased=True)
        #    url = '/hotels/' + slugify(mHotel.name.lower()) + '-h' + str(hotel_utils.generate_hash(mHotel.id))

        #    if imageList:
        #        image = imageList[0]
        #        mHotelsArray.append({
        #            'mHotelName': mHotel.name,
        #            'mHotelLocality': mHotel.locality,
        #            'mPriceToShow': mHotel.minRate,
        #            'mImageOfHotel': str(image.url),
        #            'mHotelUrl': url
        #        })
        # mCityBlog = CityBlog.objects.filter(city=mCity)
        # mCityBlogDetails = []
        # for mCityBlogObject in mCityBlog:
        #    mCityBlogDetails.append({
        #        'mHighlight': mCityBlogObject.highlight,
        #        'mBlogImage': str(mCityBlogObject.image_url),
        #        'mBlogLink': mCityBlogObject.link,
        #    })

        # context = {'mCityCoverImage': mCityCoverImage, 'mCityName': mCityName, 'scripts': ['js/city.js'],
        #           'styles': ['css/city.css'],
        #           'mCitySubscript': mCitySubscript, 'mCityDescription': mCityDescription,
        #           'mCityBlogDetails': mCityBlogDetails, 'mHotelsArray': mHotelsArray, 'cityDetails': sortedDetails,
        #           'mCityTagline': mCityTagline}
        # return context

    def __parseRequest(self, request, kwargs):
        d = request.GET
        cityId = hotel_utils.decode_hash(kwargs.get('hashed_city_id', ''))
        cityString = kwargs.get('city_string', '')
        return cityId, cityString
