import logging
import re

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.bookings.models import Booking, AsyncJobStatus
from apps.bookings.repositories.booking_repository import BookingServiceRepository
from apps.bookings.services.booking import BookingService
from apps.common.exceptions.booking_exceptions import BookingNotFoundException
from apps.common.exceptions.custom_exception import IncompleteGSTDetailInRequestException, InvalidRequestException
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)

SUCCESS = 'Success'
FAILED = 'Failed'
gstin_pattern = re.compile('^\d{2}[a-zA-Z]{5}\d{4}[a-zA-Z][1-9a-zA-Z][zZ][0-9a-zA-Z]$')


class UpdateGSTDetail(TreeboAPIView):

    authentication_classes = [CsrfExemptSessionAuthentication, ]

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateGSTDetail, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        request_data = request.data

        booking_order_id = request_data.get('order_id')
        if booking_order_id:
            booking_order_id = str(booking_order_id).strip()

        gstin = request_data.get('gstin')
        if gstin:
            gstin = str(gstin).strip()

        organization_name = request_data.get('organization_name')
        if organization_name:
            organization_name = str(organization_name).strip()

        organization_address = request_data.get('organization_address')
        if organization_address:
            organization_address = str(organization_address).strip()

        try:
            if not (booking_order_id and gstin and organization_name and organization_address):
                raise IncompleteGSTDetailInRequestException("Request params: order_id, gstin, organization_name, "
                                                            "organization_address are mandatory")

            match = gstin_pattern.match(str(gstin))
            if not match:
                raise IncompleteGSTDetailInRequestException("Request param: gstin is invalid")

            rows_matched = BookingServiceRepository.update_booking_with_gst_details(booking_order_id,
                                                                                    gstin,
                                                                                    organization_name,
                                                                                    organization_address)
            # rows_matched will return 0 if no booking found
            if not rows_matched:
                logger.exception("Booking not found for order_id : {0}".format(booking_order_id))
                raise BookingNotFoundException("Booking not found for order_id : {0}".format(booking_order_id))

            booking = Booking.objects.get(order_id=booking_order_id)

            BookingService.update_gst_detail(booking.order_id)

            response_body = {'status': SUCCESS}

            return JsonResponse(response_body, status=HTTP_200_OK)

        except (BookingNotFoundException, IncompleteGSTDetailInRequestException) as e:
            logger.exception('Exception while updating GST detail for Order ID: {0}, GSTIN: {1}, '
                             'Organization Name: {2} and Organization Address {3}'
                             .format(booking_order_id, gstin, organization_name, organization_address))
            data = {'error_message': e.developer_message if e.developer_message else e.message}
            response_body = {
                'status': FAILED,
                'data': data
            }
            return JsonResponse(response_body, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.exception('Exception while updating GST detail for Order ID: {0}, GSTIN: {1}, '
                             'Organization Name: {2} and Organization Address {3}'
                             .format(booking_order_id, gstin, organization_name, organization_address))
            data = {'error_message': str(e)}
            response_body = {
                'status': FAILED,
                'data': data
            }
            return JsonResponse(response_body, status=HTTP_500_INTERNAL_SERVER_ERROR)
