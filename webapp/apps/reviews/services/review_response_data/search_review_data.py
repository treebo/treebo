import logging
from apps.reviews.services.review_response_data.base_review_data import BaseReviewData

logger = logging.getLogger(__name__)


class SearchReviewData(BaseReviewData):

    def __init__(self, hotel_id, ta_loc_id):
        self.count = None
        self.image = None
        self.rating = None
        self.award = {}
        super(SearchReviewData, self).__init__(hotel_id, ta_loc_id)

    def build(self,):
        try:
            self.count, self.image, self.rating = self.overall_rating_obj.num_reviews_count, self.overall_rating_obj.url_overall_rating_image.replace(
                "http:", "https:"), self.overall_rating_obj.overall_rating
            awards = self.get_awards_list()
            if awards:
                self.award = awards[0]
        except BaseException:
            logger.info(
                'Exception occured in building Searh review data %s : %s',
                self.hotel_id,
                self.ta_location_id)
