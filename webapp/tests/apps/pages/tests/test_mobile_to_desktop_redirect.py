import mock
from django.core.urlresolvers import reverse
from django.test import TestCase, RequestFactory

from apps.pages.views import (
    aboutus, bookings, contactus, faq, iesupport, itinerary, join, offers,
    policy, terms, voucher, landing, order
)


class RedirectTest(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.redirect_views = {
            aboutus.Aboutus: reverse('pages:aboutus'),
            contactus.Contactus: reverse('pages:contact-us'),
            iesupport.Iesupport: reverse('pages:iesupport'),
            join.Join: reverse('pages:joinus'),
            # Should login be also be redirected? getData is not implemented there.
            # login.Login: reverse('login'),
            offers.Offers: reverse('pages:offers'),
            policy.Policy: reverse('pages:policy'),
            voucher.Voucher: reverse('pages:voucher'),
            # unconfirmed.Unconfirmed: reverse('pages:unconfirmed'),
        }
        cls.noredirect_views = {
            faq.Faq: reverse('pages:faq'),
            landing.Index: reverse('pages:index'),
            terms.Terms: reverse('pages:terms'),
            bookings.Bookings: reverse('pages:booking_history'),
            order.Order: reverse(
                'pages:order',
                kwargs={
                    'order_id': 'TRB-123234123'})}
        super(RedirectTest, cls).setUpClass()

    def setUp(self):
        self.factory = RequestFactory()
        self.patchers = []

        # Patch getData and getMobileData for all the views we're going to
        # call. We're just testing the template_name used and redirects
        import itertools
        for view, url in list(
            dict(
                itertools.chain(
                    iter(
                list(
                    self.redirect_views.items())), iter(
                        list(
                            self.noredirect_views.items())))).items()):
            get_data_patcher = mock.patch.object(
                view, 'getData', self.mock_get_data)
            get_mobile_data_patcher = mock.patch.object(
                view, 'getMobileData', self.mock_get_mobile_data)
            get_data_patcher.start()
            get_mobile_data_patcher.start()
            self.patchers.append(get_data_patcher)
            self.patchers.append(get_mobile_data_patcher)

    def tearDown(self):
        for patcher in self.patchers:
            patcher.stop()

    def mock_get_mobile_data(self, request, args, kwargs):
        return dict()

    def mock_get_data(self, request, args, kwargs):
        return dict()

    def test_valid_redirect(self):
        for view, url in list(self.redirect_views.items()):
            try:
                request = self.factory.get(url)
                request.is_mobile = True
                view_object = view.as_view(template_name='test_template.html')
                response = view_object(request=request)
                self.assertEqual(
                    response.template_name,
                    ['desktop/test_template.html'])
            except AssertionError as e:
                print(("Failed for view:", view, ' and url:', url))
                print(e)

    def test_mobile_request_no_redirect(self):
        for view, url in list(self.noredirect_views.items()):
            try:
                request = self.factory.get(url)
                request.is_mobile = True
                view_object = view.as_view(template_name='test_template.html')
                response = view_object(request=request)
                self.assertEqual(
                    response.template_name,
                    ['mobile/test_template.html'])
            except AssertionError as e:
                print(("Failed for view:", view, ' and url:', url))
                print(e)
