# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('pricing', '0007_auto_20160518_1006'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='channelmanagerrateplan',
            options={},
        ),
        migrations.AlterModelOptions(
            name='luxurytax',
            options={},
        ),
        migrations.AlterModelOptions(
            name='preferredpromo',
            options={},
        ),
        migrations.AlterModelOptions(
            name='promoconditions',
            options={},
        ),
        migrations.AlterModelOptions(
            name='roomprice',
            options={},
        ),
        migrations.AlterModelOptions(
            name='roomrateplan',
            options={},
        ),
        migrations.AlterModelOptions(
            name='statetax',
            options={},
        ),
    ]
