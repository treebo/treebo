import logging

from django.db.models.query_utils import Q
from django.conf import settings
from apps.hotels.service.hotel_meta_service import HotelMetaService
from apps.search.constants import SEARCH_PARAMS_PRIORITY_MAP, LOCATION_TYPE_SEARCH_PARAMS_MAP
from dbcommon.models.hotel import Hotel
from dbcommon.models.location import City, Locality, CityAlias, State
from apps.hotels.service.hotel_search_service import HotelSearchService
from dbcommon.models.landmark import SearchLandmark
from apps.common.exceptions.custom_exception import NoHotelsFoundException
from apps.promotions.services.promotion_service import PromotionsService

logger = logging.getLogger(__name__)

LOCATION_TYPES = {
    "hotel": "hotel",
    "locality": "locality",
    "landmark": "landmark",
    "city": "city",
    "state": "state",
    "country": "country",
    "geo": "geo"}
SEARCH_LOCATION_TYPE_METHOD_MAP = {
    'country': 'get_country_hotels',
    'state': 'get_state_hotels',
    'city': 'get_city_hotels',
    'locality': 'get_locality_hotels',
    'landmark': 'get_landmark_hotels',
    'hotel': 'get_hotel',
    'nearby_hotels': 'get_nearby_hotels',
    'category': 'get_category_hotel',
    'geo': 'get_geo_based_hotels'
}


class SearchService:
    def __init__(self, hotel_id=None, hotel_name=None, locality_name=None, landmark_name=None, city_name=None,
                 state_name=None, country_name=None, location_type=None, nearby=False, distance_cap=False,
                 category=None, amenity=None, meta_keys=None, latitude=None, longitude=None, radius=None):
        self.hotel_id = hotel_id
        self.hotel_name = hotel_name
        self.locality_name = locality_name
        self.landmark_name = landmark_name
        self.city_name = city_name
        self.state_name = state_name
        self.country_name = country_name
        self.longitude = longitude
        self.latitude = latitude
        self.radius = radius
        self.location_type = location_type
        self.nearby = nearby
        self.sort_params = {}
        self.category = category
        self.amenity = amenity
        self.extra_params = {}
        self.distance_cap = distance_cap
        if category:
            self.extra_params.update({'category__name__iexact': category})
        if amenity:
            self.extra_params.update({'facility__name__iexact': amenity})
        self.meta_keys = meta_keys

    def __get_location_type(self):
        '''
        location_type: Based on location type the search is made for the given search params.
        If location type is not present in the request, following is the priority for the
        search made hotel id being at the highest priority:
        SEARCH_PARAMS_PRIORITY_MAP = {
            'hotel_id': 6,
            'hotel_name': 5,
            'locality_name': 4,
            'city_name': 3,
            'state_name': 2,
            'country_name': 1
        }
        :return: returns the location type
        '''
        if self.location_type:
            return self.location_type
        filter_params_obj = self.__dict__
        filter_params = list(filter_params_obj.keys())
        location_type = ''
        current_priority = 0
        for param in filter_params:
            if filter_params_obj.get(param) and SEARCH_PARAMS_PRIORITY_MAP.get(
                    param, 0) > current_priority:
                location_type = LOCATION_TYPE_SEARCH_PARAMS_MAP.get(param)
                current_priority = SEARCH_PARAMS_PRIORITY_MAP.get(param)
        return location_type

    def search(self):
        '''
        :return: Return hotel objects for the given searched parameters
        '''
        self.location_type = self.__get_location_type()
        mapped_method_name = SEARCH_LOCATION_TYPE_METHOD_MAP.get(
            self.location_type.lower())
        if mapped_method_name:
            method_name = getattr(self, mapped_method_name)
            try:
                hotel_meta_svc = HotelMetaService(
                    keys=self.meta_keys, hotels=method_name())
                return hotel_meta_svc.filter()
            except RuntimeError as e:
                logger.exception("No hotel was returned in the search results")
        return []

    def check_if_hotels_found(self, sort_params):
        if not 'distance' in sort_params:
            return False
        else:
            if not ('lat' in sort_params['distance'] and 'long' in sort_params['distance']):
                return False
        return True

    def get_hotel(self):
        """
        :return: all hotels close to given hotel id
        """

        return self.__get_hotel_city_hotels()

    def get_locality_hotels(self):
        '''
        :return: Return hotels in the locality or nearby hotels for the locality
        '''
        if self.nearby:
            return self.__get_locality_city_hotels()
        else:
            return self.__get_locality_hotels()

    def get_city_hotels(self):
        '''
        :return: Return hotels in the city. Further implementations can also include
        nearby hotels for the city.
        '''
        return self.__get_city_hotels()

    def get_state_hotels(self):
        '''
        :return:  Return hotels in state.
        '''
        return self.__get_state_hotels()

    def get_country_hotels(self):
        '''
        Getting all the hotels assuming the implementation only for one country
        :param country: country Name
        :return: Hotels for the given country
        '''
        return Hotel.enabled_hotel_objects.all()

    def get_category_hotel(self):
        '''Returns all the hotels belonging to a particular category'''
        return Hotel.enabled_hotel_objects.filter(
            category__name__iexact=self.category)

    def get_geo_based_hotels(self):
        hotel_search_service = HotelSearchService(city='', latitude=self.latitude, longitude=self.longitude,
                                                  distance_cap=self.radius, amenity_name=self.amenity,
                                                  category_name=self.category)
        hotel_result = hotel_search_service.search_hotels_with_radius()
        return hotel_result

    def __get_state_hotels(self):
        states_obj = State.objects.filter(
            Q(name__iregex='^.*' + self.state_name.strip() if self.state_name else ''))
        hotels_obj = []
        state_obj = states_obj.first()
        if state_obj:
            hotels_obj = Hotel.enabled_hotel_objects.filter(state=state_obj)
        return hotels_obj

    def __get_city_hotels(self):
        cities_alias_obj = CityAlias.objects.filter(
            Q(alias__istartswith=self.city_name.strip() if self.city_name else '')).values('city')
        hotels_obj = []
        city_alias_obj = cities_alias_obj.first()
        city_name = None
        if city_alias_obj:
            city_name = city_alias_obj['city']
        if city_name:
            city_name = city_name.lower()
            city = City.objects.filter(
                Q(name__iexact=city_name) | Q(slug__iexact=city_name)).first()
            if city:
                hotels_obj = Hotel.enabled_hotel_objects.filter(
                    city=city, **self.extra_params)
        return hotels_obj

    def get_city_lat_lng(self):
        city_obj = City.objects.filter(Q(
            name__iregex='^.*' + self.city_name.strip() if self.city_name else ''))
        city = city_obj.first()
        if city:
            self.sort_params['distance'] = {}
            self.sort_params['distance']['lat'] = city.city_latitude
            self.sort_params['distance']['long'] = city.city_longitude
        return

    def __get_locality_hotels(self):
        locality_obj = Locality.objects.filter(
            Q(Q(city__name__iexact=self.city_name.lower()) | Q(city__slug__iexact=self.city_name.lower()))
            & Q(Q(name__iexact=self.locality_name))).first()

        distance_cap = settings.SEARCH_DEFAULT_SEO_DISTANCE_CAP
        if locality_obj:
            self.sort_params['distance'] = {}
            self.sort_params['distance']['lat'] = locality_obj.latitude
            self.sort_params['distance']['long'] = locality_obj.longitude
            if self.distance_cap and locality_obj.distance_cap:
                distance_cap = locality_obj.distance_cap
        else:
            self.get_city_lat_lng()

        if not self.check_if_hotels_found(self.sort_params):
            raise NoHotelsFoundException

        hotel_search_service = HotelSearchService(city='', latitude=self.sort_params['distance']['lat'],
                                                  longitude=self.sort_params['distance']['long'],
                                                  distance_cap=distance_cap, amenity_name=self.amenity,
                                                  category_name=self.category)
        hotel_result = hotel_search_service.search_hotels_with_radius()
        return hotel_result

    def __get_locality_city_hotels(self):
        localities_obj = Locality.objects.filter(
            Q(
                name__iregex='^.*' +
                self.locality_name.strip() if self.locality_name else '')).select_related('city')
        locality = localities_obj.first()
        hotels_obj = []
        if locality:
            self.sort_params['distance'] = {}
            self.sort_params['distance']['lat'] = locality.latitude
            self.sort_params['distance']['long'] = locality.longitude
            city = locality.city
            hotels_obj = Hotel.enabled_hotel_objects.filter(city=city)
        return hotels_obj

    def __get_hotel_city_hotels(self):

        if self.hotel_id:
            hotels_obj = Hotel.enabled_hotel_objects.filter(id=self.hotel_id)
        else:
            hotels_obj = Hotel.enabled_hotel_objects.filter(
                Q(name__iregex='^.*' + self.hotel_name if self.hotel_name else ''))
        hotel_obj = hotels_obj.first()
        if hotel_obj:
            self.sort_params['distance'] = {}
            self.sort_params['distance']['lat'] = hotel_obj.latitude
            self.sort_params['distance']['long'] = hotel_obj.longitude
        else:
            self.get_city_lat_lng()

        distance_cap = settings.SEARCH_DEFAULT_CITY_DISTANCE_CAP
        hotel_search_service = HotelSearchService(city='', latitude=self.sort_params['distance']['lat'],
                                                  longitude=self.sort_params['distance']['long'],
                                                  distance_cap=distance_cap, amenity_name=self.amenity,
                                                  category_name=self.category)
        hotel_result = hotel_search_service.search_hotels_with_radius()
        return hotel_result

    def get_landmark_hotels(self):

        landmark_obj = SearchLandmark.objects.filter(
            Q(Q(city__name__iexact=self.city_name.lower()) | Q(city__slug__iexact=self.city_name.lower())) &
            Q(Q(free_text_url__iexact=self.landmark_name) | Q(seo_url_name__iexact=self.landmark_name))).first()

        distance_cap = settings.SEARCH_DEFAULT_SEO_DISTANCE_CAP
        if landmark_obj:
            self.sort_params['distance'] = {}
            self.sort_params['distance']['lat'] = landmark_obj.latitude
            self.sort_params['distance']['long'] = landmark_obj.longitude
            if self.distance_cap and landmark_obj.distance_cap:
                distance_cap = landmark_obj.distance_cap
        else:
            self.get_city_lat_lng()

        if not self.check_if_hotels_found(self.sort_params):
            raise NoHotelsFoundException

        hotel_search_service = HotelSearchService(city='', latitude=self.sort_params['distance']['lat'],
                                                  longitude=self.sort_params['distance']['long'],
                                                  distance_cap=distance_cap, amenity_name=self.amenity,
                                                  category_name=self.category)
        hotel_result = hotel_search_service.search_hotels_with_radius()

        return hotel_result
