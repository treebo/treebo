# -*- coding:utf-8 -*-
"""
    HX Booking Client Utils
"""


import base64
import sys
import traceback
import zlib
from datetime import datetime, time

import pytz
import requests
from django.conf import settings
from django.views.debug import ExceptionReporter

from apps.bookings.constants import CATALOGUING_SERVICE_APPLICATION_URL, CATALOGUING_SERVICE_SUB_CHANNELS_URL, \
    CATALOGUING_SERVICE_ROOM_TYPES_URL, CS_OTHER_SUB_CHANNEL, CS_OTHER_APPLICATION
from apps.bookings.services.notification.email.cancellation_email import CancellationEmailService
from common.custom_logger.booking.log import get_booking_logger

logger = get_booking_logger(__name__)
ist_timezone = pytz.timezone('Asia/Kolkata')
room_type_map_direct_to_cs = {}
application_id_map_direct_to_cs = {}
sub_channel_id_map_direct_to_cs = {}


def get_exception_mail(e, subject=None, request=None):
    """
    :param e:
    :param subject:
    :param request:
    :return:
    """
    logger.info("Getting exception mail")
    subject, message, html = get_exception_mail_details(e, subject, request)
    logger.debug("Message for exception mail is %s", message)
    # compressed as size of frame for rmq message is exceeded sometimes
    base64_html = base64.b64encode(zlib.compress(html.encode('utf-8')))
    return {
        "exception_mail": {
            "subject": subject,
            "message": message,
            "html": base64_html
        }
    }


def get_exception_mail_details(e, subject=None, request=None):
    """
    :param e:
    :param subject:
    :param request:
    :return:
    """
    exc_info = sys.exc_info()
    reporter = ExceptionReporter(request, is_email=True, *exc_info)
    if not subject:
        subject = "Create booking failed"
    if request:
        message = "%s\n\n%s" % (
            '\n'.join(traceback.format_exception(*exc_info)),
            reporter.filter.get_request_repr(request)
        )
    else:
        message = "%s\n\n" % (
            '\n'.join(traceback.format_exception(*exc_info))
        )
    return subject, message, reporter.get_traceback_html()


def get_cancellation_mail(order_id):
    """
    :param order_id:
    :return:
    """
    return {
        'cancellation_mail': {
            'subject': 'Cancellation Failed for Order Id: {0}'.format(order_id),
            'body': 'Cancellation request failed for Order ID: {0}. Reason: Unable to get order details from '
            ''.format(order_id),
            'to': settings.SERVER_EMAIL,
            'from': settings.CANCELLATION_FAILURE_EMAIL_LIST}}


def send_booking_cancellation_email(booking):
    """
    send cancellation email
    :param booking:
    :return:
    """

    try:
        subject = "Booking Cancellation - " + \
            str(booking.hotel.name) + " - " + booking.order_id
        email_service = CancellationEmailService(
            CancellationEmailService.NOTIFICATION_EMAIL, "html", [
                booking.guest_email], subject, None)

        email_service.collateEmailData(booking)
        email_service.createMessage()
        email_service.sendMessage()

    except Exception:
        logger.exception("BookingCancellation Mail send failed")


def get_response(status_code, response_message, response_data=None):
    """
    :param status_code:
    :param response_message:
    :param response_data:
    :return:
    """
    data = {}
    if response_data:
        for obj in response_data:
            for key in list(obj.keys()):
                data[key] = obj[key]

    return {
        "status_code": status_code,
        "response_message": response_message,
        "response_data": data
    }


def convert_date_to_checkin_datetime(date):
    return datetime.combine(date, time(12, 00))


def convert_date_to_checkout_datetime(date):
    return datetime.combine(date, time(11, 00))


def convert_date_to_datetime(date):
    return datetime.combine(date, datetime.min.time())


def convert_utc_to_localtime(date_time):
    time_tzinfo = pytz.UTC.localize(date_time)
    return time_tzinfo.astimezone(ist_timezone)


def add_time_zone(date_time):
    return ist_timezone.localize(date_time)


def get_cs_room_type_code(room_type_code):
    if not room_type_map_direct_to_cs:
        get_all_room_types_from_cs()

    return room_type_map_direct_to_cs.get(room_type_code)


def get_cs_application_id(booking_channel):
    if not application_id_map_direct_to_cs:
        get_all_applications_from_cs()

    cs_application_id = application_id_map_direct_to_cs.get(booking_channel)
    if not cs_application_id:
        cs_application_id = application_id_map_direct_to_cs.get(CS_OTHER_APPLICATION)

    return cs_application_id


def get_cs_sub_channel_id(booking_channel, utm_source):
    if not sub_channel_id_map_direct_to_cs:
        get_all_sub_channels_from_cs()

    if booking_channel and utm_source:
        direct_sub_channel = booking_channel.upper() + '-' + utm_source.upper()
    else:
        direct_sub_channel = CS_OTHER_SUB_CHANNEL

    cs_sub_channel_id = sub_channel_id_map_direct_to_cs.get(direct_sub_channel)
    if not cs_sub_channel_id:
        cs_sub_channel_id = sub_channel_id_map_direct_to_cs.get(CS_OTHER_SUB_CHANNEL)

    return cs_sub_channel_id


def get_all_applications_from_cs():
    url = settings.CATALOGUING_SERVICE_BASE_URL + CATALOGUING_SERVICE_APPLICATION_URL
    response = requests.get(url)
    applications = response.json()
    for application in applications:
        application_id_map_direct_to_cs[application.get("name")] = application.get("id")


def get_all_sub_channels_from_cs():
    url = settings.CATALOGUING_SERVICE_BASE_URL + CATALOGUING_SERVICE_SUB_CHANNELS_URL
    response = requests.get(url)
    sub_channels = response.json()
    for sub_channel in sub_channels:
        sub_channel_id_map_direct_to_cs[sub_channel.get("name").upper()] = sub_channel.get("id")


def get_all_room_types_from_cs():
    url = settings.CATALOGUING_SERVICE_BASE_URL + CATALOGUING_SERVICE_ROOM_TYPES_URL
    response = requests.get(url)
    room_types = response.json()

    for room_type in room_types:
        room_type_map_direct_to_cs[room_type.get("crs_room_type_code")] = room_type.get("code")


def get_room_codes(room_types):
    room_codes = []
    for room_type in room_types:
        room_code = get_cs_room_type_code(room_type.upper())
        room_codes.append(room_code)
    return room_codes
