import logging

from apps.common.exceptions.custom_exception import InvalidPageException
from apps.seo.pages.common_page import CommonPage
from apps.seo.pages_v3.hd_page import HDPageV3
from apps.seo.pages_v3.me import Me
from apps.content.constants import PAGES
from apps.seo.pages.category_page import CategoryPage
from apps.seo.pages.city_page import CityPage
from apps.seo.pages.hd_page import HDPage
from apps.seo.pages.landmark_page import LandmarkPage
from apps.seo.pages.locality_page import LocalityPage
from apps.seo.pages_v3.city_page import CityPage as CityV3
from apps.seo.pages_v3.landmark_page import LandmarkPage as LandmarkV3
from apps.seo.pages_v3.category_page import CategoryPage as CategoryV3
from apps.seo.pages_v3.locality_page import LocalityPage as LocalityV3
from apps.seo.pages_v3.city_amenity_page import CityAmenityPage
from apps.seo.pages_v3.locality_category_page import LocalityCategoryPage
from apps.seo.pages_v3.landmark_category_page import LandmarkCategoryPage

__author__ = 'ansilkareem'

logger = logging.getLogger(__name__)


class MetaFactory():

    def build_meta(self, page, query):
        page = page
        query = query
        meta_object = None
        if page == PAGES.CITY:
            meta_object = CityPage(page, query)
        elif page == PAGES.CATEGORY:
            meta_object = CategoryPage(page, query)
        elif page == PAGES.HD:
            meta_object = HDPage(page, query, False)
        if page == PAGES.LANDMARK:
            meta_object = LandmarkPage(page, query)
        elif page == PAGES.LOCALITY:
            meta_object = LocalityPage(page, query)
        elif page == PAGES.COMMON:
            meta_object = CommonPage(page, query)
        meta_object.build_meta()
        return meta_object.meta_content

    def build_meta_v3(self, page, query, is_amp, ta_review_count):
        meta_object = None
        try:
            page = page
            query = query
            pages = {
                PAGES.CITY: CityV3,
                PAGES.CATEGORY: CategoryV3,
                PAGES.HD: HDPageV3,
                PAGES.LANDMARK: LandmarkV3,
                PAGES.LOCALITY: LocalityV3,
                PAGES.CITY_AMENITY: CityAmenityPage,
                PAGES.LOCALITY_CATEGORY: LocalityCategoryPage,
                PAGES.LANDMARK_CATEGORY: LandmarkCategoryPage,
                PAGES.NEAR_ME: Me,
                PAGES.COMMON: CommonPage
            }
            meta_object = pages[page](page, query, is_amp)
            build_meta_request_args = {
                "ta_review_count": ta_review_count
            }
            meta_object.build_meta(build_meta_request_args)
        except InvalidPageException as e:
            logger.exception(e.__str__())
        return meta_object.meta_content
