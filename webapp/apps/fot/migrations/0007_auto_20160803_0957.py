# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fot', '0006_auto_20160629_1447'),
    ]

    operations = [
        migrations.AddField(
            model_name='fot',
            name='feedback_dropped_out',
            field=models.PositiveIntegerField(default=0, null=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='free_nights_deemed',
            field=models.PositiveIntegerField(default=0, null=True),
        ),
        migrations.AddField(
            model_name='fot',
            name='is_active',
            field=models.NullBooleanField(default=False),
        ),
    ]
