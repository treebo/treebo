from rest_framework import serializers


class BlogSerializer(serializers.Serializer):
    name = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False,
        max_length=1000)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False,
        max_length=1000)
    image_url = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False,
        max_length=1000)
    blog_url = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False,
        max_length=1000)
    city = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False,
        max_length=1000)
    priority = serializers.IntegerField()


class BlogComponentSerializer(serializers.Serializer):
    title = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True,
        max_length=50)
    content = BlogSerializer(allow_null=True, required=False, many=True)
