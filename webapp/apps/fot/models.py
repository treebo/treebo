from django.db import models
from django.db.models.expressions import F
from djutil.models import TimeStampedModel

from apps.bookings.models import Booking
from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.models import profile as profiles_models
from dbcommon.models.hotel import Hotel

from services.restclient.bookingrestclient import BookingClient


class FotHotel(TimeStampedModel, DefaultPermissions):
    hotel = models.ForeignKey(Hotel, on_delete=models.DO_NOTHING)
    credits_per_audit = models.PositiveIntegerField(
        db_column='credits_per_week', default=1000)
    ACTIVE = 'ACTIVE'
    BLACKOUT = 'BLACKOUT'
    WEEK = 'WEEK'
    MONTH = 'MONTH'
    YEAR = 'YEAR'
    STATUS_CHOICES = [(ACTIVE, ACTIVE), (BLACKOUT, BLACKOUT)]
    SLOTS_CHOICES = [(WEEK, WEEK), (MONTH, MONTH), (YEAR, YEAR)]
    status = models.CharField(
        default=ACTIVE,
        choices=STATUS_CHOICES,
        max_length=100)
    slots_allowed = models.PositiveIntegerField(null=False, default=1)
    interval_between_slots = models.CharField(
        default=WEEK, choices=SLOTS_CHOICES, max_length=100)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_fothotel'
        verbose_name = 'FOT Hotel'
        verbose_name_plural = 'FOT Hotels'

    def __str__(self):
        return self.hotel.name


class FotHotelBlackOut(TimeStampedModel, DefaultPermissions):
    fot_hotel = models.ForeignKey(FotHotel, on_delete=models.DO_NOTHING)
    blackout_start_time = models.DateTimeField()
    blackout_end_time = models.DateTimeField()
    ACTIVE = 'ACTIVE'
    BLACKOUT = 'BLACKOUT'
    STATUS_CHOICES = [(ACTIVE, ACTIVE), (BLACKOUT, BLACKOUT)]
    status = models.CharField(
        default=ACTIVE,
        choices=STATUS_CHOICES,
        max_length=100)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'FOT Hotel blackout dates'
        verbose_name_plural = 'FOT Hotel blackout dates'

    def __str__(self):
        return self.fot_hotel.hotel.name


class FotReservationsRequest(TimeStampedModel, DefaultPermissions):
    user = models.ForeignKey(
        profiles_models.User,
        db_column='user_id',
        on_delete=models.DO_NOTHING)
    check_in_date = models.DateField(null=True)
    check_out_date = models.DateField(null=True)
    hotel = models.ForeignKey(Hotel, on_delete=models.DO_NOTHING)
    no_of_guests = models.PositiveIntegerField(null=True)
    booking = models.ForeignKey(
        Booking, null=True, on_delete=models.DO_NOTHING)
    PENDING = 'PENDING'
    SCHEDULED = 'SCHEDULED'
    CANCELLED = 'CANCELLED'
    CANCEL_PENDING = 'CANCEL_PENDING'
    CHECKEDIN = 'CHECKEDIN'
    CHECKEDOUT = 'CHECKEDOUT'
    NOSHOW = 'NOSHOW'
    FEEDBACK_SUBMITTED = 'FEEDBACK_SUBMITTED'
    FEEDBACK_NOT_SUBMITTED = 'FEEDBACK_NOT_SUBMITTED'
    STATUS_CHOICES = {
        (PENDING, PENDING),
        (SCHEDULED, SCHEDULED),
        (CANCELLED, CANCELLED),
        (CANCEL_PENDING, CANCEL_PENDING),
        (CHECKEDIN, CHECKEDIN),
        (CHECKEDOUT, CHECKEDOUT),
        (NOSHOW, NOSHOW),
        (FEEDBACK_SUBMITTED, FEEDBACK_SUBMITTED),
        (FEEDBACK_NOT_SUBMITTED, FEEDBACK_NOT_SUBMITTED)}
    status = models.CharField(
        default=PENDING,
        choices=STATUS_CHOICES,
        max_length=100)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_fotreservationsrequest'
        verbose_name = 'FOT Hotel reservation'
        verbose_name_plural = 'FOT Hotel reservations'

    def cancel_audit(self):
        return BookingClient.cancel_booking(
            self.booking.order_id, self.user.email)

    def __str__(self):
        try:
            return self.booking.order_id
        except BaseException:
            return ''


class Fot(TimeStampedModel, DefaultPermissions):
    user = models.ForeignKey(
        profiles_models.User,
        null=False,
        db_column="user_id",
        on_delete=models.DO_NOTHING)
    user_credits = models.PositiveIntegerField(
        default=0, null=False, db_column="credits_available")
    user_slots = models.PositiveIntegerField(null=False, default=2)
    score = models.DecimalField(
        max_digits=8,
        decimal_places=2,
        null=True,
        blank=True,
        default=0)
    PASSED = 'PASSED'
    FAILED = 'FAILED'
    BLOCKED = 'BLOCKED'
    IN_PROGRESS = 'IN_PROGRESS'
    NOT_CONFIRMED = 'NOT_CONFIRMED'
    STATUS_CHOICES = {
        (PASSED, PASSED),
        (FAILED, FAILED),
        (BLOCKED, BLOCKED),
        (IN_PROGRESS, IN_PROGRESS),
        (NOT_CONFIRMED, NOT_CONFIRMED)}
    status = models.CharField(
        default=IN_PROGRESS,
        choices=STATUS_CHOICES,
        max_length=100)
    notification = models.TextField(null=True)
    show_notification = models.BooleanField(default=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    marital_status = models.CharField(max_length=100, null=True, blank=True)
    assets = models.CharField(max_length=100, null=True, blank=True)
    profession = models.CharField(max_length=100, null=True, blank=True)
    audit_duration = models.CharField(max_length=100, null=True, blank=True)
    travel_experience = models.TextField(null=True, blank=True)
    about_yourself = models.TextField(null=True, blank=True)
    why_treebo = models.TextField(null=True, blank=True)
    source_website = models.CharField(max_length=100, null=True, blank=True)
    social_web_profile = models.URLField(max_length=200, null=True, blank=True)
    current_address = models.CharField(max_length=100, null=True, blank=True)
    friend_name = models.CharField(max_length=100, null=True, blank=True)
    questions_asked = models.CharField(max_length=100, null=True, blank=True)
    answers_answered = models.CharField(max_length=100, null=True, blank=True)
    confirmation_key = models.CharField(max_length=100, null=True, blank=True)
    total_audits_scheduled = models.PositiveIntegerField(default=0)
    future_audits = models.PositiveIntegerField(default=0)
    no_shows = models.PositiveIntegerField(default=0)
    feedbacks_submitted = models.PositiveIntegerField(default=0)
    qualified_feedbacks = models.PositiveIntegerField(default=0)
    total_free_nights = models.PositiveIntegerField(default=0)
    free_nights_remaining = models.PositiveIntegerField(default=0)
    free_nights_deemed = models.PositiveIntegerField(default=0, null=True)
    total_credits_redeemed = models.PositiveIntegerField(default=0, null=True)
    feedback_dropped_out = models.PositiveIntegerField(default=0, null=True)
    is_active = models.NullBooleanField(default=False, null=True)

    # isActive

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_fot'
        verbose_name = 'FOT User'
        verbose_name_plural = 'FOT Users'

    def __str__(self):
        return self.user.email

    def free_audit_slot(self):
        self.future_audits = F('future_audits') - 1
        self.save()

    def conduct_audit(self):
        self.total_audits_scheduled = F('total_audits_scheduled') + 1
        self.future_audits = F('future_audits') + 1
        self.save()


class FotCredits(TimeStampedModel, DefaultPermissions):
    earned = models.PositiveIntegerField(null=False)
    fot = models.ForeignKey(Fot, on_delete=models.DO_NOTHING)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_fotcredits'


class FotSignupQuestions(models.Model):
    question = models.TextField(null=True)
    RADIO = 'RADIO'
    CHECKBOX = 'CHECKBOX'
    TEXT = 'TEXT'
    TEXTAREA = 'TEXTAREA'
    DROPDOWN = 'DROPDOWN'
    PERSONAL = 'PERSONAL'
    PROBLEM_SOLVING = 'PROBLEM_SOLVING'
    ABOUT_FOT = 'ABOUT_FOT'
    STATUS_CHOICES = {
        (RADIO, RADIO),
        (CHECKBOX, CHECKBOX),
        (TEXT, TEXT),
        (TEXTAREA, TEXTAREA),
        (DROPDOWN, DROPDOWN)
    }
    SECTION_CHOICES = {
        (PERSONAL, PERSONAL),
        (PROBLEM_SOLVING, PROBLEM_SOLVING),
        (ABOUT_FOT, ABOUT_FOT),
    }
    question_type = models.CharField(choices=STATUS_CHOICES, max_length=100)
    section = models.CharField(
        choices=SECTION_CHOICES,
        max_length=100,
        default='PERSONAL')

    class Meta(DefaultPermissions.Meta):
        pass

    def __str__(self):
        return str(self.id)


class FotSignupAnswers(models.Model):
    question = models.ForeignKey(
        FotSignupQuestions,
        on_delete=models.DO_NOTHING)
    option = models.CharField(max_length=300)
    score = models.IntegerField(default=0)
    value = models.CharField(max_length=100, default='A')

    class Meta(DefaultPermissions.Meta):
        pass


class FotConfig(models.Model):
    start_day_threshold = models.PositiveIntegerField(default=1)
    end_day_threshold = models.PositiveIntegerField(default=42)
    max_guests_allowed = models.PositiveIntegerField(default=2)
    score_to_pass = models.PositiveIntegerField(default=60)
    max_credits_to_be_redeemed = models.PositiveIntegerField(default=1000)
    credit_equivalent_per_night = models.PositiveIntegerField(default=1000)
    coupon_expiry = models.PositiveIntegerField(default=60)
    # max_credits_to_be_redeemed should always be greater than or equal to
    # credit_equivalent_per_night

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'FOT Config'
        verbose_name_plural = 'FOT Config'

    def __str__(self):
        return str(self.id)
