class PhonePeConfirmationDataDTO(object):
    def __init__(self, providerReferenceId, paymentState, amount,
                 payReposneCode, transactionId,
                 merchantId):
        self.provider_reference_id = providerReferenceId
        self.payment_state = paymentState
        self.amount = amount
        self.pay_reposne_code = payReposneCode
        self.transaction_id = transactionId
        self.merchant_id = merchantId


class PhonePeConfirmationDTO(object):
    def __init__(self, success, message, code, phonepe_confiramtion_data):
        self.success = success
        self.message = message
        self.code = code
        self.phonepe_confiramtion_data = phonepe_confiramtion_data
