# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0024_auto_20160719_0538'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='instant_booking',
            field=models.NullBooleanField(default=None),
        ),
    ]
