import logging

from django.contrib import admin

from apps.seo.models.web_catalog_hotel_mapping import WebCatalogHotelMapping, CatalogRoomMapping

logger = logging.getLogger(__name__)


@admin.register(WebCatalogHotelMapping)
class WebCatalogHotelMappingAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'catalog_hotel_id',
        'get_hotel_id',
        'get_hotel_name',
        'get_hotelogix_id')
    search_fields = (
        'catalog_hotel_id',
        'hotel__id',
        'hotel__name',
        'hotel__hotelogix_id')
    list_filter = (
        'catalog_hotel_id',
        'hotel__id',
        'hotel__name',
        'hotel__hotelogix_id')
    actions = ['delete_selected']

    def has_add_permission(self, request):
        return True

    def get_hotel_id(self, obj):
        if obj and obj.hotel:
            return str(obj.hotel.id)
        else:
            return None

    def get_hotel_name(self, obj):
        if obj and obj.hotel:
            return str(obj.hotel.name)
        else:
            return None

    def get_hotelogix_id(self, obj):
        if obj and obj.hotel:
            return str(obj.hotel.hotelogix_id)
        else:
            return None

    get_hotel_id.short_description = 'Hotel ID'
    get_hotel_name.short_description = 'Hotel Name'
    get_hotelogix_id.short_description = 'Hotelogix ID'


@admin.register(CatalogRoomMapping)
class CatalogRoomMappingAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'get_catalog_hotel_id',
        'get_hotel_id',
        'get_hotel_name',
        'get_hotelogix_id',
        'get_room_type_code',
        'get_room_id',
        'catalog_room_id')
    search_fields = (
        'hotel_map__catalog_hotel_id',
        'hotel_map__hotel__id',
        'hotel_map__hotel__name',
        'hotel_map__hotel__hotelogix_id',
        'room__room_type_code',
        'room__id',
        'catalog_room_id')
    list_filter = (
        'hotel_map__catalog_hotel_id',
        'hotel_map__hotel__id',
        'hotel_map__hotel__name',
        'hotel_map__hotel__hotelogix_id',
        'room__room_type_code',
        'room__id')

    def has_add_permission(self, request):
        return False

    def get_hotel_id(self, obj):
        if obj and obj.hotel_map.hotel:
            return str(obj.hotel_map.hotel.id)
        else:
            return None

    def get_hotel_name(self, obj):
        if obj and obj.hotel_map.hotel:
            return str(obj.hotel_map.hotel.name)
        else:
            return None

    def get_hotelogix_id(self, obj):
        if obj and obj.hotel_map.hotel:
            return str(obj.hotel_map.hotel.hotelogix_id)
        else:
            return None

    def get_catalog_hotel_id(self, obj):
        if obj and obj.hotel_map:
            return str(obj.hotel_map.catalog_hotel_id)
        else:
            return None

    def get_room_type_code(self, obj):
        if obj and obj.room:
            return str(obj.room.room_type_code)
        else:
            return None

    def get_room_id(self, obj):
        if obj and obj.room:
            return str(obj.room.id)
        else:
            return None

    get_catalog_hotel_id.short_description = 'Catalog Hotel ID'
    get_hotel_id.short_description = 'Hotel ID'
    get_hotel_name.short_description = 'Hotel Name'
    get_hotelogix_id.short_description = 'Hotelogix ID'
    get_room_type_code.short_description = 'Room Type Code'
    get_room_id.short_description = 'Room ID'
