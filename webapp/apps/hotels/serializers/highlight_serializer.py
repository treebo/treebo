from rest_framework import serializers

from dbcommon.models.hotel import Highlight

__author__ = 'Rohit'


class HighlightSerializer(serializers.ModelSerializer):
    class Meta:
        model = Highlight
        fields = ('id', 'description')
