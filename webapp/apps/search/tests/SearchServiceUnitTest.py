import unittest

from mock.mock import patch, NonCallableMock

from apps.search.services.search import SearchService

__author__ = 'varunachar'


class SearchServiceUnitTest(unittest.TestCase):
    def setUp(self):
        self.search = SearchService()

    def test_shouldReturnAllHotelsForIndiaSearch(self):
        with patch('search.Hotel') as mockHotel:
            hotels = [NonCallableMock(id=1, name="1"),
                      NonCallableMock(id=2, name="2"),
                      NonCallableMock(id=3, name="3"),
                      ]
            mockHotel.objects.all = hotels
        returnHotels = self.search.search('india')
        self.assertEqual(len(returnHotels), 3, "Not all hotels retrieved")
