import logging
from data_services.soa_clients.property_client import CatalogClient, CatalogAPIResponsesProcessor

logger = logging.getLogger(__name__)

property_parser = CatalogAPIResponsesProcessor()
cs_client = CatalogClient(cs_response_processor=property_parser,
                          )
def get_active_hotels_from_cs():
    active_hotels_in_cs = cs_client.get_all_properties()
    for hotel in active_hotels_in_cs:
        get_hotel_from_cs_and_sync_with_db(hotel)

def get_hotel_from_cs_and_sync_with_db(hotel):
    try:
        cs_id = hotel.cs_id
        if cs_id:
            #Sync all details related to hotel
            cs_hotel = cs_client.get_hotel_data_from_cs(cs_property_id=cs_id)
            #store this data
            #parse this data and update db
    except Exception as e:
        logger.error(e)
