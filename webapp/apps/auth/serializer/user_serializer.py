from rest_framework import serializers
from dbcommon.models.profile import User


class UserSerializer(serializers.ModelSerializer):
    """UserRegisterSerializer"""

    class Meta:
        model = User
        exclude = (
            'is_otp_verified',
            'password',
            'middle_name',
            'user_permissions',
            'last_login',
            'is_superuser',
            'gender',
            'dob',
            'is_staff',
            'is_active',
            'uuid',
            'is_guest',
            'city',
            'auth_id',
            'created_at',
            'modified_at',
            'anniversary_date',
            'groups')
