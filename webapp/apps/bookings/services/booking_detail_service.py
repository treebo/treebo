# pylint: disable=no-value-for-parameter,simplifiable-if-expression,protected-access,too-many-locals,line-too-long
import logging
from datetime import datetime
from django.conf import settings
from treebo_commons.utils import dateutils

from apps.bookings.constants import PAY_AT_HOTEL
from apps.bookings.data_classes.guest import Adult, Child
from apps.bookings.providers.get_booking.treebo import TreeboCRSGetBooking
from apps.common.exceptions.booking_exceptions import BookingNotFoundException, RoomStaysNotFound

from apps.common.exceptions.booking_exceptions import BookedHotelNotFound
from apps.growth.services.growth_services import GrowthServices
from apps.hotels.service.hotel_service import HotelService
from data_services.soa_clients.property_client import CatalogClient, CatalogAPIResponsesProcessor

logger = logging.getLogger(__name__)


class BookingDetailService:
    def __init__(self, booking_id):
        self.booking_id = booking_id

    def get_booking_detail(self):
        try:
            booking_data_from_crs = TreeboCRSGetBooking.search_from_booking_id(
                thsc_booking_id_or_reference_id=self.booking_id)

            current_date = datetime.now().date()
            upcoming_bookings = []
            previous_bookings = []
            if booking_data_from_crs:
                growth_service = GrowthServices()
                checkin_date = dateutils.to_date(booking_data_from_crs.checkin)
                checkout_date = dateutils.to_date(booking_data_from_crs.checkout)
                checkin_time = booking_data_from_crs.checkin.hour
                checkout_time = booking_data_from_crs.checkin.hour
                total_no_of_nights = (checkout_date - checkin_date).days
                if booking_data_from_crs.room_stays:
                    room_config_dict = self.get_room_config_guest_count_dict(booking_data_from_crs.room_stays,
                                                                             booking_data_from_crs.guests)
                else:
                    logger.exception(
                        "Room stays or Guests object is not present in the booking object.{booking_dict}".format(
                            booking_dict=booking_data_from_crs))
                    raise RoomStaysNotFound()

                payment_detail_dict = self.get_payment_detail_dict(booking_data_from_crs)

                user_detail_dict = self.get_booking_owner_details(booking_data_from_crs.booking_owner)
                hotel_id = booking_data_from_crs.hotel_id
                hotel_dict = self.get_hotel_detail_dict(catalog_id=hotel_id)

                booking_dict = {
                    "status": booking_data_from_crs.status.name.upper(),
                    "checkin_date": checkin_date,
                    "checkout_date": checkout_date,
                    "checkin_time": checkin_time,
                    "checkout_time": checkout_time,
                    "total_number_of_nights": total_no_of_nights,
                    "group_code": "",
                    "booking_code": "",
                    "user_detail": user_detail_dict,
                    "hotel_detail": hotel_dict,
                    "room_config": room_config_dict,
                    "order_id": self.booking_id,
                    "hotel_name": hotel_dict.get('hotel_name'),
                    "audit": False,
                    "payment_detail": payment_detail_dict,
                    "paid_at_hotel": True if booking_data_from_crs.stay_source and booking_data_from_crs.stay_source.lower() == PAY_AT_HOTEL.lower() else False
                }
                if booking_dict.get('checkin_date') >= current_date:
                    upcoming_bookings.append(booking_dict)
                    upcoming_bookings = growth_service.get_partpay_link(upcoming_bookings)
                    upcoming_bookings = growth_service.get_cancel_url(upcoming_bookings)
                else:
                    previous_bookings.append(booking_dict)
                booking_context_dict = {
                    "upcoming": upcoming_bookings,
                    "previous": previous_bookings
                }
            else:
                logger.info("No booking found in the CRS with the {booking_id} ".format(booking_id=self.booking_id))
                raise BookingNotFoundException(developer_message="unable to find Booking")
        except Exception as e:
            logger.exception(e)
            raise e
        return booking_context_dict

    def get_room_config_guest_count_dict(self, room_stays, guests):
        room_config = {
            "room_type": "",
            "room_count": len(room_stays),
            "description": "",
            "name": "",
        }
        for per_room_stay in room_stays:
            room_type_id_from_crs = per_room_stay.room_type_id
            for room_type, room_type_id in (settings.CS_ROOM_TYPE_MAPPING).items():
                if room_type_id == room_type_id_from_crs.lower():
                    room_config.update({
                        "room_type": room_type,
                        "name": room_type,
                    })
                    break
            break

        room_config.update({
            "no_of_adults": len([guest for guest in guests if guest.age == Adult.end_age]) or 0,
            "no_of_childs": len([child for child in guests if child.age == Child.end_age]) or 0
        })

        return room_config

    def get_booking_owner_details(self, booking_owner):
        user_detail_dict = {
            "name": booking_owner.first_name or '',
            "email": booking_owner.email or '',
            "contact_number": booking_owner.phone_number or '',
            "gst_details": {
                "organisation_name": booking_owner.gst_details.legal_name if booking_owner.gst_details else '',
                "gstin": booking_owner.gst_details.gstin_num if booking_owner.gst_details else '',
                "organisation_address": booking_owner.gst_details.address.field1 if booking_owner.gst_details else ''
            }
        }
        return user_detail_dict

    def get_payment_detail_dict(self, booking_data):
        payment_detail_dict = dict(room_price=booking_data.pre_tax_amount,
                                   tax=booking_data.tax_amount,
                                   total=booking_data.total_amount,
                                   paid_amount=booking_data.paid_amount,
                                   pending_amount=booking_data.payable_amount)
        return payment_detail_dict

    def get_hotel_detail_dict(self, catalog_id):
        property_parser = CatalogAPIResponsesProcessor()
        cs_client = CatalogClient(cs_response_processor=property_parser)
        try:
            hotel_dict_from_catalog_client = cs_client.get_hotel_data_from_cs(cs_property_id=catalog_id)
            hotel_image_set_from_cs = cs_client.get_property_images(cs_property_id=catalog_id)
            hotel_image_set = [per_image.url.url for per_image in hotel_image_set_from_cs if per_image.url]
            hotel_detail_dict = {
                "hotel_id": hotel_dict_from_catalog_client.id,
                "hotel_name": HotelService().get_display_name_from_hotel_name(
                    hotel_name=hotel_dict_from_catalog_client.name),
                "city": hotel_dict_from_catalog_client.city_name,
                "address": hotel_dict_from_catalog_client.address,
                "locality": hotel_dict_from_catalog_client.locality.name,
                "state": hotel_dict_from_catalog_client.state.name,
                "images": hotel_image_set,
                "contact_number": hotel_dict_from_catalog_client.phone_number,
                "property": {
                    "type": hotel_dict_from_catalog_client.property_type,
                    "provider": hotel_dict_from_catalog_client.provider_name
                },
                "latitude": hotel_dict_from_catalog_client.latitude,
                "longitude": hotel_dict_from_catalog_client.longitude,
                "hotel_cs_id": catalog_id,
                "hotel_code": catalog_id

            }
            return hotel_detail_dict
        except Exception as e:
            logger.exception(e)
            raise BookedHotelNotFound()
