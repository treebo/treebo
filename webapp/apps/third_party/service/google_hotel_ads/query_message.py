

import logging

from lxml import etree as et
from lxml import objectify

from apps.third_party.service.google_hotel_ads import constants

logger = logging.getLogger(__name__)


class QueryMessageService(object):
    def generate_query_message(self, ):
        try:
            root = et.Element('QueryControl')
            itinerary_capabilities = et.SubElement(
                root, 'ItineraryCapabilities')
            default_value = et.SubElement(
                itinerary_capabilities, 'DefaultValue')
            max_advance_purchase = et.SubElement(
                default_value, "MaxAdvancePurchase")
            max_advance_purchase.text = constants.MAX_ADVANCE_PURCHASE
            max_length_of_stay = et.SubElement(
                default_value, "MaxLengthOfStay")
            max_length_of_stay.text = constants.MAX_LENGTH_OF_STAY
            # State = et.SubElement(DefaultValue, "State")
            # State.text = "enabled"
            # PropertyOverride = et.SubElement(ItineraryCapabilities, 'PropertyOverride')
            # MaxAdvancePurchase = et.SubElement(PropertyOverride, "MaxAdvancePurchase")
            # MaxAdvancePurchase.text = "240"
            # MaxLengthOfStay = et.SubElement(PropertyOverride, "MaxLengthOfStay")
            # MaxLengthOfStay.text = "18"
            # State = et.SubElement(PropertyOverride, "State")
            # State.text = "enabled"
            # Property = et.SubElement(PropertyOverride, "Property")
            # Property.text = "some hotel id"
            objectify.deannotate(root, xsi_nil=True, cleanup_namespaces=True)
            tree = et.ElementTree(root)
            return tree
        except Exception as e:
            logger.exception("Exception in generating GHA query message file")
            raise e
