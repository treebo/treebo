import json
import logging

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR

from apps.discounts.services.create_coupon_service import CreateCouponService
from apps.discounts.tasks import create_coupons
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class CreateCouponAPI(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CreateCouponAPI, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request):
        try:
            if not (hasattr(request, 'user')
                    and request.user and request.user.is_authenticated()):
                context = {"status": "error", "msg": "User login required"}
                return Response(context, status=HTTP_400_BAD_REQUEST)
            else:
                coupon = json.loads(request.GET['coupon_data'])
                constraints_data = json.loads(request.GET['constraint_data'])
                apply_constraint = json.loads(request.GET['constraints'])
                user_email = str(request.user.email)
                create_coupons.apply_async(
                    args=[
                        coupon,
                        constraints_data,
                        apply_constraint,
                        user_email])
                context = {
                    "status": "success",
                    "msg": "We will send coupons to your email id shortly."}
                return Response(context, status=HTTP_200_OK)

        except Exception as exc:
            logger.exception(exc)
            context = {"status": "error", "msg": "Unbale to generate coupons"}
            return Response(context, status=HTTP_500_INTERNAL_SERVER_ERROR)

    @log_args(logger)
    def post(self, request, format=None):
        raise Exception('Method not supported')
