# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookingstash', '0006_auto_20160523_0950'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='availability',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Availability',
                'verbose_name_plural': 'Availabilities'},
        ),
        migrations.AlterModelOptions(
            name='hotelroommapper',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Hotel Room Mapper',
                'verbose_name_plural': 'Hotel Room Mappers'},
        ),
        migrations.AlterModelOptions(
            name='reservationbooking',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Reservation Booking',
                'verbose_name_plural': 'Reservation Bookings'},
        ),
        migrations.AlterModelOptions(
            name='reservationbookingmessage',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read')},
        ),
        migrations.AlterModelOptions(
            name='reservationbookingmessagestay',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read')},
        ),
        migrations.AlterModelOptions(
            name='reservationbookingstay',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Reservation Booking Stay',
                'verbose_name_plural': 'Reservation Booking Stays'},
        ),
        migrations.AlterModelOptions(
            name='reservationrestriction',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read')},
        ),
        migrations.AlterModelOptions(
            name='reservationrestrictionmessage',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read')},
        ),
    ]
