from apps.reviews.cache.builders.hd_cache_builder import CacheDataBuilderHD
from apps.reviews.cache.builders.search_cache_builder import CacheDataBuilderSearch
from apps.reviews.cache.page_config import PageConfig


class CacheBuilderFactory(object):

    def get_builder(self, hotel_id, page):
        builder_obj = None
        if page == PageConfig.SEARCH_PAGE:
            builder_obj = CacheDataBuilderSearch()
        elif page == PageConfig.HD_PAGE:
            builder_obj = CacheDataBuilderHD()
        if builder_obj:
            builder_obj.set_key(hotel_id, page)
        return builder_obj

    def get_builders(self, hotel_ids, page):
        return {hotel_id: self.get_builder(hotel_id, page) for hotel_id in hotel_ids}
