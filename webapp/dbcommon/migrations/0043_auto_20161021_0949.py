# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0042_seodetails_hotel_canonical_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='seodetails',
            name='titleDescription',
            field=models.CharField(
                max_length=200,
                db_column='title_description'),
        ),
    ]
