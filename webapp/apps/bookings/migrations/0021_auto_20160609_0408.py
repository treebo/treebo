# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0020_auto_20160523_0950'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking', name='status', field=models.IntegerField(
                default=1, choices=[
                    (1, 'Confirm'), (0, 'Cancel'), (-1, 'Cancellation Pending')]), ), ]
