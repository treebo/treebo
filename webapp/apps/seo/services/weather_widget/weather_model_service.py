from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import City, Locality
from dbcommon.models.landmark import SearchLandmark
from data_services.search_landmark_repository import SearchLandMarkRepository
import logging

logger = logging.getLogger(__name__)


class WeatherModelService():

    search_landmark_repository = RepositoriesFactory.get_landmark_repository()

    def fetch_city_from_city_id(self, city):
        city_data_service = RepositoriesFactory.get_city_repository()
        city_objs = city_data_service.filter_cities(id=city)
        if city_objs:
            city_obj = city_objs[0]
        else:
            city_obj = None
        #city_obj = City.objects.filter(id = city).first()
        if city_obj is not None:
            logger.info(
                'City corresponding to City id : %s successfully fetched',
                city)
        else:
            logger.info('City corresponding to City id : %s not found', city)
        return city_obj

    def fetch_locality_from_locality_id(self, locality):
        locality_obj = Locality.objects.filter(id=locality).first()
        if locality_obj is not None:
            logger.info(
                'locality corresponding to locality id : %s successfully fetched',
                locality)
        else:
            logger.info(
                'locality corresponding to locality id : %s not found',
                locality)
        return locality_obj

    def fetch_landmark_from_landmark_id(self, landmark):
        landmark_objs = self.search_landmark_repository.get_landmark_objects_with_filter(
            id=landmark)
        if landmark_objs:
            landmark_obj = landmark_objs[0]
        else:
            landmark_obj = None
        # landmark_obj = SearchLandmark.objects.filter(id=landmark).first()
        if landmark_obj is not None:
            logger.info(
                'landmark corresponding to landmark id : %s successfully fetched',
                landmark)
        else:
            logger.info(
                'landmark corresponding to landmark id : %s not found',
                landmark)
        return landmark_obj
