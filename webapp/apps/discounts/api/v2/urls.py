# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.discounts.api.v2.available_coupons import AvailableCouponAPI

app_name = 'discount_v2'
urlpatterns = [url(r"^featured/$",
                   AvailableCouponAPI.as_view(),
                   name='available-discounts'),
               ]
