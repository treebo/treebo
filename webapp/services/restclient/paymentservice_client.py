from django.conf import settings
import logging
import requests
import json
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import AuthException, PaymentOrderException, PaymentValidationException
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class PaymentServiceClient():
    def generate_order(self, amount, notes):
        headers = {
            'content-type': "application/json",
        }
        data = {
            "amount": amount,
            "currency": "INR",
            "notes": notes
        }
        import json
        response = self.__call_payment_service(
            'post',
            settings.PAYMENT_SERVICE_URL_ENDPOINTS['order'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        message = api_response.fetch_message_from_response(response.json())
        logger.error(
            " generate order  error %s with message %s ",
            response.text,
            message)
        slack_alert.publish_alert(
            'web alert PaymentServiceClient %s : %s ' %
            ('generate_order', str(message)), channel=slack_alert.AUTH_ALERTS)
        if message:
            raise PaymentOrderException(
                'Unable To generate order.',
                message,
                response.status_code)
        raise PaymentOrderException(
            'Unable To generate order.', '', response.status_code)

    def validate_payment(
            self,
            pg_payment_id,
            pg_payment_signature,
            pg_order_id,
            ps_order_id):
        headers = {
            'content-type': "application/json",
        }
        data = {
            "pg_payment_id": pg_payment_id,
            "pg_payment_signature": pg_payment_signature,
            "pg_order_id": pg_order_id,
            "order_id": ps_order_id,
            "ps_order_id": ps_order_id,
        }
        response = self.__call_payment_service(
            'post',
            settings.PAYMENT_SERVICE_URL_ENDPOINTS['payment'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            data = response.json()['data']
            status = data['status']
            if status == 'Failed':
                logger.error(
                    " payment validation failed because status is failed")
                raise PaymentValidationException('Unable To validate payment.')
            return response.json()['data']
        message = api_response.fetch_message_from_response(response.json())
        logger.error(
            " generate validate payment with error %s with message %s ",
            response.text,
            message)
        slack_alert.publish_alert(
            'web alert PaymentServiceClient %s : %s ' %
            ('validate_payment', str(message)), channel=slack_alert.AUTH_ALERTS)
        if message:
            raise PaymentValidationException(message, response.status_code)
        raise PaymentValidationException(
            'Unable To validate payment.',
            response.status_code)

    def __call_payment_service(
            self,
            method,
            end_url,
            headers={},
            params={},
            data={}):
        host = settings.PAYMENT_SERVICE_HOST
        url = host + end_url
        logger.info(
            "calling paymentservice %s ,pa %s, %s ,h: %s ",
            url,
            params,
            data,
            headers)
        response = requests.request(method, url, headers=headers, data=data)
        logger.info(
            "paymentservice response  %s , test : %s ",
            response.status_code,
            response.text)
        return response
