'use strict';

import BaseView from './treebolibs/baseview';
import cookies from 'js-cookie';
import '../css/confirmation.less';

let ConfirmationView = Object.create(BaseView);
ConfirmationView.pageLoaded = () => {
    ConfirmationView.addToAnalytics();
};

ConfirmationView.addToAnalytics = () => {
    if (!rt.clientAnalyticsObj) {
        setTimeout(ConfirmationView.addToAnalytics, 1000);
        return;
    }
    rt.clientAnalyticsObj.increment('num_bookings');
    rt.clientAnalyticsObj.trackCharge(rt.totalAmount);
    rt.clientAnalyticsObj.setPeopleProperty(rt.lastPurchaseDate);
    rt.clientAnalyticsObj.union('visited_cities',rt.city);
    rt.clientAnalyticsObj.union('hotels_booked',rt.hotelId);
    rt.clientAnalyticsObj.increment('LTV',rt.totalAmount);
    rt.clientAnalyticsObj.alias(rt.userId);
    rt.clientAnalyticsObj.identifySession(rt.userId);
    ConfirmationView.setOrderIdCookie();
};

ConfirmationView.setOrderIdCookie = () => {
  cookies.set('order_id', rt.confirm_order_analytics.orderId, {expires: 365});
};

let analyticsConfigfunc = function(){
   return  {
       booking_code: rt.booking_code,
       hrental_id: rt.hotelId,
       hrental_pagetype: 'conversion',
       hrental_startdate: rt.checkInDate,
       hrental_enddate: rt.checkOutDate,
       hrental_totalvalue: rt.totalAmount,
   }
}

let confirmationConfig = { name: 'Confirmation Page', widgets: [], analyticsConfigfunc };
ConfirmationView.setup(confirmationConfig);
