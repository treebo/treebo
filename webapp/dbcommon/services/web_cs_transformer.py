import json

from dbcommon.models.hotel import Hotel
from django.core.cache import cache

import logging

logger = logging.getLogger(__name__)


class WebCsTransformer:
    @staticmethod
    def get_hotel_cs_ids_from_web_ids(hotel_web_ids):
        live = {}
        churned = {}
        hotel_web_ids_to_cs_ids = {
            'live': live,
            'churned': churned
        }

        try:
            hotel_objects = Hotel.all_hotel_objects.prefetch_related().filter(id__in=hotel_web_ids).all()

            for hotel in hotel_objects:
                if hotel.status == Hotel.ENABLED:
                    live[int(hotel.id)] = hotel.cs_id
                else:
                    churned[int(hotel.id)] = hotel.cs_id

            logger.info("Hotel web ids to cs ids is {0}".format(str(hotel_web_ids_to_cs_ids)))
        except Exception as err:
            logger.exception("Couldn't find cs_ id for %s due to %s", str(hotel_web_ids), str(err))

        return hotel_web_ids_to_cs_ids

    @staticmethod
    def get_hotel_web_ids_from_cs_ids(hotel_cs_ids):
        param = 'cs_id_'
        live = {}
        churned = {}
        onelink_urls = {}
        hotel_cs_ids_to_web_ids = {
            'live': live,
            'churned': churned,
            'onelink_urls': onelink_urls
        }

        try:

            for cs_id in hotel_cs_ids:
                cache_value = cache.get(param + cs_id)
                if cache_value:
                    if cache_value.get('status') == Hotel.ENABLED:
                        live[cs_id] = int(cache_value.get('web_id'))
                        cached_onelink_urls = cache.get('onelink_' + cs_id, {})
                        web_id, url = cached_onelink_urls.get('web_id'), cached_onelink_urls.get('onelink_urls')
                        if web_id and url:
                            onelink_urls[web_id] = url
                    else:
                        churned[cs_id] = int(cache_value.get('web_id'))
                else:
                    hotel_object = Hotel.all_hotel_objects.prefetch_related().filter(cs_id=cs_id).first()
                    if hotel_object:
                        if hotel_object.status == Hotel.ENABLED:
                            onelink_urls[hotel_object.id] = hotel_object.onelink_urls
                            cache.set('onelink_' + cs_id, {'web_id': hotel_object.id, 'onelink_urls': hotel_object.onelink_urls}, 24 * 60 * 7)
                            live[cs_id] = int(hotel_object.id)
                            cache.set(param + cs_id, {'status': Hotel.ENABLED, 'web_id': hotel_object.id}, 24 * 60 * 7)
                        else:
                            churned[cs_id] = int(hotel_object.id)
                            cache.set(param + cs_id, {'status': Hotel.DISABLED, 'web_id': hotel_object.id}, 24 * 60 * 7)


            logger.info("Hotel cs ids to web ids is {0}".format(str(hotel_cs_ids_to_web_ids)))
        except Exception as err:
            logger.exception("Couldn't find hotel_ids for cs_ids %s due to %s", hotel_cs_ids, str(err))

        return hotel_cs_ids_to_web_ids
