# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0008_auto_20160215_0853'),
    ]

    state_operations = [
        migrations.CreateModel(
            name='ConstraintExpression',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('expression', models.CharField(default='', max_length=2000, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConstraintKeywords',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('keyword', models.CharField(default='', max_length=200, null=True)),
                ('type', models.CharField(default='', max_length=20, null=True)),
                ('description', models.CharField(default='', max_length=1000, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConstraintOperators',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('operator', models.CharField(default='', max_length=10, null=True)),
                ('description', models.CharField(default='', max_length=100, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DiscountCoupon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('code', models.CharField(default='', unique=True, max_length=200)),
                ('discount_operation', models.CharField(default='PERCENTAGE', max_length=20,
                                                        choices=[('FIXED', 'FIXED'),
                                                                 ('PERCENTAGE', 'PERCENTAGE'),
                                                                 ('NIGHT', 'NIGHT'), (
                                                                     'FLATPERNIGHT',
                                                                     'FLATPERNIGHT')])),
                ('discount_value', models.DecimalField(default=0, max_digits=5, decimal_places=1)),
                ('max_available_usages', models.IntegerField(default=0)),
                ('max_available_discount', models.PositiveIntegerField(default=0)),
                ('expiry', models.DateField(null=True, blank=True)),
                ('is_active', models.BooleanField(default=False)),
                ('tagline', models.CharField(default='', max_length=200, null=True)),
                ('terms', models.CharField(default='', max_length=5000, null=True)),
                ('discount_application',
                 models.IntegerField(default=1, choices=[(0, 'Pretax'), (1, 'Posttax')])),
                ('coupon_type', models.CharField(default='DISCOUNT', max_length=200,
                                                 choices=[('DISCOUNT', 'DISCOUNT'),
                                                          ('CASHBACK', 'CashBack'),
                                                          ('ROOMNIGHTS', 'RoomNights'),
                                                          ('VOUCHER', 'Voucher')])),
                ('coupon_message', models.CharField(default='Discount value', max_length=500)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DiscountCouponConstraints',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('type', models.CharField(max_length=200, null=True, blank=True)),
                ('value_one', models.CharField(max_length=200, null=True, blank=True)),
                ('value_two', models.CharField(max_length=200, null=True, blank=True)),
                (
                    'coupon',
                    models.ForeignKey(default='', to='discounts.DiscountCoupon', null=True, on_delete=models.DO_NOTHING)),
                ('keyword',
                 models.ForeignKey(default='', to='discounts.ConstraintKeywords', null=True, on_delete=models.DO_NOTHING)),
                ('operation',
                 models.ForeignKey(default='', to='discounts.ConstraintOperators', null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='constraintexpression',
            name='coupon',
            field=models.ForeignKey(blank=True, to='discounts.DiscountCoupon', null=True, on_delete=models.DO_NOTHING),
        ),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(state_operations=state_operations)
    ]
