import csv
import os

from django.apps import apps
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    args = 'Argument is needed as table name'
    help = 'Django custom management command for exporting data as tables. '

    def add_arguments(self, parser):
        parser.add_argument('my_argument', type=str)

    def handle(self, *args, **options):
        if not options['my_argument'] == '':
            table_name = options['my_argument']
            self.stdout.write("Exporting table...")
            self.csv_exporter(table_name)
            self.stdout.write("Table exporting is complete")
        else:
            self.stdout.write("Please enter atleast one valid argument")
            self.stdout.write(
                "Arguments is needed. Available are the table names to be exported")
            self.stdout.write(
                "For example: " +
                '\033[1m' +
                "python manage.py csvuploader fot" +
                '\033[0m')

    def csv_exporter(self, table_name):
        try:
            os.makedirs('csv')
        except OSError:
            if not os.path.isdir('csv'):
                raise
        outfile_path = 'csv/' + table_name + '.csv'
        table_name = apps.get_model(app_label='fot', model_name=table_name)
        queryset = table_name.objects.all()
        model = queryset.model
        with open(outfile_path, 'wb') as csvfile:
            writer = csv.writer(csvfile)
            headers = []
            for field in model._meta.fields:
                headers.append(field.name)
            writer.writerow(headers)
            for obj in queryset:
                writer.writerow([getattr(obj, field) for field in headers])
