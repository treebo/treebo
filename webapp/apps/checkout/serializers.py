from rest_framework import serializers

from apps.checkout.models import BookingRequest
from datetime import date, datetime


def json_dateserialiser(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        return data


class BookingRequestSerializer(serializers.ModelSerializer):
    room_type = serializers.CharField(max_length=50, required=False)
    hotel_id = serializers.CharField(max_length=20)
    checkin_date = serializers.DateField()
    checkout_date = serializers.DateField()
    room_config = serializers.CharField()
    coupon_code = serializers.CharField(required=False, allow_blank=True)
    booking_channel = serializers.CharField(
        required=False, default=BookingRequest.WEBSITE)
    rate_plan_meta = JSONSerializerField(required=False)
    wallet_applied = serializers.BooleanField(default=False, required=False)
    wallet_deduction = serializers.DecimalField(
        max_digits=9, decimal_places=2, default=0.0, required=False)
    user_login_state = serializers.CharField(
        max_length=30, allow_null=True, allow_blank=True)
    is_flash_sale_booking = serializers.BooleanField(default=False)
    is_flat_price_booking = serializers.BooleanField(default=False)
    flash_sale_discount = serializers.IntegerField(required=False, allow_null=True)
    utm_tracking_id = serializers.CharField(max_length=200, required=False, allow_null=True)

    class Meta:
        model = BookingRequest
        fields = '__all__'


class PayAtHotelRequestSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=254, required=True)
    mobile = serializers.IntegerField(required=True)
    bid = serializers.IntegerField(required=True)
    email = serializers.EmailField(required=True, allow_blank=True)
    message = serializers.CharField(required=True, allow_blank=True)
    recaptcha_user_token = serializers.CharField(required=False, allow_blank=True)

    def validate(self, attrs):
        if attrs['name'].isdigit():
            raise serializers.ValidationError('Name should contain at least 1 character')
        return attrs
