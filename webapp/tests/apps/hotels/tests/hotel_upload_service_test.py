import os
import unittest
import zipfile


class HotelUploadServiceTest(unittest.TestCase):
    def test_directory_walking(self):
        folder_name = "/Users/varunachar/Work/data/Treebo Park Classic/FOR_WEB_ONLY"
        for root, dirs, files in os.walk(folder_name):
            for name in files:
                if name.endswith(".jpg"):
                    print(("image path " + root.replace(folder_name, ".") + "/" + name))
                    room = root[len(folder_name) + 1:]
                    if len(room) > 0:
                        print(("room is: " + room))

    def test_unzip_images(self):
        zip_file = '/Users/varunachar/Downloads/FOR_WEB_ONLY 2.zip'
        file = open(zip_file, mode='r')
        folder_name = "/Users/varunachar/Work/data/Treebo Classic"
        zip = zipfile.ZipFile(file, 'r')
        zip.extractall(folder_name)
        print(
            ((folder_name + "/" + file.name.rsplit("/", 1)[1]).replace(".zip", "")))
