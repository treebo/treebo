import logging

from dbcommon.models.room import Room
from dbcommon.models.room import ExternalProviderRoomDetails


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


class RoomDto(object):
    def __init__(self, kwargs):
        try:
            acceptable_keys_list = [
                field.name for field in Room._meta.get_fields()]
            for k, v in list(kwargs.items()):
                if k in acceptable_keys_list:
                    self.__setattr__(k, v)
        except Exception as e:
            logger.error(e)


class ExternalProviderRoomDetailsDto(object):
    def __init__(self, kwargs):
        try:
            acceptable_keys_list = [
                field.name for field in ExternalProviderRoomDetails._meta.get_fields()]
            for key, value in list(kwargs.items()):
                if key in acceptable_keys_list:
                    self.__setattr__(key, value)
        except Exception as e:
            logger.error(str(e))

