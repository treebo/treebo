# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('pricing', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RoomRatePlan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('is_active', models.BooleanField(default=False)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('base_price', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('price_increment',
                 models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('hotel', models.ForeignKey(to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
                ('room', models.ForeignKey(to='dbcommon.Room', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'hotels_roomrateplan',
            },
        ),
    ]
