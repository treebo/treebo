import django
from django.conf import settings
from django.core.management import BaseCommand
from apps.third_party.tasks import trivago_save_hotel_data_in_csv

django.setup()


class Command(BaseCommand):
    help = 'Trivago Csv File update command'
    pass

    def handle(self, *args, **options):
        self.update_trivago_csv_file()

    def update_trivago_csv_file(self):
        url_prefix = settings.EXTERNAL_WEB_URL
        trivago_save_hotel_data_in_csv(url_prefix)

