from django.http.response import HttpResponseRedirect

from base.views.template import TreeboTemplateView

__author__ = 'varunachar'


class ForgotPassword(TreeboTemplateView):
    template_name = "forgot_password/index.html"

    def getMobileData(self, request, args, kwargs):
        if request.user and request.user.is_active and request.user.is_authenticated():
            return HttpResponseRedirect('/')
        return {
            'scripts': ['mobile/js/forgotpassword.js'],
            "styles": ['mobile/css/login.css'],
            'footer': False}
