from base import log_args
from data_services.decorators import SingletonDecorator


@SingletonDecorator
class RoomRepository(object):

    def __init__(self, room_web_repository, cs_api_client, is_cs_api_querying_enabled=False):
        self.room_web_repository = room_web_repository
        self.cs_api_client = cs_api_client
        self.is_cs_api_querying_enabled = is_cs_api_querying_enabled

    @log_args()
    def get_all_rooms(self):
        return self.room_web_repository.get_all_rooms()

    @log_args()
    def get_single_Room(self, *args, **kwargs):
        return self.room_web_repository.get_single_Room(*args, **kwargs)

    def get_room_configs_from_cs(self, hotel_id):
        try:
            if self.is_cs_api_querying_enabled:
                return self.cs_api_client.get_property_room_configs_from_web_hotel_id(hotel_id)
            return None
        except Exception as e:
            return None

    def get_rooms_for_hotel(self, hotel_id):
        # room_configs = self.get_room_configs_from_cs(hotel_id)
        # if not room_configs:
        return self.room_web_repository.get_rooms_for_hotel(hotel_id)


    def get_room_types_for_hotel(self, hotel_id):
        return self.room_web_repository.get_room_types_for_hotel(hotel_id)

    @log_args()
    def filter_rooms(self, *args, **kwargs):
        return self.room_web_repository.filter_rooms(*args, **kwargs)

    @log_args()
    def filter_room_by_string_matching(self, fields_list, filter_params):
        return self.room_web_repository.filter_room_by_string_matching(
            fields_list, filter_params)

    @log_args()
    def get_room_with_related_entities(
            self,
            select_list,
            prefetch_list,
            filter_params):
        return self.room_web_repository.get_room_with_related_entities(
            select_list, prefetch_list, filter_params)

    def get_facility_set_for_room(self, room):
        if hasattr(room, 'facility_set'):
            try:
                return room.facility_set.all()
            except Exception as e:
                return room.facility_set

    def get_occupancy_string_for_room(self, room):
        try:
            return room.get_occupancy_string()
        except Exception as e:
            if hasattr(room, 'occupancy_string'):
                return room.occupancy_string

    def get_mm_hotel_code(self, room):
        try:
            return room.get_mm_hotel_code()
        except Exception as e:
            if hasattr(room, 'mm_hotel_code'):
                return room.mm_hotel_code

    def get_mm_room_code(self, room):
        try:
            return room.get_mm_room_code()
        except Exception as e:
            if hasattr(room, 'mm_room_code'):
                return room.mm_room_code

    def get_roomtypes_in_hotel(self, hotels):
        try:
            return self.room_web_repository.get_roomtypes_in_hotel(hotels)
        except Exception as e:
            return dict()


class RoomDoesNotExist(Exception):
    pass
