import json
import logging

from django.conf import settings
from django.shortcuts import redirect

from apps.referral import tasks as referral_tasks
from apps.referral.service.service import ReferralService
from base.views.template import TreeboTemplateView
from dbcommon.models.profile import UserReferralReward
from services.restclient.contentservicerestclient import ContentServiceClient

logger = logging.getLogger(__name__)


class Referral(TreeboTemplateView):
    template_name = "referral/index.html"

    @staticmethod
    def __is_logged_in_user(request):
        if request.user.is_authenticated():
            user = request.user
            referral_tasks.generate_referral_code.delay(user.id)
            return True
        return False

    @staticmethod
    def __extract_referral_link(request):
        avclick = request.GET.get(
            settings.APPVIRALITY_SHARE_LINK_QUERY_PARAM, None)
        return avclick

    @staticmethod
    def __get_referral_details(request):
        avclick = Referral.__extract_referral_link(request)
        if avclick:
            referrer_detail = ReferralService.get_referrer_detail(avclick)
            return referrer_detail

    @staticmethod
    def __get_campaign_details(campaign_id, friend_name):
        campaign_details_response = ReferralService.get_campaign_details(
            campaign_id)
        campaigns = campaign_details_response["AppCampaignsDetails"]
        campaign_text = """You've received a %(reward)s off coupon code from %(friend_name)s for your first stay. Signup and get your voucher."""
        for campaign in campaigns:
            if campaign_id == campaign["CampaignID"]:
                for rule in campaign["RewardRules"]:
                    if str(rule["RewardUserType"]) == "Friend" and str(
                            rule["EventName"]) == UserReferralReward.ReferralEvents.Signup:
                        reward = rule["Reward"] + " " + \
                            rule["RewardUnit"].replace("PERCENT", "%")
                        campaign_text = campaign_text % {
                            "friend_name": friend_name, "reward": reward}
                        return campaign_text

        return campaign_text % {
            "reward": "free credits",
            "friend_name": "your friend"
        }

    @staticmethod
    def __update_context_with_referral_details(context, request):
        referrer_details = Referral.__get_referral_details(request)
        if referrer_details:
            # Not showing Campaign Text on signup page now
            # campaign_id = ContentServiceClient.getValueForKey(request, "referral_campaign_id", "1")
            # campaign_text = Referral.__get_campaign_details(campaign_id["text"],
            #                                                 referrer_details["name"])
            context.update({
                "referrer_details": json.dumps({
                    "name": referrer_details["name"],
                    "referral_code": referrer_details["shortcode"]
                    # "campaign_text": campaign_text
                })

            })

    def getData(self, request, args, kwargs):
        is_logged_in = self.__is_logged_in_user(request)
        if is_logged_in:
            return redirect('pages:index')
        context = {}
        self.__update_context_with_referral_details(context, request)
        context.update({
            'styles': ['desktop/css/referral.css'],
            'scripts': ['desktop/js/referral.js']
        })
        return context

    def getMobileData(self, request, args, kwargs):
        is_logged_in = self.__is_logged_in_user(request)
        if is_logged_in:
            return redirect('pages:index')
        context = {}
        self.__update_context_with_referral_details(context, request)
        context.update({
            'styles': ['mobile/css/referral.css'],
            'scripts': ['mobile/js/referral.js'],
            'footer': 'hide',
            'googleLogin': True
        })
        return context
