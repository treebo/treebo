from django.conf.urls import url
from apps.pricing.api.v5.cheapest_price_for_all_hotels import CheapestPriceAPIView
from apps.pricing.api.v5.hotel_prices import HotelRoomPrices

app_name = 'pricing_v5'
urlpatterns = [url(r'^hotels/$',
                   CheapestPriceAPIView.as_view(),
                   name='cheapest_prices_for_all_hotels_v5'),
               url(r"^hotels/(?P<hotel_id>[0-9]+)/room-prices/$",
                   HotelRoomPrices.as_view(),
                   name='room-prices_v5'),
               ]
