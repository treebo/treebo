import json
import logging
from operator import attrgetter

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from base import log_args
from base.views.api import TreeboAPIView
from dbcommon.models.hotel import Hotel
from data_services.respositories_factory import RepositoriesFactory

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)


class GetAllHotels(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetAllHotels, self).dispatch(*args, **kwargs)

    def post(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def get(self, request):
        """
        Get all hotels
        :param request:
        :return:
        """
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotels = list(hotel_repository.get_active_hotel_list())
        hotels = sorted(hotels, key=lambda hotel: hotel.city.id)
        result_list = [{'id': hotel.hotelogix_id, 'name': hotel.name}
                       for hotel in hotels]
        return Response(
            json.loads(
                json.dumps(result_list)),
            content_type="application/json")
