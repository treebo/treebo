import json

import psycopg2

host_name = 'web-s-sharedapps-rds-sg.cadmhukcksta.ap-southeast-1.rds.amazonaws.com'
db_port = '5432'
db_name = 'web_db'
db_user = 'web_user'
db_pass = 'qW4rTwyger685r#'

myConnection = psycopg2.connect(host=host_name, user=db_user, dbname=db_name, port=db_port, password=db_pass)
cur = myConnection.cursor()
cur.execute(
    """Select id, ps_order_id, booking_order_id, ps_payment_id, gateway_type, status, created_at from bookings_paymentorder where status = 'CAPTURED' and created_at::date > '2019-05-10' and created_at::date < '2019-05-22';""")
result = cur.fetchall()
result_dict = [{"id": row[0], "ps_order_id": row[1], "booking_order_id": row[2], "ps_payment_id": row[3],
                "gateway_type": row[4]} for row in result]
error_dir = "/tmp/error_payments.csv"
download_dir = "/tmp/payment_dump.csv"
success_csv = open(download_dir, "w")
error_csv = open(error_dir, "w")
success_csv.write(
    'gateway_type' + ',' + 'booking_id' + ',' + 'ps_payment_id' + ',' + 'ps_order_id' + ',' + 'bill_id' + "\n")
failed_booking = []
for index, res in enumerate(result_dict):
    id = res['id']
    result = None
    booking_order_id = res['booking_order_id']
    if booking_order_id:
        try:
            query = """Select id,external_crs_booking_extra_info from bookings_booking where order_id = '""" + str(
                booking_order_id) + "';"
            cur.execute(query)
            result = cur.fetchall()
            crs_booking_bill_id = json.loads(result[0][1]).get('crs_booking_bill_id')
            ps_order_id = res['ps_order_id']
            ps_payment_id = res['ps_payment_id']
            gateway_type = res['gateway_type']
            if not gateway_type:
                error_csv.write(str(id) + '\n')
            if gateway_type == 'treebo_wallet' and booking_order_id and ps_payment_id:
                success_csv.write(
                    gateway_type + "," + booking_order_id + "," + ps_payment_id + "," + ps_order_id + "," + crs_booking_bill_id + "\n")
            elif gateway_type != 'treebo_wallet' and booking_order_id and ps_payment_id:
                success_csv.write(
                    gateway_type + "," + booking_order_id + "," + ps_payment_id + "," + ps_order_id + "," + crs_booking_bill_id + "\n")
            else:
                error_csv.write(str(id) + '\n')
        except Exception as e:
            print(booking_order_id)
            failed_booking.append(booking_order_id)
            error_csv.write(str(id) + "," + booking_order_id + '\n')
