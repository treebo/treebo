import geopy
import geopy.distance

from apps.seo.components.base_component import BaseComponent
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.serializers.serializer import CitiesNearbySerializer
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.location import City
from data_services.city_repository import CityRepository

__author__ = 'ansilkareem'

city_data_service = RepositoriesFactory.get_city_repository()


class CitiesNearCity(BaseComponent):

    def __init__(self, current_city, footer_data):
        self.near_by_cities = dict()
        self.sorted_nearby_cities = []
        super(CitiesNearCity, self).__init__(current_city, footer_data, [])

    def build(self):
        if self.current_city and self.current_city.enable_for_seo:
            all_cities = [city for city in city_data_service.filter_cities(
                enable_for_seo=True, status=1) if city.id != self.current_city.id]
            current_city_point = geopy.Point(
                self.current_city.city_latitude,
                self.current_city.city_longitude)
            for city in all_cities:
                pt2 = geopy.Point(city.city_latitude, city.city_longitude)
                distance = geopy.distance.distance(current_city_point, pt2).km
                if distance <= self.distance_cap:
                    # adding to near_by cities only if the distance cap matches
                    self.near_by_cities[str(city.name)] = (distance, city)
            # Now sorting the dict of near_by_cities
            self.sorted_nearby_cities = [
                seq[1][1] for seq in sorted(
                    list(
                        self.near_by_cities.items()),
                    key=lambda x: x[1][0])]
            data = {
                'key': 'city',
                'title': 'CITIES NEAR %s' % self.current_city.slug,
                'content': AutoCompleteResultBuilder.build_city_result(
                    self.sorted_nearby_cities[:self.count_cap]
                )
            } if self.sorted_nearby_cities else {'key': 'city',
                                                 'title': 'CITIES NEAR %s' % self.current_city.slug,
                                                 'content': []
                                                 }

            serialized_obj = CitiesNearbySerializer(data=data)
            if serialized_obj.is_valid():
                return serialized_obj.validated_data

        return {
            'key': 'city',
            'title': 'CITIES CITIES NEAR %s' % self.current_city.slug,
            'content': []}
