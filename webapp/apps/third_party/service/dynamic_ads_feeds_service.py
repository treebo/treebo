import csv
import datetime
import json
import logging
import os

from boto.s3.connection import S3Connection
from boto.s3.key import Key

import requests
from django.conf import settings
from django.core.cache import cache
from django.core.files import File
from django.core.mail import send_mail, EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify

import common.constants.common_constants as const
from apps.content.models import ContentStore
from apps.third_party.service.dynamic_ads_cache_set import DynamicAdsCacheSetService
from conf.base import TRIPADVISOR_STAR_RATING_URL
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)

__author__ = "Niket Gaurav"

PRICING_URL = 'https://www.treebo.com/api/web/v6/pricing/hotels/'


class DynamicAdsService(object):
    hotel_ids_with_priority = set()

    @staticmethod
    def get_hotels_query_set():
        hotel_obj_query_set = Hotel.objects.filter(
            status = Hotel.ENABLED).order_by('id').all()
        return hotel_obj_query_set

    @staticmethod
    def get_hotel_overall_star_rating(hotel_id):
        rating_value_dict = {
            "star_rating":   3,
            "total_ratings": 1
        }
        key = "TA_RATING_" + str(hotel_id)
        try:
            rating_value_dict = cache.get(key)
            if not rating_value_dict:
                response = requests.get(TRIPADVISOR_STAR_RATING_URL + str(hotel_id))
                logger.info("Trip Advisor request %s", TRIPADVISOR_STAR_RATING_URL + str(hotel_id))
                result = json.loads(response._content)
                logger.info("Trip Advisor Hote Star Rating %s", result)
                if result["data"]["ta_reviews"]["ta_enabled"] == True:
                    rating_value_dict["star_rating"] = float(str(result["data"]["ta_reviews"]["overall_rating"]["rating"]))
                    rating_value_dict["total_ratings"] = int(float(str(result["data"]["ta_reviews"]["num_reviews"])))
        except Exception as e:
            logger.exception('Setting default values')

        return [rating_value_dict["star_rating"], rating_value_dict["total_ratings"]]

    @staticmethod
    def get_new_hotel_url(hotel):
        try:
            kwargs = {'city_slug': slugify(hotel.city.name),
                      'hotel_slug': slugify(hotel.name),
                      'locality_slug': slugify(hotel.locality.name),
                      'hotel_id': hotel.id}
            new_hotel_url = reverse('pages:hotel-details-new', kwargs=kwargs)
            new_hotel_url = 'https:' + settings.TREEBO_BASE_URL + new_hotel_url
        except BaseException:
            logger.exception('Building hotel url failed - Trivago')
            new_hotel_url = ''
        return new_hotel_url

    @staticmethod
    def get_image_url(hotel):
        showcase_image = hotel.image_set.first()
        if showcase_image:
            logger.info("Image url got as %s", showcase_image.url.name)
            return (showcase_image.url.name + str("?w=300&h=300"))
        else:
            raise Exception

    @staticmethod
    def get_hotel_description(hotel):
        try:
            description = hotel.seodetails_set.first().metaContent
        except BaseException:
            description = hotel.description
        return description

    @staticmethod
    def get_hotel_keywords(hotel):
        keywords = ''
        keywords += 'hotels in ' + slugify(hotel.city.name) + ';'
        keywords += slugify(hotel.name) + ';'
        keywords += 'hotels in' + slugify(hotel.locality.name) + ';'
        keywords += DynamicAdsService.DYNAMIC_ADS_DEFAULT_KEYWORD_STRING
        return keywords

    @staticmethod
    def fetch_room_prices(hotel_id):
        try:
            today = datetime.date.today()
            data = {
                'hotel_id':   hotel_id,
                'channel':    'website',
                'checkin':    today + datetime.timedelta(days = 1),
                'checkout':   today + datetime.timedelta(days = 2),
                'roomconfig': '1-0',
                'utm_source': 'direct'
            }
            response = requests.get(url = PRICING_URL, params = data)
            return response.json().get('data', None).get(str(hotel_id))
        except:
            raise Exception

    @staticmethod
    def get_room_prices(hotel_id, for_app=False):
        """
        To get rack_price and sale_price
        :param hotel_id:
        :param for_app:
        :return:
        """
        try:
            if for_app:
                key = "PRICING_APP_" + str(hotel_id)
            else:
                key = "PRICING_" + str(hotel_id)
            pricing_response_data = cache.get(key)

            if not pricing_response_data:
                if for_app:
                    pricing_response_data = DynamicAdsCacheSetService.get_pricing_for_app(hotel_id)
                else:
                    pricing_response_data = DynamicAdsService.fetch_room_prices(hotel_id)
                logger.info("Response from Pricing Function %s", pricing_response_data)

            if pricing_response_data.get("sell_price") == 0:
                raise Exception("Rooms Sold Out")
            price_dict = {
                'rack_rate': '{:.0f} {}'.format(
                    pricing_response_data["rack_rate"],
                    const.CURRENCY),
                'sell_rate': '{:.0f} {}'.format(
                    pricing_response_data["sell_price"],
                    const.CURRENCY),
            }

            logger.info(
                "Room Prices : rack_rate: %s sell_rate: %s",
                price_dict['rack_rate'],
                price_dict['sell_rate'])
            return price_dict
        except:
            raise Exception

    @staticmethod
    def send_email_with_file(proxy_object, to_email):
        """
        For only Sending Emails
        :param proxy_object:
        :param to_email: for sending email with attachments for viewing purpose
        :return:
        """
        file = File(open(proxy_object.file_path_name, 'r'))
        logger.info("Sending email to %s for proxy_object %s", to_email, proxy_object)
        logger.info("Opened the File %s", file)
        try:
            mail_obj = EmailMultiAlternatives()
            mail_obj.attach(proxy_object.file_name, file.read(), 'text/csv')
            mail_obj.subject = proxy_object.email_subject
            mail_obj.body = proxy_object.email_body
            mail_obj.from_email = settings.SERVER_EMAIL
            mail_obj.to = [to_email]
            mail_obj.send()
            logger.info('message sent successfully')
        except BaseException:
            logger.exception('sending email failed for Dynamic ads')
            raise

    @staticmethod
    def create_file_if_doesnot_exist(proxy_obj):
        #
        """
        To create the path if doesn't exist
        If exist then delete the previous file and create new one
        :param proxy_obj:
        :return:
        """
        if not os.path.exists(proxy_obj.file_path):
            logger.info("Path created with name %s", proxy_obj.file_path)
            try:
                os.makedirs(proxy_obj.file_path)
            except OSError:
                if not os.path.exists(proxy_obj.file_path):
                    raise
        else:
            if os.path.exists(proxy_obj.file_path_name):
                os.remove(proxy_obj.file_path_name)

            if proxy_obj.get_app_file_path() and os.path.exists(proxy_obj.get_app_file_path()):
                os.remove(proxy_obj.get_app_file_path())

    @staticmethod
    def send_task_failure_mail(feed_type):
        """
        In case of failure Mail goes to admin directly
        :param feed_type:
        :return:
        """
        message = 'Dynamic ads task failed for %s' % feed_type
        logger.error("Building Feeds failed for %s", feed_type)
        send_mail(
            subject = 'Dynamic ads task failed',
            message = message,
            from_email = settings.SERVER_EMAIL,
            recipient_list = DynamicAdsService.DYNAMIC_ADS_ADMIN_LIST)

    @staticmethod
    def perform_build_csv_operations(proxy_object):
        count = 0
        total = 0
        dynamic_ads_service = DynamicAdsService()
        try:
            dynamic_ads_service.create_file_if_doesnot_exist(proxy_object)
        except Exception as e:
            logger.exception("File Couldn't be found To write")

        DynamicAdsService.get_hotels_with_priority_list()

        hotel_obj_query_set = dynamic_ads_service.get_hotels_query_set()
        with open(proxy_object.file_path_name, 'w') as f:
            file = csv.writer(f, delimiter = ',')
            logger.info("Created file at path %s with File name %s", proxy_object.file_path,
                        proxy_object.file_path_name)
            headers = proxy_object.get_headers()
            logger.info("Got Headers %s", headers)
            file.writerow(headers)
            for obj in hotel_obj_query_set:
                total += 1
                logger.info("Found an obj with name %s", obj.__dict__)
                try:
                    as_list = proxy_object.as_list(obj)
                    logger.info("Recieved List as %s", as_list)
                    file.writerow(as_list)
                    count = count + 1
                except Exception as e:
                    logger.exception('Exception whileWriting data')

        if proxy_object.get_app_file_path():
            with open(proxy_object.get_app_file_path(), 'w') as f:
                file = csv.writer(f, delimiter=',')
                logger.info("Created file at path %s with File name %s", proxy_object.file_path,
                            proxy_object.get_app_file_path())
                headers = proxy_object.get_headers()
                logger.info("Got Headers %s", headers)
                file.writerow(headers)
                for obj in hotel_obj_query_set:
                    logger.info("Found an obj with name %s", obj.__dict__)
                    try:
                        as_list = proxy_object.as_list(obj, for_app=True)
                        logger.info("Recieved List as %s", as_list)
                        file.writerow(as_list)
                    except Exception as e:
                        logger.exception('Exception whileWriting data')

        DynamicAdsService.push_file_s3_bucket(proxy_object)

    @staticmethod
    def push_file_s3_bucket(proxy_object):
        conn_s3 = S3Connection(host='s3-ap-southeast-1.amazonaws.com')
        bucket = conn_s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
        bucket_key = Key(bucket)
        path = proxy_object.file_name
        bucket_key.key = settings.S3_STATIC_EXTERNAL_FILE_PATH + path
        bucket_key.set_contents_from_filename(proxy_object.file_path_name, replace=True, policy='public-read')

        path = proxy_object.get_app_file_name()
        if path:
            bucket_key.key = settings.S3_STATIC_EXTERNAL_FILE_PATH + path
            bucket_key.set_contents_from_filename(proxy_object.get_app_file_path(), replace=True, policy='public-read')

    @staticmethod
    def is_hotel_on_priority(hotel_id):
        return hotel_id in DynamicAdsService.hotel_ids_with_priority

    @staticmethod
    def get_hotels_with_priority_list():
        try:
            content_store = ContentStore.objects.get(name='hotel_ids_with_priority')
        except ContentStore.DoesNotExist:
            logger.error("No value found in Content Store for name: hotel_ids_with_priority")
            return

        hotel_ids = json.loads(content_store.value)
        DynamicAdsService.hotel_ids_with_priority = set(hotel_ids)
