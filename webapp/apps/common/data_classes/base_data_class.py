class BaseDataClass(object):

    def __eq__(self, other):
        return NotImplemented

    def __ne__(self, other):
        equivalent = self.__eq__(other)
        if equivalent is NotImplemented:
            return NotImplemented
        return not equivalent

    def __hash__(self):
        return NotImplemented
