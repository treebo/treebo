# pylint: disable = too-many-locals
import json
import logging
import datetime
from django.http import HttpResponse

from apps.bookings.providers.exceptions import BookingAlreadyCancelledException
from apps.growth.integrations.treebo_realization import TreeboRealizationClient
from apps.growth.services.submit_cancel_api_response import SubmitCancelResponse
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.growth.constants import BOOKING_CANCELLED_MESSAGE, CANCELLATION_REASONS_MAP
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class SubmitCancel(TreeboAPIView):
    authentication_classes = [CsrfExemptSessionAuthentication, ]

    def modify_response(self):
        return {
            "status": "success",
            "data": {
                "booking_status": {
                    "status": "CANCELLED",
                    "cancelled_dateTime": datetime.datetime.now(),
                    "message": BOOKING_CANCELLED_MESSAGE
                }
            }
        }

    # pylint: disable = arguments-differ,useless-super-delegation
    def dispatch(self, *args, **kwargs):
        return super(SubmitCancel, self).dispatch(*args, **kwargs)

    def get(self, request):
        raise Exception("Get method not supported")

    def post(self, request):
        try:
            submit_data = request.data
            logger.info("request data %s", submit_data)
            booking_hash = submit_data.get('booking_hash')
            cancellation_reason = CANCELLATION_REASONS_MAP.get(int(submit_data.get('reason')),
                                                               submit_data.get('message'))
            TreeboRealizationClient().cancel_booking(cancellation_hash=booking_hash,
                                                     cancellation_reason=cancellation_reason)

            submit_cancel = SubmitCancelResponse()
            submit_cancel_response = submit_cancel.create_submit_cancel_response()
            return HttpResponse(json.dumps(submit_cancel_response),
                                content_type="application/json",
                                status=200)

        except BookingAlreadyCancelledException as e:
            return HttpResponse(
                json.dumps(
                    {
                        "errors": {
                            "message": e.args[0]}}),
                content_type="application/json",
                status=400)
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.POST,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception("Submit API for cancel failed")
            return HttpResponse(
                json.dumps(
                    {
                        "errors": {
                            "message": "Exception in submitting API"}}),
                content_type="application/json",
                status=400)
