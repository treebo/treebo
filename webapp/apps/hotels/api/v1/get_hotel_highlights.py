import logging

from apps.hotels import utils
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory

__author__ = 'Rajdeep'

logger = logging.getLogger(__name__)

highlight_data_service = RepositoriesFactory.get_highlights_repository()


class HotelHighlights(TreeboAPIView):
    def getTreeboTrilights(self, hashedHotelId=""):

        hotelId = utils.decode_hash(hashedHotelId)
        trilightsSet = self.__getTrighlightQueerySet(hotelId)
        allTrilights, message = self.__getTrilights(trilightsSet)
        response = self.__generateResponse(allTrilights, message)

        return response

    def __getTrilights(self, trilightsSet):
        result = []
        message = "No trilights found"
        try:
            for trilight in trilightsSet:
                result.append(trilight.description)
                message = "success"

        except Exception as exc:
            logger.debug(exc)
            logger.debug(message)

        return result, message

    def __getTrighlightQueerySet(self, hotelId):
        trighlightObject = highlight_data_service.filter_localities(
            hotel_id=hotelId)
        return trighlightObject

    def __generateResponse(self, allTrilights, message):
        result = {"trilights": allTrilights, "message": message, 'status': 0}
        if message == 'success':
            result['status'] = 1

        return result
