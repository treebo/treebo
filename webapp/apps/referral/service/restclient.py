import logging
import time

import requests
from django.conf import settings

logger = logging.getLogger(__name__)


class ReferralClient(object):
    BASE_URL = 'https://api.appvirality.com'
    api_key = settings.APP_VIRALITY_API_KEY
    private_key = settings.APP_VIRALITY_PRIVATE_KEY

    @staticmethod
    def call_appvirality_server(url, data, method='GET'):

        headers = {
            'content-type': "application/json"
        }
        #response = requests.request(method=method, data=data, headers=headers, url=url)
        response = {"success": False, "message": "", "statuscode": 400}
        return response

    @classmethod
    def generate_referral_code(
            cls,
            name,
            email,
            phone_no,
            user_id,
            city,
            referrer_code="",
            share_link_identifier="",
            state="",
            country=""):
        """
        Sample Response:
            {
                "userkey": "76b2fced2bdb4247bcbfa61f004bd67a",
                "shareurl": "http://r.appvirality.com/zmlog",
                "shortcode": "zmlog",
                "isExistingUser": true,
                "hasReferrer": false,
                "successcode": 1
            }
        """
        url = cls.BASE_URL + '/v1/registerwebuser'
        data = {
            "apikey": cls.api_key,
            "privatekey": cls.private_key,
            "EmailId": email,
            "AppUserName": name,
            "UserIdInstore": user_id,
            "Phone": phone_no,
            "city": city,
            "state": state,
            "country": country
        }
        if share_link_identifier:
            data["avClick"] = share_link_identifier
        if referrer_code:
            data["ReferrerCode"] = referrer_code

        logger.debug('Calling API /v1/registerwebuser with request: %s', data)
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        return cls.__build_response(response)

    @classmethod
    def get_referrer_details(cls, click_id):
        """
        Sample Response:
        {
            "userkey": "91f27bd17daa4dafae39a61f00502ca8",
            "UserIdInstore": "TEST_ID",
            "name": "Rohit",
            "email": "rohit.jain.pce2011@gmail.com",
            "shortcode": "zmlyu",
            "profileimage": null,
            "successcode": 1
        }
        """
        url = cls.BASE_URL + '/v1/getreferrer/{0}'.format(click_id)
        response = cls.call_appvirality_server(method='GET', url=url)
        return cls.__build_response(response)

    @classmethod
    def register_event(
            cls,
            user_key,
            event_name,
            extra_info="",
            transaction_value="0",
            transaction_unit="Rs"):
        """
        Sample Response:
        {
            "success": false/true,
            "message": null
        }
        """
        url = cls.BASE_URL + '/v1/registerconversionevent'
        data = {
            "userkey": user_key,
            "apikey": cls.api_key,
            "privatekey": cls.private_key,
            "eventName": event_name,
            "extrainfo": extra_info,
            "transactionValue": transaction_value,
            "transactionUnit": transaction_unit,
            "tskey": int(time.time())
        }
        logger.debug(
            'Calling API /v1/registerconversionevent with request: %s',
            data)
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        return cls.__build_response(response)

    @classmethod
    def get_user_details(cls, user_key):
        """
        Sample Response:
            {
                "userkey": "76b2fced2bdb4247bcbfa61f004bd67a", // All values below would be null for invalid userkey in request
                "deviceId": "web::rohitjain.pce2011@gmail.com",
                "EmailId": "rohitjain.pce2011@gmail.com",
                "DeviceAdvertisingId": null,
                "AppUserName": "Rohit",
                "FriendReferralCode": null,
                "ReferralCode": "zmlog",
                "UserIdInstore": "TEST_ID",
                "success": true,
                "message": null
            }
        """
        url = cls.BASE_URL + '/v1/getappuserdetails'
        data = {
            "userkey": user_key,
            "apikey": cls.api_key,
            "privatekey": cls.private_key
        }
        logger.debug(
            'Calling API /v1/getappuserdetails with request: %s',
            data)
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        return cls.__build_response(response)

    @classmethod
    def get_friend_rewards(cls, user_key):
        url = cls.BASE_URL + '/v1/getfriendrewards'
        data = {
            "apikey": cls.api_key,
            "privatekey": cls.private_key
        }
        logger.debug('Calling API /v1/getfriendrewards with request: %s', data)
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        return cls.__build_response(response)

    @classmethod
    def get_referrer_rewards(cls, user_key):
        url = cls.BASE_URL + '/v1/getreferrerrewards'
        data = {
            "apikey": cls.api_key,
            "privatekey": cls.private_key
        }
        logger.debug(
            'Calling API /v1/getreferrerrewards with request: %s',
            data)
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        return cls.__build_response(response)

    @classmethod
    def __build_data(cls):
        data = {
            "apikey": cls.api_key,
            "privatekey": cls.private_key
        }
        return data

    @classmethod
    def __build_response(cls, data):
        #logger.info("response received from appvirality %s", data)
        return {
            "status": 400,
            "data": data
        }

    @classmethod
    def get_campaign_details(cls, campaign_id):
        url = cls.BASE_URL + '/v1/getcampaign'
        data = cls.__build_data()
        data.update({
            "campaignid": campaign_id
        })
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        return cls.__build_response(response)

    @classmethod
    def register_user(cls, user, click_link, referrer):
        """
        Register a user in AppVirality
        :param (dbcommon.models.profile.User) user:
        :param click_link:
        :param referrer:
        """
        url = cls.BASE_URL + '/v1/registerwebuser'
        data = cls.__build_data()
        data.update({
            "avClick": click_link,
            "ReferrerCode": referrer,
            "EmailId": user.email,
            "AppUserName": user.get_full_name(),
            "UserIdInstore": str(user.uuid),
            "ProfileImage": "",
            "Phone": user.phone_number,
            "city": "",
            "state": "",
            "country": ""
        })
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        logger.info("response from register received %s", response.status_code)
        return cls.__build_response(response)

    @classmethod
    def approve_reward(cls, reward_id):
        url = cls.BASE_URL + '/v1/changerewardstatus'
        data = cls.__build_data()
        data.update({
            "rewardslist": [
                {
                    "rewarddtlid": reward_id,
                    "reward_status": "Approved"
                }
            ]
        })
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        return cls.__build_response(response)

    @classmethod
    def update_user(cls, user, user_referral):
        """

        :param dbcommon.models.profile.User user:
        :param dbcommon.models.profile.UserReferral user_referral:
        """
        url = cls.BASE_URL + '/v1/updateuserinfo'
        data = cls.__build_data()
        data.update({
            "userkey": user_referral.userkey,
            "EmailId": user.email,
            "AppUserName": user.first_name,
            "ProfileImage": "",
            "UserIdInstore": str(user.uuid),
            "city": user.city,
            "state": "",
            "country": "",
            "Phone": user.phone_number
        })
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        return cls.__build_response(response)

    @classmethod
    def distribute_reward(cls, reward_id):
        url = cls.BASE_URL + "/v1/approvereward"
        data = cls.__build_data()
        data.update({
            "rewards": [
                {
                    "rewardid": reward_id,
                    "reward_type": "Coupon",
                    "status": "Distribute"
                }
            ]
        })
        response = cls.call_appvirality_server(method='POST', data=data, url=url)
        return cls.__build_response(response)
