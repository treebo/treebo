import logging
from datetime import datetime

from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response

from apps.bookings.api.v1.bookingstub import BookingsByOrderId
from apps.bookings.services.booking import BookingService
from apps.common import date_time_utils
from apps.common.error_codes import INVALID_ORDER_ID, INVALID_ORDER_ID_KEY
from apps.common.error_codes import get as error_msg
from apps.content.models import ContentStore
from apps.growth.services.growth_services import GrowthServices
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from base.views.api import TreeboAPIView
from dbcommon.models.direction import Directions
from services.restclient.contentservicerestclient import ContentServiceClient
from data_services.respositories_factory import RepositoriesFactory
from data_services.directions_repository import DirectionsRepository

__author__ = 'jatinder singh'

logger = logging.getLogger(__name__)


class ConfirmedBookingDetail(TreeboAPIView):
    __booking_service = BookingService()

    def get(self, request, *args, **kwargs):
        try:
            order_id = kwargs["order_id"]
        except BaseException:
            return JsonResponse(
                {"error": error_msg(INVALID_ORDER_ID_KEY)}, status=400)

        booking_instance = BookingsByOrderId()
        response = booking_instance.get(request, orderId=order_id)

        if response.status_code == 404:
            return JsonResponse(
                {"error": error_msg(INVALID_ORDER_ID)}, status=404)

        booking_dict = response.data
        booking_dict_confirmpage = self.get_confirmationdata(
            request, booking_dict, order_id)
        booking_dict_confirmpage["order_id"] = order_id

        return Response(booking_dict_confirmpage, status=status.HTTP_200_OK)

    def get_confirmationdata(self, request, booking_dict, order_id):
        hotel_name = booking_dict["hotel_name"]
        policies = ContentServiceClient.getValueForKey(
            request, 'policies_list', 1)

        hotel_repository = RepositoriesFactory.get_hotel_repository()
        myhotel = hotel_repository.get_hotel_by_name(hotel_name=hotel_name)
        directions = hotel_repository.get_directions_set_for_hotel(myhotel)
        # directions = directions_repository.get_all_directions_for_hotel(
        #     hotel_name=hotel_name)
        booking_dict_confirmpage = {}
        booking_dict_confirmpage["user"] = booking_dict["user_detail"]
        booking_dict_confirmpage["guest"] = {
            "adults": booking_dict["room_config"]["no_of_adults"],
            "children": booking_dict["room_config"]["no_of_childs"]}
        hotel_image_url = hotel_repository.get_showcased_image_url(myhotel)
        #hotel_image_url = hotel_image_obj.url if hotel_image_obj else ''
        booking_dict_confirmpage["hotel"] = {
            "name": booking_dict["hotel_name"],
            "city": booking_dict["hotel_detail"]["city"],
            "street": myhotel.street if myhotel and myhotel.street else "",
            "id": myhotel.id if myhotel and myhotel.id else None,
            "image_url": get_image_uncode_url(hotel_image_url),
            "coordinates": {
                "lat": myhotel.latitude if myhotel and myhotel.latitude else None,
                "lng": myhotel.longitude if myhotel and myhotel.latitude else None},
            'group_code': booking_dict["group_code"]}
        room_config = booking_dict["room_config_string"].split(',')
        config = []
        for rm in room_config:
            if rm != '':
                current_room = rm.split('-')
                config.append(
                    {"adults": int(current_room[0]), "children": int(current_room[1])})

        booking_dict_confirmpage["room"] = {
            "count": booking_dict["room_config"]["room_count"],
            "type": booking_dict["room_config"]["room_type"],
            "config": config}

        checkin_date = datetime.strptime(
            booking_dict["checkin_date"],
            date_time_utils.DATE_FORMAT).date()
        checkout_date = datetime.strptime(
            booking_dict["checkout_date"],
            date_time_utils.DATE_FORMAT).date()
        number_of_nights = (checkout_date - checkin_date).days

        booking_dict_confirmpage["date"] = {"checkin": checkin_date,
                                            "checkout": checkout_date}
        booking_dict_confirmpage["number_of_nights"] = number_of_nights

        discount_amount = float(booking_dict["payment_detail"]["discount"])
        voucher_amount = float(
            booking_dict["payment_detail"]["voucher_amount"])
        discount = max(discount_amount, voucher_amount)

        if booking_dict['payment_detail'].get("member_discount"):
            discount = discount + \
                float(booking_dict['payment_detail'].get("member_discount"))

        booking_dict_confirmpage["price"] = {
            "pretax_price": float(booking_dict["payment_detail"]["room_price"]),
            "total_tax": float(booking_dict["payment_detail"]["tax"]),
            "actual_total_cost": float(booking_dict["payment_detail"]["total"]),
            "discount_amount": discount
        }
        booking_dict_confirmpage["pay_at_hotel"] = booking_dict["paid_at_hotel"]
        booking_dict_confirmpage["status"] = booking_dict["status"]
        booking_dict_confirmpage["booking_date"] = booking_dict["booking_date"]
        booking_dict_confirmpage["cancel_date"] = booking_dict["cancel_date"]
        booking_dict_confirmpage["policies"] = policies
        directions_list = []
        for direction in directions:
            steps_list = direction.directions_for_email_website.split('\\n')
            directions_list.append(
                {
                    "landmark": {
                        "name": direction.landmark_obj.name,
                        "coordinates": {
                            "lat": direction.landmark_obj.latitude if direction.landmark_obj and direction.landmark_obj.latitude else None,
                            "lng": direction.landmark_obj.longitude if direction.landmark_obj and direction.landmark_obj.latitude else None},
                        "type": direction.landmark_obj.type},
                    "steps": steps_list,
                })
        booking_dict_confirmpage["hotel"]["directions"] = directions_list

        partpay_bookings_list = []
        partpay_booking_dict = {}
        partpay_booking_dict['order_id'] = order_id
        partpay_booking_dict['status'] = booking_dict["status"]
        partpay_booking_dict['audit'] = booking_dict["audit"]
        partpay_booking_dict['paid_at_hotel'] = booking_dict["paid_at_hotel"]
        partpay_booking_dict['hotel_detail'] = {
            "hotel_code": booking_dict["hotel_detail"]["hotel_code"]
        }

        partpay_bookings_list.append(partpay_booking_dict)
        growth_service = GrowthServices()
        booking_dict_confirmpage['partial_payment_link'], \
            booking_dict_confirmpage['partial_payment_amount'] = growth_service.booking_partpay_details(partpay_bookings_list)

        content_object = ContentStore.objects.filter(
            name="confirmation_page_partpay_msg", version=1).first()
        if content_object:
            booking_dict_confirmpage['partial_payment_message'] = str(
                content_object.value)
        else:
            booking_dict_confirmpage['partial_payment_message'] = ''

        return booking_dict_confirmpage
