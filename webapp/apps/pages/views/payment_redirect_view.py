import logging

from django.core.urlresolvers import reverse
from django.shortcuts import redirect

from apps.checkout.models import BookingRequest
from apps.checkout.service.checkout_service import save_booking_state
from common.services.feature_toggle_api import FeatureToggleAPI
from base.views.template import TreeboTemplateView
from ipware.ip import get_ip

logger = logging.getLogger(__name__)


class PaymentRedirectView(TreeboTemplateView):
    template_name = "confirmation/transaction.html"

    def get(self, request, args, kwargs):
        raise Exception('Method not supported')

    def __freeze_booking_request(self, request, channel, instant_booking):
        bid = request.POST.get("bid")
        pay_at_hotel = request.POST.get("pay_at_hotel")
        booking_channel = request.POST.get('channel', BookingRequest.WEBSITE)
        booking_payload = {
            "bid": bid,
            "pay_at_hotel": pay_at_hotel,
            "name": request.POST.get(
                "name",
                ""),
            "email": request.POST.get(
                "email",
                ""),
            "phone": request.POST.get(
                "mobile",
                ""),
            "user_signup_channel": request.COOKIES.get("user_signup_channel"),
            "booking_channel": booking_channel,
            "message": request.POST.get("message"),
            "check_in_time": request.POST.get("check_in_time"),
            "check_out_time": request.POST.get("check_out_time"),
            "otp_verified_number": request.POST.get('final_hidden_otp_verified_number'),
            "user": request.user,
            "instant_booking": instant_booking,
            "http_host": request.META['HTTP_HOST'],
            "utm_source": str(
                request.COOKIES.get('utm_source')) if request.COOKIES.get('utm_source') else "",
            "utm_medium": str(
                request.COOKIES.get('utm_medium')) if request.COOKIES.get('utm_medium') else "",
            "referral_click_id": str(
                request.COOKIES.get('avclk')) if request.COOKIES.get('avclk') else "",
            "utm_campaign": str(
                request.COOKIES.get('utm_campaign')) if request.COOKIES.get('utm_campaign') else "",
            "payment_mode": "Not Paid" if str(pay_at_hotel) == "1" else "Paid",
            "host_ip_address": str(
                get_ip(request)) if get_ip(request) else ""}

        logger.info(
            "Booking Payload for freezing booking request: %s",
            booking_payload)
        booking_request = save_booking_state(booking_payload)
        return booking_request

    def __parse_request(self, request, channel):
        booking_request = self.__freeze_booking_request(
            request, channel, False)
        url_string = request.META['HTTP_REFERER']
        if request.POST.get(
                'pay_at_hotel') == "2" and url_string.endswith("/"):
            url_string += "?"
        booking_request.booking_url = url_string + "&error=Unable to save booking"
        booking_request.meta_http_host = request.META['HTTP_HOST']

        booking_request.save()
        return booking_request

    def get_common_context(self, request, channel):
        logger.info(
            "get_common_context called with channel: %s, and POST data: %s",
            channel,
            request.POST)
        if FeatureToggleAPI.is_enabled("booking", "instant_booking", True):
            booking_request = self.__freeze_booking_request(
                request, channel, True)
            return redirect(
                reverse("pages:transaction") +
                "?referenceid={0}".format(
                    booking_request.id))
        else:
            booking_request = self.__parse_request(request, channel)
            return redirect(reverse("pages:transaction") +
                            '?referenceid={0}'.format(str(booking_request.id)))

    def postData(self, request, args, kwargs):
        logger.info("PaymentRedirectView postData called")
        context = self.get_common_context(request, "desktop")
        return context

    def postMobileData(self, request, args, kwargs):
        context = self.get_common_context(request, "mobile")
        return context
