from apps.content.models import ContentStore
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel, Categories
from dbcommon.models.landmark import SearchLandmark
from dbcommon.models.location import City, Locality
from data_services.hotel_repository import HotelRepository
from data_services.city_repository import CityRepository
import logging
import csv

from django.core.management.base import BaseCommand

__author__ = 'ansilkareem'


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Update databse for seo changes"
    footer_data, create = ContentStore.objects.get_or_create(
        name='footer_data')
    popular_cities = []
    distance_cap = None
    count_cap = None
    city_repository = RepositoriesFactory.get_city_repository()

    def handle(self, *args, **options):
        try:
            # self.update_popular_cities()
            # self.update_near_by_city_cap()
            # self.update_footer_data()
            # self.update_seo_enabled_city()
            # self.update_seo_locality()
            self.update_category()
            print("updated successfully")
        except Exception as ex:
            import traceback
            print(("traceback", traceback.format_exc()))

    def update_footer_data(self):
        self.footer_data.value = {'popular_cities': self.popular_cities,
                                  'distance_cap': self.distance_cap,
                                  'no_of_hotels_cap': self.count_cap}
        self.footer_data.save()

    def update_popular_cities(self,):
        with open('seo/popular_cities.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                self.popular_cities.append(row['Popular Cities'])

    def update_near_by_city_cap(self):
        with open('seo/nearby_city_list.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                self.distance_cap = int(row['Distance_cap'])
                self.count_cap = int(row['Count_Cap'])

    def update_seo_enabled_city(self):
        with open('seo/seo_enabled_city_list.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                city_obj = self.city_repository.filter_cities(
                    id=int(row['City_id']))
                if city_obj:
                    city_obj.update(enable_for_seo=1)
                else:
                    print(("city not found for row", row))

    def update_seo_landmark(self):
        with open('seo/landmark_sheet.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            kwargs = {}
            for row in reader:
                kwargs['latitude'] = float(row['lat'])
                kwargs['longitude'] = float(row['long'])
                kwargs['distance_cap'] = int(row['Distance cap'])
                kwargs['enable_for_seo'] = int(row['is seo enabled'])
                kwargs['free_text_url'] = str(row['free text url'])
                kwargs['city'] = self.city_repository.get_single_city(
                    id=int(row['city_id']))
                kwargs['seo_url_name'] = ' '.join(
                    str(row['Landmark Name']).split('-'))
                if row['Search Label']:
                    kwargs['search_label'] = str(row['Search Label'])
                if kwargs['city']:
                    landmarks = SearchLandmark.objects.filter(
                        city=kwargs['city'], seo_url_name__iexact=kwargs['seo_url_name'])
                    if landmarks:
                        landmarks.update(**kwargs)
                    else:
                        land_obj, created = SearchLandmark.objects.get_or_create(
                            city=kwargs['city'], seo_url_name=kwargs['seo_url_name'])
                        if created:
                            land_obj.latitude = kwargs['latitude']
                            land_obj.longitude = kwargs['longitude']
                            land_obj.distance_cap = kwargs['distance_cap']
                            land_obj.enable_for_seo = kwargs['enable_for_seo']
                            land_obj.free_text_url = kwargs['free_text_url']
                            if not row['Search Label']:
                                land_obj.search_label = kwargs['seo_url_name']
                            else:
                                land_obj.search_label = kwargs['search_label']
                            land_obj.save()

    def update_seo_locality(self):
        with open('seo/locality_sheet.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            kwargs = {}
            for row in reader:
                kwargs['latitude'] = float(row['Lat'])
                kwargs['longitude'] = float(row['Long'])
                kwargs['distance_cap'] = int(row['Distance Cap'])
                kwargs['enable_for_seo'] = int(row['is_seo_enabled'])
                kwargs['pincode'] = abs(int(row['Pincode']))
                kwargs['city'] = self.city_repository.get_single_city(
                    id=int(row['City_ID']))
                kwargs['name'] = ' '.join(str(row['Name']).split('-'))
                if kwargs['city']:
                    localities = Locality.objects.filter(
                        city=kwargs['city'], name__iexact=kwargs['name'])
                    if localities:
                        localities.update(**kwargs)
                    else:

                        loc_obj, created = Locality.objects.get_or_create(
                            city=kwargs['city'], name=kwargs['name'], pincode=kwargs['pincode'], latitude=kwargs['latitude'], longitude=kwargs['longitude'], )
                        if created:
                            loc_obj.distance_cap = kwargs['distance_cap']
                            loc_obj.enable_for_seo = kwargs['enable_for_seo']
                            loc_obj.save()

    def update_category(self):
        with open('seo/category_sheet.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            kwargs = {}
            for row in reader:
                kwargs['category_name'] = str(row['category_name'])
                kwargs['id'] = int(row['hotel_id'])
                kwargs['hotel_name'] = str(row['hotel_name'])
                try:
                    try:
                        hotel = Hotel.objects.get(id=kwargs['id'])
                    except Hotel.DoesNotExist:
                        hotel = None
                    if hotel:
                        category, create = Categories.objects.get_or_create(
                            name=kwargs['category_name'].lower())
                        hotel.category.add(category)
                        hotel.save()
                    else:
                        print(("Hotel not found", kwargs))
                except Hotel.MultipleObjectsReturned:
                    print(("Multiple entry found for", kwargs))
