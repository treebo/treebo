import logging
import threading

from rest_framework.response import Response

from apps.checkout.service.phonepe_service import PhonePePaymentConfirmationService
from apps.common.exceptions.custom_exception import StatusPendingException
from services.restclient.payment_client_v2 import PaymentClientV2

logger=logging.getLogger(__name__)

class PhonePeStatusPendingCronService(threading.Thread):
    phonepe_payment_confirmation_service = PhonePePaymentConfirmationService()
    payment_client = PaymentClientV2()

    def run(self):
        self.process_update_pending_requests("phonepe")  # run this in new thread as it processin
        #  intensive

    def process_update_pending_requests(self, gateway):
        logger.info('Starting to update the pending status requests')
        try:
            payment_statuses = self.payment_client.status_pending(gateway)
        except StatusPendingException as e:
            logger.exception('Could not get the updated the status')
            return Response(data='Error while hitting payment service')

        logger.info('mocking the updated status with s2s callback for %s number of status', len(payment_statuses))
        for status in payment_statuses:
            try:
                if 'data' in status and 'providerReferenceId' in status['data']:
                    provider_reference_id = status['data']['providerReferenceId']
                else:
                    provider_reference_id = None
                ps_payment_id = status['ps_payment_id']
                pg_meta = {}
                # this is pg_payment_id
                pg_meta['provider_reference_id'] = provider_reference_id
                pg_meta['code'] = status['code']

                self.phonepe_payment_confirmation_service.phonepe_confirm_payment(pg_meta, ps_payment_id)
            except Exception as e:
                logger.exception('Error while processing %s', status)
        logger.info('Done processing all the update status')
        msg = 'Processed %s pending records'%{len(payment_statuses)}
        return Response(data =msg)


