import mock
from datetime import datetime
from django.test import SimpleTestCase

from apps.profiles.serializers import serializers
from apps.profiles.serializers.serializers import UserProfileSerializer, UserSerializer, UserRegisterSerializer, UserActivationSerializer, PhoneValidationSerializer
from dbcommon.models.profile import User, UserOtp, UserEmail


class UserProfileSerializerTest(SimpleTestCase):
    @mock.patch.object(serializers.UserEmail, 'objects')
    def test_work_email_with_no_useremail(self, mock_useremail_manager):
        mock_useremail_manager.get.side_effect = UserEmail.DoesNotExist()
        user = User(email='test@treebohotels.com')
        data = UserProfileSerializer(instance=user).data
        self.assertEqual(data['work_email'], user.email)

    @mock.patch.object(serializers.UserEmail, 'objects')
    def test_work_email_with_useremail(self, mock_useremail_manager):
        mock_useremail = UserEmail(email='test2@treebohotels.com')
        mock_useremail_manager.get.return_value = mock_useremail
        user = User(email='test@treebohotels.com')
        data = UserProfileSerializer(instance=user).data
        self.assertEqual(data['work_email'], mock_useremail.email)

    def test_anniversary_date(self):
        user = User(anniversary_date=datetime(1985, 8, 25))
        data = UserProfileSerializer(instance=user).data
        self.assertEqual(data['anniversary'], '1985-08-25')


class UserRegisterSerializerTest(SimpleTestCase):
    def test_valid_serializer(self):
        valid_data = {
            "first_name": "Rohit",
            "email": "test@treebohotels.com",
            "password": "Test",
        }
        serializer = UserRegisterSerializer(data=valid_data)
        self.assertTrue(
            serializer.is_valid(),
            "Serializer Data not valid. Error: {0}".format(
                serializer.errors))

    def test_invalid_serializer(self):
        valid_data = {
            "email": "test@treebohotels.com",
        }
        serializer = UserRegisterSerializer(data=valid_data)
        self.assertFalse(serializer.is_valid())
