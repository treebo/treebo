import re

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

from dbcommon.models.profile import User


class CustomValidator(object):
    @staticmethod
    def normalise_email(email):
        if email:
            return User.objects.strip_gmail_special_chars(email)
        else:
            return email

    @staticmethod
    def normalise_phone_number(phone_number):
        if phone_number:
            phone_re = re.compile('^(6|7|8|9)\d{9}$')
            if not phone_re.match(phone_number):
                raise ValidationError("not valid")
        return phone_number


GSTIN_VALIDATOR = RegexValidator(
    regex=r'[\d]{2}[A-Z]{5}[\d]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$',
    message='Invalid GSTIN',
    code='invalid_gstin')
