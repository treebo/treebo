from django.utils.text import slugify
__author__ = 'ansilkareem'


class BreadCrumb():

    def __init__(self, current_city, footer_data, page_name, item_name, current_locality=None, current_landmark=None):
        self.current_city = current_city
        self.current_locality = current_locality
        self.current_landmark = current_landmark
        self.footer_data = footer_data
        self.page_name = page_name
        self.item_name = item_name

    def build(self, ):

        item = None
        city_item = ''
        breadcrumbs = []
        if 'breadcrumbs' in self.footer_data:
            if self.page_name.lower() == 'landmark':
                if 'landmark' in self.footer_data['breadcrumbs']:
                    item = self.footer_data['breadcrumbs']['landmark'] % self.item_name
            elif self.page_name.lower() == 'locality':
                if 'locality' in self.footer_data['breadcrumbs']:
                    item = self.footer_data['breadcrumbs']['locality'] % self.item_name
            elif self.page_name.lower() == 'category':
                if 'category' in self.footer_data['breadcrumbs']:
                    item = self.footer_data['breadcrumbs']['category'] % (
                        self.item_name, self.current_city.slug if self.current_city.slug
                        else self.current_city.name)
            elif self.page_name.lower() == 'hd':
                item = self.item_name.title()
            elif self.page_name.lower() == 'city':
                if 'city' in self.footer_data['breadcrumbs']:
                    city_item = self.footer_data['breadcrumbs'][
                                    'city'] % self.current_city.slug if self.current_city.slug else self.current_city.name
            elif self.page_name.lower() == 'city-amenity':
                if 'city-amenity' in self.footer_data['breadcrumbs']:
                    item = self.footer_data['breadcrumbs']['city-amenity'] % (self.current_city.slug
                                    if self.current_city.slug else self.current_city.name, self.item_name)
            elif self.page_name.lower() == 'locality-category':
                if 'locality-category' in self.footer_data['breadcrumbs']:
                    item = self.footer_data['breadcrumbs']['locality-category'] % (
                        self.item_name, self.current_locality.name)
            elif self.page_name.lower() == 'landmark-category':
                if 'landmark-category' in self.footer_data['breadcrumbs']:
                    item = self.footer_data['breadcrumbs']['landmark-category'] % (
                        self.item_name, self.current_landmark.seo_url_name
                    )
        breadcrumbs.append(
            {
                "label": "Home",
                "destination": "/",
                "schema": "Treebo.com",
                "position": 1,
            }
        )
        if city_item:
            breadcrumbs.append(
                {
                    "label": city_item.title(),
                    "destination": "",
                    "schema": city_item.title(),
                    "position": 2,
                }
            )
        elif item:
            breadcrumbs.append(
                {
                    "label": "Hotels in " + self.current_city.slug.title() if self.current_city.slug else self.current_city.name.title(),
                    "destination": "/hotels-in-" + self.current_city.slug + "/" if self.current_city.slug else self.current_city.name,
                    "schema": "Hotels in " + self.current_city.slug.title() if self.current_city.slug else self.current_city.name.title(),
                    "position": 2,
                })
            if self.current_locality or self.current_landmark:
                if self.current_locality:
                    breadcrumbs.append(
                        {
                            "label": "Hotels in " + self.current_locality.name.title(),
                            "destination": "/hotels-in-" + slugify(
                                self.current_locality.name) + '-' + str(self.current_city.slug) + "/",
                            "schema": "Hotels in " + self.current_locality.name.title(),
                            "position": 3,
                        })
                elif self.current_landmark:
                    breadcrumbs.append(
                        {
                            "label": "Hotels near " + self.current_landmark.seo_url_name.title(),
                            "destination": "/hotels-near-" + slugify(
                                self.current_landmark.free_text_url if self.current_landmark.free_text_url else self.current_landmark.seo_url_name + '-' + str(
                                    self.current_city.slug)) + "/",
                            "schema": "Hotels near " + self.current_landmark.seo_url_name.title(),
                            "position": 3,
                        })
                breadcrumbs.append(
                    {
                        "label": item.title(),
                        "destination": "",
                        "schema": item.title(),
                        "position": 4,
                    })

            else:
                breadcrumbs.append(
                    {
                        "label": item.title(),
                        "destination": "",
                        "schema": item.title(),
                        "position": 3,
                    })
        return breadcrumbs
