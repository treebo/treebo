'use strict';

import $ from 'jquery';
import {Observable} from "rx";
import BaseView from './treebolibs/baseview';
import logger from './treebolibs/logger';
import '../css/faq.less';

let FaqView = Object.create(BaseView);

FaqView.pageLoaded = () => {

    $(".faq-block__status").click(e => {
    	const $parentElem = $(e.target).closest('.faq-block__qa');
    	$parentElem.find('.faq-block__a').slideToggle();
    	$parentElem.find(".up-down-arrow").toggleClass("rotate");
    	$parentElem.find(".up-down-arrow").toggleClass("rotate-reset");
    })
    logger.log(" FAQ page loaded");

}

let faqConfig = { name:'', widgets:[]};
let faqView = FaqView.setup(faqConfig);
