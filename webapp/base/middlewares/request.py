import json
import logging
import uuid
from django.http.response import HttpResponseRedirect
from log_request_id import local

logger = logging.getLogger(__name__)

thread_local = local


def get_domain():
    return getattr(thread_local, 'domain', None)


class RequestMiddleWare(object):
    def is_auth_request(self, path):
        return (path[0:20] in ["/account/settings", "/user/reset-password"])

    def process_request(self, request):

        thread_local.domain = request.build_absolute_uri('/')[:-1]
        # IE support page for IE browser
        if 'HTTP_USER_AGENT' in request.META:
            user_agent = request.META['HTTP_USER_AGENT'].lower()
            if ('msie' in user_agent or 'trident' in user_agent) and 'iesupport' in request.path:
                pass
            elif 'msie' in user_agent or 'trident' in user_agent:
                return HttpResponseRedirect('/iesupport')

        # WEB-1976
        if not hasattr(
                request,
                'user') or not request.user.is_active or not request.user.is_authenticated():
            self.set_user_id(request)

        # setattr(request, 'is_mobile', 1)

    def set_user_id(self, request):
        if hasattr(request, 'user_id'):
            return
        user_id = request.COOKIES.get('user_id', None)
        if not user_id:
            user_id = str(uuid.uuid4())
        setattr(request, 'user_id', user_id)

    def process_response(self, request, response):
        try:
            """It is probably not the best way to pass user id in cookies like this. However, this user id is only being
                used currently for activity tracking for non logged in users. So it should be safe.
                If we decide to do more complex activities in future, we will need to revisit this
            """
            if not hasattr(
                    request,
                    'user') or not request.user.is_active or not request.user.is_authenticated():
                if hasattr(request, 'user_id') and request.user_id:
                    response.set_cookie(
                        'user_id', request.user_id, max_age=24 * 60)
        except Exception as e:
            logger.exception(e)
        return response
