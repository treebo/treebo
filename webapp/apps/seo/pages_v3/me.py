from apps.content.services.content_store_service import ContentStoreService
from apps.seo.pages.pages import PAGES


class Me(PAGES):
    ME_META_KEY = "search_me_desc"

    def __init__(self, page='', query='', is_amp=''):
        self.page = "near"
        self.query = "me"
        super(Me, self).__init__(page, query, '')

    def update_meta(self, ):
        pass

    def build_meta(self, *args):
        entries = ContentStoreService.get_contents([Me.ME_META_KEY])
        for entry in entries:
            self.meta_content = entry.value
            break
