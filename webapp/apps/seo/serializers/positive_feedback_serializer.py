from rest_framework import serializers


class ItemsDTO(serializers.Serializer):
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    description = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)


class PositiveFeeedbackDTO(serializers.Serializer):
    items = ItemsDTO(required=False, many=True, allow_null=True)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
