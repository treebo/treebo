# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0014_auto_20160418_1759'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='booking_url',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
