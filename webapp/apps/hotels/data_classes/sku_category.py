# pylint: disable=invalid-name
from collections import namedtuple

SkuCategory = namedtuple('SkuCategory', ['expense_item_id', 'uid'])

LunchSku = SkuCategory("lunch", "food")
BeveragesSku = SkuCategory("bevereges", "bevereges")
LaundrySku = SkuCategory("laundry", "laundry")
BanquetSku = SkuCategory("banquet", "banquet")
CabSku = SkuCategory("taxi", "taxi")
DinnerSku = SkuCategory("dinner", "food")
SnacksSku = SkuCategory("snacks", "food")
RoomSku = SkuCategory("roomnight", "roomnight")
ExtraBedSku = SkuCategory("", "")
ConferenceRoomSku = SkuCategory("", "")
