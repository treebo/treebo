import logging

from rest_framework.renderers import BaseRenderer

logger = logging.getLogger(__name__)


class XMLRenderer(BaseRenderer):
    """
    XML Renderer.
    """
    media_type = 'text/xml'

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """
        Simply return a string representing the body of the request.
        """
        logger.info(data)
        return data
