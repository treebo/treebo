# -*-coding:utf-8-*-
import logging
from concurrent.futures import ThreadPoolExecutor, as_completed
from functools import reduce

import requests

from apps.common.exceptions.custom_exception import BadITSAPIRequestException, ITSServiceUnavailableException, \
    InactiveHotelITSRequestException
from apps.common.slack_alert import SlackAlertService as slack_alert
from availability.services.models import TDAvailabilityAcrossConfig, TDConfigAvailabilityOnDate, TDRoomConfigDet, \
    TDHotelAvailabilitySearchParams, TDHotelAvailability, TDRoomTypeAvailabilityOnDate, TDRoomTypeDet
from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)
hotel_repository = RepositoriesFactory.get_hotel_repository()


class ITSSearchService:
    """
    This class is used for parsing the information of availability of rooms via
    Inventory Throttling Service
    """
    URL_FOR_MINIMAL_ACROSS_ALL_CONFIG = "%s/its/api/v1/availability/minimal-across-all-config"
    URL_V2_FOR_MINIMAL_ACROSS_ALL_CONFIG = "%s/its/api/v2/availability/minimal-across-all-config"
    URL_FOR_DATEWISE_AVAILABILITY_ACROSS_ALL_CONFIG = "%s/its/api/v1/availability/date-wise"
    CHECK_IN = 'checkin'
    CHECK_OUT = 'checkout'
    HOTELS = 'hotels'
    ROOM_CONFIG = 'roomconfig'
    CHANNEL = 'channel'
    SUB_CHANNEL = 'subchannel'
    STATUS = "status"
    DATA = "data"
    HOTEL_ID = "hotel_id"
    DATE_WISE_AVAILABILITY = "date_wise_availability"
    DATE = "date"
    ROOM_CONFIG_WISE_AVAILABILITY = "room_config_wise_availability"
    ROOM_CONFIG_STR = "room_config"
    ROOM_TYPE_AVAILABILITY = "room_type_availability"
    ROOM_TYPE = "room_type"
    AVAILABILITY = "availability"

    @staticmethod
    def transform_search_params_for_its_call(search_params):
        """
        This is the API to convert the parameters passed in to the Search into standard format that
        the standard format that requests object expects
        :param search_params: The HotelAvailabilitySearchParams that contains filter information
        :return: the params dict
        """
        if not search_params.cs_ids:
            hotel_cs_ids_map = {hotel.cs_id.strip(): hotel.id
                            for hotel in hotel_repository.get_hotels_for_id_list(search_params.hotels) if hotel.cs_id is not None}
            if len(hotel_cs_ids_map.items()) == 0:
                raise InactiveHotelITSRequestException("Hotel is inactive")
            else:
                hotel_cs_ids = reduce(lambda x, y: x.strip() + ',' + y.strip(), hotel_cs_ids_map)
            search_params.hotel_cs_ids_map = hotel_cs_ids_map
        else:
            hotel_cs_ids = ",".join(cs_id for cs_id in search_params.cs_ids)

        params = {
            ITSSearchService.CHECK_IN: search_params.checkin,
            ITSSearchService.CHECK_OUT: search_params.checkout,
            ITSSearchService.HOTELS: hotel_cs_ids,
            ITSSearchService.CHANNEL: search_params.channel,
            ITSSearchService.SUB_CHANNEL: search_params.sub_channel
        }
        if search_params.room_config:
            params[ITSSearchService.ROOM_CONFIG] = search_params.room_config
        return params

    @staticmethod
    def parse_its_response_for_availability(response_json, hotel_cs_ids_map):
        available_hotels = []
        for hotel in response_json.get(ITSSearchService.DATA):
            ht = TDHotelAvailability()
            ht.hotel_id = hotel_cs_ids_map.get(hotel[ITSSearchService.HOTEL_ID])
            ht.hotel_cs_id = hotel[ITSSearchService.HOTEL_ID]
            room_types = []
            for avl in hotel[ITSSearchService.AVAILABILITY]:
                availability_for_room_type = TDRoomTypeDet()
                availability_for_room_type.room_type = avl[ITSSearchService.ROOM_TYPE].lower()
                availability_for_room_type.rooms_available = avl[ITSSearchService.AVAILABILITY]
                room_types.append(availability_for_room_type)
            ht.room_type_availability = room_types
            available_hotels.append(ht)
        return available_hotels

    @staticmethod
    def get_availability_across_config(search_param):
        """
        API that gets the minimum assured availabilty across all different config offerings
        in each hotel that we own
        :param search_param: The HotelAvailabilitySearchParams that contains filter information
        :return: A pojo representing the minimum assured availabilty across all different offerings
        in each hotel
        """
        hotel_availability_response_json = {ITSSearchService.DATA: {}}
        try:
            param = ITSSearchService.transform_search_params_for_its_call(search_param)
            hotel_availability_response_json = ITSSearchService.make_its_call(
                ITSSearchService.URL_FOR_MINIMAL_ACROSS_ALL_CONFIG, param)
        except (ITSServiceUnavailableException, BadITSAPIRequestException,
                InactiveHotelITSRequestException):
            logger.exception("ITS Request Exception")

        available_hotels = ITSSearchService.parse_its_response_for_availability(
            response_json=hotel_availability_response_json, hotel_cs_ids_map=search_param.hotel_cs_ids_map)

        logger.debug('The request %s succeeded to be retrieve %s' %
                     (search_param, available_hotels))
        return available_hotels

    @staticmethod
    def get_day_wise_availability_across_config(search_param):
        """
        The datewise availability of number of rooms in each category that we have satisfying
        the given criteria
        :param search_param: The HotelAvailabilitySearchParams that contains filter information
        :return: A pojo giving the datewise availability of number of rooms in each type
            that we have
        """
        hotel_availability = {ITSSearchService.DATA: {}}
        try:
            param = ITSSearchService.transform_search_params_for_its_call(search_param)
            hotel_availability = ITSSearchService.make_its_call(
                ITSSearchService.URL_FOR_DATEWISE_AVAILABILITY_ACROSS_ALL_CONFIG, param)
        except (ITSServiceUnavailableException, BadITSAPIRequestException,
                InactiveHotelITSRequestException):
            logger.exception("ITS Request Exception")

        across_config_availability = TDAvailabilityAcrossConfig()
        across_config_availability.request_data = search_param
        across_config_availability.data = []
        for hotel in hotel_availability.get(ITSSearchService.DATA):
            ht = TDHotelAvailability()
            ht.hotel_cs_id = hotel[ITSSearchService.HOTEL_ID]
            ht.hotel_id = search_param.hotel_cs_ids_map.get(ht.hotel_cs_id)
            hotel_config_date_wise_availability = []
            for dt in hotel[ITSSearchService.DATE_WISE_AVAILABILITY]:
                dt_availability = dt[ITSSearchService.DATE]
                date_wise_availability = TDConfigAvailabilityOnDate()
                date_wise_availability.date = dt_availability
                rm_config_list = []
                for room_config in dt[ITSSearchService.ROOM_CONFIG_WISE_AVAILABILITY]:
                    rm_config_det = TDRoomConfigDet()
                    rm_config_det.room_config = room_config[ITSSearchService.ROOM_CONFIG_STR]
                    rm_types = []
                    for room_type in room_config[ITSSearchService.ROOM_TYPE_AVAILABILITY]:
                        rm_type = TDRoomTypeDet()
                        rm_type.room_type = room_type[ITSSearchService.ROOM_TYPE]
                        rm_type.rooms_available = room_type[ITSSearchService.AVAILABILITY]
                        rm_types.append(rm_type)
                    rm_config_det.room_types_available = rm_types
                    rm_config_list.append(rm_config_det)
                date_wise_availability.room_config_list = rm_config_list
                hotel_config_date_wise_availability.append(date_wise_availability)
            ht.config_wise_availability = hotel_config_date_wise_availability
            across_config_availability.data.append(ht)
        logger.debug('The request %s succeeded to be retrieve %s',
                     (search_param, across_config_availability))
        return across_config_availability

    @staticmethod
    def date_wise_room_type_ws(search_param):
        """
        Date wise availability of number of rooms available in each room type
        :param search_param: The HotelAvailabilitySearchParams that contains filter information
        :return: Json representation of availability in format
        {
           "2":{
              "2017-10-18":{
                 "mahogany":1,
                 "acacia":0,
                 "maple":0
              },
              "2017-10-19":{
                 "mahogany":14,
                 "acacia":8,
                 "maple":0
              },
              "2017-10-20":{
                 "mahogany":12,
                 "acacia":0,
                 "maple":0
              }
           },
           "22":{
              "2017-10-18":{
                 "mahogany":1,
                 "acacia":1,
                 "maple":0
              },
              "2017-10-19":{
                 "mahogany":2,
                 "acacia":0,
                 "maple":1
              },
              "2017-10-20":{
                 "mahogany":5,
                 "acacia":2,
                 "maple":3
              }
           }
        }
        """
        param = ITSSearchService.transform_search_params_for_its_call(search_param)
        try:
            no_of_hotels = len(search_param.hotel_cs_ids_map)
            if no_of_hotels > 20:
                with ThreadPoolExecutor(max_workers=5) as executor:
                    hotel_cs_ids = param.get(ITSSearchService.HOTELS, '').split(',')
                    no_of_calls = int(no_of_hotels / 20) + 1
                    futures = []
                    for i in range(no_of_calls):
                        i_hotels = hotel_cs_ids[i*20:
                                                no_of_hotels if no_of_hotels < (i + 1) * 20 else (i + 1) * 20]
                        param[ITSSearchService.HOTELS] = reduce(lambda x, y: x.strip() + ',' + y.strip(), i_hotels)

                        future = executor.submit(ITSSearchService.make_its_call,
                                                 ITSSearchService.URL_FOR_DATEWISE_AVAILABILITY_ACROSS_ALL_CONFIG,
                                                 param)
                        futures.append(future)

                    hotel_availability = None
                    for future in as_completed(futures):
                        response = future.result()

                        if hotel_availability:
                            hotel_availability['data'].update(response.get('data'))
                        else:
                            hotel_availability = response
            else:
                hotel_availability = ITSSearchService.make_its_call(
                    ITSSearchService.URL_FOR_DATEWISE_AVAILABILITY_ACROSS_ALL_CONFIG, param)
        except (ITSServiceUnavailableException, BadITSAPIRequestException, InactiveHotelITSRequestException) as e:
            logger.error("ITSServiceUnavailableException/BadITSAPIRequestException in ITS Client "
                         "for day-wise API with param {0}".format(search_param), exc_info=e)
            hotel_availability = {ITSSearchService.DATA: {}}
        except Exception as e:
            logger.error("Exception in ITS client for day-wise API with param {0}"
                         .format(search_param), exc_info=e)
            slack_alert.send_slack_alert_for_exceptions(status=400, request_param=search_param,
                                                        message=e.__str__(),
                                                        class_name=ITSSearchService.__class__.__name__)
            hotel_availability = {ITSSearchService.DATA: {}}

        available_hotels = {search_param.hotel_cs_ids_map[hotel]: availability
                            for hotel, availability in hotel_availability.get(ITSSearchService.DATA).items()}
        return available_hotels

    @staticmethod
    def date_wise_room_type_availability(search_param: TDHotelAvailabilitySearchParams):
        """
        :param search_param: The HotelAvailabilitySearchParams that contains filter information
        :return: A pojo giving the datewise availability of number of rooms for each date
             in each type that we have
        """
        across_config_availability = TDAvailabilityAcrossConfig()
        across_config_availability.request_data = search_param
        across_config_availability.data = []
        hotel_availability_dict = ITSSearchService.date_wise_room_type_ws(search_param)
        for hotel_id in hotel_availability_dict.keys():
            ht = TDRoomTypeAvailabilityOnDate()
            ht.hotel_id = hotel_id
            ht_data = {}
            for dt in hotel_availability_dict[hotel_id].keys():
                room_types = []
                for rt in hotel_availability_dict[hotel_id][dt].keys():
                    rm_type = TDRoomTypeDet()
                    rm_type.room_type = rt
                    rm_type.rooms_available = hotel_availability_dict[hotel_id][dt][rt]
                    room_types.append(rm_type)
                ht_data[dt] = room_types
            ht.data = ht_data
            across_config_availability.data.append(ht)
        logger.debug('The request %s succeeded to be retrieve %s' %
                     (search_param, across_config_availability))
        return across_config_availability

    @staticmethod
    def make_its_call(query_url, search_params):
        query_url = query_url % get_base_its_url()
        res = {}
        try:
            res = requests.get(query_url, search_params)
        except Exception as e:
            logger.error(e)
            slack_alert.send_slack_alert_for_third_party(status=res.status_code,
                                                         class_name=ITSSearchService.__class__.__name__,
                                                         url=query_url,
                                                         reason='ITS Service Unavailable'
                                                         )
            raise ITSServiceUnavailableException('ITS service not available. query_url {0} and search_params {1}'
                                                 .format(query_url, search_params))

        logger.debug("ITS url requested %(url)s with params %(params)s and result %(res)s",
                     {'url': query_url, 'params': search_params, 'res': res.text})
        if res.status_code != requests.codes.ok or res.json()['status'] != 200:
            slack_alert.send_slack_alert_for_third_party(status=res.status_code,
                                                         class_name=ITSSearchService.__class__.__name__,
                                                         url=query_url,
                                                         reason='ITS api call failed for search_params {0}'
                                                         .format(search_params)
                                                         )
            logger.error('ITS api call failed for search_params {0}'.format(search_params))
            raise BadITSAPIRequestException('ITS api call failed with status {0} for '
                                            'query_url: {1} and search_params {2}'
                                            .format(res.status_code, query_url, search_params))

        return res.json()

    @staticmethod
    def get_availability_across_room_config(search_param):
        availability_response = {ITSSearchService.DATA: {}}
        try:
            param = ITSSearchService.transform_search_params_for_its_call(search_param)
            availability_response = ITSSearchService.make_its_call(
                ITSSearchService.URL_V2_FOR_MINIMAL_ACROSS_ALL_CONFIG, param)
        except (ITSServiceUnavailableException, BadITSAPIRequestException,
                InactiveHotelITSRequestException):
            logger.exception("ITS Request Exception")

        return availability_response


def get_base_its_url():
    from django.conf import settings
    return settings.BASE_ITS_URL

