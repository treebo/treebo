from rest_framework import serializers


class CancelBookingDTO(serializers.Serializer):
    order_id = serializers.CharField(max_length=40)
