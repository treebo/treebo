import json
import logging

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class FeatureToggleStatus(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FeatureToggleStatus, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request):
        try:
            namespace = request.GET['namespace']
            feature = request.GET['feature']

            is_enabled = FeatureToggleAPI.is_enabled(namespace, feature, False)
            context = {}

            context['is_enabled'] = is_enabled
            return Response(context)
        except Exception as exc:
            logger.exception(exc)
            context = {
                "status": "error",
                "msg": "Unbale to fetch feature toggle information"}
            return Response(context, status=500)

    @log_args(logger)
    def post(self, request, format=None):
        raise Exception('Method not supported')
