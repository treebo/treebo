import logging

from django.utils.dateformat import DateFormat

from apps.checkout.models import BookingRequest
from apps.checkout.service.special_details_service import SpecialDetailsService
from apps.common import date_time_utils
from apps.hotels.service.hotel_policy_service import HotelPolicyService
from base import log_args
from base.views.template import TreeboTemplateView
from common.constants import common_constants
from data_services.respositories_factory import RepositoriesFactory

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)

directions_service = RepositoriesFactory.get_directions_repository()


class PdfData(TreeboTemplateView):
    """
    Receipt HTMl For PDF
    """
    # template_name = 'hotels/receipt.html'
    template_name = 'voucher/index.html'

    special_details_service = SpecialDetailsService()

    # FIXME Get rid of booking service

    @log_args(logger)
    def getData(self, request, args, kwargs):

        error = ""
        booking_request = BookingRequest.objects.get(
            order_id=request.GET.get("order_id"))
        logger.info("Booking Object in print_pdf: %s", booking_request)

        user = booking_request.user
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotel = hotel_repository.get_single_hotel(
            id=booking_request.hotel_id)
        hotel_policies = HotelPolicyService.get_hotel_policies(
            booking_request.hotel_id)

        guest = booking_request.adults
        days = (
            booking_request.checkout_date -
            booking_request.checkin_date).days
        adults = booking_request.adults
        children = booking_request.children
        checkin_year = DateFormat(booking_request.checkin_date).format('Y')
        checkout_year = DateFormat(booking_request.checkout_date).format('Y')

        checkinDay = DateFormat(booking_request.checkin_date).format('l')
        checkoutDay = DateFormat(booking_request.checkout_date).format('l')

        checkin = DateFormat(booking_request.checkin_date).format('d M')
        checkout = DateFormat(booking_request.checkout_date).format('d M')

        actualTotalCost = booking_request.total_amount.quantize(2)
        pretaxPrice = booking_request.pretax_amount.quantize(2)
        total_tax = booking_request.tax_amount.quantize(2)
        treebo_points = booking_request.wallet_deduction.quantize(
            2) if booking_request.wallet_applied else 0.0

        image_url = self.__getDesktopImageUrl(hotel)

        special_rates = self.special_details_service.get_special_date_details(
            booking_request)

        room_count = len(booking_request.room_config.split(","))
        booking_code, booking_date, discount_amount, payment_text = self.__populateDesktopPdfData(
            booking_request)

        pah = 0
        if "Not Paid" in booking_request.payment_mode:
            pah = 1
        if booking_request.is_audit:
            actualTotalCost = 0

        context = {
            "error": error,
            'page_name': 'Receipt',
            "checkin": checkin,
            "checkout": checkout,
            "hotel": hotel,
            "actual_total_cost": actualTotalCost,
            "room": booking_request.room_type,
            "room_count": room_count,
            "adults": adults,
            "children": children,
            "days": days,
            "guest": guest,
            "booking": booking_request,
            "pretax_price": pretaxPrice,
            'customer_care_number': common_constants.CUSTOMER_CARE_NUMBER,
            "guest_name": booking_request.name,
            "guest_email": booking_request.email,
            "guest_mobile": booking_request.phone,
            "comments": booking_request.comments,
            "booking_code": booking_code,
            "image_url": image_url,
            "payment_amount": booking_request.payment_amount,
            "discount_amount": discount_amount,
            "total_tax": total_tax,
            "payment_text": payment_text,
            "treebo_points": -1 * treebo_points,
            "total_payable_amount": float(actualTotalCost) - float(treebo_points),
            "booking_date": booking_date,
            "checkin_year": checkin_year,
            "checkout_year": checkout_year,
            "rate_plan_type": booking_request.rate_plan_meta.get(
                'tag',
                'Refundable') if booking_request.rate_plan_meta else "Refundable",
            "checkin_day": checkinDay,
            "checkout_day": checkoutDay,
            'pah': pah,
            "special_rates": special_rates,
            "hotel_policies": hotel_policies}
        return context

    def __populateDesktopPdfData(self, booking_request):
        # type: (BookingRequest) -> object
        booking_request.guestMobile = booking_request.phone[:4] + "-" + \
            booking_request.phone[4:7] + "-" + booking_request.phone[7:]
        booking_code = booking_request.order_id
        discount_amount = 0
        if booking_request.voucher_amount:
            discount_amount = booking_request.voucher_amount
        else:
            discount_amount = booking_request.discount_value

        if booking_request.member_discount:
            discount_amount = round(
                float(discount_amount) +
                float(
                    booking_request.member_discount),
                2)

        booking_date = date_time_utils.build_date(booking_request.created_at)
        paymentText = "Payment Done"
        if "Not Paid" in booking_request.payment_mode:
            paymentText = "Pay at Hotel"
        if booking_request.is_audit:
            paymentText = "Payment Done"

        return booking_code, booking_date, discount_amount, paymentText

    def __getDesktopImageUrl(self, hotel):
        """
        :param hotel dbcommon.models.hotel.Hotel
        :rtype: object
        """
        image_url = None
        try:
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            for image in hotel_repository.get_image_set_for_hotel(hotel):
                if image.is_showcased:
                    image_url = image.url

        except Exception as exc:
            logger.exception("Exception on __getDesktopImageUrl = %s" % exc)
        return image_url

    @log_args(logger)
    def getMobileData(self, request, args, kwargs):
        return self.getData(request, args, kwargs)

    def __getDirectionFromLandmark(self, hotel_id):
        directions_repository = RepositoriesFactory.get_directions_repository()
        directions_obj = directions_repository.get_all_directions_for_hotel(
            hotel_id=hotel_id)
        for obj in directions_obj:
            text = obj.directions_for_email_website
            text_list = text.split("\\n")
            obj.text = text_list
            logger.debug("Direction Text = %s" % obj.text)
        return directions_obj
