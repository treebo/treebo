# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0052_city_enable_for_seo'),
    ]

    operations = [
        migrations.RenameField(
            model_name='locality',
            old_name='no_of_hotels_to_show',
            new_name='distance_cap',
        ),
        migrations.RenameField(
            model_name='searchlandmark',
            old_name='no_of_hotels_to_show',
            new_name='distance_cap',
        ),
    ]
