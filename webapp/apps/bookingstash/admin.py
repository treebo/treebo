from django.contrib import admin
from django.core.urlresolvers import reverse

from apps.bookingstash.models import ReservationBooking, ReservationBookingStay, \
    Availability, HotelRoomMapper, ReservationRestriction

from dbcommon.admin import ReadOnlyAdminMixin


@admin.register(ReservationBooking)
class ReservationBookingAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'booking_id',
        'group_code',
        'hotel_code',
        'guest_email',
        'group_email',
        'checkin_date',
        'checkout_date',
        'source',
        'status')
    search_fields = ('guest_email', 'booking_id')
    list_filter = ('source', 'hotel_code')


@admin.register(ReservationBookingStay)
class ReservationBookingStayAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'reservation',
        'from_date',
        'to_date',
        'room_type_code')


@admin.register(Availability)
class AvailabilityAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'room', 'get_room_id', 'availablerooms', 'date')
    search_fields = ('room__hotel__name',)
    date_hierarchy = 'date'

    def get_room_id(self, obj):
        room_link = reverse('admin:dbcommon_room_change', args=[obj.room.id])
        return '<a href="%s" target="_blank">%s</a>' % (room_link, obj.room.id)

    get_room_id.allow_tags = True
    get_room_id.short_description = 'Room ID'


@admin.register(HotelRoomMapper)
class HotelRoomMapperAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'room_id',
        'hotelogix_name',
        'room_type',
        'mm_hotel_code',
        'mm_room_code')
    search_fields = ('room_id', 'mm_hotel_code')
    ordering = ('room_id',)


@admin.register(ReservationRestriction)
class ReservationRestrictionAdmin(admin.ModelAdmin):
    list_display = (
        'created_at',
        'modified_at',
        'restriction_code',
        'hotel_code',
        'start_date',
        'end_date',
        'room_type_code',
        'room_number',
        'restriction_status')
    search_fields = ('hotel_code',)
    list_filter = ('restriction_status', 'room_type_code')
    date_hierarchy = 'end_date'
