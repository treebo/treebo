# pylint: disable-msg=no-member

import json
import logging
from http import HTTPStatus

from django.conf import settings
from rest_framework import serializers

from apps.checkout.models import BookingRequest
from apps.checkout.service.itinerary_handler_v2 import ItineraryHandlerV2
from apps.common import date_time_utils
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import RoomNotAvailableException, \
    HotelNotFoundException
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.discounts.services.promotion_service import PromotionService
from apps.featuregate.manager import FeatureManager
from apps.hotels.service.hotel_policy_service import HotelPolicyService
from apps.hotels.service.hotel_service import HotelService
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from apps.intelligent_pah.services.pah_services import PAHEnable
from apps.pages.data_model import ItineraryPageData
from apps.pages.helper import PageHelper
from apps.pricing.dateutils import date_to_ymd_str
from apps.profiles import utils
from base import log_args
from base.decorator.request_validator import validate_request
from base.views import validators
from base.views.api_v2 import TreeboAPIView2
from common.services.feature_toggle_api import FeatureToggleAPI
from data_services.exceptions import HotelDoesNotExist
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.data_services.external_provider_room_service import ExternalProvideRoomService
from webapp.apps.bookings.services.booking_utils import get_room_codes

logger = logging.getLogger(__name__)


class ItineraryPageSerializer(serializers.Serializer):
    hotel_id = serializers.IntegerField()
    checkin = serializers.DateField(validators=[validators.validate_date_of_request])
    checkout = serializers.DateField(validators=[validators.validate_date_of_request])
    roomconfig = serializers.CharField(max_length=100, validators=[validators.validate_roomconfig])
    apply_wallet = serializers.BooleanField(default=settings.APPLY_WALLET, required=False)
    rateplan = serializers.ChoiceField(choices=BookingRequest.RATE_PLAN_CHOICES)
    roomtype = serializers.CharField(max_length=100)
    utm_source = serializers.CharField(max_length=100, default=None, allow_null=True,
                                       allow_blank=True)
    channel = serializers.CharField(max_length=100, default=None, allow_null=True, allow_blank=True)


class ItineraryPageAPI(TreeboAPIView2):
    __pagename = 'itinerary'
    checkout_service = ItineraryHandlerV2()
    hotel_service = HotelService()
    hotel_repository = RepositoriesFactory.get_hotel_repository()
    promotion_service = PromotionService()

    @validate_request(ItineraryPageSerializer)
    def get(self, request, *args, **kwargs):
        """
        Gets the data for itinerary page.
        hotel_id -- Regular hotel id (not hashed value).
        checkin -- Check In Date in YYYY-MM-DD format.
        checkout -- Check Out Date in YYYY-MM-DD format.
        roomconfig -- Room Configuration as 1-0,2-1,3-1. Individual configurations seperated by commas in the string.
        """
        try:
            context = self.__get_itinerary_data(request, *args, **kwargs)
            response = api_response.prep_success_response(context)
            return response

        except RoomNotAvailableException as err:
            response = api_response.treebo_exception_error_response(err)
            return response

        except HotelNotFoundException as err:
            logger.exception("Hotel Not Active %s", err)
            response = api_response.treebo_exception_error_response(err)
            return response
        except Exception as exception:
            logger.exception("Exception occurred in ItineraryPageAPI v7 for request %s",
                             str(request.GET))
            slack_alert.send_slack_alert_for_exceptions(status=HTTPStatus.INTERNAL_SERVER_ERROR,
                                                        request_param=request.GET,
                                                        dev_msg="ItineraryPageAPI v7",
                                                        message=exception.__str__(),
                                                        class_name=self.__class__.__name__)

            response = api_response. \
                internal_error_response(messages=exception.__str__(),
                                        api_name=self.__class__,
                                        developer_message="Exception occurred in ItineraryPageAPI v7")
            return response

    @log_args(logger)
    def __get_itinerary_data(self, request, *args, **kwargs):

        context, booking_request = self.get_itinerary_data(request, *args, **kwargs)
        self.post_process_context(request, context, booking_request)
        return context

    def get_itinerary_data(self, request, *args, **kwargs):  # pylint: disable-msg=R0914,R0915
        data = request.GET.dict()
        country_code = data.get('country_code', '91')
        logger.info("Itinerary page data: %s", json.dumps(data))
        page_data = ItineraryPageData(**data)
        try:
            hotel = self.hotel_repository.get_hotel_by_id_from_web_or_raise(
                hotel_id=page_data.hotel_id)
            if hotel.status == 0:
                raise HotelNotFoundException("hotel %s not active " % page_data.hotel_id)
            # hotel = hotel_repository.filter_hotels(id=booking_request.hotel_id)[0]
        except HotelDoesNotExist:
            raise HotelNotFoundException(
                "hotel %s doesn't exits " %
                page_data.hotel_id)
        room_types = self.hotel_service.get_room_types_for_hotel(
            page_data.hotel_id)
        if not [room_type for room_type in room_types if
                page_data.room_type.lower() in room_type.lower()]:
            raise RoomNotAvailableException(
                'room type %s not available' %
                (page_data.room_type.lower()))

        if getattr(page_data, 'coupon_code', None):
            page_data.coupon_code = page_data.coupon_code.upper()
            logger.info("coupon code recieved %s", page_data.coupon_code)

        booking_request = self.checkout_service.get_or_update_booking_request(
            request, page_data)
        checkin_date_string, checkout_date_string = date_to_ymd_str(
            booking_request.checkin_date), date_to_ymd_str(
            booking_request.checkout_date)
        adults, children, room_count = PageHelper.parseRoomConfig(
            booking_request.room_config)

        days = date_time_utils.get_day_difference(checkin_date_string, checkout_date_string)

        room_config_array = PageHelper.getRoomConfigData(
            booking_request.room_config)

        is_call_center = bool(
            request.user.is_authenticated() and utils.get_customer_care(request.user.id))

        email, country_code, mobile, name = self.__get_booking_user_details(request,
                                                                            country_code=country_code)

        hotel_showcased_image_url = self.hotel_repository.get_showcased_image_url(
            hotel)
        is_otp_enabled = FeatureToggleAPI.is_enabled(
            "booking", "otp_verification_required", False)
        policies = HotelPolicyService.get_hotel_policies(hotel.id)

        context = {
            "page_name": "Itinerary",
            "number_of_nights": days,
            "channel": booking_request.booking_channel,
            "date": {
                "checkin": checkin_date_string,
                "checkout": checkout_date_string
            },
            "is_callcenter": is_call_center,
            "room": {
                "count": room_count,
                "config": room_config_array,
                "type": booking_request.room_type
            },
            "hotel": {
                "id": hotel.id,
                "name": HotelService().get_display_name(hotel),
                "street": hotel.street,
                "locality": hotel.locality.name,
                "city": hotel.city.name,
                "city_id": hotel.city.id,
                "state": hotel.state.name,
                "pincode": hotel.locality.pincode,
                "image_url": get_image_uncode_url(hotel_showcased_image_url),
                "coordinates": {
                    "lat": hotel.latitude,
                    "lng": hotel.longitude
                },
                "hotel_policies": policies,
                "property": {
                    "provider": hotel.provider_name,
                    "type": hotel.property_type,
                }
            },
            "pay_at_hotel": True,
            "ask_for_advance": False,
            "guest": {
                "name": name,
                "email": email,
                "country_code": country_code,
                "mobile": mobile,
                "adults": adults,
                "children": children
            },
            "comments": "",
            'razorpay_keys': settings.RAZORPAY_KEYS,
            "bid": booking_request.id,
            "isOtpEnabled": is_otp_enabled
        }
        ExternalProvideRoomService.add_external_room_details_to_room_object(hotel_id=hotel.id,
                                                                            room_dict=context[
                                                                                "room"],
                                                                            room_type=
                                                                            context["room"]["type"])

        page_data_dict = page_data.__dict__
        hotel_id = page_data_dict['hotel_id']
        cs_web_id_map = {}
        for hotel in self.hotel_repository.get_hotels_for_id_list([hotel_id]):
            if hotel.cs_id is not None:
                cs_web_id_map[hotel.cs_id.strip()] = hotel.id

        room_codes = get_room_codes([booking_request.room_type])
        request_skus = self.checkout_service.sku_request_service.get_web_request_skus(
            page_data_dict['room_config'], list(cs_web_id_map.keys()), room_codes)

        availability_request_data = self.checkout_service.get_availability_request_data(
            page_data_dict,
            request_skus,
            cs_web_id_map)
        pricing_request_data = self.checkout_service.get_pricing_request_data(page_data_dict,
                                                                              request_skus,
                                                                              cs_web_id_map)

        price = self.checkout_service.get_price(request, data, availability_request_data,
                                                pricing_request_data, True,
                                                booking_request=booking_request)

        payment_offer_coupon_code = None
        if price["selected_rate_plan"].get("payment_offer_price"):
            payment_offer_coupon_code = price["selected_rate_plan"]["payment_offer_price"][
                "coupon_code"]
        coupon_code = price["selected_rate_plan"]["price"]["coupon_code"]
        self.checkout_service.update_booking_request_with_prices(
            booking_request,
            price["selected_rate_plan"],
            request.user,
            coupon_code=coupon_code,
            rate_plan_detail=price["selected_rate_plan"]["rate_plan"],
            wallet_info=price["wallet_info"],
            payment_offer=price.get('payment_offer')
        )
        self.checkout_service.add_committed_price_for_booking_request(
            booking_request,
            price["selected_rate_plan"]["price"]
        )
        context["coupon_code"] = payment_offer_coupon_code or coupon_code
        context["coupon_applied"] = bool(coupon_code)

        context.update({
            "price": price
        })

        return context, booking_request

    def post_process_context(self, request, context, booking_request):
        utm_source = str(request.COOKIES.get('utm_source')).lower()
        utm_medium = str(request.COOKIES.get('utm_medium')).lower()

        feature_manager = FeatureManager(
            self.__pagename,
            context['date']['checkin'],
            context['date']['checkout'],
            context['hotel']['id'],
            booking_request.total_amount,
            context["hotel"]["locality"],
            context['hotel']['city_id'],
            utm_source,
            utm_medium)
        over_ridden_values = feature_manager.get_overriding_values()

        context['pah_enabled'] = True
        context['paynow_enabled'] = True
        hotel_id = context['hotel']['id']
        bid = context['bid']
        for overridden_key in list(over_ridden_values.keys()):
            context[overridden_key] = over_ridden_values[overridden_key]
        logger.info(
            "Feature gate return PAHEnable as %s for hotel id %s", context['pah_enabled'], hotel_id)
        is_pah_enabled, pah_message = PAHEnable().pay_at_hotel_status(
            check_in=context['date']['checkin'],
            check_out=context['date']['checkout'],
            room_count=context['room']['count'],
            hotel_id=hotel_id, bid=bid,
            channel=context['channel'],
            feature_gate_override=context['pah_enabled'])
        context['pah_enabled'] = is_pah_enabled
        context['pah_message'] = pah_message
        logger.info("The value for PAHEnable is %s for hotel id : %s", context['pah_enabled'],
                    hotel_id)
        context['coupon_desc'] = ""
        return context

    def __get_booking_user_details(self, request, country_code='91'):
        logger.debug('Getting Booking User Details')
        email = mobile = name = None
        if request.user.is_authenticated():
            name = request.user.first_name
            email = request.user.email
            mobile = request.user.phone_number
            country_code = country_code
        return email, country_code, mobile, name
