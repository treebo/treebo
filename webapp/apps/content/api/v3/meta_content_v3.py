from apps.content.meta_cache_service import MetaCacheService
from apps.content.constants import DEFAULT_TA_REVIEW_COUNT

__author__ = 'ansilkareem'

import logging
from rest_framework.response import Response

from base.views.api import TreeboAPIView
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class MetaContentAPIV3(TreeboAPIView):

    def get(self, request, *args, **kwargs):

        try:
            page_name = request.GET.get('page')
            query = request.GET.get('q')
            is_amp = request.GET.get('is_amp')
            ta_review_count = int(request.GET.get('review_count', DEFAULT_TA_REVIEW_COUNT))
            if is_amp and is_amp.lower() == 'true':
                is_amp = True
            else:
                is_amp = False
            search_dict = dict()
            if page_name == 'search':
                search_dict['landmark'] = request.GET.get('landmark')
                search_dict['locality'] = request.GET.get('locality')
                search_dict['city'] = request.GET.get('city')
                search_dict['hd'] = request.GET.get('hd')

            meta_content = MetaCacheService(page_name, query, ta_review_count,
                                            is_amp, search_dict).get_meta_data_v3()
            return Response(meta_content)

        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            return Response(status=500,
                            data={
                                'msg': e.__str__()
                            })
