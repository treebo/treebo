
let treejax = require('./treebolibs/treejax');
let $ = require('jquery');
let queryString = require('query-string');
let cookies = require('js-cookie');
let _ = require('underscore');
let parsed = queryString.parse(location.search);

// import Promise from 'bluebird';

let $loader = $("#commonLoader");

let googleDefer = $.Deferred();
let fbDefer = $.Deferred();


const auth = {
    login(email, password) {
        let $loginLoader = $(".js-login-loader");
        $loginLoader.removeClass("hide");

        const p = treejax({
                url: '/api/v1/auth/login/',
                type: 'POST',
                data: {
                 email,
                 password,
                 csrfmiddlewaretoken:$('input[name="csrfmiddlewaretoken"]').val()
                }
            });

            p.finally(() => {
                $loginLoader.addClass("hide");
            });
            return p;
    },
    signup(data) {
        let $signupLoader = $(".js-signup-loader");
        const self = this;
        $signupLoader.removeClass("hide");
        const p = treejax({
                url: '/api/v1/auth/register/',
                type: 'POST',
                data: {
                    name: data.name,
                    mobile: data.mobile,
                    email: data.email,
                    password: data.password,
                    referral: data.referral,
                    csrfmiddlewaretoken:$('input[name="csrfmiddlewaretoken"]').val(),
                    page: rt.page
                }
            }).then(data => {
                self.setSignupChannelCookie('EMAIL');
                rt.userId = data.userId;
                self.alias();
                return data;
            });
            p.finally(() => {
                $signupLoader.addClass("hide");
            });
            return p;
    },
    forgotpassword(email, reject) {
        var reject = reject || function (treeboError) { console.log(treeboError); }
        let $fpLoader = $(".js-fp-loader");
        $fpLoader.removeClass("hide");

        const p = treejax({
                url: '/api/v1/auth/forgot-password/',
                type: 'POST',
                data: {
                    email,
                    csrfmiddlewaretoken:$('input[name="csrfmiddlewaretoken"]').val()
                }
            }).then((data, status, xhr) => {
                $('.forgot__error').html(data.msg);
            },reject);

            p.finally(() => {
                $fpLoader.addClass("hide");
            });
        return p;
    },
	attemptLogin(response) {
        if (response.authResponse)
        {
            const accessToken = response.authResponse.accessToken; //get access token
            const userId = response.authResponse.userID; //get FB UID
            let imageUrl;
            const self = this;
            FB.api(`/${userId}/picture?type=large`, response => {
                imageUrl = response.data.url;
                FB.api('/me?fields=name,email,gender', response => {
                    self.sendRegistrationDataToServer(imageUrl, response, accessToken)
                });
            });
		}else {

        }
    },
    sendRegistrationDataToServer(imageUrl, response, accessToken) {
        const userEmail = response.email; //get user email
        let p ='';
        const self = this;
        if (userEmail) {
            $loader.removeClass("hide");
            p = treejax({
                type: 'POST',
                url: '/api/v1/auth/fbLogin/',
                data: {
                    imageUrl,
                    email: userEmail,
                    accessToken,
                    page: rt.page
                },
            }).then(data => {
                if(data.register) {
                    self.setSignupChannelCookie('FB');
                    rt.userId = data.userId;
                    self.alias();
                }
                fbDefer.resolve(data);
            });

            p.finally(() => {
                $loader.addClass('hide');
            });

        }
        else {
            // TODO: Show some response if permission denied
        }
    },
    sendGoogleRegistrationDataToServer(authToken) {
        const self = this;
        $loader.removeClass('hide');

        const p = treejax({
            type: 'POST',
            url: '/api/v1/auth/googleLogin/',
            data: {
                auth_token: authToken,
                page: rt.page
            },
        }).then((data) => {
            // analytics.track('e_cs_resp#fb_signin_auth_success', {success_code: data['msg']});
            if (data.register) {
                self.setSignupChannelCookie('GOOGLE');
                rt.userId = data.userId;
                self.alias();
            }
            return data;
        });

        p.finally(() => {
            $loader.addClass('hide');
        });

        return p;

    },
    fbLogin() {
        FB.login(_.bind(this.attemptLogin,this), {scope: 'email', auth_type: 'rerequest'});
        return fbDefer.promise();
    },
    loadFB() {
    	window.fbAsyncInit = function () {
		    FB.init({
		        appId: window.rt.fbAppId, //localhost app id
		        oauth: true,
		        status: true, // check login status
		        cookie: true, // enable cookies to allow the server to access the session
		        xfbml: true, // parse XFBML
		        version: 'v2.5'
		    });
		};
		// Load the SDK asynchronously
		(function (d, s, id) {
            let js;
            let fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(window.document, 'script', 'facebook-jssdk'));
    },
    loadGoogle() {
        window.initGoogleSignIn = function() {
            gapi.load('auth2', () => {
                const auth2 = gapi.auth2.init({
                    cookie_policy: 'single_host_origin',
                    requestvisibleactions: 'http://schemas.google.com/AddActivity',
                });
                googleDefer.resolve(auth2);
            });
        };
    },
    getGoogleHandler() {
        return googleDefer.promise();
    },
    bindEvents() {

    },
    alias() {
        rt.clientAnalyticsObj.alias(rt.userId);
    },
    setSignupChannelCookie(channel) {
        cookies.set('user_signup_channel',channel);
        cookies.set('user_signup_date',new Date());
    }
};

auth.loadFB();
auth.loadGoogle();

module.exports = auth;
