# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('discounts', '0007_auto_20160526_1110'),
    ]

    operations = [
        migrations.AddField(
            model_name='discountcoupon',
            name='price_component_applicable',
            field=models.CharField(default=b'Sale Price', max_length=50,
                                   choices=[(b'Sale Price', b'Sale Price'),
                                            (b'Rack Rate', b'Rack Rate')]),
        ),
    ]
