import json
import logging

from django.http import JsonResponse
from rest_framework import reverse

from apps.hotels.api.v1.get_hotels import HotelInformation
from apps.hotels.api.v1.get_landing_images import BannerInformation
from apps.hotels.serializers.city_serializer import CitySerializer
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.location import City
from data_services.city_repository import CityRepository

logger = logging.getLogger(__name__)

city_data_service = RepositoriesFactory.get_city_repository()


class CityInformation(TreeboAPIView):
    def get(self, request, hashedCityId="", format=None):
        cityInformation, message = CityInformation.getCities()
        response = self.__generate_response(request, cityInformation, message)
        return JsonResponse(response)

    @staticmethod
    def getCities():
        try:
            city_list = city_data_service.filter_cities(status=1)
            cityInformation = CitySerializer(city_list, many=True).data

            for city in cityInformation:
                city['alt'] = ""
            message = "success"
            return cityInformation, message
        except Exception as exc:
            message = "Not a valid City Object or No City Present in the List"
            logger.exception(exc)
            return [], message

    def __generate_response(self, request, cityInformation, message):
        response = {'message': message, 'status': 0}

        if message == 'success':
            response['cities'] = cityInformation
            response['city_count'] = len(cityInformation)
            response['status'] = 1

        response['hotel_count'] = HotelInformation.getNumberOfHotels()
        response['find_treebo_link'] = reverse.reverse(
            'pages:index', request=request)
        response['landing_banner'] = BannerInformation.getBannerInformation()

        return response
