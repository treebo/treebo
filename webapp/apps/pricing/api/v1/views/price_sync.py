import logging
from datetime import datetime, timedelta
from django.db.models import Max
from rest_framework.response import Response

from apps.pricing.event_publishers import publish_price
from apps.pricing.models import RoomPriceV2
from apps.pricing.tasks import sync_prices
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class PriceSyncTrigger(TreeboAPIView):

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        hotel_id = request.GET.get("hotel_id")
        start_date = request.GET.get("start_date")
        end_date = request.GET.get("end_date")
        if not hotel_id:
            return Response(
                {"message": "Missing hotel_id query parameter."}, status=400)
        elif not start_date or not end_date:
            return Response(
                {"message": "Missing start_date or end_date query parameter."}, status=400)
        sync_prices.delay(hotel_id, start_date, end_date)
        return Response({"message": "RoomPrice sync initiated."}, status=202)
