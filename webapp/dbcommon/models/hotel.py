import urllib.request
import urllib.parse
import urllib.error
import uuid

import requests

from django.apps import apps
from django.core.exceptions import ValidationError
from caching.base import CachingMixin
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.text import slugify
from djutil.models import TimeStampedModel
from django.db.models import UUIDField, Case, When, IntegerField
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.search.constants import DIRECT
from common.constants import common_constants as const
from dbcommon.constants import MAX_NO_OF_IMAGES_ACROSS_CATEGORIES
from apps.hotels.constants import RoomType
from dbcommon.models.default_permissions import DefaultPermissions
from django.conf import settings
from dbcommon.models.location import City, Locality, State
from dbcommon.managers.hotel_manager import HotelManager
from dbcommon.managers.hotel_manager import HotelManagerWithoutPrefetch
from django.db import transaction
from django.db.models import Value
import logging
import jsonfield

logger = logging.getLogger(__name__)


class Categories(CachingMixin, TimeStampedModel, DefaultPermissions):
    name = models.TextField(max_length=200)
    description = models.TextField(max_length=500, null=True, blank=True)
    status = models.IntegerField(
        default=const.ENABLED,
        choices=const.STATUS_CHOICES)
    enable_for_seo = models.BooleanField(default=False)

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    def get_sitemap_url(self, city_name):
        category_name = slugify(str(self.name))
        return '/' + category_name.lower() + '-in-' + city_name.lower() + '/'

    class Meta(DefaultPermissions.Meta):
        ordering = ['name']


class CSPropertyDetails(TimeStampedModel):
    hotelogix_id = models.CharField(max_length=100, null=False, unique=True)
    is_active = models.BooleanField()
    cs_id = models.CharField(max_length=200, unique=True)
    cs_data = jsonfield.JSONField()


class CSConsumerDetails(TimeStampedModel):
    cs_id = models.CharField(max_length=200)
    cs_data = jsonfield.JSONField()
    cs_route = models.CharField(max_length=200)
    cs_queue = models.CharField(max_length=200)
    cs_exchange = models.CharField(max_length=200)
    is_consumed = models.BooleanField(default=True)
    failure_message = models.CharField(max_length=1000, null=True)

    class Meta:
        unique_together = ('cs_id', 'cs_route',)


class Policy(CachingMixin, TimeStampedModel, DefaultPermissions):
    policy_type = models.CharField(max_length=200, null=False, blank=False)
    title = models.CharField(max_length=200, null=False, blank=False)
    description = models.CharField(max_length=700, null=False, blank=False)
    display_in_need_to_know = models.BooleanField(default=False)
    display_in_policy = models.BooleanField(default=False)
    is_global = models.BooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_policy'
        verbose_name = 'Policy'
        verbose_name_plural = 'Policies'
        ordering = ['id']

    def __unicode__(self):  # __unicode__ on Python 2
        return '{0}'.format(self.title)

    def __str__(self):
        return '{0}'.format(self.title)


class Hotel(CachingMixin, TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200, default="")
    hotelogix_id = models.CharField(max_length=200, null=True, blank=True)
    hotelogix_name = models.CharField(max_length=200, null=True, blank=True)
    tagline = models.CharField(max_length=200)
    description = models.TextField(max_length=10000, null=True, blank=True)
    phone_number = models.CharField(max_length=20)
    room_count = models.PositiveIntegerField(default=1)
    checkin_time = models.TimeField('checkin Time')
    checkout_time = models.TimeField('checkout Time')
    # Earlier there was no arg. Assuming that no arg passed meant do nothing
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    locality = models.ForeignKey(Locality, on_delete=models.DO_NOTHING)
    street = models.CharField(max_length=200)
    landmark = models.CharField(max_length=200, blank=True)
    latitude = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        null=True,
        blank=True,
        default=12.9667)
    longitude = models.DecimalField(
        max_digits=20,
        decimal_places=10,
        null=True,
        blank=True,
        default=77.5667)
    price_increment = models.DecimalField(max_digits=8, decimal_places=2)
    DISABLED = const.DISABLED
    ENABLED = const.ENABLED
    STATUS_CHOICES = const.STATUS_CHOICES
    status = models.IntegerField(default=DISABLED, choices=STATUS_CHOICES)
    state = models.ForeignKey(
        State,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True)
    consumer_secret = models.CharField(max_length=100, null=True, blank=True)
    consumer_key = models.CharField(max_length=100, null=True, blank=True)
    uuid = UUIDField(default=uuid.uuid4, null=True, blank=True)
    cs_id = models.CharField(max_length=200, null=True, blank=True)
    enabled_hotel_objects = HotelManager()
    objects = enabled_hotel_objects
    all_hotel_objects = models.Manager()
    category = models.ManyToManyField(Categories, null=True, blank=True)
    policies = models.ManyToManyField(
        Policy, through='HotelPolicyMap', null=True, blank=True)
    forward_reference_objects = HotelManagerWithoutPrefetch()
    distance = None
    property_type = models.CharField(max_length=100, default='hotel')
    provider_name = models.CharField(max_length=100, default='treebo')
    hygiene_shield_name = models.CharField(max_length=100, null=True, blank=True)
    onelink_urls = jsonfield.JSONField(null=True, blank=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_hotel'
        verbose_name = 'Hotel'
        verbose_name_plural = 'Hotels'
        ordering = ['name']

    def __unicode__(self):  # __unicode__ on Python 2
        return '{0}({1})'.format(self.name, self.hotelogix_id)

    def __repr__(self):
        return '{0}({1})'.format(self.name, self.hotelogix_id)

    def __str__(self):
        return '{0}({1})'.format(self.name, self.hotelogix_id)

    def get_showcased_image_url(self):
        showcase_image = self.image_set.filter(is_showcased=True).first()
        if showcase_image is not None:
            return showcase_image.url
        image = self.image_set.first()
        if image:
            return image.url
        logger.error("Could not find an image for hotel %s", self.name)
        return None

    def reset_is_showcased_images(self):
        # for a hotel is_showcased image is determined by
        # if image belongs to ImagesPerCategory that is
        # selected for the hotel and if position falls in
        # no of images to showcase from the category

        # used get_model to avoid cyclic loop
        ImagesPerCategory = apps.get_model('dbcommon', 'ImagesPerCategory')
        Image = apps.get_model('dbcommon', 'Image')
        images = ImagesPerCategory.get_images_by_category(self)

        # if no images found return False
        if not images:
            return False

        # No of images found associated with category
        # should be equal to MAX_NO_OF_IMAGES_ACROSS_CATEGORIES
        images_to_showcased_ids = [image.id for image in images]
        if len(images_to_showcased_ids) != MAX_NO_OF_IMAGES_ACROSS_CATEGORIES:
            return False

        Image.objects.filter(hotel=self, is_showcased=True).update(is_showcased=False)
        Image.objects.filter(id__in=images_to_showcased_ids,
                             hotel=self).update(is_showcased=True)
        return True

    def get_slugified_url(self):
        return '/hotels-in-' + slugify(self.city.name) + '/' \
                + slugify(self.name) + '-' + str(self.id)

    @property
    def get_location_url(self):
        url = '//maps.google.com/?' + urllib.parse.urlencode(
            {'q': str(self.latitude) + ',' + str(self.longitude)})
        return url

    def get_consumer_key(self):
        return self.consumer_key if self.consumer_key else settings.CONSUMER_KEY

    def get_consumer_secret(self):
        return self.consumer_secret if self.consumer_secret else settings.CONSUMER_SECRET

    def address(self):
        address = self.street + ", " + self.locality.name + ", " + self.get_city_name()
        return address

    def get_city_name(self):
        city_name = ''
        try:
            city = City.all_cities.get(hotel=self)
            city_name = city.name
        except City.DoesNotExist:
            logger.error("city not found for hotel %s ", self)
        return city_name

    def get_sitemap_url(self):
        # FIXME: Importing locally to avoid the circular dependency
        from apps.hotels.utils import generate_hash
        city_string = slugify(self.city.slug)
        kwargs = {'cityString': city_string}
        locality = Locality.objects.get(id=self.locality_id)
        return reverse('pages:hotels_in_city', kwargs=kwargs) + "/" + slugify(
            self.name) + "-" + slugify(locality.name) + "-" + str(self.id) + "/"

    def get_hotel_canonical_url(self):
        """
        Get the canonical url for the hotel using the city and locality.
        Generates a url like '/hotels-in-bangalore-treebo-elmas-koramangala-2/'
        :return:
        """
        hyphen_separated_name = slugify(self.name)
        hyphen_separated_locality = slugify(self.locality.name)
        hotel_part = hyphen_separated_name + '-' + \
            hyphen_separated_locality + '-' + str(self.id)
        hyphen_separated_city = slugify(self.city.slug)
        city_part = 'hotels-in-' + hyphen_separated_city
        url_string = city_part + '/' + hotel_part + '/'
        return url_string

    def get_categorised_images(self):
        ImagesPerCategory = apps.get_model('dbcommon', 'ImagesPerCategory')
        Image = apps.get_model('dbcommon', 'Image')
        categories = ImagesPerCategory.objects.filter(hotel=self).order_by('ranking')
        ordering = [Case(*[When(room__room_type_code=room_type, then=order)
                           for room_type, order in RoomType.ROOM_TYPE_ORDER_API])]
        all_images = []
        for category in categories:
            if category.category in RoomType.ROOM_COLLECTIVE_CATEGORY:
                images = Image.objects.filter(category=category.category, hotel=self)\
                    .order_by('-is_showcased', *ordering, 'position')
            else:
                images = Image.objects.filter(category=category.category, hotel=self)\
                    .order_by('-is_showcased', 'position')
            allowed_image = images[0:category.no_of_images]
            all_images += list(allowed_image)
        if len(all_images) != MAX_NO_OF_IMAGES_ACROSS_CATEGORIES:
            all_images = list(Image.objects.filter(hotel=self))

        return all_images[0:MAX_NO_OF_IMAGES_ACROSS_CATEGORIES]

    def save(
            self,
            force_insert=False,
            force_update=False,
            using=None,
            update_fields=None):
        logger.debug("In Save method of hotel model %s", self.hotelogix_id)
        if not self.cs_id:
            try:
                cs_property = CSPropertyDetails.objects.get(
                    hotelogix_id=self.hotelogix_id)
                self.cs_id = cs_property.cs_id
            except CSPropertyDetails.DoesNotExist as e:
                logger.info(
                    "There is no cs id for hotel %s",
                    self.hotelogix_id)
                self.status = const.DISABLED
        logger.info(
            "The CS ID of the hotel is %s and status is %s",
            self.cs_id,
            self.status)
        super(Hotel, self).save(force_insert=force_insert,
                                force_update=force_update,
                                using=using,
                                update_fields=update_fields)

    def clean(self):
        logger.debug('Clean method called on Hotel %s', self.hotelogix_id)
        if not self.cs_id and self.status == const.ENABLED:
            try:
                cs_property = CSPropertyDetails.objects.get(
                    hotelogix_id=self.hotelogix_id)
                self.cs_id = cs_property.cs_id
            except CSPropertyDetails.DoesNotExist as e:
                logger.info(
                    'Hotel %s was to be enabled even without cs id.. Returning',
                    self.hotelogix_id)
                raise ValidationError(
                    'Hotel Cannot be enabled without a corresponding valid Catalogue Service Hotel ID')
            self.cs_id = cs_property.cs_id
        logger.debug(
            "Hotel validation successful for hotel %s. Returning from clean",
            self.hotelogix_id)


class HotelPolicyMap(CachingMixin, TimeStampedModel, DefaultPermissions):
    hotel_id = models.ForeignKey(Hotel)
    policy_id = models.ForeignKey(Policy)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_hotel_policy_map'
        verbose_name = 'hotel_policy_map'
        verbose_name_plural = 'hotel_policy_map'
        ordering = ['id']

    def __str__(self):
        return super(HotelPolicyMap, self).__str__()


@receiver(
    post_save,
    sender=Hotel,
    dispatch_uid="Hotel disabled notification sent to prowl")
def notification_prowl(sender, instance, **kwargs):
    # TODO: Remove this try catch. Only for dev purposes
    try:
        if instance.status == const.DISABLED:
            url = settings.PROWL_HOTEL_DISABLE
            data = {
                "hotelogix_id": instance.hotelogix_id
            }
            response = requests.post(url, data=data)
    except Exception as e:
        logger.exception(e)


class HotelImage(Hotel):
    class Meta(DefaultPermissions.Meta):
        proxy = True


class Highlight(CachingMixin, TimeStampedModel, DefaultPermissions):
    description = models.CharField(max_length=300)
    hotel = models.ForeignKey(
        Hotel,
        on_delete=models.DO_NOTHING,
        related_name='hotel_highlights')

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_highlight'
        verbose_name = 'Highlight'
        verbose_name_plural = 'Highlights'

    def __unicode__(self):  # __unicode__ on Python 2
        return self.hotel.name + ' ' + self.description

    def __str__(self):  # __unicode__ on Python 2
        return self.hotel.name + ' ' + self.description


class SeoDetails(CachingMixin, TimeStampedModel, DefaultPermissions):
    hotel = models.ForeignKey(Hotel, on_delete=models.DO_NOTHING)
    titleDescription = models.CharField(
        max_length=200, db_column="title_description")
    metaContent = models.CharField(max_length=500, db_column="meta_content")
    hotel_canonical_url = models.CharField(
        max_length=500, blank=True, null=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_seodetails'
        verbose_name = 'SEO Details'
        verbose_name_plural = 'SEO Details'

    def update_hotel_canonical_url(self, hotel_ids=[]):
        if hotel_ids:
            all_seo_details_obj = SeoDetails.objects.filter(
                hotel_id__in=hotel_ids)
        else:
            all_seo_details_obj = SeoDetails.objects.all()
        with transaction.atomic():
            for seo_detail_obj in all_seo_details_obj:
                seo_detail_obj.hotel_canonical_url = seo_detail_obj.hotel.get_hotel_canonical_url()
                seo_detail_obj.save()

    def update_hotel_titles(self, hotel_ids=[]):
        if hotel_ids:
            all_seo_details_obj = SeoDetails.objects.select_related(
                'hotel').filter(hotel_id__in=hotel_ids)
        else:
            all_seo_details_obj = SeoDetails.objects.select_related(
                'hotel').all()
        with transaction.atomic():
            for seo_detail_obj in all_seo_details_obj:
                existing_title = seo_detail_obj.titleDescription
                seo_detail_obj.titleDescription = seo_detail_obj.hotel.name + ' | ' + existing_title
                seo_detail_obj.save()


class CitySeoDetails(CachingMixin, TimeStampedModel, DefaultPermissions):
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    citySeoTitleContent = models.CharField(
        max_length=500, db_column="city_seo_title")
    citySeoDescriptionContent = models.CharField(
        max_length=500, db_column="city_seo_description")
    citySeoKeywordContent = models.CharField(
        max_length=500, db_column="city_seo_keyword")

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_cityseodetails'
        verbose_name = 'City SEO Details'
        verbose_name_plural = 'City SEO Details'


class CategorySeoDetails(CachingMixin, TimeStampedModel, DefaultPermissions):
    category = models.ForeignKey(Categories, on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    category_seo_title = models.CharField(max_length=500)
    category_seo_description = models.TextField(null=True, blank=True)
    category_seo_keyword = models.TextField(null=True, blank=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_categoryseodetails'
        verbose_name = 'Cateogry SEO Details'
        verbose_name_plural = 'Cateogry SEO Details'

    def __str__(self):
        return '%s %s' % (self.category.name, self.category_seo_title)


class EnableCategory(CachingMixin, TimeStampedModel, DefaultPermissions):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    hotel_type = models.ManyToManyField(Categories, null=True, blank=True)


class HotelMetaData(CachingMixin, TimeStampedModel, DefaultPermissions):
    class Keys(object):
        B2B_BLACKLIST = 'b2b_blacklist'
        SORTED_VALUE = 'sorted_value'

        CHOICES = (B2B_BLACKLIST, SORTED_VALUE,)

        MODEL_CHOICES = ((x, x) for x in CHOICES)

        def __iter__(self):
            for elem in self.CHOICES:
                yield elem

    hotel = models.ForeignKey(
        Hotel,
        on_delete=models.DO_NOTHING,
        related_name='meta_data')
    key = models.CharField(max_length=100, choices=Keys.MODEL_CHOICES)
    value = models.IntegerField(null=False)
    channel = models.CharField(max_length=100, default=DIRECT)

    class Meta(DefaultPermissions.Meta):
        unique_together = ('hotel', 'key')
        verbose_name_plural = 'Hotel Meta data'

    @staticmethod
    def getHotelMetaDataInfo(hotel_ids, search_channel):
        return HotelMetaData.objects.filter(
            hotel_id__in=hotel_ids, channel=search_channel,
            key='sorted_value').order_by('value')


class PolicyConfiguration(CachingMixin, TimeStampedModel, DefaultPermissions):
    provider = models.CharField(max_length=200)
    enable_global_policy = models.BooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_policy_configuration'

    def __str__(self):
        return '%s %s' % (self.provider, self.enable_global_policy)


class CSHotelBrands(TimeStampedModel, DefaultPermissions):
    code = models.CharField(max_length=100, null=False, editable=False)
    color = models.CharField(max_length=100, null=False)
    display_name = models.CharField(max_length=100, null=False, editable=True)
    legal_name = models.CharField(max_length=100, null=False)
    name = models.CharField(max_length=100, null=False)
    short_description = models.CharField(max_length=100, null=False)
    logo = models.CharField(max_length=100, default='')
    status = models.CharField(max_length=100, default='NOT ACTIVE')

    def __unicode__(self):
        return str(self.display_name)

    def __str__(self):
        return super(CSHotelBrands, self).__str__()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_brands'
        ordering = ['id']


class HotelBrandMapping(TimeStampedModel, DefaultPermissions):
    hotel_id = models.ForeignKey(Hotel, on_delete=models.DO_NOTHING)
    cs_brand = models.ForeignKey(CSHotelBrands, on_delete=models.DO_NOTHING)

    def __unicode__(self):
        return str(self.hotel_id, self.cs_brand)

    def __str__(self):
        return super(HotelBrandMapping, self).__str__()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_brands_mapping'


class HotelAlias(TimeStampedModel, DefaultPermissions):
    hotel = models.ForeignKey(Hotel, related_name='hotel_alias', unique=True)
    name = models.CharField(max_length=200, default=None, unique=True)
    is_enabled = models.BooleanField(default=True, help_text=(
        "Mark this false if you don't want to replace display name with hotel name"))

    class Meta(DefaultPermissions.Meta):
        db_table = "hotels_hotel_alias"


class HotelAliasCounter(TimeStampedModel, DefaultPermissions):
    locality = models.CharField(max_length=200, default=None, unique=True)
    counter = models.IntegerField(null=False)

    class Meta(DefaultPermissions.Meta):
        db_table = "hotels_hotel_alias_counter"
