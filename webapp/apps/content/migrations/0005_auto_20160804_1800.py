# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0004_auto_20160523_0950'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contentstore',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Content Store',
                'verbose_name_plural': 'Content Stores'},
        ),
    ]
