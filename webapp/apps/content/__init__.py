__author__ = 'baljeet'

from django.apps import AppConfig

default_app_config = 'apps.content.ContentAppConfig'

class ContentAppConfig(AppConfig):

    name = 'apps.content'

    def ready(self):
        from . import signals
