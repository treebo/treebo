import logging

from rest_framework.response import Response
from rest_framework.views import APIView

from apps.third_party.service.trip_advisor.generate_trip_advisor_csv import GenerateTACSVService
from apps.third_party.service.trip_advisor.hotel_availability import HotelAvailabilityService
from base import log_args

logger = logging.getLogger(__name__)


class HotelAvailability(APIView):

    @log_args()
    def post(self, request):
        try:
            logger.info("request data %s", request.data)
            hotel_availability = HotelAvailabilityService()
            availability_response = hotel_availability.read_request_data(
                request.data)
            return availability_response
        except Exception as e:
            logger.exception(
                'Error occured in creating Hotel Availability file for trip adviosr')
            return Response({"success": False}, status=400)


class GenerateTACSV(APIView):
    def get(self, request, *args, **kwargs):
        try:
            logger.info("generating csv and mailing")
            generate_ta_csv = GenerateTACSVService()
            generate_ta_csv.create_and_mail_csv()
            return Response({"success": True}, status=200)
        except Exception:
            logger.exception(
                'Error occured in creating Hotel Availability file for trip adviosr')
            return Response({"success": False}, status=400)
