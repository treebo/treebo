import os

from django.http import HttpResponse


class FileUtils:
    @staticmethod
    def generate_xml_file(data, filename):
        data.write(
            filename,
            pretty_print=True,
            xml_declaration=True,
            encoding="UTF-8")
        query_message_file = open(filename, 'r')
        response = HttpResponse(query_message_file,
                                content_type="application/xml; charset=utf-8")
        response['Content-Disposition'] = 'attachment; filename=' + filename
        response['Content-Length'] = str(os.path.getsize(filename))
        return response
