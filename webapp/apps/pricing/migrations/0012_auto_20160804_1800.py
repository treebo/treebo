# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pricing', '0011_featuretoggle_environment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='channelmanagerrateplan',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Channel Manager Rate Plan',
                'verbose_name_plural': 'Channel Manager Rate Plans'},
        ),
        migrations.AlterModelOptions(
            name='featuretoggle',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Feature Toggle',
                'verbose_name_plural': 'Feature Toggles'},
        ),
        migrations.AlterModelOptions(
            name='luxurytax',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Luxury Tax',
                'verbose_name_plural': 'Luxury Taxes'},
        ),
        migrations.AlterModelOptions(
            name='preferredpromo',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Preferred Promo',
                'verbose_name_plural': 'Preferred Promos'},
        ),
        migrations.AlterModelOptions(
            name='promoconditions',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Promo Condition',
                'verbose_name_plural': 'Promo Conditions'},
        ),
        migrations.AlterModelOptions(
            name='roomprice',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Room Price',
                'verbose_name_plural': 'Room Prices'},
        ),
        migrations.AlterModelOptions(
            name='roompricev2',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Room Price V2',
                'verbose_name_plural': 'Room Prices V2'},
        ),
        migrations.AlterModelOptions(
            name='roomrateplan',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Room Rate Plan',
                'verbose_name_plural': 'Room Rate Plans'},
        ),
        migrations.AlterModelOptions(
            name='statetax',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'State Tax',
                'verbose_name_plural': 'State Taxes'},
        ),
    ]
