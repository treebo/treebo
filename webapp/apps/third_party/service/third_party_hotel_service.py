import collections
import datetime
import json
import logging
from datetime import timedelta

import requests
from django.conf import settings
from django.core.mail.message import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify

from common.constants import common_constants as const
from data_services.respositories_factory import RepositoriesFactory
from django.core.mail import send_mail
from apps.third_party.utm_source_mapping import UTM
from apps.third_party.exceptions import RoomDataParseError

logger = logging.getLogger(__name__)

TRIVAGO_API_VERSION = 4
TRIVAGO_API_LANGUAGE_DEFAULT = 'en_IN'
TRIVAGO_API_CURRENCY_DEFAULT = 'INR'


class ThirdPartyHotelService:
    @staticmethod
    def email_hotel_data_in_csv(url_prefix, to_email, source):
        try:
            csv_file = ThirdPartyHotelService.fetch_hotel_data_in_csv(url_prefix, source=source)
            ThirdPartyHotelService.send_csv_email(csv_file, to_email, source=source)
        except BaseException:
            logger.exception(source+'CsvHotelData failed')
            raise

    @staticmethod
    def fetch_hotel_data_in_csv(url_prefix, source='trivago'):
        third_party_hotel_list = []
        try:
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            hotels_id_list = hotel_repository.get_all_active_hotel_ids()
            hotels_id_list = sorted(hotels_id_list)
            logger.info("found hotels for %s %d ", source, len(hotels_id_list))
            url_len_exceed_count = 0
            email_body_message = ''
            for hotel_id in hotels_id_list:
                try:
                    hotel_detail = ThirdPartyHotelService.hotel_detail_fetch(
                        url_prefix, hotel_id, source=source)
                    third_party_hotel = ThirdPartyHotelService.build_third_party_hotel(
                        hotel_detail)

                    if len(hotel_detail['complete_address']) > 150:
                        # send mail if url length exceed 150 and continue
                        url_len_exceed_count += 1
                        email_body_message += ' <br/> Hotel Name : ' + str(third_party_hotel['name']) \
                                              + ' <br/> hotel_id : ' + str(hotel_id) \
                                              + ' <br/> Url : ' + str(third_party_hotel['url']) \
                                              + ' <br/> Address : ' + str(hotel_detail['complete_address']) \
                                              + ' <br/> <br/> ------------------ '

                        continue

                    third_party_hotel_csv = ThirdPartyHotelService.format_third_party_csv(source,
                                                                                          third_party_hotel,
                                                                                          hotel_detail)
                    third_party_hotel_list.append(third_party_hotel_csv)
                except Exception as e:
                    logger.exception(
                        " hotel %d , url_prefix %s %s failed %s ",
                        hotel_id,
                        url_prefix,
                        source,
                        str(e))
                    continue

            if url_len_exceed_count > 0:
                send_mail('<URGENT> Address for ' + str(
                    url_len_exceed_count) + ' hotel/hotels exceeded 150 characters length',
                          '',
                          settings.SERVER_EMAIL,
                          settings.TRIVAGO_URL_LIMIT_EXCEED_RECEIVER_EMAILS,
                          html_message=email_body_message
                          )
            csv_file = ThirdPartyHotelService.build_csv_file(source, third_party_hotel_list)
            return csv_file
        except Exception as e:
            raise Exception(source+'CsvHotelData failed ' + str(e))

    @staticmethod
    def format_third_party_csv(source, third_part_hotel, hotel_detail):
        return UTM[source]['csv_mapper'](third_part_hotel, hotel_detail)

    @staticmethod
    def build_csv_file(source, third_party_hotel_list):
        return UTM[source]['csv_file_creator'](third_party_hotel_list)

    @staticmethod
    def send_csv_email(csv_file, to_email, source='trivago'):
        try:
            mail_obj = EmailMultiAlternatives()
            mail_obj.subject = 'Reg Hotel data for '+UTM[source]['utm_source']
            mail_obj.body = 'Please find the attached hotel data csv'
            mail_obj.from_email = settings.SERVER_EMAIL
            mail_obj.to = [to_email]
            mail_obj.attach(
                UTM[source]['utm_source']+'_hotel_data.csv',
                csv_file.getvalue(),
                'text/csv')
            mail_obj.send()
            logger.info('message sent successfully')
        except BaseException:
            logger.exception('sending email failed for'+UTM[source]['utm_source']+'CsvHotelData')
            raise

    @staticmethod
    def hotel_detail_fetch(url_prefix, hotel_id, source='trivago'):
        try:
            today = datetime.date.today() + timedelta(days=1)
            tomorrow = today + timedelta(days=1)
            hotel_url_params = '?checkin={0}&checkout={1}&roomconfig={2}'
            hotel_hit_url = reverse(
                'hotels_v3:hotel-detail-v3',
                kwargs={
                    'hotel_id': hotel_id}) + hotel_url_params.format(
                today,
                tomorrow,
                '1-0')
            hotel_hit_url = url_prefix + hotel_hit_url
            logger.info(source+" hotel fetch url " + hotel_hit_url)
            response = requests.get(hotel_hit_url)
            hotel_response_data = response.json()
            hotel_detail = hotel_response_data['data']
            logger.info(hotel_detail)
            hotel_slug = slugify(hotel_detail['name'])
            locality_slug = slugify(hotel_detail['address']['locality'])
            city_slug = slugify(hotel_detail['address']['locality'])
            kwargs = {
                'city_slug': city_slug,
                'hotel_slug': hotel_slug,
                'locality_slug': locality_slug,
                'hotel_id': hotel_id}
            new_hotel_url = reverse(
                'pages:hotel-details-new',
                kwargs=kwargs) + '?utm_source='+UTM[source]['utm_source']+'&utm_medium=' + \
                UTM[source]['utm_medium']+'&utm_campaign='+UTM[source]['utm_campaign']
            hotel_detail['url'] = url_prefix + new_hotel_url
            address_list = ['street', 'city', 'state', 'pincode']
            hotel_detail['complete_address'] = ', '.join(
                [str(hotel_detail['address'][item]) for item in address_list])
        except BaseException:
            raise Exception(
                'fetching hotel data from API failed for '+source+'HotelService')

        return hotel_detail

    @staticmethod
    def build_third_party_hotel(hotel_detail):
        try:
            hotel_id = hotel_detail['id']
            hotel_url = hotel_detail['url']
            hotel_name = hotel_detail['name']
            hotel_street = hotel_detail['address']['street']
            hotel_city = hotel_detail['address']['city']
            hotel_pincode = hotel_detail['address']['pincode']
            hotel_state = hotel_detail['address']['state']
            hotel_country = 'INDIA'
            hotel_latitude = hotel_detail['coordinates']['lat']
            hotel_logitude = hotel_detail['coordinates']['lng']
            hotel_description = hotel_detail['description']
            hotel_amenities = [x['name'] for x in hotel_detail['facilities']]
            hotel_email = settings.FEEDBACK_SENDER
            hotel_phone = hotel_detail.get('phone_number', '9322800100')
            hotel_fax = const.CUSTOMER_CARE_NUMBER
            rooms_list = hotel_detail['rooms']
            hotel_rooms = ThirdPartyHotelService._parse_room_list(
                rooms_list, hotel_url, hotel_id)
            hotel_images = hotel_detail['images']

            third_party_hotel_params = (
                ('partner_reference', hotel_id), ('name', hotel_name), ('street', hotel_street),
                ('city', hotel_city), ('postal_code', hotel_pincode), ('state', hotel_state),
                ('country', hotel_country), ('latitude', hotel_latitude), ('longitude', hotel_logitude),
                ('desc', hotel_description), ('amenities', hotel_amenities), ('url', hotel_url),
                ('email', hotel_email), ('phone', hotel_phone), ('fax', hotel_fax), ('room_types', hotel_rooms),
                ('images', hotel_images))

            third_party_hotel_dict = collections.OrderedDict(third_party_hotel_params)
        except BaseException:
            logger.exception(
                'Building hotel data failed for ThirdPartyHotelService')
            raise

        return third_party_hotel_dict

    @staticmethod
    def _parse_room_list(rooms_list, hotel_url, hotel_id):
        room_detail_dict = {}
        try:
            room_repository = RepositoriesFactory.get_room_repository()
            rooms = room_repository.get_rooms_for_hotel(hotel_id=hotel_id)
            # rooms = room_repository.filter_rooms(hotel_id=hotel_id)
            rooms_desc_dict = {
                x.room_type_code.lower(): x.description for x in rooms}
            for room in rooms_list:
                room_type = room['type']
                room_desc = rooms_desc_dict[room_type.lower()]
                room_url = hotel_url + '?roomtype=' + room_type
                room_tuple = (('url', room_url), ('desc', room_desc), ('room_count', room['room_count']))
                room_dict = collections.OrderedDict(room_tuple)
                room_detail_dict[room_type] = room_dict
        except BaseException as e:
            logger.exception(
                'Parsing room data failed for ThirdPartyHotelService the room list is:{}'.format(json.dumps(rooms_list)))
            raise RoomDataParseError()

        return room_detail_dict
