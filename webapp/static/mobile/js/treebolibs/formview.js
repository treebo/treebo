/**
 * Created by varunachar on 19/10/15.
 *
 * Extend FormView instead of BaseView for all forms that required validation.
 *
 *
 *  Basic usage:
 *  let validationConfig = {
 *      'formSelector': 'form.feedback-form'
 *  };
 *
 *  Instantiate the FormView like below.
 *
 *  let feedbackConfig = {
 *      name: 'FeedbackView',
 *      widgets: [],
 *      selecter: 'body',
 *      eventConfig: eventConfig,
 *      validator: validationConfig
 *  };
 *  let feedbackView = new FeedbackView(feedbackConfig);
 *
 * 	submit() {
 *		if(!this.validate()){
 *			return;
 *		}	
 *	}
 *
 *  You can check if the form is valid by checking if(this.isValid)
 *
 * formSelector is the css selector used for find the form in the HTML.
 *
 * FormView uses the formSelector to automatically perform certain basic validations.
 * To activate validations, add the class .validate to the input and add one of the
 * following currently supported attributes
 *
 * 1. Required:- Add attr data-validation-required="true" to your input tag
 * 2. Min Length, Max Length :- Add attr data-validation-minLength="<value>" or data-validation-maxLength="<value>"
 * 3. Min Value, Max Value :- Add attr data-validation-minValue="<value>" or data-validation-maxValue="<value>"
 *
 * ***********************************
 *          Custom Validations
 * ***********************************
 * let validationConfig = {
 *       'formSelector': 'form.feedback-form',
 *      'validations' : {
 *        'field-selector-under-above-formSelector' : {
 *            fn : function(e) {
 *                if(valid) {
 *                    return true;
 *                }
 *                else {
 *                    return false;
 *                }
 *            },
 *            msg : 'Error message to display in case of validation failure'
 *         }
 *      }
 *  };
 *
 * Each entry in the 'validations' object above is used for running some custom validations
 * on form field, e.g. password and confirm password matching validation.
 *
 * Instantiate the FormView like below.
 *
 * let feedbackConfig = {
 *      name: 'FeedbackView',
 *      widgets: [],
 *      selecter: 'body',
 *      eventConfig: eventConfig,
 *      validator: validationConfig
 *  };
 *  let feedbackView = new FeedbackView(feedbackConfig);
 *
 */

let BaseView = require('./baseview'),
	l = require('./logger'),
	$ = require('jquery'),
	_ = require('underscore');


class FormView extends BaseView {

	constructor(opts) {
		super(opts);
		if (opts.validator) {
			this._checkValidationConfig(opts.validator);
			this.validator = opts.validator;
		}
	}

	/**
	 * Validate the form's validation configs
	 * @private
	 */
	_checkValidationConfig(validator) {
		if (!validator.formSelector) {
			throw new Error("No form selector specified.");
		}
		_.each(validator.validations, function (val, key) {
			if (!val) {
				throw new Error("No validation function provided for " + key);
			}
			if (!_.isFunction(val) && !val.fn) {
				throw new Error("No validation function provided for " + key);
			}
		}, this);
	}

	/**
	 * Validate if the form is valid or not. Binds the same validations to on blur event of inputs.
	 * @returns {boolean}
	 */
	validate() {
		// Defaults to valid. If any validation fails, it marks the complete form as invalid.
		this.isValid = true;
		if (!this._validationsBound) {
			this._bindFormValidations();
			this._validationsBound = true;
		}

		let $elements = $(this.validator.formSelector).find(".validate");
		_.each($elements, function (ele, index, list) {
			this._checkValidations(ele);
		}, this);

		let v = this.validator.validations;
		_.each(v, function (eventConfig, fieldSelector) {
			this._validateCustomEvent.call(this, eventConfig, fieldSelector);
		}, this);
		return this.isValid
	}

	/**
	 *
	 * @param eventConfig
	 * @param fieldSelector
	 * @private
	 */
	_validateCustomEvent(eventConfig, fieldSelector) {
		let isValid = true;
		let fn, msg;
		if (_.isFunction(eventConfig)) {
			fn = eventConfig;
			msg = 'Invalid value entered';
		}
		else {
			fn = eventConfig.fn;
			msg = eventConfig.msg;
		}
		let $e = $(this.validator.formSelector).find(fieldSelector);
		isValid = fn.call(this, $e);
		if (!isValid) {
			FormView._markInvalid($e, msg);
			this.isValid = false;
		}
		else {
			FormView._markValid($e);
		}

	}

	_checkValidations(ele) {
		let $e = $(ele);
		let isRequired = $e.attr('data-validation-required');
		if (isRequired) {
			if (!this._isValuePresent($e)) {
				FormView._markInvalid($e, 'Please enter a value for this field');
				this.isValid = false;
				return;
			}
			else {
				FormView._markValid($e);
			}
		}

		let val = $e.val();
		let minMax = this._getMinMax($e, 'data-validation-minLength', 'data-validation-maxLength');
		let minValue = minMax.min;
		let maxValue = minMax.max;

		if (minValue || maxValue) {
			let validate = this._isWithinRange(minValue, maxValue, val, "Length");
			if (validate !== true) {
				FormView._markInvalid($e, validate);
				this.isValid = false;
			}
			else {
				FormView._markValid($e);
			}
		}
		else {
			minMax = this._getMinMax($e, 'data-validation-minValue', 'data-validation-maxValue');
			minValue = minMax.min;
			maxValue = minMax.max;
			if (minValue || maxValue) {
				let validate = this._isWithinRange(minValue, maxValue, val, "Value");
				if (validate !== true) {
					FormView._markInvalid($e, validate);
					this.isValid = false;
				}
				else {
					FormView._markValid($e);
				}
			}
		}
	}

	/**
	 *
	 * @param $e
	 * @param minAttr
	 * @param maxAttr
	 * @returns {{min: *, max: *}}
	 * @private
	 */
	_getMinMax($e, minAttr, maxAttr) {
		let minValue = $e.attr(minAttr);
		let maxValue = $e.attr(maxAttr);

		if (!maxValue && !minValue) {
			return {};
		}
		try {
			if (!!minValue) {
				minValue = parseInt(minValue, 10);
			}
			if (!!maxValue) {
				maxValue = parseInt(maxValue, 10);
			}
		} catch (e) {
			return {};
		}
		if ((minValue && maxValue) && (minValue > maxValue)) {
			throw new Error('Invalid validation parameters supplied. Min value cannot be greater than max value');
		}
		return {
			min: minValue,
			max: maxValue
		};
	}

	/**
	 * Binds the validators to their respective fields.
	 * @private
	 */
	_bindFormValidations() {
		let $elements = $(this.validator.formSelector).find(".validate");
		_.each($elements, function (ele, index, list) {
			let $e = $(ele);
			$e.blur({
				self: this
			}, function (e) {
				let self = e.data.self;
				self._checkValidations.call(self, $(this));
				if (!this.isValid) {
					e.stopPropagation();
				}
			});
		}, this);
		this._attachCustomEvents();
	}

	/**
	 * Attaches the custom events to the fields
	 * @private
	 */
	_attachCustomEvents() {
		let v = this.validator.validations;
		if (v) {
			_.each(v, function (value, key) {
				$(this.validator.formSelector).find(key).blur({
					eventConfig: value,
					selector: key,
					self: this
				}, function (e) {
					let self = e.data.self;
					self._validateCustomEvent.call(self, e.data.eventConfig, e.data.selector);
				});
			}, this);
		}
	}

	_isValuePresent($e) {
		return !!$e.val();
	}

	/**
	 * Checks if the value is within range of min & max. If not, returns the error message that should be displayed.
	 * @param min
	 * @param max
	 * @param val
	 * @param type Length check or Value check
	 * @returns {*} True if within range, else error message
	 * @private
	 */
	_isWithinRange(min, max, value, type) {
		let val = 0;
		try {
			val = type.toLowerCase() === "length" ? value.length : parseInt(value);
		} catch (e) {
			val = 0;
		}

		if ((min && max) && val < min || val > max) {
			return type + ' should fall between ' + min + ' and ' + max;
		}
		else {
			if (min && (val < min)) {
				return type + ' should be atleast ' + min + ' digits';
			}
			else if (max && val > max) {
				return type + ' should be smaller than ' + max + ' digits';
			}
		}
		return true;

	}

	static _markInvalid($elem, msg) {
		if (!$elem) {
			$('.error').html(msg).removeClass('invisible');
		} else {
			var $errorLabel = $elem.closest('.form-group').find('.error');
			if($errorLabel.hasClass('hide')) {
				$errorLabel.removeClass('hide').html(msg);    
			}
			
		}
	}

	static _markValid($elem){
		if (!$elem) {
			$('.error').html(msg).addClass('invisible');
		} else {
			$elem.closest('.form-group').find('.error').addClass('hide');
		}
	}

	displayGlobalSuccessMessage(msg) {
		$(this.validator.formSelector).find('.global-msg')
									.removeClass('global-error')
									.addClass('global-success')
									.children('label')
									.first()
									.html(msg)
									.removeClass('global-error-txt')
									.addClass('global-success-txt');
	}

	displayGlobalErrorMessage(msg) {
		$(this.validator.formSelector).find('.global-msg')
									.removeClass('global-success')
									.addClass('global-error')
									.children('label')
									.first()
									.html(msg)
									.removeClass('global-success-txt')
									.addClass('global-error-text');
	}
}

module.exports = FormView;