from django.utils.html import escape
from apps.posts.models import QAMapping, Question, Answer, QAToggle
from apps.posts.serializers import QASerializer
from apps.seo.services.common_services.seo_title_string_util import SeoTitleStringUtil

__author__ = 'ansilkareem'


class PostService(object):
    def __init__(self, page, query, item_names=[]):
        self.page = page.upper()
        self.query = query
        self.item_names = item_names
        self.config = QAToggle.objects.all().first()

    def get_post(self):

        posts = []
        if self.is_enabled():
            qa_mappings = QAMapping.objects.filter(
                page_type=self.page,
                page_type_id=self.query,
                status=1,
            ).order_by('-priority').values('question_id')
            question_ids = [item['question_id'] for item in qa_mappings]
            questions = Question.objects.filter(
                status=1, id__in=question_ids, state=Question.APPROVED)
            posts = []
            for item in questions:
                posts.append({
                    'question': str(item.question_data),
                    'answers': [{'text': str(ans.answer_data),
                                 'user': str(escape(ans.user)),
                                 'posted_date': ans.created_at.strftime('%d-%m-%Y'),
                                 'verified': ans.verified
                                 } for ans in Answer.objects.filter(related_question_id=item.id,
                                                                    state=Answer.APPROVED)],
                    'user': str(escape(item.user)),
                    'posted_date': item.created_at.strftime('%d-%m-%Y')
                })
            serializer = QASerializer(data=posts, many=True)
            if serializer.is_valid():
                posts = serializer.validated_data
            else:
                posts = []

        title = 'Questions & Answers About ' + ' '.join(self.item_names)
        return {
            'posts': {
                'title': title,
                'content': posts
            }
        }

    def get_post_info(self, page, hotel_name=None, city_name=None, locality_name=None, landmark_name=None, category_name=None, amenity_name=None, limit=True):
        posts = []
        total = 0
        if self.is_enabled():
            qa_mappings = QAMapping.objects.filter(page_type=self.page, page_type_id=self.query, status=1, ).order_by(
                '-priority').values('question_id')
            question_ids = [item['question_id'] for item in qa_mappings]
            questions = Question.objects.filter(status=1, id__in=question_ids,
                                                state=Question.APPROVED)
            posts = []
            for item in (questions[:5] if limit else questions):
                posts.append({
                    'question': str(item.question_data),
                    'answers': [{'text': str(ans.answer_data),
                                 'user': str(escape(ans.user)),
                                 'posted_date': ans.created_at.strftime('%d-%m-%Y'),
                                 'verified': ans.verified
                                 } for ans in Answer.objects.filter(related_question_id=item.id,
                                                                    state=Answer.APPROVED)],
                    'user': str(escape(item.user)),
                    'posted_date': item.created_at.strftime('%d-%m-%Y')
                })
            serializer = QASerializer(data=posts, many=True)
            if serializer.is_valid():
                posts = serializer.validated_data
            else:
                posts = []

        title = SeoTitleStringUtil.get_qa_page_title(
            page=page,
            city_name=city_name,
            locality_name=locality_name,
            landmark_name=landmark_name,
            category_name=category_name,
            amenity_name=amenity_name,
            hotel_name=hotel_name
        )

        return {
            'posts': {
                'title': title,
                'content': posts,
                'total': len(questions)
            }
        }

    @staticmethod
    def add_question(question, user, page, submitted_by_user=False):
        question = Question.objects.create(
            question_data=str(escape(question)),
            user=user,
            state=Question.NOT_PROCESSED,
            page=page,
            submitted_by_user=submitted_by_user)
        return question

    def add_qa_mapping(self, question_obj):
        qa_mapping = QAMapping.objects.create(
            question=question_obj,
            page_type=self.page,
            page_type_id=self.query)
        return qa_mapping

    def is_enabled(self):
        if self.config and self.config.enable_qa:
            return True
        else:
            return False
