from rest_framework import serializers

from dbcommon.models.miscellanous import WifiLocationMap


class WifiLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = WifiLocationMap
        fields = ('id', 'location_key', 'wifi_name')
