# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookingstash', '0007_auto_20160804_1800'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='availability',
            unique_together=set([('date', 'room')]),
        ),
    ]
