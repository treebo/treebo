import logging

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.utils import dateformat
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR

from apps.bookingstash.service.availability_service import check_availability
from apps.checkout.models import BookingRequest
from apps.checkout.service import checkout_service
from apps.checkout.service.checkout_service import create_booking_request
from apps.common import date_time_utils
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.common.error_codes import get as get_error_msg
from apps.discounts.models import DiscountCoupon
from apps.featuregate.manager import FeatureManager
from apps.hotels import utils as hotel_utils
from apps.hotels.service.hotel_service import HotelService
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from apps.hotels.utils import generate_hash
from apps.pages.data_model import ItineraryPageData
from apps.pages.helper import PageHelper
from apps.pricing.dateutils import date_to_ymd_str
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.profiles import utils
from base import log_args
from base.views import validators
from base.views.api import TreeboAPIView
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room
from apps.intelligent_pah.services.pah_services import PAHEnable

logger = logging.getLogger(__name__)


class ItineraryPageSerializer(serializers.Serializer):
    hotel_id = serializers.IntegerField()
    checkin = serializers.DateField()
    checkout = serializers.DateField()
    roomconfig = serializers.CharField(
        max_length=100, validators=[
            validators.validate_roomconfig])


class ItineraryPageAPI(TreeboAPIView):
    __pagename = 'itinerary'
    validationSerializer = ItineraryPageSerializer

    def get(self, request, *args, **kwargs):
        """
        Gets the data for itinerary page.
        hotel_id -- Regular hotel id (not hashed value).
        checkin -- Check In Date in YYYY-MM-DD format.
        checkout -- Check Out Date in YYYY-MM-DD format.
        roomconfig -- Room Configuration as 1-0,2-1,3-1. Individual configurations seperated by commas in the string.
        """
        try:
            context, status = self.__get_itinerary_data(request, args, kwargs)
        except Room.DoesNotExist as e:
            return Response({"error": error_message(
                codes.ROOM_NOT_FOUND)}, status=HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"error": error_message(
                codes.REQUEST_FAILED)}, status=HTTP_500_INTERNAL_SERVER_ERROR)

        if status != 'SUCCESS':
            return Response({"error": get_error_msg(status)}, status=404)

        return Response(context)

    def __update_or_get_booking_request(self, request, page_data):
        if not page_data.booking_request_id:
            try:
                booking_request = create_booking_request(request, page_data)
                page_data.booking_request_id = booking_request.id
            except BaseException:
                return "", codes.INVALID_HOTEL_ID

        else:
            booking_request = BookingRequest.objects.get(
                pk=page_data.booking_request_id)
            if not page_data.room_type or not page_data.hashed_hotel_id:
                if not page_data.room_type:
                    page_data.room_type = booking_request.room_type
                if not page_data.hashed_hotel_id:
                    page_data.hashed_hotel_id = hotel_utils.generate_hash(
                        booking_request.hotel_id)

        booking_request = BookingRequest.objects.get(
            pk=page_data.booking_request_id)
        if page_data.checkin_date != booking_request.checkin_date:
            booking_request.checkin_date = page_data.checkin_date
        if page_data.checkout_date != booking_request.checkout_date:
            booking_request.checkout_date = page_data.checkout_date
        if page_data.room_type != booking_request.room_type:
            booking_request.room_type = page_data.room_type
        if page_data.room_config != booking_request.room_config:
            booking_request.room_config = page_data.room_config
        booking_request.meta_http_host = request.META.get('HTTP_HOST', '')
        booking_request.save()
        return booking_request, 'SUCCESS'

    def post_process_context(self, request, context, booking_request):
        hashed_hotel_id = generate_hash(booking_request.hotel_id)
        utm_source = str(request.COOKIES.get('utm_source')).lower()
        utm_medium = str(request.COOKIES.get('utm_medium')).lower()

        featureManager = FeatureManager(
            self.__pagename,
            context['date']['checkin'],
            context['date']['checkout'],
            context['hotel']['id'],
            booking_request.total_amount,
            context["hotel"]["locality"],
            context['hotel']['city_id'],
            utm_source,
            utm_medium)
        overRiddenValues = featureManager.get_overriding_values()

        context['pah_enabled'] = True
        context['paynow_enabled'] = True
        check_in = context['date']['checkin']
        hotel_id = context['hotel']['id']
        bid = context['bid']
        for overriddenKey in list(overRiddenValues.keys()):
            context[overriddenKey] = overRiddenValues[overriddenKey]
        logger.info(
            "Feature gate return PAHEnable as %s for hotel id %s",
            context['pah_enabled'],
            hotel_id)
        is_pah_enabled, pah_message = PAHEnable().intelligent_pah(check_in, hotel_id, bid)
        context['pah_enabled'] = context['pah_enabled'] and is_pah_enabled
        context['pah_message'] = pah_message
        logger.info(
            "The value for PAHEnable is %s for hotel id : %s",
            context['pah_enabled'],
            hotel_id)
        couponDesc = ""
        couponObj = DiscountCoupon.objects.filter(
            code__iexact=booking_request.coupon_code).first()
        if couponObj:
            couponDesc = str(couponObj.terms.replace('\\n', ' '))
        context['coupon_desc'] = couponDesc
        return context

    def get_itinerary_data(self, request, args, kwargs):
        page_data = ItineraryPageData(**request.GET.dict())
        # if page_data.status != 'SUCCESS':
        #     return {}, -1, page_data.status
        hotel_service = HotelService()
        room_types = hotel_service.get_room_types_for_hotel(page_data.hotel_id)
        room_type_exists = False
        for room_type in room_types:
            if page_data.room_type.lower() in room_type.lower():
                room_type_exists = True
                break
        if not room_type_exists:
            raise Room.DoesNotExist
        booking_request, status = self.__update_or_get_booking_request(
            request, page_data)
        if status != 'SUCCESS':
            return "", "", status
        checkin_date_string, checkout_date_string = date_to_ymd_str(
            booking_request.checkin_date), date_to_ymd_str(
            booking_request.checkout_date)
        adults, children, room_count = PageHelper.parseRoomConfig(
            booking_request.room_config)
        days = date_time_utils.get_day_difference(
            checkin_date_string, checkout_date_string)

        hotel = get_object_or_404(Hotel, pk=booking_request.hotel_id)
        room_config_array = PageHelper.getRoomConfigData(
            booking_request.room_config)

        check_in = dateformat.format(booking_request.checkin_date, "jS M'y")
        check_out = dateformat.format(booking_request.checkout_date, "jS M'y")

        if request.user.is_authenticated() and utils.get_customer_care(request.user.id):
            is_call_center = True
        else:
            is_call_center = False

        email, mobile, name = self.__get_booking_user_details(request)

        available = check_availability(
            hotel,
            booking_request.room_type,
            booking_request.room_config,
            checkin_date_string,
            checkout_date_string)
        hotel_showcased_image_obj = hotel.get_showcased_image_url()
        hotel_showcased_image_url = hotel_showcased_image_obj.url if hotel_showcased_image_obj else ''
        is_otp_enabled = FeatureToggleAPI.is_enabled(
            "booking", "otp_verification_required", False)

        context = {
            "page_name": "Itinerary",
            "number_of_nights": days,
            "channel": booking_request.booking_channel,
            "date": {
                "checkin": checkin_date_string,
                "checkout": checkout_date_string},
            "is_callcenter": is_call_center,
            "room": {
                "count": room_count,
                "config": room_config_array,
                "type": booking_request.room_type},
            "hotel": {
                "id": hotel.id,
                "name": hotel.name,
                "street": hotel.street,
                "locality": hotel.locality.name,
                "city": hotel.city.name,
                "city_id": hotel.city.id,
                "state": hotel.state.name,
                "pincode": hotel.locality.pincode,
                "image_url": get_image_uncode_url(hotel_showcased_image_url),
                "coordinates": {
                    "lat": hotel.latitude,
                    "lng": hotel.longitude}},
            "pay_at_hotel": True,
            "ask_for_advance": False,
            "guest": {
                "name": name,
                "email": email,
                "mobile": mobile,
                "adults": adults,
                "children": children},
            "sold_out": not available,
            "comments": "",
                        'razorpay_keys': settings.RAZORPAY_KEYS,
                        "coupon_code": booking_request.coupon_code,
                        "bid": booking_request.id,
            "isOtpEnabled": is_otp_enabled}
        # Adding If else condition to work with soa as well as local database
        # TODO: Move this logic from here to the api in Pricing App
        prices = checkout_service.build_pricing_for_booking_with_member_prices(
            request, booking_request)
        context.update({
            "price": prices
        })
        return context, booking_request, status

    @log_args(logger)
    def __get_itinerary_data(self, request, args, kwargs):

        context, booking_request, status = self.get_itinerary_data(
            request, args, kwargs)
        if status != 'SUCCESS':
            return context, status
        self.post_process_context(request, context, booking_request)
        return context, status

    def __get_booking_user_details(self, request):
        logger.debug('Getting Booking User Details')
        email = mobile = name = None
        if request.user.is_authenticated():
            name = request.user.first_name
            email = request.user.email
            mobile = request.user.phone_number
        return email, mobile, name
