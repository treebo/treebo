import datetime

delta = datetime.timedelta(days=1)


def date_range(start_date, end_date):
    yield start_date
    current_date = start_date + delta
    while current_date < end_date:
        yield current_date
        current_date = current_date + delta


def ymd_str_to_date(date_ymd_str):
    return datetime.datetime.strptime(date_ymd_str, '%Y-%m-%d').date()


def date_to_ymd_str(date_obj):
    try:
        return date_obj.strftime('%Y-%m-%d')
    except AttributeError:
        # Handle date_obj of str type. This assumes that, date_obj is already
        # in given format
        return date_obj
