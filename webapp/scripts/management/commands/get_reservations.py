import hmac
import logging
import sys
from datetime import datetime, timedelta
from hashlib import sha1

import requests
from bs4 import BeautifulSoup
from apps.common import date_time_utils
from django.conf import settings
from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand

from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.reservation import Reservation
from data_services.room_repository import RoomRepository
from dbcommon.models.hotel import Hotel

logger = logging.getLogger('cron')

room_data_service = RepositoriesFactory.get_room_repository()

# TODO : Enable mail for hotel not found , change page
# Get an instance of a logger


class Command(BaseCommand):
    help = 'Calls the get reservation api and stores data in db'
    consumer_key = settings.CONSUMER_KEY
    consumer_secret = settings.CONSUMER_SECRET
    hotelogix_url = settings.HOTELOGIX_URL
    bookingdetails_url = settings.BOOKING_DETAILS_URL
    sender_email = 'kulizadev@gmail.com'
    recipients = [
        'baljeet.kumar@treebohotels.com',
        'shekhar.khanna@kuliza.com']
    username = 'kulizadev@gmail.com'
    password = 'kuliza123'
    offset = 100

    def handle(self, *args, **options):
        try:
            self.get_reservations()
        except Exception as exc:
            message = "Unhandled Exception has occured : " + exc.message
            print(message)
            self.sendExceptionEmail(message)

    # def my_scheduled_job(self):
    #     print("test job")
    #     print(__name__)
    #     print("testing 123");

    def ws_auth_xml(self):
        time = datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + \
            "'><request method='wsauth' key='" + Command.consumer_key + "'></request></hotelogix>"
        return xml

    def ws_auth_signature(self, xml):
        signature = hmac.new(Command.consumer_secret, xml, sha1)
        ws_auth_signature = signature.hexdigest()
        return ws_auth_signature

    def search_xml(self, accesskey, start_date, end_date):
        current_time = datetime.utcnow().isoformat()

        xml = '''<?xml version="1.0"?>
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing">
              <soap:Header xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:htng="http://htng.org/PWSWG/2007/02/AsyncHeaders">
                <wsa:MessageID>4ce4bec7-8fd2-e495-f023-82231bad9bd9</wsa:MessageID>
                <htng:CorrelationID>4ce4bec7-8fd2-e495-f023-82231bad9bd8</htng:CorrelationID>
                <wsa:To>https://partners.url.com</wsa:To>
                <wsa:Action>http://hotelogix.local/ws/webv2/getreservations</wsa:Action>
                <wsa:ReplyTo>
                  <wsa:Address>https://partners.url.com</wsa:Address>
                </wsa:ReplyTo>
                <htng:ReplyTo>
                  <wsa:Address/>
                </htng:ReplyTo>
                <wsse:Security xmlns:wsu="http://schemas.xmlsoap.org/ws/2003/06/utility" soap:mustUnderstand="true">
                  <wsse:UsernameToken>
                    <wsse:Username>''' + accesskey + '''</wsse:Username>
                    <wsse:Password/>
                    <wsu:Created>''' + current_time + '''</wsu:Created>
                  </wsse:UsernameToken>
                </wsse:Security>
              </soap:Header>
              <soap:Body>
                <OTA_ReadRQ TimeStamp="''' + current_time + '''" Version="1">
                  <ReadRequests>
                    <HotelReadRequest>
                      <SelectionCriteria Start="''' + start_date + '''" End="''' + end_date + '''"/>
                    </HotelReadRequest>
                  </ReadRequests>
                </OTA_ReadRQ>
              </soap:Body>
            </soap:Envelope>'''

        return xml

    def create_signature(self, accesssecret, xml):
        signature = hmac.new(
            str(accesssecret),
            str(xml),
            digestmod=sha1).hexdigest()
        return signature

    def get_reservations(self):
        start_date = (datetime.now() + timedelta(days=-1)).strftime("%Y-%m-%d")
        end_date = (datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
        auth_xml = self.ws_auth_xml()
        auth_signature = self.ws_auth_signature(auth_xml)
        # set what your server acceptsprint( headers
        headers = {
            'Content-Type': 'text/xml',
            'X-HAPI-Signature': auth_signature}

        response = requests.post(
            self.hotelogix_url + "wsauth",
            data=auth_xml,
            headers=headers)
        resp_text = response.text
        print(resp_text)
        xml = BeautifulSoup(resp_text, "xml")
        accesskey = xml.hotelogix.response.accesskey["value"]
        accesssecret = xml.hotelogix.response.accesssecret["value"]

        print((accesskey + " " + accesssecret))

        srch_xml = self.search_xml(accesskey, start_date, end_date)
        search_signature = self.create_signature(accesssecret, srch_xml)
        page = 1
        processed = self.offset
        total_processed = 0
        while (True and processed == self.offset):
            search_headers = {
                'Content-Type': 'text/xml',
                'X-HAPI-Signature': search_signature,
                'Page': page,
                'Offset': self.offset}
            url = self.bookingdetails_url + "getreservations"
            print(("Page = " + str(page) + " Offset = " + str(self.offset)))
            print(srch_xml)
            response = requests.post(
                url, data=srch_xml, headers=search_headers)
            print((response.status_code))
            processed = 0
            if response.status_code == 200:
                xml = BeautifulSoup(response.text, "xml")
                reservations = xml.findAll("HotelReservation")
                for reservation in reservations:
                    processed += 1
                    total_processed += 1
                    print(reservation)
                    print((reservation["ResStatus"] + " " +
                           reservation.UniqueID["ID"] + " "))
                    self.save_reservations(reservation)
            else:
                break
                print("Oops! something went wrong")
            page += 1
            print(("Reservations in this loop = " + str(processed)))
        print(("Total reservations processed = " + str(total_processed)))

    def save_reservations(self, reservation):
        try:
            reservation_obj = Reservation()
            reservation_obj.reservation_id = reservation.UniqueID["ID"]
            reservation_obj.reservation_status = reservation["ResStatus"]
            reservation_obj.reservation_checkin_date = datetime.strptime(
                reservation.find('TimeSpan')["Start"], '%Y-%m-%d')
            reservation_obj.reservation_checkout_date = datetime.strptime(
                reservation.find('TimeSpan')["End"], '%Y-%m-%d')
            reservation_obj.adult_count = reservation.find('GuestCount')[
                "Count"]
            reservation_obj.total_amount = reservation.find(
                'Rate').Base["AmountAfterTax"]
            reservation_obj.room_unit = reservation.find("RoomRate")[
                "NumberOfUnits"]
            hotelogix_id = reservation.find(
                reservation.find("BasicPropertyInfo")["HotelCode"])
            try:
                hotel = Hotel.objects.get(hotelogix_id=hotelogix_id)
                #hotel = hotel_repository.get_single_hotel(hotelogix_id=hotelogix_id)
            except Exception as e:
                message = "exception is " + \
                    str(e) + " " + str(hotelogix_id) + " " + reservation.UniqueID["ID"]
                print(message)
                hotel = None
            reservation_obj.hotel = hotel
            if reservation_obj.hotel is not None:
                reservation_obj.room = self.get_room_from_hotelogix(
                    reservation.find("RoomRate")["RoomTypeCode"],
                    reservation_obj.hotel.id, reservation.UniqueID["ID"])
            print("room is set")
            try:
                reservation_obj.guest_name = reservation.find(
                    "Customer").PersonName.GivenName.string
            except Exception as exc:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                self.buildError(
                    "customer name " + str(exc),
                    reservation.UniqueID["ID"],
                    exc_tb.tb_lineno)
            try:
                reservation_obj.guest_email = reservation.find(
                    "Customer").Email.string
            except Exception as exc:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                self.buildError(
                    "email " + str(exc),
                    reservation.UniqueID["ID"],
                    exc_tb.tb_lineno)
            try:
                reservation_obj.guest_mobile = self.get_telephone_number(
                    reservation.findAll("Telephone"))
            except Exception as exc:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                self.buildError(
                    "mobile " + str(exc),
                    reservation.UniqueID["ID"],
                    exc_tb.tb_lineno)
            reservation_obj.reservation_create_time = datetime.strptime(
                reservation["CreateDateTime"], '%Y-%m-%d %H:%M:%S')
            savedReservation = self.get_saved_reservation(
                reservation_obj.reservation_id)
            if savedReservation is None:
                print("Saving reservation , creating new entry")
                reservation_obj.save()
            else:
                print(("Updating status of already present reservation , ReservationId " +
                       str(reservation_obj.reservation_id)))
                savedReservation.reservation_status = reservation_obj.reservation_status
                savedReservation.save()
        except Exception as exc:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            self.buildError(
                str(exc),
                reservation.UniqueID["ID"],
                exc_tb.tb_lineno)

    def get_room_from_hotelogix(
            self,
            room_type_code,
            hotel_id,
            reservation_id):
        print((str(hotel_id) + " " + str(room_type_code)))
        fields_list = [{'name': room_type_code,
                        'value': room_type_code,
                        'partial': False,
                        'case_sensitive': False}]
        rooms = room_data_service.filter_room_by_string_matching(
            fields_list=fields_list, filter_params={'hotel': hotel_id})
        if not rooms:
            message = "Rooms not found for " + str(room_type_code) + " " + str(
                hotel_id) + " " + str(
                reservation_id)
            print(message)
            self.sendExceptionEmail(message)
            return None
        return rooms

    def get_saved_reservation(self, reservation_id):
        try:
            return Reservation.objects.get(reservation_id=reservation_id)
        except Reservation.DoesNotExist:
            print(("exception is " + "Reservation not found"))
            return None

    def get_telephone_number(self, telephones):
        for telephone in telephones:
            try:
                return telephone["PhoneNumber"]
            except Exception as exc:
                print(("exception is " + str(exc)))

    def sendExceptionEmail(self, content):
        # send mail
        subject = "Exception raised in cron"
        receiver = self.recipients
        sender = self.sender_email
        msg = EmailMessage(subject, content, sender, receiver)
        msg.send()

    #
    def buildError(self, exception_message, reservation_id, line_number):
        message = "Exception " + exception_message + " " + \
            reservation_id + " line " "number = " + str(line_number)
        print(message)
        self.sendExceptionEmail(message)
        # def main():
        # 	my_scheduled_job()
        #
        # if __name__ == '__main__':
        #
        # 	main()
