# -*- coding: utf-8 -*-
# pylint: disable=too-many-arguments
from apps.common.data_classes.base_data_class import BaseDataClass


class Address(BaseDataClass):
    mandatory_fields = ['line1', 'city', 'state', 'pincode', 'country']

    def __init__(self, line1, line2, city, state, pincode, country, default=False):
        self.line1 = line1
        self.line2 = line2 or ','
        self.city = city
        self.state = state
        self.pincode = pincode
        self.country = country
        self.default = default

    @classmethod
    def empty(cls):
        return cls('', '', 'NA', 'NA', 'NA', 'NA')

    def __eq__(self, other):
        if isinstance(other, Address):
            return (self.line1 == other.line1
                    and self.line2 == other.line2
                    and self.city == other.city
                    and self.state == other.state
                    and self.country == other.country
                    and self.pincode == other.pincode)
        return NotImplemented

    def __hash__(self):
        return hash(tuple(sorted(self.__dict__.items())))

    def __repr__(self):
        return "{l1},{l2}, {ct}{st}-{p}, {cn}".format(l1=self.line1,
                                                      l2=self.line2,
                                                      ct=self.city,
                                                      st=self.state,
                                                      p=self.pincode,
                                                      cn=self.country,
                                                      )

    def __str__(self):
        return self.__repr__()

    def __bool__(self):
        return all([getattr(self, field) for field in self.mandatory_fields])

    __nonzero__ = __bool__
