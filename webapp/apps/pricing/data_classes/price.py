# -*- coding: utf-8 -*-
from __future__ import division

import hashlib
import warnings

import datetime
import time
from cached_property import cached_property
from django.conf import settings

from apps.common.data_classes.base_data_class import BaseDataClass
from apps.common.date_time_utils import daterange
from apps.pricing.data_classes import DatePrice


class Price(BaseDataClass):
    verification_delimiter = '::'

    def __init__(self, prices):
        self._prices = frozenset(prices)
        self.day_wise_price_map = {price.date_str(): price for price in prices}

    @property
    def total(self):
        return sum(self._prices)

    @cached_property
    def date_wise_breakup(self):
        return sorted(self._prices, key=lambda price: price.date)

    @cached_property
    def per_day(self):
        return self.total / len(self._prices)

    @property
    def start_date(self):
        return min([price.date for price in self.date_wise_breakup])

    @property
    def end_date(self):
        return max([price.date for price in self.date_wise_breakup])

    @classmethod
    def from_total_price(cls, pre_tax, tax, start_date, end_date):
        """ Alternative constrictor to construct Price from total price"""
        dates = list(daterange(start_date, end_date))
        span = len(dates)
        _pre_tax = pre_tax / span
        _tax = tax / span
        return cls([DatePrice(date, _pre_tax, _tax) for date in dates])

    @classmethod
    def from_per_day_price(cls, pre_tax, tax, start_date, end_date):
        """ Alternative constrictor to construct Price from per day price"""
        return cls([DatePrice(date, pre_tax, tax) for date in daterange(start_date, end_date)])

    @classmethod
    def format_maybe_date_to_string(cls, date):
        if isinstance(date, str):
            date = datetime.datetime.strptime(date, '%Y-%m-%d')
        return date.strftime('%Y-%m-%d')

    def verify(self, verification_id):
        """ Checks the sha and also validates if the price has been generated in 30 mins (1800 secs)"""
        gen_verification_id = self.generate_verification_id()
        gen_sha, gen_epoch = gen_verification_id.split(self.verification_delimiter)
        sha, epoch = verification_id.split(self.verification_delimiter)

        if str(gen_sha) != str(sha) or (int(gen_epoch) - int(epoch)) > 1800:
            error = 'Unverified price:{p} and verification id: {v}'.format(p=str(self.total), v=verification_id)
            raise ValueError(error)

    def generate_verification_id(self):
        # use pretax price instead of hash for consistency
        _hash = self.__hash__()
        sha = hashlib.sha1('{}+{}'.format(_hash, settings.SECRET_KEY)).hexdigest()
        epoch = int(time.time())
        return "{sha}{dlm}{ep}".format(sha=sha, dlm=self.verification_delimiter, ep=epoch)

    def __getitem__(self, date):
        _date = self.format_maybe_date_to_string(date)
        return self.day_wise_price_map[_date]

    def __setitem__(self, date, price):
        assert isinstance(price, DatePrice)
        _date = self.format_maybe_date_to_string(date)
        assert _date == price.date_str()

        self._prices = frozenset(list(self._prices).append(price))
        self.day_wise_price_map[_date] = price

    def __iter__(self):
        return iter(self._prices)

    def __repr__(self):
        return '<{kls} ({p})>'.format(kls=self.__class__.__name__,
                                      p='\n,'.join(map(str, self._prices)),
                                      )

    def __str__(self):
        # better with a more human readable format that __repr__
        return self.__repr__()

    def __eq__(self, other):
        if isinstance(other, Price):
            total_price_equivalence = hash(self) == hash(other)
            individual_prices_equivalence = (hash(self._prices) == hash(
                other._prices))  # pylint: disable=protected-access
            if total_price_equivalence and not individual_prices_equivalence:
                warnings.warn("There must be some difference in tax for this to happen."
                              "{s}\n{o}".format(s=self, o=other))
            return total_price_equivalence
        return NotImplemented

    def __hash__(self):
        """
        removing hashing with hash(self._prices) as it is causing inconsistencies.
        caused by the difference in how we speak with crs date wise and with athena per day
        assume scenario:
        "day_wise_price": [
                    {
                        "date": "2018-09-12T00:00:00",
                        "pre_tax": "1350.3000",
                        "tax": "243.0540"
                    },
                    {
                        "date": "2018-09-13T00:00:00",
                        "pre_tax": "1350.3000",
                        "tax": "162.0360"
                    }
                ],
                "per_day_price": {
                    "pre_tax": "1350.3000",
                    "tax": "202.5450"
                }

        CRS DatePrice will have tax of 243, 162
        B2B Price will have tax of 202.54, 202.54

        Hence relying on total price
        """
        price = self.total
        if price.pre_tax is not None and price.tax is not None:
            return hash((price.pre_tax, price.tax))
        if price.post_tax:
            return hash(price.post_tax)

    def is_tax_populated(self):
        return not bool([price for price in self._prices if not price.tax])
