import logging
from django.core.cache import cache
from django.db.models import Q
from django.template.defaultfilters import slugify

from apps.search.services.auto_completion_service import AutoCompletionService
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.location import Locality, City, State

logger = logging.getLogger(__name__)


class LocalityBasedAutoCompletionService(AutoCompletionService):
    CACHE_TIMEOUT = 24 * 60 * 60

    def apply(self, search_string, **kwargs):
        try:
            result = cache.get(
                'cs_autocomplete_cache_locality_' +
                slugify(search_string))
        except Exception as e:
            result = None
        if result:
            return result

        search_string_arr = search_string.split(" ")
        query = (Q(name__istartswith=search_string))
        query |= (Q(name__icontains=' '+search_string))
        alias_query = Q(alias__istartswith=search_string)
        alias_query |= Q(alias__icontains=' '+search_string)

        for string_count in range(1, len(search_string_arr)):
            curr_search_string = ' '.join(search_string_arr[0:len(search_string_arr)-string_count])
            query |= Q(name__istartswith=curr_search_string)
            query |= Q(name__icontains=' '+curr_search_string)
            alias_query |= Q(alias__istartswith=curr_search_string)
            alias_query |= Q(alias__icontains=' '+curr_search_string)

        city_data_service = RepositoriesFactory.get_city_repository()
        cities = city_data_service.get_all_cities_for_search_string(
            query, alias_query, original_search_query=search_string)

        locality_data_service = RepositoriesFactory.get_locality_repository()
        localities = locality_data_service.filter_localities_with_query(query=query)

        # Exclude New Delhi as State, if it is already present in City list
        states = State.objects.filter(query).exclude(
            name__in=[city.name for city in cities])

        locality_results = AutoCompleteResultBuilder.build_locality_result(
            localities)
        city_results = AutoCompleteResultBuilder.build_city_result(cities)
        state_results = AutoCompleteResultBuilder.build_state_result(states)
        result = city_results + locality_results + state_results

        if result:
            cache.set('cs_autocomplete_cache_locality_' + slugify(search_string),
                      result, LocalityBasedAutoCompletionService.CACHE_TIMEOUT)

        return result
