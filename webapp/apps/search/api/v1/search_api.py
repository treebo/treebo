import collections
import json
import logging
from datetime import datetime
from decimal import Decimal, ROUND_UP

from django.conf import settings
from django.db.models.query_utils import Q
from rest_framework.response import Response

import common.constants.common_constants as const
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from apps.common import date_time_utils
from apps.common import utils as commonUtils
from apps.hotels.serializers.image_serializer import ImageSerializer
from apps.hotels.service.hotel_service import HotelService
from apps.search.services.search import SearchService
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.location import Landmark, City, Locality
from dbcommon.models.miscellanous import Image
from services import maps
from services.restclient.pricingrestclient import PricingClient

logger = logging.getLogger(__name__)


class SearchView(TreeboAPIView):
    __searchService = SearchService()
    __hotelService = HotelService()

    @staticmethod
    def getSubLocality(locality, searchString):
        queryArray = searchString.split(",")
        subLocality = queryArray[0]
        if queryArray[0] == locality or len(queryArray) <= 3:
            subLocality = ""
        return subLocality

    @staticmethod
    def getLandmarkDetailsFromCityName(city):
        landmarkSet = []
        try:
            # cityObject = City.objects.get(name=cityName, status=City.ENABLED)
            landmarks = city.landmark_set.filter(status=Landmark.HIDE)
            if landmarks:
                for landmark in landmarks:
                    landmarkSet.append({
                        'name': landmark.name,
                        'latitude': landmark.latitude,
                        'longitude': landmark.longitude
                    })
            return landmarkSet, city
        except City.DoesNotExist:
            logger.info("No city found")

        return landmarkSet, ""

    def get(self, request, *args, **kwargs):
        """
        Search API to get list of available hotels for Search Page.
        query -- Search Query String
        checkin -- Checkin Date (in YYYY-mm-dd format)
        checkout -- Checkout Date (in YYYY-mm-dd format)
        roomconfig -- Room Configuration required
        """
        d = request.GET
        search_string = d.get('query')
        check_in = d.get('checkin')
        check_out = d.get('checkout')
        room_config = d.get('roomconfig', '1-0')
        latitude = d.get('lat', const.BANGALORE_LAT)
        longitude = d.get('long', const.BANGALORE_LONG)
        locality = d.get('locality', const.DEFAULT_LOCALITY)
        location_type = d.get('locationtype')
        rooms_config = []
        for room in room_config.split(','):
            room_tuple = room.split('-')
            adults = int(room_tuple[0])
            if len(room_tuple) > 1:
                children = int(room_tuple[1])
            else:
                children = 0
            rooms_config.append((adults, children))

        check_in_date = datetime.strptime(
            check_in, date_time_utils.DATE_FORMAT)
        check_out_date = datetime.strptime(
            check_out, date_time_utils.DATE_FORMAT)

        search_service_result = self.__searchService.search(
            search_string,
            locality,
            latitude,
            longitude,
            location_type if location_type else "")

        parent_query = search_service_result.get('parentQuery', '')
        search_query_result = search_service_result['searchResults']
        nearby_query_result = search_service_result['nearbyResults']
        is_locality_search = search_service_result['isLocalitySearch']
        is_city_state_hotel_search = search_service_result[
            'cityOrStateOrHotelNotSearchedExplicitly']
        city = search_service_result['city']
        city_name = city.name if city else None
        current_locality = search_service_result['locality']

        # Avoid Locality Query here, by returning locality object from search
        # method above, instead of localityName
        if not current_locality:
            current_locality = Locality.objects.filter(
                Q(name__iregex='^.*')).first()

        if not search_query_result and nearby_query_result:
            response = {
                'allLocalityList': None,
                'parentQuery': None,
                'isLocalitySearch': is_locality_search,
                'cityOrStateOrHotelNotSearchedExplicitly': is_city_state_hotel_search,
                'localityName': current_locality.name if current_locality else None,
                'cityName': city_name,
                'hotels': []}
            return Response(response, status=200)

        hotels = list(search_query_result) + list(nearby_query_result)

        localities = set()
        for hotel in hotels:
            localities.add(hotel.locality.name)
        localities = list(localities)

        sub_locality = SearchView.getSubLocality(locality, search_string)

        if latitude and longitude and city is not None:
            landmarks, city = SearchView.getLandmarkDetailsFromCityName(city)
            __myMap = maps.Maps()
            hotels = __myMap.getHotelDistanceWithoutGMAP(
                hotels, city, latitude, longitude, sub_locality)

        hotel_ids = [selected_hotel.id for selected_hotel in hotels]
        room_config_param = self.__get_rooms_config_params(rooms_config)
        cheapest_room_price_and_availability = PricingClient.get_cheapest_price_and_availability_for_all_hotels(
            request, check_in, check_out, room_config_param, hotel_ids)
        cheapest_room_type_with_availability = {}
        for k, v in list(cheapest_room_price_and_availability.items()):
            cheapest_room_type_with_availability[int(k)] = v['cheapest_room']

        quickbook_url_dict = self.__hotelService.get_quickbook_url_dict_by_hotel(
            hotels,
            check_in_date,
            check_out_date,
            rooms_config,
            cheapest_room_type_with_availability)

        width, height = commonUtils.Utils.get_image_size("mobile", "search")
        image_city_params = commonUtils.Utils.build_image_params(
            width, height, const.FIT_TYPE, const.CROP_TYPE, const.QUALITY)

        result = []
        hotel_showcased_img_dict = self.get_hotel_showcased_image_url(hotels)
        for hotel in hotels:
            pricing = cheapest_room_price_and_availability.get(str(hotel.id))
            if not pricing:
                # Price not found. Skip the hotel from the result
                continue
            quickbookUrl = quickbook_url_dict.get(hotel.id)
            # This would never throw DoesNotExist error
            trilight = [
                highlight.description for highlight in hotel.hotel_highlights.all()]
            image_url, image_set = hotel_showcased_img_dict.get(
                hotel.id), hotel.image_set.all()
            if image_url:
                image_url = get_image_uncode_url(image_url.url)
            facilities = [
                facility.name for facility in hotel.facility_set.all()]
            nights_breakup = []
            # WHere is breakup populated
            price_breakup = collections.OrderedDict(
                sorted(pricing['breakup'].items()))

            TWO_DECIMAL_PLACES = Decimal('.01')
            for price_key in list(price_breakup.keys()):
                current_date = price_key
                nights_breakup.append(
                    {
                        "date": current_date, "base_price": float(
                            Decimal(
                                price_breakup[price_key]['pretax']).quantize(
                                TWO_DECIMAL_PLACES, rounding=ROUND_UP)), "tax": float(
                            Decimal(
                                price_breakup[price_key]['tax']).quantize(
                                TWO_DECIMAL_PLACES, rounding=ROUND_UP)), "final_price": float(
                                    Decimal(
                                        price_breakup[price_key]['sell']).quantize(
                                            TWO_DECIMAL_PLACES, rounding=ROUND_UP))})

            image_serializer = ImageSerializer(instance=image_set, many=True)
            image_set = image_serializer.data
            item = {
                'id': hotel.id,
                'hotelName': hotel.name,
                'cheapest_room_type': pricing['cheapest_room']['room_type'],
                'priority': 1,
                'categories': [],
                'quickbookUrl': quickbookUrl,
                'area': str(hotel.street).encode('ascii', 'ignore'),
                'promoTagline': '',
                'amenities': facilities,
                'locality': hotel.locality.name,
                'city': hotel.city.name,
                'latitude': hotel.latitude,
                'longitude': hotel.longitude,
                'distance': hotel.distance if hasattr(hotel, 'distance') else "",
                'pincode': hotel.locality.pincode,
                'roomsLeft': pricing['cheapest_room']['availability'],
                'available': pricing['cheapest_room']['availability'] != 0,
                # 'available': hotel.availability,
                'nights': json.dumps(nights_breakup),
                'trilights': trilight,
                'price': pricing['sell'],
                'basePrice': pricing['rack_rate'],
                'priceBreakup': {
                    'price': pricing['pretax'],  # for testing
                    'tax': pricing['tax'],
                    'discount': 0,
                    'total': pricing['sell'],
                },
                'imageset': image_set,
                # TODO Change to basePrice later
                'image': {
                    'alt': hotel.name,
                    'imageUrl': image_url,
                    'imageParams': image_city_params
                }
            }
            result.append(item)

        response = {
            'allLocalityList': localities,
            'parentQuery': parent_query,
            'isLocalitySearch': is_locality_search,
            'cityOrStateOrHotelNotSearchedExplicitly': is_city_state_hotel_search,
            'localityName': current_locality.name,
            'cityName': city_name,
            'hotels': result}
        return Response(response)

    def __get_rooms_config_params(self, roomsConfigList):
        tempRoomsConfig = ['-'.join([str(i), str(j)])
                           for i, j in roomsConfigList]
        roomconfigParams = ','.join([i for i in tempRoomsConfig])
        return roomconfigParams

    def get_hotel_showcased_image_url(self, hotels):
        image_data_service = RepositoriesFactory.get_image_repository()
        image_obj_list = image_data_service.get_images_for_hotels(
            hotels=hotels, is_showcased=True)
        #image_obj_list = Image.objects.filter(hotel__in=hotels, is_showcased=True)
        hotel_img_dict = {
            image_obj.hotel_id: image_obj.url for image_obj in image_obj_list}
        return hotel_img_dict
