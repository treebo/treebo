import urllib.parse

from rest_framework import serializers

from dbcommon.models.miscellanous import Image


class HotelImageRequestSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()
    room_type_code = serializers.SerializerMethodField()

    class Meta:
        model = Image
        fields = ('id', 'image_name', 'image_callout', 'url', 'position', 'category',
                  'room_type_code')

    def get_url(self, obj):
        if not obj.url:
            return ''

        url = str(obj.url.url)
        return urllib.parse.unquote(url)

    def get_room_type_code(self, obj):
        if not obj.room:
            return ''

        return str(obj.room.room_type_code)
