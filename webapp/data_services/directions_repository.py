from data_services.decorators import SingletonDecorator


@SingletonDecorator
class DirectionsRepository:

    def __init__(self, directions_web_repository):
        self.directions_web_repository = directions_web_repository

    def get_all_directions_for_hotel(self, hotel_name=None, hotel_id=None):
        return self.directions_web_repository.get_all_directions_for_hotel(hotel_name=hotel_name, hotel_id=hotel_id)

    def get_directions_ordered_by_landmark_popularity(self, hotel_id):
        return self.directions_web_repository.get_directions_ordered_by_landmark_popularity(hotel_id=hotel_id)
