import logging

from base.views.template import TreeboTemplateView
from services.restclient.discountrestclient import DiscountClient

__author__ = 'devang'

logger = logging.getLogger(__name__)


class Offers(TreeboTemplateView):
    template_name = "offers/index.html"
    redirect_mobile_to_desktop_view = True

    def getData(self, request, args, kwargs):
        template_name = "offers/index.html"
        response = DiscountClient.getExternalDiscounts(request)
        discounts = response['data']
        offers = []

        for discount in discounts:
            terms = [discount['terms']] if isinstance(
                discount['terms'], str) else discount['terms']
            content = {
                "tagline": discount['tagline'],
                "value": "",
                "code": discount['code'],
                'terms': terms}
            offers.append(content)

        return {'offers': offers, 'scripts': [],
                'styles': ['desktop/css/offers.css']}
