import logging

from django.db.models import QuerySet

from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class HotelMetaService(object):
    """
    Service for accessing all meta properties stored in HotelMetaData.

    Hotels to be passed is a queryset. All the keys are iterated and filtered upon this hotels.
    If hotels is not passed all active hotels will be taken.

    The filter will look for handlers to be defined in HotelMetaHandlers.

    """

    class Keys(object):
        """
        All supported keys for this Service. New keys are to be added here
        """
        B2B_SALEABLE = 'b2b_saleable'

        def __iter__(self):
            for k, v in list(self.__class__.__dict__.items()):
                if not k.startswith('_'):
                    yield v

    def __init__(self, keys, hotels=None):
        self.hotels = hotels
        #self.hotels = self._validate_hotels(hotels)
        self.keys = self._validate_keys(keys)

    def filter(self):
        from apps.hotels.service.hotel_meta_handlers import HotelMetaHandlers

        for key in self.keys:
            # **IMPORTANT**: make sure all handlers operate on self.hotels only so as to obtain intersection
            self.hotels = HotelMetaHandlers.execute_handler(
                hotels=self.hotels, key=key)
        return self.hotels

    def _validate_hotels(self, hotels):
        if hotels is None:
            hotels = Hotel.objects.all()
        elif not isinstance(hotels, QuerySet):
            raise RuntimeError('Hotels are supposed to be Queryset')
        return hotels

    def _validate_keys(self, keys):
        if keys is None:
            keys = []
        elif isinstance(keys, str):
            # support for comma seperated keys flowing in
            keys = keys.split(',')
        elif not isinstance(keys, (list, set, tuple)):
            raise RuntimeError(
                'Keys are supposed to be list/set/tuple/strings')

        assert all(key in self.Keys() for key in keys)

        return keys
