import logging

import requests
from django.core.urlresolvers import reverse

from services.restclient.request_wrapper import wrap_internal_api_call

logger = logging.getLogger(__name__)


class PricingClient(object):
    @staticmethod
    def getDiscountPricingDetails(
            request,
            checkinDate,
            checkoutDate,
            couponCode,
            hotelId,
            roomConfig,
            roomType):
        logger.info("Inside getDiscountPricingDetails")
        url = request.build_absolute_uri(reverse('pricing_v1:pAvailability'))
        payload = {
            "checkin": checkinDate,
            "checkout": checkoutDate,
            "couponcode": couponCode,
            "roomconfig": roomConfig,
            "roomtype": roomType,
            "hotelid": hotelId}
        logger.info("Calling Pricing API with data: %s", payload)
        response = wrap_internal_api_call(
            request, requests.get, url, params=payload)
        # response = requests.get(url, params=payload)
        rJson = None
        if response.status_code == 200 and 'data' in response.json() and response.json()[
                'data']['status'] == 'success':
            rJson = response.json()['data']
        else:
            logger.error("Pricing API Error Response: %s", response.json())
        return rJson

    @staticmethod
    def getPricingAvailability(
            request,
            checkInDate,
            checkOutDate,
            roomConfig,
            hashedHotelId,
            roomType,
            couponCode):
        url = request.build_absolute_uri(
            reverse('pricing_v1:pAvailability') +
            '?checkin={0}&checkout={1}&roomconfig={2}&hotelid={3}&roomtype={4}&couponcode={5}'.format(
                checkInDate,
                checkOutDate,
                roomConfig,
                hashedHotelId,
                roomType,
                couponCode))
        logger.debug('Pricing URL: %s', url)
        response = wrap_internal_api_call(request, requests.get, url)
        # response = requests.get(url)
        # print response.json()
        logger.debug('get Pricing Response: %s', response.json())
        data = None
        if response.status_code == 200 or response.status_code == 304:
            if 'data' in response.json():
                data = response.json()['data']
        else:
            logger.error("Pricing API Error Response: %s", response.json())

        return data

    @staticmethod
    def get_cheapest_price_and_availability_for_all_hotels(
            request, checkin_date, checkout_date, room_config, hotel_ids=[]):
        query = '?checkin={0}&checkout={1}&roomconfig={2}'.format(
            checkin_date, checkout_date, room_config)
        if hotel_ids:
            ids = [str(id) for id in hotel_ids]
            id_params = ",".join(ids)
            query = query + "&hotel_id=" + id_params
        url = request.build_absolute_uri(
            reverse('pricing_v2:cheapest_prices_for_hotels') + query)
        response = wrap_internal_api_call(request, requests.get, url)
        data = {}
        if response.status_code == 200:
            resp_json = response.json()
            if 'data' in resp_json:
                data = resp_json['data']
        return data

    @staticmethod
    def get_room_type_price_details(
            request,
            checkin_date,
            checkout_date,
            room_config,
            room_type,
            coupon_code,
            coupon_value,
            coupon_type,
            hotel_ids=[]):
        query = '?checkin={0}&checkout={1}&roomconfig={2}&room_type={3}'.format(
            checkin_date, checkout_date, room_config, room_type)
        if hotel_ids:
            ids = [str(id) for id in hotel_ids]
            id_params = ",".join(ids)
            query = query + "&hotel_id=" + id_params
        if coupon_code and coupon_value and coupon_type:
            query += (
                "&coupon_type={0}&coupon_code={1}&coupon_value={2}".format(
                    coupon_type, coupon_code, coupon_value))
        url = request.build_absolute_uri(
            reverse('pricing_v2:pricing_details') + query)
        response = wrap_internal_api_call(request, requests.get, url)
        data = {}
        if response.status_code == 200:
            resp_json = response.json()
            if 'data' in resp_json:
                data = resp_json['data']
        return data

    @staticmethod
    def get_room_type_price_details_for_member_pricing(
            request,
            checkin_date,
            checkout_date,
            room_config,
            room_type,
            coupon_code,
            coupon_value,
            coupon_type,
            hotel_ids=[]):
        query = '?checkin={0}&checkout={1}&roomconfig={2}&room_type={3}'.format(
            checkin_date, checkout_date, room_config, room_type)
        if hotel_ids:
            ids = [str(id) for id in hotel_ids]
            id_params = ",".join(ids)
            query = query + "&hotel_id=" + id_params
        if coupon_code and coupon_value and coupon_type:
            query += (
                "&coupon_type={0}&coupon_code={1}&coupon_value={2}".format(
                    coupon_type, coupon_code, coupon_value))
        url = request.build_absolute_uri(
            reverse('pricing_v3:pricing_breakup_v3') + query)
        response = wrap_internal_api_call(request, requests.get, url)
        data = {}
        if response.status_code == 200:
            resp_json = response.json()
            if 'data' in resp_json:
                data = resp_json['data']
        return data
