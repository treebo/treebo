import logging
import re

from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from base.decorator.request_validator import validate_request
from base.views import validators
from base.views.api import TreeboAPIView
from web_sku.catalog.services.sku_repository import SkuRepository
from webapp.data_services.soa_clients.utils import *

logger = logging.getLogger(__name__)


class SkuSerializer(serializers.Serializer):
    roomconfig = serializers.CharField(max_length=100,
                                       validators=[validators.validate_roomconfig])

    def validate(self, data):
        room_config = data['roomconfig'].strip(',')
        data['roomconfig'] = room_config
        room_config_list = room_config.split(',')
        for room_config in room_config_list:
            if not bool(re.match('^[0-9]*-[0-9]*$', room_config)):
                raise serializers.ValidationError("Invalid room config")
        return data


class PropertySku(TreeboAPIView):

    @validate_request(SkuSerializer)
    def get(self, request, *args, **kwargs):
        result = dict()
        hotel_id = int(kwargs.get('hotel_id'))
        try:
            validated_data = kwargs['validated_data']
            room_config = validated_data['roomconfig']
            hotel_cs_id = get_cs_ids_from_web_hotel_ids([hotel_id])
            result = dict(sku_data=SkuRepository(room_configs=room_config,
                                                 cs_property_ids=hotel_cs_id).get_property_skus())
        except ValidationError as e:
            logger.exception(e.__str__())
            raise e
        except Exception as e:
            logger.exception("Error %s in fetching property sku" % e.__str__())
        return Response(result)
