import logging

from django.dispatch import receiver
from django.db.models.signals import post_delete, post_save
from apps.bookingstash.models import HotelRoomMapper
from apps.bookingstash.models import Availability
logger = logging.getLogger(__name__)


@receiver([post_save, post_delete], sender=HotelRoomMapper)
def on_tax_update(sender, instance, created, *args, **kwargs):
    from dbcommon import event_publishers
    from dbcommon.models.room import Room
    room_id = instance.room_id
    room = Room.objects.get(pk=room_id)
    hotel = room.hotel
    event_publishers.publish_hotel(hotel)


@receiver([post_save], sender=Availability)
def on_inventory_update(sender, instance, created, *args, **kwargs):
    from dbcommon import event_publishers
    logger.info("Got update!!")
    event_publishers.publish_availability(instance)
