# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.payments.api.v2.paymentorder_details import PaymentOrderDetails

app_name = 'payments_v2'
urlpatterns = [url(r'^payments-details/$',
                   PaymentOrderDetails.as_view(),
                   name="paymentorder-details_v2"),
               ]
