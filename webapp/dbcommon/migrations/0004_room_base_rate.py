# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0003_auto_20151027_1148'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='base_rate',
            field=models.DecimalField(
                default=0,
                max_digits=8,
                decimal_places=2),
        ),
    ]
