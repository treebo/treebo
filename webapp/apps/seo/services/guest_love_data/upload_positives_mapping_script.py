import csv
import logging

from django.utils.encoding import smart_str

from apps.seo.models import FeedbackPositives, PositivesCategory, PositiveCategoryContentMapping

logger = logging.getLogger(__name__)


class PositiveMappingScript(object):
    def upload_hotel_positives_mapping(self, ):
        with open('positive_mentions.csv') as csvfile:
            file = csv.reader(csvfile, delimiter=',')
            for row in file:
                if row[0] != "Positives":
                    try:
                        positive = smart_str(row[0])
                        category = smart_str(row[2])
                        content = smart_str(row[3])
                        hotel_pos_obj, pos_create = FeedbackPositives.objects.get_or_create(
                            name=positive)
                        pos_cat_obj, category_create = PositivesCategory.objects.get_or_create(
                            name=category)
                        content_obj, content_create = PositiveCategoryContentMapping.objects.get_or_create(
                            content=content, category=pos_cat_obj, positive=hotel_pos_obj)
                        logger.info(
                            "saved for positive %s category %s content %s",
                            positive,
                            category,
                            content)
                    except Exception as e:
                        logger.exception(
                            'Exception while updating hotel_postive-category-content mapping  from csv for entry %s',
                            row)
                        raise e
