# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0014_auto_20170927_0816'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='organization_address',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='organization_name',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='organization_taxcode',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
