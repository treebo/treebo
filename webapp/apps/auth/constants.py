from ths_common.constants.base_enum import BaseEnum

ANDROID_USER_AGENT = 'starscream/treebo-android'
IOS_USER_AGENT = 'starscream/treebo-ios'

APP_USER_AGENT = [ANDROID_USER_AGENT, IOS_USER_AGENT]


class UserStatus(BaseEnum):
    BLOCKED = 'blocked'
    EXPIRED = 'expired'
    ANON = 'anon'
    VERIFIED = 'verified'


class UserContactType(BaseEnum):
    EMAIL = 'email'
    PHONE = 'phone'


class VerifiedThrough(BaseEnum):
    OTP = 'otp'
    SOCIAL_LOGIN = 'social_login'


class ProfileType(BaseEnum):
    B2B = 'b2b'
    B2C = 'b2c'
