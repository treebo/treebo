# -*- coding:utf-8 -*-
"""
    Logging Config
"""
import logging

from apps.common.json_encoder import LogstashJsonEncoder


def get(log_root=None, formatter=None, handler=None):
    """
    :param log_root:
    :param formatter:
    :param handler:
    :return:
    """
    if not formatter:
        formatter = 'logstash'

    if not handler:
        handler = 'logging.handlers.WatchedFileHandler'

    if not log_root.endswith('/'):
        log_root += '/'

    return {
        'version': 1,
        'disable_existing_loggers': False,

        'filters': {
            'job_filter': {
                '()': 'base.filters.JobIDFilter'
            },
            'request_filter': {
                '()': 'log_request_id.filters.RequestIDFilter'
            },
            'error_filter': {
                '()': 'base.filters.LogLevelFilter',
                'level': logging.ERROR
            },
            'warn_filter': {
                '()': 'base.filters.LogLevelFilter',
                'level': logging.WARN
            },
            'debug_filter': {
                '()': 'base.filters.LogLevelFilter',
                'level': logging.DEBUG
            },
            'info_filter': {
                '()': 'base.filters.LogLevelFilter',
                'level': logging.INFO
            },
            'require_debug_true': {
                '()': 'django.utils.log.RequireDebugTrue',
            },
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse',
            }
        },
        'formatters': {
            'job_verbose': {
                'format': "[%(job_id)s] [%(request_id)s] [%(asctime)s] %(levelname)s  [%(name)s:%(lineno)s] %(message)s request_id=%(request_id)s",
                'datefmt': "%d/%b/%Y %H:%M:%S"
            },
            'verbose': {
                'format': '%(asctime)s %(levelname)s [%(name)s:%(lineno)s] %(module)s %(process)d %(thread)d %(message)s request_id=%(request_id)s',
                'datefmt': "%d/%b/%Y %H:%M:%S"
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
            'logstash_fmtr': {
                '()': 'logstash_formatter.LogstashFormatterV1',
                'json_cls': LogstashJsonEncoder
            },
            'std_output_fmtr': {
                'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
            }
        },
        'handlers': {
            'null': {
                'level': 'INFO',
                'class': 'logging.NullHandler',
            },
            'error_handler': {
                'level': 'ERROR',
                'filters': ['request_filter', 'error_filter'],
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/app_error.log',
                'formatter': 'logstash_fmtr',
            },
            'warn_handler': {
                'level': 'WARN',
                'filters': ['warn_filter', 'request_filter'],
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/app_warn.log',
                'formatter': 'logstash_fmtr',
            },
            'info_handler': {
                'level': 'INFO',
                'filters': ['request_filter', 'info_filter'],
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/app_info.log',
                'formatter': 'logstash_fmtr',
            },
            'debug_handler': {
                'level': 'DEBUG',
                'filters': ['debug_filter', 'request_filter'],
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/app_debug.log',
                'formatter': 'logstash_fmtr',
            },
            'metrics': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/metrics.log',
                'formatter': 'logstash_fmtr',
            },
            'cron': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/cron.log',
                'formatter': 'logstash_fmtr',
            },
            'segment': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/segment.log',
                'formatter': 'logstash_fmtr',
            },
            'hx': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/hx.log',
                'formatter': 'logstash_fmtr',
                'filters': ['job_filter'],
            },
            'easyjoblite': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/easyjoblite.log',
                'formatter': 'logstash_fmtr',
                'filters': ['job_filter'],
            },
            'queue_listeners': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': log_root + '/queue_listeners.log',
                'formatter': 'simple',
            },
        },

        'loggers': {
            'queue_listeners': {
                'handlers': ['queue_listeners'],
                'level': 'INFO',
                'propagate': False,
            },
            'log_request_id': {
                'handlers': ['error_handler'],
                'level': 'ERROR',
                'propagate': False,
            },
            'ab_client' : {
                'handlers': ['error_handler'],
                'level': 'ERROR',
                'propagate': False,
            },
            'django': {
                'handlers': ['error_handler'],
                'level': 'ERROR',
                'propagate': False,
            },
            'celery': {
                'handlers': ['info_handler'],
                'level': 'INFO',
                'propagate': False,
            },
            'cron': {
                'handlers': ['cron'],
                'level': 'DEBUG',
                'propagate': False,
            },
            'metrics': {
                'handlers': ['metrics'],
                'level': 'INFO',
                'propagate': False,
            },
            'segment': {
                'handlers': ['segment'],
                'level': 'DEBUG'
            },
            'easyjoblite': {
                'handlers': ['easyjoblite'],
                'level': 'INFO',
                'propagate': False,
            },
            'hx': {
                'handlers': ['hx'],
                'level': 'INFO',
                'propagate': False,
            },
            '': {
                'handlers': ['info_handler', 'warn_handler', 'error_handler'],
                'level': 'INFO',
                'propagate': True,
            },
        },
    }
