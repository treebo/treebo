import phonenumbers

from rest_framework.response import Response

from apps.phone_number_check.serializers import PhoneNumberSerializer
from base.decorator.request_validator import validate_request
from base.views.api import TreeboAPIView
from common.constants.common_constants import COUNTRY_CODE


class PhoneNumberValidation(TreeboAPIView):

    validationSerializer = PhoneNumberSerializer

    @validate_request(validationSerializer)
    def get(self, request, *args, **kwargs):
        """
            An Api to check if the phone_number entered by the user
            is a valid phone number or not.
            Required parameters
            "phone_number : "
        """

        phone_number = request.GET['phone_number']
        try:
            number = phonenumbers.parse(phone_number, COUNTRY_CODE)
            return Response({
                    'status': phonenumbers.is_valid_number(number)
                    })
        except Exception as ex:
            return Response({
                'status': False,
                'error_msg': str(ex)
            })
