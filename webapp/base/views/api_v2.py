# pylint: disable=invalid-name,logging-not-lazy,no-member,unsubscriptable-object,not-callable,unsupported-membership-test,no-else-return,useless-object-inheritance
import datetime
import logging

from django.conf import settings
from django.http import JsonResponse
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from apps.common.slack_alert import SlackAlertService as slack_alert

from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import InvalidRequestException
from base.renderers import TreeboCustomJSONRenderer
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class TreeboValidationMixin2:
    validation_serializer = None
    serializer_object = None
    renderer_classes = (TreeboCustomJSONRenderer,)

    def dispatch(self, request, *args, **kwargs):
        request_data = {}
        if request.method == 'GET':
            request_data = request.GET
        elif request.method == 'POST':
            request_data = request.POST
        elif request.method == 'PUT':
            request_data = request.PUT

        method = request.method.lower()
        if self.validation_serializer:
            if isinstance(
                self.validation_serializer,
                dict) and method in self.validation_serializer and self.validation_serializer[method]:
                self.serializer_object = self.validation_serializer[method](
                    data=request_data)
            else:
                self.serializer_object = self.validation_serializer(
                    data=request_data)

            if not self.serializer_object.is_valid():
                logger.error(
                    "treebo invalid request exception for class %s with error %s ", str(
                        self.__class__.__name__), self.serializer_object.errors)
                rest_response = api_response.invalid_request_error_response(
                    self.serializer_object.errors, str(self.__class__.__name__))
                django_response = JsonResponse(rest_response.data)
                return django_response
        return super(
            TreeboValidationMixin2,
            self).dispatch(
            request,
            *
            args,
            **kwargs)


class TreeboAPIView2(TreeboValidationMixin2, APIView):
    renderer_classes = (TreeboCustomJSONRenderer,)

    validation_serializer = None
    serializer_object = None
    default_error_code = None

    def initialize_request(self, request, *args, **kwargs):
        request = super(
            TreeboAPIView2,
            self).initialize_request(
            request,
            *
            args,
            **kwargs)
        if settings.DEBUG:
            self.renderer_classes += (BrowsableAPIRenderer,)
        return request

    def dispatch(self, request, *args, **kwargs):
        st = datetime.datetime.now()
        dispatcher_response = super(
            TreeboAPIView2, self).dispatch(
            request, *args, **kwargs)
        et = datetime.datetime.now()
        logger.info(
            "API {api} took {time} ms", extra={
                "api": str(
                    self.__class__.__name__), "time": int(
                    (et - st).total_seconds() * 1000)})
        return dispatcher_response

    def handle_exception(self, exc):

        if isinstance(exc, InvalidRequestException):
            return api_response.invalid_request_error_response(
                exc.errors, self.get_view_name())
        elif isinstance(exc, TreeboException):
            logger.error(
                "treebo exception for class %s  exc %s error code %s" %
                (self.get_view_name(), exc, exc.error_code))
            response = api_response.treebo_exception_error_response(exc)
        else:
            if hasattr(exc, 'msg') and exc.msg:
                message = exc.msg
            else:
                message = exc
            logger.exception(
                "internal exception for class %s exc %s " %
                (self.get_view_name(), message))
            response = api_response.internal_error_response(
                error_codes.get(
                    self.default_error_code)['msg'],
                self.get_view_name(),
                developer_message=message)
        return response

    def get_simple_response_from_error_code_for_booking(
        self, error_code, status_code=HTTP_400_BAD_REQUEST, message=''):
        return api_response.error_response_from_error_code(
            error_code,
            status_code,
            api_name=self.get_view_name(),
            channel=slack_alert.DIRECT_APP_ALERTS,
            message=message
        )

    def get_simple_response_from_treebo_exception_for_booking(self, e):
        return api_response.error_response_from_exception(
            e)

    def get_validation_error_response(self, error_code):
        error_dict = error_codes.get(error_code)
        message = error_dict.get('msg')
        code = error_dict.get('code')
        return api_response.prep_simple_error_response(status_code=HTTP_400_BAD_REQUEST, msg=message,
                                                       code=code)
