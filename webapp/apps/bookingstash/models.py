import logging

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.functional import cached_property
from djutil.models import TimeStampedModel

from apps.bookingstash.tasks import update_third_party_availability
from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.models.room import Room
from . import constants as stash_constants

logger = logging.getLogger(__name__)


class ReservationBooking(TimeStampedModel, DefaultPermissions):
    booking_id = models.CharField(max_length=200)
    group_code = models.CharField(blank=True, max_length=200)
    hotel_code = models.CharField(max_length=200)
    guest_email = models.EmailField(max_length=200)
    guest_first_name = models.CharField(max_length=200)
    guest_last_name = models.CharField(max_length=200)
    guest_phone = models.CharField(max_length=20)
    group_email = models.EmailField(max_length=200)
    group_first_name = models.CharField(max_length=200)
    group_last_name = models.CharField(max_length=200)
    group_phone = models.CharField(max_length=20)
    checkin_date = models.DateField()
    checkout_date = models.DateField()
    adult_count = models.PositiveIntegerField()
    child_count = models.PositiveIntegerField()
    pretax_amount = models.DecimalField(max_digits=20, decimal_places=10)
    tax_amount = models.DecimalField(max_digits=20, decimal_places=10)
    paid_amount = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    group_paid_amount = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    source = models.CharField(max_length=100)
    stay_source = models.CharField(max_length=100, default='')
    group_stay_source = models.CharField(max_length=100, default='sx')
    statuses = stash_constants.BOOKING_STATUS
    status = models.CharField(
        default=stash_constants.RESERVED,
        choices=statuses,
        max_length=100)
    cancellation_datetime = models.DateTimeField(blank=True, null=True)
    modification_datetime = models.DateTimeField(blank=True, null=True)

    class Meta(DefaultPermissions.Meta):
        unique_together = ('booking_id', 'hotel_code',)
        index_together = ('group_code', 'hotel_code',)
        verbose_name = 'Reservation Booking'
        verbose_name_plural = 'Reservation Bookings'

    def __unicode__(self):  # __unicode__ on Python 2
        return '%s %s %s %s' % (self.hotel_code,
                                self.guest_first_name,
                                self.guest_phone,
                                self.status)

    def __str__(self):
        return '%s %s %s %s' % (self.hotel_code,
                                self.guest_first_name,
                                self.guest_phone,
                                self.status)


class ReservationBookingStay(TimeStampedModel, DefaultPermissions):
    from_date = models.DateField()
    to_date = models.DateField()
    room_type_code = models.CharField(max_length=200)
    rate_code = models.CharField(max_length=200)
    room_number = models.CharField(max_length=200)
    pretax_amount = models.DecimalField(max_digits=20, decimal_places=10)
    tax_amount = models.DecimalField(max_digits=20, decimal_places=10)
    reservation = models.ForeignKey(
        ReservationBooking,
        blank=False,
        null=False,
        related_name="booking_stay",
        on_delete=models.DO_NOTHING)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Reservation Booking Stay'
        verbose_name_plural = 'Reservation Booking Stays'

    def __unicode__(self):
        return '%s--%s: %s->%s' % (self.reservation.hotel_code,
                                   self.room_number, self.from_date, self.to_date)

    def __str__(self):
        return '%s--%s: %s->%s' % (self.reservation.hotel_code, self.room_number, self.from_date, self.to_date)


class ReservationBookingMessage(TimeStampedModel, DefaultPermissions):
    reservation = models.ForeignKey(
        ReservationBooking,
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING)
    booking_id = models.CharField(max_length=200)
    group_code = models.CharField(max_length=200)
    hotel_code = models.CharField(max_length=200)
    guest_email = models.EmailField(max_length=200)
    guest_first_name = models.CharField(max_length=200)
    guest_last_name = models.CharField(max_length=200)
    guest_phone = models.CharField(max_length=20)
    checkin_date = models.DateField()
    checkout_date = models.DateField()
    adult_count = models.PositiveIntegerField()
    child_count = models.PositiveIntegerField()
    pretax_amount = models.DecimalField(max_digits=20, decimal_places=10)
    tax_amount = models.DecimalField(max_digits=20, decimal_places=10)
    paid_amount = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    group_paid_amount = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    source = models.CharField(max_length=100, default='')
    stay_source = models.CharField(max_length=100, default='')
    group_stay_source = models.CharField(max_length=100, default='')
    statuses = stash_constants.BOOKING_STATUS
    status = models.CharField(
        default=stash_constants.RESERVED,
        choices=statuses,
        max_length=200)
    cancellation_datetime = models.DateTimeField(blank=True, null=True)
    messageStatuses = stash_constants.MESSAGE_STATUS
    message_status = models.CharField(
        default=stash_constants.COMMIT,
        choices=messageStatuses,
        max_length=200)
    modification_datetime = models.DateTimeField(blank=True, null=True)

    class Meta(DefaultPermissions.Meta):
        pass

    def __unicode__(self):
        return '%s - %s' % (self.booking_id, self.reservation)

    def __str__(self):
        return '%s - %s' % (self.booking_id, self.reservation)


class ReservationBookingMessageStay(TimeStampedModel, DefaultPermissions):
    from_date = models.DateField()
    to_date = models.DateField()
    room_type_code = models.CharField(max_length=200)
    rate_code = models.CharField(max_length=200)
    room_number = models.CharField(max_length=200)
    pretax_amount = models.DecimalField(max_digits=20, decimal_places=10)
    tax_amount = models.DecimalField(max_digits=20, decimal_places=10)
    reservationMessage = models.ForeignKey(
        ReservationBookingMessage,
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING)

    class Meta(DefaultPermissions.Meta):
        pass

    def __unicode__(self):
        return '%s--%s:%s->%s' % (self.reservationMessage.hotel_code,
                                  self.room_number, self.from_date, self.to_date)

    def __str__(self):
        return '%s--%s:%s->%s' % (self.reservationMessage.hotel_code, self.room_number, self.from_date, self.to_date)


class ReservationRestriction(TimeStampedModel, DefaultPermissions):
    restriction_code = models.CharField(max_length=200)
    hotel_code = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField()
    cancellation_date_time = models.DateTimeField(blank=True, null=True)
    room_type_code = models.CharField(max_length=200)
    room_number = models.CharField(max_length=200)
    restrictionStatuses = stash_constants.RESTRICTION_STATUS
    restriction_status = models.CharField(
        default=stash_constants.BLOCKED,
        choices=restrictionStatuses,
        max_length=200)

    class Meta(DefaultPermissions.Meta):
        unique_together = ('restriction_code', 'hotel_code',)

    def __unicode__(self):
        return '%s--%s:%s->%s' % (self.hotel_code,
                                  self.room_number,
                                  self.from_date,
                                  self.to_date)

    def __str__(self):
        return '%s--%s:%s->%s' % (self.hotel_code,
                                  self.room_number,
                                  self.from_date,
                                  self.to_date)


class ReservationRestrictionMessage(TimeStampedModel, DefaultPermissions):
    restriction_code = models.CharField(max_length=200)
    hotel_code = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField()
    cancellation_date_time = models.DateTimeField(blank=True, null=True)
    room_type_code = models.CharField(max_length=200)
    room_number = models.CharField(max_length=200)
    restrictionStatuses = stash_constants.RESTRICTION_STATUS
    restriction_status = models.CharField(
        default=stash_constants.BLOCKED,
        choices=restrictionStatuses,
        max_length=200)
    messageStatuses = stash_constants.MESSAGE_STATUS
    message_status = models.CharField(
        default=stash_constants.COMMIT,
        choices=messageStatuses,
        max_length=200)
    reservationRestriction = models.ForeignKey(
        ReservationRestriction,
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING)

    class Meta(DefaultPermissions.Meta):
        pass

    def __unicode__(self):
        return '%s::%s--%s:%s->%s' % (self.message_status,
                                      self.hotel_code,
                                      self.room_number,
                                      self.from_date,
                                      self.to_date)

    def __str__(self):
        return '%s::%s--%s:%s->%s' % (self.message_status,
                                      self.hotel_code,
                                      self.room_number,
                                      self.from_date,
                                      self.to_date)


class Availability(TimeStampedModel, DefaultPermissions):
    room = models.ForeignKey(Room, on_delete=models.DO_NOTHING)
    availablerooms = models.PositiveIntegerField(default=0)
    date = models.DateField()

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Availability'
        verbose_name_plural = 'Availabilities'
        unique_together = (('date', 'room'),)

    def __unicode__(self):
        return '%s [%s]' % (self.date, self.availablerooms)

    def __str__(self):
        return '%s [%s]' % (self.date, self.availablerooms)



@receiver(
    post_save,
    sender=Availability,
    dispatch_uid="update_availability_axis_room")
def update_availability_axis_room(sender, instance, **kwargs):
    # To Do : Logging required.
    # url = "http://corporates.preprod.treebo.com/ext/api/daywiseInventory/"
    payload = dict(
        hotel_ids=instance.room.hotel.hotelogix_id,
        date=instance.date.strftime("%Y-%m-%d"),
        hx='true'
    )
    update_third_party_availability.delay(payload)
    logging.debug(
        'Triggering third party API for hotel id :{0}'.format(
            payload['hotel_ids']))


class HotelRoomMapper(TimeStampedModel, DefaultPermissions):
    mm_hotel_code = models.CharField(max_length=200)
    mm_room_code = models.CharField(max_length=200)
    room_id = models.IntegerField()

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Hotel Room Mapper'
        verbose_name_plural = 'Hotel Room Mappers'

    @cached_property
    def room_type(self):
        return Room.objects.get(id=self.room_id).room_type

    @cached_property
    def hotelogix_name(self):
        room = Room.objects.get(id=self.room_id)
        return room.hotel.hotelogix_name

    def __unicode__(self):
        return '%s %s %s' % (
            self.room_id, self.mm_hotel_code, self.mm_room_code)

    def __str__(self):
        return '%s %s %s' % (
            self.room_id, self.mm_hotel_code, self.mm_room_code)
