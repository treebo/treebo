from base.views.api import TreeboAPIView

__author__ = 'Rajdeep'


class HotelPolicies(TreeboAPIView):
    @staticmethod
    def getHotelPolicies():
        response = {}
        result = [{"desc": "All guests need to carry a valid photo ID proof at time of check-in",
                   "target_url": "/faq/#important-policies"},
                  {"desc": "Our hotels reserve the right of admission to ensure safety and comfort of other guests. This may include cases such as local residents, unmarried and unrelated couples among others",
                   "target_url": "/faq/#important-policies"},
                  {"desc": "No cancellation fee is charged if booking is cancelled 24 hours prior to check-in",
                   "target_url": "/faq/#cancellation-policy"},
                  {"desc": "Early check-in and late check-out is subject to availability of rooms",
                   "target_url": "/faq#checkin-checkout-policy"},
                  ]

        response['policies'] = result

        return response
