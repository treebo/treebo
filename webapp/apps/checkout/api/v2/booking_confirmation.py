import logging

from django.conf import settings
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST
from django.http import JsonResponse
from rest_framework.response import Response

from apps.checkout.models import BookingRequest
from apps.checkout.tasks import send_booking_confirmation_notification
from apps.checkout.service.booking_confirmation_service import BookingConfirmationService
from apps.checkout.service.itinerary_handler import ItineraryHandler
from apps.common.slack_alert import SlackAlertService
from apps.payments.service.payment_service import PaymentService
from base.views.api import TreeboAPIView
from services.restclient.bookingrestclient import BookingClient

from services.restclient.paymentgatewayclient import PaymentGatewayClient
from apps.checkout.service.analytics.order_analytics import confirm_booking_analytics
from rest_framework.authentication import BasicAuthentication
from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication

logger = logging.getLogger(__name__)


class ConfirmBooking(TreeboAPIView):
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    booking_confirmation_service = BookingConfirmationService()
    checkout_service = ItineraryHandler()

    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    def __payment_gateway_handler(
            self,
            order_id,
            status,
            razorpay_payment_id,
            amount_paid):
        PaymentGatewayClient.razor_pay_handler(
            order_id, status, razorpay_payment_id, amount_paid)

    def __booking_handler(self, booking_request):
        order_id = booking_request.order_id
        BookingClient.confirm_booking(order_id)
        booking_request.status = BookingRequest.COMPLETED
        booking_request.save()
        send_booking_confirmation_notification.delay(booking_request.order_id)
        return order_id

    def __parse_request(self, json_data):
        order_id = json_data.get('order_id')
        status = json_data.get('status', "success")
        razorpay_payment_id = json_data.get('razorpay_payment_id')
        amount_paid = json_data.get('amount_paid')
        return order_id, status, razorpay_payment_id, amount_paid

    def __get_v5_dto_payload(self, order_id, booking_request, pg_payment_id):
        v5_payload = {}
        payment_orders = PaymentService.get_payments_for_booking(order_id)
        if payment_orders and (len(payment_orders) > 0):
            payment_order = payment_orders[0]
        else:
            return v5_payload
        v5_payload['ps_order_id'] = payment_order.ps_order_id
        v5_payload['order_id'] = order_id
        v5_payload['gateway'] = 'razorpay'
        v5_payload['pg_order_id'] = payment_order.pg_order_id
        v5_payload['amount'] = self.booking_confirmation_service.get_external_payment_gateway_share(
            booking_request)
        v5_payload['pg_meta'] = {}
        v5_payload['pg_meta']['pg_payment_id'] = pg_payment_id
        v5_payload['pg_meta']['validate_using_amount'] = True
        return v5_payload

    def post(self, request, *args, **kwargs):
        json_data = request.data
        try:

            order_id, status, razorpay_payment_id, amount_paid = self.__parse_request(
                json_data)
            booking_request = BookingRequest.objects.filter(
                order_id=order_id).last()

            if not booking_request:
                logger.exception(
                    "Unable to confirm booking - invalid order id")
                return JsonResponse({
                    "error": {
                        'msg': "Invalid order id.",
                        'code': 5004
                    }
                }, status=HTTP_400_BAD_REQUEST)

            # building the v5 dto
            v5_payload = self.__get_v5_dto_payload(
                order_id, booking_request, razorpay_payment_id)

            # fetching the author user from the bid-user map
            request_user = request.user
            if booking_request.wallet_applied:
                request_user = self.checkout_service.get_user_by_booking_request(
                    booking_request)

            order_id, reason = self.booking_confirmation_service.confirm_paynow_booking_v5(
                request_user, v5_payload, booking_request)
            order_event_args = confirm_booking_analytics(
                request, booking_request)

            return Response({
                'status': BookingRequest.COMPLETED,
                'order_id': order_id,
                'order_event_args': order_event_args
            })
        except BookingRequest.DoesNotExist:
            logger.exception("Unable to confirm booking")
            return JsonResponse({
                "error": {
                    'msg': "Could not confirm booking. Invalid Order Id",
                    'code': 5004
                }
            }, status=HTTP_404_NOT_FOUND)
        except Exception as e:
            logger.exception("Unable to confirm booking")
            return JsonResponse({
                "error": {
                    'msg': "Could not confirm booking. ",
                    'code': 5004
                }
            }, status=HTTP_500_INTERNAL_SERVER_ERROR)
