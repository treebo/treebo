from rest_framework.status import HTTP_400_BAD_REQUEST

from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes


class InvalidCouponException(TreeboException):
    """
    Handles all exceptions raised during hotel upload process

    """

    def __init__(self, *args):
        super(InvalidCouponException, self).__init__(*args)
        error = error_codes.get(error_codes.INVALID_COUPON_CODE)
        self.message = error['msg']
        self.developer_message = ''
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error['code'])


class AvailableCouponsException(TreeboException):
    """
        Returns exception in Available Coupons API
    """

    def __init__(self, *args):
        super(AvailableCouponsException, self).__init__(*args)

class AvailablePromotionsException(TreeboException):
    """
        Returns exception in Available Coupons API
    """

    def __init__(self, *args):
        super(AvailablePromotionsException, self).__init__(*args)


class HotelAutoApplyCouponOverlapException(TreeboException):
    """
        Returns exception in Available Coupons API
    """

    def __init__(self, *args):
        super(HotelAutoApplyCouponOverlapException, self).__init__(*args)


class HotelAutoApplyCouponInvalidInputException(TreeboException):
    """
        Returns exception in Available Coupons API
    """

    def __init__(self, *args):
        super(HotelAutoApplyCouponInvalidInputException, self).__init__(*args)
