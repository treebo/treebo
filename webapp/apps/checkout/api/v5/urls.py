from django.conf.urls import url
from apps.checkout.api.v5.apply_coupon import ApplyCoupon
from apps.checkout.api.v5.apply_wallet import ApplyWallet
from apps.checkout.api.v5.booking_confirmation import ConfirmBooking
from apps.checkout.api.v5.confirmed_booking_details import ConfirmedBookingDetail
from apps.checkout.api.v5.unverified_pay_at_hotel_booking import IsUnverifiedBookingMade
from apps.checkout.api.v7.itinerary_page_api import ItineraryPageAPI
from apps.checkout.api.v5.pay_at_hotel import PayAtHotel
from apps.checkout.api.v5.pay_now import PayNow
from apps.checkout.api.v5.payment_status import PaymentStatusAPI
from apps.checkout.api.v5.phonepe_status_pending_cron import PhonePeStatusPendingCronAPI
from apps.checkout.api.v5.remove_wallet import RemoveWallet

app_name = 'checkout_v5'
# Todo: Modify add/remove coupon, add/remove wallet api
urlpatterns = [
    url(r'^coupon/$', ApplyCoupon.as_view(), name='apply-coupon-v5'),
    url(r'^coupon/remove/$', ApplyCoupon.as_view(), name='remove-coupon-v5'),
    url(r'^itinerary/$', ItineraryPageAPI.as_view(), name='itinerary-page-v7'),
    url(r'^paynow/$', PayNow.as_view(), name='paynow-v5'),
    url(r'^confirmbooking/$', ConfirmBooking.as_view(),name='confirmbooking-v5'),
    url(r'^payathotel/$', PayAtHotel.as_view(), name='payathotel-v5'),
    url(r'^wallet/apply/$', ApplyWallet.as_view(), name='wallet-apply-v5'),
    url(r'^wallet/remove/$', RemoveWallet.as_view(), name='wallet-remove-v5'),
    url(r'^phonepe/status/pending/cron/$', PhonePeStatusPendingCronAPI.as_view(), name='phonepe-status-penging-cron-v5'),
    url(r'^payment/status/(?P<order_id>[\w-]+)/', PaymentStatusAPI.as_view(), name='payment-status-v5'),
    url(r'^confirmed-booking/(?P<order_id>[\w-]+)/', ConfirmedBookingDetail.as_view(),
        name='confirmed-booking-v5'),
    url(r'^payathotel/is-unverified-booking-made/$', IsUnverifiedBookingMade.as_view(),
        name='payathotel-is-unverified-booking-made'),
]