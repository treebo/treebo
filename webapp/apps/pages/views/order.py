import datetime
import json
import logging
import traceback
from decimal import Decimal

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404
from django.utils import dateformat
from django.utils.dateformat import DateFormat
from ipware.ip import get_ip

from apps.bookings.models import Booking
from apps.checkout.models import BookingRequest
from apps.common import utils as commonUtils
from apps.common.utils import round_to_two_decimal, round_to_nearest_integer
from apps.hotels import utils as hotel_utils
from apps.hotels.api.v1.get_policies import HotelPolicies
from apps.pages.helper import PageHelper
from apps.pricing import dateutils
from base.views.template import TreeboTemplateView
from common.constants import common_constants as const
from apps.common import date_time_utils
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.direction import Directions
from data_services.directions_repository import DirectionsRepository
from services.maps import Maps

logger = logging.getLogger(__name__)


class Order(TreeboTemplateView):

    directions_service = RepositoriesFactory.get_directions_repository()
    # template_name = "mobile/confirmation/order.html"
    template_name = "confirmation/index.html"

    def __populate_common_context(self, request, booking, channel):
        logger.info("Booking hotel id: %s", booking.hotel)
        hotel = booking.hotel
        hotelogixHotelName = hotel.hotelogix_name

        grand_total = booking.total_amount
        room = booking.room
        days = (booking.checkout_date - booking.checkin_date).days
        guest = booking.adult_count

        logger.debug("Grand Total in Confirmation Page: %s", grand_total)

        checkinDate, checkoutDate = booking.checkin_date, booking.checkout_date
        checkin = DateFormat(booking.checkin_date).format(
            'd M') + " " + DateFormat(booking.checkin_date).format('Y')
        checkout = DateFormat(booking.checkout_date).format(
            'd M') + " " + DateFormat(booking.checkout_date).format('Y')

        checkin_display = dateformat.format(booking.checkin_date, "jS M'y")
        checkout_display = dateformat.format(booking.checkout_date, "jS M'y")

        imageUrl = room.hotel.get_showcased_image_url()

        context = {}
        if channel == 'desktop':
            bookingCode, bookingDate, discountAmount, actualTotalCost, imageParams, orderCheckinDate, \
                orderCheckoutDate, roomCount, shareImageParams, adults, children, \
                couponCode, pretaxPrice, totalTax, roomConfig = self.__collateDesktopData(
                    booking, checkinDate, checkoutDate, grand_total, request)
            voucherAmount = 0
            if not discountAmount:
                booking_request = BookingRequest.objects.get(
                    booking_id=booking.id)
                if booking_request.voucher_amount:
                    assumedPaidAmount = float(booking_request.voucher_amount)
                    voucherAmount = commonUtils.round_to_two_decimal(
                        assumedPaidAmount)

            directions = hotel_utils.get_directions_from_landmark(hotel.id)
            staticMapUrl = hotel_utils.generate_static_map_url(
                hotel, directions, showPolyline=True)

            context.update({
                "directions_obj": directions, "static_map_url": staticMapUrl,
                "pretax_price": pretaxPrice, 'total_tax': totalTax,
                "discount_amount": discountAmount, "coupon_code": couponCode,
                "room_config": roomConfig, "voucherAmount": voucherAmount,
                'styles': ['desktop/css/confirmation.css'],
                'scripts': ['desktop/js/confirmation.js'],
            })
        else:
            bookingCode, bookingDate, discountAmount, actualTotalCost, imageParams, orderCheckinDate, \
                orderCheckoutDate, roomCount, shareImageParams, adults, children, \
                couponCode, pretaxPrice, totalTax, roomConfig = self.__collateDesktopData(
                    booking, checkinDate, checkoutDate, grand_total, request)

            context.update({
                'styles': ['mobile/css/confirmation.css'], 'scripts': ['mobile/js/confirmation.js'],
            })

        policies = HotelPolicies.getHotelPolicies()
        kwargs = hotel_utils.get_hotel_url_kwargs(hotel)
        hdUrl = reverse(
            'pages:hotel-details',
            kwargs=kwargs) + "?checkin={0}&checkout={1}&roomconfig={2}".format(
            checkinDate,
            checkoutDate,
            roomConfig)

        confirm_order_args = self.__confirmOrderAnalytics(
            context,
            booking,
            days,
            discountAmount,
            actualTotalCost,
            hotelogixHotelName,
            couponCode,
            request,
            room,
            roomCount)

        import urllib.request
        import urllib.parse
        import urllib.error
        url = '//maps.google.com/?' + \
            urllib.parse.urlencode({'q': str(hotel.latitude) + ',' + str(hotel.longitude)})
        context.update({'page_name': 'Order',
                        "checkin": checkin_display,
                        "checkout": checkout_display,
                        "order_checkin_date": orderCheckinDate,
                        "hotel": hotel,
                        "order_checkout_date": orderCheckoutDate,
                        "room": room,
                        "room_count": roomCount,
                        "days": days,
                        "guest": guest,
                        "booking": booking,
                        "room_type": room.room_type_code,
                        "policies": policies['policies'],
                        'customer_care_number': const.CUSTOMER_CARE_NUMBER,
                        "adults": adults,
                        "children": children,
                        "guest_name": booking.guest_name,
                        "guest_email": booking.guest_email,
                        "guest_mobile": booking.guest_mobile,
                        "comments": booking.comments,
                        "booking_code": bookingCode,
                        "location_url": url,
                        "image_url": imageUrl,
                        "actual_total_cost": actualTotalCost,
                        "booking_date": bookingDate,
                        "image_params": imageParams,
                        "share_image_params": shareImageParams,
                        "hd_url": hdUrl,
                        'jsvars': str(json.dumps({'lastPurchaseDate': str(booking.created_at),
                                                  'city': hotel.city.name,
                                                  'hotel': hotel.name,
                                                  'booking_code': bookingCode,
                                                  'confirm_order_analytics': confirm_order_args,
                                                  'hotelId': hotel.id,
                                                  'totalAmount': float(actualTotalCost),
                                                  'userId': booking.user_id_id,
                                                  'checkInDate': checkin,
                                                  'checkOutDate': checkout}))})

        paymentText = "PAY NOW"
        if "Not Paid" in booking.payment_mode:
            paymentText = "PAH"

        logger.debug('Payment Mode in Confirmation Page: %s', paymentText)
        context['pah'] = True if paymentText == 'PAH' else False

        try:
            ip = get_ip(request)
        except Exception:
            ip = ''
        analytics_event = {
            "type": paymentText,
            'hotel_id': hotel.id,
            'hotel_name': hotel.name,
            'checkin': checkin,
            'checkout': checkout,
            'num_nights': days,
            'adults': adults,
            'kids': children,
            'rooms': roomCount,
            'room_type': room.room_type,
            'total_price': str(actualTotalCost),
            'discount_coupon_applied': couponCode,
            'discount_value': str(Decimal(discountAmount).quantize(2)),
            'special_prefs': booking.comments,
            'user_login_state': 'LOGGEDIN' if request.user and request.user.is_authenticated() else 'LOGGEDOUT',
            'user_signup_channel': request.COOKIES.get('user_signup_channel'),
            'user_signup_date': request.COOKIES.get('user_signup_channel'),
            'source': channel,
            'client_ip': ip,
            'orderId': booking.order_id,
            "email": booking.guest_email,
            "phone": booking.guest_mobile,
            "userId": booking.user_id_id,
            "is_mobile": request.is_mobile,
            "timestamp": datetime.datetime.now().strftime('%Y-%m-%d %H:%M %p'),
            'event_date': datetime.datetime.now().strftime('%Y-%m-%d'),
            'event_time': datetime.datetime.now().strftime('%H:%M %p'),
            'hotelId': hotel.id,
        }
        context.update({
            "ADWORDS_CONVERSION_ID": settings.ADWORDS_CONVERSION_ID,
            "ADWORDS_CONVERSION_LABEL": settings.ADWORDS_CONVERSION_LABEL,

        })
        return context, analytics_event

    def getData(self, request, args, kwargs):
        """

        :param request:
        :return:
        """
        try:
            error = ""
            order_id = kwargs.get("order_id")
            if not order_id:
                logger.warn("Missing Order Id in query param")
                error = const.ERROR_MESSAGE
                raise Http404
            booking = Booking.objects.filter(
                order_id=order_id).order_by("-id").first()
            if not booking:
                logger.warn("Invalid Order ID received")
                raise Http404
            context, analytics_event = self.__populate_common_context(
                request, booking, 'desktop')
            if not self.is_refresh(context, request, booking):
                commonUtils.trackAnalyticsServerEvent(
                    request, 'Booking Successful', analytics_event, channel=booking.channel)
            commonUtils.trackAnalyticsServerEvent(
                request, 'Confirmation Page Served', {}, channel=booking.channel)
            return context
        except Exception as exc:
            logger.exception("Exception on order page %s" % exc)

    def is_refresh(self, context, request, booking):
        is_refreshed = request.COOKIES.get(
            'order_id', None) == str(
            booking.order_id)
        if is_refreshed:
            context.update({
                'is_refresh': True
            })
        else:
            context.update({
                'is_refresh': False
            })
        return is_refreshed

    def getMobileData(self, request, args, kwargs):
        """

        :param request:
        :return:
        """
        try:
            error = ""
            order_id = kwargs.get("order_id")
            if not order_id:
                logger.warn("Missing Order Id in query param")
                error = const.ERROR_MESSAGE
                raise Http404
            booking = Booking.objects.filter(
                order_id=order_id).order_by("-id").first()
            if not booking:
                logger.warn("Invalid Order ID received")
                raise Http404
            policies = HotelPolicies.getHotelPolicies()
            context, analytics_event = self.__populate_common_context(
                request, booking, 'mobile')
            if not self.is_refresh(context, request, booking):
                commonUtils.trackAnalyticsServerEvent(
                    request, 'Booking Successful', analytics_event, channel=booking.channel)
            commonUtils.trackAnalyticsServerEvent(
                request, 'Confirmation Page Served', {}, channel=booking.channel)
            return context
        except Exception as exc:
            logger.exception("Exception on order page %s" % exc)

    def __collate_common_data(
            self,
            booking,
            checkin,
            checkout,
            grandTotal,
            request):
        bookingCode = self.__getBookingCode(booking)
        orderCheckinDate = date_time_utils.build_date(checkin)
        orderCheckoutDate = date_time_utils.build_date(checkout)
        booking.guestMobile = booking.guest_mobile[:4] + "-" + \
            booking.guest_mobile[4:7] + "-" + booking.guest_mobile[7:]
        roomCount = len(booking.room_config.split(","))
        bookingDate = date_time_utils.build_date(booking.created_at)
        actualTotalCost = round_to_nearest_integer(grandTotal)

        width, height = commonUtils.Utils.get_image_size(
            request.flavour, "order")
        imageParams = commonUtils.Utils.build_image_params(
            width, height, const.FIT_TYPE, const.CROP_TYPE, const.QUALITY)
        width, height = commonUtils.Utils.get_image_size(
            request.flavour, "share")
        shareImageParams = commonUtils.Utils.build_image_params(
            width, height, const.FIT_TYPE, const.CROP_TYPE, const.QUALITY)
        adults, children, roomRequired = PageHelper.parseRoomConfig(
            booking.room_config)
        roomConfig = PageHelper.getRoomConfigData(booking.room_config)
        couponCode = booking.coupon_code
        return bookingCode, bookingDate, actualTotalCost, imageParams, orderCheckinDate, \
            orderCheckoutDate, roomCount, shareImageParams, adults, children, roomConfig, couponCode

    def __collateDesktopData(
            self,
            booking,
            checkin,
            checkout,
            grandTotal,
            request):
        bookingCode, bookingDate, actualTotalCost, imageParams, orderCheckinDate, \
            orderCheckoutDate, roomCount, shareImageParams, adults, children, roomConfig, couponCode = self.__collate_common_data(
                booking, checkin, checkout, grandTotal, request)

        pretaxPrice = round_to_two_decimal(booking.pretax_amount)
        totalTax = round_to_two_decimal(booking.tax_amount)
        discountAmount = round_to_two_decimal(max(booking.discount, 0))

        return bookingCode, bookingDate, discountAmount, actualTotalCost, imageParams, orderCheckinDate, \
            orderCheckoutDate, roomCount, shareImageParams, adults, children, \
            couponCode, pretaxPrice, totalTax, roomConfig

    def __collate_mobile_data(
            self,
            booking,
            checkin,
            checkout,
            grandTotal,
            request):
        return self.__collate_common_data(
            booking, checkin, checkout, grandTotal, request)

    def __getDesktopDirectionLandmark(self, hotelId):
        directions = self.directions_service.get_directions_ordered_by_landmark_popularity(
            hotel_id=hotelId)
        directions = directions[0]
        # directions = Directions.objects.filter(hotel=hotelId).order_by("-landmark_obj__popularity")[
        #              :1].get()
        text = directions.directions_for_email_website
        text_list = text.split("\\n")
        directions.text = text_list
        logger.debug("Direction Text = %s" % directions.text)
        return directions

    def __generateStaticMapUrl(self, hotel, directions):
        origin_latlong = str(hotel.latitude) + "," + str(hotel.longitude)
        dest_latlong = str(directions.landmark_obj.latitude) + "," + str(
            directions.landmark_obj.longitude)
        map_obj = Maps()
        directions.distance = map_obj.calculateDistanceWithoutGMAP(
            origin_latlong, dest_latlong)
        static_map_url = map_obj.getStaticMapUrl(
            origin_latlong, dest_latlong, directions, "400x450")
        logger.debug("static_map_url=%s" % str(static_map_url))
        return static_map_url

    def __getBookingCode(self, booking):
        if booking.group_code:
            return booking.group_code
        elif booking.booking_code:
            return booking.booking_code.replace("|", "")
        else:
            return ""

    def __getDirectionFromLandmark(self, hotelId):
        directions_obj = self.directions_service.get_all_directions_for_hotel(
            hotel_id=hotelId)
        # directions_obj = Directions.objects.filter(hotel=hotelId)
        for obj in directions_obj:
            text = obj.directions_for_email_website
            text_list = text.split("\\n")
            obj.text = text_list
            logger.debug("Direction Text = %s" % obj.text)
        return directions_obj

    def __confirmOrderAnalytics(
            self,
            context,
            booking,
            days,
            discount_amount,
            grand_total,
            hotelogix_hotel_name,
            order_coupon,
            request,
            room,
            room_count):
        """

        :param apps.bookings.models.Booking booking:
        :param days:
        :param discount_amount:
        :param grand_total:
        :param hotelogix_hotel_name:
        :param order_coupon:
        :param request:
        :param room:
        :param room_count:
        :return:
        """
        try:
            order_event_args = {
                "orderId": booking.order_id,
                "total": float(discount_amount) +
                float(grand_total),
                "revenue": float(grand_total),
                "shipping": 0,
                "tax": 0,
                "discount": float(discount_amount),
                "coupon": order_coupon,
                "email": booking.guest_email,
                "phone": booking.guest_mobile,
                "userid": booking.user_id_id,
                "currency": "INR",
                "payment_mode": booking.payment_mode,
                "products": [
                    {
                        "id": hotelogix_hotel_name,
                        "sku": hotelogix_hotel_name,
                        "name": hotelogix_hotel_name,
                        "price": float(grand_total) +
                        float(discount_amount),
                        "quantity": days *
                        room_count,
                        "category": room.hotel.city.name}],
                "hrental_id": booking.hotel_id,
                "hrental_startdate": dateutils.date_to_ymd_str(
                    booking.checkin_date),
                "hrental_enddate": dateutils.date_to_ymd_str(
                    booking.checkout_date),
                "hrental_totalvalue": float(
                    round_to_nearest_integer(
                        booking.total_amount)),
            }
            if not self.is_refresh(context, request, booking):
                commonUtils.trackConfirmOrder(
                    request,
                    "Completed Order",
                    order_event_args,
                    channel=booking.channel)
            return order_event_args
        except Exception as err:
            logger.debug(
                "__confirmOrderAnalytics Error %s " %
                traceback.format_exc())
            return {}
