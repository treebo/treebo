from decimal import Decimal
from services.restclient.rewardserviceclient import RewardServiceClient


class RewardService():
    reward_service_client = RewardServiceClient()

    def get_wallet_info(self, user, application_code=None):
        wallet_types = self.reward_service_client.get_wallet_types()
        wallet_info = []
        for wallet_type in wallet_types:
            wallet_balance, wallet_points, wallet_spendable_amount, wallet_debit_percentage, \
            debit_sources, debit_mode, loyalty_type = self.reward_service_client.get_wallet_info(
                user, wallet_type, application_code)
            wallet_info.append({
                "type": wallet_type,
                "total_balance": wallet_balance,
                "total_points": wallet_points,
                "spendable_amount": wallet_spendable_amount,
                "debit_sources": debit_sources,
                "debit_percentage": wallet_debit_percentage,
                "debit_mode": debit_mode,
                "loyalty_type": loyalty_type
            })
        return wallet_info

    def get_wallet_statement(self, user, wallet_type):
        wallet_statement_dto = self.reward_service_client.get_wallet_statement(
            user_id=user.auth_id, wallet_type=wallet_type)
        return wallet_statement_dto

    def get_wallet_balance(self, user, application_code):
        wallet_applied, total_wallet_balance, total_wallet_points, \
            total_wallet_spendable_amount, debit_sources, debit_percentage, debit_mode, loyalty_type = \
            False, 0, 0, 0, None, None, None, None
        if user and user.is_authenticated():
            for wallet_info in self.get_wallet_info(user, application_code):
                wallet_applied = True if wallet_info['debit_sources'] and (application_code in wallet_info['debit_sources']) else False
                total_wallet_balance += wallet_info['total_balance']
                total_wallet_points += wallet_info['total_points']
                total_wallet_spendable_amount += wallet_info['spendable_amount']
                debit_sources = wallet_info['debit_sources']
                debit_percentage = wallet_info['debit_percentage']
                debit_mode = wallet_info['debit_mode']
                loyalty_type = wallet_info['loyalty_type']

        if total_wallet_spendable_amount == 0:
            wallet_applied = False

        return bool(
            wallet_applied), total_wallet_balance, total_wallet_points, \
               total_wallet_spendable_amount, debit_sources, debit_percentage, debit_mode, loyalty_type

    def get_wallet_breakup(
            self,
            apply_wallet,
            total_wallet_spendable_amount,
            sell_price):
        wallet_deductable_amount = Decimal(
            total_wallet_spendable_amount) if total_wallet_spendable_amount <= sell_price else sell_price
        wallet_deduction = wallet_deductable_amount
        if not apply_wallet:
            wallet_deduction = Decimal(0.00)
        return wallet_deductable_amount, wallet_deduction
