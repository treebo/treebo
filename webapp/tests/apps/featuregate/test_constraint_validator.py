from django.test import TestCase

from apps.featuregate import constraint_validator


class ConstraintValidatorTest(TestCase):
    def setUp(self):
        self.validator = constraint_validator.ConstraintValidator(
            '2016-06-20', '2016-06-20', 20, '123.34', 'Jaipur', 'Bangalore')

    def test_checkin_date(self):
        self.assertTrue(
            self.validator._ConstraintValidator__checkinDateValidator(
                'eq', '2016-06-20', None))
        self.assertFalse(
            self.validator._ConstraintValidator__checkinDateValidator(
                'eq', '2016-06-21', None))
        self.assertTrue(
            self.validator._ConstraintValidator__checkinDateValidator(
                'ls', '2016-07-21', None))
        self.assertFalse(
            self.validator._ConstraintValidator__checkinDateValidator(
                'ls', '2016-06-18', None))
        self.assertTrue(
            self.validator._ConstraintValidator__checkinDateValidator(
                'between', '2016-06-18', '2016-06-22'))
        self.assertFalse(
            self.validator._ConstraintValidator__checkinDateValidator(
                'between', '2016-06-18', '2016-05-22'))
        self.assertTrue(
            self.validator._ConstraintValidator__checkinDateValidator(
                'in', '2016-06-18, 2016-06-20, 2016-06-28', None))

    def test_hotel_id(self):
        self.assertTrue(
            self.validator._ConstraintValidator__hotelValidator(
                'eq', '20', None))
        self.assertFalse(
            self.validator._ConstraintValidator__hotelValidator(
                'eq', '21', None))
        self.assertTrue(
            self.validator._ConstraintValidator__hotelValidator(
                'ls', '22', None))
        self.assertFalse(
            self.validator._ConstraintValidator__hotelValidator(
                'ls', '19', None))
        self.assertTrue(
            self.validator._ConstraintValidator__hotelValidator(
                'between', '17', '25'))
        self.assertFalse(
            self.validator._ConstraintValidator__hotelValidator(
                'between', '21', '27'))
        self.assertTrue(
            self.validator._ConstraintValidator__hotelValidator(
                'in', '10, 23, 20, 33, 12, 21, 9', None))
        self.assertFalse(
            self.validator._ConstraintValidator__hotelValidator(
                'in', '2, 3, 19, 210', None))

    def test_locality(self):
        self.assertTrue(
            self.validator._ConstraintValidator__localityValidator(
                'eq', 'Jaipur', None))
        self.assertFalse(
            self.validator._ConstraintValidator__localityValidator(
                'eq', 'jaipur', None))
        self.assertTrue(
            self.validator._ConstraintValidator__localityValidator(
                'in', 'Jaipur, Bangalore, New Delhi', None))
        self.assertFalse(
            self.validator._ConstraintValidator__localityValidator(
                'in', 'Bangalore, New Delhi', None))

    def test_cart_cost(self):
        self.assertTrue(
            self.validator._ConstraintValidator__cartCostValidator(
                'eq', '123.34', None))
        self.assertFalse(
            self.validator._ConstraintValidator__cartCostValidator(
                'eq', '123.344', None))
        self.assertTrue(
            self.validator._ConstraintValidator__cartCostValidator(
                'ls', '123.345', None))
        self.assertFalse(
            self.validator._ConstraintValidator__cartCostValidator(
                'ls', '123.32', None))
        self.assertTrue(
            self.validator._ConstraintValidator__cartCostValidator(
                'between', '123.30', '123.35'))
        self.assertFalse(
            self.validator._ConstraintValidator__cartCostValidator(
                'between', '123.35', '123.36'))
        self.assertTrue(
            self.validator._ConstraintValidator__cartCostValidator(
                'in', '12, 123.34, 9', None))
        self.assertFalse(
            self.validator._ConstraintValidator__cartCostValidator(
                'in', '34.34, 19,12, 210', None))
