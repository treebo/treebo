

import $ from 'jquery';
import _ from 'underscore';
import {Observable} from "rx";
import "slick-carousel";
import BaseView from './treebolibs/baseview';
import Calendar from './libs/calendar';
import moment from 'moment';
import queryString from 'query-string';

import React from 'react';
import {render} from 'react-dom';
import Discount from './widgets/discount';
import PriceDetail from './widgets/pricedetails';
import {methods,store} from './widgets/search';
import '../css/details.less';

const treejax = require('./treebolibs/treejax');
const parsed = queryString.parse(location.search);


let $loader = $("#bookingLoader");


const Price = React.createClass({

    componentWillReceiveProps(nextProps){
        if(nextProps.data.total_price != this.props.data.total_price){
            // Code to animate numbers
            // var finalPrice = Math.round(nextProps.data.room_price + nextProps.data.taxes - nextProps.data.discount),
            let finalPrice = nextProps.data.final_price;

            let tempPrice = finalPrice-50;
            let counter = tempPrice;
            const interval = setInterval(() => {
                if(counter<=finalPrice){
                    $('.js-totalprice').html(counter++);
                }else{
                    clearInterval(interval);
                    if($('.js-totalprice').html() == $('.js-rackrate').html()){
                        $('.js-rackratecontainer').addClass('invisible')
                    }else{
                        $('.js-rackratecontainer').removeClass('invisible')
                    }
                }
             },10);
        };
    },



    render(){
        const totalPrice = Math.round(this.props.data.room_price + this.props.data.taxes);
        const grand_total = this.props.data.final_price;
        //if (this.props.data.discount == 0) {
        //    grand_total = Math.round(this.props.data.total_price);
        //} else {
        //    grand_total = Math.round(this.props.data.room_price + this.props.data.taxes - this.props.data.discount);
        //}
        let crossRateClass = (parseInt(this.props.data.rack_rate,10) <= totalPrice) ? "invisible" : "";
        crossRateClass = `js-rackratecontainer greyTxt line-through ${crossRateClass}`;

        const chkIn = new Date(methods.getCheckInDate());
        const chkOut = new Date(methods.getCheckOutDate());
        const timeDiff = Math.abs(chkIn.getTime() - chkOut.getTime());
        const days = Math.ceil(timeDiff / (1000 * 3600 * 24));
        const dayStr = days > 1 ? `${days} Nights` : `${days} Night`;
        var discount_value;
        if (this.props.data.voucherAmount) {
            discount_value = this.props.data.voucherAmount;
        } else {
            discount_value = this.props.data.discount;
        }

        return (
            <div className="flex-row">
                <div className="sidebar__totalcost_right_block text-right pos-rel">
                    <div className="sidebar__totalcost sidebar__bold sidebar__mini mt20">Grand Total for {dayStr}</div>
                    <div className="sidebar__mini grayTxt" id="inclusiveTax">(Inclusive of all taxes)</div>
                </div>
                <div className="text-right sidebar__totalcost_left_block">
                    <div className={crossRateClass}>
                        <i className="icon-rupee mr5"></i><span className="js-rackrate">{this.props.data.rack_rate}</span>
                    </div>
                    <div className="sidebar__totalprice ">
                        <i className="icon-rupee mr5 price__rupee"></i><span className="analytics-totalprice js-totalprice">{this.props.data.final_price}</span>
                    </div>
                    <div>
                        <div className="hotel__price__breakup hover-popup" id="price-popup">
                            <div className="title sidebar__mini mt10 farebreakuptitle link">View Fare Breakup</div>
                            <div className="hotel__price__breakup__popup hover-popup__body hover-popup__twisty hover-popup__twisty--bottom">
                                <div className="breakup__item flex-row flex--space-between">
                                    <div className="breakup__label">Room Price</div>
                                    <div><i className="icon-rupee mr5"></i> {this.props.data.room_price}</div>
                                </div>
                                <div className="breakup__item flex-row flex--space-between">
                                    <div className="breakup__label">Taxes</div>
                                    <div><i className="icon-rupee mr5 "></i> {this.props.data.taxes}</div>
                                </div>
                                <div className="breakup__item flex-row flex--space-between">
                                    <div className="breakup__label">Discount</div>
                                    <div> - <i className="icon-rupee mr5 "></i> {discount_value}</div>
                                </div>
                                <div className="breakup__item breakup__item--total flex-row flex--space-between flex--align-center">
                                    <div className="breakup__label">Total</div>
                                    <div className=""><span className="total-price"><i className="icon-rupee mr5 "></i>{grand_total}</span> <br /><span className="inc">Incl all taxes</span></div>
                                </div>

                                <div className="text-center breakup__per-night-price link" id="price-detail-link">view daily price breakup</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

$(".sidebar").on('click',"#price-detail-link",e => {
        $("#price-detail").removeClass('hide');
});

let hdmodel = {
    roomType:"",
    currentcoupon:""
};
rt.hdmodel = hdmodel;

// let baseBookingUrl = '/booking';
// let baseBookingUrl = '/rest/v1/checkout/initiate';
let baseBookingUrl = rt.quickbook_base_url;

let fadeCarouselConfig = {
        fade:true,
	    arrows: true,
	    dots:false,
	    asNavFor: '.slider-nav',
        infinite:false
}

let slideCarouselConfig = {
        arrows: true,
	    dots:false,
        slidesToShow:3,
        infinite:false
};


let thumbNailWidth = 60;

$('.js-gallery-nav').slick({
    asNavFor: '.js-main-gallery',
    arrows: true,
    dots:false,
    slidesToShow:7,
    focusOnSelect: true,
    infinite:false,
    lazyLoad: 'ondemand'
});

$('.js-main-gallery').slick({
    arrows: true,
    infinite:false,
    fade:true,
    lazyLoad: 'ondemand'
});

let DetailsView = Object.create(BaseView);

// -------Google Map-----------
// DetailsView.initMap = () => {
//     let myOptions = {
//             zoom:10,
//             center:new google.maps.LatLng(12.914409,77.636736),
//             mapTypeId: google.maps.MapTypeId.ROADMAP
//     };
//     let map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
//     let marker = new google.maps.Marker({
//         map: map,
//         position: new google.maps.LatLng(12.914409,77.636736)
//     });
//     let infowindow = new google.maps.InfoWindow({
//         content:'<strong>Treebo Hotels</strong><br> BDA Complex, HSR, Bangalore<br>'
//     });

//     google.maps.event.addListener(marker, 'click', function(){
//             infowindow.open(map,marker);
//         });
//     infowindow.open(map,marker);
// }

DetailsView.pageLoaded = () => {
    let $bannerCarousel = $(".js-details-carousel");
    let $bannerCarouselContainer = $('.banner-carousel');
    let $close = $('.js-banner-close');
    let $similarCarousel = $('.js-similar-carousel');
    let winHeight = window.innerHeight;

    $bannerCarousel.height(winHeight-thumbNailWidth);
    $bannerCarouselContainer.height(winHeight);

    //$bannerCarousel.slick(fadeCarouselConfig);

    const closeEvents = Rx.Observable.fromEvent($close, 'click');

	const subscription = closeEvents.subscribe(e => {
  		$bannerCarouselContainer.addClass('hide');
	});

    let path;
    if(rt['previous_path']) {
        path = rt['previous_path'].split("/");
    } else {
        path = [];
    }

    const backToString = path[1];
    if(backToString && backToString != "hotels")
        $(".search-back-text").html(` Back to ${backToString}`);
    else
        $(".search-back").addClass('hide');

    $similarCarousel
    .on('init', slick => {
        $similarCarousel.fadeIn(3000);
    })
    .slick({
        arrows: true,
        // fade: true,
        // focusOnSelect: true,
        dots: false,
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 3
    });

    $('.js-room-carousel').slick(slideCarouselConfig);





    DetailsView.initCalendar();

    DetailsView.initAboutHotel();
    DetailsView.bindScroll();
    DetailsView.bindGallery();
    DetailsView.bindSideBar();

    DetailsView.bindRoomOptionsWidget();
    DetailsView.bindBookNow();
    DetailsView.bindRoomTypes();

    methods.initRoomWidget();

    DetailsView.initRoomWidget();


    $('body').bind('roomconfigchanged datechanged roomtypechanged',DetailsView.paramChangeHandler);
    $('body').bind('discount:applied',(event, param) => {

        const data = param.priceobj;
        const obj = {
            rack_rate:DetailsView.truncatePrice(data.rackRate),
            total_price:data.sellRate,
            discount:data.discount,
            voucherAmount:data.voucherAmount,
            taxes:data.tax,
            room_price:data.price,
            final_price:data.final_price,
        };
        hdmodel.currentcoupon = param.code;
        DetailsView.renderPriceWidget(obj);
    })


    $('body').bind('discount:removed',(event, param) => {
        hdmodel.currentcoupon="";
        hdmodel.discount = param.value;
        hdmodel.voucherAmount = param.voucher_value;
        hdmodel.taxes = param.priceobj.tax;
        hdmodel.final_price = param.priceobj.final_price;
        DetailsView.renderPriceWidget({
            rack_rate:hdmodel.rack_rate,
            total_price:hdmodel.total_price,
            discount:hdmodel.discount,
            voucherAmount:hdmodel.voucherAmount,
            taxes:hdmodel.taxes,
            room_price:hdmodel.room_price,
            final_price:hdmodel.final_price
        });
    });

    DetailsView.makeParams();
    DetailsView.markSelected();


    // To close room-type drop down
    $(window).on('click',e => {
        const $elem = $(e.target);
        if($elem.parents('.sidebar__roomselection').length==0){
            $('.sidebar__roomlist').addClass('hide');
        }
    });

    if(backToString == "hotels")
        $(".search-back").addClass('hide');
    $(".search-back").on("click", e => {
            window.location.href = rt['previous_url'];
    });

    //google map
    // google.maps.event.addDomListener(window, 'load', DetailsView.initMap);
    // logger.log(" Hotel detail page loaded");
}

DetailsView.markSelected = () => {
    var $room;
    let roomtype;
    if(!parsed.roomtype){
        roomtype = $($('.sidebar__roomoption')[0]).attr('roomtype');
    }else{
        roomtype = parsed.roomtype;
    }

    var $room = $(`.room-types__room[roomtype=${roomtype}]`);
    if(!$room.hasClass('unavailable')){
        $room.find('.js-select').addClass('room-types__selected');
        $room.find('.js-select i').removeClass('hide');
    }
}

DetailsView.initRoomWidget = () => {
    hdmodel.roomType = parsed.roomtype || "";
    let str = "";
    let price = "";
    if(hdmodel.roomType){
        price = $(`.room-types__room[roomtype="${hdmodel.roomType}"]`).find('.js-roomprice').attr('roomprice');
        price = Math.round(parseFloat(price));
        str = `${hdmodel.roomType} (&#8377; ${price} Per Night)`;
    }else{
        hdmodel.roomType = $($('.sidebar__roomoption')[0]).find('.sidebar__roomname').attr('roomname');
        price = $($('.sidebar__roomoption')[0]).find('.js-pricepernight').attr('price');
        price = Math.round(parseFloat(price));
        str = `${hdmodel.roomType} (&#8377;${price} Per Night)`;

    }
    $('.sidebar__selectedroomtype').html(str);
}


DetailsView.paramChangeHandler =() =>{
    //DetailsView.selectRoom();
    DetailsView.makeParams();
    setTimeout(DetailsView.changeUrl,700);
}

DetailsView.changeUrl = () => {
    // queryString.parse(location.search)
    const newParams = _.extend({},{roomconfig:methods.getRoomConfigUrl(),roomtype:hdmodel.roomType,checkin:methods.getCheckInDate(),checkout:methods.getCheckOutDate()});
    const newUrl = `${location.pathname}?${$.param(newParams)}`;
    history.pushState(null, null, newUrl);
}

DetailsView.renderPriceWidget = (data) => {
    // TODO remove this hardcoding.
    render(<Price data={data}/>, $('#priceWidget')[0]);
}

DetailsView.renderDiscount = (data) => {
    data.url = '/api/v1/pricing/availability?';
    render(<Discount data={data}/>, $('#applyCoupon')[0]);
}

DetailsView.renderPricePopup = (data) => {
    render(<PriceDetail data={data}/>, $('#price-detail')[0]);
}


DetailsView.refreshRooms = (data) => {

    const roomdetails = data.roomdetails;

    $('.sidebar__roomoption').removeClass('sidebar__sold sidebar__roomoption--selected');

    // Refresh left side rooms
    $('.room-types__room').each((index, room) => {
        const $room = $(room);
        const roomType = $room.attr('roomtype');

        const roomdata = _.find(roomdetails,room => roomType == room.roomtype);

        if(roomdata){
            const $sidebarRoomType = $(`.sidebar__roomoption[roomtype=${roomType}]`);


            if(roomdata.available){
                DetailsView.markAavailable($room);
                $sidebarRoomType.removeClass('sidebar__sold');

           }else{
                DetailsView.markUnavailable($room);
                $sidebarRoomType.addClass('sidebar__sold');
           }

           if(roomdata.rackrate){
                var rackrate = DetailsView.truncatePrice(roomdata.rackrate);
                $room.find('.js-roomprice').html(`&#8377;${rackrate}`);
                $room.find('.js-roomprice').attr('roomprice',rackrate);
                const $sideBarPriceElem = $(`.sidebar__roomoption[roomtype=${roomType}]`).find('.js-pricepernight');

               $sideBarPriceElem.attr('price',rackrate);
               $sideBarPriceElem.html(`&#8377;${rackrate}`);

           }
       }

    });

    DetailsView.selectSideBarRoomOption();
    setTimeout(() => {
        DetailsView.updateSelectedRoomPrice();
    },0)
}

DetailsView.updateSelectedRoomPrice = () => {
    let price = $('.sidebar__roomoption--selected').find('.js-pricepernight').attr('price');
    price = DetailsView.truncatePrice(price);
    $('.sidebar__selectedroomtype').html(`${hdmodel.roomType} (&#8377; ${price} Per Night)`);
}


DetailsView.truncatePrice = (price) => {
    if(typeof price=="number"){
        price = Math.round(price);
    }
    price = `${price}`;
    return (price.indexOf('.')!=-1) ? (price).substring(0,price.indexOf('.')) : price;
}

DetailsView.markAavailable = ($room) => {
    $room.removeClass('unavailable');
    $room.find('.js-select').removeClass('hide');
    $room.find('.js-unavailable').addClass('hide');
}

DetailsView.markUnavailable = ($room) => {
    $room.addClass('unavailable');
    $room.find('.js-unavailable').removeClass('hide');
    $room.find('.js-select').addClass('hide');
}

DetailsView.selectSideBarRoomOption = () => {
    $(`.sidebar__roomoption[roomtype=${hdmodel.roomType}]`).addClass('sidebar__roomoption--selected');
}


DetailsView.bindRoomTypes = () => {

    const moreRoom = Rx.Observable.fromEvent($('.js-room-more'), 'click');
    const moreSub = moreRoom.subscribe(e => {
        const $this = $(e.target);
        const $img = $this.closest('.room-types__room').find('.room-types__imgwrapper');
        if(!$this.hasClass('more')){
             $img.hide();
             $this.html('&nbsp;less');
             $this.closest('p').find('.js-extra-text').removeClass('hide');
             $this.addClass('more');
        }else{
            $img.show();
            $this.html('&nbsp;more');
            $this.closest('p').find('.js-extra-text').addClass('hide');
            $this.removeClass('more');
        }
        e.preventDefault();
    });


    const events = Rx.Observable.fromEvent($('.room-types__select'), 'click');
    const subscription = events.subscribe(e => {

        const $room = $(e.target).closest('.room-types__room');

        if($room.find('.js-select').hasClass('room-types__selected')){
            return;
        }

        $.each($('.room-types__room'),(i,v)=>{
            $(v).find('.js-select').removeClass('hide room-types__selected');
            $(v).find('.js-select i').addClass('hide');
        })


        $room.find('.js-select').addClass('room-types__selected');
        $room.find('.js-select i').removeClass('hide');


        hdmodel.roomType = $.trim($room.find('.room-types__category').attr('category'));

        //console.log($('.js-roomprice').attr('roomprice'));

        const price = parseFloat($room.find('.js-roomprice').attr('roomprice'));

        $('.sidebar__selectedroomtype').html(`${hdmodel.roomType} (&#8377; ${price} Per Night)`);

        $(".sidebar__roomoption").removeClass('sidebar__roomoption--selected');

        DetailsView.selectSideBarRoomOption();

        $('body').trigger('roomtypechanged');
    });



}

DetailsView.initAboutHotel = () => {
    const $about = $('.js-about');
    const content = $about.html();
    const showChar = 250;
    const ellipsestext = "...";
    const moretext = "more";

    if(content.length > showChar) {
        const c = content.substr(0, showChar);
        const h = content.substr(showChar, content.length - showChar);
        var html = `${c}<span class="moreellipses">${ellipsestext}&nbsp;</span><span class="morecontent"><span class="hide">${h}</span><a href="javascript:void(1);" class="about__more morelink link">${moretext}</a></span>`;
        $about.html(html);
    }
    $(".morelink").click(() => {
        $('.morecontent span').removeClass('hide');
         $(".morelink").addClass('hide');
         $('.moreellipses').addClass('hide');
        return false;
    });
}

DetailsView.toggleBook = () => {
    $('.js-book').toggleClass('sidebar__book-disabled');
}


window.jq = $;

DetailsView.bindBookNow = () => {
    let bookEvents = Rx.Observable.fromEvent($('.js-book'), 'click');
    let subscription = bookEvents.subscribe(e => {
        if($(".js-book").hasClass('sidebar__book-disabled')){
            return;
        }
        $loader.removeClass('hide');


        let {adults ,children} = methods.getRoomConfig();
        let {rooms} = methods.getStore(); // Its an array - {adults:1,children:1}
        let checkIn = methods.getCheckInDate();
        let roomType = hdmodel.roomType;
        let roomconfig = methods.getRoomConfigUrl();
        let checkOut = methods.getCheckOutDate();



        let queryParams = `hotel_id=${window.rt.hotelId}&checkin=${checkIn}&checkout=${checkOut}&roomconfig=${roomconfig}&roomtype=${roomType}&couponcode=${hdmodel.currentcoupon}`;

        window.location = `${baseBookingUrl}?${queryParams}`;
    });
}

DetailsView.bindRoomOptionsWidget = () =>{
    const events = Rx.Observable.fromEvent($('.sidebar__roomselection'), 'click');
    const subscription = events.subscribe(e => {
        $('.sidebar__roomlist').toggleClass('hide');
    });

    const optionEvents = Rx.Observable.fromEvent($('.sidebar__roomoption'), 'click');
    const optionEventsSubscription = optionEvents.subscribe(e => {
        $('.sidebar__roomlist').toggleClass('hide');
        let $option = $(e.target).closest('.sidebar__roomoption');
        hdmodel.roomType = $option.find('.sidebar__roomname').attr('roomname');
        let price = $option.find('.js-pricepernight').attr('price');

        $.each($('.room-types__room'),(index, room) => {
            const $room = $(room);
            if(!$room.hasClass('unavailable') && $room.attr('roomtype')==hdmodel.roomType){
                $room.find('.js-select').addClass('room-types__selected');
                $room.find('.js-select i').removeClass('hide');
            }else if(!$room.hasClass('unavailable') && $room.attr('roomtype')!=hdmodel.roomType){
                $room.find('.js-select').removeClass('room-types__selected');
                $room.find('.js-select i').addClass('hide');
            }
        });


        price = Math.round(parseFloat(price));

        $('.sidebar__selectedroomtype').html(`${hdmodel.roomType} (&#8377; ${price} Per Night)`);

        $('body').trigger('roomtypechanged');
    });
}


DetailsView.bindSideBar = () => {
    const $left = $('.main_wrapper');
    const $right = $("sidebar_wrapper");
    const $window = $(window);
    const $sidebar = $('.sidebar');
    const mainWrapperBottomEdge = $('.main_wrapper').offset().top + $('.main_wrapper').height();
    const sidebarBottomEdge = $sidebar.offset().top + $sidebar.height();

    $sidebar.css({
        position:'fixed',
        top:$('.main_wrapper').offset().top
    })


    $window.scroll(() => {
        throttled();
    });

    const updatePosition = function(){
        const mainWrapperBottomEdge = $('.main_wrapper').offset().top + $('.main_wrapper').height();
        const sidebarBottomEdge = $sidebar.offset().top + $sidebar.height();

        if( DetailsView.isSimilarTreeboInView('topOnly') == false && DetailsView.isFooterInView()==false){  // going up
            //console.log("Fixed triggered");
            $sidebar.css({
                position:'fixed',
                top:$('.main_wrapper').offset().top
            })
        }else if(mainWrapperBottomEdge <= sidebarBottomEdge){
            //console.log('going up');
            $sidebar.css({
                position:'absolute',
                top:  $('.sidebar_wrapper').height() - $sidebar.height()
            });
        }
    };
    const throttled = _.throttle(updatePosition, 10);
}

$.fn.inView = function(inViewType){
    const viewport = {};
    viewport.top = $(window).scrollTop();
    viewport.bottom = viewport.top + $(window).height();
    const bounds = {};
    bounds.top = this.offset().top;
    bounds.bottom = bounds.top + this.outerHeight();
    switch(inViewType){
      case 'bottomOnly':
        return ((bounds.bottom <= viewport.bottom) && (bounds.bottom >= viewport.top));
      case 'topOnly':
        return ((bounds.top <= viewport.bottom) && (bounds.top >= viewport.top));
      case 'both':
        return ((bounds.top >= viewport.top) && (bounds.bottom <= viewport.bottom));
      default:
        return ((bounds.top >= viewport.top) && (bounds.bottom <= viewport.bottom));
    }
};

DetailsView.isSimilarTreeboInView = (edge) =>{
    if(!$('.similar')) return;
    return $('.similar').inView(edge);
}

DetailsView.isFooterInView = () =>{
    if(!$('.sitefooter')) return;
    return $('.sitefooter').inView('topOnly');
}

DetailsView.isMainTreeboInView = () =>{
    if(!$('.main_wrapper')) return;
    return $('.main_wrapper').inView('bottomOnly');
}


DetailsView.initCalendar = () => {
    methods.initCalander();
    methods.setDefaultValuesFromCookie();
    methods.initSearchControls();
}


DetailsView.bindScroll = () => {
    let $body = $(window);
    $body.on('scroll',() => {
        DetailsView[($body.scrollTop() > 715) ? 'headerOff' : 'headerOn']();
    });
}

// TODO : Refactor into pub sub pattern
DetailsView.headerOff = () => {
    $('.page__header').css('visibility','hidden');
    $('.head').addClass('head__visible');
}

DetailsView.headerOn = () => {
    $('.page__header').css('visibility','visible');
    $('.head').removeClass('head__visible');
}

DetailsView.bindGallery = ()=>{

    $('.main__expand').click(() => {

        const currSlide = parseInt(jq('.js-main-gallery').find('.slick-active').attr('data-slick-index'),10);

        $('.banner-carousel').removeClass('hide');
        $(".js-details-carousel").slick({
            fade:true,
            arrows: true,
            dots:false,
            asNavFor: '.slider-nav',
            infinite:false,
            initialSlide:currSlide
        });
        $('.slider-nav').slick({
            asNavFor: '.js-details-carousel',
            arrows: true,
            dots:false,
            slidesToShow:6,
            focusOnSelect: true,
            infinite:false,
            initialSlide:currSlide
        });
    });
}

window.methods = methods;
//window._ = _;
DetailsView.makeParams = () => {


    const $bookBtn = $('.js-book');

    $bookBtn.removeClass('sidebar__book-disabled');

    setTimeout(() => {
        DetailsView.fetchPrice({couponcode:hdmodel.currentcoupon,roomconfig:methods.getRoomConfigUrl(),roomtype:hdmodel.roomType,checkin:methods.getCheckInDate(),checkout:methods.getCheckOutDate(), hotelid: rt.hotelId});
    },0);

}


DetailsView.showError = () => {
    $('.sidebar__error__container').show();
}

DetailsView.hideError = () => {
    $('.sidebar__error__container').hide();
}

// Needs to be refactored
DetailsView.fetchPrice = (params) => {

    DetailsView.hideError();

    DetailsView.toggleBook();

    const query = $.param(params);
    $loader.removeClass('hide');
    treejax({
        url: `/api/v1/pricing/availability?${query}`,
        type : 'get',
    }).then((data, status, xhr) => {

        hdmodel.currentcoupon = data.couponcode;

        DetailsView.refreshSidebarDetail(data);

        DetailsView.refreshRooms(data);

        DetailsView.updateModel(data);

        DetailsView.renderPriceWidget({
            rack_rate:hdmodel.rack_rate,
            total_price:hdmodel.total_price,
            discount:hdmodel.discount,
            voucherAmount:data.voucherAmount,
            taxes:hdmodel.taxes,
            room_price:hdmodel.room_price,
            final_price:data.final_price
        });

        const pricedetailsData = {nights:data.nights};
        DetailsView.renderPricePopup(pricedetailsData);

        DetailsView.renderDiscount({
            'query_param': {
                'couponDesc':data.couponDesc,
                'discount':data.discount,
                'voucherAmount':data.voucherAmount,
                'autopromo':data.autopromo,
                'roomconfig':methods.getRoomConfigUrl(),
                'roomtype':hdmodel.roomType,
                'couponcode':data.couponcode,
                'checkin': methods.getCheckInDate(),
                'checkout': methods.getCheckOutDate(),
                'total_cost': data.sellRate,
                'total_pretax_cost':30,
                'hotelid': window.rt.hotelId,
                'csrfmiddlewaretoken': ''//$csrfMiddlewareToken
            },
            'couponDesc':data.couponDesc,
            'discount':data.discount,
            'voucherAmount':data.voucherAmount,
            'autopromo':data.autopromo,
            'roomconfig':methods.getRoomConfigUrl(),
            'roomtype':hdmodel.roomType,
            'couponcode':data.couponcode,
        });

        if(data.priceError){
            $('.sidebar__error').html(data.priceError);
            DetailsView.showError();
            DetailsView.refreshSidebarDetail();
        }else{
            $('.js-book').removeClass('sidebar__book-disabled');
        }




    },
    treeboError => {
        $('.sidebar__error').html(treeboError.message);
        $('.js-book').addClass('sidebar__book-disabled');
        DetailsView.showError();
        DetailsView.refreshSidebarDetail();
    }).finally(() => {
        $loader.addClass('hide');

    });
}

DetailsView.updateModel = (data) => {
    hdmodel = _.extend(hdmodel,{
        rack_rate:DetailsView.truncatePrice(data.rackRate),
        total_price:data.sellRate,
        discount:data.discount,
        voucherAmount:data.voucherAmount,
        taxes:data.tax,
        room_price:data.price,
        final_price:data.final_price,
    });
}


DetailsView.refreshSidebarDetail = (data) => {
    if(data && data.available){
        DetailsView.showSidebarDetail();
    }else{
        DetailsView.hideSidebarDetail();
    }
}

DetailsView.showSidebarDetail = () => {
    $('.js-sidebar-detail').removeClass('hide');
}

DetailsView.hideSidebarDetail = () => {
    $('.js-sidebar-detail').addClass('hide');
}

let analyticsConfigfunc = function(){
    return  {
        hotel_id:rt.hotelId,
        hotel_name:rt.hotelName,
        tags:rt.tags,
        locality: rt.locality,
        city:rt.cityName,
        hrental_id: rt.hotelId,
        hrental_pagetype: 'offerdetail',
        hrental_startdate: rt.getCheckin(),
        hrental_enddate: rt.getCheckout(),
        hrental_totalvalue: $('.js-totalprice').text(),
    }
}


let detailView = DetailsView.setup({ name:'Hotel Detail Page', widgets:[], analyticsConfigfunc});
