# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0003_auto_20160331_1119'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='cancellation_datetime',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
