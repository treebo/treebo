import logging

from apps.auth.constants import UserContactType
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.social_login_dto import SocialLoginDTO
from apps.auth.serializer.user_serializer import UserSerializer
from apps.auth.services.cookie_service import CookieService
from apps.auth.services.social_login_service import SocialLoginService
from apps.auth.services.user_profile_service import UserProfileService
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import UPSClientException
from apps.profiles.service.user_service import UserService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class GoogleLoginApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    social_login_service = SocialLoginService()
    user_service = UserService()
    cookie_service = CookieService()
    user_profile_service = UserProfileService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            social_login_dto = SocialLoginDTO(data=request.data)
            if not social_login_dto.is_valid():
                logger.error("google login invalid request %s ", request.data)
                return api_response.invalid_request_error_response(
                    social_login_dto.errors, GoogleLoginApi.__name__)
            user, token, user_details = self.social_login_service.google_login(
                social_login_dto.data)
            self.user_service.login_user(request, user)
            user_dto = UserSerializer(instance=user)
            user_data = user_dto.data
            user_data['token'] = token
            response = api_response.prep_success_response(user_data)
            self.cookie_service.set_user_info_in_cookie(response, user)
            self.user_profile_service.create_user_and_user_profile(user_details, UserContactType.EMAIL)
        except UPSClientException as e:
            logger.error("Creating User failed due to {error}".format(error=e))
            return response
        except TreeboException as e:
            logger.exception("treebo google login exception %s ", request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception(
                "internal error google login exception %s ",
                request.data)
            response = api_response.internal_error_response(error_codes.get(
                error_codes.LOGIN_FAILED)['msg'], GoogleLoginApi.__name__)
        return response
