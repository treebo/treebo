from rest_framework import serializers


class HotelSearchDTO(serializers.Serializer):
    hotelogix_id = serializers.CharField(max_length=20, required=False)
    longitude = serializers.DecimalField(
        max_digits=25, decimal_places=15, required=False)
    latitude = serializers.DecimalField(
        max_digits=25, decimal_places=15, required=False)
    radius = serializers.DecimalField(
        max_digits=25, decimal_places=15, required=False)


class HotelSearchQuery:
    def __init__(self, hotel_id, locality_name=None, landmark_name=None, city_name=None,
                 state_name=None, country_name=None, location_type=None, nearby=None,
                 category=None, amenity=None, meta_keys=None, latitude=None, longitude=None,
                 radius=None, distance_cap=None, channel=None, hotel_name=None,
                 checkin_date=None, checkout_date=None, room_config=None, meta_hotel_id=None,
                 search_channel=None):
        self.hotel_id = hotel_id
        self.locality_name = locality_name
        self.landmark_name = landmark_name
        self.city_name = city_name
        self.state_name = state_name
        self.country_name = country_name
        self.location_type = location_type
        self.nearby = nearby
        self.category = category
        self.amenity = amenity
        self.meta_keys = meta_keys
        self.latitude = latitude
        self.longitude = longitude
        self.radius = radius
        self.distance_cap = distance_cap
        self.channel = channel
        self.hotel_name = hotel_name
        self.checkin_date = checkin_date
        self.checkout_date = checkout_date
        self.room_config = room_config
        self.meta_hotel_id = meta_hotel_id
        self.search_channel = search_channel
