import logging

from rest_framework.response import Response
from rest_framework.views import APIView

from apps.common.file_utils import FileUtils
from apps.pricing import parsers
from apps.pricing import xmlrendrer
from apps.third_party.service.google_hotel_ads.gha_reconcilation_report import GenerateGHACSVService
from apps.third_party.service.google_hotel_ads.hotel_list import HotelListFeedService
from apps.third_party.service.google_hotel_ads.point_of_sale import POSService
from apps.third_party.service.google_hotel_ads.query_message import QueryMessageService
from apps.third_party.service.google_hotel_ads.transaction_pricing import TransactionService
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class UpdatePOS(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        try:
            pos_service = POSService()
            pos_feed_response = pos_service.pos_service()
            response = FileUtils.generate_xml_file(
                pos_feed_response, 'point_of_sale.xml')
            return response
        except Exception:
            logger.exception(
                'Error occured in updating latest Point of Sale file')
            return Response({"success": False}, status=400)


class UpdateHotelList(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        try:
            hotel_list_servce = HotelListFeedService()
            hotel_feed_response = hotel_list_servce.generate_hotel_list_feed()
            response = FileUtils.generate_xml_file(
                hotel_feed_response, 'google-local.xml')
            return response
        except Exception:
            logger.exception(
                'Error occured in updating latest Hotel List Feed file')
            return Response({"success": False}, status=400)


class CreateQueryMessage(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        try:
            query_message_service = QueryMessageService()
            query_message_response = query_message_service.generate_query_message()
            response = FileUtils.generate_xml_file(
                query_message_response, 'query_message.xml')
            return response
        except Exception:
            logger.exception('Error occured in creating Query Message file')
            return Response({"success": False}, status=400)


class Transaction(APIView):
    parser_classes = (parsers.XMLParser,)
    renderer_classes = (xmlrendrer.XMLRenderer,)

    def post(self, request):
        try:
            logger.info("transaction api request data %s", request.data)
            transaction_service = TransactionService()
            transaction_response = transaction_service.read_from_query_xml(
                request.data)
            response = FileUtils.generate_xml_file(
                transaction_response, 'transaction_message.xml')
            return response
        except Exception:
            logger.exception(
                'Error occured in creating Transaction and Pricing file')
            return Response({"success": False}, status=400)


class GenerateGHACSV(APIView):
    def get(self, request, *args, **kwargs):
        try:
            logger.info("GHA - generating csv and mailing")
            generate_ta_csv = GenerateGHACSVService()
            generate_ta_csv.create_and_mail_csv()
            return Response({"success": True}, status=200)
        except Exception:
            logger.exception(
                'Error occured in creating Reconcilation Report file for GHA')
            return Response({"success": False}, status=400)
