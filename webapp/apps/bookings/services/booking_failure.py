import base64
import zlib

import logging
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from thsc.crs.entities.hotel import Hotel as CRSHotel

from apps.bookings import utils
from apps.bookings.models import Booking
from apps.bookings.services.booking_utils import logger, get_exception_mail
from apps.common import metrics_logger
from base.middlewares.set_request_id import get_current_user
from services.restclient.treebo_notificationservice import TreeboNotificationService
from services.tasks import exception_mailer

logger = logging.getLogger(__name__)


class BookingFailureHandler():
    @staticmethod
    def handle_addons(data):
        """
        :param data:
        :return:
        [
            {
                "failure_mail": {
                    "booking_id": booking_id,
                    "reason": reason,
                }
            },
            {
                "exception_mail": {
                    "subject": subject,
                    "message": message,
                    "html": html
                }
            },
            {
                'cancellation_mail': {
                'subject': 'Cancellation Failed for Order Id: {0}'.format(order_id),
                'body': 'Cancellation request failed for Order ID: {0}. Reason: Unable to get order details from '
                        ''.format(order_id),
                'from': ,
                'to': settings.CANCELLATION_FAILURE_EMAIL_LIST
            }
        ]
        """
        logger.info('handle addons called with data : {0}'.format(data))

        errors = BookingFailureHandler.validate_or_get_failure_data(data)

        if not errors:
            logger.exception(
                'No failure info available for data {0}'.format(data))
            return

        data = errors['data']
        try:
            fail_mail = data.get("track_failure")
            if fail_mail:
                _booking_id = fail_mail['booking_id']
                _reason = fail_mail['reason']
                _subject = fail_mail['subject']
                BookingFailureHandler.track_failure(
                    _booking_id, _reason, _subject)
        except Exception as e:
            logger.exception(
                "error while sending track_failure in handle addons")

        try:
            exc_mail = data.get("exception_mail")
            if exc_mail:
                _subject = exc_mail['subject']
                _message = exc_mail['message']
                _html = exc_mail['html']
                _html = zlib.decompress(base64.b64decode(_html))
                _html = get_string_output_from_input(_html)
                logger.info("In sending eception mail, subject is %s", _html)
                logger.info(
                    'sending exception mail with subject {0}'.format(_subject))
                exception_mailer(_subject, _message, _html)
        except Exception as e:
            logger.exception(
                "error while sending exception_mail in handle addons")

        try:
            cancel_mail = data.get("cancellation_mail")
            if cancel_mail:
                _subject = cancel_mail['subject']
                _subject = get_string_output_from_input(_subject)
                _body = cancel_mail['body']
                _body = get_string_output_from_input(_body)
                _from = settings.SERVER_EMAIL
                _to = settings.CANCELLATION_FAILURE_EMAIL_LIST
                TreeboNotificationService().send_email(
                    emails=_to, content=_body, subject=_subject)
        except Exception as e:
            logger.exception(
                "error while sending cancellation_mail in handle addons")

    @staticmethod
    def track_failure(booking_id, reason, subject,
                      email_list=settings.RESERVATION_FAILURE_EMAIL_LIST):
        """
        :param booking_id:
        :param reason:
        :param email_list:
        :return:
        """
        logger.info(
            'track failure called for booking_id {0}'.format(booking_id))
        try:
            booking = Booking.objects.select_related(
                'hotel').get(pk=booking_id)
        except ObjectDoesNotExist:
            logger.exception(
                'Booking not found with id {0}'.format(booking_id))
            return
        analytics_data = {
            'page': 'Initiate Booking',
            'hotel_id': booking.hotel.id,
            'coupon_code': booking.coupon_code,
            'pay_at_hotel': booking.payment_mode,
            'guest_name': booking.guest_name,
            'guest_email': booking.guest_email,
            'guest_phone': booking.guest_mobile,
            'user_signup_channel': booking.channel,
            'gstin': booking.organization_taxcode,
            'organization_name': booking.organization_name,
            'organization_address': booking.organization_address,
            'point_of_failure': reason,
        }
        utils.unableToSaveAnalytics(
            get_current_user(),
            analytics_data,
            booking.channel)
        metrics_logger.log(reason, analytics_data)
        subject = subject + str(booking.order_id)
        message = "Reservation attempt by customer failed with following details: \n\n" + \
                  booking.get_mail_representation()
        TreeboNotificationService().send_email(
            emails=email_list,
            content=message,
            subject=subject)
        logger.info("track_failure sent mail %s to users %s ",
                    subject, email_list)

    @staticmethod
    def validate_or_get_failure_data(data):
        """
        {
            'job_api': 'initiate_booking_async',
            'errors': [{
                u 'status_code': 500,
                u 'message': u 'No rateIds found',
                u 'data': {}
            }, {
                u 'status_code': 500,
                u 'message': u 'No rateIds found',
                u 'data': {}
            }, {
                u 'status_code': 500,
                u 'message': u 'No rateIds found',
                u 'data': {}
            }],
            'data': {
                u 'booking_id': 175365,
                u 'user_id': None,
                u 'request_id': u 'b5a62027e01b4adcb27463223de23365'
            },
            'job_id': u '149879067426-0',
            'tag': u 'initiate_booking'
        }
        :return:
        """
        booking_id = data['data'].get('booking_id', None)
        logger.info("Booking id received is %s", str(booking_id))
        try:
            booking = Booking.objects.select_related(
                'hotel').get(pk=booking_id)
        except ObjectDoesNotExist:
            logger.exception(
                'No Booking found in handle addons with id {0}'.format(booking_id))
            return
        track_failure = BookingFailureHandler.validate_or_get_track_mail(
            data['errors'], booking)
        exception_mail = BookingFailureHandler.validate_or_get_exception_mail(
            data['errors'], booking)
        cancellation_mail = None
        for error in data['errors']:
            if error.get('data'):
                if error['data'].get('cancellation_mail', None):
                    cancellation_mail = error['data']['cancellation_mail']
                    break
        return {
            'data': {
                'track_failure': track_failure,
                'exception_mail': exception_mail,
                'cancellation_mail': cancellation_mail
            }
        }

    @staticmethod
    def validate_or_get_exception_mail(errors_list, booking):
        """
        :param errors_list:
        :param booking:
        :return:
        """
        for error in errors_list:
            if error.get('data', None):
                if error['data'].get('exception_mail', None):
                    return error['data']['exception_mail']
        return get_exception_mail(
            Exception(),
            "Unable to create booking in Hx. Order Id: {0}".format(
                booking.order_id))

    @staticmethod
    def validate_or_get_track_mail(errors_list, booking):
        """
        :param errors_list:
        :param booking:
        :return:
        """
        for error in errors_list:
            if error.get('data', None):
                if error['data'].get('track_failure', None):
                    return error['data']['track_failure']

        if settings.USE_CRS and CRSHotel.is_managed_by_crs(booking.hotel.cs_id):
            return BookingFailureHandler.get_track_failure(booking, "Unable to create booking in CRS",
                                                           "URGENT: Reservation Attempt Failure in "
                                                           "CRS (PMS). Order ID: ")
        else:
            return BookingFailureHandler.get_track_failure(booking, "Unable to create booking in Hx",
                                                           "URGENT: Reservation Attempt Failure. Order ID: ")

    @staticmethod
    def handler_internal_failure(errors):
        logger.info("handler_internal_failure %s  ", errors)
        for error_name, error_data in list(errors.items()):
            try:
                logger.info(
                    "error_name %s and error_data %s ",
                    error_name,
                    error_data)
                if error_name == "track_failure":
                    _booking_id = error_data['booking_id']
                    _reason = error_data['reason']
                    _subject = error_data['subject']
                    BookingFailureHandler.track_failure(
                        _booking_id, _reason, _subject)
                if error_name == 'exception_mail':
                    _subject = error_data['subject']
                    _message = error_data['message']
                    _html = error_data['html']
                    _html = zlib.decompress(base64.b64decode(_html))
                    _html = get_string_output_from_input(_html)
                    logger.info(
                        'sending exception mail with subject {0}'.format(_subject))
                    exception_mailer(_subject, _message, _html)
            except Exception as e:
                logger.exception(
                    " error while sending exceptional mail in handler_internal_failure %s ,%s ",
                    error_name,
                    error_data)

    @staticmethod
    def get_track_failure(
        booking,
        reason,
        subject='URGENT: Reservation Attempt Failure. Order ID: '):
        """
        :param booking:
        :param reason:
        :param subject:
        :return:
        """
        return {
            'track_failure': {
                "booking_id": booking.id,
                "reason": reason,
                "subject": subject,
            }
        }


def get_string_output_from_input(input):
    if type(input) == bytes:
        return input.decode()
    return input
