# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bookings', '0006_auto_20160413_1119'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='user',
            field=models.ForeignKey(
                related_name='userrequests',
                blank=True,
                to=settings.AUTH_USER_MODEL,
                null=True,
                on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='user_login_state',
            field=models.CharField(
                max_length=200,
                null=True,
                blank=True),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='user_signup_channel',
            field=models.CharField(
                max_length=200,
                null=True,
                blank=True),
        ),
    ]
