import logging

from apps.common.exceptions.custom_exception import WifiConfigNotFound
from apps.hotels.service.hotel_service import HotelService
from apps.hotels.service.wifi_service import WifiService
from apps.search.services.google_location_service import LocaiontService
from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class HotelSearchService(object):
    location_service = LocaiontService()
    hotel_service = HotelService()
    wifi_service = WifiService()

    def get_hotel_near_by(self, hotel_search_dto):
        """
        :param (dict)hotel_search_dto:
        :return:
        """
        hotelogix_id = hotel_search_dto.get('hotelogix_id', '')
        lat = float(hotel_search_dto.get('longitude', 0.0))
        lng = float(hotel_search_dto.get('latitude', 0.0))
        radius = float(hotel_search_dto.get('radius', 0.0))
        nearest_hotel = None
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotels = hotel_repository.filter_hotels(
            hotelogix_id=hotelogix_id)
        hotels_list = list(hotels)
        hotel = hotels_list[0] if hotels_list else None
        if hotel and self.is_bhaifi_enabled(hotel):
            nearest_hotel = hotel
        if not nearest_hotel:
            state = self.location_service.get_state(lat, lng)
            hotels = hotel_repository.filter_hotels(
                state=state)
            nearest_hotel = self.find_nearest_wifi_hotel(
                hotels, radius, lat, lng)
        return nearest_hotel

    def find_nearest_wifi_hotel(self, hotels, radius, lat, lng):
        min_distance = 0
        nearest_hotel = None
        for hotel in hotels:
            if not self.is_bhaifi_enabled(hotel):
                continue
            h_lat = float(hotel.longitude)
            h_log = float(hotel.latitude)
            distance = self.location_service.get_distance_between_two_coordinate_in_meters(
                h_lat, h_log, lat, lng)
            logger.info(
                "hotel(%s,%s) my (%s,%s) hotel %s distance %s ",
                h_lat,
                h_log,
                lat,
                lng,
                hotel,
                distance)
            if min_distance == 0 or min_distance > distance:
                min_distance = distance
                nearest_hotel = hotel
        logger.info(
            "nearest_hotel %s min_distance %s ",
            nearest_hotel,
            min_distance)
        if nearest_hotel:
            return self.check_for_range(nearest_hotel, min_distance, radius)
        return None

    def check_for_range(self, nearest_hotel, min_distance, radius):
        config = self.wifi_service.get_wifi_common_config()
        if not config:
            raise WifiConfigNotFound('need to add bhaifi config in db')
        global_radius = config.search_radius_in_meter
        if min_distance <= min(global_radius, radius):
            return nearest_hotel
        logger.info(" nearest hotel %s , not in range %s min : %s (g:%s,w:%s) ",
                    nearest_hotel, min_distance, min(global_radius, radius), global_radius, radius)
        return None

    def is_bhaifi_enabled(self, hotel):
        if not hasattr(hotel, 'wifilocationmap'):
            logger.info(" hotel %s do not have bhaifi ", hotel)
            return False
        wifilocation = hotel.wifilocationmap
        if not wifilocation.is_enabled:
            logger.info(" hotel %s bhaifi is in active ", hotel)
            return False
        return True
