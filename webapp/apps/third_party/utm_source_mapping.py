from django.conf import settings
from apps.third_party.third_party_csv_formater import format_trivago_csv, format_ixigo_csv, \
    build_ixigo_csv_file, build_trivago_csv_file


UTM = {
    "trivago": {"utm_source": settings.TRIVAGO_EXTERNAL_UTM_SOURCE,
                "utm_medium": "Meta",
                "utm_campaign": "cpc",
                "csv_path_name": "trivago.csv",
                "csv_mapper": format_trivago_csv,
                "csv_file_creator": build_trivago_csv_file
                },
    "ixigo": {"utm_source": settings.IXIGO_UTM_SOURCE,
              "utm_medium": "Meta",
              "utm_campaign": "",
              "csv_path_name": "ixigo.csv",
              "csv_mapper":format_ixigo_csv,
              "csv_file_creator": build_ixigo_csv_file
              }
}
