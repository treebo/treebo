import json
import logging

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.checkout.models import BookingRequest
from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.common.json_encoder import DecimalEncoder
from apps.pricing.exceptions import RequestError
from apps.pricing.services.price_request_dto import PriceRequestDto
from apps.pricing.services.pricing_service import SoaError
from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.pricing.transformers.coupon_api_transformer import CouponAPITransformer
from apps.pricing.transformers.itinerary_api_transformer import ItineraryAPITransformer
from base.views.api import TreeboAPIView
from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

logger = logging.getLogger(__name__)


class RemoveCouponSerializer(serializers.Serializer):
    bid = serializers.IntegerField()


class RemoveCoupon(TreeboAPIView):
    validationSerializer = RemoveCouponSerializer
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    pricing_service = PricingServiceV2()
    price_response_transformer = ItineraryAPITransformer()

    def get(self, request, *args, **kwargs):
        """
        Gets the discount for a booking id and coupon code.
        couponcode -- Coupon Code
        bid -- BookingRequest Id
        """
        validated_data = self.serializerObject.validated_data
        booking_request_id = validated_data['bid']
        booking_request = BookingRequest.objects.filter(
            pk=booking_request_id).first()

        if not booking_request:
            return Response({"error": error_message(
                codes.INVALID_BOOKING)}, status=400)

        current_user = getattr(request, 'user', None)
        logged_in_user = True if current_user and current_user.is_authenticated() else False

        try:
            room_configs = booking_request.room_config.split(",")
            price = self.get_price(
                booking_request,
                room_configs,
                logged_in_user=logged_in_user)

        except RequestError as e:
            return Response({"error": error_message(
                codes.INVALID_COUPON)}, status=HTTP_400_BAD_REQUEST)
        except SoaError as e:
            logger.exception(
                " SoaError Exception occurred while applying coupon code using SOA API")
            return Response({"error": error_message(
                codes.INVALID_COUPON)}, status=e.status)
        except Exception as e:
            logger.exception(
                "unknown Exception occurred while applying coupon code using SOA API")
            return Response({"error": error_message(
                codes.INVALID_COUPON)}, status=HTTP_500_INTERNAL_SERVER_ERROR)

        logger.info(
            "Pricing API response in remove_coupon API for bid: %s => %s",
            booking_request.id,
            json.dumps(
                price,
                cls=DecimalEncoder))
        self._update_booking_request_with_prices(
            booking_request,
            price["selected_rate_plan"]["price"],
            logged_in_user=logged_in_user)

        logger.info(
            "Prices stored in booking_request with bid: %s => [pretax_price: %s, mm_promo: %s, "
            "coupon_discount: %s, member_discount: %s, tax_amount: %s, total_amount: %s]",
            booking_request.id,
            booking_request.pretax_amount,
            booking_request.mm_promo,
            booking_request.discount_value,
            booking_request.member_discount,
            booking_request.tax_amount,
            booking_request.total_amount)
        response = price
        return Response(response)

    def get_price(self, booking_request, room_configs, logged_in_user=False):
        price_request_dto = PriceRequestDto(
            booking_request.checkin_date, booking_request.checkout_date, [booking_request.hotel_id],
            room_configs, room_type=booking_request.room_type, rate_plan=booking_request.rate_plan,
            channel=booking_request.booking_channel, logged_in_user=logged_in_user)

        prices = self.pricing_service.get_itinerary_page_price(
            price_request_dto)
        price = self.price_response_transformer.transform_price_for_display(
            prices, booking_request.rate_plan)
        return price

    def _update_booking_request_with_prices(
            self, booking_request, prices, logged_in_user=False):
        """
        rate_plan_detail will be dict of this form:
                {
                    "code": selected_rate_plan,
                    "description": rate_plan_meta.get("description"),
                    "tag": rate_plan_meta.get("tag")
                },
        :param booking_request:
        :param prices:
        :param logged_in_user:
        :param rate_plan_detail:
        :return:
        """
        rp_price = prices
        booking_request.pretax_amount = rp_price['pretax_price']
        booking_request.rack_rate = rp_price["base_price"]
        booking_request.mm_promo = rp_price["promo_discount"]
        booking_request.discount_value = rp_price["coupon_discount"]
        booking_request.member_discount = rp_price['member_discount']
        # booking_request.discount_value = prices["coupon_discount"]
        booking_request.voucher_amount = rp_price['voucher_amount']
        booking_request.tax_amount = rp_price["tax"]
        booking_request.coupon_code = ""
        booking_request.coupon_apply = False
        booking_request.calculate_total_amount()
        booking_request.save()
