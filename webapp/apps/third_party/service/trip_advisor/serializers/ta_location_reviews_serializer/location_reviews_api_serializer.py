from rest_framework import serializers


class PagingSerializer(serializers.Serializer):
    next = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    previous = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    results = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    total_results = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    skipped = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)


class UserLocationSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    id = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    user_location = UserLocationSerializer(required=False, allow_null=True)


class SubratingSerializer(serializers.Serializer):
    rating_image_url = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
    name = serializers.CharField(
        required=True,
        allow_blank=True,
        allow_null=True)
    value = serializers.CharField(
        required=True,
        allow_blank=True,
        allow_null=True)
    localized_name = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)


class OwnerResponseSerializer(serializers.Serializer):
    id = serializers.CharField(
        required=True,
        allow_blank=True,
        allow_null=True)
    lang = serializers.CharField(
        required=True,
        allow_blank=True,
        allow_null=True)
    published_date = serializers.CharField(
        required=True, allow_blank=True, allow_null=True)
    author = serializers.CharField(
        required=True,
        allow_blank=True,
        allow_null=True)
    text = serializers.CharField(
        required=True,
        allow_blank=True,
        allow_null=True)
    title = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)


class ReviewDataSerializer(serializers.Serializer):
    id = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    lang = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    location_id = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    published_date = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    rating = serializers.IntegerField(required=False, initial=0)
    helpful_votes = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    rating_image_url = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
    url = serializers.URLField(
        required=False,
        max_length=200,
        min_length=None,
        allow_blank=False)
    trip_type = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    travel_date = serializers.CharField(
        required=False, allow_blank=True, allow_null=True)
    text = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    title = serializers.CharField(
        required=False,
        allow_blank=True,
        allow_null=True)
    user = UserSerializer(required=True, allow_null=True)
    owner_response = OwnerResponseSerializer(required=False, allow_null=True)
    subratings = SubratingSerializer(required=True, many=True, allow_null=True)
    partner_crawlable = serializers.BooleanField(required=False)


class LocationReviewsAPISerializer(serializers.Serializer):
    data = ReviewDataSerializer(many=True, required=True, allow_null=True)
    paging = PagingSerializer(required=False, allow_null=True)
