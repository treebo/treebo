from django.conf import settings
import requests
from django.utils.encoding import smart_str
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_409_CONFLICT, HTTP_201_CREATED, HTTP_403_FORBIDDEN
import logging
import json

from apps.auth.dto.otp_login_dto import NOT_AVAILABLE
from apps.common.api_response import APIResponse as api_response
import urllib.request
import urllib.parse
import urllib.error
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)
from apps.common.exceptions.custom_exception import AuthException, TokenNotFoundException, \
    UserEmailNotFoundException, UserWithPhoneAlreadyExist, UserWithEmailAlreadyExist, UserAppCredentialsNotFound, \
    InvalidTokenException, OTPSendException, OTPVerifyException


class TreeboAuthClient():
    # validation states
    NEW_USER = "NEW_USER"
    OLD_USER = "OLD_USER"
    TOKEN_INVALID = "TOKEN_INVALID"
    TOKEN_ABSENT = "TOKEN_ABSENT"

    def validate_token(self, token):
        #token = str(token)
        if not token:
            raise TokenNotFoundException('login')

        token = str(token)

        headers = {
            'Authorization': 'Bearer ' + token
        }

        response = self.__call_auth_server(
            'get', settings.AUTH_URL_ENDPOINTS['token_validate'], headers=headers)
        if response.ok:
            return response.json()['data']
        logger.error("invalid token error for token %s ->", token)
        if response.status_code == HTTP_403_FORBIDDEN:
            raise InvalidTokenException(token)
        raise AuthException('Invalid token.', response.status_code)

    def register(self, user_dto):
        headers = {
            'content-type': "application/json",
        }
        email = user_dto['email'] if user_dto['email'] else None
        phone_number = user_dto['phone_number'] if user_dto['phone_number'] else None
        data = {
            "email": email,
            "first_name": user_dto['first_name'],
            "last_name": user_dto.get(
                'last_name',
                None),
            "password": user_dto['password'],
            "app_type": 'web',
            "user_permissions": "",
            "groups": "",
            "phone_number": phone_number,
            "redirect_url": settings.TREEBO_WEB_URL +
            settings.TREEBO_WEB_EMAIL_VERIFY_URL}

        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['register'],
            data=json.dumps(data),
            headers=headers)
        status_code = response.status_code
        if response.ok:
            return response.json()['data']
        logger.error(
            "auth register error with request %s -> %s ",
            data,
            response.text)
        if status_code == HTTP_409_CONFLICT:
            raise UserWithPhoneAlreadyExist()
        message = api_response.fetch_message_from_response(response.json())
        if 'unique' in message:
            raise UserWithEmailAlreadyExist()
        if message:
            raise AuthException(message, response.status_code)
        raise AuthException('Register failed.', response.status_code)

    # remove token
    def logout(self, access_token):
        headers = {'Authorization': 'Bearer ' + access_token}
        response = self.__call_auth_server(
            'post', settings.AUTH_URL_ENDPOINTS['logout'], headers=headers)
        if response.ok:
            return ''
        logger.error(
            "auth logout token error from token %s -> %s ",
            access_token,
            response.text)
        message = api_response.fetch_message_from_response(response.json())
        if message:
            raise AuthException(message, response.status_code)
        raise AuthException('Logout failed.', response.status_code)

    #  create new token
    def login(self, email, password, verification_code=''):
        credentials = settings.APP_CRENDENTIALS
        credential = credentials.get('password', None)
        if not credential:
            raise UserAppCredentialsNotFound('password')

        payload = {
            "client_id": str(credential['client_id']),
            "client_secret": str(credential['client_secret']),
            "password": str(password),
            "grant_type": str("password"),
            "username": str(email),
            "verification_code": str(verification_code),
            "app_type": "web"
        }
        payload = urllib.parse.urlencode(payload)

        headers = {
            'content-type': "application/x-www-form-urlencoded",
        }
        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['login'],
            data=payload,
            headers=headers)
        if response.ok:
            return response.json()
        logger.error(
            "auth login error with -> %s ",
            response.text)
        raise AuthException('Invalid Credentials.', HTTP_400_BAD_REQUEST)

    def call_refresh_token(self, refresh_token):
        """

        :param refresh_token:
        :return:
        """
        if not refresh_token:
            raise TokenNotFoundException('refresh_token')

        credentials = settings.APP_CRENDENTIALS
        credential = credentials.get('password', None)
        if not credential:
            raise UserAppCredentialsNotFound('password')

        payload = "client_id=" + str(credential['client_id'])
        payload += "&client_secret=" + str(credential['client_secret'])
        payload += "&grant_type=" + str("refresh_token")
        payload += "&refresh_token=" + str(refresh_token)
        headers = {
            'content-type': "application/x-www-form-urlencoded",
        }
        logger.info(
            "calling auth api refresh token p: %s ,h: %s ",
            payload,
            headers)
        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['login'],
            data=payload,
            headers=headers)
        if response.ok:
            return response.json()
        logger.error(
            "auth refresh_token error with token %s -> %s ",
            refresh_token,
            response.text)
        raise AuthException('Invalid refresh token.', response.status_code)

    def forgot_password(self, email, template_type):
        headers = {
            'content-type': "application/json",
        }
        if not email:
            raise UserEmailNotFoundException(email)
        data = {
            "validation_type": "email",
            "email": str(email),
            "app_type": "web",
            "template_type": template_type,
            "redirect_url": settings.TREEBO_WEB_URL +
            settings.TREEBO_WEB_RESET_PASSWORD_URL}
        import json
        logger.info(
            "calling auth api forgot_password  p: %s ,h: %s ",
            json.dumps(data),
            headers)
        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['forgot_password'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        logger.error(
            "auth forgot_password  error with request %s -> %s ",
            json.dumps(data),
            response.text)
        message = api_response.fetch_message_from_response(response.json())
        if message:
            raise AuthException(message, response.status_code)
        raise AuthException('Email not sent.', response.status_code)

    def change_password(self, access_token, old_password, new_password):
        if not access_token:
            raise TokenNotFoundException('change_password')
        headers = {
            'content-type': "application/json",
            'Authorization': 'Bearer ' + access_token
        }
        data = {
            "old_password": old_password,
            "new_password": new_password
        }

        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['change_password'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        logger.error(
            "auth change_password error with request %s -> %s ",
            json.dumps(data),
            response.text)
        message = api_response.fetch_message_from_response(response.json())
        if message:
            logger.info("The error message from auth server is %s", message)
            raise AuthException(message, response.status_code)
        logger.info(
            "The status code from auth server is %s",
            response.status_code)
        raise AuthException('Password not changed.', response.status_code)

    def get_bytes_representation(self, input_data):
        if isinstance(input_data, bytes):
            return input_data
        return input_data.encode('utf-8')

    def reset_password(self, verification_code, confirm, password):
        headers = {
            'content-type': "application/json",
        }
        data = {
            "type": "email",
            "confirm": confirm,
            "verification_code": verification_code,
            "password": password,
            "app_type": "web"
        }
        import json
        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['reset_password'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        logger.error(
            "auth reset_password error with request %s -> %s ",
            json.dumps(data),
            response.text)
        message = api_response.fetch_message_from_response(response.json())
        if message:
            raise AuthException(message, response.status_code)
        raise AuthException('Password not changed.', response.status_code)

    def email_verification(self, verification_code):
        headers = {
            'content-type': "application/json",
        }
        data = {
            "verification_code": str(verification_code),
            "app_type": "web"
        }
        import json
        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['email_verification'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        logger.error(
            "auth email_verification error with request %s -> %s ",
            json.dumps(data),
            response.text)
        message = api_response.fetch_message_from_response(response.json())
        if message:
            raise AuthException(message, response.status_code)
        raise AuthException('Email not sent.', response.status_code)

    def update_profile(self, update_profile_dto):
        headers = {
            'content-type': "application/json",
        }
        data = {
            "first_name": update_profile_dto['first_name'] if update_profile_dto['first_name'] else '',
            "last_name": update_profile_dto['last_name'] if update_profile_dto['last_name'] else '',
            "phone_number": update_profile_dto['phone_number'] if update_profile_dto['phone_number'] else None,
            "email": update_profile_dto['email'] if update_profile_dto['email'] else None,
            "gender": update_profile_dto['gender'] if update_profile_dto['gender'] else '',
            "auth_id": update_profile_dto['auth_id'],
        }
        import json
        response = self.__call_auth_server(
            'put',
            settings.AUTH_URL_ENDPOINTS['update_profile'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        logger.error(
            "auth profile update error with request %s -> %s ",
            json.dumps(data),
            response.text)
        message = api_response.fetch_message_from_response(response.json())
        if message:
            raise AuthException(message, response.status_code)
        raise AuthException('Unable to update profile.', response.status_code)

    def login_via_otp(self, phone_number, channel=NOT_AVAILABLE):
        headers = {
            'content-type': "application/json",
        }
        data = {
            "phone_number": phone_number,
            "app_type": "web"
        }
        if channel != NOT_AVAILABLE:
            data['channel'] = channel

        import json
        logger.info(
            "calling auth api login via otp p: %s ,h: %s ",
            data,
            headers)
        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['login_via_otp'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        logger.error(
            "auth login_via_otp error: %s",
            response.text)

        try:
            errors = response.json().get('errors', [])
            if len(errors) > 0:
                message = errors[0].get('message')
                otp_request_attempts = errors[0].get('otp_request_attempts')
                max_otp_request_attempts = errors[0].get('max_otp_request_attempts')
            else:
                message = 'Failed to send OTP.'
                otp_request_attempts = None
                max_otp_request_attempts = None

            if not otp_request_attempts:
                raise OTPSendException(message=message, status_code=response.status_code)
            else:
                raise OTPSendException(message=message,
                                       status_code=response.status_code,
                                       otp_request_attempts=otp_request_attempts,
                                       max_otp_request_attempts=max_otp_request_attempts)
        except OTPSendException as e:
            raise e
        except Exception:
            logger.exception("auth login_via_otp error: %s", response.text)
            raise OTPSendException('Failed to send OTP.', response.status_code)

    def login_via_otp_verify(self, phone_number, verification_code, channel=NOT_AVAILABLE):
        headers = {
            'content-type': "application/json",
        }
        data = {
            "phone_number": smart_str(phone_number),
            "app_type": "web",
            "verification_code": str(verification_code)
        }
        if channel != NOT_AVAILABLE:
            data['channel'] = channel

        import json
        logger.info(
            "calling auth api login_via_otp_verify  p: %s ,h: %s ",
            data,
            headers)
        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['login_via_otp_verify'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        logger.error(
            "auth login_via_otp_verify error: %s",
            response.text)

        try:
            errors = response.json().get('errors', [])
            if len(errors) > 0:
                message = errors[0].get('message')
                failed_validation_attempts = errors[0].get('failed_validation_attempts')
                max_failed_validation_attempts = errors[0].get('max_failed_validation_attempts')
            else:
                message = 'OTP verification failed.'
                failed_validation_attempts = None
                max_failed_validation_attempts = None

            if not failed_validation_attempts:
                raise OTPVerifyException(message=message, status_code=response.status_code)
            else:
                raise OTPVerifyException(message=message,
                                         status_code=response.status_code,
                                         failed_validation_attempts=failed_validation_attempts,
                                         max_failed_validation_attempts=max_failed_validation_attempts)
        except OTPVerifyException as e:
            raise e
        except Exception:
            logger.exception("auth login_via_otp_verify error %s", response.text)
            raise OTPVerifyException('OTP verification failed.', response.status_code)

    def otp_is_verified(self, phone_number):
        headers = {
            'content-type': "application/json",
        }
        data = {
            "phone_number": smart_str(phone_number),
        }
        import json
        logger.info(
            "calling auth api otp_is_verified  p: %s ,h: %s ",
            data,
            headers)
        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['otp_is_verified'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        logger.error(
            "auth otp_is_verified  error with request %s -> %s ",
            json.dumps(data),
            response.text)
        message = api_response.fetch_message_from_response(response.json())
        if message:
            raise AuthException(message, response.status_code)
        raise AuthException('OTP not verified.', response.status_code)

    def convert_token(self, social_token, backend):
        credentials = settings.APP_CRENDENTIALS
        credential = credentials.get('password', None)
        if not credential:
            raise UserAppCredentialsNotFound('password')

        payload = "client_id=" + str(credential['client_id'])
        payload += "&client_secret=" + str(credential['client_secret'])
        payload += "&grant_type=" + str("convert_token")
        payload += "&backend=" + str(backend)
        payload += "&token=" + str(social_token)
        headers = {
            'content-type': "application/x-www-form-urlencoded",
        }
        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['convert_token'],
            data=payload,
            headers=headers)
        if response.ok:
            return response.json()
        logger.error("convert token error %s -> %s ", payload, response.text)
        raise AuthException('Invalid Credentials', HTTP_400_BAD_REQUEST)

    def user_details(self, user_dto):
        headers = {
            'content-type': "application/json",
        }
        email = user_dto['email'] if user_dto['email'] else None
        phone_number = user_dto['phone_number'] if user_dto['phone_number'] else None
        data = {
            "email": email,
            "first_name": user_dto['first_name'],
            "last_name": user_dto['last_name'],
            "password": user_dto['password'],
            "phone_number": phone_number,
        }

        response = self.__call_auth_server(
            'post',
            settings.AUTH_URL_ENDPOINTS['user_detail'],
            data=json.dumps(data),
            headers=headers)
        if response.ok:
            return response.json()['data']
        message = api_response.fetch_message_from_response(response.json())

        if message:
            raise AuthException(message, response.status_code)
        raise AuthException(
            'Unable to fetch user details.',
            response.status_code)

    def __call_auth_server(
            self,
            method,
            end_url,
            headers=None,
            params=None,
            data=None):
        host = settings.AUTH_SERVER_HOST
        auth_url = host + end_url
        if not headers:
            headers = {}
        if not params:
            params = {}
        if not data:
            data = {}
        if method == 'get':

            response = requests.get(auth_url, headers=headers)
        elif method == 'post':

            response = requests.post(auth_url, headers=headers, data=data)
        elif method == 'put':

            response = requests.put(auth_url, headers=headers, data=data)
        else:
            raise AuthException('Invalid method call', HTTP_400_BAD_REQUEST)
        logger.info(
            "auth api url %s param %s with status code %s  ",
            auth_url,
            params,
            response.status_code)

        if response.status_code == 500:
            slack_alert.send_slack_alert_for_third_party(status=response.status_code,
                                                         class_name="auth_client",
                                                         url=auth_url,
                                                         reason="{reason} params = {params} data={data}".format(
                                                             reason=str(response.text),
                                                             params=str(params), data=str(data)))

        return response

