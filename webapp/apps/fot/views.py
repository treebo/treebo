import csv
import json
import logging
from datetime import datetime, timedelta
from datetime import time
from decimal import Decimal

import hashlib
import io
import requests
from django.contrib.auth import login
from django.contrib.auth import logout
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models import Count
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect
from django.utils import dateformat
from operator import itemgetter
from rest_framework.response import Response

import common.constants.common_constants as const
from apps.bookingstash.service.availability_service import get_availability_service
from apps.common import date_time_utils
from apps.discounts import utils
from apps.fot import emailers as fot_emailers
from apps.fot.models import FotHotel, FotHotelBlackOut, FotReservationsRequest, Fot, FotConfig
from apps.fot.models import FotSignupAnswers
from apps.hotels.helpers import CityHotelHelper
from apps.hotels.service.hotel_service import HotelEnquiryValidator
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.profiles.service.user_registration import UserRegistration
from base.views.api import TreeboAPIView
from base.views.template import TreeboTemplateView
from common.constants import common_constants
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboValidationException
from dbcommon.models.hotel import Hotel
from dbcommon.models.location import City
from dbcommon.models.profile import User
from services import maps
from services.restclient.contentservicerestclient import ContentServiceClient

logger = logging.getLogger(__name__)

''' mobile fot login '''


class FotLogin(TreeboTemplateView):
    template_name = "fot/login/index.html"

    def getMobileData(self, request, args, kwargs):
        if request.user and request.user.is_active and request.user.is_authenticated():
            return HttpResponseRedirect('/fot/')
        context = {
            'scripts': ['mobile/js/login.js'],
            'styles': ['mobile/css/login.css'],
            'footer': False,
            'googleLogin': True}
        return context


''' mobile fot logout '''


class FotLogout(TreeboTemplateView):
    template_name = ''

    def getMobileData(self, request, args, kwargs):
        logout(request)
        currentUrl = request.GET.get('url')
        if currentUrl is None:
            return HttpResponseRedirect('/fot/')
        return HttpResponseRedirect(currentUrl)


''' USER CONFIRMATION '''


class UserConfirmation(TreeboAPIView):
    def get(self, request):
        userKey = request.GET.get('confirmationkey', '')
        userId = (userKey.split('-'))[0]
        try:
            fot = Fot.objects.get(user_id=userId, confirmation_key=userKey)
            if fot.status == Fot.NOT_CONFIRMED:
                fot.status = Fot.IN_PROGRESS
                fot.save()
        except Exception as e:
            logger.exception('user not found or update failed')
            pass
        return HttpResponseRedirect('/fot/')


''' LANDING PAGE '''


class FotLandingPage(TreeboTemplateView, TreeboAPIView):
    template_name = "fot/landing/index.html"

    def getData(self, request, args, kwargs):
        context = self.landing_page_validation(request)
        seo = ContentServiceClient.getValueForKey(request, 'seo_pages', 1)
        context['scripts'] = ['desktop/js/fot/landing_fot.js']
        context['styles'] = ['desktop/css/fot/landing_fot.css']
        context['seo'] = seo['fot']
        return context

    def getMobileData(self, request, args, kwargs):
        context = self.landing_page_validation(request)
        context['scripts'] = ['mobile/js/fot/landing_fot.js']
        context['styles'] = ['mobile/css/fot/landing_fot.css']
        return context

    def landing_page_validation(self, request):
        registrationStatus = 'FAILED'
        hotelsCount = Hotel.objects.filter(status=const.ENABLED).count()
        citiesCount = City.objects.filter(status=const.ENABLED).count()
        if request.user.is_authenticated() and not request.user.is_anonymous():
            try:
                fot = Fot.objects.get(user_id=request.user.id)
                registrationStatus = fot.status
                loginCheck = True
            except Fot.DoesNotExist:
                loginCheck = False
        else:
            loginCheck = False

        context = {'pageName': 'homePage', 'isLogin': loginCheck,
                   'hotelsCount': hotelsCount,
                   'citiesCount': citiesCount,
                   'registrationStatus': registrationStatus,
                   'footerCopyrightDisabled': True
                   }
        return context


''' CREDITS REDEMPTION '''


class CreditsRedemption(TreeboTemplateView, TreeboAPIView):
    def post(self, request):
        is_redemption_enable = FeatureToggleAPI.is_enabled(
            "fot", "fot_credit_redemption", False)
        if is_redemption_enable:
            fot = Fot.objects.get(user=request.user)
            credits_to_be_redeemed = request.POST.get('credits', '0')
            max_credits_to_be_redeemed = FotConfig.objects.get(
                id=1).max_credits_to_be_redeemed
            try:
                credits_to_be_redeemed = int(credits_to_be_redeemed)
                if credits_to_be_redeemed <= 0:
                    return Response({'success': False,
                                     'msg': 'Please enter a number greater than 0.'})
                elif credits_to_be_redeemed > max_credits_to_be_redeemed:
                    return Response({'success': False,
                                     'msg': 'You can redeem at max ' + str(max_credits_to_be_redeemed) + ' only.'})
                elif credits_to_be_redeemed > fot.user_credits:
                    return Response({'success': False,
                                     'msg': 'You can enter a maximum of your available credits only.'})
                else:
                    return Response({'success': True,
                                     'creditsToBeRedeemed': credits_to_be_redeemed})
            except Exception as e:
                logger.exception('credits calculation failed')
                return Response({'success': False,
                                 'msg': 'Please enter the value in numbers'})
        else:
            return Response(
                {'success': False, 'msg': 'Coupon generation is temporarily unavailable. We regret the inconvenience.'})


class FOTCreditRedemption(TreeboAPIView):
    """
    FOT credits redemption into nights
    """

    def dispatch(self, *args, **kwargs):
        return super(FOTCreditRedemption, self).dispatch(*args, **kwargs)

    def post(self, request):
        userId, expiry, credits_redeem = self.__parseRequest(request)
        try:
            with transaction.atomic():
                user = User.objects.get(id=userId)
                fot = Fot.objects.select_for_update().get(user=request.user)
                maxCreditsForUser = fot.user_credits
                total_credits_redeemed = fot.total_credits_redeemed
                if int(credits_redeem) > int(
                        maxCreditsForUser) or int(credits_redeem) <= 0:
                    logger.info("User does not have that much of credits")
                    return Response({"success": False})
                couponCode = utils.createUniqueCouponCode(user, credits_redeem)
                discountCoupon = utils.createCouponForUser(
                    user, couponCode, Decimal(credits_redeem), expiry)
                credits_redeem = int(credits_redeem)
                fot.user_credits = maxCreditsForUser - credits_redeem
                fot.total_credits_redeemed = total_credits_redeemed + credits_redeem
                fot.save()
                fot_emailers.coupon_confirmation(
                    userId, discountCoupon.code, credits_redeem)
            if discountCoupon.code:
                return Response(
                    {"code": discountCoupon.code, "creditsRedeem": credits_redeem, "success": True})
            else:
                return Response({"success": False})

        except BaseException:
            logger.exception('credit redemption failed')
            return Response({"success": False})

    def __parseRequest(self, request):
        userId = request.POST.get('user_id', None)
        # nights = request.POST.get('nights',0)
        expiry = request.POST.get('expiry', 60)
        credits_redeem = request.POST.get('credits', 1000)
        return userId, expiry, credits_redeem


''' API for updating Audit booking statuses'''


class UpdateFotHotel(TreeboAPIView):
    def post(self, request):
        data = request.POST
        hotelId = data.get('hotel_id', '')
        bookingId = data.get('booking_id', '')
        reservationStatus = data.get('status', '')
        if hotelId or bookingId or reservationStatus == "":
            return Response({'success': False,
                             'msg': 'Values are not allowed to be null'})
        else:
            fotHotel = FotReservationsRequest.objects.get(
                hotel_id=hotelId, booking_id=bookingId)
            fotHotel.status = reservationStatus
            try:
                fotHotel.save()
                return Response({'success': True, 'msg': 'Save success'})
            except Exception as e:
                logger.exception('updatefothotel failed')
                return Response({'success': False, 'msg': 'Save failed'})


''' SORT AND SEARCH FOR DASHBOARDS'''


def _search_and_sort_dashboard(querySetObj, request, defaultSortType):
    queryParams = request.GET
    querySetObjBackup = querySetObj
    filterDict = {}
    sortFieldList = []

    filterParams = queryParams.get('filterparam')
    sortParams = queryParams.get('sortparam')
    daterangeParams = queryParams.get('daterangeparam')

    # url can be /?sortparam=k1-v1,k2-v2,k3-v3&filterparam=k1-v1,k2-v2
    # for sort k is the field name, v is the order of sort
    # for filter k is the field name, v is the value to filter

    if bool(filterParams):
        try:
            filterParamList = filterParams.split(',')
            for filterParam in filterParamList:
                filterField, filterValue = filterParam.split('-')
                filterField = _parse_search_sort_field(filterField)
                filterDict[str(filterField) + '__icontains'] = filterValue

            if bool(filterValue):
                querySetObj = querySetObj.filter(**filterDict)

        except Exception as e:
            logger.exception('filter failed')
            querySetObj = querySetObjBackup

    if bool(sortParams):
        try:
            sortParamList = sortParams.split(',')
            for sortParam in sortParamList:
                sortField, sortType = sortParam.split('-')
                sortField = _parse_search_sort_field(sortField)
                if (sortType[0]).lower() == 'd':
                    sortField = '-' + str(sortField)
                if bool(sortField):
                    sortFieldList.append(sortField)

            # default sort
            if not sortFieldList:
                if defaultSortType == 'audits':
                    sortFieldList.append('-id')
                elif defaultSortType == 'users':
                    sortFieldList.append('id')

            if sortFieldList:
                querySetObj = querySetObj.order_by(*sortFieldList)

        except Exception as e:
            logger.exception('sort failed')

    if bool(daterangeParams):
        try:
            startDate, endDate = daterangeParams.split(',')
            querySetObj = querySetObj.filter(
                check_in_date__gte=startDate,
                check_in_date__lte=endDate)
        except Exception as e:
            logger.exception('Date Range failed')

    return querySetObj


def _parse_search_sort_field(field):
    if field == 'name':
        field = 'user__first_name'
    elif 'email' in field:
        field = 'user__email'
    elif 'phone' in field:
        field = 'user__phone_number'

    return field


''' PAGINATOR FOR DASHBOARDS'''


def _dashboard_paginator(valuesQuerySetObj, pageNumber, itemsPerPage):
    paginatorEnabled = True
    if paginatorEnabled:

        try:
            pageNumber = int(pageNumber)
        except BaseException:
            pageNumber = 1

        try:
            itemsPerPage = int(itemsPerPage)
        except BaseException:
            itemsPerPage = 25

        try:
            paginatorObj = Paginator(valuesQuerySetObj, itemsPerPage)
            numberOfPages = paginatorObj.num_pages
            pageItems = paginatorObj.page(pageNumber)
            pageItemsList = [item for item in pageItems]
        except Exception as e:
            pageItemsList = []
            numberOfPages = 0
            logger.exception('Paginator failed due to exception')

        return pageItemsList, pageNumber, numberOfPages, itemsPerPage
    else:
        return valuesQuerySetObj, 0, 0, 0


''' CSV EXPORTER FOR DASHBOARDS'''


class DashboardCsvExporter(TreeboAPIView):
    def get(self, request):
        csvPage = request.GET.get('csvpage')
        if csvPage:
            if csvPage == 'audits':
                fields = [
                    'user__first_name',
                    'user__email',
                    'check_in_date',
                    'hotel__name',
                    'no_of_guests',
                    'user__phone_number',
                    'id',
                    'user_id',
                    'hotel__city__name',
                    'status',
                    'booking__order_id']

                querySet = FotReservationsRequest.objects.all().values(*fields)
                csvFileName = 'audits'
            elif csvPage == 'users':
                fields = [
                    'created_at',
                    'user__first_name',
                    'user__email',
                    'user__phone_number',
                    'city',
                    'marital_status',
                    'status',
                    'user_id',
                    'user_credits',
                    'total_audits_scheduled',
                    'future_audits',
                    'no_shows',
                    'feedbacks_submitted',
                    'qualified_feedbacks',
                    'total_free_nights',
                    'free_nights_remaining']

                querySet = Fot.objects.all().values(*fields)
                csvFileName = 'users'
            else:
                return Response(
                    {'success': False, 'msg': 'Please provide a valid value for csvPage'})
            try:
                file = io.StringIO()
                writer = csv.writer(file)
                writer.writerow(fields)

                for q in querySet:
                    list = []
                    for field in fields:
                        try:
                            list.append(str(q[field]))
                        except UnicodeError:
                            list.append('')
                    writer.writerow(list)
                file.seek(0)
                response = HttpResponse(file, content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename=' + \
                    csvFileName + '.csv'
                return response
            except Exception as e:
                logger.exception('csv exporter failed')
                return Response(
                    {'success': False, 'msg': 'csv cannot be generated due to an error'})

        else:
            return Response(
                {'success': False, 'msg': 'csv page cannot be empty'})


''' DASHBOARD APIS '''


class AuditsDashboard(TreeboAPIView):
    def dispatch(self, *args, **kwargs):
        return super(AuditsDashboard, self).dispatch(*args, **kwargs)

    def get(self, request):
        try:
            fields = [
                'user__first_name',
                'user__email',
                'check_in_date',
                'hotel__name',
                'no_of_guests',
                'user__phone_number',
                'id',
                'user_id',
                'hotel__city__name',
                'status',
                'booking__order_id',
                'created_at']
            hotelsList = FotReservationsRequest.objects.all()
            hotelsList = _search_and_sort_dashboard(
                hotelsList, request, 'audits')
            hotelsList = hotelsList.values(*fields)
        except Exception as e:
            logger.exception('getting audits failed')
            hotelsList = []
        hotels, currPage, totalPages, itemsPerPage = _dashboard_paginator(
            hotelsList, request.GET.get('page'), request.GET.get('itemsperpage'))

        return Response({'hotel': hotels,
                         'currPage': currPage,
                         'totalPages': totalPages,
                         'itemsPerPage': itemsPerPage})


class FotUsersDashboard(TreeboAPIView):
    def get(self, request):
        try:
            fields = [
                'created_at',
                'user__first_name',
                'user__email',
                'user__phone_number',
                'city',
                'marital_status',
                'status',
                'user_id',
                'user_credits',
                'total_audits_scheduled',
                'future_audits',
                'no_shows',
                'feedbacks_submitted',
                'qualified_feedbacks',
                'total_free_nights',
                'free_nights_remaining',
                'free_nights_deemed',
                'feedback_dropped_out',
                'is_active',
                'score']
            fotUserList = Fot.objects.all()
            fotUserList = _search_and_sort_dashboard(
                fotUserList, request, 'users')
            fotUserList = fotUserList.values(*fields)
        except Exception as e:
            logger.exception('getting users failed')
            fotUserList = []
        statusList = ['PASSED', 'FAILED', 'IN_PROGRESS', 'BLOCKED']

        usersList, currPage, totalPages, itemsPerPage = _dashboard_paginator(
            fotUserList, request.GET.get('page'), request.GET.get('itemsperpage'))

        return Response({'users': usersList,
                         'statusList': statusList,
                         'totalPages': totalPages,
                         'currPage': currPage,
                         'itemsPerPage': itemsPerPage})

    def post(self, request):
        fotUserId = request.POST.get('userid', '')
        blockStatus = request.POST.get('block_status', '')
        userCredits = request.POST.get('userCredits')

        blockStatusList = [
            'PASSED',
            'FAILED',
            'BLOCKED',
            'IN_PROGRESS',
            'NOT_CONFIRMED']
        if not fotUserId == '' and blockStatus in blockStatusList:
            user = Fot.objects.get(user_id=fotUserId)
            preUserStatus = user.status
            if not blockStatus == '':
                user.status = blockStatus
            try:
                user.user_credits = int(userCredits)
                creditsResp = ''
            except BaseException:
                logger.exception('saving credits failed')
                creditsResp = 'saving credits failed'
            try:
                user.save()
                if blockStatus == 'BLOCKED' and not preUserStatus == "BLOCKED":
                    fot_emailers.account_blocking_confirmation(user, '')
                if preUserStatus == "BLOCKED" and not blockStatus == 'BLOCKED':
                    fot_emailers.account_unblocking_confirmation(user.id)
                return Response(
                    {'success': True, 'msg': 'Save success' + creditsResp})
            except Exception as e:
                logger.exception('user blocking/unblocking failed')
                return Response({'success': False, 'msg': 'Save failed'})
        else:
            logger.info('values not allowed')
            return Response({'success': False, 'msg': 'values not allowed'})


class CreditsDashboard(TreeboAPIView):
    def get(self, request):
        try:
            hotelsList = FotHotel.objects.all().values(
                'hotel__name', 'hotel__city__name', 'credits_per_audit', 'id')
        except Exception as e:
            logger.exception('getting hotels for credits failed')
            hotelsList = []
        # FOT PAGINATION
        # hotels, currPage, totalPages, itemsPerPage = _dashboard_paginator(hotelsList,request.GET.get('page'),
        # request.GET.get('itemsperpage'))
        hotels, currPage, totalPages, itemsPerPage = [], 1, 1, 0
        return Response({'hotels': hotelsList, 'totalPages': totalPages,
                         'currPage': currPage, 'itemsPerPage': itemsPerPage})

    def post(self, request):
        fotHotelId = request.POST.get('hotelid', '')
        bulkCheck = request.POST.get('bulk_credits', False)
        fotCredits = request.POST.get('credits', '')
        if not fotHotelId == '' and not fotCredits == '':
            try:
                hotel = FotHotel.objects.get(id=fotHotelId)
                try:
                    fotCredits = int(fotCredits)
                    if fotCredits >= 0:
                        hotel.credits_per_audit = fotCredits
                        try:
                            hotel.save()
                            return Response(
                                {'success': True, 'msg': 'Update success'})
                        except Exception as e:
                            logger.exception(
                                'updating credits for hotels failed')
                            return Response(
                                {'success': False, 'msg': 'Update failed'})
                    else:
                        logger.info('credits cant be less than zero')
                        return Response(
                            {'success': False, 'msg': 'Credits cant be less than zero'})
                except Exception as e:
                    logger.exception('Credits does not seem to be an integer')
                    return Response(
                        {'success': False, 'msg': 'Credits does not seem to be an integer'})
            except Exception as e:
                logger.exception('Hotel id is not valid')
                return Response(
                    {'success': False, 'msg': 'Hotel id is not valid'})

        elif fotHotelId == '' and bulkCheck:
            hotel = FotHotel.objects.all()

            try:
                fotCredits = int(fotCredits)
                if fotCredits >= 0:
                    try:
                        hotel.update(credits_per_audit=fotCredits)
                        return Response(
                            {'success': True, 'msg': 'Update success'})
                    except Exception as e:
                        logger.exception('bulk Update hotls credit failed')
                        return Response(
                            {'success': False, 'msg': 'Update failed'})
                else:
                    logger.info('credits cant be less than zero')
                    return Response(
                        {'success': False, 'msg': 'Credits cant be less than zero'})
            except Exception as e:
                logger.exception('Credits does not seem to be an integer')
                return Response(
                    {'success': False, 'msg': 'Credits does not seem to be an integer'})

        else:
            logger.info('one or more variable is empty')
            return Response(
                {'success': False, 'msg': 'One or more variables required seems to be empty'})


class NotificationsDashboard(TreeboAPIView):
    def post(self, request):
        fotNotificationContent = request.POST.get('notification_content', '')
        try:
            FotUserList = Fot.objects.all()
            FotUserList.update(
                notification=fotNotificationContent,
                show_notification=False)
            return Response(
                {'success': True, 'msg': 'All users updated with notification content'})
        except Exception as e:
            logger.exception('no users found or update failed')
            return Response({'success': False,
                             'msg': 'no users found or update failed'})


class SlotsPerHotelDashboard(TreeboAPIView):
    def get(self, request):
        try:
            hotelsList = FotHotel.objects.all().values(
                'hotel__name', 'hotel__city__name', 'slots_allowed', 'id')
        except Exception as e:
            logger.exception('getting hotels for slots failed')
            hotelsList = []
        # FOT PAGINATION
        # hotels, currPage, totalPages, itemsPerPage = _dashboard_paginator(hotelsList,request.GET.get('page'),
        # request.GET.get('itemsPerPage'))
        hotels, currPage, totalPages, itemsPerPage = [], 1, 1, 0
        return Response({'hotels': hotelsList, 'totalPages': totalPages,
                         'currPage': currPage, 'itemsPerPage': itemsPerPage})

    def post(self, request):
        hotelId = request.POST.get('hotelid', '')
        slots = request.POST.get('slots', '')
        if not hotelId == '':
            try:
                hotel = FotHotel.objects.get(id=hotelId)
                if not slots == '':
                    try:
                        slots = int(slots)
                        if not slots < 0:
                            hotel.slots_allowed = int(slots)
                            try:
                                hotel.save()
                                return Response(
                                    {'success': True, 'msg': 'Save success'})
                            except Exception as e:
                                logger.exception(
                                    'Save failed while updating slots')
                                return Response(
                                    {'success': False, 'msg': 'Save failed'})
                        else:
                            logger.info('slotes less than zero')
                            return Response(
                                {'success': False, 'msg': 'slots is less than zero'})
                    except Exception as e:
                        logger.exception('slots is not an integer')
                        return Response(
                            {'success': False, 'msg': 'slots is not an integer'})
                else:
                    logger.info('slots is empty')
                    return Response(
                        {'success': False, 'msg': 'slots is  empty'})
            except Exception as e:
                logger.exception('Invalid hotel id provided')
                return Response(
                    {'success': False, 'msg': 'Invalid hotel id provided'})
        else:
            logger.info('hotel id is empty')
            return Response({'success': False, 'msg': 'hotel id is  empty'})


class SlotsPerUserDashboard(TreeboAPIView):
    def post(self, request):
        slotsPerUser = request.POST.get('auditPerUser', '')
        if not slotsPerUser == '':
            try:
                slots = int(slotsPerUser)
                if slots >= 0:
                    try:
                        userList = Fot.objects.all()
                        userList.update(user_slots=slots)
                        return Response(
                            {'success': True, 'msg': 'Update success'})
                    except Exception as e:
                        logger.exception('Update failed or no users to update')
                        return Response({'success': False,
                                         'msg': 'Update failed or no users to update'})
                else:
                    logger.info('slotes provided is less than zero')
                    return Response(
                        {'success': False, 'msg': 'Slots provided cannot be less than'})
            except Exception as e:
                logger.exception('Slots provided is not an integer')
                return Response({'success': False,
                                 'msg': 'Slots provided is not an integer'})
        else:
            return Response(
                {'success': False, 'msg': 'Slots provided is empty'})


class HotelBlockOutDatesDashboard(TreeboAPIView):
    def get(self, request):
        try:
            blockHotelsList = []
            allHotelsList = FotHotel.objects.all().values(
                'id', 'hotel__name').order_by('id')
            allBlackoutHotelsIdList = FotHotelBlackOut.objects.all().values_list(
                'fot_hotel_id', flat=True).order_by('fot_hotel_id')
            for x in allHotelsList:
                if x['id'] in allBlackoutHotelsIdList:
                    fotHotelBlackOutObjs = FotHotelBlackOut.objects.filter(
                        fot_hotel_id=x['id']).values(
                        'fot_hotel_id',
                        'fot_hotel__hotel__name',
                        'id',
                        'blackout_start_time',
                        'blackout_end_time')
                else:
                    fotHotelBlackOutObjs = {}
                counter = len(fotHotelBlackOutObjs)
                for y in fotHotelBlackOutObjs:
                    hotelsDetailDict = {}
                    blockStartDate = (y['blackout_start_time']).date()
                    blockEndDate = (y['blackout_end_time']).date()
                    hotelsDetailDict['fot_hotel_id'] = y['fot_hotel_id']
                    hotelsDetailDict['hotel__name'] = x['hotel__name']
                    hotelsDetailDict['tag'] = 'bo-Two-' + str(y['id'])
                    hotelsDetailDict['blackout_start_time'] = blockStartDate
                    hotelsDetailDict['blackout_end_time'] = blockEndDate
                    blockHotelsList.append(hotelsDetailDict)
                if counter >= 2:
                    pass
                elif counter == 1:
                    hotelsDetailDict = {}
                    hotelsDetailDict['fot_hotel_id'] = x['id']
                    hotelsDetailDict['hotel__name'] = x['hotel__name']
                    hotelsDetailDict['tag'] = 'bo-One-'
                    hotelsDetailDict['blackout_start_time'] = ''
                    hotelsDetailDict['blackout_end_time'] = ''
                    blockHotelsList.append(hotelsDetailDict)
                else:
                    for z in range(2):
                        hotelsDetailDict = {}
                        hotelsDetailDict['fot_hotel_id'] = x['id']
                        hotelsDetailDict['hotel__name'] = x['hotel__name']
                        hotelsDetailDict['tag'] = 'ho-One-'
                        hotelsDetailDict['blackout_start_time'] = ''
                        hotelsDetailDict['blackout_end_time'] = ''
                        blockHotelsList.append(hotelsDetailDict)
        except Exception as e:
            logger.exception('getting hotels blacklist failed')
            blockHotelsList = {}
        return Response({'hotel': blockHotelsList})

    def post(self, request):
        fotHotelId = request.POST.get('hotelid', '')
        blockStartDate = request.POST.get('block_start_date', '')
        blockEndDate = request.POST.get('block_end_date', '')
        tag = request.POST.get('tag', '')
        resetStatus = request.POST.get('resetStatus', False)
        today = datetime.now()
        if not fotHotelId == '':
            if not resetStatus:
                if not blockStartDate == '' and not blockEndDate == '':
                    try:
                        blockStartDate = datetime.strptime(
                            blockStartDate, date_time_utils.DATE_FORMAT)
                        blockEndDate = datetime.strptime(
                            blockEndDate, date_time_utils.DATE_FORMAT)
                        if blockStartDate >= today and blockEndDate >= today:
                            if not tag == '':
                                tag, count, hId = tag.split('-')
                                if tag == 'bo' and count == 'Two':
                                    try:
                                        fotBlackoutObj = FotHotelBlackOut.objects.get(
                                            id=hId)
                                        fotBlackoutObj.blackout_start_time = blockStartDate
                                        fotBlackoutObj.blackout_end_time = blockEndDate
                                        fotBlackoutObj.save()
                                        return Response(
                                            {'success': True, 'msg': 'Blockout updation success'})
                                    except Exception as e:
                                        logger.exception(
                                            'Blockout updation failed')
                                        return Response(
                                            {'success': False, 'msg': 'Blockout updation failed'})
                                else:
                                    try:
                                        FotHotelBlackOut.objects.create(
                                            fot_hotel_id=fotHotelId,
                                            blackout_start_time=blockStartDate,
                                            blackout_end_time=blockEndDate)

                                        return Response(
                                            {'success': True, 'msg': 'Blockout creation success'})
                                    except Exception as e:
                                        logger.exception(
                                            'Blockout creation failed')
                                        return Response(
                                            {'success': False, 'msg': 'Blockout creation failed'})
                            else:
                                logger.info('tag not provided')
                                return Response(
                                    {'success': False, 'msg': 'tag not provided'})
                        else:
                            logger.info('dates are old')
                            return Response(
                                {'success': False, 'msg': 'Dates are previous dates'})
                    except ValueError:
                        logger.exception('startdate or enddate is not a date')
                        return Response(
                            {'success': False, 'msg': 'startdate or enddate is not a date'})
                    except Exception as e:
                        logger.exception('Blockout updation/creation failed')
                        return Response(
                            {'success': False, 'msg': 'Some thing went wrong'})
                else:
                    logger.info('start or end date is empty')
                    return Response(
                        {'success': False, 'msg': 'startdate or enddate is empty'})
            else:
                try:
                    tag, count, hId = tag.split('-')
                    if tag == 'bo' and count == 'Two':
                        fotBlackoutObj = FotHotelBlackOut.objects.get(id=hId)
                        fotBlackoutObj.delete()
                        return Response(
                            {'success': True, 'msg': 'Blockout delete success'})
                    else:
                        logger.info('wrong tag for delete')
                        return Response(
                            {'success': True, 'msg': 'Wrong tag for delete'})
                except BaseException:
                    logger.exception('delete failed')
                    return Response({'success': False, 'msg': 'delete failed'})
        else:
            logger.info('fothotel id is empty')
            return Response({'success': False, 'msg': 'Fothotel id is empty'})


class FotConfigDashboard(TreeboAPIView):
    def get(self, request):
        try:
            config = FotConfig.objects.filter(
                id=1).values(
                'max_credits_to_be_redeemed',
                'credit_equivalent_per_night',
                'coupon_expiry')
        except Exception as e:
            logger.exception('getting config failed')
            config = {}
        return Response({'config': config})

    def post(self, request):
        creditEquivalentPerNight = request.POST.get(
            'creditEquivalentPerNight', '')
        couponExpiry = request.POST.get('couponExpiry', '')
        maxCreditsToBeRedeemed = request.POST.get(
            'max_credits_to_be_redeemed', '')
        configObj = FotConfig.objects.get(id=1)
        if not maxCreditsToBeRedeemed == '':
            try:
                max_credits_to_be_redeemed = int(maxCreditsToBeRedeemed)
                configObj.max_credits_to_be_redeemed = max_credits_to_be_redeemed
            except Exception as e:
                logger.exception('max_credits_to_be_redeemed id is invalid')
                return Response(
                    {'success': False, 'msg': 'max_credits_to_be_redeemed id is invalid'})
        if not couponExpiry == '':
            try:
                couponExpiry = int(couponExpiry)
                configObj.coupon_expiry = couponExpiry
            except Exception as e:
                logger.exception('couponExpiry id is invalid')
                return Response(
                    {'success': False, 'msg': 'couponExpiry id is invalid'})
        if not creditEquivalentPerNight == '':
            try:
                creditEquivalentPerNight = int(creditEquivalentPerNight)
                configObj.credit_equivalent_per_night = creditEquivalentPerNight
            except Exception as e:
                logger.exception('creditEquivalentPerNight id is invalid')
                return Response(
                    {'success': False, 'msg': 'creditEquivalentPerNight id is invalid'})
        try:
            configObj.save()
            return Response({'success': True,
                             'msg': 'FotConfig saved sucessfully'})
        except Exception as e:
            logger.exception('save failed for fot config')
            return Response({'success': False, 'msg': 'Save failed'})


''' Signup API '''


class FotRegistration(TreeboTemplateView, TreeboAPIView):
    template_name = "fot/signup/index.html"

    def getData(self, request, args, kwargs):
        context = self.process_signup_get_request(request)
        seo = ContentServiceClient.getValueForKey(request, 'seo_pages', 1)
        context['scripts'] = ['desktop/js/fot/signup_fot.js']
        context['styles'] = ['desktop/css/fot/signup_fot.css']
        context['seo'] = seo['fotsignup']
        return context

    def getMobileData(self, request, args, kwargs):
        context = self.process_signup_get_request(request)
        context['scripts'] = ['mobile/js/fot/signup_fot.js']
        context['styles'] = ['mobile/css/fot/signup_fot.css']
        context['footer'] = 'hide'
        return context

    def process_signup_get_request(self, request):
        signupCheck = False
        registrationStatus = True
        isFotSignup = False
        personalDetails = {'name': '', 'email': '', 'phone': ''}
        if request.user.is_authenticated() and not request.user.is_anonymous():
            try:
                fot = Fot.objects.get(user=request.user)
                signupCheck = True
                isFotSignup = True
                if fot.status == Fot.IN_PROGRESS:
                    registrationStatus = False
                else:
                    return redirect('/fot/')
                    # questions = FotSignupQuestions.objects.all() TODO: For next release
                    # answers = FotSignupAnswers.objects.all()
            except Fot.DoesNotExist:
                signupCheck = False
                name = request.user.first_name + request.user.last_name
                personalDetails = {
                    'name': name,
                    'email': request.user.email,
                    'phone': request.user.phone_number}
        context = {
            'page_name': 'Fot Register',
            'isSignup': signupCheck,
            'registrationStatus': registrationStatus,
            'personalDetails': personalDetails,
            'isFotSignup': isFotSignup,
            'footerCopyrightDisabled': True}
        return context

    def post(self, request):
        if request.user.is_authenticated() and not request.user.is_anonymous():
            email = request.user.email
            user = User.objects.get(email=email)
            response = self._CheckFotUser(user, request, email)
            alreadySignup = response.data['alreadySignup']
            successCheck = response.data['success']

        else:
            email = request.POST.get('email')
            phone = request.POST.get('mobile')
            try:
                if User.objects.filter(
                        email=email).exists() or User.objects.filter(
                        phone_number=phone).exists():
                    alreadySignup = True
                    successCheck = True
                else:
                    user = User.objects.get(email=email)
                    response = self._CheckFotUser(user, request, email)
                    alreadySignup = response.data['alreadySignup']
                    successCheck = response.data['success']
            except User.DoesNotExist:
                response = self._makeUserEntry(request)
                alreadySignup = False
                successCheck = response.data['success']

        return Response(
            {'success': successCheck, 'alreadySignup': alreadySignup})

    def _CheckFotUser(self, user, request, email):
        try:
            fot = Fot.objects.get(user_id=user.id)
            successState = True
            if fot.status == Fot.PASSED:
                successState = True
            elif fot.status == Fot.IN_PROGRESS:
                if not request.POST.get('form-type', '') == 'fot-profile-form':
                    successState = self.validateQuestinnare(request, email)
            elif fot.status == Fot.FAILED:
                successState = False
            return Response({'success': successState, 'alreadySignup': True})
        except Fot.DoesNotExist:
            if request.POST.get('form-type', '') == 'fot-profile-form':
                confirmationKey = self.makeFotEntry(email)
                fot_emailers.signup_confirmation(email, confirmationKey)
                self._loginFotUser(user, request)
            return Response({'success': True, 'alreadySignup': False})

    def _loginFotUser(self, user, request):
        logger.info('logging in the user')
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)

    def _makeUserEntry(self, request):
        successState = True
        if request.POST.get('form-type', '') == 'fot-profile-form':
            name = request.POST['user']
            email = request.POST['email']
            phone = request.POST['mobile']
            password = request.POST['password']
            if not any((name, email, phone, password)):
                successState = False
            else:
                name = name.split(" ", 2)
                try:
                    user_registration = UserRegistration()
                    user_dto = {
                        'email': email,
                        "first_name": name[0].strip(),
                        "last_name": name[1].strip() if len(name) is 2 else "",
                        "phone_number": phone,
                        "password": password,
                    }
                    user = user_registration.register_internally(user_dto)
                except TreeboValidationException as e:
                    user, email, phone = User.objects.get_existing_user(
                        email, phone, None)
                if user is not None:
                    user = User.objects.get(email=user.email)
                    confirmationKey = self.makeFotEntry(email)
                    fot_emailers.signup_confirmation(email, confirmationKey)
                    self._loginFotUser(user, request)
                    successState = True
                else:
                    successState = False
        return Response({'success': successState})

    def makeFotEntry(self, email):
        user = User.objects.get(email=email)
        Fot.objects.get_or_create(user_id=user.id)
        confirmationKey = str(hashlib.sha512(email).hexdigest().upper()[0:16])
        fot = Fot.objects.get(user_id=user.id)
        confirmationKey = str(user.id) + '-' + confirmationKey
        fot.status = Fot.IN_PROGRESS
        fot.confirmation_key = confirmationKey
        fot.save()
        return confirmationKey

    def validateQuestinnare(self, request, email):
        user = User.objects.get(email=email)
        fotObj = Fot.objects.get(user_id=user.id)
        allPersonalDetailsDict = {
            'city': 'city',
            'marital_status': 'marital-status',
            'assets': 'assets',
            'profession': 'profession',
            'audit_duration': 'audit-duration',
            'travel_experience': 'travel-experience',
            'about_yourself': 'about-yourself',
            'current_address': 'address',
            'social_web_profile': 'social-web-profile',
            'why_treebo': 'why-treebo',
            'source_website': 'sourceWebsite',
            'friend_name': 'friend-name'}

        for key in allPersonalDetailsDict:
            Fetchedvalue = request.POST.getlist(allPersonalDetailsDict[key])
            Fetchedvalue = ','.join(Fetchedvalue)
            setattr(fotObj, key, Fetchedvalue)
            fotObj.save()

        scoreThreshold = FotConfig.objects.get(id=1).score_to_pass
        score = 0
        questionsAsked = ''
        answersAnswered = ''
        for q in range(1, 8):
            questionId = '3_' + str(q)
            qid = q
            answerValue = request.POST.get(questionId, '')
            if not answerValue == '':
                try:
                    answerScore = FotSignupAnswers.objects.get(
                        question_id=qid, value=answerValue).score
                    score += int(answerScore)
                except Exception as e:
                    logger.exception('validating answer score failed')
                    pass
            questionsAsked += str(questionId) + ','
            answersAnswered += str(answerValue) + ','
        for q in range(1, 3):
            questionId = '4_' + str(q)
            qid = 7 + q
            answerValue = request.POST.get(questionId, '')
            if not answerValue == '':
                try:
                    answerScore = FotSignupAnswers.objects.get(
                        question_id=qid, value=answerValue).score
                    score += int(answerScore)
                except Exception as e:
                    logger.exception('validating answer score failed')
                    pass
            questionsAsked += str(questionId) + ','
            answersAnswered += str(answerValue) + ','

        travelExperience = fotObj.travel_experience
        if len(travelExperience.split()) > 100:
            score += 5
        elif len(travelExperience.split()) > 50:
            score += 2
        elif len(travelExperience.split()) > 20:
            score += 1

        fotObj.score = score
        fotObj.questions_asked = questionsAsked
        fotObj.answers_answered = answersAnswered
        if score >= scoreThreshold:
            fotObj.status = Fot.PASSED
            mailflag = 'PASSED'
            status = True
        else:
            fotObj.status = Fot.FAILED
            mailflag = 'FAILED'
            status = False
        # TODO front end change for status from boolean to string for accepting
        # except message
        try:
            fotObj.save()
            fot_emailers.registration_confirmation(email, mailflag)
        except Exception as e:
            logger.exception('saving fot object failed after giving test')
            status = False
        return status


''' Search API '''


class SearchAudit(TreeboTemplateView):
    template_name = "fot/audit/index.html"
    defaultSearchUrl = ''
    roomConfigStr = ''
    pagename = 'search'
    hotelpage = 'searchhotel'

    def __init__(self, **kwargs):
        super(SearchAudit, self).__init__(**kwargs)
        self.__myMap = maps.Maps()
        SearchAudit.__myMap = self.__myMap

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect('/fot/')
        if not Fot.objects.filter(user_id=request.user.id).exists():
            return redirect('/fot/')
        elif not Fot.objects.get(user_id=request.user.id).status == Fot.PASSED:
            return redirect('/fot/')
        return super(SearchAudit, self).dispatch(request, *args, **kwargs)

    def getData(self, request, args, kwargs):
        city_list = City.objects.filter(
            status=common_constants.ENABLED).values('name').order_by('name')
        is_redemption_enable = FeatureToggleAPI.is_enabled(
            "fot", "fot_credit_redemption", False)
        context = self.__getSearchData(request)
        context['styles'] = ['desktop/css/fot/search_audit_fot.css']
        context['scripts'] = ['desktop/js/fot/search_audit_fot.js']
        context['googleMaps'] = True
        context['citylist'] = city_list
        context['is_redemption_enable'] = is_redemption_enable
        return context

    def getMobileData(self, request, args, kwargs):
        context = self.__getSearchData(request)
        context['styles'] = ['mobile/css/fot/search_audit_fot.css']
        context['scripts'] = ['mobile/js/fot/search_audit_fot.js']
        return context

    def __getSearchData(self, request):

        searchString, checkInDate, checkOutDate, roomsConfig, latitude, longitude, locality = self.__parseRequest(
            request)

        if not all([bool(request.GET.get('query')),
                    bool(request.GET.get('checkin'))]):
            searchUrl = request.build_absolute_uri(self.defaultSearchUrl)
            return HttpResponseRedirect(searchUrl)

        roomRequired = len(roomsConfig)
        validateQuery = request.GET.get('query', '')
        validateCheckin = request.GET.get('checkin', '')
        FotConfigObj = FotConfig.objects.get(id=1)
        startDayThreshold = FotConfigObj.start_day_threshold
        endDayThreshold = FotConfigObj.end_day_threshold
        maxGuestsAllowed = FotConfigObj.max_guests_allowed
        startDayAllowed = datetime.now() + timedelta(days=startDayThreshold)
        startDayAllowed = startDayAllowed.replace(
            hour=0, minute=0, second=0, microsecond=0)
        endDayAllowed = datetime.now() + timedelta(days=endDayThreshold)
        endDayAllowed = endDayAllowed.replace(
            hour=0, minute=0, second=0, microsecond=0)

        if not startDayAllowed <= checkInDate or not checkInDate <= endDayAllowed:
            # this is done to ensure it does not redirect if fot/search is
            # reffered directly without any params
            if not validateQuery == '' and not validateCheckin == '':
                context = {
                    'jsvars': str(
                        json.dumps(
                            {
                                'startDayThreshold': startDayThreshold,
                                'endDayThreshold': endDayThreshold,
                                'maxGuestsAllowed': maxGuestsAllowed,
                                'dateRangeFailed': True,
                                'urlRedirect': self.defaultSearchUrl}))}
                return context

        try:
            HotelEnquiryValidator(
                checkInDate,
                checkOutDate,
                roomsConfig).validate()
        except TreeboValidationException as e:
            logger.info("Invalid search request received: %s", str(e))
            if e.code == error_codes.INVALID_DATES:
                return HttpResponseRedirect(self.defaultSearchUrl)
            raise e
        roomsConfigParams = self.__get_rooms_config_params(roomsConfig)
        queryUrl = '?city={0}'.format(
            searchString)
        # new search api
        search_rest_api = reverse('search_v2:search_api') + queryUrl
        logger.info('Search API URL: %s', search_rest_api)
        response = requests.get(request.build_absolute_uri(search_rest_api))
        search_result = response.json()["data"]

        hotels = search_result['result']

        #  --------------------   FOT logic ----------------- <
        userId = request.user.id
        fotObj = Fot.objects.get(user_id=userId)
        today = datetime.now().date()
        # for credits redemption
        fotUserCredits = fotObj.user_credits
        max_credits_to_be_redeemed = FotConfigObj.max_credits_to_be_redeemed
        credit_equivalent_per_night = int(
            FotConfigObj.credit_equivalent_per_night)
        max_allowed_credits = min(max_credits_to_be_redeemed, fotUserCredits)
        maxCreditsList = [
            x *
            credit_equivalent_per_night for x in range(
                1,
                (max_allowed_credits //
                 credit_equivalent_per_night) +
                1)]
        fotHotelList = FotHotel.objects.all().order_by('hotel_id')
        fotHotelList_ids = list([a.hotel_id for a in fotHotelList])
        hotels[:] = [x for x in hotels if x['id'] in fotHotelList_ids]
        # fetching the hotel blacklist
        fotHotelBlackList = FotHotelBlackOut.objects.filter(
            blackout_start_time__lt=checkInDate,
            blackout_end_time__gte=checkInDate,
            status='ACTIVE').values_list(
            'fot_hotel_id__hotel_id',
            flat=True).order_by('fot_hotel_id__hotel_id')
        # fetching the user active and allowed slot count
        userSlotsLimit = FotReservationsRequest.objects.filter(
            user_id=userId, status__in=[
                'PENDING', 'SCHEDULED'], check_in_date__gte=today).count()
        userSlotsAllowed = Fot.objects.get(user_id=request.user.id).user_slots
        # fetching existing user-hotel reservation
        fotHotelsAlreadyForUser = FotReservationsRequest.objects.filter(
            user_id=userId,
            status__in=[
                'PENDING',
                'SCHEDULED'],
            check_in_date__gte=today).distinct().values_list(
            'hotel_id',
            flat=True)
        fotHotelsAlreadyForUserOrdersId = FotReservationsRequest.objects.filter(
            user_id=userId, hotel_id__in=fotHotelsAlreadyForUser, status__in=[
                'PENDING', 'SCHEDULED'], check_in_date__gte=today).values(
            'hotel_id', 'booking__order_id', 'check_in_date')
        fotHotelsAlreadyForUserCheckin = FotReservationsRequest.objects.filter(
            user_id=userId, status__in=[
                'PENDING', 'SCHEDULED'], check_in_date=checkInDate).values('hotel_id')
        userAlreadyBookedToday = bool(fotHotelsAlreadyForUserCheckin)
        #  converting hotel credit to dictionary for fast access
        fotHotelCredit = FotHotel.objects.values(
            'hotel_id', 'credits_per_audit')
        fotHotelCredit = {d["hotel_id"]: d["credits_per_audit"]
                          for d in fotHotelCredit}
        # week is sunday to saturday
        thisWeekSunday = datetime.combine(
            checkInDate.date() -
            timedelta(
                days=(
                    checkInDate.weekday() +
                    1) %
                7),
            time.min)
        thisWeekSaturday = datetime.combine(
            thisWeekSunday.date() + timedelta(days=6), time.max)
        # fetching the hotels and available slots
        fotHotelIdAndSlots = FotHotel.objects.values(
            'hotel_id', 'slots_allowed', 'interval_between_slots')
        # fetching hotels and thier active reservations count
        fotHotelReservationsCount = FotReservationsRequest.objects.filter(
            check_in_date__range=(
                thisWeekSunday,
                thisWeekSaturday)).exclude(
            status__in=[
                FotReservationsRequest.CANCELLED,
                FotReservationsRequest.CANCEL_PENDING,
                FotReservationsRequest.NOSHOW]).values('hotel_id').annotate(
            count=Count('hotel_id'))
        # sorting for reducing comparision time
        fotHotelIdAndSlots, fotHotelReservationsCount = [
            sorted(
                hotelIdsinBothLists, key=itemgetter('hotel_id')) for hotelIdsinBothLists in (
                fotHotelIdAndSlots, fotHotelReservationsCount)]
        # get list of fothotel id and slots
        # get list of no of reservations and ids
        # generate a list of mismatching records where slots allowed <= count
        # is registration
        hotelListsOfInvalidSlots = []
        for hotelList_in_fotHotel in fotHotelIdAndSlots:
            for hotelList_in_fotReservations in fotHotelReservationsCount:
                fotHotelId = hotelList_in_fotHotel.get('hotel_id')
                fotReservationsId = hotelList_in_fotReservations.get(
                    'hotel_id')
                if fotHotelId == fotReservationsId:
                    slotsAllowed = hotelList_in_fotHotel.get('slots_allowed')
                    intervalBetweenSlots = hotelList_in_fotHotel.get(
                        'interval_between_slots')
                    if intervalBetweenSlots == FotHotel.WEEK:
                        intervalBetweenSlots = 1
                    elif intervalBetweenSlots == FotHotel.MONTH:
                        intervalBetweenSlots = 4
                    elif intervalBetweenSlots == FotHotel.YEAR:
                        intervalBetweenSlots = 53
                    else:
                        intervalBetweenSlots = 1
                    # TODO: Need for more logic verification according to use
                    # cases - convert to char field
                    slotsPerWeek = int(
                        slotsAllowed / int(intervalBetweenSlots))
                    reservationCount = hotelList_in_fotReservations.get(
                        'count')
                    if slotsPerWeek <= reservationCount:
                        hotelListsOfInvalidSlots.append(fotHotelId)

        for hotel in hotels:
            if userAlreadyBookedToday:
                hotelAvailability = 'closedaudit'
            elif userSlotsLimit >= userSlotsAllowed:
                hotelAvailability = 'closedaudit'
            elif hotel['id'] in fotHotelBlackList:
                hotelAvailability = 'closedaudit'
            elif hotel['id'] in hotelListsOfInvalidSlots:
                hotelAvailability = 'closedaudit'
            elif bool(get_availability_service().get_available_rooms(int(hotel['id']), checkInDate, checkOutDate, roomsConfig)):
                hotelAvailability = 'scheduleaudit'
            else:
                hotelAvailability = 'soldout'
            orderId = ''
            cancelAuditDate = ''
            if hotel['id'] in fotHotelsAlreadyForUser:
                # should be put in a seperate if loop to override above
                hotelAvailability = 'canceldaudit'
                for item in fotHotelsAlreadyForUserOrdersId:
                    if item['hotel_id'] == hotel['id']:
                        orderId = item['booking__order_id']
                        cancelAuditDate = item['check_in_date']

            hotel['credits'] = fotHotelCredit.get(hotel['id'])
            hotel['orderId'] = orderId
            hotel['available'] = hotelAvailability
            hotel['cancelAuditDate'] = cancelAuditDate
        # --------------------   FOT logic ----------------- >

        sortedCityDetails = CityHotelHelper.getCityHotels()

        no_of_nights = (checkOutDate - checkInDate).days
        context = {
            'cities': sortedCityDetails,
            'results': hotels,
            'footerCopyrightDisabled': True,
            'userSlotsAllowed': userSlotsAllowed,
            'auditcount': userSlotsLimit,
            'maxGuestsAllowed': list(
                range(maxGuestsAllowed)),
            'maxCreditsList': maxCreditsList,
            'scripts': ['js/search.js'],
            'userAlreadyBookedToday': userAlreadyBookedToday,
            'queryParams': {
                'checkIn': dateformat.format(
                    checkInDate,
                    "jS M'y"),
                'checkOut': dateformat.format(
                    checkOutDate,
                    "jS M'y"),
                'no_of_nights': no_of_nights,
                'rooms': roomRequired,
                'search': searchString,
                'latitude': latitude,
                'longitude': longitude,
                'locality': locality},
            'jsvars': str(
                json.dumps(
                    {
                        'locality': searchString,
                        'city': searchString,
                        'userCredits': fotUserCredits,
                        'startDayThreshold': startDayThreshold,
                        'endDayThreshold': endDayThreshold,
                        'maxGuestsAllowed': maxGuestsAllowed,
                        'dateRangeFailed': False,
                        'max_allowed_credits': max_allowed_credits}))}

        return context

    def __get_rooms_config_params(self, roomsConfig):
        # Assuming roomsConfig values are single integers or - separated values
        roomConfigList = roomsConfig.split('-')
        if len(roomConfigList) != 2:
            roomsConfig = roomsConfig + '-' + '0'
        return roomsConfig

    def __parseRequest(self, request):

        d = request.GET
        query = d.get('query', 'Bengaluru')
        query = str(str(query).encode('ascii', 'ignore'))
        try:
            checkInDate = datetime.strptime(
                d.get('checkin', ''), date_time_utils.DATE_FORMAT)
        except ValueError:
            logger.error(
                "Invalid date format received for checkIn: %s", d.get(
                    'checkin', "None"))
            checkInDate = datetime.now()
            checkInDate = checkInDate + timedelta(days=1)
            checkInDate = checkInDate.replace(
                hour=0, minute=0, second=0, microsecond=0)
        try:
            checkOutDate = datetime.strptime(
                d.get('checkout', ''), date_time_utils.DATE_FORMAT)
        except ValueError:
            logger.error(
                "Invalid date format received for checkOut: %s", d.get(
                    'checkout', "None"))
            checkOutDate = checkInDate + timedelta(days=1)
            checkOutDate = checkOutDate.replace(
                hour=0, minute=0, second=0, microsecond=0)

        roomConfigString = d.get('roomconfig', '1-0')
        if roomConfigString == '':
            roomConfigString = '1-0'

        if len(roomConfigString) == 1:
            roomConfigString += '-0'

        latitude = d.get('lat') if 'lat' in d and d.get(
            'lat') != "" else const.BANGALORE_LAT
        longitude = d.get('long') if 'long' in d and d.get(
            'long') != "" else const.BANGALORE_LONG
        locality = d.get('locality') if 'locality' in d and d.get(
            'locality') != "" else const.DEFAULT_LOCALITY

        FotConfigObj = FotConfig.objects.get(id=1)
        startDayThreshold = FotConfigObj.start_day_threshold
        checkoutDayThreshold = startDayThreshold + 1

        self.defaultSearchUrl = "{0}?locality={1}&lat={2}&long={3}&query={4}&checkin={5}&checkout={6}&roomconfig={7}".format(
            reverse('pages:SearchAudit'),
            locality,
            latitude,
            longitude,
            query,
            (datetime.now() +
             timedelta(
                days=startDayThreshold)).strftime(
                date_time_utils.DATE_FORMAT),
            (datetime.now() +
             timedelta(
                days=checkoutDayThreshold)).strftime(
                    date_time_utils.DATE_FORMAT),
            roomConfigString)

        return query, checkInDate, checkOutDate, roomConfigString, latitude, longitude, locality
