
from apps.reviews.cache.cache_service import CacheService
from apps.reviews.subscribers.connection import RedisCache


class PubSub(object):

    redis_cache = RedisCache()
    cache_service = CacheService()

    def __init__(self):
        self.pubsub = self.redis_cache.connection.pubsub()

    def subscribe(self,):
        raise NotImplementedError("Cannot be implemented")

    def publish(self, key):
        raise NotImplementedError("Cannot be implemented")
