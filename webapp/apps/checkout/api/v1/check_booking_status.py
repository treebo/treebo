import logging

import requests
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.reverse import reverse

from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from base.logging_decorator import log_args
from base.views.api import TreeboAPIView
from common.constants import common_constants
from services.restclient.bookingrestclient import BookingClient
from data_services.respositories_factory import RepositoriesFactory
from data_services.exceptions import HotelDoesNotExist

logger = logging.getLogger(__name__)


class CheckBookingStatus(TreeboAPIView):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CheckBookingStatus, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, _format=None):
        logger.info("format " + str(_format))
        if 'pretty' in request.GET:
            return self.post(request)
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request):
        reference_id = request.data.get("reference_id")
        booking_request = BookingRequest.objects.get(pk=reference_id)
        context = {
            "status": common_constants.SUCCESS,
            "msg": "Awaiting Confirmation",
            "jobstatus": "IN_PROGRESS"
        }
        try:
            status = BookingClient.is_booking_confirmed(
                booking_request.order_id)
            context.update({
                "jobstatus": status
            })
            if status == "Confirm":
                if booking_request.is_audit:
                    hotel_repository = RepositoriesFactory.get_hotel_repository()
                    hotels = hotel_repository.filter_hotels(
                        id=booking_request.hotel_id)
                    if not hotels:
                        logger.error(
                            "Could not find hotel with id {}".format(
                                booking_request.hotel_id))
                        raise HotelDoesNotExist
                    #hotel = Hotel.objects.prefetch_related('city', 'locality').get(pk=booking_request.hotel_id)
                    hotel = hotels[0]
                    redirect_url = BookingHelper.build_fot_confirmation_page(
                        booking_request, hotel)
                else:
                    redirect_url = reverse(
                        "pages:order", kwargs={
                            "order_id": booking_request.order_id})
                context.update({
                    "msg": "Booking is confirmed",
                    "redirectUrl": redirect_url,
                    "order_id": booking_request.order_id
                })
            elif status == "Failure":
                context.update(
                    {
                        "msg": "Booking has failed",
                        "redirectUrl": BookingHelper.build_itinerary_page_url_with_error(
                            booking_request,
                            'Unable to confirm booking'),
                        "order_id": booking_request.order_id})
        except requests.HTTPError:
            logger.exception("Unable to determine the status of booking")
        return Response(context)
