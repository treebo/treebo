from django.db import models
from djutil.models import TimeStampedModel

from dbcommon.models.default_permissions import DefaultPermissions


class Partner(TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20)
    email = models.EmailField()
    hotel_name = models.CharField(max_length=200)
    query = models.TextField()
    is_contacted = models.BooleanField(default=False)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_partner'


class Corporate(TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20)
    email = models.EmailField()
    organisation = models.CharField(max_length=200)
    query = models.TextField()
    is_contacted = models.BooleanField(default=False)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_corporate'


class PartnerInfo(TimeStampedModel, DefaultPermissions):
    name = models.CharField(max_length=200)
    role = models.CharField(max_length=200)
    email = models.EmailField()
    phone_number = models.CharField(max_length=20)
    hotel_name = models.CharField(max_length=200)
    hotel_phone_number = models.CharField(max_length=20)
    hotel_address1 = models.CharField(max_length=200)
    hotel_address2 = models.CharField(max_length=200, null=True, blank=True)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    pincode = models.PositiveIntegerField()
    no_of_rooms = models.PositiveIntegerField()
    average_room_rate = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    current_occupancy = models.CharField(max_length=20)
    extra_info = models.TextField(max_length=2000, null=True, blank=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_partnerinfo'
