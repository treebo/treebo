

from django.conf.urls import url
from apps.pricing.api.v2.cheapest_room_and_price_for_all_hotels import PricingAPIView
from apps.pricing.api.v1.views.get_price import Pricing
from apps.pricing.api.v2.pricing import PricingDetails

app_name = 'pricing_v2'
urlpatterns = [
    url(r'^availability/$', Pricing.as_view(), name='pAvailability'),
    # url(r'^hotels/$', PricingAPIView.as_view(), name='cheapest_prices_for_hotels_v2'),
    url(r'^pricingdetails/$', PricingDetails.as_view(), name='pricing_details'),

]
