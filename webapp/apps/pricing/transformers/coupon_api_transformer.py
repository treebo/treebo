
class CouponAPITransformer(object):
    @staticmethod
    def _default_price_component():
        default_price_component = {
            "rate_plan": {
                "code": None,
                "description": None,
                "tag": None
            },
            "price": {
                "base_price": 0,
                "rack_rate": 0,
                "base_price_tax": 0,
                "voucher_amount": 0,
                "member_discount": 0,
                "coupon_discount": 0,
                "promo_discount": 0,
                "pretax_price": 0,
                "final_pretax_price": 0,
                "tax": 0,
                "sell_price": 0,
                "total_discount_percent": 0,
                "nights_breakup": [],
            }
        }
        return default_price_component

    def transform_price_for_display(self, prices, selected_rate_plan):
        total_price = prices.get('cumulative_price')
        date_wise_breakup = prices.get('date_wise_breakup')
        price_breakup = self._build_price_breakup(date_wise_breakup)
        price = {}
        rate_plan_detail = total_price.get(selected_rate_plan)

        member_discount_available = False
        member_discount_applied = False
        if total_price and rate_plan_detail:
            rp_price = rate_plan_detail.get('price')
            rate_plan_meta = rate_plan_detail.get('meta')
            member_discount_available = member_discount_available or rp_price.member_discount_available
            member_discount_applied = member_discount_applied or rp_price.member_discount_applied
            total_discount_percent = rp_price.get_total_discount_percent()

            rate_plan_dict = dict(code=selected_rate_plan)
            if rate_plan_meta:
                rate_plan_dict.update(rate_plan_meta)

            selected_rate_plan_price = {
                "rate_plan": rate_plan_dict,
                "price": {
                    "base_price": rp_price.base_price,
                    "rack_rate": rp_price.rack_rate,
                    "base_price_tax": rp_price.base_price_tax,
                    "voucher_amount": 0,
                    "member_discount": rp_price.member_discount,
                    "coupon_discount": rp_price.coupon_discount,
                    "promo_discount": rp_price.promo_discount,
                    "pretax_price": rp_price.base_price -
                    rp_price.promo_discount,
                    "final_pretax_price": rp_price.pretax_price,
                    "tax": rp_price.tax,
                    "sell_price": rp_price.sell_price,
                    "total_discount_percent": total_discount_percent,
                    "nights_breakup": price_breakup}}
            price['selected_rate_plan'] = selected_rate_plan_price
        else:
            price['selected_rate_plan'] = self._default_price_component()

        price['member_discount_applied'] = member_discount_applied
        price['member_discount_available'] = member_discount_available

        return price

    def retrieve_coupon_discount(self, prices, selected_rate_plan):
        total_price = prices.get('cumulative_price')
        rp_price = total_price.get(selected_rate_plan)
        if total_price and rp_price:
            return rp_price.get('price').coupon_discount
        else:
            return 0

    def _build_price_breakup(self, date_wise_price_breakup):
        price_breakup = []
        for date_, price in list(date_wise_price_breakup.items()):
            price_breakup.append(dict(sell_price=price.sell_price,
                                      pretax_price=price.pretax_price,
                                      total_discount=price.promo_discount,
                                      date=date_,
                                      tax=price.tax))

        sorted_price_breakup = sorted(price_breakup, key=lambda p: p['date'])
        return sorted_price_breakup
