from django.conf import settings
from django.core.cache import cache
from django.db.models import Q
from django.utils.text import slugify

from apps.content.constants import PAGES
from apps.posts.service import PostService
from apps.seo.components.breadcrumb import BreadCrumb
from apps.seo.components.categories import Category
from apps.seo.components.cities_nearby import CitiesNearby
from apps.seo.components.city_coordinate_component import CityCoordinateComponent
from apps.seo.components.left_nav import LeftNav
from apps.seo.components.popular_cities import PopularCities
from apps.seo.components.top_reviews import TopReviews
from apps.seo.components.weather_widget import WeatherWidget
from apps.seo.pages.pages import PAGES
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Categories, CategorySeoDetails
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'ansilkareem'


class CategoryPage(PAGES):

    def __init__(self, page, query, is_amp):
        self.is_amp = is_amp
        city = query.split('-')[-1].strip('-')
        self.category_name = ' '.join(query.split('-')[:-1]).strip()
        self.category = Categories.objects.filter(
            status=1, name__iexact=self.category_name).first()
        city_data_service = RepositoriesFactory.get_city_repository()
        city_search_args = []
        city_search_args.append(
            {'name': 'name', 'value': city, 'case_sensitive': False, 'partial': False})
        city_search_args.append(
            {'name': 'slug', 'value': city, 'case_sensitive': False, 'partial': False})
        current_cities = city_data_service.filter_cities_by_string_matching(
            city_search_args, {'status': 1})
        if current_cities:
            self.current_city = current_cities[0]
        else:
            self.current_city = None
        # self.current_city = City.objects.filter(Q(name__iexact=city, status=1) | Q(slug__iexact=city, status=1)).first()
        self.about_key = 'about_' + '_'.join(self.category_name.lower(
        ).split()) + '_' + '_'.join(self.current_city.name.lower().split())
        self.description = ContentServiceClient.getValueForKey(
            None, self.about_key, 1)
        super(CategoryPage, self).__init__(page, query, self.category_name)

    def update_meta(self, ):
        min_price_key = str(self.current_city.name) + '_min_price'
        min_sell_price_key = str(self.current_city.name) + '_min_sell_price'
        min_rack_rate_key = str(self.current_city.name) + '_min_rack_rate'
        max_discount_key = str(self.current_city.name) + '_max_discount'
        min_price = cache.get(min_price_key)
        min_sell_price = cache.get(min_sell_price_key)
        min_rack_rate = cache.get(min_rack_rate_key)
        max_discount = cache.get(max_discount_key)
        if max_discount:
            total_discount = str(max_discount) + "%"
        else:
            total_discount = str(settings.DEFAULT_DISCOUNT) + "%"
        temp_dict = {
            "category_name": self.category.name,
            "city_name": self.current_city.slug.title() if self.current_city.slug else self.current_city.name.title(),
            "price": str(min_price) if min_price else str(settings.DEFAULT_PRICE),
            "sell_price": str(min_sell_price) if min_sell_price else str(
                settings.DEFAULT_SELL_PRICE),
            "rack_rate": str(min_rack_rate) if min_rack_rate else str(settings.DEFAULT_RACK_RATE),
            "total_discount": total_discount
        }
        category_seo_details = CategorySeoDetails.objects.filter(category=self.category,
                                                                 city=self.current_city).first()
        full_canonical_url = settings.SITE_HOST_NAME + slugify(self.category.name) + '-in-' + \
                             slugify(self.current_city.slug) + '/'
        amp_url = full_canonical_url + 'amp/'
        if category_seo_details:
            title = category_seo_details.category_seo_title % temp_dict
            description = category_seo_details.category_seo_description % temp_dict
            self.override_seo_content(title,
                                      description,
                                      category_seo_details.category_seo_keyword,
                                      full_canonical_url, is_amp=self.is_amp, amp_url=amp_url)
        else:
            title = None
            description = None
            if 'category_title' in self.footer_data:
                title = self.footer_data['category_title'] % temp_dict
            if 'category_description' in self.footer_data:
                description = self.footer_data['category_description'] % temp_dict
            if title and description:
                self.override_seo_content(title,
                                          description,
                                          None,
                                          full_canonical_url, is_amp=self.is_amp, amp_url=amp_url)

    def build_meta(self, *args):
        for arg in args:
            ta_review_count = arg.get('ta_review_count')
        self.update_meta()
        temp_dict = {
            'category_name': self.category.name.upper(),
            'city_name': self.current_city.slug.upper() if self.current_city.slug
            else self.current_city.name.upper()
        }
        if 'category_about_title' in self.footer_data:
            about_title = self.footer_data['category_about_title'] % temp_dict
        else:
            about_title = ''
        self.meta_content['content'] = {
            'description': {'title': about_title,
                            'content': self.description['text']
                            } if self.description else {}
        }
        min_price_key = str(self.current_city.name) + '_min_sell_price'
        min_price_value = cache.get(min_price_key)
        self.meta_content['content'].update(
            {'min_price': min_price_value if min_price_value else settings.DEFAULT_SELL_PRICE})
        top_reviews = TopReviews(self.current_city, self.footer_data, ta_review_count, [])
        self.meta_content['content'].update(top_reviews.
                                            build_reviews_using_area_name(page=PAGES.CATEGORY,
                                                                          category_name=self.category.name))
        left_nav = LeftNav(self.current_city, self.footer_data, [])
        self.meta_content['content'].update(left_nav.build())
        self.meta_content['content'].update({
            'city_image': self.current_city.seo_image if self.current_city.seo_image else self.city_image_url
        })
        cities_nearby = CitiesNearby(self.current_city, self.footer_data)
        self.meta_content['content'].update(
            {
                'links': [
                    cities_nearby.build()],
                'breadcrumbs': BreadCrumb(
                    self.current_city,
                    self.footer_data,
                    self.page,
                    self.item_name).build()})

        # seo ph2
        area = {}
        area['page'] = self.page
        area['identifier'] = self.current_city
        weather_widget = WeatherWidget(area, self.footer_data)
        self.meta_content['content'].update(weather_widget.build())
        city_coordinate = CityCoordinateComponent(area, self.footer_data)
        self.meta_content['content'].update(city_coordinate.build())
        popular_cities = PopularCities(
            self.current_city, self.footer_data, [
                city.id for city in cities_nearby.sorted_nearby_cities])
        self.meta_content['content']['links'].append(popular_cities.build())
        category = Category(self.current_city, self.footer_data, [])
        self.meta_content['content']['links'].append(category.build())
