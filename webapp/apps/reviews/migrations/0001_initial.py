# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0049_auto_20170412_1052'),
    ]

    operations = [
        migrations.CreateModel(
            name='HotelTAOverallRatings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('overall_rating', models.DecimalField(default=0.0, max_digits=20, decimal_places=10)),
                ('url_overall_rating_image', models.TextField()),
                ('write_review_url', models.TextField()),
                ('read_review_url', models.TextField()),
                ('overall_ranking', models.PositiveIntegerField(default=0)),
                ('overall_ranking_out_of', models.PositiveIntegerField(default=0)),
                ('overall_ranking_string', models.TextField(default=True, null=True)),
                ('geo_location_name', models.CharField(max_length=200, null=True, blank=True)),
                ('num_reviews_count', models.PositiveIntegerField(default=0)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'trip_advisor_hotel_id_mapping',
                'verbose_name': 'TA Hotel ID Mapping',
                'verbose_name_plural': 'TA Hotel ID Mapping',
            },
        ),
        migrations.CreateModel(
            name='ReviewAppConfiguration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('enable_review_app', models.BooleanField(default=False)),
                ('enable_for_all_hotels', models.BooleanField(default=False)),
                ('disable_for_hotel_list', models.TextField(default=b'', null=True, blank=True)),
                ('enable_for_hotel_list', models.TextField(default=b'', null=True, blank=True)),
                ('is_audit_enable', models.BooleanField(default=False)),
                ('percent_for_ranking_visibility', models.PositiveIntegerField(default=15)),
                ('num_subratings_to_show', models.PositiveIntegerField(default=4)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'trip_advisor_app_toggle',
                'verbose_name': 'TA App Toggle',
                'verbose_name_plural': 'TA App Toggle',
            },
        ),
        migrations.CreateModel(
            name='TAAPIAuditTable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('location_api_url', models.TextField(default=b'', null=True, blank=True)),
                ('location_api_url_response_json', models.TextField(default=b'', null=True, blank=True)),
                ('location_api_url_response_status', models.CharField(default=b'SUCCESS', max_length=100, choices=[(b'SUCCESS', b'SUCCESS'), (b'FAILURE', b'FAILURE')])),
                ('location_reviews_api_url', models.TextField(default=b'', null=True, blank=True)),
                ('location_reviews_api_url_response_json', models.TextField(default=b'', null=True, blank=True)),
                ('location_reviews_api_url_response_status', models.CharField(default=b'SUCCESS', max_length=100, choices=[(b'SUCCESS', b'SUCCESS'), (b'FAILURE', b'FAILURE')])),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'trip_advisor_api_audit',
                'verbose_name': 'TA API Audit',
                'verbose_name_plural': 'TA API Audit',
            },
        ),
        migrations.CreateModel(
            name='TAAwardsHotelMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('award_image_url', models.TextField(null=True, blank=True)),
                ('award_year', models.CharField(max_length=20, null=True, blank=True)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'trip_advisor_awards_hotel_mapping',
                'verbose_name': 'TA Awards Hotel Mapping',
                'verbose_name_plural': 'TA Awards Hotel Mapping',
            },
        ),
        migrations.CreateModel(
            name='TAAwardslist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('award_name', models.TextField(unique=True)),
                ('award_category', models.TextField(null=True, blank=True)),
                ('is_enabled', models.BooleanField(default=True)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'trip_advisor_awards',
                'verbose_name': 'TA Awards',
                'verbose_name_plural': 'TA Awards',
            },
        ),
        migrations.CreateModel(
            name='TAHotelIDMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('ta_location_id', models.CharField(max_length=200)),
                ('hotel', models.ForeignKey(to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'trip_advisor_hotel_mapping',
                'verbose_name': 'TA Hotel ID Mapping',
                'verbose_name_plural': 'TA Hotel ID Mapping',
            },
        ),
        migrations.CreateModel(
            name='TAHotelReviews',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('ta_review_id', models.CharField(max_length=20)),
                ('review_text', models.TextField(null=True, blank=True)),
                ('review_title', models.TextField(null=True, blank=True)),
                ('review_published_date', models.DateTimeField(null=True, blank=True)),
                ('helpful_votes', models.CharField(max_length=20, null=True, blank=True)),
                ('user_rating', models.DecimalField(default=0.0, max_digits=20, decimal_places=10)),
                ('user_rating_image_url', models.TextField(null=True, blank=True)),
                ('user_rating_text_url', models.TextField(null=True, blank=True)),
                ('user_name', models.CharField(max_length=200, null=True, blank=True)),
                ('user_image_url', models.TextField(null=True, blank=True)),
                ('owner_rating_reply_text', models.TextField(null=True, blank=True)),
                ('owner_description', models.TextField(null=True, blank=True)),
                ('owner_reply_published_date', models.DateTimeField(null=True, blank=True)),
                ('review_is_active', models.BooleanField(default=True)),
                ('hotel_ta_mapping', models.ForeignKey(to='reviews.TAHotelIDMapping', on_delete=models.DO_NOTHING)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'trip_advisor_top_reviews',
                'verbose_name': 'TA Awards Reviews',
                'verbose_name_plural': 'TA Awards Reviews',
            },
        ),
        migrations.CreateModel(
            name='TAHotelSubratingMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('subrating_value', models.DecimalField(default=0.0, max_digits=20, decimal_places=10)),
                ('url_subrating', models.TextField()),
                ('hotel_ta_mapping', models.ForeignKey(to='reviews.TAHotelIDMapping', on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
                'verbose_name_plural': 'TA Hotel Sub Rating Mapping',
                'db_table': 'trip_advisor_hotel_subrating_mapping',
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'verbose_name': 'TA Hotel Sub Rating Mapping',
            },
        ),
        migrations.CreateModel(
            name='TAReviewRatingCountMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('review_rating', models.CharField(default=b'5', max_length=100, choices=[(b'1', b'1'), (b'2', b'2'), (b'3', b'3'), (b'4', b'4'), (b'5', b'5')])),
                ('count_of_customers', models.PositiveIntegerField(default=0)),
                ('percentage_of_customers', models.DecimalField(default=0.0, max_digits=20, decimal_places=10)),
                ('hotel_ta_mapping', models.ForeignKey(to='reviews.TAHotelIDMapping', on_delete=models.DO_NOTHING)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'trip_advisor_review_rating_hotel_mapping',
                'verbose_name': 'TA Review Rating Mapping',
                'verbose_name_plural': 'TA Review Rating Mapping',
            },
        ),
        migrations.CreateModel(
            name='TASubrating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('subrating_name', models.TextField(unique=True, null=True, blank=True)),
                ('subrating_localized_name', models.TextField(null=True, blank=True)),
                ('is_enabled', models.BooleanField(default=True)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'trip_advisor_subratings',
                'verbose_name': 'TA Subratings',
                'verbose_name_plural': 'TA Subratings',
            },
        ),
        migrations.AddField(
            model_name='tahotelsubratingmapping',
            name='subrating',
            field=models.ForeignKey(to='reviews.TASubrating', on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='taawardshotelmapping',
            name='award',
            field=models.ForeignKey(to='reviews.TAAwardslist', on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='taawardshotelmapping',
            name='hotel_ta_mapping',
            field=models.ForeignKey(to='reviews.TAHotelIDMapping', on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='taapiaudittable',
            name='hotel_ta_mapping',
            field=models.ForeignKey(to='reviews.TAHotelIDMapping', on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='hoteltaoverallratings',
            name='hotel_ta_mapping',
            field=models.ForeignKey(to='reviews.TAHotelIDMapping', on_delete=models.DO_NOTHING),
        ),
        migrations.AlterUniqueTogether(
            name='tahotelsubratingmapping',
            unique_together=set([('hotel_ta_mapping', 'subrating')]),
        ),
    ]
