import logging
import json
import urllib.request
import urllib.parse
import urllib.error
from django.contrib.auth import login
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status

from apps.common import utils
from apps.auth.services.user_verification_service import UserVerificationService
from apps.common.utils import AnalyticsEventTracker
from apps.profiles.service.user_registration import UserRegistration
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboValidationException
from services.common_services.otp_service import OtpService
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication

logger = logging.getLogger(__name__)


# Check how to write this validator from
# http://www.django-rest-framework.org/api-guide/serializers/#validation
class UserRegisterValidator(serializers.Serializer):
    """UserRegisterSerializer"""
    requiredErrorMessage = {
        'required': error_codes.INVALID_LOGIN_DATA,
        'blank': error_codes.INVALID_LOGIN_DATA,
    }
    name = serializers.CharField(
        max_length=100,
        required=True,
        error_messages=requiredErrorMessage)
    password = serializers.CharField(
        required=True, error_messages=requiredErrorMessage)
    mobile = serializers.IntegerField(
        required=True,
        min_value=(
            10 ** 9),
        max_value=(
            10 ** 10 - 1),
        error_messages={
            'required': error_codes.INVALID_LOGIN_DATA,
            'blank': error_codes.INVALID_LOGIN_DATA,
            'min_value': error_codes.INVALID_MOBILE_NO,
            'max_value': error_codes.INVALID_MOBILE_NO,
            'invalid': error_codes.INVALID_MOBILE_NO})
    email = serializers.EmailField(
        required=True,
        error_messages={
            'required': error_codes.INVALID_LOGIN_DATA,
            'invalid': error_codes.INVALID_LOGIN_DATA,
            'blank': error_codes.INVALID_LOGIN_DATA,
        })


class RegisterAPI(TreeboAPIView):
    # validationSerializer = {'get': UserRegisterValidator, 'post': UserRegisterValidator}
    # validationSerializer = UserRegisterValidator
    authentication_classes = [CsrfExemptSessionAuthentication, ]

    # don't know what this does - kaddy
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(RegisterAPI, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        raise Exception('Method not supported')

    def post(self, request, format=None):
        """Register user
        """

        data = request.data
        click_id = str(request.COOKIES.get('avclk')
                       ) if request.COOKIES.get('avclk') else ''
        referral_code = ''
        name = data['name'].split(" ", 2)
        try:
            user_registration = UserRegistration()
            user_dto = {
                'email': data['email'],
                "first_name": name[0].strip(),
                "last_name": name[1].strip() if len(name) is 2 else "",
                "phone_number": data['mobile'],
                "password": data['password'],
            }
            logger.info("registerwith email {} phone_number {}".format(user_dto['email'],
                                                                              user_dto['phone_number']))
            user = user_registration.register_internally(
                user_dto, click_id=click_id, referral_code=referral_code)
            utils.trackAnalyticsServerEvent(request,
                                            'Signup Successful',
                                            {"medium": 'EMAIL',
                                             'email': data['email'],
                                                'userid': user.id,
                                                'name': user.first_name},
                                            channel=AnalyticsEventTracker.WEBSITE)
            self._loginUser(user, request)
            response = Response({
                'status': 'success',
                'msg': 'User successfully registered',
                'isSignUp': True,
                'userId': user.id,
                'email': user.email,
                'phone': user.phone_number,
                'name': user.first_name,
                'last_name': user.last_name
            })
            authenticated_user_cookie_map = {
                "userId": user.id,
                "email": user.email,
                "phone": user.phone_number,
                "name": user.first_name,
            }
            response.set_cookie(
                'authenticated_user', urllib.parse.quote(
                    json.dumps(authenticated_user_cookie_map)))
            return response
        except TreeboValidationException as e:
            if hasattr(request, 'is_mobile') and request.is_mobile:
                return Response({
                    'status': 'error',
                    'redirect': True,  # For front end redirection
                    'msg': str(e)
                })
            else:
                return Response({"error": {"msg": str(e),
                                           'status': 'error',
                                           'redirect': True
                                           }
                                 }, status=status.HTTP_400_BAD_REQUEST)

    def _loginUser(self, user, request):
        logger.info('logging in the user')
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)


class UserOtpAPI(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserOtpAPI, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request):
        mobile = request.data.get('mobile', '')
        if request.user.is_authenticated():
            request.user.phone_number = mobile
            request.user.save()
            OtpService.send_otp(mobile, request.user.first_name)
            return Response(data={
                'status': 'success',
                'msg': 'Otp successfully sent',
            })
        return Response(data={
            'status': 'error',
            'msg': "User isn't logged in",
        })


class VerifyUserAPI(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(VerifyUserAPI, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    def post(self, request):
        mobile = request.data.get('mobile', '')
        otp = request.data.get('otp', '')
        if request.user.is_authenticated():
            is_verified, post_verification_data = UserVerificationService.verify_user(
                request.user.id, mobile, otp)
            if is_verified:
                return Response({
                    "status": "success",
                    "msg": "User verified"
                })
        return Response({
            "status": "error",
            "msg": "User verification failed"
        })
