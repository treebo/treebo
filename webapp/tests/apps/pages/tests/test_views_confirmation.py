import mock
from django.test import TestCase, RequestFactory


class ConfirmationViewTest(TestCase):
    """
    Just doing a mock setup of all the methods required to be tested
    """

    def setUp(self):
        self.factory = RequestFactory()
        self.mock_confirmation_service = mock.Mock()
        self.mock_booking_service = mock.Mock()

    def test_add_to_cart_return_error_on_failed_addToCart_api(self):
        pass

    def test_add_to_cart_return_None_on_success_addToCart_api(self):
        pass

    def test_get_booking_user_detail_return_empty_data_for_non_authenticated_user(
            self):
        pass

    def test_get_booking_user_detail_for_authenticated_user(self):
        pass

    def test_parse_desktop_request_return_session_data_for_booking_id_in_session(
            self):
        pass

    def test_parse_desktop_request_return_GET_request_param_for_missing_booking_id_in_session(
            self):
        pass

    def test_get_room_types_from_hotelogix_return_empty_xml_on_exception(self):
        pass

    def test_get_room_types_from_hotelogix_return_valid_xml(self):
        pass

    def test_get_itinerary_data(self):
        # Main method
        pass
