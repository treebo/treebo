from rest_framework import serializers

from dbcommon.models.facilities import Facility

__author__ = 'Rajdeep'


class FacilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields = ('name', 'url', 'css_class')
