import logging

from base.views.template import TreeboTemplateView
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'devang'

logger = logging.getLogger(__name__)


class Policy(TreeboTemplateView):
    template_name = "privacy-policy/index.html"
    redirect_mobile_to_desktop_view = True

    def getData(self, request, args, kwargs):
        template_name = "privacy-policy/index.html"
        seo = ContentServiceClient.getValueForKey(request, 'seo_pages', 1)

        return {'scripts': [], 'styles': ['desktop/css/policy.css'],
                'scripts': ['desktop/js/policy.js'],
                'seo': seo['policy'], 'footerCopyrightDisabled': True}
