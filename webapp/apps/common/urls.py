from django.conf.urls import url

from apps.common.api.v1.logger_check import LoggerCheck
from apps.common.api.v1.feature_toggle_status import FeatureToggleStatus
#from apps.common.api.v1.cs_test import CSTest

app_name = 'common'

urlpatterns = [
    url(r"^featuretoggle/", FeatureToggleStatus.as_view(), name='feature-toggle'),
    url(r"^logger-check/", LoggerCheck.as_view(), name='logger-check'),
]
