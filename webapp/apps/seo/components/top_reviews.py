import logging

from django.core.cache import cache
from django.utils.html import escape
from django.utils.text import slugify

from apps.content.constants import PAGES
from apps.hotels.service.hotel_search_service import HotelSearchService
from apps.reviews.models import TAHotelReviews
from apps.reviews.services.trip_advisor.review_config_service import ReviewAppConfigService
from apps.seo.components.base_component import BaseComponent
from apps.seo.serializers.serializer import TopReviewsSerializer
from apps.seo.services.common_services.seo_title_string_util import SeoTitleStringUtil
from services.common_services.url_converter import UrlConverter
from apps.common.exceptions.custom_exception import InvalidPageException

__author__ = 'ansilkareem'

logger = logging.getLogger(__name__)


class TopReviews(BaseComponent):
    MAX_REVIEWS_TO_DISPLAY = 5000
    CACHE_TIMEOUT = 60 * 60

    def __init__(self, current_city, footer_data, tripadvisor_review_count, exclude_list):
        super(
            TopReviews,
            self).__init__(
            current_city,
            footer_data,
            exclude_list),
        self.tripadvisor_review_count = tripadvisor_review_count

    def build_reviews_using_area_name(self, page, category_name=None, amenity_name=None):
        reviews = []
        top_reviews_result = None
        top_reviews_cache_key = self.fetch_cache_key(page=page, category_name=category_name,
                                                     amenity_name=amenity_name)
        if category_name and not self.is_jcr_enabled(category_name):
            return self.build_response_model(reviews=reviews, title='',
                                             top_reviews_cache_key=top_reviews_cache_key)
        try:
            if top_reviews_cache_key:
                top_reviews_result = cache.get(top_reviews_cache_key)
        except Exception as e:
            logger.exception("Unable to fetch reviews from cache: %s", e.__str__())
        if top_reviews_result:
            return top_reviews_result
        search_service = HotelSearchService(self.current_city,
                                            category_name=category_name,
                                            amenity_name=amenity_name)
        hotels = search_service.search()
        hotel_ids = [int(hotel.id) for hotel in hotels]
        reviews = TAHotelReviews.objects.filter(hotel_ta_mapping__hotel__id__in=hotel_ids,
                                                review_is_user_generated=True,
                                                review_is_active=True).prefetch_related(
            'hotel_ta_mapping__hotel').order_by('-user_rating')[:self.tripadvisor_review_count]

        title = SeoTitleStringUtil.get_jcr_page_title(
            page=page,
            city_name=self.current_city.name,
            locality_name=None,
            landmark_name=None,
            category_name=category_name,
            amenity_name=amenity_name)
        return self.build_response_model(reviews, title, top_reviews_cache_key)

    def build_reviews_with_area_radius(self, page, latitude, longitude, distance_cap,
                                       city_name=None,
                                       locality_name=None, landmark_name=None, category_name=None,
                                       amenity_name=None):
        reviews = []
        top_reviews_result = None
        top_reviews_cache_key = self.fetch_cache_key(page=page, category_name=category_name,
                                                     locality_name=locality_name,
                                                     landmark_name=landmark_name)
        if category_name and not self.is_jcr_enabled(category_name):
            return self.build_response_model(reviews=reviews, title='',
                                             top_reviews_cache_key=top_reviews_cache_key)
        try:
            if top_reviews_cache_key:
                top_reviews_result = cache.get(top_reviews_cache_key)
        except Exception as e:
            logger.exception("Unable to fetch reviews from cache: %s", e.__str__())
        if top_reviews_result:
            return top_reviews_result
        title = SeoTitleStringUtil.get_jcr_page_title(
            page,
            city_name,
            locality_name,
            landmark_name,
            category_name,
            amenity_name)
        return self.get_nearby_reviews(latitude, longitude, distance_cap, title, category_name,
                                       amenity_name,
                                       top_reviews_cache_key)

    def get_nearby_reviews(self, latitude, longitude, distance_cap, title, category_name,
                           amenity_name,
                           top_reviews_cache_key=None):

        search_service = HotelSearchService(self.current_city,
                                            latitude=latitude,
                                            longitude=longitude,
                                            distance_cap=distance_cap,
                                            category_name=category_name,
                                            amenity_name=amenity_name)
        hotels = search_service.search()
        hotel_ids = [int(hotel.id) for hotel in hotels]

        reviews = TAHotelReviews.objects.filter(hotel_ta_mapping__hotel__id__in=hotel_ids,
                                                review_is_user_generated=True,
                                                review_is_active=True).prefetch_related(
            'hotel_ta_mapping__hotel').order_by('-user_rating')[:self.tripadvisor_review_count]

        return self.build_response_model(reviews, title,
                                         top_reviews_cache_key=top_reviews_cache_key)

    def build_response_model(self, reviews, title, top_reviews_cache_key=None):
        top_reviews = []
        top_reviews_content = {
            'top_reviews': {
            }
        }
        try:
            for review in reviews:
                temp_dict = {
                    'review_text': str(escape(review.review_text)),
                    'review_ratings': review.user_rating,
                    'rating_image': review.user_rating_image_url,
                    'review_title': review.review_title,
                    'review_date': review.review_published_date,
                    'is_crawlable': review.review_is_crawlable,
                    'hotel': {
                        'name': review.hotel_ta_mapping.hotel.name,
                        'id': review.hotel_ta_mapping.hotel.id
                    },
                    'user': {
                        'name': review.user_name,
                        'image': review.user_image_url
                    }
                }
                temp_dict['rating_image'] = UrlConverter.replace_url_string(
                    temp_dict['rating_image'])
                top_reviews.append(temp_dict)
            serialized_obj = TopReviewsSerializer(data=top_reviews, many=True)
            if serialized_obj.is_valid(True) and serialized_obj.validated_data:
                result = serialized_obj.validated_data
                top_reviews_content = {
                    'top_reviews': {
                        'title': title,
                        'content': result
                    }
                }
            else:
                logger.error("Empty result in serialized reviews data, returning empty dict")
        except Exception as e:
            logger.error('Error in building top reviews %s', str(e))
        if top_reviews_cache_key:
            cache.set(top_reviews_cache_key, top_reviews_content, self.CACHE_TIMEOUT)
        return top_reviews_content

    def is_jcr_enabled(self, category):
        review_config_service = ReviewAppConfigService()
        config = review_config_service.fetch_app_config()
        if not review_config_service.is_disabled_for_category(config=config, category=category):
            return True
        return False

    def fetch_cache_key(self, page, category_name=None, locality_name=None, landmark_name=None,
                        amenity_name=None):
        cache_key = None
        try:
            args = dict(category_name=category_name, locality_name=locality_name,
                        landmark_name=landmark_name,
                        amenity_name=amenity_name)
            keys_with_values = [key for key in args if args[key]]
            modified_args = {key: args[key] for key in keys_with_values}
            page_cache_keys = {
                PAGES.CITY: self.fetch_default_cache_key,
                PAGES.CITY_AMENITY: self.fetch_amenity_cache_key,
                PAGES.CATEGORY: self.fetch_category_cache_key,
                PAGES.LOCALITY: self.fetch_locality_cache_key,
                PAGES.LANDMARK: self.fetch_landmark_cache_key,
                PAGES.LOCALITY_CATEGORY: self.fetch_locality_category_cache_key,
                PAGES.LANDMARK_CATEGORY: self.fetch_landmark_category_cache_key,
            }
            cache_key = page_cache_keys[page](**modified_args)
        except Exception as e:
            logger.exception(e.__str__)
            dev_msg = "Invalid key %s" % page
            raise InvalidPageException(dev_msg)
        return cache_key

    def fetch_default_cache_key(self):
        return 'top_reviews_' + slugify(self.current_city.name)

    def fetch_amenity_cache_key(self, amenity_name):
        return self.fetch_default_cache_key() + '_' + slugify(amenity_name)

    def fetch_category_cache_key(self, category_name):
        return self.fetch_default_cache_key() + '_' + slugify(category_name)

    def fetch_locality_cache_key(self, locality_name):
        return self.fetch_default_cache_key() + '_' + slugify(locality_name)

    def fetch_landmark_cache_key(self, landmark_name):
        return self.fetch_default_cache_key() + '_' + slugify(landmark_name)

    def fetch_locality_category_cache_key(self, locality_name, category_name):
        return self.fetch_default_cache_key() + '_' + slugify(locality_name) + '_' + slugify(
            category_name)

    def fetch_landmark_category_cache_key(self, landmark_name, category_name):
        return self.fetch_default_cache_key() + '_' + slugify(landmark_name) + '_' + slugify(
            category_name)

    @staticmethod
    def invalidate_cache():
        top_reviews_key_pattern = 'top_reviews_*'
        category_page_key_pattern = 'meta_category_*'
        cache.delete_pattern(top_reviews_key_pattern)
        cache.delete_pattern(category_page_key_pattern)
