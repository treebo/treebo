import logging

from django.core.management import BaseCommand

from apps.reviews.subscribers.subscriber import Subscriber

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    subscriber = Subscriber()

    def handle(self, *args, **options):
        try:
            self.subscriber.subscribe()
        except BaseException:
            logger.info('exception occured in handler')
