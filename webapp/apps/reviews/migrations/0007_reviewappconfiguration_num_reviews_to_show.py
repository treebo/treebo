# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0006_reviewappconfiguration_award_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewappconfiguration',
            name='num_reviews_to_show',
            field=models.PositiveIntegerField(default=5),
        ),
    ]
