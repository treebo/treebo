

from django.conf.urls import url
from apps.third_party.api.v1.third_party_hotel_availability import ThirdPartyHotelAvailability
from apps.pricing.api.v1.views.RatePlanUpdateAPI import UpdateRateAmount
from apps.pricing.api.v1.views.get_price import Pricing
from django.views.decorators.csrf import csrf_exempt
from apps.pricing.api.v1.views import channel_manager
from apps.content.api.views import GetContent

app_name = 'deprecated_urls'
urlpatterns = [url(r'^rest/v1/trivago/hotelavailability/',
                   ThirdPartyHotelAvailability.as_view(),
                   name='deprecated_trivago-hotelavailability'),
               url(r'^rest/v1/pricing/availability$',
                   Pricing.as_view(),
                   name='pAvailability'),
               url(r'^rest/v1/pricing/rates/',
                   csrf_exempt(UpdateRateAmount.as_view()),
                   name='rate_push_api'),
               url(r'^rest/v1/contentService/',
                   GetContent.as_view(),
                   name='content'),
               url(r"^setPromotions",
                   channel_manager.SetPromotions.as_view(),
                   name='setPromotions'),
               url(r"^updateRateAmount",
                   channel_manager.UpdateRateAmount.as_view(),
                   name='updateRateAmount'),
               url(r"^updateHotelAvailability",
                   channel_manager.UpdateHotelAvailability.as_view(),
                   name='updateRateAmount'),
               url(r"^getHotelRatePlan",
                   channel_manager.GetHotelRatePlan.as_view(),
                   name='getHotelRatePlan'),
               url(r"^getPromotions",
                   channel_manager.GetPromotions.as_view(),
                   name='getPromotions'),
               url(r"^deletePromotions",
                   channel_manager.DeletePromotions.as_view(),
                   name='deletePromotions'),
               ]
