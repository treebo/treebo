import csv
import datetime
import itertools
import os
from datetime import timedelta

import xlrd
from django.core.management.base import BaseCommand

from apps.bookings.models import Booking
from apps.fot.models import Fot, FotReservationsRequest
from apps.common import date_time_utils
from dbcommon.models.profile import User
from data_services.exceptions import HotelDoesNotExist
from dbcommon.models.hotel import Hotel

# TODO: sanitize the email data
''' ONLY REQUIREMENT FOR FOT TABLE IS TO HAVE USER AS FOREIGN KEY.. REST ALL ARE POPULATED BY DEFAULT '''


class Command(BaseCommand):
    args = 'Argument is needed as table name'
    help = 'Django custom management command for exporting data as tables. '

    def add_arguments(self, parser):
        parser.add_argument('my_argument', type=str)

    def handle(self, *args, **options):
        if options['my_argument'] == 'fot':
            self.stdout.write("Loading table")
            self.load_fot_table()
            self.stdout.write(
                "Loading complete please verify completion report in csv/fot_importer_summary.csv")
        elif options['my_argument'] == 'audits':
            self.stdout.write("Loading table")
            self.load_audits_table()
            self.stdout.write(
                "Loading complete please verify completion report in csv/audits_importer_summary.csv")
        else:
            self.stdout.write("Please enter atleast one valid argument")
            self.stdout.write(
                "Arguments is needed. Available are audits and fot")
            self.stdout.write(
                "For example: " +
                '\033[1m' +
                "python manage.py csvuploader audits" +
                '\033[0m')

    def load_fot_table(self):

        file_name = "xls/FOT Dashboard.xlsx"
        xl_workbook = xlrd.open_workbook(file_name)
        xl_sheet = xl_workbook.sheet_by_name('FOTs')
        updates, creates, fails, nofot, nousers = [], [], [], [], []
        headers = ['updates', 'creates', 'fails', 'nofot', 'nousers']
        statusList = ['PASSED', 'FAILED', 'BLOCKED', 'IN_PROGRESS']

        for row_idx in range(0, xl_sheet.nrows):

            # skipping the header
            if row_idx == 0:
                continue

            email = xl_sheet.cell(row_idx, 2).value

            try:
                user = User.objects.get(email=email)
                created_at = xl_sheet.cell(row_idx, 0).value
                created_at = datetime.datetime(
                    *xlrd.xldate_as_tuple(created_at, xl_workbook.datemode))
                city = str(
                    xl_sheet.cell(
                        row_idx,
                        4).value).decode('string_escape')
                maritalStatus = str(
                    xl_sheet.cell(
                        row_idx,
                        5).value).decode('string_escape')
                status = str(
                    xl_sheet.cell(
                        row_idx,
                        6).value).decode('string_escape')
                try:
                    totalAuditsScheduled = int(xl_sheet.cell(row_idx, 7).value)
                except BaseException:
                    totalAuditsScheduled = 0
                try:
                    noshows = int(xl_sheet.cell(row_idx, 9).value)
                except BaseException:
                    noshows = 0
                try:
                    feedbackSubmitted = int(xl_sheet.cell(row_idx, 10).value)
                except BaseException:
                    feedbackSubmitted = 0
                try:
                    qualifiedFeedbacks = int(xl_sheet.cell(row_idx, 11).value)
                except BaseException:
                    qualifiedFeedbacks = 0
                try:
                    totalFreeNights = int(xl_sheet.cell(row_idx, 15).value)
                except BaseException:
                    totalFreeNights = 0
                try:
                    freeNightsRemaining = int(xl_sheet.cell(row_idx, 17).value)
                except BaseException:
                    freeNightsRemaining = 0

                if status not in statusList:
                    status = ""

                try:
                    fot, created = Fot.objects.get_or_create(user_id=user.id)
                    if not created_at == "":
                        fot.created_at = created_at
                    if not city == "":
                        fot.city = city
                    if not maritalStatus == "":
                        fot.marital_status = maritalStatus
                    if not status == "":
                        fot.status = status

                    fot.total_audits_scheduled = totalAuditsScheduled
                    fot.no_shows = noshows
                    fot.feedbacks_submitted = feedbackSubmitted
                    fot.qualified_feedbacks = qualifiedFeedbacks
                    fot.total_free_nights = totalFreeNights
                    fot.free_nights_remaining = freeNightsRemaining

                    try:
                        fot.save()
                        if created:
                            creates.append(email)
                        else:
                            updates.append(email)
                    except BaseException:
                        fails.append(email)
                except BaseException:
                    nofot.append(email)
            except BaseException:
                nousers.append(email)

        totals = [
            len(updates),
            len(creates),
            len(fails),
            len(nofot),
            len(nousers)]
        try:
            os.makedirs('csv')
        except OSError:
            if not os.path.isdir('csv'):
                raise
        outfile_path = 'csv/fot_importer_summary.csv'
        with open(outfile_path, 'wb') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow(headers)
            for i in itertools.zip_longest(
                    updates, creates, fails, nofot, nousers, fillvalue=''):
                writer.writerow(i)
            writer.writerow(totals)

    def load_audits_table(self):

        file_name = "xls/FOT Dashboard.xlsx"
        xl_workbook = xlrd.open_workbook(file_name)
        xl_sheet = xl_workbook.sheet_by_name('Audits')
        creates, fails, noaudit, nohotels, nousers = [], [], [], [], []
        headers = ['creates', 'fails', 'noaudit', 'nohotels', 'nousers']

        for row_idx in range(0, xl_sheet.nrows):

            # skipping the header
            if row_idx == 0:
                continue

            email = xl_sheet.cell(row_idx, 1).value
            hotel_name = str(
                xl_sheet.cell(
                    row_idx,
                    3).value).decode('string_escape')

            try:
                user = User.objects.get(email=email)
                #hotel = hotel_repository.get_single_hotel(name=hotel_name)
                hotel = Hotel.objects.get(name=hotel_name)
                try:
                    auditDuration = int(xl_sheet.cell(row_idx, 13).value)
                except BaseException:
                    auditDuration = 1
                try:
                    auditDate = xl_sheet.cell(row_idx, 2).value
                    auditDate = datetime.datetime(
                        *xlrd.xldate_as_tuple(auditDate, xl_workbook.datemode))
                    checkoutDate = auditDate + timedelta(days=auditDuration)
                    auditDate = auditDate.strftime(date_time_utils.DATE_FORMAT)
                    checkoutDate = checkoutDate.strftime(
                        date_time_utils.DATE_FORMAT)
                except BaseException:
                    auditDate = ''
                    checkoutDate = ''
                try:
                    noOfGuests = int(xl_sheet.cell(row_idx, 4).value)
                except BaseException:
                    noOfGuests = ''
                status = (str(xl_sheet.cell(row_idx, 6).value).decode(
                    'string_escape')).lower()
                bookingId = str(
                    xl_sheet.cell(
                        row_idx,
                        7).value).decode('string_escape')
                try:
                    bookingId = int(bookingId)
                    bookingObj = Booking.objects.get(id=bookingId)
                    if bookingObj is None:
                        bookingId = 1
                except BaseException:
                    bookingId = 1  # TODO: Default for booking id

                if status == "done":
                    status = 'FEEDBACK_SUBMITTED'
                elif status == "":
                    status = 'SCHEDULED'
                else:
                    status = 'CANCELLED'
                try:
                    audit = FotReservationsRequest.objects.create(
                        user_id=user.id, hotel_id=hotel.id, booking_id=bookingId)
                    if not auditDate == "":
                        audit.check_in_date = auditDate
                    if not checkoutDate == "":
                        audit.check_out_date = checkoutDate
                    if not noOfGuests == "":
                        audit.no_of_guests = noOfGuests
                    if not status == "":
                        audit.status = status
                    try:
                        audit.save()
                        creates.append(email)
                    except Exception as e:
                        fails.append(email)
                except BaseException:
                    noaudit.append(email)
            except HotelDoesNotExist:
                nohotels.append(hotel_name)
            except User.DoesNotExist:
                nousers.append(email)

        totals = [
            len(creates),
            len(fails),
            len(noaudit),
            len(nohotels),
            len(nousers)]
        try:
            os.makedirs('csv')
        except OSError:
            if not os.path.isdir('csv'):
                raise
        outfile_path = 'csv/audit_import_summary.csv'
        with open(outfile_path, 'wb') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow(headers)
            for i in itertools.zip_longest(
                    creates,
                    fails,
                    noaudit,
                    nohotels,
                    nousers,
                    fillvalue=''):
                writer.writerow(i)
            writer.writerow(totals)
