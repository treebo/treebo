import json
import logging

from django.http import HttpResponse
from rest_framework.response import Response

from apps.reviews.cache.cache_service import CacheService
from apps.reviews.cache.page_config import PageConfig
from apps.reviews.services.review_response_data.non_jcr_review_data import NonJCRReviewData
from apps.reviews.services.trip_advisor.create_jcr_url import GenerateJCRURL
from apps.reviews.services.trip_advisor.generate_write_review_url_service import GenerateWriteReviewUrlService
from apps.reviews.services.trip_advisor.hotel_ta_mapping_service import HotelTAMappingService
from apps.reviews.services.trip_advisor.review_config_service import ReviewAppConfigService
from apps.third_party.service.trip_advisor.fetch_reviews_service import TAReviewAPIService
from base.views.api import TreeboAPIView
from data_services.web_repositories.hotel_web_repository import HotelWebRepository

logger = logging.getLogger(__name__)


class TripAdvisorReviewData(TreeboAPIView):
    hotel_ta_mapping_service = HotelTAMappingService()
    ta_review_api_service = TAReviewAPIService()

    def get(self, request, *args, **kwargs):

        cs_hotel_id = request.GET.get('cs_hotel_id', '')
        app_config = ReviewAppConfigService()
        hotel_id = None
        jcr_and_non_jcr_reviews = {}

        try:
            hotel = HotelWebRepository().get_hotel_by_cs_id(cs_hotel_id)
            hotel_id = hotel.id
        except Exception as e:
            logger.exception("hotel id does not exist %s", hotel_id, e)

        try:
            if app_config.check_toggle_settings(str(hotel_id)):

                # fetch top reviews for non jcr
                hotel_ta_mapping_obj = self.hotel_ta_mapping_service.fetch_mapping(hotel_id=hotel_id)
                hotel_ta_location_id = hotel_ta_mapping_obj.ta_location_id
                response, url, status, serialized_reviews = self.ta_review_api_service.call_ta_location_reviews_api(
                    hotel_ta_location_id)
                non_jcr_reviews_data = serialized_reviews.data['data']
                non_jcr_review_service = NonJCRReviewData(non_jcr_reviews_data, hotel_id, hotel_ta_location_id)
                top_reviews = non_jcr_review_service.build()

                # fetch reviews for ta
                fetch_hotel_reviews = CacheService()
                review_response = fetch_hotel_reviews.get_or_update_cache(
                    hotel_id, PageConfig.HD_PAGE)
                gen_write_review = GenerateWriteReviewUrlService()
                review_response = gen_write_review.generate_review_url(
                    request, hotel_id, review_response)
                ta_reviews = review_response['ta_reviews']

                if not review_response:  # return only top reviews for non jcr
                    jcr_and_non_jcr_reviews['top_reviews'] = top_reviews


                else:  # combine non jcr and ta top reviews
                    ta_reviews['top_reviews'].extend(top_reviews)
                    jcr_and_non_jcr_reviews = ta_reviews


        except Exception as e:
            logger.exception(
                "Fetching non jcr  Reviews Failed for request.user %s",
                request.user, e)

        return Response(jcr_and_non_jcr_reviews)


class TAReviewData(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        review_response = {"ta_reviews": {"ta_enabled": False}}
        try:
            hotel_id = request.GET.get('hotel_id', '')
            logger.info("Hotel id  %s", hotel_id)
            app_config = ReviewAppConfigService()
            if app_config.check_toggle_settings(str(hotel_id)):
                fetch_hotel_reviews = CacheService()
                review_response = fetch_hotel_reviews.get_or_update_cache(
                    hotel_id, PageConfig.HD_PAGE)
                gen_write_review = GenerateWriteReviewUrlService()
                review_response = gen_write_review.generate_review_url(
                    request, hotel_id, review_response)
                if not review_response:
                    review_response = {"ta_reviews": {}}
            return Response(review_response)
        except Exception:
            logger.exception("Fetching Reviews Failed")
            return Response(review_response)


class JCRUrlGeneration(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        try:
            hotel_id = request.GET.get('hotel_id', None)
            email = request.GET.get('email', None)
            name = request.GET.get('name', None)
            generate_jcr_url = GenerateJCRURL()
            user_details = dict()
            user_details['hotel_id'] = hotel_id

            # TODO: Review
            user_details['email'] = None if (
                email and 'None' in email) else email
            user_details['name'] = None if (name and 'None' in name) else name

            logger.info(
                "user_details dict for generating  review iframe url %s",
                user_details)

            url = generate_jcr_url.create_url(user_details)
            if not url:
                logger.error(
                    "Error in generation url for hotel %s request.user %s",
                    hotel_id,
                    request.user)
                return HttpResponse(
                    json.dumps({"status": "Failure", "message": "Error in generating Review Collection URL"}),
                    content_type="application/json",
                    status=400)
            else:
                logger.info("")
                return HttpResponse(json.dumps(
                    {"url": url, "status": "Success", "message": "URL Successfully generated"}),
                    content_type="application/json",
                    status=200)

        except Exception:
            logger.exception(
                "Fetching Reviews Failed for request.user %s",
                request.user)
            return HttpResponse(
                json.dumps({"status": "Failure", "message": "Exception  in generating Review Collection URL"}),
                content_type="application/json",
                status=400)


class TANonJCRReviewData(TreeboAPIView):
    ta_review_api_service = TAReviewAPIService()
    hotel_ta_mapping_service = HotelTAMappingService()

    def get(self, request, *args, **kwargs):
        review_response = {"ta_non_jcr_reviews": {"ta_enabled": False}}
        try:
            hotel_id = request.GET.get('hotel_id', '')
            logger.info('Hotel id is %s', hotel_id)
            app_config = ReviewAppConfigService()
            if app_config.check_toggle_settings(str(hotel_id)):
                hotel_ta_mapping_obj = self.hotel_ta_mapping_service.fetch_mapping(hotel_id=hotel_id)
                hotel_ta_location_id = hotel_ta_mapping_obj.ta_location_id
                response, url, status, serialized_reviews = self.ta_review_api_service.call_ta_location_reviews_api(
                    hotel_ta_location_id)
                non_jcr_reviews_data = serialized_reviews.data['data']
                non_jcr_review_service = NonJCRReviewData(non_jcr_reviews_data, hotel_id, hotel_ta_location_id)
                top_reviews = non_jcr_review_service.build()
                review_response["ta_non_jcr_reviews"]["top_reviews"] = top_reviews
                review_response["ta_non_jcr_reviews"]["ta_enabled"] = True
            return Response(review_response)
        except Exception as e:
            logger.error("Error in fetching non jcr reviews due to error %s", str(e))
            return Response(review_response)


class UpdateReviews(TreeboAPIView):

    def get(self, request, *args, **kwargs):
        """
        call update_reviews_cache command
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        from django.core.management import call_command
        try:
            call_command('update_reviews_cache')
            response = {'message': 'Update reviews done successfully'}
        except Exception as e:
            logger.error(str(e))
            response = {'message': 'Error in updating reviews'}
        return Response(response)
