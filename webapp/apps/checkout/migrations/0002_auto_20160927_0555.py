# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='user',
            field=models.ForeignKey(
                related_name='userrequests',
                blank=True,
                to=settings.AUTH_USER_MODEL,
                null=True,
                on_delete=models.DO_NOTHING),
        ),
    ]
