import logging

from rest_framework.response import Response
from django.core.cache import cache
from apps.common.utils import Utils
from apps.content import models
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class GetContent(TreeboAPIView):
    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'apicache_content_{0}'

    @staticmethod
    def invalidate_cache(key):
        if key:
            cache.delete(GetContent.CACHE_KEY.format(key))
        else:
            cache.delete_pattern(GetContent.CACHE_KEY.format('*'))

    def get_cache_key(self, key):
        return GetContent.CACHE_KEY.format(key)

    @log_args(logger)
    def get(self, request, format=None):
        key_name, version = self.__parse_get_request(request)
        cache_key = self.get_cache_key(key_name)
        result = None

        try:
            result = cache.get(cache_key)
        except Exception as e:
            logger.exception(e)

        if result:
            return Response(result)
        try:
            contentObject = models.ContentStore.objects.get(
                name=key_name, version=version)
        except Exception as e:
            logger.warn(
                'Exception in getcontent API. Content not found with key: %s, version: %s',
                key_name,
                version)
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.INVALID_DATA))

        context = {"status": "success", "value": contentObject.value}
        cache.set(cache_key, context,
                  GetContent.CACHE_TIMEOUT)
        return Response(context)

    @log_args(logger)
    def post(self, request, format=None):
        keyName, version, value = self.__parse_post_request(request)
        try:
            contentObject = models.ContentStore.objects.get(
                name=keyName, version=version)
        except Exception as e:
            logger.warn(
                'Exception occurred while fetching ContentStore object for key: %s, and version: %s',
                keyName,
                version)
            contentObject = models.ContentStore()

        try:
            contentObject.name = keyName
            contentObject.value = value
            contentObject.version = version
            contentObject.save()
        except TreeboException as e:
            logger.exception('Unable to save new ContentObject')
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.INVALID_DATA))

        context = {
            "status": "success",
            "value": contentObject.value,
            "key": keyName,
            "version": version}
        return Response(context)

    def __parse_get_request(self, request):
        d = request.GET
        keyName = d.get('key', None)
        versionNumber = d.get('version', 1)
        return keyName, versionNumber

    def __parse_post_request(self, request):
        d = request.data
        keyName = d.get('key', None)
        versionNumber = d.get('version', 1)
        value = d.get('value', '')
        return keyName, versionNumber, value
