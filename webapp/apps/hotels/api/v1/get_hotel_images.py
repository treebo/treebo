import json
import logging

from django.http import JsonResponse

from apps.hotels import utils as hotel_utils
from apps.hotels.serializers.image_serializer import ImageSerializer
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.miscellanous import Image
from data_services.image_repository import ImageRepository

logger = logging.getLogger(__name__)

image_data_service = RepositoriesFactory.get_image_repository()


class HotelImages(TreeboAPIView):
    def get(self, hashedHotelId="", format=None):

        hotelId = hotel_utils.decode_hash(hashedHotelId)
        imageObjectsList = image_data_service.get_images_for_hotels(hotels=[
                                                                    hotelId])
        #imageObjectsList = self.__getImageQuerySet(hotelId)
        allImage, message = HotelImages.getAllImage(imageObjectsList)
        response = self.__getReposnse(allImage, message)

        return JsonResponse(response)

    @staticmethod
    def getAllImage(imageObjectsList):

        result = []
        message = "Not a valid Image Object or No Images Present in the Hotel"
        try:
            serialData = ImageSerializer(imageObjectsList, many=True)
            serialData = json.dumps(serialData.data)
            serialData = json.loads(serialData)
            for s in serialData:
                result.append(s)
            message = "success"
        except Exception as exc:
            logger.exception(exc)
            logger.debug(message)

        return result, message

    def __getReposnse(self, allImage, message):
        response = {'message': message, 'status': 0}

        if message == 'success':
            response['allImage'] = allImage
            status = 1
            response['status'] = status

        return response

    # def __getImageQuerySet(self, hotelId):
    #     return Image.objects.filter(hotel_id=hotelId)
