import logging

from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR
from apps.discounts.services.promotion_service import PromotionService
from apps.checkout.models import BookingRequest
from apps.common import error_codes
from apps.common.error_codes import get as error_message
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.discounts.exceptions import AvailablePromotionsException
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class AvailablePromotionsSerializer(serializers.Serializer):
    bid = serializers.IntegerField()


class AvailablePromotionAPI(TreeboAPIView):
    validationSerializer = AvailablePromotionsSerializer
    promotion_service = PromotionService()

    def get(self, request, *args, **kwargs):
        try:
            validated_data = self.serializerObject.validated_data
            booking_request = BookingRequest.objects.filter(
                pk=validated_data['bid']).first()
            if not booking_request:
                return Response({"error": error_message(
                    error_codes.INVALID_BOOKING)}, status=400)

            applicable_promotions = self.promotion_service.get_promotions(
                booking_request=booking_request)

            return Response(applicable_promotions)

        except AvailablePromotionsException as e:
            logger.exception(e)
            return Response({"error": error_message(error_codes.ERROR_AVAIL_PROMOTIONS)},
                            status=HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            logger.exception(e)
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        dev_msg="AvailablePromotionAPI",
                                                        class_name=self.__class__.__name__,
                                                        message=e.__str__())
            return Response({"error": error_message(error_codes.ERROR_AVAIL_PROMOTIONS)},
                            status=HTTP_500_INTERNAL_SERVER_ERROR)
