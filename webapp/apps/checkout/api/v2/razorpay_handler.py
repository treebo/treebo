import logging

from django.conf import settings
from rest_framework.response import Response
from django.http import JsonResponse

from rest_framework.authentication import BasicAuthentication
from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from rest_framework.views import APIView

from apps.checkout.api.v1.PaymentHandler import PaymentHandler
from apps.checkout.models import BookingRequest
from apps.checkout.tasks import capture_razorpay_payment
from base import log_args
from base.middlewares.set_request_id import get_current_user, get_current_request_id

logger = logging.getLogger(__name__)


class RazorPay(APIView, PaymentHandler):
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)

    @log_args(logger)
    def post(self, request):
        try:
            status, order_id, razorpay_payment_id = self.__parse_request(
                request)

            logger.info('RazorPay Handler called with data: %s', request.data)
            booking_type = request.data.get('type', 'RazorPay')
            booking_request = BookingRequest.objects.get(order_id=order_id)
            self._update_payments(
                float(
                    booking_request.total_amount),
                order_id,
                razorpay_payment_id,
                "RazorPay")
            self.__store_razorpay_details(
                int(booking_request.total_amount * 100), razorpay_payment_id)
            payment_gateway_response = {
                'response': 'success'
            }
            return Response(payment_gateway_response, status=200)
        except Exception as e:
            logger.exception("Unable to update payments")
            return JsonResponse(
                {"error": "Unable to create Booking object"}, status=400)

    def __store_razorpay_details(self, stored_amount, razorpay_payment_id):
        razorpay_id = settings.RAZORPAY_KEYS
        razorpay_secret = settings.RAZORPAY_SECRET
        capture_razorpay_payment.delay(stored_amount,
                                       razorpay_secret,
                                       razorpay_id,
                                       razorpay_payment_id,
                                       user_id=get_current_user(),
                                       request_id=get_current_request_id())

    def __parse_request(self, request):
        status = request.data['status']
        order_id = request.data['order_id']
        gatewayPaymentId = request.data['razorpay_payment_id']
        return status, order_id, gatewayPaymentId
