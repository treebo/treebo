import functools
import logging
import traceback
import re

from django.http import HttpRequest
from rest_framework.request import Request

LOG = logging.getLogger(__name__)


# Should not be used on @staticmethod or @classmethod. This will fail.
def log_args(logger=None, only_on_exception=False):
    def decorator(func):
        try:
            argnames = func.__code__.co_varnames[:func.__code__.co_argcount]
        except BaseException:
            argnames = None
        try:
            fname = func.__name__
        except BaseException:
            fname = None

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            argument_list = []
            if argnames:
                for entry in zip(argnames, args):
                    name = entry[0]
                    value = entry[1]
                    if name == 'request':
                        if isinstance(value, HttpRequest):
                            if value.method == 'GET':
                                name = 'TemplateView request.GET'
                                value = value.GET
                            elif value.method == 'POST':
                                name = 'TemplateView request.POST'
                                value = value.POST
                        elif isinstance(value, Request):
                            if value.method == 'GET':
                                name = 'APIView request.GET'
                                value = value.GET
                            else:
                                name = 'APIView request.data'
                                value = value.data
                    argument_list.append('{0}={1}'.format(name, value))
            else:
                for arg in args:
                    argument_list.append('{0}'.format(arg))
            log = logger if logger else LOG
            for entry in list(kwargs.items()):
                try:
                    argument_list.append('{0}={1}'.format(entry[0], entry[1]))
                except Exception as e:
                    log.exception("Error in adding arg list")

            argument_values = ', '.join(argument_list)

            try:
                return func(*args, **kwargs)
            except BaseException:
                if only_on_exception:
                    log.error(traceback.format_exc())
                    log.exception(
                        "Function:[%s] called with args: %s",
                        fname,
                        argument_values)
                raise
        return wrapper

    return decorator
