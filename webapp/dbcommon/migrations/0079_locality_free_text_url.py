# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0078_remove_locality_free_text_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='locality',
            name='free_text_url',
            field=models.CharField(default=b'', max_length=500, null=True, blank=True),
        ),
    ]
