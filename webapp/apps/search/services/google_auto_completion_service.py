from django.conf import settings
from django.core.cache import cache
from django.template.defaultfilters import slugify

from googleplaces import GooglePlaces, types

from apps.search.services.auto_completion_service import AutoCompletionService
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
import datetime
from pytz import timezone


class GoogleAutoCompletionService(AutoCompletionService):
    CACHE_TIMEOUT = 24 * 60 * 60
    google_places = GooglePlaces(settings.GOOGLE_API_SERVER_KEY)
    timezone_india = timezone('Asia/Kolkata')
    SESSION_TOKEN_EPOCH_KEY = 'autocomplete_session_token_epoch'
    SESSION_TIMEOUT = 60 * 3

    def _remove_duplicates_from_google_api_results(
            self, predictions, current_results):
        """
        Every item in current_results is a dictionary of the form: {'label': 'Locality, City, State'}
        Now, to remove duplicates, we just match the locality_name from existing result set with google api results.
        Hence, we just get the `'label'` key, and split the result on comma (,), and get first element, which will be `Locality`.

        Then we filter the predictions list, based on current_labels retrieved. We ignore any prediction, whose first part of string is same as locality,
        """
        current_labels = [result['label'].split(
            ",")[0] for result in current_results]
        filtered_predictions = [prediction for prediction in predictions if all(label.lower(
        ) != prediction.description.split(",")[0].lower() for label in current_labels)]
        return filtered_predictions

    def _get_session_token_epoch(self):
        """
        This function will return epoch time as session token
        :return:
        """
        epoch_time_now = datetime.datetime.now(self.timezone_india).timestamp()
        epoch_cache_time = cache.get(self.SESSION_TOKEN_EPOCH_KEY)

        if epoch_cache_time and epoch_time_now - epoch_cache_time < self.SESSION_TIMEOUT:
            session_token = epoch_cache_time
        else:
            session_token = epoch_time_now

        cache.set(self.SESSION_TOKEN_EPOCH_KEY,
                  session_token, GoogleAutoCompletionService.CACHE_TIMEOUT)
        return int(session_token)

    def apply(self, search_string, **kwargs):
        """
        Uses Python Google Places library
        Reference: https://github.com/slimkrazy/python-google-places
        """
        try:
            result = cache.get(
                'cs_autocomplete_cache_googleapi_' +
                slugify(search_string))
        except Exception as e:
            result = None
        if result:
            return result
        else:
            return []
        # data = {
        #     'input': search_string,
        #     'sessiontoken': kwargs['session_token'] if kwargs['session_token'] else self._get_session_token_epoch(),
        #     'lat_lng': {
        #         'lat': None,
        #         'lng': None
        #     },
        #     'location': '',
        #     'radius': 3200,
        #     'language': '',
        #     'types': '(regions)',
        #     'components': [('country', 'IN')]
        # }
        # google_autocomplete_search_result = self.google_places.autocomplete(
        #     **data)
        # predictions = google_autocomplete_search_result.predictions
        # filtered_predictions = self._remove_duplicates_from_google_api_results(
        #     predictions, kwargs['current_results'])
        # result = AutoCompleteResultBuilder.build_google_api_result(
        #     filtered_predictions)
        # cache.set('cs_autocomplete_cache_googleapi_' + slugify(search_string),
        #           result, GoogleAutoCompletionService.CACHE_TIMEOUT)
        # return result
