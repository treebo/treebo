# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.admin.options import flatten_fieldsets
from django.core.urlresolvers import reverse
# Register your models here.
from apps.bookings.models import Booking, Reservation, RoomBooking, AsyncJobStatus, PaymentOrder
from dbcommon.admin import ReadOnlyAdminMixin


@admin.register(PaymentOrder)
class PaymentOrderAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'ps_order_id',
        'order_id',
        'pg_order_id',
        'amount',
        'status',
        'gateway_type')
    search_fields = (
        'id',
        'ps_order_id',
        'order_id',
        'pg_order_id',
        'amount',
        'status')
    list_filter = ('gateway_type', 'status')


@admin.register(Booking)
class BookingAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'order_id',
        'created_at',
        'booking_code',
        'hotel',
        'guest_email',
        'booking_status',
        'payment_mode',
        'channel',
        'get_payment_order',
        'comments')
    search_fields = (
        'order_id',
        'booking_code',
        'group_code',
        'guest_email',
        'guest_mobile',
        'hotel__name',
        'coupon_code')
    list_filter = (
        'channel',
        'is_complete',
        'hotel',
        'payment_mode',
        'booking_status')
    list_per_page = 50

    def get_payment_order(self, obj):
        payment_order = obj.paymentorder
        if payment_order:
            link = reverse(
                'admin:bookings_paymentorder_change', args=(payment_order.id,))
            return '<a href="{0}" target="_blank">{1}</a>'.format(
                link, 'Payment Order')
        return ''

    get_payment_order.allow_tags = True
    get_payment_order.short_description = 'Payment Order'


@admin.register(Reservation)
class ReservationAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'reservation_id',
        'reservation_status',
        'hotel',
        'room',
        'guest_email',
        'guest_mobile')
    search_fields = ('guest_email', 'guest_mobile')


@admin.register(RoomBooking)
class RoomBookingAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'room', 'get_booking_id', 'reservation_id')
    search_fields = ('booking__order_id', 'reservation_id',)

    def get_booking_id(self, obj):
        return obj.booking.order_id

    get_booking_id.short_description = 'Booking Order ID'
    get_booking_id.admin_order_field = 'booking__order_id'


@admin.register(AsyncJobStatus)
class AsyncJobStatus(admin.ModelAdmin):
    """
        AsyncJobStatus
    """
    list_display = (
        'job_id',
        'booking_id',
        'tag',
        'data',
        'code',
        'status',
        'message',
        'created_at',
        'modified_at')
    search_fields = (
        'job_id',
        'booking_id',
        'tag',
        'data',
        'code',
        'status',
        'message',
        'created_at',
        'modified_at')
    list_filter = ('tag', 'status')
