
def get_rabbit_mq_heartbeat_interval():
    from django.conf import settings
    return settings.RABBIT_MQ_HEART_BEAT_INTERVAL


def get_rabbit_mq_retry_policy():
    from django.conf import settings
    return settings.RABBIT_MQ_RETRY_POLICY


def get_rabbit_mq_transport_options():
    from django.conf import settings
    return settings.RABBIT_MQ_TRANSPORT_OPTIONS


def get_rabbit_mq_prefetch_count():
    from django.conf import settings
    return settings.RABBIT_MQ_PREFETCH_COUNT
