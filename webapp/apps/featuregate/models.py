from caching.base import CachingManager, CachingMixin
from django.db import models
from djutil.models import TimeStampedModel

from dbcommon.models.default_permissions import DefaultPermissions


class Page(CachingMixin, TimeStampedModel, DefaultPermissions):
    page_name = models.CharField(default='', null=False, max_length=200)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        pass

    def __unicode__(self):
        return self.page_name

    def __str__(self):
        return self.page_name


class PageVariables(CachingMixin, TimeStampedModel, DefaultPermissions):
    page = models.ForeignKey(
        Page,
        related_name='variables',
        on_delete=models.DO_NOTHING)
    name = models.CharField(default='', null=False, max_length=200)
    type = models.CharField(default='String', null=False, max_length=200)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Page Variable'
        verbose_name_plural = 'Page Variables'

    def __unicode__(self):
        return '%s-%s' % (self.page.page_name, self.name)

    def __str__(self):
        return '%s-%s' % (self.page.page_name, self.name)


class ConstraintKeywords(CachingMixin, TimeStampedModel, DefaultPermissions):
    keyword = models.CharField(max_length=200, default="", null=True)
    type = models.CharField(max_length=20, default="", null=True)
    description = models.CharField(max_length=1000, default="", null=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Constraint Keyword'
        verbose_name_plural = 'Constraint Keywords'

    def __unicode__(self):
        return '%s-%s' % (self.keyword, self.type)

    def __str__(self):
        return '%s-%s' % (self.keyword, self.type)


class ConstraintOperators(CachingMixin, TimeStampedModel, DefaultPermissions):
    operator = models.CharField(max_length=10, default="", null=True)
    description = models.CharField(max_length=100, default="", null=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Constraint Operator'
        verbose_name_plural = 'Constraint Operators'

    def __unicode__(self):
        return '%s' % (self.operator,)

    def __str__(self):
        return '%s' % (self.operator,)


class ConstraintExpression(CachingMixin, TimeStampedModel, DefaultPermissions):
    expression = models.TextField(default="", null=True)
    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Constraint Expression'
        verbose_name_plural = 'Constraint Expressions'

    def __unicode__(self):
        return '%s' % self.expression

    def __str__(self):
        return '%s' % self.expression


class VariableCondition(CachingMixin, TimeStampedModel, DefaultPermissions):
    expression = models.ForeignKey(
        ConstraintExpression,
        on_delete=models.DO_NOTHING)
    page_variable = models.ForeignKey(
        PageVariables,
        related_name='variable_conditions',
        on_delete=models.DO_NOTHING)
    final_value = models.CharField(max_length=2000, default="", null=True)
    active = models.BooleanField(default=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Variable Condition'
        verbose_name_plural = 'Variable Conditions'

    def __unicode__(self):
        return '%s-%s' % (self.expression.expression,
                          self.page_variable.page.page_name)

    def __str__(self):
        return '%s-%s' % (self.expression.expression,
                          self.page_variable.page.page_name)


class Constraints(CachingMixin, TimeStampedModel, DefaultPermissions):
    keyword = models.ForeignKey(
        ConstraintKeywords,
        null=True,
        on_delete=models.DO_NOTHING)
    operation = models.ForeignKey(
        ConstraintOperators,
        null=True,
        on_delete=models.DO_NOTHING)
    value_one = models.TextField(null=True, blank=True)
    value_two = models.TextField(null=True, blank=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Constraints'
        verbose_name_plural = 'Constraints'

    def __unicode__(self):
        return '%s-%s' % (self.keyword.keyword, self.operation.operator)

    def __str__(self):
        return '%s-%s' % (self.keyword.keyword, self.operation.operator)
