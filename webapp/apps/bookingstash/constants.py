RESERVED = 'RESERVE'
CHECKIN = 'CHECKIN'
CHECKOUT = 'CHECKOUT'
CANCELLED = 'CANCEL'

BOOKING_STATUS = (
    (RESERVED, 'Reserved'),
    (CHECKIN, 'Checkin'),
    (CHECKOUT, 'Checkout'),
    (CANCELLED, 'Cancelled')
)

MODIFY = 'Modify'
CANCEL = 'Cancel'
COMMIT = 'Commit'

MESSAGE_STATUS = (
    (MODIFY, 'Modify'),
    (CANCEL, 'Cancel'),
    (COMMIT, 'Commit')
)

BLOCKED = 'DNR'
RELEASED = 'FREE'

RESTRICTION_STATUS = (
    (BLOCKED, 'Reserved'),
    (RELEASED, 'Released')
)
RESERVATIONS_MISSING_ERROR = "No key reservations found"
