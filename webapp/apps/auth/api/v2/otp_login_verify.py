import logging

from apps.auth.constants import UserContactType
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.otp_verify_dto import OtpVerifyDTO
from apps.auth.serializer.user_serializer import UserSerializer
from apps.auth.services.cookie_service import CookieService
from apps.auth.services.otp_service import OTPService
from apps.auth.services.user_profile_service import UserProfileService
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import OTPVerifyException, UPSClientException
from apps.profiles.service.user_service import UserService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class OTPLoginVerifyApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    otp_service = OTPService()
    cookie_service = CookieService()
    user_service = UserService()
    user_profile_service = UserProfileService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            otp_verify_dto = OtpVerifyDTO(data=request.data)
            if not otp_verify_dto.is_valid():
                logger.error(
                    "otp login verify invalid request %s ",
                    request.data)
                return api_response.invalid_request_error_response(
                    otp_verify_dto.errors, OTPLoginVerifyApi.__name__)
            user, token_info, user_details = self.otp_service.verify_otp_and_store_token(
                otp_verify_dto.data)
            self.user_service.login_user(request, user)
            user_dto = UserSerializer(instance=user)
            user_data = user_dto.data
            user_data['token'] = token_info.get('access_token')
            user_data['failed_validation_attempts'] = token_info.get('failed_validation_attempts')
            user_data['max_failed_validation_attempts'] = token_info.get('max_failed_validation_attempts')
            response = api_response.prep_success_response(user_data)
            self.cookie_service.set_user_info_in_cookie(response, user)
            self.user_profile_service.mark_verfied_anon_user(otp_verify_dto.data['phone_number'], user_details)
        except UPSClientException as e:
            logger.error("Creating User failed due to {error}".format(error=e))
            return response
        except OTPVerifyException as e:
            logger.exception(
                "treebo otp login verify exception %s ",
                request.data)
            response = api_response.otp_validation_failed_error_response(e)
        except TreeboException as e:
            logger.exception(
                "treebo otp login verify exception %s ",
                request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception(
                "internal error otp login verify exception %s ",
                request.data)
            response = api_response.internal_error_response(error_codes.get(
                error_codes.OTP_VERIFY_FAILED)['msg'], OTPLoginVerifyApi.__name__)
        return response
