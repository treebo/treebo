import logging

from django.http.response import HttpResponseRedirect

from base.views.template import TreeboTemplateView

__author__ = 'varunachar'

logger = logging.getLogger(__name__)


class Login(TreeboTemplateView):
    template_name = "login/index.html"

    def getData(self, request, args, kwargs):
        referrer = request.GET.get('next', "")
        return HttpResponseRedirect("/?next=" + referrer)

    def getMobileData(self, request, args, kwargs):
        if request.user and request.user.is_active and request.user.is_authenticated():
            return HttpResponseRedirect('/')
        referral_msg = None
        return {
            'scripts': ['mobile/js/login.js'],
            'styles': ['mobile/css/login.css'],
            'footer': False,
            'googleLogin': True,
            "referral_msg": referral_msg}
