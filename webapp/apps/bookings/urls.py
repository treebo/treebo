# -*- coding: utf-8 -*-


from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from apps.bookings.api.v1.booking_history_api import BookingHistoryApi
from apps.bookings.api.v1.booking_request import BookingRequestAPI
from apps.bookings.api.v1.bookingstub import GetLastBookingsByHotel, GetBookingsByUser, GetRoomBookingByRoom, \
    BookingDetails, BookingsByOrderId, GetReservationForHotel, ReservationsForToday
from apps.bookings.api.v1.cancelbooking import CancelBookingAPI
from apps.bookings.api.v1.confirm_booking import ConfirmBooking
from apps.bookings.api.v1.create_booking import CreateBooking
from apps.bookings.api.v1.initiate_booking import InitiateBooking
from apps.bookings.api.v1.update_payment_info import UpdatePaymentInfo

app_name = 'bookings_v1'
urlpatterns = [
    url(r'^cancel/$', CancelBookingAPI.as_view(), name='cancel-booking'),
    url(r'^update-payments/$',
        UpdatePaymentInfo.as_view(),
        name="update-payment"),
    url(r'^initiate/$', InitiateBooking.as_view(), name="init-booking"),
    url(r'^book/$', CreateBooking.as_view(), name="create-booking"),
    url(r'^confirm/$', ConfirmBooking.as_view(), name="confirm-booking"),

    # FIXME Fix this api format
    url(r'^hotel/(?P<hotelId>[0-9]+)/last-booked/delta/(?P<delta>[0-9]+)/$', GetLastBookingsByHotel.as_view(),
        name='get-last-booking-time'),
    url(r'^user/(?P<email>[^/]+)/(?P<userId>[0-9]+)/bookings/$', GetBookingsByUser.as_view(),
        name='get_bookings_by_users'),
    url(r'^roombookings/(?P<bookingId>[0-9|]+)/$', GetRoomBookingByRoom.as_view(),
        name='get-room-bookings'),
    url(r'^(?P<bookingId>[0-9|]+)/$',
        BookingDetails.as_view(),
        name='get-bookings'),
    url(r'^order/(?P<orderId>[\w-]+)/$',
        BookingsByOrderId.as_view(),
        name='booking_by_order_id'),
    url(r'^hotel/$', GetReservationForHotel.as_view(), name='get-reservations'),
    url(r"^today/$", ReservationsForToday.as_view(), name='reservation-for-today'),
    url(r"^booking-request/$",
        BookingRequestAPI.as_view(),
        name='booking-request'),
    url(r"^booking-history/$",
        BookingHistoryApi.as_view(),
        name='booking-history'),
]
