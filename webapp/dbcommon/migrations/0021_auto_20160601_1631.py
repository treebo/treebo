# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0020_auto_20160523_0950'),
    ]

    operations = [
        migrations.CreateModel(
            name='HotelImage',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('dbcommon.hotel',),
        ),
        migrations.AlterModelOptions(
            name='image',
            options={'ordering': ['position'], 'verbose_name_plural': 'Images'},
        ),
        migrations.AddField(
            model_name='image',
            name='position',
            field=models.PositiveSmallIntegerField(default=1, null=True),
        ),
    ]
