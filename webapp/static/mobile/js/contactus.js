'use strict';

import $ from 'jquery';
import _ from 'underscore';
import {Observable} from "rx";
import BaseView from './treebolibs/baseview';
import parsley from 'parsleyjs';
import logger from './treebolibs/logger';
import treejax from './treebolibs/treejax';
import * as toastr from 'toastr';
import '../css/contactus.less';

let $loader = $("#commonLoader");
const ContactusView = Object.create(BaseView);
ContactusView.initMap = () => {
	let myOptions = {
			zoom:10,
			center:new google.maps.LatLng(12.90007,77.631193),
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	let map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
	let marker = new google.maps.Marker({
		map,
		position: new google.maps.LatLng(12.90007,77.631193)
	});
	let infowindow = new google.maps.InfoWindow({
		content:'<strong>Treebo Hotels</strong><br> AMR Tech Park, Bommanahalli, Bangalore<br>'
	});

	google.maps.event.addListener(marker, 'click', () => {
			infowindow.open(map,marker);
		});
	infowindow.open(map,marker);
}

window.jq = $;

let activeForm = '';
const defaultForm = $( "select option:selected" ).attr( "formtoshow" );
activeForm=defaultForm;


ContactusView.pageLoaded = () => {

    $('.contactus__select-tag').change(() => {

    	const title = $( "select option:selected" ).attr( "formtoshow" );
    	$(".form_type > div").addClass('hide');
	   	$(`#${title}`).removeClass("hide");

    	activeForm = title;
    });

    $('.booking__select').change(() => {
    	const head = $(".booking__select option:selected").attr( "policytoshow" );
      	$(".cancel-modi-policy > div").addClass('hide');
	   	$(`#${head}`).removeClass("hide");
    });

	google.maps.event.addDomListener(window, 'load', ContactusView.initMap);
	logger.log(" Contactus page loaded");
}

$('.submit').click(e => {

		e.preventDefault();
		const submitParsley = $(`#${activeForm}`).find('form').parsley();

		submitParsley.validate();

		if (submitParsley.isValid()){
				$loader.removeClass('hide')
                submitForm(e);
        }

});

let submitForm = ()=>{
 		const data = $(`#${activeForm}`).find('form').serializeArray();
		const contactus_json = _.object(_.map(data, _.values));
		contactus_json['form-type'] = activeForm;
		contactus_json['csrfmiddlewaretoken'] = $('input[name="csrfmiddlewaretoken"]').val();

    	const p = treejax({
            url: '/rest/v1/contactus/',
            type: 'POST',
            data: contactus_json
        }).then((data, status, xhr) => {
            toastr.success('Your form is submitted successfully. We will get in touch with you shortly.', 'Thank You!', {timeOut: 5000, closeButton: true});
        },treeboError => {
        	toastr.error('Your form could not be submitted. Please try again later.', 'Sorry!', {timeOut: 5000, closeButton: true});

        }).finally(() => {
        	$loader.addClass('hide');
        });
    return p;
}

let contactusConfig = { name:'', widgets:[]};
let contactusView = ContactusView.setup(contactusConfig);
