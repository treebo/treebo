

from django.conf.urls import url

from apps.bookings.api.v2.booking_history_api import BookingHistoryAPI

app_name = 'bookings_v2'
urlpatterns = [url(r"^booking-history/$",
                   BookingHistoryAPI.as_view(),
                   name='booking-history'),
               ]
