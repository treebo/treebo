# -*- coding: utf-8 -*-


from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0036_auto_20170815_0812'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='rate_plan_meta',
            field=jsonfield.fields.JSONField(null=True, blank=True),
        ),
    ]
