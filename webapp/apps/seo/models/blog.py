from django.db import models
from dbcommon.models.hotel import City


class Blog(models.Model):
    name = models.TextField()
    title = models.TextField()
    image_url = models.TextField()
    blog_url = models.TextField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    priority = models.IntegerField()
    is_enabled = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Blog'
        verbose_name_plural = 'Blogs'

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)