from apps.reviews.models import HotelTAOverallRatings
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.components.base_component import BaseComponent
from apps.seo.serializers.serializer import TopRatedSerializer

__author__ = 'ansilkareem'


class TopRated(BaseComponent):
    def __init__(self, current_city, footer_data, exclude_list):
        super(TopRated, self).__init__(current_city, footer_data, exclude_list)

    def build(self):
        ratings = HotelTAOverallRatings.objects.filter(
            hotel_ta_mapping__hotel__city=self.current_city.id,
            hotel_ta_mapping__hotel__city__enable_for_seo=True,
            hotel_ta_mapping__hotel__status=1).exclude(
            hotel_ta_mapping__hotel__id__in=self.exclude_list). order_by('-overall_rating').distinct()
        hotels = []
        for item in ratings:
            if item.hotel_ta_mapping.hotel not in hotels:
                hotels.append(item.hotel_ta_mapping.hotel)
            if len(hotels) == self.count_cap:
                break
        data = {
            'key': 'top_rated',
            'title': 'Top rated Treebos',
            'content': AutoCompleteResultBuilder.build_hotel_result(hotels)
        }
        serialized_obj = TopRatedSerializer(data=data)
        if serialized_obj.is_valid():
            return serialized_obj.validated_data
        else:
            return {
                'key': 'top_rated',
                'title': 'Top rated Treebos',
                'content': []
            }
