from django.conf import settings

from apps.reward.service.reward import RewardService
import logging
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import UserUnauthroized
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes

logger = logging.getLogger(__name__)


class WalletStatement(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    reward_service = RewardService()

    @log_args(logger)
    def post(self, request, format=None):
        raise Exception('Method not supported')

    def get(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            if not (hasattr(request, 'user')
                    and request.user and request.user.is_authenticated()):
                response = api_response.treebo_exception_error_response(
                    UserUnauthroized())
            else:
                user = request.user
                wallet_type = settings.WALLET_TYPE
                wallet_statement = self.reward_service.get_wallet_statement(
                    user, wallet_type)
                balance_data = {
                    "user_id": user.id,
                    "auth_id": user.auth_id,
                    "wallet_statement": wallet_statement
                }
                response = api_response.prep_success_response(balance_data)
        except TreeboException as e:
            logger.exception(
                "treebo exception %s for user %s " %
                (self.get_view_name(), request.user))
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception(
                "internal error statement  %s for user %s" %
                (self.get_view_name(), request.user))
            response = api_response.internal_error_response(error_codes.get(
                error_codes.UNABLE_TO_FETCH_WALLET_BALANCE)['msg'], self.get_view_name())
        return response
