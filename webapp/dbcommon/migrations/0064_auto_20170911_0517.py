# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0063_enablecategory'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hotel',
            options={
                'ordering': ['name'],
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Hotel',
                'verbose_name_plural': 'Hotels'},
        ),
    ]
