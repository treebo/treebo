import logging

from easyjoblite.response import EasyResponse
from ths_common.constants.scheduled_job_constants import JOB_SUCCESS_CODE, JOB_FAILURE_CODE

from common.job.job_repository import JobRepository

logger = logging.getLogger(__name__)

job_repository = JobRepository()


def execute_job(job_data):
    logger.info("Executing job with id: %s", job_data['data']['job_id'])
    job_entity = job_repository.get_job(job_data['data']['job_id'])
    failure_message = None
    try:
        from common.job.jobs.register_job import job_registry
        handler_function = job_registry.get_executor(job_data['data']['job_name'])
        handler_function(**job_data['data']['job_args'])
        success = True
        logger.info("Job execution status for job id: %s -> %s", job_entity.job_id, success)
    except Exception as e:
        logger.exception('Error while executing job: %s' % job_data)
        success = False
        failure_message = str(e)
    job_entity = job_repository.update_status(job_entity, success, failure_message)
    job_repository.update(job_entity)
    return EasyResponse(JOB_SUCCESS_CODE) if success else EasyResponse(JOB_FAILURE_CODE)
