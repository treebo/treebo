import geopy
import geopy.distance

from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.components.base_component import BaseComponent
from apps.seo.serializers.serializer import LandmarkSerializer
from dbcommon.models.landmark import SearchLandmark

__author__ = 'ansilkareem'


class NearbyLandmarks(BaseComponent):

    def __init__(self, current_city, footer_data, origin_latitude, origin_longitude, max_results=None):
        self.origin_latitude = origin_latitude
        self.origin_longitude = origin_longitude
        self.nearby_landmarks = dict()
        self.sorted_nearby_landmarks = []
        self.max_results = max_results
        super(NearbyLandmarks, self).__init__(current_city, footer_data, [])

    def build(self):
        result = dict(key='near_by_landmarks',
                      title='Nearby landmarks')
        if self.current_city and self.current_city.enable_for_seo:
            all_landmarks = SearchLandmark.objects.filter(city__name__iexact=self.current_city.name,
                                                          enable_for_seo=True, city__enable_for_seo=True,
                                                          is_hotel_present=True,
                                                          status=1)
            current_location = geopy.Point(self.origin_latitude, self.origin_longitude)
            for landmark in all_landmarks:
                landmark_point = geopy.Point(landmark.latitude, landmark.longitude)
                distance = geopy.distance.distance(current_location, landmark_point).km
                if distance <= self.distance_cap:
                    self.nearby_landmarks[str(landmark.seo_url_name)] = (
                        distance, landmark)
            self.sorted_nearby_landmarks = [seq[1][1] for seq in sorted(
                list(self.nearby_landmarks.items()), key=lambda x: x[1][0])]
            result['content'] = AutoCompleteResultBuilder.build_landmark_result(self.sorted_nearby_landmarks[:self.max_results]) \
                if self.sorted_nearby_landmarks else []
            serialized_obj = LandmarkSerializer(data=result)
            if serialized_obj.is_valid():
                return serialized_obj.validated_data
        return result
