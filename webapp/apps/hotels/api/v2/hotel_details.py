import logging

from collections import defaultdict
from django.core.cache import cache
from rest_framework.response import Response
from apps.common.error_codes import get as error_message
from apps.common.error_codes import INVALID_HOTEL_ID
from base.views.api import TreeboAPIView
from dbcommon.models.hotel import Hotel
from services.restclient.contentservicerestclient import ContentServiceClient
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from django.db.models import Prefetch
from dbcommon.models.room import Room
from dbcommon.models.direction import Directions

logger = logging.getLogger(__name__)


class HotelDetails(TreeboAPIView):
    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'apicache_hotel_details_{0}'

    def get_cache_key(self, hotel_ids):
        return HotelDetails.CACHE_KEY.format(hotel_ids)

    @staticmethod
    def invalidate_cache(hotel_id):
        if hotel_id:
            cache.delete(HotelDetails.CACHE_KEY.format(hotel_id))
        else:
            cache.delete_pattern(HotelDetails.CACHE_KEY.format('*'))

    def get(self, request, *args, **kwargs):
        """
        Gives basic hotel and its room details for a list of hotels
        """
        hotel_ids = request.query_params.get('hotels').split(',')
        try:
            result = cache.get(self.get_cache_key(hotel_ids))
        except Exception as e:
            result = None
        if result:
            return Response(result)

        # WARNING: Explicitly using base_manager here to return data for
        # inactive hotels also.
        hotels = Hotel._base_manager.select_related(
            'city',
            'locality',
            'state').prefetch_related(
            'image_set',
            'facility_set',
            'seodetails_set',
            'hotel_highlights',
            Prefetch(
                'rooms',
                queryset=Room.objects.order_by("-base_rate").prefetch_related(
                    "image_set",
                    "facility_set")),
            Prefetch(
                'directions_set',
                queryset=Directions.objects.select_related("landmark_obj")),
        ).filter(
            id__in=hotel_ids)

        hotels_information = []
        for hotel in hotels:
            directions_css_dist_list = self.get_hotel_directions(
                hotel.directions_set.all())
            hotel_facilities_list = [{"name": facility.name}
                                     for facility in hotel.facility_set.all()]
            rooms = self.get_categorised_guest_supported_info(
                hotel.rooms.all())
            hotel_essential_information = {
                "id": hotel.id,
                "name": hotel.name,
                "phone": hotel.phone_number,
                "is_active": True if hotel.status == 1 else False,
                "address": {
                    "city": hotel.city.name,
                    "street": hotel.street,
                    "locality": hotel.locality.name,
                    "landmark": hotel.landmark,
                    "state": hotel.state.name,
                    "pincode": hotel.locality.pincode},
                "coordinates": {
                    "lat": hotel.latitude,
                    "lng": hotel.longitude},
                "description": hotel.description,
                "rooms": rooms,
                "accessibilities": directions_css_dist_list,
                "facilities": hotel_facilities_list,
                "trilights": [
                    highlight.description for highlight in hotel.hotel_highlights.all()],
                "image_url": get_image_uncode_url(hotel.image_set.first().url.url)
                 if hotel.image_set.first() else None}
            hotels_information.append(hotel_essential_information)
        cache.set(self.get_cache_key(hotel_ids), hotels_information,
                  HotelDetails.CACHE_TIMEOUT)
        return Response(hotels_information)

    def get_categorised_guest_supported_info(self, rooms_list):
        supported_guest_info = []
        for room in rooms_list:
            supported_guest_info.append({
                "type": room.room_type_code.lower(),
                "max_adults": room.max_guest_allowed,
                "max_guests": room.max_adult_with_children,
                "max_children": room.max_children,
                "size": room.room_area,
                "occupancy": room.get_occupancy_string()
            })
        return supported_guest_info

    def get_hotel_directions(self, hotel_direction_objs):
        direction_css_dict_list = []
        for direction_obj in hotel_direction_objs:
            direction_css_dict = {}
            landmark_name = direction_obj.landmark_obj.name
            direction_css_dict['name'] = landmark_name
            direction_css_dict['distance'] = direction_obj.distance
            direction_css_dict['mode'] = direction_obj.mode
            direction_css_dict['landmark_type'] = direction_obj.landmark_obj.type
            direction_css_dict['coordinates'] = {
                "lat": direction_obj.landmark_obj.latitude if direction_obj.landmark_obj and direction_obj.landmark_obj.latitude else None,
                "lng": direction_obj.landmark_obj.longitude if direction_obj.landmark_obj and direction_obj.landmark_obj.latitude else None}
            direction_css_dict['landmark_type'] = direction_obj.landmark_obj.type
            direction_css_dict['steps'] = direction_obj.directions_for_email_website
            if landmark_name:
                if landmark_name.lower().find('airport') != -1:
                    direction_css_dict['css_class'] = 'airport'
                elif landmark_name.lower().find('railway') != -1:
                    direction_css_dict['css_class'] = 'railway'
                elif landmark_name.lower().find('bus') != -1:
                    direction_css_dict['css_class'] = 'bustand'
                else:
                    direction_css_dict['css_class'] = ''
            else:
                direction_css_dict['css_class'] = ''
            direction_css_dict_list.append(direction_css_dict)
        return direction_css_dict_list
