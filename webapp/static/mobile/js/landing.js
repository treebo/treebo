import $ from 'jquery';
import BaseView from './treebolibs/baseview';
import searchWidget from './widgets/search';
import '../css/landing.less';


let LandingView = Object.create(BaseView);

LandingView.pageLoaded = () => {
};

let analyticsConfigfunc = function(){
    return  {
        hrental_pagetype: 'home',
        hrental_startdate: rt.getCheckin(),
        hrental_enddate: rt.getCheckout(),
    }
}


let landingConfig = { name: 'Home Page', widgets: [searchWidget], analyticsConfigfunc };
let landingView = LandingView.setup(landingConfig);
