BEGIN;
insert into discounts_constraintoperators (created_at, modified_at, operator, description) values (NOW(), NOW(), 'not_in', 'not in');
insert into discounts_constraintoperators (created_at, modified_at, operator, description) values (NOW(), NOW(), 'ne', 'not equal');
insert into discounts_constraintoperators (created_at, modified_at, operator, description) values (NOW(), NOW(), 'exclude_range', 'exclude the range');
COMMIT;
