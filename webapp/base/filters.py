# coding=utf-8
"""
    Filters
"""
import logging

from base.middlewares.set_request_id import get_current_user, get_current_request_id, get_job_data
from apps.common import thread_local_utils


class LogLevelFilter(logging.Filter):
    """
        LogLevelFilter
    """

    def __init__(self, level):
        """
        :param level:
        """
        self.level = level

    def filter(self, record):
        """
        :param record:
        :return:
        """
        return record.levelno == self.level


class RequestIDFilter(logging.Filter):
    """
        RequestIDFilter
    """

    def filter(self, record):
        """
        :param record:
        :return:
        """
        current_user = get_current_user()
        current_request_id = get_current_request_id()
        record.user_id = current_user
        record.request_id = current_request_id
        return True


class JobIDFilter(logging.Filter):
    """
            JobIDFilter
    """

    def filter(self, record):
        """
        :param record:
        :return:
        """
        data = get_job_data()
        record.request_id = data['request_id']
        record.job_id = data['job_id']
        return True


class CeleryRequestIDFilter(logging.Filter):
    """
        CeleryRequestIDFilter
    """

    def __init__(self, user_id, request_id, task_id, task_name):
        """
        :param user_id:
        :param request_id:
        :param task_id:
        :param task_name:
        """
        self.user_id = user_id
        self.request_id = request_id
        self.task_id = task_id
        self.task_name = task_name
        thread_local_utils.set_thread_variable('user_id', self.user_id)
        thread_local_utils.set_thread_variable('request_id', self.request_id)

    def filter(self, record):
        """
        :param record:
        :return:
        """
        record.user_id = self.user_id
        record.request_id = self.request_id
        record.task_id = self.task_id
        record.task_name = self.task_name
        return True
