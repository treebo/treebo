import logging

from django.contrib import admin

from apps.seo.models import FeedbackPositives, PositivesCategory, PositiveCategoryContentMapping, FeatureToggle, \
    HotelContentCountMapping

logger = logging.getLogger(__name__)


@admin.register(FeedbackPositives)
class FeedbackPositivesAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'created_at', 'modified_at', 'name')
    search_fields = ('name',)
    list_filter = ('name',)
    actions = ['delete_selected']


@admin.register(PositivesCategory)
class PositivesCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'created_at', 'modified_at', 'name')
    search_fields = ('name',)
    list_filter = ('name',)
    actions = ['delete_selected']


@admin.register(PositiveCategoryContentMapping)
class PositiveCategoryContentMappingAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'get_positive_name',
        'get_category_name',
        'content')
    search_fields = ('content', 'positive__name', 'category__name')
    list_filter = ('positive__name', 'category__name')
    actions = ['delete_selected']

    def get_positive_name(self, obj):
        if obj and obj.positive:
            return str(obj.positive.name)
        else:
            return None

    def get_category_name(self, obj):
        if obj and obj.category:
            return str(obj.category.name)
        else:
            return None

    get_positive_name.short_description = 'Positive'
    get_category_name.short_description = 'Category'


@admin.register(FeatureToggle)
class FeatureToggleAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'num_of_days',
        'num_of_positives_shown')
    search_fields = ('num_of_days', 'num_of_positives_shown')
    list_filter = ('num_of_days', 'num_of_positives_shown')
    actions = ['delete_selected']


@admin.register(HotelContentCountMapping)
class HotelContentCountMappingAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'modified_at',
        'get_hotel_name',
        'get_hotel_id',
        'get_hotelogix_id',
        'get_positive_name',
        'get_category_name',
        'get_content',
        'count')
    search_fields = (
        'feedback_content__positive__name',
        'feedback_content__category__name',
        'feedback_content__content',
        'count',
        'hotel__name',
        'hotel__id',
        'hotel__hotelogix_id')
    list_filter = (
        'feedback_content__positive__name',
        'feedback_content__category__name',
        'feedback_content__content',
        'hotel__name')

    def has_add_permission(self, request):
        return False

    def get_positive_name(self, obj):
        if obj and obj.feedback_content.positive:
            return str(obj.feedback_content.positive.name)
        else:
            return None

    def get_category_name(self, obj):
        if obj and obj.feedback_content.category:
            return str(obj.feedback_content.category.name)
        else:
            return None

    def get_content(self, obj):
        if obj and obj.feedback_content:
            return str(obj.feedback_content.content)
        else:
            return None

    def get_hotel_name(self, obj):
        if obj and obj.hotel:
            return str(obj.hotel.name)
        else:
            return None

    def get_hotel_id(self, obj):
        if obj and obj.hotel:
            return str(obj.hotel.id)
        else:
            return None

    def get_hotelogix_id(self, obj):
        if obj and obj.hotel:
            return str(obj.hotel.hotelogix_id)
        else:
            return None

    get_positive_name.short_description = 'Positive'
    get_category_name.short_description = 'Category'
    get_content.short_description = 'Content'
    get_hotel_name.short_description = 'Hotel Name'
    get_hotel_id.short_description = 'Hotel ID'
    get_hotelogix_id.short_description = 'Hotelogix ID'
