from ab_client.api import API
from ab_client.utils import set_thread_variable
from ab_client.flask_extn.flask_request_interceptor import FlaskRequestInterceptor
from common.services.feature_toggle_api import FeatureToggleAPI
import logging

logger = logging.getLogger(__name__)


class ABClientServices():
    api = API(None)

    def get_user_buckets_from_ab(self, request):
        buckets_for_user = None
        ab_id = FlaskRequestInterceptor().set_ab_id_for_request(request)
        set_thread_variable('ab_id', ab_id)
        if not FeatureToggleAPI.is_enabled("ab_service", "ab_turnoff", True):
            buckets_for_user = self.api.get_user_buckets_for_all_experiments()
        return buckets_for_user
