import logging
from django.conf import settings
from django.utils.text import slugify
from apps.posts.service import PostService
from apps.seo.components.breadcrumb import BreadCrumb
from apps.seo.components.categories import Category
from apps.seo.components.cities_nearby import CitiesNearby
from apps.seo.components.left_nav import LeftNav
from apps.seo.components.popular_cities import PopularCities
from apps.seo.components.top_reviews import TopReviews
from apps.seo.components.popular_amenities import PopularAmenities

from apps.seo.pages.pages import PAGES
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import CitySeoDetails
from dbcommon.models.location import City
from services.restclient.contentservicerestclient import ContentServiceClient
from django.core.cache import cache
from apps.seo.components.weather_widget import WeatherWidget
from apps.seo.components.city_coordinate_component import CityCoordinateComponent
from apps.seo.components.blogs_component import BlogComponent
from apps.seo import constants

__author__ = 'ansilkareem'
logger = logging.getLogger(__name__)


class CityPage(PAGES):

    def __init__(self, page, query, is_amp):
        self.is_amp = is_amp
        self.city = City.objects.get_by_slug(query)
        self.key = 'search_' + query.lower() + '_desc'
        self.description = ContentServiceClient.getValueForKey(
            None, self.key, 1)
        if not self.description:
            self.key = 'about_' + query.lower()
            self.description = ContentServiceClient.getValueForKey(
                None, self.key, 1)
        super(CityPage, self).__init__(page, query, query)

    def update_meta(self, ):
        min_price_key = str(self.city.name) + '_min_price'
        min_sell_price_key = str(self.city.name) + '_min_sell_price'
        min_rack_rate_key = str(self.city.name) + '_min_rack_rate'
        max_discount_key = str(self.city.name) + '_max_discount'
        min_price = cache.get(min_price_key)
        min_sell_price = cache.get(min_sell_price_key)
        min_rack_rate = cache.get(min_rack_rate_key)
        max_discount = cache.get(max_discount_key)
        if max_discount:
            total_discount = str(max_discount) + "%"
        else:
            total_discount = str(settings.DEFAULT_DISCOUNT) + "%"
        temp_dict = {
            "city_name": self.city.slug.title() if self.city.slug else self.city.name.title(),
            "price": str(min_price) if min_price else str(settings.DEFAULT_PRICE),
            "sell_price": str(min_sell_price) if min_sell_price else str(
                settings.DEFAULT_SELL_PRICE),
            "rack_rate": str(min_rack_rate) if min_rack_rate else str(settings.DEFAULT_RACK_RATE),
            "total_discount": total_discount
        }
        city_seo_details = CitySeoDetails.objects.filter(city=self.city).first()
        full_canonical_url = settings.SITE_HOST_NAME + 'hotels-in-' + slugify(self.city.slug) + '/'
        amp_url = full_canonical_url + 'amp/'
        if city_seo_details:
            title = city_seo_details.citySeoTitleContent % temp_dict
            description = city_seo_details.citySeoDescriptionContent % temp_dict
            self.override_seo_content(title,
                                      description,
                                      city_seo_details.citySeoKeywordContent,
                                      full_canonical_url, self.city.name, is_amp=self.is_amp,
                                      amp_url=amp_url)
        else:
            title = None
            description = None
            if 'city_title' in self.footer_data:
                title = self.footer_data['city_title'] % temp_dict
            if 'city_description' in self.footer_data:
                description = self.footer_data['city_description'] % temp_dict
            if title and description:
                self.override_seo_content(title,
                                          description,
                                          None,
                                          full_canonical_url, is_amp=self.is_amp, amp_url=amp_url)

    def build_meta(self,*args):
        for arg in args:
            ta_review_count = arg.get('ta_review_count')
        self.update_meta()
        temp_dict = {'city_name': self.city.slug.upper(
        ) if self.city.slug else self.city.name.upper()}
        if 'city_about_title' in self.footer_data:
            about_title = self.footer_data['city_about_title'] % temp_dict
        else:
            about_title = ''
        self.meta_content['content'] = {
            'description': {'title': about_title.title(),
                            'content': self.description['text']
                            } if self.description else {}
        }
        min_price_key = str(self.city.name) + '_min_sell_price'
        min_price_value = cache.get(min_price_key)

        self.meta_content['content'].update(
            {'min_price': min_price_value if min_price_value else settings.DEFAULT_SELL_PRICE})
        top_reviews = TopReviews(self.city, self.footer_data,ta_review_count, [])
        self.meta_content['content'].update(top_reviews.
                                            build_reviews_using_area_name(page=PAGES.CITY))
        left_nav = LeftNav(self.city, self.footer_data, [])
        self.meta_content['content'].update(left_nav.build())
        self.meta_content['content'].update({
            'city_image': self.city.seo_image if self.city.seo_image else self.city_image_url
        })
        cities_nearby = CitiesNearby(self.city, self.footer_data)
        self.meta_content['content'].update({
            'links': [cities_nearby.build()],
            'breadcrumbs': BreadCrumb(self.city, self.footer_data, self.page,
                                      self.item_name).build()
        })

        area = {}
        area['page'] = self.page
        area['identifier'] = self.city
        weather_widget = WeatherWidget(area, self.footer_data)
        self.meta_content['content'].update(weather_widget.build())
        blogs = BlogComponent(self.city, self.footer_data)
        self.meta_content['content'].update(blogs.build())
        city_coordinate = CityCoordinateComponent(area, self.footer_data)
        self.meta_content['content'].update(city_coordinate.build())
        popular_cities = PopularCities(
            self.city, self.footer_data, [
                city.id for city in cities_nearby.sorted_nearby_cities])
        self.meta_content['content']['links'].append(popular_cities.build())
        category = Category(self.city, self.footer_data, [])
        self.meta_content['content']['links'].append(category.build())
        popular_amenities = PopularAmenities(self.city, self.footer_data, [])
        self.meta_content['content']['links'].append(popular_amenities.build())

    @staticmethod
    def delete_page_meta_cache(city_name):
        logger.info(
            'deleting city meta cache : ' + 'meta_city_' + slugify(city_name) + '_*')
        cache.delete_pattern('meta_city_' + slugify(city_name) + '_*')
