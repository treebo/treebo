from base.views.template import TreeboTemplateView
from common.constants import common_constants
from dbcommon.models.profile import UserMetaData

__author__ = 'varunachar'


class Settings(TreeboTemplateView):
    template_name = 'settings/index.html'

    def getMobileData(self, request, args, kwargs):
        user = request.user
        emailPromotions = 'checked'
        try:
            notificationEmailObj = UserMetaData.objects.get(
                user_id=user, key=common_constants.DO_NOT_SEND_NOTIFICATION_EMAIL)
            if notificationEmailObj.value == "1":
                emailPromotions = ''
        except UserMetaData.DoesNotExist:
            pass

        phonePromotions = 'checked'
        try:
            notificationPhoneObj = UserMetaData.objects.get(
                user_id=user, key=common_constants.DO_NOT_SEND_NOTIFICATION_PHONE)
            if notificationPhoneObj.value == "1":
                phonePromotions = ''
        except UserMetaData.DoesNotExist:
            pass

        emailLaunch = True
        emailCity = ""
        try:
            cityEmailObj = UserMetaData.objects.get(
                user_id=user, key=common_constants.DO_NOT_SEND_CITY_EMAIL)
            if cityEmailObj.value == "1":
                emailLaunch = False
            else:
                emailCity = '' if cityEmailObj.value == '0' else cityEmailObj.value

        except UserMetaData.DoesNotExist:
            pass

        phoneLaunch = True
        phoneCity = ""
        try:
            cityPhoneObj = UserMetaData.objects.get(
                user_id=user, key=common_constants.DO_NOT_SEND_CITY_PHONE)
            if cityPhoneObj.value == "1":
                phoneLaunch = False
            else:
                phoneCity = '' if cityPhoneObj.value == '0' else cityPhoneObj.value

        except UserMetaData.DoesNotExist:
            pass
        context = {
            'email': {
                'promotions': emailPromotions,
                'launch': emailLaunch,
                'cities': emailCity
            },
            'phone': {
                'promotions': phonePromotions,
                'launch': phoneLaunch,
                'cities': phoneCity
            },
            'scripts': ['js/settings.js'],
            'styles': ['css/settings.css'],
            'googleMaps': True
        }
        return context
