import csv
import datetime
import logging

from django.conf import settings
from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand
from django.core.urlresolvers import reverse

from apps.bookingstash.utils import StashUtils
from apps.hotels import utils
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.pricing_api_impl import PriceUtilsV2
from apps.pricing.utils import PriceUtils
from common.constants import common_constants as const
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Get hotels feed summary"

    def __get_cheapest_room_type(
            self,
            hotels,
            checkInDate,
            checkOutDate,
            roomConfig):
        logger.info(
            'Getting cheapest room type Using getCheapestRoomTypeFromAvailabilitySync')
        cheapest_room_type_with_availability = StashUtils.get_cheapest_room_type_from_availability_sync(
            hotels, checkInDate, checkOutDate, roomConfig)
        return cheapest_room_type_with_availability

    def __get_price_for_hotels(
            self,
            checkin_date,
            checkout_date,
            hotels,
            room_config):
        cheapest_room_type_with_availability = self.__get_cheapest_room_type(
            hotels, checkin_date, checkout_date, room_config)
        hotel_wise_prices = PriceUtils.getPriceForMultipleHotelsWithRoomConfigDateRange(
            checkin_date, checkout_date, hotels, room_config, cheapest_room_type_with_availability)
        cheapest_room_type_with_availability = self.__get_cheapest_room_type(
            hotels, checkin_date, checkout_date, room_config)
        if FeatureToggleAPI.is_enabled("pricing", "v2_impl_enabled", False):
            hotel_wise_prices = PriceUtilsV2.getPriceForMultipleHotelsWithRoomConfigDateRange(
                checkin_date, checkout_date, hotels, room_config, cheapest_room_type_with_availability)
        else:
            hotel_wise_prices = PriceUtils.getPriceForMultipleHotelsWithRoomConfigDateRange(
                checkin_date, checkout_date, hotels, room_config, cheapest_room_type_with_availability)
        return hotel_wise_prices

    def handle(self, *args, **options):
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotels = hotel_repository.get_active_hotel_list(
            related_entities_list=['rooms', 'images'])
        host = settings.TREEBO_WEB_URL
        image_url_host = get_image_uncode_url()
        with open('hotels_feed.csv', 'wb') as csvfile:
            writer = csv.writer(csvfile, delimiter='\t')
            headers = [
                'Property ID',
                'Property Name',
                'Final URL',
                'Image URL',
                'Destination Name',
                'Price']
            writer.writerow(headers)
            checkin_date = datetime.datetime.now()
            checkout_date = checkin_date + datetime.timedelta(days=1)
            hotel_wise_prices = self.__get_price_for_hotels(
                checkin_date, checkout_date, hotels, [(1, 0)])
            for hotel in hotels:
                prices = hotel_wise_prices[hotel.id]
                kwargs = utils.get_hotel_url_kwargs(hotel)
                hotel_url = host + \
                    reverse('pages:hotel-details', kwargs=kwargs)
                hashed_hotel_id = kwargs.get('hashedHotelId')
                image_urls = [image for image in hotel_repository.get_image_set_for_hotel(
                    hotel) if image.is_showcased]
                if image_urls:
                    image_url = image_urls[0]
                else:
                    image_url = None
                if image_url:
                    image_url = image_url.url
                    image_url = image_url_host + image_url
                sell_rate = int(prices['sell'])
                row = [
                    hashed_hotel_id,
                    hotel.name,
                    hotel_url,
                    image_url,
                    hotel.city.name,
                    '{0} {1}'.format(
                        sell_rate,
                        'INR')]
                writer.writerow(row)

        mail = EmailMessage(
            'Hotels Feed for Date: {0}'.format(checkin_date),
            'Hotel Feed',
            settings.SERVER_EMAIL,
            settings.HOTELS_FEED_MAIL_LIST)
        mail.attach_file('hotels_feed.csv')
        mail.send()
