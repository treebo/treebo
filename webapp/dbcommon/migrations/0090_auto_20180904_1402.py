# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-09-04 14:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0089_auto_20180904_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='externalproviderroomdetails',
            name='name',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AlterField(
            model_name='externalproviderroomdetails',
            name='rate_plan_code',
            field=models.CharField(default='', max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='externalproviderroomdetails',
            name='rate_plan_name',
            field=models.CharField(default='', max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='externalproviderroomdetails',
            name='room_code',
            field=models.CharField(default='', max_length=50),
        ),
    ]
