# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0072_auto_20171214_1646'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cspropertydetails',
            name='cs_id',
            field=models.CharField(unique=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='hotel',
            name='cs_id',
            field=models.IntegerField(max_length=200, null=True),
        ),
    ]
