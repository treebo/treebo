# -*- coding: utf-8 -*-


from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0010_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='rate_plan_meta',
            field=jsonfield.fields.JSONField(null=True, blank=True),
        ),
    ]
