import datetime
import logging
from decimal import Decimal

from apps.discounts import constants
from apps.discounts.models import DiscountCouponConstraints, DiscountCoupon
from apps.checkout.models import BookingRequest
from base import log_args

logger = logging.getLogger(__name__)


class ConstraintValidator:
    @log_args(logger)
    def __init__(self, **kwargs):
        """
        Unpacking kwargs

        :param checkin:
        :param checkout:
        :param hotel_id:
        :param user:
        :param no_bookings:
        :param rooms:
        :param booking_date:
        :param paytype:
        :param days:
        :param weeks:
        :param months:
        :param corporate:
        :param totalcost:
        :return:
        """
        self.message = ''
        self.__checkin = kwargs['checkin']
        self.__checkout = kwargs['checkout']
        self.__hotel_id = kwargs['hotel_id']
        self.__user = kwargs['user'] if kwargs.get('user', None) else None
        self.__rooms = kwargs['rooms'] if kwargs.get('rooms', None) else None
        self.__booking_date = kwargs['booking_date'] if kwargs.get(
            'booking_date', None) else None
        self.__totalcost = kwargs['totalcost'] if kwargs.get(
            'totalcost', None) else None
        self.__no_bookings = kwargs['no_bookings'] if kwargs.get(
            'no_bookings', None) else 0
        self.__paytype = kwargs['paytype'] if kwargs.get(
            'paytype', None) else ""
        self.__days = kwargs['days'] if kwargs.get('days', None) else ''
        self.__weeks = kwargs['weeks'] if kwargs.get('weeks', None) else ''
        self.__months = kwargs['months'] if kwargs.get('months', None) else ''
        self.__corporate = kwargs['corporate'] if kwargs.get(
            'corporate', None) else ''
        self.__roomConfig = kwargs['room_config']
        self.__city_id = kwargs['city'].id if kwargs['city'] else None
        self.__coupon = kwargs.get('coupon')
        self.__booking_channel = kwargs.get('booking_channel')
        self.__current_user = kwargs.get('current_user')
        self.__utm_source = kwargs['utm_source'] if kwargs.get(
            'utm_source', None) else ''

        self.__funcDict = {
            'hotel': self.__hotelValidator,
            'room': self.__roomTypeValidator,
            'user': self.__userValidator,
            'checkin': self.__checkinDateValidator,
            'checkout': self.__checkoutDateValidator,
            'corporate': self.__corporateValidator,
            'days': self.__daysOfWeekValidator,
            'loggedin': self.__loggedInValidator,
            'ntime': self.__ntimeValidator,
            'month': self.__monthOfYearValidator,
            'week': self.__weekOfMonthValidator,
            'cartnight': self.__cartNightValidator,
            'cartcost': self.__cartCostValidator,
            'kidcount': self.__kidCountValidator,
            'adultcount': self.__adultCountValidator,
            'city': self.__cityValidator,
            'bookingchannel': self.__bookingChannelValidator,
            'abw': self.__abwValidator,
            'utm_source': self.__utmSourceValidator
        }

    def validatorFactory(self, parsedTerm):
        if isinstance(parsedTerm, list):
            flag = False
            operand = parsedTerm[0]
            del parsedTerm[0]
            if operand == "OR":
                for term in parsedTerm:
                    result = self.validatorDelegate(term)
                    if operand == "OR":
                        flag = flag or result
                        if flag:
                            self.message = constants.SUCCESS
        else:
            result = self.validatorDelegate(parsedTerm)
            if result:
                self.message = constants.SUCCESS

    def validatorDelegate(self, constraintId):
        constraint = DiscountCouponConstraints.objects.get(pk=constraintId)
        validatorFunction = self.__funcDict[constraint.keyword.keyword]
        return validatorFunction(
            constraint.operation.operator,
            constraint.value_one,
            constraint.value_two)

    def __bookingChannelValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                self.__booking_channel,
                vOne,
                vTwo,
                str):
            source_map = {BookingRequest.MSITE: constants.MOBILE_SITE,
                          BookingRequest.WEBSITE: constants.DESKTOP,
                          BookingRequest.APP: constants.APP,
                          BookingRequest.ANDROID: constants.ANDROID,
                          BookingRequest.IOS: constants.IOS,
                          BookingRequest.GDC: constants.GDC
                          }
            self.message = constants.BOOKING_CHANNEL_CONSTRAINT % source_map[vOne]
            return False
        return True

    def __hotelValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation, self.__hotel_id, vOne, vTwo, int):
            self.message = constants.HOTEL_CONSTRAINT
            return False
        else:
            return True

    def __cityValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation, self.__city_id, vOne, vTwo, str):
            self.message = constants.CITY_CONSTRAINT
            return False
        else:
            return True

    def __roomTypeValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                self.__rooms.lower(),
                vOne.lower(),
                vTwo,
                str):
            self.message = constants.ROOM_CONSTRAINT
            return False
        else:
            return True

    def __userValidator(self, operation, vOne, vTwo):
        coupon = self.__coupon
        if not self.__user:
            if str(coupon.coupon_type) == DiscountCoupon.VOUCHER and str(
                    coupon.discount_operation) == DiscountCoupon.NIGHT:
                return True
            self.message = constants.NO_BOOKING_USER
            return False
        elif self.__operatorSelection(operation, self.__user.id, vOne, vTwo, int):
            self.message = constants.USER_CONSTRAINT
            return False
        else:
            return True

    def __parser(self, date_string):
        return date_string

    def __checkinDateValidator(self, operation, vOne, vTwo):
        delta = datetime.timedelta(days=1)
        checkin_date = datetime.datetime.strptime(
            self.__checkin, "%Y-%m-%d").date()
        checkout_date = datetime.datetime.strptime(
            self.__checkout, "%Y-%m-%d").date()
        while checkin_date < checkout_date:
            start = datetime.datetime.strptime(vOne, "%Y-%m-%d").date()
            end = datetime.datetime.strptime(vTwo, "%Y-%m-%d").date()
            if self.__operatorSelection(
                    operation,
                    checkin_date,
                    start,
                    end,
                    self.__parser):
                self.message = constants.DATE_CONSTRAINT
                return False
            checkin_date += delta
        return True

    def __checkoutDateValidator(self, operation, vOne, vTwo):
        pass

    def __corporateValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                self.__corporate,
                vOne,
                vTwo,
                str):
            self.message = constants.CORPORATE_CONSTRAINT
            return False
        else:
            return True

    def __daysOfWeekValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(operation, self.__days, vOne, vTwo, int):
            self.message = constants.DAY_CONSTRAINT
            return False
        else:
            return True

    def __monthOfYearValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(operation, self.__months, vOne, vTwo, int):
            self.message = constants.MONTH_CONSTRAINT
            return False
        else:
            return True

    def __weekOfMonthValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(operation, self.__weeks, vOne, vTwo, int):
            self.message = constants.WEEK_CONSTRAINT
            return False
        else:
            return True

    def __loggedInValidator(self, operation, vOne, vTwo):
        user_logged_in = str(bool(self.__current_user and
                                  not self.__current_user.is_anonymous())).lower()
        if self.__operatorSelection(
                operation,
                user_logged_in,
                vOne.lower(),
                vTwo,
                str):
            self.message = constants.USER_CONSTRAINT
            return False
        return True

    def __ntimeValidator(self, operation, vOne, vTwo):
        if self.__no_bookings == 0:
            self.message = constants.USER_CONSTRAINT
            return False
        elif self.__operatorSelection(operation, self.__no_bookings, vOne, vTwo, int):
            self.message = constants.USER_CONSTRAINT
            return False
        else:
            return True

    def __cartNightValidator(self, operation, vOne, vTwo):
        checkin = datetime.datetime.strptime(self.__checkin, "%Y-%m-%d").date()
        checkout = datetime.datetime.strptime(
            self.__checkout, "%Y-%m-%d").date()
        diffDays = (checkout - checkin).days

        if self.__operatorSelection(operation, diffDays, int(vOne), vTwo, int):
            self.message = constants.DURATION_ERROR
            return False
        else:
            return True

    def __cartCostValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                float(
                    self.__totalcost),
                float(vOne),
                vTwo,
                Decimal):
            self.message = constants.COUPON_MIN_CART % str(int(vOne))
            return False
        else:
            return True

    def __kidCountValidator(self, operation, vOne, vTwo):
        child = 0
        for room in self.__roomConfig:
            child += room[1]
        if self.__operatorSelection(operation, child, int(vOne), vTwo, int):
            self.message = constants.KID_CONSTRAINT % vOne
            return False
        else:
            return True

    def __adultCountValidator(self, operation, vOne, vTwo):
        adult = 0
        for room in self.__roomConfig:
            adult += room[0]
        if self.__operatorSelection(operation, adult, int(vOne), vTwo, int):
            self.message = constants.ADULT_CONSTRAINT % vOne
            return False
        else:
            return True

    def __abwValidator(self, operation, vOne, vTwo):
        booking_date = datetime.datetime.today().date()
        checkin_date = datetime.datetime.strptime(
            self.__checkin, "%Y-%m-%d").date()
        checkout_date = datetime.datetime.strptime(
            self.__checkout, "%Y-%m-%d").date()
        advance_booking_days_start = (checkin_date - booking_date).days
        advance_booking_days_end = (checkout_date - booking_date).days - 1
        """
        Booking must satisfy abw constraint for all stay days.
        Hence, checking the constraint for stay_start and stay_end
        checkout_date is not included in the stay duration
        """
        if self.__operatorSelection(
                operation,
                advance_booking_days_start,
                int(vOne),
                vTwo,
                int):
            self.message = constants.ABW_CONSTRAINT % (vOne, vTwo)
            return False

        if self.__operatorSelection(
                operation,
                advance_booking_days_end,
                int(vOne),
                vTwo,
                int):
            self.message = constants.ABW_CONSTRAINT % (vOne, vTwo)
            return False

        return True

    def __utmSourceValidator(self, operation, vOne, vTwo):
        if self.__operatorSelection(
                operation,
                self.__utm_source,
                vOne,
                vTwo,
                str):
            self.message = constants.UTM_SOURCE_CONSTRAINT % (vOne)
            return False
        return True

    def __operatorSelection(self, operation, sourceVal, valOne, valTwo, cast):
        if operation == "eq":
            return False if cast(sourceVal) == cast(valOne) else True
        elif operation == "ls":
            return False if cast(sourceVal) < cast(valOne) else True
        elif operation == "gr":
            return False if cast(sourceVal) > cast(valOne) else True
        elif operation == "ge":
            return False if cast(sourceVal) >= cast(valOne) else True
        elif operation == "le":
            return False if cast(sourceVal) <= cast(valOne) else True
        elif operation == "between":
            return False if cast(valOne) <= cast(
                sourceVal) <= cast(valTwo) else True
        elif operation == "in":
            valList = [cast(val.strip()) for val in valOne.split(",")]
            return False if cast(sourceVal) in valList else True
        elif operation == 'not_in':
            valList = [cast(val.strip()) for val in valOne.split(",")]
            return cast(sourceVal) in valList
        elif operation == 'ne':
            return cast(sourceVal) == cast(valOne)
        elif operation == 'exclude_range':
            return not (cast(sourceVal) < cast(valOne)
                        or cast(sourceVal) > cast(valTwo))
