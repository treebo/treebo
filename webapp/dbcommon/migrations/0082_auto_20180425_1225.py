# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0081_auto_20180425_1223'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cityamenitymapforautopagecreation',
            options={'default_permissions': ('add', 'change', 'delete', 'read'), 'verbose_name': 'City Amenity Map for AutoPage Creation'},
        ),
    ]
