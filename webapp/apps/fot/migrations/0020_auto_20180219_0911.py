# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-02-19 09:11


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fot', '0019_auto_20180216_1002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fot',
            name='status',
            field=models.CharField(
                choices=[
                    ('FAILED',
                     'FAILED'),
                    ('BLOCKED',
                     'BLOCKED'),
                    ('NOT_CONFIRMED',
                     'NOT_CONFIRMED'),
                    ('PASSED',
                     'PASSED'),
                    ('IN_PROGRESS',
                     'IN_PROGRESS')],
                default='IN_PROGRESS',
                max_length=100),
        ),
        migrations.AlterField(
            model_name='fotreservationsrequest',
            name='status',
            field=models.CharField(
                choices=[
                    ('NOSHOW',
                     'NOSHOW'),
                    ('CHECKEDIN',
                     'CHECKEDIN'),
                    ('CHECKEDOUT',
                     'CHECKEDOUT'),
                    ('FEEDBACK_NOT_SUBMITTED',
                     'FEEDBACK_NOT_SUBMITTED'),
                    ('FEEDBACK_SUBMITTED',
                     'FEEDBACK_SUBMITTED'),
                    ('CANCELLED',
                     'CANCELLED'),
                    ('PENDING',
                     'PENDING'),
                    ('SCHEDULED',
                     'SCHEDULED'),
                    ('CANCEL_PENDING',
                     'CANCEL_PENDING')],
                default='PENDING',
                max_length=100),
        ),
        migrations.AlterField(
            model_name='fotsignupquestions',
            name='question_type',
            field=models.CharField(
                choices=[
                    ('CHECKBOX',
                     'CHECKBOX'),
                    ('DROPDOWN',
                     'DROPDOWN'),
                    ('TEXTAREA',
                     'TEXTAREA'),
                    ('RADIO',
                     'RADIO'),
                    ('TEXT',
                     'TEXT')],
                max_length=100),
        ),
        migrations.AlterField(
            model_name='fotsignupquestions',
            name='section',
            field=models.CharField(
                choices=[
                    ('PROBLEM_SOLVING',
                     'PROBLEM_SOLVING'),
                    ('PERSONAL',
                     'PERSONAL'),
                    ('ABOUT_FOT',
                     'ABOUT_FOT')],
                default='PERSONAL',
                max_length=100),
        ),
    ]
