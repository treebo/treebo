import logging

from apps.seo.models import HotelContentCountMapping, PositiveCategoryContentMapping
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class UpdateHotelsPositives(object):
    def update_hotel_positive_feedback(self, hotel_id, feedback_data):
        try:
            if feedback_data:
                logger.info(
                    "Updating the guest feedback for hotel %s with data %s",
                    hotel_id,
                    feedback_data)
                HotelContentCountMapping.objects.filter(
                    hotel__id=hotel_id).delete()
                for positive, count in list(feedback_data.items()):
                    pos_cat_content_map_obj = PositiveCategoryContentMapping.objects.filter(
                        positive__name=positive).first()
                    hotel = Hotel.objects.get(id=hotel_id)
                    #hotel = hotel_repository.get_single_hotel(id=hotel_id)
                    if pos_cat_content_map_obj and hotel:
                        logger.info(
                            "positive %s positive_db %s , category %s content %s",
                            positive,
                            pos_cat_content_map_obj.positive.name,
                            pos_cat_content_map_obj.category.name,
                            pos_cat_content_map_obj.content)
                        content_count_obj = HotelContentCountMapping.objects.create(
                            hotel=hotel, feedback_content=pos_cat_content_map_obj, count=count)
                        content_count_obj.save()
                    else:
                        logger.error(
                            "No such postive %s or hotel %s exists in db ",
                            positive,
                            hotel_id)
            else:
                logger.error(
                    "No data for positive feedbacks and count for hotel %s %s",
                    hotel_id,
                    feedback_data)
        except Exception:
            logger.exception(
                "Exception in updating the database with hotel positive feedback")
