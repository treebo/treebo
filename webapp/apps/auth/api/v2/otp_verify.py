import logging

from rest_framework.status import HTTP_400_BAD_REQUEST

from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.otp_verify_dto import OtpVerifyDTO
from apps.auth.services.cookie_service import CookieService
from apps.auth.services.otp_service import OTPService
from apps.auth.services.user_profile_service import UserProfileService
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import OTPVerifyException, UPSClientException
from apps.profiles.service.user_service import UserService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class OTPVerifyApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    otp_service = OTPService()
    cookie_service = CookieService()
    user_service = UserService()
    user_profile_service = UserProfileService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        is_verified = False
        try:
            otp_verify_dto = OtpVerifyDTO(data=request.data)
            if not otp_verify_dto.is_valid():
                logger.error(
                    "otp login verify invalid request %s ",
                    request.data)
                return api_response.invalid_request_error_response(
                    otp_verify_dto.errors, OTPVerifyApi.__name__)
            token_info, user, user_data = self.otp_service.verify_otp(otp_verify_dto.data)
            is_verified = True
            response = api_response.prep_success_response({
                'is_valid': is_verified,
                'message': "OTP is verified.",
                'failed_validation_attempts': token_info.get('failed_validation_attempts'),
                'max_failed_validation_attempts': token_info.get('max_failed_validation_attempts')
            })
            self.user_profile_service.mark_verfied_anon_user(otp_verify_dto.data['phone_number'], user_data)
        except UPSClientException as e:
            logger.error("Failed marking anon user verfied in ups")
            return response
        except OTPVerifyException as e:
            logger.exception("treebo otp verify exception %s ", request.data)

            message = {
                'error_type': str(e),
                'is_valid': is_verified,
                'message': e.message
            }

            if e.failed_validation_attempts:
                message['failed_validation_attempts'] = e.failed_validation_attempts
                message['max_failed_validation_attempts'] = e.max_failed_validation_attempts

            response = api_response.prep_success_response(response_code=e.status_code,
                                                          message=message)
        except TreeboException as e:
            logger.exception("treebo otp verify exception %s ", request.data)
            response = api_response.prep_success_response(response_code=HTTP_400_BAD_REQUEST,
                                                          message={
                                                              'error_type': str(e),
                                                              'is_valid': is_verified,
                                                              'message': "Invalid OTP"
                                                          })
        except Exception as e:
            logger.exception("otp verify exception %s ", request.data)
            response = api_response.prep_success_response(response_code=HTTP_400_BAD_REQUEST,
                                                          message={
                                                              'error_type': str(e),
                                                              'is_valid': is_verified,
                                                              'message': "Invalid OTP"
                                                          })
        return response
