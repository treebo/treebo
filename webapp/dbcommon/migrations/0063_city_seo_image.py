# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0062_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='city',
            name='seo_image',
            field=models.FileField(
                max_length=200,
                null=True,
                upload_to='city/',
                blank=True),
        ),
    ]
