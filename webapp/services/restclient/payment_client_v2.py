import datetime
import json
import logging

import requests
from django.conf import settings
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

from apps.checkout.dto.payment_service_dto import PaymentServiceV2InitiateDTO, PaymentServiceV2ConfirmDTO
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import PaymentGenerationException, PaymentVerifyException, \
    StatusPendingException, PaymentConfirmationPullException
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class PaymentClientV2:
    def initiate(self, amount, receipt, gateway, redirection_url, booking_order_id, utm_source=None,
                 notes=[], currency="INR", auto_capture=True, payment_offer_coupon_codes=None):

        if amount <= 1:
            return None

        data = {"amount": str(round(amount, 2)), "currency": currency, "receipt": receipt,
                "notes": notes, "gateway": gateway,
                "pg_meta": {"auto_capture": auto_capture, "redirection_url": redirection_url,
                            'booking_order_id': booking_order_id,
                            'utm_source': utm_source, 'offer_ids': payment_offer_coupon_codes}
                }

        try:
            response = self.__make_call("POST", "/paynow/v2/initiate/", data)
            status_code = response.status_code
            dev_message = api_response.fetch_message_from_response(
                response.json())
        except Exception as e:
            response = None
            status_code = HTTP_400_BAD_REQUEST
            dev_message = 'Payment service is down'
        if not response or not response.ok:
            logger.exception(
                " generate_payment_details exception from data %s ",
                json.dumps(data))
            raise PaymentGenerationException(
                'Unable to generate order.', dev_message, status_code)
        else:
            data = response.json()['data']
            logger.info('PaymentClientV2 initiate response %s ',data)
            res = PaymentServiceV2InitiateDTO(**data)
            return res

    def confirm(
            self,
            order_id,
            payment_id,
            pg_customer_id=None,
            pg_meta={},
            card={}):

        data = {
            "order_id": order_id,
            "payment_id": payment_id,
            "pg_meta": pg_meta}

        if card:
            data["card"] = card

        if pg_customer_id:
            data["pg_customer_id"] = pg_customer_id

        try:
            response = self.__make_call("POST", "/paynow/v2/confirm/", data)
            status_code = response.status_code
            dev_message = api_response.fetch_message_from_response(
                response.json())
        except Exception as e:
            response = None
            status_code = HTTP_400_BAD_REQUEST
            dev_message = 'Payment service is down'

        if not response or not response.ok:
            logger.exception(
                " confirm_payment exception from data %s ",
                json.dumps(data))
            raise PaymentVerifyException(
                'Payment Verification Failed.', dev_message, status_code)
        else:
            data = response.json()['data']
            logger.info('PaymentClientV2 confirm response %s ',data)
            res = PaymentServiceV2ConfirmDTO(**data)
            return res

    def pay(
            self,
            amount,
            receipt,
            gateway,
            currency="INR",
            notes=[],
            pg_meta={}):
        data = {"amount": str(round(amount, 2)), "receipt": receipt,
                "notes": notes, "gateway": gateway,
                "currency": currency, "pg_meta": pg_meta}
        try:
            response = self.__make_call("POST", "/paynow/v2/pay/", data)
            status_code = response.status_code
            dev_message = api_response.fetch_message_from_response(
                response.json())
        except Exception as e:
            response = None
            status_code = HTTP_400_BAD_REQUEST
            dev_message = 'Payment service is down'
            logger.info("Error in wallet debit")
            logger.exception(e)

        if not response or not response.ok:
            logger.exception(
                " confirm_payment exception from data %s ",
                json.dumps(data))
            raise PaymentVerifyException(
                'Unable to confirm order.', dev_message, status_code)
        else:
            data = response.json()['data']
            res = PaymentServiceV2InitiateDTO(**data)
            return res

    def status_pending(self, gateway):
        data = {"gateway" : gateway}
        try:
            response = self.__make_call("POST", "/paynow/v2/status/pending", data)
            status_code = response.status_code
            dev_message = api_response.fetch_message_from_response(response.json())
        except Exception as e:
            response = None
            status_code = HTTP_400_BAD_REQUEST
            dev_message = 'Payment service is down'
            logger.info("Error while getting the pending status")
            logger.exception(e)

        if not response or not response.ok:
            logger.error(" status_pending exception from data %s ", json.dumps(data))
            raise StatusPendingException('Unable to update status.', dev_message,
                                         status_code)
        else:
            data = response.json()['data']
            return data

    def get_and_update_payment_status_from_gateway(self, order_id, payment_id):
        data = dict(order_id=order_id, payment_id=payment_id)
        try:
            response = self.__make_call("POST", "/paynow/v2/payment/status", data)
            status_code = response.status_code
            dev_message = api_response.fetch_message_from_response(
                response.json())
        except Exception as e:
            response = None
            status_code = HTTP_400_BAD_REQUEST
            dev_message = 'Payment service is down'
            logger.exception("Error while making the payment service call to pull the payment status "
                             "from gateway for payment id:{}".format(payment_id))
        if not response or not response.ok:
            logger.error("Payment service response from for pulling payment confirmation from gateway. "
                         "the error response is:{}".format(dev_message))
            raise PaymentConfirmationPullException('Unable to confirm payment.', dev_message, status_code)
        logger.info('Successfully triggered the payment status update for payment_id={}'.format(payment_id))


    def __make_call(self, method, page_name, data, params='', *args):
        url = settings.PAYMENT_SERVICE_HOST + page_name
        logger.info("payment client v2 %s , %s %s at %s ", page_name,
                    url, data, datetime.datetime.now().isoformat())
        res = requests.request(
            method,
            url,
            params=params,
            json=data,
            allow_redirects=True)
        logger.info(
            "payment client v2 %s , %s %s with response %s at %s  ",
            page_name,
            url,
            data,
            res.json(),
            datetime.datetime.now().isoformat())

        if not res:
            status_code = HTTP_500_INTERNAL_SERVER_ERROR
            message = "Payment Service is Down"
        else:
            status_code = res.status_code
            message = res.json()
        if str(status_code).startswith('5') or str(status_code).startswith('4'):
            slack_alert.send_slack_alert_for_third_party(status = status_code, url = url,
                                                         class_name = self.__class__.__name__,
                                                         reason = message, data = data)
        return res

    def refund(self, payment_order_id, gateway, amount, notes):

        data = {
            "order_id": payment_order_id,
            "gateway": gateway,
            "amount": amount,
            "notes": [notes]
        }

        try:
            logger.info("calling payment service refund api with data %s", data)
            response = self.__make_call("POST", "/paynow/refund/", data)

        except Exception as err:
            logger.exception("Unable to call payment client refund api => %s", err.__str__())
            return False

        if not response.ok:
            message = api_response.fetch_message_from_response(
                response.json())
            logger.exception("Exception occurred in payment client refund api => %s", message)
            return False

        return True
