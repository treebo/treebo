__author__ = 'amithsoman'

from django.apps import AppConfig
from django.db.models.signals import post_migrate
from apps.hms.send_notification import notify_hms

default_app_config = 'dbcommon.DBCommonAppConfig'


class DBCommonAppConfig(AppConfig):
    name = 'dbcommon'

    def ready(self):
        post_migrate.connect(notify_hms, sender=self)
        from .models import signals
