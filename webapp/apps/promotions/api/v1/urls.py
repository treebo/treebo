from django.conf.urls import url

from apps.promotions.api.v1.meta_redirection_type import MetaRedirectionType


app_name = 'promotions_v1'

urlpatterns = [
    url(r'^meta/redirection/type$', MetaRedirectionType.as_view(), name='meta-redirection-type-v1'),
]
