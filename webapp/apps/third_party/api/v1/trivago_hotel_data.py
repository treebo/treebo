import logging
import collections
import csv

from django.core.cache import cache
from django.conf import settings
from django.http import HttpResponse
from rest_framework.response import Response
from django.core.files import File

from apps.third_party.models import TrivagoFileUploadModel
from base.views.api import TreeboAPIView
from common.constants import common_constants as const
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from apps.third_party.tasks import trivago_fetch_hotel_data_and_email, \
    trivago_save_hotel_data_in_csv
from apps.third_party.service.third_party_hotel_service import TRIVAGO_API_VERSION, \
    TRIVAGO_API_LANGUAGE_DEFAULT, ThirdPartyHotelService
from apps.third_party.utm_source_mapping import UTM
from apps.common.slack_alert import SlackAlertService as slack_alert

from boto.s3.connection import S3Connection
from boto.s3.key import Key

logger = logging.getLogger(__name__)


class TrivagoHotelData(TreeboAPIView):
    CACHE_TIMEOUT = 60 * 60 * 24
    CACHE_KEY = 'cs_apicache_trivago_hotel_detail_v3_{0}'

    def get_trivago_hotel_detail_from_cache(self, hotel_id):
        cache_key = self.CACHE_KEY.format(hotel_id)
        try:
            value = cache.get(cache_key)
        except Exception as e:
            value = None

        return value

    def get(self, request):
        trivago_hotel_list = []
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        try:
            hotels_id_list = hotel_repository.get_all_active_hotel_ids()
            hotels_id_list = sorted(hotels_id_list)
            url_prefix = request.build_absolute_uri('/').rstrip('/')
            for hotel_id in hotels_id_list:
                trivago_hotel_detail = self.get_trivago_hotel_detail_from_cache(hotel_id)
                if not trivago_hotel_detail:
                    try:
                        hotel_detail = ThirdPartyHotelService.hotel_detail_fetch(
                            url_prefix, hotel_id)
                        trivago_hotel_detail = ThirdPartyHotelService.build_third_party_hotel(
                            hotel_detail)
                    except Exception as e:
                        logger.exception("exception while getting the hotel data for hotel id:{}".format(hotel_id))
                        slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                                    message="Exception while getting"
                                                                            " the hotel data for hotel id:{}".format(hotel_id),
                                                                    class_name=self.__class__.__name__)
                        continue
                    cache.set(self.CACHE_KEY.format(hotel_id), trivago_hotel_detail)
                trivago_hotel_list.append(trivago_hotel_detail)
            trivago_hotel_data_response_tuple = (
                ('api_version',
                 TRIVAGO_API_VERSION),
                ('lang',
                 TRIVAGO_API_LANGUAGE_DEFAULT),
                ('hotels',
                 trivago_hotel_list))
            trivago_hotel_data_response = collections.OrderedDict(
                trivago_hotel_data_response_tuple)
        except BaseException:
            logger.exception(
                'Hotel data consolidation failed for TrivagoHotelData')
            raise

        return Response(trivago_hotel_data_response)


class TrivagoCsvHotelData(TreeboAPIView):
    def get(self, request, **kwargs):
        try:
            source = kwargs['source'] if 'source' in kwargs else 'trivago'
            url_prefix = request.build_absolute_uri('/').rstrip('/')
            to_email = str(request.GET.get('email'))
            if to_email:
                trivago_fetch_hotel_data_and_email.delay(url_prefix, to_email, source)
                return HttpResponse(
                    '<h1>You will receive an email with the attachment shortly</h1>')
            else:
                return HttpResponse('<h1>Please provide an email</h1>')
        except BaseException:
            logger.exception('GET failed for TrivagoCsvHotelData')
            return HttpResponse('<h1>The request has failed</h1>')


class TrivagoCSVDownloader(TreeboAPIView):
    def get(self, request, **kwargs):
        url_prefix = request.build_absolute_uri('/').rstrip('/')
        source = kwargs['source'] if 'source' in kwargs else 'trivago'
        try:

            conn_s3 = S3Connection(host = 's3-ap-southeast-1.amazonaws.com')
            bucket = conn_s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
            bucket_key = Key(bucket)
            path = UTM[source]["csv_path_name"]
            bucket_key.key = settings.S3_STATIC_EXTERNAL_FILE_PATH + path

            bucket_key.get_contents_to_filename(settings.TRIVAGO_CSV_LINK)
            download_file = File(open(settings.TRIVAGO_CSV_LINK, 'r'))

            response = HttpResponse(download_file, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename=trivago_hotel_data.csv'
            return response
        except BaseException as e:
            logger.exception('GET failed for %sCsvHotelData : '+str(e), source)
            return HttpResponse(
                '<h1>The request has failed ,retry after 10 mins</h1>')
        finally:
            trivago_save_hotel_data_in_csv.delay(url_prefix, source=source)


class TrivagoUpdateCSVFile(TreeboAPIView):
    def get(self, request):
        url_prefix = request.build_absolute_uri('/').rstrip('/')
        try:
            trivago_save_hotel_data_in_csv.delay(url_prefix)
            return HttpResponse('<h1>Process is initiated , File will be updated in 10 mins</h1>')
        except Exception as e:
            logger.exception('GET failed for TrivagoUpdateCSVFile : ' + str(e))
            return HttpResponse('<h1>The request has failed ,retry after 10 mins</h1>')


class TrivagoUpdateCSVFileSync(TreeboAPIView):
    def get(self, request):
        url_prefix = request.build_absolute_uri('/').rstrip('/')
        try:
            trivago_save_hotel_data_in_csv(url_prefix)
            return HttpResponse('<h1>File will be updated successfully , you can download now.</h1>')
        except Exception as e:
            logger.exception('GET failed for TrivagoUpdateCSVFileSync : ' + str(e))
            return HttpResponse('<h1>The request has failed ,retry after 10 mins</h1>')


class TrivagoBidFileDownloader(TreeboAPIView):
    def get(self, request):
        try:
            trivago_file_inst = TrivagoFileUploadModel.objects.latest(
                'created_at')
            file_path = settings.MEDIA_ROOT + \
                str(trivago_file_inst.trivago_file)
            logger.info(
                "getting bid file from file path %s", str(
                    trivago_file_inst.trivago_file))
            download_file = File(open(file_path, 'r'))
            response = HttpResponse(download_file, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename=trivago_bid_data.csv'
            return response
        except Exception as e:
            logger.exception('GET failed for TrivagoCsvHotelData')
            return HttpResponse(
                '<h1>The request has failed ,retry after 10 mins</h1>')
