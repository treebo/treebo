# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('checkout', '0015_auto_20170928_0832'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingRequestUserMapForWallet',
            fields=[
                ('id',
                 models.AutoField(
                     verbose_name='ID',
                     serialize=False,
                     auto_created=True,
                     primary_key=True)),
                ('created_at',
                 models.DateTimeField(
                     auto_now_add=True,
                     verbose_name='Created at')),
                ('modified_at',
                 models.DateTimeField(
                     auto_now=True,
                     verbose_name='Modified at')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='wallet_applied',
            field=models.NullBooleanField(
                default=False),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='wallet_deduction',
            field=models.DecimalField(
                default=0,
                null=True,
                max_digits=9,
                decimal_places=4),
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='status',
            field=models.CharField(
                default='Initiated',
                choices=[
                    ('Initiated',
                     'Initiated'),
                    ('Payment In Progress',
                     'Payment In Progress'),
                    ('Completed',
                     'Completed'),
                    ('Part Success',
                     'Part Success')],
                max_length=50,
                blank=True,
                null=True,
                db_index=True),
        ),
        migrations.AddField(
            model_name='bookingrequestusermapforwallet',
            name='booking_request',
            field=models.ForeignKey(
                related_name='bookingrequestmaps',
                blank=True,
                to='checkout.BookingRequest',
                null=True,
                on_delete=models.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='bookingrequestusermapforwallet',
            name='user',
            field=models.ForeignKey(
                related_name='usermaps',
                blank=True,
                to=settings.AUTH_USER_MODEL,
                null=True,
                on_delete=models.DO_NOTHING),
        ),
    ]
