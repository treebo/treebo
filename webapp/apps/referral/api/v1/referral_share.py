import logging

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED

from apps.referral.service.service import ReferralService
from base.views.api import TreeboAPIView
from services.restclient.contentservicerestclient import ContentServiceClient
from rest_framework.authentication import BasicAuthentication
from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication

logger = logging.getLogger(__name__)


class ReferralShareAPI(TreeboAPIView):
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)

    def dispatch(self, *args, **kwargs):
        return super(ReferralShareAPI, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        raise MethodNotAllowed("GET")

    def post(self, request, format=None):
        if request.user.is_authenticated():
            data = request.data
            emails = data.get('emails', None)
            if emails:
                emails = emails.split(",")
                campaign = ContentServiceClient.getValueForKey(
                    request, "referral_campaign_id", "1")
                ReferralService.share_referral_email(
                    emails, request.user, campaign["text"])
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)
        return Response(data={
            "status": "success",
            "msg": "Emails sent successfully"
        })
