from apps.seo.components.base_component import BaseComponent
from apps.seo.services.city_coordinate.city_coordinate import CityCoordinate
from apps.seo.serializers.city_coordinate import CityCoordinateSerializer
import logging

logger = logging.getLogger(__name__)


class CityCoordinateComponent(BaseComponent):

    def __init__(self, city, footer_data):
        super(CityCoordinateComponent, self).__init__(city, footer_data, [])

    def build(self):
        content = CityCoordinate().city_coordinate(self.current_city)
        if len(content) == 0:
            data = {}
        else:
            data = content
        ser_obj = CityCoordinateSerializer(data=data)
        if ser_obj.is_valid():
            logger.info(data)
            return {
                'place_coordinate': ser_obj.validated_data
            }
        else:
            logger.error(
                'serializer error in city coordinate %s',
                ser_obj.errors)

        return {
            'place_coordinate': {}
        }
