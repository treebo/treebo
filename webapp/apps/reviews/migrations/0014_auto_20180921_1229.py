# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-09-21 12:29
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0013_reviewappconfiguration_disable_for_category_list'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reviewappconfiguration',
            name='disable_for_category_list',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, max_length=50), blank=True, default=[], size=20),
        ),
    ]
