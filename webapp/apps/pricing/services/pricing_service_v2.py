import datetime
import logging
from collections import defaultdict, Counter
from datetime import datetime
from decimal import Decimal

import requests
from django.conf import settings

from apps.pricing.pricing_utils import PricingUtils
from apps.pricing.api.v4 import RatePlanPrice
from apps.pricing.dateutils import date_to_ymd_str, date_range
from apps.pricing.exceptions import SoaError
from base import log_args
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class PricingServiceV2(object):
    def __init__(self):
        pass

    @log_args(logger)
    def get_search_page_price(self, pdto):
        supported_room_prices = self.get_price(pdto)
        sale_details = PricingUtils.get_sale_details(pdto)
        hotel_wise_cumulative_prices = self._get_cumulative_prices(
            pdto, supported_room_prices, sale_details=sale_details)
        return hotel_wise_cumulative_prices

    @log_args(logger)
    def get_hd_page_price(self, pdto):
        supported_room_prices = self.get_price(pdto)
        sale_details = PricingUtils.get_sale_details(pdto)
        hotel_wise_cumulative_prices = self._get_cumulative_prices(
            pdto, supported_room_prices, sale_details=sale_details)
        return hotel_wise_cumulative_prices.get(pdto.hotel_ids[0])

    @log_args(logger)
    def get_itinerary_page_price(self, pdto):
        supported_room_prices = self.get_price(pdto)
        sale_details = PricingUtils.get_sale_details(pdto)
        hotel_wise_cumulative_prices = self._get_cumulative_prices_for_room_and_rateplan(
            pdto, supported_room_prices, sale_details=sale_details)
        return hotel_wise_cumulative_prices.get(pdto.hotel_ids[0])

    def _get_cumulative_prices_for_room_and_rateplan(self, pdto, supported_room_prices, sale_details=None):
        """
        supported_room_prices[hotel_id][room_type][rate_plan][(date, room_config)] = rp
        """
        required_room_type = pdto.room_type
        required_rate_plan = pdto.rate_plan
        room_configs = pdto.room_configs
        room_config_counter = Counter(room_configs)
        cumulative_prices = dict()
        for hotel_id, room_wise_prices in list(supported_room_prices.items()):
            room_prices = room_wise_prices.get(required_room_type.lower())
            if not room_prices:
                cumulative_prices[hotel_id] = dict(
                    cumulative_price=None, date_wise_breakup={})
                continue

            cumulative_price = dict()
            date_wise_breakup = dict()
            date_and_room_wise_price = {}
            for rate_plan, date_and_rc_wise_price in list(room_prices.items()):
                cumulative_price[rate_plan] = self._sum_rateplan_wise_prices(
                    pdto, date_and_rc_wise_price, room_config_counter, sale_details=sale_details, hotel_id=hotel_id)
                if rate_plan == required_rate_plan:
                    date_wise_breakup = self._get_date_wise_breakup(
                        pdto, date_and_rc_wise_price, room_config_counter, sale_details=sale_details, hotel_id=hotel_id)
                if rate_plan == required_rate_plan:
                    date_and_room_wise_price = self._get_date_and_room_wise_breakup(
                        pdto, date_and_rc_wise_price, room_config_counter, sale_details=sale_details, hotel_id=hotel_id)
            cumulative_prices[hotel_id] = dict(
                cumulative_price=cumulative_price,
                date_wise_breakup=date_wise_breakup,
                date_and_room_wise_price=date_and_room_wise_price)
        return cumulative_prices

    def _get_cumulative_prices(self, pdto, supported_room_prices, sale_details=None):
        """
        supported_room_prices[hotel_id][room_type][rate_plan][(date, room_config)] = rp
        """
        room_configs = pdto.room_configs
        room_config_counter = Counter(room_configs)
        cumulative_prices = dict()
        for hotel_id, room_wise_prices in list(supported_room_prices.items()):
            room_wise_cumulative_price = dict()
            for room_type, rate_plan_wise_price in list(
                    room_wise_prices.items()):
                rate_plan_cumulative_price = dict()
                for rate_plan, date_and_rc_wise_price in list(
                        rate_plan_wise_price.items()):
                    rate_plan_cumulative_price[rate_plan] = self._sum_rateplan_wise_prices(
                        pdto, date_and_rc_wise_price, room_config_counter, sale_details=sale_details, hotel_id=hotel_id)
                room_wise_cumulative_price[room_type] = rate_plan_cumulative_price
            cumulative_prices[hotel_id] = room_wise_cumulative_price
        return cumulative_prices

    def _sum_rateplan_wise_prices(
            self,
            pdto,
            date_and_rc_wise_price,
            room_config_counter,
            sale_details=None,
            hotel_id=None):
        rate_plan_cumulative_price = None
        rate_plan_meta = None
        for date_config, rate_plan_price in list(
                date_and_rc_wise_price.items()):
            room_count = room_config_counter.get(date_config[1])
            rate_plan_meta = rate_plan_price['meta']
            rate_plan_price = rate_plan_price['prices']
            rate_plan_price = PricingUtils.get_sale_rate_plan(pdto, hotel_id, rate_plan_price, sale_details=sale_details)
            if rate_plan_price.sell_price <= 0:
                rate_plan_cumulative_price = rate_plan_price
                break
            if room_count > 1:
                rate_plan_price = rate_plan_price.multiply(room_count)
            if rate_plan_cumulative_price:
                rate_plan_cumulative_price = rate_plan_cumulative_price.add(
                    rate_plan_price)
            else:
                rate_plan_cumulative_price = rate_plan_price

        rate_plan_cumulative_price.do_round_off()
        return dict(price=rate_plan_cumulative_price, meta=rate_plan_meta)

    def _get_date_wise_breakup(
            self,
            pdto,
            date_and_rc_wise_price,
            room_config_counter,
            sale_details=None,
            hotel_id=None):
        date_wise_cumulative_price = defaultdict(RatePlanPrice)
        for date_config, rate_plan_price in list(
                date_and_rc_wise_price.items()):
            room_count = room_config_counter.get(date_config[1])
            date = date_config[0]
            rate_plan_price = rate_plan_price['prices']
            rate_plan_price = PricingUtils.get_sale_rate_plan(pdto, hotel_id, rate_plan_price, sale_details=sale_details)
            if room_count > 1:
                rate_plan_price = rate_plan_price.multiply(room_count)
            price = date_wise_cumulative_price.get(date)
            if price:
                price = price.add(rate_plan_price)
            else:
                price = rate_plan_price
            date_wise_cumulative_price[date] = price

        for date_, price in list(date_wise_cumulative_price.items()):
            price.do_round_off()

        return date_wise_cumulative_price

    def _get_date_and_room_wise_breakup(
            self,
            pdto,
            date_and_rc_wise_price,
            room_config_counter,
            sale_details=None,
            hotel_id=None):
        date_and_room_wise_price = defaultdict(RatePlanPrice)
        for date_config, rate_plan_price in list(date_and_rc_wise_price.items()):
            room_count = room_config_counter.get(date_config[1])
            date = date_config[0]
            rate_plan_price = rate_plan_price['prices']
            rate_plan_price = PricingUtils.get_sale_rate_plan(pdto, hotel_id, rate_plan_price, sale_details=sale_details)
            if room_count > 1:
                rate_plan_price = rate_plan_price.multiply(room_count)
            price = rate_plan_price
            date_and_room_config = (date, date_config[1])
            date_and_room_wise_price[date_and_room_config] = price

        for date_and_room_config, price in list(date_and_room_wise_price.items()):
            price.do_round_off()

        return date_and_room_wise_price

    def get_price(self, pdto):
        room_config_str = ''
        for config in pdto.room_configs:
            room_config_str += '&room_config=' + config

        hotel_list = ''
        for hid in pdto.hotel_ids:
            hotel_list += '&hotel_id=' + str(hid)

        url = settings.BASE_PRICING_URL_V2 + "stay_start={0}&stay_end={1}".format(
            date_to_ymd_str(pdto.checkin_date), date_to_ymd_str(pdto.checkout_date))
        url += room_config_str
        url += hotel_list

        if pdto.channel:
            url += "&channel={0}".format(pdto.channel)

        if pdto.coupon_code and pdto.coupon_value and pdto.coupon_type:
            url += "&coupon_type={0}&coupon_code={1}&coupon_value={2}".format(
                pdto.coupon_type, pdto.coupon_code, round(pdto.coupon_value, 2))

        # if pdto.get_from_cache:
        #     url += "&use_cache=1"

        if pdto.logged_in_user:
            url += "&user_logged_in=1"

        if pdto.utm_source:
            url += "&utm_source={0}".format(pdto.utm_source)

        response = requests.get(url)
        logger.info(
            "Pricing API V2 called got status %s and url %s",
            response.status_code,
            url)

        if response.status_code != requests.codes.ok:
            logger.error(
                "Pricing API V2 Error response:%s %s",
                response.status_code,
                response.content)

            if response.status_code != 400:
                slack_alert.send_slack_alert_for_third_party(status=response.status_code, url=url,
                                                             class_name="PricingServiceV2")

            raise SoaError(
                response.status_code,
                'Unable to fetch prices for the hotel.',
                response.status_code)

        data = response.json()
        if not data.get('data'):
            logger.error(" prices are not available %s ", url)
            raise SoaError(
                response.status_code,
                'Prices are not available for this hotel.',
                response.status_code)

        transformed_response = self._transform_pricing_response(
            data, pdto.checkin_date, pdto.checkout_date, pdto.room_configs)
        return transformed_response

    def _transform_pricing_response(
            self,
            response,
            checkin_date,
            checkout_date,
            room_configs):
        all_booking_units = self._get_all_unique_date_and_room_config_tuple_set_requested(
            checkin_date, checkout_date, room_configs)
        hotel_wise_prices = self._build_hotel_wise_room_wise_prices_from_response(
            response)
        supported_room_prices = self._filter_room_type_supporting_all_room_config(
            all_booking_units, hotel_wise_prices)
        return supported_room_prices

    @staticmethod
    def _get_all_unique_date_and_room_config_tuple_set_requested(
            checkin_date, checkout_date, room_configs):
        all_booking_units = []
        for d in date_range(checkin_date, checkout_date):
            dstr = datetime.strftime(d, '%Y-%m-%d')
            for rc in room_configs:
                all_booking_units.append((dstr, rc))
        all_booking_units = set(all_booking_units)
        return all_booking_units

    @staticmethod
    def _build_hotel_wise_room_wise_prices_from_response(price_response):
        def tree(): return defaultdict(tree)
        prices = price_response.get('data', [])
        hotel_wise_prices = tree()
        for price in prices:
            bu = price['booking_unit']
            hotel_id, room_type, date, room_config = bu['hotel_id'], bu[
                'room_type_code'], bu['date'], bu['room_config']
            rate_plans = price['rate_plans']
            for rp in rate_plans:
                rp['prices'] = RatePlanPrice.build(rp['prices'])
                rp['meta'] = rp['meta']
                hotel_wise_prices[hotel_id][room_type][rp['name']][(
                    date, room_config)] = rp
        return hotel_wise_prices

    @staticmethod
    def _filter_room_type_supporting_all_room_config(
            all_booking_units, hotel_wise_prices):
        def tree(): return defaultdict(tree)
        supported_room_prices = tree()
        for hotel_id, room_wise_prices in list(hotel_wise_prices.items()):
            for room_type, rate_plan_wise_prices in list(
                    room_wise_prices.items()):
                for rate_plan, date_and_rc_wise_prices in list(
                        rate_plan_wise_prices.items()):
                    if not (all_booking_units -
                            set(date_and_rc_wise_prices.keys())):
                        supported_room_prices[hotel_id][room_type][rate_plan] = date_and_rc_wise_prices
        return supported_room_prices

    @staticmethod
    def calculate_total_amount(
            pretax,
            member_discount,
            coupon_discount,
            voucher,
            tax_amount):
        """
        Calculates the total amount using all the components of the price. This is done because of inconsistencies
        in python's floating point calculation.
        :param pretax:
        :param coupon_discount:
        :param member_discount:
        :param voucher:
        :param tax_amount:
        :return: (Decimal) total amount
        """
        return Decimal(pretax) - Decimal(member_discount) - \
            Decimal(coupon_discount) - Decimal(voucher) + Decimal(tax_amount)
