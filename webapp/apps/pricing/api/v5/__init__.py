import logging

from decimal import Decimal

TWO_PLACES = Decimal("0.01")

logger = logging.getLogger(__name__)


def round_to_two_place(decimal_price):
    return decimal_price.quantize(TWO_PLACES)


class Price(object):
    def __init__(self, pretax=None, tax=None, posttax=None):
        self.pretax = pretax
        self.tax = tax
        self.posttax = posttax

    def __repr__(self):
        return str(self.__dict__)


class RatePlanPrice(object):

    BASE_PRICE_KEY = 'base_price'
    PROMO_DISCOUNT_KEY = 'promo_discount'
    MEMBER_DISCOUNT_KEY = 'member_discount'
    COUPON_DISCOUNT_KEY = 'coupon_discount'
    EXTRA_ADULT_CHARGE = 'extra_adult_charge'

    def __init__(
            self,
            base_price,
            final_price,
            promo_discount=0,
            coupon_discount=0,
            member_discount=0,
            member_discount_applied=False,
            member_discount_available=False):
        self.base_price = base_price.pretax
        self.base_price_tax = base_price.tax
        self.rack_rate = base_price.posttax
        self.pretax_price = final_price.pretax
        self.tax = final_price.tax
        self.sell_price = final_price.posttax
        self.promo_discount = promo_discount
        self.coupon_discount = coupon_discount
        self.member_discount = member_discount
        self.member_discount_applied = member_discount_applied
        self.member_discount_available = member_discount_available

    def do_round_off(self):
        self.base_price = round_to_two_place(Decimal(self.base_price))
        self.base_price_tax = round_to_two_place(Decimal(self.base_price_tax))
        self.rack_rate = round_to_two_place(Decimal(self.rack_rate))
        self.pretax_price = round_to_two_place(Decimal(self.pretax_price))
        self.tax = round_to_two_place(Decimal(self.tax))
        self.sell_price = round_to_two_place(Decimal(self.sell_price))
        self.promo_discount = round_to_two_place(Decimal(self.promo_discount))
        self.coupon_discount = round_to_two_place(
            Decimal(self.coupon_discount))
        self.member_discount = round_to_two_place(
            Decimal(self.member_discount))
        self.member_discount_applied = self.member_discount_applied
        self.member_discount_available = self.member_discount_available

    def add(self, addend):
        base_price = Price(pretax=self.base_price + addend.base_price,
                           posttax=self.rack_rate + addend.rack_rate,
                           tax=self.base_price_tax + addend.base_price_tax)
        final_price = Price(pretax=self.pretax_price + addend.pretax_price,
                            posttax=self.sell_price + addend.sell_price,
                            tax=self.tax + addend.tax)
        new_rate_plan_price = RatePlanPrice(
            base_price,
            final_price,
            self.promo_discount + addend.promo_discount,
            self.coupon_discount + addend.coupon_discount,
            self.member_discount + addend.member_discount,
            member_discount_applied=(
                self.member_discount_applied or addend.member_discount_applied),
            member_discount_available=(
                self.member_discount_available or addend.member_discount_available))
        return new_rate_plan_price

    def multiply(self, multiplier):
        base_price = Price(pretax=self.base_price * multiplier,
                           posttax=self.rack_rate * multiplier,
                           tax=self.base_price_tax * multiplier)
        final_price = Price(pretax=self.pretax_price * multiplier,
                            posttax=self.sell_price * multiplier,
                            tax=self.tax * multiplier)
        new_rate_plan_price = RatePlanPrice(
            base_price,
            final_price,
            self.promo_discount * multiplier,
            self.coupon_discount * multiplier,
            self.member_discount * multiplier,
            member_discount_applied=self.member_discount_applied,
            member_discount_available=self.member_discount_available)
        return new_rate_plan_price

    @classmethod
    def get_price_from_discount_block(cls, precomputation_block):
        if precomputation_block and precomputation_block['applied']:
            price = precomputation_block['value']
        else:
            price = 0
        return price

    @classmethod
    def get_member_discount_applicability(cls, precomputation_block):
        applied = precomputation_block and precomputation_block['applied']
        available = bool(
            precomputation_block and precomputation_block['value'])
        return applied, available

    @classmethod
    def build(cls, prices):
        initial_price = prices['initial_price']
        final_price = prices['final_price']
        pretax_computations = final_price['pre_tax_computation']

        base_price = Price(pretax=initial_price['pre_tax'],
                           tax=initial_price['tax'],
                           posttax=initial_price['post_tax'])
        final_price = Price(pretax=final_price['pre_tax'],
                            tax=final_price['tax'],
                            posttax=final_price['post_tax'])

        pretax_computation = dict()
        for computation in pretax_computations:
            pretax_computation[computation['name']] = computation

        promo_discount = cls.get_price_from_discount_block(
            pretax_computation.get(RatePlanPrice.PROMO_DISCOUNT_KEY))
        member_discount = cls.get_price_from_discount_block(
            pretax_computation.get(RatePlanPrice.MEMBER_DISCOUNT_KEY))
        coupon_discount = cls.get_price_from_discount_block(
            pretax_computation.get(RatePlanPrice.COUPON_DISCOUNT_KEY))
        extra_adult_charge = cls.get_price_from_discount_block(
            pretax_computation.get(RatePlanPrice.EXTRA_ADULT_CHARGE))
        applied, available = cls.get_member_discount_applicability(
            pretax_computation.get(RatePlanPrice.MEMBER_DISCOUNT_KEY))

        rate_plan_price = RatePlanPrice(
            base_price,
            final_price,
            promo_discount,
            coupon_discount=coupon_discount,
            member_discount=member_discount,
            member_discount_applied=applied,
            member_discount_available=available)
        return rate_plan_price

    def get_total_discount_percent(self):
        promo_discount = self.promo_discount
        coupon_discount = self.coupon_discount
        member_discount = self.member_discount
        base_price = self.base_price

        total_discount = promo_discount + coupon_discount + member_discount
        total_discount_percent = Decimal(
            total_discount) / Decimal(base_price) * 100
        return float(total_discount_percent.quantize(TWO_PLACES))

    def __repr__(self):
        return "RatePlanPriceObj [" + str(self.__dict__) + "]"


class RoomPrice(object):
    def __init__(self, room_type, rate_plan, price, meta=None):
        self.room_type = room_type
        self.rate_plan = rate_plan
        self.price = price
        self.meta = meta if meta else dict()

    def __repr__(self):
        return str(self.__dict__)
