import logging

from apps.common.date_time_utils import get_current_ist_date_time
from apps.content.models import ContentStore
from apps.growth import constants
from datetime import datetime

logger = logging.getLogger(__name__)


class SubmitCancelResponse(object):
    def create_submit_cancel_response(self):
        response_dict = dict()
        message = None
        cancelled_dateTime = get_current_ist_date_time()
        try:
            content_store_obj = ContentStore.objects.filter(
                name=constants.STATUS_MAPPING_TO_MESSAGE, version=1).first()
            booking_status = constants.SUBMIT_CANCEL_CANCELLED.lower()
            if content_store_obj:
                content_store_val = content_store_obj.value
                if content_store_val.get(booking_status):
                    message = content_store_val[booking_status]
            response_dict['data'] = {
                'booking_status': {
                    'status': constants.SUBMIT_CANCEL_CANCELLED,
                    'message': str(message),
                    'cancelled_dateTime': str(cancelled_dateTime)
                }
            }
            return response_dict
        except Exception as e:
            logger.exception(
                "Exception in creating submit cancel response in web")
            raise e
