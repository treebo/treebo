from pyquery import PyQuery as pQ


class ResponseXmlFactory(object):
    @staticmethod
    def build_empty_rate_id_xml():
        xml = """
        <hotelogix version="1.0" datetime="2016-06-24T10:47:12">
          <response languagecode="en">
            <hotels>
              <hotel id="Rdys9RXaEjFZTS2NZQxUDcTtsbB_ENPRZSZCg5E4Wn8|" code="20620" title="White Inn(20620)">
                <status code="1400" message="Success"/>
                <roomtypes>
                  <roomtype code="Oak" title="Oak (Standard)">
                    <rates>
                    </rates>
                  </roomtype>
                </roomtypes>
              </hotel>
            </hotels>
          </response>
          <request method="rateidsforadtocart" key="AmRdd3WyVZN3jIG" languagecode="en">
            <stay checkindate="2016-07-23" checkoutdate="2016-07-24"/>
            <hotels>
              <hotel id="20620"/>
            </hotels>
          </request>
        </hotelogix>
        """
        return pQ(xml)

    @staticmethod
    def build_rate_id_xml_without_treebo_promotional():
        xml = """
        <hotelogix version="1.0" datetime="2016-06-24T10:47:12">
          <response languagecode="en">
            <hotels>
              <hotel id="Rdys9RXaEjFZTS2NZQxUDcTtsbB_ENPRZSZCg5E4Wn8|" code="20620" title="White Inn(20620)">
                <status code="1400" message="Success"/>
                <roomtypes>
                  <roomtype code="Oak" title="Oak (Standard)">
                  <rates>
                      <rate code="WEBLM" title="Last Min Rate">
                        <pax adult="0" child="1" id="gi4nGMKoN_hHMLEH7-CDPhIVBczHcpCW9-3InF2b4UtZW6Rb8HwYYnkmrHSp3FzWt0zLgjV4-_W"/>
                        <pax adult="0" child="2" id="MS4nEPN2uDfHm_u6kzyHcYAHWyQSn6-eY9gZ2ZHBS-jqXgiaSSc-q8u4hcp7LQCUlx-sNEna3Dj"/>
                        <pax adult="0" child="3" id="dhI77he_2rT9-A567BOxdrr9uGECbMW-2XXlYOL0tEjhWgCessa4SyBs7XhMeTnDccPKVblzDH8"/>
                        <pax adult="1" child="0" id="ZDbx1E2RyIKgG0FTc2bhN1BC-NDUKJ2DZDSJkSpBlPtVtpw2eglNy-Gra96uo59xFS6ACcYWl6D"/>
                        <pax adult="1" child="1" id="z1daNr12csN1V_50LZN3jbxO2eJirXHQJWVzXEnpZYxghQNsN2Og040RRAC30kbKxkewXrophrr"/>
                        <pax adult="1" child="2" id="kRLPCSyK-OBReR6ZHExn-rbRayq5IhpBW7ZUrObBb7UBcvsy4EMIo8UKl9h-CSBC07mIJiJQiil"/>
                        <pax adult="2" child="0" id="kjgOoF9cszf5jNCgUoefalxfslNUilIAWh1sq6jCgG8yYAejq9zFpN0ksTOmsMmsadfsdf-SPiu"/>
                        <pax adult="2" child="1" id="q4F-K-MJYsSwXbFN8jXEUTDcVVqenEYb3nI7c9nESNt_EZIQb3433aHNKJBnt3CYFn_cTATsKzz"/>
                        <pax adult="3" child="0" id="posyXY_znJg3qdQhi59E3gLnookLLisPtARPgq46SpNqOLKn607u1xc5joDDQNN5k7yGsfspZ7W"/>
                      </rate>
                      <rate code="WEBW" title="Web Rate">
                        <pax adult="0" child="1" id="gi4nGMKoN_hHMLEH7-CDPhIVBczHcpCW9-3InF2b4UsM-9zNacTuy0wfxHvTI8fBUgtk-_f5ys4VHhmj_5JYqg||"/>
                        <pax adult="0" child="2" id="MS4nEPN2uDfHm_u6kzyLrFZnKrwHm-8pwNPaLbgeb5pVqXVHcPw||"/>
                        <pax adult="0" child="3" id="dhI77he_2rT9-A567BOxdrr9u7pvAHP5wTUXbXArmOQXHN07eMNOYqKmw||"/>
                        <pax adult="1" child="0" id="ZDbx1E2RyIKgG0FTctC17KY3cZEn1-3lej9zXDMndYueP0pdbBu8-jxodxBVyDU6NsglD8eMSq1YIyvEp_-Sqw||"/>
                        <pax adult="1" child="1" id="z1daNr12csN1V_50LZN3jbxO2eJirXf2EP0yxJM_siTm4BWLxi2MtqjFIDZ_zDA5xefzYEerTgEhwh_gvqKlfQ||"/>
                        <pax adult="1" child="2" id="kRLPCSyK-OBReR6ZHExn-rbRayq5IhpBu1XhLLPkGK_1THNoAn-n9Fcw||"/>
                        <pax adult="2" child="0" id="kjgOoF9cszf5jNCgUfyXtvCTFur86Mzo_RRVin8IrAb_Wd3sKW0WQ_4XHMD4wa2GCvt_lcMvetgIkXzZv3tq7g||"/>
                        <pax adult="2" child="1" id="q4FDD4-K-MJYsSwXbFN8jXEUTDcVVqenEYb34C8qEnWIk_A3OpkFV8Qr8g9RNkcwzzEZcIbfTt7-XtdHBQEXsg||"/>
                        <pax adult="3" child="0" id="posyXY_Fw6ULvfRF6wy5gmfGC310a1pTE5wIz9Q47Yt5_-Zw||"/>
                      </rate>
                      <rate code="WEBS" title="Seasonal Rate">
                        <pax adult="0" child="1" id="gi4nGMKoN_hHMLEH7-CDPhIVBczHcpCW9-3InF2b4UsM-9zNacTuy0wfxHvTI8fBUgtk-_f5ys4VHhmj_5JYqg||"/>
                        <pax adult="0" child="2" id="MS4nEPN2uDfHm_u6kzyLrFZnKrwHm-8pwNPaLbgeb5pVqXVHcPw||"/>
                        <pax adult="0" child="3" id="dhI77he_2rT9-A567BOxdrr9u7pvAHP5wTUXbXArmOQXHN07eMNOYqKmw||"/>
                        <pax adult="1" child="0" id="ZDbx1E2RyIKgG0FTctC17KY3cZEn1-3lej9zXDMndYueP0pdbBu8-jxodxBVyDU6NsglD8eMSq1YIyvEp_-Sqw||"/>
                        <pax adult="1" child="1" id="z1daNr12csN1V_50LZN3jbxO2eJirXf2EP0yxJM_siTm4BWLxi2MtqjFIDZ_zDA5xefzYEerTgEhwh_gvqKlfQ||"/>
                        <pax adult="1" child="2" id="kRLPCSyK-OBReR6ZHExn-rbRayq5IhpBu1XhLLPkGK_1THNoAn-n9Fcw||"/>
                        <pax adult="2" child="0" id="kjgOoF9cszf5jNCgUfyXtvCTFur86Mzo_RRVin8IrAb_Wd3sKW0WQ_4XHMD4wa2GCvt_lcMvetgIkXzZv3tq7g||"/>
                        <pax adult="2" child="1" id="q4FDD4-K-MJYsSwXbFN8jXEUTDcVVqenEYb34C8qEnWIk_A3OpkFV8Qr8g9RNkcwzzEZcIbfTt7-XtdHBQEXsg||"/>
                        <pax adult="3" child="0" id="posyXY_Fw6ULvfRF6wy5gmfGC310a1pTE5wIz9Q47Yt5_-Zw||"/>
                      </rate>
                    </rates>
                  </roomtype>
                </roomtypes>
              </hotel>
            </hotels>
          </response>
          <request method="rateidsforadtocart" key="AmRdd3WyVZN3jIG" languagecode="en">
            <stay checkindate="2016-07-23" checkoutdate="2016-07-24"/>
            <hotels>
              <hotel id="20620"/>
            </hotels>
          </request>
        </hotelogix>
        """
        return pQ(xml)

    @staticmethod
    def build_valid_rate_id_xml():
        xml = """
        <hotelogix version="1.0" datetime="2016-06-24T10:47:12">
          <response languagecode="en">
            <hotels>
              <hotel id="Rdys9RXaEjFZTS2NZQxUDcTtsbB_ENPRZSZCg5E4Wn8|" code="20620" title="White Inn(20620)">
                <status code="1400" message="Success"/>
                <roomtypes>
                  <roomtype code="Oak" title="Oak (Standard)">
                  <rates>
                      <rate code="WEBLM" title="Last Min Rate">
                        <pax adult="0" child="1" id="gi4nGMKoN_hHMLEH7-CDPhIVBczHcpCW9-3InF2b4UtZW6Rb8HwYYnkmrHSp3FzWt0zLgjV4-_W"/>
                        <pax adult="0" child="2" id="MS4nEPN2uDfHm_u6kzyHcYAHWyQSn6-eY9gZ2ZHBS-jqXgiaSSc-q8u4hcp7LQCUlx-sNEna3Dj"/>
                        <pax adult="0" child="3" id="dhI77he_2rT9-A567BOxdrr9uGECbMW-2XXlYOL0tEjhWgCessa4SyBs7XhMeTnDccPKVblzDH8"/>
                        <pax adult="1" child="0" id="ZDbx1E2RyIKgG0FTc2bhN1BC-NDUKJ2DZDSJkSpBlPtVtpw2eglNy-Gra96uo59xFS6ACcYWl6D"/>
                        <pax adult="1" child="1" id="z1daNr12csN1V_50LZN3jbxO2eJirXHQJWVzXEnpZYxghQNsN2Og040RRAC30kbKxkewXrophrr"/>
                        <pax adult="1" child="2" id="kRLPCSyK-OBReR6ZHExn-rbRayq5IhpBW7ZUrObBb7UBcvsy4EMIo8UKl9h-CSBC07mIJiJQiil"/>
                        <pax adult="2" child="0" id="kjgOoF9cszf5jNCgUoefalxfslNUilIAWh1sq6jCgG8yYAejq9zFpN0ksTOmsMmsadfsdf-SPiu"/>
                        <pax adult="2" child="1" id="q4F-K-MJYsSwXbFN8jXEUTDcVVqenEYb3nI7c9nESNt_EZIQb3433aHNKJBnt3CYFn_cTATsKzz"/>
                        <pax adult="3" child="0" id="posyXY_znJg3qdQhi59E3gLnookLLisPtARPgq46SpNqOLKn607u1xc5joDDQNN5k7yGsfspZ7W"/>
                      </rate>
                      <rate code="PKG990802c323100704525254" title="Treebo Promotional">
                        <pax adult="0" child="1" id="zero-one-rate-id"/>
                        <pax adult="1" child="0" id="one-zero-rate-id"/>
                        <pax adult="1" child="1" id="one-one-rate-id"/>
                        <pax adult="1" child="2" id="one-two-rate-id"/>
                        <pax adult="2" child="0" id="two-zero-rate-id"/>
                        <pax adult="2" child="1" id="two-one-rate-id"/>
                        <pax adult="3" child="0" id="three-zero-rate-id"/>
                      </rate>
                    </rates>
                  </roomtype>
                </roomtypes>
              </hotel>
            </hotels>
          </response>
          <request method="rateidsforadtocart" key="AmRdd3WyVZN3jIG" languagecode="en">
            <stay checkindate="2016-07-23" checkoutdate="2016-07-24"/>
            <hotels>
              <hotel id="20620"/>
            </hotels>
          </request>
        </hotelogix>
        """
        return pQ(xml)

    @staticmethod
    def build_empty_cart():
        xml = """
        <?xml version="1.0"?>
        <hotelogix version="1.0" datetime="2016-06-24T15:09:16">
            <response>
            </response>
        </hotelogix>
        """
        return pQ(xml)

    @staticmethod
    def build_cart_xml_with_item():
        xml = """
        <?xml version="1.0"?>
        <hotelogix version="1.0" datetime="2016-06-24T15:09:16">
            <response>
                <status code="1500" message="SUCCESS"/>
                <hotels>
                    <hotel id="0qg8r-GlSwiUUzqkQB08fplO6jDLRCv6JVuTpYDUIVI|" title="Dummy Hotel Elmas(12792)" currencycode="INR" minRate="">
                        <status></status>
                        <bookings>
                            <booking id="e6DsVErhM8GI" checkindate="2016-07-07" checkoutdate="2016-07-10" adult="1" child="0" infant="0" code="0624" groupcode="" hotelid="0qg8r-GlSwiUUzqkQB08fplO6jDLRCv6JVuTpYDUIVI|" hotelname="Dummy Hotel Elmas" currencycode="INR" statuscode="RESERVE">
                            <rates>
                                <rate price="4367.610000" title="Treebo Promotional" tax="729.390870">
                                    <description><![CDATA[Treebo Promotional]]></description>
                                    <cancellationpolicies>
                                    <cancellationpolicy duration="9999" durationtype="D" canceldate="1989-02-20T12:00:00" chargetype="PV" charge="10"/>
                                    </cancellationpolicies>
                                </rate>
                            </rates>
                            </booking>
                        </bookings>
                    </hotel>
                </hotels>
            </response>
        </hotelogix>
        """
        return pQ(xml)

    @staticmethod
    def build_save_booking_response_xml():
        xml = """
        <hotelogix version="1.0" datetime="2016-06-24T00:27:55">
          <response>
            <status code="1600" message="SUCCESS"/>
            <order id="CRS-1307199414" orderdate="2016-06-24T00:27:52">
              <owner>
                <fname>Test</fname>
                <lname/>
                <email>treebotestauto12@gmail.com</email>
                <phone/>
                <mobile>9000000000</mobile>
                <country>India</country>
                <state>Uttar Pradesh</state>
                <address>-</address>
                <city>-</city>
                <zip>-</zip>
              </owner>
              <bookings>
                <booking id="BAGANOe4AoVIyyk|" checkindate="2016-09-07" checkoutdate="2016-09-08" adult="1" child="0" infant="0" code="0624607" groupcode="" hotelid="_ShRK3p_vSrdFZNarGuy-tpD_aKl8qQqUmw4g7sbA24|" hotelname="Sanctum Suites" currencycode="INR" statuscode="RESERVE">
                  <roomtypes>
                    <roomtype id="vyl6T1uok30r7LmVcsLZDyJiWl1qFc9i4WvQEgOb0AM|" title="Oak (Standard)" availableroom="" basepax="1" maxpax="3" minrooms="1" minRate="">
                      <description>Oak (Standard)</description>
                      <imgs/>
                      <amenities>
                        <amenity title="WiFi"/>
                        <amenity title="Complementary Breakfast"/>
                        <amenity title="Electric Kettle"/>
                        <amenity title="TV"/>
                        <amenity title="24 hours Hot water supply"/>
                        <amenity title="AC"/>
                      </amenities>
                      <stay checkindate="2016-09-07" checkoutdate="2016-09-08"/>
                    </roomtype>
                  </roomtypes>
                  <rates>
                    <rate price="2147.930000" title="Treebo Promotional" tax="451.065300">
                      <description>Treebo Promotional</description>
                    </rate>
                  </rates>
                  <gueststays>
                    <gueststay id="AeVjON5g7fM|" guestid="AhVQfwT2gQ||" checkindate="2016-09-07" checkoutdate="2016-09-08" status="RESERVE" type="Adult" isPrimary="1"/>
                  </gueststays>
                  <preference>[Checkin Time:12pm, Checkout Time:11am] </preference>
                  <amount depositamount="0"/>
                  <totalamount totalamount="2598.9953"/>
                </booking>
                <booking id="BAGANOe4AoVIyyk|" checkindate="2016-09-07" checkoutdate="2016-09-08" adult="1" child="0" infant="0" code="0624607" groupcode="" hotelid="_ShRK3p_vSrdFZNarGuy-tpD_aKl8qQqUmw4g7sbA24|" hotelname="Sanctum Suites" currencycode="INR" statuscode="RESERVE">
                  <roomtypes>
                    <roomtype id="vyl6T1uok30r7LmVcsLZDyJiWl1qFc9i4WvQEgOb0AM|" title="Oak (Standard)" availableroom="" basepax="1" maxpax="3" minrooms="1" minRate="">
                      <description>Oak (Standard)</description>
                      <imgs/>
                      <amenities>
                        <amenity title="WiFi"/>
                        <amenity title="Complementary Breakfast"/>
                        <amenity title="Electric Kettle"/>
                        <amenity title="TV"/>
                        <amenity title="24 hours Hot water supply"/>
                        <amenity title="AC"/>
                      </amenities>
                      <stay checkindate="2016-09-07" checkoutdate="2016-09-08"/>
                    </roomtype>
                  </roomtypes>
                  <rates>
                    <rate price="2147.930000" title="Treebo Promotional" tax="451.065300">
                      <description>Treebo Promotional</description>
                    </rate>
                  </rates>
                  <gueststays>
                    <gueststay id="AeVjON5g7fM|" guestid="AhVQfwT2gQ||" checkindate="2016-09-07" checkoutdate="2016-09-08" status="RESERVE" type="Adult" isPrimary="1"/>
                  </gueststays>
                  <preference>[Checkin Time:12pm, Checkout Time:11am] </preference>
                  <amount depositamount="0"/>
                  <totalamount totalamount="2598.9953"/>
                </booking>
              </bookings>
              <orderamount amount="2598.995300"/>
              <deposittotal amount="0"/>
              <paidamount amount="0.000000"/>
            </order>
          </response>
          <request method="savebooking" key="ncXNfdR|NwfPkNh">
            <guest>
                <fname>Test</fname>
                <lname/>
                <email>treebotestauto12@gmail.com</email>
                <phone/>
                <mobile>9000000000</mobile>
                <country code="IN">India</country>
                <state code="UP"/>
                <address>-</address>
                <city>-</city>
                <zip>-</zip>
            </guest>
            <hotels>
                <hotel id="_ShRK3p_vSrdFZNarGuy-tpD_aKl8qQqUmw4g7sbA24|"><preference>[Checkin Time:12pm, Checkout Time:11am] </preference></hotel>
            </hotels>
          </request>
        </hotelogix>
        """
        return pQ(xml)
