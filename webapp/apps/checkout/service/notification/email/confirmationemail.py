# pylint: disable=invalid-name,no-member

import ast
import decimal
import json
import logging
import urllib.error
import urllib.parse
import urllib.request
from datetime import datetime, time
from decimal import Decimal

from django.conf import settings
from django.template import Context
from django.utils.dateformat import DateFormat

from apps.bookings.constants import HOMESTAY_PROVIDER_WT
from apps.bookings.constants import WALLET_GATEWAY_TYPE, WALLET_SUCCESS_STATUS
from apps.bookings.models import Booking
from apps.bookings.models import PaymentOrder
from apps.checkout.service.notification.basenotification import NotificationService
from apps.checkout.service.special_details_service import SpecialDetailsService
from apps.common import date_time_utils
from apps.content.models import ContentStore
from apps.featuregate.manager import FeatureManager
from apps.growth.services.growth_services import GrowthServices
from apps.hotels import utils
from apps.hotels.service.hotel_policy_service import HotelPolicyService
from apps.hotels.service.hotel_service import HotelService
from apps.hotels.service.hotel_sub_brand_service import HotelSubBrandService
from apps.search.constants import INDEPENDENT_HOTEL_BRAND_CODE
from common.constants import common_constants
from common.exceptions.treebo_exception import TreeboException
from common.utilities import number_utils
from data_services.respositories_factory import RepositoriesFactory
from data_services.web_repositories.room_web_repository import RoomWebRepository
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class ConfirmationPriceDto(object):
    actual_total_cost = 0
    discount_amount = 0
    pretax_price = 0
    total_tariff = 0
    total_tax = 0
    wallet_deduction = 0

    def __init__(
        self,
        actual_total_cost,
        discount_amount,
        pretax_price,
        total_tariff,
        total_tax,
        wallet_deduction):
        self.actual_total_cost = actual_total_cost
        self.discount_amount = discount_amount
        self.pretax_price = pretax_price
        self.total_tariff = total_tariff
        self.total_tax = total_tax
        self.wallet_deduction = wallet_deduction


class ConfirmationEmailService(NotificationService):
    template_name = "email/booking/confirmation.html"

    growth_service = GrowthServices()
    special_details_service = SpecialDetailsService()
    hotel_repository = RepositoriesFactory.get_hotel_repository()

    def __init__(
        self,
        notification_type,
        content_subtype,
        to_address,
        subject,
        data):
        super(
            ConfirmationEmailService,
            self).__init__(
            notification_type,
            content_subtype,
            to_address,
            subject,
            data)
        self.__pagename = "confirmation_email"

    def _setData(self, data):
        NotificationService._setData(self, data)

    def createMessage(self):
        NotificationService._createMessage(self)

    def addAttachments(self, pdf_path, pdf_name):
        NotificationService._addAttachments(self, pdf_path, pdf_name)

    def sendMessage(self):
        NotificationService._sendMessage(self)

    def collateEmailData(self, booking_request):
        """
        Collate Email Data for Content
        :param apps.checkout.models.BookingRequest booking_request:
        :return:
        """
        try:
            hotels = self.hotel_repository.filter_hotels(
                related_entities_list=[
                    'locality', 'city'], pk=booking_request.hotel_id)
            if hotels:
                hotel = hotels[0]
            else:
                return
            # hotel = Hotel.objects.select_related('locality', 'city').get(pk=booking_request.hotel_id)
            check_in_time = str(hotel.checkin_time.strftime("%I:%M %p"))
            check_out_time = str(hotel.checkout_time.strftime("%I:%M %p"))
            phone_number = hotel.phone_number

            room_count = len(booking_request.room_config.split(","))
            check_in_day = DateFormat(booking_request.checkin_date).format('l')
            check_out_day = DateFormat(
                booking_request.checkout_date).format('l')

            logger.info("The checkin date for boooking is %s", str(booking_request.checkin_date))
            check_in = date_time_utils.build_date(booking_request.checkin_date)
            logger.info("Got the date from datetime utils for checkin is %s", check_in)
            checkout = date_time_utils.build_date(
                booking_request.checkout_date)

            confirmation_price_dto = self.get_payment_details_for_booking(
                booking_request)

            directions = utils.get_directions_from_landmark(hotel.id)
            size = settings.EMAIL_GOOGLE_MAP_IMAGE_SIZE

            if directions:
                static_map_url = utils.generate_static_map_url(
                    hotel, directions, size)
            else:
                static_map_url = None

            # Get part-pay information
            part_pay_amount, part_pay_link = None, None
            if not hotel.provider_name == HOMESTAY_PROVIDER_WT:
                part_pay_amount, part_pay_link = self.get_part_pay_info_for_booking(
                    booking_request, hotel)

            bookings_data_list = []
            bookings_data = {
                'order_id': booking_request.order_id,
                'hotel_detail': dict(hotel_code=hotel.hotelogix_id, hotel_cs_id=hotel.cs_id)
            }
            bookings_data_list.append(bookings_data)
            cancel_hash_url = self.growth_service.booking_cancel_hash_url(
                bookings_data_list)

            url_1, url_2, url_3, url_4, url_5 = self.growth_service.booking_exp_hash_url(
                booking_request.order_id, hotel.hotelogix_id)
            if booking_request.is_audit:
                actual_total_cost = 0

            special_rates = self.special_details_service.get_special_date_details(
                booking_request)
            policies = HotelPolicyService.get_hotel_policies(hotel.id)

            showcased_image_url = self.hotel_repository.get_showcased_image_url(
                hotel)
            # showcased_image_url = showcased_image_obj.url if showcased_image_obj else ''
            room_type_code = booking_request.room_type
            hotel_id = booking_request.hotel_id

            is_independent_hotel = False
            if INDEPENDENT_HOTEL_BRAND_CODE in HotelSubBrandService.get_brand_list_for_hotel(hotel_id):
                is_independent_hotel = True

            room_dict = RoomWebRepository.get_external_room_details(hotel_id=hotel_id,
                                                                    room_type_code=room_type_code)
            room_type = room_dict['external_room_details__name']
            booking_channel = booking_request.booking_channel
            try:
                contentstore_key = 'pah_confirmation_email_template_' + str(booking_channel)
                email_body_message_object = ContentStore.objects.filter(name=contentstore_key).first()
                email_body_message_dict = (email_body_message_object.value).get(booking_channel, None)
                if email_body_message_dict is None:
                    email_body_message_object = ContentStore.objects.filter(name='pah_default_email_template').first()
                    if booking_channel in ['website', 'gdc']:
                        email_body_message_dict = (email_body_message_object.value).get('website')
            except Exception as err:
                logger.exception("Couldn't Find contentstore key so setting default %s", str(err))
                email_body_message_object = ContentStore.objects.filter(name='pah_default_email_template').first()
                email_body_message_dict = (email_body_message_object.value).get('website')

            prepaid_amount = Decimal(booking_request.payment_amount).quantize(2)
            if prepaid_amount:
                prepaid_amount = prepaid_amount - confirmation_price_dto.wallet_deduction

            data = Context(
                {
                    "booking": booking_request,
                    "checkin": check_in,
                    "checkout": checkout,
                    "checkin_time": check_in_time,
                    "checkout_time": check_out_time,
                    "phone_number": phone_number,
                    "days": (
                        booking_request.checkout_date - booking_request.checkin_date).days,
                    "hotel_name": HotelService().get_post_booking_display_name(hotel),
                    "room_type": room_type,
                    "pretax_price": confirmation_price_dto.pretax_price,
                    "checkout_day": check_out_day,
                    "checkin_day": check_in_day,
                    "goolge_link": common_constants.GOOGLE_MAPS_LINK + str(
                        hotel.latitude) + "," + str(
                        hotel.longitude),
                    "total_tax": confirmation_price_dto.total_tax,
                    "adults": booking_request.adults,
                    "children": booking_request.children,
                    "room_count": room_count,
                    "discount_amount": confirmation_price_dto.discount_amount,
                    "booking_code": booking_request.order_id,
                    "booking_date": date_time_utils.build_date(
                        booking_request.created_at),
                    "total_tariff": confirmation_price_dto.total_tariff +
                                    confirmation_price_dto.discount_amount +
                                    confirmation_price_dto.wallet_deduction,
                    "customer_care_number": common_constants.CUSTOMER_CARE_NUMBER,
                    "image_url": showcased_image_url,
                    "calendar_link": self.__createCalendarEvent(
                        hotel,
                        booking_request.checkin_date,
                        booking_request.checkout_date),
                    'first_name': booking_request.name.split()[0],
                    'pah': 1 if booking_request.payment_mode in [Booking.NOT_PAID, Booking.PARTIAL_RESERVE] else 0,
                    "address": "{0}, {1}, {2}, {3}".format(
                        hotel.street,
                        hotel.locality.name,
                        hotel.city.name,
                        hotel.locality.pincode),
                    'static_map_url': static_map_url,
                    'rate_plan_type': booking_request.rate_plan_meta.get(
                        'tag',
                        'Refundable') if booking_request.rate_plan_meta else "Refundable",
                    "prepaid_amount": prepaid_amount,
                    "balance_amount": confirmation_price_dto.total_tariff - prepaid_amount,
                    "part_pay_link": part_pay_link,
                    "part_pay_amount": part_pay_amount,
                    "booking_channel": str(
                        booking_request.booking_channel).lower(),
                    "cancel_hash_url": cancel_hash_url,
                    "booking_exp_url_1": url_1,
                    "booking_exp_url_2": url_2,
                    "booking_exp_url_3": url_3,
                    "booking_exp_url_4": url_4,
                    "booking_exp_url_5": url_5,
                    "special_rates": special_rates,
                    "wallet_deduction": confirmation_price_dto.wallet_deduction,
                    "hotel_policies": policies,
                    "email_body": email_body_message_dict.format(part_pay_amount, part_pay_link),
                    "is_independent_hotel": is_independent_hotel
                })
            logger.info("data dict for confirmation email %s", data)
            try:
                feature_manager = FeatureManager(
                    self.__pagename,
                    check_in,
                    checkout,
                    hotel.id,
                    confirmation_price_dto.actual_total_cost,
                    hotel.locality.name,
                    hotel.city.id)
                email_banner_key = 'email_bannel_urls'
                overridden_values = feature_manager.get_overriding_values()
                for overriddenKey in list(overridden_values.keys()):
                    email_banner_key = overridden_values[overriddenKey]
                banners = ContentStore.objects.get(
                    name=email_banner_key, version="1").value
                data.update({
                    'banners': json.loads(banners)
                })
            except Exception as e:
                logger.exception(e)

        except Exception as exc:
            logger.exception(exc)
            raise TreeboException(exc)

        self._setData(data)

    def get_payment_details_for_booking(self, booking_request):
        """
        Finds the booking price details
        :param booking_request:
        :return:
        """
        discount_amount = 0
        if number_utils.is_valid_number(booking_request.discount_value):
            discount_amount = booking_request.discount_value

        if booking_request.member_discount:
            discount_amount = discount_amount + booking_request.member_discount

        total_tariff = decimal.Decimal(
            booking_request.pretax_amount).quantize(2) + decimal.Decimal(
            booking_request.tax_amount).quantize(2)
        actual_total_cost = decimal.Decimal(
            booking_request.pretax_amount).quantize(2) + decimal.Decimal(
            booking_request.tax_amount).quantize(2)
        wallet_deduction = 0

        if booking_request.wallet_applied:
            payment_orders = PaymentOrder.objects.filter(booking_order_id=booking_request.order_id)

            for payment_order in payment_orders:
                if payment_order.gateway_type == WALLET_GATEWAY_TYPE and payment_order.status == WALLET_SUCCESS_STATUS:
                    wallet_deduction = decimal.Decimal(
                        booking_request.wallet_deduction).quantize(2)
                    actual_total_cost = total_tariff - wallet_deduction

        pretax_price = booking_request.pretax_amount
        total_tax = booking_request.tax_amount
        return ConfirmationPriceDto(
            actual_total_cost,
            discount_amount,
            pretax_price,
            total_tariff,
            total_tax,
            wallet_deduction)

    def get_part_pay_info_for_booking(self, booking_request, hotel):
        """
        Get the part pay detail for a booking
        :param (BookingRequest) booking_request:
        :param (Hotel) hotel:
        :return:
        """
        part_pay_link, part_pay_amount = None, None
        content_object = ContentStore.objects.filter(
            name="part_pay_enable_channel", version=1).first()
        if content_object:
            content_object_value = content_object.value
            enabled_channel_list = ast.literal_eval(content_object_value)
        if "Not Paid" in booking_request.payment_mode:
            if str(booking_request.booking_channel).lower(
            ) in enabled_channel_list:
                part_pay_bookings_list = []
                part_pay_booking_dict = {
                    'order_id': booking_request.order_id,
                    'status': 'reserved' if booking_request.status.lower() == 'completed' else '',
                    'audit': booking_request.is_audit,
                    'paid_at_hotel': booking_request.pay_at_hotel,
                    'payment_mode': booking_request.payment_mode,
                    'hotel_detail': {
                        "hotel_code": hotel.hotelogix_id,
                        "hotel_cs_id": hotel.cs_id
                    }
                }
                part_pay_bookings_list.append(part_pay_booking_dict)
                part_pay_link, part_pay_amount = self.growth_service.booking_partpay_details(
                    part_pay_bookings_list)
        return part_pay_amount, part_pay_link

    def __get_image_url(self, hotel):
        image_url = None
        try:
            for image in hotel.images:
                if image.is_showcased:
                    image_url = image.url
                    break
        except Exception as exc:
            logger.exception(exc)
        return image_url

    def __createCalendarEvent(self, hotel, checkinDate, checkoutDate):
        GOOGLE_URL = "https://www.google.com/calendar/event?action=TEMPLATE&text=%(text)s&dates=%(start)s/%" \
                     "(end)s&details=%(details)s&location=%(location)s"
        text = urllib.parse.quote_plus("Stay at " + hotel.name)
        startDate = datetime.combine(
            checkinDate, time(
                12, 0, 0, 0)).isoformat()
        endDate = datetime.combine(checkoutDate, time(11, 0, 0, 0)).isoformat()
        details = urllib.parse.quote_plus(
            hotel.name +
            " " +
            hotel.landmark +
            " " +
            hotel.street +
            " " +
            hotel.phone_number)
        location = urllib.parse.quote_plus(hotel.name)

        context = {
            "text": text,
            "start": startDate,
            "end": endDate,
            "details": details,
            "location": location}
        return GOOGLE_URL % context
