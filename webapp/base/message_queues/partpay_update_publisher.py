from django.conf import settings
from kombu import Connection, Exchange, Queue
from kombu.pools import producers

from common.utilities.queue_utils import get_rabbit_mq_heartbeat_interval, get_rabbit_mq_retry_policy, \
    get_rabbit_mq_transport_options

connection = Connection(settings.GROWTH_BROKER_URL,
                        heartbeat=get_rabbit_mq_heartbeat_interval(),
                        transport_options=get_rabbit_mq_transport_options())
model_exchange = Exchange(settings.GROWTH_EXCHANGE_NAME)

def get_routing_key(event_type):
    return str(settings.DOMAIN_EVENT_ROUTING_KEY_PREFIX) + str(event_type)

def publish(message_type, payload):
    """
    :param message_type:
    :param payload:
    """
    routing_key = get_routing_key(message_type)
    with producers[connection].acquire(block=True) as producer:
        producer.publish(payload,
                         exchange=model_exchange,
                         routing_key=routing_key,
                         declare=[model_exchange],
                         serializer='json',
                         retry=True,
                         retry_policy=get_rabbit_mq_retry_policy(),
                         compression='zlib')
