'use strict';

import BaseView from '../treebolibs/baseview';
import logger from '../treebolibs/logger';
import $ from 'jquery';
import _ from 'underscore';
import 'slick-carousel';
import '../../css/fot/landing_fot.less';

let FotLandingView = Object.create(BaseView);

FotLandingView.pageLoaded = () => {
	$('.js-room-carousel').slick();
    logger.log(" Fot Landing page loaded");
}

$("#downArrow").click(function() {
   $('html, body').animate({
       scrollTop: $("#process").offset().top
   }, 700);
});

let fotlandingConfig = { name:'Fot Landing Page', widgets:[]};
let fotlandingView = FotLandingView.setup(fotlandingConfig);
