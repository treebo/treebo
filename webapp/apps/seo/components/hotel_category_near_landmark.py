import logging
from apps.seo.components.base_component import BaseComponent
from django.conf import settings
from django.core.cache import cache
from django.utils.text import slugify
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.services.common_services.sql_query_helper import SqlQueryHelper
from apps.seo.serializers.serializer import HotelTypesSerializer
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class HotelCategoryNearLandmark(BaseComponent):

    def __init__(self, landmark):
        self.landmark = landmark

    def build(self):
        return self.fetch_and_build_hotel_category_cache(self.landmark)

    @staticmethod
    def fetch_and_build_hotel_category_cache(landmark):
        logger.info(
            "category X landmark mapping, landmark_name : %s , city_name: %s " % (landmark.seo_url_name, landmark.city.name))
        data = cache.get(HotelCategoryNearLandmark.get_cache_key(landmark.seo_url_name, landmark.city.name))
        if data:
            return data
        distance_query_str = SqlQueryHelper.get_distance_query_str(landmark.latitude, landmark.longitude)

        if landmark.distance_cap is None:
            landmark.distance_cap = float(settings.DEFAULT_SEO_LANDMARK_RADIUS)

        sql_query = 'select distinct on (dc."name") dc."name" from hotels_hotel hh' \
                    ' inner join hotels_hotel_category hhc on hhc.hotel_id = hh.id' \
                    ' inner join dbcommon_citycategorymapforautopagecreation ccmap' \
                    ' on ccmap.category_id = hhc.categories_id' \
                    ' inner join dbcommon_categories dc on dc.id = ccmap.category_id' \
                    ' where ' + distance_query_str + ' < ' + str(landmark.distance_cap) + ' and' \
                    ' hh.status = 1 and ccmap.is_enabled = true and dc.enable_for_seo = true and' \
                    ' ccmap.city_id = hh.city_id'

        logger.info("landmark fetch_and_build_hotel_category_cache sql query : " + sql_query)

        all_categories = SqlQueryHelper.execute_query_and_map_to_dictionary(sql_query)
        response = {
            'key': 'hotel_types',
            'title': 'HOTEL TYPES NEAR ' + landmark.seo_url_name.upper(),
            'content': AutoCompleteResultBuilder.build_hotel_types_near_landmark_result(all_categories,
                                                                                        landmark)
        }
        serialized_obj = HotelTypesSerializer(data=response)
        if serialized_obj.is_valid():
            response = serialized_obj.validated_data
        else:
            response['content'] = []

        cache.set(HotelCategoryNearLandmark.get_cache_key(landmark.seo_url_name,
                                    landmark.city.name), response, timeout=None)
        logger.info("hotel category response for landmark : " + landmark.seo_url_name)
        logger.info(response)
        return response

    @staticmethod
    def generate_category_mapping_list_to_delete(category):
        all_hotels = Hotel.objects.filter(category__name=category.name)

        category_landmark_mapping_list_to_delete = {}
        for hotel in all_hotels:
            category_landmark_mapping_list_to_delete.update(
                HotelCategoryNearLandmark.generate_hotel_mapping_list_to_delete(hotel))

        return category_landmark_mapping_list_to_delete

    @staticmethod
    def generate_hotel_mapping_list_to_delete(hotel):
        category_landmark_mapping_list_to_delete = {}
        distance_query_str = SqlQueryHelper.get_distance_query_str(hotel.latitude, hotel.longitude)

        sql_query = "select distinct on (sl.seo_url_name) sl.seo_url_name as landmark, " \
                    "hc.name as city from search_landmark sl " \
                    "inner join hotels_city hc on sl.city_id = hc.id where " + distance_query_str \
                    + " < case when sl.distance_cap is null then " \
                    + str(settings.DEFAULT_SEO_LANDMARK_RADIUS) + " else sl.distance_cap end " \
                    #+ "and sl.enable_for_seo = true"

        all_landmark_category_for_hotel = SqlQueryHelper.execute_query_and_map_to_dictionary(sql_query)
        logger.info("landmark generate_hotel_mapping_list_to_delete sql query : " + sql_query)
        logger.info(all_landmark_category_for_hotel)

        for landmark_category_for_hotel in all_landmark_category_for_hotel:
            hash_key = landmark_category_for_hotel["landmark"] + "_" + landmark_category_for_hotel["city"]
            category_landmark_mapping_list_to_delete[hash_key] = landmark_category_for_hotel

        return category_landmark_mapping_list_to_delete

    @staticmethod
    def generate_city_mapping_list_to_delete(city_category_map_instance):
        category_landmark_mapping_list_to_delete = {}
        sql_query = "select distinct on (sl.seo_url_name) sl.seo_url_name as landmark, " \
                    "hc.name as city from search_landmark sl " \
                    "inner join hotels_city hc on sl.city_id = hc.id where " \
                    "sl.city_id = " + str(city_category_map_instance.city.id)

        all_landmark_category_for_hotel = SqlQueryHelper.execute_query_and_map_to_dictionary(sql_query)
        logger.info("landmark generate_city_mapping_list_to_delete sql query : " + sql_query)
        logger.info(all_landmark_category_for_hotel)

        for landmark_category_for_hotel in all_landmark_category_for_hotel:
            hash_key = landmark_category_for_hotel["landmark"] + "_" + landmark_category_for_hotel["city"]
            category_landmark_mapping_list_to_delete[hash_key] = landmark_category_for_hotel

        return category_landmark_mapping_list_to_delete

    @staticmethod
    def get_cache_key(landmark, city):
        return "seo_hotel_category_landmark_" + slugify(landmark)+"_"+slugify(city)

    @staticmethod
    def invalidate_hotel_category_with_landmark_cache(landmark, city):
        from apps.seo.pages_v3.landmark_page import LandmarkPage
        logger.info(
            "invalidating cache landmark_name : %s , city_name: %s " % (landmark, city))
        cache.delete(HotelCategoryNearLandmark.get_cache_key(landmark, city))
        LandmarkPage.delete_page_meta_cache(slugify(landmark), slugify(city))

    @staticmethod
    def delete_all_key_in_cache(category_landmark_mapping_list_to_delete):
        for key, category_landmark_mapping in list(category_landmark_mapping_list_to_delete.items()):
            HotelCategoryNearLandmark.invalidate_hotel_category_with_landmark_cache(category_landmark_mapping["landmark"],
                                                     category_landmark_mapping["city"])

    @staticmethod
    def update_all_key_in_cache(category_landmark_mapping_list_to_delete):
        from dbcommon.models.landmark import SearchLandmark
        for key, category_landmark_mapping in list(category_landmark_mapping_list_to_delete.items()):
            landmark = SearchLandmark.objects.filter(seo_url_name=category_landmark_mapping["landmark"],
                                            city__name=category_landmark_mapping["city"]).first()
            if landmark is not None:
                HotelCategoryNearLandmark.fetch_and_build_hotel_category_cache(landmark)

    @staticmethod
    def update_landmark_key_in_cache(landmark):
        if landmark is not None:
            HotelCategoryNearLandmark.fetch_and_build_hotel_category_cache(landmark)
