import logging
import requests
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from datetime import datetime
from django.conf import settings
from django.core.validators import RegexValidator
from django.http import JsonResponse
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from apps.bookingstash.utils import StashUtils
from apps.pricing.exceptions import SoaError, RequestError
from apps.pricing.services.pricing_service import PricingService
from base.views import validators
from base.views.api import TreeboAPIView
from base.views.api import TreeboValidationMixin
from apps.common.error_codes import get as error_message
from apps.common import error_codes as codes
from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.pricing_api_impl import PriceUtilsV2
from apps.pricing.utils import PriceUtils
from apps.common import date_time_utils
from dbcommon.models.hotel import Hotel
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class PricingSerializer(serializers.Serializer):
    hotel_id = serializers.CharField(
        validators=[
            RegexValidator(
                regex='^(?:\d+,?)+$',
                message='Invalid value for hotel_id parameter. '
                        'Please provide a comma-separated list of hotel_ids',
                code='invalid_hotel_ids'
            )
        ]
    )
    checkin = serializers.DateField()
    checkout = serializers.DateField()
    roomconfig = serializers.CharField(
        max_length=100, validators=[
            validators.validate_roomconfig])


class PricingDetails(TreeboAPIView):
    '''
    Api for returning pricing details
    '''
    validationSerializer = PricingSerializer

    def get(self, request, *args, **kwargs):
        hotel_ids = request.GET.get('hotel_id').split(",")
        checkin = request.GET.get('checkin')
        checkout = request.GET.get('checkout')
        roomconfig = request.GET.get('roomconfig')
        room_type = request.GET.get('room_type')
        coupon_code = request.GET.get('coupon_code', None)
        coupon_value = float(request.GET.get('coupon_value', 0))
        coupon_type = request.GET.get('coupon_type', None)

        # TODO: Move SOA enable or disable logic here
        try:
            data = self.get_from_soa(
                hotel_ids,
                checkin,
                checkout,
                roomconfig,
                room_type,
                coupon_code,
                coupon_value,
                coupon_type)
        except RequestError as e:
            return JsonResponse(
                {"error": {"code": e.status, "msg": str(e)}}, status=HTTP_400_BAD_REQUEST)
        except SoaError as e:
            return JsonResponse(
                {"error": {"code": e.code, "msg": e.msg}}, status=e.status)

        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        dev_msg="WEB Pricing V2", message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            return JsonResponse(
                {
                    "error": {
                        "code": HTTP_500_INTERNAL_SERVER_ERROR,
                        "msg": str(e)}},
                status=HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(data)

    def get_from_soa(
            self,
            hotel_id_list,
            checkin,
            checkout,
            roomconfig,
            room_type,
            coupon_code=None,
            coupon_value=None,
            coupon_type=None):

        prices = PricingService.get_price_from_soa(
            checkin=checkin,
            checkout=checkout,
            hotel_ids=hotel_id_list,
            room_config=roomconfig,
            coupon_code=coupon_code,
            coupon_value=coupon_value,
            coupon_type=coupon_type,
            include_price_breakup=True,
            get_from_cache=False)

        data = [(hotel["rooms"], hotel["hotel_id"])
                for hotel in prices["data"]["hotels"]]
        room_price_details = {}
        # TODO: Right now returning only for specific room type
        # TODO: Implement it for more generic way to return for different input
        # params
        result_dict = {}
        for data_element in data:
            hotel_wise_pricing_result = {}
            hotel_rooms_pricing_details = data_element[0]
            hotel_id = data_element[1]
            if room_type:
                hotel_rooms_pricing_details = list(
                    [d for d in hotel_rooms_pricing_details if d['room_type'].lower() == room_type.lower()])
            for ind_hotel_room_price_detail in hotel_rooms_pricing_details:
                room_config_wise_prices = ind_hotel_room_price_detail.get(
                    'room_config_wise_prices', [])
                date_wise_price_details = {}
                for config_wise_price_details in room_config_wise_prices:
                    date_wise_prices = config_wise_price_details['date_wise_prices']
                    for date_wise_price in date_wise_prices:
                        room_config_date_wise_price = date_wise_price['price']
                        night_date = date_wise_price['date']
                        if date_wise_price_details.get(night_date):
                            date_wise_price_details[night_date]['pretax_price'] += room_config_date_wise_price['autopromo_price_without_tax']
                            date_wise_price_details[night_date]['tax'] += room_config_date_wise_price['tax']
                            date_wise_price_details[night_date]['sell_price'] += room_config_date_wise_price['sell_price']
                            date_wise_price_details[night_date]['rack_rate'] += room_config_date_wise_price['rack_rate']
                            date_wise_price_details[night_date]['base_price'] += room_config_date_wise_price['base_price']
                            date_wise_price_details[night_date]['discount'] += room_config_date_wise_price['coupon_discount']
                        else:
                            date_wise_price_details[night_date] = {
                                'pretax_price': room_config_date_wise_price['autopromo_price_without_tax'],
                                'tax': room_config_date_wise_price['tax'],
                                'sell_price': room_config_date_wise_price['sell_price'],
                                'rack_rate': room_config_date_wise_price['rack_rate'],
                                'base_price': room_config_date_wise_price['base_price'],
                                'discount': room_config_date_wise_price['coupon_discount']}

                room_price_details = ind_hotel_room_price_detail['price']
                hotel_wise_pricing_result[ind_hotel_room_price_detail['room_type']] = {
                    'breakup': date_wise_price_details,
                    'total_price_details': room_price_details
                }
            result_dict[hotel_id] = hotel_wise_pricing_result
        return result_dict
