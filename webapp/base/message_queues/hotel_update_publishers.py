import logging

from django.conf import settings
from kombu import Connection, Exchange
from kombu.pools import producers

from common.utilities.queue_utils import get_rabbit_mq_heartbeat_interval, get_rabbit_mq_retry_policy, \
    get_rabbit_mq_transport_options

logger = logging.getLogger(__name__)
connection = Connection(settings.PRICING_BROKER_URL,
                        heartbeat=get_rabbit_mq_heartbeat_interval(),
                        transport_options=get_rabbit_mq_transport_options())
model_exchange = Exchange("hotel_sync_exchange", type="topic")

routing_keys = {
    'hotel': 'hotel_sync',
}


def publish(payload):
    """
    :param payload:
    """
    try:
        logger.debug("Publishing hotel")
        with producers[connection].acquire(block=True) as producer:
            producer.publish(payload,
                             exchange=model_exchange,
                             routing_key='hotel_sync',
                             declare=[model_exchange],
                             serializer='json',
                             compression='zlib',
                             retry=True,
                             retry_policy=get_rabbit_mq_retry_policy()
                             )
    except Exception as e:
        logger.exception(e)
