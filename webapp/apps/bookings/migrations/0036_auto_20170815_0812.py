# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0035_auto_20170729_0015'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking', name='rate_plan', field=models.CharField(
                max_length=50, null=True, blank=True), ), migrations.AlterField(
            model_name='booking', name='channel', field=models.CharField(
                default='', max_length=50, blank=True, choices=[
                        ('msite', 'msite'), ('website', 'website'), ('app', 'app'), ('gdc', 'gdc')]), ), ]
