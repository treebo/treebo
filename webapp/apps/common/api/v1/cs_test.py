import logging

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from base import log_args
from base.views.api import TreeboAPIView
from data_services.soa_clients.cataloguing_service_client import CataloguingServiceClient

logger = logging.getLogger(__name__)


class CSTest(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CSTest, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request):
        csc = CataloguingServiceClient()
        property_id = request.GET['p_id']
        p = csc.get_property(property_id)

    @log_args(logger)
    def post(self, request, format=None):
        raise Exception('Method not supported')
