from django.utils import dateformat
from rest_framework import serializers

from apps.bookings.models import Booking
from apps.bookingstash.models import ReservationBooking
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from apps.checkout.models import BookingRequest


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        return data


class BookingCreationSerializer(serializers.ModelSerializer):
    rate_plan_meta = JSONSerializerField(required=False, allow_null=True)

    class Meta:
        model = Booking
        fields = '__all__'

    def update(self, instance, validated_data):
        validated_data.pop('booking_status', None)
        validated_data.pop('adult_count', None)
        validated_data.pop('child_count', None)
        validated_data.pop('checkin_date', None)
        validated_data.pop('checkout_date', None)
        validated_data.pop('channel', None)
        validated_data.pop('order_id', None)
        validated_data.pop('hotel', None)
        validated_data.pop('room', None)
        validated_data.pop('is_audit', None)
        validated_data.pop('room_config', None)

        for (key, value) in list(validated_data.items()):
            setattr(instance, key, value)

        instance.save(update_fields=list(validated_data.keys()))

        return instance


class BookingSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField('get_status_string')
    booking_date = serializers.SerializerMethodField()
    cancel_date = serializers.SerializerMethodField()
    checkin_time = serializers.SerializerMethodField()
    checkout_time = serializers.SerializerMethodField()
    group_code = serializers.SerializerMethodField()
    booking_code = serializers.SerializerMethodField()
    user_detail = serializers.SerializerMethodField()
    hotel_detail = serializers.SerializerMethodField()
    paid_at_hotel = serializers.SerializerMethodField()
    payment_mode = serializers.SerializerMethodField()
    payment_detail = serializers.SerializerMethodField()
    room_config = serializers.SerializerMethodField()
    hotel_name = serializers.SerializerMethodField()
    audit = serializers.SerializerMethodField()
    room_config_string = serializers.SerializerMethodField()
    total_number_of_nights = serializers.SerializerMethodField()
    rate_plan_meta = JSONSerializerField(required=False, allow_null=True)
    wallet_applied = serializers.BooleanField(required=False, default=False)
    wallet_deduction = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Booking
        fields = (
            'status',
            'booking_date',
            'cancel_date',
            'checkin_time',
            'checkout_time',
            'group_code',
            'booking_code',
            'user_detail',
            'hotel_detail',
            'paid_at_hotel',
            'payment_mode',
            'payment_detail',
            'room_config',
            'hotel_name',
            'checkin_date',
            'checkout_date',
            'order_id',
            'audit',
            'room_config_string',
            'total_number_of_nights',
            'id',
            'rate_plan',
            'rate_plan_meta',
            'wallet_applied',
            'wallet_deduction')

    def get_status_string(self, obj):
        return obj.booking_status

    def get_booking_date(self, obj):
        return obj.created_at.strftime('%Y-%m-%d')

    def get_cancel_date(self, obj):
        if obj.cancellation_datetime:
            return obj.cancellation_datetime.strftime('%Y-%m-%d %H:%M:%S')
        else:
            return obj.modified_at.strftime('%Y-%m-%d %H:%M:%S')

    def get_checkin_time(self, obj):
        try:
            time_tuple = obj.comments.split(',')
            return str(time_tuple[0].split(':')[1])
        except Exception as exc:
            return ""

    def get_checkout_time(self, obj):
        try:
            time_tuple = obj.comments.split(',')
            checkout_time = str(time_tuple[1].split(':')[1])
            checkout_time = checkout_time[0:4]
            return checkout_time
        except Exception as exc:
            return ""

    def get_total_number_of_nights(self, obj):
        return (obj.checkout_date - obj.checkin_date).days

    def get_group_code(self, obj):
        if not obj.group_code:
            return ""
        return str(obj.group_code)

    def get_booking_code(self, obj):
        if not obj.booking_code:
            return ""
        return str(obj.booking_code)

    def get_user_detail(self, obj):
        gst_details = {
            "gstin": obj.organization_taxcode,
            "organization_name": obj.organization_name,
            "organization_address": obj.organization_address
        }

        user_detail = {
            "name": obj.guest_name,
            "email": obj.guest_email,
            "country_code": obj.guest_country_code,
            "contact_number": obj.guest_mobile,
            "gst_details": gst_details
        }
        return user_detail

    def get_hotel_detail(self, obj):
        hotel_detail = {
            "state": str(obj.hotel.state.name) if obj.hotel and obj.hotel.state else "",
            "address": str(obj.hotel.address()) if obj.hotel and obj.hotel.address() else "",
            "city": str(obj.hotel.get_city_name()) if obj.hotel and obj.hotel.get_city_name() else "",
            "locality": str(obj.hotel.locality.name) if obj.hotel and obj.hotel.locality else "",
            'hotel_code': str(obj.hotel.hotelogix_id) if obj.hotel and obj.hotel.hotelogix_id else "",
            'hotel_cs_id': str(obj.hotel.cs_id) if obj.hotel and obj.hotel.cs_id else "",
            'hotel_id': obj.hotel.id if obj.hotel and obj.hotel.id else "",
            'contact_number': obj.hotel.phone_number if obj.hotel and obj.hotel.phone_number else "",
            'hotel_name': str(obj.hotel.name) if obj.hotel and obj.hotel.name else "",
            'longitude': str(obj.hotel.longitude) if obj.hotel and obj.hotel.longitude else "",
            'latitude': str(obj.hotel.latitude) if obj.hotel and obj.hotel.latitude else "",
            'images': [str(get_image_uncode_url(image.url.url)) for image in obj.hotel.image_set.all()[:5]],
            'showcased_image_url': str(get_image_uncode_url(obj.hotel.get_showcased_image_url().url))
            if obj.hotel and obj.hotel.get_showcased_image_url() and obj.hotel.get_showcased_image_url().url else "",
            'property': {
                'provider': obj.hotel.provider_name,
                'type': obj.hotel.property_type
            }
        }
        return hotel_detail

    def get_paid_at_hotel(self, obj):
        return obj.payment_mode in [Booking.NOT_PAID, Booking.RAZORPAY_PARTPAY, Booking.PARTIAL_RESERVE]

    def get_payment_mode(self, obj):
        return obj.payment_mode

    def get_payment_detail(self, obj):
        coupon_code = str(obj.coupon_code) if obj.coupon_code else ''
        voucher_amount = obj.voucher_amount if obj.voucher_amount else 0
        total_amount = obj.total_amount if obj.total_amount else 0
        booking_request = BookingRequest.objects.filter(order_id=obj.order_id).first()
        paid_amount = round(booking_request.payment_amount, 2)
        payment_detail = {
            "room_price": str(round(obj.pretax_amount, 2)),
            "tax": str(round(obj.tax_amount, 2)),
            "total": str(round(obj.total_amount, 2)),
            "external_pg_share": str(round(total_amount - obj.wallet_deduction, 2)),
            "paid_amount": str(paid_amount),
            "discount": str(round(obj.discount, 2)),
            "voucher_amount": str(round(voucher_amount, 2)),
            "member_discount": str(round(obj.member_discount, 2)) if obj.member_discount else 0,
            "coupon_code": coupon_code,
            "cancellation_charges": '',
            "rate_plan": obj.rate_plan,
            "pending_amount": str(round(obj.total_amount - paid_amount, 2)),
            "wallet_applied": bool(obj.wallet_applied),
            "wallet_deduction": str(round(obj.wallet_deduction, 2)),
            "wallet_payment_amount": obj.get_wallet_payment_amount()
        }
        return payment_detail

    def get_room_config(self, obj):
        roomType = obj.room.room_type_code if obj.room else ""
        roomCount = len(obj.room_config.split(",")) if obj.room_config else 0

        room_config = {
            "room_count": roomCount, "no_of_adults": obj.adult_count,
            "no_of_childs": obj.child_count, "room_type": roomType,
        }
        return room_config

    def get_hotel_name(self, obj):
        return obj.hotel.name if obj.hotel else ""

    def get_audit(self, obj):
        return obj.is_audit

    def get_room_config_string(self, obj):
        roomconfig = obj.room_config if obj.room_config else ''
        return roomconfig

    def get_wallet_deduction(self, obj):
        return str(round(obj.wallet_deduction, 2))

    def get_wallet_applied(self, obj):
        return bool(obj.wallet_deduction)


class BookingConfirmSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField('get_status_string')
    booking_date = serializers.SerializerMethodField()
    cancel_date = serializers.SerializerMethodField()
    user_detail = serializers.SerializerMethodField()
    hotel = serializers.SerializerMethodField()
    paid_at_hotel = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    room = serializers.SerializerMethodField()
    time = serializers.SerializerMethodField()
    guest = serializers.SerializerMethodField()

    class Meta:
        model = Booking
        fields = (
            'status',
            'booking_date',
            'cancel_date',
            'user_detail',
            'hotel',
            'paid_at_hotel',
            'price',
            'room',
            'time',
            'guest',
            'order_id',
            'id')

    def get_status_string(self, obj):
        return obj.booking_status

    def get_booking_date(self, obj):
        return obj.created_at.strftime('%Y-%m-%d')

    def get_cancel_date(self, obj):
        return obj.cancellation_datetime

    def get_checkin_time(self, obj):
        try:
            time_tuple = obj.comments.split(',')
            return str(time_tuple[0].split(':')[1])
        except Exception as exc:
            return ""

    def get_checkout_time(self, obj):
        try:
            time_tuple = obj.comments.split(',')
            checkout_time = str(time_tuple[1].split(':')[1])
            checkout_time = checkout_time[0:4]
            return checkout_time
        except Exception as exc:
            return ""

    def get_group_code(self, obj):
        if not obj.booking_code:
            return ""
        bookingCodes = obj.booking_code.replace('|', ',')
        bookingCodes = bookingCodes[:len(bookingCodes) - 1]
        bookingCode = bookingCodes.split(',')[0]
        try:
            reservationBookingObj = ReservationBooking.objects.get(
                booking_id=bookingCode)
            groupId = reservationBookingObj.group_code
        except Exception as exc:
            groupId = bookingCodes
        return groupId

    def get_user_detail(self, obj):
        user_detail = {
            "name": obj.guest_name,
            "email": obj.guest_email,
            "contact_number": obj.guest_mobile
        }
        return user_detail

    def get_hotel(self, obj):
        hotel_detail = {
            "city": obj.hotel.city.name if obj.hotel and obj.hotel.city else "",
            "name": obj.hotel.name if obj.hotel and obj.hotel.name else "",
            "locality": obj.hotel.locality.name if obj.hotel and obj.hotel.locality else "",
            "street": obj.hotel.street if obj.hotel and obj.hotel.street else "",
            "image_url": get_image_uncode_url(obj.hotel.get_showcased_image_url().url) if obj.hotel and
            obj.hotel.get_showcased_image_url() and obj.hotel.get_showcased_image_url().url else "",
            "id": obj.hotel.id if obj.hotel and obj.hotel.id else ""
        }
        return hotel_detail

    def get_paid_at_hotel(self, obj):
        return (obj.payment_mode ==
                'Not Paid' or obj.payment_mode == 'RAZORPAY_PARTPAY')

    def get_price(self, obj):
        total_amount = obj.total_amount
        total_discount = round(obj.discount, 2)
        flag = 1
        if bool(obj.pretax_amount) is False or bool(obj.tax_amount) is False:
            flag = 0

        price = {}
        if flag == 1:
            price = {"pretax_price": (round(obj.pretax_amount, 2)),
                     "total_tax": (round(obj.tax_amount, 2)),
                     "actual_total_price": (round(total_amount, 2)),
                     "discount_amount": (round(total_discount, 2)),
                     "mmpromo": obj.mm_promo,
                     "voucher_amount": obj.voucher_amount
                     }
        else:
            price = {"pretax_price": 0,
                     "total_tax": 0,
                     "actual_total_price": (round(total_amount, 2)),
                     "discount_amount": (round(total_discount, 2)),
                     "mmpromo": obj.mm_promo,
                     "voucher_amount": obj.voucher_amount
                     }
        return price

    def get_room(self, obj):
        type = obj.room.room_type_code if obj.room else ""
        if obj.room_config:
            room_config = obj.room_config.split(",")
        else:
            room_config = []
        count = len(room_config)
        config = []
        for rm in room_config:
            current_room = rm.split('-')
            config.append(
                {"adults": int(current_room[0]), "children": int(current_room[1])})

        rooms_detail = {
            "count": count, "type": type, "config": config
        }
        return rooms_detail

    def get_audit(self, obj):
        return obj.is_audit

    def get_guest(self, obj):
        children = obj.child_count if obj and obj.child_count else 0
        adults = obj.adult_count if obj and obj.adult_count else 0

        guest = {"children": children,
                 "adults": adults
                 }
        return guest

    def get_time(self, obj):
        checkin_date = obj.checkin_date if obj and obj.checkin_date else ""
        checkout_date = obj.checkout_date if obj and obj.checkout_date else ""
        days = (checkout_date - checkin_date).days
        check_in = dateformat.format(checkin_date, "jS M'y")
        check_out = dateformat.format(checkout_date, "jS M'y")

        time = {"checkin_date": checkin_date,
                "check_in": check_in,
                "checkout_date": checkout_date,
                "checkout": check_out,
                "days": days
                }

        return time
