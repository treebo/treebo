import json
import logging

from django.db import transaction

from apps.bookings.models import Booking
from apps.common.exceptions.booking_exceptions import BookingNotFoundException
from apps.common.exceptions.custom_exception import EmptyPayloadInCRSMigrationEventException, \
    BookingUpdateFailedInCRSIntegrationException
from base.db_decorator import db_retry_decorator

logger = logging.getLogger(__name__)


class IntegrationEventMessageProcessor:

    def process(self, payload):
        if not payload:
            logger.error("CRS Migration :: Empty payload for integration event")
            raise EmptyPayloadInCRSMigrationEventException("Empty payload for integration event")

        message = payload
        hotel_id = message['hotel_id']
        bookings = message['bookings']

        for booking in bookings:
            booking_order_id = booking['reference_number']
            logger.info("CRS Migration :: Processing booking for booking_order_id {0}".format(booking_order_id))


            try:
                crs_booking_id = booking['crs_booking_id']
                crs_bill_id = booking.get('crs_bill_id')
                self.update_booking_in_db(booking_order_id, crs_booking_id, crs_bill_id)

                logger.info("CRS Migration :: Booking updated for order_id {0}, external_crs_booking_id {1} "
                            "and hotel_id {2}".format(booking_order_id, crs_booking_id, hotel_id))
            except BookingNotFoundException as e:
                logger.error("Booking not found for order_id {0} and hotel_id {1}"
                             .format(booking_order_id, hotel_id), exc_info=e)
                # continue processing next booking in payload
            except Exception as e:
                logger.error("CRS Migration :: Updating booking failed for order_id {0}, external_crs_booking_id {1} "
                             "and hotel_id {2}".format(booking_order_id, crs_booking_id, hotel_id), exc_info=e)
                raise BookingUpdateFailedInCRSIntegrationException(str(e))

        logger.info("CRS Migration :: Finished processing for integration event")

    @transaction.atomic
    @db_retry_decorator()
    def update_booking_in_db(self, booking_order_id, crs_booking_id, crs_bill_id):
        try:
            booking = Booking.objects.select_for_update().get(order_id=booking_order_id)
        except Booking.DoesNotExist:
            raise BookingNotFoundException('Booking not found for order_id {0}'.format(booking_order_id))

        booking.external_crs_booking_id = crs_booking_id
        booking.external_crs_booking_version = 1
        extra_info = {'crs_booking_bill_id': str(crs_bill_id)}
        booking.external_crs_booking_extra_info = json.dumps(extra_info)
        booking.save()
