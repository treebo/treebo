from django.dispatch import receiver
from django.db.models.signals import pre_save, post_delete, post_save

from apps.pricing import event_publishers
from apps.pricing.models import LuxuryTax, StateTax


@receiver([post_save, post_delete], sender=StateTax)
@receiver([post_save, post_delete], sender=LuxuryTax)
def on_tax_update(sender, instance, created, *args, **kwargs):
    state = instance.state
    state_taxes = StateTax.objects.filter(state=state)
    luxury_taxes = LuxuryTax.objects.filter(state=state)
    event_publishers.publish_taxes(state.id, state_taxes, luxury_taxes)

