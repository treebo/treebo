from django.db import models


class Configuration(models.Model):
    enable_app = models.BooleanField(
        default=False,
        null=False,
        help_text="Enable intelligent PAH app")
    white_list = models.TextField(
        blank=True,
        null=True,
        default='',
        help_text="included hotel ids")
    enable_for_all_hotels = models.BooleanField(default=False, null=False)
    black_list = models.TextField(
        blank=True,
        null=True,
        default='',
        help_text="excluded hotel ids")
    enable_auditing = models.BooleanField(default=False, null=False)
