'use strict';

import BaseView from '../treebolibs/baseview';
import '../../css/fot/search_audit_fot.less';

let SearchResultView = Object.create(BaseView);

SearchResultView.pageLoaded = () => {

}

let resultConfig = { name:'', widgets:[]};
let searchResultView = SearchResultView.setup(resultConfig);
