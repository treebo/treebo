import datetime
from unittest import TestCase

from apps.pricing.dateutils import date_range, ymd_str_to_date


# import mock

class TestDaterange(TestCase):
    def test_date_range_at_month_end(self):
        today = datetime.date(day=31, month=5, year=2016)
        third_day = datetime.date(day=2, month=6, year=2016)
        list_of_dates = list(date_range(today, third_day))
        expected_list = [datetime.date(day=31, month=5, year=2016),
                         datetime.date(day=1, month=6, year=2016), ]
        self.assertTrue(len(list_of_dates) > 0)
        self.assertItemsEqual(expected_list, list_of_dates)

    def test_date_range_at_year_end(self):
        today = datetime.date(day=31, month=12, year=2016)
        third_day = datetime.date(day=3, month=1, year=2017)
        list_of_dates = list(date_range(today, third_day))
        expected_list = [datetime.date(day=31, month=12, year=2016),
                         datetime.date(day=1, month=1, year=2017),
                         datetime.date(day=2, month=1, year=2017)]
        self.assertTrue(len(list_of_dates) > 0)
        self.assertItemsEqual(expected_list, list_of_dates)

    def test_date_range_at_leap_year_feb(self):
        today = datetime.date(day=28, month=2, year=2016)
        third_day = datetime.date(day=3, month=3, year=2016)
        list_of_dates = list(date_range(today, third_day))
        expected_list = [datetime.date(day=28, month=2, year=2016),
                         datetime.date(day=29, month=2, year=2016),
                         datetime.date(day=1, month=3, year=2016),
                         datetime.date(day=2, month=3, year=2016)]
        self.assertTrue(len(list_of_dates) > 0)
        self.assertItemsEqual(expected_list, list_of_dates)

    def test_dict_sum_values(self):
        prices = {
            1: 1.5,
            2: 1.7,
            3: 1.1
        }
        self.assertEqual(float(1.5) + float(1.7) +
                         float(1.1), sum(prices.values()))
        mixed_prices = {
            1: 1,
            2: 2,
            3: 3.1
        }
        self.assertEqual(float(1) + float(2) + float(3.1),
                         sum(mixed_prices.values()))


class TestUtils(TestCase):
    def test_dict_comprehension(self):
        input_list = ['hello', 'world', 'hello', 'india']
        grouped_dict = {word: len(word) for word in input_list}

    def test_find_first_matching_element(self):
        input_list = ['hello', 'world', 'hello', 'india']
        first_hello = next(x for x in input_list if x == 'india')


class TestUpdateRatePlanFromChannelManager(TestCase):
    def test_to_date(self):
        self.assertEqual(
            datetime.datetime(
                2016,
                0o1,
                0o1).date(),
            ymd_str_to_date('2016-01-01'))

        # def test_updateRatePlanFromChannelManager(self):
        #     rate_plan = parse_rate_plan_update_request('BAR00001')
        #     self.assertEqual(rate_plan.plan_code, 'BAR00001')
        #     self.assertEqual(rate_plan.hotel_id, '3352')
        #     self.assertEqual(rate_plan.start_date, datetime.datetime(2016, 05, 01))
        #     self.assertEqual(rate_plan.end_date, datetime.datetime(2017, 05, 31))
        #     self.assertTrue(rate_plan.rates)
        #     rates = rate_plan.rates


sampleXml = '''<?xml version="1.0" encoding="UTF-8"?>
<OTA_HotelRatePlanNotifRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchemainstance" EchoToken="1e7dcee3730fb542fe5ad10460c3" TimeStamp="2013-05-14T11:11:38.05" Version="1" MessageContentCode="8">
	<RatePlans HotelCode="3352">
		<!--Always use Overlay in RateplanNotifyType-->
		<RatePlan RatePlanNotifType="Overlay" RatePlanCode="BAR00001" RatePlanCategory="Bar" Start="2016-05-01" End="2017-05-31" CurrencyCode="INR">
			<!--DestinationSystemsCode are at RatePlan level. If DestinationSystemsCode are not present then updates will be pushed to all the channels -->
			<DestinationSystemsCode>
				<DestinationSystemCode>1</DestinationSystemCode>
			</DestinationSystemsCode>
			<Rates>
				<!--Hotelogix can always pass EP as mealplan code-->
				<Rate InvTypeCode="DLX" MealPlanCode="EP" Sun="1" Mon="1" Tue="1" Wed="1" Thu="1" Fri="1" Sat="1">
				<BaseByGuestAmts>
				<BaseByGuestAmt AgeQualifyingCode="12" NumberOfGuests="1" Amount="1000.00"/>
				<!--Hotelogix can choose to pass Numberofguests='2'.Its an optional field which is needed for double occupancy-->
				<BaseByGuestAmt AgeQualifyingCode="12" NumberOfGuests="2" Amount="1700.00"/>
				</BaseByGuestAmts>
				<!--Hotelogix can choose to pass additional guestamounts. Its an optional field-->
               <AdditionalGuestAmounts>
               <AdditionalGuestAmount AgeQualifyingCode="10" Amount="500.00"/>
               <AdditionalGuestAmount AgeQualifyingCode="8" Amount="500.00"/>
               </AdditionalGuestAmounts>
               <TPA_Extensions><RateDescription>Description</RateDescription><SeasonName>abcd</SeasonName></TPA_Extensions>
            </Rate>
				<!--Hotelogix can always pass EP as mealplan code-->
				<Rate InvTypeCode="CLB" MealPlanCode="EP" Sun="1" Mon="1" Tue="1" Wed="1" Thu="1" Fri="1" Sat="1">
					<BaseByGuestAmts>
						<BaseByGuestAmt AgeQualifyingCode="10" NumberOfGuests="1" Amount="4000.00"/>
						<BaseByGuestAmt AgeQualifyingCode="10" NumberOfGuests="2" Amount="6000.00"/>
					</BaseByGuestAmts>
					<!--Hotelogix can choose to pass additional guestamounts. Its an optional field-->
					<AdditionalGuestAmounts>
						<AdditionalGuestAmount AgeQualifyingCode="10" Amount="500.00"/>
						<AdditionalGuestAmount AgeQualifyingCode="8" Amount="500.00"/>
					</AdditionalGuestAmounts>
					<TPA_Extensions>
						<RateDescription>Description</RateDescription>
						<SeasonName>abcd</SeasonName>
					</TPA_Extensions>
				</Rate>
			</Rates>
			<TPA_Extensions>
				<Description Name="Short description">
					<Text>Special Rare January 2006</Text>
				</Description>
			</TPA_Extensions>
		</RatePlan>
		<RatePlan RatePlanNotifType="Overlay" RatePlanCode="BAR00002" RatePlanCategory="Bar" Start="2016-05-01" End="2018-05-31" CurrencyCode="INR">
			<Rates>
				<Rate InvTypeCode="SUP" MealPlanCode="EP" Sun="1" Mon="1" Tue="1" Wed="1" Thu="1" Fri="1" Sat="1">
					<BaseByGuestAmts>
						<BaseByGuestAmt AgeQualifyingCode="10" NumberOfGuests="1" Amount="4000.00"/>
						<!--Hotelogix can choose to pass Numberofguests='2'.
Its an optional field which is needed for double occupancy-->
						<BaseByGuestAmt AgeQualifyingCode="10" NumberOfGuests="2" Amount="6000.00"/>
					</BaseByGuestAmts>
					<TPA_Extensions>
						<RateDescription>Room By The Sea</RateDescription>
					</TPA_Extensions>
				</Rate>
			</Rates>
		</RatePlan>
	</RatePlans>
</OTA_HotelRatePlanNotifRQ>'''
