from apps.posts.api import PostQuestionAPI

__author__ = 'ansilkareem'

from django.conf.urls import url

from webapp.apps.posts.api import FaqsApi

app_name = 'posts'
urlpatterns = [
    url(r"^add/question/", PostQuestionAPI.as_view(), name="post_question"),
    url(r"^faqs/", FaqsApi.as_view(), name="get_faqs")
]
