from rest_framework import serializers


class WalletSingleStatementDTO(serializers.Serializer):
    type_choices = ['Cr', 'Dr', 'Rf']
    status_choices = ['temp', 'active', 'expired']
    particular = serializers.CharField(max_length=200)
    amount = serializers.DecimalField(max_digits=25, decimal_places=2)
    type = serializers.ChoiceField(choices=type_choices)
    is_perishable = serializers.BooleanField()
    expires_on = serializers.IntegerField(allow_null=True)
    created_on = serializers.IntegerField()
    status = serializers.ChoiceField(choices=status_choices)
    custom_field_one = serializers.CharField(
        max_length=200,
        allow_null=True,
        allow_blank=True,
        required=False)
    custom_field_two = serializers.CharField(
        max_length=200,
        allow_null=True,
        allow_blank=True,
        required=False)


class WalletStatementDTO(serializers.Serializer):
    type_choices = ['Cr', 'Dr', 'Rf']
    status_choices = ['temp', 'active', 'expired']
    particular = serializers.CharField(max_length=200)
    amount = serializers.DecimalField(max_digits=25, decimal_places=2)
    type = serializers.ChoiceField(choices=type_choices)
    is_perishable = serializers.BooleanField()
    expires_on = serializers.IntegerField(allow_null=True)
    created_on = serializers.IntegerField()
    status = serializers.ChoiceField(choices=status_choices)
    custom_field_one = serializers.CharField(
        max_length=200,
        allow_null=True,
        allow_blank=True,
        required=False)
    custom_field_two = serializers.CharField(
        max_length=200,
        allow_null=True,
        allow_blank=True,
        required=False)
    related_credits = WalletSingleStatementDTO(many=True)
