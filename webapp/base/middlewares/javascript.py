import datetime
import json
import logging
import os.path

from apps.common.utils import get_utm_params_from_request, get_user_identifier
from django.conf import settings
from django.core.urlresolvers import resolve
from ipware.ip import get_ip

from apps.common.utils import AnalyticsEventTracker
from webapp.common.services.feature_toggle_api import FeatureToggleAPI
from webapp.analytics_tracker import config as constants

logger = logging.getLogger(__name__)

env = getattr(settings, "STATIC_URL", None)


class BaseJsObjMiddleware(object):
    # Adds the global 'rt' object to request
    def process_request(self, request):
        # request.isReferralEnabled = FeatureToggleAPI.is_enabled("referral", "isReferralEnabled",
        #                                                         False)
        request.isReferralEnabled = False
        request.baseJsObj = json.dumps(self.get_base_jobj(request))


    def load_analytics_events_config(self, request):
        event_config = {}
        is_mobile = self.get_channel(request)
        if "rest/" not in request.path:
            page_config = self.get_page_config(
                resolve(request.path), is_mobile)
            try:
                if page_config is None:
                    return event_config

                page_id = page_config.get('page_id', None)
                if page_config is not None and page_id is not None:
                    analytics_config_obj = constants.CLIENT_SIDE_EVENTS_LOAD.get(
                        page_id, {})
                    for include_config in page_config.get('include_list'):
                        merge_information = constants.CLIENT_SIDE_EVENTS_LOAD.get(
                            include_config, {})
                        self.merge_analytics_config(
                            analytics_config_obj, merge_information)
                    event_config = analytics_config_obj
                else:
                    event_config = {}
            except Exception as e:
                logger.error(str(e))
        return event_config

    def get_channel(self, request):
        if hasattr(request, 'is_mobile') and request.is_mobile is True:
            is_mobile = '1'
        else:
            is_mobile = '0'
        return is_mobile

    # Function to merge page events.
    def merge_analytics_config(self, orig_config_dict, merge_config):
        try:
            self.add_unique_elemets_dict(
                orig_config_dict.get("available_elements"),
                merge_config.get("available_elements"))
            self.add_unique_elemets_dict(
                orig_config_dict.get("element_override"),
                merge_config.get("element_override"))
            self.add_unique_elements_list(orig_config_dict.get('events_list'),
                                          merge_config.get("events_list"))
        except Exception as err:
            logger.exception("Exception in merge_analytics_config")

    def add_unique_elemets_dict(self, orig_dict, merge_dict):
        for element in merge_dict:
            if orig_dict.get(element, None) is None:
                orig_dict[element] = merge_dict[element]

    def add_unique_elements_list(self, orig_list, merge_list):
        for element in merge_list:
            if element not in orig_list:
                orig_list.append(element)

    def get_base_jobj(self, request):
        base_obj = {
        }
        base_obj['staticPath'] = env
        base_obj['analytics'] = {
            'identifier': get_user_identifier(request)
        }
        is_mobile = self.get_channel(request)
        base_obj['analytics']['ISMOBILE'] = True if is_mobile == "1" else False
        base_obj['analytics']['utm_params'] = get_utm_params_from_request(
            request)
        base_obj['fbAppId'] = settings.FACEBOOK_APP_ID
        base_obj['loggedin'] = True if request.user and request.user.is_authenticated(
        ) else False
        base_obj['userId'] = request.user.id if request.user and request.user.is_authenticated(
        ) else ""
        base_obj['user'] = self.get_user(
            request.user) if request.user and request.user.is_authenticated() else {}
        ip = get_ip(request)
        if ip is not None:
            base_obj['analytics']['IP'] = ip
        base_obj['analytics']['TIMESTAMP'] = datetime.datetime.now().strftime(
            '%Y-%m-%d %H:%M %p')
        if is_mobile == '1':
            channel = 'mobile'
            base_obj['analytics']['segment_io_key'] = settings.SEGMENT_EVENTS_KEYS[AnalyticsEventTracker.MSITE]
        else:
            base_obj['analytics']['segment_io_key'] = settings.SEGMENT_EVENTS_KEYS[AnalyticsEventTracker.WEBSITE]
            channel = 'desktop'

        base_obj['analytics']['channel'] = channel

        base_obj['analytics']['event_date'] = datetime.datetime.today().strftime(
            '%Y-%m-%d')
        base_obj['analytics']['event_time'] = datetime.datetime.now().strftime(
            '%H:%M %p')
        base_obj['analytics']['config'] = self.load_analytics_events_config(
            request)
        return base_obj

    def get_user(self, user):
        return {
            'email': user.email,
            'phone': user.phone_number,
            'firstName': user.first_name}

    def get_page_config(self, resolver_match, is_mobile):
        try:
            request_src = constants.DEVICE_TO_CONFIG_MAP.get(
                is_mobile, "DEFAULT")
            action_info = constants.ACTION_TO_EVENTS_MAPPER[request_src].get(
                "{0}:{1}".format(resolver_match.namespace, resolver_match.url_name), None)
            if action_info:
                return action_info
            else:
                action_info = constants.ACTION_TO_EVENTS_MAPPER[request_src].get(
                    resolver_match, None)
                return action_info
        except Exception as e:
            logger.exception("Exception in get_page_config")
            return None

    def generate_sections_of_url(self, resolver_match):
        sections = []
        temp = ""
        while resolver_match != '/':
            temp = os.path.split(resolver_match)
            resolver_match = temp[0]
            sections.append(temp[1])
        return sections

