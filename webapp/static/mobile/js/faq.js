'use strict';

import $ from 'jquery';
import {Observable} from "rx";
import BaseView from './treebolibs/baseview';
import logger from './treebolibs/logger';
import '../css/faq.less';

let FaqView = Object.create(BaseView);

FaqView.pageLoaded = () => {
	var location_hash = location.hash;
	if(location_hash.length) {
		$(location_hash).find('.faq-block__title').trigger('click');
		$(location_hash).find('.faq-block-queries').slideDown();
	}

	$(".faq-block__status").click(function(e){
		var $parentElem = $(e.target).closest('.faq-block__qa');
		$parentElem.find('.faq-block__a').slideToggle();
		$parentElem.find(".up-down-arrow").toggleClass("rotate");
		$parentElem.find(".up-down-arrow").toggleClass("rotate-reset");
	})

	$(".faq-block__title").click(function(e){
		var $parentElem = $(e.target).closest('.faqs-desc');

		var faqs = $('.faqs-desc--open');
		faqs.find('.faq-block-queries').slideUp();
		var isOpened = $parentElem.hasClass('faqs-desc--open');
		faqs.removeClass('faqs-desc--open');
		if(isOpened){
			return ;
		}
		$parentElem.find('.faq-block-queries').slideDown();
		$parentElem.toggleClass('faqs-desc--open');
	})
	logger.log(" FAQ page loaded");

}

let faqConfig = { name:'', widgets:[]};
let faqView = FaqView.setup(faqConfig);
