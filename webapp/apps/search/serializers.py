from rest_framework import serializers
import re


class SearchSerializer(serializers.Serializer):
    checkin = serializers.DateField(required=True)
    checkout = serializers.DateField(required=True)
    roomconfig = serializers.CharField(max_length=100, default='1-0', required=False)
    meta_hotel_id = serializers.CharField(max_length=100, required=False)

    def validate(self, data):
        if data['checkout'] <= data['checkin']:
            raise serializers.ValidationError("checkin date must be before checkout date")

        room_config = data['roomconfig'].strip(',')
        data['roomconfig'] = room_config
        room_config_list = room_config.split(',')
        check_room_config = []

        for room_config in room_config_list:
            check_room_config.append(bool(re.match('^[0-9]*-[0-9]*$', room_config)))

        if False in check_room_config:
            raise serializers.ValidationError("Invalid room config")

        return data
