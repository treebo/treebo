# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0025_auto_20160720_1000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='discount_value',
            field=models.DecimalField(
                default=0,
                max_digits=20,
                decimal_places=10),
        ),
    ]
