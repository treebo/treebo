'use strict';
var $ = require('jquery'),
    _ = require('underscore');

// Event Delegation utility.
/*
    Usage:
    var config = {
        "#content:click dbclick":function(e){
            console.log("Am called with scope ",this);
        }
    }
    binder('body',config,window);
*/
module.exports = (elemSelector,config, scope)=>{
    var events, parts, selector;
    _.each(_.keys(config),key => {
        parts = key.split(":");
        selector = parts[0];
        events = parts[1].split(" ");
         _.each(events,evnt => {
            $(elemSelector).on(evnt, selector , scope ? _.bind(config[key],scope) : config[key] );
        });
    });
}