import abc
import logging

from django.db import DatabaseError, close_old_connections
from django.db import transaction
from hx.exceptions import HxNonRetryableAPIFailure, HxAPIMaxRetriesExceeded
from ths_common.exceptions import CRSException

from apps.bookings import constants
from apps.bookings.models import Booking, RoomBooking, PaymentOrder
from apps.bookings.repositories.booking_repository import BookingServiceRepository
from apps.bookings.services.booking_failure import BookingFailureHandler
from apps.bookings.services.booking_utils import get_response, get_exception_mail, \
    get_cancellation_mail, send_booking_cancellation_email
from apps.bookings.services.external_booking_service import ExternalBookingService
from apps.bookingstash.utils import StashUtils
from apps.common.exceptions.booking_exceptions import InvalidBookingRequestException, CommittedPriceNotSetException
from apps.common.exceptions.custom_exception import PublishEventToGrowthError
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.growth.services.growth_services import GrowthServices
from apps.profiles import utils as profile_utils
from base.db_decorator import db_retry_decorator
from webapp.common.services.feature_toggle_api import FeatureToggleAPI

logger = logging.getLogger(__name__)


class AbstractExternalBookingService(ExternalBookingService):
    """
        Abstract Implementation for ExternalBookingService APIs
    """

    @db_retry_decorator()
    def initiate_booking(
            self,
            booking_id,
            user_id=None,
            request_id=None,
            is_secure=True):
        """
        Creates a temporary booking in the reservation system.
        :param booking_id:
        :param user_id:
        :param request_id:
        :param is_secure:
        :return:
        """
        try:
            logger.debug("Entering method initiate_booking for booking_id : {0}".format(booking_id))
            save_booking_response = self.save_booking(booking_id, user_id, request_id, is_secure)

            status_code = save_booking_response['status_code']
            logger.info("The status of this booking is %d %s", status_code, save_booking_response)

            return save_booking_response
        finally:
            logger.debug("Exiting method initiate_booking for booking_id : {0}".format(booking_id))

    @db_retry_decorator()
    def create_booking(
            self,
            booking_id,
            user_id=None,
            request_id=None,
            is_secure=True):
        """
        Creates a temporary booking and then confirms it in the reservation system, in single method call.
        Basically a (initiate_booking + finalise_booking)
        :param booking_id:
        :param user_id:
        :param request_id:
        :param is_secure:
        :return:
        """
        try:
            logger.debug("Entering method create_booking for booking_id : {0}".format(booking_id))
            initiate_booking_response = self.initiate_booking(booking_id, user_id, request_id, is_secure)

            if initiate_booking_response['status_code'] in [400, 500]:
                return initiate_booking_response

            finalise_booking_response = self.finalise_booking(booking_id, user_id, request_id)
            return finalise_booking_response

        finally:
            logger.debug("Exiting method create_booking for booking_id : {0}".format(booking_id))

    @db_retry_decorator()
    @transaction.atomic
    def save_booking(
            self,
            booking_id,
            user=None,
            request_id=None,
            is_secure=True):
        """
        :param booking_id:
        :param user:
        :param request_id:
        :param is_secure:
        :return:
        """
        try:
            logger.debug("Entering method save_booking for booking_id : {0}".format(booking_id))

            try:
                _ = Booking.objects.select_for_update().get(pk=booking_id)
                booking = Booking.objects.select_related('hotel', 'room').get(pk=booking_id)
            except Exception as e:
                logger.error("Booking not found for booking_id : {0}".format(booking_id), exc_info=e)
                return get_response(500, "Booking not found for booking_id : {0}".format(booking_id))

            try:
                if booking.booking_status in (Booking.TEMP_BOOKING_CREATED, Booking.CONFIRM):
                    return get_response(200, "Booking already present with id : {0} and status : {1}".format(
                                            booking_id, booking.booking_status))

                if booking.booking_status == Booking.CANCEL:
                    return get_response(200, "Booking already cancelled : {0}".format(booking_id))

                user = booking.user_id
                is_customer_care = profile_utils.get_customer_care(user.id)

                if not is_customer_care and not user:
                    logger.error("No user attached to Booking Request")
                    track_failure = BookingFailureHandler.get_track_failure(booking, "No user attached to booking",
                                                                            self.get_subject_for_track_failure())
                    return get_response(400, "No user attached to Booking Request", [track_failure])

                room_configs = StashUtils.room_config_string_to_list(booking.room_config)
                special_rates_enabled = False
                rate_name = 'Treebo Promotional'

                response = self.save_booking_on_external_reservation_system(booking, room_configs, rate_name, special_rates_enabled)
                logger.info('savebooking response {0}'.format(response.__dict__))

                retry_attempts = 3
                for attempt in range(retry_attempts):
                    try:
                        self.save_booking_to_db(booking, response)
                        self.save_room_booking_to_db(booking, response)
                    except DatabaseError:
                        logger.warning("Database error occurred for attempt : %s", attempt)
                        if attempt < retry_attempts:
                            logger.info("Closing old connections before retrying again")
                            close_old_connections()
                    response_data = self.create_save_booking_response(response, booking_id)
                    return get_response(200, "Temp booking created with id : {booking_id} and status : {booking_status}"
                                        .format(booking_id=booking_id, booking_status=booking.booking_status),
                                        [response_data])

            except HxAPIMaxRetriesExceeded as e:
                logger.error("HxAPIMaxRetriesExceeded occurred while creating Booking in HX. Order Id: {order_id}"
                             .format(order_id=booking.order_id), exc_info=e)
                track_failure = BookingFailureHandler.get_track_failure(booking, "Unable to create temp booking in HX",
                                                                        self.get_subject_for_track_failure())
                exception_mail = get_exception_mail(str(e), "Unable to create temp booking in Hx. Order Id: {order_id}"
                                                    .format(order_id=booking.order_id))
                return get_response(500, str(e), [track_failure, exception_mail])

            except (CRSException, InvalidBookingRequestException, CommittedPriceNotSetException) as e:
                logger.error("Exception occurred while creating Booking in CRS. Order Id: {order_id}"
                             .format(order_id=booking.order_id), exc_info=e)
                logger.exception("Error occured in initiate call with CRS as %s %s", str(e.description),
                                 str(e.extra_payload))
                track_failure = BookingFailureHandler.get_track_failure(booking, "Unable to create temp booking in CRS",
                                                                        self.get_subject_for_track_failure())
                exception_mail = get_exception_mail(
                    str(e.extra_payload) if hasattr(e, 'extra_payload') else str(e.message),
                    "Unable to create temp booking. Order Id: {order_id}"
                        .format(order_id=booking.order_id))
                return get_response(500, str(e.extra_payload) if hasattr(e, 'extra_payload') else str(e.message),
                                    [track_failure, exception_mail])

            except (HxNonRetryableAPIFailure, Exception) as e:
                logger.error("Exception occurred while creating Booking in Reservation system (HX/CRS). "
                             "Order Id: {order_id}".format(order_id=booking.order_id), exc_info=e)
                logger.exception("Exception occured while initiate api %s", str(e))
                track_failure = BookingFailureHandler.get_track_failure(booking, "Unable to create temp booking",
                                                                        self.get_subject_for_track_failure())
                exception_mail = get_exception_mail(str(e), "Unable to create temp booking. Order Id: {order_id}"
                                                    .format(order_id=booking.order_id))
                return get_response(400, str(e), [track_failure, exception_mail])

        finally:
            logger.debug("Exiting method save_booking for booking_id : {0}".format(booking_id))

    @db_retry_decorator()
    @transaction.atomic
    def cancel_booking(self, order_id, user_id=None, request_id=None):
        """
        :param order_id:
        :param user_id:
        :param request_id:
        :return:
        """
        try:
            logger.debug("Entering method cancel_booking for order_id : {0}".format(order_id))

            try:
                _ = Booking.objects.select_for_update().get(order_id=order_id)
                booking = Booking.objects.select_related('hotel').get(order_id=order_id)
            except Booking.DoesNotExist as e:
                logger.error("Booking not found for order_id : {0}".format(order_id), exc_info=e)
                return get_response(500, "Booking not found for order_id {0}".format(order_id))

            try:
                room_bookings = RoomBooking.objects.filter(booking=booking)
                self.cancel_booking_on_external_reservation_system(booking, room_bookings, order_id)

                return get_response(200, "Booking cancelled with id : {0}".format(
                        booking.order_id))
            except HxAPIMaxRetriesExceeded as e:
                logger.error("HxAPIMaxRetriesExceeded occurred while cancelling Booking in HX", exc_info=e)
                cancellation_mail = get_cancellation_mail(order_id)
                return get_response(500, str(e), [cancellation_mail])

            except CRSException as e:
                logger.error("Exception occurred while cancelling Booking in CRS", exc_info=e)
                cancellation_mail = get_cancellation_mail(order_id)
                return get_response(500, e.extra_payload if e.extra_payload else 'message: {0}, description: {1}'
                                    .format(e.message, e.description), [cancellation_mail])

            except (HxNonRetryableAPIFailure, Exception) as e:
                logger.error("Exception occurred while cancelling Booking in Reservation system (HX/CRS)", exc_info=e)
                cancellation_mail = get_cancellation_mail(order_id)
                return get_response(400, "Cancellation Failed for Order Id: {0}".format(order_id),
                                    [cancellation_mail])

        finally:
            logger.debug("Exiting method cancel_booking for order_id : {0}".format(order_id))

    @db_retry_decorator()
    @transaction.atomic
    def confirm_booking(self, booking_id, user_id=None, request_id=None):
        """
        Equilant to tasks.py : confirm_booking
        :param booking_id:
        :param user_id:
        :param request_id:
        :return:
        """
        try:
            logger.info("Entering method confirm_booking for booking_id : {0}".format(booking_id))

            try:
                _ = Booking.objects.select_for_update().get(pk=booking_id)
                booking = Booking.objects.select_related('hotel').get(pk=booking_id)
            except Booking.DoesNotExist as e:
                logger.error("unable to fetch booking details for booking_id : {0}".format(booking_id), exc_info=e)
                return get_response(500, "Booking not found for id {0}".format(booking_id))

            try:
                if booking.booking_status == Booking.CONFIRM:
                    return get_response(200, "SUCCESS")

                if booking.booking_status == Booking.CANCEL:
                    return get_response(200, "Booking already cancelled : {0}".format(booking_id))

                booking.booking_status = Booking.INITIATED
                booking.save()

                paid_source = "Web (Prepaid)" if booking.payment_mode.lower() == "paid" else "Web (Pay@hotel)"
                response_json = self.confirm_booking_on_external_reservation_system(booking, paid_source)

                if response_json.get('booking_status') == Booking.CONFIRM and \
                    booking.payment_mode == booking.PARTIAL_RESERVE:
                    try:
                        partpay_confirmation_object = self.get_data_to_update_upfront_partpay_in_growth(booking)
                        GrowthServices.publish_upfront_part_payment_confirmation(partpay_confirmation_object)
                    except PublishEventToGrowthError as err:
                        slack_alert.send_slack_alert_for_exceptions(
                            status=500, booking=booking.__dict__(),
                            dev_msg="Publishing part payment confirmation to Growth",
                            message=err.__str__(), class_name=self.__class__.__name__)
                return get_response(200, "Booking confirmed : {booking_id}".format(booking_id=booking_id),
                                    [response_json])
            except HxAPIMaxRetriesExceeded as e:
                logger.error("HxAPIMaxRetriesExceeded occurred while confirm Booking in HX. Order Id {order_id}"
                             .format(order_id=booking.order_id), exc_info=e)
                track_failure = BookingFailureHandler.get_track_failure(booking, "Confirm Booking Failure",
                                                                        self.get_subject_for_track_failure())
                exception_mail = get_exception_mail(str(e), subject="Unable to confirm booking in Hx. Order ID: "
                                                                    "{order_id}".format(order_id=booking.order_id))
                return get_response(500, str(e), [track_failure, exception_mail])

            except CRSException as e:
                logger.error("Exception occurred while confirming booking in CRS. Order Id {order_id}"
                             .format(order_id=booking.order_id), exc_info=e)
                logger.exception("Exception Occured with CRS booking %s %s", str(e.message), str(e.extra_payload))
                track_failure = BookingFailureHandler.get_track_failure(booking, "Confirm Booking Failure",
                                                                        self.get_subject_for_track_failure())
                exception_mail = get_exception_mail(
                    str(e.extra_payload) if hasattr(e, 'extra_payload') else str(e.message),
                    "Unable to confirm booking. Order Id: {order_id}"
                    .format(order_id=booking.order_id))
                return get_response(500, str(e.extra_payload) if hasattr(e, 'extra_payload') else str(e.message),
                                    [track_failure, exception_mail])

            except (HxNonRetryableAPIFailure, Exception) as e:
                logger.error("Exception occurred while confirming booking in Reservation system (HX/CRS). "
                             "Order Id {order_id}".format(order_id=booking.order_id), exc_info=e)
                logger.exception("Exception Occured with CRS booking %s", str(e))
                track_failure = BookingFailureHandler.get_track_failure(booking, "Confirm Booking Failure",
                                                                        self.get_subject_for_track_failure())
                exception_mail = get_exception_mail(str(e), "Unable to confirm booking. Order Id: {order_id}"
                                                    .format(order_id=booking.order_id))
                return get_response(400, str(e), [track_failure, exception_mail])

        finally:
            logger.debug("Exiting method confirm_booking for booking_id : {0}".format(booking_id))

    @abc.abstractmethod
    def finalise_booking(self, booking_id, user_id, request_id):
        """
        :param booking_id:
        :param user_id:
        :param request_id:
        :return:
        """
        pass

    @abc.abstractmethod
    def save_booking_on_external_reservation_system(self, booking, room_configs, rate_name, special_rates_enabled):
        """
        Create temp booking on External Reservation System (HX or CRS)
        :param booking:
        :param room_configs:
        :param rate_name:
        :param special_rates_enabled:
        :param channel_id:
        :param sub_channel_id:
        :param application_id:
        :return:
        """
        pass

    @abc.abstractmethod
    def save_booking_to_db(self, booking, save_booking_response):
        """
        Save Booking to DB
        :param booking:
        :param save_booking_response:
        :return:
        """
        pass

    @abc.abstractmethod
    def save_room_booking_to_db(self, booking, save_booking_response):
        """
        Save RoomBooking to DB
        :param booking:
        :param save_booking_response:
        :return:
        """
        pass

    @abc.abstractmethod
    def create_save_booking_response(self, save_booking_response, booking_id):
        """
        Creates Save Booking Response JSON
        :param save_booking_response:
        :param booking_id:
        :return:
        """
        pass

    @abc.abstractmethod
    def cancel_booking_on_external_reservation_system(self, booking, room_bookings, order_id):
        """
        Cancel booking on External Reservation System (HX or CRS)
        :param booking:
        :param room_bookings:
        :param order_id:
        :return:
        """
        pass

    @abc.abstractmethod
    def confirm_booking_on_external_reservation_system(self, booking, paid_source):
        """
        Confirm booking on External Reservation System (HX or CRS)
        :param booking:
        :param paid_source:
        :return:
        """
        pass

    @abc.abstractmethod
    def get_data_to_update_upfront_partpay_in_growth(self, booking):
        """
        generate the event payload for growth which includes group code and hotel code which
        differs based on the external service
        :param booking:
        :return:
        """
        pass

    @abc.abstractmethod
    def get_subject_for_track_failure(self):
        """
        get subject for track_failure
        :return:
        """
        pass

    def is_wallet_applied_and_wallet_payment_failed(self, booking):
        if not booking.wallet_applied or float(booking.wallet_deduction) <= 1.0:
            wallet_applied = False
        else:
            wallet_applied = True

        if wallet_applied:
            payment_order = BookingServiceRepository.get_paymentorder_by_booking_order_id(booking.order_id)
            if payment_order and payment_order.status != PaymentOrder.CAPTURED:
                logger.info(
                    "is_wallet_applied_and_wallet_payment_failed returns True for bid %s wallet applied %s "
                    "and wallet_deduction %s and payment_order %s",
                    booking.id,
                    booking.wallet_applied,
                    booking.wallet_deduction,
                    payment_order)
                return True
        return False

    @transaction.atomic
    def update_gst(self, booking):
        """
        Updates the gst detail associated with a booking
        :param booking:
        :return:
        """
        if booking.booking_status != Booking.CONFIRM and booking.booking_status != Booking.PART_SUCCESS:
            exception_mail = get_exception_mail(Exception(),
                                                "Unable to update guest details in {0}. Booking not confirmed for "
                                                "Order Id: {1}".format(self.get_external_reservation_system_name(),
                                                                       booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                "updating gstin details",
                "Please Update GSTIN details in {0} for order id : ".format(self.get_external_reservation_system_name()))
            return get_response(500,
                                'Booking not yet confirmed in {0}'.format(self.get_external_reservation_system_name()),
                                [exception_mail, track_failure])

        return self.update_gst_detail_of_booking_owner_on_external_reservation_system(booking)

    @abc.abstractmethod
    def update_gst_detail_of_booking_owner_on_external_reservation_system(self, booking):
        pass

    @abc.abstractmethod
    def get_external_reservation_system_name(self):
        pass

    def update_payment_for_booking(self, booking):
        if booking.booking_status != Booking.CONFIRM and booking.booking_status != Booking.PART_SUCCESS:
            exception_mail = get_exception_mail(Exception(),
                                                "Unable to update payment details in {0}. Booking not confirmed for "
                                                "Order Id: {1}".format(self.get_external_reservation_system_name(), booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                "updating payment details",
                "Please Update Payment in {0} for order id :".format(self.get_external_reservation_system_name()))
            return get_response(500,
                                'Booking not yet confirmed in {0}'.format(self.get_external_reservation_system_name()),
                                [exception_mail, track_failure])

        update_all_payment_for_booking_response = self.update_payment_for_booking_on_external_reservation_system(
            booking.id)
        logger.info("update_all_payment_for_booking_response  for bid %s is %s ", booking.id,
                    update_all_payment_for_booking_response)

        return update_all_payment_for_booking_response

    @abc.abstractmethod
    def update_payment_for_booking_on_external_reservation_system(self, booking_id):
        """
        :param booking_id:
        :return:
        """
        pass
