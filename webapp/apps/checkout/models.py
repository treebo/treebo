# pylint: disable-msg=invalid-name

import enum
from django.db import models
from djutil.models import TimeStampedModel
from jsonfield import JSONField

from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.bookings.models import Booking
from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.models.profile import User


class PaymentModes(enum.Enum):
    PARTIAL_RESERVE = Booking.PARTIAL_RESERVE
    PART_PAID = Booking.PART_PAID
    PAID = Booking.PAID
    NOT_PAID = Booking.NOT_PAID

    @staticmethod
    def get(key):
        if key and key.upper() in PaymentModes.__members__:
            return PaymentModes[key.upper()].value
        return None

    @staticmethod
    def if_pay_at_hotel(key):
        if key and key.upper() in PaymentModes.__members__ and PaymentModes[key.upper()].value in \
            [PaymentModes.PARTIAL_RESERVE.value, PaymentModes.NOT_PAID.value]:
            return '1'
        return '0'


class BookingRequest(TimeStampedModel, DefaultPermissions):
    INITIATED = "Initiated"
    PAYMENT_IN_PROGRESS = "Payment In Progress"
    COMPLETED = "Completed"
    PART_SUCCESS = "Part Success"
    STATUS = (
        (INITIATED, INITIATED),
        (PAYMENT_IN_PROGRESS, PAYMENT_IN_PROGRESS),
        (COMPLETED, COMPLETED),
        (PART_SUCCESS, PART_SUCCESS)
    )
    MSITE = 'msite'
    WEBSITE = 'website'
    APP = 'app'
    GDC = 'gdc'
    ANDROID = 'android'
    IOS = 'ios'
    BOOKING_CHANNEL = (
        (MSITE, MSITE),
        (WEBSITE, WEBSITE),
        (APP, APP),
        (GDC, GDC),
        (ANDROID, ANDROID),
        (IOS, IOS)
    )
    PAY_AT_HOTEL = "PAH"
    FOT = "FOT"
    RAZOR_PAY = "RazorPay"
    PAY_U = "PayU"

    EP = 'EP'
    NRP = 'NRP'

    RATE_PLAN_CHOICES = (
        (EP, EP),
        (NRP, NRP)
    )

    BOOKING_MODE = (
        (PAY_AT_HOTEL, PAY_AT_HOTEL),
        (FOT, FOT),
        (RAZOR_PAY, RAZOR_PAY),
        (PAY_U, PAY_U)
    )

    adults = models.PositiveIntegerField()
    children = models.PositiveIntegerField()
    room_required = models.PositiveIntegerField()
    add_to_cart_hotel_id = models.CharField(
        max_length=200, null=True, blank=True)
    user = models.ForeignKey(
        User,
        blank=True,
        null=True,
        related_name='userrequests',
        on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(
        max_length=200,
        null=True,
        blank=True,
        db_index=True)
    country_code = models.CharField(max_length=8, null=True)
    phone = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        db_index=True)
    coupon_code = models.CharField(max_length=200, null=True, blank=True)
    discount_value = models.DecimalField(
        max_digits=20, decimal_places=10, default=0)
    discount_id = models.CharField(max_length=50, null=True, blank=True)
    checkin_time = models.CharField(max_length=200, null=True, blank=True)
    checkout_time = models.CharField(max_length=200, null=True, blank=True)
    comments = models.TextField(default="")

    booking_channel = models.CharField(
        max_length=50,
        choices=BOOKING_CHANNEL,
        blank=True,
        default='')
    booking_subchannel = models.CharField(max_length=50, null=True, blank=True)
    booking_application = models.CharField(max_length=50, null=True, blank=True)
    user_login_state = models.CharField(max_length=30, null=True, blank=True)
    user_signup_channel = models.CharField(
        max_length=200, null=True, blank=True)
    pay_at_hotel = models.CharField(max_length=10, null=True, blank=True)
    booking_id = models.PositiveIntegerField(null=True)
    order_id = models.CharField(max_length=200, null=True, blank=True)
    session_id = models.CharField(max_length=200, null=True, blank=True)
    booking_url = models.CharField(max_length=500, null=True, blank=True)
    savebooking_job_id = models.CharField(
        max_length=200, null=True, blank=True)
    confirmbooking_job_id = models.CharField(
        max_length=200, null=True, blank=True)
    # Below 2 fields are not used now
    accesssecret = models.CharField(max_length=200, null=True, blank=True)
    accesskey = models.CharField(max_length=200, null=True, blank=True)
    # Below field is not used now
    meta_http_host = models.CharField(max_length=200, null=True, blank=True)
    host_ip_address = models.CharField(max_length=200, null=True, blank=True)
    utm_source = models.CharField(max_length=200, null=True, blank=True)
    utm_medium = models.CharField(max_length=200, null=True, blank=True)
    utm_campaign = models.CharField(max_length=200, null=True, blank=True)

    checkin_date = models.DateField(null=True)
    checkout_date = models.DateField(null=True)
    hotel_id = models.PositiveIntegerField(null=True)
    room_config = models.CharField(max_length=200, null=True, blank=True)
    room_type = models.CharField(max_length=50, null=True, blank=True)
    coupon_apply = models.NullBooleanField(default=False)
    is_audit = models.NullBooleanField(null=True, default=False)
    instant_booking = models.NullBooleanField(null=True, default=None)
    status = models.CharField(
        max_length=50,
        default=INITIATED,
        choices=STATUS,
        null=True,
        blank=True,
        db_index=True)
    rack_rate = models.DecimalField(
        max_digits=20, null=True, decimal_places=10)
    mm_promo = models.DecimalField(max_digits=20, null=True, decimal_places=10)
    pretax_amount = models.DecimalField(
        max_digits=20, null=True, decimal_places=10)
    tax_amount = models.DecimalField(
        max_digits=20, null=True, decimal_places=10)
    total_amount = models.DecimalField(
        null=True, max_digits=20, decimal_places=2)
    payment_amount = models.DecimalField(
        null=True, max_digits=20, decimal_places=2, default=0)
    payment_mode = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        default="Not Paid")
    voucher_amount = models.DecimalField(
        max_digits=20, decimal_places=2, default=0, null=True)
    otp_verified_number = models.CharField(
        max_length=20, null=True, blank=True)
    organization_name = models.CharField(max_length=200, null=True, blank=True)
    organization_address = models.CharField(
        max_length=200, null=True, blank=True)
    organization_taxcode = models.CharField(
        max_length=100, null=True, blank=True)

    rate_plan = models.CharField(max_length=50, null=True, blank=True)
    rate_plan_meta = JSONField(blank=True, null=True)
    member_discount = models.DecimalField(
        max_digits=9, null=True, decimal_places=4, default=0)

    wallet_applied = models.NullBooleanField(default=False, null=True)
    wallet_deduction = models.DecimalField(
        max_digits=9, decimal_places=4, default=0, null=True)
    is_flash_sale_booking = models.NullBooleanField(null=True)
    flash_sale_discount = models.IntegerField(null=True)
    is_flat_price_booking = models.NullBooleanField(null=True)
    cancel_hash_value = models.CharField(max_length=50, null=True, blank=True)
    utm_tracking_id = models.CharField(max_length=200, null=True, blank=True)
    part_pay_amount = models.DecimalField(null=True, max_digits=20, decimal_places=2)
    payment_offer_id = models.CharField(max_length=50, null=True, blank=True)
    payment_offer_price = models.DecimalField(null=True, max_digits=20, decimal_places=2)

    def calculate_total_amount(self):
        self.total_amount = PricingServiceV2.calculate_total_amount(
            self.pretax_amount,
            self.member_discount,
            self.discount_value,
            self.voucher_amount,
            self.tax_amount)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Booking Request'
        verbose_name_plural = 'Booking Requests'
        db_table = "bookings_bookingrequest"

    def __unicode__(self):
        return 'check_in: %s checkout: %s hotel %s room %s config %s' % (
            self.checkin_date, self.checkout_date, self.hotel_id, self.room_type, self.room_config)

    def __str__(self):
        return 'check_in: %s checkout: %s hotel %s room %s config %s' % (
            self.checkin_date, self.checkout_date, self.hotel_id, self.room_type, self.room_config)


class BookingRequestPrice(TimeStampedModel, DefaultPermissions):

    booking_request = models.ForeignKey(BookingRequest, null=False,
                                        related_name='bookingrequest_price',
                                        on_delete=models.CASCADE)
    pretax_amount = models.DecimalField(max_digits=20, null=True, decimal_places=10)
    discount_id = models.CharField(max_length=50, null=True, blank=True)
    discount_value = models.DecimalField(max_digits=20, decimal_places=10, default=0)
    tax_amount = models.DecimalField(max_digits=20, null=True, decimal_places=10)
    wallet_deduction = models.DecimalField(max_digits=9, decimal_places=4, default=0, null=True)
    wallet_applied = models.NullBooleanField(default=False, null=True)
    rate_plan_meta = JSONField(blank=True, null=True)
    coupon_code = models.CharField(max_length=200, null=True, blank=True)
    coupon_apply = models.NullBooleanField(default=False)
    total_amount = models.DecimalField(null=True, max_digits=20, decimal_places=2)
    booking_mode = models.CharField(max_length=100, null=False, blank=False)
    rate_plan = models.CharField(max_length=50, null=True, blank=True)
    part_pay_amount = models.DecimalField(null=True, max_digits=20, decimal_places=2)
    datewise_pricing = models.TextField(null=True)

    class Meta(DefaultPermissions.Meta):
        verbose_name = 'Booking Request Price'
        verbose_name_plural = 'Booking Requests Price'
        db_table = "bookings_bookingrequest_price"
        unique_together = ('booking_request', 'booking_mode', 'rate_plan')


class BookingRequestUserMapForWallet(TimeStampedModel):
    user = models.ForeignKey(
        User,
        blank=True,
        null=True,
        related_name='usermaps',
        on_delete=models.DO_NOTHING)
    booking_request = models.ForeignKey(
        BookingRequest,
        blank=True,
        null=True,
        related_name='bookingrequestmaps',
        on_delete=models.DO_NOTHING)


class CommittedPrice(TimeStampedModel):
    booking_request = models.ForeignKey(BookingRequest,
                                null=False,
                                on_delete=models.CASCADE)
    datewise_pricing = models.TextField(
        db_column="data",
        blank=False
    )
