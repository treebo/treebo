import logging
import requests

from rest_framework.response import Response
from django.core.urlresolvers import reverse
from apps.referral import tasks
from base.middlewares.request import get_domain
from dbcommon.models.profile import User, UserReferral
from services.common_services.otp_service import OtpService

logger = logging.getLogger(__name__)


class PostVerificationService():

    @staticmethod
    def execute(command, data):
        if command == "referral":
            return requests.post(
                get_domain() +
                reverse("referral:signup"),
                data={
                    "user_id": data["user_id"]},
                allow_redirects=True)
        else:
            return Response(status=200)
