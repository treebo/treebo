RANKING_STRING = "Ranked {0} of {1} hotels in {2}"
READ_REVIEW_URL = "#REVIEWS"
EXCELLENCE_AWARD_NAME = "Certificate of Excellence"
DEFAULT_USER_IMAGE_URL = "https://images.treebohotels.com/images/user.png"
DEFAULT_NUM_REVIEWS_TO_FETCH = 5
DEFAULT_GUEST_NAME = "Guest"
DEFAULT_TA_REVIEW_THRESHOLD_KEY = "ta_review_threshold"
DEFAULT_TA_REVIEW_THRESHOLD = 3.5

