# hotel_upload_service

import datetime
import json
import logging
import os
import re
import zipfile

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from django.core.exceptions import MultipleObjectsReturned
from django.db import transaction
from django.db.models.query_utils import Q
from django.template.defaultfilters import slugify
from openpyxl import load_workbook

from data_services.respositories_factory import RepositoriesFactory
from . import hotel_upload_excel_mappings as mappings
from apps.bookingstash.models import HotelRoomMapper
from apps.content.models import ContentStore
from apps.hotels.exceptions import HotelUploadException
from common.constants import common_constants
from dbcommon.models.direction import Directions
from dbcommon.models.facilities import Facility
from dbcommon.models.hotel import Hotel, Highlight, SeoDetails, CitySeoDetails
from dbcommon.models.location import Landmark, Locality, City, State, CityAlias
from dbcommon.models.miscellanous import Image
from dbcommon.models.room import Room
from data_services.city_repository import CityRepository

log = logging.getLogger(__name__)

city_data_service = RepositoriesFactory.get_city_repository()


class HotelUploadService():
    __file_path = os.path.dirname(os.path.realpath(__file__))

    def __init__(self, settings, workbook_file, images_file):
        # type: (settings, file, file) -> HotelUploadService

        if settings is None:
            raise Exception("No settings file provided")

        self.__settings = settings
        if workbook_file is None:
            log.error("No file uploaded")
            raise Exception("No file uploaded")

        if images_file is None:
            log.error("No images uploaded")
            raise Exception("No file uploaded")
        self.__images = images_file

        workbook = load_workbook(filename=workbook_file, read_only=True)
        if workbook is None:
            log.error("No workbook found")
            raise Exception('No workbook found')

        # Only way to get the first sheet. Can't get by index
        for sheet in workbook:
            self.__sheet = sheet
            break
        if self.__sheet is None:
            raise Exception("No sheet found")

    @transaction.atomic
    def process_upload(
            self,
            seo_meta_title,
            seo_meta_desc,
            city_desc,
            city_image,
            city_seo_meta_title,
            city_seo_meta_desc,
            facilities,
            hotelogix_id,
            hotelogix_name,
            hotel_mm_mapping,
            room_acacia_mapping,
            room_oak_mapping,
            room_maple_mapping,
            room_mahogany_mapping,
            price_acacia,
            price_oak,
            price_maple,
            price_mahogany,
            room_amenity_data):
        try:
            hotel, city, rooms, state = self.__save_hotel_data(hotelogix_id, hotelogix_name, price_acacia,
                                                               price_oak,
                                                               price_maple,
                                                               price_mahogany, facilities,
                                                               room_amenity_data)
            self.__upload_to_s3(hotel.name, rooms)
            self.__save_addition_city_details(city, city_desc, city_image)
            self.__update_seo_details(
                seo_meta_desc,
                seo_meta_title,
                hotel,
                city_seo_meta_desc,
                city_seo_meta_title,
                city)
            self.__setup_hotel_mappings(
                rooms,
                hotel_mm_mapping,
                room_acacia_mapping,
                room_oak_mapping,
                room_maple_mapping,
                room_mahogany_mapping)
            return hotel, city, rooms, state
        except Exception as e:
            log.exception("Unable to save hotel")
            raise e

    def __save_addition_city_details(self, city, city_desc, city_image):
        log.info("__save_addition_city_details")
        if city.id is None:
            city.description = city_desc
            city_image_path = self.__save_and_upload_city_image(
                city, city_image)
            city.cover_image = city_image_path
            city.landscape_image = city_image_path
            city.portrait_image = city_image_path
            city.save()
            self.__save_city_desc_in_content_store(city.name, city_desc)

    def __save_city_desc_in_content_store(self, city_name, city_desc):
        log.info("__save_city_desc_in_content_store")
        content_store = ContentStore()
        content_store.name = "search_" + slugify(city_name) + "_" + "desc"
        content_store.value = json.dumps({
            "text": city_desc
        })
        content_store.save()

    def __setup_hotel_mappings(
            self,
            rooms,
            hotel_mm_mapping,
            room_acacia_mapping,
            room_oak_mapping,
            room_maple_mapping,
            room_mahogany_mapping):
        log.info("__setup_hotel_mappings")
        for room in rooms:
            hotel_room_mapper = HotelRoomMapper()
            hotel_room_mapper.mm_hotel_code = hotel_mm_mapping
            hotel_room_mapper.room_id = room.id
            if room.room_type_code.lower() == 'acacia':
                if HotelUploadService.__sanitize(room_acacia_mapping) == '':
                    continue
                hotel_room_mapper.mm_room_code = room_acacia_mapping
            elif room.room_type_code.lower() == 'oak':
                if HotelUploadService.__sanitize(room_oak_mapping) == '':
                    continue
                hotel_room_mapper.mm_room_code = room_oak_mapping
            elif room.room_type_code.lower() == 'maple':
                if HotelUploadService.__sanitize(room_maple_mapping) == '':
                    continue
                hotel_room_mapper.mm_room_code = room_maple_mapping
            elif room.room_type_code.lower() == 'mahogany':
                if HotelUploadService.__sanitize(room_mahogany_mapping) == '':
                    continue
                hotel_room_mapper.mm_room_code = room_mahogany_mapping
            hotel_room_mapper.save()

    def __update_seo_details(
            self,
            hotel_seo_desc,
            hotel_seo_meta_title,
            hotel,
            city_seo_meta_desc,
            city_seo_meta_title,
            city):
        """
        Creates the basic seo content for the hotel.
        :param hotel_seo_desc:
        :param hotel_seo_meta_title:
        :param hotel:
        :param city_seo_meta_desc:
        :param city_seo_meta_title:
        :param city:
        """
        seo_details = SeoDetails()
        seo_details.titleDescription = hotel_seo_meta_title
        seo_details.hotel = hotel
        seo_details.metaContent = hotel_seo_desc
        seo_details.save()
        SeoDetails.update_hotel_canonical_url(
            seo_details, hotel_ids=[hotel.id])

    @staticmethod
    def __create_city_seo_details(
            city,
            city_seo_meta_desc,
            city_seo_meta_title):
        """
        **DEPRECATED**
        Function was originally created to auto create the City SEO details,
        but this wasn't useful since the data is still empty inside this. Now SEO team needs to create this manually
        :param city:
        :param city_seo_meta_desc:
        :param city_seo_meta_title:
        """
        city_seo_details = CitySeoDetails()
        city_seo_details.city = city
        city_seo_details.citySeoTitleContent = city_seo_meta_title
        city_seo_details.citySeoDescriptionContent = city_seo_meta_desc
        city_seo_details.citySeoKeywordContent = city.name
        city_seo_details.save()

    def __parse_directions(self, directions):
        """
        Convert the directions into form that can be emailed
        :param directions:
        :return: The formatted directions
        """
        if re:
            split_directions = re.split(
                "\d+\.", self.__strip_unicode_char(directions))
            return "\n".join([dir.strip()
                              for dir in split_directions if len(dir.strip()) > 0])
        else:
            return ""

    def __strip_unicode_char(self, input_str):
        """
        replaces the windows left & right single quote with normal single quote
        :param input_str:
        :return:
        """
        if input_str is not None:
            if isinstance(input_str, str):
                return input_str.replace(
                    "\\u2018",
                    "'").replace(
                    "\\u2019",
                    "'").replace(
                    "\\u00E9",
                    'e')
            else:
                return input_str
        else:
            return ""

    def __extract_highlights(self):
        """

        :rtype: Highlight[]
        """
        highlights = []
        highlight_extract = self.__get_cell_value(mappings.HIGHLIGHTS)
        if highlight_extract is not None:
            trilights = highlight_extract.split("\n")
            for trilight in trilights:
                if trilight.strip() != '':
                    highlight = Highlight()
                    highlight.description = trilight
                    highlights.append(highlight)
        return highlights

    def __save_hotel_data(
            self,
            hotelogix_id,
            hotelogix_name,
            price_acacia,
            price_oak,
            price_maple,
            price_mahogany,
            facilities,
            room_amenity_data):
        # hotel worksheet and create objects
        """
        Read excel sheet and extract information and save the hotel details in the db
        :return: tuple of rooms extracted from the sheet
        """
        airport_directions, airport_landmark, city, city_alias, facilities, \
            highlights, hotel, landmark_1, landmark_1_directions, landmark_2, \
            landmark_2_directions, locality, railway_directions, railway_landmark, \
            rooms, state, rooms_amenities = self.extract_excel(facilities, hotelogix_id,
                                                               hotelogix_name,
                                                               price_acacia,
                                                               price_oak,
                                                               price_maple,
                                                               price_mahogany, room_amenity_data)

        return self.__save_entities(
            airport_directions,
            airport_landmark,
            city,
            city_alias,
            facilities,
            highlights,
            hotel,
            landmark_1,
            landmark_1_directions,
            landmark_2,
            landmark_2_directions,
            locality,
            railway_directions,
            railway_landmark,
            rooms,
            state,
            rooms_amenities)

    def __save_entities(
            self,
            airport_directions,
            airport_landmark,
            city,
            city_alias,
            facilities,
            highlights,
            hotel,
            landmark_1,
            landmark_1_directions,
            landmark_2,
            landmark_2_directions,
            locality,
            railway_directions,
            railway_landmark,
            rooms,
            state,
            rooms_amenities):
        state.save()
        city.save()
        city_alias.save()
        log.debug("Saved state, city, city alias")

        locality.city = city
        locality.save()
        log.debug("Saved locality")

        hotel.state = state
        hotel.city = city
        if locality:
            hotel.locality = locality
        hotel.status = Hotel.DISABLED
        hotel.save()
        log.debug("Saved hotel")

        landmark_1.city = city
        landmark_1.save()
        log.debug("Saved landmark 1")

        landmark_1_directions.hotel = hotel
        landmark_1_directions.landmark_obj = landmark_1
        landmark_1_directions.save()
        log.debug("Saved landmark 1 direction")

        landmark_2.city = city
        landmark_2.save()
        log.debug("Saved landmark 2")

        landmark_2_directions.hotel = hotel
        landmark_2_directions.landmark_obj = landmark_2
        landmark_2_directions.save()
        log.debug("Saved landmark 2 directions")

        airport_landmark.city = city
        airport_landmark.save()
        log.debug("Saved airport landmark")

        airport_directions.hotel = hotel
        airport_directions.landmark_obj = airport_landmark
        airport_directions.save()
        log.debug("Saved airport landmark directions")

        railway_landmark.city = city
        railway_landmark.save()
        log.debug("Saved railway landmark")

        railway_directions.hotel = hotel
        railway_directions.landmark_obj = railway_landmark
        railway_directions.save()
        log.debug("Saved railway landmark directions")

        for facility in facilities:
            facility.hotels.add(hotel)
        log.debug("Saved facilities")

        for highlight in highlights:
            highlight.hotel = hotel
            highlight.save()
        log.debug("Saved all highlights")

        if airport_directions.distance < 7:
            facility = Facility.objects.filter(
                name__icontains='Airport').first()
            if facility is None:
                facility = Facility()
                facility.name = 'Easily Accessible from Airport'
                facility.url = '#'
                facility.save()
            facility.hotels.add(hotel)
            facility.save()
            log.debug("Added airport accessibility as a facility")

        if railway_directions.distance < 7:
            facility = Facility.objects.filter(
                name__icontains='Railway').first()
            if facility is None:
                facility = Facility()
                facility.name = 'Easily Accessible from Railway Station'
                facility.url = '#'
                facility.save()
            facility.hotels.add(hotel)
            facility.save()
            log.debug("Added railway accessibility as a facility")

        for room in rooms:
            room.hotel = hotel
            if room.room_type_code in rooms_amenities:
                room_facility = rooms_amenities[room.room_type_code]
                log.info("rooms_amenities[room.room_type_code] = %s  and room_facility %s",
                         rooms_amenities[room.room_type_code], room_facility)
                room.save()
                for facility in room_facility:
                    facility.rooms.add(room)
                    facility.save()
            room.save()
        log.debug("Saved rooms and corresponding facilities, if any")
        return hotel, city, rooms, state

    def extract_excel(
            self,
            facilities,
            hotelogix_id,
            hotelogix_name,
            price_acacia,
            price_oak,
            price_maple,
            price_mahogany,
            room_amenity_data):
        hotel = self.__extract_hotel(hotelogix_id, hotelogix_name)

        facility_obj = Facility.objects.filter(
            Q(catalog_mapped_name='24_hour_front_desk') | Q(
                catalog_mapped_name='wakeup_service') | Q(catalog_mapped_name='tea_coffee') | Q(
                catalog_mapped_name='wifi') | Q(catalog_mapped_name='kettle') | Q(
                catalog_mapped_name='water') | Q(catalog_mapped_name='coffee_table') | Q(
                catalog_mapped_name='treebo_toiletries'))

        facilities = self.__extract_facilities(facilities, facility_obj)
        highlights = self.__extract_highlights()
        city, city_alias = self.__extract_city()

        state = self.__extract_state()
        locality = self.__extract_locality(city, hotel)
        hotel.landmark = ''
        landmark_1, landmark_1_directions, landmark_2, landmark_2_directions = self.__extract_landmark_directions(
            city, hotel)
        street, pincode = HotelUploadService.__strip_street_of_city_pincode(
            str(hotel.street), str(city.name))
        hotel.street = street
        locality.pincode = pincode
        airport_landmark, airport_directions = self.__extract_airport_directions(
            city, hotel)
        railway_landmark, railway_directions = self.__extract_railway_directions(
            city, hotel)
        rooms, rooms_amenities = self.__extract_room_info(
            price_acacia, price_oak, price_maple, price_mahogany, room_amenity_data)
        return airport_directions, airport_landmark, city, city_alias, facilities, highlights, hotel, landmark_1, \
            landmark_1_directions, landmark_2, landmark_2_directions, locality, railway_directions, railway_landmark, \
            rooms, state, rooms_amenities

    def __extract_facilities(self, selected_facilities, extra_facilities_obj):
        """

        :rtype: Facility[]
        """
        facilities = []
        try:
            if selected_facilities:
                for facility in selected_facilities:
                    if Facility.objects.filter(Q(name__icontains=facility) | Q(
                            catalog_mapped_name__icontains=facility)).exists():
                        facility_object = Facility.objects.filter(Q(name__icontains=facility) | Q(
                            catalog_mapped_name__icontains=facility)).first()
                        facilities.append(facility_object)

            # updating some common hotel/room facilities
            for facility in extra_facilities_obj:
                if facility not in facilities:
                    facilities.append(facility)
            log.info("extracted facilities %s", facilities)
        except Exception:
            log.exception("Exceptio in extraction hotel facilities ")

        return facilities

    def __extract_state(self):
        """

        :rtype: State
        """
        log.info("Extracting state")
        try:
            state = State.objects.get(
                name=self.__get_cell_value(
                    mappings.STATE_NAME))
            log.info("old state %s", state.name)
        except State.DoesNotExist:
            state = State()
            state.name = self.__get_cell_value(mappings.STATE_NAME)
            log.info("new state %s ", state.name)
        return state

    def __extract_room_info(
            self,
            price_acacia,
            price_oak,
            price_maple,
            price_mahogany,
            room_amenity_data):
        """
        Extract room info and save it in the db
        :param hotel: The hotel associated with these rooms
        :return: (Room, room amenities list)
        """
        rooms = []
        rooms_amenities = {}
        try:
            # facility_obj = Facility.objects.filter(
            #     Q(catalog_mapped_name='tea_coffee') | Q(
            #         catalog_mapped_name='wifi') | Q(catalog_mapped_name='kettle') | Q(
            #         catalog_mapped_name='water') | Q(catalog_mapped_name='coffee_table') | Q(
            #         catalog_mapped_name='treebo_toiletries'))
            facility_obj = []
            if price_acacia > 0:
                acacia_facilities = room_amenity_data['acacia_facilities']
                log.info("acacia_facilities %s", acacia_facilities)
                acacia, acacia_facility = self.__extract_acacia(
                    price_acacia, acacia_facilities, facility_obj)
                log.info("acacia_facility %s", acacia_facility)
                if acacia is not None:
                    rooms.append(acacia)
                    if acacia_facility:
                        rooms_amenities[acacia.room_type_code] = acacia_facility
            if price_oak > 0:
                oak_facilities = room_amenity_data['oak_facilities']
                oak, oak_facility = self.__extract_oak(
                    price_oak, oak_facilities, facility_obj)
                if oak is not None:
                    rooms.append(oak)
                    if oak_facility:
                        rooms_amenities[oak.room_type_code] = oak_facility

            if price_maple > 0:
                maple_facilities = room_amenity_data['maple_facilities']
                maple, maple_facility = self.__extract_maple(
                    price_maple, maple_facilities, facility_obj)
                if maple is not None:
                    rooms.append(maple)
                    if maple_facility:
                        rooms_amenities[maple.room_type_code] = maple_facility

            if price_mahogany > 0:
                mahogany_facilities = room_amenity_data['mahogany_facilities']
                mahogany, mahogany_facility = self.__extract_mahogany(
                    price_mahogany, mahogany_facilities, facility_obj)
                if mahogany is not None:
                    rooms.append(mahogany)
                    if mahogany_facility:
                        rooms_amenities[mahogany.room_type_code] = mahogany_facility

            log.info(
                "type for rooms_amenities %s, dict for room amenities %s",
                type(rooms_amenities),
                rooms_amenities)
        except Exception:
            log.exception("exception in extracting room info")

        return rooms, rooms_amenities

    def __is_room_exists(self, mapping):
        quantity = self.__get_cell_value(mapping, True)
        if quantity is 0:
            return False
        return True

    def __extract_acacia(self, price, acacia_facilities, facility_obj):
        if self.__is_room_exists(mappings.ACACIA_ROOM_COUNT):
            log.info("room_count %s", mappings.ACACIA_ROOM_COUNT)
            room = Room()
            room.room_type_code = 'Acacia'
            room.room_type = 'Acacia (Solo)'
            room.price = price
            room.quantity = self.__get_cell_value(
                mappings.ACACIA_ROOM_COUNT, True)
            room.max_guest_allowed = self.__get_cell_value(
                mappings.ACACIA_MAX_GUEST_COUNT, True)
            room.max_adult_with_children = self.__get_cell_value(
                mappings.ACACIA_MAX_ADULT_WITH_CHILDREN, True)
            room.description = (
                self.__get_cell_value(
                    mappings.ACACIA_DESCRIPTION))
            room.available = room.quantity
            room.base_rate = room.price
            acacia_facility = self.__extract_facilities(
                acacia_facilities, facility_obj)
            return room, acacia_facility
        else:
            return None, None

    def __extract_oak(self, price, oak_facilities, facility_obj):
        if self.__is_room_exists(mappings.OAK_ROOM_COUNT):
            room = Room()
            room.room_type_code = 'Oak'
            room.room_type = 'Oak (Standard)'
            room.price = price
            room.quantity = self.__get_cell_value(
                mappings.OAK_ROOM_COUNT, True)
            room.max_guest_allowed = self.__get_cell_value(
                mappings.OAK_MAX_GUEST_COUNT, True)
            room.max_adult_with_children = self.__get_cell_value(
                mappings.OAK_MAX_ADULT_WITH_CHILDREN, True)
            room.description = (
                self.__get_cell_value(
                    mappings.OAK_DESCRIPTION))
            room.available = room.quantity
            room.base_rate = room.price
            oak_facility = self.__extract_facilities(
                oak_facilities, facility_obj)
            return room, oak_facility
        else:
            return None, None

    def __extract_maple(self, price, maple_facilities, facility_obj):
        if self.__is_room_exists(mappings.MAPLE_ROOM_COUNT):
            room = Room()
            room.room_type_code = 'Maple'
            room.room_type = 'Maple (Deluxe)'
            room.price = price
            room.quantity = self.__get_cell_value(
                mappings.MAPLE_ROOM_COUNT, True)
            room.max_guest_allowed = self.__get_cell_value(
                mappings.MAPLE_MAX_GUEST_COUNT, True)
            room.max_adult_with_children = self.__get_cell_value(
                mappings.MAPLE_MAX_ADULT_WITH_CHILDREN, True)
            room.description = (
                self.__get_cell_value(
                    mappings.MAPLE_DESCRIPTION))
            room.available = room.quantity
            room.base_rate = room.price
            maple_facility = self.__extract_facilities(
                maple_facilities, facility_obj)
            return room, maple_facility
        else:
            return None, None

    def __extract_mahogany(self, price, mahogany_facilities, facility_obj):
        if self.__is_room_exists(mappings.MAHOGANY_ROOM_COUNT):
            room = Room()
            room.room_type_code = 'Mahogany'
            room.room_type = 'Mahogany (Premium)'
            room.price = price
            room.quantity = self.__get_cell_value(
                mappings.MAHOGANY_ROOM_COUNT, True)
            room.max_guest_allowed = self.__get_cell_value(
                mappings.MAHOGANY_MAX_GUEST_COUNT, True)
            room.max_adult_with_children = self.__get_cell_value(
                mappings.MAHOGANY_MAX_ADULT_WITH_CHILDREN, True)
            room.description = (
                self.__get_cell_value(
                    mappings.MAHOGANY_DESCRIPTION))
            room.available = room.quantity
            room.base_rate = room.price
            mahogany_facility = self.__extract_facilities(
                mahogany_facilities, facility_obj)
            return room, mahogany_facility
        else:
            return None, None

    def __extract_railway_directions(self, city, hotel):
        """

        :rtype: (Landmark, Directions)
        """
        railway_name = None
        try:
            railway_name = self.__get_cell_value(mappings.RAILWAY_NAME)
            railway_landmark = Landmark.objects.get(
                name=railway_name, city=city)
        except Landmark.DoesNotExist:
            railway_landmark = Landmark()
            railway_landmark.name = railway_name
            railway_landmark.latitude = float(
                self.__get_cell_value_lat_long(
                    mappings.LANDMARK_RAILWAY_LAT).replace(
                    ',', ''))
            railway_landmark.longitude = float(
                self.__get_cell_value_lat_long(
                    mappings.LANDMARK_RAILWAY_LONG).replace(
                    ',', ''))
            railway_landmark.description = 'City Railway Station'
            railway_landmark.status = common_constants.DISPLAY
            log.info("extracted railway landmark")
        except MultipleObjectsReturned:
            railway_landmark = Landmark.objects.filter(
                name=railway_name, city=city).first()

        railway_directions = Directions()
        railway_directions.distance = HotelUploadService.__strip_distance(
            self.__get_cell_value(mappings.DIRECTIONS_RAILWAY_DISTANCE))
        railway_directions.directions_for_email_website = self.__parse_directions(
            self.__get_cell_value(mappings.DIRECTIONS_RAILWAY_EMAIL))
        railway_directions.directions_for_sms = railway_directions.directions_for_email_website

        log.info("extracted railway landmark directions")
        return railway_landmark, railway_directions

    def __extract_airport_directions(self, city, hotel):
        """

        :rtype: (Landmark, Directions)
        """
        try:
            airport_name = self.__get_cell_value(mappings.AIRPORT_NAME)
            airport_landmark = Landmark.objects.get(
                name=airport_name, city=city)
        except Landmark.DoesNotExist:
            airport_landmark = Landmark()
            airport_landmark.name = airport_name
            airport_landmark.description = 'City Airport'
            airport_landmark.latitude = float(
                self.__get_cell_value_lat_long(
                    mappings.LANDMARK_AIRPORT_LAT).replace(
                    ',', ''))
            airport_landmark.longitude = float(
                self.__get_cell_value_lat_long(
                    mappings.LANDMARK_AIRPORT_LONG).replace(
                    ',', ''))
            airport_landmark.status = common_constants.DISPLAY
        except MultipleObjectsReturned:
            airport_landmark = Landmark.objects.filter(
                name=airport_name, city=city).first()

            log.info("extracted airport landmark")

        airport_directions = Directions()
        airport_directions.distance = HotelUploadService.__strip_distance(
            self.__get_cell_value(mappings.DIRECTIONS_AIRPORT_DISTANCE))
        airport_directions.directions_for_email_website = self.__parse_directions(
            self.__get_cell_value(mappings.DIRECTIONS_AIRPORT_EMAIL))
        airport_directions.directions_for_sms = airport_directions.directions_for_email_website

        log.info("extracted airport directions")

        return airport_landmark, airport_directions

    def __extract_landmark_directions(self, city, hotel):
        # LANDMARK 1
        """

        :rtype: (Landmark, Directions, Landmark, Directions)
        """
        landmark_name = self.__get_cell_value(mappings.LANDMARK_1_NAME)
        if not landmark_name or len(landmark_name) == 0:
            return
        else:
            try:
                landmark_1 = Landmark.objects.get(
                    name=landmark_name, city=city)
            except Landmark.DoesNotExist:
                log.info("caught from DoesNotExist")
                landmark_1 = Landmark()
                landmark_1.name = landmark_name
                landmark_1.description = ''
                landmark_1.latitude = float(
                    self.__get_cell_value_lat_long(
                        mappings.LANDMARK_1_LAT).replace(
                        ',', ''))
                landmark_1.longitude = float(
                    self.__get_cell_value_lat_long(
                        mappings.LANDMARK_1_LONG).replace(
                        ',', ''))
                landmark_1.status = common_constants.DISPLAY
                landmark_1.popularity = 1
            except MultipleObjectsReturned:
                landmark_1 = Landmark.objects.filter(
                    name=landmark_name, city=city).first()

            log.info("extracted landmark_1")

            landmark_1_directions = Directions()
            landmark_1_directions.distance = HotelUploadService.__strip_distance(
                self.__get_cell_value(mappings.LANDMARK_1_DISTANCE))
            landmark_1_directions.directions_for_email_website = self.__parse_directions(
                self.__get_cell_value(mappings.LANDMARK_1_EMAIL_DIRECTIONS))
            landmark_1_directions.directions_for_sms = landmark_1_directions.directions_for_email_website

            log.info("extracted landmark_1 directions")

        # LANDMARK 2
        landmark_name = self.__get_cell_value(mappings.LANDMARK_2_NAME)
        if not landmark_name or len(landmark_name) == 0:
            return
        else:
            try:
                landmark_2 = Landmark.objects.get(
                    name=landmark_name, city=city)
            except Landmark.DoesNotExist:
                landmark_2 = Landmark()
                landmark_2.name = landmark_name
                landmark_2.description = ''
                landmark_2.latitude = float(
                    self.__get_cell_value_lat_long(
                        mappings.LANDMARK_2_LAT).replace(
                        ',', ''))
                landmark_2.longitude = float(
                    self.__get_cell_value_lat_long(
                        mappings.LANDMARK_2_LONG).replace(
                        ',', ''))
                landmark_2.status = common_constants.DISPLAY
                landmark_2.popularity = 0
            except MultipleObjectsReturned:
                landmark_2 = Landmark.objects.filter(
                    name=landmark_name, city=city).first()

            log.info("extracted landmark_2")

            landmark_2_directions = Directions()
            landmark_2_directions.distance = HotelUploadService.__strip_distance(
                self.__get_cell_value(mappings.LANDMARK_2_DISTANCE))
            landmark_2_directions.directions_for_email_website = self.__parse_directions(
                self.__get_cell_value(mappings.LANDMARK_2_EMAIL_DIRECTIONS))
            landmark_2_directions.directions_for_sms = landmark_2_directions.directions_for_email_website

            log.info("extracted landmark_2 directions")
        return landmark_1, landmark_1_directions, landmark_2, landmark_2_directions

    def __extract_locality(self, city, hotel):
        try:
            locality = Locality.objects.get(
                name=self.__get_cell_value(
                    mappings.LOCALITY_NAME), city=city)
        except Locality.DoesNotExist:
            locality = Locality()
            locality.name = self.__get_cell_value(mappings.LOCALITY_NAME)
            locality.pincode = self.__get_cell_value(
                mappings.LOCALITY_PINCODE, True)
            locality.latitude = hotel.latitude
            locality.longitude = hotel.longitude
            log.info("extracted new locality")
        except MultipleObjectsReturned:
            locality = Locality.objects.filter(
                name=self.__get_cell_value(
                    mappings.LOCALITY_NAME),
                city=city).first()
        return locality

    def __extract_city(self):
        """
        Returns a new instance of City & CityAlias models if a new city is added,
        else returns the existing city.
        :return (City, CityAlias):
        """
        city_name = self.__get_cell_value(mappings.CITY_NAME)
        city_alias = CityAlias.objects.filter(
            Q(city=city_name) | Q(alias=city_name)).first()
        if city_alias is not None:
            cities = city_data_service.filter_cities(name=city_alias.city)
            city = cities[0] if cities else None
        else:
            city = City()
            city.name = self.__get_cell_value(mappings.CITY_NAME)
            city.description = (
                self.__get_cell_value(
                    mappings.CITY_DESCRIPTION))
            city.tagline = (self.__get_cell_value(mappings.CITY_TAGLINE))
            city.subscript = (self.__get_cell_value(mappings.CITY_SUBSCRIPT))
            city.status = common_constants.ENABLED
            lat = self.__get_cell_value_lat_long(
                mappings.CITY_LAT).replace(',', '')
            city.city_latitude = 0 if len(lat) is 0 else float(lat)
            longitude = self.__get_cell_value_lat_long(
                mappings.CITY_LONG).replace(',', '')
            city.city_longitude = 0 if len(
                longitude) is 0 else float(longitude)

            city_alias = CityAlias()
            city_alias.city = city.name
            city_alias.alias = city.name
        log.info("extracted city %s %s", city, type(city))
        return city, city_alias

    def __extract_hotel(self, hotelogix_id, hotelogix_name):
        """
        Extract the hotel from the worksheet
        :rtype: Hotel
        :param sheet:
        """
        try:
            hotel = Hotel.all_hotel_objects.filter(
                hotelogix_id=hotelogix_id).first()
            if hotel is not None:
                raise HotelUploadException("Hotel already exists")
        except Exception as e:
            log.exception(e)
            raise e
        hotel = Hotel()
        hotel.hotelogix_id = str(hotelogix_id)
        hotel.name = self.__get_cell_value(mappings.HOTEL_NAME)
        hotel.hotelogix_name = hotelogix_name + \
            '(' + str(hotel.hotelogix_id) + ')'
        hotel.tagline = (self.__get_cell_value(mappings.HOTEL_TAGLINE))
        hotel.description = (self.__get_cell_value(mappings.HOTEL_DESCRIPTION))
        hotel.phone_number = str(
            int(self.__get_cell_value(mappings.HOTEL_PHONE_NUMBER, True)))
        hotel.room_count = self.__get_cell_value(mappings.ACACIA_ROOM_COUNT,
                                                 True) + self.__get_cell_value(mappings.OAK_ROOM_COUNT,
                                                                               True) + self.__get_cell_value(mappings.MAPLE_ROOM_COUNT,
                                                                                                             True) + self.__get_cell_value(mappings.MAHOGANY_ROOM_COUNT,
                                                                                                                                           True)
        hotel.latitude = self.__get_cell_value(mappings.HOTEL_LAT, True)
        hotel.longitude = self.__get_cell_value(mappings.HOTEL_LONG, True)
        hotel.checkin_time = datetime.time(12, 0)
        hotel.checkout_time = datetime.time(11, 0)
        street = (self.__get_cell_value(mappings.HOTEL_STREET))
        hotel.street = street
        hotel.price_increment = 300
        log.info("extracted hotel")
        return hotel

    @staticmethod
    def __strip_street_of_city_pincode(street, city):
        """
        :param str street:
        :rtype str, str
        :return Stripped street and pincode found in the street
        """
        match = re.match(".*(\d{6,6}).*", street)
        new_street = street
        if match:
            new_street = re.sub(
                r"(,|\s|-)*\s*" +
                city +
                "\s*-*\s*\d*.*",
                "",
                street,
                flags=re.IGNORECASE)
        return new_street, match.group(1)

        # def download_images_from_drive(self, hotel_name, rooms):
        #     """
        #     Download images from Google Drive
        #     """
        # print "downloading images from drive"
        # with open(Command.file_path + '/server_secret.json') as key_file:
        #     data = json.load(key_file)
        # credentials = SignedJwtAssertionCredentials(data["client_email"], data["private_key"],
        #                                             scope="https://www.googleapis.com/auth/drive")
        # http = Http()
        # credentials.authorize(http)

        # service = build('drive', 'v2', http=http)
        # param = dict()
        # param["q"] = "title='Hotel Images - Public' and sharedWithMe=true"

        # drive_folder = service.files().list(**param).execute()

        # param["q"] = "title='{0}.zip'".format(hotel_name)
        # hotel_zip = service.children().list(folderId=drive_folder["items"][0]["id"], **param).execute()

        # download_file_location = open(config.DOWNLOAD_FILE_LOCATION + hotel_name.replace(" ", "_") + ".zip", "w")
        # request = service.files().get_media(fileId=hotel_zip["items"][0]["id"])
        # media_request = googleapiclient.http.MediaIoBaseDownload(download_file_location, request)
        # while True:
        #     try:
        #         download_progress, done = media_request.next_chunk()
        #     except errors.HttpError, error:
        #         print 'An error occurred: %s' % error
        #         return
        #     if download_progress:
        #         print 'Download Progress: %d%%' % int(download_progress.progress() * 100)
        #     if done:
        #         print 'Download Complete'
        #         break
        # download_file_location.close()

    def __unzip_hotel_images(self, hotel_name):
        """
        Unzip hotel images to a folder with same name as hotel
        :rtype : str
        :returns: Name of folder in which images were unzipped
        """
        log.info("unzipping file")
        folder_name = self.__settings.HOTEL_UPLOAD_FILE_LOCATION + hotel_name
        zip = zipfile.ZipFile(self.__images, 'r')
        zip.extractall(folder_name)
        return folder_name

    def __upload_to_s3(self, hotel_name, rooms):
        """
        Upload file to s3
        """
        folder_name = self.__unzip_hotel_images(hotel_name)
        subdirs = os.listdir(folder_name)
        actual_folder = ''
        for f in subdirs:
            if f != '__MACOSX' and os.path.isdir(folder_name + '/' + f):
                actual_folder = f

        folder_name = folder_name + '/' + actual_folder

        log.info("uploading to S3")
        conn_s3 = S3Connection(host='s3-ap-southeast-1.amazonaws.com')
        bucket = conn_s3.get_bucket(self.__settings.AWS_STORAGE_BUCKET_NAME)
        log.info("S3 connection established")
        for root, dirs, files in os.walk(folder_name):
            for name in files:
                if name.lower().endswith(".jpg") or name.lower().endswith(
                        ".jpeg") or name.lower().endswith(".png"):
                    bucket_key = Key(bucket)
                    path = hotel_name + \
                        root.replace(folder_name, "") + "/" + name
                    path = path.replace(" ", "_")
                    bucket_key.key = self.__settings.S3_STATIC_FILE_PATH + path
                    bucket_key.set_contents_from_filename(
                        root + "/" + name, replace=True, policy='public-read')
                    image = Image()
                    image.hotel = rooms[0].hotel
                    image.tagline = hotel_name
                    image.url = "./" + path
                    for room in rooms:
                        # if room.room_type_code.lower() ==
                        # root[len(folder_name) + 1:].lower():
                        if room.room_type_code.lower() in name.lower():
                            image.room = room
                            # image.room = next((room for room in rooms if room.room_type_code.lower() is str(dirs[0].lower())),
                            #                   None)
                    image.save()

    @staticmethod
    def __sanitize_image_name(hotel_name, image_file_name):
        image_path = hotel_name + "/" + image_file_name
        image_path = image_path.replace(" ", "_")
        return image_path

    def __get_cell_value(self, mapping, as_num=False):
        """
        Get the value of the cell using the mapping passed
        :rtype: string
        """
        val = 0 if as_num else ''
        row_value = self.__sheet.cell(
            row=mapping['row'], column=mapping['col'])
        if row_value is not None and row_value.value is not None:
            val = row_value.value
            if not as_num:
                val = val.strip()
                val = HotelUploadService.__sanitize(val)
            else:
                if isinstance(val, str) or isinstance(val, str):
                    val = 0
        return val

    def __get_cell_value_lat_long(self, mapping):
        """
        Get the value of the cell using the mapping passed
        :rtype: string
        """
        val = None
        row_value = self.__sheet.cell(
            row=mapping['row'], column=mapping['col'])
        if row_value is not None and row_value.value is not None:
            val = row_value.value
            if isinstance(val, str) or isinstance(val, str):
                val = str(val).strip()
                val = HotelUploadService.__sanitize(val)
            if isinstance(val, tuple):
                val = val[0]

        return str(val)

    @staticmethod
    def __sanitize(row_value):
        row_value_lower = row_value.lower()
        if row_value_lower == 'n/a' or row_value_lower == 'na' or row_value_lower == '#n/a' or row_value_lower == '#na':
            return ''
        return row_value

    @staticmethod
    def __strip_distance(val):
        if val is not None:
            if any(x in val.lower() for x in ["km", "kilometer"]):
                val = float(HotelUploadService.__strip_metric(val))
            elif any(x in val.lower() for x in ["meter", "mtr", "mt", "m"]):
                val = float(HotelUploadService.__strip_metric(val)) / 1000
            else:
                try:
                    val = float(val)
                except BaseException:
                    val = None
        return val

    @staticmethod
    def __strip_metric(val):
        return re.sub(
            r"\s{0,}(meter|mtr|mt|m|km|kilometer)[s]{0,}",
            "",
            val,
            flags=re.IGNORECASE)

    def validate(
            self,
            airport_directions,
            airport_landmark,
            city,
            city_desc,
            city_alias,
            facilities,
            highlights,
            hotel,
            landmark_1,
            landmark_1_directions,
            landmark_2,
            landmark_2_directions,
            locality,
            railway_directions,
            railway_landmark,
            rooms,
            state):
        warnings = []

        if hotel:
            if not hotel.hotelogix_id or len(hotel.hotelogix_id) != 5:
                warnings.append({
                    "Hotelogix Id": "Invalid hotelogix id provided"
                })
            if not hotel.hotelogix_name:
                warnings.append({
                    "Hotelogix Name": "No hotogix name provided"
                })
            if not hotel.street:
                warnings.append({
                    "hotel street": "Hotel address not found"
                })
            if not hotel.phone_number:
                warnings.append({
                    "hotel phone number": "Hotel phone number not found"
                })
            if not hotel.description:
                warnings.append({
                    "hotel description": "No hotel description found"
                })
            if not hotel.latitude or not hotel.longitude:
                warnings.append({
                    "hotel coordinates": "Hotel coordinates are invalid"
                })
        else:
            warnings.append({
                "hotel": "No hotel could be found!!"
            })

        if not rooms or len(rooms) == 0:
            warnings.append({
                "rooms": "No rooms could be found!!"
            })
        else:
            for room in rooms:
                if room.price < 999:
                    warnings.append({
                        room.room_type_code + " price": "Price for room is less than 999"
                    })
                if room.max_adult_with_children <= room.max_guest_allowed:
                    warnings.append(
                        {
                            room.room_type_code +
                            " guests": "Max guests with children is less than or equal to max adults allowed"})
                if not room.room_area:
                    warnings.append({
                        room.room_type_code + " area": "Size of room not specified"
                    })

        if city and city.id is None:
            warnings.append({
                "city": "New city " + city.name + " is being added!"
            })
            if city_desc is None:
                warnings.append({
                    'city desc': 'City Description has not been added!'
                })

        if not facilities or len(facilities) < 2:
            warnings.append({
                "facilities": "Less than 2 facilities have been selected"
            })

        if not highlights or len(highlights) < 3:
            warnings.append({
                "trilights": "3 trilights not entered"
            })

        if airport_landmark is not None:
            if airport_directions:
                if airport_directions.distance > 50:
                    warnings.append({
                        "airport distance": "Distance greater than 50 Kms"
                    })
                if not airport_directions.directions_for_sms or not airport_directions.directions_for_email_website:
                    warnings.append({
                        "airport directions": "No airport directions are specified"
                    })

            else:
                warnings.append({
                    "airport directions": "No airport directions are specified"
                })

        if railway_landmark is not None:
            if railway_directions:
                if railway_directions.distance > 50:
                    warnings.append({
                        "railway distance": "Distance greater than 50 Kms"
                    })
                if not railway_directions.directions_for_sms or not railway_directions.directions_for_email_website:
                    warnings.append({
                        "railway directions": "No railway directions are specified"
                    })

            else:
                warnings.append({
                    "railway directions": "No railway directions are specified"
                })

        if landmark_1 is not None:
            if landmark_1_directions:
                if landmark_1_directions.distance > 50:
                    warnings.append({
                        landmark_1.name + " distance": "Distance greater than 50 Kms"
                    })
                if not landmark_1_directions.directions_for_sms or not landmark_1_directions.directions_for_email_website:
                    warnings.append({
                        landmark_1.name + " directions": "No directions are specified"
                    })

            else:
                warnings.append({landmark_1.name +
                                 " directions": "No directions are specified"})

        if landmark_2 is not None:
            if landmark_2_directions:
                if landmark_2_directions.distance > 50:
                    warnings.append({
                        landmark_2.name + " distance": "Distance greater than 50 Kms"
                    })
                if not landmark_2_directions.directions_for_sms or not landmark_2_directions.directions_for_email_website:
                    warnings.append({
                        landmark_2.name + " directions": "No directions are specified"
                    })

            else:
                warnings.append({landmark_2.name +
                                 " directions": "No directions are specified"})

        return warnings

    def __save_and_upload_city_image(self, city, city_image):
        log.info("uploading city image to S3")
        conn_s3 = S3Connection(host='s3-ap-southeast-1.amazonaws.com')
        bucket = conn_s3.get_bucket(self.__settings.AWS_STORAGE_BUCKET_NAME)
        log.info("S3 connection established")
        bucket_key = Key(bucket)
        path = city_image.name.replace(" ", "_")
        bucket_key.key = self.__settings.S3_CITY_FOLDER_PATH + path
        bucket_key.set_contents_from_file(
            city_image, replace=True, policy='public-read')
        return "/".join([".", self.__settings.S3_CITY_FOLDER_PATH + path])
