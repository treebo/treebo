# -*- coding: utf-8 -*-


from django.conf.urls import url

from common.job.jobs.payment_dropoff_notification import SoftBookingJobCronAPI

urlpatterns = [
    url(r'^soft-booking-job', SoftBookingJobCronAPI.as_view(), name='soft-booking-job-cron'),
]
