from base import log_args
from dbcommon.models.miscellanous import Image
from data_services.decorators import SingletonDecorator


@SingletonDecorator
class ImageRepository():

    def __init__(self, image_web_repository):
        self.image_web_repository = image_web_repository

    def get_all_images(self):
        return Image.objects.all()

    def filter_images(self, *args, **kwargs):
        return Image.objects.filter(**kwargs)

    def get_single_image(self, *args, **kwargs):
        return Image.objects.get(**kwargs)

    def get_images_for_hotels(self, hotels, **kwargs):
        return Image.objects.filter(hotel__in=hotels, **kwargs)

    def save_image(self):
        pass

    def get_room_type_from_image(self, image):
        if hasattr(image, 'room'):
            if hasattr(image.room, 'room_type_code'):
                return image.room.room_type_code
            return image.room
        return None

    def get_images_by_hotel_id_with_room(self, hotel_id):
        return Image.objects.filter(hotel_id=hotel_id).select_related('room')
