from django.core.cache import cache

CACHE_KEY = 'TRUE_CALLER_'


class CacheService:
    NOT_SET = 'Not Set'
    FAILED_TO_SET = 'Failed To Set'

    @staticmethod
    def set(key, value=None):
        cache.set(
            key=CACHE_KEY + str(key),
            value=value if value else CacheService.NOT_SET,
            timeout=24*60*60)

    @staticmethod
    def get(key):
        return cache.get(CACHE_KEY + str(key))

