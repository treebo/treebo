from django.conf import settings

THIRD_PARTY_TRIGGER_TASK = [
    'apps.bookings.tasks.add',
    'apps.hotels.tasks.email_hx',
    'apps.pricing.tasks.sync_prices',
    'apps.pricing.tasks.updatePrices',
    'apps.pricing.tasks.updateRatePlanFromChannelManager',
    'apps.profiles.tasks.send_registration_email',
    'apps.referral.tasks.generate_referral_code',
    'apps.referral.tasks.send_sign_up_event',
    'apps.referral.tasks.send_transaction_event',
    'apps.third_party.tasks.trivago_fetch_hotel_data_and_email',
    'apps.third_party.tasks.trivago_upload_hotel_data_automatic',
    'apps.bookingstash.tasks.update_third_party_availability',
]


class MyRouter(object):
    def route_for_task(self, task, args=None, kwargs=None):
        if task in THIRD_PARTY_TRIGGER_TASK:
            return {'exchange': settings.CELERY_THIRD_PARTY_QUEUE,
                    'exchange_type': 'direct',
                    'routing_key': settings.CELERY_THIRD_PARTY_QUEUE}

        if task == settings.CELERY_CREATE_COUPON_TASK_NAME:
            return {'exchange': settings.CELERY_CREATE_COUPON_QUEUE,
                    'exchange_type': 'direct',
                    'routing_key': settings.CELERY_CREATE_COUPON_QUEUE}

        if task == settings.CELERY_TRIVAGO_HOTEL_DETAILS:
            return {'exchange': settings.TRIVAGO_EXCHANGE_NAME,
                    'exchange_type': 'direct',
                    'routing_key': settings.CELERY_TRIVAGO_HOTEL_DETAILS}

        if task == settings.CELERY_DYNAMIC_ADS_TASK_NAME:
            return {
                'exchange':      settings.CELERY_DYNAMIC_ADS_QUEUE_NAME,
                'exchange_type': 'direct',
                'routing_key':   settings.CELERY_DYNAMIC_ADS_QUEUE_NAME
            }

        # All other tasks will use default exchange - 'celery'
        return None
