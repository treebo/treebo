from rest_framework import serializers
from apps.common.validators import CustomValidator as custom_validator


class OtpIsVerifyDTO(serializers.Serializer):
    phone_number = serializers.CharField(max_length=50)

    def validate_phone_number(self, phone_number):
        return custom_validator.normalise_phone_number(phone_number)
