class Helper:
    def __init__(self, **kwargs):
        pass

    EMAIL_TEMPLATE = '''
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head/>
        <body>
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td>
                        <table style="border: 1px solid black; border-collapse:collapse;" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
                            <tr style="border: 1px solid black; border-collapse:collapse;">
                                <th style="border: 1px solid black; border-collapse:collapse;">Name</th>
                                <th style="border: 1px solid black; border-collapse:collapse;">Value</th>
                            </tr>
                            {0}
                        </table>
                    </td>
                </tr>
            </table>
        </body>
    </html>'''
    TR = '''<tr><td style="border: 1px solid black; border-collapse:collapse;">{0}</td><td style="border: 1px solid black; border-collapse:collapse;">{1}</td></tr>'''

    @staticmethod
    def getEmailContent(jsonData):
        rows = ""
        for k in jsonData:
            rows += Helper.TR.format(k, jsonData[k])
        content = Helper.EMAIL_TEMPLATE.format(rows)
        return content
