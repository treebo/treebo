from django.contrib import admin

from apps.featuregate.models import Page, PageVariables, ConstraintKeywords, ConstraintOperators, \
    ConstraintExpression, VariableCondition, Constraints
from dbcommon.admin import ReadOnlyAdminMixin


@admin.register(Page)
class PageAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'page_name')


@admin.register(PageVariables)
class PageVariableAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'page', 'name', 'type')


@admin.register(ConstraintKeywords)
class ConstraintKeywordAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'keyword', 'type', 'description')


@admin.register(ConstraintOperators)
class ConstraintOperatorAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'operator', 'description')


@admin.register(ConstraintExpression)
class ConstraintExpressionAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'expression')


@admin.register(VariableCondition)
class VariableConditionAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'expression',
        'page_variable',
        'final_value',
        'active')


@admin.register(Constraints)
class ConstraintAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'keyword', 'operation', 'value_one', 'value_two')
