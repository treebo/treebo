'use strict';

import $ from 'jquery';
import {Observable} from "rx";
import BaseView from './treebolibs/baseview';
import parsley from 'parsleyjs';
import logger from './treebolibs/logger';
import '../css/resetpassword.less';

let treejax = require('./treebolibs/treejax');



let ResetView = Object.create(BaseView);

ResetView.pageLoaded = () => {
	ResetView.bindSubmit();

	logger.log(" Reset page loaded");
}

ResetView.bindSubmit = () => {
	$("#resetSubmit").on('click', (e)=>{
	    	const resetParsley = $('#resetForm').parsley();
	    	resetParsley.on('form:submit',() => {

	    		const data = {
					slug:rt.slug,
	    			password:$('#pass').val().trim(),
	    			confirm:$('#repeatpass').val().trim(),
	    			csrfmiddlewaretoken:$('input[name="csrfmiddlewaretoken"]').val()
	    		};

	    		ResetView.resetPassword(data);
	            return false;
	        })
	});
}

ResetView.resetPassword = (data) => {
	const reject = reject || function (treeboError) { console.log(treeboError); };
	const p = treejax({
                url: '/api/v1/auth/reset-password/',
                type: 'POST',
                data
            }).then((data, status, xhr) => {
            	$('#resetForm').hide();
		       const $elem = $('<div class="reset__success alert alert--success">Password changed successfully. Click here to <a class="anchor" href="/login/">login</a></div>');
               $('.resetmsg').html($elem).removeClass('hide');

            },treeboError => {
				const $elem = $(`<div class="alert alert--error">${treeboError.message}</div>`);
				$('.resetmsg').html($elem).removeClass('hide');
			});

            return p;
}

let resetConfig = { name:'', widgets:[]};
let resetView = ResetView.setup(resetConfig);
