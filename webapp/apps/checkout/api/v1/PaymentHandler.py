import logging

import requests
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect

from apps.checkout.bookinghelper import BookingHelper
from apps.checkout.models import BookingRequest
from apps.checkout.service import checkout_service
from apps.checkout.tasks import send_booking_confirmation_notification
from apps.discounts.models import DiscountCoupon
from services.restclient.bookingrestclient import BookingClient
from services.send_exception_mail import send_exception_mail

logger = logging.getLogger(__name__)


class PaymentHandler:

    def __init__(self):
        pass

    def _update_payments(self, amount, order_id, payment_id, payment_mode):
        try:
            BookingClient.update_payment_details(
                payment_id, order_id, amount, payment_mode)
        except requests.HTTPError as http_error:
            logger.exception(
                "Unable to update payment info for booking {0}".format(order_id))

    def _confirm_booking(self, order_id, booking_id, request):
        booking_request = BookingRequest.objects.get(pk=booking_id)

        try:
            BookingClient.confirm_booking(booking_request.order_id)
            if booking_request.coupon_code and booking_request.coupon_apply:
                try:
                    coupon_obj = DiscountCoupon.objects.get(
                        code__iexact=booking_request.coupon_code)
                    coupon_obj.mark_used_for_transaction()
                except DiscountCoupon.DoesNotExist:
                    logger.exception(
                        "Unable to find coupon %s",
                        booking_request.coupon_code)

            send_booking_confirmation_notification.delay(
                booking_request.order_id)

            if booking_request.is_audit:
                checkout_service.make_fot_reservation(booking_request)
        except requests.HTTPError as http_error:
            return self._handle_exception(booking_request, http_error, request)

        if booking_request.instant_booking:
            return redirect(
                reverse(
                    "pages:order", kwargs={
                        "order_id": booking_request.order_id}))
        else:
            return redirect(
                reverse('pages:transaction') +
                '?referenceid={0}&payment=successful'.format(
                    booking_request.id))

    def _handle_exception(self, booking_request, http_error, request):
        logger.exception(
            "Unable to confirm booking {0}".format(
                booking_request.order_id))
        send_exception_mail(http_error, request)
        BookingHelper.track_booking_failure(
            'Booking Unsuccessful', booking_request, request)

        return HttpResponseRedirect(
            BookingHelper.constructItineraryUrlOnError(
                request, "Unable to confirm your booking"))
