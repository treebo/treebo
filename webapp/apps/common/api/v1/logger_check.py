import logging

from rest_framework.response import Response
from rest_framework.views import APIView

from base import log_args

logger = logging.getLogger(__name__)


class LoggerCheck(APIView):

    @log_args()
    def get(self, request):
        logger.debug("Debug Log")
        logger.info("Info Log")
        logger.warning("Warn log")
        logger.error("Error Log")
        return Response(data={"success": True}, status=200)
