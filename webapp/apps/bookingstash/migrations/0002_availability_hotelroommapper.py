# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0014_facility_rooms'),
        ('bookingstash', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HotelRoomMapper',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('mm_hotel_code', models.CharField(max_length=200)),
                ('mm_room_code', models.CharField(max_length=200)),
                ('room_id', models.IntegerField()),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
