from django.conf.urls import url

from apps.checkout.api.v4.remove_coupon import RemoveCoupon
from apps.checkout.api.v4.apply_coupon import ApplyCoupon

from apps.checkout.api.v4.itinerary_page_api import ItineraryPageAPI

from webapp.apps.checkout.api.v4.confirmed_booking_details import ConfirmedBookingDetail

app_name = 'checkout_v4'
urlpatterns = [url(r'^coupon/$',
                   ApplyCoupon.as_view(),
                   name='apply-coupon-v4'),
               url(r'^coupon/remove/$',
                   RemoveCoupon.as_view(),
                   name='remove-coupon-v4'),
               url(r'^itinerary/$',
                   ItineraryPageAPI.as_view(),
                   name='itinerary-page-v4'),
               url(r'^confirmed-booking/(?P<order_id>[\w-]+)/',
                   ConfirmedBookingDetail.as_view(),
                   name='confirmed-booking'),
               ]
