import logging
from datetime import datetime, timedelta

from collections import OrderedDict
from django.db.models import Q, Min

from apps.bookingstash.models import Availability
from apps.bookingstash.service.availability_service import get_availability_service
from apps.checkout.models import BookingRequest
from apps.common import date_time_utils
from apps.hotels import utils as hotelUtils
from data_services.respositories_factory import RepositoriesFactory
from . import constants as stashConstants
from . import models as stashModels

logger = logging.getLogger(__name__)

hotel_repository = RepositoriesFactory.get_hotel_repository()
room_data_service = RepositoriesFactory.get_room_repository()


class StashUtils(object):

    @staticmethod
    def get_cheapest_room(
            hotel_id,
            check_in_date_str,
            checkout_date_str,
            room_config):
        room_count = len(room_config)
        checkin_date = datetime.strptime(check_in_date_str, "%Y-%m-%d").date()
        checkout_date = datetime.strptime(checkout_date_str, "%Y-%m-%d").date()
        rooms_available = []
        delta = timedelta(days=1)

        current_hotel = hotelUtils.getHotelObject(hotel_id)
        current_rooms = hotel_repository.get_rooms_for_hotel(current_hotel)
        room_type_max_limits = {}

        room_type_order = {'Acacia': 0, 'Oak': 1, 'Maple': 2, 'Mahogany': 3}

        room_type_indexed_rooms = {}

        for room in current_rooms:
            room_type_max_limits[room.room_type_code] = {
                'totalRooms': room.available, 'maxGuests': room.max_guest_allowed}
            room_type_indexed_rooms[room.room_type_code] = room
        if current_hotel is None:
            return None

        hotelogix_code = current_hotel.hotelogix_id
        room_type_available = {}
        for room_type in list(room_type_max_limits.keys()):
            room_type_available[room_type] = room_type_max_limits[room_type]['totalRooms']

        while checkin_date < checkout_date:
            end_range = checkin_date + delta

            room_type_bookings = {}
            for room_type in list(room_type_max_limits.keys()):
                room_type_bookings[room_type] = 0
            room_type_dnrs = {}
            for room_type in list(room_type_max_limits.keys()):
                room_type_dnrs[room_type] = 0

            rooms_booked = stashModels.ReservationBooking.objects.filter(
                ~Q(status=stashConstants.CANCELLED),
                checkin_date__lte=checkin_date,
                checkout_date__gte=end_range,
                hotel_code=hotelogix_code).prefetch_related(
                'booking_stay')
            logger.info(rooms_booked.query)
            rooms_blocked = stashModels.ReservationRestriction.objects.filter(
                Q(restriction_status=stashConstants.BLOCKED), start_date__lte=checkin_date,
                end_date__gte=end_range,
                hotel_code=hotelogix_code)

            for booked_room in rooms_booked:
                for booking_stay in booked_room.booking_stay.all():
                    room_type_bookings[booking_stay.room_type_code] = room_type_bookings.get(
                        booking_stay.room_type_code, 0) + 1
            for blocked_room in rooms_blocked:
                room_type_dnrs[blocked_room.room_type_code] = room_type_dnrs.get(
                    blocked_room.room_type_code, 0) + 1

            for room_type in list(room_type_max_limits.keys()):
                if room_type_available.get(
                    room_type,
                    0) > (
                    room_type_max_limits[room_type]['totalRooms'] -
                    room_type_bookings.get(
                        room_type,
                        0) -
                    room_type_dnrs.get(
                        blocked_room.room_type_code,
                        0)):
                    room_type_available[room_type] = (
                        room_type_max_limits[room_type]['totalRooms'] -
                        room_type_bookings.get(
                            room_type,
                            0) -
                        room_type_dnrs.get(
                            blocked_room.room_type_code,
                            0))

            checkin_date += delta

        cheapest_room_type = None

        for room_type in list(room_type_max_limits.keys()):
            if room_type_available.get(room_type, 0) >= room_count:
                if StashUtils.room_config_supported(
                        room_type_indexed_rooms[room_type], room_config):
                    if cheapest_room_type is None:
                        cheapest_room_type = room_type
                    elif room_type_order[cheapest_room_type] > room_type_order[room_type]:
                        cheapest_room_type = room_type
        return cheapest_room_type

    @staticmethod
    def room_config_supported(room, room_config):
        for single_room_config in room_config:
            if not hotelUtils.isOccupancySupported(
                    room, single_room_config[0], single_room_config[1]):
                return False
        return True

    @staticmethod
    def room_config_string_to_list(room_config_string):
        rooms_config = []
        for room in room_config_string.split(','):
            room_tuple = room.split('-')
            adults = int(room_tuple[0])
            if len(room_tuple) > 1:
                children = int(room_tuple[1])
            else:
                children = 0
            rooms_config.append((adults, children))
        return rooms_config

    # NOTE: Return value type change. Handle at all palces
    @staticmethod
    def get_cheapest_room_type_from_availability_sync(
            hotels, check_in_date, check_out_date, rooms_config):
        all_room_availabilities = get_availability_service().get_room_type_min_availability_for_all_hotels(
            check_in_date.strftime(date_time_utils.DATE_FORMAT),
            check_out_date.strftime(date_time_utils.DATE_FORMAT))
        room_count = len(rooms_config)

        room_type_order = {'Acacia': 0, 'Oak': 1, 'Maple': 2, 'Mahogany': 3}

        hotel_dict = {}
        hotel_availabilities = {}
        for hotel in hotels:
            hotel_dict[hotel.id] = hotel
            hotel_availabilities[hotel.id] = {}
            all_rooms = hotel_repository.get_rooms_for_hotel(hotel)
            room_type_dict = {}
            for room in all_rooms:
                room_type_dict[room.room_type_code] = {'room': room}
            hotel_availabilities[hotel.id] = OrderedDict(
                sorted(list(room_type_dict.items()), key=lambda rtype: room_type_order[rtype[0]]))

        for singleRoom in all_room_availabilities:
            hotel_id = singleRoom[0]
            room_type = singleRoom[1]
            availability = singleRoom[2]
            if hotel_id in list(hotel_dict.keys()):
                if hotel_id in list(hotel_availabilities.keys()):
                    hotel_availabilities[hotel_id][room_type]['availability'] = availability

        hotel_room = {}
        for hotel in sorted(hotels, key=lambda h: h.id):
            hotel_room[hotel.id] = {}
            cheapest_room_type = None
            for room_type, value in list(hotel_availabilities[hotel.id].items()):
                if 'availability' in value:
                    if value['availability'] >= room_count and StashUtils.room_config_supported(
                            value['room'], rooms_config):
                        hotel_room[hotel.id]['availability'] = value['availability']
                        hotel_room[hotel.id]['room_type'] = room_type
                        break
            else:
                hotel_room[hotel.id]['availability'] = 0
                try:
                    hotel_room[hotel.id]['room_type'] = next(
                        iter(list(hotel_availabilities[hotel.id].items())))[0]
                except Exception as e:
                    logger.exception(e)
                    print(hotel.id)
        return hotel_room

    @staticmethod
    def check_booking_status_and_update_inventory(
            booking_request, booking_status):
        if booking_status == BookingRequest.COMPLETED:
            # update inventory
            # writing a blanket except to avoid booking failure due to a
            # precautionary step (inv. update) failing
            try:
                StashUtils.update_inventory(
                    booking_request.hotel_id,
                    booking_request.checkin_date,
                    booking_request.checkout_date,
                    booking_request.room_config,
                    booking_request.room_type)
            except Exception as e:
                logger.error(
                    "Inventory update failed for %s %s %s %s %s." %
                    (str(
                        booking_request.hotel_id), str(
                        booking_request.checkin_date), str(
                        booking_request.checkout_date), str(
                        booking_request.room_config), str(
                        booking_request.room_type)))
                logger.exception(e)

    @staticmethod
    def update_inventory(
            hotel_id,
            check_in_date,
            check_out_date,
            room_config,
            room_type):
        room_config_list = room_config.split(',')
        number_of_rooms = len(room_config_list)
        room_availabilities = Availability.objects.filter(
            room__hotel_id=hotel_id,
            date__gte=check_in_date,
            date__lt=check_out_date,
            room__room_type_code__iexact=room_type)
        # reducing inventory on all the applicable rooms
        for room_availability in room_availabilities:
            if number_of_rooms <= room_availability.availablerooms:
                room_availability.availablerooms = room_availability.availablerooms - number_of_rooms
            else:
                # rare scenario... many threads updating the same room near
                # concurrently
                room_availability.availablerooms = 0
            room_availability.save()
