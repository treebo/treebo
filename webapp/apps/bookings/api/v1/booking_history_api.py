from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.bookings.services.booking_history_service import BookingHistoryService
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import UserUnauthroized
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
import logging
from apps.common import error_codes

logger = logging.getLogger(__name__)


class BookingHistoryApi(TreeboAPIView):
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    renderer_classes = [TreeboCustomJSONRenderer, ]
    booking_history_service = BookingHistoryService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    def post(self, request, format=None):
        """Register user
        """
        if not (hasattr(request, 'user')
                and request.user and request.user.is_authenticated()):
            response = api_response.treebo_exception_error_response(
                UserUnauthroized())
        else:
            user = request.user
            try:
                booking_dict, profile, cities, google_maps = self.booking_history_service.get_booking_history(
                    user)
                context = {
                    'context': booking_dict,
                    'profile': profile,
                    'cities': cities,
                    'googleMaps': google_maps,
                }
                response = api_response.prep_success_response(context)
            except TreeboException as e:
                logger.exception("treebo booking history exception %s ", user)
                response = api_response.treebo_exception_error_response(e)
            except Exception as e:
                logger.exception("internal error booking history v1 %s", user)
                response = api_response.internal_error_response(error_codes.get(
                    error_codes.UNABLE_TO_FETCH_BOOKING), BookingHistoryApi.__name__)
        return response
