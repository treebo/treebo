# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('featuregate', '0005_auto_20160523_0950'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='constraintexpression',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Constraint Expression',
                'verbose_name_plural': 'Constraint Expressions'},
        ),
        migrations.AlterModelOptions(
            name='constraintkeywords',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Constraint Keyword',
                'verbose_name_plural': 'Constraint Keywords'},
        ),
        migrations.AlterModelOptions(
            name='constraintoperators',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Constraint Operator',
                'verbose_name_plural': 'Constraint Operators'},
        ),
        migrations.AlterModelOptions(
            name='constraints',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Constraints',
                'verbose_name_plural': 'Constraints'},
        ),
        migrations.AlterModelOptions(
            name='page',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read')},
        ),
        migrations.AlterModelOptions(
            name='pagevariables',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Page Variable',
                'verbose_name_plural': 'Page Variables'},
        ),
        migrations.AlterModelOptions(
            name='variablecondition',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Variable Condition',
                'verbose_name_plural': 'Variable Conditions'},
        ),
    ]
