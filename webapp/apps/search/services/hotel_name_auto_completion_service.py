import logging
from django.core.cache import cache
from django.template.defaultfilters import slugify
from django.db.models import Q

from apps.hotels.service.hotel_meta_service import HotelMetaService
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.search.services.auto_completion_service import AutoCompletionService
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class HotelBasedAutoCompletionService(AutoCompletionService):
    CACHE_TIMEOUT = 24 * 60 * 60

    def apply(self, search_string, **kwargs):
        try:
            meta_keys = kwargs.get('meta_keys')
            cache_key = 'cs_autocomplete_cache_hotel_' + slugify(search_string)
            if meta_keys:
                # replace comma with underscore for multiple metakeys
                cache_key += '_{meta_slug}'.format(
                    meta_slug=slugify(meta_keys.replace(',', '_')))

            try:
                result = cache.get(cache_key)
            except Exception as e:
                result = None
            if result:
                return result
            hotels = Hotel.objects.select_related(
                'locality', 'city').filter((Q(name__istartswith=search_string) |
                                            Q(name__icontains=' '+search_string)),
                                           status=Hotel.ENABLED)
            result_hotels = []
            for hotel in hotels:
                hotel_alias = hotel.hotel_alias.first()
                if not (hotel_alias and hotel_alias.is_enabled):
                    result_hotels.append(hotel)
            hotel_meta_svc = HotelMetaService(keys=meta_keys, hotels=result_hotels)
            result_hotels = hotel_meta_svc.filter()
            result = AutoCompleteResultBuilder.build_hotel_result(result_hotels)
            cache.set(
                cache_key,
                result,
                HotelBasedAutoCompletionService.CACHE_TIMEOUT)
            return result
        except Exception as e:
            logger.exception("Exception in building results for search string %s", search_string)
            raise
