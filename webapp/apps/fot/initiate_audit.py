
import logging

from datetime import datetime, timedelta, time
from django.http import HttpResponseRedirect
from rest_framework.response import Response

from apps.checkout.api.v1.checkout_actions import initiate_booking
from apps.fot.models import FotReservationsRequest, FotHotel, Fot, FotConfig
from base.views.api import TreeboAPIView
from apps.common import date_time_utils

logger = logging.getLogger(__name__)


class InitiateAudit(TreeboAPIView):

    def post(self, request):
        try:
            data = request.POST
            user_id = request.user.id
            self.process_post_request(data, user_id)
            return initiate_booking(request)
        except CustomFotException as e:
            logger.info(
                'Audit validation fot_user: %s due to %s' %
                (request.user.email, e))
            return HttpResponseRedirect(
                request.META.get('HTTP_REFERER') +
                '&validate=false')
        except BaseException:
            logger.exception('Validate audit failed')
            return HttpResponseRedirect(
                request.META.get('HTTP_REFERER') + '&confirm=false')

    def process_post_request(self, data, user_id):
        fot_reservation_obj = None
        try:
            hotel_id = data.get("hotelid")
            audit_checkin_date = data.get('checkin')
            audit_checkout_date = data.get('checkout')
            room_config = str(data.get('roomconfig'))
            fot_reservation_obj = FotReservationsRequest.objects.create(
                user_id=user_id,
                hotel_id=hotel_id,
                status=FotReservationsRequest.PENDING,
                check_in_date=audit_checkin_date,
                check_out_date=audit_checkout_date,
                no_of_guests=room_config)
            self.validate_hotel_stats(hotel_id, audit_checkin_date)
            self.validate_user_stats(user_id, audit_checkin_date)

        except BaseException:
            self.invalidate_pending_audit(fot_reservation_obj)
            raise

    def validate_user_stats(self, user_id, audit_checkin_date):
        audit_checkin_date = datetime.strptime(
            audit_checkin_date, date_time_utils.DATE_FORMAT)
        today = datetime.now().date()
        fot_reservations = FotReservationsRequest.objects.filter(
            user_id=user_id,
            status__in=[
                FotReservationsRequest.PENDING,
                FotReservationsRequest.SCHEDULED,
                FotReservationsRequest.CHECKEDIN,
                FotReservationsRequest.CHECKEDOUT,
                FotReservationsRequest.FEEDBACK_NOT_SUBMITTED,
                FotReservationsRequest.FEEDBACK_SUBMITTED])

        if fot_reservations.filter(
                check_in_date=audit_checkin_date).count() > 1:
            raise CustomFotException('Already booked for checkin date')

        fot_object = Fot.objects.get(user_id=user_id)
        audits_count = fot_reservations.filter(
            check_in_date__gte=today).count()
        if audits_count > fot_object.user_slots:
            raise CustomFotException('Already booked all audit slots')

    def validate_hotel_stats(self, hotel_id, audit_checkin_date):
        fot_hotel = FotHotel.objects.get(hotel_id=hotel_id)
        audit_checkin_date = datetime.strptime(
            audit_checkin_date, date_time_utils.DATE_FORMAT)
        week_start = datetime.combine(
            audit_checkin_date.date() -
            timedelta(
                days=(
                    audit_checkin_date.weekday() +
                    1) %
                7),
            time.min)
        week_end = datetime.combine(
            week_start.date() +
            timedelta(
                days=6),
            time.max)
        audits_count = FotReservationsRequest.objects.filter(
            hotel_id=hotel_id,
            check_in_date__range=(
                week_start,
                week_end),
            status__in=[
                FotReservationsRequest.PENDING,
                FotReservationsRequest.SCHEDULED,
                FotReservationsRequest.CHECKEDIN,
                FotReservationsRequest.CHECKEDOUT,
                FotReservationsRequest.FEEDBACK_NOT_SUBMITTED,
                FotReservationsRequest.FEEDBACK_SUBMITTED]).count()
        if audits_count > fot_hotel.slots_allowed:
            raise CustomFotException(
                'Sorry! Hotel slots are fully occupied now.')

    def invalidate_pending_audit(self, fot_reservation_obj):
        if fot_reservation_obj:
            # TODO: Not appropriate status but intoducing a new status will
            # break fot search.
            fot_reservation_obj.status = FotReservationsRequest.CANCEL_PENDING
            fot_reservation_obj.save()


class CustomFotException(Exception):
    pass
