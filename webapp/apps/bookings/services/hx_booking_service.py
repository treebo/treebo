# -*- coding:utf-8 -*-
"""
    HX Implemention for BookingClient API's
"""
import datetime
import json
import logging
from decimal import Decimal

from django.conf import settings
from django.db import transaction
from dto.booking import Booking as BookingDTO
from hx.exceptions import HxAPIMaxRetriesExceeded, HxNonRetryableAPIFailure, HxAuthenticationFailure
from hx.hx_booking_manager import HxBookingManager

from apps.bookings import constants
from apps.bookings.dto.partpay_confirm_event import PartpayStatusForGrowth, PartpayConfirmReasons, PartpayConfirmEvent
from apps.bookings.models import Booking, RoomBooking
from apps.bookings.repositories.booking_repository import BookingServiceRepository
from apps.bookings.services.abstract_external_booking_service import AbstractExternalBookingService
from apps.bookings.services.booking_failure import BookingFailureHandler
from apps.bookings.services.booking_utils import get_cancellation_mail, get_response, get_exception_mail
from apps.bookings.utils import get_value
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.common.utils import round_to_two_decimal
from apps.payments.service.payment_service import PaymentService
from base.db_decorator import db_retry_decorator
from common.constants import common_constants
from common.exceptions.treebo_exception import TreeboValidationException

logger = logging.getLogger(__name__)


class HXBookingService(AbstractExternalBookingService):
    """
        Hotelogix Implementation for ExternalBookingService APIs
    """

    def __init__(self):
        self.hx_api_client = None

    def save_booking_on_external_reservation_system(self, booking, room_configs, rate_name, special_rates_enabled):
        room_type, hotel = booking.room.room_type_code, booking.hotel
        self.setup_hx_client(booking)

        try:
            rate_ids = self.get_multiple_rate_ids(room_type, hotel, room_configs, rate_name)
        except TreeboValidationException as e:
            if booking.is_audit and special_rates_enabled:
                # Special Rate failed for is_audit booking. Fail over to Treebo Promotional
                rate_name = 'Treebo Promotional'
                rate_ids = self.get_multiple_rate_ids(room_type, hotel, room_configs, rate_name)
            else:
                track_failure = BookingFailureHandler.get_track_failure(booking, "Unable to create temp booking in Hx",
                                                                        self.get_subject_for_track_failure())
                exception_mail = get_exception_mail(e, "Unable to create temp booking in Hx. Order Id: {0}".format(
                    booking.order_id))
                return get_response(500, "No rateIds found", [track_failure, exception_mail])

        response = self.add_to_cart(rate_ids)
        self.modify_amount(response.bookings)

        tax_code = ''
        logger.info(
            "calling hx_savebooking with %s %s [%s, %s]",
            booking.guest_email,
            booking.order_id,
            tax_code,
            booking.organization_address)

        response = self.hx_api_client.hx_savebooking(
            first_name=booking.guest_name,
            last_name="",
            guest_phone=booking.guest_mobile,
            guest_email=booking.guest_email,
            owner_id=None,
            booking_id=booking.order_id,
            comments=booking.comments,
            hotel_id=response.hotel_id,
            source='Web(Prepaid)',
            taxcode=tax_code,
            address=booking.organization_address)

        return response

    def save_booking_to_db(self, booking, save_booking_response):
        """
        update booking detail from HX to db
        :param booking:
        :param save_booking_response:
        :return:
        """

        booking.booking_code = save_booking_response.booking_code
        booking.group_code = save_booking_response.group_code
        booking.booking_status = Booking.TEMP_BOOKING_CREATED
        BookingServiceRepository.save_booking(booking)

    def save_room_booking_to_db(self, booking, save_booking_response):
        """
        :param booking:
        :param save_booking_response:
        :return:
        """

        for room in save_booking_response.rooms:
            reservation_id = room['reservation_id']

            room_bookings = RoomBooking.objects.filter(
                booking=booking, reservation_id=reservation_id).count()
            if room_bookings > 0:
                continue

            room_booking = RoomBooking()
            room_booking.booking = booking
            room_booking.price = room['price']
            room_booking.reservation_id = reservation_id
            room_booking.room = booking.room
            BookingServiceRepository.save_room_booking(room_booking)

    def create_save_booking_response(self, save_booking_response, booking_id):
        response_json = {
            'guest_ids': save_booking_response.guest_ids,
            'booking_ids': save_booking_response.booking_ids,
            'group_ids': save_booking_response.group_ids
        }

        return response_json

    def setup_hx_client(self, booking):
        """
        :param booking:
        :return:
        """
        booking_dto = self.get_booking_dto(booking)
        self.hx_api_client = HxBookingManager(booking_dto, logger)

    def add_to_cart(self, rate_ids):
        """
        :param rate_ids:
        :return:
        """
        try:
            self.hx_api_client.hx_discard_previous_cart()
            self.hx_api_client.hx_loadcart()
            response = self.hx_api_client.hx_addtocart(rate_ids)
            logger.info("AddToCart response : %s", str(response))
            return response
        except Exception as e:
            logger.error("Exception in add_to_cart for order_id : {0}".format(self.hx_api_client.booking.booking_id),
                         exc_info=e)
            raise e

    def get_multiple_rate_ids(self, room_type, hotel, room_configs, rate_name):
        """
        :param room_type:
        :param hotel:
        :param room_configs:
        :param rate_name:
        :return:
        """
        selected_rateids = list()
        logger.debug(
            'Hotelogix ID used for getting multiple rate id: %s',
            hotel.hotelogix_id)
        rates_list = self.hx_api_client.hx_rateidsforadtocart()
        room_type_rate_id_dict = self.parse_rate_list(
            rates_list.room_type_rates_guest_count_details, rate_name)

        logger.info(
            'Rate ID Dict received from GET Rate ID API: %s',
            str(room_type_rate_id_dict))
        rate_id_dict = room_type_rate_id_dict.get(room_type, {})

        for room_config in room_configs:
            config = room_config[0]
            rate_id = rate_id_dict.get(config, None)
            if not rate_id:
                logger.error("Empty Rate ID from HX = %s" % hotel.name)
                raise TreeboValidationException(
                    "Rate Ids not found for room_config: {0}-{1}".format(room_config[0], room_config[1]))
            selected_rateids.append(rate_id)

        logger.info('Selected Rate IDs: %s', selected_rateids)
        return selected_rateids

    @staticmethod
    def parse_rate_list(rates_list, rate_name):
        """
        :param rates_list:
        :param rate_name:
        :return:
        """
        room_rate_list = dict()
        for room_type in rates_list:
            rate_type_list = rates_list[room_type]
            room_rate_list[str(room_type)] = dict()
            for rate_type in rate_type_list:
                rates = rate_type_list[rate_type]
                # sometimes there is space in ratenames , case Treebo
                # Promotional
                if rate_type.strip() == rate_name.strip():
                    for rate in rates:
                        if not int(rate['child_count']):
                            room_rate_list[room_type][int(
                                rate['adult_count'])] = rate['rate_id']
        return room_rate_list

    def modify_amount(self, hx_bookings):
        """
        :param hx_bookings:
        :return:
        """
        logger.info("modify_amount : {0}".format(hx_bookings))
        cart_booking_rates = self.parse_booking_modify_amount(hx_bookings)
        loadcart_response = self.hx_api_client.hx_loadcart(retry_auth=False)
        logger.info("load cart response : {0}".format(loadcart_response))
        self.hx_api_client.hx_modifyamount(
            cart_booking_rates, loadcart_response.hotel_id)

    def parse_booking_modify_amount(self, hx_bookings):
        """
        :param self:
        :param hx_bookings:
        :return:
        """
        reservation_map_list = list()
        amount = (self.hx_api_client.booking.pretax_amount - self.hx_api_client.booking.discount -
                  self.hx_api_client.booking.member_discount) / len(hx_bookings)
        for hx_booking_id in hx_bookings:
            reservation_map = {
                "amount": amount,
                "rate_type": 0,
                "cart_booking_id": hx_booking_id}
            reservation_map_list.append(reservation_map)
        return reservation_map_list

    def confirm_booking_on_external_reservation_system(self, booking, paid_source):
        self.setup_hx_client(booking)
        response = self.confirm_bookings(
            booking, paid_source)
        return response

    def get_data_to_update_upfront_partpay_in_growth(self, booking):
        # not sending booking_Code because all the records with the given group code should be confirmed
        return PartpayConfirmEvent(
                            group_code=booking.group_code if booking.group_code else booking.booking_code,
                            hotel_code=booking.hotel.hotelogix_id,
                            booking_code=None,
                            status=PartpayStatusForGrowth.CONFIRM,
                            confirmation_reason=PartpayConfirmReasons.UPFRONT_PARTPAY
                        )

    def confirm_bookings(self, booking, paid_source):
        """
        :param booking:
        :param paid_source:
        :return:
        """
        response = self.hx_api_client.hx_getorder()
        order_amount_from_hx = response.amount
        amount = self.get_confirm_booking_amount(
            booking, order_amount_from_hx)
        response = self.hx_api_client.hx_confirmbooking(
            paid_source, amount, paid_source)
        logger.info('confirm booking response {0}'.format(response.__dict__))

        booking = self.finalize_booking_confirmation(booking.id, response)

        response_data = {
            'guestids': response.guestids,
            'booking_status': booking.booking_status,
            'wallet_applied': booking.wallet_applied}

        return response_data

    @staticmethod
    def get_confirm_booking_amount(booking, order_amount_from_hx):
        """
        :param booking:
        :param order_amount_from_hx:
        :return:
        """
        # As we are updating paid amount separately by calling to hx so sending
        # amount amount as zero here..
        if booking.is_audit:
            amount = float(booking.total_amount)
        else:
            amount = 0

        booking_amount_rounded = round_to_two_decimal(amount)
        logger.info(
            "get_confirm_booking_amount booking %s(%s) amount paid via ext %s, payment_amount %s, "
            "wallet_deduction %s => total amount %s ",
            booking.id,
            booking.order_id,
            amount,
            float(
                booking.payment_amount),
            booking.wallet_deduction,
            booking.total_amount)
        return booking_amount_rounded

    def cancel_booking_on_external_reservation_system(self, booking, room_bookings, order_id):
        self.setup_hx_client(booking)

        response = self.hx_api_client.hx_getorder()
        status = response.status
        logger.info('HX getorder API call status: %s', status)

        if status['message'] == common_constants.SUCCESS:
            bookings = response.bookings
            logger.debug(
                'All booking status from HX : %s', [
                    b['statuscode'] for b in bookings])
            cancelled_in_hx = all(
                b['statuscode'] == 'CANCEL' for b in bookings)
            logger.debug('Booking Cancelled in HX: %s', cancelled_in_hx)
            cancellation_failed_in_hx = False
            if not cancelled_in_hx:
                for room_booking in room_bookings:
                    reservation_id = room_booking.reservation_id
                    response = self.hx_api_client.hx_cancel(
                        order_id, reservation_id)
                    status = response.status
                    if status['message'] != common_constants.SUCCESS:
                        logger.error(
                            'Booking Cancellation Failed for Order ID: %s, and room booking reservation Id: %s',
                            order_id,
                            reservation_id)
                        cancellation_mail = get_cancellation_mail(order_id)
                        cancellation_failed_in_hx = True
                        return get_response(
                            500, 'Cancellation failed for order ID: {0}, reservation Id: {1}'.format(
                                order_id, reservation_id), [cancellation_mail])

            if not cancellation_failed_in_hx:
                current_date = datetime.datetime.now().date()
                booking.booking_status = booking.CANCEL
                booking.cancellation_datetime = current_date
                booking.save()
                logger.debug(
                    'Cancellation successful for Booking ID: %s',
                    booking.order_id)
        else:
            logger.error(
                'Booking Cancellation failed. getorder API return status: %s', status)
            cancellation_mail = get_cancellation_mail(order_id)
            return get_response(500, "Booking Cancellation failed. getorder API return status: {0}".format(
                status)[cancellation_mail])

    @db_retry_decorator()
    def finalise_booking(self, booking_id, user_id=None, request_id=None):
        """
        Equilant to tasks.py : confirm_booking
        Confirms a temporary booking in Hx.
        Basically a (confirmbookinginhx)
        :param booking_id:
        :param user_id:
        :param request_id:
        :return:
        """
        confirm_response = self.confirm_booking(
            booking_id, user_id=user_id, request_id=request_id)
        try:
            logger.info("confirm_response %s ", json.dumps(confirm_response))
        except Exception as e:
            logger.exception("Could not dump confirm booking response")

        return confirm_response

    def update_guest_details(self, booking_id, guest_ids):
        guest_ids = set(guest_ids)
        try:
            booking = Booking.objects.select_related(
                'hotel').get(pk=booking_id)
        except Booking.DoesNotExist:
            return get_response(
                500, "booking not found for id {0}".format(booking_id))
        taxcode = get_value(booking.organization_taxcode)
        organization_address = get_value(booking.organization_address)
        organization_name = get_value(booking.organization_name)

        logger.info(
            "in updating guest details for guest %s with tax code %s %s %s ",
            guest_ids,
            taxcode,
            organization_name,
            organization_address)
        if not (taxcode and organization_address and organization_name):
            logger.info(
                "no gst details found so not calling updateguestdetails api ")
            return get_response(200, "No Gst info : {0}".format(booking_id))

        self.setup_hx_client(booking)
        try:
            if booking.guest_name:
                name = str(booking.guest_name).split()
                first_name = name[0]
                last_name = name[1] if len(name) > 1 else '.'
            else:
                first_name = 'Guest'
                last_name = '.'
            for guest_id in guest_ids:
                if not guest_id:
                    logger.info(
                        "updating guest details for guest  but guest id is null")
                    continue
                logger.info(
                    " updating guest details for guest %s taxcode %s %s %s ",
                    guest_id,
                    taxcode,
                    organization_name,
                    organization_address)

                updateguestdetails_response = self.hx_api_client.hx_updateguestdetails(
                    guest_id=guest_id,
                    first_name=first_name,
                    last_name=last_name,
                    guest_email=booking.guest_email,
                    phone_number=booking.guest_mobile,
                    organization_name=organization_name,
                    organization_address=organization_address,
                    taxcode=taxcode)
                logger.info(
                    " updateguestdetails_response for %s %s -> %s  ",
                    booking_id,
                    guest_id,
                    updateguestdetails_response.data)
            return get_response(
                200, "Updated Guest Details : {0}".format(booking_id))
        except HxAPIMaxRetriesExceeded as e:
            exception_mail = get_exception_mail(
                e,
                'updating gstin details',
                "Unable to update guest details in Hx. Order Id: {0}".format(
                    booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                'updating gstin details',
                "Please Update GSTIN details in Hx / Do not create booking for order id : ")
            logger.exception(
                "HxAPIMaxRetriesExceeded occurred while update guest details in HX for order id %s ",
                booking.order_id)
            return get_response(
                500, str(e), [
                    exception_mail, track_failure])
        except (HxNonRetryableAPIFailure, Exception) as e:
            exception_mail = get_exception_mail(
                e, "Unable to update guest details in Hx. Order Id: {0}".format(
                    booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                'updating gstin details',
                "Please Update GSTIN details in Hx / Do not create booking for order id : ")
            logger.exception(
                "Exception occurred while update guest details in hx for order id %s ",
                booking.order_id)
            return get_response(
                400, str(e), [
                    exception_mail, track_failure])

    def update_payment(self, booking_id, hx_booking_ids, hx_group_ids):
        try:
            booking = Booking.objects.select_related(
                'hotel').get(pk=booking_id)
        except Exception:
            return get_response(
                500, "booking not found for id {0}".format(booking_id))

        self.setup_hx_client(booking)
        try:
            logger.info(" update_payment  edit booking call %s ", booking_id)
            hx_booking_id = hx_booking_ids[0]
            hx_group_booking_id = hx_group_ids[0]
            if booking.group_code:
                booking_type = 'G'
                group_booking_id = hx_group_booking_id
            else:
                booking_type = 'S'
                group_booking_id = hx_booking_id
            edit_booking_response = self.hx_api_client.hx_editbooking(
                booking_type=booking_type, group_booking_id=group_booking_id)
            temp_booking_id = str(
                edit_booking_response.booking_ids).split(',')[0]
            temp_group_id = str(edit_booking_response.group_ids).split(',')[0]
            amount = float(booking.pretax_amount) + 1.0
            if booking_type == 'G':
                edit_pretax_response = self.hx_api_client.hx_editgrouppretax(
                    temp_bookingid=temp_group_id, amount=amount)
            else:
                edit_pretax_response = self.hx_api_client.hx_editbookingpretax(
                    temp_bookingid=temp_booking_id, amount=amount)
            self.hx_api_client.hx_commiteditbooking(
                booking_type=booking_type, group_booking_id=group_booking_id)
            response_data = {}
            return get_response(
                200,
                "Updated Payment Details : {0}".format(booking_id),
                [response_data])
        except HxAPIMaxRetriesExceeded as e:
            exception_mail = get_exception_mail(
                e, "Unable to update payment in Hx. Order Id: {0}".format(
                    booking.order_id))
            logger.exception(
                "HxAPIMaxRetriesExceeded occurred while update guest details in HX for order id %s ",
                booking.order_id)
            return get_response(500, str(e), [exception_mail])
        except (HxNonRetryableAPIFailure, Exception) as e:
            exception_mail = get_exception_mail(
                e, "Unable to update payment in Hx. Order Id: {0}".format(
                    booking.order_id))
            logger.exception(
                "Exception occurred while updating payment in hx for order id %s ",
                booking.order_id)
            return get_response(400, str(e), [exception_mail])

    @transaction.atomic
    def update_payment_for_booking_on_external_reservation_system(self, booking_id):
        try:
            _ = Booking.objects.select_for_update().get(pk=booking_id)
            booking = Booking.objects.select_related('hotel').get(pk=booking_id)
            hotel = booking.hotel
        except Exception:
            return get_response(
                500, "booking not found for id {0}".format(booking_id))
        self.setup_hx_client(booking)
        payment_orders = PaymentService.get_non_wallet_payments_for_booking(
            booking.order_id)
        booking_type = booking.booking_type()

        if len(payment_orders) == 0 and not booking.wallet_applied:
            logger.info("No payment for booking %s", booking_id)
            return get_response(200, "Booking doesn't have payments : {0}".format(booking_id))

        try:
            ext_booking_id = self._fetch_booking_id_from_hx_from_booking_code(
                booking_type, booking.group_code, booking.booking_code)
            for payment_order in payment_orders:
                if payment_order.updated_in_booking_service:
                    logger.info(
                        "Paymentorder %s for booking %s is already updated for bid %s ",
                        payment_order.ps_order_id,
                        payment_order.booking_order_id,
                        booking_id)
                    continue
                if not payment_order.is_verified():
                    logger.info(
                        "Paymentorder is not verified ps_orderid %s booking_order_id %s for bid %s ",
                        payment_order.ps_order_id,
                        payment_order.booking_order_id,
                        booking_id)
                    continue
                self._update_payment_in_booking(booking, payment_order)
                update_single_payment_response = self.update_single_payment(
                    booking_type, ext_booking_id, payment_order)
                logger.info(
                    "In update_single_payment_response for booking_id %s(%s) payment %s and response %s ",
                    booking_id,
                    booking.order_id,
                    payment_order.ps_order_id,
                    update_single_payment_response)

            if booking.wallet_applied:
                self._update_payment_via_wallet_in_booking(booking)

                update_wallet_payment_response = self.update_wallet_payment_from_booking(
                    booking_type, ext_booking_id, booking)
                logger.info(
                    "Response for updating wallet payment on Hx for booking_id %s(%s) is %s ",
                    booking_id,
                    booking.order_id,
                    update_wallet_payment_response)

            return get_response(200, "Payments Are updated in hx: {0}".format(booking_id))

        except (HxAPIMaxRetriesExceeded, HxAuthenticationFailure) as e:
            logger.exception("HxAPIMaxRetriesExceeded occurred while updating payment details in HX for Order Id {0}"
                             .format(booking.order_id))

            exception_mail = get_exception_mail(
                e, "Unable to update payment details in Hx. Order Id: {0}".format(
                    booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                'Unable to update payment in Hx',
                "Please Update Payment in Hx / Do not create booking for order id :")
            return get_response(500, str(e), [exception_mail, track_failure])

        except (HxNonRetryableAPIFailure, Exception) as e:
            logger.exception("Payment Update Failure")
            slack_alert.send_slack_alert_for_payment_update_failure(order_id=booking.order_id,
                                                                    hotel_name=hotel.name, exc=e)
            exception_mail = get_exception_mail(
                e, "Unable to update payment details in Hx. Order Id: {0}".format(
                    booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                'Unable to update payment in Hx',
                "Please Update Payment in Hx / Do not create booking for order id :")
            return get_response(400, str(e), [exception_mail, track_failure])

    def update_single_payment(
            self,
            booking_type,
            ext_booking_id,
            payment_order):
        if not payment_order:
            logger.info(
                "No payment order found for ext_booking_id %s  ",
                ext_booking_id)
            return get_response(
                200, "No PaymentOrder to update : {0}".format(ext_booking_id))

        ps_order_id = payment_order.ps_order_id
        booking_order_id = payment_order.booking_order_id
        gateway = str(payment_order.gateway_type).lower().strip()
        payment_mode = settings.HX_GATEWAY_CONFIG[gateway]['payment_mode']
        payment_type = settings.HX_GATEWAY_CONFIG[gateway]['payment_type']
        amount = str(float(payment_order.amount))
        logger.info(
            "update_payment  ps_order_id  %s for booking %s ",
            ps_order_id,
            booking_order_id)
        if booking_type == 'G':
            hx_addpaymenttogroup_response = self.hx_api_client.hx_addpaymenttogroup(
                ext_booking_id, payment_mode, payment_type, amount, ps_order_id)
        else:
            hx_addpaymenttogroup_response = self.hx_api_client.hx_addpaymenttobooking(
                ext_booking_id, payment_mode, payment_type, amount, ps_order_id)
        logger.info("hx_addpaymenttogroup_response for %s -> %s  ",
                    booking_order_id, hx_addpaymenttogroup_response.data)
        payment_order.updated_in_booking_service = True
        payment_order.save()
        return get_response(
            200, "PaymentOrder to Updated Success : {0}".format(ps_order_id))

    def update_wallet_payment_from_booking(
            self,
            booking_type,
            ext_booking_id,
            booking):

        payment_objects = self._fetch_payment_response(booking.group_code, booking.booking_code)
        if payment_objects:
            for key, payment_list in payment_objects.items():
                if payment_list.pay_type.short_name == 'TP':
                    return get_response(
                        200, "Success. Payment for wallet updated in Hx for booking id {0}".format(booking.order_id))

        booking_order_id = booking.order_id
        payment_mode = settings.HX_GATEWAY_CONFIG.get(constants.WALLET_GATEWAY_TYPE).get('payment_mode')
        payment_type = settings.HX_GATEWAY_CONFIG.get(constants.WALLET_GATEWAY_TYPE).get('payment_type')
        amount = str(float(booking.wallet_deduction))
        logger.info(
            "updating wallet payment for booking %s ",
            booking_order_id)
        if booking_type == 'G':
            hx_add_payment_response = self.hx_api_client.hx_addpaymenttogroup(
                ext_booking_id, payment_mode, payment_type, amount, booking_order_id)
        else:
            hx_add_payment_response = self.hx_api_client.hx_addpaymenttobooking(
                ext_booking_id, payment_mode, payment_type, amount, booking_order_id)
        logger.info("hx_addpaymenttogroup_response for %s -> %s  ",
                    booking_order_id, hx_add_payment_response.data)
        return get_response(
            200, "Success. Payment for wallet updated in Hx for booking id {0}".format(booking_order_id))

    def _fetch_booking_id_from_hx_from_booking_code(
            self, booking_type, group_code, booking_code):
        if booking_type == 'G':
            group_code = str(group_code)
            getbooking_response = self.hx_api_client.hx_getgroup(
                group_code=group_code)
            groupids = str(getbooking_response.groupids).split(',')
            hx_booking_id = groupids[0]
        else:
            booking_code = str(booking_code)
            getbooking_response = self.hx_api_client.hx_getbooking(
                booking_code=booking_code)
            hx_booking_id = str(getbooking_response.bookingid)
        return hx_booking_id

    def _update_payment_in_booking(self, booking, payment_order):
        logger.info('updating the total amount for booking  %s with status  %s', booking.order_id,
                    booking.booking_status)
        booking.payment_amount += Decimal(payment_order.amount)
        booking.save()

    def _update_payment_via_wallet_in_booking(self, booking):
        logger.info('updating the total amount for booking  %s with status  %s', booking.order_id,
                    booking.booking_status)
        booking.payment_amount += Decimal(booking.wallet_deduction)
        booking.save()

    def get_booking_dto(self, booking):
        """
        :param booking:
        :return:
        """
        _hotel = booking.hotel
        _user = None
        _booked_rooms = [booking.room]
        _booking_dto = BookingDTO(_hotel, _user, _booked_rooms)
        _booking_dto.booking_id = booking.order_id
        _booking_dto.booking = booking
        _booking_dto.hotel = booking.hotel
        _booking_dto.check_in = booking.checkin_date
        _booking_dto.check_out = booking.checkout_date
        _booking_dto.pretax_amount = booking.pretax_amount
        _booking_dto.discount = booking.discount
        _booking_dto.member_discount = booking.member_discount if booking.member_discount else 0
        return _booking_dto

    def finalize_booking_confirmation(self, booking_id, response):
        """
        :param booking_id:
        :param response:
        :return:
        """
        booking = Booking.objects.get(pk=booking_id)
        logger.info(
            "final confirm booking_id %s wallet_applied %s booking.wallet_deduction %s ",
            booking.id,
            booking.wallet_applied,
            booking.wallet_deduction)

        booking.booking_code = response.booking_id
        booking.group_code = response.group_code
        if self.is_wallet_applied_and_wallet_payment_failed(booking):
            booking.booking_status = Booking.PART_SUCCESS
        else:
            booking.booking_status = Booking.CONFIRM
        booking.is_complete = True
        BookingServiceRepository.save_booking(booking)
        logger.info(
            "Inside finalize_booking_confirmation. Finalizing Booking Confirmation for booking_id: %s with status %s",
            booking_id,
            booking.booking_status)

        return booking

    def get_subject_for_track_failure(self):
        return 'URGENT: Reservation Attempt Failure. Order ID: '

    def get_external_reservation_system_name(self):
        return 'Hx'

    def update_gst_detail_of_booking_owner_on_external_reservation_system(self, booking):
        """
        update GST Detail of the booking owner on HX
        :param booking:
        :return:
        """
        self.setup_hx_client(booking)
        hx_order = self.hx_api_client.hx_getorder()

        guest_ids = set()
        for hx_booking in hx_order.bookings:
            for guest_stay in hx_booking.get('gueststays'):
                guest_id = guest_stay.get('guestid')
                if guest_id:
                    guest_ids.add(guest_id)

        tax_code = get_value(booking.organization_taxcode)
        organization_address = get_value(booking.organization_address)
        organization_name = get_value(booking.organization_name)

        try:
            if booking.guest_name:
                name = booking.guest_name.split()
                first_name = name[0]
                last_name = name[1] if len(name) > 1 else '.'
            else:
                first_name = 'Guest'
                last_name = '.'

            for guest_id in guest_ids:
                update_guest_details_response = self.hx_api_client.hx_updateguestdetails(
                    guest_id=guest_id,
                    first_name=first_name,
                    last_name=last_name,
                    guest_email=booking.guest_email,
                    phone_number=booking.guest_mobile,
                    organization_name=organization_name,
                    organization_address=organization_address,
                    taxcode=tax_code)

                logger.info("update_guest_details_response for booking_id: {0}, guest_id: {1} is {2}"
                            .format(booking.id, guest_id, update_guest_details_response.data))

                return get_response(200, "Updated Guest Details : {0}".format(booking.id))

        except (HxAPIMaxRetriesExceeded,HxAuthenticationFailure) as e:
            logger.exception("HxAPIMaxRetriesExceeded occurred while updating guest details in HX for Order Id {0}"
                             .format(booking.order_id))

            exception_mail = get_exception_mail(e, "updating gstin details", "Unable to update guest details in Hx. "
                                                                             "Order Id: {0}".format(booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                "updating gstin details",
                "Please Update GSTIN details in Hx / Do not create booking for order id : ")
            return get_response(500, str(e), [exception_mail, track_failure])

        except (HxNonRetryableAPIFailure, Exception) as e:
            logger.exception("Exception occurred while updating guest details in hx for Order Id {0}"
                             .format(booking.order_id))

            exception_mail = get_exception_mail(e, "Unable to update guest details in Hx. "
                                                   "Order Id: {0}".format(booking.order_id))
            track_failure = BookingFailureHandler.get_track_failure(
                booking,
                'updating gstin details',
                "Please Update GSTIN details in Hx / Do not create booking for order id : ")
            return get_response(400, str(e), [exception_mail, track_failure])

    def _fetch_payment_response(
            self, group_code, booking_code):
        if group_code and 'G' in group_code:
            getbooking_response = self.hx_api_client.hx_getgroup(
                group_code=group_code)
        else:
            booking_code = str(booking_code)
            getbooking_response = self.hx_api_client.hx_getbooking(
                booking_code=booking_code)
        logger.info(" Payment response from _fetch_payment_response %s", str(getbooking_response))
        return getbooking_response.payments