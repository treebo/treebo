import json
import logging
from string import Template

import requests
from geopy.distance import vincenty

from common.constants import common_constants as const
from data_services.city_repository import CityRepository
from data_services.respositories_factory import RepositoriesFactory

city_data_service = RepositoriesFactory.get_city_repository()

logger = logging.getLogger(__name__)

GMAP_STATIC_URL = "https://maps.googleapis.com/maps/api/staticmap?size=$size&markers=icon:$icon_src%7C$origin_latlng&markers=icon:$icon_dest%7Clabel:$dest_label%7C$destination_latlng&path=weight:6%7Ccolor:blue%7Cenc:$polyline&key=$key"


class Maps:
    """

    """

    def getHotelDistance(self, hotels, city, lat, long, sub_locality):
        """
        Enrich each hotel model with information regarding distance of hotel from supplied lat, long
        :param hotels:
        :param city:
        :param lat:
        :param long:
        :param sub_locality:
        :return:
        """
        origin_latlng = str(lat) + "," + str(long)
        for hotel in hotels:
            hotel.distance = None
        if sub_locality or city:
            for hotel in hotels:
                destination_latlng = str(
                    hotel.latitude) + "," + str(hotel.longitude)
                distance = self.calculateDistanceWithoutGMAP(
                    origin_latlng, destination_latlng)
                hotel.distance = "{0:.2f}".format(distance)
                logger.debug(hotel.name)
                logger.debug(hotel.distance)
        return hotels

    def getHotelDistanceWithoutGMAP(
            self, hotels, city, lat, long, sub_locality):
        """
        Enrich each hotel model with information regarding distance of hotel from supplied lat, long
        :param hotels:
        :param city:
        :param lat:
        :param long:
        :param sub_locality:
        :return:
        """
        origin_latlng = str(lat) + "," + str(int)
        for hotel in hotels:
            hotel.distance = None
        if sub_locality or city:
            for hotel in hotels:
                destination_latlng = str(
                    hotel.latitude) + "," + str(hotel.longitude)
                distance = self.calculateDistanceWithoutGMAP(
                    origin_latlng, destination_latlng)
                hotel.distance = "{0:.2f}".format(distance)
                logger.debug(hotel.name)

                logger.debug(hotel.distance)
        return hotels

    def getCityName(self, lat, long):
        """

        :param lat:
        :param long:
        :return:
        """

        origin_latlng = str(lat) + "," + str(int)
        cities = city_data_service.filter_cities(status=1)
        #cities = City.objects.filter(status=City.ENABLED)
        city_name = None
        for city in cities:
            logger.debug(city.name)
            lat = city.city_latitude
            long = city.city_longitude
            destination_latlng = str(lat) + "," + str(int)

            distance = self.calculateDistance(
                origin_latlng, destination_latlng)
            logger.debug(distance)
            if distance < 50:
                return city.name

    def getCityNameWithoutGMAP(self, lat, long):
        """

        :param lat:
        :param long:
        :return:
        """
        origin_latlng = str(lat) + "," + str(int)
        cities = city_data_service.filter_cities(status=1)
        #cities = City.objects.filter(status=City.ENABLED)
        city_name = None
        for city in cities:
            logger.debug(city.name)
            lat = city.city_latitude
            long = city.city_longitude
            destination_latlng = str(lat) + "," + str(int)
            distance = self.calculateDistanceWithoutGMAP(
                origin_latlng, destination_latlng)
            logger.debug(distance)
            if distance < const.DEFAULT_MIN_DISTANCE:
                return city.name

    def calculateDistance(self, origin_latlng, destination_latlng):
        """

        :return:
        """
        gmap_url = const.GMAP_URL
        gmap_url = Template(gmap_url).safe_substitute(
            origin_latlng=origin_latlng, destination_latlng=destination_latlng)
        distance_object = requests.get(gmap_url)
        distance = 99999
        for distance in json.loads(distance_object.text)['rows']:
            for elements in distance['elements']:
                distance = elements['distance']['value']
        if distance:
            return float(str(distance)) / 1000.0
        else:
            return 99999

    def calculateDistanceWithoutGMAP(self, origin_latlng, destination_latlng):
        """

        :param origin_latlng:
        :param destination_latlng:
        :return:
        """
        origin = (origin_latlng)
        destination = (destination_latlng)
        return vincenty(origin, destination).kilometers

    def getDirectionPolyline(
            self,
            origin_latlng,
            destination_latlng,
            directions_obj):
        """

        :param origin_latlng:
        :param destination_latlng:
        :return:
        """
        gmap_url = const.GMAP_DIRECTIONS_URL
        api_key = const.GMAP_API_KEY
        mode = const.DIRECTION_MODE_WALKING
        gmap_url_walking = Template(gmap_url).safe_substitute(
            origin_latlng=origin_latlng,
            destination_latlng=destination_latlng,
            key=api_key,
            mode=mode)
        distance_polygon = None
        try:

            response = requests.post(gmap_url_walking)
            data = json.loads(response.text)
            distance_text = int(
                data["routes"][0]["legs"][0]["distance"]["value"])
            logger.debug(
                "Google maps directions call for distance %s" %
                distance_text)
            if distance_text > const.GMAP_MODE_DISTANCE:
                mode = const.DIRECTION_MODE_DRIVING
                gmap_url_driving = Template(gmap_url).safe_substitute(
                    origin_latlng=origin_latlng,
                    destination_latlng=destination_latlng,
                    key=api_key,
                    mode=mode)
                response = requests.post(gmap_url_driving)
                data = json.loads(response.text)
                logger.debug(
                    "Get Google maps directions for distance greater than 500 meters")
                distance_polygon = str(
                    data["routes"][0]["overview_polyline"]["points"])
                # Save in driections object
                directions_obj.polyline = distance_polygon
                directions_obj.mode = directions_obj.DRIVING
                directions_obj.save()
            else:
                distance_polygon = str(
                    data["routes"][0]["overview_polyline"]["points"])
                # Save in driections object
                directions_obj.polyline = distance_polygon
                directions_obj.mode = directions_obj.WALKING
                directions_obj.save()
            logger.debug("Polyline = %s" + distance_polygon)
        except Exception as exc:
            logger.exception("Maps direction api error %s" % str(exc))
        return distance_polygon

    def getStaticMapUrl(
            self,
            origin_latlng,
            destination_latlng,
            directions_obj,
            newSize=None,
            showPolyline=True):
        """

        :param origin_latlng:
        :param destination_latlng:
        :return:
        """

        distance_polyline = None
        if directions_obj.polyline is None:
            distance_polyline = self.getDirectionPolyline(
                origin_latlng, destination_latlng, directions_obj)
            directions_obj.polyline = distance_polyline
            directions_obj.save()
        else:
            distance_polyline = directions_obj.polyline
        static_map_url = None

        size = const.GMAP_STATIC_SIZE
        if newSize:
            size = newSize
        GMAP_STATIC_HOTEL_ICON_URL = "https://s3-ap-southeast-1.amazonaws.com/treebo/static/images/Icons/treebo_hotel_location_pin-new.png"
        GMAP_STATIC_DESTINATION_ICON_URL = "https://s3-ap-southeast-1.amazonaws.com/treebo/static/images/Icons/Location_pin_black.png"

        if showPolyline:
            try:
                if distance_polyline is not None:
                    static_map_url = GMAP_STATIC_URL
                    static_map_url = Template(static_map_url).safe_substitute(
                        size=size,
                        icon_src=GMAP_STATIC_HOTEL_ICON_URL,
                        icon_dest=GMAP_STATIC_DESTINATION_ICON_URL,
                        origin_latlng=origin_latlng,
                        destination_latlng=destination_latlng,
                        polyline=distance_polyline,
                        key=const.GMAP_API_KEY)
            except Exception as exc:
                logger.exception("Get Static Map URL failed!")
        else:
            try:
                static_map_url = GMAP_STATIC_URL
                static_map_url = Template(static_map_url).safe_substitute(
                    size=size, origin_latlng=origin_latlng, key=const.GMAP_API_KEY)
            except Exception as exc:
                logger.exception("Get Static Map URL failed!")

        return static_map_url
