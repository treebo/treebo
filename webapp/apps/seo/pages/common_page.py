import logging
from apps.seo.pages.pages import PAGES

__author__ = 'ansilkareem'
logger = logging.getLogger(__name__)


class CommonPage(PAGES):

    def __init__(self, page, query, is_amp=''):
        super(CommonPage, self).__init__(page, query, '')

    def update_meta(self, ):
        title_key = self.query + '_title'
        description_key = self.query + '_description'
        logger.info(" title_key : %s , description_key: %s " % (title_key, description_key))
        title = None
        description = None
        if title_key in self.footer_data:
            title = self.footer_data[title_key]
        if description_key in self.footer_data:
            description = self.footer_data[description_key]
        logger.info(" title : %s , description: %s " % (title, description))
        for meta in self.meta_content["meta"]:
            if meta.get("property") == "og:title":
                if title:
                    meta["content"] = title
            elif meta.get("property") == "og:description" or meta.get("name") == "description":
                if description:
                    meta["content"] = description
        logger.info(self.meta_content["meta"])
        if title:
            self.meta_content["page"]["title"] = title

    def build_meta(self, *args):
        self.update_meta()
        self.meta_content['content'] = {
            'breadcrumbs': []
        }
