import logging

import requests
from django.conf import settings

from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.common.exceptions.custom_exception import ReCaptchaServiceUnavailableException, \
    ReCaptchaVerifyError, InvalidRequestParamInAPIException, ReCaptchaScoreBelowThresholdError

logger = logging.getLogger(__name__)


class ReCaptchaV3Service:

    re_captcha_error_codes = {
        'missing-input-secret': 'The secret parameter is missing.',
        'invalid-input-secret': 'The secret parameter is invalid or malformed.',
        'missing-input-response': 'The recaptcha_user_token parameter is missing.',
        'invalid-input-response': 'The recaptcha_user_token parameter is invalid or malformed.',
        'bad-request': 'The request is invalid or malformed.'
    }

    @staticmethod
    def validate_captcha(recaptcha_token, threshold_score, action_on_page=None):
        if not recaptcha_token:
            raise InvalidRequestParamInAPIException('The recaptcha_user_token parameter is required but missing.')

        score = ReCaptchaV3Service.verify_response_token_and_get_score(recaptcha_token, action_on_page)
        if score < threshold_score:
            raise ReCaptchaScoreBelowThresholdError('ReCAPTCHA score {0} below threshold.'.format(score))

    @staticmethod
    def verify_response_token_and_get_score(recaptcha_token, action_on_page=None):
        response = ReCaptchaV3Service.call_google_recaptcha_service(recaptcha_token)

        success = response.get('success')
        action = response.get('action')
        score = response.get('score')

        if success and ((not action_on_page) or (action_on_page and action == action_on_page)):
            return score
        else:
            error_messages = []

            if not success:
                error_messages.append('User failed reCAPTCHA challenge.')

            if action_on_page and action != action_on_page:
                error_messages.append('Action name mismatch.')

            raise ReCaptchaVerifyError('Error while verifying via reCAPTCHA for user token: {0}'
                                       .format(recaptcha_token), error_messages)

    @classmethod
    def call_google_recaptcha_service(cls, recaptcha_token):
        url = settings.GOOGLE_RECAPTCHA_V3_VERIFY_URL
        try:
            request_data = {
                'secret': settings.GOOGLE_RECAPTCHA_V3_SECRET_KEY,
                'response': recaptcha_token
            }
            res = requests.post(url, data=request_data)
        except Exception as e:
            logger.exception('Exception in calling Google reCAPTCHA service for url: {0} and user token: {1}'
                             .format(url, recaptcha_token))
            slack_alert.send_slack_alert_for_third_party(status=500,
                                                         class_name=ReCaptchaV3Service.__class__.__name__,
                                                         url=url,
                                                         reason='Exception in calling Google reCAPTCHA service '
                                                                'for url: {0} and user token: {1}. {2}'
                                                         .format(url, recaptcha_token, str(e))
                                                         )
            raise ReCaptchaServiceUnavailableException(
                'Google reCAPTCHA service not available for url: {0} and user token : {1}'
                .format(url, recaptcha_token))

        response = res.json()
        error_codes = response.get('error-codes')

        if error_codes:
            slack_alert.send_slack_alert_for_third_party(status=res.status_code,
                                                         class_name=ReCaptchaV3Service.__class__.__name__,
                                                         url=url,
                                                         reason='Error while verifying via reCAPTCHA '
                                                                'for url: {0} and user token: {1}. {2}'
                                                         .format(url, recaptcha_token, response)
                                                         )

            error_messages = []
            for error_code in error_codes:
                error_message = cls.re_captcha_error_codes.get(error_code)
                if not error_message:
                    error_message = error_code.replace('-', ' ') + '.'
                error_messages.append(error_message)

            raise ReCaptchaVerifyError('Error while verifying via reCAPTCHA for url: {0} and user token: {1}. {2}'
                                       .format(url, recaptcha_token, response), error_messages)

        return response
