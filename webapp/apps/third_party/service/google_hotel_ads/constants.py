BASE_URL = "www.treebo.com"
EMAIL_DATA = "alerts.webbooking@treebohotels.com"
MAX_LENGTH_OF_STAY = "30"
MAX_ADVANCE_PURCHASE = "330"
PACKAGE_DESCRIPTION_TEXT = "Air-conditioned rooms(Not in Hill Stations)\nFree Wi-Fi\nComplimentary breakfast\nTV with DTH connection" \
                           "\nFully functional bathroom" \
                           "\nSpotless,fresh linens(towels and bed sheets)\nFree good quality toiletries" \
                           "\nComplimentary bottled water" \
                           "\nElectric kettle"
IMAGE_URL = "https://images.treebohotels.com/files/"
TREEBO_PACKAGE = "TREEBO_STANDARD"
TREEBO_PACKAGE_TEXT = "100% Quality Guarantee"
CURRENCY = "INR"
DEFAULT_CONTACT = "+91 9322-800-100 / 080-3017-8026"
REFUNDABLE_UNTIL_DAYS = "1"
REFUNDABLE_UNTIL_TIME = "12:00:00"
ROOM_CONFIG = "2-0"
# POS_URL = "https://%s%s?checkin=(CHECKINYEAR)-(CHECKINMONTH)-(CHECKINDAY)&checkout=" \
#           "(CHECKOUTYEAR)-(CHECKOUTMONTH)-(CHECKOUTDAY)&city=%s&hotel_id=%s&q=%s" \
#           "&roomconfig=%s&roomtype=(PARTNER-ROOM-ID)" \
#           "&utm_source=affiliates&utm_medium=googlehotelads"
POS_URL = "https://%s(CUSTOM1)?checkin=(CHECKINYEAR)-(CHECKINMONTH)-(CHECKINDAY)&checkout=" \
          "(CHECKOUTYEAR)-(CHECKOUTMONTH)-(CHECKOUTDAY)&(CUSTOM2)" \
          "&roomconfig=%s&roomtype=(PARTNER-ROOM-ID)" \
          "&utm_source=%s&utm_medium=%s&utm_campaign=(IF-PAYMENT-ID)commission(ELSE)cpc(ENDIF)"

GHA_UTM_SOURCE = "googlehotelads"
GHA_UTM_MEDIUM = "affiliates"
GHA_UTM_CAMPAIGN_COMMISSION = "commission"


# GHA - CSV MAIL DETAILS
GHA_CSV_EMAIL_SUBJECT = "GHA RECONCILATION REPORT"
GHA_CSV_EMAIL_BODY = "PFA the reconcilation report"
GHA_CSV_FILE_NAME = "gha_reconcilation_report.csv"
COUNTRY_CODE = "IN"

IST_TIMEZONE = "Asia/Kolkata"
PAYMENT_STATUS = "Invoice Required"
BOOKING_STATUS = {
    "checkout": "Stayed",
    "cancel": "Cancelled",
    "noshow": "No-show",
    "non-commissioned": "Non-commissionable"}


GHA_COMMISSION_DEFAULT_VALUE_IN_PERCENT = 20
