# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
from apps.bookings import constants
from apps.common.data_classes.base_data_class import BaseDataClass
from apps.bookings.models import Booking as BookingModel


class Status(BaseDataClass):
    def __init__(self, name, priority, old_name):
        self.name = name
        self.old_name = old_name
        self.priority = priority

    def __gt__(self, other):
        return self.priority > other.priority

    def __ge__(self, other):
        return self.priority >= other.priority

    def __lt__(self, other):
        return self.priority < other.priority

    def __le__(self, other):
        return self.priority <= other.priority

    def __eq__(self, other):
        return self.priority == other.priority

    def __repr__(self):
        return "<{kls}:{n}>".format(kls=self.__class__.__name__,
                                    n=self.name,
                                    )

    def __hash__(self):
        return hash(self.name)

    def __str__(self):
        return self.name


InitiatedStatus = Status('initiated', 1, BookingModel.INITIATED)
Temporary = Status('temporary', 2, BookingModel.TEMP_BOOKING_CREATED)
ReservedStatus = Status('reserved', 3, BookingModel.RESERVED)
ConfirmedStatus = Status('confirmed', 4, BookingModel.CONFIRM)
CancelledStatus = Status('cancelled', 5, BookingModel.CANCEL)
CheckedOutStatus = Status('checked_out', 6, BookingModel.CHECKEDOUT)
NoShowStatus = Status('noshow', 7, BookingModel.NOSHOW)
CheckedInStatus = Status('checked_in', 8, BookingModel.CHECKEDIN)
PartCheckedInStatus = Status('part_checked_in', 9, BookingModel.PARTCHECKEDIN)
PartCheckedOutStatus = Status('part_checked_out', 10, BookingModel.PARTCHECKEDOUT)

AllStatuses = {
    InitiatedStatus,
    ReservedStatus,
    Temporary,
    ConfirmedStatus,
    CancelledStatus,
    CheckedOutStatus,
    NoShowStatus,
    CheckedInStatus,
    PartCheckedInStatus,
    PartCheckedOutStatus
}

ActiveStatuses = AllStatuses


def status_from_db_name(value):
    try:
        status = [status for status in AllStatuses if status.old_name.lower() == str(value).lower()][0]
        return status
    except IndexError:
        raise RuntimeError('Invalid booking status: {}'.format(value))


def status_from_value(value):
    try:
        status = [status for status in AllStatuses if status.name.lower() == str(value).lower()][0]
        return status
    except IndexError:
        raise RuntimeError('Invalid booking status: {}'.format(value))


def status_in_active_statuses(status):
    return status in ActiveStatuses


def status_in_disabled_statuses(status):
    return []
