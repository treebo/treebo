import logging
import random

from apps.auth.constants import UserContactType
from apps.auth.treebo_authN_client import TreeboAuthNClient
from apps.auth.treebo_authZ_client import TreeboAuthZClient
from apps.auth.user_profile_service_client import UserProfileServiceClient
from apps.common.exceptions.custom_exception import UserWithPhoneAlreadyExist, PhoneNumberDoesnotMatch
from services.restclient.treebo_notificationservice import TreeboNotificationService

logger = logging.getLogger(__name__)


class PmsUserService:
    authN_client = TreeboAuthNClient()
    authZ_client = TreeboAuthZClient()
    treebo_notification_service = TreeboNotificationService()
    ups_client = UserProfileServiceClient()

    def create_user_and_assign_role(self, pms_user_creation_dto):
        first_name = pms_user_creation_dto.get('first_name')
        last_name = pms_user_creation_dto.get('last_name')
        email = pms_user_creation_dto.get('email')
        role = pms_user_creation_dto.get('role', None)
        phone = pms_user_creation_dto.get('phone_number')
        hotel_id = pms_user_creation_dto.get('hotel_id', None)
        aom_email = pms_user_creation_dto.get('aom_email')
        pin = random.randint(1111, 9999)
        authn_user_by_email = self.get_user_by_email(email)
        if not authn_user_by_email:
            user = self.authN_client.create_user(first_name, last_name, email, pin, phone)
            self.update_and_send_pin(pin, first_name, phone, email)
            if aom_email:
                self.send_details_to_aom(aom_email, user, pin)
        else:
            authn_user_by_phone, user_data = self.get_user_by_phone(phone)
            if not authn_user_by_email.phone_number and not authn_user_by_phone:
                self.authN_client.update_user_details(authn_user_by_email.authn_id, first_name, last_name, email, phone)
                self.update_and_send_pin(pin, first_name, phone, email)
                if aom_email:
                    self.send_details_to_aom(aom_email, authn_user_by_email, pin)
            elif not authn_user_by_email.phone_number and authn_user_by_phone:
                raise UserWithPhoneAlreadyExist
            elif authn_user_by_email.phone_number and authn_user_by_email.phone_number == phone:
                self.update_and_send_pin(pin, first_name, phone, email)
                if aom_email:
                    self.send_details_to_aom(aom_email, authn_user_by_email, pin)
            elif authn_user_by_email.phone_number and authn_user_by_email.phone_number != phone:
                raise PhoneNumberDoesnotMatch
            user = authn_user_by_email
        self.authZ_client.assign_role(user.authn_id, hotel_id, role)
        self.create_user_in_ups(user)
        return user

    def get_user_by_phone(self, phone):
        user, user_data = self.authN_client.get_user_by_phone(phone)
        return user, user_data

    def get_user_by_email(self, email):
        user = self.authN_client.get_user_by_email(email)
        return user

    def update_and_send_pin(self, pin, first_name, phone_number, email):
        self.authN_client.reset_auth_password(email, pin)
        message = "Hi {first_name}, Your Password (OTP) for Hotel Superhero (PMS) is {pin}.Thank " \
                  "you.".format(first_name=first_name, pin=pin)
        self.send_message(phone_number, email, message)
        return True

    def send_message(self, phone, email, message):
        notification_id = self.treebo_notification_service.send_sms([phone], message)
        self.treebo_notification_service.send_email([email], message, "New PMS Login Details")
        if notification_id:
            return True

    def send_details_to_aom(self, aom_email, user, pin):
        subject = "New PMS User Details"
        body = """<b>PMS User Details:</b><br/>First name: {first_name}
                  <br/>Last name: {last_name}
                  <br/>Phone number: {phone_number}
                  <br/>Email: {email}
                  <br/>Pin (Password): {pin}
                  <br/>""".format(first_name=user.first_name, last_name=user.last_name,
                                  phone_number=user.phone_number, email=user.email, pin=pin)
        self.treebo_notification_service.send_email(emails=[aom_email], content=body, subject=subject)

    def create_user_in_ups(self, user):
        is_existing_user = self.ups_client.get_user_details(phone_number=user.phone_number)
        if is_existing_user['users']:
            return
        name = dict(first_name=user.first_name, middle_name=None, last_name=user.last_name)

        whatsapp_optin_status = self.notification_client.get_whatsapp_optin_status(phone_number=user.phone_number)

        user_contact_with_phone_number = [dict(
            contact_type=UserContactType.PHONE.value, contact_value=user.phone_number, is_verified=False,
            verified_through=None, is_whatsapp_opted_in=whatsapp_optin_status,
            auth_id=user.authn_id, last_login=None
        )]

        user_contact_with_email = [dict(
            contact_type=UserContactType.PHONE.value, contact_value=user.email, is_verified=False,
            verified_through=None, is_whatsapp_opted_in=None,
            auth_id=user.authn_id, last_login=None
        )]

        user_contact = user_contact_with_phone_number + user_contact_with_email

        data = {
            "data": {
                "user": {
                    "name": name,
                    "gender": None,
                    "date_of_birth": None,
                    "metadata": {},
                },
                "user_contacts": user_contact
            },
        }
        self.ups_client.create_new_user(data)
