from apps.growth.providers.treebo_realization import GrowthServiceProvider


class TreeboRealizationClient:

    def get_cancellation_hash(self, bookings):
        hash_dict = dict()
        request_payload = dict(bookings=[self._convert_booking_to_cancel_url_payload(booking) for booking in bookings])
        cancellation_hash_data = GrowthServiceProvider().cancellation_hash_api_call(request_payload)
        for hash_data in cancellation_hash_data.get('data'):
            hash_dict[hash_data['booking_id']] = (hash_data['hash_value'], hash_data['hash_url'])
        return hash_dict

    def get_booking_details_for_cancellation(self, cancellation_hash):
        booking_details_for_cancellation = GrowthServiceProvider().cancellation_booking_details_api_call(
            cancellation_hash)
        return booking_details_for_cancellation["data"] if booking_details_for_cancellation else None

    def cancel_booking(self, cancellation_hash, cancellation_reason):
        request_payload = dict(cancellation_hash=cancellation_hash, cancellation_reason=cancellation_reason)
        GrowthServiceProvider().cancel_booking_api_call(request_payload)

    @staticmethod
    def _convert_booking_to_cancel_url_payload(booking):
        return dict(booking_id=booking['order_id'],
                    hotel_id=booking['hotel_detail']['hotel_cs_id'],
                    checkin_date=str(booking['checkin_date']))

