# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0046_auto_20161110_0902'),
        ('pricing', '0013_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='luxurytax',
            name='hotel',
            field=models.ForeignKey(
                blank=True,
                to='dbcommon.Hotel',
                null=True,
                on_delete=models.DO_NOTHING),
        ),
    ]
