import celery

from celery import shared_task
from celery.signals import task_prerun
from celery.utils.log import get_task_logger
from django.conf import settings
from django.db.models import F
from apps.fot import emailers as fot_emailers
from apps.fot.models import FotReservationsRequest, Fot
from apps.hms.services.hms_services import HmsServices
from base.filters import CeleryRequestIDFilter
from base.logging_decorator import log_args

logger = get_task_logger(__name__)


@task_prerun.connect()
def set_request_id(
        signal=None,
        sender=None,
        task_id=None,
        task=None,
        args=None,
        **kwargs):
    logger.addFilter(
        CeleryRequestIDFilter(
            kwargs.get('user_id'),
            kwargs.get('request_id'),
            task_id,
            task.name))
    logger.info(
        "set_request_id called with kwargs: %s, and task_id: %s",
        kwargs,
        task_id)


@shared_task(name=settings.CELERY_HMS_TASK_NAME)
@log_args(logger)
def process_hms_events(data):
    """
    This will consumer from Broadcast Queue and then if matches our queue
    :return:
    """
    reservations = data.get('reservations')
    if reservations:
        handle_referral_task_hms(reservations)
        handle_hms_sync_fot(reservations)
        hms_service = HmsServices()
        hms_service.handle_user_and_send_checkout_email(reservations)


def handle_referral_task_hms(reservations):
    logger.info('referral_task_hms called')
    try:
        for reservation in reservations:
            if __is_checked_out_web_booking(reservation):
                hms_service = HmsServices()
                guest = hms_service.fetch_primary_guest(reservation)
                guest_email = str(guest['EMAIL'])
                pre_tax_amount = float(reservation["TOTALPRICEBEFORETAX"])
                tax = float(reservation["TOTALTAX"])
                celery.current_app.send_task(
                    'apps.referral.tasks.send_transaction_event', args=(
                        guest_email, pre_tax_amount + tax))
    except Exception:
        logger.exception('Referral task failed in hms sync')


def handle_hms_sync_fot(reservations):
    logger.info('hms_sync_fot called')
    try:
        reservation_status_dict = {
            'CHECKIN': FotReservationsRequest.CHECKEDIN,
            'CHECKOUT': FotReservationsRequest.CHECKEDOUT,
            'CANCELLED': FotReservationsRequest.CANCELLED,
            'NOSHOW': FotReservationsRequest.NOSHOW}
        for reservation in reservations:
            booking_code = str(reservation.get('ID')) + '|'
            order_id = reservation.get('WRID')
            hotel_hx_id = reservation.get('HOTELID')
            reservation_status = reservation.get('RESSTATUS')
            if reservation_status in reservation_status_dict:
                fot_reservation = fetch_fot_reservation(
                    order_id, booking_code, hotel_hx_id)
                if fot_reservation:
                    logger.info(
                        'hms_sync_fot fetched for order_id: %s, booking_code: %s, new_status: %s, '
                        'reservations_id: %s, old_status: %s' %
                        (order_id,
                         booking_code,
                         reservation_status,
                         fot_reservation.id,
                         fot_reservation.status))
                    fot_reservation.status = reservation_status_dict[reservation_status]
                    fot_reservation.save()
                    block_fot_user_on_noshow(
                        reservation_status, fot_reservation)
    except BaseException:
        logger.exception('hms_sync_fot failed due to an exception')


def fetch_fot_reservation(order_id, booking_code, hotel_hx_id):
    fot_reservation = None
    try:
        if bool(order_id):
            fot_reservation = FotReservationsRequest.objects.get(
                hotel__hotelogix_id=hotel_hx_id, booking__order_id=order_id)
        if bool(booking_code) and not fot_reservation:
            fot_reservation = FotReservationsRequest.objects.get(
                hotel__hotelogix_id=hotel_hx_id, booking__booking_code=booking_code)

    except FotReservationsRequest.DoesNotExist:
        logger.info(
            'hms_sync_fot fetched for order_id: %s, booking_code: %s, hx_id: %s' %
            (order_id, booking_code, hotel_hx_id))

    return fot_reservation


def block_fot_user_on_noshow(reservation_status, fot_reservations_object):
    BLOCK_FOT_USER_ON_NO_SHOW = False

    if reservation_status == 'NOSHOW' and BLOCK_FOT_USER_ON_NO_SHOW:
        try:
            Fot.objects.filter(
                user_id=fot_reservations_object.user_id).update(
                status='BLOCKED', no_shows=F('no_shows') + 1)
            status = fot_emailers.account_blocking_confirmation(
                fot_reservations_object, True)
            logger.info(status)
        except BaseException:
            logger.exception(
                'Fot is not present or sending block email failed')
    else:
        logger.info(
            'Not blocking fot_res_id: %s ' %
            fot_reservations_object.id)


def __is_checked_out_web_booking(reservation):
    if "crs api" in reservation["SOURCE"].lower(
    ) and reservation["RESSTATUS"] == "CHECKOUT":
        return True
