# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0039_city_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='state',
            field=models.ForeignKey(
                blank=True,
                to='dbcommon.State',
                null=True,
                on_delete=models.DO_NOTHING),
        ),
    ]
