class JobRegistry(object):
    def __init__(self):
        self._register = dict()

    def register(self, job_name, job_executor):
        assert job_name not in self._register, 'Duplicate Job %s' % job_name
        self._register[job_name] = job_executor

    def get_executor(self, job_name):
        return self._register[job_name]

    def get(self, job_name):
        return self._register.get(job_name)

    def is_job_registered(self, job_name):
        return job_name in self._register
