# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('discounts', '0013_auto_20170215_0632'),
    ]

    operations = [
        migrations.AddField(
            model_name='discountcoupon',
            name='is_referral',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='discountcoupon',
            name='is_visible',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='discountcoupon',
            name='start_date',
            field=models.DateField(default=datetime.date.today, null=True),
        ),
    ]
