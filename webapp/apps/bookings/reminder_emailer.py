import datetime
import requests
import logging
from datetime import timedelta
from django.core import mail
from django.core.mail import BadHeaderError, EmailMultiAlternatives
from django.template import Context
from django.conf import settings
from django.utils.encoding import smart_str
from django.template.loader import get_template

from apps.bookings.models import Booking
from webapp.conf import base

logger = logging.getLogger(__name__)


def compile_email(from_email, to_email, subject, context, template):
    if from_email == "":
        from_email = 'Treebo Hotels <alerts.webbooking@treebohotels.com>'
    html_template = 'email/booking/' + template + '.html'
    html_content = get_template(html_template).render(context)
    msg = EmailMultiAlternatives(subject, "", from_email, [to_email])
    msg.attach_alternative(html_content, "text/html")
    return msg


def send_email(messages):
    try:
        connection = mail.get_connection()
        connection.open()
        try:
            connection.send_messages(messages)
            connectionStatus = 'Mail sent sucessfully'
        except BadHeaderError:
            connectionStatus = 'Invalid header found.'
            logger.exception(connectionStatus)
        connection.close()
    except BaseException:
        connectionStatus = 'Connection error in sending mails'
        logger.exception(connectionStatus)
    return connectionStatus


def send_sms(sms_template, to_mobile, context):
    text_template = 'email/booking/' + sms_template + '.txt'
    text_content = get_template(text_template).render(context)
    sms_url = str(settings.SMS_VENDOR_URL) + "?method=SendMessage&msg_type=TEXT&userid=" + str(
        settings.SMS_VENDOR_ID) + "&auth_scheme=plain&password=" + str(
        settings.SMS_VENDOR_PASSWORD) + "&v=1.1&format=text&send_to=" + str(
        to_mobile) + "&msg=" + smart_str(text_content)
    logger.info(requests.get(sms_url).text)
    return 'Sms succesfully sent'
