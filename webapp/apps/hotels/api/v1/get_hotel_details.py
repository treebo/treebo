import json
import logging
from datetime import datetime, timedelta

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import JsonResponse

from apps.bookingstash.utils import StashUtils
from apps.common import date_time_utils
from apps.common import utils
from apps.hotels import utils as hotel_utils
from apps.hotels.api.v1.get_deals import HotelDeals
from apps.hotels.api.v1.get_policies import HotelPolicies
from apps.hotels.api.v1.get_rules import HotelRules
from apps.hotels.service.hotel_service import HotelService
from apps.pricing import utils as priceUtils
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.pricing_api_impl import PriceUtilsV2
from base.views.api import TreeboAPIView
from common.constants import common_constants
from common.utilities.deprecation_utils import deprecated
from data_services.respositories_factory import RepositoriesFactory
from data_services.room_repository import RoomDoesNotExist
from dbcommon.models.location import Locality
from services.restclient.pricingrestclient import PricingClient

logger = logging.getLogger(__name__)


class HotelDetails(TreeboAPIView):
    __hotelService = HotelService()

    hotel_repository = RepositoriesFactory.get_hotel_repository()
    room_repository = RepositoriesFactory.get_room_repository()
    facilities_repository = RepositoriesFactory.get_facility_repository()

    def __populatePricingDetails(
            self,
            request,
            checkInDate,
            checkoutDate,
            hotelId,
            roomTypes,
            roomConfigString):
        # TODO: Check on optimising this. We probably don't need to fire the APi for every room type.
        # The API already gives result for all roomTypes

        roomConfig = StashUtils.room_config_string_to_list(roomConfigString)

        for room in roomTypes:
            roomConfigToCheck = roomConfigString
            markSoldOutForRoom = False
            try:
                roomObj = self.room_repository.get_single_Room(pk=room['id'])
                if not StashUtils.room_config_supported(roomObj, roomConfig):
                    roomConfigToCheck = '1-0'
                    markSoldOutForRoom = True
            except RoomDoesNotExist:
                markSoldOutForRoom = True

            if not markSoldOutForRoom:
                checkin_date = checkInDate.strftime(
                    date_time_utils.DATE_FORMAT)
                checkout_date = checkoutDate.strftime(
                    date_time_utils.DATE_FORMAT)
                pricingDetailsAvailability = PricingClient.getPricingAvailability(
                    request, checkin_date, checkout_date, roomConfigToCheck, hotelId, room['room_type'], '')
                if not pricingDetailsAvailability or not pricingDetailsAvailability.get(
                        'available', None):
                    room['available'] = False
                else:
                    room['available'] = True
            else:
                room['available'] = False

            checkin_date = checkInDate.strftime(date_time_utils.DATE_FORMAT)
            end_date = (
                checkInDate +
                timedelta(
                    days=1)).strftime(
                date_time_utils.DATE_FORMAT)
            pricingDetailsRackRate = PricingClient.getPricingAvailability(
                request, checkin_date, end_date, '1-0', hotelId, room['room_type'], '')
            room['price_per_night'] = pricingDetailsRackRate['rackRate']

    @deprecated
    def get(self, request, *args, **kwargs):
        """
        Hotel Detail API, to fetch data for HD page.
        checkin -- Checkin Date (in YYYY-mm-dd format)
        checkout -- Checkout Date (in YYYY-mm-dd format)
        roomconfig -- Room Configuration. Defaults to 1-0
        """
        try:

            hashedHotelId = kwargs.get('hashedHotelId', '')
            checkInDate = datetime.strptime(
                request.GET.get('checkin'),
                date_time_utils.DATE_FORMAT)
            checkOutDate = datetime.strptime(
                request.GET.get('checkout'),
                date_time_utils.DATE_FORMAT)

            hotelId = hotel_utils.decode_hash(hashedHotelId)
            hotel = self.hotel_repository.get_hotel_details_with_all_related_objects(
                hotelId)

            # TODO:Change this serializer part. This is directly dependent on django
            #hotel_serializer = HotelSerializer(instance=hotel)

            hotel_data = self.hotel_repository.get_serialized_data_for_hotel(
                hotel)
            if isinstance(hotel_data, str):
                hotel_data = json.loads(hotel_data)
            #hotel_data = hotel_serializer.data
            #logger.info("Fetching facilities for this hotel, %d", hotelId)
            # hotel_facilities = facilities_data_service.get_facilities_for_hotel(hotelId)['property_amenities']
            # hotel_data['facility_set'] = [{'name':facility.name,
            #                                'css_class' : facility.css_class,
            #                                'url':facility.url} for facility in hotel_facilities]
            self.__populatePricingDetails(
                request,
                checkInDate,
                checkOutDate,
                hotelId,
                hotel_data['rooms'],
                request.GET.get('roomconfig'))
            rulesResult = HotelRules.getHotelRules()
            policiesResult = HotelPolicies.getHotelPolicies()
            dealsResult = HotelDeals.getHotelDeals()
            nearByHotels = self.__getNearByHotels(
                checkInDate, checkOutDate, hotel, request)
            response = self.__getResponse(
                hotel_data,
                rulesResult,
                policiesResult,
                dealsResult,
                nearByHotels)
            return JsonResponse(response)
        except Exception as e:
            logger.error("Error in fetching hotel details", e)
            raise e

    def __getNearByHotels(self, checkInDate, checkOutDate, hotel, request):
        nearByHotels = self.__hotelService.getNearbyHotels(hotel)
        # self.__hotelService.getAvailabilityAndPriceFromDb(nearByHotels, checkInDate, checkOutDate)
        nearByHotelsData = []
        nearByImageParam = utils.Utils.getMobileImageSize("detail-nearBy")
        width, height = utils.Utils.getMobileImageSize("detail-nearBy")

        allLocalities = Locality.objects.all()
        allLocalitiesDict = {}
        for singleLocality in allLocalities:
            allLocalitiesDict[singleLocality.id] = singleLocality

        roomTypeOrder = {'Acacia': 0, 'Oak': 1, 'Maple': 2, 'Mahogany': 3}

        quickbookUrlDict = {}
        roomConfigString = request.GET.get(
            'roomconfig') if 'roomconfig' in request.GET and request.GET.get('roomconfig') else '1-0'
        roomsConfig = StashUtils.room_config_string_to_list(roomConfigString)
        quickbookSuffix = "&checkin={0}&checkout={1}&roomconfig={2}&couponcode=".format(
            checkInDate.strftime(
                date_time_utils.DATE_FORMAT), checkOutDate.strftime(
                date_time_utils.DATE_FORMAT), str(roomConfigString))

        # NOTE START===:Below 4 lines for code is same as in
        # cheapest_pricing_for_hotels API. That API can be modified to accept
        # list of hotels, to avoid duplication
        cheapest_room_type_with_availability = StashUtils.get_cheapest_room_type_from_availability_sync(
            nearByHotels, checkInDate, checkOutDate, [(1, 0)])
        if FeatureToggleAPI.is_enabled("pricing", "v2_impl_enabled", False):
            hotel_wise_prices = PriceUtilsV2.getPriceForMultipleHotelsWithRoomConfigDateRange(
                checkInDate, checkOutDate, nearByHotels, [(1, 0)], cheapest_room_type_with_availability)
        else:
            hotel_wise_prices = priceUtils.PriceUtils.getPriceForMultipleHotelsWithRoomConfigDateRange(
                checkInDate, checkOutDate, nearByHotels, [(1, 0)], cheapest_room_type_with_availability)

        for k, v in list(hotel_wise_prices.items()):
            v['cheapest_room'] = cheapest_room_type_with_availability.get(k)
        # NOTE END=====

        quickbookUrlDict = self.__hotelService.get_quickbook_url_dict_by_hotel(
            nearByHotels,
            checkInDate,
            checkOutDate,
            roomsConfig,
            cheapest_room_type_with_availability)

        for nearByHotel in nearByHotels:
            pricing = hotel_wise_prices[nearByHotel.id]
            # image = nearByHotel.image_set.filter(is_showcased=True).first()

            # TODO: Fix the below logic. Shouldn't be use S3 URL directly to
            # construct image url
            showcased_url = self.hotel_repository.get_showcased_image_url(
                nearByHotel)
            #showcased_url = showcased_obj.url if showcased_obj else ''
            if showcased_url:
                image_url = settings.HOTELS_S3_URL + showcased_url
            else:
                image_url = ''

            hd_url = reverse('pages:hotel-details-new',
                             kwargs=hotel_utils.get_hotel_kwargs(nearByHotel))

            hd_url += "/?checkin={0}&checkout={1}&roomconfig={2}".format(
                checkInDate.strftime(date_time_utils.DATE_FORMAT),
                checkOutDate.strftime(date_time_utils.DATE_FORMAT),
                roomConfigString)
            nearByHotelsData.append({
                'id': nearByHotel.id,
                'name': nearByHotel.name,
                'quickbookUrl': quickbookUrlDict.get(nearByHotel.id),
                'tags': [],
                'coupon': "",
                "remaining": None,
                'url': hd_url,
                'city': nearByHotel.city.as_json(),
                'baseRate': pricing['rack_rate'],
                'priceBreakup': {
                    'price': pricing['pretax'],  # for testing
                    'tax': pricing['tax'],
                    'discount': 0,
                    'total': pricing['sell'],
                },
                'distance': nearByHotel.distance,
                'address': allLocalitiesDict[nearByHotel.locality_id].name,
                'actual_rate': pricing['sell'],
                'pincode': nearByHotel.locality.pincode,
                'available': pricing['cheapest_room']['availability'] != 0,
                'imageParams': utils.Utils.build_image_params(width, height,
                                                              common_constants.FIT_TYPE,
                                                              common_constants.CROP_TYPE,
                                                              common_constants.QUALITY),
                'image_url': image_url
            })
        nearByHotelSortedByDistance = sorted(
            nearByHotelsData, key=lambda h: float(h['distance']))
        return nearByHotelSortedByDistance

    def __getResponse(
            self,
            hotel_data,
            rulesResult,
            policiesResult,
            dealsResult,
            nearByHotels):
        response = {}
        try:
            response['message'] = 'success'
            response['status'] = 1
            response['hotel'] = hotel_data
            response['similar_treebos'] = nearByHotels
            response['rules'] = rulesResult['rules']
            response['policies'] = policiesResult['policies']
            response['deal_block'] = dealsResult['deals']
            response['deal'] = ''
        except Exception as exc:
            logger.debug(exc)

        return response
