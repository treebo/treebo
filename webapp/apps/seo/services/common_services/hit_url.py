import json
import logging

import requests

logger = logging.getLogger(__name__)


class URLBaseClass(object):
    def get_response(self, url, headers=None, content_type=None, data=None):
        response_json = []
        try:
            logger.info(
                "url %s headers %s content_type %s",
                url,
                headers,
                content_type)
            response = requests.get(url=url, headers=headers)
            if response and response.status_code == 200:
                response_json = json.loads(response.text)
                logger.info("response_json %s", response_json)
            else:
                logger.error(
                    "Error in fetching data from prowl api for url %s %s",
                    url,
                    response.status_code)
            return response_json
        except Exception as e:
            logger.exception("Exception in call the url %s", url)
            raise e
