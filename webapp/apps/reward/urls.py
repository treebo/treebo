# -*- coding: utf-8 -*-


from django.conf.urls import url

from apps.reward.api.v1.balance import WalletBalance
from apps.reward.api.v1.statement import WalletStatement

app_name = 'reward_v1'
urlpatterns = [
    url(r'^statement/$', WalletStatement.as_view(), name="wallet-statement-v1"),
    url(r'^balance/$', WalletBalance.as_view(), name="wallet-balance-v1"),
]
