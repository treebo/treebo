# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0037_booking_rate_plan_meta'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='member_discount',
            field=models.DecimalField(
                default=0,
                null=True,
                max_digits=9,
                decimal_places=4),
        ),
    ]
