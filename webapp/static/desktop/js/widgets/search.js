
//l = require('../treebolibs/logger'),

import $ from 'jquery';
import {Observable} from "rx";
import {_} from 'underscore';
import cookies from 'js-cookie';
import queryString from 'query-string';
import {history} from 'html5-history-api';
import moment from 'moment';
import Calendar from '../libs/calendar';

import React from 'react';
import {render, findDOMNode} from 'react-dom';
import { createStore, combineReducers } from 'redux';
import l  from '../treebolibs/logger';
let classnames =require('classnames');

const parsed = queryString.parse(location.search);

let $loader = $("#searchLoader");
let store = '', checkInDate, checkOutDate, calendar;

let roomConfigError = "Please try a different combination for better results.";


var searchdata = {
    lat: '',
    long: '',
    locality: '',
    checkIn: '',
    checkOut: '',
	types: [],
}

$(window).on('click', function(e) {
    if(!e.originalEvent) return;
    var state = e.originalEvent.state;
    var target = $(e.target);
    let $cityDD = $(".extra-cities")
    if (target.attr('id')!="searchSubmitBtn"){
        $cityDD.addClass('hide');
    }

    if(target.closest("#guestNRoomWidget").length ==0){
        if(store){
            store.dispatch({type: "CLOSE_DD"});
        }

    }

});


$("#calendar").on('click', function(e) {
    if(store){
        store.dispatch({type: "CLOSE_DD"});
    }
});

var methods = {
    searchWidgetLoaded: () => {
        methods.initGoogleAutoComplete();
        methods.initCalander();
        methods.initExtraCities();
        methods.initRoomWidget();
        methods.setDefaultValuesFromCookie();
        methods.initSearchControls();
        //methods.initSearchControls();
        $('#searchLoader').removeClass('initial-load');
    },
    initCalander : () =>{
        calendar = new Calendar({
            element: $('.daterange--double'),
            format: {input: 'M/D/YYYY'},
            earliest_date: moment(),
            //latest_date: new Date(),
            start_date: moment().add(1, 'day'),
            end_date: moment().add(2, 'day'),
            callback: function() {
                checkInDate  = moment(this.start_date).format('YYYY-MM-DD'),
                checkOutDate = moment(this.end_date).format('YYYY-MM-DD');
                console.debug('Start Date: '+ checkInDate +'\nEnd Date: '+ checkOutDate);
                $('body').trigger('datechanged');
            }
        });

    },
    initRoomWidget : ()=>{
        // this is reducer function
        let initialState = {
            ddVisbility: false,
            roomConfigVisibility : false,
            ddOptionVisibility : true,
            options : [
                {   selected: true,
                    adults: 1,
                    rooms: 1
                },
                {   selected: false,
                    adults: 2,
                    rooms: 1
                },
                {   selected: false,
                    adults: 2,
                    rooms: 2
                }
            ],
            rooms : [
                {
                    adults : 1,
                    children : 0,
                }
            ]
        };

        if(parsed.roomconfig){
            let rmc = parsed.roomconfig.split(",");
            if(rmc.length < 3 && rmc[0].length ==1 ){
                let index = 0;
                if(rmc.length == 2){
                    index = 2;
                } else if(rmc[0]==2){
                    index = 1;
                }

                let options = initialState.options.map((v,i)=>{
                    return Object.assign({}, v, {
                        selected : index == i
                    })
                })
                initialState = Object.assign({}, initialState, {
                    options : options
                });

            } else {
                let rmcMapped = rmc.map(function(v){
                    let [adults, children] = v.split("-");
                    return {
                        adults : +adults,
                        children : +children
                    };
                });
                initialState = Object.assign({}, initialState, {
                    rooms : rmcMapped,
                    roomConfigVisibility : true,
                    ddOptionVisibility : false
                });
            }

        }


        const roomWidget = (state = initialState, action) => {
            let rooms, udaptedRoom, index;
            switch (action.type) {
                case 'TOGGLE_DD':
                    if(state.ddVisbility==true){ ($('body').trigger('roomconfigchanged')); }
                    return Object.assign({}, state, {
                        ddVisbility : !state.ddVisbility
                    });
                case 'CLOSE_DD':
                    if(state.ddVisbility==true){ ($('body').trigger('roomconfigchanged')); }
                    return Object.assign({}, state, {
                        ddVisbility : false
                    });
                case 'UPDATE_OPTION':
                    let options = state.options.map((v,i)=>{
                        return Object.assign({}, v, {
                            selected : action.id == i
                        })
                    })
                    if(state.ddVisbility==true){ ($('body').trigger('roomconfigchanged')); }
                    return Object.assign({}, state, {
                        options : options
                    }, {
                        ddVisbility : !state.ddVisbility
                    });
                case 'SHOW_ROOMCONFIG':
                    return Object.assign({}, state, {
                        roomConfigVisibility : true
                    }, {
                        ddOptionVisibility : false
                    });
                case 'ADD_ROOM':
                    let addroom = {
                        adults : 1,
                        children : 0,
                    };
                    state.rooms.push(addroom);
                    return state;
                case 'REMOVE_ROOM':
                    rooms = state.rooms, index = action.id;
                    return Object.assign({}, state, {
                        rooms : [
                            ...rooms.slice(0, index),
                            ...rooms.slice(index + 1)
                          ]
                    });
                case 'UPDATE_ADULT':
                    rooms = state.rooms, index = action.id;
                    udaptedRoom = {
                        adults : +action.value,
                        children : +rooms[index]["children"]
                    }
                    return Object.assign({}, state, {
                        rooms : [
                            ...rooms.slice(0, index),
                            udaptedRoom,
                            ...rooms.slice(index + 1)
                          ]
                    });
                case 'UPDATE_CHILDREN':
                    rooms = state.rooms, index = action.id;
                    udaptedRoom = {
                        adults : +rooms[index]["adults"] ,
                        children : +action.value
                    }
                    return Object.assign({}, state, {
                        rooms : [
                            ...rooms.slice(0, index),
                            udaptedRoom,
                            ...rooms.slice(index + 1)
                          ]
                    });
                default:
                    return state;
            }
        }

        var Option = React.createClass({
        	render: function() {
                const {option, keyid} = this.props;

                let sClass  = option.selected ? 'dd__item--selected' : '' ;

        		return (
                    <li className={`dd__item ${sClass}`} onClick= { ()=> {
                            store.dispatch({
                                type: "UPDATE_OPTION",
                                id: keyid
                            })
                        }}>
                        {option.adults}{option.adults > 1 ? ' Adults' : ' Adult' } , {option.rooms} {option.rooms > 1 ? ' Rooms' : ' Room' }
                    </li>
        		);
        	}
        });

        var OptionList = React.createClass({
        	render: function() {
                const {options} = this.props;

        		return (
                    <ul>{
                        options.map((option,i) =>{
                            return (
                                <Option key={i} keyid={i} option = {option}/>
                            )
                        })
                    }
                    </ul>
        		);
        	}
        });

        var Room = React.createClass({
            componentDidUpdate : function () { findDOMNode(this).scrollTop = 0; },

            render: function() {
                const {room, keyid} = this.props;
                let removeLink='';
                if(keyid !== 0){
                    removeLink = (<a href="#" className="room-config__remove-link" onClick= {(e) => {
                        e.preventDefault();
                        e.stopPropagation();
                        store.dispatch({type: "REMOVE_ROOM", id: keyid});
                    }}>&times;</a>);
                }
                let {adults, children} = room;

                let errorClass = classnames({
                    'room-config__error':true,
                    'error':true,
                    'hide': adults <= 3 && children <=2
                });

                return (
                    <li className="room-widget__config__item room-config flex-column">
                        <div className="flex-row flex--align-center">
                            <div className="room-config__room">Room {`${keyid + 1}`}</div>
                            <div className="select room-config__adults">
                                <select className="" value={adults} onChange= {(e) => {
                                    store.dispatch({type: "UPDATE_ADULT", value: e.target.value, id: keyid});
                                }}>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>
                            </div>
                            <div className="select room-config__children">
                                <select className=" " value={children} onChange= {(e) => {
                                    store.dispatch({type: "UPDATE_CHILDREN", value: e.target.value, id: keyid});
                                }}>
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                </select>
                            </div>
                            {removeLink}
                        </div>
                        <div className={errorClass}>{roomConfigError}</div>
                    </li>
                );
            }
        });

        var ConfigOptionList = React.createClass({
            componentDidUpdate: function() {
               findDOMNode(this).scrollTop = 100000;
             },
        	render: function() {
                const {rooms} = this.props;

        		return (
                    <ul className="config-options-list">{
                        rooms.map((room,i) =>{
                            return (
                                <Room key={i} keyid={i} room = {room}/>
                            )
                        })
                    }</ul>
        		);
        	}
        });

        var RoomConfigWidget = React.createClass({

            render: function() {
                const {rooms, keyid} = this.props;

                //let sClass  = option.selected ? 'room-widget-dd__item--selected' : '' ;

                return (
                    <div>
                        <div className="flex-row flex--align-center config-option-label">
                            <span className="room-config__room">&nbsp;</span>
                            <span className="room-config__adults">Adults</span>
                            <span className="room-config__children">Kids (&lt;8 yrs)</span></div>
                            <ConfigOptionList rooms={rooms}/>
                    </div>
                );
            }
        });

        store = createStore(roomWidget);

        var RoomApp = React.createClass({
        	render: function() {
                const {state} = this.props;
                let roomConfigText = '';
                let {rooms} = state;
                let seletedOption = methods.getRoomConfig();

                let kids = seletedOption.children ? seletedOption.children + (seletedOption.children > 1 ? ' Kids, ' : ' Kid, ') : ''
                roomConfigText = `${seletedOption.adults} ${seletedOption.adults > 1 ? 'Adults, ' : 'Adult, '}${kids} ${seletedOption.rooms} ${seletedOption.rooms >1 ? 'Rooms' : 'Room'} `;


        		return (
                    <div className="room-widget">
                        <div className="room-widget__header flex-column" onClick={() => {
                                store.dispatch({type: "TOGGLE_DD"});
                            }}>
                            <div className="room-widget__title search-box__label">Guest and room</div>
                            <div className="room-widget__total search-box__title">{roomConfigText}</div>
                        </div>
                        <div className={`room-widget__dd ${state.ddVisbility ? 'room-widget__dd--show': ''}`}>
                            <div className={`flex-column pos-abs room-widget__options ${state.ddOptionVisibility ? 'room-widget__options--show': ''}`}>
                                <OptionList options={state.options}/>
                                <div className="room-widget__options__item room-widget__options__item--customise" onClick={() => {
                                    store.dispatch({type: "SHOW_ROOMCONFIG"});
                                }}>
                                Customise Room</div>
                            <div className="room-widget__options__customise-info overflow-ellipsis" title="A room of your own, custom built just for you!">A room of your own, custom built just for you!</div>
                            </div>
                            <div className={`flex-column pos-abs room-widget__config ${state.roomConfigVisibility ? 'room-widget__config--show': ''}`}>
                                <RoomConfigWidget rooms={state.rooms}/>
                                <div className="room-widget__config__item room-widget__config__add-room" onClick={() => {
                                    store.dispatch({type: "ADD_ROOM"});
                                }}><i className="icon-plus-circle"></i>Add Room</div>
                            </div>
                        </div>

                    </div>
        		);
        	}
        });

        const renderRoom = () => {
          render(
            // Render the roomApp
            <RoomApp state={store.getState()}/>,document.getElementById('guestNRoomWidget')
          )
        };

        store.subscribe(renderRoom);
        renderRoom();
    },
    initExtraCities: () => {
        $('.extra-cities').click(function(e){
            methods.cityHandler(e);
        });
    },
    initGoogleAutoComplete: () => {
        let searchInput = document.getElementById('searchInput'),
            autoCompleteBox = new google.maps.places.Autocomplete(searchInput, {
                componentRestrictions: {
                    country: "in"
                }
            });

        // on press enter select first result
        (function pacSelectFirst(input) {
        	// store the original event binding function
        	var _addEventListener = (input.addEventListener) ? input.addEventListener : input.attachEvent;

        	function addEventListenerWrapper(type, listener) {
        		// Simulate a 'down arrow' keypress on hitting 'return' when no pac suggestion is selected,
        		// and then trigger the original listener.

        		if (type == "keydown") {
        			var orig_listener = listener;
        			listener = function (event) {
        				if (event.which == 13 ||  event.keyCode == 13 || event.which == 9 ||  event.keyCode == 9) {
        					var suggestion_selected = $(".pac-item.pac-selected")
        						.length > 0;
        					if (!suggestion_selected) {
        						var simulated_downarrow = $.Event("keydown", {
        							keyCode: 40,
        							which: 40
        						})
        						orig_listener.apply(input, [simulated_downarrow]);
        					}
        				}

        				orig_listener.apply(input, [event]);
        			};
        		}

        		// add the modified listener
        		_addEventListener.apply(input, [type, listener]);
        	}

        	if (input.addEventListener)
        		input.addEventListener = addEventListenerWrapper;
        	else if (input.attachEvent)
        		input.attachEvent = addEventListenerWrapper;

        })(searchInput);


        google.maps.event.addListener(autoCompleteBox, 'place_changed', () => {
            $('.extra-cities').addClass('hide');

            var place = autoCompleteBox.getPlace(),
                addressType;
            searchdata['lat'] = place.geometry.location.lat();
            searchdata['long'] = place.geometry.location.lng();
			searchdata['types'] = place.types;
            _.each(place.address_components, function(ac) {
                addressType = ac.types[0];
                if (addressType && addressType === 'locality') {
                    searchdata['locality'] = ac['long_name']
                }
            });
        });
    },
    initSearchControls: () => {
        const parsed = queryString.parse(location.search),
            $searchInput = $("#searchInput");
        l.log($('.query'));
        $('.query').html(parsed.query);
        $searchInput.data('lat', parsed.lat);
        $searchInput.data('long', parsed.long);
        let query = parsed.query;
        if(!parsed.query){
            query = rt.city;
        }
        $searchInput.val(query);

        if(moment().diff(checkInDate, "days") > 0){
            calendar.setStartDate(moment().add(1, 'day').format('YYYY-MM-DD'));
            checkInDate  = moment(calendar.start_date).format('YYYY-MM-DD');
            calendar.setEndDate(moment(checkInDate).add(1, 'day').format('YYYY-MM-DD'));
            checkOutDate  = moment(calendar.end_date).format('YYYY-MM-DD');
        }

        if(moment(checkInDate).diff(checkOutDate, "days") >= 0){
            calendar.setEndDate(moment(checkInDate).add(1, 'day').format('YYYY-MM-DD'));
            checkOutDate  = moment(calendar.end_date).format('YYYY-MM-DD');
        }

        $searchInput.keyup(function(){
            $('.extra-cities').addClass('hide');
        })

    },
    setDefaultValuesFromCookie: () => {
        l.log("Write logic to read from cookie and set the default values");

        if(parsed.checkin && parsed.checkout){
            calendar.setStartDate(parsed.checkin);
            calendar.setEndDate(parsed.checkout);
        }else{
            let cookie_checkin = cookies.get('checkin');
            let cookie_checkout = cookies.get('checkout');
            if(cookie_checkin && cookie_checkout){
                cookie_checkin ? calendar.setStartDate(cookie_checkin) : "";
                cookie_checkout ? calendar.setEndDate(cookie_checkout) : "";
            }
        }
        checkInDate  = moment(calendar.start_date).format('YYYY-MM-DD');
        checkOutDate = moment(calendar.end_date).format('YYYY-MM-DD');
    },
    search: (e) => {
        let baseUrl = '/search/',
            $searchInput = $('#searchInput'),
            lat = searchdata['lat'] || "",
            long = searchdata['long'] || '',
            checkIn = checkInDate,
            checkOut = checkOutDate,
            locality = searchdata.locality || '',
            {rooms} = store.getState(),
            query = encodeURIComponent($searchInput.val().trim()),
            locationtype = searchdata.types.join(',') || parsed.locationtype || '',
            roomsCount = rooms.length;

        let roomConfig = methods.getRoomConfigUrl();
        checkIn ? cookies.set('checkin', checkIn) : "";
        checkOut ? cookies.set('checkout', checkOut) : "";

        let queryParams = `locality=${locality}&query=${query}&lat=${lat}&long=${long}&checkin=${checkIn}&checkout=${checkOut}&roomconfig=${roomConfig}&locationtype=${locationtype}`;

        if (!query) {
            $('.extra-cities').removeClass('hide');
            return;
        } else {
            $loader.removeClass('hide');
        }

        window.location = `${baseUrl}?${queryParams}`;
    },
    getStore:()=>{
       return store.getState();
    },
    getCheckInDate:()=>{
        return checkInDate;
    },
    getCheckOutDate:()=>{
        return checkOutDate;
    },
   getRoomConfig : ()=>{
       const state = store.getState();
       let {rooms} = state, seletedOption;
       if(state.roomConfigVisibility){
           seletedOption = rooms.reduce((p,n)=>{
               var toReturn = {
                   adults: p.adults + n.adults,
                   children: p.children + n.children
               }
               return toReturn;
           },{adults : 0, children : 0});

           seletedOption.rooms = rooms.length;
           //roomConfigText = `${seletedOption.adults} Adults, ${seletedOption.children} Kids, ${seletedOption.rooms} Rooms`;

       } else {
           [seletedOption] = state.options.filter((v,i)=>{
                   return v.selected;
           });
           //roomConfigText = `${seletedOption.adults} adults, ${seletedOption.rooms}rooms`;
       }
       return seletedOption;
   },
   getRoomConfigUrl : ()=>{
        if(!store) return;
       const state = store.getState();
       let {rooms} = state, seletedOption, spa = [];
       if(state.roomConfigVisibility){
           seletedOption = rooms.map(function(v){
               return `${v.adults}-${v.children}`;
           }).join(",");

       } else {
           [seletedOption] = state.options.filter((v,i)=>{
                   return v.selected;
           });


           if(seletedOption.rooms == 2){
               seletedOption = "1,1";
           } else {
               seletedOption = seletedOption.adults;
           }


           //roomConfigText = `${seletedOption.adults} adults, ${seletedOption.rooms}rooms`;
       }
       return seletedOption;
   },
   cityHandler: (e) => {
        var $li = $($(e.target).closest(".city").get(0));
        searchdata.lat = $li.attr('latitude');
        searchdata.long = $li.attr('longitude');
        searchdata.locality = $li.attr('locality');
        $('#searchInput').val(searchdata.locality);
        $li.parent().addClass('hide');
    },
    getAdults(){
        var config = String(methods.getRoomConfigUrl() || parsed.roomconfig);
        if(config.indexOf(",")==-1){
            return config;
        }
        var adults = _.compact(_.map(config.split(','),function(room){
            return room.split('-')[0];
        }))
        return _.reduce(adults, function(memo, num){ return memo + parseInt(num,10); }, 0);
    },
    getKids(){
        var config = String(methods.getRoomConfigUrl() || parsed.roomconfig);
        if(config.indexOf(",")==-1){
            return "0";
        }
        var kids = _.compact(_.map(config.split(','),function(room){
            return room.split('-')[1];
        }));

        return _.reduce(kids, function(memo, num){ return memo + parseInt(num,10); }, 0);
    },
    getRooms(){
        var config = String(methods.getRoomConfigUrl() || parsed.roomconfig);
        if(config.indexOf(",")==-1){
            return config
        }
        var rooms = config.split(",");
        return rooms.length;
    }

}

$("#searchSubmitBtn").click(function(){
    methods.search();
});

// Exposing functions for analytics
rt.getCheckin = methods.getCheckInDate;
rt.getCheckout =methods.getCheckOutDate;
rt.getRooms = methods.getRooms;
rt.getAdults = methods.getAdults;
rt.getKids = methods.getKids;

module.exports = {
    name: "searchWidget",
    methods: methods
}
