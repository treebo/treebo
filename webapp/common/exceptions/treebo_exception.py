# TODO define a dictionary of error codes and error messages within the
# custom exception classes and access / output that
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from common.constants import error_codes


class TreeboException(Exception):
    """
    try:
        raise ErrorWithArgs(1, "text", "some more text")
    except ErrorWithArgs as e:
        print "%d: %s - %s" % (e.args[0], e.args[1], e.args[2])
    """

    def __init__(self, *args):
        # *args is used to get a list of the parameters passed in
        self.message = 'Unhandled exception.'
        self.developer_message = ''
        self.status_code = HTTP_500_INTERNAL_SERVER_ERROR
        self.app_type = 'WEB_'
        self.error_code = self.app_type + "1000"
        super(TreeboException, self).__init__(*args)

    def __str__(self):
        return self.message


class TreeboValidationException(TreeboException):
    def __init__(self, *args):
        super(TreeboValidationException, self).__init__(*args)
        code = args[0]
        if error_codes.get(code) is not None:
            self.code = code
            self.message = error_codes.get(self.code).get('msg')
        else:
            self.code = '400'
            self.message = str(code)


class ExternalServiceException(TreeboException):
    """
        Wrapper Exception to be raised when external API calls fail
    """

    def __init__(self, *args):
        super(ExternalServiceException, self).__init__(*args)
