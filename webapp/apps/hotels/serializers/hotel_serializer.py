from django.conf import settings
from rest_framework import serializers
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from dbcommon.models.hotel import Hotel
from .direction_serializer import DirectionSerializer
from .facility_serializer import FacilitySerializer
from .image_serializer import ImageSerializer
from .room_serializer import RoomSerializer
from .seo_serializer import HotelSeoDetailsSerializer

__author__ = 'Rajdeep'


class BasicHotelSerializer(serializers.ModelSerializer):
    city = serializers.SerializerMethodField('getCityName')
    locality = serializers.SerializerMethodField('getLocalityName')
    state = serializers.SerializerMethodField('get_state_name')
    image_set = ImageSerializer(required=False, many=True)
    image_url = serializers.SerializerMethodField()
    room_types = serializers.SerializerMethodField()

    class Meta:
        model = Hotel
        fields = (
            'id',
            'name',
            'description',
            'city',
            'state',
            'hotelogix_id',
            'phone_number',
            'room_count',
            'checkin_time',
            'checkout_time',
            'locality',
            'street',
            'latitude',
            'longitude',
            'hotelogix_name',
            'status',
            'image_set',
            'image_url',
            'room_types')

    def getCityName(self, hotel):
        city_name = hotel.city.name
        return city_name

    def getLocalityName(self, hotel):
        localityName = hotel.locality.name
        return localityName

    def get_state_name(self, hotel):
        state_name = hotel.state.name
        return state_name

    def get_image_url(self, hotel):
        showcased_url = hotel.get_showcased_image_url()
        if showcased_url and showcased_url.url:
            return get_image_uncode_url(showcased_url.url)
        else:
            return ""

    def get_room_types(self, hotel):
        rooms = hotel.rooms.all()
        return [room.room_type_code for room in rooms]


class HotelSerializer(serializers.ModelSerializer):
    complete_address = serializers.SerializerMethodField('get_hotel_details')
    cityName = serializers.SerializerMethodField('getCityName')
    localityName = serializers.SerializerMethodField('getLocalityName')
    rooms = RoomSerializer(required=False, many=True)
    directions_set = DirectionSerializer(required=False, many=True)
    image_set = ImageSerializer(required=False, many=True)
    facility_set = FacilitySerializer(required=False, many=True)
    seodetails_set = HotelSeoDetailsSerializer(required=False, many=True)
    hotel_highlights = serializers.SlugRelatedField(read_only=True, many=True,
                                                    slug_field='description')

    def get_hotel_details(self, hotel):
        city_name = hotel.city.name
        pincode = hotel.locality.pincode
        street = hotel.street
        localityName = hotel.locality.name

        return street + ", " + city_name + ", " + str(pincode)

    def getCityName(self, hotel):
        city_name = hotel.city.name
        return city_name

    def getLocalityName(self, hotel):
        localityName = hotel.locality.name
        return localityName

    class Meta:
        model = Hotel
        fields = (
            'name',
            'description',
            'city',
            'cityName',
            'localityName',
            'state',
            'hotelogix_id',
            'phone_number',
            'room_count',
            'checkin_time',
            'checkout_time',
            'locality',
            'street',
            'latitude',
            'longitude',
            'hotelogix_name',
            'status',
            'price_increment',
            'complete_address',
            'rooms',
            'directions_set',
            'image_set',
            'facility_set',
            'seodetails_set',
            'hotel_highlights')

class HotelSerializerWithoutFacilities(serializers.ModelSerializer):#TODO:Obviously its a weird name
    complete_address = serializers.SerializerMethodField('get_hotel_details')
    cityName = serializers.SerializerMethodField('getCityName')
    localityName = serializers.SerializerMethodField('getLocalityName')
    rooms = RoomSerializer(required=False, many=True)
    directions_set = DirectionSerializer(required=False, many=True)
    image_set = ImageSerializer(required=False, many=True)
    seodetails_set = HotelSeoDetailsSerializer(required=False, many=True)
    hotel_highlights = serializers.SlugRelatedField(read_only=True, many=True,
                                                    slug_field='description')

    def get_hotel_details(self, hotel):
        city_name = hotel.city.name
        pincode = hotel.locality.pincode
        street = hotel.street
        localityName = hotel.locality.name

        return street + ", " + city_name + ", " + str(pincode)

    def getCityName(self, hotel):
        city_name = hotel.city.name
        return city_name

    def getLocalityName(self, hotel):
        localityName = hotel.locality.name
        return localityName

    class Meta:
        model = Hotel
        fields = (
            'name',
            'description',
            'city',
            'cityName',
            'localityName',
            'state',
            'hotelogix_id',
            'phone_number',
            'room_count',
            'checkin_time',
            'checkout_time',
            'locality',
            'street',
            'latitude',
            'longitude',
            'hotelogix_name',
            'status',
            'price_increment',
            'complete_address',
            'rooms',
            'directions_set',
            'image_set',
            'facility_set',
            'seodetails_set',
            'hotel_highlights')

class MinimalDetailsHotelSerializer(serializers.ModelSerializer):
    city = serializers.SerializerMethodField('get_city_name')
    state = serializers.SerializerMethodField('get_state_name')

    class Meta:
        model = Hotel
        fields = (
            'id',
            'name',
            'city',
            'state',
            'hotelogix_id',
            'phone_number',
            'room_count',
            'checkin_time',
            'checkout_time',
            'street',
            'latitude',
            'longitude',
            'hotelogix_name',
            'status',
        )

    def get_city_name(self, hotel):
        city_name = hotel.city.name
        return city_name

    def get_state_name(self, hotel):
        state_name = hotel.state.name
        return state_name


class CSHotelSerializer(serializers.ModelSerializer):
    city = serializers.SerializerMethodField('getCityName')
    locality = serializers.SerializerMethodField('getLocalityName')
    state = serializers.SerializerMethodField('get_state_name')
    image_set = ImageSerializer(required=False, many=True)
    image_url = serializers.SerializerMethodField()
    room_types = serializers.SerializerMethodField()

    class Meta:
        model = Hotel
        fields = (
            'id',
            'name',
            'cs_id',
            'description',
            'city',
            'state',
            'hotelogix_id',
            'phone_number',
            'room_count',
            'checkin_time',
            'checkout_time',
            'locality',
            'street',
            'latitude',
            'longitude',
            'hotelogix_name',
            'status',
            'image_set',
            'image_url',
            'room_types')

    def getCityName(self, hotel):
        city_name = hotel.city.name
        return city_name

    def getLocalityName(self, hotel):
        localityName = hotel.locality.name
        return localityName

    def get_state_name(self, hotel):
        state_name = hotel.state.name
        return state_name

    def get_image_url(self, hotel):
        showcased_url = hotel.get_showcased_image_url()
        if showcased_url and showcased_url.url:
            return settings.S3_URL  + 'files/' + showcased_url.url
        else:
            return ""

    def get_room_types(self, hotel):
        rooms = hotel.rooms.all()
        return [room.room_type_code for room in rooms]
