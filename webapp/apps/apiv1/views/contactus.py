import datetime
import json
import logging

from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from apps.apiv1.apihelper import Helper
from base import log_args
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class SaveContactUs(TreeboAPIView):
    def __init__(self, **kwargs):
        super(SaveContactUs, self).__init__(**kwargs)
        self.__funcDict = {
            'hotel-rooms-availability-form': self.__availability,
            'corporate-travelagent-form': self.__corporate,
            'booking-form': self.__booking,
            'refund-form': self.__refund,
            'checkin-checkout-form': self.__checkinout,
            'others-form': self.__other
        }

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SaveContactUs, self).dispatch(*args, **kwargs)

    def get(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        try:
            jsonString = json.dumps(request.data)
            jsonData = json.loads(jsonString)
            subject, emailContent, toEmail = self.__delegate(jsonData)
            fromEmail = settings.SERVER_EMAIL

            status = self.__email(
                subject + " - " + str(
                    datetime.datetime.now().strftime('%Y-%m-%d %H:%M UTC')),
                emailContent,
                fromEmail,
                toEmail)
            if "success" == status:
                return Response(
                    {"status": "success", "msg": "Successfully Submitted"})
            else:
                return Response(
                    {"status": "error", "msg": "Unable to save data"}, status=404)
        except Exception as exc:
            logger.exception(exc)
            data = {"status": "error", "msg": "Unbale to save data"}
            return Response(data, status=404)

    def __delegate(self, jsonData):
        """Dispatch method"""
        formType = jsonData['form-type']
        del jsonData['form-type']
        if "csrfmiddlewaretoken" in jsonData:
            del jsonData["csrfmiddlewaretoken"]
        method = self.__funcDict[formType]
        return method(jsonData)

    def __availability(self, jsonData):
        subject = settings.CONTACTUS_AVAILABILITY_SUBJECT
        emailList = settings.CONTACTUS_AVAILABILITY_EMAIL_LIST
        return subject, Helper.getEmailContent(jsonData), emailList

    def __corporate(self, jsonData):
        subject = settings.CONTACTUS_CORPORATE_SUBJECT
        emailList = settings.CONTACTUS_CORPORATE_EMAIL_LIST
        return subject, Helper.getEmailContent(jsonData), emailList

    def __booking(self, jsonData):
        subject = settings.CONTACTUS_BOOKING_SUBJECT % {
            'category': jsonData['category']}
        emailList = settings.CONTACTUS_BOOKING_EMAIL_LIST
        return subject, Helper.getEmailContent(jsonData), emailList

    def __refund(self, jsonData):
        subject = settings.CONTACTUS_REFUND_SUBJECT
        emailList = settings.CONTACTUS_REFUND_EMAIL_LIST
        return subject, Helper.getEmailContent(jsonData), emailList

    def __checkinout(self, jsonData):
        subject = settings.CONTACTUS_CHECKINOUT_SUBJECT
        emailList = settings.CONTACTUS_CHECKINOUT_EMAIL_LIST
        return subject, Helper.getEmailContent(jsonData), emailList

    def __other(self, jsonData):
        subject = settings.CONTACTUS_OTHER_SUBJECT
        emailList = settings.CONTACTUS_OTHER_EMAIL_LIST
        return subject, Helper.getEmailContent(jsonData), emailList

    def __email(self, subject, emailContent, fromEmail, toMailList):
        try:
            msg = EmailMessage(subject, emailContent, fromEmail, toMailList)
            msg.content_subtype = "html"
            msg.send()

            logger.info("Successfully email %s" % subject)
            return "success"

        except Exception as exc:
            logger.exception(
                "Error in sending %s Email with exception %s" %
                (subject, exc))
            return "failure"
