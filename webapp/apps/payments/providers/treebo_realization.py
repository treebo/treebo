import json
import logging

import requests
from django.conf import settings

from apps.common.slack_alert import SlackAlertService as slack_alert
from data_services.exceptions import ExternalClientException

logger = logging.getLogger(__name__)


class GrowthServiceProvider:
    PARTPAY_AMOUNT_API = "/growth/payment/partpay-amount/"
    GENERATE_PAYMENT_LINK_API = "/growth/payment/generate-pending-amount-payment-link/"

    def partpay_amount_api_call(self, params):
        response = self.__make_call(method="GET", page_name=self.PARTPAY_AMOUNT_API, params=params)
        if response and response.get("data"):
            return response
        else:
            logger.error("Unexpected response from realization client: {}".format(response))

    def generate_payment_link_api_call(self, data):
        response = self.__make_call(method="POST", page_name=self.GENERATE_PAYMENT_LINK_API,
                                    data=data)
        if response and response.get("data"):
            return response
        else:
            logger.error("Unexpected response from realization client: {}".format(response))

    @staticmethod
    def __make_call(method, page_name, params=None, data=None):
        url = settings.GROWTH_REALIZATION_API_HOST + page_name
        logger.info('Calling growth api to get partial payment amount. '
                    'Url: {url} and Params: {params}'.format(url=url, params=params))
        response = requests.request(method, url=url, data=json.dumps(data), allow_redirects=True,
                                    headers={'Content-Type': 'application/json'}, params=params)

        if not response or not response.ok:
            slack_alert.send_slack_alert_for_third_party(status=response.status_code,
                                                         class_name=GrowthServiceProvider.__class__.__name__,
                                                         url=url,
                                                         reason=response.json().get('message'),
                                                         data=json.dumps(data))
            raise ExternalClientException(
                "Growth Service API Error. Status Code: {0}, Errors: {1}".format(
                    response.json().get('status_code'), response.json().get('message')))
        return response.json()
