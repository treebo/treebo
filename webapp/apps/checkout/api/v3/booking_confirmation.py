import logging

from requests.exceptions import HTTPError
from rest_framework.authentication import BasicAuthentication
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.checkout.dto.booking_confirmation_dto import BookingConfirmationDTO
from apps.checkout.models import BookingRequest
from apps.checkout.service.analytics.order_analytics import confirm_booking_analytics
from apps.checkout.service.booking_confirmation_service import BookingConfirmationService
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)


class ConfirmBooking(TreeboAPIView):
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    booking_confirmation_service = BookingConfirmationService()

    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    def post(self, request, *args, **kwargs):
        try:
            booking_confirmation_dto = BookingConfirmationDTO(
                data=request.data)
            if not booking_confirmation_dto.is_valid():
                logger.error(
                    " invalid request booking confirmation %s ",
                    request.data)
                return api_response.error_response_from_error_code(
                    error_codes.INVALID_CONFIRM_BOOKING_REQUEST, api_name=ConfirmBooking.__name__)

            booking_request = BookingRequest.objects.filter(
                order_id=booking_confirmation_dto.data['order_id']).last()
            if not booking_request:
                logger.exception(
                    "booking not found %s ",
                    booking_confirmation_dto.data['order_id'])
                return api_response.error_response_from_error_code(
                    error_codes.BOOKING_NOT_FOUND, api_name=ConfirmBooking.__name__)

            order_id, is_already_verified = self.booking_confirmation_service.confirm_paynow_booking(
                booking_confirmation_dto.data, booking_request)
            order_event_args = confirm_booking_analytics(
                request, booking_request)
            data = {
                'status': BookingRequest.COMPLETED,
                'order_id': order_id,
                "is_already_verified": is_already_verified,
                'order_event_args': order_event_args
            }
            return api_response.prep_success_response(data)
        except HTTPError as e:
            logger.exception(
                "Unable to confirm booking with error %s ",
                request.data)
            return api_response.error_response_from_error_code(
                error_codes.CONFIRM_BOOKING_FAILED,
                status_code=e.response.status_code,
                api_name=ConfirmBooking.__name__)
        except TreeboException as e:
            return api_response.treebo_exception_error_response(e)
        except Exception:
            logger.exception(
                "Unable to confirm booking with internal server error %s ",
                request.data)
            return api_response.error_response_from_error_code(
                error_codes.CONFIRM_BOOKING_FAILED,
                status_code=HTTP_500_INTERNAL_SERVER_ERROR,
                api_name=ConfirmBooking.__name__)
