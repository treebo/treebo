from rest_framework import serializers


class TokenLoginDTO(serializers.Serializer):
    token = serializers.CharField(max_length=200)
