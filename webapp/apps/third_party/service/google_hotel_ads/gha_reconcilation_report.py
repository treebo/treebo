

import io
import csv
import logging

import datetime
from django.conf import settings
from django.core.mail import EmailMessage
from django.db.models import Prefetch
from pytz import timezone

from apps.bookings.models import Booking
from apps.checkout.models import BookingRequest
from apps.third_party.service.google_hotel_ads import constants
from apps.third_party.service.google_hotel_ads.booking_details import FetchBookingDetails
from apps.third_party.service.google_hotel_ads.hotel_list import HotelListFeedService
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class GenerateGHACSVService(object):
    def create_and_mail_csv(self):
        logger.info("Updating/Creating GHA csv")
        try:
            csv_writer, csv_file = self.initilaize_csv()
            report_start_date_time = datetime.datetime.now(
            ) - datetime.timedelta(days=settings.GHA_REPORT_INTERVAL_DAYS)
            report_start_date_time = report_start_date_time.replace(
                hour=0, minute=0, second=0, microsecond=0)
            report_end_date_time = datetime.datetime.now() - datetime.timedelta(
                days=1)
            report_end_date_time = report_end_date_time.replace(
                hour=0, minute=0, second=0, microsecond=0)
            logger.info(
                "report_start_date_time %s, report_end_date_time %s",
                report_start_date_time,
                report_end_date_time)
            booking_request_data = BookingRequest.objects.filter(
                created_at__gte=report_start_date_time,
                created_at__lte=report_end_date_time,
                utm_source__icontains=constants.GHA_UTM_SOURCE,
                utm_campaign__icontains=constants.GHA_UTM_CAMPAIGN_COMMISSION)
            # logger.info("booking_request_data %s", booking_request_data)

            booking_request_id_list = []
            wrid_list = []
            for booking_request in booking_request_data:
                booking_request_id_list.append(booking_request.booking_id)
                wrid_list.append(booking_request.order_id)
            logger.info(
                "booking_request_id_list %s wrid_list %s",
                booking_request_id_list,
                wrid_list)

            fetch_booking_details = FetchBookingDetails()
            booking_status_response = fetch_booking_details.fetch_booking_status(
                wrid_list)
            logger.info("booking_status_response %s", booking_status_response)

            bookings_data = Booking.objects.select_related(
                'room',
                'user_id',
                'paymentorder').prefetch_related(
                Prefetch(
                    'hotel',
                    queryset=Hotel.objects.filter(
                        status=1).select_related(
                        'city',
                        'locality',
                        'state'))).filter(
                id__in=booking_request_id_list)

            # logger.info("bookings_data %s", bookings_data)

            for booking in bookings_data:
                booking_request_info = booking_request_data.filter(
                    booking_id=booking.id).first()
                if booking.order_id in booking_status_response:
                    booking_status = booking_status_response[booking.order_id]
                    mapped_status = constants.BOOKING_STATUS[booking_status.lower(
                    )]
                    logger.info(
                        "Found the wrid in the response with status %s and mapped status %s .. append to csv ",
                        booking_status,
                        mapped_status)
                    self.append_to_csv(
                        csv_writer,
                        booking.hotel,
                        booking,
                        booking_request_info,
                        mapped_status)
                else:
                    logger.info(
                        "Could not find the wrid in the response ..  do not append to csv ")
            self.send_csv_file(csv_file)
        except Exception:
            logger.exception("Exception in creating GHA CSV FILE")

    def initilaize_csv(self, ):
        try:
            logger.info(
                " Initializing CSV for GHA reconciliation report of Treebo")
            csv_file = io.StringIO()
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow(
                [
                    'Hotel ID',
                    'Hotel Name',
                    'Hotel Address',
                    'Hotel City',
                    'Hotel State / Region',
                    'Hotel Postal Code',
                    'Hotel Country Code',
                    'Hotel Phone Number',
                    'Booking Reference',
                    'Booking Date andTime',
                    'Check - in Date',
                    'Check - out Date',
                    'Number of Rooms',
                    'Number of Guests',
                    'Booking Revenue',
                    'Booking Revenue Currency',
                    'Booking Revenue Currency to Billing Currency Conversion Rate',
                    'Booking Status',
                    'Commission',
                    'Commission Currency',
                    'Commission Currency to Billing Currency Conversion Rate',
                    'Payment Date',
                    'Payment Status'])
            return csv_writer, csv_file
        except Exception:
            logger.exception("Exception in initializing csv writer for GHA")

    def send_csv_file(self, csv_file):
        try:
            logger.info("Sending Updated GHA reconcilation report CSV")
            mail = EmailMessage(
                constants.GHA_CSV_EMAIL_SUBJECT,
                constants.GHA_CSV_EMAIL_BODY,
                settings.SERVER_EMAIL,
                settings.GHA_CSV_EMAIL_ID)
            mail.attach(
                constants.GHA_CSV_FILE_NAME,
                csv_file.getvalue(),
                'text/csv')
            mail.send()
            logger.info("mail sent")
        except Exception:
            logger.exception(
                "Error occured in sending  google hotel ads renconcilation report email")

    def append_to_csv(
            self,
            csv_writer,
            hotel,
            booking,
            booking_request_info,
            mapped_status):
        try:
            logger.info("Appending to CSV for the Hotel ID %s", hotel.id)
            hotel_list_feed_service = HotelListFeedService()
            phone_number = hotel_list_feed_service.generate_main_phone_number(
                hotel)
            booking_date_time = timezone(
                constants.IST_TIMEZONE).localize(
                booking.created_at).replace(
                microsecond=0)
            booking_date_time = booking_date_time.isoformat()
            num_guests = booking.adult_count + booking.child_count
            if mapped_status.lower() == 'stayed':
                commission = (
                    constants.GHA_COMMISSION_DEFAULT_VALUE_IN_PERCENT / 100) * booking.total_amount
            else:
                commission = 0
            csv_writer.writerow([hotel.id,
                                 hotel.name,
                                 hotel.street,
                                 hotel.city.name,
                                 hotel.state.name,
                                 hotel.locality.pincode,
                                 constants.COUNTRY_CODE,
                                 phone_number,
                                 booking.order_id,
                                 booking_date_time,
                                 booking.checkin_date,
                                 booking.checkout_date,
                                 booking_request_info.room_required,
                                 num_guests,
                                 booking.total_amount,
                                 constants.CURRENCY,
                                 1,
                                 mapped_status,
                                 commission,
                                 constants.CURRENCY,
                                 1,
                                 '',
                                 constants.PAYMENT_STATUS])
        except Exception:
            logger.exception(
                "Exception in appending a row to GHA CSV for booking %s and hotel code %s",
                booking.order_id,
                hotel.id)
