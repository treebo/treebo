import logging
import requests
import json
import uuid
import urllib.request
import urllib.parse
import urllib.error
import os
import ast

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from django.conf import settings
from django.core.urlresolvers import reverse

from apps.content.models import ContentStore
from webapp.common.utilities.date_time_utils import get_current_ist_time
from services.notification.email.email_notification_service import SendEmailService
from decimal import Decimal
from datetime import datetime

logger = logging.getLogger(__name__)


class CreateCouponService():
    def create_coupon(
            self,
            coupon,
            constraints_data,
            apply_constraint,
            user_email):
        logger.info("in create coupon")
        total_coupon = 0
        success_coupon = []
        failed_coupon = []
        success_constraint = []
        failed_constraint = []
        expiry_date = ""
        try:
            if coupon['total_coupon']:
                total_coupon = int(coupon['total_coupon'])

            if total_coupon > 0:
                coupon_codes = self.generate_coupon_code(total_coupon)
            else:
                coupon_codes = str(coupon['custom_coupon']).split(',')

            coupon_payload = self.create_coupon_payload(coupon)
            for code in coupon_codes:
                code = code.strip()
                coupon_payload["code"] = code
                response = requests.post(
                    settings.TREEBO_WEB_URL +
                    reverse("discount:create-discount"),
                    data=coupon_payload,
                    allow_redirects=True,
                    timeout=20)
                if response.status_code == 200:
                    logger.info(
                        "success response recieved from coupon creation API")
                    post_data = response.json()
                    data = post_data['data']
                    if data['status'].lower() == "success":
                        success_coupon.append(code)
                        if str(apply_constraint).lower() == "yes":
                            success, failed = self.create_constraint(
                                code, constraints_data)
                            success_constraint.append(success)
                            failed_constraint.append(failed)
                    else:
                        failed_coupon.append(code)
                else:
                    failed_coupon.append(code)
            logger.debug("Successful coupons created %s", success_coupon)
            logger.debug("Failed coupons %s", failed_coupon)
            self.store_coupons_and_send_email(
                success_coupon, failed_coupon, user_email)
            logger.info("Create coupon done")
        except Exception as exc:
            logger.exception(
                'coupon creation failed for data coupn %s  constraints_data %s apply_constraint %s user_email %s ',
                coupon,
                constraints_data,
                apply_constraint,
                user_email)
            raise Exception(str(exc))

    def generate_coupon_code(self, total_coupon):

        code_length = 7
        content_object = ContentStore.objects.filter(
            name="coupon_code_length", version=1).first()
        if content_object:
            code_length = int(content_object.value)

        coupon_codes = [str(uuid.uuid1().hex.upper()[0:code_length])
                        for _ in range(0, total_coupon)]
        return coupon_codes

    def create_coupon_payload(self, coupon):
        try:
            logger.info("In create coupon payload")

            coupon_payload = {
                "discount_operation": str(coupon["discount_operation"]).upper(),
                "discount_value": Decimal(coupon["discount_value"]) if coupon["discount_value"] else 0,
                "max_available_usages": int(coupon["max_available_usages"]) if coupon["max_available_usages"] else 0,
                "max_available_discount": int(coupon["max_available_discount"]) if coupon["max_available_discount"] else 0,
                "expiry": str(coupon["expiry"]) if coupon["expiry"] else "",
                "is_active": "true" if ("is_active" in coupon and str(coupon["is_active"]).lower() == "on") else False,
                "tagline": str(coupon["tagline"]) if coupon["tagline"] else "",
                "terms": str(coupon["terms"]) if coupon["terms"] else "",
                "coupon_message": str(coupon["coupon_message"]) if coupon["coupon_message"] else "",
                "coupon_type": str(coupon["coupon_type"]).upper(),
                "discount_application": int(coupon["discount_application"]),
                "is_auto_applicable": "true" if ("is_auto_applicable" in coupon and str(coupon["is_auto_applicable"]).lower() == "on") else False,
                "series_value": str(coupon["series_value"]) if coupon["series_value"] else "",
                "is_internal": "true" if ("is_internal" in coupon and str(coupon["is_internal"]).lower() == "on") else False,
                "price_component_applicable": str(coupon["price_component_applicable"]),
            }

            return coupon_payload
        except Exception as exc:
            logger.exception(exc)
            raise Exception("Coupon payload creation failed")

    def create_constraint(self, code, constraints_data):

        logger.info('in create constraint')
        success_constraint = []
        failed_constraint = []
        try:
            for constraint in constraints_data:
                response = requests.post(
                    settings.TREEBO_WEB_URL +
                    reverse("discount:create-constraint"),
                    data={
                        "code": code,
                        "keyword": str(
                            constraint['keyword']),
                        "operation": str(
                            constraint['operator']),
                        "value_one": str(
                            constraint['value_one']),
                        "value_two": str(
                            constraint['value_two'])},
                    allow_redirects=True,
                    timeout=20)
                if response.status_code == 200:
                    logger.info(
                        "success response recieved from constraint creation API")
                    post_data = response.json()
                    data = post_data['data']
                    if data['status'].lower() == "success":
                        success_constraint.append(code)
                    else:
                        failed_constraint.append(code)
                else:
                    failed_constraint.append(code)
            return success_constraint, failed_constraint
        except Exception as exc:
            logger.exception(exc)
            raise Exception("Failed to create constraint")

    def store_coupons_and_send_email(
            self,
            success_coupon,
            failed_coupon,
            user_email):
        logger.info("in send email")

        success_file_path_name = ""
        failed_file_path_name = ""
        success_file_name = ""
        failed_file_name = ""
        success_coupon_url = ""
        attachments_list = []
        try:
            if len(success_coupon) > 0:
                success_file_path_name, success_file_name = self.generate_file_path(
                    len(success_coupon), "success")
                with open(success_file_path_name, 'w+') as file:
                    for item in success_coupon:
                        file.write("%s\n" % item)
                    logger.info("Success coupon data written done")

            if len(failed_coupon) > 0:
                failed_file_path_name, failed_file_name = self.generate_file_path(
                    len(failed_coupon), "failed")
                with open(failed_file_path_name, 'w+') as file:
                    for item in failed_coupon:
                        file.write("%s\n" % item)
                    logger.info("Failed coupon data written done")

            self.upload_file_to_s3(
                success_file_path_name,
                failed_file_path_name,
                success_file_name,
                failed_file_name)

            if success_file_name:
                success_coupon_url = settings.S3_GENERATED_COUPONS_URL + \
                    urllib.parse.quote_plus(success_file_name)
                attachments_list.append(
                    {'filename': "Success Coupons " + str(len(success_coupon)), "url": success_coupon_url})

            if failed_file_name:
                failed_coupon_url = settings.S3_GENERATED_COUPONS_URL + \
                    urllib.parse.quote_plus(failed_file_name)
                attachments_list.append(
                    {'filename': "Failed Coupons " + str(len(failed_coupon)), "url": failed_coupon_url})

            try:
                if settings.GENERATED_COUPONS_RECEIVER_EMAIL:
                    receivers_email_list = settings.GENERATED_COUPONS_RECEIVER_EMAIL
                    receivers_email_list.append(str(user_email))
                else:
                    receivers_email_list = [str(user_email)]

                email_payload = {
                    "subject": "Newly Created Coupon",
                    "body_text": "Please find attached files for successful and failed coupons",
                    "body_html": "",
                    "sender": settings.TREEBO_SENDER_EMAIL,
                    "sender_name": settings.TREEBO_SENDER_NAME,
                    "receivers": receivers_email_list,
                    "consumer": "Coupons creation",
                    "attachments": attachments_list,

                }

                SendEmailService.send_email(email_payload)
            except Exception as exc:
                logger.exception("Sending email failed for coupon generation")
        except Exception as exc:
            logger.exception(exc)

    def generate_file_path(self, total_count, type):
        DISCOUNT_DIR = os.path.dirname(os.path.dirname(__file__))
        COUPONS_GEN_DIR = os.path.join(DISCOUNT_DIR, 'coupons_generated')
        file_name = (str(get_current_ist_time().replace(tzinfo=None)) +
                     type + "_" + str(total_count) + ".csv").replace(" ", "_")
        file_path_name = COUPONS_GEN_DIR + "/" + file_name
        return file_path_name, file_name

    def upload_file_to_s3(
            self,
            success_file_path_name,
            failed_file_path_name,
            success_file_name,
            failed_file_name):

        try:
            conn_s3 = S3Connection(host='s3-ap-southeast-1.amazonaws.com')

            bucket = conn_s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
            bucket_key = Key(bucket)
            if success_file_path_name:
                bucket_key.key = settings.S3_GENERATED_COUPONS_PATH + success_file_name
                bucket_key.set_contents_from_filename(
                    success_file_path_name, replace=True, policy='public-read')

            if failed_file_path_name:
                bucket_key.key = settings.S3_GENERATED_COUPONS_PATH + failed_file_name
                bucket_key.set_contents_from_filename(
                    failed_file_path_name, replace=True, policy='public-read')

            logger.info("Uploading coupon files to S3 succeed")
        except Exception as exc:
            logger.exception(exc)
            logger.error(
                "Uploading coupon files to S3 failed %s %s",
                success_file_name,
                failed_file_name)
