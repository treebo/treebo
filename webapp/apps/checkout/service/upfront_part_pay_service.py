import logging
import decimal
from apps.common.exceptions.custom_exception import InvalidPartpayPolicyInContentStore, \
    KeyNotFoundInContentStore, PartpayNotApplicable
from apps.content.services.content_store_service import ContentStoreService
from django.conf import settings
from apps.checkout.dto.partpay_policy_dto import PartpayPolicyDto, PartpayValueType
from apps.featuregate.manager import FeatureManager
from apps.featuregate.constants import FEATURE_PAGE_NAMES

logger = logging.getLogger(__name__)

MIN_PART_PAYMENT_AMOUNT = 1


class UpfrontPartPayStatus(object):
    SUCCESS = 'success'
    PENDING = 'pending'


class UpfrontPartPayConstraints(object):
    HOTEL = {'keyword': 'hotel', 'keyword_type': 'ids', 'operator': 'in'}


class UpfrontPartPayService(object):
    UPFRONT_PART_PAY_VARIABLE = 'upfront_partpay_enabled'

    def fetch_content_from_content_store(self):
        try:
            self.partpay_policy_object = ContentStoreService.get_content(
                key=settings.PARTIAL_PAYMENT_POLICY_CONTENT_KEY_NAME)
            self.partpay_policy = PartpayPolicyDto(**self.partpay_policy_object.value)
        except (InvalidPartpayPolicyInContentStore, KeyNotFoundInContentStore) as e:
            raise e
        except Exception as e:
            message = "Exception while reading the value from ContentStore for {key} due " \
                      "to:{reason}".format(key=settings.PARTIAL_PAYMENT_POLICY_CONTENT_KEY_NAME,
                                           reason=str(e))
            logger.exception(message)
            raise InvalidPartpayPolicyInContentStore(message)

    def update_hotel_constraint(self, hotel_list_to_enable, hotel_list_to_disable):
        self.fetch_content_from_content_store()
        feature_manager = FeatureManager(
            FEATURE_PAGE_NAMES.ITINERARY, None, None, None, None, None, None)
        page_variable_conditions, already_applied_constraints = feature_manager.get_existing_rule(
            UpfrontPartPayService.UPFRONT_PART_PAY_VARIABLE, UpfrontPartPayConstraints.HOTEL.get('keyword'),
            UpfrontPartPayConstraints.HOTEL.get('keyword_type'), UpfrontPartPayConstraints.HOTEL.get('operator'))
        existing_enabled_hotel_list = []
        for constraint in already_applied_constraints:
            existing_enabled_hotel_list += [int(val.strip()) for val in constraint.value_one.split(",")]
        updated_list_of_enabled_hotel = [id for id in existing_enabled_hotel_list if id not in hotel_list_to_disable]
        updated_list_of_enabled_hotel += [id for id in hotel_list_to_enable if id not in updated_list_of_enabled_hotel]
        hotel_list = ','.join(str(x) for x in updated_list_of_enabled_hotel)
        hotel_list = hotel_list.strip()
        new_constraint = self.fetch_from_existing_or_create_new_constraint(feature_manager,
                                                                                            already_applied_constraints,
                                                                                            hotel_list)
        for page_variable_condition in page_variable_conditions:
            page_variable_condition.expression.expression = str(new_constraint.id)
            page_variable_condition.expression.save()

    def fetch_from_existing_or_create_new_constraint(self, feature_manager, already_applied_constraints, new_value_one):
        new_constraint = [constraint for constraint in already_applied_constraints if constraint.value_one == new_value_one]
        if len(new_constraint) == 0:
            new_constraint = feature_manager.create_constraint(UpfrontPartPayConstraints.HOTEL.get('keyword'),
                                                               UpfrontPartPayConstraints.HOTEL.get('keyword_type'),
                                                               UpfrontPartPayConstraints.HOTEL.get('operator'),
                                                               new_value_one, None)
        else:
            new_constraint = new_constraint[0]
        return new_constraint

    def is_applicable(self, page_name, utm_source, platform, hotel_id):
        if self.is_applicable_for_part_payment(utm_source, platform):
            featureManager = FeatureManager(
                page_name, None, None, hotel_id, None, None, None, utm_source=utm_source)
            overRiddenValues = featureManager.get_overriding_values()
            if overRiddenValues.get(self.UPFRONT_PART_PAY_VARIABLE):
                return True
        return False

    def is_applicable_for_part_payment(self, utm_source, platform):
        self.fetch_content_from_content_store()
        if self.partpay_policy.applicable_for.platforms and self.partpay_policy.applicable_for.utm_sources:
            return utm_source in self.partpay_policy.applicable_for.utm_sources \
                   and platform in self.partpay_policy.applicable_for.platforms
        elif not self.partpay_policy.applicable_for.utm_sources and self.partpay_policy.applicable_for.platforms:
            return platform in self.partpay_policy.applicable_for.platforms
        elif not self.partpay_policy.applicable_for.platforms and self.partpay_policy.applicable_for.utm_sources:
            return utm_source in self.partpay_policy.applicable_for.utm_sources
        else:
            return True

    def get_partpay_amount(self, total_booking_payable_amount):
        self.fetch_content_from_content_store()
        amount = self.convert_policy_value_to_amount(self.partpay_policy.value,
                                                     self.partpay_policy.value_type,
                                                     total_booking_payable_amount,
                                                     self.partpay_policy.default_percentage_value)
        if amount < MIN_PART_PAYMENT_AMOUNT:
            raise PartpayNotApplicable("PartPay not applicable for "
                                 "amount:{amount} since the amount is too less and the part pay "
                                 "calculated is less than {min_part_pay_amount}".format(
                amount=total_booking_payable_amount, min_part_pay_amount=MIN_PART_PAYMENT_AMOUNT))
        return amount

    def validate_and_get_partpay_amount(self, total_booking_payable_amount, utm_source, platform, hotel_id, page_name):
        try:
            if self.is_applicable(page_name, utm_source, platform, hotel_id):
                return self.get_partpay_amount(total_booking_payable_amount)
            else:
                raise PartpayNotApplicable("PartPay not applicable for "
                                           "utm_source:{utm},platform:{platform}, "
                                           "amount:{amount}".format(utm=utm_source, platform=platform,
                                                                    amount=total_booking_payable_amount))
        except PartpayNotApplicable as e:
            raise PartpayNotApplicable("PartPay not applicable for "
                                           "utm_source:{utm},platform:{platform}, "
                                           "amount:{amount} due to:{exception}".format(
                utm=utm_source, platform=platform, amount=total_booking_payable_amount, exception=str(e)))

    def convert_policy_value_to_amount(
        self, value, value_type, booking_amount, default_percentage_value
    ):
        if value_type == PartpayValueType.PERCENTAGE and value < 100:
            return value * decimal.Decimal(booking_amount) / 100
        if self.is_partpay_amount_above_booking_value(value_type, value, booking_amount):
            return default_percentage_value * decimal.Decimal(booking_amount) / 100
        return value

    def is_partpay_amount_above_booking_value(self, value_type, value, booking_amount):
        if (value_type == PartpayValueType.PERCENTAGE and value >= 100) or (
            value_type == PartpayValueType.ABSOLUTE and value >= booking_amount):
            return True
        return False






