# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0005_auto_20170427_1513'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewappconfiguration',
            name='award_description',
            field=models.TextField(
                default=b'TripAdvisor Certificate of Excellence - Awarded to properties that consistently earn great reviews'),
        ),
    ]
