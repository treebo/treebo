import $ from 'jquery';
import BaseView from './treebolibs/baseview';
import parsley from 'parsleyjs';

let auth = require('./auth');
let ForgotView = Object.create(BaseView);
let auth2;

ForgotView.pageLoaded = () => {

	$("#forgotButton").on('click', (e)=>{
		let $form = $('#forgotForm');
		let email = $('#forgotEmailInput').val().trim();

		const forgotParsley = $form.parsley();
		forgotParsley.on('form:submit',() => {
			e.preventDefault();
			auth.forgotpassword(email,treeboError => {
				$('.forgot__error').html(treeboError.message).removeClass('hide');
			});
			return false;
		});
	})
};

let forgotConfig = {
	name: 'forgot Page',
	widgets: []
};
let forgotView = ForgotView.setup(forgotConfig);
