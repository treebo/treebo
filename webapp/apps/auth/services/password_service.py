import logging

from apps.auth.treebo_auth_client import TreeboAuthClient
from apps.profiles.service.user_registration import UserRegistration
from base.token_helper import TokenHelper
from dbcommon.models.profile import User

logger = logging.getLogger(__name__)


class PasswordService(object):
    auth_client = TreeboAuthClient()
    token_helper = TokenHelper()
    user_registration = UserRegistration()

    def reset_password(self, reset_password_dto):
        verification_code = reset_password_dto['verification_code']
        confirm = reset_password_dto['confirm']
        password = reset_password_dto['password']
        logger.debug(" reset password for code %s ", verification_code)
        token_info = self.auth_client.reset_password(
            verification_code, confirm, password)
        logger.info(
            " reset password for code %s sucess , token info %s ",
            verification_code,
            token_info)
        auth_id = token_info['auth_id']
        user = User.objects.get_user_by_auth_id(auth_id)
        if not user:
            user, user_details = self.user_registration.register_user_with_token(
                token_info['access_token'])
        self.token_helper.revoke_all_tokens_for_user(user)
        self.token_helper.save_access_token(token_info, user)
        return user, token_info['access_token']

    def change_password(self, request, change_password_dto):
        token = self.token_helper.get_token(request)
        user = request.user
        if not token:
            logger.debug(" token not preset in request fetching from user ")
            token = self.token_helper.get_active_token_from_user(user)
        logger.info("change password with token %s, user %s", token, user)
        token_info = self.auth_client.change_password(
            token,
            change_password_dto['old_password'],
            change_password_dto['new_password'])
        logger.info("The call to change password is successful")
        self.token_helper.revoke_all_tokens_for_user(user)
        self.token_helper.save_access_token(token_info, user)
        return user, token_info['access_token']

    def send_forgot_password_email(self, forgot_password_dto):
        logger.debug(
            " sending forgot email to %s ",
            forgot_password_dto['email'])
        self.auth_client.forgot_password(
            forgot_password_dto['email'], 'forgotpassword')

    def verify_email(self, email_hash):
        token_info = self.auth_client.email_verification(email_hash)
        auth_id = token_info['auth_id']
        user = User.objects.get_user_by_auth_id(auth_id)
        if not user:
            user, user_details = self.user_registration.register_user_with_token(
                token_info['access_token'])
        return user, token_info['access_token']
