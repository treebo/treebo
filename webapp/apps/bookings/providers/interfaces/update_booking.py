# -*- coding: utf-8 -*-
import abc
import json

from django.conf import settings

from apps.bookings import constants
from apps.bookings.models import Booking as BookingModel
from apps.bookings.data_classes import Booking
from apps.bookings.data_classes.status import CancelledStatus
from apps.bookings.providers.exceptions import BookingAlreadyCancelledException


class UpdateBookingInterface(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, old_booking, new_booking, **kwargs):
        self.new_booking = new_booking
        self.old_booking = old_booking
        self.kwargs = kwargs

    def update(self):
        # No updates are allowed on cancelled bookings
        if self.old_booking.status.name == CancelledStatus.name:
            raise BookingAlreadyCancelledException(self.old_booking.booking_id)

        updated_booking = self._update()
        self._persist_details(updated_booking)
        assert isinstance(updated_booking, Booking)
        return updated_booking

    @abc.abstractmethod
    def _update(self):
        pass

    def _persist_details(self, updated_booking):
        booking_model = BookingModel.objects.get(order_id=self.old_booking.booking_id)
        booking_model.external_crs_booking_version = updated_booking.crs_version
        booking_model.external_crs_booking_extra_info = json.dumps(
            {'crs_booking_bill_id': updated_booking.crs_bill_id})
        booking_model.booking_status = updated_booking.status.old_name
        booking_model.save()
