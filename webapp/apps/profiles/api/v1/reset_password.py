import logging
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from rest_framework.response import Response

from apps.auth.dto.reset_password_dto import ResetPasswordDTO
from apps.auth.services.password_service import PasswordService
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes

__author__ = 'varunachar'

logger = logging.getLogger(__name__)


class ResetPasswordValidator(serializers.Serializer):
    requiredErrorMessage = {
        'required': error_codes.PASSWORD_NOT_ENTERED,
        'blank': error_codes.PASSWORD_NOT_ENTERED,
    }
    password = serializers.CharField(
        required=True, error_messages=requiredErrorMessage)
    confirm = serializers.CharField(
        required=True, error_messages=requiredErrorMessage)
    slug = serializers.CharField(required=True)


class ResetPasswordAPI(TreeboAPIView):
    validationSerializer = ResetPasswordValidator
    reset_password_service = PasswordService()

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ResetPasswordAPI, self).dispatch(*args, **kwargs)

    def post(self, request, format=None):
        data = request.data
        slug = data.get('slug')
        password = data.get('password')
        confirm = data.get('confirm')
        logger.info("password reset with verification {}".format(slug))
        reset_password_dto = ResetPasswordDTO(data={
            'verification_code': slug,
            'password': password,
            'confirm': confirm

        })
        try:
            reset_password_dto.is_valid()
            self.reset_password_service.reset_password(
                reset_password_dto.validated_data)
            return Response({
                'status': 'success',
                'msg': 'Password reset successfully'
            })
        except Exception as e:
            logger.exception("password reset fail")
            return Response({
                'status': 'error',
                'msg': 'Password reset failed'
            })
