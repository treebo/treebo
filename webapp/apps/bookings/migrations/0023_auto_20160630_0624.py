# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0022_booking_is_audit'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='is_audit',
            field=models.NullBooleanField(default=False),
        ),
    ]
