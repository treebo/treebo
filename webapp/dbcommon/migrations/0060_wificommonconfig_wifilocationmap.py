# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0059_categoryseodetails_city'),
    ]

    operations = [
        migrations.CreateModel(
            name='WifiCommonConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('search_radius_in_meter', models.PositiveIntegerField(default=100)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'dbcommon_wificommon_config',
                'verbose_name': 'Wifi Common Config',
                'verbose_name_plural': 'Wifi Common Configs',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='WifiLocationMap',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('location_key', models.CharField(max_length=100)),
                ('wifi_name', models.CharField(default='Treebo Hotels', max_length=30)),
                ('is_enabled', models.BooleanField(default=False)),
                ('hotel', models.OneToOneField(to='dbcommon.Hotel', on_delete=models.DO_NOTHING)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'dbcommon_wifilocation_map',
                'verbose_name': 'Wifi Location Map',
                'verbose_name_plural': 'Wifi Location Maps',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
    ]
