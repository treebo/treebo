# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0022_auto_20160608_1040'),
        ('pricing', '0009_auto_20160523_0950'),
    ]

    operations = [
        migrations.CreateModel(
            name='FeatureToggle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('namespace', models.CharField(max_length=100)),
                ('feature', models.CharField(max_length=200)),
                ('is_enabled', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'feature_toggle',
                'verbose_name': 'Feature Toggle',
                'verbose_name_plural': 'Feature Toggles',
            },
        ),
        migrations.CreateModel(
            name='RoomPriceV2',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('date', models.DateField(db_index=True)),
                ('guest_count', models.IntegerField(default=1)),
                ('base_price', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('extra_adult_rate', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('extra_kid_rate', models.DecimalField(default=0, max_digits=20, decimal_places=10)),
                ('room', models.ForeignKey(to='dbcommon.Room', on_delete=models.DO_NOTHING)),
            ],
            options={
                'db_table': 'pricing_roomprice_v2',
                'verbose_name': 'Room Price V2',
                'verbose_name_plural': 'Room Prices V2',
            },
        ),
    ]
