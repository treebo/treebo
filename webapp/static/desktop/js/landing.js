'use strict';

import $ from 'jquery';
import {Observable} from "rx";
import "slick-carousel";
import queryString from 'query-string';
import BaseView from './treebolibs/baseview';
import searchWidget from './widgets/search';
import auth from './auth';
import '../css/landing.less';


let LandingView = Object.create(BaseView);

LandingView.pageLoaded = () => {
    const parsed = queryString.parse(location.search);

    //Wait for google auth
    auth.getGoogleHandler().then(() => {
        setTimeout(() => {
            //Open login popup
            if(parsed.next) {
                $('.js-headerlogin').trigger('click');
            }
        }, 0);
    });

    $(".js-banner-carousel").slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false
        // lazyLoad : 'progressive'
    });

    $(".js-allcity").slick({
    	infinite: true,
        arrows: true,
        slidesToShow:5,
        slidesToScroll: 5,
        lazyLoad : 'progressive'
    })

    console.log(" Landing page loaded");
    $(".home-search-box, #calendar").on('click', () => {

        const scrollPosition = $('body').scrollTop();
        const elementOffset = $(".home-search-box").offset().top;
        const elementDistance = (elementOffset - scrollPosition);
        const windowHeight = $(window).height();

        if(windowHeight - elementDistance < 400){
            let sp = elementOffset+400-windowHeight;
            $('html, body').animate({scrollTop: sp}, 500);
        }

    });

    $("#bookATreebo").on('click', () => {

        const scrollPosition = $('body').scrollTop();
        const elementOffset = $(".home-search-box").offset().top;
        const elementDistance = (elementOffset - scrollPosition);
        const windowHeight = $(window).height();

        let sp = elementOffset+400-windowHeight;
        $('html, body').animate({scrollTop: sp}, 500);

    })


}

let analyticsConfigfunc = function(){
    return  {
        hrental_pagetype: 'home',
        hrental_startdate: rt.getCheckin(),
        hrental_enddate: rt.getCheckout(),
    }
}

let landingConfig = { name:'Home Page', widgets:[searchWidget], analyticsConfigfunc };
let landingView = LandingView.setup(landingConfig);
