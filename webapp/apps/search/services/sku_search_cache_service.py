import logging

from django.core.cache import cache
from web_sku.availability.services.availability_service import AvailabilityService

from apps.search.constants import SEARCH_BASE_ROOM_PRICE
from base import log_args
from common.services.sku_service import SKUService
from dbcommon.services.web_cs_transformer import WebCsTransformer

logger = logging.getLogger(__name__)


class SkuSearchCacheService:
    @log_args(logger)
    def __init__(self):
        self.availibility_service = AvailabilityService()
        self.web_cs_transformer = WebCsTransformer()

    def get_min_hotels_price_from_cache(self, hotel_ids, room_wise_availability_per_hotel, cs_id_map):
        hotel_price_list = {}
        available_hotel_ids = []
        for hotel_id in hotel_ids:
            hotel_price_list[hotel_id] = {
                "available_room_types": [],
                "net_payable_amount": 0
            }
            cs_id = cs_id_map.get(hotel_id)
            if self.get_rooms_avalability_per_hotel(room_wise_availability_per_hotel.get(cs_id)):
                available_hotel_ids.append(hotel_id)
                availability_dict = room_wise_availability_per_hotel.get(cs_id, None)
                hotel_price_list[hotel_id]["available_room_types"] = list(availability_dict) if availability_dict else []

        cache_min_price_keys = []
        for hotel_id in hotel_ids:
            if hotel_id in available_hotel_ids:
                cache_min_price_keys.append(self.get_cache_key(hotel_id))
        min_sell_price_map = cache.get_many(cache_min_price_keys)

        for hotel_id in hotel_ids:
            if hotel_id in available_hotel_ids:
                min_sell_price = min_sell_price_map.get(self.get_cache_key(hotel_id), None)
                hotel_price_list[hotel_id]["net_payable_amount"] = SEARCH_BASE_ROOM_PRICE
                if min_sell_price:
                    hotel_price_list[hotel_id]["net_payable_amount"] = float(min_sell_price)

        return hotel_price_list

    def get_availability_data(self, cs_ids, checkin_date, checkout_date, room_config, channel='direct',
                              subchannel='website-direct'):
        room_wise_availability_per_hotel = {}
        logger.info("get_availability_data request params %s", cs_ids)
        if not cs_ids:
            logger.exception("Couldnot find cs_ids for hotels %s %s %s", checkin_date, checkout_date, room_config)
            return room_wise_availability_per_hotel
        available_sku_availibility = {}
        try:
            skus = SKUService.get_sku_for_hotels(
                hotel_cs_id_list=cs_ids,
                room_config=room_config,
                room_types=None)
            if skus:
                sku_availability = SKUService.get_sku_availability_for_hotels(
                    skus=skus,
                    hotel_cs_id_list=cs_ids,
                    room_config=room_config,
                    checkin_date=(checkin_date).isoformat(),
                    checkout_date=(checkout_date).isoformat(),
                    channel=channel,
                    sub_channel=subchannel)
                for cs_id in sku_availability:
                    if sku_availability.get(cs_id):
                        available_sku_availibility.update({cs_id: sku_availability.get(cs_id)})
                if available_sku_availibility:
                    room_wise_availability_per_hotel = SKUService.get_room_wise_availability_from_sku_availability(
                        skus=skus,
                        sku_availability=available_sku_availibility)
        except Exception as err:
            logger.exception("Availibilty call from sku cache failed due to %s for %s", str(err), cs_ids)
        return room_wise_availability_per_hotel

    def get_rooms_avalability_per_hotel(self, hotel_avalability_dict):
        try:
            if hotel_avalability_dict:
                for sku_room_type in hotel_avalability_dict:
                    rooms_available = hotel_avalability_dict.get(sku_room_type)
                    if rooms_available > 0:
                        return True
            return False
        except Exception as err:
            logger.exception("Couldn't get room Availability %s", str(err))
            return False

    def get_cache_key(self, hotel_id):
        return str(hotel_id) + '_SKU_MIN_PRICE'