from django.conf import settings
from apps.checkout.dto.payment_service_dto import PaymentServiceV2InitiateDTO, \
    PaymentServiceV2ConfirmDTO
from apps.common.exceptions.custom_exception import PaymentVerifyException
from apps.payments.payment_order_helper import PaymentOrderHelper
from apps.bookings.models import PaymentOrder, Booking
from apps.checkout.constants import DebitModes
from apps.payments.constants import PAYMENT_OFFER_PAYMENT_GATEWAY
from apps.payments.offers.services.payment_offer import PaymentOfferService
from services.restclient.treebo_notificationservice import TreeboNotificationService
import logging
from services.restclient.payment_client_v2 import PaymentClientV2
from data_services.respositories_factory import RepositoriesFactory
from webapp.apps.bookings.services.booking_utils import get_room_codes
from web_sku.catalog.services.sku_request_service import SkuRequestService
from web_sku.availability.services.availability_service import AvailabilityService
from apps.checkout.models import BookingRequest

logger = logging.getLogger(__name__)

MIN_PAYMENT_AMOUNT = 1


class PaymentException(Exception):
    pass


class PaymentServiceV2():
    payment_client = PaymentClientV2()
    payment_order_helper = PaymentOrderHelper()
    treebo_notification_service = TreeboNotificationService()
    hotel_repository = RepositoriesFactory.get_hotel_repository()
    sku_request_service = SkuRequestService()
    availability_service = AvailabilityService()

    def initiate_order_from_paymentservice(self, amount, gateway, receipt, redirection_url,
                                           booking_order_id='', utm_source=None):
        logger.info(
            "initiate_order_from_paymentservice  for amount %s gateway %s reciept %s orderid %s and redirection_url %s utm_source %s",
            amount, gateway, receipt, booking_order_id, redirection_url, utm_source)
        if amount < MIN_PAYMENT_AMOUNT:
            logger.info("for receipt %s amount is low ", receipt, amount)
            return None, None

        receipt = booking_order_id if booking_order_id else receipt

        ext_payment_offer_id = PaymentOfferService(
        ).get_gateway_payment_coupon_codes_for_utm_source(utm_source)
        payment_details = self.payment_client.initiate(
            amount, receipt, gateway, redirection_url,booking_order_id, utm_source=utm_source,
            payment_offer_coupon_codes=ext_payment_offer_id)
        assert type(payment_details) is PaymentServiceV2InitiateDTO
        logger.info('initiate_payment order response %s', payment_details)
        logger.info(" calling create payment order for %s ", receipt)
        payment_order = self.payment_order_helper.create_payment_order_from_paymentdetails(amount,
                                                                                           gateway,
                                                                                           payment_details,
                                                                                           PaymentOrder.ISSUED,
                                                                                           booking_order_id)
        logger.info(
            "initiate_order_from_paymentservice is completed for (receipt %s and order %s) ",
            receipt, booking_order_id)
        return payment_order, payment_details

    def generate_ui_redirect_urls_to_be_used_upon_payment_confirmation(self, order_id, bid):
        try:
            redirect_url_on_gatewaypayment_completion = settings.TREEBO_WEB_URL + settings.PAYMENT_PROCESS_URL.format(
                order_id)
        except Exception as e:
            logger.exception('Exception while generating the redirect urls for bid: %s', bid)
            raise
        return redirect_url_on_gatewaypayment_completion

    def verify_order_from_paymentservice_and_update_paymentorder(self, ps_order_id, pg_meta,
                                                                 booking_order_id):
        # checks the payment status and updates the status in payment order table
        logger.info('ps_order_id %s pg_meta %s booking_order_id %s', ps_order_id, pg_meta,
                    booking_order_id)
        payment_order = self.payment_order_helper.get_payment_order(ps_order_id)
        already_verified = False
        confirm_booking = True
        logger.info("verify_order_from_paymentservice for ps_orderid %s and payment order %s",
                    ps_order_id, payment_order)

        if payment_order.booking_order_id is None or payment_order.booking_order_id != booking_order_id:
            logger.exception('booking_order_id is invalid in payment_order: {0}'.format(
                payment_order.booking_order_id))
            raise PaymentException(
                'Booking order id is invalid: %s' % payment_order.booking_order_id)

        if payment_order.is_verified():
            already_verified = True
            logger.info(
                "payment order _is_already_verified %s %s %s ",
                ps_order_id,
                booking_order_id,
                pg_meta)
            return already_verified, confirm_booking

        payment_id = payment_order.ps_payment_id
        pg_customer_id = ''
        logger.info("calling payment confirm ps_order_id %s booking_order_id %s", ps_order_id,
                    booking_order_id)

        # already_verified_in_payment_service flag is true in case of phonepe and amazonpay
        # since the call back is coming via payment microservice rather than web backend
        # and the field (already_verified) is not present in case of razor pay
        already_verified_in_payment_service = ('already_verified' in pg_meta) and pg_meta[
            'already_verified'] == 'True'
        if not already_verified_in_payment_service:
            payment_details = self.payment_client.confirm(ps_order_id, payment_id, pg_customer_id,
                                                          pg_meta, card=None)
            assert type(payment_details) is PaymentServiceV2ConfirmDTO
            logger.info(
                "payment confirm success ps_order_id %s booking_order_id %s  payment_details %s ",
                ps_order_id,
                booking_order_id, payment_details)
            self.payment_order_helper.update_status_in_paymentorder(payment_order,
                                                                    PaymentOrder.CAPTURED)
        elif ('status' in pg_meta) and (pg_meta['status']).lower() in (
            PaymentOrder.PASSED.lower(), PaymentOrder.CAPTURED.lower()):
            logger.info(
                "payment confirm success ps_order_id %s booking_order_id %s  payment_details %s ",
                ps_order_id,
                booking_order_id, pg_meta)
            self.payment_order_helper.update_status_in_paymentorder(payment_order,
                                                                    PaymentOrder.CAPTURED)
        else:
            logger.info(
                "payment confirm failed ps_order_id %s booking_order_id %s  payment_details %s ",
                ps_order_id,
                booking_order_id, pg_meta)
            # the payment has failed at the gateway end so the booking should not be confirmed,
            # hence confirm_booking is false
            confirm_booking = False
            self.payment_order_helper.update_status_in_paymentorder(payment_order,
                                                                    PaymentOrder.FAILED)
        return already_verified, confirm_booking

    def create_payment_for_payment_offer(self, amount, booking_order_id, ps_payment_id):
        if amount <= 0:
            return
        self.payment_order_helper.create_payment_order(
            ps_order_id=None,
            pg_payment_id=None,
            pg_order_id=None,
            ps_payment_id=ps_payment_id,
            status=PaymentOrder.CAPTURED,
            gateway_type=PAYMENT_OFFER_PAYMENT_GATEWAY,
            amount=amount,
            booking_order_id=booking_order_id)

    def one_step_pay(self, current_user, amount, booking_order_id, debit_mode,
                     particulars=None, auth_id=None):
        logger.info('Entered one step pay for amount %s booking_order_id %s', amount,
                    booking_order_id)
        wallet_payment_success = False
        if not current_user.is_authenticated():
            logger.info('Current User not authenticated for booking_order_id %s', booking_order_id)
            booking = Booking.objects.filter(order_id=booking_order_id).prefetch_related(
                'user_id').first()
            auth_id = booking.user_id.auth_id
            logger.info('auth id fetched is: %s for booking_order_id %s', auth_id, booking_order_id)
            if not auth_id:
                logger.info('Auth id is null booking_order_id %s', booking_order_id)
                return wallet_payment_success, None, 'user not logged in'

        if amount < 1:
            logger.info('amount is negative i.e %s for booking_order_id %s', amount,
                        booking_order_id)
            return wallet_payment_success, None, 'wallet amount is low'
        pg_meta = {}
        if current_user and hasattr(current_user, 'auth_id'):
            pg_meta['user_id'] = current_user.auth_id
        else:
            pg_meta['user_id'] = auth_id
        pg_meta['wallet_type'] = settings.WALLET_TYPE
        pg_meta['event_type'] = settings.WALLET_BURN_EVENT_TYPE
        pg_meta['cf_one'] = booking_order_id
        pg_meta['cf_two'] = None
        if particulars:
            pg_meta['particulars'] = particulars
        pg_meta['event_type'] = 'room_booking'
        receipt = booking_order_id
        gateway = settings.TREEBO_WALLET_GATEWAY
        payment_order = None
        try:
            logger.info('one step pay for amount %s booking_order_id %s', amount, booking_order_id)
            payment_details = self.payment_client.pay(amount, receipt, gateway, pg_meta=pg_meta)
            if debit_mode == DebitModes.POST_TAX:
                payment_order = self.payment_order_helper.create_payment_order_from_paymentdetails(
                    amount, gateway, payment_details,
                    PaymentOrder.CAPTURED,
                    booking_order_id)
            reason = 'success'
        except PaymentVerifyException as e:
            logger.error('One step payment failed for booking_order_id %s', booking_order_id)
            payment_details = {}
            payment_order = self.payment_order_helper.create_payment_order_from_paymentdetails(
                amount, gateway, payment_details, PaymentOrder.FAILED, booking_order_id)
            reason = 'insuficient funds'
        return True, payment_order, reason

    def _is_already_verified(self, payment_order, booking_order_id):
        if payment_order.booking_order_id != booking_order_id:
            return False
        if payment_order.status == PaymentOrder.CAPTURED:
            return True
        return False

    def send_payment_failure_notification_to_gdc(self, payment_order, booking):
        subject = 'payment failure for confirmed booking - {}'.format(
            payment_order.booking_order_id)
        message = 'Please note, \n ' \
                  'Booking Id - {} \n' \
                  'payment status - "Failed" ' \
                  'Please call the guest on their phone number - {} and check the following\n' \
                  '1. Did they make this booking by mistake? If they made it by mistake, please ask ' \
                  'reservation team to cancel the booking immediately.\n \n' \
                  '2. If they intend to keep the booking, we need to tell them that their payment failed on payment gateway''s end but their booking is confirmed and immediately share the payment link with the traveler; however, if the guest insists to PAH, please don''t push for partial payments and let them know their booking is confirmed.\n'.format(
            payment_order.booking_order_id, booking.guest_mobile)

        self.treebo_notification_service.send_email(emails=settings.GDC_TEAM_EMAIL, content=message,
                                                    subject=subject)

    def initiate_refund(self, order_id):
        if not order_id:
            return
        all_payment_orders = PaymentOrder.objects.filter(booking_order_id=order_id)

        for payment_order in all_payment_orders:
            self.payment_client.refund(payment_order_id=payment_order.ps_order_id,
                                       gateway=payment_order.gateway_type,
                                       amount=payment_order.amount,
                                       notes="Refund for Homestays booking failure")

        return

    def is_available(self, booking_request):
        cs_web_id_map = {}
        hotel_id = booking_request.hotel_id
        room_type = booking_request.room_type
        cs_hotel_id = ''
        for hotel in self.hotel_repository.get_hotels_for_id_list([hotel_id]):
            if hotel.cs_id is not None:
                cs_web_id_map[hotel.cs_id.strip()] = hotel.id
                cs_hotel_id = hotel.cs_id.strip()

        if not cs_hotel_id:
            return False

        room_codes = get_room_codes([room_type])
        request_skus = self.sku_request_service.get_web_request_skus(
            booking_request.room_config, [cs_hotel_id], room_codes)

        availability_request_data = {
            'skus': request_skus,
            'checkin': booking_request.checkin_date,
            'checkout': booking_request.checkout_date,
            'hotels': [cs_hotel_id],
            'occupancy_config': booking_request.room_config,
            'channel': booking_request.booking_channel,
            'subchannel': booking_request.booking_subchannel,
            'cs_web_id_map': cs_web_id_map
        }
        sku_availability = self.availability_service.get_sku_availability(availability_request_data)
        room_wise_availability = self.availability_service. \
            get_room_wise_availability_from_sku_availability(availability_request_data,
                                                             sku_availability)
        if room_wise_availability.get(str(cs_hotel_id), {}).get(room_type.upper(), 0) > 0:
            return True
        else:
            return False
