import os
from data_services.city_repository import CityRepository
from data_services.web_repositories.city_web_repository import CityWebRepository
from data_services.web_repositories.directions_web_repository import DirectionsWebRepository
from data_services.web_repositories.highlights_web_repository import HighlightsWebRepository
from data_services.web_repositories.hotel_web_repository import HotelWebRepository
from data_services.web_repositories.image_web_repository import ImageWebRepository
from data_services.web_repositories.landmark_web_repository import LandmarkWebRepository
from data_services.web_repositories.locality_web_repository import LocalityWebRepository
from data_services.web_repositories.room_web_repository import RoomWebRepository
from data_services.web_repositories.state_web_repository import StateWebRepository
from data_services.directions_repository import DirectionsRepository
from data_services.facility_repository import FacilityRepository
from data_services.highlights_repository import HighlightsRepository
from data_services.hotel_repository import HotelRepository
from data_services.image_repository import ImageRepository
from data_services.search_landmark_repository import SearchLandMarkRepository
from data_services.locality_repository import LocalityRepository
from data_services.room_repository import RoomRepository
from data_services.soa_clients.property_client import CatalogClient, CatalogAPIResponsesProcessor
from data_services.state_repository import StateRepository

class RepositoriesFactory:
    cs_responses_processor = CatalogAPIResponsesProcessor()
    catalog_client = CatalogClient(cs_response_processor=cs_responses_processor)
    is_cs_querying_enabled = os.getenv('IS_CS_QUERYING_ENABLED', True)

    @classmethod
    def get_hotel_repository(cls):
        hotel_web_repository = HotelWebRepository()
        hotel_repository = HotelRepository(hotel_web_repository=hotel_web_repository, cs_api_client=cls.catalog_client,
                                           is_cs_api_querying_enabled=cls.is_cs_querying_enabled)
        return hotel_repository

    @classmethod
    def get_room_repository(cls):
        room_web_repository = RoomWebRepository()
        room_repository = RoomRepository(room_web_repository=room_web_repository, cs_api_client=cls.catalog_client,
                                         is_cs_api_querying_enabled=cls.is_cs_querying_enabled)
        return room_repository

    @classmethod
    def get_directions_repository(cls):
        directions_web_repository = DirectionsWebRepository()
        directions_repository = DirectionsRepository(directions_web_repository)
        return directions_repository

    @classmethod
    def get_highlights_repository(cls):
        highlights_web_repository = HighlightsWebRepository()
        highlights_repository = HighlightsRepository(highlights_web_repository)
        return highlights_repository

    @classmethod
    def get_image_repository(cls):
        image_web_repository = ImageWebRepository()
        image_repository = ImageRepository(image_web_repository)
        return image_repository

    @classmethod
    def get_landmark_repository(cls):
        landmark_web_repository = LandmarkWebRepository()
        search_landmark_repository = SearchLandMarkRepository(landmark_web_repository)
        return search_landmark_repository

    @classmethod
    def get_locality_repository(cls):
        locality_web_repository = LocalityWebRepository()
        locality_repository = LocalityRepository(locality_web_repository)
        return locality_repository

    @classmethod
    def get_state_repository(cls):
        state_web_repository = StateWebRepository()
        state_repository = StateRepository(state_web_repository)
        return state_repository

    @classmethod
    def get_city_repository(cls):
        city_web_repository = CityWebRepository()
        city_repository = CityRepository(city_web_repository)
        return city_repository

    @classmethod
    def get_facility_repository(cls):
        facility_repository = FacilityRepository(facility_web_repository=None, cs_api_client=cls.catalog_client)
        return facility_repository
