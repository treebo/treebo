import logging

from kombu import Exchange, Queue
from kombu.common import Broadcast
from webapp.conf import log_conf
from webapp.conf.base import *


TREEBO_BASE_HOST_URL = 'preprod.treebo.com'
TREEBO_WEB_URL = "https://" + TREEBO_BASE_HOST_URL
TREEBO_BASE_URL = '//' + TREEBO_BASE_HOST_URL

INSTALLED_APPS += [
    "django.contrib.admindocs",  # "debug_toolbar", "template_profiler_panel"
]

MIDDLEWARE_CLASSES += ['django.contrib.sessions.middleware.SessionMiddleware']
INSTALLED_APPS += ['django.contrib.sessions']
DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    # Do a pip install django-debug-toolbar-template-profiler
    # 'template_profiler_panel.panels.template.TemplateProfilerPanel'
]

ADMINS = [('Sunil Rana', 'sunil.rana@treebohotels.com')]
APP_VIRALITY_PRIVATE_KEY = 'd97a79edacd64ef8855ba61e00a03a68'
APP_VIRALITY_API_KEY = 'a63403cb67874c838480a61e00a0aba0'
PROWL_HOTEL_UPLOAD = 'https://dummy/url/to/hotelupload/'
PROWL_HOTEL_DISABLE = 'http://dummy/url/to/hotelupload/'
ENVIRONMENT = 'dev'
DOMAIN = 'dev'
FEATURE_TOGGLE_ENV = 'local'

EMAIL_USE_TLS = True
EMAIL_HOST = 'email-smtp.eu-west-1.amazonaws.com'
EMAIL_HOST_USER = 'AKIAYZ7GE5SNPHU6ZJWY'
EMAIL_HOST_PASSWORD = 'BNLkNWuJPOSn2WIDtBdiNhl1jLb2LB6kNU7iqkv++7CZ'
EMAIL_PORT = 587
SERVER_EMAIL = 'Treebo Developers <dev@treebohotels.com>'

NOTIFY_HMS_EMAIL = False

DEBUG = False
DEVELOPMENT = True

CONSUMER_KEY = "D2140AEC22BFF5037C56DE27BECE87E113782ABB"
CONSUMER_SECRET = "7752C96724C2BC6437CE8885363E0E4B7A939442"

HOTELOGIX_URL = "http://crs.hotelogix.net/ws/web/"
HOTELOGIX_ADMIN_URL = "http://admin.hotelogix.net/ws/web/"

BOOKING_DETAILS_URL = "http://hotelogix.stayezee.com/ws/webv2/"

# CELERY_DEFAULT_QUEUE = '{{ celery_queue }}'
# CELERY_BROADCAST_BOOKING_QUEUE = 'prod_broadcast_booking'
CELERY_HMS_TASK_NAME = 'amith_hms_sync_booking'
# celery configuration
BROKER_URL = 'amqp://guest:guest@172.40.10.104:5672/'
PRICING_BROKER_URL = 'amqp://guest:guest@172.40.10.104:5672/'

# celery broadcast queue name
CELERY_BROADCAST_BOOKING_QUEUE = 'amith_broadcast_booking'
CELERY_BROADCAST_BOOKING_QUEUE_TASK = 'amith_hms_sync_booking'

# define exachange for celery

CELERY_DEFAULT_QUEUE = 'celery'
EXCHANGE = Exchange(CELERY_BROADCAST_BOOKING_QUEUE, type='fanout')

CELERY_CREATE_COUPON_TASK_NAME = "create_coupon_task"
CELERY_CREATE_COUPON_QUEUE = "celery_create_coupon"

CELERY_QUEUES = (
    Queue(
        CELERY_DEFAULT_QUEUE,
        Exchange(CELERY_DEFAULT_QUEUE),
        routing_key=CELERY_DEFAULT_QUEUE),
    Queue(
        CELERY_CREATE_COUPON_QUEUE,
        Exchange(CELERY_CREATE_COUPON_QUEUE),
        routing_key=CELERY_CREATE_COUPON_QUEUE),
    Broadcast(
        CELERY_BROADCAST_BOOKING_QUEUE,
        routing_key='booking.all',
        exchange=EXCHANGE),
    Queue(
        CELERY_THIRD_PARTY_QUEUE,
        THIRD_PARTY_TRIGGER_EXCHANGE,
        routing_key=CELERY_THIRD_PARTY_QUEUE),
    Queue(
        CELERY_TRIVAGO_HOTEL_DETAILS,
        Exchange('trivago'),
        routing_key=CELERY_TRIVAGO_HOTEL_DETAILS),
    Queue(
        CELERY_CREATE_COUPON_QUEUE,
        Exchange(CELERY_CREATE_COUPON_QUEUE),
        routing_key=CELERY_CREATE_COUPON_QUEUE),
)

DATABASES_PREPROD = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'treebo',
        'USER': 'treeboadmin',
        'PASSWORD': 'caTwimEx3',
        'HOST': 'treebo-pg-master-sing-01.cadmhukcksta.ap-southeast-1.rds.amazonaws.com',
        'PORT': '5432',
    }
}

# DATABASES_VARUN = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'varunachar',
#         'USER': 'varunachar',
#         'PASSWORD': 'varunachar',
#         'HOST': '172.40.20.210',
#         'PORT': '5432',
#     }
# }

DATABASES_VARUN = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'treebo',
        'USER': 'postgres',
        'PASSWORD': 'treebo1',
        'HOST': 'localhost',
        'PORT': '5433',

    }
}
DATABASES = DATABASES_VARUN

BASE_PRICING_HOST = 'http://172.40.20.27:8080'
BASE_PRICING_URL_V3 = BASE_PRICING_HOST + "/v3/price?"
BASE_PRICING_URL_V2 = BASE_PRICING_HOST + "/v2/price?"
BASE_PRICING_URL_V1 = BASE_PRICING_HOST + "/v1/price?"


PRICING_URL = "https://preprod.treebo.com/api/v2/hotels/"
TREEBO_BASE_REST_HOST = TREEBO_WEB_URL

STATIC_URL = '//static.treebohotels.com/dist/'

S3_URL = 'https://images.treebohotels.com/'

# PAYU TEST KEYS
PAYU_MERCHANT_ID = 'gtKFFx'
PAYU_SALT = 'eCwWELxi'
PAYU_HANDLER_URL = '//varunachar.treebohotel.in/checkoutaction/'
PAYU_ACTION_URL = 'https://test.payu.in/_payment'
TREEBO_BASE_REST_HOST = 'http://sunil.rana.treebohotel.in'

RAZORPAY_KEYS = "rzp_test_upYNslfmrDV2tg"
RAZORPAY_SECRET = 'CAzoPh58gaTYCED3RHCJe4Pm'
LOG_REQUESTS = True

# Exclude list from tracking server responses - middleware : response.py
SERVER_STATUS_CAPTURE_FILTER = [200, 301, 302]

# Facebook Test App
FACEBOOK_APP_ID = '1064417653612165'
FACEBOOK_APP_SECRET = '8b041c5bc196772fc0a9f1fb5e1b3c20'

CONTACTUS_AVAILABILITY_SUBJECT = 'Hotel room availability query'
CONTACTUS_CORPORATE_SUBJECT = 'Corporate travel agent query'
CONTACTUS_BOOKING_SUBJECT = 'Booking %(category)s query'
CONTACTUS_REFUND_SUBJECT = 'Refund related query'
CONTACTUS_CHECKINOUT_SUBJECT = 'Checkin checkout query'
CONTACTUS_OTHER_SUBJECT = 'Other query'

CONTACTUS_AVAILABILITY_EMAIL_LIST = [
    'amith.soman@treebohotels.com',
    'sunil.rana@treebohotels.com',
    'devang.paliwal@treebohotels.com']
CONTACTUS_CORPORATE_EMAIL_LIST = [
    'amith.soman@treebohotels.com',
    'sunil.rana@treebohotels.com',
    'devang.paliwal@treebohotels.com']
CONTACTUS_BOOKING_EMAIL_LIST = [
    'amith.soman@treebohotels.com',
    'sunil.rana@treebohotels.com',
    'devang.paliwal@treebohotels.com']
CONTACTUS_REFUND_EMAIL_LIST = [
    'amith.soman@treebohotels.com',
    'sunil.rana@treebohotels.com',
    'devang.paliwal@treebohotels.com']
CONTACTUS_CHECKINOUT_EMAIL_LIST = [
    'amith.soman@treebohotels.com',
    'sunil.rana@treebohotels.com',
    'devang.paliwal@treebohotels.com']
CONTACTUS_OTHER_EMAIL_LIST = [
    'amith.soman@treebohotels.com',
    'sunil.rana@treebohotels.com',
    'devang.paliwal@treebohotels.com']

JOINUS_SUBJECT = 'Join us enquiry'
JOINUS_EMAIL_LIST = [
    'amith.soman@treebohotels.com',
    'sunil.rana@treebohotels.com',
    'devang.paliwal@treebohotels.com']
AVAILABILITY_FROM_BASIC_SEARCH = False
AVAILABILITY_DAY_LIMIT = 30

GUEST_PREF_SUBJECT = 'Special Request Guest: %(guestname)s, Booking Id: %(bookingid)s, Hotel %(hotelname)s'
GUEST_PREF_EMAIL_LIST = ['amith.soman@treebohotels.com']
# CANCELLATION_FAILURE_EMAIL_LIST = ['rohit.jain@treebohotels.com']
# RESERVATION_FAILURE_EMAIL_LIST = ['varun.achar@treebohotels.com']
CANCELLATION_FAILURE_EMAIL_LIST = ['']
RESERVATION_FAILURE_EMAIL_LIST = ['ankur.jain@treebohotels.com']
HMS_GET_ALL_RESERVATIONS_FOR_TODAY = 'http://api.treebohotels.com/hmssync/rest/v1/todaysreservations'

AVAILABILITY_HOOK_API = {
    'b2b_axis_room_hook_api': "http://corporates.preprod.treebo.com/ext/api/daywiseInventory/"
}

"""
Get LOG_ROOT from env variable. In which case, we can have log config only in base.py file.
"""
LOG_ROOT = os.environ.get('LOG_ROOT', '/var/log/website')


LOGGING = log_conf.get('/var/log/website')

# Make celery to use the Django root logger config, instead of using its own
CELERYD_HIJACK_ROOT_LOGGER = False

from celery.signals import setup_logging


@setup_logging.connect
def configure_celery_log(sender=None, **kwargs):
    import logging.config
    logging.config.dictConfig(LOGGING)


HOTEL_UPLOAD_FILE_LOCATION = LOG_ROOT

IMAGE_UPLOAD_PATH = '/home/{{ username }}/logs/image_upload/'
POS_URL = 'http://sunil.rana.treebohotel.in/api/v1/external/hotelads/updateposfile.xml'
TOOLS_POS_URL = 'http://sunil.rana.treebohotel.in/api/v1/external/hotelads/updateposfile.xml'
# Third party configs
TRIVAGO_CSV_LINK = '/home/ankur.jain/trivago.csv'
TRIVAGO_BID_FILE_PATH = '/home/ankur.jain/'
# saveemail API failure configs
SAVE_EMAIL_API_FAILURE_EMAIL_ID = ['snehil.rastogi@treebohotels.com']
# GHA Updated Files
POS_EMAIL = ["snehil.rastogi@treebohotels.com"]
FEATURE_POS_MAIL = True
TOOLS_TRANSACTION_URL = "http://sunil.rana.treebohotel.in/api/v1/external/hotelads/transactionmessage.xml"
# trip advisor
TA_CSV_EMAIL_ID = ["snehil.rastogi@treebohotels.com"]
# criteo
CRITEO_CSV_EMAIL_ID = [
    "santosh.kumar@treebohotels.com",
    "ansil.kareem@treebohotels.com"]

# partial payent growth api
GROWTH_PARTPAY_URL = 'http://staging.treebohotels.com/growth/partpay/web/link/'

GHA_CSV_EMAIL_ID = ['snehil.rastogi@treebohotels.com']
GHA_REPORT_INTERVAL_DAYS = 14

# growth url
GROWTH_ADD_HOTEL_URL = "http://staging.treebohotels.com/growth/common_service/update_hotel_info/"
# GHA
FETCH_BOOKING_STATUS = "http://staging.treebohotels.com/growth/transformer/booking/status/?wrid="
GROWTH_REFERRAL_COMM_API_URL = "http://growth.treebohotel.in/growth/referral/communication"

# cancel hash urls
GROWTH_FETCH_BOOKING_DATA_URL = "http://growth.treebohotel.in/growth/cancellation/cancelbookingdetails/"
GROWTH_SUBMIT_BOOKING_URL = "http://growth.treebohotel.in/growth/cancellation/submitcancellationrequest/"
GROWTH_CANCEL_HASH_URL = "http://growth.treebohotel.in/growth/cancellation/cancelhash"
GROWTH_BOOKING_EXP_URL = "http://growth.treebohotel.in/growth/booking_exp/booking_exp_url/"

AUTH_SERVER_HOST = 'http://172.40.20.183:8051'
APP_CRENDENTIALS = {
    'password': {
        'client_id': 'S5zQjG2Ljd1p3hBL56WKtMrCVnXzUe1nEVEXnlXL',
        'client_secret':
            'hG13WIBlkhgv9WTWGiwQMLcjgSHSt6TV16JJk67w9xpHMQomepvq5gLDSGzZPCvw13Ph4LYuJRhfH8U6UGXS5lzcrmQiu5pYOXm9xjVh0GRxFEoFWFF3bV8Xd5EOLeJA'
    }
}

# by default we use staging
HX_CONFIG = HX_STAGING
EASY_JOB_LITE_SETTINGS_PATH = os.path.join(
    EASY_JOB_LITE_SETTINGS_DIR, ENVIRONMENT + ".yaml")

REDIS_URL = 'treebo-rediscache.l4kzjq.0001.apse1.cache.amazonaws.com'
PROWL_HOTEL_POSITIVES_DATA_URL = "http://staging.treebohotels.com/prowl/rest/v3/hotels/positivereviews/?hotelogix_code={0}&days={1}"
REDIS_PORT = 6379
REDIS_DB = 0
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': REDIS_URL + ':' + str(REDIS_PORT),
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'SOCKET_CONNECT_TIMEOUT': 5,
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            },
            'PICKLE_VERSION': -1,
        }
    }
}
# seo phase 2
GROWTH_FREQUENTLY_BOOKED_URL = 'http://staging.treebohotels.com/growth/transformer/frequently_booked/'

TREEBO_WEB_URL = "http://sunil.rana.treebohotel.in"
TRIPADVISOR_BASE_URL = "https://rcp-demo.ext.tripadvisor.com/"
AB_SETTINGS = {
    'is_ab_activated': False,
    'ab_base_url': 'http://ab.treebo.com',
    'application_id': 'website',
    'cookies_enabled': False
}

CATALOGUE_SERVICE_RABBITMQ_SETTINGS = {
    'name': 'catalogue_service',
    'url': 'amqp://guest:guest@172.40.20.210:5672//',
    'exchange': 'cs_exchange',
    'routing_key': 'com.cs.property',
}

EMAIL_HOST_HOTEL_SMTP = 'smtp.gmail.com'

CS_QUEUE_NAME = 'cs_queue_dev'

HOTEL_POLICIES_EMAILS = [
    'neeraj.prasad@treebohotels.com',
    'gaurav.kinra@treebohotels.com',
    'amitakhya.bhuyan@treebohotels.com']