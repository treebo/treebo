import django.utils.html
from caching.base import CachingMixin
from django.conf import settings
from django.db import models
from django.contrib import messages
from djutil.models import TimeStampedModel

from apps.hotels.constants import RoomType
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from dbcommon.models.default_permissions import DefaultPermissions
from .hotel import Hotel
from .room import Room
from dbcommon.constants import MAX_NO_OF_IMAGES_ACROSS_CATEGORIES
from dbcommon.models.location import City
from dbcommon.models.hotel import Categories


class Image(TimeStampedModel, DefaultPermissions):
    IMAGE_MAIN_CATEGORY = [
        ('Reception', 'Reception'),
        ('Lobby', 'Lobby'),
        ('Facade', 'Facade'),
        ('Dining', 'Dining'),
        ('Nearby', 'Nearby'),
        ('Lift', 'Lift'),
        ('Room', 'Room'),
        ('Washroom', 'Washroom'),
        ('Restaurant', 'Restaurant'),
        ('Bar', 'Bar'),
        ('Banquet Hall', 'Banquet Hall'),
        ('Gym', 'Gym'),
        ('Pool', 'Pool'),
        ('Spa', 'Spa'),
        ('Roof Top Cafe', 'Roof Top Cafe'),
        ('Others', 'Others'),
    ]

    url = models.FileField(max_length=200)
    is_showcased = models.BooleanField(default=False)
    tagline = models.CharField(max_length=200)
    hotel = models.ForeignKey(Hotel, blank=True, on_delete=models.DO_NOTHING)
    room = models.ForeignKey(
        Room,
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING)
    position = models.PositiveSmallIntegerField(null=True, default=1)
    category = models.CharField(max_length=20,
                                choices=IMAGE_MAIN_CATEGORY,
                                default='Others')
    image_callout = models.CharField(max_length=50,
                                     blank=True)
    image_name = models.CharField(max_length=50,
                                  blank=True)

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_image'
        ordering = [
            '-is_showcased',
            'position',
            'hotel__name',
            'room__room_type']
        verbose_name_plural = "Images"

    def thumb(self):
        if self.url:
            print(("The url in images is %s", settings.S3_URL))
            # Use django-storages (S3BotoStorage) to upload images, so as to get S3 url automatically. There shouldn't be need to use S3_URL here, or in templates.
            url = get_image_uncode_url(str(self.url.url) + "?w=250&fit=crop&fm=pjpg&h=200")
            return django.utils.html.format_html('<img src=%s width=250 height=250 />' % (url,))
        else:
            return 'No image file found'

    def is_showcased_image(self):
        if self.is_showcased:
            return '✅' # emoji
        else:
            return '❌'

    def get_hotel_name(self):
        if self.hotel is not None:
            return self.hotel.name
        else:
            return ""

    def get_room_name(self):
        if self.room is not None:
            return self.room.room_type
        else:
            return ""

    def get_image_name_and_callout(self):
        if self.image_name:
            return self.image_name, self.image_callout
        if self.category == "Others":
            return self.get_hotel_name(), self.image_callout
        if self.room is not None:
            image_name = '{} {}'.format(
                self.room.room_type_code,
                'Room'
            )
            return image_name, RoomType.ROOM_TYPE_DICT.get(self.room.room_type_code)

        return self.category, self.image_callout

    def __unicode__(self):  # __unicode__ on Python 2
        imageString = ""
        if self.hotel is not None:
            imageString += ("Hotel:" + self.hotel.name + " ")
        if self.room is not None:
            imageString += ("| Room: " + self.room.room_type + " ")
        return imageString

    def __str__(self):  # __unicode__ on Python 2
        imageString = ""
        if self.hotel is not None:
            imageString += ("Hotel:" + self.hotel.name + " ")
        if self.room is not None:
            imageString += ("| Room: " + self.room.room_type + " ")
        return imageString


class ImagesPerCategory(CachingMixin, TimeStampedModel, DefaultPermissions):
    hotel = models.ForeignKey(Hotel, blank=True, on_delete=models.DO_NOTHING)
    category = models.CharField(max_length=20,
                                choices=Image.IMAGE_MAIN_CATEGORY,
                                default='Others')
    no_of_images = models.IntegerField(default=0)
    ranking = models.PositiveSmallIntegerField(null=True)

    class Meta(DefaultPermissions.Meta):
        ordering = ['ranking']
        unique_together = ('hotel', 'ranking')

    def __str__(self):
        return "For {}'s category {} allowed no of images are {}".format(
            self.hotel.name,
            self.category,
            self.no_of_images)

    @staticmethod
    def get_images_by_category(hotel):
        categories = ImagesPerCategory.objects.filter(hotel=hotel)
        all_images = []
        for category in categories:
            images = Image.objects.filter(category=category.category,
                                          hotel=hotel).order_by('position')
            allowed_image = images[0:category.no_of_images]
            all_images += list(allowed_image)
        return all_images[0:MAX_NO_OF_IMAGES_ACROSS_CATEGORIES]


class Feedback(CachingMixin, TimeStampedModel, DefaultPermissions):
    sender = models.CharField(max_length=200, blank=True, null=True)
    receiver = models.CharField(max_length=200, blank=True, null=True)
    message = models.TextField(default="")
    page_url = models.CharField(max_length=200, blank=True, null=True)
    device = models.CharField(max_length=200, blank=True, null=True)

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_feedback'
        verbose_name = 'Feedback'
        verbose_name_plural = 'Feedbacks'

    def __unicode__(self):  # __unicode__ on Python 2
        return self.sender

    def __str__(self):  # __unicode__ on Python 2
        return self.sender


class NotificationArchive(CachingMixin, TimeStampedModel, DefaultPermissions):
    SMS = "SMS"
    EMAIL = "EMAIL"
    NOTIFY_CHOICES = (
        (SMS, 'SMS'),
        (EMAIL, 'EMAIL'),
    )
    notification_type = models.CharField(
        max_length=10, default=SMS, choices=NOTIFY_CHOICES)
    hotel = models.ForeignKey(
        Hotel,
        blank=True,
        null=True,
        on_delete=models.DO_NOTHING)
    guest_name = models.CharField(
        max_length=200,
        null=True,
        blank=True,
        default='')
    guest_mobile = models.CharField(
        max_length=200, null=True, blank=True, default='')
    SUCCESS = "SS"
    FAILURE = "FL"
    DELIVERY_STATUS_CHOICES = (
        (SUCCESS, 'SUCCESS'),
        (FAILURE, 'FAILURE'),
    )
    delivery_status = models.CharField(max_length=20, default=SUCCESS,
                                       choices=DELIVERY_STATUS_CHOICES)
    error_text = models.CharField(
        max_length=3000,
        null=True,
        blank=True,
        default='')

    # objects = CachingManager()

    class Meta(DefaultPermissions.Meta):
        db_table = 'hotels_notificationarchive'
        verbose_name = 'Notification Archive'
        verbose_name_plural = 'Notification Archives'

    def __unicode__(self):
        return '%s %s %s' % (
            self.hotel.name, self.guest_name, self.guest_mobile)

    def __str__(self):
        return '%s %s %s' % (
            self.hotel.name, self.guest_name, self.guest_mobile)


class WifiLocationMap(CachingMixin, TimeStampedModel, DefaultPermissions):
    hotel = models.OneToOneField(Hotel, on_delete=models.DO_NOTHING)
    location_key = models.CharField(max_length=100)
    wifi_name = models.CharField(max_length=30, default='Treebo Hotels')
    is_enabled = models.BooleanField(default=False)

    class Meta(DefaultPermissions.Meta):
        db_table = 'dbcommon_wifilocation_map'
        verbose_name = 'Wifi Location Map'
        verbose_name_plural = 'Wifi Location Maps'

    def __unicode__(self):
        return '%s %s' % (self.hotel.name, self.location_key)

    def __str__(self):
        return '%s %s' % (self.hotel.name, self.location_key)


class WifiCommonConfig(CachingMixin, TimeStampedModel, DefaultPermissions):
    search_radius_in_meter = models.PositiveIntegerField(default=100)

    class Meta(DefaultPermissions.Meta):
        db_table = 'dbcommon_wificommon_config'
        verbose_name = 'Wifi Common Config'
        verbose_name_plural = 'Wifi Common Configs'

    def __unicode__(self):
        return '%s' % (self.search_radius_in_meter)

    def __str__(self):
        return '%s' % (self.search_radius_in_meter)


class CityCategoryMapforAutoPageCreation(CachingMixin, TimeStampedModel, DefaultPermissions):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    category = models.ForeignKey(Categories, null=True, blank=True)
    is_enabled = models.BooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        unique_together = ('city', 'category')
        verbose_name = 'City Category Map for AutoPage Creation'
        verbose_name_plural = 'City Category Map for AutoPage Creation'

    def __unicode__(self):
        return self.city.name + ' ' + self.category.name
