# -*- coding: utf-8 -*-
import abc

from apps.bookings.data_classes import Booking
from apps.bookings.models import Booking as BookingModel
import json


class CreateBookingInterface(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, booking, **kwargs):
        self.booking = booking
        self.kwargs = kwargs

    def create(self):
        created_booking = self._create()
        assert isinstance(created_booking, Booking)
        self._persist_details(created_booking)
        return created_booking

    def _persist_details(self, created_booking):
        booking = BookingModel.objects.get(order_id=self.booking.uid)
        booking.external_crs_booking_id = created_booking.crs_reference_id
        booking.external_crs_booking_version = created_booking.crs_version
        extra_info = {'crs_booking_bill_id': created_booking.crs_bill_id}
        booking.external_crs_booking_extra_info = json.dumps(extra_info)
        booking.booking_status = created_booking.status.old_name
        booking.save()

    @abc.abstractmethod
    def _create(self):
        pass
