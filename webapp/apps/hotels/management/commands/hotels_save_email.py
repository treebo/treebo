import logging

from django.core.management.base import BaseCommand

from apps.hotels.tasks import email_hx
from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Change Email for all Hotels from Hotelogix Email to Treebo Mail"

    def handle(self, *args, **options):
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotelogix_ids = hotel_repository.get_all_hotelogix_ids()
        for hotelogix_id in hotelogix_ids:
            try:
                logger.info("Calling saveemailapi for hotel %s", hotelogix_id)
                email_hx(hotelogix_id)
            except Exception as e:
                logger.exception("Unable to execute save email api")
