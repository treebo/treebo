# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hoteltaoverallratings',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'TA Hotel Overall Ratings',
                'verbose_name_plural': 'TA Hotel Overall Ratings'},
        ),
        migrations.AlterModelOptions(
            name='tahotelreviews',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'TA Top Reviews',
                'verbose_name_plural': 'TA Top Reviews'},
        ),
        migrations.AlterModelOptions(
            name='tareviewratingcountmapping',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'TA Review Rating Count',
                'verbose_name_plural': 'TA Review Rating Count'},
        ),
        migrations.AlterModelTable(
            name='hoteltaoverallratings',
            table='trip_advisor_hotel_overall_ratings',
        ),
        migrations.AlterModelTable(
            name='tareviewratingcountmapping',
            table='trip_advisor_review_rating_count',
        ),
    ]
