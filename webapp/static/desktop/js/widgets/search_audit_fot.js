
import $ from 'jquery';
import {_} from 'underscore';
import cookies from 'js-cookie';
import queryString from 'query-string';
import {history} from 'html5-history-api';
import moment from 'moment';
import Calendar from '../libs/calendar';
import treejax from '../treebolibs/treejax';

import l  from '../treebolibs/logger';

let $loader = $("#searchFotLoader");
var audit_date = "";

let Requests = {
    QueryString : function(item){
        var svalue = location.search.match(new RegExp("[\?\&]" + item + "=([^\&]*)(\&?)","i"));
        return svalue ? svalue[1] : svalue;
    }
};

var methods = {
    searchFotWidgetLoaded: () => {
        var city = rt.city,
            guest = cookies.get('guest');

        audit_date = Requests.QueryString("checkin");
        if (audit_date) {
            audit_date = moment(audit_date).format("YYYY-MM-DD");
        }
        $("div.select-city select").val(city);

        if (guest) {
            $("div.select-guest select").val(guest);
        } else {
            $("div.select-guest select").val(1);
        }
        methods.auditCalander();
    },
    auditCalander : () =>{
        var startDay = rt.startDayThreshold ? rt.startDayThreshold : 1;
        var endDay = rt.endDayThreshold ? rt.endDayThreshold : 42;
        if(!audit_date) {
            audit_date = moment().add(startDay, 'day').format("YYYY-MM-DD");

        }
        var calendar_audit = new Calendar({
                element: $('#auditDate'),
                format: {input: 'DD/MM/YYYY'},
                current_date: audit_date,
                earliest_date: moment().add(startDay, 'day'),
                latest_date: moment().add(endDay, 'day'),
                start_date: moment(),
                end_date: moment().add(endDay, 'days'),
                callback: function() {
                    audit_date  = moment(this.current_date).format('YYYY-MM-DD'),
                    $('body').trigger('datechanged');
                }
        });

    },
    
    search: () => {
        let baseUrl = '/fot/search',
    	    lat = "",
    	    long = '',
    	    checkIn = audit_date,
    	    checkOut = '',
    	    locality = '',
            query = $('#cityFot').val(),
            roomsCount = 0;

		let roomConfig = $('#fotRoomConfig').val();
        cookies.set('city', query);
        cookies.set('guest', roomConfig);
		
		let queryParams = `locality=${locality}&query=${query}&lat=${lat}&long=${long}&checkin=${checkIn}&checkout=${checkOut}&roomconfig=${roomConfig}`;
		window.location = `${baseUrl}?${queryParams}`;
    }
    	

}

$("#searchFotSubmitBtn").click(function(){
    methods.search();
    $loader.removeClass('hide');
});

module.exports = {
    name: "searchFotWidget",
    methods: methods
}
