from rest_framework.status import HTTP_400_BAD_REQUEST
from common.exceptions.treebo_exception import TreeboException

class FeaturePageVariableNotFound(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(FeaturePageVariableNotFound, self).__init__()
        self.message = 'Unable to find feature page variable.'
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + "4002"

class KeywordNotFound(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(KeywordNotFound, self).__init__()
        self.message = 'Unable to find ConstraintKeyword.'
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + "4002"

class OperatorNotFound(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(OperatorNotFound, self).__init__()
        self.message = 'Unable to find ConstraintOperator.'
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + "4002"

class FeatureConstraintUpdateFailure(TreeboException):
    def __init__(self, developer_message, *args, **kwargs):
        super(FeatureConstraintUpdateFailure, self).__init__()
        self.message = 'exception while updating the feature constraint'
        self.developer_message = developer_message
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + "4002"