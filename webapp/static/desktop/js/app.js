import "expose?$!expose?jQuery!jquery";
const $ = require('jquery');
const lazy = require('./libs/jquery.lazyload.js');
import {Observable} from "rx";
const auth = require('./auth');
import discount from './widgets/discount';
import cookies from 'js-cookie';

/*
import * as toastr from 'toastr';
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-left",
  "preventDuplicates": true,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
// Display a warning toast, with no title
toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!')

// Display a success toast, with a title
toastr.success('Have fun storming the castle!', 'Miracle Max Says')

// Display an error toast, with a title
toastr.error('I do not think that word means what you think it means.', 'Inconceivable!')

// Immediately remove current toasts without using animation
toastr.remove()

// Remove current toasts using animation
toastr.clear()

// Override global options
toastr.success('We do have the Kapua suite available.', 'Turtle Bay Resort', {timeOut: 5000})

toastr.success("Are you the six fingered man?")
*/

const urlStr = 'url';
const pathStr = 'path';

const currentUrl = window.location.href;
const currentPath = window.location.pathname;
const previousUrl = cookies.get(urlStr);
const previousPath = cookies.get(pathStr);

rt['previous_url'] = previousUrl;
rt['previous_path'] = previousPath;
cookies.set(urlStr, currentUrl);
cookies.set(pathStr, currentPath);

$(".lazy").lazyload({
    effect : "fadeIn",
    threshold: 300,
});

let auth2 = "";
const queryString = require('query-string');
const parsed = queryString.parse(location.search);
const treejax = require('./treebolibs/treejax');
const parsley = require('parsleyjs');

const onLogin = () => {
    const { next } = parsed;
    if (next) {
        location.href = next;
    } else {
        location.reload();
    }
};

const bindLoginHandler = () => {
    auth2.attachClickHandler('googleSignup', {}, onSignIn, onSignInFailure);
    auth2.attachClickHandler('googleSignIn', {}, onSignIn, onSignInFailure);
};

// TODO - needs to be refactored.
const loginEvents = Rx.Observable.fromEvent($('.js-headerlogin'), 'click');
var subscription = loginEvents.subscribe(e => {
        $('.js-common-modal-body').html($('.auth').html());
        $('#commonModal').removeClass("hide");
        $('#commonModal .login').removeClass('hide');
        $('#commonModal .signup').addClass('hide');
        $('#commonModal .forgot').addClass('hide');

    bindLoginHandler();
});

const signupEvents = Rx.Observable.fromEvent($('.js-headersignup'), 'click');
var subscription = signupEvents.subscribe(e => {
        $('.js-common-modal-body').html($('.auth').html());
        $('#commonModal').removeClass("hide");
        $('#commonModal .login').addClass('hide');
        $('#commonModal .signup').removeClass('hide');
        $('#commonModal .forgot').addClass('hide');
        auth2.attachClickHandler('googleSignup', {}, onSignIn, onSignInFailure);
});

auth.getGoogleHandler().then((res) => {
    auth2 = res;
    auth2.attachClickHandler('guestGoogleLogin', {}, onSignIn, onSignInFailure);
});

function onSignIn(googleUser) {
    auth
        .sendGoogleRegistrationDataToServer(googleUser.getAuthResponse().id_token)
        .then(onLogin);
}
function onSignInFailure() {
  // Handle sign-in errors
}

function bindAuthEvents(){

    let commonModal = Rx.Observable.fromEvent($('#commonModal'), 'click');
    let s = commonModal.subscribe(e => {
        let $elem = $(e.target);
        $('.forgot__error').html('');
        $('.signup__error').html('');
        $('.login__error').html('');

        if($elem.hasClass('js-login-btn')){

            let $form = $elem.closest('.login__form');
            let email = $($form.find('input')[0]).val();
            let password = $($form.find('input')[1]).val();

            const loginParsley = $('#popup-login-form').parsley();
            loginParsley.on('form:submit',() => {
                auth.login(email, password).then(onLogin, (treeboError) => {
                    $('.login__error').html(treeboError.message).show();
                });
                return false;
            });
        } else if ($elem.hasClass('js-signup-btn')) {
            const $form = $('#popup-signup-form');
            const data = {};
            $form.serializeArray().forEach((x) => {
                data[x.name] = x.value;
            });

            $form.parsley().on('form:submit', () => {
                auth.signup(data).then(onLogin, treeboError => {
                    $('.signup__error').html(treeboError.message).show();
                });
                return false;
            });
        }
        else if($elem.hasClass('js-forgot-btn')){
            let $form = $elem.closest('.forgot__form');
            let email = $($form.find('input')[0]).val();

            const forgotParsley = $('#popup-forgot-form').parsley();
            forgotParsley.on('form:submit',() => {
                auth.forgotpassword(email,treeboError => {
                    $('.forgot__error').html(treeboError.message).show();
                });
                return false;
            });
        }
        else if($elem.hasClass('js-back-link')){
            $('.signup').addClass('hide');
            $('.login').removeClass('hide');
            $('.forgot').addClass('hide');
        }

        else if($elem.hasClass('js-signup-link')){
            $('.login').addClass('hide');
            $('.signup').removeClass('hide');
            $('.forgot').addClass('hide');

        }
        else if($elem.hasClass('js-login-link')){
            $('.signup').addClass('hide');
            $('.login').removeClass('hide');
            $('.forgot').addClass('hide');

        }else if($elem.hasClass('js-fblogin') || $elem.parents('.js-fblogin').length){
            auth.fbLogin().then(onLogin);
        }else if($elem.hasClass('js-forgot-link')){
            $('.signup').addClass('hide');
            $('.login').addClass('hide');
            $('.forgot').removeClass('hide');
        }
    });
}

$(() => {
    bindAuthEvents();
})


// Todo : refactor
// var ww = window.innerWidth;
$("img.tree-lazy").each(function() {
    const data_original = $(this).attr("data-original");
    // data_original = data_original.replace(/w=(\d+)/g, "w=" + ww);
    $(this).attr("src", data_original);
});
//
// $("img.js-resize-fit").each(function() {
//     var $this = $(this);
//     var src = $this.attr("data-original");
//     src = src.replace(/w=(\d+)/g, "w=" + ww);
//     $this.attr("src", src);
//     $this.removeAttr("data-original");
// });
//
// $(".lazy").lazyload({
//     container: document.getElementById('mainContainer'),
//     threshold: 200,
//     // data_src: "data-src",
//     effect: "fadeIn"
// });


$(".js-modal__close").click(function(e) {
    $(this).closest(".modal").addClass('hide');
    e.preventDefault();
});

$(".alert--top__close").click(function(e) {
    $(this).closest(".alert--top").addClass('hide');
    e.preventDefault();
});



$(document).keyup(e => {
  if (e.keyCode == 27) {
      e.preventDefault();
      $(".modal").addClass('hide');
  }
});


let feedbackEvent = Rx.Observable.fromEvent($('.feedback'), 'click');
let feedbacksubscription = feedbackEvent.subscribe(e => {
    e.preventDefault();
    $('#feedback').removeClass('hide');
});



let submitFeedbackEvent = Rx.Observable.fromEvent($('#feedbackSubmit'), 'click');
let submitFeedbacksubscription = submitFeedbackEvent.subscribe(e => {
    e.preventDefault();
    const feedbackParsley = $('#feedbackForm').parsley();
    feedbackParsley.validate();
    if(feedbackParsley.isValid()){
        submitFeedback();
    };
});


let cancelFeedbackEvent = Rx.Observable.fromEvent($('#feedbackCancel'), 'click');
let cancelFeedbacksubscription = cancelFeedbackEvent.subscribe(e => {
    $("#feedback").addClass("hide");
});

let $feedbackLoader = $("#feedbackLoader")
let submitFeedback = ()=>{
    let data = {
            name        :$('#feedbackName').val(),
            email       :$('#feedbackEmail').val(),
            comments    :$('#feedbackcomments').val(),
            referrer    :window.location.href,
            device : 'desktop',
            csrfmiddlewaretoken:$('input[name="csrfmiddlewaretoken"]').val()
        }

    $feedbackLoader.removeClass('hide');
    const p = treejax({
            url: '/rest/v1/feedback/',
            type: 'POST',
            data
        }).then((data, status, xhr) => {
            $("#messageLoader").removeClass('hide');
            $(".successMessage").removeClass('hide');
            $("#feedbackForm")[0].reset();


        },treeboError => {
            $("#messageLoader").removeClass('hide');
            $(".failureMessage").removeClass('hide');
            $("#feedbackForm")[0].reset();
        }).finally(() => {
            $feedbackLoader.addClass('hide');
            $("#feedbackForm")[0].reset();
            setTimeout(() => {

                    $("#messageLoader").addClass('hide');
                    $(".successMessage").addClass('hide');
                    $(".failureMessage").addClass('hide');
                    $('#feedback').addClass('hide');
                    $('#feedbackSuccessMsg').hide()
                }, 4000);
        });

    return p;
}



const WebFont = require('webfontloader');

WebFont.load({
    google: {
        families: ['Roboto:400,500', 'Open Sans:800']
    }
});
