import logging

from ths_common.constants.billing_constants import ChargeBillToTypes, ChargeTypes
from ths_common.constants.booking_constants import ProfileTypes
from ths_common.value_objects import Address as THSCAddress, GSTDetails, BookingSource as Source
from thsc.crs.entities.booking import Booking as THSCBooking, Room, Price
from thsc.crs.entities.customer import Customer

from apps.auth.services.user_profile_service import UserProfileService
from apps.bookings.models import Booking as BookingModel
from apps.bookings.providers.get_booking.treebo import TreeboCRSGetBooking
from apps.bookings.providers.interfaces.create_booking import CreateBookingInterface
from apps.bookings.providers.thsc_utils import ThscUtils
from apps.reward.service.loyalty_service import LoyaltyService

logger = logging.getLogger(__name__)


class TreeboCRSCreateBooking(CreateBookingInterface):

    def __init__(self, booking, **kwargs):
        ThscUtils.set_thsc_context(booking.source.application)
        super(TreeboCRSCreateBooking, self).__init__(booking, **kwargs)

    def _create(self):
        ThscUtils.set_thsc_context(self.booking.source.application)
        logger.info("Confirming {b}".format(b=self.booking))
        thsc_booking = self.thsc_booking()

        new_thsc_booking = thsc_booking.create()

        logger.info("Confirmed {b}".format(b=self.booking))

        new_booking = TreeboCRSGetBooking.get_from_thsc_booking(new_thsc_booking)
        return new_booking

    def thsc_booking(self):

        thsc_booking = THSCBooking(hotel_id=self.booking.hotel_id,
                                   booking_owner=self.owner(),
                                   reference_number=self.booking.booking_id,
                                   rooms=self.rooms(),
                                   source=self.source(),
                                   payments=self.payments(),
                                   extra_information={},
                                   )
        if self.booking.expiry_time_stamp:
            thsc_booking.hold_till = self.booking.expiry_time_stamp

        stay_source = self.booking.stay_source
        thsc_booking.extra_information = {"stay_source": stay_source}

        thsc_booking.comments = self.booking.comments or 'N/A'

        return thsc_booking

    def source(self):
        _source = self.booking.source
        return Source(channel_code=_source.channel,
                      subchannel_code=_source.sub_channel,
                      application_code=_source.application,
                      )

    def owner(self):
        booking_owner = self.booking.booking_owner
        gst_details = self.booking.gst_details
        thsc_gst = None
        loyalty_type = None
        if gst_details.gstin_number:
            address = gst_details.address

            thsc_address = THSCAddress(
                field_1=address.line1,
                field_2=address.line2,
                city=address.city,
                state=address.state,
                pincode=address.pincode,
                country=address.country,
            )

            thsc_gst = GSTDetails(
                legal_name=gst_details.legal_name,
                address=thsc_address,
            )
            if gst_details.gstin_number:
                thsc_gst.gstin_num = gst_details.gstin_number

            loyaltyservice = LoyaltyService()
            loyalty_type = loyaltyservice.get_loyalty_type(booking_owner.phone_number)

        profile_type = self._get_profile_type(loyalty_type)

        user = self.kwargs['user']
        user_login_state = self.kwargs['user_login_state']
        if user_login_state == 'LOGGEDIN':
            ups_user_id = UserProfileService().get_user_id(phone_number=user.phone_number)
            ups_user_profile_id = UserProfileService().get_user_profile_id(ups_user_id) if ups_user_id else None
        else:
            ups_user_id = UserProfileService().get_user_id(phone_number=booking_owner.phone_number)
            ups_user_profile_id = UserProfileService().get_user_profile_id(ups_user_id) if ups_user_id else None

        customer = Customer(
            first_name=booking_owner.first_name,
            email=booking_owner.email,
            country_code=booking_owner.country_code,
            phone_number=booking_owner.phone_number,
            reference_id=ups_user_id,
            profile_type=profile_type,
            gst_details=thsc_gst,
            user_profile_id=ups_user_profile_id
        )

        return customer

    def _get_profile_type(self, loyalty_type):
        if not loyalty_type:
            return ProfileTypes.INDIVIDUAL
        profile_dict = {
            "restricted_loyalty": ProfileTypes.INDIVIDUAL,
            "paid_sme": ProfileTypes.SME,
            "corporate": ProfileTypes.CORPORATE
        }
        return profile_dict.get(loyalty_type)

    def rooms(self):
        rooms = []
        for room_stay in self.booking.room_stays:
            prices = [Price(applicable_date=price.date,
                            posttax_amount=price.post_tax,
                            bill_to_type=ChargeBillToTypes.GUEST,
                            type=ChargeTypes.NON_CREDIT,
                            ) for price in room_stay.price]

            room = Room(checkin_date=room_stay.checkin,
                        checkout_date=room_stay.checkout,
                        prices=prices,
                        room_type_id=room_stay.room_type_id,
                        guests=self.guest_stays(room_stay),
                        )
            rooms.append(room)
        return rooms

    def guest_stays(self, room_stay):
        guest_stays = [ThscUtils.thsc_guest_stay_from_guest(guest_stay.guest) for guest_stay in
                       room_stay.guest_stays]
        return guest_stays

    def payments(self):
        thsc_payments = ThscUtils.get_thsc_payments_from_payments(self.booking.payments)
        return thsc_payments

    def crs_to_booking_model_status_mapping(self):
        return {
            "temporary": BookingModel.TEMP_BOOKING_CREATED,
            "reserved": BookingModel.RESERVED,
            "confirmed": BookingModel.RESERVED,
            "cancelled": BookingModel.CANCEL
        }

    @classmethod
    def _map_b2b_rooms_with_thsc_rooms(cls, old_booking, new_booking):

        already_done_room_stays = set()

        def get_thsc_room_stay(room_stay):
            for thsc_room_stay in new_booking.room_stays:
                if thsc_room_stay in already_done_room_stays:
                    continue
                equal = False
                if thsc_room_stay.room_type_id == room_stay.room_type_id:  # this multi if is for efficiency purposes
                    if thsc_room_stay.checkin == room_stay.checkin and thsc_room_stay.checkout == room_stay.checkout:
                        if len(thsc_room_stay.guest_stays) == len(room_stay.guest_stays):
                            if cls._is_same_guest_stays(thsc_room_stay.guest_stays,
                                                        room_stay.guest_stays):
                                equal = True
                if equal:
                    already_done_room_stays.add(thsc_room_stay)
                    return thsc_room_stay
            return None

        room_stay_map = {}
        for room_stay in old_booking.room_stays:
            thsc_room_stay = get_thsc_room_stay(room_stay)
            room_stay_map[room_stay.uid] = thsc_room_stay.uid

        return room_stay_map

    @classmethod
    def _is_same_guest_stays(cls, thsc_guest_stays, guest_stays):
        already_done_guest_stays = set()

        def get_thsc_guest_stay(guest_stay):
            for thsc_guest_stay in thsc_guest_stays:
                if thsc_guest_stay in already_done_guest_stays:
                    continue
                equal = (thsc_guest_stay.guest.name == guest_stay.guest.name
                         and thsc_guest_stay.checkin == guest_stay.checkin
                         and thsc_guest_stay.checkout == guest_stay.checkout
                         )
                if equal:
                    already_done_guest_stays.add(thsc_guest_stay)
                    return thsc_guest_stay
            return None

        matched_guest_stays = [get_thsc_guest_stay(guest_stay) for guest_stay in guest_stays]
        matched_guest_stays = [gs for gs in matched_guest_stays if gs]  # filtering empty values
        return len(matched_guest_stays) == len(guest_stays)
