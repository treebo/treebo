(function($){   
    $(function(){
        $(document).ready(function() {
            $('#id_page_type').bind('change', type_change);
            $("#id_page_type_id").hide();
            $("<select id='entity'></select>").insertAfter("#id_page_type_id");

            $('#entity').bind('change', add_value);
            $('#id_city').bind('change', type_change);
            type_change();
        });
});  
})(django.jQuery);

var $ = django.jQuery.noConflict();

function add_value(){
    value =  $('#entity').find(":selected").text();
    $("#id_page_type_id").val(value);
}

function type_change()
{
    $('#entity >option').remove();
    var page_type = $('#id_page_type').val();
    var current_value =$("#id_page_type_id").val();
    var city = $('#id_city').val();
    if(page_type && city){
        $.ajax({
            "type"     : "GET",
            "url"      : "/qa/page_type/choices/?page_type="+page_type+"&&city="+city,
            "dataType" : "json",
            "cache"    : false,
            "success"  : function(json) {
                for(var j = 0; j < json.length; j++){
                    if(current_value && current_value==json[j]){
                        console.log('match found');
                        $('#entity').append($('<option selected></option>').val(json[j]).html(json[j])).attr('selected','selected');
                    }
                    else{
                        console.log('not found');
                        $('#entity').append($('<option></option>').val(json[j]).html(json[j]));
                    }
                }
                add_value();
            }
        });
    }



}