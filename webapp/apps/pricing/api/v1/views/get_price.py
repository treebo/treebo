import collections
import logging
from datetime import datetime, timedelta

from django.conf import settings
from django.shortcuts import get_object_or_404
from rest_framework.response import Response

import common.constants.common_constants as const
from apps.bookingstash.service.availability_service import \
    get_room_type_min_availability_for_hotel_from_availability_sync
from apps.common import date_time_utils
from apps.common import utils
from apps.common.utils import Utils, AnalyticsEventTracker
from apps.discounts import utils as discountUtils
from apps.discounts.discount_manager import DiscountManager
from apps.discounts.models import DiscountCoupon
from apps.hotels import utils as hotel_utils
from apps.hotels.service.hotel_service import HotelService, HotelEnquiryValidator
from common.services.feature_toggle_api import FeatureToggleAPI
from apps.pricing.pricing_api_impl import PriceUtilsV2
from apps.pricing.utils import PriceUtils
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes
from common.exceptions.treebo_exception import TreeboValidationException
from common.utilities.deprecation_utils import deprecated
from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room
from services.hotellogix.web_services import Hotelogix

logger = logging.getLogger(__name__)


class Pricing(TreeboAPIView):
    __hotelService = HotelService()

    @deprecated
    @log_args(logger)
    def get(self, request, format=None):
        """
        Pricing API
        hotelid -- Hashed Hotel ID
        checkin -- Checkin Date (in YYYY-mm-dd format)
        checkout -- Checkout Date (in YYYY-mm-dd format)
        roomconfig -- Room Configuration required
        roomtype -- Room Types, like: Oak, Maple
        couponcode -- Optional
        """

        logger.debug(
            '==================================JSON room Availability Start================================')
        checkInDate, checkOutDate, roomsConfig, hashedHotelId, roomType, couponCode = self.__parseRequest(
            request)
        logger.debug(
            'Parsed Request: [hashedHotelId: %s, couponCode: %s, roomsConfig: %s]',
            hashedHotelId,
            couponCode,
            roomsConfig)
        if hashedHotelId is None or roomType is None:
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.NO_HOTEL_ROOM_ID_SUPPLIED))

        try:
            HotelEnquiryValidator(
                datetime.strptime(
                    checkInDate,
                    date_time_utils.DATE_FORMAT),
                datetime.strptime(
                    checkOutDate,
                    date_time_utils.DATE_FORMAT),
                roomsConfig).validate()
        except TreeboValidationException as e:
            logger.warn("Invalid request received: %s", str(e))
            raise e
        hotel_id = hotel_utils.decode_hash(hashedHotelId)
        logger.debug('Decoded hotel id: %s', hotel_id)
        hotel = get_object_or_404(Hotel, pk=hotel_id)
        singleDayDelta = timedelta(days=1)
        room = None
        try:
            room = hotel.rooms.get(room_type_code=roomType)
        except Room.DoesNotExist:
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.NO_HOTEL_ROOM_ID_SUPPLIED))

        allAllowed = True
        roomCount = len(roomsConfig)
        for singleRoomConfig in roomsConfig:
            if len(singleRoomConfig) > 1:
                child = singleRoomConfig[1]
            else:
                child = 0
            if not hotel_utils.isOccupancySupported(
                    room, singleRoomConfig[0], child):
                allAllowed = False
                break
        if not allAllowed:
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.INVALID_GUESTS_SUPPLIED))

        rackRate = 0
        voucher_amount = 0
        sellRate = 0
        pretaxPrice = 0
        tax = 0
        discount = 0
        priceError = ""
        discountError = ""
        preferredPromo = False
        dateWiseBreakup = {}
        checkin = datetime.strptime(
            checkInDate, date_time_utils.DATE_FORMAT).date()
        checkout = datetime.strptime(
            checkOutDate, date_time_utils.DATE_FORMAT).date()
        startDate = datetime.strptime(
            checkInDate, date_time_utils.DATE_FORMAT).date()
        begin = datetime.strptime(
            checkInDate, date_time_utils.DATE_FORMAT).date()
        endDate = datetime.strptime(
            checkOutDate, date_time_utils.DATE_FORMAT).date()
        while startDate < endDate:
            dateWiseBreakup[startDate] = {
                'pretax': 0,
                'tax': 0,
                'sell': 0,
                'pretax_rackrate': 0,
                'rack_rate': 0,
                'discount': 0}
            startDate += singleDayDelta
        numberOfNights = (
            datetime.strptime(
                checkOutDate,
                date_time_utils.DATE_FORMAT).date() -
            datetime.strptime(
                checkInDate,
                date_time_utils.DATE_FORMAT).date()).days

        pretax_rack_rate = 0
        logger.debug(
            'Date wise breakup initial auto promo: %s',
            dateWiseBreakup)
        try:
            logger.info('Total Room Config: %s', len(roomsConfig))
            for singleRoomConfig in roomsConfig:
                logger.info(
                    'For loop running for Room Config: %s',
                    singleRoomConfig)
                if FeatureToggleAPI.is_enabled(
                        "pricing", "v2_impl_enabled", False):
                    prices, dateWisePrices, preferredPromo = PriceUtilsV2.getRoomPriceForDateRange(
                        hotel, checkin, checkout, room, singleRoomConfig[0])
                else:
                    prices, dateWisePrices, preferredPromo = PriceUtils.getRoomPriceForDateRange(
                        checkin, checkout, room, singleRoomConfig[0])
                logger.info(
                    'Prices: %s.. Date Wise Prices: %s.. Preferred Promo: %s',
                    prices,
                    dateWisePrices,
                    preferredPromo)
                sellRate += prices['sell']
                pretaxPrice += prices['pretax']
                tax += prices['tax']
                rackRate += prices['rack_rate']
                pretax_rack_rate += prices['pretax_rackrate']
                begin, end = checkin, checkout
                while begin < endDate:
                    dateWiseBreakup[begin]['pretax'] += float(
                        dateWisePrices[begin]['pretax'])
                    dateWiseBreakup[begin]['tax'] += float(
                        dateWisePrices[begin]['tax'])
                    dateWiseBreakup[begin]['sell'] += float(
                        dateWisePrices[begin]['sell'])
                    dateWiseBreakup[begin]['rack_rate'] += float(
                        dateWisePrices[begin]['rack_rate'])
                    dateWiseBreakup[begin]['pretax_rackrate'] += float(
                        dateWisePrices[begin]['pretax_rackrate'])
                    begin += singleDayDelta

            logger.debug('Date wise breakup: %s', dateWiseBreakup)
        except Exception as e:
            logger.exception("Exception while getting Preferred Promo")
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.INVALID_DATA))

        if request.user.is_authenticated():
            user = request.user
        else:
            user = None

        autoPromo = False
        coupon_desc = ""
        if not bool(couponCode):
            kwargs = {
                'checkin': checkInDate,
                'checkout': checkOutDate,
                'hotel_id': hotel.id,
                'user': user,
                "no_bookings": roomCount,
                'rooms': room.room_type_code,
                'booking_date': datetime.now(),
                'total_pretax_cost': pretaxPrice,
                'totalcost': sellRate,
                'room_config': roomsConfig,
                'city': hotel.city}

            autoPromoCouponResponse = discountUtils.getAutoPromos(**kwargs)
            logger.debug(
                'AutoPromoCouponResponse: %s',
                autoPromoCouponResponse)
            if autoPromoCouponResponse['code'] == 200:
                couponCode = autoPromoCouponResponse['coupon_code']
                autoPromo = True

        logger.info('Rack Rate before coupon: %s', rackRate)
        final_price = sellRate

        coupon = None
        if bool(couponCode):
            if not autoPromo:
                autoPromo = discountUtils.isAutoPromo(couponCode)

            kwargs = {
                'code': couponCode,
                'checkin': checkInDate,
                'room_config': roomsConfig,
                'checkout': checkOutDate,
                'hotel_id': hotel.id,
                'user': user,
                'no_bookings': roomCount,
                'booking_date': datetime.now(),
                'total_pretax_cost': pretaxPrice,
                'totalcost': sellRate,
                'rack_rate': rackRate,
                'rooms': room.room_type_code,
                'city': hotel.city,
                'pretax_rack_rate': pretax_rack_rate}
            discountManager = DiscountManager(**kwargs)
            coupon, message = discountManager.validate()
            if coupon:
                if coupon.terms:
                    coupon_desc = str(coupon.terms.replace('\\n', ' '))
                    # terms_list = coupon.terms.split("\\n")
                    # coupon.terms = terms_list
                discountApplied = discountManager.apply(coupon, roomCount)
                coupon_code, couponMessage, couponType, terms = coupon.code, coupon.coupon_message, \
                    coupon.coupon_type, coupon.terms
                logger.debug("DiscountManager response... discount" + str(
                    discountApplied) + "terms" + "coupon msg" + couponMessage)
                discount = discountApplied
                """Calculate TAX again in case of pretax discount"""
                # tax = PriceUtils.getTaxOnPrice(hotel, float(pretaxPrice) - float(discount))

                # NOTE: Assumption here is RACK_RATE discount is applied only
                # on Posttax RackRate. This will change later on, then we've to
                # start calculating tax on rack_rate too
                if coupon.price_component_applicable != DiscountCoupon.RACK_RATE:
                    percentage = (float(discount) / float(pretaxPrice)) * 100
                else:
                    percentage = (
                        float(discount) / float(pretax_rack_rate)) * 100

                voucherCounter = 0
                newRackRate = 0
                newSellRate = 0
                newPretaxPrice = 0
                tax = 0
                pretax_rack_rate = 0
                startDate = datetime.strptime(
                    checkInDate, date_time_utils.DATE_FORMAT).date()
                endDate = datetime.strptime(
                    checkOutDate, date_time_utils.DATE_FORMAT).date()
                while startDate < endDate:
                    dateWiseBreakup[startDate] = {
                        'pretax': 0,
                        'tax': 0,
                        'sell': 0,
                        'pretax_rackrate': 0,
                        'rack_rate': 0,
                        'discount': 0}
                    startDate += singleDayDelta
                logger.debug('Date Wise Breakup initial: %s', dateWiseBreakup)
                for singleRoomConfig in roomsConfig:
                    if FeatureToggleAPI.is_enabled(
                            "pricing", "v2_impl_enabled", False):
                        prices, dateWisePrices, preferredPromo = PriceUtilsV2.getRoomPriceForDateRange(
                            hotel, checkin, checkout, room, singleRoomConfig[0], percentage)
                    else:
                        prices, dateWisePrices, preferredPromo = PriceUtils.getRoomPriceForDateRange(
                            checkin, checkout, room, singleRoomConfig[0], percentage, coupon)

                    newSellRate += prices['sell']
                    newPretaxPrice += prices['pretax']
                    tax += prices['tax']
                    newRackRate += prices['rack_rate']
                    pretax_rack_rate += prices['pretax_rackrate']
                    logger.info("Pretax Rack Rate: %s", pretax_rack_rate)
                    begin, end = checkin, checkout
                    while begin < endDate:
                        dateWiseBreakup[begin]['pretax'] += float(
                            dateWisePrices[begin]['pretax'])
                        dateWiseBreakup[begin]['tax'] += float(
                            dateWisePrices[begin]['tax'])
                        dateWiseBreakup[begin]['sell'] += float(
                            dateWisePrices[begin]['sell'])
                        dateWiseBreakup[begin]['rack_rate'] += float(
                            dateWisePrices[begin]['rack_rate'])
                        dateWiseBreakup[begin]['discount'] += float(
                            dateWisePrices[begin]['discount'])
                        dateWiseBreakup[begin]['pretax_rackrate'] += float(
                            dateWisePrices[begin]['pretax_rackrate'])

                        # voucher_amount, voucherCounter, newSellRate, newPretaxPrice, tax = \
                        #     self._calculate_voucher_amount(self, coupon, voucher_amount, voucherCounter,
                        #                                dateWisePrices[begin], newSellRate,
                        # newPretaxPrice, tax)
                        begin += singleDayDelta

                try:
                    if coupon and str(
                            coupon.coupon_type) == DiscountCoupon.VOUCHER and str(
                            coupon.discount_operation) == DiscountCoupon.FOTREDEEM:
                        discount_value = float(coupon.discount_value)
                        if discount_value <= float(newSellRate):
                            newSellRate -= discount_value
                            voucher_amount = discount_value
                        else:
                            voucher_amount = newSellRate
                            newSellRate = 0.0
                    else:
                        logger.debug("Coupon code is not valid")
                except Exception as e:
                    logger.exception(e)
                    logger.exception('voucher_amount generation failed')

                logger.debug('Voucher amount: %s' % str(voucher_amount))
                if voucher_amount and not discount:
                    discountApplied = voucher_amount
                rackRate = newRackRate
                pretaxPrice = newPretaxPrice
                sellRate = newSellRate
                logger.debug('Date wise breakup final: %s', dateWiseBreakup)
                logger.debug("Recalculated tax on discount = %s " % tax)
                # if coupon.price_component_applicable !=
                # DiscountCoupon.RACK_RATE:
                final_price = float(pretaxPrice) + \
                    float(tax) - float(discountApplied)
                # else:
                #	final_price = float(rackRate) - float(discountApplied)
            else:
                discountError = message

            if request.GET.get('isDiscountCall') is not None:
                utils.trackAnalyticsServerEvent(
                    request,
                    'Apply Discount Coupon',
                    {
                        'hotel_id': hotel.id,
                        'hotel_name': hotel.name,
                        'discount_coupon': couponCode,
                        'user_login_state': 'LOGGEDIN' if request.user and request.user.is_authenticated() else 'LOGGEDOUT',
                        'user_signup_channel': request.COOKIES.get('user_signup_channel'),
                        'user_signup_date': request.COOKIES.get('user_signup_channel'),
                        'discount_value': discount,
                        'status': 'success' if not discountError else 'fail'},
                    channel=AnalyticsEventTracker.WEBSITE)

        if settings.AVAILABILITY_FROM_BASIC_SEARCH:
            roomDetails = self.__getAvailabilityFromHotelogix(
                hotel, checkInDate, checkOutDate, roomsConfig, request)
        else:
            roomDetails = get_room_type_min_availability_for_hotel_from_availability_sync(
                        hotel, checkInDate, checkOutDate, roomsConfig)

        availability = False
        for singleRoomDetail in roomDetails:
            if roomType == singleRoomDetail['roomtype']:
                availability = singleRoomDetail['available']

        priceError = ""
        if not availability:
            priceError = roomType + " is not Available"

        nightsBreakup = []
        dateWiseBreakup = collections.OrderedDict(
            sorted(dateWiseBreakup.items()))

        for priceKey in list(dateWiseBreakup.keys()):
            curdate = priceKey.strftime("%Y-%m-%d")
            night_breakup = {
                "date": curdate,
                "base_price": utils.round_to_two_decimal(dateWiseBreakup[priceKey]['pretax']),
                "tax": utils.round_to_two_decimal(dateWiseBreakup[priceKey]['tax']),
                "sell_rate": utils.round_to_two_decimal(dateWiseBreakup[priceKey]['sell']),
                "rack_rate": utils.round_to_two_decimal(dateWiseBreakup[priceKey]['rack_rate']),
                "pretax_rack_rate": utils.round_to_two_decimal(dateWiseBreakup[priceKey]['pretax_rackrate']),
                "pretax_price": utils.round_to_two_decimal(dateWiseBreakup[priceKey]['pretax_rackrate']),
                "discount": utils.round_to_two_decimal(dateWiseBreakup[priceKey]['discount'])
            }
            if coupon and coupon.price_component_applicable != DiscountCoupon.RACK_RATE:
                night_breakup['final_price'] = utils.round_to_two_decimal(
                    night_breakup['rack_rate'] - night_breakup['discount'])
            else:
                night_breakup['final_price'] = utils.round_to_two_decimal(
                    night_breakup['sell_rate'] - night_breakup['discount'])
            nightsBreakup.append(night_breakup)

        context = {
            "couponDesc": coupon_desc,
            "status": "success",
            "available": availability,
            "nights": nightsBreakup,
            "priceError": priceError,
            "discountError": discountError,
            "couponcode": couponCode,
            "mmpromo": preferredPromo,
            "autopromo": autoPromo,
            "roomdetails": roomDetails,
            "final_price": utils.round_to_nearest_integer(final_price),
            "rackRate": utils.round_to_nearest_integer(rackRate),
            "sellRate": utils.round_to_nearest_integer(sellRate),
            "price": utils.round_to_two_decimal(pretaxPrice),
            "tax": utils.round_to_two_decimal(tax),
            "voucherAmount": utils.round_to_two_decimal(voucher_amount),
            "discount": utils.round_to_two_decimal(discount),
            "pretax_rack_rate": utils.round_to_two_decimal(pretax_rack_rate),
            "pretax_no_rounding": pretaxPrice,
            "tax_no_rounding": tax,
            "discount_no_rounding": discount,
            "pretax_rack_rate_no_rounding": pretax_rack_rate,
            "final_price_rounded_two_decimal": utils.round_to_two_decimal(final_price),
        }

        return Response(context)

    def __parseRequest(self, request):
        d = request.GET

        # hotelId = hotel_utils.decode_hash(args[1]['hashedHotelId'])
        try:
            checkInDate = d.get('checkin') if 'checkin' in d and d.get(
                'checkin') else datetime.now().strftime("%Y-%m-%d")
        except ValueError:
            logger.info(
                "Invalid date format received for checkIn :" +
                d.get(
                    'checkin',
                    "None"))
            checkInDate = datetime.now().strftime("%Y-%m-%d")
        try:
            checkOutDate = d.get('checkout') if 'checkout' in d and d.get(
                'checkout') else (
                datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
        except ValueError:
            logger.info(
                "Invalid date format received for checkOut :" +
                d.get(
                    'checkout',
                    "None"))
            checkOutDate = (
                datetime.now() +
                timedelta(
                    days=1)).strftime("%Y-%m-%d")

        roomConfig = []

        try:
            roomConfigString = d.get('roomconfig') if 'roomconfig' in d and d.get(
                'roomconfig') else '1-0'
            roomWiseConfigs = roomConfigString.split(',')
            for room in roomWiseConfigs:
                roomTuple = room.split('-')
                adults = int(roomTuple[0])
                if len(roomTuple) > 1:
                    children = int(roomTuple[1])
                else:
                    children = 0
                roomConfig.append((adults, children))
        except ValueError:
            logger.info("Invalid roomConfig Format")
            roomConfig = [(1, 0)]

        hashedHotelId = d.get('hotelid', None)
        roomType = d.get('roomtype', None)
        couponCode = d.get('couponcode', None)

        return checkInDate, checkOutDate, roomConfig, hashedHotelId, roomType, couponCode

    def __getAvailabilityFromHotelogix(
            self,
            hotel,
            checkInDate,
            checkOutDate,
            roomConfig,
            request):
        xml = self.__callHotelogixForAvailability(
            checkInDate, checkOutDate, roomConfig, hotel, request)
        rooms = self.__hotelService.getRoomAvailabilityAndPrice(
            hotel, datetime.strptime(
                checkInDate, date_time_utils.DATE_FORMAT), datetime.strptime(
                checkOutDate, date_time_utils.DATE_FORMAT), xml)
        roomList = []
        for room in rooms:
            roomList.append({'roomtype': room.room_type_code,
                             'available': room.isAvailable if hasattr(room,
                                                                      'isAvailable') else False})
        return roomList

    def __callHotelogixForAvailability(
            self,
            checkInDate,
            checkOutDate,
            roomConfig,
            hotel,
            request):
        # TODO - Why do we use maxCount?? - Amith
        roomCount = len(roomConfig)
        maxAdult = 1
        maxChild = 0
        for singleRoomConfig in roomConfig:
            if singleRoomConfig[0] > maxAdult:
                maxAdult = singleRoomConfig[0]
            if len(singleRoomConfig) > 1:
                if singleRoomConfig[1] > maxChild:
                    maxChild = singleRoomConfig[1]

        try:
            self._setAccessKeys(request)
            xml = Hotelogix.buildDetailsXml(
                checkInDate,
                checkOutDate,
                maxAdult,
                maxChild,
                0,
                roomCount,
                100,
                0,
                0,
                hotel.hotelogix_id)

            xml = self._callHotelogix("search", xml, request)

        except Exception as exp:
            logger.debug(exp)
            xml = const.EMPTY_XML

        return xml

    # def _calculate_voucher_amount(self, coupon, voucher_amount, voucherCounter, beginDateWisePrices, newSellRate, newPretaxPrice, tax):
    # return voucher_amount, voucherCounter, newSellRate, newPretaxPrice, tax
