from rest_framework import serializers
from apps.common.validators import CustomValidator as custom_validator


class ForgotPasswordDTO(serializers.Serializer):
    email = serializers.EmailField(max_length=200)

    def validate_email(self, email):
        return custom_validator.normalise_email(email)
