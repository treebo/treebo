import json
import logging

from django.core.cache import cache
from rest_framework.response import Response

from apps.content import models
from apps.content.serializers import ContentStoreSerializer
from base import log_args
from base.views.api import TreeboAPIView
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class ContentAPI(TreeboAPIView):
    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'apicache_content_v2_{0}'

    @staticmethod
    def invalidate_cache(key):
        if key:
            cache.delete(ContentAPI.CACHE_KEY.format(key))
        else:
            cache.delete_pattern(ContentAPI.CACHE_KEY.format('*'))

    def get_cache_key(self, key):
        return ContentAPI.CACHE_KEY.format(key)

    @log_args(logger)
    def get(self, request, format=None):
        """
        Content API
        keys -- comma separated keys to fetch
        """
        try:
            key_names = self.__parse_get_request(request)
            keys = [self.get_cache_key(key) for key in key_names]
            cached_keys = []
            try:
                cached_keys = cache.get_many(keys)
            except Exception as e:
                logger.exception("Exception in getting cached keys")
            values = {}
            keys_to_fetch_from_db = []
            for key in key_names:
                keys_to_fetch_from_db.append(key) if self.get_cache_key(
                    key) not in cached_keys else values.update({key: cached_keys.get(key)})
            content_object = models.ContentStore.objects.filter(
                name__in=keys_to_fetch_from_db, version=1)
            response = ContentStoreSerializer(content_object, many=True)
            data = response.data
            new_values = {
                content['name']: json.loads(
                    content['value']) for content in data}
            cache.set_many(data=new_values, timeout=ContentAPI.CACHE_TIMEOUT)
            values.update(new_values)
        except KeyError:
            return Response(
                status=400,
                data={
                    'status': 'error',
                    'msg': 'Please pass at least one key to fetch from content store'})

        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            return Response(
                status=500,
                data={
                    'msg': e.__str__()
                })
        return Response(values)

    def __parse_get_request(self, request):
        try:
            d = request.GET
            key_name = d.get('keys')
            keys = [key.strip() for key in key_name.split(',')]
            return keys
        except Exception as e:
            raise KeyError
