import datetime
import logging
from concurrent.futures import ThreadPoolExecutor, as_completed
from math import ceil

from django.core.cache import cache
from web_sku.availability.services.availability_service import AvailabilityService
from web_sku.catalog.services.sku_request_transformer import SkuRequestTransformer
from web_sku.pricing.services.pricing_service import PricingService

from common.constants import common_constants as const
from common.services.sku_service import SKUService
from dbcommon.models.hotel import Hotel, City, Locality
from dbcommon.services.web_cs_transformer import WebCsTransformer
from apps.common.slack_alert import SlackAlertService as slack_alert


logger = logging.getLogger(__name__)

direct = 'direct'
website = 'website'


class UpdateMinPriceServiceSKU:

    def __init__(self):
        self.room_configs = '1-0'
        self.batch_size = 30
        self.web_cs_transformer = WebCsTransformer()
        self.hotels = Hotel.objects.filter(status=const.ENABLED).all()
        self.cities = City.objects.filter(status=const.ENABLED).all()
        self.hotel_ids = [hotel.id for hotel in self.hotels]
        self.cs_ids = [hotel.cs_id for hotel in self.hotels]
        self.hotel_cs_map = self.web_cs_transformer.get_hotel_cs_ids_from_web_ids(self.hotel_ids).get('live')
        self.sku_transformer = SkuRequestTransformer(cs_ids=self.cs_ids, room_configs=self.room_configs)
        self.pricing_service = PricingService()
        self.availability_service = AvailabilityService()
        self.timeout = 4 * 24 * 60 * 60

    def create_key(self, key_prefix):
        return (str(key_prefix) + "_SKU_MIN_PRICE")

    def create_old_key(self, key_prefix):
        return (str(key_prefix) + "_min_price")

    def create_min_sell_price_key(self, key_prefix):
        return (str(key_prefix) + "_min_sell_price")

    def create_max_key(self, key_prefix):
        return ('hotel_id_' + str(key_prefix) + "_max_price")

    def update_min_price_in_cache(self):
        hotels_price_response = self.fetch_sku_prices_for_all_hotels()
        for hotel_id in self.hotel_ids:
            try:
                min_price_hotel_id = float(10000.0)
                default_min_price = float(799.0)
                cs_id = self.hotel_cs_map.get(hotel_id)
                hotel_price_dict = hotels_price_response.get(cs_id, None)
                if hotel_price_dict is None or hotel_price_dict.get("room_wise_price", None) is None:
                    key = self.create_key(key_prefix=hotel_id)
                    key_old = self.create_old_key(key_prefix=hotel_id)
                    key_min_sell_price = self.create_min_sell_price_key(key_prefix=hotel_id)
                    cache.set(key, str(int(ceil(default_min_price))), timeout=self.timeout)
                    cache.set(key_old, str(int(ceil(default_min_price))), timeout=self.timeout)
                    cache.set(key_min_sell_price, str(int(ceil(default_min_price))), timeout=self.timeout)
                    logger.exception("Prices Not available for hotel_id %s", hotel_id)
                    continue
                hotel_room_wise_price = hotel_price_dict.get("room_wise_price", None)
                for room_type in hotel_room_wise_price:
                    min_room_type_price = self.get_min_room_price_each_type(hotel_room_wise_price.get(room_type))
                    if min_room_type_price < min_price_hotel_id:
                        min_price_hotel_id = min_room_type_price
                key = self.create_key(key_prefix=hotel_id)
                key_old = self.create_old_key(key_prefix=hotel_id)
                key_min_sell_price = self.create_min_sell_price_key(key_prefix=hotel_id)
                cache.set(key,  str(int(ceil(min_price_hotel_id))), timeout=self.timeout)
                cache.set(key_old, str(int(ceil(min_price_hotel_id))), timeout=self.timeout)
                cache.set(key_min_sell_price, str(int(ceil(min_price_hotel_id))), timeout=self.timeout)
            except Exception as err:
                slack_alert.send_slack_alert_for_exceptions(status=500, request_param='GET',
                                                            dev_msg="Minimum room_price cache set failed",
                                                            api_name="update_cache_price",
                                                            class_name="[UpdateMinPriceServiceSKU]",
                                                            message=str(err))
                logger.exception("Minimum room_price cache set failed due to %s", str(err))
                raise Exception("Minimum room_price cache set failed %s", str(err))
        self.update_city_wise_min_price()
        self.update_locality_wise_min_price()

    def update_max_price_in_cache(self):
        hotels_max_price_response = self.fetch_sku_max_prices_for_all_hotels()
        for hotel_id in self.hotel_ids:
            try:
                max_price_hotel_id = float(4000.0)
                default_max_price = float(999.0)
                cs_id = self.hotel_cs_map.get(hotel_id)
                hotel_price_dict = hotels_max_price_response.get(cs_id, None)
                if hotel_price_dict is None or hotel_price_dict.get("room_wise_price", None) is None:
                    key = self.create_max_key(key_prefix=hotel_id)
                    cache.set(key, str(int(ceil(max_price_hotel_id))), timeout=self.timeout)
                    logger.exception("Prices Not available for hotel_id %s", hotel_id)
                    continue
                hotel_room_wise_price = hotel_price_dict.get("room_wise_price", None)
                max_room_type_price = default_max_price
                for room_type in hotel_room_wise_price:
                    max_room_type_price = self.get_max_room_price_each_type(hotel_room_wise_price.get(room_type))
                    if max_room_type_price > default_max_price:
                        default_max_price = max_room_type_price
                key = self.create_max_key(key_prefix=hotel_id)
                cache.set(key, str(int(ceil(max_room_type_price))), timeout=self.timeout)
            except Exception as err:
                slack_alert.send_slack_alert_for_exceptions(status=500, request_param='GET',
                                                            dev_msg="Maximum room_price cache set failed",
                                                            api_name="update_cache_price",
                                                            class_name="[UpdateMinPriceServiceSKU]",
                                                            message=str(err))
                logger.exception("Maximum room_price cache set failed due to %s", str(err))
                raise Exception("Maximum room_price cache set failed %s", str(err))

    def update_city_wise_min_price(self):
        for city in self.cities:
            try:
                hotel_objects_city_wise = [hotel for hotel in self.hotels if hotel.city_id == city.id]
                if hotel_objects_city_wise:
                    hotel_ids = [hotel.id for hotel in hotel_objects_city_wise]
                min_price = self.calculate_min_price_in_hotel_ids(hotel_ids)
                key = self.create_key(key_prefix=city.name)
                key_old = self.create_old_key(key_prefix=city.name)
                key_min_sell_price = self.create_min_sell_price_key(key_prefix=city.name)
                cache.set(key, str(int(ceil(min_price))), timeout=self.timeout)
                cache.set(key_old, str(int(ceil(min_price))), timeout=self.timeout)
                cache.set(key_min_sell_price, str(int(ceil(min_price))), timeout=self.timeout)
            except Exception as err:
                logger.exception("Update city cache failed %s", str(err))

    def update_locality_wise_min_price(self):
        try:
            active_city_id = [city.id for city in self.cities]
            self.localities = Locality.objects.filter(city_id__in=active_city_id).all()
            for locality in self.localities:
                hotel_ids = [hotel.id for hotel in self.hotels if hotel.locality_id == locality.id]
                min_price = self.calculate_min_price_in_hotel_ids(hotel_ids)
                key = self.create_key(key_prefix=locality.name)
                key_old = self.create_old_key(key_prefix=locality.name)
                key_min_sell_price = self.create_min_sell_price_key(key_prefix=locality.name)
                cache.set(key, str(int(ceil(min_price))), timeout=self.timeout)
                cache.set(key_old, str(int(ceil(min_price))), timeout=self.timeout)
                cache.set(key_min_sell_price, str(int(ceil(min_price))), timeout=self.timeout)
        except Exception as err:
            logger.exception("Update cache for locality failed %s", str(err))

    def calculate_min_price_in_hotel_ids(self, hotel_ids):
        min_price = float(10000.0)
        for hotel_id in hotel_ids:
            key = self.create_key(key_prefix=hotel_id)
            price = int(cache.get(key))
            if ceil(price) < min_price:
                min_price = ceil(price)

        return float(min_price)

    def fetch_sku_prices_for_all_hotels(self):
        total_active_prices = {}
        futures = []
        total_batches = ceil(len(self.hotel_ids) / self.batch_size)
        with ThreadPoolExecutor(max_workers=5) as executor:

            for batch in range(0, total_batches):
                try:
                    if (self.batch_size * (batch + 1)) > len(self.hotel_ids):
                        hotel_batch_ids = self.hotel_ids[self.batch_size * batch:len(self.hotel_ids) + 1]
                    else:
                        hotel_batch_ids = self.hotel_ids[self.batch_size * batch:self.batch_size * (batch + 1)]
                    cs_ids = self.get_cs_ids_for_hotel_ids(hotel_batch_ids)
                    future = executor.submit(self.get_detailed_pricing_response, cs_ids)
                    futures.append(future)
                except Exception as err:
                    logger.exception("Fetching Sku for batches failed %s", str(err))
            for future in as_completed(futures):
                response = future.result()
                total_active_prices.update(response)
        logger.info('All active prices for cache updation {}'.format(total_active_prices))
        return total_active_prices

    def get_cs_ids_for_hotel_ids(self, hotel_batch_ids):
        cs_ids = []
        for hotel_id in hotel_batch_ids:
            try:
                cs_ids.append(self.hotel_cs_map.get(hotel_id))
            except Exception as err:
                logger.exception("Couldn't find cs_id %s due to %s", str(hotel_id),str(err))
        return cs_ids

    def get_detailed_pricing_response(self, cs_ids):
        hotel_prices = {}
        sku_availability = {}
        if not cs_ids:
            return sku_availability

        skus = SKUService.get_sku_for_hotels(
            hotel_cs_id_list=cs_ids,
            room_config='1-0',
            room_types=None)
        available_cs_ids = []
        for cs_id in skus:
            if skus.get(cs_id):
                available_cs_ids.append(cs_id)
        if available_cs_ids:
            sku_availability = SKUService.get_sku_availability_for_hotels(
                skus=skus,
                hotel_cs_id_list=available_cs_ids,
                room_config=self.room_configs,
                checkin_date=(datetime.date.today() + datetime.timedelta(days=1)).isoformat(),
                checkout_date=(datetime.date.today() + datetime.timedelta(days=4)).isoformat(),
                channel=direct,
                sub_channel='website-direct')
            available_skus_for_cs_id = {}
            for cs_id in sku_availability:
                if sku_availability.get(cs_id):
                    available_skus_for_cs_id.update({cs_id: sku_availability.get(cs_id)})
            hotel_prices = SKUService.get_cheapest_price_from_sku_availability(
                skus=skus,
                sku_availability=available_skus_for_cs_id,
                checkin_date=(datetime.datetime.today() + datetime.timedelta(days=1)).strftime(
                    '%Y-%m-%dT%H:%M:%S.000Z'),
                checkout_date=(datetime.datetime.today() + datetime.timedelta(days=4)).strftime(
                    '%Y-%m-%dT%H:%M:%S.000Z'),
                channel=direct,
                sub_channel='website-direct',
                source=direct,
                application=website
            )
        return hotel_prices

    def get_min_room_price_each_type(self, room_price_dict):
        min_price = float(10000.0)
        for policy_wise_price in room_price_dict:
            if policy_wise_price["policy_name"] in ['EP', 'ANY', 'rp', 'nrp']:
                date_wise_prices = policy_wise_price["date_wise_price_breakup"]
                for date_wise_price in date_wise_prices:
                    if date_wise_price["post_tax"] < min_price:
                        min_price = date_wise_price["post_tax"]
        return min_price

    def get_max_detailed_pricing_response(self, cs_ids):
        overall_hotel_prices = {}
        hotel_prices = {}
        sku_availability = {}
        max_count_config = {0: '3-0', 1: '2-1', 2: '2-0', 3: '1-0'}
        non_available_cs_ids = cs_ids
        count = 0
        while count < 4 and non_available_cs_ids:
            skus = SKUService.get_sku_for_hotels(
                hotel_cs_id_list=non_available_cs_ids,
                room_config=max_count_config.get(count),
                room_types=None)
            available_cs_ids = []
            for cs_id in skus:
                if skus.get(cs_id):
                    available_cs_ids.append(cs_id)
            if available_cs_ids:
                sku_availability = SKUService.get_sku_availability_for_hotels(
                    skus=skus,
                    hotel_cs_id_list=available_cs_ids,
                    room_config=max_count_config.get(count),
                    checkin_date=(datetime.date.today() + datetime.timedelta(days=2)).isoformat(),
                    checkout_date=(datetime.date.today() + datetime.timedelta(days=3)).isoformat(),
                    channel=direct,
                    sub_channel='website-direct')
                available_skus_for_cs_id = {}
                for cs_id in sku_availability:
                    if sku_availability.get(cs_id):
                        available_skus_for_cs_id.update({cs_id: sku_availability.get(cs_id)})
                        non_available_cs_ids.remove(cs_id)
                count += 1
                hotel_prices = SKUService.get_cheapest_price_from_sku_availability(
                    skus=skus,
                    sku_availability=available_skus_for_cs_id,
                    checkin_date=(datetime.datetime.today() + datetime.timedelta(days=2)).strftime(
                        '%Y-%m-%dT%H:%M:%S.000Z'),
                    checkout_date=(datetime.datetime.today() + datetime.timedelta(days=3)).strftime(
                        '%Y-%m-%dT%H:%M:%S.000Z'),
                    channel=direct,
                    sub_channel='website-direct',
                    source=direct,
                    application=website
                )
                overall_hotel_prices.update(hotel_prices)

        return overall_hotel_prices

    def get_max_room_price_each_type(self, room_price_dict):
        max_price = float(999.0)
        for policy_wise_price in room_price_dict:
            if policy_wise_price["policy_name"] in ['EP', 'ANY', 'rp', 'nrp']:
                date_wise_prices = policy_wise_price["date_wise_price_breakup"]
                for date_wise_price in date_wise_prices:
                    if date_wise_price["post_tax"] > max_price:
                        max_price = date_wise_price["post_tax"]

        return max_price

    def fetch_sku_max_prices_for_all_hotels(self):
        total_active_prices = {}
        futures = []
        total_batches = ceil(len(self.hotel_ids) / self.batch_size)
        with ThreadPoolExecutor(max_workers=10) as executor:
            for batch in range(0, total_batches):
                try:
                    if (self.batch_size * (batch + 1)) > len(self.hotel_ids):
                        hotel_batch_ids = self.hotel_ids[self.batch_size * batch:len(self.hotel_ids) + 1]
                    else:
                        hotel_batch_ids = self.hotel_ids[self.batch_size * batch:self.batch_size * (batch + 1)]
                    cs_ids = self.get_cs_ids_for_hotel_ids(hotel_batch_ids)
                    future = executor.submit(self.get_max_detailed_pricing_response, cs_ids)
                    futures.append(future)
                except Exception as err:
                    logger.exception("Fetching Sku for batches failed %s", str(err))
                for future in as_completed(futures):
                    response = future.result()
                    total_active_prices.update(response)
        logger.info('All active prices for cache updation {}'.format(total_active_prices))
        return total_active_prices
