import json
import logging

from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.hotels.serializers.hotel_serializer import MinimalDetailsHotelSerializer
from apps.hotels.serializers.wifi_serializer import WifiLocationSerializer
from apps.search.dto.hotel_serarch_dto import HotelSearchDTO
from apps.search.services.hotel_search_service import HotelSearchService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common.slack_alert import SlackAlertService as slack_alert
from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)


class HotelSearchApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    hotel_search_service = HotelSearchService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            hotel_search_dto = HotelSearchDTO(data=request.data)
            if not hotel_search_dto.is_valid():
                logger.error("hotel search invalid request %s ", request.data)
                return api_response.invalid_request_error_response(
                    hotel_search_dto.errors, HotelSearchApi.__name__)
            logger.info(
                "HotelSearchApi request %s  ",
                json.dumps(
                    hotel_search_dto.data))
            hotel = self.hotel_search_service.get_hotel_near_by(
                hotel_search_dto.data)
            logger.info("got hotel %s  ", hotel)
            hotel_data = {}
            wifi_location_data = {}
            is_bhaifi = False
            if hotel:
                is_bhaifi = True
                hotel_repository = RepositoriesFactory.get_hotel_repository()
                #hotel_data = json.loads(jsonpickle.encode(hotel))
                hotel_data = hotel_repository.get_minimum_serialized_data(
                    hotel)
                #hotel_data = MinimalDetailsHotelSerializer(instance=hotel).data
                #hotel_data = hotel.__dict__
                wifilocationmap = hotel.wifilocationmap
                wifi_location_data = WifiLocationSerializer(
                    instance=wifilocationmap).data
            response = api_response.prep_success_response({
                "is_bhaifi": is_bhaifi,
                "hotel_details": hotel_data,
                "wifi_location_data": wifi_location_data
            })
        except TreeboException as e:
            logger.exception("treebo hotel search exception ")
            response = api_response.treebo_exception_error_response(e)

        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.POST,
                                                        message=e.__str__(), class_name=self.__class__.__name__)

            logger.exception("internal error hotel search exception")
            response = api_response.internal_error_response(error_codes.get(
                error_codes.HOTEL_WIFI_INFO_ERROR)['msg'], HotelSearchApi.__name__)
        return response
