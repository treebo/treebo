import logging
from datetime import datetime
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.discounts.exceptions import HotelAutoApplyCouponOverlapException, InvalidCouponException
from apps.discounts.services.discount_coupon_services import DiscountCouponServices
from django.db.models import Q
from apps.discounts.models import HotelCouponMapForAutoApply, HotelCouponMapForAutoApplyUploadLog
from apps.discounts.constants import SUCCESS, FAILURE
from dbcommon.models.hotel import Hotel
from django.conf import settings

logger = logging.getLogger(__name__)


class HotelCouponAutoApplyService(object):
    @staticmethod
    def get_active_coupons_basis_hotel_and_date_range(
            hotel_id, exclude_coupon_code, start_date, end_date):
        coupon_codes = HotelCouponMapForAutoApply.objects.values_list(
            'coupon_code', flat=True).filter(
            Q(
                hotel_id=hotel_id), Q(
                is_enabled=True), Q(
                    start_date__lte=start_date, end_date__gte=start_date) | Q(
                        start_date__lte=end_date, end_date__gte=end_date) | Q(
                            start_date__gt=start_date, end_date__lt=end_date)).exclude(
                                coupon_code=exclude_coupon_code)
        return coupon_codes

    @staticmethod
    def upsert_hotel_coupon_auto_apply(
            hotel,
            coupon_code,
            start_date,
            end_date,
            is_enabled,
            user):

        # date range sanity...
        if end_date < start_date:
            raise InvalidCouponException("The given date range is invalid")

        # let's check if a matching record exists

        hotel_coupon = HotelCouponMapForAutoApply.objects.filter(
            hotel=hotel,
            coupon_code=coupon_code,
            start_date=start_date,
            end_date=end_date).first()

        if not hotel_coupon:
            # no match found... let's create a new record
            hotel_coupon = HotelCouponMapForAutoApply()
            hotel_coupon.hotel = hotel
            hotel_coupon.coupon_code = coupon_code
            hotel_coupon.start_date = start_date
            hotel_coupon.end_date = end_date

        # checking if the coupon exists

        # imposing the hotel-coupon pair uniqueness
        if (is_enabled):
            # checking if the coupon exists for the given date range
            coupon_exists = DiscountCouponServices.check_coupon_validity_basis_date_range(
                coupon_code, start_date, end_date)
            if not coupon_exists:
                raise InvalidCouponException(
                    "Coupon not found for the given date range")

            overlapping_utm_coupons_applicable = DiscountCouponServices.get_overlapping_utm_coupons(
                coupon_code=coupon_code, start_date=start_date, end_date=end_date)
            if overlapping_utm_coupons_applicable:
                raise HotelAutoApplyCouponOverlapException(
                    "Overlapping coupon found: %s" % str(
                        overlapping_utm_coupons_applicable[0]))

        hotel_coupon.user = user
        hotel_coupon.is_enabled = is_enabled
        hotel_coupon.save()

    @staticmethod
    def get_hotel_coupons_basis_hotel_ids_and_utm_source(
            hotel_ids, utm_source, start_date, end_date):
        if not hotel_ids:
            return {}
        current_date = datetime.now().date()
        hotel_coupons = HotelCouponMapForAutoApply.objects.filter(
            hotel_id__in=hotel_ids,
            is_enabled=True,
            start_date__lte=start_date,
            end_date__gte=end_date)
        res_map = {}

        # retrieving a list of all the applicable coupons for the said utm
        # source
        utm_coupons_applicable = DiscountCouponServices.get_discount_coupons_from_utmsource_applicable_within_range(
            utm_source, start_date, end_date)
        if not utm_coupons_applicable:
            return res_map

        hotel_overlap_coupons_map = {}

        # creating the {coupon_code:hotel_id_list map
        for hotel_coupon in hotel_coupons:
            if hotel_coupon.coupon_code in utm_coupons_applicable:
                # checking if hotel id exists in the result map - raise an
                # alert if it does
                old_code = res_map.get(hotel_coupon.hotel_id)

                if old_code == hotel_coupon.coupon_code:
                    logger.warn(
                        "Duplicate coupon (%s) occurance found for the hotel id %s", old_code, str(
                            hotel_coupon.hotel_id))
                    continue

                if not old_code:
                    # adding the hotel-coupon pair to the result ONLY IF the
                    # hotel hasn't encountered overlapping coupons
                    if not hotel_overlap_coupons_map.get(
                            hotel_coupon.hotel_id):
                        res_map[hotel_coupon.hotel_id] = hotel_coupon.coupon_code
                    else:
                        hotel_overlap_coupons_map[hotel_coupon.hotel_id].append(
                            hotel_coupon.coupon_code)
                else:
                    # if conflicting coupons found, no coupon will be auto
                    # applied
                    del res_map[hotel_coupon.hotel_id]
                    # for further processing, adding the hotel to the blacklist
                    hotel_overlap_coupons_map[hotel_coupon.hotel_id] = [
                        old_code, hotel_coupon.coupon_code]

        # Applying PERFECTSTAY coupon for all hotels redirecting from gha and not having aa code
        # AA code price Will be visible only on web and not on gha listing
        #if source != "external" and utm_source == settings.GOOGLE_ADS_UTM_SOURCE:
        #    for hotel_id in hotel_ids:
        #        if not res_map.get(hotel_id):
        #            res_map[int(hotel_id)] = "PERFECTSTAY"

        # TODO: raise slack alert async
        if hotel_overlap_coupons_map:
            logger.error(
                "got overlapping coupons on hotels: %s",
                str(hotel_overlap_coupons_map))
        return res_map

    @staticmethod
    def insert_hotel_coupon_for_auto_apply_upload_log(
            row_attempted, is_success=True, error_message=''):
        hotel_coupon_for_auto_apply_upload_log = HotelCouponMapForAutoApplyUploadLog()
        hotel_coupon_for_auto_apply_upload_log.update_attempted = row_attempted
        hotel_coupon_for_auto_apply_upload_log.error_message = error_message
        hotel_coupon_for_auto_apply_upload_log.status = SUCCESS if is_success else FAILURE
        hotel_coupon_for_auto_apply_upload_log.save()
