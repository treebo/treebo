# -*- coding: utf-8 -*-
from apps.common.data_classes.base_data_class import BaseDataClass
from apps.common.data_classes.container import Container
from .sku_category import RoomSku


class RoomType(BaseDataClass):
    sku = RoomSku

    def __init__(self, uid, name, max_occupancy):
        self.uid = uid
        self.name = name.lower()
        self.max_occupancy = int(max_occupancy or 0)

    def __repr__(self):
        return "<{kls}: ({u}: {n})>".format(kls=self.__class__.__name__,
                                            u=self.uid,
                                            n=self.name,
                                            )

    def __str__(self):
        return self.__repr__()

    def __eq__(self, other):
        if isinstance(other, RoomType):
            return hash(self) == hash(other)
        return NotImplemented

    def __hash__(self):
        return hash((self.uid, self.name,))


class RoomTypes(Container):

    def get_from_name(self, name):
        try:
            return [item for item in self.items if item.name == name][0]
        except IndexError:
            raise ValueError("Invalid room type: {r}".format(r=name))
