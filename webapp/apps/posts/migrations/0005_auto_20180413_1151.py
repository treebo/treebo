# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0004_question_page'),
    ]

    operations = [
        migrations.AlterField(
            model_name='qamapping',
            name='page_type',
            field=models.CharField(max_length=100, choices=[(b'', b''), (b'CITY', b'CITY'), (b'LANDMARK', b'LANDMARK'), (b'CATEGORY', b'CATEGORY'), (b'LOCALITY', b'LOCALITY'), (b'HD', b'HD'), (b'CITY-AMENITY', b'CITY-AMENITY'), (b'LOCALITY-CATEGORY', b'LOCALITY-CATEGORY'), (b'LANDMARK-CATEGORY', b'LANDMARK-CATEGORY')]),
        ),
    ]
