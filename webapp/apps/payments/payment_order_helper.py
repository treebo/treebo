from apps.bookings.models import PaymentOrder
from apps.common.exceptions.custom_exception import PaymentOrderAlreadyPresent
import logging

logger = logging.getLogger(__name__)


class PaymentOrderHelper():
    def create_payment_order_from_paymentdetails(
            self, amount, gateway, payment_details, status, booking_order_id):
        ps_order_id = payment_details.ps_order_id if payment_details else ''
        pg_payment_id = payment_details.ps_payment.pg_payment_id if payment_details else ''
        pg_order_id = payment_details.pg_order_id if payment_details else ''
        ps_payment_id = payment_details.ps_payment.ps_payment_id if payment_details else ''
        booking_order_id = booking_order_id
        payment_order = self.create_payment_order(
            ps_order_id,
            pg_payment_id,
            pg_order_id,
            ps_payment_id,
            status,
            gateway,
            amount,
            booking_order_id)
        return payment_order

    def create_payment_order(
            self,
            ps_order_id,
            pg_payment_id,
            pg_order_id,
            ps_payment_id,
            status,
            gateway_type,
            amount,
            booking_order_id):
        logger.info(
            "create_payment_order ps_order_id (%s) values (%s %s %s %s %s)",
            ps_order_id,
            pg_order_id,
            gateway_type,
            booking_order_id,
            amount,
            ps_payment_id)
        if ps_order_id:
            try:
                PaymentOrder.objects.get(ps_order_id=ps_order_id)
                raise PaymentOrderAlreadyPresent()
            except PaymentOrder.DoesNotExist as e:
                logger.error(
                    "payment order not present (%s) ",
                    ps_order_id)
                payment_order = PaymentOrder(ps_order_id=ps_order_id)
        else:
            payment_order = PaymentOrder()
            logger.info(
                "ps order id is null for gateway %s amount %s orderid %s ",
                gateway_type,
                amount,
                booking_order_id)
        payment_order.pg_order_id = pg_order_id
        payment_order.order_id = pg_payment_id or ''
        payment_order.ps_payment_id = ps_payment_id
        payment_order.status = status
        payment_order.gateway_type = gateway_type
        payment_order.amount = amount
        payment_order.booking_order_id = booking_order_id
        payment_order.updated_in_booking_service = False
        payment_order.save()
        return payment_order

    def update_orderid_in_paymentorder(self, paymentorder, booking_order_id):
        if paymentorder:
            paymentorder.booking_order_id = booking_order_id
            paymentorder.save()

    def update_status_in_paymentorder(self, paymentorder, status):
        if paymentorder:
            paymentorder.status = status
            paymentorder.save()

    def get_payment_order(self, ps_orderid):
        payment_order = PaymentOrder.objects.get(ps_order_id=ps_orderid)
        return payment_order
