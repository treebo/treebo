import logging
from apps.featuregate.manager import FeatureManager

logger = logging.getLogger(__name__)


class FeatureManagerService():

    def get_overridden_values(self, feature_data):
        context = {}
        try:
            featureManager = FeatureManager.feature_manager_factory(
                feature_data)
            overridden_values = featureManager.get_overriding_values()
            return overridden_values
        except Exception as exc:
            logger.exception(exc)
            return None
