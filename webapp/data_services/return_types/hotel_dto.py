from data_services.return_types.dto import DTO


class HotelDto(DTO):

    def __init__(self, properties_dict):
        super(HotelDto, self).__init__(properties_dict)

    def get_showcased_image_url(self):
        return self.showcased_image_url

    @property
    def get_location_url(self):
        return self.location_url

    def get_consumer_key(self):
        return self.consumer_key

    def get_consumer_secret(self):
        return self.consumer_secret

    def address(self):
        return self.address

    def get_city_name(self):
        return self.city_name

    def get_sitemap_url(self):
        return self.sitemap_url

    def get_hotel_canonical_url(self):
        return self.hotel_canonical_url