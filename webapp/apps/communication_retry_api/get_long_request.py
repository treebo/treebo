import logging

import time
from rest_framework.response import Response
from apps.checkout.tasks import send_booking_confirmation_notification
from base.views.api import TreeboAPIView


logger = logging.getLogger(__name__)


class LongRequest(TreeboAPIView):

    def get(self, request, *args, **kwargs):
        num_seconds = request.GET.get('num')
        time.sleep(float(num_seconds))
        return Response(status=200)
