import logging

from django.conf import settings

from apps.seo.models import FeatureToggle
from apps.seo.services.common_services.hit_url import URLBaseClass
from apps.seo.services.guest_love_data.update_hotel_positives_service import UpdateHotelsPositives
from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)


class FetchHotelsPositives(object):
    def get_all_hotels_positive(self):
        hotel_positive = UpdateHotelsPositives()
        try:
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            hotels = hotel_repository.get_active_hotel_list()
            config = FeatureToggle.objects.filter().first()
            if config:
                for hotel in hotels:
                    logger.info(
                        "calling prowl api for %s %s %s",
                        hotel.name,
                        hotel.hotelogix_id,
                        hotel.id)
                    hotel_data = self.get_response_from_prowl_api(
                        hotel, config)
                    if hotel_data:
                        sorted_dict = dict(
                            sorted(
                                list(
                                    hotel_data.items()), key=lambda k: int(
                                    k[1]), reverse=True)[
                                :config.num_of_positives_shown])
                        hotel_positive.update_hotel_positive_feedback(
                            hotel_id=hotel.id, feedback_data=sorted_dict)
            else:
                logger.error(
                    "No config is present to fetch the 'things guest loves'")
        except Exception:
            logger.exception(
                "Exception in fetching the hotels postivies for all hotels")

    def get_response_from_prowl_api(self, hotel, config):
        hotel_data = None
        try:
            num_of_days = config.num_of_days
            url = settings.PROWL_HOTEL_POSITIVES_DATA_URL.format(
                str(hotel.hotelogix_id), str(num_of_days))
            headers = {
                "USERNAME": config.username,
                "PASSWORD": config.password,
            }
            url_response = URLBaseClass()
            response_json = url_response.get_response(url=url, headers=headers)
            if response_json.get('status').lower() == 'success':
                logger.info(
                    "Success in fetching positives for hotel %s",
                    hotel.id)
                if response_json.get('data') and len(
                        response_json.get('data')):
                    hotel_data = response_json.get('data')

        except Exception:
            logger.exception(
                "Exception in fetching the positives data from prowl url for hotel %s %s",
                hotel.id,
                hotel.hotelogix_id)
        logger.info(
            "hotel data from prowl api for hotel %s - data %s",
            hotel.id,
            hotel_data)
        return hotel_data
