import logging
from random import randint

from django.core.cache import cache
from rest_framework.response import Response
from collections import OrderedDict

from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from data_services.exceptions import NoImageForHotelExist
from apps.hotels.serializers.hotel_image_serializer import HotelImageRequestSerializer
from apps.hotels.constants import RoomType, Category, LEAST_PRIORITY_ROOM_INDEX
from dbcommon.models.miscellanous import ImagesPerCategory

logger = logging.getLogger(__name__)

image_repository = RepositoriesFactory.get_image_repository()


class HotelImages(TreeboAPIView):
    CACHE_TIMEOUT = 60 * 60 * 24 + randint(0, 20)
    CACHE_KEY = 'cs_apicache_hotel_images_v1_{0}'

    def get_cache_key(self, hotel_id):
        return HotelImages.CACHE_KEY.format(hotel_id)

    @staticmethod
    def invalidate_cache(hotel_id=''):
        logger.info("Invalidating cache")
        if hotel_id:
            cache.delete(HotelImages.CACHE_KEY.format(hotel_id))
        else:
            cache.delete_pattern(HotelImages.CACHE_KEY.format('*'))

    def get(self, request, *args, **kwargs):
        """
        Gives All Images for a Hotel, Sorted by Category
        """
        hotel_id = int(kwargs.get('hotel_id'))

        try:
            result = cache.get(self.get_cache_key(hotel_id))
        except Exception as e:
            result = None
        if result:
            return Response(result)

        try:
            images = HotelImageRequestSerializer(
                image_repository.get_images_by_hotel_id_with_room(hotel_id=hotel_id),
                many=True).data
        except NoImageForHotelExist:
            zero_images = {}
            return Response(zero_images)

        images_information = {}
        for image in images:
            images_information[image['category']] = images_information.get(image['category'], [])
            images_information[image['category']].append(image)

        if 'Room' in images_information:
            images_information['Room'].sort(key=lambda room: RoomType.ROOM_TYPE_ORDER.index(
                room['room_type_code'])
                if room['room_type_code'] in RoomType.ROOM_TYPE_ORDER else
                LEAST_PRIORITY_ROOM_INDEX)

        if 'Washroom' in images_information:
            images_information['Washroom'].sort(key=lambda washroom: RoomType.ROOM_TYPE_ORDER.index(
                washroom['room_type_code'])
                if washroom['room_type_code'] in RoomType.ROOM_TYPE_ORDER else
                LEAST_PRIORITY_ROOM_INDEX)

        category_ranking = {category['category']: category['ranking'] if category['ranking'] else 0
                            for category in ImagesPerCategory.objects.
                            filter(hotel_id=hotel_id).values('category', 'ranking')}

        images_information = OrderedDict(sorted(images_information.items(),
                                                key=lambda category: category_ranking.
                                                get(category[0],
                                                    Category.CATEGORY_ORDER.get(category[0]) or
                                                    Category.CATEGORY_ORDER.get('OTHER_AMENITIES'))
                                                ))
        cache.set(self.get_cache_key(hotel_id), images_information,
                  HotelImages.CACHE_TIMEOUT)
        return Response(images_information)
