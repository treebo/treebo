from apps.reviews.models import HotelTAOverallRatings
from apps.bookings.models import Booking
from apps.content.models import ContentStore
from data_services.respositories_factory import RepositoriesFactory
from data_services.hotel_repository import HotelRepository


class CategoriesModelServices():

    # def fetch_hotel_from_hotel_id(self, hotel_id):
    #     hotels = hotel_repository.filter_hotels(id=hotel_id)
    #     hotel = hotels[0] if hotels else None
    #     return hotel

    def fetch_frequently_booked_date_delta(self):
        frequently_booked_date_delta = 6
        data = ContentStore.objects.filter(name='frequently_booked_date_delta')
        if data:
            frequently_booked_date_delta = int(data.first().value)
        return frequently_booked_date_delta

    def fetch_rating(self, hotel_id):
        rating = HotelTAOverallRatings.objects.filter(
            hotel_ta_mapping__hotel__id=hotel_id).first()
        if rating:
            return rating.overall_rating
        return 0

    def fetch_bookings_in_past_n_days(self, date_then, hotel_ids):
        bookings_in_past_n_days = Booking.objects.filter(
            created_at__gte=date_then, hotel_id__in=hotel_ids)
        return bookings_in_past_n_days

    def fetch_frequently_booked_threshold(self):
        frequently_booked_threshold = 7
        data = ContentStore.objects.filter(name='frequently_booked_threshold')
        if data:
            frequently_booked_threshold = int(data.first().value)
        return frequently_booked_threshold

    def fetch_value_for_booking_threshold(self):
        value_for_booking_threshold = 6
        data = ContentStore.objects.filter(name='value_for_booking_threshold')
        if data:
            value_for_booking_threshold = int(data.first().value)
        return value_for_booking_threshold
