__author__ = 'sourabh.singh'

from django.apps import AppConfig
from django.db.models.signals import post_migrate
from apps.hms.send_notification import notify_hms

default_app_config = 'apps.bookings.BookingsAppConfig'


class BookingsAppConfig(AppConfig):
    name = 'apps.bookings'

    def ready(self):
        post_migrate.connect(notify_hms, sender=self)
