from django.conf import settings
from kombu import Connection, Exchange, Queue
from kombu.pools import producers

from common.utilities.queue_utils import get_rabbit_mq_heartbeat_interval, get_rabbit_mq_retry_policy, \
    get_rabbit_mq_transport_options

connection = Connection(settings.PRICING_BROKER_URL,
                        heartbeat=get_rabbit_mq_heartbeat_interval(),
                        transport_options=get_rabbit_mq_transport_options())
model_exchange = Exchange("pricing_sync_exchange", type="direct")

routing_keys = {
    'state': 'state_sync',
    'hotel': 'hotel_sync',
    'tax': 'tax_sync',
    'price': 'price_sync',
    'promo': 'promo_sync',
    'promo_deletion': 'promo_deletion',
    'luxury_tax_sync':'luxury_tax_sync',
    'availability': 'availability',
}


def publish(message_type, payload):
    """
    :param message_type:
    :param payload:
    """
    routing_key = routing_keys.get(message_type)
    with producers[connection].acquire(block=True) as producer:
        producer.publish(payload,
                         exchange=model_exchange,
                         routing_key=routing_key,
                         declare=[model_exchange],
                         serializer='json',
                         retry=True,
                         retry_policy=get_rabbit_mq_retry_policy(),
                         compression='zlib')
