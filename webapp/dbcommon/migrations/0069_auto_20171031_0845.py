# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0068_auto_20170927_1040'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='categories',
            options={
                'ordering': ['name'],
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read')},
        ),
        migrations.AlterModelOptions(
            name='city',
            options={
                'ordering': ['name'],
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'City',
                'verbose_name_plural': 'Cities'},
        ),
        migrations.AlterModelOptions(
            name='landmark',
            options={
                'ordering': ['name'],
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Landmark',
                'verbose_name_plural': 'Landmarks'},
        ),
        migrations.AlterModelOptions(
            name='searchlandmark',
            options={
                'ordering': ['seo_url_name'],
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'Search Landmark',
                'verbose_name_plural': 'Search Landmarks'},
        ),
    ]
