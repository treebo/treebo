from unittest.case import TestCase

import django
import requests
from mock.mock import patch, MagicMock, Mock

from apps.discounts.views import CreateDiscount, UpdateDiscount, CreateBulkDiscounts, ApplicableDiscounts, \
    PriorityDiscount, \
    CheckApplicability, ApplyDiscount, AutoPromo


class RequestTest(TestCase):
    def setUp(self):
        django.setup()
        self.client1 = CreateDiscount()
        self.client2 = UpdateDiscount()
        self.client3 = CreateBulkDiscounts()
        self.client4 = ApplicableDiscounts()
        self.client5 = PriorityDiscount()
        self.client6 = CheckApplicability()
        self.client7 = ApplyDiscount()
        self.client8 = AutoPromo()

    @patch.object(CreateDiscount, "post")
    @patch('apps.discounts.models.DiscountCoupon.objects.filter')
    @patch('requests.post')
    def test_shouldCheckCreateDiscount(
            self, mock_rpost, mock_filter, mock_post):
        mock_response = Mock()
        expected_dict = {
            "status": "success",
            "data": {
                "coupon": "WELL",
                "status": "SUCCESS",
                "code": 200,
                "msg": "Discount Coupon Created"
            }
        }

        mock_response.json.return_value = expected_dict
        mock_filter.side_effect = ValueError
        mock_post.return_value = mock_response

        url = 'http://localhost:8000/discount/'
        data = {'code': 'WELL'}
        headers = {'Content-Type': 'application/json'}

        request = requests.post(url=url, data=data, headers=headers)
        responce = MagicMock()
        responce.post = self.client1.post(request)
        responce_dict = responce.post.json.return_value
        mock_post.assert_called_once_with(request)
        self.assertEqual(responce_dict, expected_dict)

    @patch.object(UpdateDiscount, "put")
    @patch('apps.discounts.models.DiscountCoupon.objects.filter')
    @patch('requests.put')
    def test_shouldCheckUpdateDiscount(
            self, mock_rput, mock_filter, mock_post):
        mock_response = Mock()
        expected_dict = {
            "status": "success",
            "data": {
                "coupon": "WELL",
                "status": "SUCCESS",
                "code": 200,
                "msg": "Discount Coupon Created"
            }
        }

        mock_response.json.return_value = expected_dict
        mock_filter.exist.return_value = True
        mock_post.return_value = mock_response

        url = 'http://localhost:8000/discount/update/10/'
        data = {'code': 'WELL'}
        headers = {'Content-Type': 'application/json'}

        request = requests.put(url=url, data=data, headers=headers)
        responce = MagicMock()
        responce.put = self.client2.put(request)
        responce_dict = responce.put.json.return_value
        mock_post.assert_called_once_with(request)
        self.assertEqual(responce_dict, expected_dict)

    @patch.object(UpdateDiscount, "delete")
    @patch('requests.delete')
    def test_shouldCheckDeactivateDiscount(self, mock_rdel, mock_post):
        mock_response = Mock()
        expected_dict = {
            "status": "success",
            "data": {
                "coupon": "WELL",
                "status": "SUCCESS",
                "code": 200,
                "msg": "Discount Coupon Created"
            }
        }

        mock_response.json.return_value = expected_dict
        mock_post.return_value = mock_response

        url = 'http://localhost:8000/discount/update/10/'
        data = {'code': 'WELL'}
        headers = {'Content-Type': 'application/json'}

        request = requests.delete(url=url, data=data, headers=headers)
        responce = MagicMock()
        responce.delete = self.client2.delete(request)
        responce_dict = responce.delete.json.return_value
        mock_post.assert_called_once_with(request)
        self.assertEqual(responce_dict, expected_dict)

    @patch.object(CreateBulkDiscounts, "post")
    @patch('apps.discounts.models.DiscountCoupon.objects.filter')
    @patch('requests.post')
    def test_shouldCheckCreateBulkDiscounts(
            self, mock_rpost, mock_filter, mock_post):
        mock_response = Mock()
        expected_dict = {
            "status": "success",
            "data": [
                {
                    "coupon": "HELLO",
                    "status": "FAILURE",
                    "code": 200,
                    "msg": "Discount Coupon Already Exist"
                },
                {
                    "coupon": "FELLOW",
                    "status": "FAILURE",
                    "code": 200,
                    "msg": "Discount Coupon Already Exist"
                }
            ]
        }

        mock_response.json.return_value = expected_dict
        mock_filter.exist.return_value = True
        mock_post.return_value = mock_response

        url = 'http://localhost:8000/discount/bulkdiscounts/'
        data = {'code': 'WELL'}
        headers = {'Content-Type': 'application/json'}

        request = requests.post(url=url, data=data, headers=headers)
        responce = MagicMock()
        responce.post = self.client3.post(request)
        responce_dict = responce.post.json.return_value
        mock_post.assert_called_once_with(request)
        self.assertEqual(responce_dict, expected_dict)

    @patch.object(ApplicableDiscounts, "get")
    @patch('apps.discounts.models.DiscountCoupon.objects.filter')
    @patch('requests.post')
    def test_shouldCheckApplicableDiscounts(
            self, mock_rget, mock_filter, mock_get):
        mock_response = Mock()
        expected_dict = {
            "status": "success",
            "data": {
                "status": "SUCCESS",
                "msg": "success",
                "code": 200,
                "applicable_discounts": [
                    "LO",
                    "LOW"
                ]
            }
        }

        mock_response.json.return_value = expected_dict
        mock_filter.exist.return_value = True
        mock_get.return_value = mock_response

        url = 'http://localhost:8000/discount/applicablediscounts/'
        data = {'code': 'WELL'}
        headers = {'Content-Type': 'application/json'}

        request = requests.post(url=url, data=data, headers=headers)
        responce = MagicMock()
        responce.get = self.client4.get(request)
        responce_dict = responce.get.json.return_value
        mock_get.assert_called_once_with(request)
        self.assertEqual(responce_dict, expected_dict)

    @patch.object(PriorityDiscount, "get")
    @patch('apps.discounts.models.DiscountCoupon.objects.filter')
    @patch('requests.post')
    def test_shouldCheckPriorityDiscount(
            self, mock_rget, mock_filter, mock_get):
        mock_response = Mock()
        expected_dict = {
            "status": "success",
            "data": {
                "code": 200,
                "terms": "",
                "coupon_type": "DISCOUNT",
                "discount": 0,
                "coupon_code": "LO",
                "coupon_message": "Discount value",
                "message": "Discount successfully applied"
            }
        }

        mock_response.json.return_value = expected_dict
        mock_filter.exist.return_value = True
        mock_get.return_value = mock_response

        url = 'http://localhost:8000/discount/prioritydiscount/'
        data = {'code': 'WELL'}
        headers = {'Content-Type': 'application/json'}

        request = requests.post(url=url, data=data, headers=headers)
        responce = MagicMock()
        responce.get = self.client5.get(request)
        responce_dict = responce.get.json.return_value
        mock_get.assert_called_once_with(request)
        self.assertEqual(responce_dict, expected_dict)

    @patch.object(CheckApplicability, "post")
    @patch('apps.discounts.models.DiscountCoupon.objects.filter')
    @patch('requests.post')
    def test_shouldCheckCheckApplicability(
            self, mock_rpost, mock_filter, mock_post):
        mock_response = Mock()
        expected_dict = {
            "status": "success",
            "data": {
                "coupon": "LO",
                "message": "SUCCESS",
                "code": 200
            }
        }

        mock_response.json.return_value = expected_dict
        mock_filter.exist.return_value = True
        mock_post.return_value = mock_response

        url = 'http://localhost:8000/discount/checkapplicability/'
        data = {'code': 'WELL'}
        headers = {'Content-Type': 'application/json'}

        request = requests.post(url=url, data=data, headers=headers)
        responce = MagicMock()
        responce.post = self.client6.post(request)
        responce_dict = responce.post.json.return_value
        mock_post.assert_called_once_with(request)
        self.assertEqual(responce_dict, expected_dict)

    @patch.object(ApplyDiscount, "post")
    @patch('apps.discounts.models.DiscountCoupon.objects.filter')
    @patch('requests.post')
    def test_shouldCheckApplyDiscount(
            self, mock_rpost, mock_filter, mock_post):
        mock_response = Mock()
        expected_dict = {
            "status": "success",
            "data": {
                "code": 200,
                "terms": [
                    ""
                ],
                "coupon_type": "DISCOUNT",
                "discount": 0,
                "coupon_message": "Discount value",
                "message": "Discount successfully applied"
            }
        }

        mock_response.json.return_value = expected_dict
        mock_filter.exist.return_value = True
        mock_post.return_value = mock_response

        url = 'http://localhost:8000/discount/applydiscount/'
        data = {'code': 'WELL'}
        headers = {'Content-Type': 'application/json'}

        request = requests.post(url=url, data=data, headers=headers)
        responce = MagicMock()
        responce.post = self.client7.post(request)
        responce_dict = responce.post.json.return_value
        mock_post.assert_called_once_with(request)
        self.assertEqual(responce_dict, expected_dict)

    @patch.object(AutoPromo, "get")
    @patch('apps.discounts.models.DiscountCoupon.objects.filter')
    @patch('requests.post')
    def test_shouldCheckAutoPromo(self, mock_rget, mock_filter, mock_get):
        mock_response = Mock()
        expected_dict = {
            "status": "success",
            "data": {
                "code": 200,
                "terms": "",
                "coupon_type": "DISCOUNT",
                "discount": 0,
                "coupon_code": "LO",
                "coupon_message": "Discount value",
                "message": "Discount successfully applied"
            }
        }

        mock_response.json.return_value = expected_dict
        mock_filter.exist.return_value = True
        mock_get.return_value = mock_response

        url = 'http://localhost:8000/discount/autopromo/'
        data = {'code': 'WELL'}
        headers = {'Content-Type': 'application/json'}

        request = requests.post(url=url, data=data, headers=headers)
        responce = MagicMock()
        responce.get = self.client8.get(request)
        responce_dict = responce.get.json.return_value
        mock_get.assert_called_once_with(request)
        self.assertEqual(responce_dict, expected_dict)
