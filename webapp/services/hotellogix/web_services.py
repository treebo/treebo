import datetime
import hmac
import logging
import re
from hashlib import sha1

import requests
from django.conf import settings
from pyquery import PyQuery as pQ

# Get an instance of a logger
from common.exceptions.treebo_exception import ExternalServiceException

logger = logging.getLogger(__name__)


class Hotelogix:
    invalidAccessKey = "Invalid Access Key."
    invalidDataCart = "Invalid data in cart."
    invalidSignature = "Invalid Signature"

    KEY_STRING = '####key####'

    @staticmethod
    def createSignature(method, xml, access_secret, consumer_secret):
        logger.info('Function [createSignature] called with args: [method=%s, xml=%s,\
                    access_secret=%s, key=%s]', method,
                    str(xml), access_secret, consumer_secret)

        if method == "wsauth":
            signature = hmac.new(str(consumer_secret), xml, sha1)
        else:
            signature = hmac.new(str(access_secret), str(xml), sha1)
        sig_hexdigest = signature.hexdigest()
        return signature.hexdigest()

    @staticmethod
    def wsauth(consumer_key):
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version='1.0'?>
					<hotelogix version='1.0' datetime='%(time)s'>
						<request method='wsauth' key='%(key)s'>
						</request>
					</hotelogix>
			""" % {'time': time, 'key': consumer_key}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def makeHotelogixCall(api, xml, signature):
        logger.info(
            'Function [makeHotelogixCall] called with args: [api: %s, xml: %s, signature: %s]',
            api,
            str(xml),
            signature)
        url = settings.HOTELOGIX_URL + api
        logger.info('HX URL: %s', url)
        # set what your server accepts
        headers = {'Content-Type': 'text/xml', 'X-HAPI-Signature': signature}
        response = requests.post(url, data=xml, headers=headers)
        logger.info(
            "Response XML for received for [api: %s]:\n%s",
            api,
            response.content)

        if not response.content:
            raise ExternalServiceException(
                "Empty response from HX for API=%s" % api)

        xml = str(response.text)
        # For Rate ID APi, strip spaces after rateid title, before returning
        # PyQuery Object.
        if api == 'rateidsforadtocart':
            import re
            xml = re.sub(
                r'(title=[^\s]*)\s*(Treebo Promotional)\s*',
                r'\1\2',
                xml)
        xml_response = pQ(xml)

        return xml_response

    @staticmethod
    def buildSearchXmlWithIgnore(
            checkindate,
            checkoutdate,
            adultCount,
            childCount,
            infantCount,
            roomRequired,
            limit,
            offset,
            hasResult):
        logger.info(
            'Function [buildSearchXmlWithIgnore] called with args: [checkInDate: %s, checkoutDate: %s, '
            'adultCount: %s, childCount: %s, infantCount: %s, roomRequired: %s, limit: %s, offset: %s, hasResult: %s]',
            checkindate,
            checkoutdate,
            adultCount,
            childCount,
            infantCount,
            roomRequired,
            limit,
            offset,
            hasResult)
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version='1.0'?>
                    <hotelogix version='1.0' datetime='%(time)s'>
                        <request method='search' key='%(key)s'>
                            <stay checkindate='%(checkin_date)s' checkoutdate='%(checkout_date)s'/>
                            <pax adult='%(adult_count)s' child='%(child_count)s' infant='%(infant_count)s'/>
                            <roomrequire value='%(room_required)s'/>
                            <ignorelists>
                                <bookingpolicy/>
                                <cancellationpolicy/>
                                <roomtypeimage/>
                                <amenity/>
                                <rateimage/>
                                <inclusion/>
                            </ignorelists>
                            <limit value='%(limit)s' offset='%(offset)s' hasResult='%(has_result)s'/>
                        </request>
                    </hotelogix>
            """ % {
            'time': time,
            'checkin_date': checkindate,
            'checkout_date': checkoutdate,
            'adult_count': str(adultCount),
            'child_count': str(childCount),
            'infant_count': str(infantCount),
            'room_required': str(roomRequired),
            'limit': str(limit),
            'offset': str(offset),
            'has_result': str(hasResult),
            'key': Hotelogix.KEY_STRING}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def parseAuthResponse(xml):
        d = pQ(xml)
        access_key = d("accesskey").attr("value")
        access_secret = d("accesssecret").attr("value")
        logger.info(
            'Parsed Auth Response for xml: %s -> [access_key: %s, access_secret: %s]',
            str(xml),
            access_key,
            access_secret)
        return access_key, access_secret

    @staticmethod
    def buildDetailsXml(
            checkindate,
            checkoutdate,
            adultCount,
            childCount,
            infantCount,
            roomRequired,
            limit,
            offset,
            hasResult,
            hotelogix_id):
        logger.info(
            'Function [buildDetailsXml] called with args: [checkInDate: %s, checkoutDate: %s, '
            'adultCount: %s, childCount: %s, infantCount: %s, roomRequired: %s, limit: %s, offset: %s, hasResult: %s, hotelogix_id: %s]',
            checkindate,
            checkoutdate,
            adultCount,
            childCount,
            infantCount,
            roomRequired,
            limit,
            offset,
            hasResult,
            hotelogix_id)
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version='1.0'?>
                    <hotelogix version='1.0' datetime='%(time)s'>
                        <request method='search' key='%(key)s'>
                            <stay checkindate='%(checkin_date)s' checkoutdate='%(checkout_date)s'/>
                            <pax adult='%(adult_count)s' child='%(child_count)s' infant='%(infant_count)s'/>
                            <roomrequire value='%(room_required)s'/>
                            <ignorelists>
                                <cancellationpolicy/>
                                <roomtypeimage/>
                                <amenity/>
                                <rateimage/>
                                <inclusion/>
                            </ignorelists>
                            <limit value='%(limit)s' offset='%(offset)s' hasResult='%(has_result)s'/>
                            <hotels>
                                <hotel id='%(hotelogix_id)s'/>
                            </hotels>
                        </request>
                    </hotelogix>
            """ % {
            'time': time,
            'checkin_date': checkindate,
            'key': Hotelogix.KEY_STRING,
            'checkout_date': checkoutdate,
            'adult_count': str(adultCount),
            'child_count': str(childCount),
            'infant_count': str(infantCount),
            'room_required': str(roomRequired),
            'limit': str(limit),
            'offset': str(offset),
            'has_result': str(hasResult),
            'hotelogix_id': hotelogix_id}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildLoadCart():
        logger.info('Function [buildLoadCart] called')
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version='1.0'?>
                    <hotelogix version='1.0' datetime='%(time)s'>
                        <request method='loadcart' key='%(key)s'>
                        </request>
                    </hotelogix>
            """ % {'time': time, 'key': Hotelogix.KEY_STRING}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def build_save_booking(
            order_id,
            name,
            email,
            phone,
            comments,
            hotel_reservation_id):
        logger.info(
            'Function [buildSaveBooking] called with args: [name: %s, email: %s, phone: %s, comments: %s, hotel_reservation_id: %s]',
            name,
            email,
            phone,
            comments,
            hotel_reservation_id)
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version='1.0'?>
                    <hotelogix version='1.0' datetime='%(time)s'>
                        <request method='savebooking' key='%(key)s'>
                            <orderId value="%(order_id)s"/>
                            <guest>
                                <fname>%(name)s</fname>
                                <lname></lname>
                                <email>%(email)s</email>
                                <phone></phone>
                                <mobile>%(phone)s</mobile>
                                <country code='IN'>India</country>
                                <state code='UP'></state>
                                <address>-</address>
                                <city>-</city>
                                <zip>-</zip>
                            </guest>
                            <hotels>
                                <hotel id='%(hotel_reservation_id)s'>
                                    <preference><![CDATA[%(comments)s]]></preference>
                                </hotel>
                            </hotels>
                        </request>
                    </hotelogix>
                """ % {
            "order_id": order_id,
            'time': time,
            'key': Hotelogix.KEY_STRING,
            'name': name,
            'email': email,
            'phone': phone,
            'hotel_reservation_id': hotel_reservation_id,
            'comments': comments}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildSaveBooking(name, email, phone, comments, hotel_reservation_id):
        logger.info(
            'Function [buildSaveBooking] called with args: [name: %s, email: %s, phone: %s, comments: %s, hotel_reservation_id: %s]',
            name,
            email,
            phone,
            comments,
            hotel_reservation_id)
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version='1.0'?>
                    <hotelogix version='1.0' datetime='%(time)s'>
                        <request method='savebooking' key='%(key)s'>
                            <guest>
                                <fname>%(name)s</fname>
                                <lname></lname>
                                <email>%(email)s</email>
                                <phone></phone>
                                <mobile>%(phone)s</mobile>
                                <country code='IN'>India</country>
                                <state code='UP'></state>
                                <address>-</address>
                                <city>-</city>
                                <zip>-</zip>
                            </guest>
                            <hotels>
                                <hotel id='%(hotel_reservation_id)s'>
                                    <preference><![CDATA[%(comments)s]]></preference>
                                </hotel>
                            </hotels>
                        </request>
                    </hotelogix>
            """ % {
            'time': time,
            'key': Hotelogix.KEY_STRING,
            'name': name,
            'email': email,
            'phone': phone,
            'hotel_reservation_id': hotel_reservation_id,
            'comments': comments}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildConfirmBooking(payment_amount, order_id, source):
        logger.info(
            'Function [buildConfirmBooking] called with args: [payment_amount: %s, order_id: %s, source: %s]',
            payment_amount,
            order_id,
            source)
        time = datetime.datetime.now().isoformat()

        xml = """<?xml version='1.0'?>
                    <hotelogix version='1.0' datetime='%(time)s'>
                        <request method='confirmbooking' key='%(key)s'>
                            <source><bookingchannel><companyName>%(source)s</companyName></bookingchannel></source>
                            <payment amount='%(payment_amount)s'/>
                            <orderId value='%(order_id)s'/>
                        </request>
                    </hotelogix>
            """ % {'time': time, 'payment_amount': payment_amount, 'order_id': order_id,
                   'key': Hotelogix.KEY_STRING, 'source': source}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildDeleteCart(item_id):
        logger.info(
            'Function [buildDeleteCart] called with args: [item_id: %s]',
            item_id)
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version='1.0'?>
                    <hotelogix version='1.0' datetime='%(time)s'>
                        <request method='deletefromcart' key='%(key)s'>
                            <itemId value='%(item_id)s'/>
                        </request>
                    </hotelogix>
            """ % {'time': time, 'item_id': item_id, 'key': Hotelogix.KEY_STRING}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildGetOrder(order_id):
        logger.info(
            'Function [buildGetOrder] called with args: [order_id: %s]',
            order_id)
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version='1.0'?>
                    <hotelogix version='1.0' datetime='%(time)s'>
                        <request method='getorder' key='%(key)s'>
                            <orderId value='%(order_id)s'/>
                        </request>
                    </hotelogix>
            """ % {'time': time, 'order_id': order_id, 'key': Hotelogix.KEY_STRING}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildAddToCart(itemSnippet):
        logger.info(
            'Function [buildAddToCart] called with args: [itemSnippet: %s]',
            str(itemSnippet))
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version='1.0'?>
                    <hotelogix version='1.0' datetime='%(time)s'>
                        <request method='addtocart' key='%(key)s'>
                            <items>%(itemSnippet)s</items>
                        </request>
                    </hotelogix>
            """ % {'time': time, 'itemSnippet': itemSnippet, 'key': Hotelogix.KEY_STRING}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildBasicSearch(
            checkinDate,
            checkoutDate,
            hotelogixIdList,
            maxGuest,
            maxChild):
        logger.info(
            'Function [buildBasicSearch] called with args: [checkinDate: %s, checkoutDate: %s, hotelogixIdList: %s, maxGuest: %s, maxChild: %s]',
            checkinDate,
            checkoutDate,
            hotelogixIdList,
            maxGuest,
            maxChild)
        hotelsList = """"""
        for hotelogixId in hotelogixIdList:
            hotelsList += """
                <hotel id='%(hotelogixId)s'
            """ % {'hotelogixId': hotelogixId}

        time = datetime.datetime.now().isoformat()
        xml = """

        <?xml version="1.0"?>
        <hotelogix version="1.0" datetime="%(time)s">
          <request method="basicratesearch" key="%(key)s" languagecode="en">
            <stay checkindate="%(checkin)s" checkoutdate="%(checkout)s"/>
            <pax adult="%(maxGuest)s" child="%(maxChild)s" infant="0"/>
            <hotels>
                %(hotelsList)s
            </hotels>
          </request>
        </hotelogix>

        """ % {
            'time': time,
            'checkin': checkinDate,
            'checkout': checkoutDate,
            'hotelsList': hotelsList,
            'key': Hotelogix.KEY_STRING,
            'maxGuest': maxGuest,
            'maxChild': maxChild}

        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildAddToCartItemSnippet(itemid):
        logger.info(
            'Function [buildAddToCartItemSnippet] called with args: [itemid: %s]',
            itemid)
        xmlSnippet = "<itemid value='" + itemid + "'/>"
        return xmlSnippet

    @staticmethod
    def buildPromoAmountXml(xml, totalPrice):
        logger.info(
            'Function [buildPromoAmountXml] called with args: [xml: %s, totalPrice: %s]',
            str(xml),
            totalPrice)
        bookings = xml('booking')
        hotelid = xml('hotel').eq(0).attr('id')
        reservation_string = ''
        for bookingpq in bookings:
            booking = pQ(bookingpq)
            roomAmount = totalPrice / len(bookings)
            amount = "{0:.6f}".format(roomAmount)
            reservation_string += """<reservation id="%(reservation_id)s">
                                                        <base amount="%(total_payable_amount)s" inclusiveoftax="0"/>
                                            </reservation>""" % {
                'reservation_id': booking.attr('id'),
                'total_payable_amount': amount}
        time = datetime.datetime.now().isoformat()

        """Request XMl for modifyamount """

        xml = """<?xml version="1.0"?>
                        <hotelogix version="1.0" datetime="%(time)s">
                            <request method="modifyamount" key="%(key)s" languagecode="en">
                                <hotels>
                                    <hotel id="%(hotelid)s">
                                        <reservations>
                                            %(reservation_string)s
                                        </reservations>
                                        </hotel>
                                </hotels>
                            </request>
                        </hotelogix>
                """ % {'time': time, 'hotelid': hotelid, 'key': Hotelogix.KEY_STRING,
                       'reservation_string': reservation_string}
        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildModifyAmount(xml, total_discount):
        logger.info(
            'Function [buildModifyAmount] called with args: [xml: %s, total_discount: %s]',
            str(xml),
            total_discount)
        bookings = xml('booking')
        hotel_id = xml('hotel').eq(0).attr('id')
        reservation_string = ''
        percentage_discount = grand_total = 0

        """ For calculation of grand total """
        for booking in bookings:
            rate = booking.find('rates/rate')
            tax = float(rate.get("tax"))
            total_amount = float(rate.get("price"))
            total_payable_amount = tax + total_amount
            grand_total += total_payable_amount

        if total_discount:
            percentage_discount = (float(total_discount) / grand_total) * 100

        """For reservation string """
        for booking in bookings:
            rate = booking.find('rates/rate')
            tax = float(rate.get("tax"))
            total_amount = float(rate.get("price"))
            total_payable_amount = (
                tax + total_amount) - ((percentage_discount * (tax + total_amount)) / 100)
            reservation_string += """<reservation id="%(reservation_id)s">
                                                    <base amount="%(total_payable_amount)s" inclusiveoftax="1"/>
                                        </reservation>""" % {'reservation_id': booking.get('id'),
                                                             'total_payable_amount': total_payable_amount}

        time = datetime.datetime.now().isoformat()

        """Request XMl for modifyamount """

        xml = """<?xml version="1.0"?>
                        <hotelogix version="1.0" datetime="%(time)s">
                            <request method="modifyamount" key='%(key)s' languagecode="en">
                                <hotels>
                                    <hotel id="%(hotel_id)s">
                                        <reservations>
                                            %(reservation_string)s
                                        </reservations>
                                    </hotel>
                                </hotels>
                            </request>
                        </hotelogix>
                """ % {'time': time, 'hotel_id': hotel_id, 'reservation_string': reservation_string,
                       'key': Hotelogix.KEY_STRING}

        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def buildcancelBooking(order_id, reservation_id):
        """
        :param order_id:
        :param reservation_id:
        :return:
        """
        logger.info(
            'Function [buildcancelBooking] called with args: [order_id: %s, reservation_id: %s]',
            order_id,
            reservation_id)
        time = datetime.datetime.now().isoformat()
        xml = """<?xml version="1.0"?>
                    <hotelogix version="1.0" datetime="%(time)s">
                        <request method="cancel" key='%(key)s' languagecode="en">
                            <orderId value="%(order_id)s"/>
                            <reservationId value="%(reservation_id)s"/>
                            <cancelCharge amount="0"/>
                            <cancelDescription>Booking has been cancelled</cancelDescription>
                        </request>
                    </hotelogix>
            """ % {'time': time, 'order_id': order_id, 'reservation_id': reservation_id,
                   'key': Hotelogix.KEY_STRING}

        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def search_xml(start_date, end_date):
        logger.info(
            'Function [search_xml] called with args: [start_date: %s, end_date: %s]',
            start_date,
            end_date)
        # current_time = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        current_time = datetime.datetime.utcnow().isoformat()

        xml = '''
            <?xml version="1.0" encoding="UTF-8"?>
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <soap:Header xmlns:htng="http://htng.org/PWSWG/2007/02/AsyncHeaders" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                    <wsa:MessageID>4ce4bec7-8fd2-e495-f023-82231bad9bd9</wsa:MessageID>
                    <htng:CorrelationID>4ce4bec7-8fd2-e495-f023-82231bad9bd8</htng:CorrelationID>
                    <wsa:To>https://partners.url.com</wsa:To>
                    <wsa:Action>http://hotelogix.local/ws/webv2/getreservations</wsa:Action>
                    <wsa:ReplyTo>
                        <wsa:Address>https://partners.url.com</wsa:Address>
                    </wsa:ReplyTo>
                    <htng:ReplyTo>
                        <wsa:Address />
                    </htng:ReplyTo>
                    <wsse:Security xmlns:wsu="http://schemas.xmlsoap.org/ws/2003/06/utility" soap:mustUnderstand="true">
                        <wsse:UsernameToken>
                            <wsse:Username>%(key)s</wsse:Username>
                            <wsse:Password />
                            <wsu:Created>%(current_time)s</wsu:Created>
                        </wsse:UsernameToken>
                    </wsse:Security>
                </soap:Header>
                <soap:Body>
                    <OTA_ReadRQ TimeStamp="%(current_time)s" Version="1">
                        <ReadRequests>
                            <HotelReadRequest>
                                <SelectionCriteria Start="%(start_date)s" End="%(end_date)s" />
                            </HotelReadRequest>
                        </ReadRequests>
                    </OTA_ReadRQ>
                </soap:Body>
            </soap:Envelope>
        ''' % {'current_time': current_time, 'start_date': start_date, 'end_date': end_date,
               'key': Hotelogix.KEY_STRING}

        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def build_get_rate_id_xml_for_roomtype(
            checkin_date,
            checkout_date,
            hotelogix_id,
            room_type_code):
        logger.info(
            'Function [build_get_rate_id_xml_for_roomtype] called with args: [checkin_date: %s, checkout_date: %s, hotelogix_id: %s, room_type_code: %s]',
            checkin_date,
            checkout_date,
            hotelogix_id,
            room_type_code)
        time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        xml = """<?xml version="1.0"?>
        <hotelogix version="1.0" datetime="%(time)s">
            <request method="rateidsforadtocart" key="%(key)s" languagecode="en">
                <stay checkindate="%(checkin_date)s" checkoutdate="%(checkout_date)s"/>
                <hotels>
                    <hotel id="%(hotelogix_id)s">
                        <roomtypes>
                            <roomtype code="%(room_type_code)s"/>
                        </roomtypes>
                    </hotel>
                </hotels>
            </request>
        </hotelogix>""" % {'time': time,
                           'key': Hotelogix.KEY_STRING,
                           'checkin_date': checkin_date,
                           'checkout_date': checkout_date,
                           'hotelogix_id': hotelogix_id,
                           'room_type_code': room_type_code}

        xml = re.sub(r'\t|\n', '', xml)
        return xml

    @staticmethod
    def build_get_rate_id_xml_for_all_roomtype(
            checkin_date, checkout_date, hotelogix_id):
        logger.info(
            'Function [build_get_rate_id_xml_for_all_roomtype] with args: [checkin_date: %s, checkout_date: %s, hotelogix_id: %s]',
            checkin_date,
            checkout_date,
            hotelogix_id)
        time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        xml = """<?xml version="1.0"?>
        <hotelogix version="1.0" datetime="%(time)s">
            <request method="rateidsforadtocart" key="%(key)s" languagecode="en">
                <stay checkindate="%(checkin_date)s" checkoutdate="%(checkout_date)s"/>
                <hotels>
                    <hotel id="%(hotelogix_id)s">
                    </hotel>
                </hotels>
            </request>
        </hotelogix>""" % {'time': time,
                           'key': Hotelogix.KEY_STRING,
                           'checkin_date': checkin_date,
                           'checkout_date': checkout_date,
                           'hotelogix_id': hotelogix_id}

        xml = re.sub(r'\t|\n', '', xml)
        return xml
