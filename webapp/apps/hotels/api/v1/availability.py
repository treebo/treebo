import logging

from django.conf import settings
from rest_framework import serializers
from rest_framework.response import Response
from web_sku.availability.services.availability_service import AvailabilityService

from apps.bookingstash.service.availability_service import get_availability_service
from apps.common import error_codes as codes
from apps.common.error_codes import get as get_error_msg
from apps.search.services.sku_search_cache_service import SkuSearchCacheService
from base.views.api import TreeboAPIView
from common.services.feature_toggle_api import FeatureToggleAPI
from data_services.exceptions import HotelDoesNotExist
from dbcommon.models.hotel import Hotel
from dbcommon.services.web_cs_transformer import WebCsTransformer

logger = logging.getLogger(__name__)


class HotelAvailabilitySerializer(serializers.Serializer):
    checkin = serializers.DateField(required=True)
    checkout = serializers.DateField(required=True)
    roomconfig = serializers.CharField(max_length=100, default='1-0', required=False)

    def validate(self, data):
        if data['checkout'] <= data['checkin']:
            raise serializers.ValidationError("Checkout date must be after checkin date.")
        return data


class HotelAvailability(TreeboAPIView):
    validationSerializer = HotelAvailabilitySerializer

    def __init__(self):
        self.web_cs_transformer = WebCsTransformer()
        self.availibility_service = AvailabilityService()
        self.cache_search = SkuSearchCacheService()

    def get(self, request, *args, **kwargs):
        """
        API to fetch availability for all rooms for the given hotel.
        checkin -- Checkin Date (Use format: yyyy-MM-dd)
        checkout -- Checkout Date (Use format: yyyy-MM-dd)
        roomconfig -- Room Configuration in format: 1-0,2-0,2-1 (For a-c, 'a' is adult count, and 'c' is children count)
        """

        validated_data = self.serializerObject.validated_data
        check_in = validated_data['checkin']
        check_out = validated_data['checkout']
        room_config = validated_data['roomconfig']

        hotel_id = kwargs["hotel_id"]
        logger.info("get hotel Availability for hotel: {} "
                    "with query params:{}".format(hotel_id, str(request.GET.dict())))
        try:
            Hotel.objects.get(id=hotel_id)
        except Hotel.DoesNotExist:
            return Response({'error': get_error_msg(
                codes.INVALID_HOTEL_ID)}, status=404)
        try:
            if FeatureToggleAPI.is_enabled(settings.DOMAIN, "pricing_revamp", False):
                available_rooms = self.get_sku_room_availability([int(hotel_id)], check_in, check_out,
                                                                 room_config)
                if available_rooms:
                    available = bool(sum(available_rooms.values()))
                else:
                    available = False
                room_types = [available_room[1] for available_room in available_rooms]
                room_count = [available_rooms.get(available_room) for available_room in available_rooms]
                overall_response = {
                    'available': available,
                    'availability': {}
                }
                for count in range(0, len(room_types)):
                    overall_response['availability'][room_types[count]] = room_count[count]
                return Response(overall_response)
            else:
                available_rooms = get_availability_service().get_available_rooms(
                    int(hotel_id), check_in, check_out, room_config)

                available = bool(sum(available_rooms.values()))

                return Response(
                    {
                        'available': available,
                        'availability': {
                            room.room_type_code: availability for room,
                                                                  availability in list(
                            available_rooms.items())},
                    })
        except BaseException as e:
            logger.error("Exception in fetching available rooms for hotel_id {0}, "
                         "check_in_date {1}, check_out_date {2} and room_config {3}".
                         format(hotel_id, check_in, check_out, room_config), exc_info=e)
            return Response({'error': get_error_msg(
                codes.INVALID_ROOM_CONFIG)}, status=400)

    def get_sku_room_availability(self, hotel_ids, checkin_date, checkout_date, room_config):
        cs_id_map = self.web_cs_transformer.get_hotel_cs_ids_from_web_ids(hotel_ids).get('live')
        cs_ids = []
        room_availability_per_hotel = {}
        available_hotels_dict = {}
        try:
            for hotel_id in cs_id_map:
                cs_ids.append(cs_id_map.get(hotel_id))
            if cs_ids:
                available_hotels_dict = self.cache_search.get_availability_data(cs_ids, checkin_date, checkout_date,
                                                                                room_config)
            if available_hotels_dict:
                for hotel_id in hotel_ids:
                    cs_id = cs_id_map.get(hotel_id)
                    sku_room_availibilty = available_hotels_dict.get(cs_id)
                    if sku_room_availibilty:
                        for sku_room_type in sku_room_availibilty:
                            room_count = sku_room_availibilty.get(sku_room_type)
                            room_availability_per_hotel.update({(hotel_id, str(sku_room_type)): room_count})
            else:
                room_availability_per_hotel = {}
        except Exception as err:
            print(err)

        return room_availability_per_hotel
