import json
import logging

from django.http import JsonResponse

from apps.hotels import utils as hotel_utils
from apps.hotels.serializers.room_serializer import RoomSerializer
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from data_services.room_repository import RoomRepository

logger = logging.getLogger(__name__)

room_data_service = RepositoriesFactory.get_room_repository()


class HotelRooms(TreeboAPIView):
    def get(self, hashedHotelId="", format=None):

        hotelId = hotel_utils.decode_hash(hashedHotelId)
        hotelRoomsList = self.__getRoomQuerySet(hotelId)
        roomsInformation, message = HotelRooms.getHotelRooms(hotelRoomsList)
        response = self.__getResponse(roomsInformation, message)

        return JsonResponse(response)

    @staticmethod
    def getHotelRooms(hotelRoomsList):

        result = []
        message = "Not a valid Room Object or No Rooms Present in the Hotel"
        try:
            serialData = RoomSerializer(hotelRoomsList, many=True)
            serialData = json.dumps(serialData.data)
            serialData = json.loads(serialData)

            for s in serialData:
                result.append(s)
                message = "success"
            logger.debug("room information is : " + str(result))
        except Exception as exc:
            logger.exception(exc)
            logger.debug(message)

        return result, message

    def __getResponse(self, roomsInformation, message):
        response = {'message': message, 'status': 0}

        if message == 'success':
            response['roomsInformation'] = roomsInformation
            status = 1
            response['status'] = status

        return response

    def __getRoomQuerySet(self, hotelId):
        allRooms = room_data_service.get_room_with_related_entities(
            select_list=None, prefetch_list=[
                'image_set', 'facility_set'], filter_params={
                'hotel_id': hotelId})
        # allRooms = Room.objects.filter(hotel_id=hotelId).order_by("-base_rate").prefetch_related(
        #     "image_set", "facility_set")
        allRooms = list(allRooms)
        allRooms.sort(key=lambda x: x.base_rate, reverse=True)
        return allRooms
