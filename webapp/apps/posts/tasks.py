__author__ = 'ansilkareem'

from dbcommon.models.hotel import Categories
from apps.content.constants import POSTS_CACHE_TIMEOUT

from webapp.apps.posts.request import FaqRequest
from webapp.apps.posts.service import PostService
from webapp.services.common_services.random_number_generator import RandomNumberGenerator
from celery import shared_task
from webapp.apps.content.constants import PAGES
from webapp.apps.hotels.service.hotel_service import HotelService
from webapp.data_services.respositories_factory import RepositoriesFactory
from django.core.cache import cache

hotel_repository = RepositoriesFactory.get_hotel_repository()
city_data_service = RepositoriesFactory.get_city_repository()
locality_repository = RepositoriesFactory.get_locality_repository()
landmark_repository = RepositoriesFactory.get_landmark_repository()
CACHE_KEY = 'posts_{0}'


def get_cache_key(page, attribute):
    cache_key = 'posts_' + page + '_' + attribute
    return cache_key


@shared_task
def submit_question(question, page, user):
    question = PostService.add_question(question, user, page, True)


@shared_task
def get_faqs(faq_request):
    if faq_request.page == PAGES.SEARCH:
        if faq_request.city:
            faq_request.page = PAGES.CITY
        elif faq_request.hotel:
            faq_request.page = PAGES.HD
        elif faq_request.landmark:
            faq_request.page = PAGES.LANDMARK
        elif faq_request.category:
            faq_request.page = PAGES.CATEGORY
        elif faq_request.locality:
            faq_request.page = PAGES.LOCALITY
    faqs = process_request(faq_request)
    return faqs


def process_hd_request(hd_request: FaqRequest):
    hotel = hotel_repository.get_hotel_by_id(hotel_id=hd_request.hotel)
    hotel_name = HotelService().get_display_name_from_hotel_name(
        hotel.name).title() if hotel else hd_request.hotel
    query = ','.join([hotel.name, hotel.city.slug]) if hotel else [hd_request.hotel]
    item_names = [hotel_name]
    post_service = PostService(PAGES.HD, query, item_names)
    hd_request.query = query
    hd_faqs = post_service.get_post_info(
        page=PAGES.HD,
        city_name=hotel.city.name if hotel else '',
        hotel_name=hotel_name, limit=False)
    return hd_faqs


def process_city_request(city_request):
    city = city_data_service.get_by_slug(city_request.city)
    query = city.slug if city else city_request.city
    item_names = [city.slug.title()] if city else [city_request.city.title()]
    city_name = city.name if city else city_request.city
    post_service = PostService(PAGES.CITY, query, item_names)
    city_faqs = post_service.get_post_info(page=PAGES.CITY, city_name=city_name,
                                           limit=False)
    return city_faqs


def process_category_request(category_request: FaqRequest):
    query = category_request.category
    city = query.split('-')[-1].strip('-')
    category_name = ' '.join(query.split('-')[:-1]).strip()
    category = Categories.objects.filter(
        status=1, name__iexact=category_name).first()
    if not category:
        query = ','.join([category_name, city])
        item_names = [category_name.title(), city.title()]
    else:
        city_search_args = [
            {'name': 'name', 'value': city, 'case_sensitive': False, 'partial': False},
            {'name': 'slug', 'value': city, 'case_sensitive': False, 'partial': False}]
        current_cities = city_data_service.filter_cities_by_string_matching(
            city_search_args, {'status': 1})
        if current_cities:
            city = current_cities[0]
        query = ','.join([category.name, city.slug])
        item_names = [category.name.title(), city.slug.title()]
        category_name = category.name
    post_service = PostService(PAGES.CATEGORY, query,
                               item_names)
    category_faqs = post_service.get_post_info(page=PAGES.CATEGORY,
                                               city_name=city,
                                               category_name=category_name,
                                               limit=False)
    return category_faqs


def process_landmark_request(landmark_request):
    landmark, landmark_name = landmark_repository.get_landmark_by_query(
        landmark_request.landmark)
    city = landmark.city if landmark else ''
    query = ','.join(
        [landmark.free_text_url if landmark.free_text_url else landmark.seo_url_name,
         city.slug]) if landmark else [landmark_name]
    item_names = [landmark.seo_url_name.title()] if landmark else [landmark_name.title()]
    post_service = PostService(PAGES.LANDMARK, query,
                               item_names)
    landmark_faqs = post_service.get_post_info(page=PAGES.LANDMARK,
                                               city_name=city.name if city else '',
                                               landmark_name=landmark.seo_url_name
                                               if landmark else landmark_request.landmark,
                                               limit=False)
    return landmark_faqs


def process_locality_request(locality_request):
    query = locality_request.locality
    city = query.split('-')[-1]
    locality_name = ' '.join(query.split('-')[:-1])
    locality = locality_repository.filter_localities_with_locality_name_and_city(locality_name,
                                                                                 city)
    if not locality:
        city_name = city.lower()
        query = ','.join([locality_name, city_name])
        item_names = locality_name.title()
    else:
        city = locality.city
        query = ','.join([locality.name, city.slug])
        item_names = locality.name.title()
        city_name = city.name
        locality_name = locality.name

    post_service = PostService(PAGES.LOCALITY, query, item_names)
    locality_faqs = post_service.get_post_info(page=PAGES.LOCALITY,
                                               city_name=city_name,
                                               locality_name=locality_name, limit=False)
    return locality_faqs


def process_request(faq_request: FaqRequest):
    query = None
    item_names = None
    cache_faqs = None
    timeout_value = POSTS_CACHE_TIMEOUT + RandomNumberGenerator.generate_number(1, 100)
    if faq_request.page == PAGES.HD:
        hotel_id = faq_request.hotel
        cache_key = get_cache_key(page=PAGES.HD, attribute=str(hotel_id))
        cache_faqs = cache.get(cache_key)
        if not cache_faqs:
            cache_faqs = process_hd_request(faq_request)
            cache.set(cache_key, cache_faqs, timeout_value)

    elif faq_request.page == PAGES.CITY:
        query = faq_request.city
        cache_key = get_cache_key(page=PAGES.CITY, attribute=query)
        cache_faqs = cache.get(cache_key)
        if not cache_faqs:
            cache_faqs = process_city_request(faq_request)
            cache.set(cache_key, cache_faqs, timeout_value)

    elif faq_request.page in [PAGES.LOCALITY, PAGES.LANDMARK, PAGES.CATEGORY]:
        query = faq_request.landmark or faq_request.locality or faq_request.category
        city = query.split('-')[-1]
        cache_attribute = ' '.join(query.split('-')[:-1])
        cache_key = get_cache_key(page=faq_request.page, attribute=cache_attribute + '_' + city)
        cache_faqs = cache.get(cache_key)
        if not cache_faqs:
            cache_faqs = process_landmark_request(
                faq_request) if faq_request.landmark else (
                process_locality_request(
                    faq_request) if faq_request.locality else process_category_request(faq_request))
            cache.set(cache_key, cache_faqs, timeout_value)

    cache_faqs['posts']['offset'] = faq_request.offset
    cache_faqs['posts']['limit'] = faq_request.limit
    cache_faqs['posts']['content'] = cache_faqs['posts']['content'][
                                     faq_request.offset:faq_request.limit + faq_request.offset]
    return cache_faqs
