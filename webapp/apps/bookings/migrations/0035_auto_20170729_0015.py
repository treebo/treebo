# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0034_asyncjobstatus'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymentorder',
            name='pg_order_id',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='paymentorder',
            name='ps_order_id',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
