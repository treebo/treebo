import django
from django.core.management import BaseCommand

from apps.seo.tasks import check_and_update_city_category_seo_if_hotel_unavailable, \
    check_and_update_locality_seo_if_hotel_unavailable, \
    check_and_update_landmark_seo_if_hotel_unavailable, \
    check_and_update_categories_seo_from_category_id
from dbcommon.models.hotel import Categories
from dbcommon.models.landmark import SearchLandmark
from dbcommon.models.location import Locality, City

django.setup()


class Command(BaseCommand):
    help = 'One time locality landmark category update command'

    def handle(self, *args, **options):
        self.update_localities_seo()
        self.update_landmark_seo()
        self.update_localities_categories_seo()
        self.update_categories_seo_from_category_id()

    def update_localities_seo(self):
        all_localities = Locality.objects.all()
        for locality in all_localities:
            check_and_update_locality_seo_if_hotel_unavailable(str(locality.id))

    def update_landmark_seo(self):
        all_landmarks = SearchLandmark.objects.all()
        for landmark in all_landmarks:
            check_and_update_landmark_seo_if_hotel_unavailable(str(landmark.id))

    def update_localities_categories_seo(self):
        all_cities = City.objects.filter(status=1, enable_for_seo=True)
        for city in all_cities:
            check_and_update_city_category_seo_if_hotel_unavailable(str(city.id))

    def update_categories_seo_from_category_id(self):
        categories = Categories.objects.all()
        for category in categories:
            check_and_update_categories_seo_from_category_id(str(category.id))
