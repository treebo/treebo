import random

from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from apps.auth.dto.pms_user_password_update_dto import PmsUserPasswordUpdateDTO
from apps.auth.pms_user import PmsUser
from apps.auth.services.pms_user_creation_service import PmsUserService
from base.views.api import TreeboAPIView


class PmsUserPasswordUpdateAPI(TreeboAPIView):
    """
            Create PMS User & Authz Mapping
            ---
            parameters:qw
                - name: first_name
                  description: First name of the user
                  required: true
                  type: string
                  paramType: form
                - name: last_name
                  description: Last name of the user
                  required: true
                  type: string
                  paramType: form
                - name: email
                  description: Email of the user
                  required: true
                  type: string
                  paramType: form
                - name: phone
                  description: Phone number of the user
                  required: true
                  type: string
                  paramType: form
                - name: email
                  description: Aom Email
                  required: false
                  type: string
                  paramType: form
            """

    def post(self, request):
        """

        :param rest_framework.request.Request request:
        :return:
        """
        try:
            pms_user_service = PmsUserService()
            pms_user_password_update_dto = PmsUserPasswordUpdateDTO(data=request.data)
            pms_user_password_update_dto.is_valid()
            pms_user = PmsUser(auth_id=None,
                               first_name=pms_user_password_update_dto.data['first_name'],
                               last_name=pms_user_password_update_dto.data['last_name'],
                               phone_number=pms_user_password_update_dto.data['phone_number'],
                               email=pms_user_password_update_dto.data['email'])
            pin = random.randint(1111, 9999)
            pms_user_service.update_and_send_pin(pin,
                                                 pms_user.first_name,
                                                 pms_user.phone_number,
                                                 pms_user.email)
            if pms_user_password_update_dto.data.get('aom_email', None):
                pms_user_service.send_details_to_aom(pms_user_password_update_dto.data['aom_email'],
                                                     pms_user, pin)
            return Response({"status": "SUCCESS"})
        except ValidationError as e:
            return Response({"error": {"msg": e.detail}}, status=400)
        except Exception as e:
            return Response({"error": {"msg": str(e)}}, status=500)
