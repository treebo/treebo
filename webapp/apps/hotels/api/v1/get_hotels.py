import json
import logging

from django.http import JsonResponse
from rest_framework.views import View

from apps.hotels import utils as hotel_utils
from apps.hotels.serializers.hotel_serializer import HotelSerializer
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class HotelInformation(View):
    hotelObjectList = []
    hotel_repository = RepositoriesFactory.get_hotel_repository()

    def get(
            self,
            request,
            hashedHotelId="",
            hashedCityId="",
            hashedStateId="",
            format=None):

        self.hotelObjectList = self.get_hotels(
            hashedHotelId, hashedCityId, hashedStateId)
        result, message = HotelInformation.getHotelInformation(
            self.hotelObjectList)
        response = self.__generateResponse(result, message)

        return JsonResponse(response)

    @staticmethod
    def getHotelInformation(hotels):
        result = []
        try:
            result = HotelSerializer(instance=hotels, many=True).data
            message = "success"
            logger.debug("hotel information is : " + str(result))
        except Exception as exc:
            message = "Not a valid Hotel Object or No Hotel Present in the list"
            logger.exception(exc)
            logger.debug(message)

        return result, message

    @staticmethod
    def getNumberOfHotels():
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        return hotel_repository.get_total_hotel_count(only_active=True)

    def __generateResponse(self, result, message):
        response = {'message': message, 'status': 0}

        if message == "success":
            response['hotels'] = result
            response['hotel_count'] = HotelInformation.getNumberOfHotels()
            response['status'] = 1

        return response

    def get_hotels(self, hashedHotelId="", hashedCityId="", hashedStateId=""):

        hotelObjectList = []

        if hashedHotelId != "":
            hotelId = hotel_utils.decode_hash(hashedHotelId)
            hotelObjectList = self.hotel_repository.filter_hotels(
                related_entities_list=['city'], pk=hotelId, status=1)
            #hotelObjectList = Hotel.objects.filter(pk=hotelId, status=1).select_related("city")

        elif hashedStateId == "" and hashedCityId == "":
            hotelObjectList = self.hotel_repository.filter_hotels(
                related_entities_list=['city'])
            #hotelObjectList = Hotel.objects.all().select_related("city")

        elif hashedStateId != "" and hashedCityId == "":
            stateId = hotel_utils.decode_hash(hashedStateId)
            hotelObjectList = self.hotel_repository.filter_hotels(
                related_entities_list=['city'], stateId=stateId, status=1)
            #hotelObjectList = Hotel.objects.filter(stateId=stateId, status=1).select_related("city")

        elif hashedStateId == "" and hashedCityId != "":
            cityId = hotel_utils.decode_hash(hashedCityId)
            hotelObjectList = self.hotel_repository.filter_hotels(
                related_entities_list=['city'], cityId=cityId, status=1)
            #hotelObjectList = Hotel.objects.filter(cityId=cityId, status=1).select_related("city")

        return hotelObjectList
