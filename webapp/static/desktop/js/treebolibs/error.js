
class TreeboError extends Error {
    constructor(data) {
        super(data.msg || data.message);
        this.code = data.code;
        this.redirect = data.redirect;
    }
}

module.exports = TreeboError;
