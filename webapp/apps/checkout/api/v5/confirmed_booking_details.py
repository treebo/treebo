import logging
from datetime import datetime

from django.conf import settings
from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response

from apps.bookings.api.v1.bookingstub import BookingsByOrderId
from apps.bookings.services.booking import BookingService
from apps.checkout.service.sku_discount_coupon_service import SkuDiscountCouponService
from apps.common import date_time_utils
from apps.common.error_codes import INVALID_ORDER_ID, INVALID_ORDER_ID_KEY, CONFIRM_BOOKING_FETCH_FAILED
from apps.common.error_codes import get as error_msg
from apps.content.models import ContentStore
from apps.growth.services.growth_services import GrowthServices
from apps.hotels.service.hotel_policy_service import HotelPolicyService
from apps.hotels.service.hotel_service import HotelService
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from base.views.api import TreeboAPIView
from common.services.feature_toggle_api import FeatureToggleAPI
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.data_services.external_provider_room_service import ExternalProvideRoomService
from apps.checkout.service.analytics.order_analytics import confirm_booking_analytics
from apps.bookings.models import PaymentOrder
from apps.checkout.models import BookingRequest
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.common.api_response import APIResponse as api_response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR
from django.conf import settings


__author__ = 'Neeraj Prasad'

logger = logging.getLogger(__name__)


class ConfirmedBookingDetail(TreeboAPIView):
    __booking_service = BookingService()

    def get(self, request, *args, **kwargs):
        try:
            order_id = kwargs["order_id"]
        except BaseException:
            return JsonResponse(
                {"error": error_msg(INVALID_ORDER_ID_KEY)}, status=400)

        try:
            booking_instance = BookingsByOrderId()
            response = booking_instance.get(request, orderId=order_id)
            if response.status_code == 404:
                return JsonResponse(
                    {"error": error_msg(INVALID_ORDER_ID)}, status=404)
            booking_dict = response.data
            booking_request = BookingRequest.objects.filter(booking_id=booking_dict['id']).first()
            payment_details = PaymentOrder.objects.filter(booking_order_id=order_id, status='CAPTURED').all()
            booking_dict_confirmpage = self.get_confirmationdata(
                request, booking_dict, order_id)
            booking_dict_confirmpage["order_id"] = order_id
            if payment_details and booking_request:
                booking_dict_confirmpage['order_event_args'] = confirm_booking_analytics(
                    request, booking_request, send_completed_order_event=True)

            try:
                if FeatureToggleAPI.is_enabled(settings.DOMAIN, "pricing_revamp", False):
                    if bool(booking_request.discount_id):
                        SkuDiscountCouponService.update_discount_coupon_in_pricing(booking_request)
            except Exception as e:
                slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                            message=e.__str__(),
                                                            dev_msg="Error in debiting coupon "
                                                                    "for hotel_id {} and coupon_code {} and order_id {} and discount_id {} and booking_id {} ".format(
                                                                str(booking_request.hotel_id),
                                                                str(booking_request.coupon_code),
                                                                str(booking_request.order_id),
                                                                str(booking_request.discount_id),
                                                                str(booking_request.booking_id)),
                                                            class_name=self.__class__.__name__)
                logger.exception(
                    "Unable to debit coupon with request data {} and error {} for order_id {}".format(
                        str(request.GET), str(e), str(booking_request.order_id)))

            logger.info("Response for order_id:{order_id} is {response}".format(order_id=order_id, response=booking_dict_confirmpage))
            return Response(booking_dict_confirmpage, status=status.HTTP_200_OK)

        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                message=e.__str__(), dev_msg="Error in fetching confirm booking "
                                "details for hotel_id {}".format(str(booking_request.hotel_id)),
                                class_name=self.__class__.__name__)
            logger.exception(
                "Unable to fetch confirm booking details with request data %s error %s ",
                request.GET, e.__str__())
            return api_response.error_response_from_error_code(
                CONFIRM_BOOKING_FETCH_FAILED,
                status_code=HTTP_500_INTERNAL_SERVER_ERROR,
                api_name=ConfirmedBookingDetail.__name__)

    def get_confirmationdata(self, request, booking_dict, order_id):
        hotel_name = booking_dict["hotel_name"]
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        myhotel = hotel_repository.get_hotel_by_name(hotel_name=hotel_name, related_entities_list=['image'])
        directions = hotel_repository.get_directions_set_for_hotel(myhotel)
        policies = HotelPolicyService.get_hotel_policies(myhotel.id)
        hotel_image_url = hotel_repository.get_showcased_image_url(myhotel)
        #hotel_image_url = hotel_image_obj.url if hotel_image_obj else ''
        booking_dict_confirmpage = {}
        booking_dict_confirmpage["user"] = booking_dict["user_detail"]
        booking_dict_confirmpage["guest"] = {
            "adults": booking_dict["room_config"]["no_of_adults"],
            "children": booking_dict["room_config"]["no_of_childs"]}

        booking_dict_confirmpage["hotel"] = {
            "name": HotelService().get_display_name_from_hotel_name(hotel_name),
            "city": booking_dict["hotel_detail"]["city"],
            "street": myhotel.street if myhotel and myhotel.street else "",
            "id": myhotel.id if myhotel and myhotel.id else None,
            "image_url": get_image_uncode_url(hotel_image_url),
            "coordinates": {
                "lat": myhotel.latitude if myhotel and myhotel.latitude else None,
                "lng": myhotel.longitude if myhotel and myhotel.latitude else None},
            'group_code': booking_dict["group_code"],
            "property": {
                "provider": myhotel.provider_name,
                "type": myhotel.property_type
            }
        }
        room_config = booking_dict["room_config_string"].split(',')
        config = []
        for rm in room_config:
            if rm != '':
                current_room = rm.split('-')
                config.append(
                    {"adults": int(current_room[0]), "children": int(current_room[1])})
        booking_dict_confirmpage["room"] = {
            "count": booking_dict["room_config"]["room_count"],
            "type": booking_dict["room_config"]["room_type"],
            "config": config}
        ExternalProvideRoomService.add_external_room_details_to_room_object(hotel_id=myhotel.id,
                                                                            room_dict=booking_dict_confirmpage["room"],
                                                                            room_type=booking_dict_confirmpage["room"]["type"])
        checkin_date = datetime.strptime(
            booking_dict["checkin_date"],
            date_time_utils.DATE_FORMAT).date()
        checkout_date = datetime.strptime(
            booking_dict["checkout_date"],
            date_time_utils.DATE_FORMAT).date()
        number_of_nights = (checkout_date - checkin_date).days

        booking_dict_confirmpage["date"] = {"checkin": checkin_date,
                                            "checkout": checkout_date}
        booking_dict_confirmpage["number_of_nights"] = number_of_nights

        booking_dict_confirmpage["price"] = self.get_price_component(
            booking_dict)

        booking_dict_confirmpage["pay_at_hotel"] = booking_dict["paid_at_hotel"]
        booking_dict_confirmpage["status"] = booking_dict["status"]
        booking_dict_confirmpage["booking_date"] = booking_dict["booking_date"]
        booking_dict_confirmpage["cancel_date"] = booking_dict["cancel_date"]
        directions_list = []
        for direction in directions:
            steps_list = direction.directions_for_email_website.split('\\n')
            directions_list.append({"landmark": {"name": direction.landmark_obj.name,
                                                 "coordinates": {
                                                     "lat": direction.landmark_obj.latitude if direction.landmark_obj and
                                                     direction.landmark_obj.latitude else None,
                                                     "lng": direction.landmark_obj.longitude if direction.landmark_obj and
                                                     direction.landmark_obj.latitude else None
                                                 },
                                                 "type": direction.landmark_obj.type},
                                    "steps": steps_list,
                                    })
        booking_dict_confirmpage["hotel"]["directions"] = directions_list
        booking_dict_confirmpage["hotel"]["hotel_policies"] = policies

        partpay_bookings_list = []
        partpay_booking_dict = {}
        partpay_booking_dict['order_id'] = order_id
        partpay_booking_dict['status'] = booking_dict["status"]
        partpay_booking_dict['audit'] = booking_dict["audit"]
        partpay_booking_dict['paid_at_hotel'] = booking_dict["paid_at_hotel"]
        partpay_booking_dict['payment_mode'] = booking_dict["payment_mode"]
        partpay_booking_dict['hotel_detail'] = {
            "hotel_code": booking_dict["hotel_detail"]["hotel_code"],
            "hotel_cs_id": booking_dict["hotel_detail"]["hotel_cs_id"]
        }

        partpay_bookings_list.append(partpay_booking_dict)
        growth_service = GrowthServices()
        booking_dict_confirmpage['partial_payment_link'], \
            booking_dict_confirmpage['partial_payment_amount'] = growth_service.booking_partpay_details(partpay_bookings_list)

        content_object = ContentStore.objects.filter(
            name="confirmation_page_partpay_msg", version=1).first()
        if content_object:
            booking_dict_confirmpage['partial_payment_message'] = str(
                content_object.value)
        else:
            booking_dict_confirmpage['partial_payment_message'] = ''

        return booking_dict_confirmpage

    def get_price_component(self, booking_dict):
        discount_amount = float(booking_dict["payment_detail"]["discount"])
        voucher_amount = float(
            booking_dict["payment_detail"]["voucher_amount"])
        discount = max(discount_amount, voucher_amount)
        if booking_dict["payment_detail"].get("member_discount"):
            discount = round(
                discount +
                float(
                    booking_dict["payment_detail"].get("member_discount")),
                2)
        sell_price = float(booking_dict["payment_detail"]["total"])
        wallet_deduction = float(booking_dict['wallet_deduction'])
        wallet_applied = booking_dict['wallet_applied']
        wallet_paid_amount = booking_dict['payment_detail']['wallet_payment_amount']
        treebo_points_discount = float(wallet_deduction) - float(wallet_paid_amount)
        selected_rate_plan_price = {
            "rate_plan": {
                "code": booking_dict["payment_detail"]["rate_plan"],
                "description": ""},
            "price": {
                "wallet_info": {
                     "wallet_applied": wallet_applied,
                     "wallet_deductable_amount": wallet_deduction,
                     "wallet_type": settings.WALLET_TYPE,

                },
                "treebo_points_discount": round(treebo_points_discount, 2),
                "net_payable_amount": round(float(sell_price) - float(
                    booking_dict["payment_detail"]["paid_amount"]), 2),
                "balance_amount": float(booking_dict["payment_detail"]["pending_amount"]),
                "paid_amount": float(booking_dict["payment_detail"]["paid_amount"]),
                "pretax_price": round(float(
                    booking_dict["payment_detail"]["room_price"]) +
                                      treebo_points_discount + discount_amount, 2),
                "final_pretax_price": float(
                    booking_dict["payment_detail"]["room_price"]),
                "tax": float(
                    booking_dict["payment_detail"]["tax"]),
                "sell_price": sell_price,
                "total_discount": discount,
            }}

        price = {
            'selected_rate_plan': selected_rate_plan_price
        }
        return price
