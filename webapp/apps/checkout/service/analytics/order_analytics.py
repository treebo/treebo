import logging
import traceback

from apps.checkout.service.analytics.constants import SegmentEvent
from apps.common import utils as commonUtils
from apps.common.utils import round_to_nearest_integer, get_number_of_days
from apps.pricing import dateutils
from data_services.respositories_factory import RepositoriesFactory
from webapp.data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)

hotel_repository = RepositoriesFactory.get_hotel_repository()


def confirm_booking_analytics(request, booking_request, send_completed_order_event=False):
    """
    request: django request object
    booking_request: booking request object
    :return:
    """
    try:
        hotel_obj = hotel_repository.get_hotel_by_id_from_web(
            hotel_id=booking_request.hotel_id)
        number_of_days = get_number_of_days(
            booking_request.checkout_date,
            booking_request.checkin_date)
        order_event_args = {
            "orderId": booking_request.order_id,
            "total": float(
                booking_request.total_amount),
            "revenue": float(
                booking_request.total_amount),
            "shipping": 0,
            "tax": float(
                booking_request.tax_amount),
            "discount": float(
                booking_request.discount_value),
            "coupon": booking_request.coupon_code,
            "email": booking_request.email,
            "country_code": booking_request.country_code,
            "phone": booking_request.phone,
            "userid": booking_request.user_id,
            "userId": booking_request.user_id,
            "currency": "INR",
            "payment_mode": booking_request.payment_mode,
            "products": [
                {
                    "id": hotel_obj.hotelogix_id,
                    "sku": hotel_obj.hotelogix_name,
                    "name": hotel_obj.hotelogix_name,
                    "price": float(
                        booking_request.total_amount),
                    "quantity": number_of_days *
                                booking_request.room_required,
                    "category": hotel_obj.city.name}],
            "hrental_id": booking_request.hotel_id,
            "hrental_startdate": dateutils.date_to_ymd_str(
                booking_request.checkin_date),
            "hrental_enddate": dateutils.date_to_ymd_str(
                booking_request.checkout_date),
            "hrental_totalvalue": float(
                round_to_nearest_integer(
                    booking_request.total_amount)),
            "hrental_totalvalue_paisa": float(round_to_nearest_integer(
                booking_request.total_amount) * 100)
        }
        if send_completed_order_event:
            commonUtils.trackConfirmOrder(
                request,
                SegmentEvent.COMPLETED_ORDER.value,
                order_event_args,
                channel=booking_request.booking_channel,
                anonymous_id=request.data.get(
                    'anonymousId',
                    None))
        return order_event_args
    except Exception as err:
        logger.debug(
            "__confirmOrderAnalytics Error %s " %
            traceback.format_exc())
        return {}


def pah_booking_complete_analytics(request, booking_request):
    """
    request: django request object
    booking_request: booking request object
    :return:
    """
    try:
        hotel_obj = hotel_repository.get_hotel_by_id_from_web(
            hotel_id=booking_request.hotel_id)
        number_of_days = get_number_of_days(
            booking_request.checkout_date,
            booking_request.checkin_date)
        order_event_args = {
            "booking_id": booking_request.order_id,
            "booker_name": booking_request.name,
            "booker_phone_number": booking_request.phone,
            "booker_email_id": booking_request.email,
            "hotel_name": hotel_obj.name,
            "booking_checkin_date": dateutils.date_to_ymd_str(
                booking_request.checkin_date),
            "booking_checkout_date": dateutils.date_to_ymd_str(
                booking_request.checkout_date),
            "no_of_rooms_booked": number_of_days *
                                  booking_request.room_required,
            "booking_final_amount": float(
                booking_request.total_amount)
        }
        commonUtils.trackConfirmOrder(
            request,
            SegmentEvent.ADWORDS_COMPLETED_ORDER_PAH.value,
            order_event_args,
            channel=booking_request.booking_channel,
            anonymous_id=request.data.get(
                'anonymousId',
                None))
        return order_event_args
    except Exception as err:
        logger.debug(
            "__pah_booking_complete_analytics Error %s " %
            traceback.format_exc())
        return {}
