from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_500_INTERNAL_SERVER_ERROR

from apps.common import error_codes
from common.exceptions.treebo_exception import TreeboException


class SoaError(Exception):
    def __init__(self, code, msg, status):
        self.code = code
        self.msg = msg
        self.status = status


class CheckinDateError(Exception):
    def __init__(self, message, status):
        self.status = status
        self.message = message


class RequestError(Exception):
    def __init__(self, message, status):
        self.status = status
        self.message = message


class NoPriceForRatePlan(TreeboException):
    def __init__(self, rate_plan):
        super(NoPriceForRatePlan, self).__init__()
        error_dict = error_codes.get(
            error_codes.PRICE_NOT_AVAILABLE_FOR_RATE_PLAN)
        self.message = error_dict['msg'].format(rate_plan)
        self.developer_message = ''
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error_dict['code'])


class PriceNotAvailableForHotel(TreeboException):
    def __init__(self, hotel_name):
        super(PriceNotAvailableForHotel, self).__init__()
        error_dict = error_codes.get(error_codes.PRICE_NOT_AVAILABLE_FOR_HOTEL)
        self.message = error_dict['msg'].format(hotel_name)
        self.developer_message = ''
        self.status_code = HTTP_400_BAD_REQUEST
        self.error_code = self.app_type + str(error_dict['code'])
