import logging
from rest_framework.response import Response

from base.views.api import TreeboAPIView
from dbcommon.event_publishers import publish_state
from dbcommon.models.location import State

logger = logging.getLogger(__name__)


class StateSyncTrigger(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        state_id = kwargs.get('state_id')
        logger.info("State Sync request received for State ID: %s", state_id)
        try:
            state = State.objects.get(pk=state_id)
            publish_state(state)
            return Response({"message": "State sync initiated."}, status=202)
        except BaseException:
            return Response(
                {"message": "Invalid State ID provided."}, status=400)
