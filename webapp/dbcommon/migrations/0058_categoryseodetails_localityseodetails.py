# -*- coding: utf-8 -*-


from django.db import models, migrations
import caching.base
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0057_auto_20170711_1710'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategorySeoDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('category_seo_title', models.CharField(max_length=500)),
                ('category_seo_description', models.TextField(null=True, blank=True)),
                ('category_seo_keyword', models.TextField(null=True, blank=True)),
                ('category', models.ForeignKey(to='dbcommon.Categories', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'hotels_categoryseodetails',
                'verbose_name': 'Cateogry SEO Details',
                'verbose_name_plural': 'Cateogry SEO Details',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
        migrations.CreateModel(
            name='LocalitySeoDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('locality_seo_title', models.CharField(max_length=500)),
                ('locality_seo_description', models.TextField(null=True, blank=True)),
                ('locality_seo_keyword', models.TextField(null=True, blank=True)),
                ('locality', models.ForeignKey(to='dbcommon.Locality', on_delete=django.db.models.deletion.CASCADE)),
            ],
            options={
                'default_permissions': ('add', 'change', 'delete', 'read'),
                'abstract': False,
                'db_table': 'hotels_localityseodetails',
                'verbose_name': 'Locality SEO Details',
                'verbose_name_plural': 'Locality SEO Details',
            },
            bases=(caching.base.CachingMixin, models.Model),
        ),
    ]
