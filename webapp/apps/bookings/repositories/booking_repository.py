"""
    Booking Client Repository
"""

from django.conf import settings

from apps.bookings.models import PaymentOrder, Booking
from common.custom_logger.booking.log import get_booking_logger

logger = get_booking_logger(__name__)


class BookingServiceRepository:

    @staticmethod
    def save_booking(booking):
        booking.save()

    @staticmethod
    def save_room_booking(room_booking):
        room_booking.save()

    @staticmethod
    def get_paymentorder_by_booking_order_id(booking_order_id):
        payment_order = PaymentOrder.objects.filter(
            booking_order_id=booking_order_id,
            gateway_type=settings.TREEBO_WALLET_GATEWAY).first()
        return payment_order

    @staticmethod
    def get_paymentorders_by_booking_order_id_except_wallet(booking_order_id):
        payment_orders = PaymentOrder.objects.filter(booking_order_id=booking_order_id).exclude(
            gateway_type=settings.TREEBO_WALLET_GATEWAY)
        return payment_orders

    @staticmethod
    def get_booking_with_hotel_by_id(booking_id):
        _ = Booking.objects.filter(pk=booking_id)
        booking = Booking.objects.select_related(
            'hotel').get(pk=booking_id)
        return booking

    @staticmethod
    def get_booking_with_hotel_by_order_id(order_id):
        _ = Booking.objects.filter(order_id=order_id)
        booking = Booking.objects.select_related(
            'hotel').get(order_id=order_id)
        return booking

    @staticmethod
    def update_booking_with_gst_details(order_id, organization_taxcode, organization_name, organization_address):
        rows_matched = Booking.objects.filter(
            order_id=order_id
        ).update(
            organization_taxcode=organization_taxcode,
            organization_name=organization_name,
            organization_address=organization_address)
        return rows_matched

    @staticmethod
    def get_booking_by_crs_booking_id(crs_booking_id):
        booking = Booking.objects.get(external_crs_booking_id=crs_booking_id)
        return booking

    @staticmethod
    def get_soft_booking_ids_by_created_at(threshold):
        soft_booking_ids = Booking.objects.filter(booking_status=Booking.TEMP_BOOKING_CREATED,
                                                  created_at__gte=threshold).values_list('external_crs_booking_id',
                                                                                         flat=True)

        return soft_booking_ids
