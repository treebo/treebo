import logging

from apps.reviews.models import TAAwardslist

logger = logging.getLogger(__name__)


class AwardService(object):
    def create_or_update_award(self, award_name, award_category=[]):
        try:
            award_list = TAAwardslist.objects.filter(award_name=award_name)
            if not award_list:
                logger.debug(
                    "Award  %s does not exist .. so create new",
                    award_name)
                award = TAAwardslist()
                award.award_name = award_name
                if award_category:
                    award_list = award_category
                    award.award_category = award_list
                award.save()
            elif award_category and award_category not in award_list.award_category:
                award_list.award_category.append(award_category)
                award_list.save()
            else:
                logger.info("Entry already exists with same award_name %s %s",
                            award_list.award_name, award_name)

        except Exception:
            logger.exception(
                "Exception in creating new entry in award table for %s",
                award_name)

    def fetch_award(self, award_name):
        award = TAAwardslist.objects.filter(award_name=award_name).first()
        if award:
            return award
        else:
            return None

    def delete_award(self, award_name):
        TAAwardslist.objects.filter(award_name=award_name).delete()
        logger.info("Deleted successfully for award %s", award_name)

    def is_award_enable(self, award_name):
        if TAAwardslist.objects.filter(
                award_name=award_name,
                is_enabled=True).exists():
            logger.info(" award %s enabled", award_name)
            return True
        return False
