# This is executed before every task. Here we can set the request_id and
# user_id in LogFilter
import errno
import socket
import sys
import traceback

from celery import shared_task
from celery.signals import task_prerun
from celery.utils.log import get_task_logger
from django.conf import settings

from base.filters import CeleryRequestIDFilter
from base.logging_decorator import log_args
from common.constants import common_constants as profile_const
from dbcommon.models.profile import User, UserDiscount
from services.notification.email.registration_email import RegistrationEmailService
from services.restclient.discountrestclient import DiscountClient

logger = get_task_logger(__name__)


@task_prerun.connect()
def set_request_id(
        signal=None,
        sender=None,
        task_id=None,
        task=None,
        args=None,
        **kwargs):
    logger.addFilter(
        CeleryRequestIDFilter(
            kwargs.get('user_id'),
            kwargs.get('request_id'),
            task_id,
            task.name))
    logger.info(
        "set_request_id called with kwargs: %s, and task_id: %s",
        kwargs,
        task_id)


@shared_task
@log_args(logger)
def send_registration_email(user_id):
    user = User.objects.get(pk=user_id)
    coupon_present = False
    unique_coupon_code = None

    if settings.SEND_SIGNUP_COUPON == 1:
        logger.debug("inside the send signup condition")
        try:
            # TODO: Code is broken. Remove requests from all service level code
            unique_coupon_code = __create_unique_coupon_code(user)
            DiscountClient.createCouponForUser(
                user.id,
                unique_coupon_code,
                profile_const.SIGNUP_COUPON_DISCOUNT,
                profile_const.SIGNUP_COUPON_MAXUSAGE,
                profile_const.SIGNUP_COUPON_MAXDISCOUNT,
                profile_const.SIGNUP_COUPON_MINVALUE,
                profile_const.SIGNUP_COUPON_VALIDDAYS,
                profile_const.SIGNUP_COUPON_TERMS)
            coupon_present = True
        except Exception as exc:
            logger.exception(exc)
    logger.debug("user %s saved" % user.get_full_name())
    try:
        email_service = RegistrationEmailService(
            RegistrationEmailService.NOTIFICATION_EMAIL, "html", [
                user.email], profile_const.REGISTRATION_SUBJECT, None)
        email_service.collateEmailData(
            user.first_name, coupon_present, unique_coupon_code)
        email_service.createMessage()
        email_service.sendMessage()
        logger.info("Welcome email sent to " + user.email)
    except socket.error as v:
        print(v)
        errorCode = v[0]
        if errorCode == errno.ECONNREFUSED:
            logger.error("Connection refused while sending email")
    except Exception as exc:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        logger.exception("Exception in send Welcome email %s" % repr(
            traceback.format_exception(exc_type, exc_obj, exc_tb)))


def __create_unique_coupon_code(user):
    firstName = user.first_name
    lastName = user.last_name
    email = user.email
    prefixString = str(firstName) + str(lastName) + str(email)
    prefixString = prefixString.strip()
    prefixString = prefixString.upper()
    prefix = prefixString
    if len(prefixString) > 3:
        prefix = prefixString[:3]
    suffix = str(profile_const.SIGNUP_COUPON_DISCOUNT)
    allPrefixesCount = UserDiscount.objects.filter(
        coupon_code__startswith=prefix).count()
    discountCount = DiscountClient.getDiscountPrefixCount(prefix)
    allPrefixesCount += discountCount
    if allPrefixesCount > 0:
        couponCount = allPrefixesCount - 1
        prefixSuffix = __base_n(couponCount, 26)
        prefix += str(prefixSuffix)
        prefix = prefix.upper()
    userDiscount = UserDiscount()
    userDiscount.user = user
    userDiscount.coupon_code = prefix + suffix
    userDiscount.save()
    return userDiscount.coupon_code


def __base_n(num, b, numerals="abcdefghijklmnopqrstuvwxyz"):
    return ((num == 0) and numerals[0]) or (
        __base_n(num // b, b, numerals).lstrip(numerals[0]) + numerals[num % b])
