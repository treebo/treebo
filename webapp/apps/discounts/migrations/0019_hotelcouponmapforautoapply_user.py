# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('discounts', '0018_hotelcouponmapforautoapplyuploadlog'),
    ]

    operations = [
        migrations.AddField(
            model_name='hotelcouponmapforautoapply',
            name='user',
            field=models.ForeignKey(
                related_name='userhotelcoupons',
                blank=True,
                to=settings.AUTH_USER_MODEL,
                null=True,
                on_delete=models.DO_NOTHING),
        ),
    ]
