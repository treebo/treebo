import logging
from django.utils.text import slugify
from django.core.urlresolvers import reverse
from django.conf import settings
from urllib.parse import urlencode

from dbcommon.models.facilities import Facility
from dbcommon.models.location import City
from data_services.soa_clients.utils import get_state_id_from_cs_id, get_hotel_id_from_cs_id

logger = logging.getLogger(__name__)

class WebSiteAttributesFetcher:

    def get_direct_specific_data_for_city(self, city_cs_id):
        city_params = {}
        try:
            city = City.objects.filter(cs_id=city_cs_id).first()
            if city:
                city_params['enable_for_seo'] = city.enable_for_seo
                city_params['city_latitude'] = city.city_latitude
                city_params['city_longitude'] = city.city_longitude
                city_params['id'] = city.id
                city_params['slug'] = city.slug
                city_string = slugify(city_params['slug'])
                kwargs = {'cityString': city_string + '/'}
                city_params['sitemap_url'] = reverse('pages:hotels_in_city', kwargs=kwargs)
                city_params['description'] = city.description
                city_params['tagline'] = city.tagline
                city_params['subscript'] = city.subscript
                city_params['status'] = 1
                city_params['seo_image'] = city.seo_image
                city_params['cover_image'] = city.cover_image
                city_params['landscape_image'] = city.landscape_image
                city_params['portrait_image'] = city.portrait_image
                return city_params
        except Exception as e:
            logger.info("Exception in getting city seo status. Returning true")
            raise e


    def get_direct_specific_data_for_state(self, state_cs_id):
        state_data = {}
        state_data['id'] = get_state_id_from_cs_id(state_cs_id)
        return state_data


    def get_locality_specific_data_from_direct(self, locality_cs_id, city_dto):
        locality_data = {}
        from dbcommon.models.location import Locality
        locality = Locality.objects.filter(cs_id=locality_cs_id).first()
        if locality:
            locality_data['id'] = locality.id
        else:
            logger.info("Locality with cs id %s doesnt have an entry in direct db", locality_cs_id)
            locality_data['id'] = None
        locality_data['city'] = city_dto
        return locality_data

    def get_landmark_specific_data_from_direct(self, city_dto):
        return {'city':city_dto, 'type' : 'landmark'}

    def get_direction_specific_data_from_direct(self, landmark_dto):
        return {'landmark_obj' : landmark_dto, 'mode' : 'DRIVING'}

    def update_hotel_data_with_details_from_direct(self, hotel_data, city_dto, locality_dto):
        hotel_data['id'] = get_hotel_id_from_cs_id(hotel_data['cs_id'])
        hotel_data['city'] = city_dto
        hotel_data['locality'] = locality_dto
        hotel_data['showcased_image_url'] = ''
        hotel_data['location_url'] = '//maps.google.com/?' + urlencode(
                {'q': str(hotel_data['latitude']) + ',' + str(hotel_data['longitude'])})
        hotel_data['consumer_key'] = settings.CONSUMER_KEY
        hotel_data['consumer_secret'] = settings.CONSUMER_SECRET
        hotel_data['hotel_canonical_url'] = self.get_hotel_canonical_url(hotel_data, hotel_data['id'])
        hotel_data['sitemap_url'] = self.get_hotel_sitemap_url(hotel_data)
        hotel_data['city_name'] = city_dto.name
        hotel_data['address'] = hotel_data['street'] + ", " + locality_dto.name + ", " + city_dto.name
        return hotel_data

    def get_hotel_canonical_url(self, hotel_data, hotel_id):
        hyphen_separated_name = slugify(hotel_data['name'])
        hyphen_separated_locality = slugify(hotel_data['locality'].name)
        hotel_part = hyphen_separated_name + '-' + hyphen_separated_locality + '-' + str(hotel_id)
        hyphen_separated_city = slugify(hotel_data['city'].name)
        city_part = 'hotels-in-' + hyphen_separated_city
        url_string = city_part + '/' + hotel_part + '/'
        return url_string

    def get_hotel_sitemap_url(self, hotel_data):
        city_string = slugify(hotel_data['city'].name)
        kwargs = {'cityString': city_string}
        return reverse('pages:hotels_in_city', kwargs=kwargs) + "/" + slugify(hotel_data['name']) + "-" + slugify(
                hotel_data['locality'].name) + "-" + str(hotel_data['id']) + "/"

    def get_direct_specific_attributes_for_amenity(self, amenity):
        try:
            logger.debug("Getting direct specific attribs for amenity %s", amenity['amenity_key'])
            facility = Facility.objects.filter(catalog_mapped_name=amenity['amenity_key']).first()
            amenity['css_class'] = facility.css_class
            amenity['url'] = facility.url
        except Exception as e:
            logger.exception(e)
            amenity['css_class'] = ''
            amenity['url'] = ''
        return amenity

