from fabric.api import env, run, sudo, local, put, require, prompt, cd, lcd, settings, task
from fabric.operations import require
from fabric.contrib.files import exists
import sys

config = {
    'celery_queue_workers': {
        "celery_p3": 4,
        "third_party_trigger_p3": 4,
        "broadcast_booking_p3": 4,
        "trivago_hotel_details_p3": 4,
        "celery_create_coupon": 2,
        "dynamic_ads_queue":5,
    },
    'celery_queues': []
}

TRUTHY =['true', 'True']


@task
def container_config(celery_q_str):
    celery_queues = celery_q_str.split(';')
    config['celery_queues'] = celery_queues
    local('echo config is set queues are %s ' % (celery_queues))


@task
def push_image(image_name, version):
    docker_options = env.copy()
    docker_options['version'] = version
    docker_options['image_name'] = image_name
    local('docker push %(DOCKER_HOST)s/%(image_name)s:%(version)s ' %
          docker_options)
    local('echo push complete')


@task
def pull_image(image_name, version):
    local('echo pulling image started')
    docker_options = env.copy()
    docker_options['version'] = version
    docker_options['image_name'] = image_name
    env.run('docker pull %(DOCKER_HOST)s/%(image_name)s:%(version)s' %
            docker_options)
    local('echo pull complete')


@task
def build(image_name, version, user=None):
    if user:
        env.user = user
    require('env_file', provided_by=(prod, dev, env.run,))
    require('base_env_file', provided_by=(prod, dev, env.run,))
    docker_options = env.copy()
    docker_options['version'] = version
    docker_options['image_name'] = image_name
    local(
        'docker build --build-arg req_file=%(req_file)s  --build-arg BASE_IMAGE=%(base_image)s -t %('
        'DOCKER_HOST)s/%(image_name)s:%(version)s -t %('
        'DOCKER_HOST)s/%(image_name)s:latest .' %
        docker_options)
    push_image(image_name, version)
    local('echo build complete')


@task
def tag_image(image_id, image_name, version):
    require('env_file', provided_by=(prod, dev, env.run,))
    require('base_env_file', provided_by=(prod, dev, env.run,))
    docker_options = env.copy()
    docker_options['version'] = version
    docker_options['image_name'] = image_name
    docker_options['image_id'] = image_id
    local(
        'docker tag %(image_id)s %(DOCKER_REGISTRY)s/%(image_name)s:%(version)s' %
        docker_options)


@task
def deploy(
        image_name,
        version,
        run_gunicorn,
        run_redis,
        run_cs_consumers,
        job=True,
        port=None,
        user=None):
    if user:
        env.user = user
    if not port:
        port = env.WEB_SERVER_DEFAULT_PORT
    local('echo setting port %s ' % (port))
    require('env_file', provided_by=(prod, dev, env.run,))
    require('base_env_file', provided_by=(prod, dev, env.run,))
    # if not exists('/etc/direct/web/docker_env/'):
    #     sudo('mkdir /etc/direct/web/docker_env/')
    local("echo copying docker_env to jenkins")
    # put('docker_env', '/etc/direct/web/docker_env/', use_sudo=True)
    local(' echo pull image ')
    pull_image(image_name, version)
    local('echo starting stop_and_remove_containers')
    stop_and_remove_containers(image_name, version)
    local('echo starting create_all_containers')
    create_all_containers(image_name, version, run_gunicorn, run_redis, run_cs_consumers, port)
    if job in TRUTHY:
        local('echo starting create_job_consumer')
        create_job_consumer(
            image_name,
            version,
            env.eash_job_container_name)
    local('echo deployment complete')


def stop_and_remove_containers(image_name, version):
    docker_options = env.copy()
    docker_options.update({'image_name': image_name, 'version': version})

    try:
        local('echo stoping containers "%(container_name)s" ' % docker_options)
        # run('docker stop $( docker ps -a -q --filter "name=%(container_name)s" )' % docker_options)
        env.run(
            'docker ps | grep "%(image_name)s" | awk \'{print $1}\' | xargs -I {} docker stop {}' %
            docker_options)
    except BaseException:
        local(
            'echo no containers found to stop "%(container_name)s" ' %
            docker_options)

    try:
        local('echo removing containers %(image_name)s ' % docker_options)
        # run('docker ps -a | grep "%(container_name)s" | awk \'{print $1}\' | xargs -I {} docker rm {}' % docker_options)
        env.run(
            'docker ps -a | grep "%(image_name)s" | awk \'{print $1}\' | xargs -I {} docker rm {}' %
            docker_options)
    except BaseException:
        local(
            'echo no containers found to remove "%(container_name)s" ' %
            docker_options)
    local('echo container stopped and removed')


def create_all_containers(image_name, version, run_gunicorn, run_redis, run_cs_consumers, port):
    if run_gunicorn in TRUTHY:
        local('echo starting gunicorn')
        create_gunicorn_container(image_name, version, port)

    if run_cs_consumers in TRUTHY:
        local('echo starting run_cs_consumers')
        create_consumer_containers(image_name, version)

    # if run_redis:
    #     local('echo starting redis')
    #     create_redis_containers(version,image_name)
    celery_queues = config['celery_queues']
    local('echo starting celery_queues %s ' % celery_queues)
    celery_container_dict = {}
    for index, celery_queue_name in enumerate(celery_queues):
        try:
            local(
                'echo starting celery_queues %s %s ' %
                (index, celery_queue_name))
            worker = config['celery_queue_workers'][celery_queue_name]
            if celery_queue_name not in celery_container_dict:
                celery_container_dict[celery_queue_name] = 1
            else:
                celery_container_dict[celery_queue_name] += 1
            create_celery_worker_containers(
                image_name, version, worker, celery_queue_name)
            local(
                'echo celery_queues %s %s is done ' %
                (index, celery_queue_name))
        except Exception as e:
            local(
                'echo celery_queues %s %s is failed with error %s ' %
                (index, celery_queue_name, str(e)))
    local('echo celery container complete')

    create_hotel_migration_event_consumer_container(image_name, version)


def create_network():
    local('docker network create -d bridge growth')


def create_gunicorn_container(image_name, version, port, network=None):
    require('env_file', provided_by=(prod, dev, env.run,))
    require('base_env_file', provided_by=(prod, dev, env.run,))
    docker_options = env.copy()
    docker_options.update(
        {'image_name': image_name, 'version': version, 'server_port': port})
    if network:
        local('echo host for web gunicorn - %(host)s ' % {'host': env.hosts})
        docker_options['network'] = 'web'
        run('docker run -v %(log_root)s:%(log_root)s -v %(host_static_root)s:%(docker_static_root)s'
            ' -v %(gunicorn_log)s:%(gunicorn_log)s -v %(host_media_root)s:%(docker_media_root)s -v %(dynamic_ads_path)s:/var/data/dynamicads/    '
            ' -v %(image_upload_path)s:%(image_upload_path)s  -v %(trivago_data_path)s:%(trivago_data_path)s  -v /usr/src/app'
            '--log-opt max-size=50m --name %(container_name)s -p%(server_port)s:8000 --network=%(network)s '
            '--restart unless-stopped '
            '--env-file=%(base_env_file)s --env-file=%(env_file)s -itd %(DOCKER_HOST)s/%(image_name)s:%(version)s '
            '%(gunicorn_script_path)s' % docker_options)
    else:
        local('echo host for web gunicorn is %(host)s ' % {'host': env.hosts})
        run('docker run -v %(log_root)s:%(log_root)s -v %(host_static_root)s:%(docker_static_root)s -v %('
            'gunicorn_log)s:%(gunicorn_log)s -v %(host_media_root)s:%(docker_media_root)s -v /usr/src/app '
            ' --log-opt max-size=50m --name %(container_name)s -p%(server_port)s:8000 '
            '--restart unless-stopped '
            '--env-file=%(base_env_file)s --env-file=%(env_file)s -itd %(DOCKER_HOST)s/%(image_name)s:%(version)s '
            '%(gunicorn_script_path)s' % docker_options)
    local('echo gunicorn container complete')



@task
def create_job_consumer(image_name, version, container_name, network=None):
    docker_options = env.copy()
    docker_options.update({})
    docker_options.update({
        'easy_job_container_name': container_name,
        'version': version,
        'image_name': image_name,
    })
    docker_options['command'] = 'python manage_job_consumer.py start'
    try:
        local(
            'echo stoping  easy job containers %(easy_job_container_name)s ' %
            docker_options)
        run('docker stop  %(easy_job_container_name)s ' % docker_options)
    except BaseException:
        local(
            "echo no running easy_job %(easy_job_container_name)s container with the specified name found" %
            docker_options)
    try:
        local(
            'echo removing easy_job containers  %(easy_job_container_name)s ' %
            docker_options)
        run('docker rm  %(easy_job_container_name)s ' % docker_options)
    except BaseException:
        local(
            "echo no %(easy_job_container_name)s easy_job container with the specified name found" %
            docker_options)

    local('echo easy_job command is %(command)s' % docker_options)
    local(
        'echo starting container  %(easy_job_container_name)s' %
        docker_options)
    if network:
        run('docker run -v %(log_root)s:%(log_root)s --volumes-from %(container_name)s --log-opt max-size=50m '
            '--no-healthcheck '
            '--name %(easy_job_container_name)s --env-file=%(base_env_file)s  --env-file=%(env_file)s --network=%(network)s '
            '-itd %(DOCKER_HOST)s/%(image_name)s:%(version)s %(command)s' % docker_options)
    else:
        run(
            'docker run -v %(log_root)s:%(log_root)s --volumes-from %(container_name)s  --log-opt max-size=50m '
            '--no-healthcheck '
            '--name %(easy_job_container_name)s --env-file=%(base_env_file)s  --env-file=%(env_file)s '
            '-itd %(DOCKER_HOST)s/%(image_name)s:%(version)s %(command)s' % docker_options)
    local('echo easy_job %(command)s  complete ' % docker_options)


@task
def create_celery_worker_containers(
        image_name,
        version,
        num_of_workers,
        celery_queue_name,
        network=None):
    require('env_file', provided_by=(prod, dev, env.run,))
    require('base_env_file', provided_by=(prod, dev, env.run,))

    docker_options = env.copy()
    docker_options.update({
        'version': version,
        'image_name': image_name,
        'num_of_workers': num_of_workers,
        'celery_queue_name': celery_queue_name,
    })
    worker_commands = (
        'newrelic-admin run-program celery worker -c %(num_of_workers)s -A webapp -Q %(celery_queue_name)s '
        ' -f %(log_root)s/%(celery_queue_name)s.log' %
        docker_options,)
    for command in worker_commands:
        docker_options['celery_container_name'] = "web_" + \
            command.split()[2] + '_' + str(celery_queue_name)
        docker_options['command'] = command
        local('echo command is %(command)s' % docker_options)
        local(
            'echo starting container  %(celery_container_name)s' %
            docker_options)
        if network:
            run('docker run -v %(log_root)s:%(log_root)s -v %(dynamic_ads_path)s:/var/data/dynamicads/ --volumes-from %(container_name)s --log-opt max-size=50m '
                '--no-healthcheck '
                '--name %(celery_container_name)s --env-file=%(base_env_file)s  --env-file=%(env_file)s --network=%(network)s '
                '-itd %(DOCKER_HOST)s/%(image_name)s:%(version)s %(command)s' % docker_options)
        else:
            run(
                'docker run -v %(log_root)s:%(log_root)s --volumes-from %(container_name)s  --log-opt max-size=50m '
                '--no-healthcheck '
                '--name %(celery_container_name)s --env-file=%(base_env_file)s  --env-file=%(env_file)s '
                '-itd %(DOCKER_HOST)s/%(image_name)s:%(version)s %(command)s' % docker_options)
        local('echo celery %(command)s  complete ' % docker_options)


def create_consumer_containers(
        image_name,
        version,
        network=None):
    require('env_file', provided_by=(prod, dev, env.run,))
    require('base_env_file', provided_by=(prod, dev, env.run,))

    docker_options = env.copy()
    docker_options.update({
        'version': version,
        'image_name': image_name,
    })

    worker_commands = (
        {
            'management_command_script': 'python manage.py catalogue_service_consumer_hotels',
            'container_name': 'catalogue_service_consumer_hotels',
            'server_port': 8030
        },
        {
            'management_command_script': 'python manage.py catalogue_service_consumer_amenities',
            'container_name': 'catalogue_service_consumer_amenities',
            'server_port': 8031
        },
        {
            'management_command_script': 'python manage.py catalogue_service_consumer_rooms',
            'container_name': 'catalogue_service_consumer_rooms',
            'server_port': 8032
        },
        {
            'management_command_script': 'python manage.py catalogue_service_consumer_images',
            'container_name': 'catalogue_service_consumer_images',
            'server_port': 8033
        })

    for command in worker_commands:
        docker_options['management_command_script'] = command['management_command_script']
        docker_options['container_name'] = command['container_name']
        stop_catalogue_service_container(docker_options)
        docker_options['server_port'] = command['server_port']
        run('docker run -v %(log_root)s:%(log_root)s -v %(host_static_root)s:%(docker_static_root)s -v %('
            'gunicorn_log)s:%(gunicorn_log)s -v %(host_media_root)s:%(docker_media_root)s -v /usr/src/app '
            ' --log-opt max-size=50m --name %(container_name)s -p%(server_port)s:8000 '
            '--no-healthcheck '
            '--env-file=%(base_env_file)s --env-file=%(env_file)s -itd %(DOCKER_HOST)s/%(image_name)s:%(version)s '
            '%(management_command_script)s' % docker_options)


def create_hotel_migration_event_consumer_container(
    image_name,
    version,
    network=None):

    require('env_file', provided_by=(prod, dev, env.run,))
    require('base_env_file', provided_by=(prod, dev, env.run,))

    docker_options = env.copy()
    docker_options.update({
        'version': version,
        'image_name': image_name,
    })

    docker_options['management_command_script'] = 'python manage.py hotel_migration_event_consumer'
    docker_options['container_name'] = 'hotel_migration_event_consumer'

    try:
        local('echo stopping hotel migration event consumer container %(container_name)s ' % docker_options)
        run('docker stop %(container_name)s' % docker_options)
    except BaseException:
        local("echo no running hotel migration event consumer container with the name %(container_name)s found" %
              docker_options)
    try:
        local('echo removing containers %(container_name)s ' % docker_options)
        env.run(
            'docker ps -a | grep "%(container_name)s" | awk \'{print $1}\' | xargs -I {} docker rm {}' %
            docker_options)
    except BaseException:
        local(
            'echo no containers found to remove "%(container_name)s" ' %
            docker_options)

    docker_options['server_port'] = 5799
    run('docker run -v %(log_root)s:%(log_root)s -v %(host_static_root)s:%(docker_static_root)s -v %('
        'gunicorn_log)s:%(gunicorn_log)s -v %(host_media_root)s:%(docker_media_root)s -v /usr/src/app '
        ' --log-opt max-size=50m --name %(container_name)s -p%(server_port)s:8000 '
        '--no-healthcheck '
        '--env-file=%(base_env_file)s --env-file=%(env_file)s -itd %(DOCKER_HOST)s/%(image_name)s:%(version)s '
        '%(management_command_script)s' % docker_options)


def stop_catalogue_service_container(docker_options):
    try:
        local(
            'echo stopping  catalogue containers %(container_name)s ' %
            docker_options)
        run('docker stop  %(container_name)s ' % docker_options)
    except BaseException:
        local(
            "echo no running catalogue %(container_name)s container with the specified name found" %
            docker_options)
    try:
        local('echo removing containers %(container_name)s ' % docker_options)
        # run('docker ps -a | grep "%(container_name)s" | awk \'{print $1}\' | xargs -I {} docker rm {}' % docker_options)
        env.run(
            'docker ps -a | grep "%(container_name)s" | awk \'{print $1}\' | xargs -I {} docker rm {}' %
            docker_options)
    except BaseException:
        local(
            'echo no containers found to remove "%(container_name)s" ' %
            docker_options)

def create_redis_containers(version, image_name, network=None):
    # /usr/bin/redis-server
    # docker run -d -p 6379 -i -t johncosta/redis
    docker_options = env.copy()
    docker_options.update({})
    docker_options.update({
        'redis_container_name': 'redis_container',
        'version': version,
        'image_name': image_name,
        'port': 6379
    })
    docker_options['command'] = '/usr/bin/redis-server'
    local(
        'echo creating redis container %(container_name)s , from %(image_name)s tag : %(version)s ' %
        docker_options)
    try:
        local(
            'echo stopping  redis containers %(redis_container_name)s ' %
            docker_options)
        run('docker stop  %(redis_container_name)s ' % docker_options)
    except BaseException:
        local(
            "echo no running redis %(redis_container_name)s container with the specified name found" %
            docker_options)
    try:
        local(
            'echo removing redis containers  %(redis_container_name)s ' %
            docker_options)
        run('docker rm  %(redis_container_name)s ' % docker_options)
    except BaseException:
        local(
            "echo no %(redis_container_name)s redis container with the specified name found" %
            docker_options)

    local('echo redis command is %(command)s' % docker_options)
    local('echo starting container  %(redis_container_name)s' % docker_options)
    if network:
        run('docker run -v %(log_root)s:%(log_root)s --volumes-from %(container_name)s --log-opt max-size=50m '
            '--no-healthcheck '
            '--name %(redis_container_name)s --env-file=%(base_env_file)s  --env-file=%(env_file)s --network=%(network)s '
            '-d -p %(port)s -i -t '
            '-itd %(DOCKER_HOST)s/%(image_name)s:%(version)s %(command)s' %
            docker_options)
    else:
        run(
            'docker run -v %(log_root)s:%(log_root)s --volumes-from %(container_name)s  --log-opt max-size=50m '
            '--no-healthcheck '
            '--name %(redis_container_name)s --env-file=%(base_env_file)s  --env-file=%(env_file)s '
            '-d -p %(port)s -i -t '
            '-itd %(DOCKER_HOST)s/%(image_name)s:%(version)s %(command)s' % docker_options)
    local('echo easy_job %(command)s  complete ' % docker_options)

    pass


def base():
    env.run = run
    env.req_file = 'requirements.txt'  # to specify file at runtime
    env.base_image='docker-hub-m.treebo.com:5000/treebo-python3.5:v1'
    env.base_env_file = '/tmp/website/docker_env/base.env'
    env.push = True
    env.pull = True
    env.env_file = '/tmp/website/docker_env/dev.env'

    if not exists('/tmp/website/'):
        env.run('mkdir /tmp/website')

    put('docker_env', '/tmp/website/')


def prod_config():
    env.broadcast_celery_queue = 'prod_broadcast_booking'


@task
def prod():
    base()
    prod_config()
    require('hosts')
    local('echo host prod %(host)s ' % {'host': env.hosts})
    env.env_file = '/tmp/website/docker_env/prod.env'


@task
def external():
    base()
    prod_config()
    require('hosts')
    local('echo  host external %(host)s ' % {'host': env.hosts})
    env.env_file = '/tmp/website/docker_env/ext.env'


@task
def gdc():
    base()
    prod_config()
    require('hosts')
    local('echo  host gdc %(host)s ' % {'host': env.hosts})
    env.env_file = '/tmp/website/docker_env/gdc.env'


@task
def external():
    base()
    prod_config()
    require('hosts')
    local('echo  "host external %(host)s" ' % {'host': env.hosts})
    env.env_file = '/tmp/website/docker_env/ext.env'


@task
def tools():
    base()
    prod_config()
    require('hosts')
    local('echo  host tools %(host)s ' % {'host': env.hosts})
    env.env_file = '/tmp/website/docker_env/tools.env'

@task
def preprod1():
    base()

    if not env.hosts:
        env.hosts.extend(['13.229.167.124'])
    local('echo  preprod host %(host)s ' % {'host': env.hosts})
    env.env_file = '/tmp/website/docker_env/preprod1.env'
    env.base_image = 'docker-hub.treebo.com:5000/treebo-python3.5:v1'
    env.host_static_root = '/ebs1/spa/treebo_current/webapp/static_pipeline'
    env.host_media_root = '/ebs1/spa/treebo_current/webapp/static/files'



@task
def preprod():
    base()

    if not env.hosts:
        env.hosts.extend(['54.254.181.94'])
    local('echo  preprod host %(host)s ' % {'host': env.hosts})
    env.env_file = '/tmp/website/docker_env/preprod.env'
    env.base_image = 'docker-hub.treebo.com:5000/treebo-python3.5:v1'
    env.host_static_root = '/ebs1/spa/treebo_current/webapp/static_pipeline'
    env.host_media_root = '/ebs1/spa/treebo_current/webapp/static/files'

@task
def conversion_staging():
    base()

    if not env.hosts:
        env.hosts.extend(['54.254.181.94'])
    local('echo  conversion staging host %(host)s ' % {'host': env.hosts})
    env.env_file = '/tmp/website/docker_env/conversion_staging.env'
    env.base_image = 'docker-hub.treebo.com:5000/treebo-python3.5:v1'
    env.host_static_root = '/ebs1/spa/treebo_current/webapp/static_pipeline'
    env.host_media_root = '/ebs1/spa/treebo_current/webapp/static/files'


@task
def loadtest():
    base()
    require('hosts')
    local('echo  loadtest host %(host)s ' % {'host': env.hosts})
    env.host_static_root = '/ebs1/spa/treebo_current/webapp/static_pipeline'
    env.host_media_root = '/ebs1/spa/treebo_current/webapp/static/files'
    env.env_file = '/tmp/website/docker_env/loadtest.env'


@task
def dev():
    base()
    if not env.hosts:
        env.hosts.extend(['52.76.123.234'])
    local('echo dev host %(host)s' % {'host': env.hosts})
    env.host_static_root = '/ebs1/spa/treebo_current/treebo/webapp/static_pipeline'
    env.host_media_root = '/ebs1/spa/treebo_current/treebo/webapp/static/files'


@task
def localhost():
    base()
    env.run = local
    env.cd = lcd
    if not env.hosts:
        env.hosts.extend(['127.0.0.1'])
    local('echo  host local %(host)s ' % {'host': env.hosts})
    env.env_file = '/tmp/website/docker_env/local.env'
    env.user = 'ankurjain'
    env.DOCKER_HOST = 'docker-hub.treebo.com:5000'
    env.log_root = '/Users/neeraj/Work/logs/web'
    env.gunicorn_log = '/Users/neeraj/Work/logs/web'
    env.host_static_root = '/Users/neeraj/treeboRepo/treebo/webapp/static_pipeline'
    env.host_media_root = '/Users/neeraj/treeboRepo/treebo/webapp/static/files'


# very dangerous commands

@task
def remove_stopped_containers():
    local("docker rm $(docker ps -a | grep Exited | awk '{print $1}')")


@task
def remove_untagged_containers():
    local('docker rmi $(docker images -q --filter "dangling=true")')


@task
def stop_and_remove_all_containers():
    local('docker rm -f $(docker ps -a -q)')


@task
def test_fabric():
    local('echo "Hello There!!"')


@task
def remove_all_images():
    local('docker rmi $(docker images -q)')


@task
def remove_all_untagged_images():
    local('docker rmi $(docker images | grep "^<none>" | awk "{print $3}")')

# fab -k dev -H 172.40.20.210  deploy:image_name='web',version='v1.1',run_gunicorn=True,celery_queues=[{"queue_name":"celery","worker":4},
# {"queue_name":"third_party_trigger","worker":4},{"queue_name":"broadcast_booking","worker":4},{"queue_name":"trivago_hotel_details","worker":4}],
# user='jenkins'  -c .fabricrc


# fab -k -p jenkins dev deploy:image_name='web',version='v1.1',run_gunicorn=True,user='jenkins'  -c .fabricrc
# fab -k localhost container_config:celery_q_str='celery;third_party_trigger;broadcast_booking;trivago_hotel_details'  deploy:image_name='web',
# version='v1.1',run_gunicorn=True,
# user='jenkins'  -c .fabricrc