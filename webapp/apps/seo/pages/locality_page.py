from django.conf import settings
from django.utils.text import slugify
from django.db.models import Q
from apps.seo.components.breadcrumb import BreadCrumb

from apps.seo.pages.pages import PAGES
from dbcommon.models.location import City, Locality, LocalitySeoDetails
from apps.seo.components.categories import Category
from apps.seo.components.cities_nearby import CitiesNearby
from apps.seo.components.landmarks import Landmarks
from apps.seo.components.localities import Localities
from apps.seo.components.popular_cities import PopularCities
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'ansilkareem'


class LocalityPage(PAGES):

    def __init__(self, page, query):
        city = query.split('-')[-1]
        self.locality_exclude_list = []
        locality_name = ' '.join(query.split('-')[:-1])
        self.locality = Locality.objects.filter(Q(name__iexact=locality_name) & Q(
            Q(city__name__iexact=city) | Q(city__slug__iexact=city))).first()
        self.city = self.locality.city
        self.locality_exclude_list.append(self.locality.id)
        self.about_key = 'about_' + '_'.join(locality_name.lower().split()) + '_' + '_'.join(
            self.city.name.lower().split())
        self.description = ContentServiceClient.getValueForKey(
            None, self.about_key, 1)
        super(LocalityPage, self).__init__(page, query, locality_name)

    def update_meta(self,):
        locality_seo_details = LocalitySeoDetails.objects.filter(
            locality=self.locality).first()
        full_canonical_url = settings.SITE_HOST_NAME + 'hotels-in-' + \
            slugify(self.locality.name) + '-' + slugify(self.city.slug) + '/'
        if locality_seo_details:

            self.override_seo_content(
                locality_seo_details.locality_seo_title,
                locality_seo_details.locality_seo_description,
                locality_seo_details.locality_seo_keyword,
                full_canonical_url,
                self.locality.name)
        else:
            title = None
            description = None
            temp_dict = {
                "locality_name": self.locality.name,
                "city_name": self.locality.city.name
            }
            if 'locality_title' in self.footer_data:
                title = self.footer_data['locality_title'] % temp_dict
            if 'locality_description' in self.footer_data:
                description = self.footer_data['locality_description'] % temp_dict
            if title and description:
                self.override_seo_content(title,
                                          description,
                                          None,
                                          full_canonical_url)

    def build_meta(self):
        self.update_meta()
        self.meta_content['content'] = {
            'description': self.description['text'] if self.description else ''
        }
        category = Category(self.city, self.footer_data, [])
        landmark = Landmarks(self.city, self.footer_data, [])
        locality = Localities(self.city, self.footer_data, [self.locality.id])
        cities_nearby = CitiesNearby(self.city, self.footer_data)
        self.meta_content['content'].update(
            {
                'links': [
                    locality.build(),
                    landmark.build(),
                    category.build(),
                    cities_nearby.build()],
                'breadcrumbs': BreadCrumb(
                    self.city,
                    self.footer_data,
                    self.page,
                    self.item_name).build()})
        popular_cities = PopularCities(
            self.city, self.footer_data, [
                city.id for city in cities_nearby.sorted_nearby_cities])
        self.meta_content['content']['links'].append(popular_cities.build())
