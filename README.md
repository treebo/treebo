Initial requirements:-
	1. python 2.7
	2. postgres
		postgres setup steps:
			installation
				a. sudo apt-get install libpq-dev python-dev
				b. sudo apt-get install postgresql postgresql-contrib

			creating db and user
				a. sudo su - postgres
				b. createdb treebo
				c. createuser treebo --pwprompt
			(Please add "treebo" as password)

Setup commands     :
	1. Installing dependencies: pip install -r requirements.txt
	2. Running db migrations:
		> python manage.py makemigrations
		> python manage.py migrate
	3. Running server: python manage.py runserver

CI/CD via Jenkins setup

Before installing please add these dependencies :
	sudo apt-get install libxml2-dev libxslt-dev
	sudo apt-get install memcached (check memcache working through "sudo netstat -tapen | grep ":11211 "")


