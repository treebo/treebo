from django.db import models
from dbcommon.models.hotel import Hotel, Locality, City


class POICategory(models.Model):
    category_name = models.TextField()
    is_locality_mapped = models.BooleanField(default=False)
    is_city_mapped = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'POI Category'
        verbose_name_plural = 'POI Categories'

    def __unicode__(self):
        return self.category_name

    def __str__(self):
        return self.category_name


class POI(models.Model):
    poi_name = models.TextField()
    poi_location = models.TextField(null=True, blank=True)
    poi_city = models.TextField(null=True, blank=True)
    poi_subcategory = models.TextField(null=True, blank=True)
    category = models.ForeignKey(POICategory, on_delete=models.CASCADE)
    poi_lat = models.DecimalField(max_digits=9, decimal_places=6)
    poi_lng = models.DecimalField(max_digits=9, decimal_places=6)
    locality = models.ForeignKey(Locality, on_delete=models.CASCADE, null=True)
    city = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
        null=False,
        default=1)

    class Meta:
        verbose_name = 'POI'
        verbose_name_plural = 'POIs'

    def __unicode__(self):
        return self.poi_name

    def __str__(self):
        return self.poi_name


# POI MAPPER DELETED
