import logging

from apps.reviews.models import TAHotelIDMapping
from apps.reviews.services.trip_advisor.hotel_overall_rating_service import HotelRatingService

logger = logging.getLogger(__name__)


class GenerateWriteReviewUrlService(object):
    def generate_review_url(self, request, hotel_id, review_response=None):
        logged_in_user = False
        iframe_widget_url = False
        try:
            logger.info("request is %s", request.data)
            if hasattr(
                    request,
                    'user') and request.user and request.user.is_authenticated():
                logger.info("request.user %s is logged in ", request.user)
                logged_in_user = True
            if TAHotelIDMapping.objects.filter(
                    hotel__id=hotel_id,
                    enable_iframe_widget=True).exists():
                iframe_widget_url = True
            if logged_in_user and iframe_widget_url:
                hotel_rating = HotelRatingService()
                email = request.user.email
                name = request.user.first_name
                write_review_url = hotel_rating.generate_iframe_url(
                    hotel_id, email, name)
                if write_review_url and review_response:
                    ta_review_data = review_response.get("ta_reviews")
                    ta_review_data['write_review_url'] = write_review_url
        except Exception:
            logger.exception(
                "Exception in updating the write review url %s",
                hotel_id)
        return review_response
