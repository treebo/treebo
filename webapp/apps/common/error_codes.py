from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR

__author__ = 'varunachar'

ERROR_STATUS = "error"


def get(key):
    return __code.get(key, __code.get(DEFAULT_ERROR))


__code = dict()

DEFAULT_ERROR = '100'
__code[DEFAULT_ERROR] = {
    'code': DEFAULT_ERROR,
    'msg': 'Unable to fetch data. Internal error'
}

INVALID_CREDENTIALS = '101'
__code[INVALID_CREDENTIALS] = {
    'code': INVALID_CREDENTIALS,
    'msg': 'Invalid Username or Password'
}

INACTIVE_USER = '102'
__code[INACTIVE_USER] = {
    'code': INACTIVE_USER,
    'msg': 'Inactive user'
}

UNKNOWN_USER = '103'
__code[UNKNOWN_USER] = {
    'code': UNKNOWN_USER,
    'msg': 'Unknown email id'
}

INVALID_LOGIN_DATA = '104'
__code[INVALID_LOGIN_DATA] = {
    'code': INVALID_LOGIN_DATA,
    'msg': 'Invalid login information entered'
}

ALREADY_EXISTING_USER = '105'
__code[ALREADY_EXISTING_USER] = {
    'code': ALREADY_EXISTING_USER,
    'msg': 'User already exists'
}

INVALID_EMAIL_HASH = '106'
__code[INVALID_EMAIL_HASH] = {
    'code': INVALID_EMAIL_HASH,
    'msg': 'Invalid hash value'
}
INCORRECT_PASSWORD = '107'
__code[INCORRECT_PASSWORD] = {
    'code': INCORRECT_PASSWORD,
    'msg': 'Entered old password is invalid'

}

INVALID_BOOKING = '108'
__code[INVALID_BOOKING] = {
    'code': INVALID_BOOKING,
    'msg': 'No such booking found for user'
}

INVALID_COUPON_CODE = '109'
__code[INVALID_COUPON_CODE] = {
    'code': INVALID_COUPON_CODE,
    'msg': 'Coupon code is invalid'
}

UNABLE_TO_ADD_TO_CART = '110'
__code[UNABLE_TO_ADD_TO_CART] = {
    'code': UNABLE_TO_ADD_TO_CART,
    'msg': 'Error while trying to add to cart'
}

INVALID_ORDER_ID = '111'
__code[INVALID_ORDER_ID] = {
    'code': INVALID_ORDER_ID,
    'msg': 'No booking found for order id'
}

NO_ORDER_ID_FOUND = '112'
__code[NO_ORDER_ID_FOUND] = {
    'code': NO_ORDER_ID_FOUND,
    'msg': 'Please supply an order id'
}

INVALID_DATES = '113'
__code[INVALID_DATES] = {
    'code': INVALID_DATES,
    'msg': "Check in date is later than check out date"
}

INVALID_MOBILE_NO = '114'
__code[INVALID_MOBILE_NO] = {
    'code': INVALID_MOBILE_NO,
    'msg': 'Please supply a valid mobile number'
}

INVALID_GUESTS = '115'
__code[INVALID_GUESTS] = {
    'code': INVALID_GUESTS,
    'msg': "No of guests should be at least 1"
}

INVALID_ROOMS = '116'
__code[INVALID_ROOMS] = {
    'code': INVALID_ROOMS,
    'msg': "No of rooms should be at least 1"
}

INVALID_DATA = '117'
__code[INVALID_DATA] = {
    'code': INVALID_DATA,
    'msg': "Invalid Data Received"
}
NO_HOTEL_ROOM_ID_SUPPLIED = '118'
__code[NO_HOTEL_ROOM_ID_SUPPLIED] = {
    'code': NO_HOTEL_ROOM_ID_SUPPLIED,
    'msg': 'Invalid hotel id or room type supplied'
}
INVALID_GUESTS_SUPPLIED = '119'
__code[INVALID_GUESTS_SUPPLIED] = {
    'code': INVALID_GUESTS_SUPPLIED,
    'msg': 'No of guests is greater than room capacity'
}
PASSWORDS_DO_NOT_MATCH = '120'
__code[PASSWORDS_DO_NOT_MATCH] = {
    'code': PASSWORDS_DO_NOT_MATCH,
    'msg': 'Passwords do not match'
}

PASSWORD_NOT_ENTERED = '121'
__code[PASSWORD_NOT_ENTERED] = {
    'code': PASSWORD_NOT_ENTERED,
    'msg': 'Please enter password in both fields'
}

PROMOTIONS_NOT_PRESENT = '122'
__code[PROMOTIONS_NOT_PRESENT] = {
    'code': PROMOTIONS_NOT_PRESENT,
    'msg': 'Promotions were not provided'
}

INVALID_EMAIL_ORDER = '123'
__code[INVALID_EMAIL_ORDER] = {
    'code': INVALID_EMAIL_ORDER,
    'msg': 'The order was not made using the provided email'
}

OLD_NEW_PASSWORDS_MATCH = '124'
__code[OLD_NEW_PASSWORDS_MATCH] = {
    'code': OLD_NEW_PASSWORDS_MATCH,
    'msg': 'Old and new passwords cannot be the same'
}

INVALID_CHECK_IN_CHECKOUT_DATE = '125'
__code[INVALID_CHECK_IN_CHECKOUT_DATE] = {
    'code': INVALID_CHECK_IN_CHECKOUT_DATE,
    'msg': 'Invalid check in/checkout dates'
}

INVALID_TOTAL_COST = '126'
__code[INVALID_TOTAL_COST] = {
    'code': INVALID_TOTAL_COST,
    'msg': 'Invalid total cost'
}

INVALID_EMAIL = '127'
__code[INVALID_EMAIL] = {
    'code': INVALID_EMAIL,
    'msg': 'Please enter a valid email'
}

NO_ROOMS_AVAILABLE_FOR_THE_DATES = '128'
__code[NO_ROOMS_AVAILABLE_FOR_THE_DATES] = {
    'code': NO_ROOMS_AVAILABLE_FOR_THE_DATES,
    'msg': 'No rooms are available for the selected dates'
}

INVALID_COUPON = '129'
__code[INVALID_COUPON] = {
    'code': INVALID_COUPON,
    'msg': 'Error applying coupon.'
}
INVALID_REQUEST = '130'
__code[INVALID_REQUEST] = {
    'code': INVALID_REQUEST,
    'msg': 'Invalid request. '
}

REQUEST_FAILED = '131'
__code[REQUEST_FAILED] = {
    'code': REQUEST_FAILED,
    'msg': 'request failed. '
}
BOOKING_NOT_FOUND = '132'
__code[BOOKING_NOT_FOUND] = {
    'code': BOOKING_NOT_FOUND,
    'msg': 'Booking not found.',
    'status_code': HTTP_404_NOT_FOUND
}

INVALID_TOKEN = "133"
__code[INVALID_TOKEN] = {
    'code': INVALID_TOKEN,
    'msg': 'Token is invalid.'
}

NO_TOKEN_FOUND = "134"
__code[NO_TOKEN_FOUND] = {
    'code': NO_TOKEN_FOUND,
    'msg': 'No token found.'
}
UNABLE_TO_LOGOUT = "135"
__code[UNABLE_TO_LOGOUT] = {
    'code': UNABLE_TO_LOGOUT,
    'msg': 'Unable to logout.'
}

ERROR_AVAIL_COUPONS = '136'
__code[ERROR_AVAIL_COUPONS] = {
    'code': ERROR_AVAIL_COUPONS,
    'msg': 'Error fetching available coupons'
}
PASSWORD_NOT_CHANGED = '137'
__code[PASSWORD_NOT_CHANGED] = {
    'code': PASSWORD_NOT_CHANGED,
    'msg': 'Password not changed.'
}

EMAIL_NOT_VERIFIED = '138'
__code[EMAIL_NOT_VERIFIED] = {
    'code': EMAIL_NOT_VERIFIED,
    'msg': 'Email not verified.'
}

LOGIN_FAILED = '139'
__code[LOGIN_FAILED] = {
    'code': LOGIN_FAILED,
    'msg': 'Unable to login.'
}
EMAIL_NOT_SENT = '140'
__code[EMAIL_NOT_SENT] = {
    'code': EMAIL_NOT_SENT,
    'msg': 'Email not sent.'
}

LOGOUT_FAILED = '141'
__code[LOGOUT_FAILED] = {
    'code': LOGOUT_FAILED,
    'msg': 'Unable to logout.'
}

OTP_VERIFY_FAILED = '142'
__code[OTP_VERIFY_FAILED] = {
    'code': OTP_VERIFY_FAILED,
    'msg': 'Unable to verify OTP.'
}

OTP_SENT_FAILED = '143'
__code[OTP_SENT_FAILED] = {
    'code': OTP_SENT_FAILED,
    'msg': 'Unable to send OTP.'
}

REGISTER_FAILED = '144'
__code[REGISTER_FAILED] = {
    'code': REGISTER_FAILED,
    'msg': 'Unable to register.'
}

UNABLE_TO_FETCH_BOOKING = '145'
__code[UNABLE_TO_FETCH_BOOKING] = {
    'code': UNABLE_TO_FETCH_BOOKING,
    'msg': 'Unable to fetch booking.'
}

INVALID_CONFIRM_BOOKING_REQUEST = '147'
__code[INVALID_CONFIRM_BOOKING_REQUEST] = {
    'code': INVALID_CONFIRM_BOOKING_REQUEST,
    'msg': 'Unable to Confirm Booking. Invalid request',
    'status_code': HTTP_400_BAD_REQUEST
}

CONFIRM_BOOKING_FAILED = '148'
__code[CONFIRM_BOOKING_FAILED] = {
    'code': CONFIRM_BOOKING_FAILED,
    'msg': 'Unable to Confirm Booking.',
    'status_code': HTTP_400_BAD_REQUEST
}

RAZORPAY_INVALID_REQUEST = '149'
__code[RAZORPAY_INVALID_REQUEST] = {
    'code': RAZORPAY_INVALID_REQUEST,
    'msg': 'Razorpay Invalid request.',
    'status_code': HTTP_400_BAD_REQUEST
}

UNABLE_TO_GENERATE_PAYMENT = '150'
__code[UNABLE_TO_GENERATE_PAYMENT] = {
    'code': UNABLE_TO_GENERATE_PAYMENT,
    'msg': 'Unable to generate payment.',
    'status_code': HTTP_400_BAD_REQUEST

}

UNABLE_TO_UPDATE_PAYMENT = '151'
__code[UNABLE_TO_UPDATE_PAYMENT] = {
    'code': UNABLE_TO_UPDATE_PAYMENT,
    'msg': 'Unable to update payment.',
    'status_code': HTTP_400_BAD_REQUEST

}

PAYMENT_ORDER_NOT_FOUND = '152'
__code[PAYMENT_ORDER_NOT_FOUND] = {
    'code': PAYMENT_ORDER_NOT_FOUND,
    'msg': 'Unable to find payment order.',
    'status_code': HTTP_400_BAD_REQUEST

}

UNABLE_TO_FETCH_PAYMENT = '153'
__code[UNABLE_TO_FETCH_PAYMENT] = {
    'code': UNABLE_TO_FETCH_PAYMENT,
    'msg': 'Unable to fetch payment.',
    'status_code': HTTP_400_BAD_REQUEST

}

INVALID_PAYMENT_REQUEST = '154'
__code[INVALID_PAYMENT_REQUEST] = {
    'code': INVALID_PAYMENT_REQUEST,
    'msg': 'Invalid payment request.',
    'status_code': HTTP_400_BAD_REQUEST

}

PAYMENT_MINIMUM_AMOUNT_ERROR = '155'
__code[PAYMENT_MINIMUM_AMOUNT_ERROR] = {
    'code': PAYMENT_MINIMUM_AMOUNT_ERROR,
    'msg': 'Minimum payment should be greter than %s.',
    'status_code': HTTP_400_BAD_REQUEST

}

HOTEL_WIFI_INFO_ERROR = '156'
__code[HOTEL_WIFI_INFO_ERROR] = {
    'code': HOTEL_WIFI_INFO_ERROR,
    'msg': 'Error while wifi information for hotel.'
}

UNABLE_TO_FETCH_WALLET_BALANCE = '157'
__code[UNABLE_TO_FETCH_WALLET_BALANCE] = {
    'code': UNABLE_TO_FETCH_WALLET_BALANCE,
    'msg': 'Unable to fetch user balance.'
}
UNABLE_TO_FETCH_WALLET_STATEMENT = '158'
__code[UNABLE_TO_FETCH_WALLET_STATEMENT] = {
    'code': UNABLE_TO_FETCH_WALLET_STATEMENT,
    'msg': 'Unable to fetch user wallet statement.'

}
HOTEL_NOT_FOUND = '159'
__code[HOTEL_NOT_FOUND] = {
    'code': HOTEL_NOT_FOUND,
    'msg': 'Hotel not found.'
}

ASSIGNMENT_ERROR = '160'
__code[ASSIGNMENT_ERROR] = {
    'code': ASSIGNMENT_ERROR,
    'msg': 'Unable to assign user.',
    'status_code': HTTP_400_BAD_REQUEST

}


NO_BOKING_SOURCE_FOUND = '161'
__code[NO_BOKING_SOURCE_FOUND] = {
    'code': NO_BOKING_SOURCE_FOUND,
    'msg': 'Please supply an booking source'
}


AXIS_ROOM_FAILURE = '162'
__code[AXIS_ROOM_FAILURE] = {
    'code': AXIS_ROOM_FAILURE,
    'msg': 'Homestay booking failed, refund will be processed shortly'
}

CONFIRM_BOOKING_FETCH_FAILED = '163'
__code[CONFIRM_BOOKING_FETCH_FAILED] = {
    'code': CONFIRM_BOOKING_FETCH_FAILED,
    'msg': 'Confirm Booking Fetch Failed'
}

# availability API error code series starts from 4001 onwards.
INVALID_ROOM_CONFIG_KEY = '4001'
__code[INVALID_ROOM_CONFIG_KEY] = {
    'code': INVALID_ROOM_CONFIG_KEY,
    'msg': 'roomconfig missing.'
}

INVALID_DATE_KEY = '4002'
__code[INVALID_DATE_KEY] = {
    'code': INVALID_DATE_KEY,
    'msg': 'checkin or checkout missing.'
}

INVALID_HOTEL_ID_KEY = '4003'
__code[INVALID_HOTEL_ID_KEY] = {
    'code': INVALID_HOTEL_ID_KEY,
    'msg': 'hotel_id missing.'

}

INVALID_ROOM_TYPE_KEY = '4004'
__code[INVALID_ROOM_TYPE_KEY] = {
    'code': INVALID_ROOM_TYPE_KEY,
    'msg': 'roomtype missing.'
}

INVALID_HOTEL_ID = '4005'
__code[INVALID_HOTEL_ID] = {
    'code': INVALID_HOTEL_ID,
    'msg': 'Invalid Hotel Id.'

}

INVALID_ROOM_TYPE = '4006'
__code[INVALID_ROOM_TYPE] = {
    'code': INVALID_ROOM_TYPE,
    'msg': 'Invalid Room Type.'
}

INVALID_DATE_FORMAT = '4007'
__code[INVALID_DATE_FORMAT] = {
    'code': INVALID_DATE_FORMAT,
    'msg': 'Invalid Date Format.'
}

INVALID_ROOM_CONFIG = '4008'
__code[INVALID_ROOM_CONFIG] = {
    'code': INVALID_ROOM_CONFIG,
    'msg': 'Invalid Room Configuration.'
}
INVALID_COUPON_CODE_KEY = '4009'
__code[INVALID_COUPON_CODE_KEY] = {
    'code': INVALID_COUPON_CODE_KEY,
    'msg': 'couponcode missing.'
}
INVALID_BID_KEY = '4010'
__code[INVALID_BID_KEY] = {
    'code': INVALID_BID_KEY,
    'msg': 'bid missing.'
}

INVALID_ORDER_ID_KEY = '4011'
__code[INVALID_ORDER_ID_KEY] = {
    'code': INVALID_ORDER_ID_KEY,
    'msg': 'order_id missing.'
}
ROOM_NOT_FOUND = '4012'
__code[ROOM_NOT_FOUND] = {
    'code': ROOM_NOT_FOUND,
    'msg': 'Room not available.'
}

INVALID_GSTIN_NUMBER = 4013
__code[INVALID_GSTIN_NUMBER] = {
    'code': INVALID_GSTIN_NUMBER,
    'msg': 'GSTIN is invalid.'
}

WALLET_BALANCE_ERROR = 4014
__code[WALLET_BALANCE_ERROR] = {
    'code': WALLET_BALANCE_ERROR,
    'msg': 'Unable to fetch balance from wallet.'
}

LOYALTY_POLICY_ERROR = 4015
__code[LOYALTY_POLICY_ERROR] = {
    'code': LOYALTY_POLICY_ERROR,
    'msg': 'Unable to fetch loyalty policy.'
}

GET_USERID_ERROR = 4016
__code[GET_USERID_ERROR] = {
    'code': GET_USERID_ERROR,
    'msg': 'Unable to fetch userid.'
}

PRICING_SERVICE_ERROR = 4017
__code[PRICING_SERVICE_ERROR] = {
    'code': PRICING_SERVICE_ERROR,
    'msg': 'Unable to fetch price.'
}

COUPON_REMOVE_ERROR = 4018
__code[COUPON_REMOVE_ERROR] = {
    'code': COUPON_REMOVE_ERROR,
    'msg': 'Unable to remove coupon.'
}

INVALID_BOOKING_REQUEST = 4019
__code[INVALID_BOOKING_REQUEST] = {
    'code': INVALID_BOOKING_REQUEST,
    'msg': 'Unable to Booking. Invalid request',
}

BOOKING_INITIATE_ERROR = 4020
__code[BOOKING_INITIATE_ERROR] = {
    'code': BOOKING_INITIATE_ERROR,
    'msg': 'Unable to initiate booking',
}

GATEWAY_NOT_FOUND = 4021
__code[GATEWAY_NOT_FOUND] = {
    'code': GATEWAY_NOT_FOUND,
    'msg': 'Unable to find %s gateway',
}

UNABLE_TO_VERIFY_PAYMENT = 4022
__code[UNABLE_TO_VERIFY_PAYMENT] = {
    'code': UNABLE_TO_VERIFY_PAYMENT,
    'msg': 'Payment verification failed.',
    'status_code': HTTP_400_BAD_REQUEST
}

BOOKING_CREATION_ERROR = 4023
__code[BOOKING_CREATION_ERROR] = {
    'code': BOOKING_CREATION_ERROR,
    'msg': 'Unable to create Booking.',
}

BOOKING_CONFIRMATION_ERROR = 4024
__code[BOOKING_CONFIRMATION_ERROR] = {
    'code': BOOKING_CONFIRMATION_ERROR,
    'msg': 'Booking confirmation failed.',
}

BOOKING_VALIDATION_ERROR = 4033
__code[BOOKING_VALIDATION_ERROR] = {
    'code': BOOKING_VALIDATION_ERROR,
    'msg': "Booking Validation Error"
}

UNABLE_TO_UPDATE_STATUS = 4025
__code[UNABLE_TO_UPDATE_STATUS] = {
    'code': UNABLE_TO_UPDATE_STATUS,
    'msg': 'Status Pending api failed.',
    'status_code': HTTP_400_BAD_REQUEST
}

UNABLE_TO_CONFIRM_PAYMENT_ON_PULL = 4026
__code[UNABLE_TO_CONFIRM_PAYMENT_ON_PULL] = {
    'code': UNABLE_TO_CONFIRM_PAYMENT_ON_PULL,
    'msg': 'failure in payment confirmation by pull on s2s call back delay.',
    'status_code': HTTP_400_BAD_REQUEST
}

COMMITTED_PRICE_NOT_SET = 4027
__code[COMMITTED_PRICE_NOT_SET] = {
    'code': COMMITTED_PRICE_NOT_SET,
    'msg': 'Committed price for the Booking request was not set.',
}


DUPLICATE_BOOKING_ERROR = 4028
__code[DUPLICATE_BOOKING_ERROR] = {
    'code': DUPLICATE_BOOKING_ERROR,
    'msg': 'Unable to create Booking the booking has already been processed.',
}

INITIATE_BOOKING_VALIDATION_ERROR = 4029
__code[INITIATE_BOOKING_VALIDATION_ERROR] = {
    'code': INITIATE_BOOKING_VALIDATION_ERROR,
    'msg': 'Error in initiate API request param',
}

CREATE_BOOKING_VALIDATION_ERROR = 4030
__code[CREATE_BOOKING_VALIDATION_ERROR] = {
    'code': CREATE_BOOKING_VALIDATION_ERROR,
    'msg': 'Create Booking Validation Error'
}

DUPLICATE_BOOKING_REQUEST = 4031
__code[DUPLICATE_BOOKING_REQUEST] = {
    'code': DUPLICATE_BOOKING_REQUEST,
    'msg': 'Unable to initiate payment on the booking, since this bid has already been processed.',
}

INCOMPLETE_GST_DETAIL = '4032'
__code[INCOMPLETE_GST_DETAIL] = {
    'code': INCOMPLETE_GST_DETAIL,
    'msg': 'Incomplete GST Detail'
}

INVALID_REQUEST_PARAM_IN_API = '4033'
__code[INVALID_REQUEST_PARAM_IN_API] = {
    'code': INVALID_REQUEST_PARAM_IN_API,
    'msg': 'Invalid request param in API'
}

PRICE_NOT_AVAILABLE_FOR_HOTEL = '5001'
__code[PRICE_NOT_AVAILABLE_FOR_HOTEL] = {
    'code': PRICE_NOT_AVAILABLE_FOR_HOTEL,
    'msg': "Price is currently not available for hotel {0}"
}

PRICE_NOT_AVAILABLE_FOR_RATE_PLAN = '5002'
__code[PRICE_NOT_AVAILABLE_FOR_RATE_PLAN] = {
    'code': PRICE_NOT_AVAILABLE_FOR_RATE_PLAN,
    'msg': "Price is currently not available for rate plan: {0}. Please select another rate plan."
}

WALLET_PAYMENT_FAILED = '5003'
__code[WALLET_PAYMENT_FAILED] = {
    'code': WALLET_PAYMENT_FAILED,
    'msg': "Payment Service could not execute wallet payment"
}

WALLET_INSUFICIENT_FUNDS = '5006'
__code[WALLET_INSUFICIENT_FUNDS] = {
    'code': WALLET_INSUFICIENT_FUNDS,
    'msg': "The wallet doesn't have sufficient funds"
}

PAYMENT_ALREADY_PRESENT = '5007'
__code[PAYMENT_ALREADY_PRESENT] = {
    'code': PAYMENT_ALREADY_PRESENT,
    'msg': "payment already present."
}

CHECKIN_GREATER_THAN_CHECKOUT = '5008'
__code[CHECKIN_GREATER_THAN_CHECKOUT] = {
    'code': CHECKIN_GREATER_THAN_CHECKOUT,
    'msg': 'requested checkin date is greater than or equal to checkout date.'
}

EMPTY_PAYLOAD_IN_CRS_MIGRATION_EVENT = '6001'
__code[EMPTY_PAYLOAD_IN_CRS_MIGRATION_EVENT] = {
    'code': EMPTY_PAYLOAD_IN_CRS_MIGRATION_EVENT,
    'msg': 'Empty payload in CRS migration event.'
}

BOOKING_UPDATE_FAILED_IN_CRS_MIGRATION = '6002'
__code[BOOKING_UPDATE_FAILED_IN_CRS_MIGRATION] = {
    'code': BOOKING_UPDATE_FAILED_IN_CRS_MIGRATION,
    'msg': 'Booking update failed in CRS migration.',
    'status_code': HTTP_500_INTERNAL_SERVER_ERROR
}

BAD_ITS_API_REQUEST = '6003'
__code[BAD_ITS_API_REQUEST] = {
    'code': BAD_ITS_API_REQUEST,
    'msg': "Bad ITS request."
}

ITS_SERVICE_UNAVAILABLE = '6004'
__code[ITS_SERVICE_UNAVAILABLE] = {
    'code': ITS_SERVICE_UNAVAILABLE,
    'msg': "ITS service not available."
}

NO_HOTELS_FOUND = '7001'
__code[NO_HOTELS_FOUND] = {
    'code': NO_HOTELS_FOUND,
    'msg': "Bad Request. Unable to Fetch Hotels"
}

EXTERNAL_SERVICE_UNAVAILABLE = '6005'
__code[EXTERNAL_SERVICE_UNAVAILABLE] = {
    'code': EXTERNAL_SERVICE_UNAVAILABLE,
    'msg': "External service not available."
}

SKU_NOT_FOUND_EXCEPTION = '6006'
__code[SKU_NOT_FOUND_EXCEPTION] = {
    'code': SKU_NOT_FOUND_EXCEPTION,
    'msg': "Sku fetch exception"
}

TRUE_CALLER_API_ERROR = '8000'
__code[TRUE_CALLER_API_ERROR] = {
    'code': TRUE_CALLER_API_ERROR,
    'msg': "Bad Truecaller API request"
}

TRUE_CALLER_SERVICE_UNAVAILABLE = '8001'
__code[TRUE_CALLER_SERVICE_UNAVAILABLE] = {
    'code': TRUE_CALLER_SERVICE_UNAVAILABLE,
    'msg': "Truecaller service not available"
}

TRUE_CALLER_FETCH_PROFILE_ERROR_USER_REJECTED = '8002'
__code[TRUE_CALLER_FETCH_PROFILE_ERROR_USER_REJECTED] = {
    'code': TRUE_CALLER_FETCH_PROFILE_ERROR_USER_REJECTED,
    'msg': "User rejected the Truecaller fetch user profile service"
}

RECAPTCHA_SERVICE_UNAVAILABLE = '8003'
__code[RECAPTCHA_SERVICE_UNAVAILABLE] = {
    'code': RECAPTCHA_SERVICE_UNAVAILABLE,
    'msg': "Google reCAPTCHA service not available."
}

RECAPTCHA_VERIFY_ERROR = '8004'
__code[RECAPTCHA_VERIFY_ERROR] = {
    'code': RECAPTCHA_VERIFY_ERROR,
    'msg': "Error while verifying via reCAPTCHA."
}

RECAPTCHA_SCORE_BELOW_THRESHOLD_ERROR = '8005'
__code[RECAPTCHA_SCORE_BELOW_THRESHOLD_ERROR] = {
    'code': RECAPTCHA_SCORE_BELOW_THRESHOLD_ERROR,
    'msg': "ReCAPTCHA score below threshold."
}

UNVERIFIED_PAH_BOOKING_LIMIT_EXCEEDED_EXCEPTION = '8006'
__code[UNVERIFIED_PAH_BOOKING_LIMIT_EXCEEDED_EXCEPTION] = {
    'code': UNVERIFIED_PAH_BOOKING_LIMIT_EXCEEDED_EXCEPTION,
    'msg': "Unverified pay at hotel booking limit reached."
}

ERROR_AVAIL_PROMOTIONS = '136'
__code[ERROR_AVAIL_PROMOTIONS] = {
    'code': ERROR_AVAIL_PROMOTIONS,
    'msg': 'Error fetching available promotions'
}