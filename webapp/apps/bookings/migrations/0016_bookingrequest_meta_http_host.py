# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0015_auto_20160418_1918'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='meta_http_host',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
