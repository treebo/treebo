from django.conf.urls import url

from apps.auth.api.v3.send_otp import SendOTPApiV3

urlpatterns = [
    url(r"^/$", SendOTPApiV3.as_view(), name='otp-send-v3'),
]
