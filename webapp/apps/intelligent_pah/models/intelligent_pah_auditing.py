from django.db import models
from dbcommon.models.hotel import Hotel
from apps.checkout.models import BookingRequest
import datetime


class Audit(models.Model):
    check_in_date = models.DateField()
    bid = models.ForeignKey(BookingRequest, on_delete=models.CASCADE)
    hotel_id = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    occupancy = models.FloatField()
    is_pah_enabled = models.BooleanField(blank=False)
