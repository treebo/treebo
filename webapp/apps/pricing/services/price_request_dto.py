class PriceRequestDto(object):
    def __init__(
            self,
            checkin_date,
            checkout_date,
            hotel_ids,
            room_configs,
            room_type=None,
            rate_plan=None,
            coupon_code=None,
            coupon_value=None,
            coupon_type=None,
            channel=None,
            logged_in_user=False,
            utm_source=None,
            wallet_applied=False):
        self.checkin_date = checkin_date
        self.checkout_date = checkout_date
        self.hotel_ids = hotel_ids
        self.room_configs = room_configs
        self.room_type = room_type
        self.rate_plan = rate_plan
        self.coupon_code = coupon_code
        self.coupon_value = coupon_value
        self.coupon_type = coupon_type
        self.channel = channel
        self.logged_in_user = logged_in_user
        self.utm_source = utm_source
        self.wallet_applied = wallet_applied


class PriceRequestDTO_V3(object):
    def __init__(
            self,
            checkin_date,
            checkout_date,
            hotel_discount_map,
            room_configs,
            room_type=None,
            rate_plan=None,
            channel=None,
            logged_in_user=False,
            utm_source=None):
        self.checkin_date = checkin_date
        self.checkout_date = checkout_date
        self.hotel_discount_map = hotel_discount_map
        self.room_configs = room_configs
        self.room_type = room_type
        self.rate_plan = rate_plan
        self.channel = channel
        self.utm_source = utm_source
        self.logged_in_user = logged_in_user
