from django.core.cache import cache
from apps.content.constants import META_CACHE_TIMEOUT
from webapp.services.common_services.random_number_generator import RandomNumberGenerator
import logging

from apps.content.meta_factory import MetaFactory

__author__ = 'ansilkareem'

logger = logging.getLogger(__name__)


class MetaCacheService(object):
    CACHE_KEY = 'meta_{0}'

    def __init__(self, page, query, ta_review_count, is_amp=False, search_dict=None):
        self.page = page
        self.query = query.lower() if query else ""
        self.key = 'meta_' + str(page)
        self.is_amp = is_amp
        self.ta_review_count = ta_review_count
        if query:
            self.key = self.key + '_' + str(query)
        if is_amp:
            self.key += '_' + str(is_amp)
        self.search_params = search_dict

    def get_meta_data(self, ):
        meta_data = None
        # TODO: ONLY Temp change BECAUSE IT WAS throwing exception decoding a
        # python2 obj
        try:
            meta_data = cache.get(self.key)
        except Exception as e:
            logger.exception(e)
        if not meta_data:
            try:
                meta_data = MetaFactory().build_meta(self.page, self.query)
                cache.set(self.key, meta_data, META_CACHE_TIMEOUT)
            except Exception as e:
                logger.exception(" Exception occured while building meta content for "
                                 "page : %s , query : %s , exception : %s" % (
                                     self.page, self.query, str(e)))
            except BaseException:
                logger.exception(
                    'Exception occured while building meta content')
        return meta_data

    def get_meta_data_v3(self, ):
        if self.page == 'search':
            if self.search_params:
                if self.search_params.get('locality'):
                    self.page = 'locality'
                    self.query = self.search_params.get('locality')
                elif self.search_params.get('landmark'):
                    self.page = 'landmark'
                    self.query = self.search_params.get('landmark')
                elif self.search_params.get('city'):
                    self.page = 'city'
                    self.query = self.search_params.get('city')
                elif self.search_params.get('hd'):
                    self.page = 'hd'
                    self.query = self.search_params.get('hd')
                else:
                    self.page = 'common'
                self.set_search_query()
        self.key += '_v3'
        try:
            meta_data = cache.get(self.key)
        except Exception as e:
            logger.error(e)
            meta_data = None
        if not meta_data:
            try:
                meta_data = MetaFactory().build_meta_v3(self.page, self.query, self.is_amp,
                                                        self.ta_review_count)
                timeout_value = META_CACHE_TIMEOUT + RandomNumberGenerator.generate_number(1, 100)
                cache.set(self.key, meta_data, timeout_value)
            except Exception as e:
                logger.exception(" Exception occured while building meta content for "
                                 "page : %s , query : %s , exception : %s" % (
                                     self.page, self.query, str(e)))
        return meta_data

    def set_cache(self, value):
        cache.set(self.key, value, META_CACHE_TIMEOUT)

    def get_cache_data(self, ):
        return cache.get(self.key)

    def delete_cache(self, ):
        cache.delete_pattern(self.CACHE_KEY.format('*'))

    def set_search_query(self):
        self.key = 'meta_' + str(self.page)
        self.key = self.key + '_' + self.query.lower()
        if self.is_amp:
            self.key += '_' + str(self.is_amp)

