from rest_framework import serializers


class PaymentOrderDetailsDTO(serializers.Serializer):
    hotel_code = serializers.CharField(
        max_length=40,
        allow_null=True,
        required=False,
        allow_blank=True)
    group_code = serializers.CharField(
        max_length=40,
        allow_null=True,
        required=False,
        allow_blank=True)
    order_id = serializers.CharField(
        max_length=40,
        allow_null=True,
        required=False,
        allow_blank=True)
