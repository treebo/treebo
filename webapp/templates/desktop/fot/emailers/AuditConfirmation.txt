Hey,

Thank you so much for taking out time to conduct this mystery audit. We really hope that you enjoy this process and have a very comfortable stay. As you plan your travel, do take a minute to read the following instructions carefully.

Link to the feedback form: https://friendsoftreebo.typeform.com/to/XZ9H1f

Some things to keep in mind for a smooth check-in:

You can check-in anytime after 12PM in the afternoon. Just go at the front desk, tell your name and show your photo ID card. The accompanying guests must also be carrying valid ID cards. Hotel reserves the right to admit or deny entry to unmarried couples.

Driver’s Licence, Aadhar Card, Passport & Voting ID card acceptable photo ID cards. Pan Card is not accepted. ID cards must not be issued by authority of same city where the hotel is.

Please note that you should NOT DISCLOSE to anyone during your stay that you are a ‘Friend of Treebo’ and have come for an audit.

You must NOT WRITE any review - positive or negative - on any review website/social media/travel portal/ regarding your audit stay.

Hotel reserves the right to admit or deny entry to unmarried couples.







We take your feedback very seriously, and expect a high-quality audit from you. Here are some tips for conducting a great audit (Please read this carefully):

Please quickly go through the form once before actual audit. We have shared the link with you below in the mail.

Be very observant, and start noticing details even before you enter the hotel. Eg- Facade, Signage boards etc.

Keep your eyes open about every detail in common areas like lobby and reception. Make sure to analyse every aspect of your stay when you are in room.

Please ensure that you give a very candid feedback. Fill the remarks sections to justify your ratings. Along with feedback, you can also send the supporting photographs (for e.g.. photo of wall paint broken, dirt at bed corner, etc) to friends@treebohotels.com

You should be able to complete you audit within 20-30 minutes. The best time to fill the feedback form is while you are in the room, or right after check-out.

Don't forget to submit your feedback within 24 hours of check-out to earn credits. You may not be able to schedule further audits in future if you fail to submit your feedback on time.

For any stay specific requirement, please call our customer support at 91-9322800100, or write to us at friends@treebohotels.com

Please click here to open your feedback form:
https://friendsoftreebo.typeform.com/to/uZ2HFS

That's it from our side.
Happy Auditing, and a very happy stay !
Thanks,
Team Treebo