# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0050_auto_20170523_1118'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='landmark',
            name='no_of_hotels_to_show',
        ),
        migrations.AddField(
            model_name='locality',
            name='enable_for_seo',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='searchlandmark',
            name='enable_for_seo',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='searchlandmark',
            name='free_text_url',
            field=models.CharField(default='', max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='searchlandmark',
            name='no_of_hotels_to_show',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
