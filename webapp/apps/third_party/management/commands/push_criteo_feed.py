from apps.third_party.service.criteo_service import GenerateCriteoCSV

__author__ = 'ansilkareem'

import logging

from django.core.management.base import BaseCommand

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "create and push criteo csv to ftp location"

    def handle(self, *args, **options):
        try:
            criteo_obj = GenerateCriteoCSV()
            criteo_obj.create_and_save_csv()
        except Exception as ex:
            logger.exception(
                'Exception occured while creating and pushing csv')
