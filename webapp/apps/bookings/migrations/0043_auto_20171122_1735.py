# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0042_auto_20171122_1626'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymentorder',
            name='booking_order_id',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='paymentorder',
            name='ps_payment_id',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='paymentorder',
            name='updated_in_booking_service',
            field=models.NullBooleanField(default=False),
        ),
    ]
