import logging

from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR

from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import HotelNotFoundException
from apps.hotels.serializers.hotel_serializer import CSHotelSerializer
from apps.hotels.service.hotel_service import HotelService
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class CSHotelDetail(TreeboAPIView):

    def get(self, request, *args, **kwargs):
        hotel_service = HotelService()
        cs_id = kwargs.get('cs_id')
        logger.info("calling v3 get hotel detail by using cs_id :%s", cs_id)
        try:
            if cs_id:
                hotel = hotel_service.get_hotel_details_using_cs_id(cs_id)
                serialized_data = CSHotelSerializer(instance=[hotel], many=True).data
            else:
                logger.info("cs_id not found")
                return Response(data='Please provide cs_id')
        except HotelNotFoundException as hnfe:
            return api_response.treebo_exception_error_response(hnfe)
        except Exception as e:
            return Response(data='something went wrong', status=HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(serialized_data)
