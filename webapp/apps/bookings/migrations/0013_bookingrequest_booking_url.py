# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0012_auto_20160414_1359'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='booking_url',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
