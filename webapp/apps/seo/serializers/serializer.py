__author__ = 'ansilkareem'

from rest_framework import serializers


class HotelDTO(serializers.Serializer):
    type = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    label = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    hotel_id = serializers.IntegerField(allow_null=True, required=False)
    hashed_hotel_id = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)
    area = serializers.DictField(allow_null=True, required=False)
    coordinates = serializers.DictField(allow_null=True, required=False)


class LocalityDTO(serializers.Serializer):
    type = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    label = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    area = serializers.DictField(allow_null=True, required=False)
    coordinates = serializers.DictField(allow_null=True, required=False)


class LandmarkDTO(serializers.Serializer):
    type = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    label = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    area = serializers.DictField(allow_null=True, required=False)
    coordinates = serializers.DictField(allow_null=True, required=False)


class TopRatedSerializer(serializers.Serializer):
    key = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    content = HotelDTO(required=False, allow_null=True, many=True)


class CitiesDTO(serializers.Serializer):
    type = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    hotel_count = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)
    label = serializers.CharField(allow_null=True, required=False)
    area = serializers.DictField(allow_null=True, required=False)


class TopCitiesSerializer(serializers.Serializer):
    key = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    content = CitiesDTO(required=False, allow_null=True, many=True)


class CategoryDTO(serializers.Serializer):
    type = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    label = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    category = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    area = serializers.DictField(allow_null=True, required=False)


class AmenityDTO(serializers.Serializer):
    type = serializers.CharField(allow_null=True, allow_blank=True, required=False)
    label = serializers.CharField(allow_null=True, allow_blank=True, required=False)
    amenity = serializers.CharField(allow_null=True, allow_blank=True, required=False)
    area = serializers.DictField(allow_null=True, required=False)


class HotelTypesSerializer(serializers.Serializer):
    key = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    content = CategoryDTO(required=False, allow_null=True, many=True)


class NearbyLocalitySerializer(serializers.Serializer):
    key = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    content = LocalityDTO(allow_null=True, required=False, many=True)


class LandmarkSerializer(serializers.Serializer):
    key = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    content = LandmarkDTO(allow_null=True, required=False, many=True)


class LocalitySerializer(serializers.Serializer):
    key = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    content = LocalityDTO(allow_null=True, required=False, many=True)


class CitiesNearbySerializer(serializers.Serializer):
    key = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    content = CitiesDTO(required=False, allow_null=True, many=True)


class PopularCitiesSerializer(serializers.Serializer):
    key = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    title = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    content = CitiesDTO(required=False, allow_null=True, many=True)


class TopReviewHotelSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)


class TopReviewUserSerializer(serializers.Serializer):
    image = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    name = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)


class TopReviewsSerializer(serializers.Serializer):
    review_text = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)
    rating_image = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)
    review_ratings = serializers.DecimalField(
        allow_null=True, required=False, decimal_places=1, max_digits=20)
    review_title = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)
    review_date = serializers.DateTimeField(format='%d %b %y')
    is_crawlable = serializers.BooleanField(required=False, default=False)
    hotel = TopReviewHotelSerializer()
    user = TopReviewUserSerializer()

class PopularAmenitiesSerializer(serializers.Serializer):
    key = serializers.CharField(allow_null=True, allow_blank=True, required=False)
    title = serializers.CharField(allow_null=True, allow_blank=True, required=False)
    content = AmenityDTO(required=False, allow_null=True, many=True)
