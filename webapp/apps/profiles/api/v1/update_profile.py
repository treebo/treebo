import logging
import datetime
from rest_framework.response import Response

from apps.profiles.service.profile_service import UserProfileService
from base import log_args
from base.views.api import TreeboAPIView
from dbcommon.models.profile import UserEmail, User
from rest_framework.authentication import BasicAuthentication
from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication

__author__ = 'devang'

logger = logging.getLogger(__name__)


class SaveProfile(TreeboAPIView):
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    profile_service = UserProfileService()

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        # add anniversary field in User model

        if not (hasattr(request, 'user')
                and request.user and request.user.is_authenticated()):
            return Response({"error": "UnAuthorized"}, status=401)

        user = request.user
        logger.debug("  saving profile for %s ", user)
        try:

            first_name, last_name, gender, dob, anniversary, email, phone, city, work_email = self.__parseRequest(
                request)
            try:
                user = User.objects.get(email=user.email)
            except User.DoesNotExist as e:
                logger.exception(" user not found for profile ")
                return Response({"error": "User not found"}, status=400)
            self.profile_service.update_profile(
                user,
                first_name,
                last_name,
                gender,
                dob,
                anniversary,
                email,
                phone,
                city,
                work_email)
            return Response({
                'status': 'success',
                'msg': 'Profile Updated successfully'
            })
        except Exception as e:
            logger.exception(" profile saving failed ")
            return Response({"error": "Unable to save profile"}, status=400)

    def __parseRequest(self, request):
        d = request.data
        # 2001-01-01
        first_name = d.get("first_name", None)
        last_name = d.get("last_name", None)
        gender = d.get("gender", None)
        anniversary = self.convert_date(d.get("anniversary", None))
        dob = self.convert_date(d.get("dob", None))
        email = d.get("email", None)
        phone = d.get("phone", None)
        city = d.get("city", None)
        work_email = d.get("work_email", None)
        return first_name, last_name, gender, dob, anniversary, email, phone, city, work_email

    def convert_date(self, date_str):
        try:
            return datetime.datetime.strptime(date_str, '%Y-%m-%d')
        except Exception:
            return None
