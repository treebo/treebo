

import io
import logging
import csv

from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.encoding import smart_str

from apps.third_party.service.trip_advisor import constants
from data_services.respositories_factory import RepositoriesFactory

logger = logging.getLogger(__name__)


class GenerateTACSVService(object):
    def create_and_mail_csv(self):
        logger.info("updating csv")
        csv_writer, csv_file = self.initilaize_csv()
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        hotel_data = hotel_repository.get_active_hotel_list()
        #hotel_data = Hotel.objects.select_related('city', 'locality', 'state').filter(status=1)
        for hotel in hotel_data:
            self.append_to_csv(csv_writer, hotel)
        self.send_csv_file(csv_file)

    def initilaize_csv(self, ):
        logger.info(" Initializing CSV for hotel inventory of Treebo")
        csv_file = io.StringIO()
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow(['Property ID',
                             'Hotel Name',
                             'Street address',
                             'city',
                             'state',
                             'country',
                             'Postal Code',
                             'latitude',
                             'longitude',
                             'url',
                             'Bucket',
                             'IN-Dmeta',
                             'IN-Mobile'])
        return csv_writer, csv_file

    def send_csv_file(self, csv_file):
        try:
            logger.info("Sending Updated Trip Advisor CSV")
            mail = EmailMessage(
                constants.TA_CSV_EMAIL_SUBJECT,
                constants.TA_CSV_EMAIL_BODY,
                settings.SERVER_EMAIL,
                settings.TA_CSV_EMAIL_ID)
            mail.attach(
                constants.TA_CSV_FILE_NAME,
                csv_file.getvalue(),
                'text/csv')
            mail.send()
            logger.info("mail sent")
        except Exception:
            logger.exception(
                "Error occured in sending  trip advisor csv email")

    def append_to_csv(self, csv_writer, hotel):
        logger.info("Appending to CSV for the Hotel ID %s", hotel.id)
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        sitemap_url = hotel_repository.get_sitemap_url(hotel)
        # https://www.treebo.com/hotels-in-koramangala/treebo-elmas-koramangala-e8LV9oJG?utm_source=TA&utm_medium=Meta&utm_campaign=cpc
        url = constants.HOTEL_HD_PAGE_URL % (constants.BASE_URL, smart_str(
            sitemap_url), constants.TA_UTM_SOURCE, constants.TA_UTM_MEDIUM, constants.TA_UTM_CAMPAIGN)
        csv_writer.writerow(
            [
                smart_str(
                    hotel.id), smart_str(
                    hotel.name), smart_str(
                    hotel.street), smart_str(
                        hotel.city.name), smart_str(
                            hotel.state.name), smart_str(
                                constants.COUNTRY), smart_str(
                                    hotel.locality.pincode), smart_str(
                                        hotel.latitude), smart_str(
                                            hotel.longitude), smart_str(url), 'B2V', '30', '15'])
