from rest_framework import serializers

from apps.pricing.models import StateTax, LuxuryTax, PreferredPromo, PromoConditions


class StateTaxSyncSerializer(serializers.ModelSerializer):
    class Meta:
        model = StateTax
        fields = (
            'tax_type',
            'tax_value',
            'luxury_tax_on_base',
            'effective_date',
            'expiry_date')


class LuxuryTaxSyncSerializer(serializers.ModelSerializer):
    class Meta:
        model = LuxuryTax
        fields = ('start_range', 'end_range', 'tax_value')


class PromoConditionSyncSerializer(serializers.ModelSerializer):
    class Meta:
        model = PromoConditions
        fields = ('id', 'condition_name', 'condition_params')


class PromoSyncSerializer(serializers.ModelSerializer):
    conditions = PromoConditionSyncSerializer(many=True)
    status = serializers.SerializerMethodField()

    class Meta:
        model = PreferredPromo
        fields = (
            'id',
            'hotel',
            'description',
            'type',
            'min_advance_days',
            'max_advance_days',
            'min_nights',
            'refundable',
            'stay_start',
            'stay_end',
            'booking_start',
            'booking_end',
            'booking_days_bitmap',
            'stay_days_bitmap',
            'discountType',
            'status',
            'conditions')

    def get_status(self, promo):
        return "Active" if promo.status == 0 else "Expired"
