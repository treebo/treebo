# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0028_auto_20160718_0946'),
    ]

    operations = [
        migrations.CreateModel(
            name='ErrorCodes',
            fields=[
                ('id',
                 models.AutoField(
                     verbose_name='ID',
                     serialize=False,
                     auto_created=True,
                     primary_key=True)),
                ('name',
                 models.CharField(
                     max_length=100)),
                ('code',
                 models.CharField(
                     max_length=100)),
                ('msg',
                 models.CharField(
                     max_length=150)),
            ],
            options={
                'db_table': 'common_error_codes',
                'verbose_name': 'Errorcode',
                                'verbose_name_plural': 'Errorcodes',
            },
        ),
    ]
