from django.http.response import HttpResponseRedirect

from base.views.template import TreeboTemplateView

__author__ = 'varunachar'


class Registration(TreeboTemplateView):
    template_name = 'register/index.html'

    def getMobileData(self, request, args, kwargs):
        if request.user and request.user.is_active and request.user.is_authenticated():
            return HttpResponseRedirect('/')
        return {
            'scripts': ['mobile/js/register.js'],
            'styles': ['mobile/css/login.css'],
            'footer': False,
            'googleLogin': True}
