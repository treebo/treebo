from rest_framework.response import Response

from apps.hotels.serializers.hotel_serializer import BasicHotelSerializer
from base.views.api import TreeboAPIView
from dbcommon.models.hotel import Hotel

class HotelList(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        hotelogix_id = kwargs.get('hotelogix_id')
        if hotelogix_id:
            hotels = [Hotel.objects.get(hotelogix_id=hotelogix_id)]
        else:
            hotels = Hotel.objects.all()
        serialized_data = BasicHotelSerializer(instance=hotels, many=True).data
        return Response(serialized_data)

