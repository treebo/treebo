def get_cs_id_from_web_hotel_id(hotel_id):
    from dbcommon.models.hotel import Hotel
    return Hotel.all_hotel_objects.get(id=hotel_id).cs_id  # return cs_id only


def get_cs_ids_from_web_hotel_ids(hotel_ids):
    from dbcommon.models.hotel import Hotel
    return Hotel.all_hotel_objects.filter(id__in=hotel_ids).values_list('cs_id', flat=True)


def get_city_id_from_cs_id(cs_id):
    from dbcommon.models.location import City
    city = City.objects.filter(cs_id=cs_id).first()
    return city.id


def get_state_id_from_cs_id(cs_id):
    from dbcommon.models.location import State
    state = State.objects.filter(cs_id=cs_id).first()
    return state.id


def get_locality_id_from_cs_id(cs_id):
    from dbcommon.models.location import Locality
    locality = Locality.objects.filter(cs_id=cs_id).first()
    return locality.id if locality else None


def get_hotel_id_from_cs_id(cs_id):
    from dbcommon.models.hotel import Hotel
    hotel = Hotel.all_hotel_objects.filter(cs_id=cs_id).first()
    return hotel.id


def get_room_id_from_cs_id(cs_id):
    from dbcommon.models.room import Room
    return Room.objects.filter(cs_id=cs_id).first()