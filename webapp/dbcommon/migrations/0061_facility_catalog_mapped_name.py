# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0060_auto_20170803_0644'),
    ]

    operations = [
        migrations.AddField(
            model_name='facility',
            name='catalog_mapped_name',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
    ]
