import logging
from datetime import datetime, timedelta

from rest_framework.response import Response

from apps.bookingstash.utils import StashUtils
from apps.common.utils import Utils
from base import log_args
from base.views.api import TreeboAPIView
from common.constants import error_codes
from apps.common import date_time_utils

logger = logging.getLogger(__name__)


class CheapestRoom(TreeboAPIView):
    @log_args(logger)
    def get(self, request, format=None):
        check_in_date, check_out_date, hashed_hotel_id, room_config = self.__parse_request(
            request)

        if hashed_hotel_id is None:
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.NO_HOTEL_ROOM_ID_SUPPLIED))

        cheapest_room = StashUtils.get_cheapest_room(
            hashed_hotel_id, check_in_date, check_out_date, room_config)

        context = {'roomtype': cheapest_room}
        return Response(context)

    def __parse_request(self, request):
        d = request.GET

        try:
            check_in_date = d.get('checkin') if 'checkin' in d and d.get(
                'checkin') else datetime.now().strftime("%Y-%m-%d")
        except ValueError:
            logger.info(
                "Invalid date format received for checkIn :" +
                d.get(
                    'checkin',
                    "None"))
            check_in_date = datetime.now().strftime("%Y-%m-%d")
        try:
            check_out_date = d.get('checkout') if 'checkout' in d and d.get(
                'checkout') else (
                datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
        except ValueError:
            logger.info(
                "Invalid date format received for checkOut :" +
                d.get(
                    'checkout',
                    "None"))
            check_out_date = (
                datetime.now() +
                timedelta(
                    days=1)).strftime("%Y-%m-%d")

        hashed_hotel_id = d.get('hotelid', None)

        room_config = []

        try:

            room_config_string = d.get('roomconfig') if 'roomconfig' in d and d.get(
                'roomconfig') else '1-0'
            room_wise_configs = room_config_string.split(',')
            for room in room_wise_configs:
                room_tuple = room.split('-')
                adults = int(room_tuple[0])
                if len(room_tuple) > 1:
                    children = int(room_tuple[1])
                else:
                    children = 0
                room_config.append((adults, children))

        except ValueError:
            logger.info(
                "Invalid roomConfig Format. Setting roomConfig to default value: [(1, 0)]")
            room_config = [(1, 0)]
        return check_in_date, check_out_date, hashed_hotel_id, room_config
