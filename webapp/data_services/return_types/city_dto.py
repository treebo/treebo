from data_services.return_types.dto import DTO


class CityDto(DTO):

    def __init__(self, properties_dict):
        super(CityDto, self).__init__(properties_dict)

    def get_sitemap_url(self):
        return self.sitemap_url


    def as_json(self):
        return dict(
            name=self.name,
            description=self.description,
            tagline=self.tagline,
            subscript=self.subscript,
            status=self.status,
            portrait_image=self.portrait_image.url if self.portrait_image else None,
            landscape_image=self.landscape_image.url if self.landscape_image else None,
            cover_image=self.cover_image.url if self.cover_image else None,
            city_latitude=float(
                self.city_latitude),
            city_longitude=float(
                self.city_longitude))
