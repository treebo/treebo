from apps.hotels.service.hotel_service import HotelService
from apps.posts.service import PostService
from apps.seo.pages.hd_page import HDPage
from apps.content.constants import PAGES


__author__ = 'ansilkareem'


class HDPageV3(HDPage):

    def __init__(self, page, query, is_amp):
        super(HDPageV3, self).__init__(page, query, is_amp)

    def build_meta(self,*args):
        super(HDPageV3, self).build_meta(*args)
