import logging
from datetime import datetime, timedelta
from django.db.models import Max
from rest_framework.response import Response

from apps.pricing.event_publishers import publish_promo
from apps.pricing.models import PreferredPromo, PromoConditions
from base import log_args
from base.views.api import TreeboAPIView
from dbcommon.models.location import State

logger = logging.getLogger(__name__)


class PromoSyncTrigger(TreeboAPIView):
    @log_args(logger)
    def get(self, request, *args, **kwargs):
        hotel_id = kwargs.get('hotel_id')
        if not hotel_id:
            return Response({"message": "Missing hotel_id."}, status=400)
        try:
            booking_start_date = request.GET.get('booking_date')
            booking_date = datetime.strptime(booking_start_date, "%Y-%m-%d")
        except BaseException:
            return Response(
                {"message": "Invalid value for booking_date query parameter"}, status=400)

        promos_for_date = PreferredPromo.objects.filter(
            booking_start=booking_date,
            status=1,
            hotel_id=hotel_id).prefetch_related('conditions').select_related('hotel')
        if promos_for_date.count() == 0:
            logger.info(
                "No Promo found for hotel_id: %s and date: %s",
                hotel_id,
                booking_date)
            return Response(
                {"message": "No Promo to sync for given booking_start_date and hotel_id"}, status=200)
        else:
            publish_promo(hotel_id, promos_for_date)
            return Response({"message": "Promo sync initiated."}, status=202)
