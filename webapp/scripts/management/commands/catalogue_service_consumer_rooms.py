import logging
import socket
from django.core.management import BaseCommand
from django.conf import settings
from kombu import Connection, Queue, Exchange

from queue_listeners.cs_message_processor import CSMessagesProcessor
from queue_listeners.cs_event_consumer import CSEventConsumer

logger = logging.getLogger(__name__)

class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            rabbit_url = settings.CATALOGUE_SERVICE_RABBITMQ_SETTINGS['url']
            exchange = Exchange("cs_exchange", type="topic")
            queue_name = settings.CS_QUEUE_NAME + "rooms"
            queues = Queue(
                name=queue_name,
                exchange=exchange,
                routing_key="com.cs.property.room.type.config")
            with Connection(rabbit_url, heartbeat=8) as conn:
                cs_message_processor = CSMessagesProcessor()
                worker = CSEventConsumer(conn, queues, cs_message_processor.process_room_message)
                worker.run()
        except Exception as e:
            logger.exception(e)
