# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
import logging
import uuid

from apps.common.data_classes.base_data_class import BaseDataClass
from apps.notifications.data_classes.email import Email

logger = logging.getLogger(__name__)


class User(BaseDataClass):
    _email = ''

    def __init__(self, uid, name, email, phone_number, country_code='91'):
        self.uid = str(uid)
        self._generated_uid = False
        if not self.uid:
            self.uid = uuid.uuid4()
            self._generated_uid = True

        self.first_name, self.last_name = User.get_name_breakup(name)

        self.email = email
        self.phone_number = phone_number
        self.country_code = country_code

    @property
    def name(self):
        return '{fn} {ln}'.format(fn=self.first_name, ln=self.last_name).strip()

    @name.setter
    def name(self, value):
        self.first_name, self.last_name = User.get_name_breakup(value)

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, value):
        if Email.is_valid_email(value):
            self._email = value

    @staticmethod
    def get_name_breakup(name):
        """ Takes name as string or an iterable. For eg: tuple('Anand', 'Kumar) or ['Anand', 'Kumar']"""
        if not name:
            name = 'guest'

        if not isinstance(name, str):
            try:
                name = ' '.join([str(char) for char in name if char])
            except TypeError:
                raise ValueError('name: {n} must be a string or iterable'.format(n=name))

        name = name.split(' ', 1)

        if not name:
            raise RuntimeError('Unable to split name: {n}. This should not happen.'.format(n=name))

        if len(name) > 1:
            fname, lname = name[0], name[1]
        else:
            fname, lname = name[0], ''

        return fname, lname

    def __repr__(self):
        return "<{kls}: {n} ({e}, {p})>".format(
            kls=self.__class__.__name__,
            n=self.name,
            e=self.email,
            p=self.phone_number,
        )

    def __str__(self):
        return "{n}{e}".format(
            n=self.name,
            e=' <{}>'.format(self.email) if self.email else '',
        )

    def __eq__(self, other):
        if isinstance(other, User):
            return hash(self) == hash(other)
        return NotImplemented

    def __hash__(self):
        return hash(tuple([self.uid, self.name, self.email, self.phone_number]))
