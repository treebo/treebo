import json
import logging

from django.http import JsonResponse

from apps.hotels import utils as hotel_utils
from apps.hotels.serializers.direction_serializer import DirectionSerializer
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.direction import Directions
from data_services.directions_repository import DirectionsRepository

logger = logging.getLogger(__name__)


class HotelDirections(TreeboAPIView):

    directions_service = RepositoriesFactory.get_directions_repository()

    def get(self, request, hashedHotelId="", format=None):

        hotelId = hotel_utils.decode_hash(hashedHotelId)
        hotelDirectionList = self.__getDirectionsQuerySet(hotelId)
        directionInformation, message = HotelDirections.getHotelDirections(
            hotelDirectionList)
        response = self.__getResponse(directionInformation, message)

        return JsonResponse(response)

    @staticmethod
    def getHotelDirections(hotelDirectionList):

        result = []
        message = "Not a valid Direction Object or No Directions Present in the list"
        try:
            serialData = DirectionSerializer(hotelDirectionList, many=True)
            serialData = json.dumps(serialData.data)
            serialData = json.loads(serialData)
            for s in serialData:
                result.append(s)
                message = "success"
                # logger.debug("direction information is : " + str(result))

        except Exception as exc:
            logger.debug(exc)
            logger.debug(message)

        return result, message

    def __getDirectionsQuerySet(self, hotelId):
        return self.directions_service.get_all_directions_for_hotel(
            hotel_id=hotelId, hotel_name=None)

    def __getResponse(self, directionInformation, message):
        response = {'message': message, 'status': 0}

        if message == 'success':
            response['directionInformation'] = directionInformation
            status = 1
            response['status'] = status

        return response
