from django.conf.urls import url

from apps.growth.api.fetch_booking_data_for_cancel import GetBookingData
from apps.growth.api.submit_cancel_api import SubmitCancel

app_name = 'growth'
urlpatterns = [url(r'^bookingdata/(?:(?P<cancel_hash>[A-Za-z0-9]+)/)?$',
                   GetBookingData.as_view(),
                   name='get-booking-data'),
               url(r'^submitcancel$',
                   SubmitCancel.as_view(),
                   name='submit-cancel'),
               ]
