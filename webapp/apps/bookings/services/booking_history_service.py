import json
import logging
from datetime import datetime

from ths_common.constants.booking_constants import BookingStatus

from apps.bookings.models import Booking as BookingModel
from apps.growth.services.growth_services import GrowthServices
from apps.hotels.helpers import CityHotelHelper
from apps.profiles.serializers.serializers import UserProfileSerializer
from apps.profiles.service.profile_service import UserProfileService
from apps.referral.service.service import ReferralService
from dbcommon.data_services.external_provider_room_service import ExternalProvideRoomService
from dbcommon.models.profile import UserReferral
from services.restclient.contentservicerestclient import ContentServiceClient
from apps.bookings.data_classes import Booking

logger = logging.getLogger(__name__)


class BookingHistoryService(object):
    def get_booking_history(self, user):
        crs_bookings = UserProfileService.get_all_bookings(user)
        previous_bookings = []
        upcoming_bookings = []
        for booking in crs_bookings:
            if booking.status.name == BookingStatus.TEMPORARY.value:
                logger.debug("Skipping booking: {bid}, as status is {status}".format(
                    bid=booking.booking_id,
                    status=booking.status))
                continue
            booking_db_model = BookingModel.objects.filter(order_id=booking.booking_id)
            if not booking_db_model:
                logger.debug("Skipping booking: {bid}, as db entry not found".format(
                    bid=booking.booking_id))
                continue
            booking_db_model = booking_db_model[0]
            try:
                checkin_date = booking.checkin.date()
            except Exception as e:
                logger.exception(
                    "Getting error {error} while retrieving checkin date".format(error=e))
                checkin_date = booking_db_model.checkin_date

            try:
                checkout_date = booking.checkout.date()
            except Exception as e:
                logger.exception(
                    "Getting error {error} while retrieving checkout date".format(error=e))
                checkout_date = booking_db_model.checkout_date
            paid_at_hotel = True if booking.stay_source == "Web (Pay@hotel)" else False
            hotel_details = booking.get_hotel_details_from_db_model(booking_db_model)
            checkin_time, checkout_time = booking.get_checkin_checkout_time_from_db_model(
                booking_db_model)
            cancel_date = booking.cancelled_timestamp.strftime(
                '%Y-%m-%d') if booking.cancelled_timestamp else None
            booking_data = dict(booking_date=booking.created_timestamp.strftime('%Y-%m-%d'),
                                cancel_date=cancel_date,
                                checkin_time=checkin_time,
                                checkout_time=checkout_time,
                                group_code="",
                                booking_code="",
                                user_detail=self._build_user_details(booking),
                                hotel_detail=hotel_details,
                                paid_at_hotel=paid_at_hotel,
                                payment_mode="Paid" if not paid_at_hotel else "Not paid",
                                payment_detail=self._build_payment_details(booking,
                                                                           booking_db_model),
                                room_config=booking.get_room_config_from_db_model(
                                    booking_db_model),
                                hotel_name=hotel_details["hotel_name"],
                                audit=booking.get_audit_from_db_model(booking_db_model),
                                room_config_string=booking.get_room_config_string_from_db(
                                    booking_db_model),
                                total_number_of_nights=(
                                    checkout_date - checkin_date).days,
                                rate_plan_meta=None,
                                wallet_applied=booking.get_wallet_applied_status_from_db_model(
                                    booking_db_model),
                                wallet_deduction=booking.get_wallet_deducted_amount_from_db_model(
                                    booking_db_model),
                                checkin_date=checkin_date.strftime('%Y-%m-%d'),
                                checkout_date=checkout_date.strftime('%Y-%m-%d'),
                                order_id=booking.booking_id,
                                id=booking.get_booking_table_id_from_db_model(booking_db_model),
                                partial_payment_link="",
                                partial_payment_amount="",
                                cancel_hash_url="",
                                cancel_hash="",
                                status="CANCELLED" if booking.status.name == BookingStatus.CANCELLED.value else "RESERVED")

            ExternalProvideRoomService.add_external_room_details_to_room_object(
                hotel_id=booking_data["hotel_detail"]["hotel_id"],
                room_dict=booking_data["room_config"],
                room_type=booking_data["room_config"]["room_type"])
            current_date = datetime.now().date()
            if checkin_date >= current_date:
                upcoming_bookings.append(booking_data)
            else:
                previous_bookings.append(booking_data)

        growth_service = GrowthServices()
        if upcoming_bookings:
            upcoming_bookings = growth_service.get_partpay_link(upcoming_bookings)
            upcoming_bookings = growth_service.get_cancel_url(upcoming_bookings)
        if previous_bookings:
            previous_bookings = growth_service.get_partpay_link(previous_bookings)
            previous_bookings = growth_service.get_cancel_url(previous_bookings)
        booking_dict = dict(upcoming=upcoming_bookings, previous=previous_bookings)
        cities = CityHotelHelper.getCityHotels()
        profile = UserProfileSerializer(instance=user).data
        return booking_dict, profile, cities, True

    def set_referral_details(self, context, user, request, is_mobile=False):
        """
        Finds the referral rewards for the user.
        Referral program has been shut down, hence this function is unsed at the moment.
        :param context:
        :param user:
        :param request:
        :param is_mobile:
        """
        referral_detail = UserReferral.objects.filter(user=user).first()
        if referral_detail is None:
            referral_detail = ReferralService.generate_referral_code(user)
        if referral_detail:
            content = ContentServiceClient.getValueForKey(
                request, "referral_share_content", "1")
            all_rewards = ReferralService.get_referrer_rewards_for_user(user)
            logger.debug('All Approved rewards: %s', all_rewards)
            referral = {
                'referral_code': referral_detail.referral_code,
                'shareurl': referral_detail.shareurl,
                'total_signup': all_rewards['total_signup'],
                'total_reward_earned': all_rewards['total_reward_earned'],
                'all_referral_rewards': all_rewards['all_referral_rewards'],
            }
            referral.update({
                "share": content
            })
            context['referral'] = referral if not is_mobile else json.dumps(
                referral)

    @staticmethod
    def _build_user_details(booking: Booking):
        gst_details = None
        if booking.booking_owner.gst_details:
            address_obj = booking.booking_owner.gst_details.address
            logger.info("address_obj details: {}".format(address_obj))
            gst_details = {
                "gstin": booking.booking_owner.gst_details.gstin_num,
                "organization_name": booking.booking_owner.gst_details.legal_name,
                "organization_address": "{f1}, {f2}, {city}".format(f1=address_obj.field1,
                                                                    f2=address_obj.field2,
                                                                    city=address_obj.city)
            }

        user_detail = {
            "name": booking.booking_owner.first_name,
            "email": booking.booking_owner.email,
            "contact_number": booking.booking_owner.phone_number,
            "gst_details": gst_details
        }
        return user_detail

    @staticmethod
    def _build_payment_details(booking: Booking, booking_db_model: BookingModel):
        wallet_deduction = booking.get_wallet_deducted_amount_from_db_model(booking_db_model)
        payment_detail = {
            "room_price": str(booking.pre_tax_amount),
            "tax": str(booking.tax_amount),
            "total": str(booking.total_amount),
            "external_pg_share": str(round(booking.total_amount - wallet_deduction, 2)),
            "paid_amount": str(booking.paid_amount),
            "discount": str(round(booking.get_discount_amount_from_db_model(booking_db_model), 2)),
            "voucher_amount": str(
                round(booking.get_voucher_amount_from_db_model(booking_db_model), 2)),
            "member_discount": str(
                round(booking.get_member_discount_from_db_model(booking_db_model), 2)),
            "coupon_code": booking.get_coupon_code_from_db_model(booking_db_model),
            "cancellation_charges": '',
            "rate_plan": booking.get_rate_plan_from_db_model(booking_db_model),
            "pending_amount": str(booking.payable_amount),
            "wallet_applied": booking.get_wallet_applied_status_from_db_model(booking_db_model),
            "wallet_deduction": wallet_deduction,
        }
        return payment_detail
