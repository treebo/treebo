import json
import logging
import socket

import requests
from django.conf import settings

from apps.common.thread_local_utils import get_current_request_id
from webapp.apps.common.create_threads import create_thread


logger = logging.getLogger(__name__)


class SlackAlertService():
    WEB_CHANNEL = "WEB_CHANNEL"
    INVENTORY_CHANNEL = "INVENTORY_CHANNEL"
    DEFAULT_CHANNEL = "DEFAULT_CHANNEL"
    AUTH_ALERTS = "AUTH_ALERTS"
    WEATHER_ALERT = "WEATHER_ALERT"
    LOYALTY_ALERT = "LOYALTY_ALERT"
    CS_MESSAGES_ALERT = "CS_MESSAGES_ALERT"
    DIRECT_APP_ALERTS = "DIRECT_APP_ALERTS"
    SEARCH_SEO_ALERTS = "SEARCH_SEO_ALERTS"
    DIRECT_PAYMENT_UPDATE_ALERTS = "DIRECT_PAYMENT_UPDATE_ALERTS"
    INVALID_DISCOUNT_COUPON = "INVALID_DISCOUNT_COUPON"
    DIRECT_STAGING_ALERT_CHANNEL = "DIRECT_STAGING_ALERT_CHANNEL"

    ALERT_CHANNEL_HOOKS = {
        WEB_CHANNEL: "https://hooks.slack.com/services/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",
        INVENTORY_CHANNEL: "https://hooks.slack.com/services/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",
        DEFAULT_CHANNEL: "https://hooks.slack.com/services/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",
        AUTH_ALERTS: "https://hooks.slack.com/services/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",
        WEATHER_ALERT: "https://hooks.slack.com/services/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",
        LOYALTY_ALERT: 'https://hooks.slack.com/services/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',
        CS_MESSAGES_ALERT: 'https://hooks.slack.com/services/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',
        DIRECT_APP_ALERTS: "https://hooks.slack.com/services/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",
        SEARCH_SEO_ALERTS: "https://hooks.slack.com/services/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",
        DIRECT_PAYMENT_UPDATE_ALERTS: "https://hooks.slack.com/services/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",
        INVALID_DISCOUNT_COUPON: "https://hooks.slack.com/services/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",
        DIRECT_STAGING_ALERT_CHANNEL: "https://hooks.slack.com/services/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"
    }

    HOST_NAME = socket.gethostname()

    @staticmethod
    def web_alert(text, *args, **kwargs):
        try:
            final_text = text % args
            WEB_ALERTS_CHANNEL = "https://hooks.slack.com/services/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"
            url = WEB_ALERTS_CHANNEL
            payload = {"text": '`' +
                               str(settings.ENVIRONMENT).upper() +
                               "`: " +
                               final_text, "username": 'web-bot'}
            json_string = json.dumps(payload, default=lambda o: o.__dict__)
            headers = {'content-type': 'application/json'}
            requests.post(url, data=json_string, headers=headers)
        except Exception as e:
            logger.error("Error while alerting in slack(Ignoring) %s", e)

    @staticmethod
    def publish_alert(text, channel=DIRECT_APP_ALERTS):
        """
        Publish the alert to a channel. if channel does not exist, then it will publish to web-alert channel
        :param text:
        :param channel:
        """
        if not (str(settings.ENVIRONMENT).upper() in ['PROD', 'STAGING', 'DEV', 'PREPROD']):
            return

        if str(settings.ENVIRONMENT).upper() in ['STAGING', 'DEV', 'PREPROD']:
            channel = SlackAlertService.DIRECT_STAGING_ALERT_CHANNEL

        payload = {
            "blocks": [
                {
                    "type": "section",
                    "fields": [
                        {
                            "type": "mrkdwn",
                            "text": "*Environment:*\n`{}`".format(str(settings.ENVIRONMENT).upper())
                        },
                        {
                            "type": "mrkdwn",
                            "text": "*RequestId:*\n`{}`".format(get_current_request_id())
                        },
                        {
                            "type": "mrkdwn",
                            "text": "*HostId:*\n`{}`".format(SlackAlertService.HOST_NAME)
                        }
                    ]
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "*Message:*\n{}".format(text)
                    }
                }
            ],
            "username": 'web-bot'
        }
        json_string = json.dumps(payload, default=lambda o: o.__dict__)
        headers = {'content-type': 'application/json'}
        try:
            requests.post(
                SlackAlertService.ALERT_CHANNEL_HOOKS.get(
                    channel,
                    SlackAlertService.ALERT_CHANNEL_HOOKS.get(
                        SlackAlertService.DEFAULT_CHANNEL)),
                data=json_string,
                headers=headers,
                timeout=settings.SLACK_ALERT_REQUEST_TIMEOUT)
        except requests.RequestException as e:
            logger.exception("Unable to publish alert to channel : %s", text)
        except Exception as e:
            logger.exception("Unable to publish alert to channel : %s", text)

    @staticmethod
    def review_alert(text, *args, **kwargs):
        try:
            final_text = text % args
            url = settings.SLACK_CHANNEL_REVIEW_ALERT_URL
            payload = {"text": '`' +
                               str(settings.ENVIRONMENT).upper() +
                               "`: " +
                               final_text}
            json_string = json.dumps(payload, default=lambda o: o.__dict__)
            headers = {'content-type': 'application/json'}
            requests.post(url, data=json_string, headers=headers)
        except Exception as e:
            logger.error("Error while alerting in slack(Ignoring) %s", e)

    @classmethod
    @create_thread
    def send_slack_alert_for_exceptions(cls, status, request_param, dev_msg="", api_name="",
                                        class_name="", message=""):

        cls.publish_alert(
            "Error code `%s` from %s *%s* class `%s` reason `%s` and request %s " % (
                status,
                api_name,
                dev_msg,
                class_name,
                str(message).strip('\n'),
                str(request_param)
            ),
            channel=cls.DIRECT_APP_ALERTS)

    @classmethod
    @create_thread
    def send_slack_alert_for_third_party(cls, status, class_name, url, reason="", data=""):

        cls.publish_alert(
            "Error code `%s` from `%s` url %s for reason => `%s` and data %s" % (
                status, class_name, str(url), str(reason), str(data)
            ),
            channel=cls.DIRECT_APP_ALERTS)

    @classmethod
    @create_thread
    def send_cs_error_msg_to_slack(cls, cs_id="", error_msg="", class_name=""):
        cls.publish_alert("Exception while storing cs property with cs id %s with error:[%s] \
                          in class:[%s]" % (str(cs_id), error_msg, class_name),
                          cls.CS_MESSAGES_ALERT)

    @classmethod
    @create_thread
    def send_slack_alert_for_search_seo(cls, request_param, api_name=""):
        cls.publish_alert(
            "`No Hotel Found` for request %s in api `%s`" % (
                str(request_param),
                api_name
            ),
            channel=cls.SEARCH_SEO_ALERTS)

    @classmethod
    @create_thread
    def send_slack_alert_for_payment_update_failure(cls, exc, order_id, hotel_name):
        cls.publish_alert(
            "Exception while updating payment for booking `%s` and hotel_name `%s` => `%s`" % (
                str(order_id),
                str(hotel_name),
                str(exc)
            ),
            channel=cls.DIRECT_PAYMENT_UPDATE_ALERTS)

    @classmethod
    def send_slack_alert_for_invalid_discount_failure(cls, status, dev_msg="", api_name="", class_name="", message=""):
        cls.publish_alert(
            "%s Error code `%s` from %s *%s* class `%s` reason `%s` " % (
                str(settings.ENVIRONMENT).upper(), status, api_name, dev_msg, class_name, str(message).strip('\n')
            ),
            channel=cls.INVALID_DISCOUNT_COUPON)
