from apps.checkout.models import BookingRequest
import logging

logger = logging.getLogger(__name__)


class CheckoutHandler():
    def get_booking_status(self, booking_request, user, wallet_payment_success,auth_id=None):
        if booking_request.wallet_applied:
            if  auth_id or user.is_authenticated():
                if wallet_payment_success:
                    booking_status = BookingRequest.COMPLETED
                else:
                    booking_status = BookingRequest.PART_SUCCESS
            else:
                booking_status = BookingRequest.PART_SUCCESS
        else:
            booking_status = BookingRequest.COMPLETED
        logger.info(
            "get_booking_status bid %s user %s wallet payment success %s got status %s ",
            booking_request.id,
            user,
            wallet_payment_success,
            booking_status)
        return booking_status
