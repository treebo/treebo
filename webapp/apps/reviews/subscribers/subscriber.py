import logging

from apps.reviews.subscribers.pub_sub import PubSub
from apps.reviews.tasks import fetch_and_update_reviews
from apps.third_party.service.trip_advisor.fetch_reviews_service import TAReviewAPIService

logger = logging.getLogger(__name__)


class Subscriber(PubSub):

    external_service = TAReviewAPIService()

    def __init__(self):
        super(Subscriber, self).__init__()
        self.pubsub.psubscribe("*keyevent*:expired")

    def subscribe(self,):
        try:
            for msg in self.pubsub.listen():
                logger.info("Recieved expiry task %s", msg)
                key = str(msg['data'])
                if key.startswith('REVIEW'):
                    # call external service and update db and cache
                    fetch_and_update_reviews.delay(key)

        except Exception as ex:
            logger.exception('Exception occured in subscriber')
