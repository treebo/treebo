import logging

from django.core.management import BaseCommand

from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from common.constants import common_constants as const
from data_services.hotel_repository import HotelRepository

from webapp.apps.reviews.tasks import fetch_and_update_reviews_through_cron

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, *args, **options):
        logger.info("Starting to load the review cache for all hotels")
        try:
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            hotels = hotel_repository.get_active_hotel_list()
            for hotel in hotels:
                logger.info("Loading cache for the hotel : %s", hotel.id)
                try:
                    fetch_and_update_reviews_through_cron.apply_async(args=[hotel.id], countdown=5)
                except BaseException:
                    logger.exception(
                        'exception occured for hotel id: %s ', hotel.id)

        except BaseException:
            logger.exception('exception occured in handler - Review Cache')
        logger.info('Loading Complete For review cache')
