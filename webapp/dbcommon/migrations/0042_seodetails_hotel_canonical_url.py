# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0041_auto_20160926_1751'),
    ]

    operations = [
        migrations.AddField(
            model_name='seodetails',
            name='hotel_canonical_url',
            field=models.CharField(max_length=500, blank=True),
        ),
    ]
