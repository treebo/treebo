from decimal import Decimal

from data_services.respositories_factory import RepositoriesFactory
from .nearby_poi_model_services import *
from geopy.distance import great_circle
from data_services.hotel_repository import HotelRepository
import logging

logger = logging.getLogger(__name__)


class FetchPOIForHotel():

    def fetch_poi_for_hotel(self, hotel_id):
        nearby_poi_model_services = NearbyPOIModelServices()
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        try:
            hotel = hotel_repository.get_hotel_by_id_from_web(hotel_id=hotel_id)
        except BaseException:
            hotel = None
        hotel_coordinate = (hotel.latitude, hotel.longitude)
        poi_list_for_hotel = nearby_poi_model_services.fetch_all_poi()
        poi_categories = nearby_poi_model_services.fetch_all_poi_categories()
        poi_cat_list = []
        for poi_category in poi_categories:
            number_cap = nearby_poi_model_services.get_number_cap(
                poi_category.id)
            if poi_category.is_city_mapped:
                poi_cat_wise_list = self.populate_city_wise(
                    hotel_coordinate, poi_category.id, poi_list_for_hotel, hotel.city.id, number_cap)
            elif poi_category.is_locality_mapped:
                poi_cat_wise_list = self.populate_locality_wise(
                    hotel_coordinate,
                    poi_category.id,
                    poi_list_for_hotel,
                    hotel.locality.id,
                    number_cap)
            else:
                distance_cap = nearby_poi_model_services.get_distance_cap_for_category(
                    poi_category.id)
                poi_cat_wise_list = self.populate(
                    hotel_coordinate,
                    poi_category.id,
                    poi_list_for_hotel,
                    distance_cap,
                    number_cap)
            if len(poi_cat_wise_list) == 0:
                continue
            poi_cat_dict = {}
            poi_cat_dict['title'] = poi_category.category_name
            poi_cat_dict['items'] = poi_cat_wise_list
            poi_cat_list.append(poi_cat_dict)
        return poi_cat_list

    def populate(
            self,
            hotel_coordinate,
            category_id,
            poi_raw_list,
            distance_cap,
            number_cap):
        threshold = distance_cap
        poi_raw_list = poi_raw_list.filter(category__id=category_id)
        poi_list = []
        for poi in poi_raw_list:
            poi_dict = {}
            poi_coordinate = (poi.poi_lat, poi.poi_lng)
            distance = great_circle(hotel_coordinate, poi_coordinate).km
            poi_dict['name'] = poi.poi_name
            poi_dict['distance'] = self.format_distance(distance)
            poi_list.append(poi_dict)
        poi_list_final = []
        for poi in poi_list:
            if poi['distance'] <= threshold:
                poi_list_final.append(poi)
        poi_list_final = sorted(poi_list_final, key=lambda x: x['distance'])
        return poi_list_final[:number_cap]

    def populate_city_wise(
            self,
            hotel_coordinate,
            category_id,
            poi_raw_list,
            city_id,
            number_cap):
        poi_raw_list = poi_raw_list.filter(
            category__id=category_id).filter(
            city=city_id)
        poi_list = []
        for poi in poi_raw_list:
            poi_dict = {}
            poi_coordinate = (poi.poi_lat, poi.poi_lng)
            distance = great_circle(hotel_coordinate, poi_coordinate).km
            poi_dict['name'] = poi.poi_name
            poi_dict['distance'] = self.format_distance(distance)
            poi_list.append(poi_dict)
        poi_list = sorted(poi_list, key=lambda x: x['distance'])
        return poi_list[:number_cap]

    def populate_locality_wise(
            self,
            hotel_coordinate,
            category_id,
            poi_raw_list,
            locality_id,
            number_cap):
        poi_raw_list = poi_raw_list.filter(
            category__id=category_id, locality_id=locality_id)
        poi_list = []
        for poi in poi_raw_list:
            poi_dict = {}
            poi_coordinate = (poi.poi_lat, poi.poi_lng)
            distance = great_circle(hotel_coordinate, poi_coordinate).km
            poi_dict['name'] = poi.poi_name
            poi_dict['distance'] = self.format_distance(distance)
            poi_list.append(poi_dict)
        poi_list = sorted(poi_list, key=lambda x: x['distance'])
        return poi_list[:number_cap]

    def format_distance(self, distance):
        ONEPLACE = Decimal(10) ** -1
        return Decimal(distance).quantize(ONEPLACE)
