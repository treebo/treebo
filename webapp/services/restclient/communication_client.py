import logging
import requests
from django.conf import settings
from apps.common.slack_alert import SlackAlertService as slack_alert

logger = logging.getLogger(__name__)


class CommunicationClient:
    SEND_COMMUNICATION_API = "/communication/api/v2/send"

    def send_communication(self, identifier: str, context_data: dict, receivers: list = None,
                           to_emails: list = None, reply_to: str = None, from_email: str = None,
                           subject: str = None, attachments: list = None):
        payload = dict(identifier=identifier,
                       context_data=context_data,
                       sms=dict(receivers=receivers) if receivers else None,
                       whatsapp=dict(receivers=receivers) if receivers else None,
                       email=dict(to_emails=to_emails,
                                  reply_to=reply_to,
                                  from_email=from_email,
                                  subject=subject,
                                  attachments=attachments if attachments else []) if to_emails and
                                                                                     subject else None)
        self.__make_call(method="POST", page_name=self.SEND_COMMUNICATION_API, data=payload)

    @staticmethod
    def __make_call(method, page_name, data, params=''):
        url = settings.COMMUNICATION_API_HOST + page_name
        logger.info("communication client %s , %s,  %s", page_name,
                    url, data)
        response = requests.request(
            method,
            url,
            params=params,
            json=data,
            allow_redirects=True)

        if not response or not response.ok:
            slack_alert.send_slack_alert_for_third_party(status=response.status_code,
                                                         class_name=CommunicationClient.__class__.__name__,
                                                         url=url,
                                                         reason=response.json().get('error'),
                                                         data=data
                                                         )
