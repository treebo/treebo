# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fot', '0008_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fot',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'FOT User',
                'verbose_name_plural': 'FOT Users'},
        ),
        migrations.AlterModelOptions(
            name='fotconfig',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'FOT Config',
                'verbose_name_plural': 'FOT Config'},
        ),
        migrations.AlterModelOptions(
            name='fothotel',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'FOT Hotel',
                'verbose_name_plural': 'FOT Hotels'},
        ),
        migrations.AlterModelOptions(
            name='fothotelblackout',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'FOT Hotel blackout dates',
                'verbose_name_plural': 'FOT Hotel blackout dates'},
        ),
        migrations.AlterModelOptions(
            name='fotreservationsrequest',
            options={
                'default_permissions': (
                    'add',
                    'change',
                    'delete',
                    'read'),
                'verbose_name': 'FOT Hotel reservation',
                'verbose_name_plural': 'FOT Hotel reservations'},
        ),
    ]
