import logging
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.change_password_dto import ChangePasswordTokenDTO
from apps.auth.serializer.user_serializer import UserSerializer
from apps.auth.services.password_service import PasswordService
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.custom_exception import UserUnauthroized
from apps.profiles.service.user_service import UserService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes
import traceback

logger = logging.getLogger(__name__)


class ChangePasswordApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    password_service = PasswordService()
    user_service = UserService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            if not (hasattr(request, 'user')
                    and request.user and request.user.is_authenticated()):
                logger.info(
                    "In change password, user not present in request, returning")
                response = api_response.treebo_exception_error_response(
                    UserUnauthroized())
            else:
                change_password_dto = ChangePasswordTokenDTO(data=request.data)
                if not change_password_dto.is_valid():
                    logger.error("change password invalid request")
                    return api_response.invalid_request_error_response(
                        change_password_dto.errors, ChangePasswordApi.__name__)
                user, token = self.password_service.change_password(
                    request, change_password_dto.data)
                self.user_service.login_user(request, user)
                user_dto = UserSerializer(instance=user)
                user_data = user_dto.data
                user_data['token'] = token
                response = api_response.prep_success_response(user_data)
        except TreeboException as e:
            logger.exception("treebo change password exception ")
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception("internal error change password exception")
            response = api_response.internal_error_response(error_codes.get(
                error_codes.PASSWORD_NOT_CHANGED)['msg'], ChangePasswordApi.__name__)
        return response
