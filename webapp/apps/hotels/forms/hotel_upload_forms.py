from django import forms
from django.core.validators import RegexValidator

from django.utils.safestring import mark_safe

from dbcommon.models.facilities import Facility


class HotelLiveForm(forms.Form):
    excel = forms.FileField(
        label='Launch Pad Excel',
        required=True,
        widget=forms.FileInput,
        help_text=mark_safe("Please copy the contents of launchpad into a new excel sheet and upload"),
    )
    images = forms.FileField(
        label='Zipped Images',
        required=True,
        widget=forms.FileInput,
        help_text=mark_safe("Please upload zip with a separate folder for each room type"),
    )

    facilities = forms.ModelMultipleChoiceField(
        required=True,
        widget=forms.CheckboxSelectMultiple,
        queryset=Facility.objects.all().exclude(
            categories__name__icontains="in room facilities"),
        help_text='Please Select only Hotel Amenities - '
        'Not the Specific Room Amenities')

    hotelogix_id = forms.IntegerField(widget=forms.NumberInput)
    hotelogix_name = forms.CharField(widget=forms.TextInput, help_text=mark_safe(
        "Enter the name configured in Hx without the Hx Id entered above, e.g. Hotel Nova"))
    hotel_seo_meta_title = forms.CharField(widget=forms.Textarea)
    hotel_seo_meta_tags = forms.CharField(widget=forms.Textarea)
    city_desc = forms.CharField(
        widget=forms.Textarea,
        label='City Description',
        required=False,
        help_text=mark_safe('Enter value only if new city is being added'))
    city_image = forms.FileField(
        widget=forms.FileInput,
        label='Home page image of the city',
        required=False,
        help_text=mark_safe('Add image only if new city is being added'))
    city_seo_meta_title = forms.CharField(
        widget=forms.Textarea,
        required=False,
        help_text=mark_safe("Enter value only if new city is being added"))
    city_seo_meta_tags = forms.CharField(
        widget=forms.Textarea,
        required=False,
        help_text=mark_safe("Enter value only if new city is being added"))
    hotel_mm_code = forms.CharField(
        widget=forms.TextInput,
        label='MM Hotel Code',
        required=True,
        validators=[
            RegexValidator(
                regex='H\d{4}',
                message='Hotel MM Code is of format H0001',
                code='invalid_mm_code',
            )])
    hotel_mm_acacia_room_code = forms.CharField(
        widget=forms.TextInput,
        label='MM Acacia Room Code',
        required=True,
        initial='NA',
        help_text=mark_safe("Leave as NA if this room type is not supported in the hotel"),
        validators=[
            RegexValidator(
                regex='(NA)|(RM\d{3})',
                message='Room MM Code is of format RM001',
                code='invalid_mm_code',
            )])
    hotel_mm_oak_room_code = forms.CharField(
        widget=forms.TextInput,
        label='MM Oak Room Code',
        initial='NA',
        help_text=mark_safe("Leave as NA if this room type is not supported in the hotel"),
        validators=[
            RegexValidator(
                regex='(NA)|(RM\d{3})',
                message='Room MM Code is of format RM001',
                code='invalid_mm_code',
            )])
    hotel_mm_maple_room_code = forms.CharField(
        widget=forms.TextInput,
        label='MM Maple Room Code',
        initial='NA',
        help_text=mark_safe("Leave as NA if this room type is not supported in the hotel"),
        validators=[
            RegexValidator(
                regex='(NA)|(RM\d{3})',
                message='Room MM Code is of format RM001',
                code='invalid_mm_code',
            )])
    hotel_mm_mahogany_room_code = forms.CharField(
        widget=forms.TextInput,
        label='MM Mahogany Code',
        initial='NA',
        help_text=mark_safe("Leave as NA if this room type is not supported in the hotel"),
        validators=[
            RegexValidator(
                regex='(NA)|(RM\d{3})',
                message='Room MM Code is of format RM001',
                code='invalid_mm_code',
            )])
    acacia_room_price = forms.IntegerField(
        widget=forms.NumberInput,
        required=True,
        initial=0,
        min_value=0,
        help_text=mark_safe("Enter value as 0 if this room type is not supported by this hotel"),
    )
    oak_room_price = forms.IntegerField(
        widget=forms.NumberInput,
        required=True,
        initial=0,
        min_value=0,
        help_text=mark_safe("Enter value as 0 if this room type is not supported by this hotel"),
    )
    maple_room_price = forms.IntegerField(
        widget=forms.NumberInput,
        required=True,
        initial=0,
        min_value=0,
        help_text=mark_safe("Enter value as 0 if this room type is not supported by this hotel"),
    )
    mahogany_room_price = forms.IntegerField(
        widget=forms.NumberInput,
        required=True,
        initial=0,
        min_value=0,
        help_text=mark_safe("Enter value as 0 if this room type is not supported by this hotel"),
    )


class RoomAmenityForm(forms.Form):
    acacia_facilities = forms.ModelMultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple,
        queryset=Facility.objects.filter(
            categories__name__icontains="in room facilities"),
        help_text='Please select amenities only for acacia room if the '
        'ACACIA Room Type is available')

    oak_facilities = forms.ModelMultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple,
        queryset=Facility.objects.filter(
            categories__name__icontains="in room facilities"),
        help_text='Please select amenities only for oak room if the '
        'OAK Room Type is available')

    maple_facilities = forms.ModelMultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple,
        queryset=Facility.objects.filter(
            categories__name__icontains="in room facilities"),
        help_text='Please select amenities only for maple room if the '
        'MAPLE Room Type is available')

    mahogany_facilities = forms.ModelMultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple,
        queryset=Facility.objects.filter(
            categories__name__icontains="in room facilities"),
        help_text='Please select amenities only for mahogany room if the '
        'MAHOGANY Room Type is available')


class ConfirmHotelForm(forms.Form):
    confirm_upload_dummy = forms.HiddenInput()


class ImageTaggingForm(forms.Form):
    image_tag = forms.TextInput()
    is_showcase_image = forms.BooleanField(widget=forms.CheckboxInput)
