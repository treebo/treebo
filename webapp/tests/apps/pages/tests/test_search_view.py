import json
import mock
from datetime import datetime
from django.test import SimpleTestCase

from apps.pages.views import search
from apps.pages.views.search import Search


class SearchViewBasicTest(SimpleTestCase):
    def setUp(self):
        self.test_object = Search()

    def test_getData(self):
        with mock.patch.object(Search, '_Search__getSearchData') as mock_get_search_data:
            mock_get_search_data.return_value = {"type": "desktop"}
            mock_request = mock.Mock()
            context = self.test_object.getData(mock_request, None, None)
            self.assertEqual({
                "type": "desktop",
                        "styles": ["desktop/css/searchresults.css"],
                        "scripts": ["desktop/js/searchresults.js"],
                        "googleMaps": True
            }, context)

    def test_getMobileData(self):
        with mock.patch.object(Search, '_Search__getSearchData') as mock_get_search_data:
            mock_get_search_data.return_value = {"type": "mobile"}
            mock_request = mock.Mock()
            context = self.test_object.getMobileData(mock_request, None, None)
            self.assertEqual({
                "type": "mobile",
                        "styles": ["mobile/css/searchresults.css"],
                        "scripts": ["mobile/js/searchresults.js"],
            }, context)


class SearchViewGenerateContextTest(SimpleTestCase):
    def setUp(self):
        self.test_object = Search()
        self.test_object.request = mock.Mock()
        self.locality_list = []
        self.facility_list = []
        self.checkout_date, self.checkin_date = datetime(
            2016, 8, 15), datetime(2016, 8, 12)
        self.parent_query = None
        self.is_locality_search = False
        self.cityOrStateOrHotelNotSearchedExplicitly = False

        self.content_get_value_for_key_patcher = mock.patch.object(
            search.ContentServiceClient, 'getValueForKey')
        self.get_city_hotels_patcher = mock.patch.object(
            search.CityHotelHelper, 'getCityHotels')
        self.mock_get_content_value = self.content_get_value_for_key_patcher.start()
        self.mock_get_city_hotels = self.get_city_hotels_patcher.start()
        self.mock_get_content_value.return_value = None
        self.mock_get_city_hotels.return_value = [mock.Mock()]

    def tearDown(self):
        self.content_get_value_for_key_patcher.stop()
        self.get_city_hotels_patcher.stop()

    def test_generate_context_sort_options_non_locality_search(self):
        context = self.test_object._Search__generateContext(
            self.checkin_date,
            self.checkout_date,
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            self.locality_list,
            self.facility_list,
            mock.Mock(),
            self.parent_query,
            self.is_locality_search,
            self.cityOrStateOrHotelNotSearchedExplicitly,
            mock.Mock())
        filter_data = json.loads(context["filterData"])
        self.assertIsNone(filter_data["sortOptions"].get("distance"))
        self.assertEqual(len(filter_data["sortArr"]), 2)
        self.assertIsNone(context.get("localityName"))

    def test_generate_context_sort_options_locality_search(self):
        self.is_locality_search = True
        mock_locality = mock.Mock()
        context = self.test_object._Search__generateContext(
            self.checkin_date,
            self.checkout_date,
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            self.locality_list,
            self.facility_list,
            mock.Mock(),
            self.parent_query,
            self.is_locality_search,
            self.cityOrStateOrHotelNotSearchedExplicitly,
            mock_locality)
        filter_data = json.loads(context["filterData"])
        self.assertIsNotNone(filter_data["sortOptions"].get("distance"))
        self.assertEqual(len(filter_data["sortArr"]), 3)
        self.assertEqual(context.get("localityName"), mock_locality)

    def test_generate_context_without_parent_query(self):
        self.parent_query = []
        context = self.test_object._Search__generateContext(
            self.checkin_date,
            self.checkout_date,
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            self.locality_list,
            self.facility_list,
            mock.Mock(),
            self.parent_query,
            self.is_locality_search,
            self.cityOrStateOrHotelNotSearchedExplicitly,
            mock.Mock())
        self.assertEqual(len(context["breadCrumbs"]), 1)

    def test_generate_context_with_parent_query(self):
        self.test_object.room_config_str = "1-0,2-0"
        self.parent_query = [
            {'name': "Jaipur", 'lat': "12.123", 'long': "72.23"},
            {'name': "HSR", 'lat': "12.23", 'long': "70.12"},
        ]
        context = self.test_object._Search__generateContext(
            self.checkin_date,
            self.checkout_date,
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            self.locality_list,
            self.facility_list,
            mock.Mock(),
            self.parent_query,
            self.is_locality_search,
            self.cityOrStateOrHotelNotSearchedExplicitly,
            mock.Mock())
        self.assertEqual(len(context["breadCrumbs"]), 3)

    def test_generate_context_marketing_banner_position(self):
        self.mock_get_content_value.return_value = {
            "value": "test marketing banner"}
        self.parent_query = []
        mock_results = [mock.Mock() for _ in range(15)]
        context = self.test_object._Search__generateContext(
            self.checkin_date,
            self.checkout_date,
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            mock_results,
            mock.Mock(),
            mock.Mock(),
            mock.Mock(),
            self.locality_list,
            self.facility_list,
            mock.Mock(),
            self.parent_query,
            self.is_locality_search,
            self.cityOrStateOrHotelNotSearchedExplicitly,
            mock.Mock())
        self.assertEqual(context["marketBanner"]["position"], 8)
