'use strict';

import $ from 'jquery';
import BaseView from '../treebolibs/baseview';
import logger from '../treebolibs/logger';
import parsley from 'parsleyjs';
import _ from 'underscore';
import treejax from '../treebolibs/treejax';
import selectize from 'selectize';
import '../../css/fot/signup_fot.less';

let SignupFotView = Object.create(BaseView);
let $loader = $("#commonLoader");
let currentStep = 1;

SignupFotView.pageLoaded = () => {

    $('#cityList').selectize();
    $('#profession').selectize();
    var isFotSignup = $('#fotSignup').val();
    var isAuthenticate = $('#isAuthenticate').val();
    if(isFotSignup == "False" && isAuthenticate == "True") {
        $loader.removeClass('hide');
        submitSignupDetailForm();
    }
    logger.log(" Sign-up Fot page loaded");
}

$('#signupSubmit').click(function (e) {
        e.preventDefault();
        var submitSignupParsley = $(".signup-detail-form").parsley();
        submitSignupParsley.validate();
        if (submitSignupParsley.isValid()){
            $loader.removeClass('hide');
            submitSignupDetailForm(e);
        }
});

let submitSignupDetailForm = ()=>{
        var data = $(".signup-detail-form").serializeArray();
        var signupDetailJson = _.object(_.pluck(data, 'name'), _.pluck(data, 'value'));
        signupDetailJson['form-type'] = "fot-profile-form";
        signupDetailJson['csrfmiddlewaretoken'] = $('input[name="csrfmiddlewaretoken"]').val();
        var p = treejax({
            url: '/fot/signup/',
            type: 'POST',
            data: signupDetailJson
        }).then(function (data, status, xhr) {
            console.log(data);
            $loader.addClass('hide');
            if(data.success && !data.alreadySignup) {
                $("#signupSuccess").removeClass('hide');
                currentStep = currentStep+1;
                toggleActiveItem('next');
                toggleSteps();
            } else if (data.alreadySignup) {
                $("#alreadySignup").removeClass('hide');
            } else {
                $("#signupFail").removeClass('hide');
            }
        },function (treeboError) {
            $loader.addClass('hide');
            $(".fot-success-fail").removeClass('hide');
            $("#signupFail").removeClass('hide');
            console.log("fail");
        }).finally(function(){
           $loader.addClass('hide');
        });
    window.scroll(0,0);
    return p;
}

$('#testSubmit').click(function (e) {
        var test_form = $(this).closest('.test-form');
        e.preventDefault();
        var submitTestParsley = $(test_form).parsley();
        submitTestParsley.validate();
        if (submitTestParsley.isValid()){
            $loader.removeClass('hide');
            submitTestForm(e);
        }
});


let submitTestForm = ()=>{
        window.scroll(0, 0);
        var data = $("#testFormOne, #testFormTwo, #testFormThree").serializeArray();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        data.push({name: 'email', value: email});
        data.push({name: 'mobile', value: mobile});
        data.push({name: 'csrfmiddlewaretoken', value: $('input[name="csrfmiddlewaretoken"]').val()})
        var p = treejax({
            url: '/fot/signup/',
            type: 'POST',
            data: data
        }).then(function (data, status, xhr) {
            console.log("success");
            $loader.addClass('hide');
            if (data.success) {
                $("#testSuccess").removeClass('hide');
            } else {
                $("#testFail").removeClass('hide');
            }
        },function (treeboError) {
            $loader.addClass('hide');
            $("#testFail").removeClass('hide');
            console.log("fail");
        }).finally(function(){
           $loader.addClass('hide');
        });
    return p;
}


$('.closeModal').on('click', function(){
	$('.modal').addClass('hide');

});

$('#continueToForm').on('click', function(){
	$loader.removeClass('hide');
	location.reload();
});
$('.next').on('click', function(e){
    var element = document.getElementById("progressBar");
    element.scrollIntoView();
    var test_form = $(this).closest('.test-form');
    var submitTestParsley = $(test_form).parsley();
    submitTestParsley.validate();
    if (submitTestParsley.isValid()){
        currentStep = currentStep+1;
        toggleActiveItem('next');
        toggleSteps();
    }
});

$('.js-back-link').on('click', function(e){
    if(currentStep !==1){
        currentStep = currentStep-1;
        e.preventDefault();
        toggleActiveItem('prev');
        toggleSteps();

    }

});

function toggleActiveItem(toActive){
    var currentItem = $('.signup-steps__indicator').find('.active');
    currentItem.removeClass('active');

    if(toActive === 'next'){
        currentItem.addClass('complete').next().addClass('active');
        currentItem.html('<i class="icon-tick"></i>');
        $('.signup-steps__heading').show();
    } else {
        currentItem.prev().addClass('active');
        currentItem.html(currentStep + 1);
        currentItem.prev().html(currentStep);
    }
}
function toggleSteps(){
    $('.step').addClass('hide');
    console.log()
    $(`.step-${currentStep}`).removeClass('hide');
}

let signupfotConfig = { name:'Signup Fot Page', widgets:[]};
let signupfotView = SignupFotView.setup(signupfotConfig);
