from celery import shared_task
from celery.signals import task_prerun
from django.conf import settings
from celery.utils.log import get_task_logger
import datetime

from apps.discounts.models import (
    DiscountCouponConstraints, ConstraintExpression, DiscountCoupon,
    ConstraintKeywords, ConstraintOperators
)
from apps.discounts.services.create_coupon_service import CreateCouponService
from base import log_args

# Task logger logs the task id and task name for each task
logger = get_task_logger(__name__)


@shared_task
@log_args(logger)
def update_coupon_constraints(
        coupon_code,
        checkin_start,
        checkout_start,
        user_id=None,
        request_id=None):
    checkin_start_date = datetime.datetime.strptime(checkin_start, '%Y-%m-%d')
    checkout_start_date = datetime.datetime.strptime(
        checkout_start, '%Y-%m-%d')
    discount_coupon = DiscountCoupon.objects.filter(code=coupon_code).first()
    if discount_coupon:
        existing_checkin_constraints = discount_coupon.discountcouponconstraints_set.filter(
            keyword__keyword='checkin').first()

        if existing_checkin_constraints:
            logger.info(
                "Found CouponConstraint for coupon_code: %s, with id: %s",
                coupon_code,
                existing_checkin_constraints.id)
            existing_checkin_constraints.value_one = checkin_start
            existing_checkin_constraints.value_two = checkout_start
            existing_checkin_constraints.save()
        else:
            keyword = ConstraintKeywords.objects.get(keyword='checkin')
            operator = ConstraintOperators.objects.get(operator='between')
            new_constraint = DiscountCouponConstraints.objects.create(
                coupon=discount_coupon,
                keyword=keyword,
                operation=operator,
                value_one=checkin_start,
                value_two=checkout_start)
            expression = ConstraintExpression.objects.create(
                coupon=discount_coupon,
                expression='{0}'.format(new_constraint.id))

            logger.info(
                "CouponConstraint created for coupon_code: %s, with id: %s, and expression created with id: %s",
                coupon_code,
                new_constraint.id,
                expression.id)


@shared_task(name=settings.CELERY_CREATE_COUPON_TASK_NAME)
@log_args(logger)
def create_coupons(coupon, constraints_data, apply_constraint, user_email):
    logger.info("In create coupon celery task")
    create_coupon_service = CreateCouponService()
    create_coupon_service.create_coupon(
        coupon, constraints_data, apply_constraint, user_email)
