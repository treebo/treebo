from rest_framework import serializers


class SocialLoginDTO(serializers.Serializer):
    token = serializers.CharField(max_length=1500)
