import uuid
from collections import namedtuple

from django.contrib.auth.models import AbstractBaseUser, User, PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import ugettext_lazy as _
from djutil.models import TimeStampedModel
from django.db.models import UUIDField

from common.constants import common_constants as const
from dbcommon.models.default_permissions import DefaultPermissions
from dbcommon.managers.user_manager import CustomUserManager
import logging

logger = logging.getLogger(__name__)


class User(
        AbstractBaseUser,
        PermissionsMixin,
        TimeStampedModel,
        DefaultPermissions):
    """
    A custom user class that basically mirrors Django's `AbstractUser` class
    and doesn't force `first_name` or `last_name` with sensibilities for
    international names.

    http://www.w3.org/International/questions/qa-personal-names
    """

    first_name = models.CharField(_('First Name'), max_length=254)
    middle_name = models.CharField(
        _('Middle Name'),
        max_length=20,
        blank=True,
        default="")
    last_name = models.CharField(
        _('Last Name'),
        max_length=254,
        blank=True,
        default="")

    email = models.EmailField(
        _('email address'),
        max_length=254,
        unique=True,
        null=True)
    phone_number = models.CharField(
        _('phone_number'), max_length=20, null=True)
    city = models.CharField(max_length=200, default='', blank=True, null=True)

    GENDER_CHOICES = (('M', 'Male'), ('F', 'Female'),)
    gender = models.CharField(
        max_length=1,
        choices=GENDER_CHOICES,
        blank=True,
        default="")

    # TODO: default null value need to be fixed
    dob = models.DateField(_('dob'), blank=True, null=True)
    anniversary_date = models.DateField(
        _('anniversary'), blank=True, null=True)
    is_staff = models.BooleanField(_('staff status'), default=False, help_text=_(
        'Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(_('active'), default=False, help_text=_(
        'Designates whether this user should be treated as active. '
        'Unselect this instead of deleting accounts.'))
    is_guest = models.BooleanField(_('is guest'), default=False)
    is_otp_verified = models.NullBooleanField(default=False, blank=True)

    # Default uuid is not for use, Added to fix migration issue
    uuid = UUIDField(default=uuid.uuid4, null=True, blank=True)
    auth_id = models.IntegerField(unique=True, null=True)
    # token = models.CharField(max_length=100, null=True)
    token = None
    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    class Meta(DefaultPermissions.Meta):
        db_table = 'profiles_user'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __unicode__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        parts = [self.first_name, self.middle_name, self.last_name]
        return " ".join(x for x in parts if x)

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name.strip()

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def password_change(self):
        password = User.objects.make_random_password()
        self.set_password(password)

    def forgot_password(self, *args, **kwargs):
        """
        send random password and link the password edit form
        """

        User.password_change(self)
        super(User, self).save(*args, **kwargs)

    @property
    def is_admin(self):
        return self.is_staff

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)

    def get_hx_keys(self):
        keys = ['accesskey', 'accesssecret']
        user_access_keys = self.usermetadata_set.filter(
            key__in=keys, user_id=self)
        hx_keys = {}
        for meta in user_access_keys:
            hx_keys[meta.key] = meta
        return hx_keys


class UserOtp(TimeStampedModel, DefaultPermissions):
    """
    model for mobile no validation through OTP
    """
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    phone_number = models.CharField(max_length=200, default="", null=True)
    otp = models.CharField(max_length=200, default="", null=True)
    VERIFIED = const.VERIFIED
    NOT_VERIFIED = const.NOT_VERIFIED
    DELETED = const.DELETED
    STATUS_CHOICE = const.PHONE_CHOICE
    status = models.CharField(
        max_length=1,
        choices=STATUS_CHOICE,
        blank=True,
        default=NOT_VERIFIED)

    class Meta(DefaultPermissions.Meta):
        db_table = 'profiles_userotp'
        unique_together = ('phone_number', 'user')
        verbose_name = 'User OTP'
        verbose_name_plural = 'User OTPs'

    def __unicode__(self):
        return '%s %s %s' % (self.user.first_name,
                             self.phone_number, self.status)


class UserMetaData(TimeStampedModel, DefaultPermissions):
    """

    """
    user_id = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    key = models.CharField(max_length=200, default="", null=True)
    value = models.TextField(max_length=1000, default="", null=True)
    image_url = models.FileField(
        upload_to='user/',
        max_length=1000,
        null=True,
        blank=True,
        default=None)

    class Meta(DefaultPermissions.Meta):
        db_table = 'profiles_usermetadata'
        verbose_name = 'User Metadata'
        verbose_name_plural = 'User Metadata'

    def __unicode__(self):
        return '%s %s' % (self.user_id, self.key)


class UserEmail(TimeStampedModel, DefaultPermissions):
    """
    model for email  validation through link
    """
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    email = models.EmailField(max_length=200)
    EMAIL_TYPE_CHOICES = (
        ('P', 'Personal'),
        ('W', 'Work'),
    )
    email_type = models.CharField(
        max_length=200,
        default="",
        null=True,
        choices=EMAIL_TYPE_CHOICES)
    is_validate = models.BooleanField(_('is validate'), default=False)

    class Meta(DefaultPermissions.Meta):
        db_table = 'profiles_useremail'

    def __unicode__(self):
        return '%s %s %s' % (self.user.first_name,
                             self.email, self.is_validate)


class UserDiscount(TimeStampedModel, DefaultPermissions):
    """
    TODO:  move it to some place more suitable
    """
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    coupon_code = models.CharField(max_length=200, unique=True)
    status = models.IntegerField(default=const.SIGNUP_COUPON_CREATED,
                                 choices=const.SIGNUP_COUPON_STATUS)

    class Meta(DefaultPermissions.Meta):
        db_table = 'profiles_userdiscount'
        verbose_name = 'User Discount'
        verbose_name_plural = 'User Discounts'

    def __unicode__(self):
        return '%s %s' % (self.user, self.coupon_code)


class EmailLinkHashValue(TimeStampedModel, DefaultPermissions):
    user = models.ForeignKey(User, models.DO_NOTHING)
    hash_value = models.CharField(max_length=255, default="")
    is_used = models.BooleanField(default=False)
    email = models.EmailField(max_length=200, blank=True)
    USER_FOR_CHOICES = (
        ('email_validate', 'Email Validate'),
        ('forgot_password', 'Forgot password'),
    )
    use_for = models.CharField(
        max_length=255,
        default="",
        choices=USER_FOR_CHOICES)

    class Meta(DefaultPermissions.Meta):
        db_table = 'profiles_emaillinkhashvalue'
        verbose_name = 'Email Link HashValue'
        verbose_name_plural = 'Email Link HashValues'

    def __unicode__(self):
        return '%s %s' % (self.user, self.is_used)


class UserReferral(TimeStampedModel, DefaultPermissions):
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING)
    userkey = models.CharField(max_length=50, blank=True, unique=True)
    shareurl = models.CharField(max_length=100, blank=True, unique=True)
    referral_code = models.CharField(max_length=12, blank=True, unique=True)
    has_referrer = models.BooleanField()
    # Share link via which he signed up
    share_link = models.CharField(max_length=50, blank=True, null=True)
    friend_referral_code = models.CharField(
        max_length=12, blank=True, null=True)

    class Meta(DefaultPermissions.Meta):
        pass


class Otp(TimeStampedModel, DefaultPermissions):
    mobile = models.CharField(
        _('mobile'),
        max_length=20,
        blank=False,
        null=False)
    otp = models.CharField(blank=False, null=False, max_length=8)
    active = models.BooleanField(default=True)

    class Meta(DefaultPermissions.Meta):
        pass


class UserReferralReward(TimeStampedModel, DefaultPermissions):
    """
    Stores which user gave reward to which user. Data in the table should be read as:
    <user> did <type> action which gave <friend> a <coupon> of <amount> reward on <reward_date>
    E.g. User2 did transaction action which gave User1 a PayTM coupon of Rs 200 on <data>
    E.g. User1 did signup action which gave User2 a PayTM coupon of Rs 200 on <data>
    """
    referrer = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        db_column="referrer")
    reward_amount = models.CharField(
        blank=True, null=True, default="0", max_length=4)
    reward_date = models.DateField(auto_now=True)
    coupon_code = models.CharField(blank=True, max_length=50)
    __Events = namedtuple('ReferralEvents', 'Signup Install Transaction')
    ReferralEvents = __Events('Signup', 'Install', 'Transaction')
    EVENT_TYPES = ((ReferralEvents.Signup, ReferralEvents.Signup),
                   (ReferralEvents.Transaction,
                    ReferralEvents.Transaction),
                   (ReferralEvents.Install, ReferralEvents.Install))
    type = models.CharField(
        max_length=25,
        blank=False,
        null=False,
        default="",
        choices=EVENT_TYPES)
    reward_id = models.CharField(max_length=25, null=True, blank=True)
    referee = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        related_name='referee',
        db_column='referee')

    class Meta(DefaultPermissions.Meta):
        pass


class AccessToken(models.Model):
    auth_user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=255, db_index=True, unique=True)
    token_type = models.CharField(max_length=255)
    refresh_token = models.CharField(max_length=255, unique=True, null=True)
    expires = models.DateTimeField()
    scope = models.TextField()

    def __str__(self):
        return self.token
