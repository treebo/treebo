class BookingRequestParser:
    @staticmethod
    def booking_request_handler(booking_request_data):
        """
        To parse the booking request after being validated for initializing booking
        :param booking_request_data:
        :return: booking_data
        """
        booking_data = {
            "checkin_date":         booking_request_data["checkin_date"],
            "checkout_date":        booking_request_data["checkout_date"],
            "adult_count":          booking_request_data["adults"],
            "child_count":          booking_request_data["children"],
            "room_config":          booking_request_data["room_config"],
            "room_type":            booking_request_data["room_type"],
            "hotel_id":             booking_request_data["hotel_id"],
            "rack_rate":            booking_request_data["rack_rate"],
            "mm_promo":             booking_request_data["mm_promo"],
            "pretax_amount":        booking_request_data["pretax_amount"],
            "tax_amount":           booking_request_data["tax_amount"],
            "total_amount":         booking_request_data["total_amount"],
            "guest_name":           booking_request_data["name"],
            "guest_email":          booking_request_data["email"],
            "guest_country_code":   booking_request_data["country_code"],
            "guest_mobile":         booking_request_data["phone"],
            "comments":             booking_request_data["comments"],
            "coupon_apply":         booking_request_data["coupon_apply"],
            "coupon_code":          booking_request_data["coupon_code"],
            "payment_mode":         booking_request_data["payment_mode"],
            "discount":             booking_request_data["discount_value"],
            "channel":              booking_request_data["booking_channel"],
            "user_id":              booking_request_data["user"],
            "is_audit":             booking_request_data["is_audit"],
            "organization_name":    booking_request_data.get("organization_name", ''),
            "organization_taxcode": booking_request_data.get("organization_taxcode", ''),
            "organization_address": booking_request_data.get("organization_address", ''),
            "order_id":             booking_request_data["order_id"],
            "payment_amount":       booking_request_data["voucher_amount"],
            "voucher_amount":       booking_request_data["voucher_amount"],
            "rate_plan":            booking_request_data.get("rate_plan"),
            "rate_plan_meta":       booking_request_data.get("rate_plan_meta"),
            "member_discount":      booking_request_data["member_discount"],
            "wallet_applied":       bool(booking_request_data.get("wallet_applied", False)),
            "wallet_deduction":     booking_request_data.get("wallet_deduction", 0.0),
        }
        return booking_data
