from rest_framework import serializers

from apps.discounts.models import DiscountCoupon, DiscountCouponConstraints
from apps.discounts.services.discount_coupon_services import DiscountCouponServices


class DiscountCouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiscountCoupon
        fields = '__all__'


class CreateConstraintSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiscountCouponConstraints
        fields = '__all__'


class DiscountCouponSummarySerializer(serializers.ModelSerializer):

    description = serializers.SerializerMethodField()
    coupon_amount = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        self.booking_request = kwargs.pop('booking_request', None)
        self.logged_in_user = kwargs.pop('logged_in_user', None)
        super(
            DiscountCouponSummarySerializer,
            self).__init__(
            *args,
            **kwargs)

    class Meta:
        model = DiscountCoupon
        fields = (
            'code',
            'description',
            'coupon_amount',
            'is_prepaid',
            'tagline',
            'expiry')

    def get_description(self, obj):
        return str(obj.terms.replace('\\n', ' '))

    def get_coupon_amount(self, obj):
        return round(DiscountCouponServices.get_coupon_amount_from_soa(
            obj, self.booking_request, self.logged_in_user), 0)
