from datetime import date

from apps.pricing.dateutils import date_to_ymd_str


class CouponValidationParams(object):
    def __init__(
            self,
            checkin_date,
            checkout_date,
            room_config,
            coupon_code,
            hotel_id,
            city_id,
            user,
            current_user,
            pretax_amount,
            member_discount,
            total_amount,
            rack_rate,
            room_type,
            booking_channel,
            paytype,
            utm_source):
        checkin = date_to_ymd_str(checkin_date)
        checkout = date_to_ymd_str(checkout_date)
        room_count = len(room_config.split(','))

        self.checkin = checkin
        self.room_config = room_config
        self.checkout = checkout
        self.hotel_id = hotel_id
        self.user = user  # Need to check why
        self.no_bookings = room_count
        self.booking_date = date.today()
        self.total_pretax_cost = pretax_amount - member_discount
        self.totalcost = total_amount
        self.rack_rate = rack_rate
        self.rooms = room_type
        self.city = city_id
        self.booking_channel = booking_channel
        self.current_user = current_user
        self.paytype = paytype
        self.code = coupon_code
        self.utm_source = utm_source
