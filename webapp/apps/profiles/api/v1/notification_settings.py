import logging

from rest_framework import serializers
from rest_framework.response import Response

from base import log_args
from base.views.api import TreeboAPIView
from common.constants import common_constants, error_codes
from dbcommon.models.profile import UserMetaData

__author__ = 'varunachar'

logger = logging.getLogger(__name__)


class SettingsSerializer(serializers.Serializer):
    promotions = serializers.BooleanField(required=True, error_messages={
        'required': error_codes.PROMOTIONS_NOT_PRESENT,
        'blank': error_codes.PROMOTIONS_NOT_PRESENT
    })
    cities = serializers.CharField(required=False)
    launch = serializers.BooleanField(required=True, error_messages={
        'required': error_codes.PROMOTIONS_NOT_PRESENT,
        'blank': error_codes.PROMOTIONS_NOT_PRESENT
    })


class NotificationSettingsValidator(serializers.Serializer):
    email = SettingsSerializer(required=True, error_messages={
        'required': error_codes.PROMOTIONS_NOT_PRESENT,
        'blank': error_codes.PROMOTIONS_NOT_PRESENT
    })
    phone = SettingsSerializer(required=True, error_messages={
        'required': error_codes.PROMOTIONS_NOT_PRESENT,
        'blank': error_codes.PROMOTIONS_NOT_PRESENT
    })


class NotificationSettingsAPI(TreeboAPIView):
    # validationSerializer = NotificationSettingsValidator

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        data = request.data
        emailPromotions = data.get('email[promotions]') in "true"
        emailCities = data.get('email[cities]')
        emailLaunch = data.get('email[launch]') in "true"
        phonePromotions = data.get('phone[promotions]') in "true"
        phoneCities = data.get('phone[cities]')
        phoneLaunch = data.get('phone[launch]') in "true"

        user_obj = request.user

        try:
            sendEmailNotification = UserMetaData.objects.get(
                user_id=user_obj, key=common_constants.DO_NOT_SEND_NOTIFICATION_EMAIL)
        except UserMetaData.DoesNotExist:
            sendEmailNotification = UserMetaData(
                user_id=user_obj, key=common_constants.DO_NOT_SEND_NOTIFICATION_EMAIL)
        sendEmailNotification.value = '0' if emailPromotions else '1'
        sendEmailNotification.save()

        try:
            sendPhoneNotification = UserMetaData.objects.get(
                user_id=user_obj, key=common_constants.DO_NOT_SEND_NOTIFICATION_PHONE)
        except UserMetaData.DoesNotExist:
            sendPhoneNotification = UserMetaData(
                user_id=user_obj, key=common_constants.DO_NOT_SEND_NOTIFICATION_PHONE)
        sendPhoneNotification.value = '0' if phonePromotions else '1'
        sendPhoneNotification.save()

        try:
            notificationForCityViaEmail = UserMetaData.objects.get(
                user_id=user_obj, key=common_constants.DO_NOT_SEND_CITY_EMAIL)
        except UserMetaData.DoesNotExist:
            notificationForCityViaEmail = UserMetaData(
                user_id=user_obj, key=common_constants.DO_NOT_SEND_CITY_EMAIL)

        if not emailLaunch:
            notificationForCityViaEmail.value = '1'
        elif emailCities and emailLaunch:
            notificationForCityViaEmail.value = emailCities
        else:
            notificationForCityViaEmail.value = '0'
        notificationForCityViaEmail.save()

        try:
            notificationForCityViaPhone = UserMetaData.objects.get(
                user_id=user_obj, key=common_constants.DO_NOT_SEND_CITY_PHONE)
        except UserMetaData.DoesNotExist:
            notificationForCityViaPhone = UserMetaData(
                user_id=user_obj, key=common_constants.DO_NOT_SEND_CITY_PHONE)

        if not phoneLaunch:
            notificationForCityViaPhone.value = '1'
        elif phoneCities and phoneLaunch:
            notificationForCityViaPhone.value = phoneCities
        else:
            notificationForCityViaPhone.value = '0'
        notificationForCityViaPhone.save()

        return Response({
            'status': 'success',
            'msg': 'Settings saved successfully'
        })
