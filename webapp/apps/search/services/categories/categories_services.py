from apps.search.services.categories.categories_model_services import *
from django.core.cache import cache
from common.constants import common_constants as const
import logging

logger = logging.getLogger(__name__)


class CategoriesServices():

    def category_dict(self, hotels, hotel_ids):
        categories_model_service = CategoriesModelServices()
        value_for_booking_threshold = categories_model_service.fetch_value_for_booking_threshold()
        pricing_hotel_id_list = self.get_pricing_list(
            hotel_ids, value_for_booking_threshold)
        category_dict = {}
        four_plus_rated_hotels = self.get_four_plus_rated_hotels(hotel_ids)
        for hotel in hotels:
            category_dict[hotel.hotelogix_id] = {}
            category_dict[hotel.hotelogix_id]['is_four_plus_rated'] = True \
                if hotel.id in four_plus_rated_hotels else False
            category_dict[hotel.hotelogix_id][
                'is_value_for_money'] = True if hotel.id in pricing_hotel_id_list else False
            category_dict[hotel.hotelogix_id]['is_frequently_booked'] = False
        return category_dict

    @staticmethod
    def get_four_plus_rated_hotels(hotel_ids):
        four_plus_rated_hotels = HotelTAOverallRatings.objects.filter(
            hotel_ta_mapping__hotel_id__in=hotel_ids, overall_rating__gte=4).values_list(
            'hotel_ta_mapping__hotel_id', flat=True)
        return four_plus_rated_hotels

    def is_value_for_money(self, hotel_id, pricing_hotel_id_list):
        flag = False
        if hotel_id in pricing_hotel_id_list:
            flag = True
        return flag

    def get_hotelogix_id_list(self, hotels):
        hotelogix_ids = []
        for hotel in hotels:
            hotelogix_ids.append(hotel.hotelogix_id)
        return hotelogix_ids

    def get_hotel_id_list(self, hotels):
        hotel_ids = []
        for hotel in hotels:
            hotel_ids.append(hotel.id)
        return hotel_ids

    def get_pricing_list(self, hotel_ids, value_for_booking_threshold):
        pricing_list = []
        cache_keys = []
        for hotel_id in hotel_ids:
            cache_keys.append(self.get_cache_key(hotel_id))
        cache_value_map = cache.get_many(cache_keys)
        for hotel_id in hotel_ids:
            pricing_dict = {}
            cache_value = 799.0
            cache_key = self.get_cache_key(hotel_id)
            pricing_dict['hotel'] = hotel_id
            if cache_key in cache_value_map.keys():
                cache_value = cache_value_map.get(cache_key)
            pricing_dict['price'] = round(float(cache_value), 2)
            pricing_list.append(pricing_dict)
        sorted(
            pricing_list,
            key=lambda x: x['price'] if 'price' in x and x['price'] else 0)
        pricing_list = pricing_list[:value_for_booking_threshold]
        pricing_hotel_id_list = [pricing_dict['hotel']
                                 for pricing_dict in pricing_list]
        return pricing_hotel_id_list

    def get_cache_key(self, hotel_id):
        return str(hotel_id) + '_min_price'
