import logging

from django.contrib.auth.models import AnonymousUser

from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.otp_is_verify_dto import OtpIsVerifyDTO
from apps.auth.services.otp_service import OTPService
from apps.common.api_response import APIResponse as api_response
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes

logger = logging.getLogger(__name__)


class OTPIsVerifyApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    otp_service = OTPService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            is_user_logged_in = self.is_user_logged_in(request)

            if is_user_logged_in:
                is_verified = True
            else:
                user = AnonymousUser()
                if hasattr(request, 'user') and request.user:
                    user = request.user
                # temporary fix to allow international numbers
                if request.data.get('country_code', '+91') != "+91":
                    return api_response.prep_success_response({
                        'is_verified': True
                    })

                otp_is_verify_dto = OtpIsVerifyDTO(data=request.data)

                if not otp_is_verify_dto.is_valid():
                    logger.error(
                        "otp isverified invalid request %s ",
                        request.data)
                    return api_response.invalid_request_error_response(
                        otp_is_verify_dto.errors, OTPIsVerifyApi.__name__)
                is_verified = self.otp_service.is_verified(
                    otp_is_verify_dto.data, user)

            response = api_response.prep_success_response({
                'is_verified': is_verified
            })
        except TreeboException as e:
            logger.exception(
                "treebo otp is verified exception %s ",
                request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception(
                "internal error otp is verified excetion %s ",
                request.data)
            response = api_response.internal_error_response(error_codes.get(
                error_codes.OTP_VERIFY_FAILED), OTPIsVerifyApi.__name__)
        return response

    def is_user_logged_in(self, request):
        return (hasattr(request, 'user') and request.user
                and request.user.is_authenticated() and request.user.is_otp_verified)
