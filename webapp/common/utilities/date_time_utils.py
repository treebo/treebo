import logging
from datetime import datetime

from pytz import timezone

logger = logging.getLogger(__name__)

DATE_FORMAT = "%Y-%m-%d"


def get_day_difference(first_date, second_date):
    """
    Gets the difference in days between 2 strings
    :param (str) first_date:
    :param (str) second_date:
    :return: (int) The difference
    """
    checkin = datetime.strptime(first_date, "%Y-%m-%d").date()
    checkout = datetime.strptime(second_date, "%Y-%m-%d").date()
    return (checkout - checkin).days


def is_valid_date(date, format):
    """
    Validates if the passed date is in the format passed
    :param (str) date:
    :param (str) format:
    :return (bool):
    """
    try:
        print((date, format))
        date = datetime.strptime(date, format)
        return True
    except BaseException:
        print("Inside exception : Not a valid date")
        return False


def get_time_diff(from_date, to_date):
    try:
        time_diff = from_date - to_date
        seconds = time_diff.total_seconds()
        minutes = int(seconds) / 60
        hours = int(minutes) / 60
        days = int(hours) / 24
        return hours, minutes
    except Exception as exc:
        logger.debug(exc)

        return '', ''


def build_time_string(hours, minutes):
    time_string = ''
    hour_string = ' hour' if hours <= 1 else ' hours'
    minute_string = ' minute' if minutes < 1 else ' minute'
    try:
        time_string = str(
            hours) + hour_string if hours > 0 else str(minutes) + minute_string
        return time_string
    except Exception as exc:
        logger.exception(exc)
        return time_string


def build_date(date):
    """
    Formats the date into human friendly format. E.g. 1st Nov 2015, 23rd Nov 2015
    :param date:
    :return:
    """
    day = int(date.strftime("%-d"))
    if 4 <= day <= 20 or 24 <= day <= 30:
        suffix = "th"
    else:
        suffix = ["st", "nd", "rd"][day % 10 - 1]
    formatted_date = date.strftime("%-d|| %b'%y")
    formatted_date = formatted_date.replace("||", suffix)
    return formatted_date


def get_current_ist_time():
    """
    Get today's date in India Standard Time
    :return: datetime
    """
    return datetime.now(get_ist_time_zone())


def get_ist_time_zone():
    """
    Get IST timezone
    :return: timezone
    """
    return timezone('Asia/Kolkata')
