import logging

from django.utils.encoding import smart_str

from apps.content.models import ContentStore
from apps.hotels.service.hotel_service import HotelService
from apps.seo import constants
from apps.seo.components.base_component import BaseComponent
from apps.seo.models import HotelContentCountMapping, FeatureToggle, PositiveCategoryContentMapping
from apps.seo.serializers.positive_feedback_serializer import ItemsDTO, PositiveFeeedbackDTO

logger = logging.getLogger(__name__)

__author__ = 'snehilrastogi'


class ThingsGuestLove(BaseComponent):
    def __init__(self, current_city, footer_data, hotel):
        self.hotel = hotel
        super(ThingsGuestLove, self).__init__(current_city, footer_data, [])

    def build(self):
        feedback_data = self.create_positive_feedback()
        logger.info(
            "feedback_response %s type %s",
            feedback_data,
            type(feedback_data))
        return {constants.DEFAULT_HD_FEEDBACK_TITLE: feedback_data}

    def create_positive_feedback(self, ):
        feedback_dict = dict()
        hotel_id = int(self.hotel.id)
        config = FeatureToggle.objects.filter().first()
        try:

            logger.info("Creating the guest feedback for hotel %s", hotel_id)
            content_store_obj = ContentStore.objects.filter(
                name=constants.HD_COMPONENT_TITLE, version=1).first()
            title = constants.DEFAULT_HD_FEEDBACK_TITLE

            if content_store_obj:
                hotel_name = HotelService().get_display_name_from_hotel_name(self.hotel.name)
                content_store_val = content_store_obj.value
                if content_store_val.get(title):
                    title = content_store_val[title].format(hotel_name)

            item_list = []

            content_count_obj = HotelContentCountMapping.objects.filter(
                hotel__id=int(hotel_id))

            if content_count_obj and config:
                for data in content_count_obj:
                    item_dict = dict()
                    item_dict['title'] = smart_str(
                        data.feedback_content.category.name)
                    item_dict['description'] = smart_str(
                        data.feedback_content.content)
                    item_data = ItemsDTO(data=item_dict)
                    if item_data.is_valid():
                        item_list.append(item_data.validated_data)
                    else:
                        logger.error(
                            "Error in validating feedback item data %s %s",
                            item_data.errors,
                            item_dict)
                diff = config.num_of_positives_shown - content_count_obj.count()
                if diff > 0:
                    logger.info(
                        "Adding some default positives for the hotel %s", hotel_id)
                    def_pos_dict = constants.DEFAULT_POSITIVES_DICT
                    content_store_def_pos_obj = ContentStore.objects.filter(
                        name=constants.DEFAULT_POSITIVES_ORDER, version=1).first()
                    if content_store_def_pos_obj:
                        def_pos_dict = content_store_def_pos_obj.value

                    default_positives = sorted(
                        def_pos_dict, key=lambda v: def_pos_dict[v])

                    for positive in default_positives:
                        pos_cat_content_obj = PositiveCategoryContentMapping.objects.filter(
                            positive__name=positive).first()
                        if pos_cat_content_obj:
                            item_dict = dict()
                            item_dict['title'] = smart_str(
                                pos_cat_content_obj.category.name)
                            item_dict['description'] = smart_str(
                                pos_cat_content_obj.content)
                            if item_dict not in item_list:
                                item_data = ItemsDTO(data=item_dict)
                                if item_data.is_valid():
                                    item_list.append(item_data.validated_data)
                                else:
                                    logger.error(
                                        "Error in validating feedback item data %s %s", item_data.errors, item_dict)
                            else:
                                logger.info(
                                    "item_dict %s already exists", item_dict)
                                continue
                        if len(item_list) == config.num_of_positives_shown:
                            break

                feedback_dict['items'] = item_list
                if item_list:
                    feedback_dict['title'] = smart_str(title)
                    feedback_data = PositiveFeeedbackDTO(data=feedback_dict)
                    if feedback_data.is_valid():
                        return feedback_data.validated_data
                    else:
                        logger.error(
                            "Error in validating feedback data for hotel %s with errors %s and data %s",
                            hotel_id,
                            feedback_data.errors,
                            feedback_dict)
            else:
                logger.error(
                    "No guest love data/ positive feedback for this hotel %s exists",
                    hotel_id)

        except Exception:
            logger.exception(
                "Exception in updating the database with hotel positive feedback")
        return feedback_dict
