# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0008_tahotelidmapping_enable_iframe_widget'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reviewappconfiguration',
            name='num_reviews_to_show',
        ),
        migrations.AddField(
            model_name='reviewappconfiguration',
            name='num_non_user_reviews_to_show',
            field=models.PositiveIntegerField(default=10),
        ),
        migrations.AddField(
            model_name='reviewappconfiguration',
            name='num_user_reviews_to_show',
            field=models.PositiveIntegerField(default=10),
        ),
        migrations.AddField(
            model_name='taapiaudittable',
            name='user_location_reviews_api_url',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='taapiaudittable',
            name='user_location_reviews_api_url_response_json',
            field=models.TextField(default=b'', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='taapiaudittable',
            name='user_location_reviews_api_url_response_status',
            field=models.CharField(default=b'SUCCESS', max_length=100, choices=[(b'SUCCESS', b'SUCCESS'), (b'FAILURE', b'FAILURE')]),
        ),
        migrations.AddField(
            model_name='tahotelreviews',
            name='review_is_user_generated',
            field=models.BooleanField(default=False),
        ),
    ]
