import logging
import requests

logger = logging.getLogger(__name__)


class APIRetryService:
    def __init__(self):
        pass

    @staticmethod
    def get_api_response_with_retry(url, payload, retry):
        err_message = 'api %s fails after retry also ' + url
        ns_error = None
        for index in range(retry):
            try:
                logger.info("trying %s  api %s %s", index, url, payload)
                r = requests.get(url=url, params=payload)
                status = r.status_code
                logger.info(" api status %s ", status)
                if status != 200:
                    logger.info("get api status not 200 %s ", r.text)
                    ns_error = r.text
                    continue
                ns_error = None
                json_reponse = r.json()
                # logger.info("json_reponse %s ", json_reponse)
                return json_reponse
            except Exception as e:
                logger.error(" api error retry %d %s", index, e)
                err_message += str(e)
        if ns_error:
            err_message += err_message + "\napi response: " + ns_error
        raise Exception(err_message)

    @staticmethod
    def post_api_response_with_retry(url, payload, headers, retry):
        err_message = 'api %s fails after retry also ' + url
        ns_error = None
        for index in range(retry):
            try:
                logger.info("trying %s)  api %s %s", index, url, payload)
                r = requests.post(url, data=payload, headers=headers)
                status = r.status_code
                logger.info(" api status %s ", status)
                if status != 200:
                    logger.info("post api status not 200 %s ", r.text)
                    err_message = r.text
                    ns_error = r.text
                    continue
                ns_error = None
                json_reponse = r.json()
                # logger.info("json_reponse %s ", json_reponse)
                return json_reponse
            except Exception as e:
                logger.error(" api error retry %d %s", index, e)
                err_message += str(e)
        if ns_error:
            err_message += err_message + "\napi response: " + ns_error
        raise Exception(err_message)
