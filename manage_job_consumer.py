# -*- coding: utf-8 -*-
"""
    Base AsyncJobRunner
"""
import logging
import logging.config
import os
import sys
from argparse import ArgumentParser

import django
from django import db
from django.conf import settings
from easyjoblite import orchestrator
from webapp.apps.common.json_encoder import LogstashJsonEncoder

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapp.conf.preprod")


def start():
    """
    :return:
    """
    django.setup()
    db.close_old_connections()

    logging.config.dictConfig(get_logging_config(settings.LOG_ROOT))

    worker = orchestrator.Orchestrator(
        rabbitmq_url=settings.BROKER_URL,
        config_file=settings.EASY_JOB_LITE_SETTINGS_PATH,
        import_paths="{base}:{base}/webapp".format(
            base=__file__))
    worker.start_service()


def stop():
    """
    :return:
    """
    os.system("job stop all")


def get_logging_config(path):
    """
    :param path:
    :return:
    """
    logging_conf = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format': "[%(request_id)s] [%(asctime)s] %(levelname)s  [%(name)s:%(lineno)s] %(message)s",
                'datefmt': "%d/%b/%Y %H:%M:%S"},
            'simple': {
                'format': '%(levelname)s %(message)s'},
            'logstash_fmtr': {
                'format': "[%(asctime)s] %(levelname)s  [%(name)s:%(lineno)s] %(message)s",
                'datefmt': "%d/%b/%Y %H:%M:%S"},
            'json_logstash_fmtr': {
                '()': 'logstash_formatter.LogstashFormatterV1',
                'json_cls': LogstashJsonEncoder},
        },
        'handlers': {
            'null': {
                'level': 'INFO',
                'class': 'logging.NullHandler',
            },
            'default': {
                'level': 'INFO',
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'filename': path + '/treebo_async_job.log',
                'when': 'midnight',
                'formatter': 'json_logstash_fmtr',
                'interval': 1,
                'backupCount': 0,
            },
            'easyjoblite': {
                'level': 'INFO',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': path + '/treebo_async_job.log',
                'formatter': 'json_logstash_fmtr',
            }},
        'loggers': {
            'easyjoblite': {
                'handlers': ['easyjoblite'],
                'level': 'INFO'},
            '': {
                'handlers': ['default'],
                'level': 'INFO',
            },
        },
    }
    return logging_conf


def main():
    """
    :return:
    """
    parser = ArgumentParser()

    # todo: get rabbitmq config params from command line (e.g. user, passwd,
    # host separately)
    parser.add_argument("command",
                        help="start/stop command",
                        default='start')

    args = parser.parse_args()

    if args.command == "start":
        start()
    elif args.command == "stop":
        stop()
    else:
        print("Invalid command")


if __name__ == '__main__':
    sys.exit(main() or 0)
