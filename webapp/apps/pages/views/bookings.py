from datetime import datetime

from apps.profiles.service.profile_service import UserProfileService
from base.views.template import TreeboTemplateView
from common.constants import common_constants
from apps.common import date_time_utils
from data_services.respositories_factory import RepositoriesFactory
from services.restclient.bookingrestclient import BookingClient
from services.restclient.supportmodels.bookingaux import Booking

__author__ = 'varunachar'


class Bookings(TreeboTemplateView):
    template_name = "booking/index.html"

    hotel_repository = RepositoriesFactory.get_hotel_repository()
    room_repository = RepositoriesFactory.get_room_repository()

    def __init__(self, **kwargs):
        super(Bookings, self).__init__(**kwargs)
        self.__userService = UserProfileService()

    def getData(self, request, args, kwargs):
        result = []
        user = request.user
        bookingsJson = BookingClient.getBookingsByUserEmail(
            request, user.email, user.id)

        for bookingJson in bookingsJson:
            booking = Booking(bookingJson)
            grandTotal = "{0:.2f}".format(booking.totalAmount)
            if grandTotal < 0:
                grandTotal = -grandTotal

            isCancellable = booking.checkoutDate > datetime.today().date(
            ) and booking.status == common_constants.BOOKING_CONFIRM

            if isCancellable:
                cancelUrl = '/api/v1/booking/cancel?orderId={0}&email={1}'.format(
                    booking.orderId, booking.guestEmail)
            else:
                cancelUrl = ''

            roomCount, roomId = BookingClient.getRoomBookingByBookingId(
                request, booking.id)
            roomObj = self.room_repository.get_single_Room(id=int(roomId))
            #roomObj = Room.objects.get(id=int(roomId))
            roomType = roomObj.room_type

            booking = {
                'checkIn': booking.checkinDate.strftime(date_time_utils.DATE_FORMAT),
                'checkOut': booking.checkoutDate.strftime(date_time_utils.DATE_FORMAT),
                'amount': booking.totalAmount,
                'nights': (booking.checkoutDate - booking.checkinDate).days,
                'guests': booking.adultCount + booking.childCount,
                'rooms': roomCount,
                'bookingId': booking.groupCode if booking.groupCode else booking.bookingCode[:-1],
                'bookingDate': booking.createdAt.strftime(date_time_utils.DATE_FORMAT),
                'travellerName': booking.guestName,
                'travellerNumber': booking.guestMobile,
                'status': booking.status,
                'isCancellable': isCancellable,
                'cancelUrl': cancelUrl,
                'printUrl': '/rest/v1/bookings/printpdf?name={0}&order_id={1}'.format(
                    booking.guestName, booking.orderId),
                'hotel': self.__getHotelDetailsDict(booking, roomType),
                'paymentInfo': self.__getPaymentDetailsDict(booking, grandTotal)
            }
            result.append(booking)

        context = {
            'result': result,
            'styles': ['css/booking.css'],
            'scripts': ['js/bookings.js']
        }
        return context

    def getMobileData(self, request, args, kwargs):

        result = []
        user = request.user
        bookingsJson = BookingClient.getBookingsByUserEmail(
            request, user.email, user.id)

        for bookingJson in bookingsJson:
            booking = Booking(bookingJson)
            grandTotal = "{0:.2f}".format(booking.totalAmount)
            if grandTotal < 0:
                grandTotal = -grandTotal

            isCancellable = booking.checkoutDate > datetime.today().date(
            ) and booking.status == common_constants.BOOKING_CONFIRM

            if isCancellable:
                cancelUrl = '/rest/v1/cancelbooking?orderId={0}&email={1}'.format(
                    booking.orderId, booking.guestEmail)
            else:
                cancelUrl = ''

            roomCount, roomId = BookingClient.getRoomBookingByBookingId(
                request, booking.id)
            roomObj = self.room_repository.get_single_Room(id=int(roomId))
            #roomObj = Room.objects.get(id=int(roomId))
            roomType = roomObj.room_type

            booking = {
                'checkIn': booking.checkinDate.strftime(date_time_utils.DATE_FORMAT),
                'checkOut': booking.checkoutDate.strftime(date_time_utils.DATE_FORMAT),
                'amount': booking.totalAmount,
                'nights': (booking.checkoutDate - booking.checkinDate).days,
                'guests': booking.adultCount + booking.childCount,
                'rooms': roomCount,
                'bookingId': booking.groupCode if booking.groupCode else booking.bookingCode[:-1],
                'bookingDate': booking.createdAt.strftime(date_time_utils.DATE_FORMAT),
                'travellerName': booking.guestName,
                'travellerNumber': booking.guestMobile,
                'status': booking.status,
                'isCancellable': isCancellable,
                'cancelUrl': cancelUrl,
                'printUrl': '/rest/v1/bookings/printpdf?name={0}&order_id={1}'.format(
                    booking.guestName, booking.orderId),
                'hotel': self.__getHotelDetailsDict(booking, roomType),
                'paymentInfo': self.__getPaymentDetailsDict(booking, grandTotal)
            }
            result.append(booking)

        context = {
            'result': result,
            'styles': ['css/booking.css'],
            'scripts': ['js/bookings.js']
        }
        return context

    def __getHotelDetailsDict(self, booking, roomType):
        hotel = self.hotel_repository.get_single_hotel(id=booking.hotel)
        #hotel = self.__getHotel(booking.hotel)
        hotelDict = {
            'name': hotel.name,
            'city': hotel.city.name,
            'address': hotel.street,
            'locality': hotel.locality.name,
            'roomType': roomType
        }
        return hotelDict

    def __getPaymentDetailsDict(self, booking, grandTotal):
        paymentDict = {
            'subTotal': "{0:.2f}".format(
                booking.totalAmount +
                booking.discount),
            'discount': "{0:.2f}".format(
                booking.discount),
            'grandTotal': grandTotal,
            'paymentMode': booking.paymentMode}
        return paymentDict
