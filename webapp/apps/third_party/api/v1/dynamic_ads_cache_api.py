from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

from apps.third_party.tasks import set_cache_for_feed_component_or_build_csv
from base.views.api import TreeboAPIView


class DynamicAdsCacheSetApi(TreeboAPIView):

    def get(self, request, **kwargs):
        feed_component = kwargs.get("feed_component")
        if not feed_component:
            return Response(status = HTTP_400_BAD_REQUEST, data = "No value to set")
        set_cache_for_feed_component_or_build_csv.apply_async(args=['cache',feed_component])
        return Response(status = 200, data = "Cache for given value will be set")
