import logging

from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response

from apps.auth.csrf_exempt_session_authentication import \
    CsrfExemptSessionAuthentication
from apps.checkout.service.phonepe_status_pending_cron import PhonePeStatusPendingCronService
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class PhonePeStatusPendingCronAPI(TreeboAPIView):
    authentication_classes = (
        BasicAuthentication, CsrfExemptSessionAuthentication)


    def get(self, request, *args, **kwargs):
        PhonePeStatusPendingCronService().start()
        return Response(data ='Processing pending payments')


    def post(self, request, *args, **kwargs):
        raise Exception('Method not supported')



