# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('featuregate', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='variablecondition',
            name='final_value',
            field=models.CharField(default=b'', max_length=2000, null=True),
        ),
    ]
