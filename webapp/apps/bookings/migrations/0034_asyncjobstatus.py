# -*- coding: utf-8 -*-


from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0033_auto_20170418_1355'),
    ]

    operations = [
        migrations.CreateModel(
            name='AsyncJobStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('job_id', models.CharField(unique=True, max_length=100, db_index=True)),
                ('tag', models.CharField(max_length=100)),
                ('data', jsonfield.fields.JSONField()),
                ('code', models.PositiveIntegerField(default=100)),
                ('message', models.CharField(default='', max_length=300)),
                ('status', models.CharField(default=b'CREATED', max_length=100)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
