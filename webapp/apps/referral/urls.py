from django.conf.urls import url

from apps.referral.api.v1.referral_share import ReferralShareAPI
from apps.referral.api.v1.referral_webhooks import FriendRewardWebhook, ReferrerRewardWebhook, \
    ConversionEventWebhook
from apps.referral.api.v1.register_signup import RegisterSignUp

urlpatterns = [
    url(r"^friend/$", FriendRewardWebhook.as_view(), name="friend-reward"),
    url(r"^referrer/$", ReferrerRewardWebhook.as_view(), name="referrer-reward"),
    url(r"^conversion/$", ConversionEventWebhook.as_view(), name="conversion-event"),
    url(r"^share/$", ReferralShareAPI.as_view(), name="user-share"),
    url(r"^signup/$", RegisterSignUp.as_view(), name="signup"),
]
