import logging

from apps.hotels.service.hotel_meta_service import HotelMetaService
from dbcommon.models.hotel import HotelMetaData

logger = logging.getLogger(__name__)


class HotelMetaHandlers(object):
    """
    Service to encompass all handlers for meta keys.
    In case a handler for key is not specified it will select the default handler.
    If the handler was specified but not defined in this service an AttributeError will be raised.

    All handlers will take Hotels Queryset, Key on which it has to filter and any other arguments. The handler
    can decide how to apply the filter based on the arguments provided.

    All handlers will return the queryset again after applying respective filters.
    """
    model = HotelMetaData

    _handler_map = {HotelMetaService.Keys.B2B_SALEABLE: 'saleable_handler'}

    @classmethod
    def get_handler(cls, key):
        try:
            handler_name = cls._handler_map[key]
            return getattr(cls, handler_name)
        except KeyError:
            return cls.default_handler

    @classmethod
    def execute_handler(cls, hotels, key, **kwargs):
        handler = cls.get_handler(key)
        return handler(hotels, key, **kwargs)

    @classmethod
    def default_handler(cls, hotels, key, **kwargs):
        hotels_with_key = cls.model.objects.filter(
            key=key).values_list(
            'hotel_id', flat=True)
        return hotels.filter(id__in=hotels_with_key)

    @classmethod
    def saleable_handler(cls, hotels, key, **kwargs):
        # we are overriding key to be black list and then exclude from list
        key = cls.model.Keys.B2B_BLACKLIST
        enabled = '1'
        hotels_with_key = cls.model.objects.filter(
            key=key, value=enabled).values_list(
            'hotel_id', flat=True)
        return [hotel for hotel in hotels if hotel.id not in hotels_with_key]
