from apps.content.meta_cache_service import MetaCacheService
from apps.posts.models import Question, Answer, QAMapping
from apps.posts.service import PostService
from services.restclient.contentservicerestclient import ContentServiceClient

__author__ = 'ansilkareem'

from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from services.notification.base_notification_service import NotificationService


@receiver(post_save, sender=Question)
def send_communication1(sender, instance, **kwargs):
    '''send communication to user when a question is added or approved'''
    if instance.submitted_by_user:
        if not instance.state:
            content = ContentServiceClient.getValueForKey(
                None, 'QA_template_question', 1)
            notification = NotificationService()
            if instance.user.phone_number:
                sms_text = content['sms']
                notification.send_sms_ns(
                    [instance.user.phone_number], sms_text)
            if instance.user.email:
                email_content = content['email']['body']
                subject = content['email']['subject']
                notification.send_email(
                    [instance.user.email], email_content, subject)


@receiver(post_save, sender=Answer)
def send_communication2(sender, instance, **kwargs):
    '''send communication to user when an answer is added'''
    # if instance.related_question.submitted_by_user:
    #     if instance.state == Answer.APPROVED:
    #         content = ContentServiceClient.getValueForKey(
    #             None, 'QA_template_answer', 1)
    #         notification = NotificationService()
    #         if instance.user.phone_number:
    #             sms_text = content['sms']
    #             notification.send_sms_ns(
    #                 [instance.user.phone_number], sms_text)
    #         if instance.user.email:
    #             email_content = content['email']['body']
    #             subject = content['email']['subject']
    #             notification.send_email(
    #                 [instance.user.email], email_content, subject)


@receiver(post_save, sender=QAMapping)
def update_cache(sender, instance, **kwargs):
    '''update cache when a new mapping is done'''
    meta_cache_service = MetaCacheService(
        instance.page_type, instance.page_type_id)
    meta_cache_data = meta_cache_service.get_cache_data()
    if meta_cache_data:
        post_service = PostService(instance.page_type, instance.page_type_id)
        meta_cache_data['content']['question_answer'] = post_service.get_post()
        meta_cache_service.set_cache(meta_cache_data)
