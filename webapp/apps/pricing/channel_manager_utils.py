import json
import logging
import string
from datetime import datetime

from bs4 import BeautifulSoup
from django.db.models import Q

from apps.pricing import event_publishers
from apps.pricing import tasks
from apps.pricing.models import PreferredPromo
from apps.pricing.models import PromoConditions
from base.middlewares.set_request_id import get_current_user, get_current_request_id
from common.utilities.date_time_utils import DATE_FORMAT
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.room import Room
from dbcommon.models.hotel import Hotel
from data_services.hotel_repository import HotelRepository
from data_services.room_repository import RoomRepository

logger = logging.getLogger(__name__)

STANDARD_PRICE_INCREMENT = 300


def getHotelRatePlanResponse(hotelId, echoToken):
    room_repository = RepositoriesFactory.get_room_repository()
    hotelRooms = room_repository.get_rooms_for_hotel(hotel_id=hotelId)
    #hotelRooms = room_repository.filter_rooms(hotel_id=hotelId)
    timestampString = datetime.now().strftime(
        '%Y-%m-%dT%H:%M:%S.%f')[:-3] + "Z"
    startTag = '<OTA_HotelRatePlanRS xmlns="http://www.opentravel.org/OTA/2003/05" EchoToken="' + \
        echoToken + '" TimeStamp="' + timestampString + '" Version="1.0">'

    endTag = '</OTA_HotelRatePlanRS>'
    successTag = '<Success/>'
    ratePlansTag = '<RatePlans HotelCode="' + str(hotelId) + '">'
    ratePlansEndTag = '</RatePlans>'
    roomTypePrefix = '<RatePlan CurrencyCode="INR"	RatePlanStatusType="Active" ><Rates>'
    roomTypePostfix = '</Rates><Description Name="Short Description"><Text Language="USENGLISH"></Text> </Description></RatePlan>'
    rates = ''

    for room in hotelRooms:
        currentRate = '<Rate InvCode="' + room.room_type + '" InvTypeCode="' + \
            room.room_type_code + '" NumberOfUnits="' + str(room.quantity) + '" />'
        rates += currentRate

    ratePlanData = startTag + successTag + ratePlansTag + \
        roomTypePrefix + rates + roomTypePostfix + ratePlansEndTag + endTag
    return ratePlanData


def getHotelCodeAndEchoFromHotelRatePlanRequest(xmlRequest):
    xmlRequest = BeautifulSoup(xmlRequest, 'xml')
    ratePlanRq = xmlRequest.find('OTA_HotelRatePlanRQ')
    echoToken = ratePlanRq['EchoToken']
    hotelRef = ratePlanRq.find('HotelRef')
    hotelId = int(hotelRef['HotelCode'])
    return hotelId, echoToken


def updatePreferredPromoFromChannelManager(xmlPromoRequest):
    xmlRequest = BeautifulSoup(xmlPromoRequest, 'xml')
    allPromotions = xmlRequest.findAll('HotelPromotion')
    allPromotionResponses = []
    for promo in allPromotions:
        promoIdTag = promo.find('Id')

        hotelCode = promo.find('HotelId').string
        hotel = Hotel.objects.get(id=int(hotelCode))
        description = promo.find('Description').string
        type = promo.find('Type').string
        status = promo.find('status').string
        roomList = []
        roomTypes = promo.findAll('RoomType')
        rooms = hotel.rooms

        for roomType in roomTypes:
            for room in rooms:
                if roomType['RoomTypeID'] == room.room_type_code:
                    roomList.append(roomType['RoomTypeID'])

        min_advance_days = promo.find('MinAdvPurchase').string
        max_advance_days = promo.find('MaxAdvPurchase').string
        min_nights = promo.find('MinStay').string
        refundableStr = promo.find('IsRefundable').string
        refundable = True
        if refundable != 'True':
            refundable = False
        stay_start = None
        stay_end = None
        stay_start_str = None
        stay_end_str = None
        booking_start = None
        booking_end = None
        booking_days_bitmap = 1111111
        stay_days_bitmap = 1111111

        dateRanges = promo.findAll('DateRange')
        for dateRange in dateRanges:
            dateRangeType = dateRange['Type']
            if dateRangeType == 'Stay':
                stay_start_str = dateRange['Start']
                stay_end_str = dateRange['End']
                stay_start = datetime.strptime(
                    stay_start_str, DATE_FORMAT).date()
                stay_end = datetime.strptime(stay_end_str, DATE_FORMAT).date()
                stay_days_bitmap = int(dateRange['DOW'])
            elif dateRangeType == 'Book':
                booking_start_str = dateRange['Start']
                booking_end_str = dateRange['End']
                booking_start = datetime.strptime(
                    booking_start_str, DATE_FORMAT).date()
                booking_end = datetime.strptime(
                    booking_end_str, DATE_FORMAT).date()
                booking_days_bitmap = int(dateRange['DOW'])
        promoBenefit = promo.find('PromotionBenefit')
        discountType = promoBenefit.find('DiscountType').string
        discountValueMondayTag = promoBenefit.find('DiscountValueMon1')
        if discountValueMondayTag is not None:
            discountValueMonday = int(discountValueMondayTag.string)
        else:
            discountValueMonday = 0
        discountValueTuesdayTag = promoBenefit.find('DiscountValueTue2')
        if discountValueTuesdayTag is not None:
            discountValueTuesday = int(discountValueTuesdayTag.string)
        else:
            discountValueTuesday = 0
        discountValueWednesdayTag = promoBenefit.find('DiscountValueWed3')
        if discountValueWednesdayTag is not None:
            discountValueWednesday = int(discountValueWednesdayTag.string)
        else:
            discountValueWednesday = 0
        discountValueThursdayTag = promoBenefit.find('DiscountValueThu4')
        if discountValueThursdayTag is not None:
            discountValueThursday = int(discountValueThursdayTag.string)
        else:
            discountValueThursday = 0
        discountValueFridayTag = promoBenefit.find('DiscountValueFri5')
        if discountValueFridayTag is not None:
            discountValueFriday = int(discountValueFridayTag.string)
        else:
            discountValueFriday = 0
        discountValueSaturdayTag = promoBenefit.find('DiscountValueSat6')
        if discountValueSaturdayTag is not None:
            discountValueSaturday = int(discountValueSaturdayTag.string)
        else:
            discountValueSaturday = 0
        discountValueSundayTag = promoBenefit.find('DiscountValueSun7')
        if discountValueSundayTag is not None:
            discountValueSunday = int(discountValueSundayTag.string)
        else:
            discountValueSunday = 0

        discountCondition = {
            'monday': discountValueMonday,
            'tuesday': discountValueTuesday,
            'wednesday': discountValueWednesday,
            'thursday': discountValueThursday,
            'friday': discountValueFriday,
            'saturday': discountValueSaturday,
            'sunday': discountValueSunday}

        preferredPromo = PreferredPromo()
        if promoIdTag is not None:
            promoId = promoIdTag.string
            if bool(promoId):
                try:
                    preferredPromo = PreferredPromo.objects.get(id=promoId)
                except PreferredPromo.DoesNotExist:
                    logger.info(
                        "Trying to update promotion that doesn't exist in db")

        existing_promo = PreferredPromo.objects.filter(
            hotel=hotel,
            description=description,
            type=type,
            stay_start=stay_start,
            stay_end=stay_end,
            booking_start=booking_start,
            booking_days_bitmap=booking_days_bitmap,
            stay_days_bitmap=stay_days_bitmap,
            status=0 if status != 'Active' else 1,
            min_nights=int(min_nights),
            min_advance_days=int(min_advance_days),
            max_advance_days=int(max_advance_days)).first()

        if not preferredPromo.id and existing_promo:
            allPromotionResponses.append(getXmlObjectForPromo(existing_promo))
        else:
            preferredPromo.hotel = hotel
            preferredPromo.description = description
            preferredPromo.type = type
            preferredPromo.status = status
            preferredPromo.min_advance_days = int(min_advance_days)
            preferredPromo.max_advance_days = int(max_advance_days)
            preferredPromo.min_nights = int(min_nights)
            preferredPromo.refundable = bool(refundable)
            preferredPromo.stay_start = stay_start
            preferredPromo.stay_end = stay_end
            preferredPromo.booking_start = booking_start
            preferredPromo.booking_end = booking_end
            preferredPromo.booking_days_bitmap = booking_days_bitmap
            preferredPromo.stay_days_bitmap = stay_days_bitmap
            promo_status = 1
            if status != 'Active':
                promo_status = 0
            preferredPromo.status = promo_status
            preferredPromo.save()

            # NOTE: This deletion was not happening earlier, due to which there were multiple
            # promo conditions getting associated with a promotion, which was resulting in
            # undefined behaviour when selection the best promotion discount
            PromoConditions.objects.filter(promo_id=preferredPromo.id).delete()

            roomCondition = PromoConditions()
            roomCondition.condition_name = 'RoomTypeList'
            roomCondition.condition_params = json.dumps(roomList)
            roomCondition.promo = preferredPromo
            roomCondition.save()
            newCondition = PromoConditions()
            newCondition.condition_name = 'DayWiseDiscountValues'
            newCondition.condition_params = json.dumps(discountCondition)
            newCondition.promo = preferredPromo

            newCondition.save()

            event_publishers.publish_promo(hotel.id, [preferredPromo])

            allPromotionResponses.append(getXmlObjectForPromo(preferredPromo))
            tasks.updatePrices.delay(
                hotel.id,
                stay_start_str,
                stay_end_str,
                user_id=get_current_user(),
                request_id=get_current_request_id())

    return getSetPromotionResponseforPromotions(allPromotionResponses)


def getPromotionsXmlFromDatabase(hotelId):
    allPreferredPromos = PreferredPromo.objects.filter(
        Q(hotel_id=hotelId), Q(status=1)).prefetch_related('conditions')
    allPromosXmlList = []
    for promo in allPreferredPromos:
        allPromosXmlList.append(getXmlObjectForPromo(promo))

    return allPromosXmlList


def getSetPromotionResponseforPromotions(promoList):
    responseXmlTemplate = string.Template(""" <GetPromotionsResponse>
    <HotelPromotions>
    $allPromotions
    </HotelPromotions>
    </GetPromotionsResponse>""")

    responseXml = responseXmlTemplate.substitute(
        allPromotions="\n".join(promoList))
    logger.info('SetPromotion Response: %s', responseXml)
    return responseXml


def getPromotionsXmlResponse(hotelId):
    responseXmlTemplate = string.Template(""" <GetPromotionsResponse>
    <HotelPromotions>
    $allPromotions
    </HotelPromotions>
    </GetPromotionsResponse>""")
    allPromoXml = getPromotionsXmlFromDatabase(hotelId)

    responseXml = responseXmlTemplate.substitute(
        allPromotions="\n".join(allPromoXml))
    return responseXml


def getXmlObjectForPromo(promo):
    roomTypeListTemplate = string.Template(
        """<RoomTypeList>$roomTypeList</RoomTypeList> """)
    roomTypeTemplate = string.Template(
        """<RoomType RoomTypeID="$roomType"></RoomType> """)
    dateRangeTemplate = string.Template(
        """<DateRange Type="$type" Start="$start" End="$end" DOW="$DOW"/> """)
    discountValueTemplate = string.Template(
        """ <$discountValueTag>$value</$discountValueTag>""")
    promoBenefitTemplate = string.Template(
        """ <PromotionBenefit> <DiscountType>$discountType</DiscountType> $discountValueList</PromotionBenefit>""")
    promoTemplate = string.Template("""<HotelPromotion>
    <Id>$promoId</Id>
    <HotelId>$hotelId</HotelId>
    <Description>$description</Description>
    <Type>$type</Type>
    <status>$status</status>
    <PromotionConditions>
    $roomTypeList
     <MinAdvPurchase>$minAdvancePurchase</MinAdvPurchase>
     <MaxAdvPurchase>$maxAdvancePurchase</MaxAdvPurchase>
     <MinStay>$minStay</MinStay>
     $stayDateRange
     $bookDateRange
    <IsRefundable>$refundable</IsRefundable>
    </PromotionConditions>
    $promoBenefits
    </HotelPromotion> """)
    conditions = promo.conditions.all()
    conditionDict = {}
    for condition in conditions:
        conditionDict[condition.condition_name] = condition

    roomTypeList = json.loads(conditionDict['RoomTypeList'].condition_params)
    print(roomTypeList)
    roomTypeTags = []

    for roomType in roomTypeList:
        roomTypeTags.append(roomTypeTemplate.substitute(roomType=roomType))
    roomTypeListTag = roomTypeListTemplate.substitute(
        roomTypeList="\n".join(roomTypeTags))
    stayDateRange = dateRangeTemplate.substitute(
        type='Stay', start=promo.stay_start, end=promo.stay_end, DOW=str(
            promo.stay_days_bitmap).zfill(7))
    bookingDateRange = dateRangeTemplate.substitute(
        type='Book', start=promo.booking_start, end=promo.booking_end, DOW=str(
            promo.booking_days_bitmap).zfill(7))
    dayWiseDiscounts = conditionDict['DayWiseDiscountValues']
    discountValueKeyMap = {
        'monday': 'DiscountValueMon1',
        'tuesday': 'DiscountValueTue2',
        'wednesday': 'DiscountValueWed3',
        'thursday': 'DiscountValueThu4',
        'friday': 'DiscountValueFri5',
        'saturday': 'DiscountValueSat6',
        'sunday': 'DiscountValueSun7'}
    discountValues = []
    print((dayWiseDiscounts.condition_params))
    dayWiseDiscountsDict = json.loads(dayWiseDiscounts.condition_params)

    for mapKey in list(discountValueKeyMap.keys()):
        print(mapKey)
        print((dayWiseDiscountsDict[mapKey]))
        if int(dayWiseDiscountsDict[mapKey]) > 0:
            discountValues.append(
                discountValueTemplate.substitute(
                    discountValueTag=discountValueKeyMap[mapKey], value=int(
                        dayWiseDiscountsDict[mapKey])))

    promoBenefitTag = promoBenefitTemplate.substitute(
        discountType=promo.discountType,
        discountValueList="\n".join(discountValues))
    promoStatus = 'Active'
    if promo.status != 1:
        promoStatus = 'Expired'
    promoTag = promoTemplate.substitute(
        promoId=promo.id,
        hotelId=promo.hotel.id,
        description=promo.description,
        type=promo.type,
        status=promoStatus,
        roomTypeList=roomTypeListTag,
        minAdvancePurchase=promo.min_advance_days,
        maxAdvancePurchase=promo.max_advance_days,
        minStay=promo.min_nights,
        stayDateRange=stayDateRange,
        bookDateRange=bookingDateRange,
        refundable=promo.refundable,
        promoBenefits=promoBenefitTag)
    return promoTag


def deletePromotions(xmlDeletePromoRequest):
    promoXml = BeautifulSoup(xmlDeletePromoRequest, 'xml')
    promos = promoXml.findAll('HotelPromotion')
    promo_ids = []
    for promo in promos:
        idTag = promo.find('Id')
        promoId = idTag.string
        promo_ids.append(promoId)
        PreferredPromo.objects.filter(id=promoId).delete()

    logger.info("Promos deleted: %s", promo_ids)
    event_publishers.publish_promo_deletion(promo_ids)
