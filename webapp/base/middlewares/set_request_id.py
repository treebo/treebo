from apps.common.thread_local_utils import get_current_request_id, \
    get_current_user, get_thread_variable, set_thread_variable, delete_thread_variable


def get_job_data():
    """
    :return:
    """
    from apps.bookings.utils import async_local
    job_id = getattr(async_local, 'job_id', None)
    request_id = getattr(async_local, 'request_id', None)
    if not request_id:
        request_id = get_thread_variable('request_id')
    return {
        'job_id': job_id,
        'request_id': request_id
    }


class UserIDMiddleware(object):
    """
        UserIDMiddleware
    """

    def process_request(self, request):
        """
        :param request:
        :return:
        """
        if request.user.is_authenticated():
            set_thread_variable('user_id', request.user.id)
        elif hasattr(request, 'user_id'):
            set_thread_variable('user_id', request.user_id)

    def process_response(self, request, response):
        """
        :param request:
        :param response:
        :return:
        """
        delete_thread_variable('user_id')
        return response
