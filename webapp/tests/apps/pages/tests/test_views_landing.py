import mock
from django.test import TestCase, RequestFactory

from apps.common import utils
from apps.pages.views import landing


class IndexTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_get_common_context_authenticated_user(self):
        request = self.factory.get('/test_path')
        request.user = mock.Mock()
        request.user.is_authenticated().return_value = True
        request.session = {}
        index_view = landing.Index()
        context = index_view._Index__get_common_context(request)
        self.assertEqual(context['user'], request.user)

    def test_get_common_context_non_authenticated_user(self):
        request = self.factory.get('/test_path')
        request.user = mock.Mock()
        request.user.is_authenticated().return_value = False
        request.session = {}
        index_view = landing.Index()
        context = index_view._Index__get_common_context(request)
        self.assertEqual(context['user'], request.user)

    # Here we patch CityHotelHelper from landing module, since we've imported that class. So lookup will happen in that module instead of hotels.helpers.
    # That is not the case with utils.Utils.getMobileImageSize, since we've
    # imported `utils` in landing.py instead of utils.Utils
    @mock.patch.object(utils.Utils, 'getMobileImageSize')
    @mock.patch.object(utils.Utils, 'build_image_params')
    def test__get_common_context_data(
            self,
            mock_build_image_params,
            mock_get_mobile_image_size):
        # setup mocked method call behaviour
        width, height = mock.Mock(), mock.Mock()
        mock_get_mobile_image_size.return_value = [width, height]
        mock_build_image_params.return_value = mock.Mock()
        request = self.factory.get('/test-path')
        request.user = mock.Mock(**{'is_authenticated.return_value': True})
        request.session = {}
        index_view = landing.Index()
        context = index_view._Index__get_common_context(request)
        # Check result
        self.assertEqual(
            context['imageCityParams'],
            mock_build_image_params.return_value)
        # verify mocked method call
        mock_get_mobile_image_size.assert_called_once_with('home')
        mock_build_image_params.assert_called_once_with(
            width, height, mock.ANY, mock.ANY, mock.ANY)
