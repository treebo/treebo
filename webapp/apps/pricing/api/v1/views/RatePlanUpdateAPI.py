import logging
from datetime import datetime

from django.http import HttpResponse
from rest_framework.views import APIView

from apps.pricing import tasks, text_xml_parser, xmlrendrer
from base import log_args
from base.middlewares.set_request_id import get_current_user, get_current_request_id
from common.exceptions.treebo_exception import TreeboValidationException, TreeboException

logger = logging.getLogger(__name__)
error_response = '<OTA_HotelRateAmountNotifRS TimeStamp="%s" Version="0" xmlns="http://www.opentravel.org/OTA/2003/05"><Errors/></OTA_HotelRateAmountNotifRS>'
success_response = '<OTA_HotelRateAmountNotifRS TimeStamp="%s" Version="0" xmlns="http://www.opentravel.org/OTA/2003/05"><Success/></OTA_HotelRateAmountNotifRS>'


class UpdateRateAmount(APIView):
    parser_classes = (text_xml_parser.XMLParser,)
    renderer_classes = (xmlrendrer.XMLRenderer,)

    @log_args(logger)
    def post(self, request):
        try:
            tasks.update_rates_from_hx.delay(
                request.data,
                request_id=get_current_request_id(),
                user_id=get_current_user())
        except TreeboValidationException:
            return HttpResponse(
                error_response %
                self.get_timestamp(),
                content_type="application/xml",
                status=400)
        except TreeboException:
            return HttpResponse(
                error_response %
                self.get_timestamp(),
                content_type="application/xml",
                status=500)
        return HttpResponse(
            success_response %
            self.get_timestamp(),
            content_type="application/xml")

    def get_timestamp(self):
        timestamp_string = datetime.now().strftime(
            '%Y-%m-%dT%H:%M:%S.%f')[:-3] + "Z"
        return timestamp_string

    def perform_authentication(self, request):
        pass
