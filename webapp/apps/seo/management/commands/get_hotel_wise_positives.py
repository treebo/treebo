import logging

from django.core.management import BaseCommand

from apps.seo.services.guest_love_data.get_hotel_positives_hd import FetchHotelsPositives

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            hotel_positives = FetchHotelsPositives()
            hotel_positives.get_all_hotels_positive()
        except Exception:
            logger.exception(
                "Exception in fetching/updating the positives data for meta api")
