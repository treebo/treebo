import json
import logging
from decimal import Decimal

from apps.checkout.models import BookingRequest, BookingRequestUserMapForWallet, CommittedPrice
from apps.checkout.service.checkout_service import create_booking_request
from apps.common.slack_alert import SlackAlertService as slack_alert
from apps.common.exceptions.booking_exceptions import BookingNotFoundException, \
    InvalidBookingRequestException, BookedHotelNotFound
from apps.common.exceptions.custom_exception import CouponException, InvalidDiscountCouponException
from apps.discounts.exceptions import InvalidCouponException
from apps.discounts.services.discount_coupon_services import DiscountCouponServices
from apps.discounts.services.hotel_coupon_auto_apply_service import HotelCouponAutoApplyService
from apps.checkout.service.upfront_part_pay_service import UpfrontPartPayService
from apps.hotels import utils as hotel_utils
from apps.pages.helper import PageHelper
from apps.pricing.exceptions import NoPriceForRatePlan
from apps.pricing.services.price_request_dto import PriceRequestDto
from apps.pricing.services.pricing_service import SoaError
from apps.pricing.services.pricing_service_v2 import PricingServiceV2
from apps.pricing.transformers.itinerary_api_transformer import ItineraryAPITransformer
from common.services.feature_toggle_api import FeatureToggleAPI
from dbcommon.models.hotel import Hotel
from apps.featuregate.constants import FEATURE_PAGE_NAMES

logger = logging.getLogger(__name__)


def get_itinerary_data_from_booking_id(booking_id):
    try:
        booking_request = BookingRequest.objects.get(booking_id=booking_id)
    except Exception as e:
        logger.exception('Booking request not found booking _id: %s', booking_id)
        raise BookingNotFoundException()
    try:
        hotel_info = Hotel.objects.get(id=booking_request.hotel_id)
    except Exception as e:
        logger.exception('Hotel not found for booking request: %s and hotel id: %s', booking_request.id,
                         booking_request.hotel_id)
        raise BookedHotelNotFound()
    
    return {
        "bid": booking_id,
        "checkin": booking_request.checkin_date,
        "checkout": booking_request.checkout_date,
        "city": hotel_info.city.name,
        "hotel_id": booking_request.hotel_id,
        "rateplan": booking_request.rate_plan,
        "roomconfig": booking_request.room_config,
        "roomtype": booking_request.room_type,
        "name": booking_request.name,
        "email": booking_request.email,
        "phone": booking_request.phone
    }


class ItineraryHandler(object):
    pricing_service = PricingServiceV2()
    price_response_transformer = ItineraryAPITransformer()
    autoapply_service = HotelCouponAutoApplyService()
    upfron_partpay_service = UpfrontPartPayService()
    
    def apply_coupon(
        self,
        request,
        bid,
        coupon_code=None,
        referral_click_id='',
        apply_wallet=False):
        try:
            booking_request = BookingRequest.objects.get(pk=bid)
        except BookingRequest.DoesNotExist as e:
            raise BookingNotFoundException('bid %d not presend ' % bid)
        utm_source = booking_request.utm_source
        current_user = getattr(request, 'user', None)
        logged_in_user = True if current_user and current_user.is_authenticated() else False
        logger.info(
            "apply_coupon %s for bid %s and apply_wallet %s",
            coupon_code,
            bid,
            apply_wallet)
        discount_percent = coupon = None
        
        if coupon_code:
            coupon, discount_percent = self.get_discount_percent(
                coupon_code, booking_request, current_user, referral_click_id)
        room_configs = booking_request.room_config.split(",")
        price_request_dto = self.__get_price_dto(
            booking_request=booking_request,
            room_configs=room_configs,
            logged_in_user=logged_in_user,
            coupon_code=coupon_code,
            discount_percent=discount_percent,
            coupon_type="Percentage",
            utm_source=utm_source,
            apply_wallet=apply_wallet)
        price = self.get_price(
                booking_request,
                price_request_dto,
                current_user)
        
        self.update_booking_request_with_prices(
            booking_request,
            price["selected_rate_plan"]["price"],
            current_user,
            bool(price_request_dto.coupon_code),
            coupon_code=price_request_dto.coupon_code,
            rate_plan_detail=price["selected_rate_plan"]["rate_plan"])
        self.add_committed_price_for_booking_request(
            booking_request,
            price["selected_rate_plan"]["price"]
        )

        return price, coupon

    def remove_coupon(self, request, bid, apply_wallet):
        try:
            booking_request = BookingRequest.objects.get(pk=bid)
        except BookingRequest.DoesNotExist as e:
            raise BookingNotFoundException('bid %d not presend ' % bid)
        current_user = getattr(request, 'user', None)
        logged_in_user = True if current_user and current_user.is_authenticated() else False
        room_configs = booking_request.room_config.split(",")
        booking_channel = booking_request.booking_channel
        if booking_channel in ['msite', 'android', 'ios']:
            new_version = True
        logger.info(
            "remove_coupon %s for bid %s and apply_wallet %s",
            booking_request.coupon_code,
            bid,
            apply_wallet)
        price_request_dto = self.__get_price_dto(
            booking_request=booking_request,
            room_configs=room_configs,
            logged_in_user=logged_in_user,
            coupon_code=None,
            discount_percent=None,
            coupon_type=None,
            utm_source=None,
            apply_wallet=apply_wallet)
        price = self.get_price(
                booking_request,
                price_request_dto,
                current_user)
        logger.info(
            "remove_coupon success for bid %s and apply_wallet %s with price details %s ",
            bid,
            apply_wallet,
            price)
        self.update_booking_request_with_prices(
                        booking_request,
                        price["selected_rate_plan"]["price"],
                        request.user,
                        False,
                        coupon_code='',
                        rate_plan_detail = price["selected_rate_plan"]["rate_plan"])
        self.add_committed_price_for_booking_request(
            booking_request,
            price["selected_rate_plan"]["price"]
        )

        return price

    def select_wallet(self, request, bid, apply_wallet):
        try:
            booking_request = BookingRequest.objects.get(pk=bid)
        except BookingRequest.DoesNotExist as e:
            raise BookingNotFoundException('bid %d not presend ' % bid)

        referral_click_id = str(request.COOKIES.get(
            'avclk')) if request.COOKIES.get('avclk') else None
        current_user = getattr(request, 'user', None)
        logged_in_user = True if current_user and current_user.is_authenticated() else False
        room_configs = booking_request.room_config.split(",")
        utm_source = booking_request.utm_source
        coupon_type = None
        coupon_code = None
        discount_percent = None
        logger.info(
            "applying wallet %s for bid %s and couponcode %s(%s)",
            apply_wallet,
            bid,
            booking_request.coupon_code,
            booking_request.coupon_apply)

        if booking_request.coupon_code:
            coupon_type = "Percentage"
            coupon_code = booking_request.coupon_code
            coupon, discount_percent = self.get_discount_percent(
                coupon_code, booking_request, current_user, referral_click_id)
        price_request_dto = self.__get_price_dto(
            booking_request=booking_request,
            room_configs=room_configs,
            logged_in_user=logged_in_user,
            coupon_code=coupon_code,
            discount_percent=discount_percent,
            coupon_type=coupon_type,
            utm_source=utm_source,
            apply_wallet=apply_wallet)
        price = self.get_price(
            booking_request,
            price_request_dto,
            current_user)
        logger.info(
            "applying wallet success %s for bid %s and couponcode %s(%s) for price %s",
            apply_wallet,
            bid,
            booking_request.coupon_code,
            booking_request.coupon_apply,
            price)
        self.update_booking_request_with_prices(
            booking_request,
            price["selected_rate_plan"]["price"],
            current_user,
            bool(price_request_dto.coupon_code),
            coupon_code=price_request_dto.coupon_code,
            rate_plan_detail=price["selected_rate_plan"]["rate_plan"])
        self.add_committed_price_for_booking_request(
            booking_request,
            price["selected_rate_plan"]["price"]
        )

        if logged_in_user and booking_request.wallet_applied:
            self.update_booking_request_user_map(booking_request, current_user)
        else:
            self.update_booking_request_user_map(booking_request, None)
        return price

    def update_booking_request_user_map(self, booking_request, user):
        bid_user = None
        if booking_request:
            try:
                bid_user = BookingRequestUserMapForWallet.objects.get(
                    booking_request=booking_request)
            except BookingRequestUserMapForWallet.DoesNotExist:
                pass
        if not bid_user:
            bid_user = BookingRequestUserMapForWallet()

        bid_user.booking_request = booking_request
        bid_user.user = user
        bid_user.save()

    def get_user_by_booking_request(self, booking_request):
        user = None
        if not booking_request:
            return user

        try:
            bid_user = BookingRequestUserMapForWallet.objects.get(
                booking_request=booking_request)
            user = bid_user.user
        except BookingRequestUserMapForWallet.DoesNotExist:
            pass

        return user
    
    def get_price(self, booking_request, price_request_dto, current_user):
        try:
            coupon_code = price_request_dto.coupon_code
            prices = self.pricing_service.get_itinerary_page_price(
                price_request_dto)
            if FeatureToggleAPI.is_enabled(FEATURE_PAGE_NAMES.ITINERARY, "upfront_partpay"):
                part_pay_applicable = self.upfron_partpay_service.is_applicable(FEATURE_PAGE_NAMES.ITINERARY,
                                                                                booking_request.utm_source,
                                                                                booking_request.booking_channel,
                                                                                booking_request.hotel_id)
            else:
                part_pay_applicable = False
    
            price = self.price_response_transformer.transform_price_for_display(
                    prices,
                    booking_request.rate_plan,
                    apply_wallet=price_request_dto.wallet_applied,
                    user=current_user,
                    coupon_code=coupon_code,
                    part_pay_applicable=part_pay_applicable
                )
        except SoaError as e:
            logger.exception(
                " SoaError Exception occurred while applying coupon code using SOA API")
            raise CouponException(str(e))
        except NoPriceForRatePlan as e:
            raise NoPriceForRatePlan(str(e))
        return price

    def get_or_update_booking_request(self, request, page_data):
        user = request.user
        aa_coupon_code = None
        booking_request = None
        # checking if the input booking request (if any) has a booking created against it
        if page_data.booking_request_id:
            try:
                booking_request = BookingRequest.objects.get(pk=page_data.booking_request_id)
            except BookingRequest.DoesNotExist:
                raise InvalidBookingRequestException("Invalid booking request id.")

            if booking_request.order_id:
                # repeat booking attempt! let's clear the page data booking id
                page_data.booking_request_id = None

        if not page_data.booking_request_id:
            try:
                booking_request = create_booking_request(request, page_data)
                page_data.booking_request_id = booking_request.id
            except Exception as e:
                logging.exception(" error while creating booking request ")
                raise InvalidBookingRequestException(e)
        else:
            # booking_request = BookingRequest.objects.get(pk=page_data.booking_request_id)
            if not page_data.room_type or not page_data.hashed_hotel_id:
                if not page_data.room_type:
                    page_data.room_type = booking_request.room_type
                if not page_data.hashed_hotel_id:
                    page_data.hashed_hotel_id = hotel_utils.generate_hash(
                        booking_request.hotel_id)
        if page_data.checkin_date != booking_request.checkin_date:
            booking_request.checkin_date = page_data.checkin_date
        if page_data.checkout_date != booking_request.checkout_date:
            booking_request.checkout_date = page_data.checkout_date
        if page_data.room_type != booking_request.room_type:
            booking_request.room_type = page_data.room_type
        if page_data.room_config != booking_request.room_config:
            booking_request.room_config = page_data.room_config
        if page_data.rate_plan != booking_request.rate_plan:
            booking_request.rate_plan = page_data.rate_plan
        if page_data.apply_wallet != booking_request.wallet_applied:
            booking_request.wallet_applied = page_data.apply_wallet

        if page_data.utm_source:
            if not booking_request.utm_source:
                booking_request.utm_source = page_data.utm_source
            hotel_coupon_map = self.autoapply_service.get_hotel_coupons_basis_hotel_ids_and_utm_source(
                [page_data.hotel_id], booking_request.utm_source, page_data.checkin_date, page_data.checkout_date)
            aa_coupon_code = hotel_coupon_map.get(int(page_data.hotel_id), '')
            if not aa_coupon_code:
                logger.error(
                    "aa_coupon_code not found %s for hotel %s  ",
                    aa_coupon_code,
                    page_data.hotel_id)

        coupon_code = page_data.coupon_code or aa_coupon_code
        booking_request.coupon_code = coupon_code
        booking_request.user_login_state = 'LOGGEDIN' if user and user.is_authenticated() else 'LOGGEDOUT'
        booking_request.user = user if user and user.is_authenticated() else None
        booking_request.adults, booking_request.children, \
        booking_request.room_required = PageHelper.parseRoomConfig(page_data.room_config)

        booking_request.save()

        if user and user.is_authenticated() and booking_request.wallet_applied:
            self.update_booking_request_user_map(booking_request, user)
        else:
            self.update_booking_request_user_map(booking_request, None)

        return booking_request

    def update_booking_request_with_prices(
        self,
        booking_request,
        prices,
        current_user,
        coupon_apply,
        coupon_code='',
        rate_plan_detail=''
    ):
        """
        rate_plan_detail will be dict of this form:
                {
                    "code": selected_rate_plan,
                    "description": rate_plan_meta.get("description"),
                    "tag": rate_plan_meta.get("tag")
                },
        :param booking_request:
        :param prices:
        :param current_user:
        :param coupon_apply:
        :param date_wise_pricing:
        :param coupon_code:
        :param rate_plan_detail:
        :return:
        """
        rp_price = prices
        booking_request.pretax_amount = rp_price['pretax_price']
        booking_request.rack_rate = rp_price["base_price"]
        booking_request.mm_promo = rp_price["promo_discount"]
        booking_request.discount_value = rp_price["coupon_discount"]
        booking_request.member_discount = rp_price['member_discount']
        booking_request.voucher_amount = rp_price.get('voucher_amount')
        booking_request.tax_amount = rp_price["tax"]
        booking_request.wallet_deduction = rp_price["wallet_deduction"] if rp_price["wallet_deduction"] else Decimal(
            0.0)
        booking_request.wallet_applied = rp_price["wallet_applied"]
        booking_request.user_login_state = 'LOGGEDIN' if current_user and current_user.is_authenticated() else 'LOGGEDOUT'
        booking_request.user = current_user if current_user and current_user.is_authenticated() else None
        if rate_plan_detail:
            booking_request.rate_plan_meta = rate_plan_detail

        booking_request.coupon_code = ""
        booking_request.coupon_apply = False
        if rp_price.get("coupon_discount") or rp_price.get("voucher_amount"):
            booking_request.coupon_code = coupon_code
        if booking_request.coupon_code:
            booking_request.coupon_apply = True

        booking_request.calculate_total_amount()
        booking_request.save()

    def add_committed_price_for_booking_request(self, booking_request, prices):
        """
        Add committed price for a booking request
        :param booking_request: booking_request
        :param prices: prices
        :return:
        """
        if 'nights_and_config_wise_breakup' in prices:
            date_and_room_wise_pricing = {}
            for date_and_room_wise_price_breakup in prices['nights_and_config_wise_breakup']:
                room_wise_pricing = date_and_room_wise_pricing.get(date_and_room_wise_price_breakup['date'])
                if not room_wise_pricing:
                    room_wise_pricing = dict()

                room_wise_pricing[date_and_room_wise_price_breakup['room_config']] = {
                    'sell_price': float(date_and_room_wise_price_breakup['sell_price'])
                }

                date_and_room_wise_pricing[date_and_room_wise_price_breakup['date']] = room_wise_pricing

            committed_price = CommittedPrice.objects.filter(booking_request=booking_request).first()
            if not committed_price:
                committed_price = CommittedPrice()
                committed_price.booking_request = booking_request
            committed_price.datewise_pricing = json.dumps(date_and_room_wise_pricing)
            committed_price.save()

    def convert_rate_plan_to_string(self, rate_plan):
        date_wise_price = {}
        date_wise_price['sell_price'] = float(rate_plan.sell_price)
        date_wise_price['base_price'] = float(rate_plan.base_price)
        date_wise_price['pre_tax_price'] = float(rate_plan.pretax_price)
        return date_wise_price

    def get_discount_percent(
        self,
        coupon_code,
        booking_request,
        current_user,
        referral_click_id=None):
        logged_user_id = str(
            current_user.id) if current_user and current_user.is_authenticated() else ""
        try:
            coupon, discount, discount_error = DiscountCouponServices.validate_coupon_and_get_discount(
                booking_request, coupon_code, referral_click_id, logged_user_id, current_user)

            if discount_error:
                slack_alert.send_slack_alert_for_invalid_discount_failure(status=400,dev_msg="get_discount_percent for coupon_code %s"%(coupon_code), message=discount_error,
                                                            class_name=self.__class__.__name__)
                raise InvalidDiscountCouponException(discount_error)
        except InvalidCouponException as e:
            slack_alert.send_slack_alert_for_invalid_discount_failure(status=400, dev_msg="get_discount_percent for coupon_code %s"%(coupon_code),
                                                        message=str(e),
                                                        class_name=self.__class__.__name__)
            raise InvalidDiscountCouponException(e)

        discount_percent = (float(
            discount) / float(booking_request.pretax_amount - booking_request.discount_value - booking_request.member_discount)) * 100
        return coupon, discount_percent

    def __get_price_dto(self, booking_request, room_configs, logged_in_user, coupon_code, discount_percent, coupon_type,
                        utm_source, apply_wallet):
        price_request_dto = PriceRequestDto(
            booking_request.checkin_date,
            booking_request.checkout_date,
            [
                booking_request.hotel_id],
            room_configs,
            room_type=booking_request.room_type,
            rate_plan=booking_request.rate_plan,
            coupon_code=coupon_code,
            coupon_value=discount_percent,
            coupon_type=coupon_type,
            channel=booking_request.booking_channel,
            logged_in_user=logged_in_user,
            utm_source=utm_source,
            wallet_applied=apply_wallet)
        return price_request_dto

    def check_coupon_code_applicable(self, coupon_code, hotel_id,start_date,end_date):
        exclude_coupon_code = None
        coupons=self.autoapply_service.get_active_coupons_basis_hotel_and_date_range(hotel_id,
                                                                             exclude_coupon_code,
                                                                             start_date,
                                                                             end_date)
        if coupon_code in coupons:
            return True
        return False