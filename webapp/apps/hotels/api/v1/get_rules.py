from base.views.api import TreeboAPIView

__author__ = 'Rajdeep'


class HotelRules(TreeboAPIView):
    @staticmethod
    def getHotelRules():
        response = {}

        result = [{"key": "Check-in time", "value": "12:00 PM"},
                  {"key": "Check-out time", "value": "11:00 AM"}]
        response['rules'] = result

        return response
