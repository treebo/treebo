import json
import logging

from datetime import datetime

from apps.common import date_time_utils
from services.restclient.contentservicerestclient import ContentServiceClient


class SpecialDetailsService:
    """
    Non sense class added to get rid of circular dependencies.
    """
    logger = logging.getLogger(__name__)

    def __init__(self):
        pass

    def get_special_date_details(self, booking_request):
        """
        Checks if the stay dates is in the special dates for which an extra charge is applied by hotel
        :param (BookingRequest) booking_request:
        :return:
        """
        special_rates = []
        content_store_value = ContentServiceClient.getValueForKey(
            None, "special_rate_hotels", 1)

        if content_store_value is not None:
            special_rate_data = dict(content_store_value)
            hotel_special_rate = special_rate_data.get(
                str(booking_request.hotel_id))
            if hotel_special_rate is not None:
                for config in hotel_special_rate.get("special_rate_dates"):
                    try:
                        if date_time_utils.is_range_overlapping(
                            datetime.strptime(
                                config['start_date'],
                                '%d/%m/%Y').date(),
                            datetime.strptime(
                                config['end_date'],
                                '%d/%m/%Y').date(),
                                booking_request.checkin_date,
                                booking_request.checkout_date):
                            special_rates.append(config["text"])
                    except ValueError:
                        self.logger.exception(
                            "Invalid date format entered in content store")

        return special_rates
