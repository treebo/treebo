# -*- coding: utf-8 -*-


from django.conf.urls import url
from apps.featuregate.api.v1.feature_manager import FeatureManagerAPI

urlpatterns = [
    url(r'^$', FeatureManagerAPI.as_view(), name='feature-manager'),
]
