# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0023_auto_20160706_0407'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserReferralReward', fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True, primary_key=True)), ('created_at', models.DateTimeField(
                        auto_now_add=True, verbose_name='Created at')), ('modified_at', models.DateTimeField(
                            auto_now=True, verbose_name='Modified at')), ('amount', models.DecimalField(
                                default=0, max_digits=3, decimal_places=2)), ('reward_date', models.DateField(
                                    auto_now=True)), ('coupon_code', models.CharField(
                                        max_length=50, blank=True)), ('type', models.CharField(
                                            default='', max_length=25, choices=[
                                                ('Signup', 'Signup'), ('Transaction', 'Transaction'), ('Install', 'Install')])), ], options={
                'abstract': False, }, ), migrations.AlterField(
            model_name='user', name='uuid', field=models.UUIDField(
                default=uuid.uuid4, max_length=32, null=True, blank=True), ), migrations.AddField(
            model_name='userreferralreward', name='user', field=models.OneToOneField(
                to=settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING), ), ]
