# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discounts', '0014_auto_20170417_1413'),
    ]

    operations = [
        migrations.AddField(
            model_name='discountcoupon',
            name='is_prepaid',
            field=models.BooleanField(default=False),
        ),
    ]
