# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0031_auto_20160927_0551'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='booking_status',
            field=models.CharField(
                default='Tentative',
                max_length=50,
                null=True,
                blank=True,
                choices=[
                    ('Tentative',
                     'Tentative'),
                    ('Awaiting HX Confirmation',
                     'Awaiting HX Confirmation'),
                    ('Confirm',
                     'Confirm'),
                    ('Cancel',
                     'Cancel'),
                    ('Cancellation Pending',
                     'Cancellation Pending'),
                    ('Failure',
                     'Failure'),
                    ('Temp Booking Created',
                     'Temp Booking Created')]),
        ),
    ]
