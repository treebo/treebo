import logging
from apps.featuregate import constants

from common.exceptions.treebo_exception import TreeboValidationException

__author__ = 'amithsoman'
logger = logging.getLogger(__name__)

"""
Here are the symbolic tokens that we'll process:
Please add additional tokens as and when required along with
appropriate behavior in the parse_expression/parse_term functions.
"""
AND = 'AND'
OR = 'OR'
LPAREN = '('
RPAREN = ')'
EOF = 'EOF'


def main():
    """Some test code."""


# test of parsing: "5*4+300/(5-2)*(6+4)+4"
# logger.info ExpressionParser([
# 	1, AND, 2, AND, 3, AND,
# 	LPAREN, 4, OR, 5, RPAREN,
# 	AND, LPAREN, 6, OR, 7, RPAREN,
# 	AND, 8, EOF]).parse_expression()
# logger.info ExpressionParser([
# 	1, AND, 2, AND, 3, AND, 4, EOF
# ]).parse_expression()
# logger.info ExpressionParser([
# 	LPAREN, 1, AND, 2, RPAREN, AND, LPAREN, 3, AND,
# 	4, RPAREN, EOF]).parse_expression()

class ExpressionParser:
    """Parses a token list and returns a nested expression structure in
    prefix form."""

    def __init__(self, tokens, constraint_validator=None):
        self.__tokens = tokens
        self.__constraint_validator = constraint_validator
        self.__op_stk = []
        self.__values_stk = []

    # self.__constraintValidator = ConstraintValidator('TREEBO20','2015-10-16','2015-10-15','2000',7,None,datetime.now().date())
    def fetch_all_operands(self):
        operands = [int(token) for token in self.__tokens if self.is_int(str(token))]
        return operands
    def peek_token(self):
        """Looks at the next token in our token stream."""
        return self.__tokens[0]

    def peek_op(self):
        if len(self.__op_stk) > 0:
            return self.__op_stk[-1]
        return None

    def peek_value(self):
        if len(self.__values_stk) > 0:
            return self.__values_stk[-1]
        return None

    def consume_token(self):
        """Pops off the next token in our token stream."""
        next_token = self.__tokens[0]
        del self.__tokens[0]
        return next_token

    def parse_expression(self):
        """Returns a parsed expression.  An expression may consist of a
        bunch of chained factors."""
        self.__op_stk = []
        self.__values_stk = []
        for token in self.__tokens:
            token = str(token)
            if self.is_int(token):
                token = int(token)
                self.__values_stk.append(token)
            elif token == LPAREN:
                self.__op_stk.append(token)
            elif token == RPAREN:
                while self.peek_op() != LPAREN:
                    self.__values_stk.append(
                        self.evaluate_op(
                            self.__op_stk.pop(),
                            self.__values_stk.pop(),
                            self.__values_stk.pop()))
                self.__op_stk.pop()
            elif token == AND or token == OR:
                while len(
                        self.__op_stk) > 0 and self.has_precedence(
                        token,
                        self.peek_op()):
                    self.__values_stk.append(
                        self.evaluate_op(
                            self.__op_stk.pop(),
                            self.__values_stk.pop(),
                            self.__values_stk.pop()))
                self.__op_stk.append(token)
            elif token == EOF:
                pass
            else:
                raise Exception(" Invalid Token %s ", token)

        # for example  :   1 AND ( 1 AND 2 AND 3 )
        while len(self.__op_stk) > 0:
            self.__values_stk.append(
                self.evaluate_op(
                    self.__op_stk.pop(),
                    self.__values_stk.pop(),
                    self.__values_stk.pop()))

        result = self.peek_value()
        result = self.run_constraint(result)

        if not isinstance(result, bool):
            logger.error(" bool is required but got this %s ", result)
            raise TypeError

        self.__constraint_validator.message = constants.FAILURE
        if result:
            self.__constraint_validator.message = constants.SUCCESS
        return result

    def evaluate_op(self, op, value_one, value_two):
        result = False
        if op == AND:
            result = self.run_constraint(
                value_one) and self.run_constraint(value_two)
        elif op == OR:
            result = self.run_constraint(
                value_one) or self.run_constraint(value_two)
        else:
            raise Exception(" invalid operator " + op)
        return result

    def run_constraint(self, value):
        if isinstance(value, bool):
            return value

        self.__constraint_validator.validatorFactory(value)
        if self.__constraint_validator.message == constants.SUCCESS:
            return True
        return False

    def has_precedence(self, op1, op2):
        """ return true if op2 has higher or same percedence than op1 """
        if op2 == AND and op1 == AND:
            return True
        if op2 == AND and op1 == OR:
            return True
        return False

    def is_int(self, token):
        value = False
        try:
            int(token)
            value = True
        except ValueError:
            pass
        return value

# if __name__ == "__main__":
#     # token_str = "True AND FALSE OR TRUE"
#     # tokens = token_str.split()
#     tokens = [False, "AND", "(", False, "OR", True, ")"]
#     exp = ExpressionParser(tokens)
#     exp.parse_expression()
