SEARCH_PARAMS_PRIORITY_MAP = {
    'hotel_id': 7,
    'hotel_name': 6,
    'landmark_name': 5,
    'locality_name': 4,
    'city_name': 3,
    'state_name': 2,
    'country_name': 1,
}
LOCATION_TYPE_SEARCH_PARAMS_MAP = {
    'hotel_id': 'hotel',
    'hotel_name': 'hotel',
    'locality_name': 'locality',
    'landmark_name': 'landmark',
    'city_name': 'city',
    'state_name': 'state',
    'country_name': 'country',
}

BRANDS = [{
    "code": "trend",
    "color": "000000",
    "display_name": "Trend",
    "legal_name": "Trend",
    "logo": "https://cs-images.treebo.com/brand/trend_depth_whitebg_horizontal.svg",
    "name": "Trend",
    "short_description": "Trend",
    "status": "ACTIVE"
},
    {
        "code": "tryst",
        "color": "000000",
        "display_name": "Tryst",
        "legal_name": "Tryst",
        "logo": "https://cs-images.treebo.com/brand/tryst_depth_whitebg_horizontal.svg",
        "name": "Tryst",
        "short_description": "Tryst",
        "status": "ACTIVE"
    }
]

INDEPENDENT_HOTEL_BRAND_CODE = "independent"
INDEPENDENT_HOTELS_SEARCH_DISTANCE_CAP = "unbranded_hotels_search_distance"
INDEPENDENT_HOTELS_DEFAULT_DISTANCE_CAP = 3

DEFAULT_SEARCH_CHANNEL = 'website'
APP_CHANNELS = ['ios', 'android']
SORT_BY_PRICING = "priceLToH"

TESTING_TEXT = ['test', 'testing']
AUTOCOMPLETE_RESULTS_MAX_COUNT = 3
SEARCH_BASE_ROOM_PRICE = 1800

DIRECT = 'direct'
GDC = 'gdc'

SEARCH_TYPE_GEO = 'geo'
