# -*- coding: utf-8 -*-
import logging

from django.contrib import messages
from django import forms
from django.conf.urls import url
from django.contrib import admin
from django.contrib.admin.options import TabularInline
from django.contrib.auth.admin import GroupAdmin as BaseGroupAdmin, UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Count
from django.template import loader

from apps.hotels.views.hotel_image_upload import HotelImageUpload
from apps.hotels.views.hotel_upload import HotelUploadWizard
from dbcommon.constants import ( MAX_NO_OF_IMAGES_ACROSS_CATEGORIES,
                                MAX_NO_OF_CATEGORIES_ALLOWED,
                                MIN_NO_OF_CATEGORIES_ALLOWED,
                                MAX_NO_OF_IMAGES_PER_CATEGORY,
                                MIN_NO_OF_IMAGES_PER_CATEGORY)
from dbcommon.models.direction import Directions
from dbcommon.models.discounts import Coupon_Constraint, Coupon_Date_Constraint, Coupon
from dbcommon.models.facilities import Amenity, AmenityMemberShip, Facility, FacilityCategory, \
    CityAmenityMapforAutoPageCreation, AmenitySeoDetails
from dbcommon.models.hotel import Highlight, Hotel, SeoDetails, CitySeoDetails, HotelImage, \
    Categories, \
    CategorySeoDetails, EnableCategory, HotelMetaData, Policy, HotelPolicyMap, HotelAlias
from dbcommon.models.location import City, CityAlias, Locality, CityBlog, Landmark, State, \
    NotifyNewLocation, \
    LandmarkType, LocalitySeoDetails
from dbcommon.models.miscellanous import Image, Feedback, NotificationArchive, WifiLocationMap, \
    WifiCommonConfig, CityCategoryMapforAutoPageCreation, ImagesPerCategory
from dbcommon.models.partner import Corporate, Partner, PartnerInfo
from dbcommon.models.profile import UserOtp, UserMetaData, User, UserEmail, EmailLinkHashValue, \
    UserDiscount, UserReferral, Otp
from dbcommon.models.room import Room
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.options import flatten_fieldsets
from django.contrib.admin.templatetags import admin_modify
from dbcommon.models.error_codes import ErrorCodes
from dbcommon.models.landmark import SearchLandmark, LandmarkSeoDetails
from dbcommon.models.hotel import CSConsumerDetails, CSHotelBrands, HotelBrandMapping
from dbcommon.models.feature_toggle import FeatureToggle
from dbcommon.models.hotel import PolicyConfiguration

IMAGES_ZIP = '/images.zip'
XLSX = '/excel.xlsx'

log = logging.getLogger(__name__)

admin.site.unregister(Group)


@admin_modify.register.inclusion_tag(
    'admin/submit_line.html',
    takes_context=True)
def submit_row(context):
    ctx = admin_modify.submit_row(context)
    ctx.update(
        {
            'show_save_and_add_another': context.get(
                'show_save_and_add_another',
                ctx['show_save_and_add_another']),
            'show_save_and_continue': context.get(
                'show_save_and_continue',
                ctx['show_save_and_continue']),
            'show_save': context.get(
                'show_save',
                ctx['show_save'])})
    return ctx


class ReadOnlyAdminMixin(admin.ModelAdmin):
    def has_change_permission(self, request, obj=None):
        status = self.validate_user_permission(request, 'read')
        return status

    def get_readonly_fields(self, request, obj=None):
        if obj:
            if not self.validate_user_permission(request, 'update'):
                if self.get_fieldsets(request, obj):
                    return flatten_fieldsets(self.get_fieldsets(request, obj))
                else:
                    return list(set(
                        [field.name for field in self.opts.local_fields] +
                        [field.name for field in self.opts.local_many_to_many]
                    ))
            return list()
        # this else is to have readonly fields even if the user has change permissions
        # will allow for more flexible control over fields that are to be
        # access protected
        else:
            return self.readonly_fields

    def has_add_permission(self, request):
        status = False
        status = self.validate_user_permission(request, 'add')
        return status

    def has_delete_permission(self, request, obj=None):
        status = False
        status = self.validate_user_permission(request, 'delete')
        return status

    def get_actions(self, request):
        # queryset.delete() still can delete - hence disabled by overriding
        # actions
        actions = super(ReadOnlyAdminMixin, self).get_actions(request)

        if not self.validate_user_permission(request, 'delete'):
            if 'delete_selected' in actions:
                del actions['delete_selected']

        return actions

    def validate_user_permission(self, request, validationType):
        contentType = ContentType.objects.get_for_model(self.model)
        status = False
        if request.user.is_superuser:
            status = True
        else:
            if validationType == 'read':
                if request.user.has_perm(
                    '%s.read_%s' %
                    (contentType.app_label, contentType.model)):
                    status = True
                elif request.user.has_perm(
                    '%s.change_%s' % (contentType.app_label, contentType.model)):
                    status = True
                elif request.user.has_perm(
                    '%s.add_%s' % (contentType.app_label, contentType.model)):
                    status = True
                elif request.user.has_perm(
                    '%s.delete_%s' % (contentType.app_label, contentType.model)):
                    status = True

            if validationType == 'update':
                if request.user.has_perm(
                    '%s.change_%s' %
                    (contentType.app_label, contentType.model)):
                    status = True
                elif request.user.has_perm(
                    '%s.add_%s' % (contentType.app_label, contentType.model)):
                    status = True
                elif request.user.has_perm(
                    '%s.delete_%s' % (contentType.app_label, contentType.model)):
                    status = True

            if validationType == 'add':
                if request.user.has_perm(
                    '%s.add_%s' %
                    (contentType.app_label, contentType.model)):
                    status = True

            if validationType == 'delete':
                if request.user.has_perm(
                    '%s.delete_%s' %
                    (contentType.app_label, contentType.model)):
                    status = True
        return status

    # we set the context here and use it in submit_row method above since we
    # cannot get user permissions there
    def change_view(self, request, object_id, form_url='', extra_context=None):
        if not self.validate_user_permission(request, 'update'):
            extra_context = extra_context or {}
            extra_context['show_save_and_add_another'] = False
            extra_context['show_save_and_continue'] = False
            extra_context['show_save'] = False

        return super(
            ReadOnlyAdminMixin,
            self).change_view(
            request,
            object_id,
            form_url='',
            extra_context=extra_context)


@admin.register(Group)
class GroupAdmin(ReadOnlyAdminMixin, BaseGroupAdmin):
    list_display = ('id', 'name', 'permissions_list')

    def transform_permission(self, name):
        if name.startswith('Can '):
            return name[4:].capitalize()
        else:
            return name

    def permissions_list(self, obj):
        permissions = [
            self.transform_permission(
                x.name) for x in obj.permissions.all()]
        return ', '.join(permissions)


@admin.register(Image)
class ImageAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    formfield_overrides = {
        models.FileField: {'widget': forms.TextInput},
    }
    readonly_fields = ('is_showcased',)
    list_display = (
        'id',
        'thumb',
        'get_hotel_name',
        'get_room_name',
        'is_showcased',
        'position',
        'category',
        'image_name',
        'image_callout')
    list_editable = ['position', 'category', 'image_name',
                     'image_callout']
    list_filter = ('hotel', 'is_showcased', 'category')
    search_field = ('hotel__name',)
    ordering = ('is_showcased', 'position', 'category')

class ImagesAcrossCategory(TabularInline):
    model = ImagesPerCategory
    extra = 0
    max_num = MAX_NO_OF_CATEGORIES_ALLOWED
    min_num = MIN_NO_OF_CATEGORIES_ALLOWED
    fields = ('hotel', 'category', 'no_of_images', 'ranking')
    verbose_name = "Image Per Category"
    verbose_name_plural = "Images Per Category"

@admin.register(User)
class UserAdmin(ReadOnlyAdminMixin, BaseUserAdmin):
    list_display = (
        'id',
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'is_superuser',
        'is_staff',
        'is_active',
        'is_guest')
    search_fields = ('email',)
    ordering = ('email',)
    list_filter = ('is_superuser', 'is_staff', 'is_active', 'is_guest')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (
            'Personal Info',
            {'fields': ('first_name', 'last_name', 'phone_number', 'city', 'gender')}),
        ('Permissions and Groups', {'fields': ('is_staff', 'is_active', 'is_superuser',
                                               'groups')})
    )
    add_fieldsets = (
        (None, {'fields': ('email', 'password1', 'password2')}),
        (
            'Personal Info',
            {'fields': ('first_name', 'last_name', 'phone_number', 'city', 'gender')}),
        ('Permissions and Groups', {'fields': ('is_staff', 'is_active', 'is_superuser',
                                               'groups')})
    )

    def save_model(self, request, obj, form, change):
        if not change:
            context = {
                'user': obj,
                'password': form.cleaned_data['password1'],
                'admin_url': request.build_absolute_uri(reverse('admin:index'))
            }
            message = loader.render_to_string(
                "admin/registration/welcome_email.html", context)
            subject = loader.render_to_string(
                "admin/registration/welcome_email_subject.html", context).strip()
            obj.email_user(subject, message)
            obj.is_staff = True
        super(UserAdmin, self).save_model(request, obj, form, change)


@admin.register(UserReferral)
class UserReferralAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id', 'user', 'userkey', 'shareurl', 'referral_code', 'has_referrer',
        'friend_referral_code')
    search_field = ('friend_referral_code',)


@admin.register(UserMetaData)
class UserMetadataAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'get_user_id', 'key',)
    list_filter = ('key',)

    def get_user_id(self, obj):
        link = reverse('admin:dbcommon_user_changelist') + \
               '?id__exact=' + str(obj.user_id.id)
        return '<a href="{0}" target="_blank">{1}</a>'.format(
            link, obj.user_id)

    get_user_id.short_description = 'User'
    get_user_id.allow_tags = True


@admin.register(UserEmail)
class UserEmailAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'user', 'email', 'email_type', 'is_validate')
    search_fields = ('email',)


@admin.register(UserDiscount)
class UserDiscountAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'user', 'coupon_code', 'status')


@admin.register(City)
class CityAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'slug',
        'tagline',
        'city_longitude',
        'city_latitude',
        'status',
        'enable_for_seo')
    readonly_fields = ('name', 'slug', 'status')


@admin.register(CityAlias)
class CityAliasAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'city', 'alias')


@admin.register(ErrorCodes)
class ErrorCodesAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'code', 'message')


@admin.register(Partner)
class PartnerAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    pass


@admin.register(Amenity)
class AmenityAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'type')


@admin.register(AmenityMemberShip)
class AmenityMembershipAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'room', 'hotel', 'amenity')


@admin.register(Locality)
class LocalityAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'city',
        'pincode',
        'latitude',
        'longitude',
        'enable_for_seo')
    search_fields = ('name',)


@admin.register(Corporate)
class CorporateAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    pass


@admin.register(Room)
class RoomAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'hotel',
        'room_type',
        'get_mm_hotel_code',
        'get_mm_room_code',
        'price',
        'quantity',
        'available',
        'max_guest_allowed',
        'base_rate',
        'max_adult_with_children',
        'max_children')
    search_fields = ('hotel__name',)
    list_filter = ('room_type',)
    list_editable = (
        'max_guest_allowed',
        'max_adult_with_children',
        'max_children')


@admin.register(Hotel)
class HotelAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'hotelogix_id',
        'hotelogix_name',
        'city',
        'locality',
        'status',
        'get_total_booking',
        'get_hotel_room_mappers',
        'property_type',
        'provider_name')
    list_filter = ('status', 'city')
    search_fields = ('name', 'property_type', 'provider_name')
    add_readonly_fields = ('uuid', 'cs_id')
    update_readonly_fields = (
        'name',
        'hotelogix_id',
        'hotelogix_name',
        'city',
        'locality',
        'checkin_time',
        'checkout_time',
        'latitude',
        'longitude',
        'uuid',
        'price_increment',
        'cs_id')

    def get_queryset(self, request):
        qs = self.model.all_hotel_objects.get_queryset()

        # we need this from the superclass method
        # otherwise we might try to *None, which is bad ;)
        ordering = self.ordering or ()
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

    def get_total_booking(self, obj):
        from apps.bookings.models import Booking
        link = reverse('admin:bookings_booking_changelist') + \
               '?hotel_id__exact=' + str(obj.id)
        return '<a href="{0}" target="_blank">{1}</a>'.format(
            link, Booking.objects.filter(hotel=obj, is_complete=True).count())

    get_total_booking.allow_tags = True
    get_total_booking.short_description = 'Total Completed Bookings'

    def get_hotel_room_mappers(self, obj):
        from apps.bookingstash.models import HotelRoomMapper
        from dbcommon.models.room import Room
        rooms = Room.objects.filter(hotel=obj)
        mappers = HotelRoomMapper.objects.filter(room_id__in=rooms)
        mapper = mappers.first()
        if mapper:
            link = reverse(
                'admin:bookingstash_hotelroommapper_changelist') + '?q=' + mapper.mm_hotel_code
            return '<a href="{0}" target="_blank">{1}</a>'.format(
                link, 'Link to Mappers')
        return ''

    get_hotel_room_mappers.allow_tags = True
    get_hotel_room_mappers.short_description = 'Hotel Room Mappers'

    def get_urls(self):
        urls = super(HotelAdmin, self).get_urls()
        my_urls = [url(r'^hotel_live/$',
                       HotelUploadWizard.as_view(),
                       name='hotel_upload'),
                   url(r'^upload_hotel_image/$',
                       HotelImageUpload.as_view(),
                       name='hotel_upload_image'),
                   ]
        return my_urls + urls

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return self.add_readonly_fields
        return self.update_readonly_fields


class HotelImageInline(TabularInline):
    model = Image
    extra = 0
    readonly_fields = ('thumb', 'get_room_name', 'is_showcased_image')
    ordering = ("-is_showcased","category", "position")
    fields = ('thumb', 'is_showcased_image', 'tagline', 'position',
              'category', 'get_room_name',
              'image_name', 'image_callout')

@admin.register(HotelImage)
class HotelImageAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'hotelogix_id',
        'hotelogix_name',
        'city',
        'locality',
        'status')
    search_fields = ('name',)
    readonly_fields = ('name',)
    exclude = (
        'tagline',
        'hotelogix_id',
        'hotelogix_name',
        'city',
        'locality',
        'checkin_time',
        'checkout_time',
        'latitude',
        'longitude',
        'state',
        'uuid',
        'status',
        'price_increment',
        'description',
        'phone_number',
        'room_count',
        'street',
        'landmark',
        'category',
        'consumer_secret',
        'consumer_key',
        'name')
    inlines = [ImagesAcrossCategory, HotelImageInline]
    
    def reset_showcased_images(self, request, hotel_instance, formset):
        if formset.model not in (ImagesPerCategory, Image):
            return
        
        status = hotel_instance.reset_is_showcased_images()
        if status:
            return
        
        if formset.model == ImagesPerCategory:
            messages.error(request,
                            "Need {} image to category association.".format(
                                MAX_NO_OF_IMAGES_ACROSS_CATEGORIES))
        elif formset.model == Image:
            # show no of images per category when showcased images failed
            category_counts = Image.objects.values('category').filter(
                hotel=hotel_instance).order_by(
                    'category').annotate(total=Count('category'))
            category_counts_str = []
            for category_count in category_counts:
                category_counts_str.append(
                    "{} has {} images".format(category_count["category"],
                                              category_count["total"])
                )
            if category_counts_str:
                messages.error(request,
                                ", ".join(category_counts_str))
    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        hotel_instance = form.instance
        to_delete = None


        if formset.model == ImagesPerCategory:
            to_delete = [obj for obj in formset.deleted_objects if isinstance(obj, ImagesPerCategory)]

            # build mapping of current categories
            existing_mapping = {}

            for img_category in ImagesPerCategory.objects.filter(hotel=hotel_instance):
                if img_category not in to_delete:
                    existing_mapping[img_category.pk] = img_category

            # check no_of_images should be in the
            # range of MIN_NO_OF_IMAGES_PER_CATEGORY to MAX_NO_OF_IMAGES_PER_CATEGORY
            for instance in instances:
                if instance.no_of_images < MIN_NO_OF_IMAGES_PER_CATEGORY:
                    messages.error(request,
                            "No of images cannot be less than {}.".format(
                                MIN_NO_OF_IMAGES_PER_CATEGORY))
                    return
                if instance.no_of_images > MAX_NO_OF_IMAGES_PER_CATEGORY:
                    messages.error(request,
                            "No of images cannot be greater than {}.".format(
                                MAX_NO_OF_IMAGES_PER_CATEGORY))
                    return

            # find total of no_of_images across categories
            # if existing_mapping doesn't exist then user
            # is adding mapping for first time else they
            # are updating.
            # note instances only contains updates
            if not existing_mapping:
                total = sum([instance.no_of_images for instance in instances])
                # build list of category names
                category_list = [instance.category for instance in instances]
            elif existing_mapping:
                # swap instance object so updated no_of_images is picked up
                for instance in instances:
                    existing_mapping[instance.pk] = instance
                total = sum([instance.no_of_images
                             for key, instance in existing_mapping.items()])

                # build list of category names
                category_list = [instance.category
                                for key, instance in existing_mapping.items()]

            # check same category is not repeated twice
            if len(set(category_list)) != len(category_list):
                messages.error(request,
                               "Cannot repeat image category.")
                return

            # total no_of_images across categories should be
            # equal to MAX_NO_OF_IMAGES_ACROSS_CATEGORIES
            if total != MAX_NO_OF_IMAGES_ACROSS_CATEGORIES:
                messages.error(request,
                               "Total of all category needs to be {}".format(
                                   MAX_NO_OF_IMAGES_ACROSS_CATEGORIES))
                return

        # save instances as check for
        # MAX_NO_OF_IMAGES_ACROSS_CATEGORIES images is successful
        for instance in instances:
            instance.save()
        formset.save_m2m()

        # delete objects marked as deleted
        for obj in formset.deleted_objects:
            obj.delete()

        self.reset_showcased_images(request, hotel_instance, formset)

    def has_add_permission(self, request):
        return False


@admin.register(Highlight)
class HighlightAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'description', 'hotel')
    search_fields = ('hotel__name', 'description')


@admin.register(SeoDetails)
class SeoDetailsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('hotel', 'titleDescription',)


@admin.register(CitySeoDetails)
class CitySeoDetails(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('city', 'citySeoTitleContent')


@admin.register(FacilityCategory)
class FacilityCategoryAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'created_at', 'modified_at', 'name', 'status')
    list_filter = ('name', 'status',)
    search_fields = ('name', 'status')
    actions = ['delete_selected']

    def has_add_permission(self, request):
        return True


# class FacilityAdminForm(ModelForm):
#     hotels = forms.ModelMultipleChoiceField(queryset=Hotel.objects.order_by('name'), required=False)
#     rooms = forms.ModelMultipleChoiceField(queryset=Room.objects.order_by('hotel__name'), required=False)
#     to_be_shown = forms.BooleanField(required=False)
#
#     class Meta:
#         model = Facility
#         fields = ('name', 'catalog_mapped_name', 'categories', 'url', 'hotels', 'rooms', 'to_be_shown')


@admin.register(Facility)
class FacilityAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'catalog_mapped_name',
        'url',
        'get_category',
        'to_be_shown',
        'css_class')
    list_filter = (
        'categories__name',
        'catalog_mapped_name',
        'url',
        'to_be_shown')
    search_fields = (
        'categories__name',
        'catalog_mapped_name',
        'name',
        'url',
        'hotels__name',
        'hotels__id',
        'css_class')
    actions = ['delete_selected']

    # form = FacilityAdminForm

    def has_add_permission(self, request):
        return True

    def get_category(self, obj):
        return [p.name for p in obj.categories.all()]


@admin.register(Feedback)
class FeedbackAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'sender', 'device', 'message', 'page_url')
    list_filter = ('device', 'sender',)
    search_fields = ('message', 'page_url')


@admin.register(Directions)
class DirectionsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'hotel', 'landmark_obj', 'distance', 'mode')
    search_fields = ('hotel__name',)
    list_filter = ('mode',)


@admin.register(PartnerInfo)
class PartnerInfoAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    pass


@admin.register(CityBlog)
class CityBlogAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    pass


@admin.register(Landmark)
class LandmarkAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'type', 'name', 'city', 'status')
    list_filter = ('city',)
    search_fields = ('name',)


@admin.register(LandmarkType)
class LandmarkTypeAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'name')


@admin.register(NotifyNewLocation)
class NotifyNewLocationAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'location', 'email', 'query')


@admin.register(State)
class StateAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    from apps.pricing.admin import StateTaxInline
    list_display = ('id', 'name', 'service_tax', 'luxury_tax_on_base')
    inlines = [StateTaxInline]


@admin.register(Coupon)
class CouponAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'code',
        'discount_operation',
        'discount_value',
        'discount_application',
        'coupon_type',
        'expiry',
        'is_active')


@admin.register(Coupon_Constraint)
class CouponConstraintAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'coupon', 'type', 'value')


@admin.register(Coupon_Date_Constraint)
class CouponDateConstraintAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'coupon', 'start_date', 'end_date')


@admin.register(UserOtp)
class UserOtp(ReadOnlyAdminMixin, admin.ModelAdmin):
    pass


@admin.register(Otp)
class Otp(ReadOnlyAdminMixin, admin.ModelAdmin):
    pass


@admin.register(EmailLinkHashValue)
class EmailLinkHashValue(ReadOnlyAdminMixin, admin.ModelAdmin):
    pass


# admin.site.register(Booking)
# admin.site.register(Reservation)
@admin.register(NotificationArchive)
class NotificationArchive(ReadOnlyAdminMixin, admin.ModelAdmin):
    pass


# admin.site.register(RoomBooking)
# admin.site.register(DiscountCoupon)
# admin.site.register(DiscountCouponConstraints)
# admin.site.register(ConstraintExpression)
# admin.site.register(ConstraintKeywords)
# admin.site.register(ConstraintOperators)

@admin.register(SearchLandmark)
class SearchLandmarkAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'seo_url_name',
        'search_label',
        'status',
        'latitude',
        'longitude',
        'get_city_name',
        'enable_for_seo')
    search_fields = (
        'seo_url_name',
        'search_label',
        'status',
        'latitude',
        'longitude',
        'city__name',
        'enable_for_seo')
    list_filter = (
        'seo_url_name',
        'status',
        'city__name',
        'search_label',
        'enable_for_seo')

    def get_city_name(self, obj):
        return obj.city.name


@admin.register(LandmarkSeoDetails)
class LandmarkSeoDetailsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'landmark_seo_title',
        'landmark_seo_description',
        'landmark_seo_keyword',
        'get_landmark_search_label',
        'get_landmark_city',
        'get_landmark_seo_name')
    search_fields = (
        'landmark_seo_title',
        'landmark_seo_description',
        'landmark_seo_keyword',
        'landmark__search_label',
        'landmark__city__name',
        'landmark__seo_url_name')
    list_filter = (
        'landmark_seo_title',
        'landmark_seo_description',
        'landmark_seo_keyword',
        'landmark__search_label',
        'landmark__city__name',
        'landmark__seo_url_name')

    def get_landmark_search_label(self, obj):
        return obj.landmark.search_label

    def get_landmark_city(self, obj):
        return obj.landmark.city.name

    def get_landmark_seo_name(self, obj):
        return obj.landmark.seo_url_name

    get_landmark_search_label.short_description = 'Landmark Search Label'
    get_landmark_city.short_description = 'Landmark City'
    get_landmark_seo_name.short_description = 'Landmark Seo Name'


@admin.register(Categories)
class CategoryAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'enable_for_seo', 'status')


@admin.register(LocalitySeoDetails)
class LocalitySeoDetailsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'locality_seo_title',
        'locality_seo_description',
        'locality_seo_keyword')
    search_fields = (
        'locality_seo_title',
        'locality_seo_description',
        'locality_seo_keyword')
    list_filter = (
        'locality_seo_title',
        'locality_seo_description',
        'locality_seo_keyword')


@admin.register(CategorySeoDetails)
class CategorySeoDetailsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'category_seo_title',
        'category_seo_description',
        'category_seo_keyword')
    search_fields = (
        'category_seo_title',
        'category_seo_description',
        'category_seo_keyword')
    list_filter = (
        'category_seo_title',
        'category_seo_description',
        'category_seo_keyword')


@admin.register(WifiLocationMap)
class WifiLocationMapAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'created_at',
        'hotel',
        'location_key',
        'wifi_name',
        'is_enabled')
    search_fields = ('hotel', 'location_key', 'wifi_name')
    list_filter = ('is_enabled', 'wifi_name')


@admin.register(WifiCommonConfig)
class WifiLocationMapAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'created_at', 'search_radius_in_meter')


@admin.register(EnableCategory)
class EnableCategoryAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'city', 'get_hotel_types')
    search_fields = ('id', 'city')
    list_filter = ('id', 'city')

    def get_hotel_types(self, obj):
        return ",".join([p.name for p in obj.hotel_type.all()])


@admin.register(HotelMetaData)
class HotelMetaDataAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'hotel', 'key', 'value')
    list_filter = ('hotel', 'key')


@admin.register(Policy)
class PolicyAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'policy_type',
        'title',
        'description',
        'display_in_need_to_know',
        'display_in_policy',
        'is_global')
    list_filter = ('policy_type', 'is_global')
    search_fields = ('policy_type',)


@admin.register(HotelPolicyMap)
class HotelPolicyMapAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'hotel_id', 'policy_id')

    list_filter = ('hotel_id', 'policy_id')
    search_fields = ('hotel_id', 'policy_id',)


@admin.register(CityAmenityMapforAutoPageCreation)
class CityAmenityMapforAutoPageCreationAdmin(
    ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'city', 'amenity', 'is_enabled')
    search_fields = ('id', 'city__name', 'amenity__name')
    list_filter = ('id', 'city', 'amenity')


@admin.register(CityCategoryMapforAutoPageCreation)
class CityCategoryMapforAutoPageCreationAdmin(
    ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'city', 'category', 'is_enabled')
    search_fields = ('id', 'city__name', 'category__name')
    list_filter = ('id', 'city', 'category')


@admin.register(AmenitySeoDetails)
class AmenitySeoDetailsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'amenity_seo_title',
        'amenity_seo_description',
        'amenity_seo_keyword')
    search_fields = (
        'amenity_seo_title',
        'amenity_seo_description',
        'amenity_seo_keyword')
    list_filter = (
        'amenity_seo_title',
        'amenity_seo_description',
        'amenity_seo_keyword')


@admin.register(CSConsumerDetails)
class CSConsumerDetailsAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'cs_id',
        'cs_data',
        'cs_route',
        'cs_queue',
        'cs_exchange',
        'is_consumed',
        'failure_message')
    search_fields = ('cs_id', 'cs_route', 'cs_exchange', 'is_consumed')
    list_filter = ('cs_id', 'cs_route', 'cs_exchange', 'is_consumed')


@admin.register(FeatureToggle)
class FeatureToggleAdmin(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'namespace', 'feature', 'environment', 'is_enabled')


@admin.register(PolicyConfiguration)
class PolicyConfiguration(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('id', 'provider', 'enable_global_policy')


@admin.register(CSHotelBrands)
class CSHotelBrands(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = (
        'id',
        'code',
        'color',
        'display_name',
        'legal_name',
        'name',
        'short_description',
        'logo',
        'status')
    search_fields = (
        'code',
        'color',
        'display_name',
        'legal_name',
        'name',
        'short_description',
        'logo',
        'status')
    list_editable = (
        'color',
        'display_name',
        'legal_name',
        'name',
        'short_description',
        'logo',
        'status')


@admin.register(HotelBrandMapping)
class HotelBrandMapping(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('hotel_id', 'cs_brand')
    search_fields = ('hotel_id', 'cs_brand', 'hotel_id_id')


@admin.register(HotelAlias)
class HotelAlias(ReadOnlyAdminMixin, admin.ModelAdmin):
    list_display = ('hotel', 'name', 'is_enabled',)
    search_fields = ('name', 'hotel__name',)
    list_editable = ('name', 'is_enabled',)

    def has_add_permission(self, request, obj=None):
        return False
