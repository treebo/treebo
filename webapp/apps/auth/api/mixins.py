from apps.auth.constants import APP_USER_AGENT

class DDosMixin(object):
    def is_otp_ddos(self, request, *args, **kwargs):
        if not request.META.get('HTTP_COOKIE', None):
            return True

        if request.META.get('HTTP_USER_AGENT', '') in APP_USER_AGENT:
            return False

        if request.META.get('HTTP_ABUSERID', None) != '':
            return True

        return False