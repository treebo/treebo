# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0004_room_base_rate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discountcoupon',
            name='discount_operation',
            field=models.CharField(
                default=b'PERCENTAGE',
                max_length=20,
                choices=[
                    (b'FIXED',
                     b'Fixed'),
                    (b'PERCENTAGE',
                     b'Percentage'),
                    (b'NIGHT',
                     b'Night'),
                    (b'FLATPERNIGHT',
                     b'Flatpernight')]),
        ),
    ]
