from apps.common.exceptions.custom_exception import NoHotelsFoundException
from apps.content.models import ContentStore
from apps.hotels.service.hotel_sub_brand_service import HotelSubBrandService
from apps.reviews.models import HotelTAOverallRatings
from apps.search.constants import SEARCH_TYPE_GEO, INDEPENDENT_HOTELS_SEARCH_DISTANCE_CAP, \
    INDEPENDENT_HOTELS_DEFAULT_DISTANCE_CAP, INDEPENDENT_HOTEL_BRAND_CODE
from apps.search.dto.hotel_serarch_dto import HotelSearchQuery
from apps.seo.services.common_services.sql_query_helper import SqlQueryHelper
from dbcommon.models.hotel import Hotel, HotelPolicyMap, HotelAlias
from dbcommon.models.landmark import SearchLandmark
from django.db.models.query_utils import Q
from django.conf import settings
from dbcommon.models.location import Locality, CityAlias
import logging

logger = logging.getLogger(__name__)


class HotelsRepository:

    def __init__(self):
        self.distance_hotel_tuple_list = {}
        self.is_distance_sort_rqd = False

    def _build_search_query(self, query, search_query: HotelSearchQuery):

        if search_query.location_type == SEARCH_TYPE_GEO and \
                search_query.latitude and search_query.longitude:
            hotel_ids = self._get_distance_sorted_hotels(lat=search_query.latitude,
                                                         long=search_query.longitude,
                                                         distance_cap=search_query.radius)
            query = query.filter(id__in=hotel_ids)

        elif search_query.hotel_id:
            hotel = self._get_hotel(hotel_id=search_query.hotel_id)
            if hotel:
                distance_cap = settings.SEARCH_DEFAULT_CITY_DISTANCE_CAP
                hotel_ids = self._get_distance_sorted_hotels(lat=hotel.latitude,
                                                             long=hotel.longitude,
                                                             distance_cap=distance_cap)
                query = query.filter(id__in=hotel_ids)
            else:
                logger.error("Hotel not found for search query {} and {}".format(
                    search_query.hotel_id, search_query.city_name))
                query = self._get_city_query(query=query, city=search_query.city_name)

        elif search_query.landmark_name and search_query.city_name:
            landmark = self._get_landmark(landmark=search_query.landmark_name,
                                          city=search_query.city_name)
            if landmark:
                hotel_ids = self._get_distance_sorted_hotels(lat=landmark.latitude,
                                                             long=landmark.longitude,
                                                             distance_cap=landmark.distance_cap)
                query = query.filter(id__in=hotel_ids)
            else:
                logger.error("Landmark not found for search query {} and {}".format(
                    search_query.landmark_name, search_query.city_name))
                query = self._get_city_query(query=query, city=search_query.city_name)

        elif search_query.locality_name and search_query.city_name:
            locality = self._get_locality(locality=search_query.locality_name,
                                          city=search_query.city_name)
            if locality:
                hotel_ids = self._get_distance_sorted_hotels(lat=locality.latitude,
                                                             long=locality.longitude,
                                                             distance_cap=locality.distance_cap)
                query = query.filter(id__in=hotel_ids)
            else:
                logger.error("Locality not found for search query {} and {}".format(
                    search_query.locality_name, search_query.city_name))
                query = self._get_city_query(query=query, city=search_query.city_name)

        elif search_query.city_name:
            query = self._get_city_query(query=query, city=search_query.city_name)

        elif search_query.state_name:
            query = query.filter(state__name__iexact=search_query.state_name)

        else:
            raise NoHotelsFoundException

        if search_query.amenity:
            query = query.filter(facility__name__iexact=search_query.amenity)

        if search_query.category:
            query = query.filter(category__name__iexact=search_query.category)

        return query

    @staticmethod
    def _get_hotel(hotel_id):
        hotel = Hotel.objects.filter(id=hotel_id, status=Hotel.ENABLED).first()
        return hotel

    @staticmethod
    def _get_locality(locality, city):
        locality = Locality.objects.filter(
                        Q(Q(city__name__iexact=city) | Q(city__slug__iexact=city)) &
                        Q(Q(name__iexact=locality))).first()
        return locality

    @staticmethod
    def _get_landmark(landmark, city):
        landmark = SearchLandmark.objects.filter(
                        Q(Q(city__name__iexact=city) | Q(city__slug__iexact=city)) &
                        Q(Q(free_text_url__iexact=landmark) | Q(seo_url_name__iexact=landmark))).first()

        return landmark

    def _get_city_query(self, query, city):
        query = query.filter(Q(city__name__iexact=city) |
                             Q(city__name__in=self._get_city_query_from_cityalias(city=city)))
        return query

    @staticmethod
    def _get_city_query_from_cityalias(city):
        query = CityAlias.objects.filter(alias__iexact=city).values('city')
        return query

    def _get_distance_sorted_hotels(self, lat, long, distance_cap):

        hotel_ids, distance_hotel_dict, self.is_distance_sort_rqd = [], {}, True
        if not distance_cap:
            distance_cap = settings.SEARCH_DEFAULT_SEO_DISTANCE_CAP

        distance_query_str = SqlQueryHelper.get_distance_query_for_param_str()
        distance_compare_str = distance_query_str % (lat, lat, long)
        sql_query = "select id, " + distance_compare_str + " as distance from hotels_hotel where " \
                    + distance_compare_str + " < " + str(distance_cap) + " and status = 1 " \
                    "order by " + distance_compare_str + " asc;"
        self.distance_hotel_tuple_list = SqlQueryHelper().execute_query_and_return_dict_with_distance(
            sql_query)

        hotel_ids = [hotel_id for hotel_id, distance in self.distance_hotel_tuple_list]

        return hotel_ids

    def _sort_hotels(self, hotels):

        sorted_hotels, unbranded_hotels, remaining_hotels, hotel_id_hotel_dict = [], [], [], {}
        for hotel in hotels:
            hotel_id_hotel_dict[hotel.id] = hotel

        unbranded_search_distance = ContentStore.objects.filter(
            name=INDEPENDENT_HOTELS_SEARCH_DISTANCE_CAP, version=1).first()
        distance_cap = unbranded_search_distance.value['distance'] if unbranded_search_distance \
            else INDEPENDENT_HOTELS_DEFAULT_DISTANCE_CAP

        unbranded_hotel_ids = HotelSubBrandService.get_hotels_ids_for_brand(
            brand_code=INDEPENDENT_HOTEL_BRAND_CODE)

        for hotel_id, distance in self.distance_hotel_tuple_list:
            if hotel_id not in hotel_id_hotel_dict:
                continue

            #setting distance in hotel object
            hotel_id_hotel_dict[hotel_id].distance = distance

            if distance <= distance_cap:
                if hotel_id not in unbranded_hotel_ids:
                    sorted_hotels.append(hotel_id_hotel_dict[hotel_id])
                else:
                    unbranded_hotels.append(hotel_id_hotel_dict[hotel_id])
            else:
                remaining_hotels.append(hotel_id_hotel_dict[hotel_id])

        sorted_hotels_list = sorted_hotels + unbranded_hotels + remaining_hotels

        return sorted_hotels_list

    def search(self, search_query):
        query = Hotel.enabled_hotel_objects.filter(status=Hotel.ENABLED)
        query = self._build_search_query(query, search_query)
        hotels = query.all()

        if self.is_distance_sort_rqd and self.distance_hotel_tuple_list:
            hotels = self._sort_hotels(hotels=hotels)

        return hotels

    @staticmethod
    def get_hotels_having_policy_type(hotel_ids, policy_type):
        """
        :param hotel_ids: list of hotel_ids
        :param policy_type:
        :return: return only hotels having policy type
        """

        hotel_ids = HotelPolicyMap.objects.filter(hotel_id__in=hotel_ids,
                                                  policy_id__policy_type=policy_type).values_list(
                                                  'hotel_id', flat=True)
        return hotel_ids

    @staticmethod
    def get_top_rated_ta_hotels(hotel_ids, rating_gte=0, reviews_gte=0):
        """
        :param hotel_ids: list of hotel_ids
        :param rating_gte: fetch ratings greater than or equal to
        :param reviews_gte: fetch reviews greater than or equal to
        :return:
        """

        hotel_ids = HotelTAOverallRatings.objects.filter(
            hotel_ta_mapping__hotel_id__in=hotel_ids, overall_rating__gte=rating_gte,
            num_reviews_count__gte=reviews_gte).values_list('hotel_ta_mapping__hotel_id', flat=True)\
            .order_by('-overall_rating', '-num_reviews_count')

        return hotel_ids

    @staticmethod
    def get_ta_rating_and_rating_url(hotel_ids):
        """
        :param hotel_ids:
        :return: return dict having hotel_id, rating, rating_url
        """
        ta_reviews = HotelTAOverallRatings.objects.filter(
            hotel_ta_mapping__hotel_id__in=hotel_ids).values(
            'hotel_ta_mapping__hotel_id', 'overall_rating', 'url_overall_rating_image')
        return ta_reviews

    def get_city_hotels(self, city):
        """
        :param city: city_name / slug
        :return: fetch hotels of given city
        """

        query = Hotel.enabled_hotel_objects.filter(status=Hotel.ENABLED)
        query = self._get_city_query(query=query, city=city)
        hotels = query.all()

        return hotels

    @staticmethod
    def get_hotel_aliases(hotel_ids):
        hotel_aliases = HotelAlias.objects.filter(hotel__id__in=hotel_ids, is_enabled=True)
        hotel_wise_alias = {alias.hotel_id: alias for alias in hotel_aliases}
        return hotel_wise_alias
