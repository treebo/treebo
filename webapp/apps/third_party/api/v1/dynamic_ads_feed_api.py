import logging

from django.core.files import File
from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from django.conf import settings
from boto.s3.connection import S3Connection
from boto.s3.key import Key

from apps.third_party.exceptions import HotelFeedDataError
from apps.third_party.service.dynamic_ads_factory import DynamicAdsFactory
from apps.third_party.service.dynamic_ads_feeds_service import DynamicAdsService
from apps.third_party.tasks import set_cache_for_feed_component_or_build_csv
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException

logger = logging.getLogger(__name__)
__author__ = "Niket Gaurav"


class DynamicAdsHotelFeedCreate(TreeboAPIView):

    def get(self, request, *args, **kwargs):
        """
        Working For Google And Facebook Feeds
        Has to be triggered by cron job
        :param request: google
        :param args:
        :param kwargs:
        """
        feed_type = kwargs.get('feed_type', '').lower()
        try:
            # creation of feeds
            if feed_type not in ['google', 'facebook', 'criteo']:
                return Response(status = HTTP_400_BAD_REQUEST, data = {
                    "data": "Feed type not available"
                })
            feed = {
                "feed_type": feed_type
            }
            logger.info("Feed_type recieved from api %s", feed)
            set_cache_for_feed_component_or_build_csv.apply_async(args=['feed',feed])
            return HttpResponse("success")
        except HotelFeedDataError as e:
            logger.exception("Sending mail for failure ")
            DynamicAdsService.send_task_failure_mail(feed_type)
        except Exception as e:
            logger.exception('Exception in DynamicAdsHotelFeed')
            return HttpResponse('<h1>Some error occurred</h1>')


class DynamicAdsFeedDownload(TreeboAPIView):

    def get(self, request, *args, **kwargs):
        """
        For providing direct download option for google or facebook
        :param proxy_object: For google and facebook feed settings
        :param feed_type: google
        :return: Response in csv format
        """
        try:
            feed_type = kwargs.get('feed_type', '').lower()
            proxy_object = DynamicAdsFactory.get_proxy(feed_type)

            conn_s3 = S3Connection(host = 's3-ap-southeast-1.amazonaws.com')
            bucket = conn_s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
            bucket_key = Key(bucket)
            path = proxy_object.file_name
            bucket_key.key = settings.S3_STATIC_EXTERNAL_FILE_PATH + path
            bucket_key.get_contents_to_filename(proxy_object.file_path_name)

            download_file = File(open(proxy_object.file_path_name, 'r'))
            response = HttpResponse(download_file, content_type='text/csv')

            response['Content-Disposition'] = 'attachment; filename=' + proxy_object.file_name
            return response
        except TreeboException as tbexc:
            logger.exception('Treebo exception in DynamicAdsHotelFeed')
            return


class DynamicAdsFeedForAppDownload(TreeboAPIView):

    def get(self, request, *args, **kwargs):
        """
        For providing direct download option for google or facebook
        :param proxy_object: For google and facebook feed settings
        :param feed_type: google
        :return: Response in csv format
        """
        try:
            feed_type = kwargs.get('feed_type', '').lower()
            proxy_object = DynamicAdsFactory.get_proxy(feed_type)

            if proxy_object.get_app_file_name():
                conn_s3 = S3Connection(host='s3-ap-southeast-1.amazonaws.com')
                bucket = conn_s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
                bucket_key = Key(bucket)
                path = proxy_object.get_app_file_name()
                bucket_key.key = settings.S3_STATIC_EXTERNAL_FILE_PATH + path
                bucket_key.get_contents_to_filename(proxy_object.get_app_file_path())

                download_file = File(open(proxy_object.get_app_file_path(), 'r'))
                response = HttpResponse(download_file, content_type='text/csv')

                response['Content-Disposition'] = 'attachment; filename=' + proxy_object.get_app_file_name()
                return response
            else:
                return HttpResponse('<h1>Invalid value in argument feed_type</h1>', status=400)
        except TreeboException as e:
            logger.error('TreeboException in DynamicAdsFeedForAppDownload', exc_info=e)
            return HttpResponse('<h1>Some error occurred. Please try after sometime</h1>', status=500)


class DynamicAdsHotelFeedEmail(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        """
        For providing direct email for verification
        :param proxy_object: For google or facebook feed settings
        :param feed_type:
        :return: Response in csv format
        """
        try:
            to_email = kwargs.get('email', None)
            logger.info("Email sent to : %s", to_email)
            feed_type = kwargs.get('feed_type', '').lower()
            logger.info("Feed type : %s", feed_type)
            proxy_object = DynamicAdsFactory.get_proxy(feed_type)
            if to_email:
                DynamicAdsService.DYNAMIC_ADS_ADMIN_LIST = [to_email]
                logger.info("Got the Email Data for %s", to_email)
                DynamicAdsService.send_email_with_file(proxy_object, to_email)
        except TreeboException as tbexc:
            logger.exception('Treebo exception in DynamicAdsHotelFeed')
            return HttpResponse('<h1>%s</h1>' % str(tbexc))
