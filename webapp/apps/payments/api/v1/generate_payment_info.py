# -*- coding:utf-8 -*-
"""
    GeneratePaymentInfo  API
"""

from decimal import Decimal

from rest_framework.status import HTTP_201_CREATED

from apps.bookings.models import PaymentOrder
from apps.common import error_codes
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from apps.payments.dto.generate_payment_dto import GeneratePaymentDTO
from apps.payments.serializer import PaymentOrderSerializer
from base.logging_decorator import log_args
from base.views.api import TreeboAPIView
from apps.common.api_response import APIResponse as api_response
from common.exceptions.treebo_exception import TreeboException
from services.restclient.paymentservice_client import PaymentServiceClient
from apps.common.slack_alert import SlackAlertService as slack_alert
import logging

logger = logging.getLogger(__name__)


class GeneratePaymentInfo(TreeboAPIView):
    payment_service_client = PaymentServiceClient()
    authentication_classes = []

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        try:
            generate_payment_dto = GeneratePaymentDTO(data=request.data)
            if not generate_payment_dto.is_valid():
                logger.error(
                    " invalid request for generate payment %s ",
                    request.data)
                return api_response.invalid_request_error_response(
                    generate_payment_dto.errors, GeneratePaymentInfo.__name__)
            amount = generate_payment_dto.data['amount']
            bid = generate_payment_dto.data['bid']
            hotel_id = generate_payment_dto.data['hotel_id']
            payment_mode = generate_payment_dto.data["payment_mode"]
            logger.info("generate payment %s ", request.data)
            payment_details = self.payment_service_client.generate_order(amount, [
                                                                         bid, hotel_id])
            logger.info(
                " payment details for request %s -> %s  ",
                request.data,
                payment_details)
            pg_order_id = payment_details['pg_order_id']
            ps_order_id = payment_details['order_id']
            payment_order, is_payment_created = PaymentOrder.objects.get_or_create(
                pg_order_id=pg_order_id, gateway_type=payment_mode)
            payment_order.amount = Decimal(amount)
            payment_order.status = PaymentOrder.ISSUED
            payment_order.ps_order_id = ps_order_id
            payment_order.save()
            payment_data = PaymentOrderSerializer(
                instance=payment_order, many=False).data
            return api_response.prep_success_response(
                payment_data, HTTP_201_CREATED)
        except TreeboException as e:
            logger.exception("treebo generate payment v1 %s ", request.data)
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.POST,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception("internal generate payment v1 %s ", request.data)
            response = api_response.internal_error_response(
                error_codes.get(
                    error_codes.UNABLE_TO_GENERATE_PAYMENT)['msg'],
                GeneratePaymentInfo.__name__)
        return response
