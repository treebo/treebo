import datetime

from rest_framework import serializers

from apps.bookingstash.models import HotelRoomMapper, Availability
from dbcommon.models.hotel import Hotel
from dbcommon.models.location import State, City
from dbcommon.models.room import Room


class StateSyncSerializer(serializers.ModelSerializer):
    state_id = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    class Meta:
        model = State
        fields = ('state_id', 'name')

    def get_state_id(self, state):
        return state.id

    def get_name(self, state):
        return state.name


class RoomSyncSerializer(serializers.ModelSerializer):
    room_id = serializers.SerializerMethodField()
    room_type = serializers.SerializerMethodField()
    max_guest_count = serializers.SerializerMethodField()
    maximojo_room_id = serializers.SerializerMethodField()
    default_base_price = serializers.SerializerMethodField()
    inventory = serializers.SerializerMethodField()

    class Meta:
        model = Room
        fields = (
            'room_id',
            'room_type',
            'max_guest_count',
            'maximojo_room_id',
            'default_base_price',
            'inventory',
            'max_guest_allowed',
            'max_adult_with_children',
            'max_children')

    def get_room_id(self, room):
        return room.id

    def get_room_type(self, room):
        return room.room_type_code.lower()

    def get_max_guest_count(self, room):
        return room.max_adult_with_children

    def get_maximojo_room_id(self, room):
        mapper = HotelRoomMapper.objects.get(room_id=room.id)
        return mapper.mm_room_code

    def get_default_base_price(self, room):
        return float(room.price)

    def get_inventory(self, room):
        return room.quantity


class CitySyncSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name',)


class HotelSyncSerializer(serializers.ModelSerializer):
    hotel_id = serializers.SerializerMethodField()
    state_id = serializers.SerializerMethodField()
    maximojo_id = serializers.SerializerMethodField()
    provider = serializers.SerializerMethodField()
    city = CitySyncSerializer()
    rooms = RoomSyncSerializer(many=True, read_only=True)

    class Meta:
        model = Hotel
        fields = (
            'hotel_id',
            'name',
            'status',
            'uuid',
            'state_id',
            'city',
            'hotelogix_id',
            'hotelogix_name',
            'maximojo_id',
            'rooms',
            'price_increment',
            'cs_id',
            'provider')

    def get_hotel_id(self, hotel):
        return hotel.id

    def get_state_id(self, hotel):
        return hotel.state_id

    def get_maximojo_id(self, hotel):
        room = hotel.rooms.first()
        if room:
            try:
                mapper = HotelRoomMapper.objects.get(room_id=room.id)
                return mapper.mm_hotel_code
            except BaseException:
                return ""
        return ""

    def get_provider(self, hotel):
        return hotel.provider_name


class AvailabilitySerializer(serializers.ModelSerializer):
    hotel_id = serializers.SerializerMethodField()
    availability = serializers.SerializerMethodField()
    room_code = serializers.SerializerMethodField()
    date = serializers.SerializerMethodField()

    class Meta:
        model = Availability
        fields = ('hotel_id', 'availability', 'room_code', 'date')

    def get_hotel_id(self, availability):
        return availability.room.hotel.hotelogix_id

    def get_availability(self, availability):
        return availability.availablerooms

    def get_room_code(self, availability):
        return availability.room.room_type_code

    def get_date(self, availability):
        return datetime.datetime.strftime(availability.date, "%Y-%m-%d")
