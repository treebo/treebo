import json
import logging

from django.core import serializers

from apps.hotels.utils import decode_hash
from base.views.api import TreeboAPIView
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.facilities import Facility
from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)


class HotelFacilities(TreeboAPIView):
    def get(self, hashedHotelId="", format=None):
        hotelId = decode_hash(hashedHotelId)
        facilityList = self.__getFacilityQuerSet(hotelId)
        allFacilities, message = self.getAllFacilities(facilityList)
        response = self.__getResponse(allFacilities, message)

        return response

    def getAllFacilities(self, facilityList):

        result = []
        message = "Not a valid Facility Object or No Facilities Present in the Hotel"
        try:
            serialData = serializers.serialize(
                'json', facilityList, fields=(
                    'name', 'url', 'css_class'))
            serialData = json.loads(serialData)
            for s in serialData:
                result.append(s['fields'])
                message = "success"
            logger.debug("city information is : " + str(result))
        except Exception as exc:
            logger.exception(exc)
            logger.debug(message)

        return result, message

    def __getResponse(self, allFacilities, message):
        response = {'message': message, 'status': 0}

        if allFacilities['message'] == 'success':
            response['facilities'] = allFacilities
            status = 1
            response['status'] = status

        return response

    def __getFacilityQuerSet(self, hotelId):
        hotelObject = Hotel.objects.get(hotel_id=hotelId)
        facilityList = Facility.objects.filter(hotels__in=hotelObject, css_class__isnull=False).exclude(css_class__exact='')

        return facilityList
