import React from 'react';
import { findDOMNode } from 'react-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Payment, { fns as PayHelpers } from 'payment';
import { HOC } from 'formsy-react';


Payment.addToCardArray({
    type: 'rupay',
    pattern: /^(508|606|607|608|652|653)/,
    format: /(\d{1,4})/g,
    length: [16],
    cvcLength: [3],
    luhn: true,
});

// Add this to the end of the array to get default values
Payment.addToCardArray({
    type: 'default',
    pattern: /^\d*/,
    format: /(\d{1,4})/g,
    length: [16],
    cvcLength: [3],
    luhn: false,
});


class CardInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDirty: false,
        };
    }

    getCardTypeDetails = num => Payment.getCardArray().find(
        v => v.pattern.test(num)
    );

    handleFieldChange = (e) => {
        const current = e.target.value.replace(/ /g, '');

        if (current && !/^\d+$/.test(current)) {
            return;
        }

        const cardType = this.getCardTypeDetails(current);
        const formattedValue = (PayHelpers.formatCardNumber(current) || '').trim();
        this.props.setValue(formattedValue);
        this.props.onChange({
            value: formattedValue,
            type: cardType,
        });
    };

    render() {
        const { value, type } = this.props.data;
        return (
            <div className="card-pay__input-container">
                <input
                    className={`float-labels__input card-pay__input card-pay__number card-pay__number--${type.type}`}
                    type="text" value={value} onChange={this.handleFieldChange}
                    onBlur={() => this.setState({ isDirty: true })}
                />
                <p className="card-pay__error error">
                    {this.state.isDirty || this.props.isFormSubmitted() ? this.props.getErrorMessage() : ''}
                </p>
            </div>
        );
    }
}

const FormsyCardInput = HOC(CardInput);

class ExpiryInput extends React.Component {
    handleFieldChange = (e) => {
        const val = {
            ...this.props.data,
            [e.target.name]: e.target.value,
        };
        this.props.setValue(val);
        this.props.onChange(val);
    };

    render() {
        const currentYear = new Date().getFullYear();
        const { month, year } = this.props.data;
        return (
            <div>
                <div className="card-pay__expiry">
                    <div className="select medium">
                        <select
                            className="card-pay__expiry-field" name="month"
                            value={month} onChange={this.handleFieldChange}
                        >
                            <option value="">MONTH</option>
                            {Array.from(Array(12), (v, i) => <option key={i + 1}>{i + 1}</option>)}
                        </select>
                    </div>
                    <div className="select medium">
                        <select
                            className="card-pay__expiry-field" name="year"
                            value={year} onChange={this.handleFieldChange}
                        >
                            <option value="">YEAR</option>
                            {Array(36).fill().map((v, i) => <option key={i}>{i + currentYear}</option>)}
                        </select>
                    </div>
                </div>
                <p className="card-pay__error error">
                    {this.props.isFormSubmitted() ? this.props.getErrorMessage() : ''}
                </p>
            </div>
        );
    }
}

const FormsyExpiryInput = HOC(ExpiryInput);

class CvvInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDirty: false,
        };
    }

    handleFieldChange = (e) => {
        const maxLength = this.props.card.type.cvcLength.slice(-1)[0];
        const current = e.target.value.slice(0, maxLength);

        if (current && !/^\d+$/.test(current)) {
            return;
        }

        this.props.setValue(current);
        this.props.onChange(current);
    };

    render() {
        return (
            <div>
                <div className="card-pay__cvv-container">
                    <input
                        className="float-labels__input card-pay__input card-pay__cvv"
                        onBlur={() => this.setState({ isDirty: true })}
                        type="text" value={this.props.data} onChange={this.handleFieldChange}
                    />
                    <div className="card-pay__cvv-details">
                        <span className="card-pay__cvv-image"></span>
                        <p className="card-pay__cvv-text">
                            Last 3 digits printed on the back of the card
                        </p>
                    </div>
                </div>
                <p className="card-pay__error error">
                    {this.state.isDirty || this.props.isFormSubmitted() ? this.props.getErrorMessage() : ''}
                </p>
            </div>
        );
    }
}

const FormsyCvvInput = HOC(CvvInput);

class NameInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDirty: false,
        };
    }

    handleFieldChange = (e) => {
        this.props.setValue(e.target.value);
        this.props.onChange(e.target.value);
    };

    render() {
        return (
            <div className="card-pay__input-container">
                <input
                    className="float-labels__input card-pay__input"
                    onBlur={() => this.setState({ isDirty: true })}
                    type="text" value={this.props.data} onChange={this.handleFieldChange}
                />
                <p className="card-pay__error error">
                    {!this.props.isPristine() || this.props.isFormSubmitted() ? this.props.getErrorMessage() : ''}
                </p>
            </div>
        );
    }
}

const FormsyNameInput = HOC(NameInput);

class CardPay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            card: {
                type: {
                    type: 'default',
                    cvcLength: [3],
                    length: [16],
                },
                value: '',
            },
            name: '',
            expiry: {
                month: '',
                year: '',
            },
            cvv: '',
        };
    }

    onChange = (field, value) => {
        this.setState({ [field]: value });
    };

    pay = () => {
        const { card, name, expiry, cvv } = this.state;
        const data = {
            method: 'card',
            'card[name]': name,
            'card[number]': card.value,
            'card[cvv]': cvv,
            'card[expiry_month]': expiry.month,
            'card[expiry_year]': expiry.year,
        };

        this.props.makePayment(data);
    };

    render() {
        const { card, name, expiry, cvv } = this.state;
        return (
            <div className="form-container">
                <Formsy.Form className="card-pay" onValidSubmit={this.pay} >
                    <div className="float-labels">
                        <FormsyCardInput
                            onChange={(v) => this.onChange('card', v)} name="card"
                            data={card}
                            validations={{
                                validCard: (vs, v) => !!PayHelpers.validateCardNumber(v),
                            }}
                            validatationError="Please Enter the Card Number"
                            validationErrors={{
                                validCard: 'Card Number Invalid',
                                isDefaultRequiredValue: 'Please Enter the Card Number',
                            }}
                            required
                        />
                        <label className="float-labels__label">Card Number<sup>*</sup></label>
                    </div>
                    <div className="float-labels">
                        <FormsyNameInput
                            onChange={(v) => this.onChange('name', v)}
                            data={name} name="name"
                            validatationError="Please Enter the Card Number"
                            validationErrors={{
                                isDefaultRequiredValue: 'Please Enter the Card Holder\'s Name',
                            }}
                            required
                        />
                        <label className="float-labels__label">Card Holder's Name<sup>*</sup></label>
                    </div>
                    <div className="float-labels">
                        <label className="float-labels__label card-pay__label">Expiry Date<sup>*</sup></label>
                        <FormsyExpiryInput
                            onChange={(v) => this.onChange('expiry', v)}
                            data={expiry} name="expiry"
                            validations={{
                                validExpiry: (vs, v) => !!(v && v.month && v.year),
                            }}
                            validationErrors={{
                                validExpiry: 'Please select the expiry Month and Year',
                            }}
                        />
                    </div>
                    <div className="float-labels">
                        <FormsyCvvInput
                            card={card} onChange={(v) => this.onChange('cvv', v)}
                            data={cvv} name="cvv"
                            validations={{
                                validCvv: (vs, v) => !!PayHelpers.validateCardCVC(v, card.type.type),
                            }}
                            validationErrors={{
                                validCvv: 'Invalid CVV',
                                isDefaultRequiredValue: 'Please Enter the CVV',
                            }}
                            required
                        />
                        <label className="float-labels__label">CVV<sup>*</sup></label>
                    </div>
                    <button
                        type="submit" name="button"
                        className="checkout__action checkout__action--continue btn btn--round"
                    >
                        Pay Now
                    </button>
                </Formsy.Form>
            </div>
        );
    }
}

class Wallet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wallet: ['mobikwik'],
            value: 'mobikwik',
            showError: false,
        };
    }

    handleFieldChange = (e) => {
        this.setState({
            value: e.target.value,
            showError: false,
        });
    };

    pay = (e) => {
        e.preventDefault();
        if (!this.state.value) {
            this.setState({ showError: true });
        } else {
            this.props.makePayment({
                method: 'wallet',
                wallet: this.state.value,
            });
        }
    };

    render() {
        const { wallet, value, showError } = this.state;
        return (
            <form className="wallet">
                <div>
                    <ul className="wallet__list">
                        {wallet.map((v, i) => (
                            <li
                                key={i}
                                className={`wallet__item ${
                                value === v && 'wallet__item--active'
                                    }`}
                            >
                                <div>
                                    <label className="flex-row flex--justify-center wallet__input-container">
                                        <input
                                            className="wallet__input" type="radio" name="bank" value={v}
                                            onChange={this.handleFieldChange} checked={value === v}
                                        />
                                        <span>
                                          <img src="https://static.treebohotels.com/dist/desktop/images/mobikwik_Logo.png" className="" />
                                        </span>
                                    </label>
                                </div>
                            </li>
                        ))}
                    </ul>
                    <div className="mobikwik__msg">
                        Use code KWIK10 for 10% Cashback (Max Rs.250) when you pay using MobiKwik. T&C apply.
                    </div>
                    {showError ? <p className="error">Please select a wallet</p> : null}
                </div>
                <div className="wallet__action">
                    <button
                        type="submit" name="button" onClick={this.pay}
                        className="checkout__action checkout__action--continue btn btn--round"
                    >
                        Pay Now
                    </button>
                </div>
            </form>
        );
    }
}

class Netbank extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            showError: false,
        };
    }

    handleFieldChange = (e) => {
        this.setState({
            value: e.target.value,
            showError: false,
        });
    };

    pay = (e) => {
        e.preventDefault();

        if (!this.state.value) {
            this.setState({ showError: true });
        } else {
            this.props.makePayment({
                method: 'netbanking',
                bank: this.state.value,
            });
        }
    };

    render() {
        const { defaultBanks, banks } = this.props;
        const { value, showError } = this.state;
        return (
            <form className="netbank">
                <ul className="netbank__default">
                    {defaultBanks.map((v, i) => (
                        <li key={i}
                            className={`netbank__default-item ${
                                    value === v.key && 'netbank__default-item--active'
                                }`}
                        >
                            <div>
                                <label className="flex-row flex--justify-center netbank__input-container">
                                    <input
                                        className="netbank__input" type="radio" name="bank" value={v.key}
                                        onChange={this.handleFieldChange} checked={value === v.key}
                                    />
                                    <span
                                        className={`netbank__display netbank__display--${v.value.toLowerCase()}`}
                                    >
                                        {v.value}
                                    </span>
                                </label>
                            </div>
                        </li>
                    ))}
                </ul>
                <div className="netbank__list">
                    <div className="select medium">
                        <select value={value} onChange={this.handleFieldChange} className="netbank__list-select" >
                            <option value="">OTHER BANKS</option>
                            {banks.map((v, i) => (<option key={i} value={v.key}>{v.value}</option>))}
                        </select>
                    </div>
                    <p className="netbank__error error">{showError ? 'Please select a bank' : null}</p>
                </div>
                <div className="netbank__action">
                    <button
                        type="submit" name="button" onClick={this.pay}
                        className="checkout__action checkout__action--continue btn btn--round"
                    >
                        Pay Now
                    </button>
                </div>
            </form>
        );
    }
}

class Pay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultBanks: [
                { key: 'SBIN', value: 'SBI' },
                { key: 'HDFC', value: 'HDFC' },
                { key: 'ICIC', value: 'ICICI' },
                { key: 'UTIB', value: 'Axis' },
                { key: 'KKBK', value: 'Kotak' },
                { key: 'YESB', value: 'Yes' },
            ],
            banks: [],
        };
    }

    componentDidMount() {
        Razorpay.configure(this.props.config);
        Razorpay.payment.getMethods(({ netbanking }) => {
            this.setState({
                banks: Object.keys(netbanking).map(key => ({ key, value: netbanking[key] })),
            });
        });

        const $tab = findDOMNode(this.refs.tabs);
        $tab.classList.remove('react-tabs');
    }

    makePayment = (data) => {
        const { bid, user, amount, onSuccessPay, onErrorPay } = this.props;
        const razorpay = new Razorpay({ amount });
        razorpay.createPayment({
            amount,
            ...user,
            currency: 'INR',
            ...data,
            'notes[order_id]': bid,
        });

        razorpay.on('payment.success', (resp) => {
            onSuccessPay(resp.razorpay_payment_id);
        });

        razorpay.on('payment.error', (resp) => {
            onErrorPay(resp.error.description);
        });
    };

    render() {
        const { banks, defaultBanks } = this.state;
        return (
            <Tabs ref="tabs">
                <TabList>
                    <Tab>
                        <div>
                            Credit/Debit Card
                        </div>
                    </Tab>
                    <Tab>
                        <div>
                            Net Banking
                        </div>
                    </Tab>
                    <Tab>
                        <div className="flex-column">
                            <div>Mobikwik Wallet</div>
                            <div className="mobikwik__offer">FLAT 10% Cashback</div>
                        </div>

                    </Tab>
                </TabList>

                <TabPanel>
                    <CardPay makePayment={this.makePayment} />
                </TabPanel>
                <TabPanel>
                    <Netbank makePayment={this.makePayment} banks={banks} defaultBanks={defaultBanks} />
                </TabPanel>
                <TabPanel>
                    <Wallet makePayment={this.makePayment} />
                </TabPanel>
            </Tabs>
        );
    }
}

export default Pay;
