# -*- coding:utf-8 -*-
"""
    BookingStubAPI
"""
import datetime
import json

import requests
from django.conf import settings
from django.db.models import Q, Sum
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

from apps.bookings.models import Booking, RoomBooking, Reservation
from apps.bookings.serializers import BookingSerializer
from base import log_args
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger

logger = get_booking_logger(__name__)


class BookingDetails(TreeboAPIView):
    """
        BookingDetails
    """

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        return super(BookingDetails, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        """
        :param request:
        :param booking_id:
        :return:
        """
        booking_id = int(kwargs.get('bookingId'))
        try:
            booking = Booking.objects.get(pk=int(booking_id))
            serializer = BookingSerializer(booking)
            return Response(serializer.data)
        except Booking.DoesNotExist as e:
            logger.debug('No booking found with booking_id=%s' % booking_id)
            return Response('No booking found', status=404)


class BookingsByOrderId(TreeboAPIView):
    """
        BookingsByOrderId
    """

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        return super(BookingsByOrderId, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        """
        :param request:
        :param order_id:
        :return:
        """
        order_id = kwargs.get('orderId')
        try:
            booking = Booking.objects.get(order_id=order_id)
            serializer = BookingSerializer(booking)
            return Response(serializer.data)
        except Booking.DoesNotExist:
            logger.debug('No booking found for order_id=%s' % order_id)
            return Response('No booking found', status=404)

    @log_args(logger)
    def put(self, request, order_id):
        """
        :param request:
        :param order_id:
        :return:
        """
        logger.info('Inside PUT BookingsByOrderId')
        data = JSONParser().parse(request)
        try:
            booking = Booking.objects.get(order_id=order_id)
            serializer = BookingSerializer(booking, data=data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)

            return Response(serializer.errors, status=404)
        except Booking.DoesNotExist:
            logger.debug('No booking found for order_id=%s' % order_id)
            return Response('No booking found', status=404)


class GetLastBookingsByHotel(TreeboAPIView):
    """
        GetLastBookingsByHotel
    """
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        return super(GetLastBookingsByHotel, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, hotelId, delta):
        """
        :param request:
        :param hotelId:
        :param delta:
        :return:
        """
        time_threshold = datetime.datetime.now() - datetime.timedelta(hours=int(delta))
        booking = Booking.objects.filter(
            hotel_id=int(hotelId),
            created_at__gt=time_threshold)
        if booking:
            last_booking_time = booking.reverse()[0].created_at
            return Response({'lastBookingTime': last_booking_time})
        else:
            logger.debug('No bookings found for hotelId=%s' % hotelId)
            return Response('No bookings found', status=404)


#
class GetBookingsByUser(TreeboAPIView):
    """
        GetBookingsByUser
    """
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        return super(GetBookingsByUser, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, email, userId):
        """
        :param request:
        :param email:
        :param userId:
        :return:
        """
        try:
            bookings = Booking.objects.filter(
                Q(guest_email=email) | Q(user_id=userId)).order_by('-created_at')
            serializer = BookingSerializer(bookings, many=True)
            return Response(serializer.data)
        except Booking.DoesNotExist:
            logger.debug('No bookings found for email=%s' % email)
            return Response('No bookings found', status=400)


class GetRoomBookingByRoom(TreeboAPIView):
    """
        GetRoomBookingByRoom
    """
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        return super(GetRoomBookingByRoom, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, bookingId):
        """
        :param request:
        :param bookingId:
        :return:
        """
        try:
            roomBookingCount = RoomBooking.objects.filter(
                booking_id=bookingId).count()
            roomBooking = RoomBooking.objects.filter(
                booking_id=bookingId)[:1].get()
            return Response({'roomBookingCount': roomBookingCount,
                             'roomBooking': json.loads(roomBooking)})
        except Booking.DoesNotExist:
            logger.debug('No room bookings found for bookingId=%s' % bookingId)
            return Response('No room bookings found', status=400)


class SaveBooking(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SaveBooking, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def put(self, request, bookingId):
        try:
            return None
        except Booking.DoesNotExist:
            logger.debug('No room bookings found for bookingId=%s' % bookingId)
            return Response('No room bookings found', status=400)


class GetReservationForHotel(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetReservationForHotel, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request):
        groupBy = request.GET['groupBy']
        sumBy = request.GET['sumBy']
        status = request.GET['status']
        checkInDateStr = request.GET['checkInDate']
        checkOutDateStr = request.GET['checkOutDate']

        checkInDate = datetime.datetime.strptime(
            checkInDateStr, '%Y-%m-%d %H:%M:%S')

        checkOutDate = datetime.datetime.strptime(
            checkOutDateStr, '%Y-%m-%d %H:%M:%S')

        roomsReserved = Reservation.objects.values(groupBy).filter(
            reservation_status=status,
            reservation_checkin_date__lte=checkInDate,
            reservation_checkout_date__gt=checkOutDate).order_by().annotate(
            room_sum=Sum(sumBy))
        if roomsReserved:
            data = list(roomsReserved)
            json_string = json.dumps(data)  # , default=lambda o: o.__dict__
            return Response(json.loads(json_string))
        else:
            logger.debug('No Reservations found')
            return Response('No Reservations found', status=404)


class ReservationsForToday(TreeboAPIView):
    """
    All who could checkout today, for feedback tab
    """

    @log_args(logger)
    def get(self, request):
        """

        :param request:
        :return:
        """
        response = requests.get(
            settings.HMS_GET_ALL_RESERVATIONS_FOR_TODAY +
            '?id=' +
            request.GET["id"])
        return JsonResponse(response.json(), safe=False)
