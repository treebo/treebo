# -*- coding:utf-8 -*-
"""
    CreateBooking  API , internal api called through other apps internally
"""

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR

from apps.common import error_codes as codes
from apps.common.error_codes import get as error_message
from apps.bookings.serializers import BookingSerializer
from apps.bookings.services.booking import BookingService
from apps.bookings import constants
from base import log_args
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger
from data_services.respositories_factory import RepositoriesFactory
from data_services.exceptions import HotelDoesNotExist
from apps.common.slack_alert import SlackAlertService as slack_alert
from rest_framework.status import HTTP_409_CONFLICT, HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST, HTTP_200_OK


logger = get_booking_logger(__name__)

hotel_repository = RepositoriesFactory.get_hotel_repository()


class CreateBooking(TreeboAPIView):
    """
        base internal api to create booking
    """
    __booking_service = BookingService()
    authentication_classes = []

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        booking_data = request.data
        booking_data = request.data
        hotel_id = booking_data.get('hotel_id')
        room_type = booking_data.get("room_type")
        hotels = hotel_repository.filter_hotels(id=int(hotel_id))
        hotel = hotels[0] if hotels else None
        if not hotel:
            raise HotelDoesNotExist
        rooms = hotel_repository.get_rooms_for_hotel(hotel)
        room = next(
            (room for room in rooms if room.room_type_code == room_type),
            None)
        booking_data["hotel"] = hotel_id
        booking_data["room"] = room.id if room else None
        # try:
        status, booking = BookingService.create_booking(booking_data, async=False)

