import logging
from apps.auth.csrf_session_auth import CsrfExemptSessionAuthentication
from apps.auth.dto.user_login_dto import UserLoginDTO
from apps.auth.dto.user_register_dto import UserRegisterDTO
from apps.auth.dto.user_simple_register_dto import UserSimpleRegisterDTO
from apps.auth.serializer.user_serializer import UserSerializer
from apps.auth.services.cookie_service import CookieService
from apps.common.api_response import APIResponse as api_response
from apps.profiles.service.user_service import UserService
from base import log_args
from base.renderers import TreeboCustomJSONRenderer
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.common import error_codes

logger = logging.getLogger(__name__)


class RegisterApi(TreeboAPIView):
    renderer_classes = [TreeboCustomJSONRenderer, ]
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    user_service = UserService()
    cookie_service = CookieService()

    def get(self, request, format=None):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, format=None):
        """
        :param request:
        :param format:
        :return:
        """
        try:
            user_simple_register_dto = UserSimpleRegisterDTO(data=request.data)
            if not user_simple_register_dto.is_valid():
                logger.error("register invalid request ")
                return api_response.invalid_request_error_response(
                    user_simple_register_dto.errors, RegisterApi.__name__)
            user, token = self.user_service.register_user(
                user_simple_register_dto.data)
            self.user_service.login_user(request, user)
            user_dto = UserSerializer(instance=user)
            user_data = user_dto.data
            user_data['token'] = token
            response = api_response.prep_success_response(user_data)
            self.cookie_service.set_user_info_in_cookie(response, user)
        except TreeboException as e:
            logger.exception("treebo register exception ")
            response = api_response.treebo_exception_error_response(e)
        except Exception as e:
            logger.exception("internal error register exception ")
            response = api_response.internal_error_response(error_codes.get(
                error_codes.REGISTER_FAILED)['msg'], RegisterApi.__name__)
        return response
