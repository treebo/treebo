import logging
import time

from rest_framework.renderers import BaseRenderer
from rest_framework.renderers import JSONRenderer

logger = logging.getLogger(__name__)


class TreeboJSONRenderer(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        status_code = renderer_context['response'].status_code
        request = renderer_context['request']
        request_data = renderer_context['kwargs']
        request_data.update(request.GET.dict())
        if status_code / 100 != 2:
            data.update({'request_data': request_data})
            return super(
                TreeboJSONRenderer,
                self).render(
                data,
                accepted_media_type,
                renderer_context)
        else:
            data = {
                'status': 'success',
                'data': data,
                'request_data': request_data}
            return super(
                TreeboJSONRenderer,
                self).render(
                data,
                accepted_media_type,
                renderer_context)


class XMLRenderer(BaseRenderer):
    """
    Plain text parser.
    """
    media_type = 'text/xml'

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Simply return a string representing the body of the request.
        """
        data_xml = stream.read()
        logger.info(data_xml)
        return data_xml


class TreeboCustomJSONRenderer(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        start_time = time.clock()
        status_code = renderer_context['response'].status_code
        request = renderer_context['request']
        request_data = renderer_context['kwargs']
        request_data.update(request.GET.dict())
        status_code = status_code if status_code else 200
        if 200 <= status_code < 300:
            data = {'status': status_code, 'data': data}
        else:
            if 'errors' in data:
                data = {'status': status_code, 'errors': data['errors']}
            elif isinstance(data, list):
                data = {'status': status_code, 'errors': data}
            else:
                data = {'status': status_code, 'errors': [data]}
        data.update({'request_data': request_data})
        response = super(
            TreeboCustomJSONRenderer,
            self).render(
            data,
            accepted_media_type,
            renderer_context)
        logger.debug(
            "JSON rendering --- %s milliseconds ---" %
            (self.elapsed(start_time)))

        return response

    def elapsed(self, start_time):
        return (time.clock() - start_time) * 1000
