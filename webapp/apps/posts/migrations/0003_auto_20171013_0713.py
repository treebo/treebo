# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0002_auto_20171012_1325'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='qamapping',
            name='question',
        ),
        migrations.AddField(
            model_name='qamapping',
            name='question_id',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='qamapping',
            name='page_type_id',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
