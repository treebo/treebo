import logging

from django.core.cache import cache
from django.db.models.query_utils import Q

from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from dbcommon.models.location import Locality, CityAlias, State

logger = logging.getLogger(__name__)

LOCATION_TYPES = {"lodging": "lodging", "establishment": "establishment"}


class SearchService:
    CACHE_TIMEOUT = 60 * 60
    CACHE_KEY = 'cs_apicache_search_service_{0}'

    def get_cache_key(self, search_str):
        return SearchService.CACHE_KEY.format(search_str)

    @staticmethod
    def invalidate_cache():
        cache.delete_pattern(SearchService.CACHE_KEY.format('*'))

    def search(
            self,
            search_str,
            city_name=None,
            latitude=None,
            longitude=None,
            location_type=None):
        try:
            cache_value = cache.get(self.get_cache_key(
                "_".join([search_str, location_type])))
        except Exception as e:
            cache_value = None
        if cache_value:
            return cache_value
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        possible_location_types = []
        if location_type:
            possible_location_types = location_type.split(',')
        likely_hotel_search = None
        for location_type in possible_location_types:
            likely_hotel_search = False
            if LOCATION_TYPES.get(location_type):
                likely_hotel_search = True
                break
        search_array = search_str.split(",")
        search_array = [s.strip() for s in search_array if s.strip()]

        search_result = {
            'searchResults': [],
            'nearbyResults': [],
            'parentQuery': None,
            'isLocalitySearch': False,
            'cityOrStateOrHotelNotSearchedExplicitly': False,
            'city': None,
            'locality': None}

        if len(search_array) < 1:
            logger.debug('Returning no result')
            return search_result

        if len(search_array) == 1:
            if search_array[0].strip().lower() == 'india':
                search_result.update({
                    'searchResults': hotel_repository.get_active_hotel_list()
                    # 'searchResults': Hotel.objects.filter(status=Hotel.ENABLED).prefetch_related(
                    #     'rooms', 'image_set', 'facility_set', 'hotel_highlights').select_related(
                    #     'city', 'state', 'locality')
                })
                logger.debug('---------- Single result India')
                cache.set(
                    self.get_cache_key(
                        search_str.join(location_type)),
                    search_result,
                    SearchService.CACHE_TIMEOUT)
                return search_result

        if likely_hotel_search is not None:
            # Check for hotel only if likely_hotel_search is true
            if likely_hotel_search:
                if not self.__has_hotel_based_search(
                        search_array, search_result):
                    self.__state_city_locality_based_search(
                        search_array, search_result)
            else:
                self.__state_city_locality_based_search(
                    search_array, search_result)

        # if locationtype is empty, don't be dependent on location type values
        # and search sequentially on hotels, locality and state
        else:
            self.__city_hotel_based_search(search_array, search_result)

        cache.set(
            self.get_cache_key(
                search_str.join(location_type)),
            search_result,
            SearchService.CACHE_TIMEOUT)
        return search_result

    def __city_hotel_based_search(self, searchArray, search_result):
        if self.__has_city_or_locality_based_search(
                searchArray, search_result):
            return search_result
        elif self.__has_state_based_search(searchArray, search_result):
            return search_result
        elif self.__has_hotel_based_search(searchArray, search_result):
            return search_result
        else:
            return search_result

    def __state_city_locality_based_search(self, searchArray, search_result):
        if self.__has_city_or_locality_based_search(
                searchArray, search_result):
            return search_result
        elif self.__has_state_based_search(searchArray, search_result):
            return search_result
        else:
            return search_result

    def __hotel_state_city_locality_based_search(
            self, searchArray, search_result):
        if self.__has_hotel_based_search(searchArray, search_result):
            return search_result

        elif self.__has_city_or_locality_based_search(searchArray, search_result):
            return search_result
        elif self.__has_state_based_search(searchArray, search_result):
            return search_result
        else:
            return search_result

    def __has_hotel_based_search(self, searchArray, search_result):
        hotelsFilter = Hotel.objects.filter(
            Q(name__iregex='^.*' + searchArray[0] if searchArray[0] else '' + ''),
            status=Hotel.ENABLED).prefetch_related('rooms', 'image_set', 'facility_set',
                                                   'hotel_highlights').select_related('city',
                                                                                      'state',
                                                                                      'locality')

        if len(hotelsFilter) > 0:
            hotel = hotelsFilter.first()
            parentQuery = [
                {'name': hotel.city.name, 'lat': hotel.city.city_latitude,
                 'long': hotel.city.city_longitude},
                {'name': hotel.locality.name, 'lat': hotel.locality.latitude,
                 'long': hotel.locality.longitude},
            ]
            if len(hotelsFilter) == 1:
                parentQuery.append({'name': hotel.name, 'lat': '', 'long': ''})

            search_result.update({
                'searchResults': hotelsFilter,
                'parentQuery': parentQuery,
                # Assuming here, cityName is same as hotel.city.name, and
                # locality is same as hotel.locality
                'city': hotel.city,
                'locality': hotel.locality
            })
            return True
        return False

    def __has_city_or_locality_based_search(self, searchArray, search_result):
        cityName = None
        locality = None
        reversedArray = list(searchArray)
        reversedArray.reverse()
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        for cityQueryIndex, singleQuery in enumerate(reversedArray):
            cityAliasQuery = CityAlias.objects.filter(
                Q(alias__iregex='^.*' + singleQuery.strip() if singleQuery else '')).values('city')
            if len(cityAliasQuery) > 0:
                cityName = cityAliasQuery[0]['city']
                break
        else:
            cityQueryIndex = 0
            cityOrStateOrHotelNotSearchedExplicitly = True
            search_result.update({
                'cityOrStateOrHotelNotSearchedExplicitly': cityOrStateOrHotelNotSearchedExplicitly,
            })

        for singleQuery in reversedArray[cityQueryIndex:]:
            # Any specific reason here to use '' + '' in else part in filter?
            localityQuery = Locality.objects.filter(Q(
                name__iregex='^.*' + singleQuery.strip() if singleQuery else '')).select_related(
                'city')
            if len(localityQuery) > 0:
                locality = localityQuery[0]
                break

        if locality or cityName:
            if locality:
                isLocalitySearch = True
                city = locality.city
                parentQuery = [
                    {'name': city.name, 'lat': city.city_latitude, 'long': city.city_longitude},
                    {'name': locality.name, 'lat': locality.latitude, 'long': locality.longitude},
                ]
            else:
                isLocalitySearch = False
                city_repository = RepositoriesFactory.get_city_repository()
                city = city_repository.get_single_city(name=cityName)
                parentQuery = [{'name': cityName,
                                'lat': city.city_latitude,
                                'long': city.city_longitude}]

            cityHotels = hotel_repository.get_enabled_hotels_for_city(city_id=city.id)

            # cityHotels = Hotel.objects.filter(city=city, status=Hotel.ENABLED).prefetch_related(
            #     'rooms', 'image_set', 'facility_set', 'hotel_highlights').select_related('city',
            #                                                                              'state',
            #                                                                              'locality')

            search_result.update({
                'searchResults': cityHotels,
                'parentQuery': parentQuery,
                'isLocalitySearch': isLocalitySearch,
                'city': city,
                'locality': locality
            })
            return True
        return False

    def __has_state_based_search(self, searchArray, search_result):
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        stateQuery = State.objects.filter(
            Q(name__iregex='^.*' + searchArray[0].strip() if searchArray[0] else '' + ''))
        if len(stateQuery) > 0:
            state = stateQuery[0]
            stateHotels = hotel_repository.filter_hotels(
                status=1,
                state=state)
            # stateHotels = Hotel.objects.filter(state=state, status=Hotel.ENABLED).prefetch_related(
            #     'rooms', 'image_set', 'facility_set', 'hotel_highlights').select_related('city',
            #                                                                              'state',
            #                                                                              'locality')
            search_result.update({
                'searchResults': stateHotels,
            })
            return True
        return False
