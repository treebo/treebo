from dbcommon.models.hotel import Hotel
from dbcommon.models.room import Room

__author__ = 'snehilrastogi'

from djutil.models import TimeStampedModel
from django.db import models


class WebCatalogHotelMapping(TimeStampedModel):
    hotel = models.ForeignKey(
        Hotel,
        null=True,
        blank=True,
        on_delete=models.CASCADE)
    catalog_hotel_id = models.CharField(max_length=200, null=True, blank=True)

    def __unicode__(self):
        return str(self.hotel.id) + "-" + str(self.catalog_hotel_id)

    def __str__(self):
        return str(self.hotel.id) + "-" + str(self.catalog_hotel_id)

    class Meta:
        db_table = 'seo_webcataloghotelmapping'
        verbose_name = 'Web Hotel and Catalog Hotel Mapping'
        verbose_name_plural = 'Web Hotel and Catalog Hotel Mapping'


class CatalogRoomMapping(TimeStampedModel):
    hotel_map = models.ForeignKey(
        WebCatalogHotelMapping,
        null=True,
        blank=True,
        on_delete=models.CASCADE)
    room = models.ForeignKey(
        Room,
        null=True,
        blank=True,
        on_delete=models.CASCADE)
    catalog_room_id = models.CharField(max_length=200, null=True, blank=True)

    def __unicode__(self):
        return str(self.hotel_map.hotel.id) + "-" + \
            str(self.hotel_map.catalog_hotel_id)

    def __str__(self):
        return str(self.hotel_map.hotel.id) + "-" + \
            str(self.hotel_map.catalog_hotel_id)

    class Meta:
        db_table = 'seo_catalogroommapping'
        verbose_name = 'Catalog Room Mapping'
        verbose_name_plural = 'Catalog Room Mapping'
