import logging

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from rest_framework.response import Response
from django.db.models import F

from apps.checkout.models import BookingRequest
from apps.common.utils import Utils
from apps.fot.emailers import audit_cancellation
from apps.fot.models import FotReservationsRequest, Fot
from base import log_args
from base.middlewares.set_request_id import get_current_user, get_current_request_id
from base.views.api import TreeboAPIView
from common.constants import common_constants as constants, error_codes
from services.restclient.bookingrestclient import BookingClient

__author__ = 'varunachar'

logger = logging.getLogger(__name__)


class CancelBookingValidator(serializers.Serializer):
    order_id = serializers.CharField(required=True, error_messages={
        'required': error_codes.PROMOTIONS_NOT_PRESENT,
        'blank': error_codes.PROMOTIONS_NOT_PRESENT
    })
    email = serializers.EmailField(required=True, error_messages={
        'required': error_codes.INVALID_EMAIL_ORDER,
        'blank': error_codes.INVALID_EMAIL_ORDER
    })


class CancelBookingAPI(TreeboAPIView):
    validationSerializer = CancelBookingValidator
    authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CancelBookingAPI, self).dispatch(*args, **kwargs)

    def get(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request):
        """

        :param request:
        :return:
        """
        current_user, current_request = get_current_user(), get_current_request_id()
        order_id = request.data.get('order_id')
        email = request.data.get('email')

        try:
            booking_request = BookingRequest.objects.get(order_id=order_id)
            BookingClient.cancel_booking(booking_request.order_id, email)
        except Exception as exc:
            logger.info('in cancel booking API')
            logger.exception(exc)
            return Response(
                Utils.buildErrorResponseContext(
                    error_codes.INVALID_BOOKING))

        context = {
            'status': constants.SUCCESS,
            'message': 'Your cancellation request has been Accepted.',
        }
        return Response(context)
