# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0016_auto_20171122_1626'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='room_config',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
