from datetime import datetime


class Booking:
    id = ""
    orderId = ""
    checkinDate = ""
    checkoutDate = ""
    adultCount = ""
    childCount = ""
    hotel = None
    room = None
    totalAmount = ""
    userId = ""
    guestName = ""
    guestEmail = ""
    guestMobile = ""
    isComplete = ""
    paymentAmount = ""
    isTraveller = ""
    bookingCode = ""
    comments = ""
    couponApply = ""
    couponCode = ""
    paymentMode = ""
    discount = ""
    groupCode = ""
    status = ""
    createdAt = ""
    is_audit = False

    def __init__(self, bookingJson):
        self.transformBookingJson(bookingJson)

    def transformBookingJson(self, bookingJson):
        bookingJson = bookingJson['data']
        self.id = int(bookingJson['id'])
        self.orderId = bookingJson['order_id']
        if bookingJson.get("checkin_date", None):
            self.checkinDate = datetime.strptime(
                bookingJson['checkin_date'], '%Y-%m-%d').date()
        if bookingJson.get("checkout_date", None):
            self.checkoutDate = datetime.strptime(
                bookingJson['checkout_date'], '%Y-%m-%d').date()
        self.adultCount = bookingJson['adult_count']
        self.childCount = bookingJson['child_count']
        self.totalAmount = bookingJson['total_amount']
        self.userId = bookingJson['user_id']
        self.guestName = bookingJson['guest_name']
        self.guestEmail = bookingJson['guest_email']
        self.guestMobile = bookingJson['guest_mobile']
        self.isComplete = bookingJson['is_complete']
        self.paymentAmount = bookingJson['payment_amount']
        self.isTraveller = bookingJson['is_traveller']
        self.bookingCode = bookingJson['booking_code']
        self.comments = bookingJson['comments']
        self.couponApply = bookingJson['coupon_apply']
        self.couponCode = bookingJson['coupon_code']
        self.paymentMode = bookingJson['payment_mode']
        self.discount = bookingJson['discount']
        self.groupCode = bookingJson['group_code']
        self.status = bookingJson['status']
        self.is_audit = bookingJson['is_audit']
        if bookingJson.get("created_at", None):
            self.createdAt = datetime.strptime(bookingJson['created_at'],
                                               '%Y-%m-%dT%H:%M:%S.%f').date()
        if bookingJson.get('hotel', None):
            self.hotel = int(bookingJson['hotel'])
        if bookingJson.get('room', None):
            self.room = int(bookingJson['room'])
