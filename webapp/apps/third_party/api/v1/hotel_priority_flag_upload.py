import json
import logging
import csv

from django.db.transaction import atomic
from rest_framework.status import HTTP_401_UNAUTHORIZED, HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR, \
    HTTP_400_BAD_REQUEST

from apps.content.models import ContentStore
from base.db_decorator import db_retry_decorator
from base.views.api import TreeboAPIView
from apps.discounts.constants import UNAUTHORIZED_REQUEST_MESSAGE
from rest_framework.response import Response
from base.middlewares.request import get_domain
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render
from io import StringIO

logger = logging.getLogger(__name__)

csv_format = {
    'hotel_id': 0,
    'hotel_name': 1,
    'prf_flag': 2}


class HotelPriorityFlag(TreeboAPIView):
    def __init__(self):
        super(HotelPriorityFlag, self).__init__()

    def get(self, request):
        action_url = get_domain() + reverse('hotel_priority_flag_upload')
        context = {'action_url': action_url}
        response = render(
            request=request,
            template_name='third_party/hotel_priority_flag_file_upload.html',
            context=context)
        return HttpResponse(response)

    def post(self, request, *args, **kwargs):
        # fetching the file from the request
        csvfile = request.FILES.get('hotels_priority_flag_file', None)
        if not csvfile:
            return Response({"status": "Failed", "error_message": "No CSV file found"}, status=HTTP_400_BAD_REQUEST)

        user = request.user
        if not (user and user.is_authenticated() and user.is_staff):
            return Response({"status": "Failed", "error": UNAUTHORIZED_REQUEST_MESSAGE}, status=HTTP_401_UNAUTHORIZED)

        try:
            csvfile = StringIO(csvfile.read().decode())
            readCSV = csv.reader(csvfile, delimiter=',')

            is_first_row = True
            hotel_ids_with_priority = []
            for row in readCSV:
                # avoiding the header
                if is_first_row:
                    logger.info('Contents of Header (First row): {0}, {1}, {2}'
                                .format(str(row[0]), str(row[1]), str(row[2])))
                    is_first_row = False
                    continue

                prf_flag = str(row[csv_format.get('prf_flag')])

                if prf_flag.upper() == 'TRUE':
                    hotel_id = int(row[csv_format.get('hotel_id')])
                    hotel_ids_with_priority.append(hotel_id)

            self.save_to_content_store(hotel_ids_with_priority)
            return Response({"status": "Success"}, status=HTTP_200_OK)
        except Exception as e:
            logger.exception("Exception while setting priority flag of hotels in Content Store")
            return Response({"status": "Failed", "error_message": str(e)}, status=HTTP_500_INTERNAL_SERVER_ERROR)

    @atomic
    @db_retry_decorator()
    def save_to_content_store(self, hotel_ids_with_priority):
        try:
            content_store = ContentStore.objects.get(name='hotel_ids_with_priority')
        except ContentStore.DoesNotExist:
            content_store = ContentStore()
            content_store.name = 'hotel_ids_with_priority'

        content_store.value = json.dumps(hotel_ids_with_priority)
        content_store.save()
