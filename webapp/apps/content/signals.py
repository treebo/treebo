from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from apps.content.models import ContentStore
from apps.search.api.v2.search_api import SearchAPI
from apps.search.api.v4.search_api import SearchApiV4


@receiver([post_save, post_delete], sender=ContentStore)
def on_contentstore_updated(sender, instance, created, *args, **kwargs):
    from apps.content.api.landing_content import LandingContentAPI
    from apps.content.api.v2.content import ContentAPI
    from apps.content.api.views import GetContent
    from apps.hotels.api.v2.hotel_detail import HotelDetail
    HotelDetail.invalidate_cache(None)
    LandingContentAPI.invalidate_cache()
    ContentAPI.invalidate_cache(None)
    GetContent.invalidate_cache(None)
    SearchAPI.invalidate_cache()
    SearchApiV4.invalidate_cache()
