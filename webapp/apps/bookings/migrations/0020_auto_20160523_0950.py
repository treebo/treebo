# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0019_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bookingrequest',
            options={
                'verbose_name': 'Booking Request',
                'verbose_name_plural': 'Booking Requests'},
        ),
        migrations.AlterModelOptions(
            name='paymentorder',
            options={
                'verbose_name': 'Payment Order',
                'verbose_name_plural': 'Payment Orders'},
        ),
        migrations.AlterModelOptions(
            name='roombooking',
            options={
                'verbose_name': 'Room Booking',
                'verbose_name_plural': 'Room Bookings'},
        ),
    ]
