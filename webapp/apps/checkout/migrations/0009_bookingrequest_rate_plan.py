# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0008_auto_20170731_1124'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='rate_plan',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
