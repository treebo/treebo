import logging

from django.core.exceptions import ObjectDoesNotExist
from psycopg2 import InterfaceError, DatabaseError

from base.db_decorator import db_retry_decorator
from dbcommon.models.room import ExternalProviderRoomDetails
from data_services.web_repositories.room_web_repository import RoomWebRepository
from apps.common.slack_alert import SlackAlertService


logger = logging.getLogger('queue_listeners')
# logger.setLevel(logging.DEBUG)
# console_handler = logging.StreamHandler()
# console_handler.setLevel(logging.DEBUG)
# formatter = logging.Formatter(
#     '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# console_handler.setFormatter(formatter)
# logger.addHandler(console_handler)


class ExternalProvideRoomService:

    def __init__(self, room_dto=None, external_provider_room_details_dto=None):
        self.room_dto = room_dto
        self.external_provider_room_details_dto = external_provider_room_details_dto

    @db_retry_decorator()
    def create_or_update_external_provider_room_details(self):
        external_provider_room_details_obj = None
        acceptable_keys_list = [
            field.name for field in ExternalProviderRoomDetails._meta.get_fields()]
        try:
            external_provider_room_details_obj = ExternalProviderRoomDetails.objects.get(
                room__cs_id=self.room_dto.cs_id,
                room_code=self.external_provider_room_details_dto.room_code)

            for attribute, value in list(vars(self.external_provider_room_details_dto).items()):
                if attribute in acceptable_keys_list:
                    setattr(external_provider_room_details_obj, attribute, value)

        except ObjectDoesNotExist:
            logger.info("Creating external provider room details object")
            external_provider_room_details_obj = ExternalProviderRoomDetails.objects.create(
                room_code=self.external_provider_room_details_dto.room_code,
                rate_plan_code=self.external_provider_room_details_dto.rate_plan_code,
                rate_plan_name=self.external_provider_room_details_dto.rate_plan_name,
                name=self.external_provider_room_details_dto.name)
        except (InterfaceError, DatabaseError) as e:
            logger.error(e)
            SlackAlertService.send_cs_error_msg_to_slack(error_msg=e.__str__(),
                                                         class_name=self.__class__.__name__)
            raise e
        except Exception as e:
            logger.error(e)
            SlackAlertService.send_cs_error_msg_to_slack(error_msg=e.__str__(),
                                                         class_name=self.__class__.__name__)
            return external_provider_room_details_obj
        external_provider_room_details_obj.save()
        return external_provider_room_details_obj

    @staticmethod
    def add_external_room_details_to_room_object(room_dict, hotel_id, room_type):
        try:
            room_object = RoomWebRepository.get_external_room_details(hotel_id, room_type)
            room_dict.update({'name': room_object["external_room_details__name"].title(),
                              'description': room_object["description"]})
        except Exception as e:
            logger.error(str(e))

