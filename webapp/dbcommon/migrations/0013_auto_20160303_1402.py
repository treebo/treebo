# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0012_cityalias'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='room_area',
            field=models.PositiveIntegerField(default=200),
        ),
    ]
