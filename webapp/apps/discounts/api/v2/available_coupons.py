import logging

from rest_framework import serializers
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from apps.auth.csrf_exempt_session_authentication import CsrfExemptSessionAuthentication
from apps.checkout.models import BookingRequest
from apps.common import error_codes
from apps.common.error_codes import get as error_message
from apps.discounts.exceptions import AvailableCouponsException, InvalidCouponException
from apps.discounts.serializers import DiscountCouponSummarySerializer
from apps.discounts.services.discount_coupon_services import DiscountCouponServices, FeaturedCouponsSku
from common.services.feature_toggle_api import FeatureToggleAPI
from base.views.api import TreeboAPIView
from apps.checkout.service.flash_sale_service import FlashSaleService
from apps.common.slack_alert import SlackAlertService as slack_alert
from django.conf import settings

logger = logging.getLogger(__name__)


class AvailableCouponSerializer(serializers.Serializer):
    bid = serializers.IntegerField()


class AvailableCouponAPI(TreeboAPIView):
    validationSerializer = AvailableCouponSerializer
    authentication_classes = (
        BasicAuthentication,
        CsrfExemptSessionAuthentication)
    
    def get(self, request, *args, **kwargs):
        try:
            if not FeatureToggleAPI.is_enabled("coupon", "availablecoupon", True):
                logger.info("AvailableCoupon API not enabled")
                coupon_serializer = DiscountCouponSummarySerializer([], many=True)
                return Response(coupon_serializer.data)
            
            validated_data = self.serializerObject.validated_data
            booking_request_id = validated_data['bid']
            booking_request = BookingRequest.objects.filter(
                pk=booking_request_id).first()
            if not booking_request:
                return Response({"error": error_message(
                    error_codes.INVALID_BOOKING)}, status=400)
            user = request.user
            
            referral_click_id = str(request.COOKIES.get(
                'avclk')) if request.COOKIES.get('avclk') else None
            current_user = getattr(request, 'user', None)
            logged_in_user = True if current_user and current_user.is_authenticated() else False

            if FeatureToggleAPI.is_enabled(settings.DOMAIN, "pricing_revamp", False):
                response_data = []
                coupons = FeaturedCouponsSku().get_coupon_codes(booking_request, user)
                if coupons:
                    response_data = [dict(code=coupon.code, tagline=coupon.tagline,
                                          coupon_amount=coupon.coupon_amount,
                                          is_prepaid=coupon.is_prepaid, description=coupon.description,
                                          expiry=coupon.expiry, is_auto_applicable=coupon.is_auto_applicable)
                                     for coupon in coupons]
            else:
                response_data = []
                coupons = DiscountCouponServices.fetch_available_coupons(
                    booking_request, user)
                if coupons:
                    logger.info("Available coupon codes: %s", [c.code for c in coupons])
                    coupon_amounts = dict()
                    for c in coupons:
                        discount_percent, coupon, error_response = self.get_discount_percent(
                            c.code, booking_request, current_user, referral_click_id)
                        if discount_percent is None:
                            continue

                        coupon_amount = DiscountCouponServices.get_coupon_amount(
                            coupon,
                            booking_request,
                            discount_percent,
                            logged_in_user,
                            utm_source=booking_request.utm_source)
                        if coupon_amount:
                            coupon_amounts[c.code] = round(coupon_amount, 0)
                    is_flash_sale_on, flash_sale_discount = \
                        FlashSaleService.get_flash_discount_percentage(booking_request.created_at,
                                                                       booking_request.hotel_id)
                    tag = None
                    amount = None
                    if is_flash_sale_on and flash_sale_discount:
                        tag = 'Flat ' + str(flash_sale_discount) + '% off'
                        amount = round((float(booking_request.pretax_amount) * (flash_sale_discount / 100)), 2)

                    response_data = [dict(code=c.code, tagline=tag if tag is not None else c.tagline,
                                          coupon_amount=amount if amount is not None else coupon_amounts[c.code],
                                          is_prepaid=c.is_prepaid, description=c.terms.replace('\\n', ' '), expiry=c.expiry,
                                          is_auto_applicable=c.is_auto_applicable)
                                     for c in coupons if c.code in coupon_amounts]

            return Response(response_data)
        except AvailableCouponsException as e:
            logger.exception(e)
            return Response({"error": error_message(error_codes.ERROR_AVAIL_COUPONS)},
                            status=HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            logger.error(e)
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.GET,
                                                        dev_msg="AvailableCouponAPI",
                                                        class_name=self.__class__.__name__,
                                                        message=e.__str__())
            return Response({"error": error_message(error_codes.ERROR_AVAIL_COUPONS)},
                            status=HTTP_500_INTERNAL_SERVER_ERROR)
    
    def get_discount_percent(
        self,
        coupon_code,
        booking_request,
        current_user,
        referral_click_id=None):
        logged_user_id = str(
            current_user.id) if current_user and current_user.is_authenticated() else ""
        try:
            coupon, discount, discount_error = DiscountCouponServices.validate_coupon_and_get_discount(
                booking_request, coupon_code, referral_click_id, logged_user_id, current_user)
            
            if discount_error:
                return None, None, Response(
                    {"error": {"msg": discount_error, "code": error_codes.INVALID_COUPON}}, status=400)
        except InvalidCouponException:
            return None, None, Response({"error": error_message(
                error_codes.INVALID_COUPON_CODE)}, status=HTTP_400_BAD_REQUEST)
        
        discount_percent = (float(discount) / float(
            booking_request.pretax_amount - booking_request.member_discount)) * 100
        return discount_percent, coupon, None
