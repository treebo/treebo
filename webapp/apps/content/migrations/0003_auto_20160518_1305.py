# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('content', '0002_auto_20160518_1006'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contentstore',
            options={},
        ),
    ]
