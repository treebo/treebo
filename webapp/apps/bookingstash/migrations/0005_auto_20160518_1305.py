# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookingstash', '0004_auto_20160518_1006'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='availability',
            options={},
        ),
        migrations.AlterModelOptions(
            name='hotelroommapper',
            options={},
        ),
        migrations.AlterModelOptions(
            name='reservationbooking',
            options={},
        ),
        migrations.AlterModelOptions(
            name='reservationbookingstay',
            options={},
        ),
    ]
