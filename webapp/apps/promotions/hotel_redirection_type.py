import enum


class HotelRedirectionType(enum.Enum):
    SEARCH = 'srp'
    DETAILS = 'hd'
