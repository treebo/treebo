import json
import logging
from django.core.cache import cache
from apps.reviews.models import HotelTAOverallRatings
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Categories, CategorySeoDetails, CitySeoDetails, SeoDetails
from dbcommon.models.landmark import LandmarkSeoDetails
from dbcommon.models.location import Locality, LocalitySeoDetails
from dbcommon.models.facilities import Facility, AmenitySeoDetails
from dbcommon.models.miscellanous import Image
from services.restclient.contentservicerestclient import ContentServiceClient
from apps.content import constants
from apps.third_party.service.trip_advisor import constants as cns
from django.db.models import Q
import json, copy
from django.utils.encoding import smart_str
from django.utils.text import slugify
from django.conf import settings
from apps.posts.service import PostService

__author__ = 'ansilkareem'
logger = logging.getLogger(__name__)

class SchemaBuilder(object):

    hotel_repository = RepositoriesFactory.get_hotel_repository()
    city_repository = RepositoriesFactory.get_city_repository()
    search_landmark_repository = RepositoriesFactory.get_landmark_repository()

    def __init__(self, page_name, query=None):
        self.page_name = page_name.lower()
        if query:
            self.query = query.lower()
        else:
            self.query = None
        self.seo_schema = ContentServiceClient.getValueForKey(
            None, 'seo_schema_tags', 2)
        self.schema = None
        self.schema_list = None
        self.pages = {
            # constants.PAGES.SEARCH.lower():'self.build_other_pages_schema()',
            constants.PAGES.HD.lower(): 'build_hd_page_schema',
            constants.PAGES.CITY.lower(): 'build_city_page_schema',
            constants.PAGES.CATEGORY.lower(): 'build_category_page_schema',
            constants.PAGES.LANDMARK.lower(): 'build_landmark_page_schema',
            constants.PAGES.LOCALITY.lower(): 'build_locality_page_schema',
        }
        self.posts = None

    def build(self, ):
        try:
            if self.page_name in self.pages:
                if self.page_name in self.seo_schema:
                    self.schema = self.seo_schema[self.page_name]
                    if self.schema:
                        method_to_call = getattr(
                            self, self.pages[self.page_name])
                        method_to_call()
                    else:
                        self.schema = self.seo_schema['common']
                else:
                    self.schema = self.seo_schema['common']
            else:
                self.schema = self.seo_schema['common']
        except BaseException as e:
            logger.exception(e)
            self.schema = self.seo_schema['common']
        if type(self.schema) is not list:
            self.schema_list = [self.schema]
            self.schema = []
        elif type(self.schema) is list:
            self.schema_list = self.schema

        city_repository = RepositoriesFactory.get_city_repository()
        hotel_repository = RepositoriesFactory.get_hotel_repository()
        city_count = city_repository.get_enabled_cities_count()
        hotel_count = hotel_repository.get_total_hotel_count(
            only_active=True)
        data = {'city_count': city_count, 'hotel_count': hotel_count}
        all_schema_elements = []
        for i, item in enumerate(self.schema_list):
            all_schema_elements.append(item)

        schema_list_json = json.dumps(all_schema_elements) % data
        schema_list_dict = json.loads(schema_list_json)
        if self.posts:
            faqs = self.build_faq(self.posts)
            if faqs:
                schema_list_dict.append(faqs)
        self.schema.append(
            {"type": "application/ld+json", "innerHTML": json.dumps(schema_list_dict)})



    def build_hd_page_schema(self):
        hotel_id = int(self.query)
        hotel = self.hotel_repository.get_hotel_by_id(hotel_id=hotel_id)
        #hotel = hotel_repository.filter_hotels(id=hotel_id)[0]
        if hotel and self.schema and isinstance(self.schema, list):
            page_qa_query = smart_str(','.join(
                [hotel.name, hotel.city.slug]))
            q_and_a = PostService(self.page_name, page_qa_query, [self.page_name.title()])
            self.posts = (q_and_a.get_post_info(page=self.page_name,
                                      hotel_name=page_qa_query))
            self.schema[0]['name'] = smart_str(hotel.name)
            seo_details = SeoDetails.objects.filter(hotel=hotel.id).first()
            if seo_details and seo_details.metaContent:
                self.schema[0]['description'] = smart_str(seo_details.metaContent)
            else:
                self.schema[0]['description'] = smart_str(hotel.description)
            hotel_repository = RepositoriesFactory.get_hotel_repository()
            image_objs = hotel_repository.get_image_set_for_hotel(hotel)
            if image_objs:
                image_obj = image_objs[0]
            else:
                image_obj = None
            #image_obj = Image.objects.filter(hotel=hotel).first()
            if image_obj:
                self.schema[0]['image'] = cns.IMAGE_URL + smart_str(image_obj.url)
            else:
                del self.schema[0]['photo']
            self.schema[0]['checkinTime'] = smart_str(hotel.checkin_time.hour)+' AM'
            self.schema[0]['checkoutTime'] = smart_str(hotel.checkout_time.hour)+' PM'
            min_price = self.get_cached_data(cache, smart_str(hotel.id)+'_min_sell_price')
            max_price = self.get_cached_data(cache, 'hotel_id_'+smart_str(hotel.id)+'_max_price')
            if not min_price or not max_price:
                min_price = settings.DEFAULT_SELL_PRICE
                max_price = settings.DEFAULT_MAX_PRICE
            if min_price and max_price:
                min_price = int(min_price)
                max_price = int(max_price)
                del self.schema[0]['reduce_price']
                temp_price = {
                    'min_price': smart_str(min_price),
                    'max_price': smart_str(max_price)
                }
                self.schema[0]['priceRange'] = self.schema[0]['priceRange'] % temp_price
            else:
                del self.schema[0]['priceRange']
                del self.schema[0]['reduce_price']
            self.schema[0]['address']['addressCountry'] = 'India'
            self.schema[0]['address']['addressLocality'] = smart_str(
                hotel.locality.name)
            self.schema[0]['address']['addressRegion'] = smart_str(
                hotel.city.name)
            self.schema[0]['address']['postalCode'] = smart_str(
                hotel.locality.pincode)
            street = hotel.street
            if street:
                self.schema[0]['address']['streetAddress'] = smart_str(street)
            else:
                del self.schema[0]['address']['streetAddress']
            rating_obj = HotelTAOverallRatings.objects.filter(
                hotel_ta_mapping__hotel__id=hotel.id).first()
            if rating_obj:
                self.schema[0]['aggregateRating']['ratingValue'] = smart_str(
                    rating_obj.overall_rating)
                self.schema[0]['aggregateRating']['bestRating'] = '5'
                self.schema[0]['aggregateRating']['reviewCount'] = smart_str(
                    rating_obj.num_reviews_count)
            else:
                del self.schema[0]['aggregateRating']
            self.schema[0]['geo']['latitude'] = smart_str(hotel.latitude)
            self.schema[0]['geo']['longitude'] = smart_str(hotel.longitude)
            self.schema[0]['hasMap'] = constants.GOOGLEMAP_URL % (
                smart_str(hotel.latitude), smart_str(hotel.longitude))
            item_list_elements = self.schema[2]['itemListElement']
            base_url = item_list_elements[0]['item']['@id']
            item = item_list_elements[1]['item']
            city_name = slugify(
                hotel.city.slug if hotel.city.slug else hotel.city.name)
            item['@id'] = base_url + "hotels-in-" + slugify(city_name) + "/"
            item['name'] = "Hotels in " + slugify(city_name)
        else:
            self.schema = self.seo_schema['common']

    def build_city_page_schema(self):
        city_search_args = []
        city_search_args.append({'name': 'name',
                                 'value': self.query,
                                 'case_sensitive': False,
                                 'partial': False})
        city_search_args.append({'name': 'slug',
                                 'value': self.query,
                                 'case_sensitive': False,
                                 'partial': False})
        current_cities = self.city_repository.filter_cities_by_string_matching(
            city_search_args, {'status': 1})
        if current_cities:
            city_obj = current_cities[0]
        else:
            city_obj = None
        city_seo = CitySeoDetails.objects.filter(city=city_obj).first()
        if city_obj and self.schema and isinstance(self.schema, list):

            # Organization
            # Website
            # BreadCrumb
            city_name = smart_str(
                city_obj.slug if city_obj.slug else city_obj.name)
            self.schema[0]['name'] = 'Treebo Hotels in ' + city_name.title()
            if city_seo:
                self.schema[0]['description'] = smart_str(
                    city_seo.citySeoDescriptionContent)
            elif city_obj.description:
                self.schema[0]['description'] = smart_str(city_obj.description)
            else:
                del self.schema[0]['description']
            self.schema[0]['url'] = 'https://www.treebo.com/hotels-in-' + \
                                    slugify(
                                        city_obj.slug if city_obj.slug else city_obj.name) + '/'
            item_list_elements = self.schema[2]['itemListElement']
            base_url = item_list_elements[0]['item']['@id']
            item = item_list_elements[1]['item']
            item['@id'] = base_url + "hotels-in-" + city_name + "/"
            item['name'] = "Hotels in " + city_name
        else:
            self.schema = self.seo_schema['common']

    def build_category_page_schema(self):
        city_name = smart_str(self.query.split('-')[-1].strip('-').lower())
        category_name = ' '.join(self.query.split('-')[:-1]).strip().lower()
        category_obj = Categories.objects.filter(
            status=1, name__iexact=category_name).first()
        if category_obj:
            cat_name = smart_str(category_obj.name)
            self.schema['name'] = cat_name.title() + ' in ' + city_name.title()
            cat_seo = CategorySeoDetails.objects.filter(category=category_obj,
                                                        city__name__iexact=city_name).first()
            if cat_seo and cat_seo.category_seo_description:
                self.schema['description'] = smart_str(
                    cat_seo.category_seo_description)
            else:
                footer_data = ContentServiceClient.getValueForKey(
                    None, 'footer_data', 1)
                if footer_data and 'category_description' in footer_data and footer_data[
                    'category_description']:
                    temp_dict = {
                        "category_name": category_obj.name,
                        "city_name": city_name
                    }
                    self.schema['description'] = footer_data['category_description'] % temp_dict
                elif category_obj.description:
                    self.schema['description'] = smart_str(
                        category_obj.description)
                else:
                    del self.schema['description']
            self.schema['url'] = 'https://www.treebo.com/' + \
                                 slugify(category_obj.name) + '-in-' + city_name + '/'
        else:
            self.schema = self.seo_schema['common']

    def build_landmark_page_schema(self):
        landmarks = self.search_landmark_repository.get_landmarks_by_freetext_url(
            query_string=' '.join(self.query.split('-')))
        if landmarks:
            landmark_obj = landmarks[0]
        else:
            landmark_obj = None
        if not landmark_obj:
            city = self.query.split('-')[-1]
            landmark_name = ' '.join(self.query.split('-')[:-1])
            landmark_obj = self.search_landmark_repository.get_landmark_for_seo_url_and_city_name(
                city_name=city, landmark_name=landmark_name)
        if landmark_obj:
            landmark_name = smart_str(
                landmark_obj.free_text_url if landmark_obj.free_text_url else landmark_obj.seo_url_name)
            city_name = smart_str(
                landmark_obj.city.slug if landmark_obj.city.slug else landmark_obj.city.name)
            self.schema['name'] = 'Treebo Hotels near ' + \
                                  landmark_name.title() + ' ' + city_name.title()
            land_seo = LandmarkSeoDetails.objects.filter(
                landmark=landmark_obj).first()
            if land_seo and land_seo.landmark_seo_description:
                self.schema['description'] = smart_str(
                    land_seo.landmark_seo_description)
            else:
                footer_data = ContentServiceClient.getValueForKey(
                    None, 'footer_data', 1)
                if footer_data and 'landmark_description' in footer_data and footer_data[
                    'landmark_description']:
                    temp_dict = {
                        "landmark_name": landmark_obj.free_text_url if landmark_obj.free_text_url else landmark_obj.seo_url_name,
                        "city_name": landmark_obj.city.slug if landmark_obj.city.slug else landmark_obj.city.name}
                    self.schema['description'] = footer_data['landmark_description'] % temp_dict
                else:
                    del footer_data['description']
            self.schema['url'] = 'https://www.treebo.com/' + 'hotels-near-' + slugify(
                landmark_obj.free_text_url if landmark_obj.free_text_url else landmark_obj.seo_url_name) + '-' + slugify(
                landmark_obj.city.slug if landmark_obj.city.slug else landmark_obj.city.name) + '/'
        else:
            self.schema = self.seo_schema['common']

    def build_locality_page_schema(self):
        city = str(self.query.split('-')[-1])
        locality_name = ' '.join(self.query.split('-')[:-1])
        locality_obj = Locality.objects.filter(Q(name__iexact=locality_name) & Q(
            Q(city__name__iexact=city) | Q(city__slug__iexact=city))).first()
        if locality_obj:
            locality_name = smart_str(locality_obj.name)
            self.schema['name'] = 'Treebo Hotels in ' + \
                                  locality_name.title() + ' ' + city.title()
            loc_seo = LocalitySeoDetails.objects.filter(
                locality=locality_obj).first()
            if loc_seo and loc_seo.locality_seo_description:
                self.schema['description'] = smart_str(
                    loc_seo.locality_seo_description)
            else:
                footer_data = ContentServiceClient.getValueForKey(
                    None, 'footer_data', 1)
                if footer_data and 'locality_description' in footer_data and footer_data[
                    'locality_description']:
                    temp_dict = {
                        "locality_name": locality_obj.name,
                        "city_name": locality_obj.city.slug if locality_obj.city.slug else locality_obj.city.name}
                    self.schema['description'] = footer_data['locality_description'] % temp_dict
                else:
                    del self.schema['description']
            self.schema['url'] = 'https://www.treebo.com/hotels-in-' + slugify(
                locality_obj.name) + '-' + slugify(
                locality_obj.city.slug if locality_obj.city.slug else locality_obj.city.name) + '/'
        else:
            self.schema = self.seo_schema['common']

    def get_cached_data(self, cache, key):
        try:
            value = cache.get(key)
        except Exception as e:
            value = None
        return value

    def build_faq(self, posts=None):
        sample_entity = {
            "@context": "https://schema.org",
            "@type": "FAQPage",
            "mainEntity": [{
                "@type": "Question",
                "name": "",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": ""
                }
            }]
        }
        sample_entity_dict = (json.loads(json.dumps(sample_entity)))

        if posts and posts.get('posts'):
            qa_entity = sample_entity_dict['mainEntity'][0]
            main_entities = []
            contents = posts['posts'].get('content')
            if not contents:
                return None
            for content in contents[:5]:
                qa_entity_copy = copy.deepcopy(qa_entity)
                qa_entity_copy['name'] = content['question']
                if not content.get('answers'):
                    continue
                qa_entity_copy['acceptedAnswer']['text'] = content['answers'][0]['text']
                main_entities.append(qa_entity_copy)
        else:
            return None

        sample_entity_dict['mainEntity']  = main_entities
        return sample_entity_dict
