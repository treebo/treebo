# -*- coding:utf-8 -*-
"""
    UpdatePaymentInfo  API
"""

from decimal import Decimal

from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status

from apps.bookings.models import Booking, PaymentOrder
from apps.bookings.services.booking import BookingService
from base.logging_decorator import log_args
from base.views.api import TreeboAPIView
from common.custom_logger.booking.log import get_booking_logger

logger = get_booking_logger(__name__)

__author__ = 'varunachar'


class UpdatePaymentInfo(TreeboAPIView):
    __booking_service = BookingService()
    authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdatePaymentInfo, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        data = request.data
        if "order_id" in data and "amount" in data and "payment_gateway_id" in data and "payment_mode" in data:
            order_id = str(data["order_id"])
            amount = Decimal(data["amount"])
            payment_gateway_id = str(data["payment_gateway_id"])
            payment_mode = str(data["payment_mode"])
            payment_order = PaymentOrder.objects.create(
                order_id=payment_gateway_id,
                gateway_type=payment_mode,
                status=PaymentOrder.CAPTURED,
                amount=Decimal(amount))
            payment_order.booking_order_id = order_id
            payment_order.updated_in_booking_service = True
            booking = Booking.objects.get(order_id=order_id)
            booking.payment_amount += Decimal(amount)
            booking.save()
            payment_order.save()
            return JsonResponse({
                "status": "success",
                "msg": "Payment info updated"
            })
        return JsonResponse({
            "status": "failed",
            "msg": "Missing order_id or amount or payment_id"
        }, status=status.HTTP_400_BAD_REQUEST)
