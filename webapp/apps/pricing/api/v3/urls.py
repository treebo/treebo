

from django.conf.urls import url

from apps.pricing.api.v3.cheapest_room_and_price_for_all_hotels import PricingAPIView
from apps.pricing.api.v3.pricing import PricingDetails

app_name = 'pricing_v3'
urlpatterns = [url(r'^hotels/$',
                   PricingAPIView.as_view(),
                   name='cheapest_prices_for_hotels_v3'),
               url(r'^pricingdetails/break-up$',
                   PricingDetails.as_view(),
                   name='pricing_breakup_v3'),
               ]
