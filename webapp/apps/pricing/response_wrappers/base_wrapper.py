from django.conf import settings
from apps.reward.service.reward import RewardService
import copy

class BaseWrapper:
    reward_service = RewardService()

    def __init__(self, user, apply_wallet, application_code):
        self.user = user
        self.apply_wallet = apply_wallet
        self.application_code = application_code

    def get_default_wallet_info(self):
        default_wallet_info = {
            'wallet_type': settings.WALLET_TYPE,
            'wallet_applied': False,
            'total_wallet_balance': 0,
            'total_wallet_points': 0,
            'wallet_deductable_amount': 0,
            'user': '',
            'debit_mode': '',
            'loyalty_type': ''
        }

        return default_wallet_info

    def get_wallet_balance(self):
        wallet_balance = {}
        if self.user and self.user.is_authenticated():
            wallet_applied, total_wallet_balance, total_wallet_points, \
                total_wallet_spendable_amount, debit_sources, debit_percentage, debit_mode, loyalty_type = \
                self.reward_service.get_wallet_balance(self.user, self.application_code)

            wallet_balance = {
                'wallet_applied': wallet_applied,
                'total_wallet_balance': total_wallet_balance,
                'total_wallet_points': total_wallet_points,
                'total_wallet_spendable_amount': total_wallet_spendable_amount,
                'debit_percentage': debit_percentage,
                'debit_sources': debit_sources,
                'debit_mode': debit_mode,
                'loyalty_type': loyalty_type
            }

        return wallet_balance

    def get_user_wallet_info(self, wallet_balance, sell_price):
        wallet_info = self.get_default_wallet_info()
        if wallet_balance:
            wallet_applied = wallet_balance['wallet_applied']
            total_wallet_balance = wallet_balance['total_wallet_balance']
            total_wallet_points = wallet_balance['total_wallet_points']
            total_wallet_spendable_amount = wallet_balance['total_wallet_spendable_amount']

            wallet_deductable_amount, wallet_deduction = self.reward_service.get_wallet_breakup(
                self.apply_wallet, total_wallet_spendable_amount, sell_price)
            wallet_info['wallet_type'] = settings.WALLET_TYPE
            wallet_info['wallet_applied'] = self.apply_wallet and wallet_applied
            wallet_info['total_wallet_balance'] = total_wallet_balance
            wallet_info['total_wallet_points'] = total_wallet_points
            wallet_info['wallet_deductable_amount'] = wallet_deductable_amount
            wallet_info['debit_percentage'] = wallet_balance['debit_percentage']
            wallet_info['debit_sources'] = wallet_balance['debit_sources']
            wallet_info['debit_mode'] = wallet_balance['debit_mode']
            wallet_info['loyalty_type'] = wallet_balance['loyalty_type']
            wallet_info['user'] = self.user.id if self.user and self.user.is_authenticated() else ''
            wallet_info['creditable_loyalty_points'] = sell_price * 0.05
        return wallet_info

    def get_applied_coupon_info(self, discounts):
        coupon_info = {
            'coupon_code': None,
            'discount_id': None,
            'discount_applied': 0,
            'discount_applied_percent': 0,
            'discount_post_conditions': []
        }
        for discount in discounts:
            if discount['type'].lower() == 'explicit':
                coupon_info['discount_id'] = discount['discount_id']
                coupon_info['coupon_code'] = discount['code']
                coupon_info['discount_applied'] = round(discount['discount_applied'], 2)
                coupon_info['discount_applied_percent'] = 0
                coupon_info['discount_post_conditions'] = discount['post_conditions']
                break

        return coupon_info

    @staticmethod
    def get_web_rate_plan(rate_plan):
        if not rate_plan:
            return rate_plan
        web_rate_plan = copy.copy(rate_plan)
        if rate_plan['policy_name'] == "rp":
            web_rate_plan['policy_name'] = "EP"

        if rate_plan['policy_name'] == "nrp":
            web_rate_plan['policy_name'] = "NRP"

        return web_rate_plan

    def throttled_treebo_discount_points(self, wallet_amount, pretax_price, debit_percentage=0):
        return round(min((debit_percentage/100) * pretax_price, wallet_amount))
