import datetime
import decimal
import json
import logging

logger = logging.getLogger(__name__)


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.isoformat()
        return json.JSONEncoder.default(self, o)


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)


class LogstashJsonEncoder(json.JSONEncoder):
    def __init__(self, skipkeys=False, ensure_ascii=True,
                 check_circular=True, allow_nan=True, sort_keys=False,
                 indent=None, separators=None, encoding='utf-8', default=None):
        super(
            LogstashJsonEncoder,
            self).__init__(
            skipkeys=True,
            ensure_ascii=ensure_ascii,
            check_circular=check_circular,
            allow_nan=allow_nan,
            sort_keys=sort_keys,
            indent=indent,
            separators=separators,
            default=default)


class CustomBytesEncoder(json.JSONEncoder):
    def default(self, o):
        import base64
        if isinstance(o, bytes):
            decoded_str = o.decode('utf-8')
            return decoded_str
        return super(CustomBytesEncoder, self).default(o)
