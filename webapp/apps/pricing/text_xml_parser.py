import logging

from rest_framework.parsers import BaseParser

logger = logging.getLogger(__name__)


class XMLParser(BaseParser):
    """
    XML parser.
    """
    media_type = 'text/xml'

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Simply return a string representing the body of the request.
        """
        data_xml = stream.read()
        logger.info(data_xml)
        return data_xml
