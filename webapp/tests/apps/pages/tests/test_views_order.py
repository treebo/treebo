from decimal import Decimal

import mock
from django.test import TestCase, RequestFactory

from apps.pages.views import order


class OrderViewTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.mock_booking_service = mock.Mock()

    @mock.patch.object(order.Order, '_Order__collate_common_data')
    def test_collate_mobile_data(self, mock_collate_common_data):
        mock_collate_common_data.return_value = mock.Mock()
        request = self.factory.get('/test_path')
        order_view = order.Order()
        mock_all_bookings, mock_booking, mock_checkin, mock_checkout, mock_grand_total = [
            mock.Mock() for x in range(5)]
        mobile_data = order_view._Order__collate_mobile_data(
            mock_booking, mock_checkin, mock_checkout, mock_grand_total, request)
        self.assertEqual(mobile_data, mock_collate_common_data.return_value)
        mock_collate_common_data.assert_called_once_with(
            mock_booking, mock_checkin, mock_checkout, mock_grand_total, request)

    @mock.patch.object(order.PageHelper, 'getRoomConfigData')
    @mock.patch.object(order.Order, '_Order__collate_common_data')
    def test_collate_desktop_data(
            self,
            mock_collate_common_data,
            mock_get_room_config):
        mock_common_data = [mock.Mock() for x in range(12)]
        mock_collate_common_data.return_value = mock_common_data
        mock_get_room_config.return_value = mock.Mock()

        request = self.factory.get('/test_path')
        mock_pretax_price, mock_total_tax = mock.Mock(), mock.Mock()
        order_view = order.Order()
        mock_all_bookings, mock_checkin, mock_checkout, mock_grand_total = [
            mock.Mock() for x in range(4)]
        mock_booking = mock.Mock(**{'discount': Decimal('10.0'),
                                    'pretax_amount': Decimal('230.35'),
                                    'tax_amount': Decimal('30.34'),
                                    'couponCode': 'TEST_COUPON'})

        desktop_data = order_view._Order__collateDesktopData(
            mock_booking, mock_checkin, mock_checkout, mock_grand_total, request)
        mock_collate_common_data.assert_called_once_with(
            mock_booking, mock_checkin, mock_checkout, mock_grand_total, request)
        self.assertEqual(mock_get_room_config.call_count, 0)

    @mock.patch.object(order.PageHelper, 'parseRoomConfig')
    @mock.patch('apps.common.utils.Utils')
    @mock.patch('apps.pages.views.order.Decimal')
    @mock.patch('apps.common.date_time_utils.build_date')
    @mock.patch.object(order.Order, '_Order__getBookingCode')
    def test_collate_common_data(
            self,
            mock_get_booking_code,
            mock_build_date,
            mock_decimal,
            mock_common_util,
            mock_parse_room_config):
        # setup mock method call behaviour
        mock_get_booking_code.return_value = 'TESTCODE'
        mock_dates = [mock.Mock(), mock.Mock(), mock.Mock()]
        mock_build_date.side_effect = mock_dates
        mock_decimal.return_value.quantize.return_value = Decimal('100')

        image_sizes = [[mock.Mock(), mock.Mock()] for x in range(2)]
        mock_common_util.get_image_size.side_effect = image_sizes
        image_params = [mock.Mock(), mock.Mock()]
        mock_common_util.build_image_params.side_effect = image_params
        mock_parse_room_config.return_value = [mock.Mock() for x in range(3)]

        # create request and view object
        request = self.factory.get('/test_path')
        request.flavour = mock.Mock()
        order_view = order.Order()

        mock_checkin, mock_checkout = [mock.Mock() for x in range(2)]
        mock_grand_total = 100.0
        mock_all_bookings = [mock.Mock() for x in range(3)]
        mock_booking = mock.Mock(**{'discount': Decimal('10.0'),
                                    'coupon_code': 'TEST_COUPON',
                                    'guest_mobile': '91239139233',
                                    'created_at': mock.Mock(),
                                    'room_config': '1-0,2-0'})

        # call view method
        common_data = order_view._Order__collate_common_data(
            mock_booking, mock_checkin, mock_checkout, mock_grand_total, request)
        # Verify result
        self.assertEqual(common_data[0], 'TESTCODE')
        self.assertEqual(common_data[1], mock_dates[2])
        self.assertEqual(common_data[2], Decimal('100'))
        # imageParams
        self.assertEqual(common_data[3], image_params[0])
        # sharedImageParams
        self.assertEqual(common_data[7], image_params[1])

        # Verify mock method calls
        expected_common_util_call = [
            mock.call.get_image_size(
                mock.ANY,
                'order'),
            mock.call.build_image_params(
                image_sizes[0][0],
                image_sizes[0][1],
                mock.ANY,
                mock.ANY,
                mock.ANY),
            mock.call.get_image_size(
                mock.ANY,
                'share'),
            mock.call.build_image_params(
                image_sizes[1][0],
                image_sizes[1][1],
                mock.ANY,
                mock.ANY,
                mock.ANY),
        ]
        self.assertEqual(
            mock_common_util.mock_calls,
            expected_common_util_call)

    def test_populate_common_context(self):
        pass
