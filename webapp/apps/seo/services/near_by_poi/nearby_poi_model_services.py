from dbcommon.models.hotel import Hotel
from apps.seo.models.nearby_poi import POICategory, POI
from apps.seo.models.prowl_hotel_positive_models import FeatureToggle


class NearbyPOIModelServices():

    def fetch_hotel_by_hotel_id(self, hotel_id):
        return Hotel.all_hotel_objects.filter(id=hotel_id).first()

    def fetch_all_poi(self):
        return POI.objects.all()

    def fetch_poi_for_city(self, city_id):
        return POI.objects.filter(city=city_id)

    def fetch_all_poi_categories(self):
        return POICategory.objects.all()

    def get_distance_cap_for_category(self, category_id):
        distance_cap = FeatureToggle.objects.filter(pk=1).first()
        if category_id == 1:
            return distance_cap.closest_landmark_dist_cap
        else:
            return distance_cap.restaurant_dist_cap

    def get_number_cap(self, category_id):
        constants = FeatureToggle.objects.filter(pk=1).first()
        if category_id == 1:
            return constants.closest_landmark_number_cap
        elif category_id == 2:
            return constants.most_popular_landmark_number_cap
        elif category_id == 4:
            return constants.airport_number_cap
        else:
            return constants.restaurants_number_cap
