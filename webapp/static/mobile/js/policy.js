'use strict';

import BaseView from './treebolibs/baseview';
import logger from './treebolibs/logger';
import '../css/policy.less';

let PolicyView = Object.create(BaseView);

PolicyView.pageLoaded = () => {
    logger.log(" Policy page loaded");
}

let policyConfig = { name:'', widgets:[]};
let policyView = PolicyView.setup(policyConfig);
