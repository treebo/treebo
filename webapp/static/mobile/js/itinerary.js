'use strict';

import $ from 'jquery';
import {_} from 'underscore';
import BaseView from './treebolibs/baseview';
import queryString from 'query-string';
import searchWidget from './widgets/search';
import 'parsleyjs';

import React from 'react';
import {render} from 'react-dom';
// import { createStore, combineReducers } from 'redux';
import Discount from './widgets/discount';
import PriceDetail from './widgets/pricedetails';
import '../css/itinerary.less';
import cookies from 'js-cookie';

let treejax = require('./treebolibs/treejax');
let auth = require('./auth');

let $loader = $("#commonLoader");
let ItineraryView = Object.create(BaseView);
let $csrfMiddlewareToken = $('input[name="csrfmiddlewaretoken"]').val();
let bookingData = $("#hiddenData").val().split("||");
let parsed = queryString.parse(location.search);
let [hotelId, rooms, roomType, guest, checkin, checkout, maxpax, totalNights] = bookingData;
let total = rt.total;

let currentStep = 1;
let openid = 'email';


class VerifyOTP extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			otp: '',
			error: '',
		};
	}

	componentDidMount() {
		$('#otpForm').parsley().on('form:submit', (e) => {
			e.submitEvent.preventDefault();
			this.props.onVerifyOTP(this.state.otp);
			return false;
		});
	}

	handleFieldChange = (e) => {
		this.props.onOtpError('');
		this.setState({
			[e.target.name]: e.target.value,
			error: '',
		});
	}

	render() {
		const { mobile, onEditMobile, onSendOTP, otperror} = this.props;
		return (
			<div className="popup">
				<div className="modal pos-abs">
					<div className="modal__body">
						<p className="popup__title">Verify your Mobile Number</p>
						<p className="popup__subtitle">A One Time Password (OTP) has been sent via SMS to <span className="popup__mobile">{mobile}</span> <small onClick={onEditMobile} className="popup__edit">Change Number</small></p>
						<form id="otpForm">
							<div className="float-labels">
								<small className="float-labels__input-link" onClick={onSendOTP}>Resend Sms</small>
								<input onChange={this.handleFieldChange} name="otp" className="float-labels__input" type="number" value={this.state.otp || ''} placeholder="One Time Passcode" pattern=".+" required="true" data-parsley-type="number" data-parsly-trigger="change" data-parsley-type-message="Enter 6 digit code" data-parsley-required="true" data-parsley-required-message="Please enter OTP here" />
								<label className="float-labels__label">One Time Passcode</label>
							</div>
							<div className="popup__error-msg">{otperror}</div>
							<div className="text-center">
								<button type="submit" className="btn popup__submit">VERIFY</button>
								<button type="button" onClick={this.props.onPayNow} className="btn popup__paynow-btn">SKIP, I WILL PAY NOW</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

class GetMobile extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			mobile: this.props.mobile || '',
		};
	}

	componentDidMount() {
		$('#mobileForm').parsley().on('form:submit', (e) => {
			e.submitEvent.preventDefault();
			this.props.onSendOTP(this.state.mobile);
			return false;
		});
	}

	handleFieldChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	}

	render() {
		return (
			<div className="popup">
				<div className="modal pos-abs">
					<div className="modal__body">
						<p className="popup__title">Enter Mobile Number</p>
						<p className="popup__subtitle">A One Time Password (OTP) will be  sent via SMS to verify your number.</p>
							<form id="mobileForm">
								<div className="float-labels popup__mobileform">
									<input onChange={this.handleFieldChange} name="mobile" className="float-labels__input"
									pattern=".+" type="number" value={this.state.mobile || ''} placeholder="Mobile Number"
									required="true" data-parsley-type="number" data-parsley-trigger="change" data-parsley-type-message="Enter valid mobile number" data-parsley-required-message="Please enter valid mobile number"
									data-parsley-length-message="Please enter 10 digit mobile number" data-parsley-minlength="10" data-parsley-maxlength="10" />
									<label className="float-labels__label">Mobile Number</label>
								</div>
								<div className="text-center">
									<button type="submit" className="btn popup__submit popup__send-otp">SEND OTP</button>
									<button type="button" onClick={this.props.onPayNow} className="btn popup__paynow-btn">SKIP, I WILL PAY NOW</button>
								</div>
							</form>
					</div>
				</div>
			</div>
		);
	}
}

class Otp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: this.props.mobile || '',
			email:this.props.email || '',
      showOTP: true,
      showMobile: false,
			otperror: '',
    };
  }

  onSendOTP = (mobile) => {
		$loader.removeClass('hide');
		mobile = typeof mobile === 'string' ? mobile : this.state.mobile;
    treejax({
      url: '/rest/v1/otp/',
      type: 'POST',
      data: {
        mobile,
        csrfmiddlewaretoken: cookies.get('csrftoken'),
      },
    }).then((res) => {
			this.setState({
				showOTP: true,
				showMobile: false,
				mobile: mobile,
			});
    }).catch((err) => {
      throw err;
    }).finally(() => {
      $loader.addClass('hide');
    });
	}

	onEditMobile = () => {
		this.setState({
			showOTP: false,
			showMobile: true,
		});
	}

	onPayNow = () => {
		this.setState({
			showOTP: false,
			showMobile: false,
		});
		$("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="0"/>');
		methods.payuserlogin('PayNow');
	}

	onVerifyOTP = (otp) => {
		$loader.removeClass('hide');
		treejax({
				url: '/rest/v1/otp/verify/',
				type: 'POST',
				data: {
						otp,
						mobile: this.state.mobile,
						csrfmiddlewaretoken: cookies.get('csrftoken'),
				}
		}).then((res) => {
			this.setState({
				showOTP: false,
				showMobile: false,
			});
			sendAnalytics(5, {
					email: this.state.email,
					mobile: this.state.mobile,
			});
			$("#final_hidden_otp_verified_number").val(this.state.mobile);
			$("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="1"/>');
			methods.payuserlogin('PAH');
		}).catch((err) => {
			$loader.addClass('hide');
			this.onOtpError("Please enter correct OTP.");
		}).finally(() => {
		});
	}

	onOtpError = (error) => {
		this.setState({
			otperror:error,
		});
	}

  render() {
    return (
      <div>
        {this.state.showMobile ? <GetMobile mobile={this.state.mobile} onSendOTP={this.onSendOTP} onPayNow={this.onPayNow}/> : null}
        {this.state.showOTP ? <VerifyOTP mobile={this.state.mobile} onEditMobile={this.onEditMobile} onVerifyOTP={this.onVerifyOTP} onSendOTP={this.onSendOTP} otperror={this.state.otperror} onOtpError={this.onOtpError} onPayNow={this.onPayNow}/> : null}
      </div>
    )
  }
}


let methods = {
    init : ()=>{
        $('#payatHotel').on('click', e => {
						var name = $('#guestName').val();
            var mobile = $('#guestMobile').val();
						var email = $('#guestEmail').val();
						const payParsley = $('#travellerValidations').parsley();
						payParsley.validate();
						if(payParsley.isValid()){
							methods.checkOtpVerified(mobile, email, name);
						}
        });

        $('#payNow').on('click', e => {
          var totalAmount = document.getElementById('grandTotal').innerHTML;
          if (totalAmount == 0) {
            $("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="1"/>');
            methods.payuserlogin('PAH');
          } else {
            $("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="0"/>');
            methods.payuserlogin('PayNow');
          }
        });


        $("#loginButton").on('click', ()=> {
            let $form = $('#loginForm');
            let email = $('#loginEmailInput').val().trim();
            let password = $('#loginPassowrdlInput').val().trim();

            const loginParsley = $form.parsley();
            loginParsley.on('form:submit', () => {
                let p = auth.login(email, password);
                p.then((data) => {
                    methods.postLoginUpdate(data);
                }, (treeboError) => {
                    $('.login__error').html(treeboError.message).show();
                });
                return false;
            });
        });
        $("#fbLogin").on('click', ()=> {
            openid = 'fb';
            auth
              .fbLogin()
              .then(methods.postLoginUpdate)
              .catch(() => location.reload());
        });

        auth
            .getGoogleHandler()
            .then((auth2) => {
                auth2.attachClickHandler('googleLogin', {}, methods.onSignIn, methods.onSignInFailure);
                auth2.attachClickHandler('googleSignup', {}, methods.onSignIn, methods.onSignInFailure);
            });

        $(".js-auth-toggle__link").on('click', (e)=> {
            e.preventDefault();
            $('.js-auth-toggle').toggleClass('hide');
        });


        $("#signupButton").on('click', (e)=>{
            const $form = $('#signupForm');
            const data = {};
            $form.serializeArray().forEach((x) => {
                data[x.name] = x.value.trim();
            });

            $form.parsley().on('form:submit', () => {
                e.preventDefault();
                let p = auth.signup(data, treeboError => {
                    $('.signup__error').html(treeboError.message).show();
                });
                p.then(()=>{
                    location.reload();
                })
                return false;
            });
        })

    },
    checkOtpVerified(mobile, email, name) {

			var otpEnabled = rt['isOtpEnabled'];
			if (otpEnabled) {
	      $loader.removeClass('hide');
	      var p = treejax({
	              url: '/rest/v1/otp/isverified/',
	              type: 'POST',
	              data: {
	                mobile,
									name,
	                csrfmiddlewaretoken: cookies.get('csrftoken'),
	              },
	          }).then(function (data, status, xhr) {
								if (!data.isOtpVerified) {
									$loader.addClass('hide');
									render(
											<Otp mobile={mobile} email={email} name={name}/>, document.getElementById('Otp')
									);
								} else {
									$("#final_hidden_otp_verified_number").val(mobile);
									$("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="1"/>');
									methods.payuserlogin('PAH');
								}
	          },function (treeboError) {
	              $loader.addClass('hide')
	          });
				} else {
					$("#hiddenrow").append('<input type="hidden" class="pay-at-hotel-hidden" name="pay_at_hotel" value="1"/>');
					methods.payuserlogin('PAH');
				}
    },
    onSignIn(googleUser) {
        openid = 'google';
        auth
            .sendGoogleRegistrationDataToServer(googleUser.getAuthResponse().id_token)
            .then(data => {
                methods.postLoginUpdate(data);
            })
            .finally(()=>{
                $loader.addClass("hide");
            })
    },
    onSignInFailure() {
        // Handle sign-in errors
    },
    postLoginUpdate : (data)=>{
        let {name, email, phone} = data;
        $('#guestName').val(name);
        $('#guestEmail').val(email);
        $('#guestMobile').val(phone);
        currentStep = currentStep+1;
        sendAnalytics(2, {
            type: 'LOGGEDIN',
            email,
            mobile: phone,
            openid,
        });
        toggleActiveItem('next');
        toggleSteps();
        $('.guest-login__name').html(name);
        $('.pre-post-login').toggleClass('hide');
    },
    initDiscount : ()=> {
        let data = {
            'query_param': {
                'couponcode':parsed.couponcode,
                'bid': parsed.bid,
            },
            'couponcode':parsed.couponcode,
            'url': rt.discount_url + "?",
            visible: true
        };
        console.log("InitDiscount Called with data: " + JSON.stringify(data));
        render(<Discount data={data}/>, document.getElementById('discountCouponContainer'));
    },

    updateDiscount : (discount)=>{
        console.log(`Discount in updateDiscount: ${JSON.stringify(discount)}`);
        if(discount.code){
            $('#grandTotal').text(discount.priceobj.final_price);
            $('#final_hidden_coupon_code').val(discount.code);
            if(discount.voucher_value) {
              $("#discountValue").text(discount.voucher_value);
            } else {
              $("#discountValue").text(discount.value);
            }
            $('#final_hidden_discount_value').val(discount.value);
        } else { // Removed the discount
            $('#grandTotal').text(discount.priceobj.final_price);
            $('#final_hidden_coupon_code').val('');
            if(discount.voucher_value) {
              $("#discountValue").text(discount.voucher_value || 0);
            } else {
              $("#discountValue").text(discount.value || 0);
            }
            $('#final_hidden_discount_value').val(discount.value || 0);
        }
        if(discount.priceobj) {
            $('#totalTax').text(discount.priceobj.tax);
            $('#pretaxPrice').text(discount.priceobj.price);
        }
    },

    payuserlogin: (idClicked) => {
        const payParsley = $('#travellerValidations').parsley();
        payParsley.validate();
        if(payParsley.isValid()){
            $loader.removeClass('hide');
            let booking_request_id = $("#booking_request_id").val();
            var url = '/api/v1/checkout/availability/?';
            var data = {
                'bid': booking_request_id,
            };

            const email = $('#guestEmail').val();
            const checkin_time = $('#checkInTime').val();
            const checkout_time = $('#checkOutTime').val();

            var query_param = $.param(data);
            var p = treejax({
                    url: url + query_param,
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRFToken", $csrfMiddlewareToken);
                    }
                }).then(function (data, status, xhr) {
                    $loader.addClass('hide');
                    sendAnalytics(3, { email });
                    sendAnalytics(4, { email, payment_type: idClicked, checkin_time, checkout_time });
                    if (data["available"]) {
                        $("#travellerValidations").submit();
                    } else {
                        $("#soldOutPopup").removeClass("hide");
                        return false;
                    }
                },function (treeboError) {
                    $loader.addClass('hide')
                    $("#soldOutPopup").removeClass("hide");
                    console.log(treeboError);
                    return false;
                });
        } else {
            return false;
        }
    },

    specialRequestChange() {
        $('.itinerary-page__guests__content__modal__button').click(() => {
            $('.special-requests__default').addClass('hide');
            methods.updateSpecialRequest();
        });
    },

    updateSpecialRequest() {
        const $special = $('.special-requests__content');
        const inTime = $('#checkInTime').val();
        const outTime = $('#checkOutTime').val();
        const guestRequest = $('#guestRequest').val();

        $special.find('.special-requests__checkin').html(inTime);
        $special.find('.special-requests__checkout').html(outTime);
        $special.find('.special-requests__text').html(guestRequest);
        $special.removeClass('hide');
    },

};


ItineraryView.pageLoaded = () => {
    methods.init();
    methods.initDiscount();
    if (window.location.pathname === '/itinerary/') {
        $('#price-detail-itinerary').addClass('show');
    }

    const $itineraryError = $('#itineraryError');
    $('body').on('discount:removed', (event, param) => {
        methods.initDiscount();
        methods.updateDiscount(param);
    }).on('discount:applied', (event, param) => {
        methods.updateDiscount(param);
    });

    $itineraryError.on('click', '.js-close', () => {
        $itineraryError.addClass('hide');
    });
    if (parsed.error) {
        $itineraryError.removeClass('hide').find('.alert__msg').html(parsed.error);
    }

    $('#guestRequest').on('click', function () {
        const elementOffset = $(this).offset().top;
        $('#specialRequestsModal').animate({ scrollTop: elementOffset }, 500);
    });

    $('#checkInTime,#checkOutTime,#guestRequest').on('change', methods.specialRequestChange);
};

rt.roomtype = parsed.roomtype;

let analyticsConfigfunc = function(){
    return  {
        checkin:parsed.checkin,
        checkout:parsed.checkout,
        room_type:parsed.roomtype,
        adults:rt.getAdults(),
        kids:rt.getKids(),
        rooms:rt.getRooms(),
        total_price:$('#grandTotal').html(),
        'PAH-status':rt.pahStatus,
        hrental_id: hotelId,
        hrental_pagetype: 'conversionintent',
        hrental_startdate: checkin,
        hrental_enddate: checkout,
        hrental_totalvalue: rt.total,
    }
}

function sendAnalytics(step, data = {}) {
    const events = {
        '1': {
            name: 'Checkout step 0 - Review itinerary',
            value: { state: rt.loggedin ?  'LOGGEDIN' : 'LOGGEDOUT' },
        },
        '2': {
            name: 'Checkout step 1 - Contact Info',
            value: { type: 'GUEST' },
        },
        '3': {
            name: 'Checkout step 2 - Traveller info',
            value: {},
        },
        '4': {
            name: 'Checkout step 3 - Special requests',
            value: {},
        },
				'5': {
						name: 'Checkout step 3 - OTP',
						value: {},
				}
    };
    const { name, value } = events[step];
    rt.clientAnalyticsObj.sendAnalyticsEvents(name, {
        ...value,
        ...data,
        ...analyticsConfigfunc(),
    });
}

$('.next').on('click', () => {
    sendAnalytics(currentStep);
    currentStep = currentStep+1;
    toggleActiveItem('next');
    toggleSteps();
});

$('.js-back-link').on('click', e => {
    if(currentStep !==1){
        currentStep = currentStep-1;
        e.preventDefault();
        toggleActiveItem('prev');
        toggleSteps();

    }

});

function toggleActiveItem(toActive){
    const currentItem = $('.itinerary-steps__indicator').find('.active');
    currentItem.removeClass('active');

    if(toActive === 'next'){
        currentItem.addClass('complete').next().addClass('active');
        currentItem.html('<i class="icon-tick"></i>');
        $('.itinerary-steps__heading').show();
    } else {
        currentItem.prev().addClass('active');
        currentItem.html(currentStep + 1);
        currentItem.prev().html(currentStep);
    }
}
function toggleSteps(){
    $('.step').addClass('hide');
    console.log()
    $(`.step-${currentStep}`).removeClass('hide');
}


$('.view-complete-breakup').on('click', e => {
    e.preventDefault();
    const stringPriceDetail = {nights:rt.nights};
    //$('#completeBreakupModal').removeClass('hide');
    render(<PriceDetail data={stringPriceDetail}/>, $('#completeBreakupModal')[0]);
});

let itineraryConfig = { name:'Itinerary Page', widgets:[], analyticsConfigfunc};
let itineraryView = ItineraryView.setup(itineraryConfig);
module.exports = methods;
