__author__ = 'baljeet'
import datetime
import hmac
import logging
from hashlib import sha1

import psycopg2
import requests
from django.conf import settings
from django.core.management import BaseCommand
from pyquery import PyQuery as pQ
from apps.hotels.service.hotel_service import HotelService

from dbcommon.models.hotel import Hotel

logger = logging.getLogger(__name__)

commandObject = None

NAME = settings.DATABASES['default']['NAME']
USER = settings.DATABASES['default']['USER']
PASS = settings.DATABASES['default']['PASSWORD']
HOST = settings.DATABASES['default']['HOST']
PORT = settings.DATABASES['default']['PORT']


def getRoomAvailabilityAndPrice(hotel, hotelDetailsXml):
    """
    Get the availability and price of rooms in the hotel. Enriches the model with isAvailable,
    hotelogixPrice, hotelogixRateId, availableRooms, maxPax
    :param hotel:
    :param hotelDetailsXml:
    :return:
    """

    rooms = hotel.rooms.all().order_by('price')
    hotelXml = pQ(hotelDetailsXml)
    roomTypes = hotelXml('roomtype')
    # TODO: Discuss what to do in this scenario
    # if len(roomTypes) is 0:
    # 	raise ExternalServiceException("No room types found for hotel " + hotel.name + " in hotelogix")

    roomsMap = {

    }
    for hotelogixRoomPq in roomTypes:
        hotelogixRoom = pQ(hotelogixRoomPq)
        for i, room in enumerate(rooms):
            if room.room_type not in roomsMap:
                roomsMap[room.room_type] = {}
            logger.info("Title %s and room type %s", hotelogixRoom.attr("title"),
                        room.room_type.strip())
            if hotelogixRoom.attr("title").strip() == room.room_type.strip():
                logger.info("Setting availability for %s, %s, %s", room.room_type.strip(),
                            hotelogixRoom.attr("availableroom"), hotelogixRoom.attr("title"))
                roomsMap[room.room_type]['availability'] = int(hotelogixRoom.attr("availableroom"))
    logger.info('Rooms Map %s', roomsMap)
    return rooms, roomsMap


def getMinimumRate(hotelogix_room):
    tax = 0
    min_rate = 0
    availability = False
    rate_id = ""
    rates = hotelogix_room('rate')
    for rate_pq in rates:
        rate = pQ(rate_pq)
        logger.info("Title rate %s", rate.attr("title").strip())
        if rate.attr("title").strip() == "Treebo Promotional":
            availability = True
            break
        elif rate.attr("title").strip() == "Treebo Standard" and (
                    min_rate == 0 or min_rate > float(rate.attr("price"))):
            availability = True
    logger.info("Availability %s", availability)
    return min_rate, rate_id, tax, availability

def extract_hotelogix_id(hotelogix_hotel):
    """
    Extracts the Hx id from the name. Returns None if not found
    :param hotelogix_hotel:
    :type hotelogix_hotel:
    :return:
    :rtype:
    """
    import re
    try:
        match = re.search('(\d+)', hotelogix_hotel.attr('title').strip())
    except Exception as err:
        return None
    if match:
        try:
            extracted_hotelogix_id = match.group(1)
            int(extracted_hotelogix_id)
            return extracted_hotelogix_id
        except ValueError:
            return None
    return None


def extract_hotelogix_id(hotelogix_hotel):
    """
    Extracts the Hx id from the name. Returns None if not found
    :param hotelogix_hotel:
    :type hotelogix_hotel:
    :return:
    :rtype:
    """
    import re
    match = re.search('(\d+)', hotelogix_hotel.attr('title').strip())
    if match:
        try:
            extracted_hotelogix_id = match.group(1)
            int(extracted_hotelogix_id)
            return extracted_hotelogix_id
        except ValueError:
            return None
    return None

def searchWorker(dateRange, hotels_to_update):
    checkInDate = datetime.datetime.strptime(dateRange, "%Y-%m-%d").date()
    singleDayDelta = datetime.timedelta(days=1)
    __hotelService = HotelService()

    checkOutDate = checkInDate + singleDayDelta
    auth_xml = commandObject.ws_auth_xml()
    ws_auth_signature = commandObject.ws_auth_signature(auth_xml)
    print("WS AUTH SIGNATURE"+ws_auth_signature)
    headers = {'Content-Type': 'text/xml',
               'X-HAPI-Signature': ws_auth_signature}  # set what your server acceptsprint headers

    response = requests.post(commandObject.hotelogix_url + "wsauth", data=auth_xml, headers=headers)

    d = pQ(response.content)
    accesskey = d("accesskey").attr("value")
    accesssecret = d("accesssecret").attr("value")
    conn = psycopg2.connect(database=NAME, host=HOST, user=USER, password=PASS, port=PORT)
    conn.set_isolation_level(0)
    cur = conn.cursor()
    srch_xml = commandObject.search_xml(accesskey, checkInDate, hotels_to_update)
    search_signature = commandObject.search_signature(accesssecret, srch_xml)
    search_headers = {'Content-Type': 'text/xml',
                      'X-HAPI-Signature': search_signature}  # set what your server accepts
    st = int(datetime.datetime.now().strftime('%s'))

    logger.info("Searching for check in date " + srch_xml)
    responseXml = requests.post(commandObject.hotelogix_url + "search", data=srch_xml,
                                headers=search_headers)
    et = int(datetime.datetime.now().strftime('%s'))
    logger.info("HX %s took %s seconds" % ('search', et - st))

    logger.info("response xml " + str(responseXml.content))

    responseXml = pQ(responseXml.content)

    hotelogixHotels = responseXml('hotel')
    for singleHotelogixHotel in hotelogixHotels:
        singleHotelogixHotel = pQ(singleHotelogixHotel)
        hotelogix_id = extract_hotelogix_id(singleHotelogixHotel)
        if hotelogix_id is not None:
            hotelogix_id = hotelogix_id.strip()
            currentHotelModel = commandObject.hotel_id_wise_mapping.get(hotelogix_id)
            if (currentHotelModel is None) or (
                            len(hotels_to_update) > 0 and str(
                        currentHotelModel.hotelogix_id) not in hotels_to_update):
                continue

            logger.info("Finding the availability for hotel " + str(currentHotelModel.id))

            roomsData, roomsMap = getRoomAvailabilityAndPrice(currentHotelModel,
                                                              singleHotelogixHotel)
            logger.info('Rooms %s', roomsMap)
            for singleRoom in roomsData:
                finalAvailability = 0
                if singleRoom.room_type in roomsMap and 'availability' in roomsMap[
                    singleRoom.room_type]:
                    finalAvailability = roomsMap[singleRoom.room_type]['availability']
                currentQuery = """WITH upsert AS (UPDATE bookingstash_availability SET created_at=now(), modified_at=now(),availablerooms=%(availablerooms)s,date='%(date)s',room_id=%(roomId)s WHERE date='%(date)s' AND room_id not in (502,351,566,713,450,731) AND  room_id=%(roomId)s RETURNING *) INSERT INTO bookingstash_availability (created_at, modified_at,date,availablerooms,room_id)
                             SELECT now(),now(),'%(date)s','%(availablerooms)s','%(roomId)s' WHERE NOT EXISTS (SELECT * FROM upsert);""" % {
                    'date': checkInDate.strftime("%Y-%m-%d"),
                    'availablerooms': finalAvailability,
                    'roomId': singleRoom.id}
                try:

                    logger.info("currentQuery %s", currentQuery)
                    cur.execute(currentQuery)
                    conn.commit()
                    logger.info("Updated data for room " + str(singleRoom.id) + " of hotel " + str(
                        currentHotelModel.id) + " with availability " + singleRoom.availableRooms if hasattr(
                        singleRoom,
                        'availableRooms') else 0)
                except Exception as exc:
                    conn.rollback()
    conn.close()


class Command(BaseCommand):
    help = 'Fetch Availability from Hotelogix and save in Database'
    consumer_key = settings.CONSUMER_KEY
    consumer_secret = settings.CONSUMER_SECRET
    hotelogix_url = settings.HOTELOGIX_URL
    bookingdetails_url = settings.BOOKING_DETAILS_URL
    __hotelService = None
    hotel_id_wise_mapping = {}
    hotelAvailabilities = []

    def add_arguments(self, parser):
        # Positional arguments

        # Named (optional) arguments
        parser.add_argument('--delta', default=1, type=int, help='Days for which to run the script')
        parser.add_argument('--start', default=None, help='Start date for sync')

        parser.add_argument('--hotels', default='',
                            help='comma separated list of hotels to run the script for. Pass hotelogix ids')

    def handle(self, *args, **options):
        start = options.get('start')
        delta = int(options.get('delta'))
        hotelsString = options.get('hotels')
        hotels_to_update = []

        logger.info("Starting fetch for hotels " + hotelsString + " for # of dates " + str(delta))

        if len(hotelsString) > 0:
            hotels_to_update = hotelsString.split(',')
        if start is None:
            startDate = datetime.datetime.now()
        else:
            startDate = datetime.datetime.strptime(start, "%d-%m-%Y")

        try:
            self.updateAvailabilityForHotels(startDate, delta, hotels_to_update)
        except Exception as exc:
            logger.exception(exc)

    def updateAvailabilityForHotels(self, start, delta, hotels_to_update):
        self.hotelAvailabilities = []

        global commandObject
        commandObject = self

        availabilityPeriod = delta
        singleDayDelta = datetime.timedelta(days=1)
        date_min = start
        date_max = datetime.datetime.combine(start + datetime.timedelta(days=availabilityPeriod),
                                             datetime.time.max)
        allHotels = Hotel.objects.all()
        allHotels = list(allHotels)

        for singleHotel in allHotels:
            self.hotel_id_wise_mapping[singleHotel.hotelogix_id] = singleHotel
        dateRanges = []
        while date_min < date_max:
            dateRanges.append(date_min.strftime("%Y-%m-%d"))
            date_min = date_min + singleDayDelta

        for curDate in dateRanges:
            try:
                searchWorker(curDate, hotels_to_update)
            except Exception as e:
                logger.exception("Unable to update inventory for hotels")

    def ws_auth_xml(self):
        time = datetime.datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + "'><request method='wsauth' key='" + Command.consumer_key + "'></request></hotelogix>"
        return xml

    def ws_auth_signature(self, xml):
        secret = Command.consumer_secret.encode('utf-8')
        xml = xml.encode('utf-8')
        signature = hmac.new(secret, xml, sha1)
        ws_auth_signature = signature.hexdigest()
        return ws_auth_signature

    def cancel_xml(self, accesskey, order_id, reservation_id):
        time = datetime.datetime.utcnow().isoformat()
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + "'><request method='cancel' key='" + accesskey + "'><orderId value='" + order_id + "'/><reservationId value='" + reservation_id + "'/><cancelCharge amount='0'/><cancelDescription>This is a test cancel from webservice</cancelDescription></request></hotelogix>"
        return xml

    def create_signature(self, accesssecret, xml):
        signature = hmac.new(str(accesssecret), str(xml), digestmod=sha1).hexdigest()
        return signature

    def search_xml(self, accesskey, in_date, hotels_to_update):
        checkin_date = in_date.strftime("%Y-%m-%d")
        checkout_date = (in_date + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
        time = datetime.datetime.utcnow().isoformat()
        hotel_xml = ""
        for hotel_id in hotels_to_update:
            hotel_xml = hotel_xml + "<hotel id='" + str(hotel_id) + "' />"
        xml = "<?xml version='1.0'?><hotelogix version='1.0' datetime='" + time + "'><request method='search' key='" + accesskey + "'><stay checkindate='" + checkin_date + "' checkoutdate='" + checkout_date + "'/><pax adult='1' child='0' infant='0'/><roomrequire value='1'/><limit value='100' offset='0' hasResult='0'/><hotels>" + hotel_xml + "</hotels></request></hotelogix>"
        return xml

    def search_signature(self, accesssecret, xml):
        accesssecret = accesssecret.encode('utf-8')
        xml = xml.encode('utf-8')
        signature = hmac.new(accesssecret, xml, digestmod=sha1).hexdigest()
        return signature
