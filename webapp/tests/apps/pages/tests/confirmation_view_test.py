from django.test import TestCase, Client


class TestConfirmationTest(TestCase):
    def test_confirmation(self):
        c = Client()
        response_cnfrm = c.get('/pages/confirmation/',
                               {'hotel_id': 1, 'room_id': 2, 'room_type': 'oak'})
        print(response_cnfrm)
        self.assertEqual(response_cnfrm.status_code, 200)
        self.assertEqual(response_cnfrm.__sizeof__(), 32)
        self.assertNotEqual(response_cnfrm, [])
