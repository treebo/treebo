from dbcommon.models.hotel import HotelBrandMapping, CSHotelBrands
import logging

logger = logging.getLogger(__name__)


class HotelSubBrandService:

    @classmethod
    def get_hotel_sub_brands_dict(self, hotel):
        if hotel.brands or len(hotel.brands) != 0:
            brands_list = self.parse_sub_brands_objects(hotel.brands)
        else:
            brands_list = self.get_brands_from_db(hotel)
        return brands_list

    @classmethod
    def get_brands_from_db(self, hotel):
        brands_list = []
        hotel_brand_mapped_objects = HotelBrandMapping.objects.filter(
            hotel_id=hotel.id).all()
        if not hotel_brand_mapped_objects:
            return brands_list

        hotel_brand_mapped_objects_codes = [
            hotel_brand_mapped_object.cs_brand.code for hotel_brand_mapped_object in hotel_brand_mapped_objects]
        cs_brand_objects = CSHotelBrands.objects.filter(
            code__in=hotel_brand_mapped_objects_codes).all()
        brands_list = self.parse_sub_brands_objects(cs_brand_objects)
        return brands_list

    @classmethod
    def get_brands_for_hotels(self, hotel_ids):
        brands_list_for_all_hotels = {}
        # TODO: Seems buggy. `hotel_id` is foreign key. So should be `hotel_id__id__in`
        hotel_brand_mapped_objects = HotelBrandMapping.objects.filter(
            hotel_id__in=hotel_ids).select_related('cs_brand').all()
        for hotel_brand_mapped_object in hotel_brand_mapped_objects:
            brands_list_for_all_hotels.update(
                {hotel_brand_mapped_object.hotel_id_id: self.parse_sub_brands_objects(
                    [hotel_brand_mapped_object.cs_brand])})
        return brands_list_for_all_hotels

    @staticmethod
    def get_hotels_ids_for_brand(brand_code):

        hotel_ids = HotelBrandMapping.objects.filter(cs_brand__code=brand_code).values_list('hotel_id', flat=True)

        return hotel_ids

    @staticmethod
    def get_brand_list_for_hotel(hotel_id):
        brand_list = []
        hotel_brand_list = HotelBrandMapping.objects.filter(hotel_id=hotel_id)
        for brand in hotel_brand_list:
            brand_list.append(brand.cs_brand.code)
        return brand_list

    @classmethod
    def parse_sub_brands_objects(self, cs_brand_objects):
        brands_list = []
        try:
            for brand_object in cs_brand_objects:
                if isinstance(brand_object, dict):
                    brand_dict = {
                        "code": brand_object.get('code'),
                        "color": brand_object.get('color'),
                        "display_name": brand_object.get('display_name'),
                        "legal_name": brand_object.get('legal_name'),
                        "logo": brand_object.get('logo'),
                        "name": brand_object.get('name'),
                        "short_description": brand_object.get('short_description'),
                        "status": brand_object.get('status')}
                else:
                    brand_dict = {

                        "code": brand_object.code,
                        "color": brand_object.color,
                        "display_name": brand_object.display_name,
                        "legal_name": brand_object.legal_name,
                        "logo": brand_object.logo,
                        "name": brand_object.name,
                        "short_description": brand_object.short_description,
                        "status": brand_object.status

                    }
                brands_list.append(brand_dict)

        except Exception as e:
            logger.exception(
                "Couldn't parse sub_brands_object %s", str(
                    cs_brand_objects.all()))
        return brands_list
