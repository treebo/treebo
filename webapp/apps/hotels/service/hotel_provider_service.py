from dbcommon.models.hotel import Hotel


class HotelProviderService():

    @staticmethod
    def get_provider_hotels(hotel_ids, enabled_property_providers):

        treebo_provider_hotel_ids = []
        hotel_details = Hotel.objects.filter(id__in=hotel_ids)
        for hotel in hotel_details:
            if hotel.provider_name in enabled_property_providers:
                treebo_provider_hotel_ids.append(hotel.id)

        return treebo_provider_hotel_ids
