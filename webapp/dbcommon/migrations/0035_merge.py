# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0033_merge'),
        ('dbcommon', '0034_auto_20160822_0947'),
    ]

    operations = [
    ]
