import logging

from requests.exceptions import HTTPError
from rest_framework.authentication import BasicAuthentication
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_400_BAD_REQUEST

from apps.auth.csrf_exempt_session_authentication import \
    CsrfExemptSessionAuthentication
from apps.checkout.models import BookingRequest
from apps.checkout.service.booking_confirmation_service import \
    BookingConfirmationService
from apps.common import error_codes
from apps.common.api_response import APIResponse as api_response
from apps.common.exceptions.booking_exceptions import BookingNotFoundException, InvalidBookingRequestException
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboException
from apps.bookings.models import PaymentOrder
from apps.common.slack_alert import SlackAlertService as slack_alert
from axis_rooms.exceptions import AxisRoomsApiFailure
from apps.common.exceptions.custom_exception import AxisRoomBookingValidationError

logger = logging.getLogger(__name__)


class ConfirmBooking(TreeboAPIView):
    authentication_classes = (
        BasicAuthentication, CsrfExemptSessionAuthentication)
    booking_confirmation_service = BookingConfirmationService()

    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            if 'order_id' not in data or data['order_id'] == "" or data['order_id'] is None:
                payment_order = PaymentOrder.objects.filter(ps_order_id=data['ps_order_id']).first()
                data['order_id'] = payment_order.booking_order_id
            order_id, reason, booking_request = \
                self.booking_confirmation_service.confirm_booking(request.user,
                                                                  data['pg_meta'],
                                                                  data['order_id'],
                                                                  data['ps_order_id'],
                                                                  data['gateway'])
            data = {
                'status': BookingRequest.COMPLETED,
                'order_id': order_id,
                "reason": reason
            }
            return api_response.prep_success_response(data)
        except HTTPError as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.data,
                                                        message=e.__str__(), dev_msg="HTTPError",
                                                        class_name=self.__class__.__name__)
            logger.exception(
                "Unable to confirm booking with error %s ",
                request.data)
            return api_response.error_response_from_error_code(
                error_codes.CONFIRM_BOOKING_FAILED,
                status_code=e.response.status_code,
                api_name=ConfirmBooking.__name__)

        except AxisRoomBookingValidationError as err:
            return api_response.error_response_from_error_code(
                error_codes.AXIS_ROOM_FAILURE,
                status_code=HTTP_400_BAD_REQUEST,
                api_name=ConfirmBooking.__name__)

        except AxisRoomsApiFailure as err:
            return api_response.error_response_from_error_code(
                error_codes.AXIS_ROOM_FAILURE,
                status_code=HTTP_400_BAD_REQUEST,
                api_name=ConfirmBooking.__name__)

        except (BookingNotFoundException, InvalidBookingRequestException) as e:
            slack_alert.send_slack_alert_for_exceptions(status=400, request_param=str(data['order_id']),
                                                        message=e.developer_message,
                                                        class_name=self.__class__.__name__)
            return api_response.error_response_from_error_code(error_codes.BOOKING_NOT_FOUND,
                                                               api_name=ConfirmBooking.__name__)

        except TreeboException as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.data,
                                                        dev_msg="TreeboException", message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            return api_response.treebo_exception_error_response(e)

        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.data,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.exception(
                "Unable to confirm booking with internal server error %s ",
                request.data)
            return api_response.error_response_from_error_code(
                error_codes.CONFIRM_BOOKING_FAILED,
                status_code=HTTP_500_INTERNAL_SERVER_ERROR,
                api_name=ConfirmBooking.__name__)
