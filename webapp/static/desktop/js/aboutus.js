import BaseView from './treebolibs/baseview';
import logger from './treebolibs/logger';
import '../css/aboutus.less';

let AboutusView = Object.create(BaseView);

AboutusView.pageLoaded = () => {
    logger.log(" Aboutus page loaded");
}

let aboutusConfig = { name:'', widgets:[]};
let aboutusView = AboutusView.setup(aboutusConfig);
