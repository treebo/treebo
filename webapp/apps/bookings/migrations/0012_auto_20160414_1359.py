# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('bookings', '0011_bookingrequest_add_to_cart_hotel_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingrequest',
            name='add_to_cart_hotel_id',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
