import json
import logging

from psycopg2 import InterfaceError
from base.db_decorator import db_retry_decorator, db_failure_retry
from dbcommon.models.direction import DirectionsNew, Directions
from dbcommon.models.hotel import Highlight, Hotel, CSConsumerDetails
from dbcommon.models.landmark import SearchLandmark
from dbcommon.models.location import State, City, Landmark, Locality, CityAlias

logger = logging.getLogger(__name__)


def update_city(params):
    try:
        city = City.all_cities.get(name=params['name'])
    except Exception as e:
        city = City()
    for key, value in list(params.items()):
        setattr(city, key, value)
    city.save()
    return city


def update_state(params):
    state_obj, created = State.objects.update_or_create(
        name=params['name'], defaults=params)
    return state_obj


def update_search_landmark(params):
    try:
        try:
            logger.info("Updating search landmark")
            search_landmark = SearchLandmark.objects.get(cs_id=params['cs_id'])
        except Exception as e:
            logger.info("Exception in getting search landmark by cs id %s", params['cs_id'])
            logger.error(e)
            try:
                search_landmark = SearchLandmark.objects.get(seo_url_name=params['seo_url_name'], city_id=params['city_id'])
            except Exception as e:
                logger.info("Didnt get seaerch landmark with name either, so creating for cs id %s", params['city_id'])
                search_landmark = SearchLandmark()
        for key, value in list(params.items()):
            setattr(search_landmark, key, value)
        search_landmark.save()
        logger.info("Saved search landmark")

        return search_landmark
    except Exception as e:
        logger.error(e)


def update_hotel_search_landmark_directions(params):
    directions, created = DirectionsNew.objects.update_or_create(
        landmark_obj_id=params['landmark_obj_id'], defaults=params)
    return directions


def update_highlight(params):
    highlight, created = Highlight.objects.update_or_create(
        hotel_id=params['hotel_id'], description=params['description'], defaults=params)
    return highlight


@db_retry_decorator()
def update_hotel(params):
    try:
        logger.info('Hotel params: %s' % params)
        is_being_created = False
        try:
            hotel = Hotel.all_hotel_objects.get(cs_id=params['cs_id'])
        except Hotel.DoesNotExist:
            logger.error("Hotel with cs id does not exist.. checking for hotelogix id")
            try:
                hotel = Hotel.all_hotel_objects.get(hotelogix_id=params['hotelogix_id'])
            except Hotel.DoesNotExist as e:
                logger.error("Hotel with hotelogix id does not exist.. so creating one")
                hotel = Hotel()
                is_being_created = True
        for key, value in list(params.items()):
            setattr(hotel, key, value)
        try:
            hotel.save()
        except Exception as e:
            logger.exception("Found error in saving hotel %s" % e.__str__())
        if is_being_created:
            pass
        logger.info("Hotel saved!! with id %d for cs id %s", hotel.id, params['cs_id'])
        return hotel, is_being_created
    except InterfaceError as e:
        logger.exception("Interface error %s" % e.__str__())
        raise e
    except Exception as e:
        logger.exception(e.__str__())

def update_landmark(params):
    try:
        logger.info("Updating the landmark")
        landmark_obj = Landmark.objects.get(cs_id=params['cs_id'])
        logger.info("Landmark exists by cs id")
    except Exception as e:
        try:
            logger.error(e)
            logger.info("Exception in getting landmark by cs id %s", params['cs_id'])
            landmark_obj = Landmark.objects.get(name=params['name'], city_id=params['city_id'])
            print("Landmark exists by name")
        except Exception as e:
            logger.info("Landmark doesnt exist.. creating one more %s", params['cs_id'])
            landmark_obj = Landmark()

    landmark_obj.city_id = params['city_id']
    landmark_obj.cs_id = params['cs_id']
    landmark_obj.name = params['name']
    if 'latitude' in params and params['latitude']:
        landmark_obj.latitude = params['latitude']
    if 'longitude' in params and params['longitude']:
        landmark_obj.longitude = params['longitude']
    landmark_obj.status = 1
    landmark_obj.save()
    print("Landmark saved")
# landmark_obj, created = Landmark.objects.update_or_create(
#     name=params['name'], city_id=params['city_id'], defaults=params)
    return landmark_obj


def update_hotel_landmark_directions(params):
    directions, created = Directions.objects.update_or_create(
        landmark_obj_id=params['landmark_obj_id'], hotel_id=params['hotel_id'], defaults=params)
    return directions


def update_locality(params):
    locality_obj, created = Locality.objects.update_or_create(
        name=params['name'], city_id=params['city_id'], defaults=params)
    logger.info("Successfully saved locality of cs id %s with id %d, and is created is %s", locality_obj.cs_id, locality_obj.id, created)
    return locality_obj


def update_city_alias(params):
    city_alias_obj, created = CityAlias.objects.update_or_create(
        alias=params['alias'], city=params['city'], defaults=params)
    logger.info("Successfully saved city alias with id %d is been created-- %s", city_alias_obj.id, created)
    return city_alias_obj


def store_dump_of_incoming_message(cs_property_id, message_body, cs_route, exchange, queue_name):
    cs_consumer_details = {}
    try:
        message_data = json.dumps(message_body['data'])
        cs_consumer_details['cs_data'] = message_data
        cs_consumer_details['cs_queue'] = queue_name
        cs_consumer_details['cs_exchange'] = exchange
        cs_consumer_details['is_consumed'] = False
        cs_consumer_details['failure_message'] = ''
        cs_consumer_details['cs_route'] = cs_route
        cs_obj = update_cs_consumer_details(cs_property_id, cs_consumer_details)
        return cs_obj
    except InterfaceError as e:
        logger.error(e)
        raise e
    except Exception as e:
        logger.error(e)
    return cs_consumer_details


@db_failure_retry(retry=3)
def update_cs_consumer_details(cs_id, cs_consumer_details_info):
    cs_obj = None
    try:
        cs_obj, created = CSConsumerDetails.objects.update_or_create(cs_id=cs_id,
                                                            cs_route=cs_consumer_details_info['cs_route'],
                                                            defaults=cs_consumer_details_info)
        logger.info("Dump of incoming message is succssfully stored for cs id %s and "
                    "routing key %s", cs_id, cs_consumer_details_info['cs_route'])
    except InterfaceError as e:
        logger.error(e)
        raise
    except Exception as e:
        logger.info("Could not store dump of incoming message")
        logger.exception(e)
    return cs_obj
