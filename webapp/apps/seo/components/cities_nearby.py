import geopy
import geopy.distance

from apps.seo.components.base_component import BaseComponent
from apps.search.services.auto_complete_result_builder import AutoCompleteResultBuilder
from apps.seo.serializers.serializer import CitiesNearbySerializer
from data_services.respositories_factory import RepositoriesFactory

__author__ = 'ansilkareem'


class CitiesNearby(BaseComponent):

    def __init__(self, current_city, footer_data):
        self.near_by_cities = dict()
        self.sorted_nearby_cities = []
        super(CitiesNearby, self).__init__(current_city, footer_data, [])

    def build(self):
        result = dict(key='city',
                      title='Nearby cities')
        if self.current_city and self.current_city.enable_for_seo:
            city_data_service = RepositoriesFactory.get_city_repository()
            cities = city_data_service.filter_cities(
                enable_for_seo=True, status=1)
            all_cities = [
                city for city in cities if city.id != self.current_city.id]
            #all_cities = City.objects.filter(enable_for_seo=True, status=1, ).exclude(id=self.current_city.id)
            current_city_point = geopy.Point(
                self.current_city.city_latitude,
                self.current_city.city_longitude)
            for city in all_cities:
                pt2 = geopy.Point(city.city_latitude, city.city_longitude)
                distance = geopy.distance.distance(current_city_point, pt2).km
                if distance <= self.distance_cap:
                    # adding to near_by cities only if the distance cap matches
                    self.near_by_cities[str(city.name)] = (distance, city)
            # Now sorting the dict of near_by_cities
            self.sorted_nearby_cities = [
                seq[1][1] for seq in sorted(
                    list(
                        self.near_by_cities.items()),
                    key=lambda x: x[1][0])]
            result['content'] = AutoCompleteResultBuilder.build_city_result(self.sorted_nearby_cities[:self.count_cap]) \
                if self.sorted_nearby_cities else []
            serialized_obj = CitiesNearbySerializer(data=result)
            if serialized_obj.is_valid():
                return serialized_obj.validated_data
        return result
