__author__ = 'ansilkareem'

from rest_framework import serializers

from webapp.apps.content.constants import PAGES
from webapp.apps.posts.request import FaqRequest


class AnswerSerializer(serializers.Serializer):
    text = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    user = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    posted_date = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)
    verified = serializers.BooleanField()


class QASerializer(serializers.Serializer):
    question = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    answers = AnswerSerializer(many=True, required=False)
    user = serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False)
    posted_date = serializers.CharField(
        allow_null=True, allow_blank=True, required=False)


class FaqsRequestSerializer(serializers.Serializer):
    page = serializers.CharField(max_length=15, required=True)
    city = serializers.CharField(required=False)
    hotel = serializers.IntegerField(required=False)
    locality = serializers.CharField(required=False)
    landmark = serializers.CharField(required=False)
    category = serializers.CharField(required=False)
    offset = serializers.IntegerField(required=False, default=0, min_value=0)
    limit = serializers.IntegerField(required=False, default=5, min_value=1)

    def validate(self, data):
        page = data.get('page')
        if page == PAGES.SEARCH:
            if data.get('hotel') and data.get('city') and data.get('locality') and data.get(
                'landmark') and data.get('category'):
                raise serializers.ValidationError(
                    "One of these fields are needed for search page: hotel, city, landmark, "
                    "locality, category")
        else:
            if page == PAGES.HD:
                if not data.get('hotel'):
                    raise serializers.ValidationError("hotel field is mandatory for page: " + page)
            elif page == PAGES.CITY:
                if not data.get('city'):
                    raise serializers.ValidationError("city field is mandatory for page: " + page)
            elif page == PAGES.LANDMARK:
                if not data.get('landmark'):
                    raise serializers.ValidationError(
                        "landmark field is mandatory for page: " + page)
            elif page == PAGES.LOCALITY:
                if not data.get('locality'):
                    raise serializers.ValidationError(
                        "locality field is mandatory for page: " + page)
            elif page == PAGES.CATEGORY:
                if not data.get('category'):
                    raise serializers.ValidationError(
                        "category field is mandatory for page: " + page)
            else:
                raise serializers.ValidationError("Invalid value is given for page: " + page)

        return FaqRequest(**data)
