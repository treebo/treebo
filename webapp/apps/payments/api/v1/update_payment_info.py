# -*- coding:utf-8 -*-
"""
    UpdatePaymentInfo  API
"""

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from apps.bookings.models import Booking, PaymentOrder
from apps.payments.dto.update_payment_dto import UpdatePaymentDTO
from base.logging_decorator import log_args
from base.views.api import TreeboAPIView
from apps.common.api_response import APIResponse as api_response
from apps.common import error_codes
from apps.common.slack_alert import SlackAlertService as slack_alert
import logging

logger = logging.getLogger(__name__)


class UpdatePaymentInfo(TreeboAPIView):
    authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdatePaymentInfo, self).dispatch(*args, **kwargs)

    @log_args(logger)
    def get(self, request, *args, **kwargs):
        raise Exception('Method not supported')

    @log_args(logger)
    def post(self, request, *args, **kwargs):
        update_paymnt_dto = UpdatePaymentDTO(data=request.data)
        if not update_paymnt_dto.is_valid():
            return api_response.invalid_request_error_response(
                update_paymnt_dto.errors)
        try:
            order_id = update_paymnt_dto.data["order_id"]
            pg_payment_id = update_paymnt_dto.data["pg_payment_id"]
            ps_order_id = update_paymnt_dto.data["ps_order_id"]
            payment_order = PaymentOrder.objects.get(ps_order_id=ps_order_id)
            payment_order.order_id = pg_payment_id
            payment_order.status = PaymentOrder.CAPTURED
            payment_order.booking_order_id = order_id
            payment_order.save()
            return api_response.prep_success_response("Payment info updated")
        except Booking.DoesNotExist as e:
            logger.error(
                "update payment info booking not found %s ",
                update_paymnt_dto.data)
            return api_response.error_response_from_error_code(
                error_codes.BOOKING_NOT_FOUND, api_name=UpdatePaymentInfo.__name__)
        except PaymentOrder.DoesNotExist as e:
            logger.error(
                "update payment info paymentorder not found %s ",
                update_paymnt_dto.data)
            return api_response.error_response_from_error_code(
                error_codes.PAYMENT_ORDER_NOT_FOUND, api_name=UpdatePaymentInfo.__name__)
        except Exception as e:
            slack_alert.send_slack_alert_for_exceptions(status=500, request_param=request.POST,
                                                        message=e.__str__(),
                                                        class_name=self.__class__.__name__)
            logger.error(
                "update payment info internal error not found %s ",
                update_paymnt_dto.data)
            return api_response.error_response_from_error_code(
                error_codes.UNABLE_TO_UPDATE_PAYMENT, api_name=UpdatePaymentInfo.__name__)
