import logging

from apps.auth.constants import VerifiedThrough, UserContactType, ProfileType, UserStatus
from apps.auth.treebo_authN_client import TreeboAuthNClient
from apps.auth.user_profile_service_client import UserProfileServiceClient
from apps.common.exceptions.custom_exception import UPSClientException

logger = logging.getLogger(__name__)


class UserProfileService:
    user_profile_service_client = UserProfileServiceClient()
    authN_client = TreeboAuthNClient()

    def create_new_user_with_phone_number(self, user_details):
        whatsapp_optin_status = self.get_whatsapp_optin_status(
            phone_number=user_details['phone_number'])

        is_verified = False
        verified_through = None
        if user_details['is_otp_verified']:
            verified_through = VerifiedThrough.OTP.value
            is_verified = True

        user_contact = dict(
            contact_type=UserContactType.PHONE.value, contact_value=user_details['phone_number'],
            is_verified=is_verified,
            verified_through=verified_through, is_whatsapp_opted_in=whatsapp_optin_status,
            auth_id=user_details['id'], last_login=user_details['last_login'],
            country_code="91",
            is_primary_contact=True
        )
        last_name = user_details.get('last_name') or None

        name = dict(first_name=user_details['first_name'], middle_name=None, last_name=last_name)

        gender = user_details['gender']
        if not gender:
            gender = None

        user_details = {
            "data": {
                "user": {
                    "name": name,
                    "gender": gender,
                    "date_of_birth": user_details['dob'],
                    "metauser_details": {},
                },
                "user_contacts": [user_contact]
            },
        }

        response = self.user_profile_service_client.create_new_user(user_details)
        return response

    def create_new_user_profile(self, user):
        contact_ids = [user_contact['contact_id'] for user_contact in user['user_contacts']]

        data = {
            "data": {
                "contact_ids": contact_ids,
                "legal_entitites": [],
                "profile_type": ProfileType.B2C.value,
                "saved_address": [],
                "user_metadata": []
            }}

        self.user_profile_service_client.create_new_user_profile(user['user_id'], data)

    def has_profile_type_b2c(self, user_details):
        for user_profile in user_details:
            if user_profile['profile_type'] == ProfileType.B2C.value:
                return True
        return False

    def create_new_user_with_email_id(self, user_details):
        is_verified = False

        verified_through = None
        if user_details['is_email_verified']:
            is_verified = True
            verified_through = VerifiedThrough.SOCIAL_LOGIN.value

        user_contact = dict(
            contact_type=UserContactType.EMAIL.value, contact_value=user_details['email'], is_verified=is_verified,
            verified_through=verified_through, is_whatsapp_opted_in=False,
            auth_id=user_details['id'], last_login=user_details['last_login'], country_code=None,
            is_primary_contact=True

        )

        last_name = user_details.get('last_name') or None

        name = dict(first_name=user_details['first_name'], middle_name=None, last_name=last_name)
        user_details = {
            "data": {
                "user": {
                    "name": name,
                    "gender": user_details.get('gender') or None,
                    "date_of_birth": user_details['dob'],
                    "metauser_details": {},
                },
                "user_contacts": [user_contact]
            },
        }

        response = self.user_profile_service_client.create_new_user(user_details)
        return response

    def create_user_and_user_profile(self, user_details, user_contact_type):
        users = self.user_profile_service_client.get_user_details(phone_number=user_details['phone_number'],
                                                                  email_id=user_details['email'],
                                                                  name=user_details['first_name'])
        if not users:
            return

        user = self.extract_most_relevant_user(users)
        if not user:
            if user_contact_type == UserContactType.EMAIL:
                new_user = self.create_new_user_with_email_id(user_details)
            else:
                new_user = self.create_new_user_with_phone_number(user_details)
            self.create_new_user_profile(new_user)
            return

        user_profiles = self.user_profile_service_client.get_user_profile(user['user_id'])
        if not user_profiles:
            self.create_new_user_profile(user)

        has_profile_type_b2c = self.has_profile_type_b2c(user_profiles)
        if not has_profile_type_b2c:
            self.create_new_user_profile(user)

    def get_user_id(self, phone_number, email_id=None, name=None):
        first_name, last_name = self.get_name_breakup(name)
        try:
            user_data = self.user_profile_service_client.get_user_details(phone_number, email_id, first_name)
            if user_data['users']:
                return user_data['users'][0]['user_id']
            return None
        except UPSClientException:
            return None

    def get_user_profile_id(self, user_id):
        user_profile = self.user_profile_service_client.get_user_profile(user_id)
        try:
            if user_profile:
                return user_profile[0]['profile_id']
            else:
                logger.error("B2C profile not found for user_id: {user_id}".format(user_id=user_id))
        except Exception as e:
            logger.error("B2C profile not found in ups for user_id: {user_id} due to: {e}".format(user_id=user_id, e=e))
        return None

    def extract_most_relevant_user(self, users):
        if users['users']:
            return users['users'][0]
        else:
            logger.info("No user found")
            return users['users']

    def create_new_user(self, user_data, user):
        # call auth here

        if not user.is_authenticated():
            auth_id = None
            last_login = None
            phone_number = None
        else:
            auth_id = user.auth_id
            last_login = user.last_login.strftime('%Y-%m-%dT%H:%M:%S%z') if user.last_login else None
            phone_number = user.phone_number

        phone_number_verified_through = None
        is_verified = False
        if phone_number == user_data['phone_number']:
            auth_user, user_details = self.authN_client.get_user_by_phone(phone_number=user_data['phone_number'])
            if user_details['is_otp_verified']:
                phone_number_verified_through = VerifiedThrough.OTP.value
                is_verified = True

        user_contact = list()
        user_contact_with_phone_number = dict(
            contact_type=UserContactType.PHONE.value, contact_value=user_data['phone_number'],
            is_verified=is_verified,
            verified_through=phone_number_verified_through,
            is_whatsapp_opted_in=user_data['is_whatsapp_opted_in'],
            auth_id=auth_id, last_login=last_login, country_code=user_data.get('country_code'),
            is_primary_contact=True
        )

        user_contact.append(user_contact_with_phone_number)

        if user_data.get('email_id'):
            user_contact_with_email_id = dict(
                contact_type=UserContactType.EMAIL.value, contact_value=user_data['email_id'], is_verified=False,
                verified_through=None, is_whatsapp_opted_in=False,
                auth_id=auth_id,
                last_login=last_login,
                country_code=None,
                is_primary_contact=True
            )
            user_contact.append(user_contact_with_email_id)

        first_name, last_name = self.get_name_breakup(user_data['name'])

        name = dict(first_name=first_name, middle_name=None, last_name=last_name)
        data = {
            "data": {
                "user": {
                    "name": name,
                },
                "user_contacts": user_contact
            },
        }

        created_user = self.user_profile_service_client.create_new_user(data)
        self.create_new_user_profile(created_user)

    def create_or_update_user_for_primary_guest(self, user_data, user):
        if user_data['phone_number']:
            users = self.user_profile_service_client.get_user_details(phone_number=user_data['phone_number'])
            if not users.get('users'):
                self.create_new_user(user_data, user)
            if users['users']:
                if users['users'][0]['status'] == UserStatus.ANON.value:
                    self.update_anon_user_details(users['users'], user_data)
                self.update_whatsapp_opt_status(users['users'], user_data)
                self.create_b2c_user_profile(users['users'])
                if user_data['email_id']:
                    update_profile_required = self.add_email_id_contact(users['users'][0], user_data['email_id'])
                    if update_profile_required:
                        users = self.user_profile_service_client.get_user_details(
                            phone_number=user_data['phone_number'])
                        if not users:
                            return
                        self.update_user_profile_contact_ids(users['users'])
        else:
            return

    def update_whatsapp_opt_status(self, user, user_data):
        for user_contact in user[0]['user_contacts']:
            if user_contact['contact_value'] == user_data['phone_number']:
                if user_contact['is_whatsapp_opted_in'] != user_data['is_whatsapp_opted_in']:
                    user_contact_data = dict(
                        contact_type=user_contact['contact_type'], contact_value=user_contact['contact_value'],
                        is_verified=True,
                        verified_through=VerifiedThrough.OTP.value,
                        is_whatsapp_opted_in=user_data['is_whatsapp_opted_in'],
                        auth_id=user_contact.get('auth_id', None),
                        country_code=user_contact.get('country_code', '91')
                    )
                    data = {"data": user_contact_data}
                    self.user_profile_service_client.update_user_contact(data, user[0]['user_id'],
                                                                         user_contact['contact_id'])

    def update_user_profile_contact_ids(self, user):
        user_profile = self.user_profile_service_client.get_user_profile(user[0]['user_id'])
        contact_ids = [user_contact['contact_id'] for user_contact in user[0]['user_contacts']]
        data = {
            "data": {
                "contact_ids": contact_ids,
            }}
        self.user_profile_service_client.update_user_profile(data, user[0]['user_id'], user_profile[0]['profile_id'])

    def add_email_id_contact(self, users, email_id):
        for user_contact in users['user_contacts']:
            if user_contact['contact_value'] == email_id:
                return False

        email_contact_count = 0
        for user_contact in users['user_contacts']:
            if user_contact['contact_type'] == UserContactType.EMAIL.value:
                email_contact_count += 1

        is_primary_contact = True
        if email_contact_count > 0:
            is_primary_contact = False

        user_contact_data = dict(
            contact_type=UserContactType.EMAIL.value, contact_value=email_id,
            is_verified=False, verified_through=None, is_whatsapp_opted_in=None,
            auth_id=None, last_login=None, country_code=None, is_primary_contact=is_primary_contact
        )
        data = {"data": [user_contact_data]}
        self.user_profile_service_client.add_user_contact(data, users['user_id'])
        return True

    def mark_verfied_anon_user(self, phone_number, user_data):
        user = self.user_profile_service_client.get_user_details(phone_number=phone_number)
        if not user['users']:
            return
        for user_contact in user['users'][0]['user_contacts']:
            if user_contact['contact_value'] == phone_number and user['users'][0]['status'] == UserStatus.ANON.value:
                user_contact_data = dict(
                    contact_type=user_contact['contact_type'], contact_value=user_contact['contact_value'],
                    is_verified=True,
                    verified_through=VerifiedThrough.OTP.value,
                    is_whatsapp_opted_in=user_contact['is_whatsapp_opted_in'],
                    auth_id=user_contact.get('auth_id', None), last_login=user_data.get('last_login', None),
                    country_code=user_contact.get('country_code', '91')
                )
                data = {"data": user_contact_data}
                self.user_profile_service_client.update_user_contact(data, user['users'][0]['user_id'],
                                                                     user_contact['contact_id'])
                return

    def update_anon_user_details(self, user, user_data):
        first_name, last_name = self.get_name_breakup(user_data['name'])

        name = dict(first_name=first_name, middle_name=None, last_name=last_name)

        data = {
            "data": {
                "name": name,
            },
        }
        self.user_profile_service_client.update_user_details(user[0]['user_id'], data)

    def create_b2c_user_profile(self, users):
        has_b2c_user_profile, user = self.extract_b2c_user_profile(users)
        if not has_b2c_user_profile:
            self.create_new_user_profile(user)

    def extract_b2c_user_profile(self, users):
        has_b2c_user_profile = False
        user_profiles = self.user_profile_service_client.get_user_profile(user_id=users[0]['user_id'])
        if not user_profiles:
            return False, users[0]
        for user_profile in user_profiles:
            if user_profile['profile_type'] == ProfileType.B2C.value:
                return True, users[0]
        return has_b2c_user_profile, users[0]

    @staticmethod
    def get_name_breakup(name):
        if not name:
            name = 'guest'

        if not isinstance(name, str):
            try:
                name = ' '.join([str(char) for char in name if char])
            except TypeError:
                raise ValueError('name: {n} must be a string or iterable'.format(n=name))

        name = name.split(' ', 1)

        if not name:
            raise RuntimeError('Unable to split name: {n}. This should not happen.'.format(n=name))

        if len(name) > 1:
            fname, lname = name[0], name[1]
        else:
            fname, lname = name[0], None

        return fname, lname

    def get_whatsapp_optin_status(self, phone_number):
        user = self.user_profile_service_client.get_user_details(phone_number=phone_number)
        if not user['users']:
            return False

        for user_contact in user['users'][0]['user_contacts']:
            if user_contact['contact_value'] == phone_number:
                return user_contact['is_whatsapp_opted_in']
        return False
