from rest_framework import serializers
from apps.common.validators import CustomValidator as custom_validator
from dbcommon.models.profile import User


class UpdateProfileDTO(serializers.Serializer):
    email = serializers.EmailField(max_length=40, allow_null=True)
    auth_id = serializers.IntegerField()
    phone_number = serializers.CharField(max_length=40, allow_null=True)
    first_name = serializers.CharField(max_length=40, allow_null=True)
    last_name = serializers.CharField(max_length=40, allow_null=True)
    gender = serializers.ChoiceField(
        choices=User.GENDER_CHOICES, allow_blank=True)

    def validate_email(self, email):
        return custom_validator.normalise_email(email)

    def validate_phone_number(self, phone_number):
        return custom_validator.normalise_phone_number(phone_number)
