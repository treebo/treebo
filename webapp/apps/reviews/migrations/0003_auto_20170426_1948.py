# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0002_auto_20170426_1312'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hoteltaoverallratings',
            name='overall_rating',
            field=models.DecimalField(
                default=0.0,
                max_digits=20,
                decimal_places=1),
        ),
        migrations.AlterField(
            model_name='tahotelreviews',
            name='user_rating',
            field=models.DecimalField(
                default=0.0,
                max_digits=20,
                decimal_places=1),
        ),
        migrations.AlterField(
            model_name='tahotelsubratingmapping',
            name='subrating_value',
            field=models.DecimalField(
                default=0.0,
                max_digits=20,
                decimal_places=1),
        ),
        migrations.AlterField(
            model_name='tareviewratingcountmapping',
            name='percentage_of_customers',
            field=models.DecimalField(
                default=0.0,
                max_digits=20,
                decimal_places=2),
        ),
    ]
