from django.apps import AppConfig

default_app_config = 'apps.pricing.PricingAppConfig'

class PricingAppConfig(AppConfig):

    name = 'apps.pricing'

    def ready(self):
        from . import signals
