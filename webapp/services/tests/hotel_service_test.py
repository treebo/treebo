import datetime
import uuid
from collections import namedtuple
from datetime import timedelta

from django.test.testcases import TestCase
from mock.mock import MagicMock
from model_mommy import mommy

from common.constants import common_constants
from common.exceptions.treebo_exception import TreeboValidationException
from dbcommon.models.hotel import Hotel
from dbcommon.models.reservation import Reservation, Booking
from dbcommon.models.room import Room
from apps.hotels.service.hotel_service import HotelService
from services.hotellogix.web_services import Hotelogix


class HotelServiceTest(TestCase):
    def setUp(self):
        hotelogix = Hotelogix()
        hotelogix.buildDetailsXml = MagicMock(return_value="<xml></xml>")
        hotelogix.makeHotelogixCall = MagicMock(return_value="")
        self.__hotelService = HotelService()
        self.__makeTonsOfHotels()

    def __getStayDates(self, checkIn, checkOut):
        StayDates = namedtuple('StayDates', 'checkInDate checkOutDate')
        dates = StayDates(datetime.datetime.strptime(checkIn, "%Y-%m-%d"),
                          datetime.datetime.strptime(checkOut, "%Y-%m-%d"))
        return dates

    def test_shouldFindAllHotelsAvailableBetweenDates(self):
        hotels = Hotel.objects.all()
        dates = self.__getStayDates('2015-10-20', '2015-10-24')
        hotels = self.__hotelService.getAvailabilityAndPriceFromDb(
            hotels, dates.checkInDate, dates.checkOutDate)
        self.assertEqual(hotels[0].minRate, 1299.00)
        self.assertTrue(hotels[0].availability)
        self.assertEqual(hotels[0].minRooms, 2)

        self.assertEqual(hotels[1].minRate, 1599.00)
        self.assertTrue(hotels[1].availability)
        self.assertEqual(hotels[1].minRooms, 3)

        self.assertEqual(hotels[2].minRate, 1599.00)
        self.assertTrue(hotels[2].availability)
        self.assertEqual(hotels[2].minRooms, 2)

    def test_shouldNotShowAvailabilityInSoldOutHotel(self):
        hotels = Hotel.objects.all()
        dates = self.__getStayDates('2015-10-20', '2015-10-24')
        hotels = self.__hotelService.getAvailabilityAndPriceFromDb(
            hotels, dates.checkInDate, dates.checkOutDate)
        self.assertEqual(hotels[3].minRate, "N/A")
        self.assertFalse(hotels[3].availability)
        self.assertEqual(hotels[3].minRooms, None)

    def test_shouldRevertToPrevBestPriceAndAvailability(self):
        hotels = Hotel.objects.all()
        dates = self.__getStayDates('2015-10-20', '2015-10-24')
        hotels = self.__hotelService.getAvailabilityAndPriceFromDb(
            hotels, dates.checkInDate, dates.checkOutDate)
        self.assertEqual(hotels[4].minRate, 1599.00)
        self.assertTrue(hotels[4].availability)
        self.assertEqual(hotels[4].minRooms, 1)

    def test_shouldThrowErrorWithInvalidDates(self):
        hotels = Hotel.objects.all()
        dates = self.__getStayDates('2015-10-24', '2015-10-22')
        with self.assertRaises(TreeboValidationException):
            hotels = self.__hotelService.getAvailabilityAndPriceFromDb(
                hotels, dates.checkInDate, dates.checkOutDate)

    def test_shouldReturnTheLatestBookingTime(self):
        hotel = Hotel.objects.get(name='A')
        time = self.__hotelService.getLastBookingTime(hotel)
        self.assertIsNotNone(time)

    def __makeTonsOfHotels(self):
        ReservationTuple = namedtuple(
            'ReservationTuple',
            'checkInDate checkOutDate units type')
        hotel = self.__makeHotel('A',
                                 [ReservationTuple('2015-10-19', '2015-10-23', 1, 'A'),
                                  ReservationTuple('2015-10-20', '2015-10-23', 1, 'B'),
                                  ReservationTuple('2015-10-18', '2015-10-19', 1, 'B'),
                                  ReservationTuple('2015-10-21', '2015-10-25', 1, 'A'),
                                  ReservationTuple('2015-10-20', '2015-10-21', 2, 'B')])
        self.__makeHotel('B',
                         [ReservationTuple('2015-10-12', '2015-10-23', 2, 'A'),
                          ReservationTuple('2015-10-18', '2015-10-20', 1, 'B'),
                          ReservationTuple('2015-10-19', '2015-10-20', 1, 'B'),
                          ReservationTuple('2015-10-21', '2015-10-22', 2, 'A'),
                          ReservationTuple('2015-10-22', '2015-10-23', 1, 'B')])
        self.__makeHotel('C',
                         [ReservationTuple('2015-10-12', '2015-10-23', 2, 'A'),
                          ReservationTuple('2015-10-18', '2015-10-20', 2, 'B'),
                          ReservationTuple('2015-10-19', '2015-10-20', 1, 'B'),
                          ReservationTuple('2015-10-21', '2015-10-22', 2, 'A'),
                          ReservationTuple('2015-10-22', '2015-10-23', 2, 'B')])
        self.__makeHotel('D',
                         [ReservationTuple('2015-10-12', '2015-10-23', 2, 'B'),
                          ReservationTuple('2015-10-18', '2015-10-26', 2, 'A'),
                          ReservationTuple('2015-10-21', '2015-10-24', 2, 'A'),
                          ReservationTuple('2015-10-21', '2015-10-22', 2, 'B')])
        self.__makeHotel('E',
                         [ReservationTuple('2015-10-12', '2015-10-24', 1, 'A'),
                          ReservationTuple('2015-10-18', '2015-10-26', 2, 'B'),
                          ReservationTuple('2015-10-21', '2015-10-24', 1, 'B'),
                          ReservationTuple('2015-10-20', '2015-10-24', 1, 'A'),
                          ReservationTuple('2015-10-22', '2015-10-24', 1, 'A'),
                          ReservationTuple('2015-10-23', '2015-10-24', 1, 'A')])
        midnight = datetime.datetime.now().replace(
            hour=0, minute=0, second=0, microsecond=0)
        mommy.make(
            Booking,
            created_at=midnight +
            timedelta(
                hours=1,
                minutes=20,
                seconds=15),
            hotel=hotel)
        mommy.make(
            Booking,
            created_at=midnight +
            timedelta(
                hours=12,
                minutes=12,
                seconds=22),
            hotel=hotel)
        mommy.make(
            Booking,
            created_at=midnight +
            timedelta(
                hours=12,
                minutes=12,
                seconds=15),
            hotel=hotel)

    def __makeHotel(self, hotelName, reservations):
        hotel = mommy.make(Hotel,
                           name=hotelName,
                           uuid=uuid.uuid4(),
                           _fill_optional=True)

        roomA = mommy.make(Room,
                           hotel=hotel,
                           room_type='R1',
                           price=1299,
                           quantity=4, _fill_optional=True)

        roomB = mommy.make(Room,
                           hotel=hotel,
                           room_type='R2',
                           price=1599,
                           quantity=4, _fill_optional=True)

        for reservation in reservations:
            self.__makeSomeReservations(
                hotel, roomA if reservation.type is 'A' else roomB, reservation)
        return hotel

    def __makeSomeReservations(self, hotel, room, reservation):
        mommy.make(
            Reservation,
            reservation_status=common_constants.RESERVED,
            reservation_checkin_date=datetime.datetime.strptime(
                reservation.checkInDate,
                "%Y-%m-%d").date(),
            reservation_checkout_date=datetime.datetime.strptime(
                reservation.checkOutDate,
                "%Y-%m-%d").date(),
            room=room,
            room_unit=reservation.units)
