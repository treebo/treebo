# -*- coding: utf-8 -*-


import jsonfield.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('content', '0002_contentstore'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contentstore',
            name='data',
        ),
        migrations.AddField(
            model_name='contentstore',
            name='value',
            field=jsonfield.fields.JSONField(default=''),
        ),
    ]
