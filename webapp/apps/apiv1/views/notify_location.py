import logging

from django.conf import settings
from django.core.mail import EmailMessage

from base import log_args
from base.views.api import TreeboAPIView
from common.constants import common_constants
from dbcommon.models.location import NotifyNewLocation

__author__ = 'amithsoman'

logger = logging.getLogger(__name__)


class NotifyLocation(TreeboAPIView):
    def dispatch(self, *args, **kwargs):
        return super(NotifyLocation, self).dispatch(*args, **kwargs)

    def post(self, request):
        raise Exception('Method not supported')

    @log_args(logger)
    def get(self, request):
        data = request.GET
        location = data.get('location')
        email = data.get('email')
        query = data.get('query')

        # send mail
        if settings.NOTIFY_MAIL:
            city = data.get('city')
            subject = common_constants.NOTIFY_LOCATION_EMAIL_SUBJECT % (
                city, location)
            receiver = common_constants.NOTIFY_LOCATION_EMAIL
            content = common_constants.NOTIFY_LOCATION_EMAIL_CONTENT % (
                location, city, email)
            msg = EmailMessage(
                subject,
                content,
                settings.EMAIL_HOST_USER,
                [receiver])
            msg.send()

        # data save in db
        notify_location_obj = NotifyNewLocation()
        notify_location_obj.email = email
        notify_location_obj.location = location
        notify_location_obj.query = query
        notify_location_obj.save()
        data = {
            "status": "success",
            "msg": "Notification request saved successfully"}
