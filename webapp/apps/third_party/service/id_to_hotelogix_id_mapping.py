import io
import csv
import logging

from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.text import slugify

from apps.third_party.service.google_hotel_ads import constants
from data_services.respositories_factory import RepositoriesFactory
from dbcommon.models.hotel import Hotel
from data_services.hotel_repository import HotelRepository

logger = logging.getLogger(__name__)

IVR_CSV_EMAIL_ID = [
    'snehil.rastogi@treebohotels.com',
    'siddarth.shukla@treebohotels.com']
IVR_CSV_EMAIL_SUBJECT = 'Mapping of Website ID to Hotelogix ID for Trip Advisor'
IVR_CSV_EMAIL = 'Please find attached mapping excel sheet which maps website\'s hotel id to hotelogix id and also contains the URL ' \
                'of the Hotel Description Page'

hotel_repository = RepositoriesFactory.get_hotel_repository()


class MappingService:
    @classmethod
    def mapping_service(cls):
        try:
            hotels = hotel_repository.get_active_hotel_list()
            #hotel_data = Hotel.objects.filter(status="1")
            logger.info(
                " Initializing CSV for mapping from website hotel id to hotelogix hotel id ", )
            csvfile = io.StringIO()
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['Hotel_Name',
                                'Website - Hotel_ID',
                                'Hotelogix - Hotel_ID',
                                'URL_Hotel_Description_Page'])
            for hotel in hotels:
                sitemap_url = hotel_repository.get_sitemap_url(hotel)
                csvwriter.writerow(
                    [slugify(hotel.name), hotel.id, (hotel.hotelogix_id),
                     "https://" + constants.BASE_URL + sitemap_url]
                )

            logger.info("Mappings")
            mail = EmailMessage(
                IVR_CSV_EMAIL_SUBJECT,
                IVR_CSV_EMAIL,
                settings.SERVER_EMAIL,
                IVR_CSV_EMAIL_ID)
            mail.attach(
                'mapping_trip_advisor.csv',
                csvfile.getvalue(),
                'text/csv')
            mail.send()

        except Exception as e:
            raise e
