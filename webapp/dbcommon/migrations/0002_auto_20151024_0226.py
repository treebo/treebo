# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConstraintExpression',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('expression', models.CharField(default=b'', max_length=2000, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConstraintKeywords',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('keyword', models.CharField(default=b'', max_length=200, null=True)),
                ('type', models.CharField(default=b'', max_length=20, null=True)),
                ('description', models.CharField(default=b'', max_length=1000, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConstraintOperators',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('operator', models.CharField(default=b'', max_length=10, null=True)),
                ('description', models.CharField(default=b'', max_length=100, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DiscountCoupon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('code', models.CharField(default=b'', unique=True, max_length=200)),
                ('discount_operation', models.CharField(default=b'OTHER', max_length=20,
                                                        choices=[(b'FIXED', b'Fixed'),
                                                                 (b'PERCENTAGE', b'Percentage'),
                                                                 (b'CASHBACK', b'CashBack'),
                                                                 (b'ROOMNIGHTS', b'RoomNights'),
                                                                 (b'OTHER', b'Other')])),
                ('discount_value', models.DecimalField(default=0, max_digits=5, decimal_places=1)),
                ('max_available_usages', models.IntegerField(default=0)),
                ('max_available_discount', models.PositiveIntegerField(default=0)),
                ('expiry', models.DateField(null=True, blank=True)),
                ('is_active', models.BooleanField(default=False)),
                ('tag_line', models.CharField(default=b'', max_length=200, null=True)),
                ('terms', models.CharField(default=b'', max_length=5000, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DiscountCouponConstraints',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True,
                                        primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('type', models.CharField(max_length=200, null=True, blank=True)),
                ('value_one', models.CharField(max_length=200, null=True, blank=True)),
                ('value_two', models.CharField(max_length=200, null=True, blank=True)),
                ('coupon', models.ForeignKey(default=b'', to='dbcommon.DiscountCoupon', null=True, on_delete=models.DO_NOTHING)),
                ('keyword',
                 models.ForeignKey(default=b'', to='dbcommon.ConstraintKeywords', null=True, on_delete=models.DO_NOTHING)),
                ('operation',
                 models.ForeignKey(default=b'', to='dbcommon.ConstraintOperators', null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='constraintexpression',
            name='coupon',
            field=models.ForeignKey(blank=True, to='dbcommon.DiscountCoupon', null=True, on_delete=models.DO_NOTHING),
        ),
    ]
