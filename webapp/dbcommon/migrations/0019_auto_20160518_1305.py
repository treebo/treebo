# -*- coding: utf-8 -*-


from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('dbcommon', '0018_auto_20160518_1006'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='amenity',
            options={'verbose_name_plural': 'Amenities'},
        ),
        migrations.AlterModelOptions(
            name='amenitymembership',
            options={},
        ),
        migrations.AlterModelOptions(
            name='city',
            options={'verbose_name_plural': 'Cities'},
        ),
        migrations.AlterModelOptions(
            name='cityalias',
            options={},
        ),
        migrations.AlterModelOptions(
            name='cityblog',
            options={},
        ),
        migrations.AlterModelOptions(
            name='cityseodetails',
            options={},
        ),
        migrations.AlterModelOptions(
            name='coupon',
            options={},
        ),
        migrations.AlterModelOptions(
            name='coupon_constraint',
            options={},
        ),
        migrations.AlterModelOptions(
            name='coupon_date_constraint',
            options={},
        ),
        migrations.AlterModelOptions(
            name='directions',
            options={},
        ),
        migrations.AlterModelOptions(
            name='emaillinkhashvalue',
            options={},
        ),
        migrations.AlterModelOptions(
            name='facility',
            options={'verbose_name_plural': 'Facilities'},
        ),
        migrations.AlterModelOptions(
            name='feedback',
            options={},
        ),
        migrations.AlterModelOptions(
            name='highlight',
            options={},
        ),
        migrations.AlterModelOptions(
            name='hotel',
            options={},
        ),
        migrations.AlterModelOptions(
            name='landmark',
            options={},
        ),
        migrations.AlterModelOptions(
            name='landmarktype',
            options={},
        ),
        migrations.AlterModelOptions(
            name='locality',
            options={'verbose_name_plural': 'Localities'},
        ),
        migrations.AlterModelOptions(
            name='notificationarchive',
            options={},
        ),
        migrations.AlterModelOptions(
            name='notifynewlocation',
            options={},
        ),
        migrations.AlterModelOptions(
            name='seodetails',
            options={},
        ),
        migrations.AlterModelOptions(
            name='state',
            options={},
        ),
        migrations.AlterModelOptions(
            name='userdiscount',
            options={},
        ),
        migrations.AlterModelOptions(
            name='usermetadata',
            options={},
        ),
        migrations.AlterModelOptions(
            name='userotp',
            options={},
        ),
    ]
