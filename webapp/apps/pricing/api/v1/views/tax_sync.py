import logging

from rest_framework.response import Response

from apps.pricing.event_publishers import publish_taxes, publish_hotel_wise_luxury_taxes
from apps.pricing.services.tax_sync_service import TaxSyncService
from base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class TaxSyncTrigger(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        state_id = kwargs.get('state_id')
        logger.info("Tax Sync request received for State ID: %s", state_id)
        try:
            if state_id:
                tax_sync_service = TaxSyncService()
                state_taxes, luxury_taxes = tax_sync_service.get_state_wise_taxes(
                    state_id)
                publish_taxes(state_id, state_taxes, luxury_taxes)
                return Response({"message": "Tax sync initiated."}, status=202)
        except BaseException:
            logger.exception("Tax Sync Failed for state_id: %s", state_id)
            return Response({"message": "Tax Sync Failed"}, status=400)


class LuxuryTaxSyncTrigger(TreeboAPIView):
    def get(self, request, *args, **kwargs):
        hotel_id = kwargs.get('hotel_id')
        logger.info("Tax Sync request received for Hotel ID: %s", hotel_id)
        try:
            if hotel_id:
                tax_sync_service = TaxSyncService()
                state, luxury_taxes = tax_sync_service.get_hotel_wise_taxes(
                    hotel_id)
                publish_hotel_wise_luxury_taxes(
                    state.id, hotel_id, luxury_taxes)
                return Response({"message": "Tax sync initiated."}, status=202)
        except BaseException:
            logger.exception("Tax Sync Failed for hotel_id: %s", hotel_id)
            return Response({"message": "Tax Sync Failed"}, status=400)
