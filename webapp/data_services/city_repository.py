from base import log_args
from dbcommon.models.location import City
from data_services.decorators import SingletonDecorator


@SingletonDecorator
class CityRepository(object):

    def __init__(self, city_web_repository):
        self.city_web_repository = city_web_repository

    @log_args()
    def get_all_cities(self):
        return self.city_web_repository.get_all_cities()

    @log_args()
    def filter_cities(self, **kwargs):
        return self.city_web_repository.filter_cities(**kwargs)

    @log_args()
    def get_single_city(self, **kwargs):
        return self.city_web_repository.get_single_city(**kwargs)

    # For string matching function, field is
    # tuple containing name of field and the value of string to match(Condition is or)
    # Django's equivalent of filter
    @log_args()
    def filter_cities_by_string_matching(self, fields_list, filter_params={}):
        return self.city_web_repository.filter_cities_by_string_matching(
            fields_list, filter_params)

    @log_args()
    def get_by_slug(self, slug):
        return self.city_web_repository.get_by_slug(slug)

    @log_args()
    def get_all_cities_for_search_string(self, query, alias_query, original_search_query=None):
        return self.city_web_repository.get_all_cities_for_search_string(
            query, alias_query, original_search_query)

    @log_args()
    def get_city_with_related_entities_by_string_matching(
            self, prefetch_list, select_list, string_fields_list):
        cities = self.filter_cities_by_string_matching(
            fields_list=string_fields_list)
        if select_list:
            cities = cities.select_related(*select_list)
        if prefetch_list:
            cities = cities.prefetch_related(*prefetch_list)
        # if filter_params:
        #     return cities.filter(**filter_params)
        return cities.all()

    @log_args()
    def get_city_with_related_entities(
            self,
            select_list,
            prefetch_list,
            filter_params):
        query = City.objects
        if select_list:
            query = query.select_related(*select_list)
        if prefetch_list:
            query = query.prefetch_related(*prefetch_list)
        if filter_params:
            return query.filter(**filter_params)
        return query.all()

    # TODO: Define the string popular_cities, Rename
    @log_args()
    def get_popular_cities(self, popular_cities, city_exclude_list):
        return City.objects.filter(
            enable_for_seo=True,
            status=1,
            name__iregex=r'(' +
            '|'.join(popular_cities) +
            ')'). exclude(
            id__in=city_exclude_list)

    def get_enabled_cities_count(self):
        return City.objects.filter(status=1).count()


class CityDoesNotExist(Exception):
    pass


def get_query_string(case_sensitive, partial):
    if case_sensitive and partial:
        return 'contains'
    if not case_sensitive and partial:
        return 'icontains'
    if case_sensitive and not partial:
        return 'exact'
    if not case_sensitive and not partial:
        return 'iexact'
    return None
