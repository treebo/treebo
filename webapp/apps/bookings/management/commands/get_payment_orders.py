from decimal import Decimal

from django.core.management.base import BaseCommand

from apps.bookings.models import Booking
from services.hotellogix.hx import HxService


class Command(BaseCommand):
    help = """Compares prices in db and hx for a set of order ids. Options --file=<path_of_file>.
            Comma separated order ids in file"""

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--file',
            default=1,
            type=str,
            help='Path of file to read orders from.')

    def handle(self, *args, **options):
        orders_file_path = options.get('file')
        orders_file = open(orders_file_path)
        orders = orders_file.readline().split(",")
        orders_with_diff = self.find_orders_with_diff(orders)
        print(orders_with_diff)

    def get_order(self, booking):
        access_key, access_secret = HxService.renew_auth_token(
            booking.hotel.get_consumer_key(), booking.hotel.get_consumer_secret())
        hx_keys = {'accesskey': access_key, 'accesssecret': access_secret}
        order = HxService.get_order_from_hotelogix(
            booking.order_id,
            hx_keys,
            booking.hotel.get_consumer_key(),
            booking.hotel.get_consumer_secret())
        return order

    def get_order_amount_from_xml(self, order_xml):
        response = order_xml('response')
        order = response('order')
        order_amount = order('orderamount')
        amount = order_amount.attr('amount')
        return Decimal(amount)

    def find_orders_with_diff(self, orders):
        orders_with_diff = []
        for order in orders:
            if len(order) > 0:
                booking = Booking.objects.select_related(
                    'hotel').get(order_id=order)
                amount = self.get_order_amount_from_xml(
                    self.get_order(booking))
                if abs(amount - booking.total_amount) > 10:
                    print(order)
                    orders_with_diff.append(order)
        return orders_with_diff
