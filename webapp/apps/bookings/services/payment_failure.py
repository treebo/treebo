from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from apps.bookings.models import Booking
from services.restclient.treebo_notificationservice import TreeboNotificationService
import logging

logger = logging.getLogger(__name__)


class PaymentFailureHandler():
    @staticmethod
    def handle_addons(data):
        """
        :param data:
        :return:
        [
            {
                "failure_mail": {
                    "booking_id": booking_id,
                    "reason": reason,
                }
            },
            {
                "exception_mail": {
                    "subject": subject,
                    "message": message,
                    "html": html
                }
            },
            {
                'cancellation_mail': {
                'subject': 'Cancellation Failed for Order Id: {0}'.format(order_id),
                'body': 'Cancellation request failed for Order ID: {0}. Reason: Unable to get order details from '
                        ''.format(order_id),
                'from': ,
                'to': settings.CANCELLATION_FAILURE_EMAIL_LIST
            }
        ]
        """
        logger.info('handle addons called with data : {0}'.format(data))

        _booking_id = data['data'].get('booking_id', None)
        try:
            booking = Booking.objects.select_related('hotel').get(pk=_booking_id)
        except ObjectDoesNotExist:
            logger.exception('Booking not found with id {0}'.format(_booking_id))
            return

        subject = 'Payment Update to HX failed for order_id %s' % booking.order_id
        TreeboNotificationService().send_email(emails=settings.RESERVATION_FAILURE_EMAIL_LIST,
                                               content=subject, subject=subject)

