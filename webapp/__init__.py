

import site
from .celeryapp import app as celery_app
from .conf import rel

__all__ = ['celery_app']

site.addpackage(rel(), "apps.pth", known_paths=set())
