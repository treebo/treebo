import logging
from collections import defaultdict, OrderedDict
from apps.hotels.templatetags.url_tag import get_image_uncode_url
from apps.reviews.services.trip_advisor.review_config_service import ReviewAppConfigService
from apps.reviews.models import TAHotelIDMapping
from apps.reviews.services.review_response_data.user_review_data import UserReviewData
from data_services.respositories_factory import RepositoriesFactory
import urllib.parse

logger = logging.getLogger(__name__)


def get_categorised_images(hotel_images):
    categorised_images = defaultdict(list)
    image_repository = RepositoriesFactory.get_image_repository()
    if not hotel_images:
        return categorised_images
    for image in hotel_images:
        image_dict = {
            "url": get_image_uncode_url(image.url.url),
            "tagline": image.tagline}
        room_type = image_repository.get_room_type_from_image(image)
        if not room_type:
            categorised_images["common"].append(image_dict)
        else:
            categorised_images[room_type.lower()].append(image_dict)

    return categorised_images


def get_categorised_images_v5(hotel_images):
    categorised_images = OrderedDict()
    if not hotel_images:
        return categorised_images

    for image in hotel_images:
        image_name, image_callout = image.get_image_name_and_callout()
        image_dict = {
            "id": image.id,
            "url": get_image_uncode_url(image.url.url),
            "tagline": image.tagline,
            "image_callout": image_callout,
            "image_name": image_name,
            "is_showcased": image.is_showcased,
        }
        categorised_images.setdefault(image.category, []).append(image_dict)

    return categorised_images


def get_hotel_directions(hotel_directions):
    direction_css_dict_list = []
    for direction in hotel_directions:
        direction_css_dict = {}
        landmark_name = direction.landmark_obj.name
        direction_css_dict['name'] = landmark_name
        direction_css_dict['distance'] = direction.distance
        direction_css_dict['mode'] = direction.mode
        if landmark_name:
            if landmark_name.lower().find('airport') != -1:
                direction_css_dict['css_class'] = 'airport'
            elif landmark_name.lower().find('railway') != -1:
                direction_css_dict['css_class'] = 'railway'
            elif landmark_name.lower().find('bus') != -1:
                direction_css_dict['css_class'] = 'bustand'
            else:
                direction_css_dict['css_class'] = ''
        else:
            direction_css_dict['css_class'] = ''
        direction_css_dict_list.append(direction_css_dict)
    return direction_css_dict_list


def get_categorised_guest_supported_info(rooms_list):
    supported_guest_info = []
    room_data_service = RepositoriesFactory.get_room_repository()
    for room in rooms_list:
        room_facilities_list = [
            {"name": facility.name, "css_class": facility.css_class}
            for facility in room_data_service.get_facility_set_for_room(room)
        ]

        supported_guest_info.append({
            "type": room.room_type_code.lower(),
            "facilities": room_facilities_list,
            "max_adults": room.max_guest_allowed,
            "max_guests": room.max_adult_with_children,
            "max_children": room.max_children,
            "size": room.room_area,
            "facilities_count": len(room_facilities_list),
            "occupancy": room_data_service.get_occupancy_string_for_room(room),
            "room_count": room.quantity,
            "name": room.external_room_details.name.title(),
            "description": room.description
        })
    return supported_guest_info


def get_user_reviews(hotel):
    reviews = {}
    try:
        user_reviews = []
        app_config = ReviewAppConfigService()
        if app_config.check_toggle_settings(str(hotel.id)):
            ta_hotel_map = TAHotelIDMapping.objects.get(hotel=hotel.id)
            user_review_data = UserReviewData(
                hotel.id, ta_hotel_map.ta_location_id)
            user_reviews = user_review_data.build()
            reviews = {
                'isTripAdvisorEnabled': True,
                'topReviews': user_reviews
            }

        else:
            reviews = {
                'isTripAdvisorEnabled': False,
                'topReviews': user_reviews
            }
    except TAHotelIDMapping.DoesNotExist:
        logger.exception(
            "TAHotelIDMapping does not exist for hotel %s",
            hotel.id)

    except TAHotelIDMapping.MultipleObjectsReturned:
        logger.exception(
            "TAHotelIDMapping multiple entries for hotel %s",
            hotel.id)

    except Exception:
        logger.exception(
            "Exception in getting the user filled revews for hotel %s %s",
            hotel.id,
            hotel.hotelogix_id)
    return reviews
