import csv
import datetime
import logging

import pytz
from django.conf import settings
from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand

from apps.bookings.models import Booking

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Generate Booking Report for previous day or for \'delta\' number of days going back from previous day'

    def add_arguments(self, parser):
        parser.add_argument(
            '--delta',
            default=1,
            type=int,
            help='Days for which to run the script')

    def handle(self, *args, **options):
        delta = int(options.get('delta'))
        tz = pytz.timezone("Asia/Kolkata")
        today = datetime.datetime.now(tz).date()
        midnight = tz.localize(
            datetime.datetime.combine(
                today, datetime.time(
                    0, 0)), is_dst=None)
        today_midnight_in_utc = midnight.astimezone(pytz.utc)
        start_day_midnight = today_midnight_in_utc - \
            datetime.timedelta(days=delta)

        start_date = start_day_midnight
        end_date = today_midnight_in_utc

        bookings = Booking.objects.filter(
            created_at__gte=start_date,
            created_at__lt=end_date) .filter(
            status=Booking.CONFIRM).filter(
            is_complete=True) .select_related(
                'hotel',
                'hotel__city',
                'room',
            'user_id') .order_by('created_at')

        logger.info('Generating Booking Report')
        with open('booking_report.csv', 'wb') as csvfile:
            writer = csv.writer(csvfile, delimiter='\t')
            headers = [
                'hotel_name',
                'city_name',
                'id',
                'created_at',
                'modified_at',
                'order_id',
                'checkin_date',
                'checkout_date',
                'adult_count',
                'child_count',
                'total_amount',
                'guest_name',
                'guest_email',
                'guest_mobile',
                'is_complete',
                'hotel_id',
                'room_id',
                'user_id',
                'is_traveller',
                'payment_amount',
                'comments',
                'booking_code',
                'coupon_apply',
                'coupon_code',
                'discount',
                'payment_mode',
                'group_code',
                'status',
                'paymentorder_id',
                'pretax_amount',
                'tax_amount',
                'cancellation_datetime',
                'channel']

            writer.writerow(headers)

            for b in bookings:
                row = [
                    b.hotel.name,
                    b.hotel.city.name,
                    b.id,
                    b.created_at,
                    b.modified_at,
                    b.order_id,
                    b.checkin_date,
                    b.checkout_date,
                    b.adult_count,
                    b.child_count,
                    b.total_amount,
                    b.guest_name,
                    b.guest_email,
                    b.guest_mobile,
                    b.is_complete,
                    b.hotel.id,
                    b.room.id,
                    b.user_id.id,
                    b.is_traveller,
                    b.payment_amount,
                    b.comments,
                    b.booking_code,
                    b.coupon_apply,
                    b.coupon_code,
                    b.discount,
                    b.payment_mode,
                    b.group_code,
                    b.status,
                    b.paymentorder_id,
                    b.pretax_amount,
                    b.tax_amount,
                    b.cancellation_datetime,
                    b.channel]
                writer.writerow(row)

        logger.info('Sending Booking Report')
        if delta == 1:
            subject = 'Booking Report for Date: {0}'.format(start_date.date())
        else:
            subject = 'Booking Report for Dates: {0} - {1}'.format(
                start_date.date(), end_date.date())
        mail = EmailMessage(
            subject,
            '',
            settings.SERVER_EMAIL,
            settings.BOOKING_REPORT_MAIL_LIST)
        mail.attach_file('booking_report.csv')
        mail.send()
        logger.info('Sent Booking Report')
