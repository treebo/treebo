import unittest
from django.test import TestCase
import datetime
from apps.bookingstash.models import Availability
from apps.checkout.models import BookingRequest
from base.middlewares.request import get_domain
from django.core.urlresolvers import reverse

from dbcommon.models.hotel import Hotel
from dbcommon.models.location import City, State, Locality
from dbcommon.models.room import Room


class CheckoutTestCase(TestCase):
    def setUp(self):
        self.bid = 1
        self.adults = 1
        self.child = 1
        self.room_required = 1
        self.discount_value = 0.0
        self.booking_channel = 'website'
        self.checkin_date = '2017-08-22'
        self.checkout_date = '2017-08-23'
        self.hotel_id = 61
        self.hotelogix_id = 12744
        self.room_id = 161
        self.mm_promo = 899.00
        self.payment_mode = 'Not Paid'
        self.pretax_amount = 899.00
        self.tax_amount = 107.94
        self.total_amount = 1007.44
        self.payment_amount = 0.00
        self.voucher_amount = 0.00
        self.otp_verified_number = False
        self.room_config = "%s-%s" % (self.adults, self.child)
        self.room_type = 'Oak (Standard)'
        self.max_guest_allowed = '3'
        self.max_adult_with_children = '2'
        self.max_children = '1'
        self.checkin_time = '11:00'
        self.checkout_time = '12:00'
        self.max_children = '1'
        self.url = reverse('checkout_v3:paynow-v3')

        self.booking_request = BookingRequest.objects.get_or_create(
            id=self.bid,
            adults=self.adults,
            children=self.child,
            room_required=self.room_required,
            discount_value=self.discount_value,
            booking_channel=self.booking_channel,
            checkin_date=self.checkin_date,
            checkout_date=self.checkout_date,
            hotel_id=self.hotel_id,
            mm_promo=self.mm_promo,
            payment_mode=self.payment_mode,
            pretax_amount=self.pretax_amount,
            tax_amount=self.tax_amount,
            payment_amount=self.payment_amount,
            voucher_amount=self.voucher_amount,
            otp_verified_number=self.otp_verified_number,
            total_amount=self.total_amount,
            room_config=self.room_config)

        self.city_name = 'Bangalore'
        self.state_name = 'Karnataka'
        self.locality_name = 'Koramangla'
        self.state, _ = State.objects.get_or_create(id=1, name=self.state_name)
        self.city, _ = City.objects.get_or_create(
            id=1, name=self.city_name, state=self.state)
        self.locality, _ = Locality.objects.get_or_create(
            id=1, name=self.locality_name, city=self.city, latitude='333.00', longitude=333.00, pincode=560102)
        self.hotel, _ = Hotel.objects.get_or_create(name='Treebo Demo', id=self.hotel_id, hotelogix_id=self.hotelogix_id,
                                                    checkin_time=self.checkin_time, locality=self.locality, checkout_time=self.checkout_time,
                                                    city=self.city, tagline='asdf', phone_number='phone_number', street='asdf', landmark='',
                                                    price_increment='10.0')
        self.room, _ = Room.objects.get_or_create(hotel=self.hotel, room_type=self.room_type, id=self.room_id,
                                                  max_guest_allowed=self.max_guest_allowed, quantity=3, available=2,
                                                  max_adult_with_children=self.max_adult_with_children, max_children=self.max_children, price=45.5)

    def tearDown(self):
        pass

    def __get_checkout_api(self, page_name):
        print(('page_name', page_name))
        print(('get_domain()', get_domain()))
        return get_domain() + reverse('checkout_v3:{0}'.format(page_name))

    def test_simple(self):
        assert 1 == 1

    def test_paynow_booking_not_found(self):
        data = {
            'bid': self.bid + 100
        }
        print((" url %s ", self.url))
        response = self.client.get(self.url, data, **{'referer': "/"})
        data = response.data
        self.assertTrue(response.status_code == 400, 'response is not 400')
        error = data.get('error')
        msg = error['msg']
        code = error['code']
        self.assertTrue('Booking Request Does not exist' in msg)
        self.assertTrue('5005' == code)

    def test_paynow_sold_out(self):
        data = {
            'bid': self.bid
        }
        print((" url %s ", self.url))
        response = self.client.get(self.url, data, **{'referer': "/"})
        data = response.data
        error = data.get('error')
        msg = error['msg']
        code = error['code']
        print(('msg ', msg))
        self.assertTrue('Room is sold out' in msg)
        self.assertTrue('5001' == code)

    def test_paynow_room_is_available(self):
        self.availablerooms = 2
        date = datetime.datetime.strptime(self.checkin_date, '%Y-%m-%d').date()
        print(("date ", date))
        self.availability = Availability.objects.get_or_create(
            room=self.room, availablerooms=self.availablerooms, date=date)
        data = {
            'bid': self.bid
        }
        print((" url %s ", self.url))
        response = self.client.get(self.url, data, **{'referer': "/"})
        data = response.data
        error = data.get('error')
        msg = error['msg']
        code = error['code']
        print(('msg ', msg))
        self.assertTrue('Room is sold out' in msg)
        self.assertTrue('5001' == code)
