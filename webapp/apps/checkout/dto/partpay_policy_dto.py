import enum
import decimal
from apps.common.exceptions.custom_exception import InvalidPartpayPolicyInContentStore

'''
sample of content to be stored in DB table contentStore
{
  "value": "20",
  "value_type": "enum[PERCENTAGE, ABSOLUTE]",
  "applicable_for": {
    "utm_sources": [
      "phonepe_app",
      "ixigo"
    ],
"platforms": [
      "msite",rum
      "website"
    ]
  }
}

'''

class PartpayValueType(enum.Enum):
    PERCENTAGE = "PERCENTAGE"
    ABSOLUTE = "ABSOLUTE"

    @staticmethod
    def from_string(key):
        if key in PartpayValueType.__members__:
            return PartpayValueType[key]
        raise InvalidPartpayPolicyInContentStore("the provided valueType in the partial "
                                                 "payment policy is not in {value_type}".format(
            value_type=list(PartpayValueType)))


class ApplicableFor(object):
    def __init__(self, utm_sources=None, platforms=None):
        self.utm_sources = utm_sources if utm_sources and isinstance(utm_sources, list) else None
        self.platforms = platforms if platforms and isinstance(platforms, list) else None

class PartpayPolicyDto(object):
    def __init__(self, value, value_type, default_percentage_value, applicable_for={}):
        self.value = decimal.Decimal(value)
        self.default_percentage_value = decimal.Decimal(default_percentage_value)
        self.value_type = PartpayValueType.from_string(value_type)
        self.applicable_for = ApplicableFor(**applicable_for)

