from django.conf.urls import url

from apps.hotels.api.v1.get_hotel_details import HotelDetails
from apps.hotels.api.v1.get_hotel_rooms import HotelRooms
from apps.hotels.api.v1.hotel_sync import HotelSyncTrigger
from apps.hotels.api.v1.hotel_images import HotelImages

urlpatterns = [
    url(r"^detail/(?P<hashedHotelId>\w+)/$", HotelDetails.as_view(), name='details'),
    url(r"^room/(?P<hashed_hotel_id>\w+)/$", HotelRooms.as_view(), name='room'),
    url(r"^(?P<hashedHotelId>\w+)/$", HotelDetails.as_view(), name='hotel-info'),
    url(r"^(?P<hashed_hotel_id>\w+)/rooms/$", HotelRooms.as_view(), name='hotel-rooms'),
    url(r"^(?P<hotel_id>\d+)/sync$", HotelSyncTrigger.as_view(), name='hotel_sync'),
    url(r"^(?P<hotel_id>[0-9]+)/images/$", HotelImages.as_view(), name='hotel-images-v1'),
]
