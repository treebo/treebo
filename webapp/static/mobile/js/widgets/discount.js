import _ from 'underscore';
import React from 'react';
import classNames from 'classnames';
var treejax = require('../treebolibs/treejax');
import parsley from 'parsleyjs';
import $ from 'jquery';


// TODO - Publish discount
// Total Discount



let ApplyScreen = React.createClass({
	getInitialState(){
		return {
			"currentcoupon":'',
			"couponerror": this.props.couponerror || ""
		}
	},

	componentDidMount(){
		var discountParsley = $('.discount__form').parsley();
		var self = this;
        discountParsley.on('form:submit',function(){
        	self.props.applyCoupon(self.state.currentcoupon);
            return false;
        });
	},

	componentWillReceiveProps(nextProps){
		this.setState({
			currentcoupon:nextProps.couponcode,
			couponerror:nextProps.couponerror
		});
	},

	handleCouponChange(evt){
		this.setState({currentcoupon: evt.target.value});
	},

	clickHandler(){
		this.setState({ couponerror:'' });
	},

	render(){
		var rootClass = classNames({
	      'apply-screen': true,
	      'hide': !this.props.visible
	    });

		return (

			<div className={rootClass}>
				<div className="flex-column js-applycouponwidget">
					<div className="flex-row">
						<form className="discount__form">
							<input className="analytics-voucher discount__voucher" required="true" value={this.state.currentcoupon} onChange={this.handleCouponChange}
                            data-parsley-required="true"

                            data-parsley-required-message="Sorry! This coupon code is not valid"
                            data-parsley-errors-container="#discountError" type="text" placeholder="Enter your code"/><input onClick={this.clickHandler} type="submit" value="Apply" className="analytics-apply flex-row flex--justify-center flex--align-center  btn discount__applybtn js-couponbtn"/>
		                </form>
	                </div>
	                <div id="discountError" className="error text-left">
	                	{this.state.couponerror}
	                </div>
				</div>
			</div>
		);
	}
});


let CouponHeader = React.createClass({
	render(){

		var rootClass = classNames({
		  'mb10':true,
	      'hide': !this.props.visible
	    });

		return (
			<div className={rootClass}>
					<span>Have a coupon ?</span>
					&nbsp;<a onClick={this.props.showApplyCouponWidget} className="js-applycoupon anchor" href="javascript:void(0);">Apply Coupon Code</a>
			</div>
		)
	}
});


var AppliedScreen = React.createClass({

	offerhandler(){
		window.location = "/offers/#"+this.props.couponcode;
	},

	discountRemoveHandler(){
		this.props.remove();
	},

	render(){

		var rootClass = classNames({
	      'discount__applied-screen': true,
	      'hide': !this.props.visible || !this.props.couponcode
	    });

		var discount_show;
    if(this.props.voucherAmount) {
      discount_show = (<div><i className="icon-inr"></i> <span className="analytics-discountvalue">{this.props.voucherAmount}</span></div>);
    } else {
      discount_show = (<div><i className="icon-inr"></i> <span className="analytics-discountvalue">{this.props.discount}</span></div>);
    }
		var removeLink = this.props.autopromo == false ? <a className="anchor" onClick={this.discountRemoveHandler} href="javascript:void(0)">remove</a> : "";

		var offerUrl = "/offers/#"+this.props.couponcode;
		return (
			<div className={rootClass}>
				<div className="details discount-coupon row">
					<div className="text-left discount__info col col-1">
						<div className="discount__info__coupon analytics-coupon">"{this.props.couponcode}" Code Applied</div>
						<div className="discount__info__subinfo analytics-coupon-desc">{this.props.couponDesc}</div>
					</div>
					<div className="text-right discount__value col col-2">
						<div>
							{discount_show}
						</div>
						<div> {removeLink}</div>
					</div>
				</div>
			</div>
		)
	}
});

var Loader = React.createClass({
	render(){

		var rootClass = classNames({
			'loader-container' : true,
			'loader-container--inline': true,
	      	'hide': !this.props.visible
	    });

		return (
			<div  className={rootClass}>
				<div className="loader-box v-center">
					<div className="sk-fading-circle small">
						<div className="sk-circle1 sk-circle"></div>
						<div className="sk-circle2 sk-circle"></div>
						<div className="sk-circle3 sk-circle"></div>
						<div className="sk-circle4 sk-circle"></div>
						<div className="sk-circle5 sk-circle"></div>
						<div className="sk-circle6 sk-circle"></div>
						<div className="sk-circle7 sk-circle"></div>
						<div className="sk-circle8 sk-circle"></div>
						<div className="sk-circle9 sk-circle"></div>
						<div className="sk-circle10 sk-circle"></div>
						<div className="sk-circle11 sk-circle"></div>
						<div className="sk-circle12 sk-circle"></div>
					</div>
				</div>
			</div>
		)
	}
});


var Discount = React.createClass({

	getInitialState(){
		return {
			autopromo:this.props.data.autopromo===true ? true : false,
			applyscreenvisible:true,
			appliedscreenvisible:this.props.data.couponcode==='' ? false : true,
			couponheadervisible: false,
			couponcode:this.props.data.couponcode,
			couponerror:'',
			discount:this.props.data.discount ? this.props.data.discount : 0,
			voucherAmount:this.props.data.voucherAmount ? this.props.data.voucherAmount : 0,
			couponDesc:this.props.data.couponDesc,
			loadervisible:false
		}
	},

	componentWillReceiveProps(nextProps){

		var newState = {
			applyscreenvisible:true,
			appliedscreenvisible:nextProps.data.couponcode==='' ? false : true,
			couponheadervisible: false,
			couponcode:nextProps.data.couponcode,
			couponerror:nextProps.data.discountError||'',
			discount:nextProps.data.discount ? nextProps.data.discount : 0,
			voucherAmount:nextProps.data.voucherAmount ? nextProps.data.voucherAmount : 0,
			couponDesc:nextProps.data.couponDesc
		}

		this.setState(_.extend({},this.state,newState));
	},

	showApplyCouponWidget(){
		this.setState({
			couponheadervisible:false,
			applyscreenvisible:true
		});
	},


	onApplySuccess(data){
		this.setState({
			appliedscreenvisible:true,
			applyscreenvisible:false,
			couponcode:data.couponcode,
			discount:data.discount,
			voucherAmount:data.voucherAmount,
			autopromo:data.autopromo,
			couponDesc:data.couponDesc
		});
	},

	onCouponRemove(){

		var self = this;
		this.setState({
			appliedscreenvisible:false,
			applyscreenvisible:true,
			couponcode:""
		});
		if(this.state.autopromo==false){
			this.applyCoupon("","remove");

		}
	},

	applyCoupon(couponcode,mode){

		var data = this.props.data.query_param;

		data['couponcode'] = (couponcode).trim();
		data['hotelid']=window.rt.hotelId;
		data['isDiscountCall']=true;

		var self = this;
		self.setState({loadervisible:true})

		var queryparams = $.param(data);
		treejax({
            url: self.props.data.url+queryparams,
            type: 'GET',
        }).then(function (data, status, xhr) {
			if(data.discountError){
				self.setState({ couponerror:"Sorry! This coupon code is not valid" });
			}else if(data.couponcode){
				self.onApplySuccess(data);
				$('body').trigger('discount:applied',{code:data.couponcode,value:data.discount, voucher_value:data.voucherAmount, priceobj:data});
			}

			if(mode && mode=="remove"){
				$('body').trigger('discount:removed',{value:data.discount, voucher_value:data.voucherAmount, priceobj:data});
			}

        },
        function (treeboError) {
            console.log(treeboError);
            self.setState({ couponerror:"Sorry! This coupon code is not valid" });
        }).finally(function(){
			self.setState({loadervisible:false})

		});
	},

	render(){
		return (
			<div className="discount pos-rel">
				<Loader visible={this.state.loadervisible}/>
				<CouponHeader visible={this.state.couponheadervisible} showApplyCouponWidget={this.showApplyCouponWidget}/>

				<ApplyScreen visible={this.state.applyscreenvisible} couponcode={this.state.couponcode} applyCoupon={this.applyCoupon} couponerror={this.state.couponerror}/>

				<AppliedScreen autopromo={this.state.autopromo} visible={this.state.appliedscreenvisible} couponcode={this.state.couponcode} remove={this.onCouponRemove} discount={this.state.discount} voucherAmount = {this.state.voucherAmount} couponDesc={this.state.couponDesc}/>
			</div>

		);
	}
});


module.exports = Discount;
