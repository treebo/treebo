import logging

from base.views.template import TreeboTemplateView
from data_services.respositories_factory import RepositoriesFactory
from services.restclient.contentservicerestclient import ContentServiceClient
from data_services.city_repository import CityRepository

__author__ = 'devang'

logger = logging.getLogger(__name__)

city_data_service = RepositoriesFactory.get_city_repository()


class Faq(TreeboTemplateView):
    template_name = "faq/index.html"
    redirect_mobile_to_desktop_view = False

    def getData(self, request, args, kwargs):
        seo = ContentServiceClient.getValueForKey(request, 'seo_pages', 1)

        cities = self.getCities()
        faqs = []
        policies = {}
        return {
            'faqs': faqs,
            'treebo_cities': cities,
            'policies': policies,
            'seo': seo['faq'],
            'scripts': ['desktop/js/faq.js'],
            'styles': ['desktop/css/faq.css'],
            'footerCopyrightDisabled': True}

    def getMobileData(self, request, args, kwargs):
        cities = self.getCities()
        return {
            'treebo_cities': cities,
            'scripts': ['mobile/js/faq.js'],
            'styles': ['mobile/css/faq.css']}

    def getCities(self):
        cities = [
            city.name for city in city_data_service.filter_cities(
                status=1)]
        return cities
