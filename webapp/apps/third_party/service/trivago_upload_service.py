import logging
import datetime
import os
from django.conf import settings
from django.core.mail import send_mail
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.files import File

from apps.third_party.models import TrivagoFileUploadModel
from apps.third_party.service.third_party_hotel_service import ThirdPartyHotelService
from apps.third_party.service.trivago_sftp_service import TrivagoSftpService
from common.constants import common_constants as const

logger = logging.getLogger(__name__)

TRIVAGO_UPLOAD_SAVE_TO_MODEL = False


class TrivagoFileUploaderService(object):

    @staticmethod
    def save_file_and_initiate_upload(uploaded_file):
        logger.info('Saving uploaded file')
        local_file_path = TrivagoFileUploaderService.fetch_save_url_controller(
            uploaded_file)
        status = TrivagoSftpService.initiate_sftp_upload(local_file_path, True)
        if not status == const.SUCCESS:
            raise Exception('Upload to trivago failed')

    @staticmethod
    def fetch_save_url_controller(uploaded_file):
        if TRIVAGO_UPLOAD_SAVE_TO_MODEL:
            trivago_upload_file = TrivagoFileUploadModel(
                trivago_file=uploaded_file)
            trivago_upload_file.save()
            save_url = trivago_upload_file.trivago_file.url
            local_file_path = settings.MEDIA_ROOT + save_url
        else:
            save_url = datetime.date.today().strftime('trivago/%Y/%m/%d')
            local_file_path = settings.MEDIA_ROOT + save_url
            TrivagoFileUploaderService.validate_path(local_file_path)
            local_file_path = local_file_path + '/' + uploaded_file.name
            TrivagoFileUploaderService.write_file_to_path(
                local_file_path, uploaded_file)
        return local_file_path

    @staticmethod
    def is_file_exits(file_path):
        return os.path.isfile(file_path)

    @staticmethod
    def validate_path(local_path):
        try:
            os.makedirs(local_path)
        except OSError:
            if not os.path.isdir(local_path):
                raise OSError

    @staticmethod
    def write_file_to_path(local_path, uploaded_file):
        logger.info('Writing file to path')
        with open(local_path, 'wb+') as destination:
            for chunk in uploaded_file.chunks():
                destination.write(chunk)

    @staticmethod
    def move_file_to_another_location(old_file_location, new_file_location):
        old_file = File(open(old_file_location, 'r'))
        TrivagoFileUploaderService.write_file_to_path(
            new_file_location, old_file)

    @staticmethod
    def get_filename_with_timestamp(old_filename):
        if old_filename:
            fname_arr = str(old_filename).split('.')
            if len(fname_arr) == 2:
                new_filename = fname_arr[0] + '_' + str(
                    datetime.datetime.now().isoformat()) + "_." + fname_arr[1]
                return new_filename
            else:
                return old_filename
        else:
            return old_filename

    @staticmethod
    def store_file(file_dir_path, file_name, file):
        file_location = file_dir_path + file_name
        TrivagoFileUploaderService.validate_path(file_dir_path)
        if TrivagoFileUploaderService.is_file_exits(file_location):
            logger.info(" file %s exits ", file_location)
            new_file_location = file_dir_path + \
                TrivagoFileUploaderService.get_filename_with_timestamp(file_name)
            TrivagoFileUploaderService.move_file_to_another_location(
                file_location, new_file_location)
        else:
            logger.info(" file %s doesn't exits ", file_location)
        TrivagoFileUploaderService.write_file_to_path(file_location, file)


class TrivagoAutomaticHotelUploadService(object):

    @staticmethod
    def automatic_hotel_detail_upload_to_trivago(url_prefix):
        logger.info('Automatic hotel upload trigerred')
        csv_file = ThirdPartyHotelService.fetch_hotel_data_in_csv(url_prefix)
        in_memory_file = TrivagoAutomaticHotelUploadService.convert_stringio_file_to_inmemory_file(
            csv_file)
        TrivagoFileUploaderService.save_file_and_initiate_upload(
            in_memory_file)

    @staticmethod
    def convert_stringio_file_to_inmemory_file(in_file):
        logger.info('Converting stringio file to inmemory file')
        in_file.seek(0, 2)
        in_memory_file = InMemoryUploadedFile(
            in_file,
            "trivagoFile",
            "hotel_inventory.csv",
            None,
            in_file.tell(),
            None)
        return in_memory_file

    @staticmethod
    def send_status_mail_on_inventory_upload(status=None):
        INVENTORY_UPLOAD_STATUS_EMAIL_LIST = ['thulasi@innoventes.co']
        if status == 'FAILED':
            message = 'Trivago inventory upload failed due to some error.'
        elif status == 'SUCCESS':
            message = 'Trivago inventory upload failed due to some error.'
        else:
            message = ''
            INVENTORY_UPLOAD_STATUS_EMAIL_LIST = []
        send_mail(
            subject='Trivago Hotel inventory Trigger ' + status,
            message=message,
            from_email=settings.SERVER_EMAIL,
            recipient_list=INVENTORY_UPLOAD_STATUS_EMAIL_LIST)
