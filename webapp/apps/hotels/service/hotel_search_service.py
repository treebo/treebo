import logging
from django.conf import settings
from dbcommon.models.hotel import Hotel
from apps.seo.services.common_services.sql_query_helper import SqlQueryHelper

logger = logging.getLogger(__name__)


class HotelSearchService():
    def __init__(self, city, category_name=None, amenity_name=None,
                 latitude=None, longitude=None, distance_cap=None):
        self.city = city
        self.category_name = category_name
        self.amenity_name = amenity_name
        self.latitude = latitude
        self.longitude = longitude
        if distance_cap is None:
            self.distance_cap = float(settings.DEFAULT_SEO_LANDMARK_RADIUS)
        else:
            self.distance_cap = distance_cap

    def search(self):
        if self.latitude and self.longitude:
            return self.search_hotels_with_radius()
        else:
            return self.search_hotels_with_city_name()

    def search_hotels_with_radius(self):
        distance_query_str = SqlQueryHelper.get_distance_query_for_param_str()
        distance_compare_str = distance_query_str % (self.latitude, self.latitude, self.longitude)
        sql_query = "select id, " + distance_compare_str + " as distance from hotels_hotel where " + \
                    distance_compare_str + " < " + str(self.distance_cap) + " and status = 1 " \
                    "order by " + distance_compare_str + " asc;"
        distance_sorted_hotel_list = SqlQueryHelper().execute_query_and_return_dict_with_distance(sql_query)

        distance_sorted_hotel_ids = [int(hotel[0]) for hotel in distance_sorted_hotel_list]
        hotels = Hotel.objects.filter(id__in=distance_sorted_hotel_ids)

        if self.amenity_name is not None:
            hotels = hotels.filter(facility__name__iexact=self.amenity_name)
        if self.category_name is not None:
            hotels = hotels.filter(category__name__iexact=self.category_name)

        hotel_objects_dict = dict([(hotel.id, hotel) for hotel in hotels])
        distance_sorted_hotels = []

        for hotel_distance_tupple in distance_sorted_hotel_list:
            hotel_id = hotel_distance_tupple[0]
            if hotel_id in hotel_objects_dict:
                hotel_objects_dict[hotel_id].distance = round(hotel_distance_tupple[1])
                distance_sorted_hotels.append(hotel_objects_dict[hotel_id])

        return distance_sorted_hotels

    def search_hotels_with_city_name(self):

        hotels = Hotel.objects.filter(city=self.city)
        if self.amenity_name is not None:
            hotels = hotels.filter(facility__name=self.amenity_name)
        if self.category_name is not None:
            hotels = hotels.filter(category__name=self.category_name)
        return hotels
