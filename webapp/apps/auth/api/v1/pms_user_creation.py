import logging

from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from webapp.apps.auth.dto.pms_user_creation_dto import PmsUserCreationDTO
from webapp.apps.auth.services.pms_user_creation_service import PmsUserService
from webapp.base.views.api import TreeboAPIView

logger = logging.getLogger(__name__)


class PmsUserCreationAPI(TreeboAPIView):
    """
            Create PMS User & Authz Mapping
            ---
            parameters:qw
                - name: first_name
                  description: First name of the user
                  required: true
                  type: string
                  paramType: form
                - name: last_name
                  description: Last name of the user
                  required: true
                  type: string
                  paramType: form
                - name: email
                  description: Email of the user
                  required: true
                  type: string
                  paramType: form
                - name: phone
                  description: Phone number of the user
                  required: true
                  type: string
                  paramType: form
                - name: hotel_id
                  description: Hotel ID where the user needs access
                  required: false
                  type: string
                  paramType: form
                - name: role
                  description: Role of the user
                  type: string
                  paramType: form
                  required: true
                - name: aom_email
                  description: Email of the AOM
                  required: false
                  type: string
                  paramType: form
            """

    def post(self, request):
        """

        :param rest_framework.request.Request request:
        :return:
        """
        try:
            pms_user_creation_dto = PmsUserCreationDTO(data=request.data)
            pms_user_creation_dto.is_valid()
            PmsUserService().create_user_and_assign_role(pms_user_creation_dto.data)
            response = Response({"status": "SUCCESS"}, status=200)
            return response
        except ValidationError as e:
            logger.info("ValidationError Occurred: %s", e)
            return Response({"error": {"msg": e.detail}}, status=400)
        except Exception as e:
            logger.info("Exception Occurred: %s", e)
            return Response({"error": {"msg": str(e)}}, status=500)
