# -*- coding: utf-8 -*-
# from b2b.constants import BookingAttributes
from apps.bookings.constants import CS_OTHER_SUB_CHANNEL
from apps.common.data_classes.base_data_class import BaseDataClass


class Channel(object):
    direct = 'direct'


class SubChannel(object):
    web = "web"
    android = "android"
    msite = "msite"


class Application(object):
    direct = "direct"


class Source(BaseDataClass):

    def __init__(self, channel, sub_channel, application):
        self.channel = channel
        self.application = application
        self.sub_channel = sub_channel

    @classmethod
    def from_booking_model(cls, booking_model):
        application = cls.application_from_model(booking_model)
        sub_channel = cls.sub_channel_from_model(booking_model)
        return cls(Channel.direct, sub_channel, application)

    @classmethod
    def sub_channel_from_model(cls, booking_model):
        return booking_model.get_booking_sub_channel()

    @classmethod
    def application_from_model(cls, booking_model):
        return booking_model.get_booking_application()

    def __repr__(self):
        return "<{kls}:{c},{sc},{a}>".format(kls=self.__class__.__name__,
                                             c=self.channel,
                                             sc=self.sub_channel,
                                             a=self.application
                                             )

    def __str__(self):
        return "<{c}- {sc}-{a}>".format(c=self.channel,
                                        sc=self.sub_channel,
                                        a=self.application
                                        )
