# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0022_auto_20160608_1040'),
    ]

    operations = [
        migrations.CreateModel(
            name='Otp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('mobile', models.CharField(max_length=20, verbose_name='mobile')),
                ('otp', models.CharField(max_length=8)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserReferral',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('userkey', models.CharField(unique=True, max_length=50, blank=True)),
                ('shareurl', models.CharField(unique=True, max_length=100, blank=True)),
                ('referral_code', models.CharField(unique=True, max_length=12, blank=True)),
                ('has_referrer', models.BooleanField()),
                ('share_link', models.CharField(max_length=50, null=True, blank=True)),
                ('friend_referral_code', models.CharField(max_length=12, null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='user',
            name='is_otp_verified',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='userreferral',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING),
        ),
    ]
