# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dbcommon', '0071_auto_20171031_1344'),
        ('discounts', '0016_auto_20170418_1253'),
    ]

    operations = [
        migrations.CreateModel(
            name='HotelCouponMapForAutoApply',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('coupon_code', models.CharField(max_length=200)),
                ('is_enabled', models.BooleanField(default=False)),
                ('start_date', models.DateField(default=datetime.date.today)),
                ('end_date', models.DateField(default=datetime.date.today)),
                ('hotel', models.ForeignKey(related_name='hotelcoupons', blank=True, to='dbcommon.Hotel', null=True, on_delete=models.DO_NOTHING)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
