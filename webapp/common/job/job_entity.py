import datetime

from ths_common.constants.scheduled_job_constants import JobStatus


class JobEntity:
    def __init__(self, job_name, crs_booking_id, data, eta, generated_at=None, job_id=None,
                 status=JobStatus.CREATED, picked_at=None, failure_message=None, total_tries=0):
        self.crs_booking_id = crs_booking_id
        self.data = data
        self.eta = eta
        self.generated_at = generated_at or datetime.datetime.now(datetime.timezone.utc)
        self.job_name = job_name
        self.job_id = job_id
        self.status = status
        self.picked_at = picked_at
        self.failure_message = failure_message
        self.total_tries = total_tries
