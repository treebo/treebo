from django.conf.urls import url
from apps.pricing.api.v6.cheapest_price_for_all_hotels import CheapestPriceAPIv6

app_name = 'pricing_v6'
urlpatterns = [url(r'^hotels/$',
                   CheapestPriceAPIv6.as_view(),
                   name='cheapest_prices_for_all_hotels_v6')
               ]
