import datetime
import logging

from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

from apps.growth.services.growth_services import GrowthServices
from dbcommon.models.profile import UserReferral, User, UserReferralReward

from apps.referral.service.service import ReferralService
from base.logging_decorator import log_args
from base.views.api import TreeboAPIView
from common.exceptions.treebo_exception import TreeboValidationException
from dbcommon.models.profile import UserReferralReward

logger = logging.getLogger(__name__)


class FriendRewardWebhook(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FriendRewardWebhook, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        raise MethodNotAllowed("GET")

    @log_args(logger)
    def post(self, request, format=None):
        """
        Handled based on payload sent from AppVirality
        https://github.com/appvirality/appvirality-sdk-android/wiki/Reward-Notifications#1-on-conversion-success-conversion-event-url-
        :param request:
        :param format:
        :return:
        """
        data = request.data
        secure_token = data.get('securekey', None)
        try:
            if secure_token is None or secure_token != settings.APPVIRALITY_SECURE_KEY:
                raise TreeboValidationException("Invalid request")
            self.handle_referee_reward(data)
        except TreeboValidationException as e:
            return Response({'message': str(e)},
                            status=HTTP_400_BAD_REQUEST)
        return Response({"msg": "Event handled"})

    @log_args(logger)
    def handle_referee_reward(self, data):
        referee = data.get("friend")
        referrer = data.get("referrer")
        amount = referee['amount']
        reward_unit = referee['reward_unit']
        coupon_code = referee['couponcode']
        reward_date = datetime.datetime.strptime(
            referee['rewarded_date'], '%d-%b-%Y')

        reward_detail_id = referee["rewarddtlid"]
        try:
            user = User.objects.get(email=referee["emailid"])
        except UserReferralReward.DoesNotExist:
            logger.error("Referee doesn't found %s", user.email)

        if coupon_code and coupon_code.lower() is not "none":
            ReferralService.send_referee_email_on_transaction(
                user.uuid, coupon_code, amount + " " + reward_unit)
            ReferralService.send_referee_sms_on_transaction(
                user.uuid, coupon_code, amount + " " + reward_unit)


class ReferrerRewardWebhook(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ReferrerRewardWebhook, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        raise MethodNotAllowed("GET")

    def post(self, request, format=None):
        """
        Handled based on payload sent from AppVirality
        https://github.com/appvirality/appvirality-sdk-android/wiki/Reward-Notifications#1-on-conversion-success-conversion-event-url-
        :param request:
        :param format:
        :return:
        """
        data = request.data
        secure_token = data.get('securekey', None)
        try:
            if secure_token is None or secure_token != settings.APPVIRALITY_SECURE_KEY:
                raise TreeboValidationException("Invalid request")
            self.handle_referrer_reward(data)
        except TreeboValidationException as e:
            return Response({'message': str(e)},
                            status=HTTP_400_BAD_REQUEST)
        return Response({"msg": "Event handled"})

    @log_args(logger)
    def handle_referrer_reward(self, data):
        referrer_id = data.get("storeuserid")
        amount = data.get("amount")
        reward_unit = data.get("reward_unit")
        coupon_code = data.get("couponcode")
        reward_date = datetime.datetime.strptime(
            data.get('rewarded_date'), '%d-%b-%Y')
        rewards = data.get("rewarddetails")
        for reward in rewards:
            reward_detail_id = reward["rewarddtlid"]
            try:
                user_referral_reward = ReferralService.get_referral_reward_details(
                    reward_detail_id)
            except UserReferralReward.DoesNotExist:
                logger.error(
                    "Referral reward %s doesn't exist",
                    reward_detail_id)
                continue
            ReferralService.save_referral_reward(
                referrer_id,
                amount + " " + reward_unit,
                coupon_code,
                UserReferralReward.ReferralEvents.Transaction,
                user_referral_reward.referee.uuid,
                reward_detail_id,
                reward_date)
            if coupon_code and coupon_code.lower() is not "none":
                referee = User.objects.get(uuid=referrer_id)
                referral = User.objects.get(
                    uuid=user_referral_reward.referee.uuid)
                growth_service = GrowthServices()
                growth_service.send_reward_communication_to_referee(
                    coupon_code, amount, referee, referral)
            break


class ConversionEventWebhook(TreeboAPIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ConversionEventWebhook, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        raise MethodNotAllowed("GET")

    def post(self, request, format=None):
        data = request.data
        event_name = data.get('eventname', None)
        secure_token = data.get('securekey', None)

        try:
            if event_name is None or secure_token is None or secure_token != settings.APPVIRALITY_SECURE_KEY:
                raise TreeboValidationException("Invalid request")

            if event_name == UserReferralReward.ReferralEvents.Transaction:
                self.handle_transaction_conversion(data)
        except TreeboValidationException as e:
            return Response(status=HTTP_400_BAD_REQUEST, data={
                "message": str(e)
            })
        return Response({"msg": "Event handled"})

    @log_args(logger)
    def handle_transaction_conversion(self, data):
        referrer = data.get("referrer", None)
        if referrer is None:
            raise TreeboValidationException(
                "No friend/referrer details passed")
        reward_id = referrer["rewarddtlid"]
        # ReferralService.approve_reward(reward_id)
        # ReferralService.distribute_reward(reward_id)
        ReferralService.save_referral_reward(
            referrer.get("storeuserid"),
            referrer["amount"] +
            " " +
            referrer["reward_unit"],
            referrer.get(
                "couponcode",
                ""),
            UserReferralReward.ReferralEvents.Transaction,
            data.get('friend').get('storeuserid'),
            reward_id,
            datetime.datetime.strptime(
                referrer['rewarded_date'],
                '%d-%b-%Y'))

    @log_args(logger)
    def handle_sign_up_conversion(self, data):
        friend = data.get("friend", None)
        referrer = data.get("referrer", None)
        if friend is None or referrer is None:
            raise TreeboValidationException(
                "No friend/referrer details passed")
        friend_id = friend.get("storeuserid")
        reward_id = friend.get("rewardid")

        # Now We are not giving any Reward coupon on signup
        # ReferralService.save_referral_reward(referrer.get('storeuserid'),
        #                                      friend["amount"] + " " + friend["reward_unit"],
        #                                      friend["couponcode"],
        #                                      UserReferralReward.ReferralEvents.Signup,
        #                                      friend_id,
        #                                      reward_id)
        # ReferralService.send_coupon_on_sign_up(friend_id, friend["couponcode"],
        #                                        friend["amount"] + " " + friend["reward_unit"],
        # referrer["name"], referrer["storeuserid"])
        ReferralService.send_email_on_sign_up(
            friend_id,
            friend["couponcode"],
            friend["amount"] +
            " " +
            friend["reward_unit"],
            referrer.get("storeuserid"))
        ReferralService.send_sms_on_sign_up(friend_id, referrer["storeuserid"])
