# pylint: disable-msg=no-member

import json
import logging

import requests
from django.conf import settings
from thsc.crs.entities.hotel import Hotel as CRSHotel

from apps.bookings.models import Booking
from apps.checkout.models import BookingRequest
from apps.common.exceptions.custom_exception import PublishEventToGrowthError
from apps.content.models import ContentStore
from apps.growth import constants
from apps.growth.constants import PARTPAY_CHANNEL_CODE
from apps.growth.integrations.treebo_realization import \
    TreeboRealizationClient as TreeboRealizationClientForCancellation
from apps.intelligent_pah.constants import GDC_SITE
from apps.intelligent_pah.services.occupancy_percentage import GetOccupancyPercentage
from apps.intelligent_pah.services.pah_services import PAHEnable
from apps.payments.integrations.treebo_realization import TreeboRealizationClient
from base.message_queues import partpay_update_publisher
from dbcommon.models.profile import User

logger = logging.getLogger(__name__)


class GrowthServices():

    @staticmethod
    def publish_upfront_part_payment_confirmation(payload_object):
        try:
            payload = payload_object.__dict__
            logger.info("Event being published to growth is:{payload}".format(payload=str(payload)))
            partpay_update_publisher.publish(settings.GROWTH_EVENT_TYPE_FOR_BOOKING_STATUS, payload)
        except Exception as e:
            message = "Exception While publishing the booking status to growth on upfront partpay " \
                      "with event payload:{payload} and event type:{event_type} due to:" \
                      "{exception}".format(payload=payload,
                                           event_type=settings.GROWTH_EVENT_TYPE_FOR_BOOKING_STATUS,
                                           exception=str(e)
                                           )
            logger.exception(message)
            raise PublishEventToGrowthError(message)

    def send_checkout_email(self, booking_detail):
        logger.info("Send Checkout email API called")

        headers = {'content-type': 'application/json'}
        booking_detail = json.dumps(booking_detail)
        response = requests.post(
            settings.GROWTH_REFERRAL_COMM_API_URL,
            data=booking_detail,
            headers=headers)
        logger.debug(
            "response received from Growth for Checkout email %s",
            response.json())
        if response.status_code == 200:
            data = response.json()
            if data['success']:
                logger.info("Checkout email sent for %s", booking_detail)
            else:
                logger.info("Checkout email sent for %s", booking_detail)

        else:
            logger.debug("Checkout email sent failed for %s", booking_detail)

    def send_reward_communication_to_referee(
            self, coupon_code, amount, referee, referral):
        logger.info("Send Reward email to Referee API called")

        reward_detail = {}
        reward_detail['user'] = 'referee'
        reward_detail['contact_email'] = referee.email
        reward_detail['contact_phone'] = referee.phone_number
        reward_detail['payload'] = {
            'referee_first_name': referee.first_name,
            'referral_first_name': referral.first_name,
            'coupon_code': coupon_code,
            'amount': amount
        }

        logger.info(
            "Calling Growth API for referee reward communication for %s",
            reward_detail)
        headers = {'content-type': 'application/json'}
        reward_detail = json.dumps(reward_detail)
        response = requests.post(
            settings.GROWTH_REFERRAL_COMM_API_URL,
            data=reward_detail,
            headers=headers)

        logger.info(
            "response received from Growth for referee reward communication %s",
            response.json())
        if response.status_code == 200:
            data = response.json()
            if data['success']:
                logger.info("Reward communication sent for %s", reward_detail)
            else:
                logger.info(
                    "Reward communication sent failed for %s",
                    reward_detail)
        else:
            logger.info(
                "Reward communication sent failed for %s",
                reward_detail)

    def get_booking_type(self, wrid, hotel_code):
        booking_type = constants.WEB_BOOKING
        try:
            booking_request = BookingRequest.objects.filter(
                order_id=wrid).first()
            if booking_request:
                user_details = User.objects.filter(
                    id=booking_request.user_id).first()
                if GDC_SITE.strip().lower() == str(
                        booking_request.meta_http_host).lower().strip():
                    booking_type = constants.GDC_BOOKING
                if user_details:
                    content_store_obj = ContentStore.objects.filter(
                        name=constants.CONTENT_STORE_TA_EMAIL_ID_NAME).first()
                    if content_store_obj:
                        content_store_val = str(
                            content_store_obj.value).split(',')
                        if user_details.email in content_store_val:
                            booking_type = constants.GDC_TRAVEL_AGENT_BOOKING
                    else:
                        logger.error(
                            "No key with %s exists in content store .. please make one ",
                            constants.CONTENT_STORE_TA_EMAIL_ID_NAME)
                else:
                    logger.error(
                        "No user with id %s exists",
                        booking_request.user_id)
            else:
                logger.error(
                    "No booking in booking request for wrid %s and hotel code %s exists",
                    wrid,
                    hotel_code)
        except Exception:
            logger.exception(
                "Exception while setting the booking_type key for partial payment API for wrid %s and hotel_code %s",
                wrid,
                hotel_code)
        return booking_type

    def generate_partial_payment_link(self, booking):

        try:
            if booking["paid_at_hotel"] and not booking['audit']:
                data = dict()
                data['wrid'] = booking['order_id']

                hotel_cs_id = booking.get('hotel_detail').get('hotel_cs_id')
                if settings.USE_CRS and CRSHotel.is_managed_by_crs(hotel_cs_id):
                    data['hotel_code'] = hotel_cs_id
                else:
                    data['hotel_code'] = booking.get('hotel_detail').get('hotel_code')

                data['email'] = booking['user_detail']['email']
                data['phone'] = booking['user_detail']['contact_number']
                data['name'] = booking['user_detail']['name']
                payment_detail = booking['payment_detail']
                total_amount = float(payment_detail['total'])
                data['balance'] = str(total_amount)
                data['booking_type'] = self.get_booking_type(
                    data['wrid'], data['hotel_code'])
                booking_request = BookingRequest.objects.filter(
                    order_id=data['wrid']).first()
                if booking_request:
                    getoccupancy = GetOccupancyPercentage()
                    try:
                        data['hotel_occupancy'] = int(
                            getoccupancy.get_occupancy_percentage_v2(
                                booking_request.hotel_id, booking_request.checkin_date, None))
                    except BaseException:
                        data['hotel_occupancy'] = 0
                else:
                    data['hotel_occupancy'] = 0
                data = json.dumps([data])
                logger.info("data to sent is %s", data)
                headers = {'content-type': 'application/json'}
                url = settings.GROWTH_PARTPAY_URL
                logger.info(
                    'Calling growth api to get partial payment link for booking %s',
                    booking)
                response = requests.post(url, data=data, headers=headers)
                response = response.json()
                logger.info('Response %s', response)
                if response['status'].lower() == 'success' and response['data']['status']:
                    payload = response['data']['payload'][0]
                    if payload['fields']['payment_status'].lower() == 'issued':
                        payment_url, amount_to_pay = payload['fields']['payment_url'], \
                            str(payload['fields']['amount_to_pay']).split('.')[0]
                        logger.debug(
                            'Part pay link and part pay amount from growth are %s, %s for booking %s',
                            payment_url,
                            amount_to_pay,
                            booking['order_id'])
                        return payment_url, amount_to_pay
                    if payload['fields']['payment_status'].lower() == 'paid':
                        return None, None
                else:
                    logger.info(
                        'Growth Partial payment api failed for wrid %s, and hotel code %s',
                        data['wrid'],
                        data['hotel_code'])
            return None, None
        except Exception:
            logger.exception(
                'Exception occured in getting partial payment link for booking %s',
                booking)
            return None, None

    def get_part_pay_amount(self, post_tax_amount, check_in, check_out, channel_code=PARTPAY_CHANNEL_CODE):
        try:
            realization_client = TreeboRealizationClient()
            part_pay_amount, part_pay_percent = realization_client.get_partpay_amount(post_tax_amount=post_tax_amount,
                                                                                      check_in=check_in,
                                                                                      check_out=check_out,
                                                                                      channel_code=channel_code)
            logger.debug("Partpay amount received from realization client: {part_pay_amount} and percentage:"
                         " {part_pay_percent}".format(part_pay_amount=part_pay_amount, part_pay_percent=part_pay_percent))
            return part_pay_amount, part_pay_percent
        except Exception:
            logger.exception("Exception while calling growth service for partpay amount")
            return 0, 0

    @staticmethod
    def is_part_pay_eligible(check_in, check_out, room_count, hotel_id, bid, channel):
        is_pah_enabled, _ = PAHEnable().pay_at_hotel_status(check_in=check_in, check_out=check_out,
                                                            room_count=room_count, hotel_id=hotel_id, bid=bid,
                                                            channel=channel)
        return not is_pah_enabled

    def booking_partpay_details(self, partpay_bookings_list):
        """
        return part pay link and part pay amount
        :param partpay_bookings_list:
        :return:
        """
        part_pay_link = ''
        part_pay_amount = ''
        try:
            partpay_bookings_list = self.get_partpay_link(partpay_bookings_list)
            if partpay_bookings_list:
                partpay_booking = partpay_bookings_list[0]
                logger.info("Partpay booking (%s)", partpay_booking)
                part_pay_link, part_pay_amount = partpay_booking['partial_payment_link'],\
                    partpay_booking['partial_payment_amount']

        except Exception:
            logger.exception(
                "Fetching part pay link and part pay amount failed for %s",
                partpay_bookings_list)
        logger.info(
            "Partpay link :%s , amount :%s",
            part_pay_link,
            part_pay_amount)
        return part_pay_link, part_pay_amount

    def get_partpay_link(self, bookings):
        """
        Fetch part pay link and part pay amount from growth
        :param bookings:
        :return:
        """
        try:
            payload = []
            crs_hotels = set()
            for booking in bookings:
                if booking['status'].lower() == Booking.RESERVED.lower():
                    hotel_cs_id = booking.get('hotel_detail').get('hotel_cs_id')
                    hotel_hx_id = booking.get('hotel_detail').get('hotel_code')
                    crs_hotels.add(hotel_hx_id)
                    hotel_code = hotel_cs_id
                    payload.append(dict(wrid=booking['order_id'], hotel_code=hotel_code))
            partpay_dict = {}
            if payload:
                data = json.dumps(payload)
                headers = {'content-type': 'application/json'}
                url = settings.GROWTH_PARTPAY_URL
                logger.info(
                    'Calling growth api for getting partial payment link')
                response = requests.post(url, data=data, headers=headers)
                response = response.json()
                logger.info('Response for part pay link %s', response)
                if response['status'].lower(
                ) == 'success' and response['data']['status'].lower() == 'success':
                    payload = response['data']['payload']
                    logger.info('Call success')
                    for item in payload:
                        if item['fields']['payment_status'].lower() == 'issued':
                            partpay_dict[(item['fields']['p_wrid'], item['fields']['p_hotel_code'])] = [
                                item['fields']['payment_url'],
                                str(item['fields']['amount_to_pay']).split('.')[0]]
                            logger.info(
                                "Partpay link :%s , partpay amount:%s", item['fields']['payment_url'], str(
                                    item['fields']['amount_to_pay']).split('.')[0])
                else:
                    logger.info('Growth Partial payment api call failed')
            for booking in bookings:
                hotel_hx_id = booking.get('hotel_detail').get('hotel_code')
                order_id = booking.get('order_id')
                hotel_cs_id = booking.get('hotel_detail').get('hotel_cs_id')

                if hotel_hx_id in crs_hotels and (order_id, hotel_cs_id) in partpay_dict:
                    booking['partial_payment_link'], booking['partial_payment_amount'] = \
                        partpay_dict.get((order_id, hotel_cs_id))
                    logger.info("Booking dict %s", booking)
                elif (order_id, hotel_hx_id) in partpay_dict:
                    booking['partial_payment_link'], booking['partial_payment_amount'] = \
                        partpay_dict.get((order_id, hotel_hx_id))
                    logger.info("Booking dict %s", booking)
                else:
                    booking['partial_payment_link'], booking['partial_payment_amount'] = None, None
                    logger.info("Booking dict %s", booking)
            logger.info(
                "Booking dict after getting partpay link and partpay amount %s",
                bookings)
            return bookings
        except Exception:
            logger.exception(
                'Exception occured while getting partpay link from growth')
            return bookings

    def booking_cancel_hash_url(self, bookings):
        """
            :param bookings list:
            :return: cancel hash url for a single booking
            """
        cancel_hash_url = None
        try:
            cancel_hash_list = self.get_cancel_url(bookings)
            if cancel_hash_list:
                cancel_hash_url_dict = cancel_hash_list[0]
                cancel_hash_url = cancel_hash_url_dict['cancel_hash_url']
                if cancel_hash_url is None:
                    cancel_hash_url = "NOT_GENERATED"
        except Exception:
            logger.exception(
                "Fetching cancel hash url failed for %s", bookings)
        return cancel_hash_url

    def get_cancel_url(self, bookings):
        hash_dict = TreeboRealizationClientForCancellation().get_cancellation_hash(bookings=bookings)
        for booking in bookings:
            if hash_dict.get(booking['order_id']):
                hash_value, hash_url = hash_dict.get(booking['order_id'])
                booking['cancel_hash'] = hash_value
                booking['cancel_hash_url'] = hash_url
                booking_request = BookingRequest.objects.filter(order_id=booking['order_id']).last()
                if not booking_request:
                    continue
                booking_request.cancel_hash_value = hash_value
                booking_request.save()
            else:
                booking['cancel_hash'] = None
                booking['cancel_hash_url'] = None
        return bookings

    def booking_exp_hash_url(self, wrid, hotel):
        booking = dict()
        booking['wrid'] = str(wrid)
        booking['hotel_code'] = str(hotel)
        try:
            data = json.dumps(booking)
            headers = {'content-type': 'application/json'}
            logger.info(
                "Calling growth api to get booking exp url with payload %s.",
                data)
            url = settings.GROWTH_BOOKING_EXP_URL
            response = requests.post(url, data=data, headers=headers)
            response = response.json()
            if response['status'] == 'failed':
                logger.info(
                    "Booking exp url creation is failed for wrid(%s) and hotel(%s).",
                    wrid,
                    hotel)
                return None, None, None, None, None
            logger.info("Booking exp url is %s for wrid(%s) and hotel(%s).", response['review_url_1'], wrid, hotel)
            return response['review_url_1'], response['review_url_2'], response['review_url_3'], response[
                'review_url_4'], response['review_url_5']
        except Exception:
            logger.exception(
                "Exception occured while calling growth API for Booking_exp_url(%s)",
                booking)
            return None, None, None, None, None
