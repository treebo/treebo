import logging

from kombu import Exchange, Queue
from kombu.common import Broadcast
from conf import log_conf
from .base import *
from conf.logging_config import LogConfig


ENVIRONMENT = 'prod'
DOMAIN = 'external'
FEATURE_TOGGLE_ENV = 'prod'
DEBUG = False
SEGMENT_DEBUG = False
DEVELOPMENT = False
TEMPLATE_DEBUG = False
PROWL_HOTEL_UPLOAD = 'http://api.treebohotels.com/prowl/rest/v1/hotels/add'
PROWL_HOTEL_DISABLE = 'http://api.treebohotels.com/prowl/rest/v1/hotels/disable'
APP_VIRALITY_PRIVATE_KEY = '47bb157cbe994fcab243a64400f3981d'
APP_VIRALITY_API_KEY = 'c5be69dff676467abaeda64400f39dd6'

SITE_ID = '4'

DYNAMIC_ADS_FILE_PATH = '/ebs1/dynamicads/'

CELERY_HMS_TASK_NAME = 'hms_sync_booking'

CELERY_BROADCAST_BOOKING_QUEUE = 'prod_broadcast_booking'
EXCHANGE = Exchange(CELERY_BROADCAST_BOOKING_QUEUE, type='fanout')

CELERY_CREATE_COUPON_TASK_NAME = "create_coupon_task"
CELERY_CREATE_COUPON_QUEUE = "celery_create_coupon"

CELERY_QUEUES = (
    Queue(CELERY_DEFAULT_QUEUE, Exchange(CELERY_DEFAULT_QUEUE), routing_key=CELERY_DEFAULT_QUEUE),
    Broadcast(CELERY_BROADCAST_BOOKING_QUEUE, routing_key='booking.all', exchange=EXCHANGE),
    Queue(CELERY_THIRD_PARTY_QUEUE, THIRD_PARTY_TRIGGER_EXCHANGE, routing_key=CELERY_THIRD_PARTY_QUEUE),
    Queue(CELERY_TRIVAGO_HOTEL_DETAILS, Exchange('trivago'), routing_key=CELERY_TRIVAGO_HOTEL_DETAILS),

)

CELERY_ROUTES = ('base.task_router.MyRouter',)

BASE_PRICING_HOST = 'http://pricing.treebohotels.com'
BASE_PRICING_URL_V3 = BASE_PRICING_HOST + "/v3/price?"
BASE_PRICING_URL_V2 = BASE_PRICING_HOST + "/v2/price?"
BASE_PRICING_URL_V1 = BASE_PRICING_HOST + "/v1/price?"

CONSUMER_KEY = "B39CEE50AC4B6925F9697409E2D84C88746593E5"
CONSUMER_SECRET = "9110CC27311F94542E7F5ACF3643B3909029F570"
HOTELOGIX_URL = "https://crs.staygrid.com/ws/web/"
HOTELOGIX_ADMIN_URL = "https://admin.staygrid.com/ws/web/"
BOOKING_DETAILS_URL = "https://crs.staygrid.com/ws/webv2/"

AVAILABILITY_HOOK_API = {
    'b2b_axis_room_hook_api': "http://corp-api.treebo.com/ext/api/daywiseInventory/"
}

SEGMENT_EVENTS_KEYS = {
    'website': 'WdZZ9zWWc1s8L8zkZc0SwrCCi8zzCywZ',
    'msite': 'WdZZ9zWWc1s8L8zkZc0SwrCCi8zzCywZ',
    'app': 'RcUV3VB55ldOiFD9lyvoG98UsBahDVSC'
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'treebo',
        'USER': 'treeboadmin',
        'PASSWORD': 'caTwimEx3',
        'HOST': 'pgb-web-mum.treebohotels.com',
        'PORT': '6432',
    }
}

STATIC_URL = '//static.treebohotels.com/dist/'
S3_URL = '//images.treebohotels.com/'
TREEBO_BASE_REST_HOST = TREEBO_WEB_URL

# PAYU PROD KEYS
PAYU_MERCHANT_ID = 'xBeWGL'
PAYU_SALT = 'cS9H6XPi'
PAYU_HANDLER_URL = TREEBO_BASE_URL + 'checkoutaction/'
PAYU_ACTION_URL = 'https://secure.payu.in/_payment'

PROWL_FOT_STATUS_UPDATE_URL = 'http://api.treebohotels.com/prowl/rest/v1/fotfeedback'
HMS_GET_ALL_RESERVATIONS_FOR_TODAY = 'http://api.treebohotels.com/hmssync/rest/v1/todaysreservations'

RAZORPAY_KEYS = "rzp_live_USOzRto8cqRWYo"
RAZORPAY_SECRET = 'p6CotYbheCsbIVuOL7yL8BI2'
RAZORPAY_API = "https://api.razorpay.com/v1/payments/"
LOG_REQUESTS = True

FACEBOOK_APP_ID = '836559819753145'
FACEBOOK_APP_SECRET = '1707372154cd7e164fa190861fda5848'

# Exclude list from tracking server responses - middleware : response.py
SERVER_STATUS_CAPTURE_FILTER = [200, 301, 302]

EMAIL_USE_TLS = True
EMAIL_HOST = 'email-smtp.eu-west-1.amazonaws.com'
EMAIL_HOST_USER = 'AKIAYZ7GE5SNPHU6ZJWY'
EMAIL_HOST_PASSWORD = 'BNLkNWuJPOSn2WIDtBdiNhl1jLb2LB6kNU7iqkv++7CZ'
EMAIL_PORT = 587
SERVER_EMAIL = 'Treebo Hotels <alerts.webbooking@treebohotels.com>'

NOTIFY_HMS_EMAIL = True

AWS_STORAGE_BUCKET_NAME = 'treebo'

# SEGMENT_EVENTS_KEY = 'WdZZ9zWWc1s8L8zkZc0SwrCCi8zzCywZ'

GTM_KEY = 'GTM-WZ2CGR'
MAPS_KEY = "AIzaSyCimC6LfTOoRHn9VWmzbx1dUR0bqheEdtE"
ADWORDS_CONVERSION_ID = 944747274
ADWORDS_CONVERSION_LABEL = "1X-NCNvc718Qiua-wgM"

CONTACTUS_AVAILABILITY_SUBJECT = 'Hotel room availability query'
CONTACTUS_CORPORATE_SUBJECT = 'Corporate travel agent query'
CONTACTUS_BOOKING_SUBJECT = 'Booking %(category)s query'
CONTACTUS_REFUND_SUBJECT = 'Refund related query'
CONTACTUS_CHECKINOUT_SUBJECT = 'Checkin checkout query'
CONTACTUS_OTHER_SUBJECT = 'Other query'

CONTACTUS_AVAILABILITY_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_CORPORATE_EMAIL_LIST = ['corporate@treebohotels.com', 'arunabh.sinha@treebohotels.com',
                                  'niharika.singh@treebohotels.com']
CONTACTUS_BOOKING_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_REFUND_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_CHECKINOUT_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']
CONTACTUS_OTHER_EMAIL_LIST = ['alerts.webbooking@treebohotels.com']

JOINUS_SUBJECT = 'Join us enquiry'
JOINUS_EMAIL_LIST = ['partners@treebohotels.com']
AVAILABILITY_FROM_BASIC_SEARCH = False
AVAILABILITY_DAY_LIMIT = 30

# BROKER_URL = 'amqp://treebo:GonejLyit@172.40.10.108:5672/rabbitmq'
BROKER_URL = 'amqp://guest:guest@10.20.1.54:5672/'
PRICING_BROKER_URL = BROKER_URL
#PRICING_BROKER_URL = 'amqp://guest:guest@pricing-prod-b-01-m.treebo.com:5672/'

GUEST_PREF_SUBJECT = 'Special Request Guest: %(guestname)s, Booking Id: %(bookingid)s, Hotel %(hotelname)s'
GUEST_PREF_EMAIL_LIST = ['guest.delight@treebohotels.com']
HOTELS_FEED_MAIL_LIST = [
    'ranavishal.singh@treebohotels.com',
    'shekhar.gupta@treebohotels.com',
    'rohit.jain@treebohotels.com']
BOOKING_REPORT_MAIL_LIST = [
    'ranavishal.singh@treebohotels.com',
    'shekhar.gupta@treebohotels.com',
    'rohit.jain@treebohotels.com']

DIRECT_TEAM = [
    'bibek.padhy@treebohotels.com',
    'varun.achar@treebohotels.com',
    'amitakhya.bhuyan@treebohotels.com',
    'gaurav.kinra@treebohotels.com',
    'aditya.shetty@treebohotels.com',
    'kapil.matani@treebohotels.com',
    'arun.midha@treebohotels.com',
    'aishwary.varshney@treebohotels.com',
    'rumani.kumari@treebohotels.com',
    'subidita.chaki@treebohotels.com',
    'vivek.gupta@treebohotels.com',
    'subham.das@treebohotels.com',
]

CANCELLATION_FAILURE_EMAIL_LIST = [] + DIRECT_TEAM

RESERVATION_FAILURE_EMAIL_LIST = [
    'gdcteam@treebohotels.com', 'escalations@treebohotels.com',
    'reservations@treebohotels.com',
    'khilan.haria@treebohotels.com',
] + DIRECT_TEAM

ADMINS = [
    ('Varun Achar',
     'varun.achar@treebohotels.com'),
    ('Web Alerts',
     'web.booking.alerts@treebohotels.com')]

LOG_PATH_VARIABLE = 'LOG_PATH'
LOG_ROOT = os.getenv(LOG_PATH_VARIABLE, '/var/log/website')

LOGGING = LogConfig(LOG_ROOT).get()

# Make celery to use the Django root logger config, instead of using its own
CELERYD_HIJACK_ROOT_LOGGER = False

from celery.signals import setup_logging


@setup_logging.connect
def configure_celery_log(sender=None, **kwargs):
    import logging.config
    logging.config.dictConfig(LOGGING)


HOTEL_UPLOAD_FILE_LOCATION = LOG_ROOT
SITE_HOST_NAME = TREEBO_WEB_URL + '/'

# Redis Configuration
REDIS_URL = 'treebo-prod-mum-elascache-01.treebo.com'
#REDIS_URL = 'localhost'
REDIS_PORT = '6379'
REDIS_DB = '4'
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        #'BACKEND': 'redis_cache.backends.dummy.RedisDummyCache',
        'LOCATION': 'redis://' + REDIS_URL + ':' + str(REDIS_PORT) + '/' + REDIS_DB,
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'SOCKET_CONNECT_TIMEOUT': 5,
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            },
            'PICKLE_VERSION': 2,
        }
    }
}

IMAGE_UPLOAD_PATH = '/ebs1/logs/image_upload/'
POS_URL = 'http://external.treebohotels.com/api/v1/external/hotelads/updateposfile.xml'
# Third party configs
TRIVAGO_CSV_LINK = '/usr/src/app/trivago.csv'
TRIVAGO_BID_FILE_PATH = '/var/data/trivago/'
TRANSACTION_URL = "http://external.treebohotels.com/api/v1/external/hotelads/transactionmessage.xml"
GHA_CSV_EMAIL_ID = ['websales@treebohotels.com']

# GHA Updated Files
FEATURE_POS_MAIL = True
GHA_REPORT_INTERVAL_DAYS = 14
TOOLS_TRANSACTION_URL = "http://tools.treebohotels.com/api/v1/external/hotelads/transactionmessage.xml"
TOOLS_POS_URL = 'http://tools.treebohotels.com/api/v1/external/hotelads/updateposfile.xml'
POS_EMAIL = ["websales@treebohotels.com", "corpbia@treebohotels.com"]
SAVE_EMAIL_API_FAILURE_EMAIL_ID = ["web@treebohotels.com"]

# partial payment growth api
GROWTH_PARTPAY_URL = 'http://growth.treebohotels.com/growth/partpay/web/link/'

# trip advisor
TA_CSV_EMAIL_ID = ["websales@treebohotels.com", "growth-team@treebohotels.com"]
CRITEO_CSV_EMAIL_ID = [
    "websales@treebohotels.com",
    "growth-team@treebohotels.com"]

# growth url
GROWTH_ADD_HOTEL_URL = "http://growth.treebohotels.com/growth/common_service/update_hotel_info/"

# GHA
FETCH_BOOKING_STATUS = "http://growth.treebohotels.com/growth/transformer/booking/status/?wrid="

GROWTH_REFERRAL_COMM_API_URL = "http://growth.treebohotels.com/growth/referral/communication"

# Auth configs
AUTH_SERVER_HOST = 'http://auth.treebo.com'
APP_CRENDENTIALS = {
    'password': {
        'client_id': 'dkQzsPZDU83jSpZeZ84WGbenu2UQ9H0WGfvy5lFW',
        'client_secret':
            'svjozF4pOa7iq6JnPe1nKtdteb0N30aJgqRzkIdO6QTYUl3SZgSBDbVORJTDfxxnOAtlS1iHD94dhEnU7ZWMhk3CEOCjtwD8YNOlVH8PHAyGY0yvU8mkZvMxMf1WwyFB'
    }
}

# for new hotels smtp
EMAIL_HOST_HOTEL_SMTP = 'smtp.gmail.com'
EMAIL_HOST_USER_HOTEL_SMTP = 'hello@treebohotels.com'
EMAIL_HOST_PASSWORD_HOTEL_SMTP = 'AdMinTreeBo@9932174'
EMAIL_PORT_HOTEL_SMTP = 465


# todo: verify this before prod. it's same as staging right now
HX_PROD = HxConfig(url='https://crs.staygrid.com/ws/web',
                   consumer_key="B39CEE50AC4B6925F9697409E2D84C88746593E5",
                   consumer_secret='9110CC27311F94542E7F5ACF3643B3909029F570',
                   counter_login='redutil@treebohotels.com',
                   counter_password='redemptionbot',
                   payment_title='Bill to Company (Treebo Collectible)')

HX_CONFIG = HX_PROD

BOOKING_CLIENT_TYPE = "HotelogixClient"

EASY_JOB_LITE_SETTINGS_PATH = os.path.join(
    EASY_JOB_LITE_SETTINGS_DIR, ENVIRONMENT + ".yaml")

# cancel hash urls
GROWTH_FETCH_BOOKING_DATA_URL = "http://growth.treebohotels.com/growth/cancellation/cancelbookingdetails/"
GROWTH_SUBMIT_BOOKING_URL = "http://growth.treebohotels.com/growth/cancellation/submitcancellationrequest/"
GROWTH_CANCEL_HASH_URL = "http://growth.treebohotels.com/growth/cancellation/cancelhash"
GROWTH_BOOKING_EXP_URL = "http://growth.treebohotels.com/growth/booking_exp/booking_exp_url/"

PROWL_HOTEL_POSITIVES_DATA_URL = "http://api.treebohotels.com/prowl/rest/v3/hotels/positivereviews/?hotelogix_code={0}&days={1}"
PAYMENT_SERVICE_HOST = 'https://payment.treebo.com/api'

NRP_ENABLED = True

# seo phase 2
GROWTH_FREQUENTLY_BOOKED_URL = 'http://growth.treebohotels.com/growth/transformer/frequently_booked/'

TREEBO_WEB_URL = "https://www.treebo.com"
TRIPADVISOR_BASE_URL = "https://www.tripadvisor.com/"
WEATHER_API_URL = 'https://api.darksky.net/forecast/c74c3f448f46294bd9e4e151966da2b2/'
WEATHER_API_URL_ALT = 'http://api.openweathermap.org/data/2.5/forecast/daily?units=imperial&type=accurate&mode=json&APPID=88ebea71050dfe7991d105af7fa2aa77&'
AB_SETTINGS = {
    'is_ab_activated': True,
    'ab_base_url': 'http://ab.treebo.com',
    'application_id': 'website',
    'cookies_enabled': False
}

CATALOGUE_SERVICE_RABBITMQ_SETTINGS = {
    'name': 'catalogue_service',
    'url': 'amqp://cmuser:M3b00@shared-p-rmq-cluster-02.treebo.pr:5672/catalog',
    'exchange': 'cs_exchange',
    'routing_key': 'com.cs.property',
}

CS_QUEUE_NAME = 'cs_queue_prod'

HEALTH_CHECK_CONF = dict(
    rmq_host=BROKER_URL,
    sqs_queue_name='queue_name',
    region_name='eu-west-1',
    aws_secret_access_key='mykey',
    aws_access_key_id='access_id',
    use_basic_db_check=True,
    soft_dependencies=[
        'CACHEHealthCheck',
        'RMQHealthCheck'])

HOTEL_POLICIES_EMAILS = [
    'ge@treebohotels.com',
    'gdcteam@treebohotels.com',
    'guest.delight@treebohotels.com']

AVAILABILITY_CORRECTION = 0


CATALOGUING_SERVICE_BASE_URL = "http://catalog.treebo.com/cataloging-service/"

# cataloguing apis
CATALOG_HOTEL_URL = 'http://catalog.treebo.com/cataloging-service/properties'
CATALOG_ROOMS_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/rooms/'
CATALOG_HOTEL_FACILITIES_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/amenities/'
CATALOG_ROOM_FACILITIES_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/rooms/{1}/amenities/'
CATALOG_BANQUET_HALL_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/banquet-halls/'
CATALOG_RESTAURANTS_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/restaurants/'
CATALOG_BARS_URL = 'http://catalog.treebo.com/cataloging-service/properties/{0}/bars/'