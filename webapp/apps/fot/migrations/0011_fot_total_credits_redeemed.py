# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fot', '0010_auto_20170215_0802'),
    ]

    operations = [
        migrations.AddField(
            model_name='fot',
            name='total_credits_redeemed',
            field=models.PositiveIntegerField(default=0, null=True),
        ),
    ]
